package com.miniclip.obsolete;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import com.google.android.gms.ads.AdRequest;
import com.miniclip.framework.AbstractActivityListener;
import com.miniclip.framework.Miniclip;
import com.miniclip.framework.MiniclipAndroidActivity;
import com.miniclip.framework.ThreadingContext;
import com.mopub.common.MoPub;
import com.mopub.common.SdkConfiguration;
import com.mopub.common.SdkInitializationListener;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubView;
import com.tapjoy.Tapjoy;
import com.tapjoy.TapjoyAuctionFlags;

public class Obsolete {
    /* access modifiers changed from: private */
    public static MiniclipAndroidActivity activity = null;
    public static RelativeLayout adLayout = null;
    public static RelativeLayout adLayoutH = null;
    /* access modifiers changed from: private */
    public static ObsoleteAdListener adListener = null;
    /* access modifiers changed from: private */
    public static MoPubViewWrapper adViewGame = null;
    /* access modifiers changed from: private */
    public static MoPubViewWrapper adViewMenu = null;
    /* access modifiers changed from: private */
    public static boolean isInitialized = false;
    private static Handler mAdDelayHandler = new Handler();
    protected static boolean mConstantAd = true;
    protected static Context mContext = null;
    public static float mDensity = 0.0f;
    protected static boolean mHAS_RETINA = true;
    public static int mHeight = 0;
    private static Runnable mHideAdsRun = new Runnable() {
        public void run() {
            Obsolete.hideAds();
        }
    };
    protected static float mINGAME_HEIGHT = 320.0f;
    public static boolean mINGAME_LANDSCAPE = true;
    protected static boolean mINGAME_SCALE = false;
    protected static float mINGAME_WIDTH = 480.0f;
    public static boolean mUSE_ADS = true;
    protected static float mUSE_ADS_HORIZONTAL_BANNER_OFFSET;
    protected static float mUSE_ADS_VERTICAL_BANNER_OFFSET;
    public static int mWidth;
    private AdRequest.Builder adRequest;

    public static native void MadClicked();

    public static native void MsetAdLoadFailed(int i);

    public static String getMoPubBannerId() {
        return "427bd0cb8c354a6d89f7e9f01c1b3660";
    }

    private static class ObsoleteActivityListener extends AbstractActivityListener {
        private ObsoleteActivityListener() {
        }
    }

    private static class ObsoleteAdListener implements MoPubView.BannerAdListener {
        public void onBannerCollapsed(MoPubView moPubView) {
        }

        public void onBannerExpanded(MoPubView moPubView) {
        }

        private ObsoleteAdListener() {
        }

        public void onBannerLoaded(final MoPubView moPubView) {
            if (Obsolete.mUSE_ADS) {
                ((Activity) Obsolete.mContext).runOnUiThread(new Runnable() {
                    public void run() {
                        MoPubViewWrapper access$000 = Obsolete.adViewMenu;
                        if (moPubView.getAdUnitId().compareTo(Obsolete.getMoPubBannerId()) == 0) {
                            access$000 = Obsolete.adViewGame;
                        }
                        if (Build.VERSION.SDK_INT > 10) {
                            access$000.setLayerType(1, null);
                        }
                        if (access$000 != null && moPubView == access$000 && access$000.getVisibility() == 0) {
                            ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f, 160.0f * Obsolete.mDensity, 0.0f);
                            scaleAnimation.setDuration(500);
                            access$000.setVisibility(1);
                            access$000.startAnimation(scaleAnimation);
                        }
                    }
                });
                Obsolete.activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
                    public void run() {
                        Obsolete.MsetAdLoadFailed(0);
                    }
                });
            }
        }

        public void onBannerFailed(MoPubView moPubView, MoPubErrorCode moPubErrorCode) {
            if (Obsolete.mUSE_ADS) {
                Log.i("OnAdFailed", "Failed to load the ad");
                Obsolete.activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
                    public void run() {
                        Obsolete.MsetAdLoadFailed(1);
                    }
                });
            }
        }

        public void onBannerClicked(MoPubView moPubView) {
            if (Obsolete.mUSE_ADS) {
                Log.i("OnAdClicked", "Ad Clicked");
                Obsolete.activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
                    public void run() {
                        Obsolete.MadClicked();
                    }
                });
            }
        }
    }

    public static void init(MiniclipAndroidActivity miniclipAndroidActivity) {
        if (!isInitialized) {
            activity = miniclipAndroidActivity;
            mContext = activity;
            activity.addListener(new ObsoleteActivityListener());
            adListener = new ObsoleteAdListener();
            mWidth = activity.getResources().getDisplayMetrics().widthPixels;
            mHeight = activity.getResources().getDisplayMetrics().heightPixels;
            mDensity = activity.getResources().getDisplayMetrics().density;
            Log.i("Dim", String.format("width: %d, height: %d, density: %f", Integer.valueOf(mWidth), Integer.valueOf(mHeight), Float.valueOf(mDensity)));
            Tapjoy.connect(mContext, "-hz2NeJaSmiiTDx4IqxC_AECdn3tq2MowB2t6CZBzFODa8ct_NmR3AgrBp5z", null, null);
            if (mUSE_ADS) {
                adViewGame = null;
                adViewMenu = null;
            }
            adLayout = new RelativeLayout(activity);
            adLayout.setMinimumWidth(mWidth);
            adLayout.setMinimumHeight(mHeight);
            adLayoutH = new RelativeLayout(activity);
            adLayoutH.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            adLayoutH.addView(adLayout);
            activity.getMainLayout().addView(adLayoutH);
            initializeMopub();
        }
    }

    private static void initializeMopub() {
        MoPub.initializeSdk(activity, new SdkConfiguration.Builder(getMoPubBannerId()).build(), initSdkListener());
    }

    private static SdkInitializationListener initSdkListener() {
        return new SdkInitializationListener() {
            public void onInitializationFinished() {
                boolean unused = Obsolete.isInitialized = true;
                new AdRequest.Builder().addTestDevice("45A4481CC8CDD33561AE709FFB885280").build();
                Log.i("Mopub", "onInitializationFinished");
            }
        };
    }

    protected static int getAdsDisabled(Context context) {
        if (context.getSharedPreferences("ADS_DISABLED", 0).getString("disabled", "false").equals("true")) {
            return 1;
        }
        return 0;
    }

    public static void setAdsConsent(boolean z) {
        if (z) {
            Tapjoy.setUserConsent(TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE);
            MoPub.getPersonalInformationManager().grantConsent();
            return;
        }
        Tapjoy.setUserConsent("0");
        MoPub.getPersonalInformationManager().revokeConsent();
    }

    public static int getAdsDisabled() {
        return getAdsDisabled(mContext);
    }

    protected static void setAdsDisabled(int i) {
        SharedPreferences.Editor edit = mContext.getSharedPreferences("ADS_DISABLED", 0).edit();
        if (i == 1) {
            edit.putString("disabled", "true");
        } else {
            edit.putString("disabled", "false");
        }
        edit.commit();
    }

    public static void enableAds() {
        showAds();
    }

    public static void enableAdsWithPosition(float f, float f2, float f3, float f4, float f5, String str) {
        if (mUSE_ADS) {
            final float f6 = f5;
            final float f7 = f;
            final float f8 = f3;
            final float f9 = f2;
            final float f10 = f4;
            final String str2 = str;
            ((Activity) mContext).runOnUiThread(new Runnable() {
                public void run() {
                    char c;
                    final MoPubViewWrapper moPubViewWrapper;
                    MoPubViewWrapper access$000;
                    MoPubViewWrapper access$0002;
                    if (!(Obsolete.adLayout == null || Obsolete.adLayout.getParent() == Obsolete.adLayoutH)) {
                        Obsolete.adLayoutH.addView(Obsolete.adLayout);
                    }
                    final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (320.0d * ((double) Obsolete.mDensity)), (int) (50.0d * ((double) Obsolete.mDensity)));
                    float f = Obsolete.mINGAME_WIDTH;
                    float f2 = Obsolete.mINGAME_HEIGHT;
                    if (f6 == 0.0f) {
                        if (Obsolete.mINGAME_LANDSCAPE && !Obsolete.mINGAME_SCALE) {
                            f = (Obsolete.mINGAME_HEIGHT / ((float) Obsolete.mHeight)) * ((float) Obsolete.mWidth);
                        } else if (!Obsolete.mINGAME_SCALE) {
                            f2 = (Obsolete.mINGAME_WIDTH / ((float) Obsolete.mWidth)) * ((float) Obsolete.mHeight);
                        }
                        if ((Obsolete.mWidth > 799 || Obsolete.mHeight > 799) && Obsolete.mHAS_RETINA) {
                            f *= 1.0f;
                            f2 *= 1.0f;
                        }
                    } else {
                        f = f6;
                    }
                    layoutParams.leftMargin = ((int) ((f7 * (((float) Obsolete.mWidth) / f)) - (f8 * (320.0f * Obsolete.mDensity)))) + ((int) (Obsolete.mUSE_ADS_VERTICAL_BANNER_OFFSET * Obsolete.mDensity));
                    layoutParams.topMargin = ((int) ((f9 * (((float) Obsolete.mHeight) / f2)) - ((1.0f - f10) * (50.0f * Obsolete.mDensity)))) + ((int) (Obsolete.mUSE_ADS_HORIZONTAL_BANNER_OFFSET * Obsolete.mDensity));
                    if (str2.compareTo(Obsolete.getMoPubBannerId()) == 0) {
                        moPubViewWrapper = Obsolete.adViewGame;
                        Obsolete.hideAd(Obsolete.adViewMenu);
                        c = 1;
                    } else {
                        moPubViewWrapper = Obsolete.adViewMenu;
                        Obsolete.hideAd(Obsolete.adViewGame);
                        c = 2;
                    }
                    if (moPubViewWrapper == null || moPubViewWrapper.getParent() != Obsolete.adLayout || !moPubViewWrapper.isShown() || !moPubViewWrapper.AdLoaded() || str2.compareTo(moPubViewWrapper.getAdUnitId()) != 0) {
                        if (!Obsolete.mConstantAd) {
                            if (moPubViewWrapper != null) {
                                Obsolete.adLayout.removeView(moPubViewWrapper);
                                moPubViewWrapper.destroy();
                            }
                            if (c == 1) {
                                MoPubViewWrapper unused = Obsolete.adViewGame = new MoPubViewWrapper(Obsolete.mContext);
                                access$000 = Obsolete.adViewGame;
                            } else {
                                MoPubViewWrapper unused2 = Obsolete.adViewMenu = new MoPubViewWrapper(Obsolete.mContext);
                                access$000 = Obsolete.adViewMenu;
                            }
                            moPubViewWrapper.setBannerAdListener(Obsolete.adListener);
                            moPubViewWrapper.setAdUnitId(str2);
                            moPubViewWrapper.setVisibility(0);
                            moPubViewWrapper.loadAd();
                        } else if (moPubViewWrapper == null) {
                            if (c == 1) {
                                MoPubViewWrapper unused3 = Obsolete.adViewGame = new MoPubViewWrapper(Obsolete.mContext);
                                access$0002 = Obsolete.adViewGame;
                            } else {
                                MoPubViewWrapper unused4 = Obsolete.adViewMenu = new MoPubViewWrapper(Obsolete.mContext);
                                access$0002 = Obsolete.adViewMenu;
                            }
                            moPubViewWrapper.setBannerAdListener(Obsolete.adListener);
                            moPubViewWrapper.setAdUnitId(str2);
                            moPubViewWrapper.setVisibility(0);
                            moPubViewWrapper.loadAd();
                        } else {
                            Obsolete.adLayout.removeView(moPubViewWrapper);
                        }
                        Obsolete.adLayout.addView(moPubViewWrapper, layoutParams);
                        if (moPubViewWrapper.AdLoaded()) {
                            ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f, 160.0f * Obsolete.mDensity, 0.0f);
                            scaleAnimation.setDuration(500);
                            moPubViewWrapper.setVisibility(1);
                            moPubViewWrapper.startAnimation(scaleAnimation);
                            return;
                        }
                        return;
                    }
                    RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) moPubViewWrapper.getLayoutParams();
                    if (layoutParams2.leftMargin != layoutParams.leftMargin || layoutParams2.topMargin != layoutParams.topMargin) {
                        ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.0f, 0.0f, 1.0f, 1.0f, 160.0f * Obsolete.mDensity, 0.0f);
                        scaleAnimation2.setDuration(500);
                        scaleAnimation2.setAnimationListener(new Animation.AnimationListener() {
                            public void onAnimationRepeat(Animation animation) {
                            }

                            public void onAnimationStart(Animation animation) {
                            }

                            public void onAnimationEnd(Animation animation) {
                                ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f, 160.0f * Obsolete.mDensity, 0.0f);
                                scaleAnimation.setDuration(500);
                                moPubViewWrapper.setLayoutParams(layoutParams);
                                moPubViewWrapper.startAnimation(scaleAnimation);
                            }
                        });
                        moPubViewWrapper.startAnimation(scaleAnimation2);
                    }
                }
            });
        }
    }

    public static void enableAdsWithPositionForGameplay(float f, float f2, float f3, float f4) {
        enableAdsWithPosition(f, f2, f3, f4, 0.0f, getMoPubBannerId());
    }

    public static void enableAdsWithPositionForGameplayGivenWidth(float f, float f2, float f3, float f4, float f5) {
        enableAdsWithPosition(f, f2, f3, f4, f5, getMoPubBannerId());
    }

    public static void enableAdsWithPositionForMenu(float f, float f2, float f3, float f4) {
        enableAdsWithPosition(f, f2, f3, f4, 0.0f, getMoPubBannerId());
    }

    public static void enableAdsWithPositionForMenuGivenWidth(float f, float f2, float f3, float f4, float f5) {
        enableAdsWithPosition(f, f2, f3, f4, f5, getMoPubBannerId());
    }

    public static void disableAds() {
        hideAds();
    }

    public static void hideAds() {
        ((Activity) mContext).runOnUiThread(new Runnable() {
            public void run() {
                if (Obsolete.adLayout != null && Obsolete.adLayout.getParent() == Obsolete.adLayoutH) {
                    Obsolete.adLayoutH.removeView(Obsolete.adLayout);
                }
                Obsolete.hideAd(Obsolete.adViewGame);
                Obsolete.hideAd(Obsolete.adViewMenu);
            }
        });
    }

    public static void hideAd(final MoPubView moPubView) {
        if (moPubView != null && moPubView.getParent() == adLayout && moPubView.getAnimation() == null) {
            ((Activity) mContext).runOnUiThread(new Runnable() {
                public void run() {
                    ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f, 1.0f, 160.0f * Obsolete.mDensity, 0.0f);
                    scaleAnimation.setDuration(500);
                    scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
                        public void onAnimationRepeat(Animation animation) {
                        }

                        public void onAnimationStart(Animation animation) {
                        }

                        public void onAnimationEnd(Animation animation) {
                            if (moPubView != null) {
                                Obsolete.adLayout.removeView(moPubView);
                            }
                        }
                    });
                    Obsolete.adLayout.removeView(moPubView);
                }
            });
        }
    }

    public static void hideAdsInSeconds(int i) {
        mAdDelayHandler.removeCallbacks(mHideAdsRun);
        mAdDelayHandler.postDelayed(mHideAdsRun, (long) (i * 1000));
    }

    public static void showAds() {
        Log.i("showAds", "deprecated use enableAdsWithPosition");
    }

    public static void showAd(final MoPubView moPubView) {
        if (mUSE_ADS && moPubView != null) {
            ((Activity) mContext).runOnUiThread(new Runnable() {
                public void run() {
                    if (moPubView != null) {
                        Obsolete.adLayout.removeView(moPubView);
                        Obsolete.adLayout.addView(moPubView);
                    }
                }
            });
        }
    }

    public static void permanentlyRemoveAds() {
        ((Activity) mContext).runOnUiThread(new Runnable() {
            public void run() {
                Obsolete.setAdsDisabled(1);
                Obsolete.disableAds();
                Obsolete.mUSE_ADS = false;
            }
        });
    }

    public static void StartActivity(String str) {
        Intent launchIntentForPackage = activity.getPackageManager().getLaunchIntentForPackage(str);
        if (launchIntentForPackage == null) {
            launchIntentForPackage = new Intent("android.intent.action.VIEW");
            launchIntentForPackage.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + str));
        }
        activity.startActivity(launchIntentForPackage);
    }

    public static boolean isFirstInstall() {
        Activity activity2 = Miniclip.getActivity();
        try {
            if (activity2.getPackageManager().getPackageInfo(activity2.getPackageName(), 0).firstInstallTime == activity2.getPackageManager().getPackageInfo(activity2.getPackageName(), 0).lastUpdateTime) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }
}
