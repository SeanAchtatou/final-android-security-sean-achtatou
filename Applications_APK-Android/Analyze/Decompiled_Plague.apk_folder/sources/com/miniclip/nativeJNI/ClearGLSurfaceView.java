package com.miniclip.nativeJNI;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import com.miniclip.nativeJNI.GLSurfaceViewProfile;
import com.miniclip.platform.MCViewManager;

public class ClearGLSurfaceView extends GLSurfaceViewProfile {
    Context mContext;
    ClearRenderer mRenderer;

    public ClearGLSurfaceView(Context context) {
        super(context);
        this.mContext = context;
        if (cocojava.mUSE_PRESERVE_CONTEXT && cocojava.mHAS_RETINA && MCViewManager.displayWidth > 799) {
            setPreserveEGLContextOnPause(true);
            CocoJNI.MpreserveContext(1);
        }
    }

    public void setRenderer(GLSurfaceViewProfile.Renderer renderer) {
        this.mRenderer = (ClearRenderer) renderer;
        super.setRenderer(renderer);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        final int[] iArr = new int[pointerCount];
        final float[] fArr = new float[pointerCount];
        final float[] fArr2 = new float[pointerCount];
        for (int i = 0; i < pointerCount; i++) {
            iArr[i] = motionEvent.getPointerId(i);
            fArr[i] = motionEvent.getX(i);
            fArr2[i] = motionEvent.getY(i);
        }
        switch (motionEvent.getAction() & 255) {
            case 0:
                final int pointerId = motionEvent.getPointerId(0);
                final float f = fArr[0];
                final float f2 = fArr2[0];
                queueEvent(new Runnable() {
                    public void run() {
                        ClearGLSurfaceView.this.mRenderer.handleActionDown(pointerId, f, f2);
                    }
                });
                return true;
            case 1:
                final int pointerId2 = motionEvent.getPointerId(0);
                final float f3 = fArr[0];
                final float f4 = fArr2[0];
                queueEvent(new Runnable() {
                    public void run() {
                        ClearGLSurfaceView.this.mRenderer.handleActionUp(pointerId2, f3, f4);
                    }
                });
                return true;
            case 2:
                queueEvent(new Runnable() {
                    public void run() {
                        ClearGLSurfaceView.this.mRenderer.handleActionMove(iArr, fArr, fArr2);
                    }
                });
                return true;
            case 3:
                queueEvent(new Runnable() {
                    public void run() {
                        ClearGLSurfaceView.this.mRenderer.handleActionCancel(iArr, fArr, fArr2);
                    }
                });
                return true;
            case 4:
            default:
                return true;
            case 5:
                int action = motionEvent.getAction() >> 8;
                final int pointerId3 = motionEvent.getPointerId(action);
                final float x = motionEvent.getX(action);
                final float y = motionEvent.getY(action);
                queueEvent(new Runnable() {
                    public void run() {
                        ClearGLSurfaceView.this.mRenderer.handleActionDown(pointerId3, x, y);
                    }
                });
                return true;
            case 6:
                int action2 = motionEvent.getAction() >> 8;
                final int pointerId4 = motionEvent.getPointerId(action2);
                final float x2 = motionEvent.getX(action2);
                final float y2 = motionEvent.getY(action2);
                queueEvent(new Runnable() {
                    public void run() {
                        ClearGLSurfaceView.this.mRenderer.handleActionUp(pointerId4, x2, y2);
                    }
                });
                return true;
        }
    }

    private void dumpEvent(MotionEvent motionEvent) {
        int i = 0;
        String[] strArr = {"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"};
        StringBuilder sb = new StringBuilder();
        int action = motionEvent.getAction();
        int i2 = action & 255;
        if (i2 != 2) {
            sb.append("event ACTION_");
            sb.append(strArr[i2]);
            if (i2 == 5 || i2 == 6) {
                sb.append("(pid ");
                sb.append(action >> 8);
                sb.append(")");
            }
            sb.append("[");
            while (i < motionEvent.getPointerCount()) {
                sb.append("#");
                sb.append(i);
                sb.append("(pid ");
                sb.append(motionEvent.getPointerId(i));
                sb.append(")=");
                sb.append((int) motionEvent.getX(i));
                sb.append(",");
                sb.append((int) motionEvent.getY(i));
                i++;
                if (i < motionEvent.getPointerCount()) {
                    sb.append(";");
                }
            }
            sb.append("]");
            Log.i("ClearGLSurefaceView", sb.toString());
        }
    }

    public void onPause() {
        queueEvent(new Runnable() {
            public void run() {
                ClearGLSurfaceView.this.mRenderer.handleOnPause();
            }
        });
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        queueEvent(new Runnable() {
            public void run() {
                ClearGLSurfaceView.this.mRenderer.queueEvent(new Runnable() {
                    public void run() {
                        ClearGLSurfaceView.this.mRenderer.handleOnResume();
                    }
                });
            }
        });
    }
}
