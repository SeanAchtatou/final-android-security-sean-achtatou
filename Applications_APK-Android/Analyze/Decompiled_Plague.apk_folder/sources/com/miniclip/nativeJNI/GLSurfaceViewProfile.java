package com.miniclip.nativeJNI;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.io.Writer;
import java.util.ArrayList;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

public class GLSurfaceViewProfile extends SurfaceView implements SurfaceHolder.Callback {
    public static final int DEBUG_CHECK_GL_ERROR = 1;
    public static final int DEBUG_LOG_GL_CALLS = 2;
    private static final boolean DRAW_TWICE_AFTER_SIZE_CHANGED = true;
    private static final boolean LOG_EGL = false;
    private static final boolean LOG_PAUSE_RESUME = false;
    private static final boolean LOG_RENDERER = false;
    private static final boolean LOG_RENDERER_DRAW_FRAME = false;
    private static final boolean LOG_SURFACE = false;
    private static final boolean LOG_THREADS = false;
    public static final int RENDERMODE_CONTINUOUSLY = 1;
    public static final int RENDERMODE_WHEN_DIRTY = 0;
    /* access modifiers changed from: private */
    public static final GLThreadManager sGLThreadManager = new GLThreadManager();
    private int mDebugFlags;
    /* access modifiers changed from: private */
    public EGLConfigChooser mEGLConfigChooser;
    /* access modifiers changed from: private */
    public int mEGLContextClientVersion;
    /* access modifiers changed from: private */
    public EGLContextFactory mEGLContextFactory;
    /* access modifiers changed from: private */
    public EGLWindowSurfaceFactory mEGLWindowSurfaceFactory;
    /* access modifiers changed from: private */
    public GLThread mGLThread;
    /* access modifiers changed from: private */
    public GLWrapper mGLWrapper;
    /* access modifiers changed from: private */
    public boolean mPreserveEGLContextOnPause;
    /* access modifiers changed from: private */
    public boolean mSizeChanged = true;

    public interface EGLConfigChooser {
        EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay);
    }

    public interface EGLContextFactory {
        EGLContext createContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig);

        void destroyContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLContext eGLContext);
    }

    public interface EGLWindowSurfaceFactory {
        EGLSurface createWindowSurface(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, Object obj);

        void destroySurface(EGL10 egl10, EGLDisplay eGLDisplay, EGLSurface eGLSurface);
    }

    public interface GLWrapper {
        GL wrap(GL gl);
    }

    public interface Renderer {
        void onDrawFrame(GL10 gl10);

        void onSurfaceChanged(GL10 gl10, int i, int i2);

        void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig);
    }

    public GLSurfaceViewProfile(Context context) {
        super(context);
        init();
    }

    public GLSurfaceViewProfile(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        getHolder().addCallback(this);
    }

    public void setGLWrapper(GLWrapper gLWrapper) {
        this.mGLWrapper = gLWrapper;
    }

    public void setDebugFlags(int i) {
        this.mDebugFlags = i;
    }

    public int getDebugFlags() {
        return this.mDebugFlags;
    }

    public void setPreserveEGLContextOnPause(boolean z) {
        this.mPreserveEGLContextOnPause = z;
    }

    public boolean getPreserveEGLContextOnPause() {
        return this.mPreserveEGLContextOnPause;
    }

    public void setRenderer(Renderer renderer) {
        checkRenderThreadState();
        if (this.mEGLConfigChooser == null) {
            this.mEGLConfigChooser = new SimpleEGLConfigChooser(true);
        }
        if (this.mEGLContextFactory == null) {
            this.mEGLContextFactory = new DefaultContextFactory();
        }
        if (this.mEGLWindowSurfaceFactory == null) {
            this.mEGLWindowSurfaceFactory = new DefaultWindowSurfaceFactory();
        }
        this.mGLThread = new GLThread(renderer);
        this.mGLThread.start();
    }

    public void setEGLContextFactory(EGLContextFactory eGLContextFactory) {
        checkRenderThreadState();
        this.mEGLContextFactory = eGLContextFactory;
    }

    public void setEGLWindowSurfaceFactory(EGLWindowSurfaceFactory eGLWindowSurfaceFactory) {
        checkRenderThreadState();
        this.mEGLWindowSurfaceFactory = eGLWindowSurfaceFactory;
    }

    public void setEGLConfigChooser(EGLConfigChooser eGLConfigChooser) {
        checkRenderThreadState();
        this.mEGLConfigChooser = eGLConfigChooser;
    }

    public void setEGLConfigChooser(boolean z) {
        setEGLConfigChooser(new SimpleEGLConfigChooser(z));
    }

    public void setEGLConfigChooser(int i, int i2, int i3, int i4, int i5, int i6) {
        setEGLConfigChooser(new ComponentSizeChooser(i, i2, i3, i4, i5, i6));
    }

    public void setEGLContextClientVersion(int i) {
        checkRenderThreadState();
        this.mEGLContextClientVersion = i;
    }

    public void setRenderMode(int i) {
        this.mGLThread.setRenderMode(i);
    }

    public int getRenderMode() {
        return this.mGLThread.getRenderMode();
    }

    public void requestRender() {
        this.mGLThread.requestRender();
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.mGLThread.surfaceCreated();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.mGLThread.surfaceDestroyed();
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        this.mGLThread.onWindowResize(i2, i3);
    }

    public void onPause() {
        this.mGLThread.onPause();
    }

    public void onResume() {
        this.mGLThread.onResume();
    }

    public void queueEvent(Runnable runnable) {
        this.mGLThread.queueEvent(runnable);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mGLThread.requestExitAndWait();
    }

    private class DefaultContextFactory implements EGLContextFactory {
        private int EGL_CONTEXT_CLIENT_VERSION;

        private DefaultContextFactory() {
            this.EGL_CONTEXT_CLIENT_VERSION = 12440;
        }

        public EGLContext createContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig) {
            int[] iArr = {this.EGL_CONTEXT_CLIENT_VERSION, GLSurfaceViewProfile.this.mEGLContextClientVersion, 12344};
            EGLContext eGLContext = EGL10.EGL_NO_CONTEXT;
            if (GLSurfaceViewProfile.this.mEGLContextClientVersion == 0) {
                iArr = null;
            }
            return egl10.eglCreateContext(eGLDisplay, eGLConfig, eGLContext, iArr);
        }

        public void destroyContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLContext eGLContext) {
            if (!egl10.eglDestroyContext(eGLDisplay, eGLContext)) {
                Log.e("DefaultContextFactory", "display:" + eGLDisplay + " context: " + eGLContext);
                throw new RuntimeException("eglDestroyContext failed: ");
            }
        }
    }

    private static class DefaultWindowSurfaceFactory implements EGLWindowSurfaceFactory {
        private DefaultWindowSurfaceFactory() {
        }

        public EGLSurface createWindowSurface(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, Object obj) {
            return egl10.eglCreateWindowSurface(eGLDisplay, eGLConfig, obj, null);
        }

        public void destroySurface(EGL10 egl10, EGLDisplay eGLDisplay, EGLSurface eGLSurface) {
            egl10.eglDestroySurface(eGLDisplay, eGLSurface);
        }
    }

    public abstract class BaseConfigChooser implements EGLConfigChooser {
        protected int[] mConfigSpec;

        /* access modifiers changed from: package-private */
        public abstract EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr);

        public BaseConfigChooser(int[] iArr) {
            this.mConfigSpec = filterConfigSpec(iArr);
        }

        public EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay) {
            int[] iArr = new int[1];
            if (!egl10.eglChooseConfig(eGLDisplay, this.mConfigSpec, null, 0, iArr)) {
                throw new IllegalArgumentException("eglChooseConfig failed");
            }
            int i = iArr[0];
            if (i <= 0) {
                throw new IllegalArgumentException("No configs match configSpec");
            }
            EGLConfig[] eGLConfigArr = new EGLConfig[i];
            if (!egl10.eglChooseConfig(eGLDisplay, this.mConfigSpec, eGLConfigArr, i, iArr)) {
                throw new IllegalArgumentException("eglChooseConfig#2 failed");
            }
            EGLConfig chooseConfig = chooseConfig(egl10, eGLDisplay, eGLConfigArr);
            if (chooseConfig != null) {
                return chooseConfig;
            }
            throw new IllegalArgumentException("No config chosen");
        }

        private int[] filterConfigSpec(int[] iArr) {
            if (GLSurfaceViewProfile.this.mEGLContextClientVersion != 2) {
                return iArr;
            }
            int length = iArr.length;
            int[] iArr2 = new int[(length + 2)];
            int i = length - 1;
            System.arraycopy(iArr, 0, iArr2, 0, i);
            iArr2[i] = 12352;
            iArr2[length] = 4;
            iArr2[length + 1] = 12344;
            return iArr2;
        }
    }

    public class ComponentSizeChooser extends BaseConfigChooser {
        protected int mAlphaSize;
        protected int mBlueSize;
        protected int mDepthSize;
        protected int mGreenSize;
        protected int mRedSize;
        protected int mStencilSize;
        private int[] mValue = new int[1];

        public ComponentSizeChooser(int i, int i2, int i3, int i4, int i5, int i6) {
            super(new int[]{12324, i, 12323, i2, 12322, i3, 12321, i4, 12325, i5, 12326, i6, 12344});
            this.mRedSize = i;
            this.mGreenSize = i2;
            this.mBlueSize = i3;
            this.mAlphaSize = i4;
            this.mDepthSize = i5;
            this.mStencilSize = i6;
        }

        public EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr) {
            for (EGLConfig eGLConfig : eGLConfigArr) {
                EGL10 egl102 = egl10;
                EGLDisplay eGLDisplay2 = eGLDisplay;
                EGLConfig eGLConfig2 = eGLConfig;
                int findConfigAttrib = findConfigAttrib(egl102, eGLDisplay2, eGLConfig2, 12325, 0);
                int findConfigAttrib2 = findConfigAttrib(egl102, eGLDisplay2, eGLConfig2, 12326, 0);
                if (findConfigAttrib >= this.mDepthSize && findConfigAttrib2 >= this.mStencilSize) {
                    EGL10 egl103 = egl10;
                    EGLDisplay eGLDisplay3 = eGLDisplay;
                    EGLConfig eGLConfig3 = eGLConfig;
                    int findConfigAttrib3 = findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12324, 0);
                    int findConfigAttrib4 = findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12323, 0);
                    int findConfigAttrib5 = findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12322, 0);
                    int findConfigAttrib6 = findConfigAttrib(egl103, eGLDisplay3, eGLConfig3, 12321, 0);
                    if (findConfigAttrib3 == this.mRedSize && findConfigAttrib4 == this.mGreenSize && findConfigAttrib5 == this.mBlueSize && findConfigAttrib6 == this.mAlphaSize) {
                        return eGLConfig;
                    }
                }
            }
            return null;
        }

        private int findConfigAttrib(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i, int i2) {
            return egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i, this.mValue) ? this.mValue[0] : i2;
        }
    }

    private class SimpleEGLConfigChooser extends ComponentSizeChooser {
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public SimpleEGLConfigChooser(boolean z) {
            super(5, 6, 5, 0, z ? 16 : 0, 0);
        }
    }

    private class EglHelper {
        EGL10 mEgl;
        EGLConfig mEglConfig;
        EGLContext mEglContext;
        EGLDisplay mEglDisplay;
        EGLSurface mEglSurface;

        public EglHelper() {
        }

        public void start() {
            this.mEgl = (EGL10) EGLContext.getEGL();
            this.mEglDisplay = this.mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            if (this.mEglDisplay == EGL10.EGL_NO_DISPLAY) {
                throw new RuntimeException("eglGetDisplay failed");
            }
            if (!this.mEgl.eglInitialize(this.mEglDisplay, new int[2])) {
                throw new RuntimeException("eglInitialize failed");
            }
            this.mEglConfig = GLSurfaceViewProfile.this.mEGLConfigChooser.chooseConfig(this.mEgl, this.mEglDisplay);
            this.mEglContext = GLSurfaceViewProfile.this.mEGLContextFactory.createContext(this.mEgl, this.mEglDisplay, this.mEglConfig);
            if (this.mEglContext == null || this.mEglContext == EGL10.EGL_NO_CONTEXT) {
                this.mEglContext = null;
                throwEglException("createContext");
            }
            this.mEglSurface = null;
        }

        public GL createSurface(SurfaceHolder surfaceHolder) {
            if (this.mEgl == null) {
                throw new RuntimeException("egl not initialized");
            } else if (this.mEglDisplay == null) {
                throw new RuntimeException("eglDisplay not initialized");
            } else if (this.mEglConfig == null) {
                throw new RuntimeException("mEglConfig not initialized");
            } else {
                if (!(this.mEglSurface == null || this.mEglSurface == EGL10.EGL_NO_SURFACE)) {
                    this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                    GLSurfaceViewProfile.this.mEGLWindowSurfaceFactory.destroySurface(this.mEgl, this.mEglDisplay, this.mEglSurface);
                }
                this.mEglSurface = GLSurfaceViewProfile.this.mEGLWindowSurfaceFactory.createWindowSurface(this.mEgl, this.mEglDisplay, this.mEglConfig, surfaceHolder);
                if (this.mEglSurface == null || this.mEglSurface == EGL10.EGL_NO_SURFACE) {
                    int eglGetError = this.mEgl.eglGetError();
                    if (eglGetError == 12299) {
                        Log.e("EglHelper", "createWindowSurface returned EGL_BAD_NATIVE_WINDOW.");
                        return null;
                    }
                    throwEglException("createWindowSurface", eglGetError);
                }
                if (!this.mEgl.eglMakeCurrent(this.mEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext)) {
                    throwEglException("eglMakeCurrent");
                }
                GL gl = this.mEglContext.getGL();
                return GLSurfaceViewProfile.this.mGLWrapper != null ? GLSurfaceViewProfile.this.mGLWrapper.wrap(gl) : gl;
            }
        }

        public boolean swap() {
            if (this.mEgl.eglSwapBuffers(this.mEglDisplay, this.mEglSurface)) {
                return true;
            }
            int eglGetError = this.mEgl.eglGetError();
            if (eglGetError == 12299) {
                Log.e("EglHelper", "eglSwapBuffers returned EGL_BAD_NATIVE_WINDOW. tid=" + Thread.currentThread().getId());
                return true;
            } else if (eglGetError == 12302) {
                return false;
            } else {
                throwEglException("eglSwapBuffers", eglGetError);
                return true;
            }
        }

        public void destroySurface() {
            if (this.mEglSurface != null && this.mEglSurface != EGL10.EGL_NO_SURFACE) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                GLSurfaceViewProfile.this.mEGLWindowSurfaceFactory.destroySurface(this.mEgl, this.mEglDisplay, this.mEglSurface);
                this.mEglSurface = null;
            }
        }

        public void finish() {
            if (this.mEglContext != null) {
                GLSurfaceViewProfile.this.mEGLContextFactory.destroyContext(this.mEgl, this.mEglDisplay, this.mEglContext);
                this.mEglContext = null;
            }
            if (this.mEglDisplay != null) {
                this.mEgl.eglTerminate(this.mEglDisplay);
                this.mEglDisplay = null;
            }
        }

        private void throwEglException(String str) {
            throwEglException(str, this.mEgl.eglGetError());
        }

        private void throwEglException(String str, int i) {
            throw new RuntimeException(str + " failed: ");
        }
    }

    class GLThread extends Thread {
        private EglHelper mEglHelper;
        private ArrayList<Runnable> mEventQueue = new ArrayList<>();
        /* access modifiers changed from: private */
        public boolean mExited;
        private boolean mHasSurface;
        private boolean mHaveEglContext;
        private boolean mHaveEglSurface;
        private int mHeight = 0;
        private boolean mPaused;
        private boolean mRenderComplete;
        private int mRenderMode = 1;
        private Renderer mRenderer;
        private boolean mRequestPaused;
        private boolean mRequestRender = true;
        private boolean mShouldExit;
        private boolean mShouldReleaseEglContext;
        private boolean mWaitingForSurface;
        private int mWidth = 0;

        GLThread(Renderer renderer) {
            this.mRenderer = renderer;
        }

        public void run() {
            setName("GLThread " + getId());
            try {
                guardedRun();
            } catch (InterruptedException unused) {
            } catch (Throwable th) {
                GLSurfaceViewProfile.sGLThreadManager.threadExiting(this);
                throw th;
            }
            GLSurfaceViewProfile.sGLThreadManager.threadExiting(this);
        }

        private void stopEglSurfaceLocked() {
            if (this.mHaveEglSurface) {
                this.mHaveEglSurface = false;
                this.mEglHelper.destroySurface();
            }
        }

        private void stopEglContextLocked() {
            if (this.mHaveEglContext) {
                this.mEglHelper.finish();
                this.mHaveEglContext = false;
                GLSurfaceViewProfile.sGLThreadManager.releaseEglContextLocked(this);
            }
        }

        /* JADX INFO: finally extract failed */
        /* JADX WARNING: Code restructure failed: missing block: B:100:0x0164, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:103:0x0167, code lost:
            throw r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:105:?, code lost:
            com.miniclip.nativeJNI.GLSurfaceViewProfile.access$700().checkGLDriver(r4);
            r10 = false;
            r8 = r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:106:0x0171, code lost:
            if (r9 == false) goto L_0x017d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:107:0x0173, code lost:
            r1.mRenderer.onSurfaceCreated(r8, r1.mEglHelper.mEglConfig);
            r9 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:108:0x017d, code lost:
            if (r11 == false) goto L_0x0185;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:109:0x017f, code lost:
            r1.mRenderer.onSurfaceChanged(r8, r13, r14);
            r11 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:110:0x0185, code lost:
            r1.mRenderer.onDrawFrame(r8);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:111:0x0190, code lost:
            if (r1.mEglHelper.swap() != false) goto L_0x0193;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:112:0x0192, code lost:
            r5 = r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:113:0x0193, code lost:
            if (r12 == false) goto L_0x001b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:114:0x0195, code lost:
            r6 = r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:88:0x013e, code lost:
            if (r15 == null) goto L_0x0145;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:91:0x0145, code lost:
            if (r10 == false) goto L_0x0171;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:92:0x0147, code lost:
            r4 = (javax.microedition.khronos.opengles.GL10) r1.mEglHelper.createSurface(r1.this$0.getHolder());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:93:0x0155, code lost:
            if (r4 != null) goto L_0x0168;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:0x0157, code lost:
            r8 = com.miniclip.nativeJNI.GLSurfaceViewProfile.access$700();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:95:0x015b, code lost:
            monitor-enter(r8);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
            stopEglSurfaceLocked();
            stopEglContextLocked();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:0x0162, code lost:
            monitor-exit(r8);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:0x0163, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void guardedRun() throws java.lang.InterruptedException {
            /*
                r18 = this;
                r1 = r18
                com.miniclip.nativeJNI.GLSurfaceViewProfile$EglHelper r2 = new com.miniclip.nativeJNI.GLSurfaceViewProfile$EglHelper
                com.miniclip.nativeJNI.GLSurfaceViewProfile r3 = com.miniclip.nativeJNI.GLSurfaceViewProfile.this
                r2.<init>()
                r1.mEglHelper = r2
                r2 = 0
                r1.mHaveEglContext = r2
                r1.mHaveEglSurface = r2
                r5 = r2
                r6 = r5
                r7 = r6
                r9 = r7
                r10 = r9
                r11 = r10
                r12 = r11
                r13 = r12
                r14 = r13
                r8 = 0
            L_0x001a:
                r15 = 0
            L_0x001b:
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r16 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x01a8 }
                monitor-enter(r16)     // Catch:{ all -> 0x01a8 }
            L_0x0020:
                boolean r3 = r1.mShouldExit     // Catch:{ all -> 0x01a4 }
                if (r3 == 0) goto L_0x0036
                monitor-exit(r16)     // Catch:{ all -> 0x01a4 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r2 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager
                monitor-enter(r2)
                r18.stopEglSurfaceLocked()     // Catch:{ all -> 0x0032 }
                r18.stopEglContextLocked()     // Catch:{ all -> 0x0032 }
                monitor-exit(r2)     // Catch:{ all -> 0x0032 }
                return
            L_0x0032:
                r0 = move-exception
                r3 = r0
                monitor-exit(r2)     // Catch:{ all -> 0x0032 }
                throw r3
            L_0x0036:
                java.util.ArrayList<java.lang.Runnable> r3 = r1.mEventQueue     // Catch:{ all -> 0x01a4 }
                boolean r3 = r3.isEmpty()     // Catch:{ all -> 0x01a4 }
                if (r3 != 0) goto L_0x004a
                java.util.ArrayList<java.lang.Runnable> r3 = r1.mEventQueue     // Catch:{ all -> 0x01a4 }
                java.lang.Object r3 = r3.remove(r2)     // Catch:{ all -> 0x01a4 }
                java.lang.Runnable r3 = (java.lang.Runnable) r3     // Catch:{ all -> 0x01a4 }
                r15 = r3
                r3 = 1
                goto L_0x013d
            L_0x004a:
                boolean r3 = r1.mPaused     // Catch:{ all -> 0x01a4 }
                boolean r4 = r1.mRequestPaused     // Catch:{ all -> 0x01a4 }
                if (r3 == r4) goto L_0x005b
                boolean r3 = r1.mRequestPaused     // Catch:{ all -> 0x01a4 }
                r1.mPaused = r3     // Catch:{ all -> 0x01a4 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r3 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x01a4 }
                r3.notifyAll()     // Catch:{ all -> 0x01a4 }
            L_0x005b:
                boolean r3 = r1.mShouldReleaseEglContext     // Catch:{ all -> 0x01a4 }
                if (r3 == 0) goto L_0x0068
                r18.stopEglSurfaceLocked()     // Catch:{ all -> 0x01a4 }
                r18.stopEglContextLocked()     // Catch:{ all -> 0x01a4 }
                r1.mShouldReleaseEglContext = r2     // Catch:{ all -> 0x01a4 }
                r7 = 1
            L_0x0068:
                if (r5 == 0) goto L_0x0071
                r18.stopEglSurfaceLocked()     // Catch:{ all -> 0x01a4 }
                r18.stopEglContextLocked()     // Catch:{ all -> 0x01a4 }
                r5 = r2
            L_0x0071:
                boolean r3 = r1.mHaveEglSurface     // Catch:{ all -> 0x01a4 }
                if (r3 == 0) goto L_0x00a0
                boolean r3 = r1.mPaused     // Catch:{ all -> 0x01a4 }
                if (r3 == 0) goto L_0x00a0
                r18.stopEglSurfaceLocked()     // Catch:{ all -> 0x01a4 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile r3 = com.miniclip.nativeJNI.GLSurfaceViewProfile.this     // Catch:{ all -> 0x01a4 }
                boolean r3 = r3.mPreserveEGLContextOnPause     // Catch:{ all -> 0x01a4 }
                if (r3 == 0) goto L_0x008e
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r3 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x01a4 }
                boolean r3 = r3.shouldReleaseEGLContextWhenPausing()     // Catch:{ all -> 0x01a4 }
                if (r3 == 0) goto L_0x0091
            L_0x008e:
                r18.stopEglContextLocked()     // Catch:{ all -> 0x01a4 }
            L_0x0091:
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r3 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x01a4 }
                boolean r3 = r3.shouldTerminateEGLWhenPausing()     // Catch:{ all -> 0x01a4 }
                if (r3 == 0) goto L_0x00a0
                com.miniclip.nativeJNI.GLSurfaceViewProfile$EglHelper r3 = r1.mEglHelper     // Catch:{ all -> 0x01a4 }
                r3.finish()     // Catch:{ all -> 0x01a4 }
            L_0x00a0:
                boolean r3 = r1.mHasSurface     // Catch:{ all -> 0x01a4 }
                if (r3 != 0) goto L_0x00b9
                boolean r3 = r1.mWaitingForSurface     // Catch:{ all -> 0x01a4 }
                if (r3 != 0) goto L_0x00b9
                boolean r3 = r1.mHaveEglSurface     // Catch:{ all -> 0x01a4 }
                if (r3 == 0) goto L_0x00af
                r18.stopEglSurfaceLocked()     // Catch:{ all -> 0x01a4 }
            L_0x00af:
                r3 = 1
                r1.mWaitingForSurface = r3     // Catch:{ all -> 0x01a4 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r3 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x01a4 }
                r3.notifyAll()     // Catch:{ all -> 0x01a4 }
            L_0x00b9:
                boolean r3 = r1.mHasSurface     // Catch:{ all -> 0x01a4 }
                if (r3 == 0) goto L_0x00ca
                boolean r3 = r1.mWaitingForSurface     // Catch:{ all -> 0x01a4 }
                if (r3 == 0) goto L_0x00ca
                r1.mWaitingForSurface = r2     // Catch:{ all -> 0x01a4 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r3 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x01a4 }
                r3.notifyAll()     // Catch:{ all -> 0x01a4 }
            L_0x00ca:
                if (r6 == 0) goto L_0x00d8
                r3 = 1
                r1.mRenderComplete = r3     // Catch:{ all -> 0x01a4 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r3 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x01a4 }
                r3.notifyAll()     // Catch:{ all -> 0x01a4 }
                r6 = r2
                r12 = r6
            L_0x00d8:
                boolean r3 = r18.readyToDraw()     // Catch:{ all -> 0x01a4 }
                if (r3 == 0) goto L_0x019a
                boolean r3 = r1.mHaveEglContext     // Catch:{ all -> 0x01a4 }
                if (r3 != 0) goto L_0x010b
                if (r7 == 0) goto L_0x00e6
                r7 = r2
                goto L_0x010b
            L_0x00e6:
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r3 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x01a4 }
                boolean r3 = r3.tryAcquireEglContextLocked(r1)     // Catch:{ all -> 0x01a4 }
                if (r3 == 0) goto L_0x010b
                com.miniclip.nativeJNI.GLSurfaceViewProfile$EglHelper r3 = r1.mEglHelper     // Catch:{ RuntimeException -> 0x0101 }
                r3.start()     // Catch:{ RuntimeException -> 0x0101 }
                r3 = 1
                r1.mHaveEglContext = r3     // Catch:{ all -> 0x01a4 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r3 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x01a4 }
                r3.notifyAll()     // Catch:{ all -> 0x01a4 }
                r9 = 1
                goto L_0x010b
            L_0x0101:
                r0 = move-exception
                r2 = r0
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r3 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x01a4 }
                r3.releaseEglContextLocked(r1)     // Catch:{ all -> 0x01a4 }
                throw r2     // Catch:{ all -> 0x01a4 }
            L_0x010b:
                boolean r3 = r1.mHaveEglContext     // Catch:{ all -> 0x01a4 }
                if (r3 == 0) goto L_0x0119
                boolean r3 = r1.mHaveEglSurface     // Catch:{ all -> 0x01a4 }
                if (r3 != 0) goto L_0x0119
                r3 = 1
                r1.mHaveEglSurface = r3     // Catch:{ all -> 0x01a4 }
                r4 = r3
                r10 = r4
                goto L_0x011b
            L_0x0119:
                r3 = 1
                r4 = r11
            L_0x011b:
                boolean r11 = r1.mHaveEglSurface     // Catch:{ all -> 0x01a4 }
                if (r11 == 0) goto L_0x0198
                com.miniclip.nativeJNI.GLSurfaceViewProfile r11 = com.miniclip.nativeJNI.GLSurfaceViewProfile.this     // Catch:{ all -> 0x01a4 }
                boolean r11 = r11.mSizeChanged     // Catch:{ all -> 0x01a4 }
                if (r11 == 0) goto L_0x0133
                int r13 = r1.mWidth     // Catch:{ all -> 0x01a4 }
                int r14 = r1.mHeight     // Catch:{ all -> 0x01a4 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile r4 = com.miniclip.nativeJNI.GLSurfaceViewProfile.this     // Catch:{ all -> 0x01a4 }
                boolean unused = r4.mSizeChanged = r2     // Catch:{ all -> 0x01a4 }
                r4 = r3
                r12 = r4
                goto L_0x0135
            L_0x0133:
                r1.mRequestRender = r2     // Catch:{ all -> 0x01a4 }
            L_0x0135:
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r11 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x01a4 }
                r11.notifyAll()     // Catch:{ all -> 0x01a4 }
                r11 = r4
            L_0x013d:
                monitor-exit(r16)     // Catch:{ all -> 0x01a4 }
                if (r15 == 0) goto L_0x0145
                r15.run()     // Catch:{ all -> 0x01a8 }
                goto L_0x001a
            L_0x0145:
                if (r10 == 0) goto L_0x0171
                com.miniclip.nativeJNI.GLSurfaceViewProfile$EglHelper r4 = r1.mEglHelper     // Catch:{ all -> 0x01a8 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile r8 = com.miniclip.nativeJNI.GLSurfaceViewProfile.this     // Catch:{ all -> 0x01a8 }
                android.view.SurfaceHolder r8 = r8.getHolder()     // Catch:{ all -> 0x01a8 }
                javax.microedition.khronos.opengles.GL r4 = r4.createSurface(r8)     // Catch:{ all -> 0x01a8 }
                javax.microedition.khronos.opengles.GL10 r4 = (javax.microedition.khronos.opengles.GL10) r4     // Catch:{ all -> 0x01a8 }
                if (r4 != 0) goto L_0x0168
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r8 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager
                monitor-enter(r8)
                r18.stopEglSurfaceLocked()     // Catch:{ all -> 0x0164 }
                r18.stopEglContextLocked()     // Catch:{ all -> 0x0164 }
                monitor-exit(r8)     // Catch:{ all -> 0x0164 }
                return
            L_0x0164:
                r0 = move-exception
                r2 = r0
                monitor-exit(r8)     // Catch:{ all -> 0x0164 }
                throw r2
            L_0x0168:
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r8 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x01a8 }
                r8.checkGLDriver(r4)     // Catch:{ all -> 0x01a8 }
                r10 = r2
                r8 = r4
            L_0x0171:
                if (r9 == 0) goto L_0x017d
                com.miniclip.nativeJNI.GLSurfaceViewProfile$Renderer r4 = r1.mRenderer     // Catch:{ all -> 0x01a8 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$EglHelper r9 = r1.mEglHelper     // Catch:{ all -> 0x01a8 }
                javax.microedition.khronos.egl.EGLConfig r9 = r9.mEglConfig     // Catch:{ all -> 0x01a8 }
                r4.onSurfaceCreated(r8, r9)     // Catch:{ all -> 0x01a8 }
                r9 = r2
            L_0x017d:
                if (r11 == 0) goto L_0x0185
                com.miniclip.nativeJNI.GLSurfaceViewProfile$Renderer r4 = r1.mRenderer     // Catch:{ all -> 0x01a8 }
                r4.onSurfaceChanged(r8, r13, r14)     // Catch:{ all -> 0x01a8 }
                r11 = r2
            L_0x0185:
                com.miniclip.nativeJNI.GLSurfaceViewProfile$Renderer r4 = r1.mRenderer     // Catch:{ all -> 0x01a8 }
                r4.onDrawFrame(r8)     // Catch:{ all -> 0x01a8 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$EglHelper r4 = r1.mEglHelper     // Catch:{ all -> 0x01a8 }
                boolean r4 = r4.swap()     // Catch:{ all -> 0x01a8 }
                if (r4 != 0) goto L_0x0193
                r5 = r3
            L_0x0193:
                if (r12 == 0) goto L_0x001b
                r6 = r3
                goto L_0x001b
            L_0x0198:
                r11 = r4
                goto L_0x019b
            L_0x019a:
                r3 = 1
            L_0x019b:
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r4 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x01a4 }
                r4.wait()     // Catch:{ all -> 0x01a4 }
                goto L_0x0020
            L_0x01a4:
                r0 = move-exception
                r2 = r0
                monitor-exit(r16)     // Catch:{ all -> 0x01a4 }
                throw r2     // Catch:{ all -> 0x01a8 }
            L_0x01a8:
                r0 = move-exception
                r2 = r0
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r3 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager
                monitor-enter(r3)
                r18.stopEglSurfaceLocked()     // Catch:{ all -> 0x01b7 }
                r18.stopEglContextLocked()     // Catch:{ all -> 0x01b7 }
                monitor-exit(r3)     // Catch:{ all -> 0x01b7 }
                throw r2
            L_0x01b7:
                r0 = move-exception
                r2 = r0
                monitor-exit(r3)     // Catch:{ all -> 0x01b7 }
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.miniclip.nativeJNI.GLSurfaceViewProfile.GLThread.guardedRun():void");
        }

        public boolean ableToDraw() {
            return this.mHaveEglContext && this.mHaveEglSurface && readyToDraw();
        }

        private boolean readyToDraw() {
            return !this.mPaused && this.mHasSurface && this.mWidth > 0 && this.mHeight > 0 && (this.mRequestRender || this.mRenderMode == 1);
        }

        public void setRenderMode(int i) {
            if (i < 0 || i > 1) {
                throw new IllegalArgumentException("renderMode");
            }
            synchronized (GLSurfaceViewProfile.sGLThreadManager) {
                this.mRenderMode = i;
                GLSurfaceViewProfile.sGLThreadManager.notifyAll();
            }
        }

        public int getRenderMode() {
            int i;
            synchronized (GLSurfaceViewProfile.sGLThreadManager) {
                i = this.mRenderMode;
            }
            return i;
        }

        public void requestRender() {
            synchronized (GLSurfaceViewProfile.sGLThreadManager) {
                this.mRequestRender = true;
                GLSurfaceViewProfile.sGLThreadManager.notifyAll();
            }
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:9|10|11|12|22|18|5) */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x000f, code lost:
            continue;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x001f */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void surfaceCreated() {
            /*
                r2 = this;
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r0 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager
                monitor-enter(r0)
                r1 = 1
                r2.mHasSurface = r1     // Catch:{ all -> 0x0029 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r1 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x0029 }
                r1.notifyAll()     // Catch:{ all -> 0x0029 }
            L_0x000f:
                boolean r1 = r2.mWaitingForSurface     // Catch:{ all -> 0x0029 }
                if (r1 == 0) goto L_0x0027
                boolean r1 = r2.mExited     // Catch:{ all -> 0x0029 }
                if (r1 != 0) goto L_0x0027
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r1 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ InterruptedException -> 0x001f }
                r1.wait()     // Catch:{ InterruptedException -> 0x001f }
                goto L_0x000f
            L_0x001f:
                java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0029 }
                r1.interrupt()     // Catch:{ all -> 0x0029 }
                goto L_0x000f
            L_0x0027:
                monitor-exit(r0)     // Catch:{ all -> 0x0029 }
                return
            L_0x0029:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0029 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.miniclip.nativeJNI.GLSurfaceViewProfile.GLThread.surfaceCreated():void");
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:9|10|11|12|22|18|5) */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x000f, code lost:
            continue;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x001f */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void surfaceDestroyed() {
            /*
                r2 = this;
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r0 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager
                monitor-enter(r0)
                r1 = 0
                r2.mHasSurface = r1     // Catch:{ all -> 0x0029 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r1 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x0029 }
                r1.notifyAll()     // Catch:{ all -> 0x0029 }
            L_0x000f:
                boolean r1 = r2.mWaitingForSurface     // Catch:{ all -> 0x0029 }
                if (r1 != 0) goto L_0x0027
                boolean r1 = r2.mExited     // Catch:{ all -> 0x0029 }
                if (r1 != 0) goto L_0x0027
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r1 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ InterruptedException -> 0x001f }
                r1.wait()     // Catch:{ InterruptedException -> 0x001f }
                goto L_0x000f
            L_0x001f:
                java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0029 }
                r1.interrupt()     // Catch:{ all -> 0x0029 }
                goto L_0x000f
            L_0x0027:
                monitor-exit(r0)     // Catch:{ all -> 0x0029 }
                return
            L_0x0029:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0029 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.miniclip.nativeJNI.GLSurfaceViewProfile.GLThread.surfaceDestroyed():void");
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:9|10|11|12|22|18|5) */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x000f, code lost:
            continue;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x001f */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onPause() {
            /*
                r2 = this;
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r0 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager
                monitor-enter(r0)
                r1 = 1
                r2.mRequestPaused = r1     // Catch:{ all -> 0x0029 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r1 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x0029 }
                r1.notifyAll()     // Catch:{ all -> 0x0029 }
            L_0x000f:
                boolean r1 = r2.mExited     // Catch:{ all -> 0x0029 }
                if (r1 != 0) goto L_0x0027
                boolean r1 = r2.mPaused     // Catch:{ all -> 0x0029 }
                if (r1 != 0) goto L_0x0027
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r1 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ InterruptedException -> 0x001f }
                r1.wait()     // Catch:{ InterruptedException -> 0x001f }
                goto L_0x000f
            L_0x001f:
                java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0029 }
                r1.interrupt()     // Catch:{ all -> 0x0029 }
                goto L_0x000f
            L_0x0027:
                monitor-exit(r0)     // Catch:{ all -> 0x0029 }
                return
            L_0x0029:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0029 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.miniclip.nativeJNI.GLSurfaceViewProfile.GLThread.onPause():void");
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:11|12|13|14|25|20|5) */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0014, code lost:
            continue;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0028 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onResume() {
            /*
                r3 = this;
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r0 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager
                monitor-enter(r0)
                r1 = 0
                r3.mRequestPaused = r1     // Catch:{ all -> 0x0032 }
                r2 = 1
                r3.mRequestRender = r2     // Catch:{ all -> 0x0032 }
                r3.mRenderComplete = r1     // Catch:{ all -> 0x0032 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r1 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x0032 }
                r1.notifyAll()     // Catch:{ all -> 0x0032 }
            L_0x0014:
                boolean r1 = r3.mExited     // Catch:{ all -> 0x0032 }
                if (r1 != 0) goto L_0x0030
                boolean r1 = r3.mPaused     // Catch:{ all -> 0x0032 }
                if (r1 == 0) goto L_0x0030
                boolean r1 = r3.mRenderComplete     // Catch:{ all -> 0x0032 }
                if (r1 != 0) goto L_0x0030
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r1 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ InterruptedException -> 0x0028 }
                r1.wait()     // Catch:{ InterruptedException -> 0x0028 }
                goto L_0x0014
            L_0x0028:
                java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0032 }
                r1.interrupt()     // Catch:{ all -> 0x0032 }
                goto L_0x0014
            L_0x0030:
                monitor-exit(r0)     // Catch:{ all -> 0x0032 }
                return
            L_0x0032:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0032 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.miniclip.nativeJNI.GLSurfaceViewProfile.GLThread.onResume():void");
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:14|15|16|17|30|23|4) */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x001b, code lost:
            continue;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0043 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onWindowResize(int r2, int r3) {
            /*
                r1 = this;
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r0 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager
                monitor-enter(r0)
                r1.mWidth = r2     // Catch:{ all -> 0x004d }
                r1.mHeight = r3     // Catch:{ all -> 0x004d }
                com.miniclip.nativeJNI.GLSurfaceViewProfile r2 = com.miniclip.nativeJNI.GLSurfaceViewProfile.this     // Catch:{ all -> 0x004d }
                r3 = 1
                boolean unused = r2.mSizeChanged = r3     // Catch:{ all -> 0x004d }
                r1.mRequestRender = r3     // Catch:{ all -> 0x004d }
                r2 = 0
                r1.mRenderComplete = r2     // Catch:{ all -> 0x004d }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r2 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x004d }
                r2.notifyAll()     // Catch:{ all -> 0x004d }
            L_0x001b:
                boolean r2 = r1.mExited     // Catch:{ all -> 0x004d }
                if (r2 != 0) goto L_0x004b
                boolean r2 = r1.mPaused     // Catch:{ all -> 0x004d }
                if (r2 != 0) goto L_0x004b
                boolean r2 = r1.mRenderComplete     // Catch:{ all -> 0x004d }
                if (r2 != 0) goto L_0x004b
                com.miniclip.nativeJNI.GLSurfaceViewProfile r2 = com.miniclip.nativeJNI.GLSurfaceViewProfile.this     // Catch:{ all -> 0x004d }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThread r2 = r2.mGLThread     // Catch:{ all -> 0x004d }
                if (r2 == 0) goto L_0x004b
                com.miniclip.nativeJNI.GLSurfaceViewProfile r2 = com.miniclip.nativeJNI.GLSurfaceViewProfile.this     // Catch:{ all -> 0x004d }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThread r2 = r2.mGLThread     // Catch:{ all -> 0x004d }
                boolean r2 = r2.ableToDraw()     // Catch:{ all -> 0x004d }
                if (r2 == 0) goto L_0x004b
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r2 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ InterruptedException -> 0x0043 }
                r2.wait()     // Catch:{ InterruptedException -> 0x0043 }
                goto L_0x001b
            L_0x0043:
                java.lang.Thread r2 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x004d }
                r2.interrupt()     // Catch:{ all -> 0x004d }
                goto L_0x001b
            L_0x004b:
                monitor-exit(r0)     // Catch:{ all -> 0x004d }
                return
            L_0x004d:
                r2 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x004d }
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.miniclip.nativeJNI.GLSurfaceViewProfile.GLThread.onWindowResize(int, int):void");
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:7|8|9|10|19|16|5) */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x000f, code lost:
            continue;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x001b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void requestExitAndWait() {
            /*
                r2 = this;
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r0 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager
                monitor-enter(r0)
                r1 = 1
                r2.mShouldExit = r1     // Catch:{ all -> 0x0025 }
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r1 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ all -> 0x0025 }
                r1.notifyAll()     // Catch:{ all -> 0x0025 }
            L_0x000f:
                boolean r1 = r2.mExited     // Catch:{ all -> 0x0025 }
                if (r1 != 0) goto L_0x0023
                com.miniclip.nativeJNI.GLSurfaceViewProfile$GLThreadManager r1 = com.miniclip.nativeJNI.GLSurfaceViewProfile.sGLThreadManager     // Catch:{ InterruptedException -> 0x001b }
                r1.wait()     // Catch:{ InterruptedException -> 0x001b }
                goto L_0x000f
            L_0x001b:
                java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0025 }
                r1.interrupt()     // Catch:{ all -> 0x0025 }
                goto L_0x000f
            L_0x0023:
                monitor-exit(r0)     // Catch:{ all -> 0x0025 }
                return
            L_0x0025:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0025 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.miniclip.nativeJNI.GLSurfaceViewProfile.GLThread.requestExitAndWait():void");
        }

        public void requestReleaseEglContextLocked() {
            this.mShouldReleaseEglContext = true;
            GLSurfaceViewProfile.sGLThreadManager.notifyAll();
        }

        public void queueEvent(Runnable runnable) {
            if (runnable == null) {
                throw new IllegalArgumentException("r must not be null");
            }
            synchronized (GLSurfaceViewProfile.sGLThreadManager) {
                this.mEventQueue.add(runnable);
                GLSurfaceViewProfile.sGLThreadManager.notifyAll();
            }
        }
    }

    static class LogWriter extends Writer {
        private StringBuilder mBuilder = new StringBuilder();

        LogWriter() {
        }

        public void close() {
            flushBuilder();
        }

        public void flush() {
            flushBuilder();
        }

        public void write(char[] cArr, int i, int i2) {
            for (int i3 = 0; i3 < i2; i3++) {
                char c = cArr[i + i3];
                if (c == 10) {
                    flushBuilder();
                } else {
                    this.mBuilder.append(c);
                }
            }
        }

        private void flushBuilder() {
            if (this.mBuilder.length() > 0) {
                Log.v("GLSurfaceView", this.mBuilder.toString());
                this.mBuilder.delete(0, this.mBuilder.length());
            }
        }
    }

    private void checkRenderThreadState() {
        if (this.mGLThread != null) {
            throw new IllegalStateException("setRenderer has already been called for this instance.");
        }
    }

    private static class GLThreadManager {
        private static String TAG = "GLThreadManager";
        private static final int kGLES_20 = 131072;
        private static final String kMSM7K_RENDERER_PREFIX = "Q3Dimension MSM7500 ";
        private GLThread mEglOwner;
        private boolean mGLESDriverCheckComplete;
        private int mGLESVersion;
        private boolean mGLESVersionCheckComplete;
        private boolean mLimitedGLESContexts;
        private boolean mMultipleGLESContextsAllowed;

        private GLThreadManager() {
        }

        public synchronized void threadExiting(GLThread gLThread) {
            boolean unused = gLThread.mExited = true;
            if (this.mEglOwner == gLThread) {
                this.mEglOwner = null;
            }
            notifyAll();
        }

        public boolean tryAcquireEglContextLocked(GLThread gLThread) {
            if (this.mEglOwner == gLThread || this.mEglOwner == null) {
                this.mEglOwner = gLThread;
                notifyAll();
                return true;
            }
            checkGLESVersion();
            if (this.mMultipleGLESContextsAllowed) {
                return true;
            }
            if (this.mEglOwner == null) {
                return false;
            }
            this.mEglOwner.requestReleaseEglContextLocked();
            return false;
        }

        public void releaseEglContextLocked(GLThread gLThread) {
            if (this.mEglOwner == gLThread) {
                this.mEglOwner = null;
            }
            notifyAll();
        }

        public synchronized boolean shouldReleaseEGLContextWhenPausing() {
            return this.mLimitedGLESContexts;
        }

        public synchronized boolean shouldTerminateEGLWhenPausing() {
            checkGLESVersion();
            return !this.mMultipleGLESContextsAllowed;
        }

        public synchronized void checkGLDriver(GL10 gl10) {
            if (!this.mGLESDriverCheckComplete) {
                checkGLESVersion();
                String glGetString = gl10.glGetString(7937);
                if (this.mGLESVersion < 131072) {
                    this.mMultipleGLESContextsAllowed = !glGetString.startsWith(kMSM7K_RENDERER_PREFIX);
                    notifyAll();
                }
                this.mLimitedGLESContexts = !this.mMultipleGLESContextsAllowed;
                this.mGLESDriverCheckComplete = true;
            }
        }

        private void checkGLESVersion() {
            if (!this.mGLESVersionCheckComplete) {
                this.mGLESVersion = 0;
                this.mMultipleGLESContextsAllowed = false;
                if (this.mGLESVersion >= 131072) {
                    this.mMultipleGLESContextsAllowed = true;
                }
                this.mGLESVersionCheckComplete = true;
            }
        }
    }
}
