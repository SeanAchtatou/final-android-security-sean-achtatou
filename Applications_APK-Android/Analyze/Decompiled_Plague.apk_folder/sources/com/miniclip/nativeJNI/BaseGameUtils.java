package com.miniclip.nativeJNI;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.GamesActivityResultCodes;

public class BaseGameUtils {
    static final String SIGN_IN_FAILED_MESSAGE = "Could not sign in. Please try again. Failed";
    static final String SIGN_IN_LICENSE_MESSAGE = "Could not sign in. Please try again. License";
    static final String SIGN_IN_MISCONFIGURED_MESSAGE = "Could not sign in. Please try again. Misconfigured";

    public static void showAlert(Activity activity, String str) {
        new AlertDialog.Builder(activity).setMessage(str).setNeutralButton(17039370, (DialogInterface.OnClickListener) null).create().show();
    }

    public static boolean resolveConnectionFailure(Activity activity, GoogleApiClient googleApiClient, ConnectionResult connectionResult, int i, String str) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(activity, i);
                return true;
            } catch (IntentSender.SendIntentException unused) {
                googleApiClient.connect();
                return false;
            }
        } else {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), activity, i);
            if (errorDialog != null) {
                errorDialog.show();
            } else {
                showAlert(activity, str);
            }
            return false;
        }
    }

    public static boolean verifySampleSetup(Activity activity, int... iArr) {
        boolean z;
        StringBuilder sb = new StringBuilder();
        sb.append("The following set up problems were found:\n\n");
        if (activity.getPackageName().startsWith("com.google.example.games")) {
            sb.append("- Package name cannot be com.google.*. You need to change the sample's package name to your own package.");
            sb.append("\n");
            z = true;
        } else {
            z = false;
        }
        int length = iArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            } else if (activity.getString(iArr[i]).toLowerCase().contains("replaceme")) {
                sb.append("- You must replace all placeholder IDs in the ids.xml file by your project's IDs.");
                sb.append("\n");
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            return true;
        }
        sb.append("\n\nThese problems may prevent the app from working properly.");
        showAlert(activity, sb.toString());
        return false;
    }

    public static void showActivityResultError(Activity activity, int i, int i2, String str) {
        Dialog dialog;
        if (activity == null) {
            Log.e("BaseGameUtils", "*** No Activity. Can't show failure dialog!");
            return;
        }
        switch (i2) {
            case GamesActivityResultCodes.RESULT_SIGN_IN_FAILED:
                dialog = makeSimpleDialog(activity, SIGN_IN_FAILED_MESSAGE);
                break;
            case GamesActivityResultCodes.RESULT_LICENSE_FAILED:
                dialog = makeSimpleDialog(activity, SIGN_IN_LICENSE_MESSAGE);
                break;
            case GamesActivityResultCodes.RESULT_APP_MISCONFIGURED:
                dialog = makeSimpleDialog(activity, SIGN_IN_MISCONFIGURED_MESSAGE);
                break;
            default:
                Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity), activity, i, null);
                if (errorDialog != null) {
                    dialog = errorDialog;
                    break;
                } else {
                    Log.e("BaseGamesUtils", "No standard error dialog available. Making fallback dialog.");
                    dialog = makeSimpleDialog(activity, str);
                    break;
                }
        }
        dialog.show();
    }

    public static Dialog makeSimpleDialog(Activity activity, String str) {
        return new AlertDialog.Builder(activity).setMessage(str).setNeutralButton(17039370, (DialogInterface.OnClickListener) null).create();
    }

    public static Dialog makeSimpleDialog(Activity activity, String str, String str2) {
        return new AlertDialog.Builder(activity).setTitle(str).setMessage(str2).setNeutralButton(17039370, (DialogInterface.OnClickListener) null).create();
    }
}
