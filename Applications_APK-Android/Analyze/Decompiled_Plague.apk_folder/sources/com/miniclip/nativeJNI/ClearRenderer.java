package com.miniclip.nativeJNI;

import android.util.Log;
import com.miniclip.input.MCInput;
import com.miniclip.nativeJNI.GLSurfaceViewProfile;
import com.miniclip.platform.MCApplication;
import com.miniclip.platform.MCViewManager;
import java.util.ArrayList;
import java.util.List;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class ClearRenderer implements GLSurfaceViewProfile.Renderer {
    private static final long NANOSECONDSPERMICROSECOND = 1000000;
    private static final long NANOSECONDSPERSECOND = 1000000000;
    private static long sAnimationInterval = 16666666;
    public boolean hasStarted = false;
    private long mLastTickInNanoSeconds;
    private boolean mNativeInitCompleted = false;
    private List<Runnable> mQueuedEvents = new ArrayList();
    private int mScreenHeight;
    private int mScreenWidth;

    private static native void nativeInit(int i, int i2, int i3);

    private static native void nativeOnPause();

    private static native void nativeOnResume();

    private static native void nativeOnSurfaceChanged(int i, int i2, int i3);

    private static native void nativeRender();

    public static void setAnimationInterval(double d) {
        sAnimationInterval = (long) (d * 1.0E9d);
    }

    public void setScreenWidthAndHeight(int i, int i2) {
        this.mScreenWidth = i;
        this.mScreenHeight = i2;
    }

    public void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
        nativeInit(this.mScreenWidth, this.mScreenHeight, cocojava.mINGAME_LANDSCAPE ^ true ? 1 : 0);
        this.mLastTickInNanoSeconds = System.nanoTime();
        this.mNativeInitCompleted = true;
    }

    public void onSurfaceChanged(GL10 gl10, int i, int i2) {
        boolean z = !cocojava.mINGAME_LANDSCAPE;
        Log.i("Dim", String.format("width: %d, height: %d", Integer.valueOf(i), Integer.valueOf(i2)));
        if (i != i2) {
            if (cocojava.mINGAME_LANDSCAPE && i2 > i) {
                int i3 = i2;
                i2 = i;
                i = i3;
            }
            Log.i("After Hack Dim", String.format("width: %d, height: %d", Integer.valueOf(i), Integer.valueOf(i2)));
            if ((cocojava.mHAS_RETINA && MCViewManager.displayWidth > 799) || cocojava.mONLY_RETINA) {
                CocoJNI.MsetIsRetina(1);
            }
            nativeOnSurfaceChanged(i, i2, z ? 1 : 0);
        }
    }

    public void onDrawFrame(GL10 gl10) {
        ArrayList<Runnable> arrayList;
        if (!MCApplication.hasFatalErrorOccurred()) {
            if (!this.hasStarted) {
                ((cocojava) cocojava.mContext).createResources();
                CocoJNI.Mrun();
                MCViewManager.setContentToGl();
                this.hasStarted = true;
            }
            synchronized (this.mQueuedEvents) {
                arrayList = new ArrayList<>(this.mQueuedEvents);
                this.mQueuedEvents.clear();
            }
            for (Runnable run : arrayList) {
                run.run();
            }
            if (((double) sAnimationInterval) <= 1.6666666666666666E7d) {
                nativeRender();
                return;
            }
            long nanoTime = System.nanoTime() - this.mLastTickInNanoSeconds;
            if (nanoTime < sAnimationInterval) {
                try {
                    Thread.sleep((sAnimationInterval - nanoTime) / NANOSECONDSPERMICROSECOND);
                } catch (Exception unused) {
                }
            }
            this.mLastTickInNanoSeconds = System.nanoTime();
            nativeRender();
        }
    }

    public void queueEvent(Runnable runnable) {
        synchronized (this.mQueuedEvents) {
            this.mQueuedEvents.add(runnable);
        }
    }

    public void handleOnPause() {
        if (this.mNativeInitCompleted) {
            nativeOnPause();
        }
    }

    public void handleOnResume() {
        nativeOnResume();
    }

    public void handleActionDown(int i, float f, float f2) {
        MCInput.nativeTouchesBegin(i, f, f2);
    }

    public void handleActionUp(int i, float f, float f2) {
        MCInput.nativeTouchesEnd(i, f, f2);
    }

    public void handleActionCancel(int[] iArr, float[] fArr, float[] fArr2) {
        MCInput.nativeTouchesCancel(iArr, fArr, fArr2);
    }

    public void handleActionMove(int[] iArr, float[] fArr, float[] fArr2) {
        MCInput.nativeTouchesMove(iArr, fArr, fArr2);
    }
}
