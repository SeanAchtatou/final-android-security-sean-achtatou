package com.miniclip.nativeJNI;

public class CocoJNI {
    public static native void MdatePickerClosed();

    public static native void MdatePickerResponce(int i, int i2, int i3);

    public static native void MnetworkTimeResponce(int i, double d, int i2);

    public static native void MonMessageBoxButtonPressed(int i, int i2);

    public static native void MpreserveContext(int i);

    public static native void Mrun();

    public static native void MsetIsRetina(int i);

    public static native void MsimplePingResponce(int i, int i2);
}
