package com.miniclip.nativeJNI;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StatFs;
import android.os.Vibrator;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.DatePicker;
import com.miniclip.NTP.NtpHandler;
import com.miniclip.Ping.PingHandler;
import com.miniclip.audio.MCAudio;
import com.miniclip.framework.MiniclipAndroidActivity;
import com.miniclip.framework.ThreadingContext;
import com.miniclip.nativeJNI.HtmlDialog;
import com.miniclip.platform.MCApplication;
import com.miniclip.platform.MCAssetManager;
import com.miniclip.platform.MCViewManager;
import com.miniclip.videoads.ProviderConfig;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import org.json.JSONException;
import org.json.JSONObject;

public class cocojava extends MiniclipAndroidActivity implements DialogInterface.OnClickListener {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final int HANDLER_CLOSE_DIALOG = 3;
    private static final int HANDLER_EXIT_APP = 5;
    private static final int HANDLER_HIDE_DIALOG = 2;
    private static final int HANDLER_OPEN_URL = 4;
    private static final int HANDLER_SHOW_DIALOG = 1;
    public static final int LIBRARY_SIZE_IN_MB = 20;
    private static boolean displayedErrorMessage = false;
    public static MiniclipAndroidActivity mContext = null;
    public static String mDeviceID = "";
    protected static boolean mHAS_RETINA = false;
    private static Handler mHandler = null;
    public static boolean mINGAME_LANDSCAPE = true;
    protected static String mNATIVE_LIBRARY_NAME = "game";
    private static NtpHandler mNtpHandler = new NtpHandler();
    protected static boolean mONLY_RETINA = false;
    private static PingHandler mPingHandler = new PingHandler();
    public static boolean mSPINNING_ANIMATION = false;
    protected static boolean mUSE_DEVICEID = true;
    protected static boolean mUSE_PRESERVE_CONTEXT = true;
    protected static Runnable mUpdateRunable = null;
    protected static boolean mUpdateRunableCall = false;
    /* access modifiers changed from: private */
    public HashMap<Integer, Dialog> mDialogs;

    public static void java_assert(String str, int i, String str2) {
    }

    /* access modifiers changed from: package-private */
    public int buttonValue2Int(int i) {
        switch (i) {
            case -3:
                return 2;
            case -2:
                return 1;
            case -1:
                return 0;
            default:
                return 0;
        }
    }

    /* access modifiers changed from: protected */
    public String getFullAppURI() {
        return "ERROR! OVERRIDE ME!";
    }

    /* access modifiers changed from: protected */
    public String getFullVersionGameImageId() {
        return "ERROR! OVERRIDE ME!";
    }

    /* access modifiers changed from: package-private */
    public int int2ButtonValue(int i) {
        switch (i) {
            case 0:
                return -1;
            case 1:
                return -2;
            case 2:
                return -3;
            default:
                return -2;
        }
    }

    /* access modifiers changed from: protected */
    public void loadNativeLibrary() {
        try {
            System.loadLibrary(mNATIVE_LIBRARY_NAME);
        } catch (UnsatisfiedLinkError e) {
            Log.i("MiniclipAndroidActivity", "Library Load Failed");
            e.printStackTrace();
            runOnUiThread(new Runnable() {
                public void run() {
                    cocojava.displayLowStorageMessage(1002);
                }
            });
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        mContext = this;
        setVolumeControlStream(3);
    }

    public static boolean isInstalledOnSdCard() {
        try {
            String absolutePath = mContext.getFilesDir().getAbsolutePath();
            if (absolutePath.startsWith("/data/")) {
                return false;
            }
            if (absolutePath.contains("/mnt/") || absolutePath.contains("/sdcard/")) {
                return true;
            }
            return false;
        } catch (Throwable unused) {
        }
    }

    public static Point getScreenSize() {
        Point point = new Point();
        point.x = MCViewManager.displayWidth;
        point.y = MCViewManager.displayHeight;
        return point;
    }

    public static void displayErrorMessage(String str, String str2) {
        if (!displayedErrorMessage) {
            displayedErrorMessage = true;
            AlertDialog create = new AlertDialog.Builder(mContext).create();
            create.setTitle(str);
            create.setMessage(str2);
            create.setButton(-1, "Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
            create.setOnDismissListener(new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialogInterface) {
                    System.exit(0);
                }
            });
            create.show();
        }
    }

    public static void displayInfoMessage(String str, String str2) {
        AlertDialog create = new AlertDialog.Builder(mContext).create();
        create.setTitle(str);
        create.setMessage(str2);
        create.setButton(-1, "Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        create.show();
    }

    public void firstRun() {
        Log.i("JAVAINFO", "firstRun");
        if (MCApplication.hasFatalErrorOccurred()) {
            displayMCApplicationErrorMessage();
            return;
        }
        loadNativeLibrary();
        if (mUSE_DEVICEID) {
            mDeviceID = getDeviceId();
        }
        try {
            MCAssetManager.firstRun();
            MCApplication.firstRun();
            mHandler = new Handler() {
                public void handleMessage(Message message) {
                    switch (message.what) {
                        case 1:
                            DialogMessage dialogMessage = (DialogMessage) message.obj;
                            cocojava.this.showDialog(dialogMessage.msgId, dialogMessage.title, dialogMessage.message, dialogMessage.cancelable, dialogMessage.buttonTitles);
                            return;
                        case 2:
                            cocojava.this.hideDialog(((DialogMessage) message.obj).msgId);
                            return;
                        case 3:
                            cocojava.this.closeDialog(((DialogMessage) message.obj).msgId);
                            return;
                        case 4:
                            cocojava.this.openURL((String) message.obj);
                            return;
                        case 5:
                            cocojava.this.finish();
                            return;
                        default:
                            return;
                    }
                }
            };
            this.mDialogs = new HashMap<>();
            MCAudio.setEffectsVolume(1.0f);
        } catch (Throwable th) {
            Log.i("JAVAINFO", "Initial Load Failed");
            th.printStackTrace();
            MCApplication.signalFatalError(1003);
            displayMCApplicationErrorMessage();
        }
    }

    /* access modifiers changed from: private */
    public void showDialog(int i, String str, String str2, final boolean z, String[] strArr) {
        Dialog dialog;
        if (!this.mDialogs.containsKey(Integer.valueOf(i))) {
            int i2 = 0;
            if (strArr.length < 4) {
                AlertDialog.Builder message = new AlertDialog.Builder(this).setTitle(str).setMessage(str2);
                while (i2 < strArr.length) {
                    if (int2ButtonValue(i2) == -1) {
                        message.setPositiveButton(strArr[i2], this);
                    } else if (int2ButtonValue(i2) == -2) {
                        message.setNegativeButton(strArr[i2], this);
                    } else if (int2ButtonValue(i2) == -3) {
                        message.setNeutralButton(strArr[i2], this);
                    }
                    i2++;
                }
                dialog = message.create();
            } else {
                AlertDialog create = new AlertDialog.Builder(this).create();
                create.setTitle(str);
                AlertDialog alertDialog = create;
                alertDialog.setMessage(str2);
                while (i2 < strArr.length) {
                    alertDialog.setButton(int2ButtonValue(i2), strArr[i2], this);
                    i2++;
                }
                dialog = create;
            }
            this.mDialogs.put(Integer.valueOf(i), dialog);
        } else {
            dialog = this.mDialogs.get(Integer.valueOf(i));
        }
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (!z || i != 4 || keyEvent.getAction() != 0) {
                    return false;
                }
                Log.i("key down", "KEYCODE_BACK");
                for (Map.Entry entry : cocojava.this.mDialogs.entrySet()) {
                    if (entry.getValue() == dialogInterface) {
                        cocojava.this.closeDialog(((Integer) entry.getKey()).intValue());
                        return true;
                    }
                }
                return false;
            }
        });
        dialog.setCancelable(z);
        dialog.show();
    }

    /* access modifiers changed from: package-private */
    public void hideDialog(int i) {
        closeDialog(i);
    }

    /* access modifiers changed from: package-private */
    public void closeDialog(int i) {
        if (this.mDialogs.containsKey(Integer.valueOf(i))) {
            Dialog dialog = this.mDialogs.get(Integer.valueOf(i));
            if (dialog != null) {
                dialog.dismiss();
            }
            this.mDialogs.remove(Integer.valueOf(i));
        }
    }

    public static void showMessageBox(int i, String str, String str2, boolean z, String[] strArr) {
        DialogMessage dialogMessage = new DialogMessage(i);
        dialogMessage.title = str;
        dialogMessage.message = str2;
        dialogMessage.cancelable = z;
        dialogMessage.buttonTitles = strArr;
        Message message = new Message();
        message.what = 1;
        message.obj = dialogMessage;
        mHandler.sendMessage(message);
    }

    public static void hideMessageBox(int i) {
        Message message = new Message();
        message.what = 2;
        message.obj = new DialogMessage(i);
        mHandler.sendMessage(message);
    }

    public static void closeMessageBox(int i) {
        Message message = new Message();
        message.what = 3;
        message.obj = new DialogMessage(i);
        mHandler.sendMessage(message);
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        for (Map.Entry next : this.mDialogs.entrySet()) {
            if (next.getValue() == dialogInterface) {
                CocoJNI.MonMessageBoxButtonPressed(((Integer) next.getKey()).intValue(), buttonValue2Int(i));
                return;
            }
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        System.exit(0);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00d0, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d1, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00d5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00d6, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00d5 A[ExcHandler: NameNotFoundException (r0v2 'e' android.content.pm.PackageManager$NameNotFoundException A[CUSTOM_DECLARE]), Splitter:B:3:0x002d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void createResources() {
        /*
            r8 = this;
            java.lang.String r0 = "JAVAINFO"
            java.io.File r1 = r8.getFilesDir()
            java.lang.String r1 = r1.getAbsolutePath()
            android.util.Log.i(r0, r1)
            java.io.File r0 = new java.io.File
            java.io.File r1 = r8.getFilesDir()
            java.lang.String r2 = "Contents/Resources"
            r0.<init>(r1, r2)
            java.lang.String r1 = "JAVAINFO"
            java.lang.String r2 = r0.getAbsolutePath()
            android.util.Log.i(r1, r2)
            r0.mkdirs()
            boolean r1 = com.miniclip.nativeJNI.cocojava.mUpdateRunableCall
            if (r1 == 0) goto L_0x002d
            java.lang.Runnable r1 = com.miniclip.nativeJNI.cocojava.mUpdateRunable
            r1.run()
        L_0x002d:
            android.content.pm.PackageManager r1 = r8.getPackageManager()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            java.lang.String r2 = r8.getPackageName()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            r3 = 0
            android.content.pm.ApplicationInfo r1 = r1.getApplicationInfo(r2, r3)     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            java.lang.String r2 = "JAVAINFO"
            java.lang.String r4 = r1.sourceDir     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            android.util.Log.i(r2, r4)     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            java.lang.String r2 = "JAVAINFO"
            java.lang.String r4 = r1.dataDir     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            android.util.Log.i(r2, r4)     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            java.util.zip.ZipFile r2 = new java.util.zip.ZipFile     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            java.lang.String r1 = r1.sourceDir     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            r2.<init>(r1)     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            java.util.Enumeration r1 = r2.entries()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
        L_0x0053:
            boolean r4 = r1.hasMoreElements()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            if (r4 == 0) goto L_0x00d9
            java.lang.Object r4 = r1.nextElement()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            java.util.zip.ZipEntry r4 = (java.util.zip.ZipEntry) r4     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            java.lang.String r5 = r4.getName()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            int r5 = r5.length()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            r6 = 15
            if (r5 >= r6) goto L_0x006c
            goto L_0x0053
        L_0x006c:
            java.lang.String r5 = "assets/unpack/"
            java.lang.String r6 = r4.getName()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            r7 = 14
            java.lang.String r6 = r6.substring(r3, r7)     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            boolean r5 = r5.equals(r6)     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            if (r5 == 0) goto L_0x0053
            java.io.File r5 = new java.io.File     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            java.lang.String r6 = r4.getName()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            java.lang.String r6 = r6.substring(r7)     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            r5.<init>(r0, r6)     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            java.io.File r6 = r5.getParentFile()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            if (r6 == 0) goto L_0x0094
            r6.mkdirs()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
        L_0x0094:
            boolean r5 = r5.exists()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            if (r5 == 0) goto L_0x009b
            goto L_0x0053
        L_0x009b:
            java.io.InputStream r5 = r2.getInputStream(r4)     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            java.io.File r6 = new java.io.File     // Catch:{ FileNotFoundException -> 0x00cb, IOException -> 0x00c6, NameNotFoundException -> 0x00d5 }
            java.lang.String r4 = r4.getName()     // Catch:{ FileNotFoundException -> 0x00cb, IOException -> 0x00c6, NameNotFoundException -> 0x00d5 }
            java.lang.String r4 = r4.substring(r7)     // Catch:{ FileNotFoundException -> 0x00cb, IOException -> 0x00c6, NameNotFoundException -> 0x00d5 }
            r6.<init>(r0, r4)     // Catch:{ FileNotFoundException -> 0x00cb, IOException -> 0x00c6, NameNotFoundException -> 0x00d5 }
            r6.delete()     // Catch:{ FileNotFoundException -> 0x00cb, IOException -> 0x00c6, NameNotFoundException -> 0x00d5 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00cb, IOException -> 0x00c6, NameNotFoundException -> 0x00d5 }
            r4.<init>(r6)     // Catch:{ FileNotFoundException -> 0x00cb, IOException -> 0x00c6, NameNotFoundException -> 0x00d5 }
            r6 = 4096(0x1000, float:5.74E-42)
            byte[] r6 = new byte[r6]     // Catch:{ FileNotFoundException -> 0x00cb, IOException -> 0x00c6, NameNotFoundException -> 0x00d5 }
        L_0x00b8:
            int r7 = r5.read(r6)     // Catch:{ FileNotFoundException -> 0x00cb, IOException -> 0x00c6, NameNotFoundException -> 0x00d5 }
            if (r7 < 0) goto L_0x00c2
            r4.write(r6, r3, r7)     // Catch:{ FileNotFoundException -> 0x00cb, IOException -> 0x00c6, NameNotFoundException -> 0x00d5 }
            goto L_0x00b8
        L_0x00c2:
            r4.close()     // Catch:{ FileNotFoundException -> 0x00cb, IOException -> 0x00c6, NameNotFoundException -> 0x00d5 }
            goto L_0x0053
        L_0x00c6:
            r4 = move-exception
            r4.printStackTrace()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            goto L_0x0053
        L_0x00cb:
            r4 = move-exception
            r4.printStackTrace()     // Catch:{ NameNotFoundException -> 0x00d5, IOException -> 0x00d0 }
            goto L_0x0053
        L_0x00d0:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00d9
        L_0x00d5:
            r0 = move-exception
            r0.printStackTrace()
        L_0x00d9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.miniclip.nativeJNI.cocojava.createResources():void");
    }

    public static int getAndroidVersion() {
        return Build.VERSION.SDK_INT;
    }

    public static void SharedPreferences_setInt(String str, int i) {
        SharedPreferences.Editor edit = mContext.getSharedPreferences("GAME_INFO", 0).edit();
        edit.putInt(str, i);
        edit.commit();
    }

    public static int SharedPreferences_getInt(String str) {
        return mContext.getSharedPreferences("GAME_INFO", 0).getInt(str, 0);
    }

    public static void SharedPreferences_setString(String str, String str2) {
        try {
            SharedPreferences.Editor edit = mContext.getSharedPreferences("GAME_INFO", 0).edit();
            edit.putString(str, str2);
            edit.commit();
        } catch (Exception unused) {
            Log.i("JAVAINFO", "Failed to set user defaults: key = " + str + " value= " + str2);
        }
    }

    public static String SharedPreferences_getString(String str) {
        if (mContext == null) {
            return "";
        }
        return mContext.getSharedPreferences("GAME_INFO", 0).getString(str, "");
    }

    public void logCustomEvent(String str, String str2) {
        Log.i("cocojava", "OVERRIDE logCustomEvent");
    }

    public static void custom_logEvent(final String str, final String str2) {
        mContext.runOnUiThread(new Runnable() {
            public void run() {
                ((cocojava) cocojava.mContext).logCustomEvent(str, str2);
            }
        });
    }

    public static String percentEncodeUTF8(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void openLink(String str) {
        Message message = new Message();
        message.what = 4;
        message.obj = str;
        mHandler.sendMessage(message);
    }

    private void showNoInternetDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please enable internet").setCancelable(false).setPositiveButton("Back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void openURL(String str) {
        if (!isOnline()) {
            showNoInternetDialog();
            return;
        }
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(str));
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected static JSONObject stringToJSON(String str) {
        try {
            return new JSONObject(str);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void exitApplication() {
        Message message = new Message();
        message.what = 5;
        mHandler.sendMessage(message);
    }

    public static void cleanAndroidData() {
        SharedPreferences_setString("APP_VERSION_NUMBER", "0");
        System.exit(0);
    }

    public boolean isOnline() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        return activeNetworkInfo.isConnectedOrConnecting();
    }

    public static void vibrateForMS(int i) {
        ((Vibrator) mContext.getSystemService("vibrator")).vibrate((long) i);
    }

    public static float availableMemoryOnDevice() {
        StatFs statFs = new StatFs(mContext.getFilesDir().getAbsolutePath());
        statFs.getBlockCount();
        statFs.getAvailableBlocks();
        return (float) ((((long) statFs.getFreeBlocks()) * ((long) statFs.getBlockSize())) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(13:0|1|2|3|4|5|6|7|8|(2:11|9)|17|12|13) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0010 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String md5(java.lang.String r5) {
        /*
            r4 = this;
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            java.lang.String r1 = "UTF-8"
            byte[] r1 = r5.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x0010 }
            r0.update(r1)     // Catch:{ UnsupportedEncodingException -> 0x0010 }
            goto L_0x0017
        L_0x0010:
            byte[] r5 = r5.getBytes()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r0.update(r5)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
        L_0x0017:
            byte[] r5 = r0.digest()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r0.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r1 = 0
        L_0x0021:
            int r2 = r5.length     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            if (r1 >= r2) goto L_0x0033
            r2 = 255(0xff, float:3.57E-43)
            byte r3 = r5[r1]     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r2 = r2 & r3
            java.lang.String r2 = java.lang.Integer.toHexString(r2)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r0.append(r2)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            int r1 = r1 + 1
            goto L_0x0021
        L_0x0033:
            java.lang.String r5 = r0.toString()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            return r5
        L_0x0038:
            r5 = move-exception
            r5.printStackTrace()
            r5 = 0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.miniclip.nativeJNI.cocojava.md5(java.lang.String):java.lang.String");
    }

    public static void showHtmlDialog(final String str, final int i) {
        mContext.runOnUiThread(new Runnable() {
            public void run() {
                new HtmlDialog(cocojava.mContext, str, i, new HtmlDialog.HtmlDialogListener() {
                    public void onCancel() {
                    }

                    public void onComplete() {
                    }

                    public void onError() {
                    }
                }).show();
            }
        });
    }

    public static int DeviceSupportsMultitouch() {
        return mContext.getPackageManager().hasSystemFeature("android.hardware.touchscreen.multitouch") ? 1 : 0;
    }

    public static String getAppVersionNumber() {
        return MCApplication.getApplicationVersionNumber();
    }

    public static void sendEmail(final String str, final String str2, final String str3) {
        mContext.runOnUiThread(new Runnable() {
            public void run() {
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("text/plain");
                if (str != null) {
                    intent.putExtra("android.intent.extra.EMAIL", new String[]{str});
                }
                if (str2 != null) {
                    intent.putExtra("android.intent.extra.SUBJECT", str2);
                }
                if (str3 != null) {
                    intent.putExtra("android.intent.extra.TEXT", str3);
                }
                cocojava.mContext.startActivity(Intent.createChooser(intent, "Send email..."));
            }
        });
    }

    public static void sendSMS(final String str) {
        mContext.runOnUiThread(new Runnable() {
            public void run() {
                Intent intent = new Intent("android.intent.action.VIEW");
                if (str != null) {
                    intent.putExtra("sms_body", str);
                }
                intent.setType("vnd.android-dir/mms-sms");
                cocojava.mContext.startActivity(intent);
            }
        });
    }

    public static double NTP_JAVA_getOffsetFromServer(String str) {
        try {
            return mNtpHandler.getOffsetFromServer(str);
        } catch (IOException e) {
            e.printStackTrace();
            return 0.0d;
        }
    }

    public static void NTP_JAVA_getOffsetFromServerListAsync(String str, int i, int i2) {
        mNtpHandler.getOffsetFromServerListAsync(str, i, i2);
    }

    public static void PING_JAVA_simplePingAsync(String str, int i, int i2) {
        mPingHandler.simplePingAsync(str, i, i2);
    }

    public static String getDeviceId() {
        String SharedPreferences_getString = SharedPreferences_getString("Device Id");
        if (SharedPreferences_getString == "") {
            Log.i("JAVAINFO", "Trying to get miniclipId from sdcard");
            String str = "";
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File("/sdcard/miniclipId.txt")), "UTF-8"));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    str = str + readLine;
                }
                bufferedReader.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            if (str != "") {
                SharedPreferences_setString("Device Id", str);
                Log.i("JAVAINFO", "DeviceID from SD Card = " + str);
                SharedPreferences_getString = str;
            }
        }
        if (SharedPreferences_getString == "") {
            Log.i("JAVAINFO", "Generating miniclipId");
            String valueOf = String.valueOf(new Date().getTime());
            Random random = new Random();
            char[] cArr = new char[10];
            for (int i = 0; i < 10; i++) {
                cArr[i] = "abcdefghijmnopqrstuvxz0123456789".charAt(random.nextInt("abcdefghijmnopqrstuvxz0123456789".length()));
            }
            SharedPreferences_getString = valueOf + "-" + new String(cArr);
            SharedPreferences_setString("Device Id", SharedPreferences_getString);
        }
        File file = new File("/sdcard/miniclipId.txt");
        if (SharedPreferences_getString != "" && !file.exists()) {
            Log.i("JAVAINFO", "Saving miniclipId to sdcard");
            try {
                file.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, "UTF-8");
                outputStreamWriter.append((CharSequence) SharedPreferences_getString);
                outputStreamWriter.close();
                fileOutputStream.close();
                Log.i("JAVAINFO", "DeviceID stored to sdcard = " + SharedPreferences_getString);
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
        Log.i("JAVAINFO", "DeviceID = " + SharedPreferences_getString);
        return SharedPreferences_getString;
    }

    public static void setClipboardText(final String str) {
        mContext.runOnUiThread(new Runnable() {
            public void run() {
                ((ClipboardManager) cocojava.mContext.getSystemService("clipboard")).setText(str);
            }
        });
    }

    public static void copyAsset(String str) {
        try {
            InputStream open = mContext.getAssets().open(str);
            FileOutputStream fileOutputStream = new FileOutputStream("/sdcard/" + str);
            copyFile(open, fileOutputStream);
            open.close();
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
    }

    public static void copyFile(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public void showDatePickerDialog(int i, int i2, int i3) {
        showDatePickerDialog(i, i2, i3, 0);
    }

    public void showDatePickerDialog(int i, int i2, int i3, int i4) {
        int i5 = i2 - 1;
        if (i4 == 0) {
            i4 = mContext.getResources().getIdentifier("DatePicker", TJAdUnitConstants.String.STYLE, mContext.getPackageName());
        }
        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, i4, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datePicker, final int i, final int i2, final int i3) {
                cocojava.this.queueEvent(ThreadingContext.GlThread, new Runnable() {
                    public void run() {
                        CocoJNI.MdatePickerResponce(i, i2 + 1, i3);
                        CocoJNI.MdatePickerClosed();
                    }
                });
            }
        }, i, i5, i3);
        datePickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                cocojava.this.queueEvent(ThreadingContext.GlThread, new Runnable() {
                    public void run() {
                        CocoJNI.MdatePickerClosed();
                    }
                });
            }
        });
        datePickerDialog.show();
    }

    public static void showDatePickerDialog_JNI(final int i, final int i2, final int i3) {
        mContext.runOnUiThread(new Runnable() {
            public void run() {
                ((cocojava) cocojava.mContext).showDatePickerDialog(i, i2, i3);
            }
        });
    }

    public static void showDatePickerDialog_JNI(final int i, final int i2, final int i3, final int i4) {
        mContext.runOnUiThread(new Runnable() {
            public void run() {
                ((cocojava) cocojava.mContext).showDatePickerDialog(i, i2, i3, i4);
            }
        });
    }

    public static String getDeviceInfo(String str) {
        if (str.contentEquals(TapjoyConstants.TJC_NOTIFICATION_DEVICE_PREFIX)) {
            return Build.DEVICE;
        }
        if (str.contentEquals(ProviderConfig.MCVideoAdsVersionKey)) {
            return Build.VERSION.RELEASE;
        }
        if (str.contentEquals("brand")) {
            return Build.BRAND;
        }
        if (!str.contentEquals(TJAdUnitConstants.String.USAGE_TRACKER_NAME)) {
            return "";
        }
        String str2 = Build.MANUFACTURER;
        String str3 = Build.MODEL;
        if (str3.startsWith(str2)) {
            return str3;
        }
        return str2 + " " + str3;
    }

    public static String getResourceString(String str, String str2) {
        try {
            int identifier = mContext.getResources().getIdentifier(str, "string", mContext.getPackageName());
            return identifier > 0 ? mContext.getString(identifier) : str2;
        } catch (Exception unused) {
            return str2;
        }
    }

    public static void displayLowStorageMessage() {
        displayLowStorageMessage(MCApplication.FATAL_ERROR_LOW_STORAGE);
    }

    public static void displayLowStorageMessage(int i) {
        MCApplication.signalFatalError(i);
        displayErrorMessage(getResourceString("load_library_error_title", "Unexpected Error (" + MCApplication.getFatalErrorCode() + ")"), String.format(getResourceString("load_library_error_text", "Please check if you have at least %d MB of free disk space.\nIf the problem persists, reinstall the game or contact us at support@miniclip.com."), 20));
    }

    public static void displayMCApplicationErrorMessage() {
        displayErrorMessage("Unexpected Error (" + MCApplication.getFatalErrorCode() + ")", "Please restart or reinstall the game.\nIf the problem persists, please contact us at support@miniclip.com");
    }

    public static void displayWaitingForOperationMessage() {
        displayInfoMessage("", getResourceString("pending_operation_text", "Waiting for current operation. If it takes too long, please restart and try again."));
    }

    public static void deleteDirectory(File file) {
        if (file.isDirectory()) {
            for (File file2 : file.listFiles()) {
                if (file2.isFile()) {
                    Log.i("Activity", file2.getName());
                    file2.delete();
                } else {
                    deleteDirectory(file2);
                }
            }
            file.delete();
        }
    }

    public static int getTimezoneOffset() {
        return new GregorianCalendar().getTimeZone().getOffset(new Date().getTime()) / 1000;
    }

    public static String getIPAddress(boolean z) {
        try {
            for (NetworkInterface inetAddresses : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                Iterator it = Collections.list(inetAddresses.getInetAddresses()).iterator();
                while (true) {
                    if (it.hasNext()) {
                        InetAddress inetAddress = (InetAddress) it.next();
                        if (!inetAddress.isLoopbackAddress()) {
                            String upperCase = inetAddress.getHostAddress().toUpperCase();
                            boolean z2 = inetAddress instanceof Inet4Address;
                            if (z && z2) {
                                return upperCase;
                            }
                            if (!z && !z2) {
                                int indexOf = upperCase.indexOf(37);
                                return indexOf < 0 ? upperCase : upperCase.substring(0, indexOf);
                            }
                        }
                    }
                }
            }
            return "";
        } catch (Exception unused) {
            return "";
        }
    }

    public static class AssertException extends RuntimeException {
        public AssertException(String str, int i, String str2) {
            super("File: " + str + " Line: " + i + " Function: " + str2);
        }
    }
}
