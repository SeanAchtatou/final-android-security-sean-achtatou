package com.miniclip.nativeJNI;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.achievement.Achievement;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.Snapshots;
import com.miniclip.plagueinc.R;
import java.io.IOException;
import java.util.ArrayList;

public class GooglePlayServices implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final int RC_RESOLVE = 5000;
    private static final int RC_SAVED_GAMES = 9009;
    private static final int RC_SIGN_IN = 9001;
    private static final int RC_UNUSED = 5001;
    static final String SIGN_IN_ERROR_MESSAGE = "Could not sign in. Please try again.";
    static final String SIGN_IN_ERROR_OTHER_MESSAGE = "Could not sign in. Please try again. Other";
    static final String SIGN_IN_MESSAGE = "Signing in with Google...";
    static final String SIGN_OUT_MESSAGE = "Signing out...";
    /* access modifiers changed from: private */
    public static Activity mActivity;
    public static String mAdvertisingID;
    /* access modifiers changed from: private */
    public String TAG = "GooglePlayServices";
    private boolean mAutoStartSignInFlow = true;
    /* access modifiers changed from: private */
    public GoogleApiClient mGoogleApiClient;
    private GooglePlayServicesListener mListener = null;
    private boolean mResolvingConnectionFailure = false;
    private boolean mSignInClicked = false;

    public interface GooglePlayServicesListener {
        void onSignInFailed();

        void onSignInSucceeded();
    }

    public interface OnAchievementsLoadedListener {
        void onAchievementsLoaded(ArrayList<AchievementEntry> arrayList);
    }

    public interface OnLeaderboardScoresLoadedListener {
        void onLeaderboardScoresLoaded(ArrayList<LeaderboardEntry> arrayList);
    }

    public interface OnSnapshotLoadListener {
        void onConflict(String str, Snapshot snapshot, Snapshot snapshot2);

        void onFailure(int i);

        void onSuccess(int i, Snapshot snapshot);
    }

    public interface OnSnapshotUpdateListener {
        void onResult(int i);
    }

    public interface OnStateLoadedListener {
        void onStateConflict(int i, String str, byte[] bArr, byte[] bArr2);

        void onStateLoaded(int i, int i2, byte[] bArr);
    }

    public class LeaderboardEntry {
        public String displayName;
        public String displayScore;

        public LeaderboardEntry() {
        }
    }

    public class AchievementEntry {
        public String achievementId;
        public String description;
        public String name;
        public int state;

        public AchievementEntry() {
        }
    }

    public GooglePlayServices(Activity activity) {
        mActivity = activity;
        new Thread(new Runnable() {
            public void run() {
                try {
                    GooglePlayServices.mAdvertisingID = AdvertisingIdClient.getAdvertisingIdInfo(GooglePlayServices.mActivity).getId();
                } catch (Exception unused) {
                }
            }
        }).start();
    }

    public void setup(GooglePlayServicesListener googlePlayServicesListener) {
        this.mListener = googlePlayServicesListener;
        if (this.mGoogleApiClient == null) {
            this.mGoogleApiClient = new GoogleApiClient.Builder(mActivity).addApi(Drive.API).addScope(Drive.SCOPE_APPFOLDER).addApi(Games.API).addScope(Games.SCOPE_GAMES).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
        }
        this.mGoogleApiClient.connect();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 9001) {
            this.mSignInClicked = false;
            this.mResolvingConnectionFailure = false;
            if (i2 == -1) {
                this.mGoogleApiClient.connect();
            } else {
                BaseGameUtils.showActivityResultError(mActivity, i, i2, SIGN_IN_ERROR_MESSAGE);
            }
        }
    }

    public void onConnected(Bundle bundle) {
        Log.d(this.TAG, "onConnected(): connected to Google APIs");
    }

    public void onConnectionSuspended(int i) {
        Log.d(this.TAG, "onConnectionSuspended(): attempting to connect");
        this.mGoogleApiClient.connect();
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(this.TAG, "onConnectionFailed(): attempting to resolve");
        if (this.mResolvingConnectionFailure) {
            Log.d(this.TAG, "onConnectionFailed(): already resolving");
        } else if (this.mSignInClicked || this.mAutoStartSignInFlow) {
            this.mAutoStartSignInFlow = false;
            this.mSignInClicked = false;
            this.mResolvingConnectionFailure = true;
            if (!BaseGameUtils.resolveConnectionFailure(mActivity, this.mGoogleApiClient, connectionResult, 9001, SIGN_IN_ERROR_OTHER_MESSAGE)) {
                this.mResolvingConnectionFailure = false;
            }
        }
    }

    public boolean isSignedIn() {
        return this.mGoogleApiClient != null && this.mGoogleApiClient.isConnected();
    }

    public void onStart(Activity activity) {
        this.mGoogleApiClient.connect();
    }

    public void onStop() {
        if (this.mGoogleApiClient.isConnected()) {
            this.mGoogleApiClient.disconnect();
        }
    }

    public void signOut() {
        this.mSignInClicked = false;
        Games.signOut(this.mGoogleApiClient);
        if (this.mGoogleApiClient.isConnected()) {
            this.mGoogleApiClient.disconnect();
        }
    }

    public void signIn() {
        this.mSignInClicked = true;
        this.mGoogleApiClient.connect();
    }

    public void showLeaderboard(String str) {
        if (isSignedIn()) {
            mActivity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(this.mGoogleApiClient, str), RC_UNUSED);
        } else {
            signIn();
        }
    }

    public void showLeaderboards() {
        if (isSignedIn()) {
            mActivity.startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(this.mGoogleApiClient), RC_UNUSED);
        } else {
            signIn();
        }
    }

    public void showAchievements() {
        if (isSignedIn()) {
            mActivity.startActivityForResult(Games.Achievements.getAchievementsIntent(this.mGoogleApiClient), RC_UNUSED);
        } else {
            signIn();
        }
    }

    public void updateScore(String str, long j, Object obj) {
        if (isSignedIn() && j >= 0) {
            Games.Leaderboards.submitScore(this.mGoogleApiClient, str, j);
        }
    }

    public void updateAchievement(String str, float f, Object obj) {
        if (isSignedIn()) {
            Games.Achievements.increment(this.mGoogleApiClient, str, (int) f);
        }
    }

    public void unlockAchievement(String str) {
        if (isSignedIn()) {
            Games.Achievements.unlock(this.mGoogleApiClient, str);
        }
    }

    public void loadTopScores(final OnLeaderboardScoresLoadedListener onLeaderboardScoresLoadedListener, String str, int i, int i2, int i3) {
        if (isSignedIn()) {
            Games.Leaderboards.loadTopScores(this.mGoogleApiClient, str, i, i2, i3).setResultCallback(new ResultCallback<Leaderboards.LoadScoresResult>() {
                public void onResult(Leaderboards.LoadScoresResult loadScoresResult) {
                    if (loadScoresResult.getStatus().getStatusCode() == 0) {
                        loadScoresResult.getLeaderboard().getLeaderboardId();
                        loadScoresResult.getLeaderboard().getDisplayName();
                        LeaderboardScoreBuffer scores = loadScoresResult.getScores();
                        ArrayList arrayList = new ArrayList();
                        int count = scores.getCount();
                        for (int i = 0; i < count; i++) {
                            LeaderboardScore leaderboardScore = scores.get(i);
                            LeaderboardEntry leaderboardEntry = new LeaderboardEntry();
                            leaderboardEntry.displayName = leaderboardScore.getScoreHolderDisplayName();
                            leaderboardEntry.displayScore = leaderboardScore.getDisplayScore();
                            arrayList.add(leaderboardEntry);
                        }
                        onLeaderboardScoresLoadedListener.onLeaderboardScoresLoaded(arrayList);
                        scores.close();
                    }
                }
            });
        }
    }

    public void loadAchievements(final OnAchievementsLoadedListener onAchievementsLoadedListener) {
        if (isSignedIn()) {
            Games.Achievements.load(this.mGoogleApiClient, true).setResultCallback(new ResultCallback<Achievements.LoadAchievementsResult>() {
                public void onResult(Achievements.LoadAchievementsResult loadAchievementsResult) {
                    if (loadAchievementsResult.getStatus().getStatusCode() == 0) {
                        ArrayList arrayList = new ArrayList();
                        AchievementBuffer achievements = loadAchievementsResult.getAchievements();
                        int count = achievements.getCount();
                        for (int i = 0; i < count; i++) {
                            Achievement achievement = achievements.get(i);
                            AchievementEntry achievementEntry = new AchievementEntry();
                            achievementEntry.name = achievement.getName();
                            achievementEntry.achievementId = achievement.getAchievementId();
                            achievementEntry.description = achievement.getDescription();
                            achievementEntry.state = achievement.getState();
                            arrayList.add(achievementEntry);
                        }
                        onAchievementsLoadedListener.onAchievementsLoaded(arrayList);
                        achievements.close();
                    }
                }
            });
        }
    }

    public String getCurrentPlayerName() {
        if (!isSignedIn()) {
            return "";
        }
        Player currentPlayer = Games.Players.getCurrentPlayer(this.mGoogleApiClient);
        if (currentPlayer != null) {
            return currentPlayer.getDisplayName();
        }
        Log.w(this.TAG, "mGamesClient.getCurrentPlayer() is NULL!");
        return "???";
    }

    private void loadCloudSaveSnapshot(final String str, final OnSnapshotLoadListener onSnapshotLoadListener) {
        new AsyncTask<Void, Void, Integer>() {
            /* access modifiers changed from: protected */
            public void onPostExecute(Integer num) {
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.google.android.gms.games.snapshot.Snapshots.open(com.google.android.gms.common.api.GoogleApiClient, java.lang.String, boolean):com.google.android.gms.common.api.PendingResult<com.google.android.gms.games.snapshot.Snapshots$OpenSnapshotResult>
             arg types: [com.google.android.gms.common.api.GoogleApiClient, java.lang.String, int]
             candidates:
              com.google.android.gms.games.snapshot.Snapshots.open(com.google.android.gms.common.api.GoogleApiClient, com.google.android.gms.games.snapshot.SnapshotMetadata, int):com.google.android.gms.common.api.PendingResult<com.google.android.gms.games.snapshot.Snapshots$OpenSnapshotResult>
              com.google.android.gms.games.snapshot.Snapshots.open(com.google.android.gms.common.api.GoogleApiClient, java.lang.String, boolean):com.google.android.gms.common.api.PendingResult<com.google.android.gms.games.snapshot.Snapshots$OpenSnapshotResult> */
            /* access modifiers changed from: protected */
            public Integer doInBackground(Void... voidArr) {
                Snapshots.OpenSnapshotResult await = Games.Snapshots.open(GooglePlayServices.this.mGoogleApiClient, str, true).await();
                int statusCode = await.getStatus().getStatusCode();
                if (statusCode == 4004) {
                    onSnapshotLoadListener.onConflict(await.getConflictId(), await.getConflictingSnapshot(), await.getSnapshot());
                } else if (statusCode == 0) {
                    onSnapshotLoadListener.onSuccess(statusCode, await.getSnapshot());
                } else {
                    String access$200 = GooglePlayServices.this.TAG;
                    Log.e(access$200, "Error while loading: " + await.getStatus().getStatusCode());
                    onSnapshotLoadListener.onFailure(statusCode);
                }
                String access$2002 = GooglePlayServices.this.TAG;
                Log.i(access$2002, "loadCloudSaveSnapshot: " + statusCode);
                return Integer.valueOf(statusCode);
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void updateCloudSaveSnapshot(final Snapshot snapshot, final byte[] bArr, final OnSnapshotUpdateListener onSnapshotUpdateListener) {
        new AsyncTask<Void, Void, Integer>() {
            /* access modifiers changed from: protected */
            public void onPostExecute(Integer num) {
            }

            /* access modifiers changed from: protected */
            public Integer doInBackground(Void... voidArr) {
                Bitmap decodeResource = BitmapFactory.decodeResource(GooglePlayServices.mActivity.getResources(), R.drawable.icon);
                snapshot.getSnapshotContents().writeBytes(bArr);
                int statusCode = Games.Snapshots.commitAndClose(GooglePlayServices.this.mGoogleApiClient, snapshot, new SnapshotMetadataChange.Builder().setCoverImage(decodeResource).setDescription("Plague Inc Android Save").build()).await().getStatus().getStatusCode();
                onSnapshotUpdateListener.onResult(statusCode);
                String access$200 = GooglePlayServices.this.TAG;
                Log.i(access$200, "updateCloudSaveSnapshot: " + statusCode);
                return Integer.valueOf(statusCode);
            }
        }.execute(new Void[0]);
    }

    public void loadCloudSave(final OnStateLoadedListener onStateLoadedListener, final int i) {
        if (isSignedIn()) {
            loadCloudSaveSnapshot("plague_save", new OnSnapshotLoadListener() {
                public void onConflict(String str, Snapshot snapshot, Snapshot snapshot2) {
                    try {
                        byte[] readFully = snapshot2.getSnapshotContents().readFully();
                        onStateLoadedListener.onStateConflict(i, str, snapshot.getSnapshotContents().readFully(), readFully);
                    } catch (IOException e) {
                        Log.e(GooglePlayServices.this.TAG, "Error while reading Snapshot.", e);
                    }
                }

                public void onSuccess(int i, Snapshot snapshot) {
                    try {
                        onStateLoadedListener.onStateLoaded(i, i, snapshot.getSnapshotContents().readFully());
                    } catch (IOException e) {
                        Log.e(GooglePlayServices.this.TAG, "Error while reading Snapshot.", e);
                    }
                }

                public void onFailure(int i) {
                    onStateLoadedListener.onStateLoaded(i, i, null);
                }
            });
        }
    }

    public void updateCloudSaveImmediate(final OnStateLoadedListener onStateLoadedListener, final int i, final byte[] bArr) {
        if (isSignedIn()) {
            loadCloudSaveSnapshot("plague_save", new OnSnapshotLoadListener() {
                public void onConflict(String str, Snapshot snapshot, Snapshot snapshot2) {
                    try {
                        byte[] readFully = snapshot2.getSnapshotContents().readFully();
                        onStateLoadedListener.onStateConflict(i, str, snapshot.getSnapshotContents().readFully(), readFully);
                    } catch (IOException e) {
                        Log.e(GooglePlayServices.this.TAG, "Error while reading Snapshot.", e);
                    }
                }

                public void onSuccess(int i, Snapshot snapshot) {
                    try {
                        byte[] readFully = snapshot.getSnapshotContents().readFully();
                        onStateLoadedListener.onStateConflict(i, "MCLocalMerge", bArr, readFully);
                    } catch (IOException e) {
                        Log.e(GooglePlayServices.this.TAG, "Error while reading Snapshot.", e);
                    }
                }

                public void onFailure(int i) {
                    onStateLoadedListener.onStateLoaded(i, i, null);
                }
            });
        }
    }

    public void resolveCloudSave(final OnStateLoadedListener onStateLoadedListener, final int i, String str, final byte[] bArr) {
        if (!isSignedIn()) {
            return;
        }
        if (str.equals("MCLocalMerge")) {
            loadCloudSaveSnapshot("plague_save", new OnSnapshotLoadListener() {
                public void onConflict(String str, Snapshot snapshot, Snapshot snapshot2) {
                    Log.d(GooglePlayServices.this.TAG, "Error snapshot conflict on MCLocalMerge");
                }

                public void onSuccess(int i, Snapshot snapshot) {
                    GooglePlayServices.this.updateCloudSaveSnapshot(snapshot, bArr, new OnSnapshotUpdateListener() {
                        public void onResult(int i) {
                            GooglePlayServices.this.loadCloudSave(onStateLoadedListener, i);
                        }
                    });
                }

                public void onFailure(int i) {
                    onStateLoadedListener.onStateLoaded(i, i, null);
                }
            });
            return;
        }
        final String str2 = str;
        final OnStateLoadedListener onStateLoadedListener2 = onStateLoadedListener;
        final int i2 = i;
        final byte[] bArr2 = bArr;
        loadCloudSaveSnapshot("plague_save", new OnSnapshotLoadListener() {
            public void onConflict(String str, Snapshot snapshot, Snapshot snapshot2) {
                if (str2 != str) {
                    Log.d(GooglePlayServices.this.TAG, "Error snapshot conflictId mismatch");
                    return;
                }
                try {
                    snapshot2.getSnapshotContents().readFully();
                    snapshot.getSnapshotContents().readFully();
                    if (snapshot2.getMetadata().getLastModifiedTimestamp() >= snapshot.getMetadata().getLastModifiedTimestamp()) {
                        snapshot = snapshot2;
                    }
                    Snapshots.OpenSnapshotResult await = Games.Snapshots.resolveConflict(GooglePlayServices.this.mGoogleApiClient, str2, snapshot).await();
                    String access$200 = GooglePlayServices.this.TAG;
                    Log.i(access$200, "resolveCloudSave: " + await.getStatus().getStatusCode());
                    if (await.getStatus().isSuccess()) {
                        GooglePlayServices.this.updateCloudSaveImmediate(onStateLoadedListener2, i2, bArr2);
                    }
                } catch (IOException e) {
                    Log.e(GooglePlayServices.this.TAG, "Error while reading Snapshot.", e);
                }
            }

            public void onSuccess(int i, Snapshot snapshot) {
                try {
                    onStateLoadedListener2.onStateLoaded(i, i2, snapshot.getSnapshotContents().readFully());
                } catch (IOException e) {
                    Log.e(GooglePlayServices.this.TAG, "Error while reading Snapshot.", e);
                }
            }

            public void onFailure(int i) {
                onStateLoadedListener2.onStateLoaded(i, i2, null);
            }
        });
    }

    public static String getGoogleAdvertisingID() {
        return mAdvertisingID != null ? mAdvertisingID : "";
    }

    public static String GPM_getGoogleAdvertisingID() {
        return getGoogleAdvertisingID();
    }
}
