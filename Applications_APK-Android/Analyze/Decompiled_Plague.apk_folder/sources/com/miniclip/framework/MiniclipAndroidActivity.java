package com.miniclip.framework;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.RelativeLayout;
import com.miniclip.audio.MCAudio;
import com.miniclip.input.MCAccelerometer;
import com.miniclip.input.MCInput;
import com.miniclip.platform.MCApplication;
import com.miniclip.platform.MCAssetManager;
import com.miniclip.platform.MCViewManager;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.SortedMap;
import java.util.TreeMap;

public abstract class MiniclipAndroidActivity extends MiniclipBaseActivity {
    private static final Comparator<Comparable> REVERSE_ORDER = new Comparator<Comparable>() {
        public int compare(Comparable comparable, Comparable comparable2) {
            return comparable2.compareTo(comparable);
        }
    };
    private SortedMap<Integer, LinkedHashSet<InputHandler>> inputHandlers = new TreeMap(REVERSE_ORDER);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        MCViewManager.init(this);
        MCAudio.init(this);
        MCAccelerometer.init(this);
        MCInput.init(this);
        MCApplication.init();
        MCAssetManager.init(this);
    }

    public boolean addInputHandler(InputHandler inputHandler, int i) {
        if (!this.inputHandlers.containsKey(Integer.valueOf(i))) {
            this.inputHandlers.put(Integer.valueOf(i), new LinkedHashSet());
        }
        return this.inputHandlers.get(Integer.valueOf(i)).add(inputHandler);
    }

    public boolean removeInputHandler(InputHandler inputHandler) {
        for (LinkedHashSet<InputHandler> remove : this.inputHandlers.values()) {
            if (remove.remove(inputHandler)) {
                return true;
            }
        }
        return false;
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        for (LinkedHashSet<InputHandler> it : this.inputHandlers.values()) {
            Iterator it2 = it.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (((InputHandler) it2.next()).onGenericMotionEvent(motionEvent)) {
                        return true;
                    }
                }
            }
        }
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 164) {
            switch (i) {
                case 24:
                case 25:
                    break;
                default:
                    for (LinkedHashSet<InputHandler> it : this.inputHandlers.values()) {
                        Iterator it2 = it.iterator();
                        while (true) {
                            if (it2.hasNext()) {
                                if (((InputHandler) it2.next()).onKeyDown(i, keyEvent)) {
                                    return true;
                                }
                            }
                        }
                    }
                    return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 164) {
            switch (i) {
                case 24:
                case 25:
                    break;
                default:
                    for (LinkedHashSet<InputHandler> it : this.inputHandlers.values()) {
                        Iterator it2 = it.iterator();
                        while (true) {
                            if (it2.hasNext()) {
                                if (((InputHandler) it2.next()).onKeyUp(i, keyEvent)) {
                                    return true;
                                }
                            }
                        }
                    }
                    return true;
            }
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void queueEvent(ThreadingContext threadingContext, Runnable runnable) {
        switch (threadingContext) {
            case AndroidUi:
            case UiThread:
                runOnUiThread(runnable);
                return;
            case Main:
            case GlThread:
                MCViewManager.runOnGlThread(runnable);
                return;
            default:
                return;
        }
    }

    public RelativeLayout getMainLayout() {
        return MCViewManager.getMainLayout();
    }
}
