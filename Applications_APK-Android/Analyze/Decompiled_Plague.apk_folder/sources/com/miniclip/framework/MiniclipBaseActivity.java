package com.miniclip.framework;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;

public abstract class MiniclipBaseActivity extends Activity implements MiniclipFacilitator {
    private LinkedHashSet<ActivityListener> activityListeners = new LinkedHashSet<>();

    public Activity getActivity() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Miniclip.setFacilitator(this);
    }

    public boolean addListener(ActivityListener activityListener) {
        return this.activityListeners.add(activityListener);
    }

    public boolean removeListener(ActivityListener activityListener) {
        return this.activityListeners.remove(activityListener);
    }

    /* renamed from: com.miniclip.framework.MiniclipBaseActivity$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$miniclip$framework$ThreadingContext = new int[ThreadingContext.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.miniclip.framework.ThreadingContext[] r0 = com.miniclip.framework.ThreadingContext.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.miniclip.framework.MiniclipBaseActivity.AnonymousClass1.$SwitchMap$com$miniclip$framework$ThreadingContext = r0
                int[] r0 = com.miniclip.framework.MiniclipBaseActivity.AnonymousClass1.$SwitchMap$com$miniclip$framework$ThreadingContext     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.miniclip.framework.ThreadingContext r1 = com.miniclip.framework.ThreadingContext.AndroidUi     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.miniclip.framework.MiniclipBaseActivity.AnonymousClass1.$SwitchMap$com$miniclip$framework$ThreadingContext     // Catch:{ NoSuchFieldError -> 0x001f }
                com.miniclip.framework.ThreadingContext r1 = com.miniclip.framework.ThreadingContext.Main     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.miniclip.framework.MiniclipBaseActivity.AnonymousClass1.<clinit>():void");
        }
    }

    public void queueEvent(ThreadingContext threadingContext, Runnable runnable) {
        if (AnonymousClass1.$SwitchMap$com$miniclip$framework$ThreadingContext[threadingContext.ordinal()] != 1) {
            runOnUiThread(runnable);
        } else {
            runOnUiThread(runnable);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Iterator it = new ArrayList(this.activityListeners).iterator();
        while (it.hasNext()) {
            ((ActivityListener) it.next()).onDestroy();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Iterator it = new ArrayList(this.activityListeners).iterator();
        while (it.hasNext()) {
            ((ActivityListener) it.next()).onResume();
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Iterator it = new ArrayList(this.activityListeners).iterator();
        while (it.hasNext()) {
            ((ActivityListener) it.next()).onPause();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        Iterator it = new ArrayList(this.activityListeners).iterator();
        while (it.hasNext()) {
            ((ActivityListener) it.next()).onStart();
        }
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Iterator it = new ArrayList(this.activityListeners).iterator();
        while (it.hasNext()) {
            ((ActivityListener) it.next()).onStop();
        }
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        Iterator it = new ArrayList(this.activityListeners).iterator();
        while (it.hasNext()) {
            ((ActivityListener) it.next()).onRestart();
        }
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Iterator it = new ArrayList(this.activityListeners).iterator();
        while (it.hasNext()) {
            ((ActivityListener) it.next()).onActivityResult(i, i2, intent);
        }
        super.onActivityResult(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        Iterator it = new ArrayList(this.activityListeners).iterator();
        while (it.hasNext()) {
            ((ActivityListener) it.next()).onNewIntent(intent);
        }
        super.onNewIntent(intent);
    }

    public void onWindowFocusChanged(boolean z) {
        Iterator it = new ArrayList(this.activityListeners).iterator();
        while (it.hasNext()) {
            ((ActivityListener) it.next()).onWindowFocusChanged(z);
        }
        super.onWindowFocusChanged(z);
    }

    public void onLowMemory() {
        Iterator<ActivityListener> it = this.activityListeners.iterator();
        while (it.hasNext()) {
            it.next().onLowMemory();
        }
        super.onLowMemory();
    }

    public void onSaveInstanceState(Bundle bundle) {
        Iterator<ActivityListener> it = this.activityListeners.iterator();
        while (it.hasNext()) {
            it.next().onSaveInstanceState(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Iterator<ActivityListener> it = this.activityListeners.iterator();
        while (it.hasNext()) {
            it.next().onRequestPermissionsResult(i, strArr, iArr);
        }
        super.onRequestPermissionsResult(i, strArr, iArr);
    }
}
