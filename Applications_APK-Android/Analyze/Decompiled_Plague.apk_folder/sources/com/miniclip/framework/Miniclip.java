package com.miniclip.framework;

import android.app.Activity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

public final class Miniclip {
    private static final LinkedHashSet<ActivityListener> activityListeners = new LinkedHashSet<>();
    private static MiniclipFacilitator facilitator;
    private static final Map<ThreadingContext, List<Runnable>> queuedEvents = new HashMap();

    public static void setFacilitator(MiniclipFacilitator miniclipFacilitator) {
        facilitator = miniclipFacilitator;
        if (miniclipFacilitator != null) {
            Iterator<ActivityListener> it = activityListeners.iterator();
            while (it.hasNext()) {
                miniclipFacilitator.addListener(it.next());
            }
            while (!queuedEvents.isEmpty()) {
                ThreadingContext next = queuedEvents.keySet().iterator().next();
                for (Runnable queueEvent : queuedEvents.remove(next)) {
                    miniclipFacilitator.queueEvent(next, queueEvent);
                }
            }
        }
    }

    public static Activity getActivity() {
        if (facilitator != null) {
            return facilitator.getActivity();
        }
        return null;
    }

    public static boolean addListener(ActivityListener activityListener) {
        return facilitator != null ? facilitator.addListener(activityListener) : activityListeners.add(activityListener);
    }

    public static boolean removeListener(ActivityListener activityListener) {
        return facilitator != null ? facilitator.removeListener(activityListener) : activityListeners.remove(activityListener);
    }

    public static void queueEvent(ThreadingContext threadingContext, Runnable runnable) {
        if (facilitator != null) {
            facilitator.queueEvent(threadingContext, runnable);
            return;
        }
        List list = queuedEvents.get(threadingContext);
        if (list == null) {
            list = new ArrayList();
            queuedEvents.put(threadingContext, list);
        }
        list.add(runnable);
    }
}
