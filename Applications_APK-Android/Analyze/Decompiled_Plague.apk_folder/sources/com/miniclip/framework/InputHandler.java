package com.miniclip.framework;

import android.view.KeyEvent;
import android.view.MotionEvent;

public interface InputHandler {
    boolean onGenericMotionEvent(MotionEvent motionEvent);

    boolean onKeyDown(int i, KeyEvent keyEvent);

    boolean onKeyUp(int i, KeyEvent keyEvent);
}
