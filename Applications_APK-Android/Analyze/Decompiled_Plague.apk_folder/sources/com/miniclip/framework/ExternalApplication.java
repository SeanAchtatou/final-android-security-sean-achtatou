package com.miniclip.framework;

import android.content.Intent;
import android.net.Uri;

public class ExternalApplication {
    public static void launch(final String str) {
        Miniclip.queueEvent(ThreadingContext.AndroidUi, new Runnable() {
            public void run() {
                Miniclip.getActivity().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            }
        });
    }
}
