package com.miniclip.gameservices;

import android.app.Activity;

public class GameServices {
    public static boolean DEBUG = false;
    private static String GAMESERVICES_TAG = "GameServices";
    private static Activity mActivity;
    /* access modifiers changed from: private */
    public static GameServicesListener mListener;

    public interface GameServicesListener {
        String gameServices_getCurrentPlayerName();

        String gameServices_getDeviceId();

        int gameServices_isSignedIn();

        void gameServices_loadAchievements(int i, int i2);

        void gameServices_loadLeaderboardScores(int i, int i2, String str, int i3, int i4, int i5);

        void gameServices_login();

        void gameServices_logout();

        void gameServices_showAchievements();

        void gameServices_showLeaderboard(String str);

        void gameServices_showLeaderboards();

        void gameServices_updateAchievement(String str, float f, Object obj);

        void gameServices_updateScore(String str, long j, Object obj);
    }

    public static native void MonGameServicesAchievementsLoaded(String[] strArr, int i, int i2, int i3);

    public static native void MonGameServicesLeaderboardScoredLoaded(String[] strArr, int i, int i2, int i3);

    public static native void MonGameServicesLoginComplete();

    public static void setup(Activity activity, GameServicesListener gameServicesListener) {
        mActivity = activity;
        mListener = gameServicesListener;
    }

    public static void login() {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (GameServices.mListener != null) {
                    GameServices.mListener.gameServices_login();
                }
            }
        });
    }

    public static void logout() {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (GameServices.mListener != null) {
                    GameServices.mListener.gameServices_logout();
                }
            }
        });
    }

    public static int isSignedIn() {
        if (mListener != null) {
            return mListener.gameServices_isSignedIn();
        }
        return 0;
    }

    public static String getCurrentPlayerName() {
        return mListener != null ? mListener.gameServices_getCurrentPlayerName() : "";
    }

    public static String getDeviceId() {
        return mListener != null ? mListener.gameServices_getDeviceId() : "";
    }

    public static void updateScore(final String str, final int i, final Object obj) {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (GameServices.mListener != null) {
                    GameServices.mListener.gameServices_updateScore(str, (long) i, obj);
                }
            }
        });
    }

    public static void updateScore(String str, int i) {
        updateScore(str, i, null);
    }

    public static void showLeaderboard(final String str) {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (GameServices.mListener != null) {
                    GameServices.mListener.gameServices_showLeaderboard(str);
                }
            }
        });
    }

    public static void showLeaderboards() {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (GameServices.mListener != null) {
                    GameServices.mListener.gameServices_showLeaderboards();
                }
            }
        });
    }

    public static void loadLeaderboardScores(int i, int i2, String str, int i3, int i4, int i5) {
        final int i6 = i;
        final int i7 = i2;
        final String str2 = str;
        final int i8 = i3;
        final int i9 = i4;
        final int i10 = i5;
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (GameServices.mListener != null) {
                    GameServices.mListener.gameServices_loadLeaderboardScores(i6, i7, str2, i8, i9, i10);
                }
            }
        });
    }

    public static void loadAchievements(final int i, final int i2) {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (GameServices.mListener != null) {
                    GameServices.mListener.gameServices_loadAchievements(i, i2);
                }
            }
        });
    }

    public static void updateAchievement(final String str, final float f, final Object obj) {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (GameServices.mListener != null) {
                    GameServices.mListener.gameServices_updateAchievement(str, f, obj);
                }
            }
        });
    }

    public static void updateAchievement(String str, float f) {
        updateAchievement(str, f, null);
    }

    public static void showAchievements() {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (GameServices.mListener != null) {
                    GameServices.mListener.gameServices_showAchievements();
                }
            }
        });
    }
}
