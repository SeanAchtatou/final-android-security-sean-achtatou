package com.miniclip.mcservices;

public final class R {

    public static final class attr {
        public static final int adSize = 2130837504;
        public static final int adSizes = 2130837505;
        public static final int adUnitId = 2130837506;
        public static final int buttonSize = 2130837507;
        public static final int circleCrop = 2130837508;
        public static final int colorScheme = 2130837509;
        public static final int imageAspectRatio = 2130837524;
        public static final int imageAspectRatioAdjust = 2130837525;
        public static final int scopeUris = 2130837533;
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2130968581;
        public static final int common_google_signin_btn_text_dark_default = 2130968582;
        public static final int common_google_signin_btn_text_dark_disabled = 2130968583;
        public static final int common_google_signin_btn_text_dark_focused = 2130968584;
        public static final int common_google_signin_btn_text_dark_pressed = 2130968585;
        public static final int common_google_signin_btn_text_light = 2130968586;
        public static final int common_google_signin_btn_text_light_default = 2130968587;
        public static final int common_google_signin_btn_text_light_disabled = 2130968588;
        public static final int common_google_signin_btn_text_light_focused = 2130968589;
        public static final int common_google_signin_btn_text_light_pressed = 2130968590;
        public static final int common_google_signin_btn_tint = 2130968591;
    }

    public static final class dimen {
        public static final int nf_action_button_front_text_padding_bottom = 2131034150;
        public static final int nf_action_button_margin_sides = 2131034151;
        public static final int nf_action_button_margin_sides_portrait = 2131034152;
        public static final int nf_action_button_middle_section_width = 2131034153;
        public static final int nf_action_button_shadow_text_padding_bottom = 2131034154;
        public static final int nf_action_button_side_section_width = 2131034155;
        public static final int nf_action_button_text_size = 2131034156;
        public static final int nf_bg_border_bottom_component_height = 2131034157;
        public static final int nf_bg_border_component_size = 2131034158;
        public static final int nf_bg_border_margin_bottom = 2131034159;
        public static final int nf_bg_border_margin_left = 2131034160;
        public static final int nf_bg_border_margin_right = 2131034161;
        public static final int nf_bg_border_margin_top = 2131034162;
        public static final int nf_bg_component_size = 2131034163;
        public static final int nf_bg_margin_bottom = 2131034164;
        public static final int nf_bg_margin_left = 2131034165;
        public static final int nf_bg_margin_right = 2131034166;
        public static final int nf_bg_margin_top = 2131034167;
        public static final int nf_bg_side_component_size = 2131034168;
        public static final int nf_bottom_buttons_height = 2131034169;
        public static final int nf_bottom_buttons_margin_bottom = 2131034170;
        public static final int nf_bottom_side_buttons_width = 2131034171;
        public static final int nf_close_button_margin_right = 2131034172;
        public static final int nf_close_button_margin_top = 2131034173;
        public static final int nf_close_button_size = 2131034174;
        public static final int nf_content_margin = 2131034175;
        public static final int nf_height_adjustment = 2131034176;
        public static final int nf_news_image_height = 2131034177;
        public static final int nf_news_image_width = 2131034178;
    }

    public static final class drawable {
        public static final int close_webpage_big = 2131099652;
        public static final int close_webpage_small = 2131099653;
        public static final int common_full_open_on_phone = 2131099685;
        public static final int common_google_signin_btn_icon_dark = 2131099686;
        public static final int common_google_signin_btn_icon_dark_focused = 2131099687;
        public static final int common_google_signin_btn_icon_dark_normal = 2131099688;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131099689;
        public static final int common_google_signin_btn_icon_disabled = 2131099690;
        public static final int common_google_signin_btn_icon_light = 2131099691;
        public static final int common_google_signin_btn_icon_light_focused = 2131099692;
        public static final int common_google_signin_btn_icon_light_normal = 2131099693;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131099694;
        public static final int common_google_signin_btn_text_dark = 2131099695;
        public static final int common_google_signin_btn_text_dark_focused = 2131099696;
        public static final int common_google_signin_btn_text_dark_normal = 2131099697;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131099698;
        public static final int common_google_signin_btn_text_disabled = 2131099699;
        public static final int common_google_signin_btn_text_light = 2131099700;
        public static final int common_google_signin_btn_text_light_focused = 2131099701;
        public static final int common_google_signin_btn_text_light_normal = 2131099702;
        public static final int common_google_signin_btn_text_light_normal_background = 2131099703;
        public static final int googleg_disabled_color_18 = 2131099709;
        public static final int googleg_standard_color_18 = 2131099710;
        public static final int nf_action_l = 2131099740;
        public static final int nf_action_m = 2131099741;
        public static final int nf_action_r = 2131099742;
        public static final int nf_b = 2131099743;
        public static final int nf_bg_bl = 2131099744;
        public static final int nf_bg_bm = 2131099745;
        public static final int nf_bg_bm_ = 2131099746;
        public static final int nf_bg_br = 2131099747;
        public static final int nf_bg_ml = 2131099748;
        public static final int nf_bg_mm = 2131099749;
        public static final int nf_bg_mr = 2131099750;
        public static final int nf_bg_tl = 2131099751;
        public static final int nf_bg_tm = 2131099752;
        public static final int nf_bg_tr = 2131099753;
        public static final int nf_close = 2131099754;
        public static final int nf_in_bl = 2131099755;
        public static final int nf_in_bm = 2131099756;
        public static final int nf_in_br = 2131099757;
        public static final int nf_in_ml = 2131099758;
        public static final int nf_in_mr = 2131099759;
        public static final int nf_in_tl = 2131099760;
        public static final int nf_in_tm = 2131099761;
        public static final int nf_in_tr = 2131099762;
        public static final int nf_news = 2131099763;
        public static final int nf_next = 2131099764;
        public static final int nf_prev = 2131099765;
    }

    public static final class id {
        public static final int adjust_height = 2131165193;
        public static final int adjust_width = 2131165194;
        public static final int auto = 2131165196;
        public static final int close_webpage_clickable = 2131165200;
        public static final int dark = 2131165221;
        public static final int icon_only = 2131165226;
        public static final int light = 2131165230;
        public static final int newsfeed_action_button_text = 2131165243;
        public static final int newsfeed_action_button_text_shadow = 2131165244;
        public static final int newsfeed_bg_bl = 2131165245;
        public static final int newsfeed_bg_bm = 2131165246;
        public static final int newsfeed_bg_br = 2131165247;
        public static final int newsfeed_bg_layout = 2131165248;
        public static final int newsfeed_bg_ml = 2131165249;
        public static final int newsfeed_bg_mm = 2131165250;
        public static final int newsfeed_bg_mr = 2131165251;
        public static final int newsfeed_bg_tl = 2131165252;
        public static final int newsfeed_bg_tm = 2131165253;
        public static final int newsfeed_bg_tr = 2131165254;
        public static final int newsfeed_blocker_button = 2131165255;
        public static final int newsfeed_border_bot = 2131165256;
        public static final int newsfeed_border_botm = 2131165257;
        public static final int newsfeed_border_botr = 2131165258;
        public static final int newsfeed_border_ml = 2131165259;
        public static final int newsfeed_border_mr = 2131165260;
        public static final int newsfeed_border_top = 2131165261;
        public static final int newsfeed_border_topm = 2131165262;
        public static final int newsfeed_border_topr = 2131165263;
        public static final int newsfeed_button_close = 2131165264;
        public static final int newsfeed_button_get_it_now = 2131165265;
        public static final int newsfeed_button_get_it_now_img_l = 2131165266;
        public static final int newsfeed_button_get_it_now_img_m = 2131165267;
        public static final int newsfeed_button_get_it_now_img_r = 2131165268;
        public static final int newsfeed_button_next = 2131165269;
        public static final int newsfeed_button_prev = 2131165270;
        public static final int newsfeed_buttons_layout = 2131165271;
        public static final int newsfeed_imageview_content = 2131165272;
        public static final int newsfeed_news_img = 2131165273;
        public static final int none = 2131165274;
        public static final int normal = 2131165275;
        public static final int popup = 2131165280;
        public static final int progressBar1 = 2131165282;
        public static final int standard = 2131165289;
        public static final int text = 2131165291;
        public static final int text2 = 2131165292;
        public static final int webview_with_webpage = 2131165296;
        public static final int wide = 2131165297;
    }

    public static final class integer {
        public static final int google_play_services_version = 2131230721;
    }

    public static final class layout {
        public static final int newsfeed0 = 2131296274;
        public static final int nf_back = 2131296275;
        public static final int progress_view = 2131296291;
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131427365;
        public static final int common_google_play_services_enable_text = 2131427366;
        public static final int common_google_play_services_enable_title = 2131427367;
        public static final int common_google_play_services_install_button = 2131427368;
        public static final int common_google_play_services_install_text = 2131427369;
        public static final int common_google_play_services_install_title = 2131427370;
        public static final int common_google_play_services_notification_ticker = 2131427371;
        public static final int common_google_play_services_unknown_issue = 2131427372;
        public static final int common_google_play_services_unsupported_text = 2131427373;
        public static final int common_google_play_services_update_button = 2131427374;
        public static final int common_google_play_services_update_text = 2131427375;
        public static final int common_google_play_services_update_title = 2131427376;
        public static final int common_google_play_services_updating_text = 2131427377;
        public static final int common_google_play_services_wear_update_text = 2131427378;
        public static final int common_open_on_phone = 2131427379;
        public static final int common_signin_button_text = 2131427380;
        public static final int common_signin_button_text_long = 2131427381;
        public static final int s1 = 2131427415;
        public static final int s2 = 2131427416;
        public static final int s3 = 2131427417;
        public static final int s4 = 2131427418;
        public static final int s5 = 2131427419;
        public static final int s6 = 2131427420;
        public static final int s7 = 2131427421;
    }

    public static final class style {
        public static final int Theme_IAPTheme = 2131492877;
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.miniclip.plagueinc.R.attr.adSize, com.miniclip.plagueinc.R.attr.adSizes, com.miniclip.plagueinc.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] LoadingImageView = {com.miniclip.plagueinc.R.attr.circleCrop, com.miniclip.plagueinc.R.attr.imageAspectRatio, com.miniclip.plagueinc.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.miniclip.plagueinc.R.attr.buttonSize, com.miniclip.plagueinc.R.attr.colorScheme, com.miniclip.plagueinc.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
