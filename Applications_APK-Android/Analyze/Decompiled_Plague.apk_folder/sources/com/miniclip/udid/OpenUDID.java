package com.miniclip.udid;

import android.content.Context;
import android.util.Log;

public class OpenUDID {
    public static boolean DEBUG = false;
    private static String OPENUDID_TAG = "OpenUDID";
    private static Context mContext;

    public static void initialize(Context context) {
        mContext = context;
        OpenUDID_manager.sync(context);
    }

    public static String getOpenUDID() {
        debugLog("getOpenUDID");
        if (OpenUDID_manager.isInitialized()) {
            return OpenUDID_manager.getOpenUDID();
        }
        errorLog("Not initialized");
        return null;
    }

    static void debugLog(String str) {
        if (DEBUG) {
            Log.d(OPENUDID_TAG, str);
        }
    }

    static void errorLog(String str) {
        Log.e(OPENUDID_TAG, str);
    }
}
