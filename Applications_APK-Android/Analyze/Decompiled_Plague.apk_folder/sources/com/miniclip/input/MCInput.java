package com.miniclip.input;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v4.view.InputDeviceCompat;
import android.util.Log;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.miniclip.framework.InputHandler;
import com.miniclip.framework.MiniclipAndroidActivity;
import com.miniclip.framework.ThreadingContext;
import java.util.ArrayList;
import java.util.Iterator;

public class MCInput implements InputHandler {
    public static final int AXIS_BRAKE = 23;
    public static final int AXIS_DISTANCE = 24;
    public static final int AXIS_GAS = 22;
    public static final int AXIS_HAT_X = 15;
    public static final int AXIS_HAT_Y = 16;
    public static final int AXIS_HSCROLL = 10;
    public static final int AXIS_LTRIGGER = 17;
    public static final int AXIS_ORIENTATION = 8;
    public static final int AXIS_PRESSURE = 2;
    public static final int AXIS_RTRIGGER = 18;
    public static final int AXIS_RUDDER = 20;
    public static final int AXIS_RX = 12;
    public static final int AXIS_RY = 13;
    public static final int AXIS_RZ = 14;
    public static final int AXIS_SIZE = 3;
    public static final int AXIS_THROTTLE = 19;
    public static final int AXIS_TILT = 25;
    public static final int AXIS_TOOL_MAJOR = 6;
    public static final int AXIS_TOOL_MINOR = 7;
    public static final int AXIS_TOUCH_MAJOR = 4;
    public static final int AXIS_TOUCH_MINOR = 5;
    public static final int AXIS_VSCROLL = 9;
    public static final int AXIS_WHEEL = 21;
    public static final int AXIS_X = 0;
    public static final int AXIS_Y = 1;
    public static final int AXIS_Z = 11;
    public static final int KEYCODE_BUTTON_A = 96;
    public static final int KEYCODE_BUTTON_B = 97;
    public static final int KEYCODE_BUTTON_C = 98;
    public static final int KEYCODE_BUTTON_L1 = 102;
    public static final int KEYCODE_BUTTON_L2 = 104;
    public static final int KEYCODE_BUTTON_MODE = 110;
    public static final int KEYCODE_BUTTON_R1 = 103;
    public static final int KEYCODE_BUTTON_R2 = 105;
    public static final int KEYCODE_BUTTON_SELECT = 109;
    public static final int KEYCODE_BUTTON_START = 108;
    public static final int KEYCODE_BUTTON_THUMBL = 106;
    public static final int KEYCODE_BUTTON_THUMBR = 107;
    public static final int KEYCODE_BUTTON_X = 99;
    public static final int KEYCODE_BUTTON_Y = 100;
    public static final int KEYCODE_BUTTON_Z = 101;
    public static final int KEYCODE_DPAD_CENTER = 23;
    public static final int KEYCODE_DPAD_DOWN = 20;
    public static final int KEYCODE_DPAD_LEFT = 21;
    public static final int KEYCODE_DPAD_RIGHT = 22;
    public static final int KEYCODE_DPAD_UP = 19;
    public static final int KEYCODE_MENU = 82;
    public static final int MAX_AXIS = 64;
    public static final int MAX_KEYCODES = 256;
    public static final int[] SUPPORTED_AXIS = {15, 16, 0, 1, 11, 14, 17, 23, 18, 19};
    private static final String TAG = "MCInput";
    private static MiniclipAndroidActivity activity;
    private static MCInput instance;
    float[] axisStates = new float[64];
    boolean[] keyStates = new boolean[256];

    /* access modifiers changed from: private */
    public native void nativeKeyEvent(int i, int i2, boolean z);

    /* access modifiers changed from: private */
    public native void nativeMotionEvent(int i, int i2, float f);

    public static native void nativeTouchesBegin(int i, float f, float f2);

    public static native void nativeTouchesCancel(int[] iArr, float[] fArr, float[] fArr2);

    public static native void nativeTouchesEnd(int i, float f, float f2);

    public static native void nativeTouchesMove(int[] iArr, float[] fArr, float[] fArr2);

    private MCInput() {
    }

    public static void init(MiniclipAndroidActivity miniclipAndroidActivity) {
        if (instance == null) {
            instance = new MCInput();
            activity = miniclipAndroidActivity;
            miniclipAndroidActivity.addInputHandler(instance, 0);
        }
        instance.debugDeviceType();
    }

    public static MCInput getInstance() {
        return instance;
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        if ((motionEvent.getSource() & 16) != 16) {
            return false;
        }
        int historySize = motionEvent.getHistorySize();
        for (int i = 0; i < historySize; i++) {
            processMotionEvent(motionEvent, i);
        }
        processMotionEvent(motionEvent, -1);
        return true;
    }

    /* access modifiers changed from: package-private */
    public float getAxisState(int i, int i2) {
        if (i2 < 64) {
            return this.axisStates[i2];
        }
        return 0.0f;
    }

    /* access modifiers changed from: package-private */
    public boolean getKeyState(int i, int i2) {
        if (i2 < 256) {
            return this.keyStates[i2];
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void setAxisState(int i, int i2, float f) {
        if (i2 < 64) {
            this.axisStates[i2] = f;
        }
    }

    /* access modifiers changed from: package-private */
    public void setKeyState(int i, int i2, boolean z) {
        if (i2 < 256) {
            this.keyStates[i2] = z;
        }
    }

    private void processMotionEvent(MotionEvent motionEvent, int i) {
        InputDevice device = motionEvent.getDevice();
        for (final int i2 : SUPPORTED_AXIS) {
            final int deviceId = motionEvent.getDeviceId();
            final float centeredAxis = getCenteredAxis(motionEvent, device, i2, i);
            if (centeredAxis != getAxisState(deviceId, i2)) {
                Log.d(TAG, "onMotion: " + deviceId + " " + i2 + " " + centeredAxis);
                setAxisState(deviceId, i2, centeredAxis);
                activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
                    public void run() {
                        MCInput.this.nativeMotionEvent(deviceId, i2, centeredAxis);
                    }
                });
            }
        }
    }

    @TargetApi(12)
    private static float getCenteredAxis(MotionEvent motionEvent, InputDevice inputDevice, int i, int i2) {
        if (Build.VERSION.SDK_INT < 12) {
            return 0.0f;
        }
        float axisValue = i2 < 0 ? motionEvent.getAxisValue(i) : motionEvent.getHistoricalAxisValue(i, i2);
        InputDevice.MotionRange motionRange = inputDevice != null ? inputDevice.getMotionRange(i, motionEvent.getSource()) : null;
        if (motionRange == null || Math.abs(axisValue) > motionRange.getFlat()) {
            return axisValue;
        }
        return 0.0f;
    }

    public boolean onKeyDown(final int i, KeyEvent keyEvent) {
        if ((keyEvent.getSource() & 1) != 1) {
            return false;
        }
        final int deviceId = keyEvent.getDeviceId();
        if (getKeyState(deviceId, i)) {
            return false;
        }
        String str = TAG;
        Log.d(str, "onKeyDown: " + deviceId + " " + i + " " + true);
        setKeyState(deviceId, i, true);
        activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
            public void run() {
                MCInput.this.nativeKeyEvent(deviceId, i, true);
            }
        });
        return true;
    }

    public boolean onKeyUp(final int i, KeyEvent keyEvent) {
        if ((keyEvent.getSource() & 1) == 1) {
            final int deviceId = keyEvent.getDeviceId();
            if (getKeyState(deviceId, i)) {
                String str = TAG;
                Log.d(str, "onKeyUp: " + deviceId + " " + i + " " + false);
                setKeyState(deviceId, i, false);
                activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
                    public void run() {
                        MCInput.this.nativeKeyEvent(deviceId, i, false);
                    }
                });
                return true;
            }
        }
        return false;
    }

    public static int[] getGameControllerIds() {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        for (int i2 : InputDevice.getDeviceIds()) {
            int sources = InputDevice.getDevice(i2).getSources();
            if (((sources & InputDeviceCompat.SOURCE_GAMEPAD) == 1025 || (sources & InputDeviceCompat.SOURCE_JOYSTICK) == 16777232) && !arrayList.contains(Integer.valueOf(i2))) {
                arrayList.add(Integer.valueOf(i2));
            }
        }
        int[] iArr = new int[arrayList.size()];
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            iArr[i] = ((Integer) it.next()).intValue();
            i++;
        }
        return iArr;
    }

    public static void setAccelerometerEnabled(boolean z) {
        MCAccelerometer.setEnabled(z);
    }

    public static boolean isTouchScreenAvailable() {
        return activity.getPackageManager().hasSystemFeature("android.hardware.touchscreen");
    }

    public static boolean isTelephonyAvailable() {
        return activity.getPackageManager().hasSystemFeature("android.hardware.telephony");
    }

    public void debugDeviceType() {
        if (isTelephonyAvailable()) {
            Log.d(TAG, "Running on phone");
        } else if (isTouchScreenAvailable()) {
            Log.d(TAG, "Running on devices that don't support telephony but do have a touch screen.");
        } else {
            Log.d(TAG, "Running on a TV!");
        }
    }
}
