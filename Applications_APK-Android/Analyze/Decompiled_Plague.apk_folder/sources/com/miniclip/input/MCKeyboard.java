package com.miniclip.input;

import android.os.Build;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.games.GamesStatusCodes;
import com.miniclip.framework.AbstractActivityListener;
import com.miniclip.framework.MiniclipAndroidActivity;
import com.miniclip.framework.ThreadingContext;

public class MCKeyboard extends AbstractActivityListener {
    private static final String TAG = "MCKeyboard";
    /* access modifiers changed from: private */
    public static MiniclipAndroidActivity activity = null;
    private static MCKeyboard instance = null;
    /* access modifiers changed from: private */
    public static EditText mEditText = null;
    public static boolean mKEYBOARD_CUSTOM_BACKGROUND = false;
    public static boolean mKEYBOARD_FULLSCREEN = true;
    public static boolean mKEYBOARD_INPUT_HIDE = true;
    public static boolean mKEYBOARD_INPUT_SINGLE_LINE = false;
    public static boolean mKEYBOARD_OVERRIDE_VISIBILITY = false;
    private static Handler mKeyboardHandler = new Handler();
    public static boolean mSHOW_KEYBOARD_INPUT = false;

    /* access modifiers changed from: private */
    public static native void MkeyboardInputCanceled(String str);

    /* access modifiers changed from: private */
    public static native void MkeyboardInputClosed(String str);

    /* access modifiers changed from: private */
    public static native void MkeyboardInputResponse(String str);

    private MCKeyboard() {
    }

    public static void init(MiniclipAndroidActivity miniclipAndroidActivity) {
        if (instance == null) {
            instance = new MCKeyboard();
            activity = miniclipAndroidActivity;
        }
        if (mEditText == null) {
            mEditText = new EditText(activity) {
                public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
                    if (i != 4) {
                        return true;
                    }
                    ((InputMethodManager) MCKeyboard.activity.getSystemService("input_method")).hideSoftInputFromWindow(getWindowToken(), 0);
                    if (!MCKeyboard.mKEYBOARD_OVERRIDE_VISIBILITY) {
                        MCKeyboard.mEditText.setVisibility(4);
                    }
                    MCKeyboard.activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
                        public void run() {
                            MCKeyboard.MkeyboardInputCanceled(MCKeyboard.mEditText.getText().toString());
                        }
                    });
                    return true;
                }
            };
            if (mKEYBOARD_INPUT_HIDE) {
                mEditText.setLayoutParams(new RelativeLayout.LayoutParams(1, 1));
            }
            mEditText.setImeOptions(6);
            mEditText.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable editable) {
                }

                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    MCKeyboard.activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
                        public void run() {
                            MCKeyboard.MkeyboardInputResponse(MCKeyboard.mEditText.getText().toString());
                        }
                    });
                }
            });
            mEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    if (i == 6 || i == 2 || ((keyEvent != null && keyEvent.getKeyCode() == 66) || i == 5)) {
                        MCKeyboard.activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
                            public void run() {
                                if (!MCKeyboard.mKEYBOARD_OVERRIDE_VISIBILITY) {
                                    MCKeyboard.mEditText.setVisibility(4);
                                }
                                ((InputMethodManager) MCKeyboard.activity.getSystemService("input_method")).hideSoftInputFromWindow(MCKeyboard.mEditText.getWindowToken(), 0);
                            }
                        });
                    }
                    MCKeyboard.activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
                        public void run() {
                            MCKeyboard.MkeyboardInputClosed(MCKeyboard.mEditText.getText().toString());
                        }
                    });
                    return false;
                }
            });
            mEditText.setWidth(GamesStatusCodes.STATUS_REQUEST_UPDATE_PARTIAL_SUCCESS);
            if (!mKEYBOARD_OVERRIDE_VISIBILITY) {
                mEditText.setVisibility(4);
            }
            if (mSHOW_KEYBOARD_INPUT) {
                miniclipAndroidActivity.getMainLayout().addView(mEditText);
            } else {
                if (mKEYBOARD_INPUT_SINGLE_LINE) {
                    mEditText.setMaxLines(1);
                }
                RelativeLayout relativeLayout = new RelativeLayout(miniclipAndroidActivity);
                relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
                relativeLayout.addView(mEditText);
                int i = Build.VERSION.SDK_INT;
                miniclipAndroidActivity.getMainLayout().addView(relativeLayout);
            }
            if (!mKEYBOARD_FULLSCREEN) {
                mEditText.setImeOptions(268435462);
            }
            mEditText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                }
            });
            if (mKEYBOARD_CUSTOM_BACKGROUND) {
                mEditText.setBackgroundDrawable(miniclipAndroidActivity.getResources().getDrawable(miniclipAndroidActivity.getResources().getIdentifier("roundsquare", "drawable", miniclipAndroidActivity.getPackageName())));
            }
        }
        miniclipAndroidActivity.addListener(instance);
    }

    public static MCKeyboard getInstance() {
        return instance;
    }

    public static void keyboardInput_Show(final String str) {
        activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                MCKeyboard.mEditText.setText(str);
                MCKeyboard.mEditText.setVisibility(0);
                ((InputMethodManager) MCKeyboard.activity.getSystemService("input_method")).toggleSoftInput(2, 2);
                MCKeyboard.mEditText.setSelection(MCKeyboard.mEditText.getText().length());
            }
        });
    }

    public static void setCaretPosition(final int i) {
        activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                MCKeyboard.mEditText.setSelection(i);
                Log.i("KeyboardInput", String.format("setCaretPosition position: %d", Integer.valueOf(i)));
            }
        });
    }

    public static int getCaretPosition() {
        int selectionStart = mEditText.getSelectionStart();
        Log.i("KeyboardInput", String.format("getCaretPosition position: %d", Integer.valueOf(selectionStart)));
        return selectionStart;
    }

    public static void keyboardInput_Show_withMaxLengthLocked(final String str, final int i) {
        activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                MCKeyboard.mEditText.setText(str);
                MCKeyboard.mEditText.setVisibility(0);
                MCKeyboard.mEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(i)});
                ((InputMethodManager) MCKeyboard.activity.getSystemService("input_method")).toggleSoftInput(2, 2);
                MCKeyboard.mEditText.setSelection(MCKeyboard.mEditText.getText().length());
            }
        });
    }

    public static void setKeyboardInputPosition(float f, float f2, float f3, float f4, float f5, float f6) {
        final float f7 = f4;
        final float f8 = f3;
        final float f9 = f;
        final float f10 = f5;
        final float f11 = f2;
        final float f12 = f6;
        activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                float f = f7;
                if (f7 == 0.0f) {
                    f = (float) MCKeyboard.mEditText.getHeight();
                }
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) f8, (int) f);
                layoutParams.leftMargin = (int) (f9 - (f8 * f10));
                layoutParams.topMargin = (int) (f11 - (f12 * f));
                Log.i("setKeyboardInputPos", String.format("newHeight: %f, anchorY: %f", Float.valueOf(f), Float.valueOf(f12)));
            }
        });
    }

    public static void setKeyboardInputStyle(final int i, final int i2, final float f) {
        activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                MCKeyboard.mEditText.setBackgroundColor(i);
                MCKeyboard.mEditText.setTextColor(i2);
                MCKeyboard.mEditText.setTextSize(f);
            }
        });
    }

    public static void setKeyboardInputVisible(final int i) {
        Log.i("setKeyboardInputVisible", String.format("visible: %d", Integer.valueOf(i)));
        activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                if (i == 0) {
                    MCKeyboard.mEditText.setVisibility(4);
                } else {
                    MCKeyboard.mEditText.setVisibility(0);
                }
            }
        });
    }

    public static void setKeyboardInputVisibleDelayed(final int i, int i2) {
        mKeyboardHandler.postDelayed(new Runnable() {
            public void run() {
                MCKeyboard.setKeyboardInputVisible(i);
            }
        }, (long) i2);
    }

    public static void setKeyboardInputText(final String str) {
        activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                MCKeyboard.mEditText.setText(str);
            }
        });
    }

    public static void keyboardInput_Hide() {
        activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                ((InputMethodManager) MCKeyboard.activity.getSystemService("input_method")).hideSoftInputFromWindow(MCKeyboard.mEditText.getWindowToken(), 0);
            }
        });
    }

    public static void setKeyboardInputSingleLine(final int i) {
        activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                EditText access$100 = MCKeyboard.mEditText;
                int i = 1;
                if (i != 1) {
                    i = 100;
                }
                access$100.setMaxLines(i);
            }
        });
    }

    public static void setKeyboardInputTypePassword(final int i) {
        Log.i("cocojava", String.format("setKeyboardInputTypePassword: %d", Integer.valueOf(i)));
        activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                if (i == 0) {
                    MCKeyboard.mEditText.setInputType(145);
                } else {
                    MCKeyboard.mEditText.setInputType(129);
                }
            }
        });
    }

    public static void setKeyboardInputMaxLength(final int i) {
        Log.i("cocojava", String.format("setKeyboardInputMaxLength: %d", Integer.valueOf(i)));
        activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                MCKeyboard.mEditText.setFilters(new InputFilter[]{new InputFilter() {
                    private final int mMax = i;

                    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
                        int i5 = 0;
                        while (i < i2) {
                            if (!Character.isHighSurrogate(charSequence.charAt(i))) {
                                i5++;
                            }
                            i++;
                        }
                        for (int i6 = 0; i6 < i3; i6++) {
                            if (!Character.isHighSurrogate(spanned.charAt(i6))) {
                                i5++;
                            }
                        }
                        while (i4 < spanned.length()) {
                            if (!Character.isHighSurrogate(spanned.charAt(i4))) {
                                i5++;
                            }
                            i4++;
                        }
                        if (i5 <= this.mMax) {
                            return null;
                        }
                        return "";
                    }
                }});
            }
        });
    }

    public static void setImeOptions(final int i) {
        Log.i("cocojava", String.format("setImeOptions: %d", Integer.valueOf(i)));
        activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                MCKeyboard.mEditText.setImeOptions(i);
            }
        });
    }

    public void onPause() {
        super.onPause();
        keyboardInput_Hide();
    }
}
