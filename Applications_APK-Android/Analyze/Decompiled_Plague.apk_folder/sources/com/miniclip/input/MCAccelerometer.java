package com.miniclip.input;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.Display;
import android.view.WindowManager;
import com.miniclip.framework.AbstractActivityListener;
import com.miniclip.framework.MiniclipAndroidActivity;

public class MCAccelerometer extends AbstractActivityListener implements SensorEventListener {
    private static Display display = null;
    private static MCAccelerometer instance = new MCAccelerometer();
    private static boolean isEnabled = false;
    private static boolean isRegistered = false;
    private static Sensor mAccelerometer = null;
    private static boolean mHasWindowFocus = false;
    private static boolean mResumeOnFocus = false;
    private static int mRotation;
    private static SensorManager mSensorManager;

    private static native void onSensorChanged(float f, float f2, float f3, long j);

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    private MCAccelerometer() {
    }

    public static void init(MiniclipAndroidActivity miniclipAndroidActivity) {
        mRotation = 1;
        mSensorManager = (SensorManager) miniclipAndroidActivity.getSystemService("sensor");
        mAccelerometer = mSensorManager.getDefaultSensor(1);
        try {
            display = ((WindowManager) miniclipAndroidActivity.getSystemService("window")).getDefaultDisplay();
            mRotation = display.getRotation();
        } catch (NoSuchMethodError e) {
            e.printStackTrace();
        }
        miniclipAndroidActivity.addListener(instance);
    }

    public static void setEnabled(boolean z) {
        isEnabled = z;
        if (isEnabled) {
            instance.register();
        } else {
            instance.unregister();
        }
    }

    private void register() {
        if (!isRegistered && isEnabled) {
            isRegistered = true;
            mSensorManager.registerListener(this, mAccelerometer, 1);
        }
    }

    private void unregister() {
        if (isRegistered) {
            isRegistered = false;
            mSensorManager.unregisterListener(this);
        }
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == 1) {
            try {
                mRotation = display.getRotation();
            } catch (NoSuchMethodError e) {
                e.printStackTrace();
            }
            if (mRotation == 0) {
                onSensorChanged(-sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2], sensorEvent.timestamp);
            } else if (mRotation == 2) {
                onSensorChanged(sensorEvent.values[0], -sensorEvent.values[1], sensorEvent.values[2], sensorEvent.timestamp);
            } else if (mRotation == 1) {
                onSensorChanged(sensorEvent.values[1], sensorEvent.values[0], sensorEvent.values[2], sensorEvent.timestamp);
            } else if (mRotation == 3) {
                onSensorChanged(-sensorEvent.values[1], -sensorEvent.values[0], sensorEvent.values[2], sensorEvent.timestamp);
            }
        }
    }

    public void onPause() {
        unregister();
    }

    public void onWindowFocusChanged(boolean z) {
        mHasWindowFocus = z;
        if (mHasWindowFocus && mResumeOnFocus) {
            mResumeOnFocus = false;
            register();
        }
    }

    public void onResume() {
        if (mHasWindowFocus) {
            mResumeOnFocus = false;
            register();
            return;
        }
        mResumeOnFocus = true;
    }
}
