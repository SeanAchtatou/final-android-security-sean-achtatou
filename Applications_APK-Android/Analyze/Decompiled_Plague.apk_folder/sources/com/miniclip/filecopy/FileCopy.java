package com.miniclip.filecopy;

import android.net.Uri;
import android.util.Log;
import com.miniclip.framework.Miniclip;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileCopy {
    public static void copyFile(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public static void copyContent(String str, String str2) {
        try {
            InputStream openInputStream = Miniclip.getActivity().getContentResolver().openInputStream(Uri.parse(str));
            FileOutputStream fileOutputStream = new FileOutputStream(str2);
            copyFile(openInputStream, fileOutputStream);
            openInputStream.close();
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
    }
}
