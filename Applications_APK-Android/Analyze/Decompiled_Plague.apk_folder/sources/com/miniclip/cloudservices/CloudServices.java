package com.miniclip.cloudservices;

import android.app.Activity;
import android.util.Log;

public class CloudServices {
    private static String CLOUDSERVICES_TAG = "CloudServices";
    public static boolean DEBUG = false;
    private static Activity mActivity;
    /* access modifiers changed from: private */
    public static CloudServicesListener mListener;

    public interface CloudServicesListener {
        void cloudServices_load(int i, int i2);

        void cloudServices_resolve(int i, int i2, String str, String str2, int i3);

        void cloudServices_update(int i, int i2, String str);
    }

    public static native void MonCloudServicesDataLoaded(int i, int i2, int i3, String str, int i4, String str2, String str3, int i5);

    public static void setup(Activity activity, CloudServicesListener cloudServicesListener) {
        mActivity = activity;
        mListener = cloudServicesListener;
    }

    public static void load(final int i, final int i2) {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (CloudServices.mListener != null) {
                    CloudServices.mListener.cloudServices_load(i, i2);
                }
            }
        });
    }

    public static void update(final int i, final int i2, final String str) {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (CloudServices.mListener != null) {
                    CloudServices.mListener.cloudServices_update(i, i2, str);
                }
            }
        });
    }

    public static void resolve(int i, int i2, String str, String str2, int i3) {
        final int i4 = i;
        final int i5 = i2;
        final String str3 = str;
        final String str4 = str2;
        final int i6 = i3;
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (CloudServices.mListener != null) {
                    CloudServices.mListener.cloudServices_resolve(i4, i5, str3, str4, i6);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void debugLog(String str) {
        if (DEBUG) {
            Log.d(CLOUDSERVICES_TAG, str);
        }
    }
}
