package com.miniclip.network;

import com.google.android.gms.games.GamesStatusCodes;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JavaSocket {
    /* access modifiers changed from: private */
    public static ArrayList<byte[]> _certs;
    /* access modifiers changed from: private */
    public DatagramSocket _datagramSocket;
    /* access modifiers changed from: private */
    public DataOutputStream _dout = null;
    /* access modifiers changed from: private */
    public InetAddress _host;
    /* access modifiers changed from: private */
    public String _hostAddress;
    /* access modifiers changed from: private */
    public int _hostPort;
    /* access modifiers changed from: private */
    public boolean _isConnected;
    /* access modifiers changed from: private */
    public long _nativeObject;
    private ByteArrayOutputStream _packetBuffer;
    private int _packetSize;
    private PacketSizeHeaderType _packetSizeHeaderType;
    private List<byte[]> _queuedData;
    /* access modifiers changed from: private */
    public Thread _sendThread;
    /* access modifiers changed from: private */
    public SocketType _socketType;
    /* access modifiers changed from: private */
    public int _workBufferSize;

    private enum PacketSizeHeaderType {
        None,
        Size16,
        Size32,
        Size64
    }

    private enum SocketType {
        TCP,
        TCPSSL,
        UDP
    }

    public static native void onConnect(long j);

    public static native void onData(long j, byte[] bArr, int i, int i2);

    public static native void onDisconnect(long j, int i, String str);

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00ae, code lost:
        if (r3.equals("none") != false) goto L_0x00d0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00f9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    JavaSocket(java.lang.String r3, java.lang.String r4, int r5, long r6, int r8, java.lang.String r9) {
        /*
            r2 = this;
            r2.<init>()
            r2._hostAddress = r4
            r2._hostPort = r5
            r2._nativeObject = r6
            r2._workBufferSize = r8
            r4 = 0
            r2._dout = r4
            r5 = 0
            r2._isConnected = r5
            r2._host = r4
            r2._datagramSocket = r4
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.List r4 = java.util.Collections.synchronizedList(r4)
            r2._queuedData = r4
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream
            r4.<init>()
            r2._packetBuffer = r4
            r2._packetSize = r5
            java.lang.String r4 = r3.toLowerCase()
            int r6 = r4.hashCode()
            r7 = -879106421(0xffffffffcb99e68b, float:-2.0172054E7)
            r8 = 1
            r0 = 2
            r1 = -1
            if (r6 == r7) goto L_0x0058
            r7 = 114657(0x1bfe1, float:1.60669E-40)
            if (r6 == r7) goto L_0x004e
            r7 = 115649(0x1c3c1, float:1.62059E-40)
            if (r6 == r7) goto L_0x0044
            goto L_0x0062
        L_0x0044:
            java.lang.String r6 = "udp"
            boolean r4 = r4.equals(r6)
            if (r4 == 0) goto L_0x0062
            r4 = r0
            goto L_0x0063
        L_0x004e:
            java.lang.String r6 = "tcp"
            boolean r4 = r4.equals(r6)
            if (r4 == 0) goto L_0x0062
            r4 = r5
            goto L_0x0063
        L_0x0058:
            java.lang.String r6 = "tcpssl"
            boolean r4 = r4.equals(r6)
            if (r4 == 0) goto L_0x0062
            r4 = r8
            goto L_0x0063
        L_0x0062:
            r4 = r1
        L_0x0063:
            switch(r4) {
                case 0: goto L_0x0087;
                case 1: goto L_0x0082;
                case 2: goto L_0x007d;
                default: goto L_0x0066;
            }
        L_0x0066:
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Invalid Socket Type: "
            r5.append(r6)
            r5.append(r3)
            java.lang.String r3 = r5.toString()
            r4.<init>(r3)
            throw r4
        L_0x007d:
            com.miniclip.network.JavaSocket$SocketType r3 = com.miniclip.network.JavaSocket.SocketType.UDP
            r2._socketType = r3
            goto L_0x008b
        L_0x0082:
            com.miniclip.network.JavaSocket$SocketType r3 = com.miniclip.network.JavaSocket.SocketType.TCPSSL
            r2._socketType = r3
            goto L_0x008b
        L_0x0087:
            com.miniclip.network.JavaSocket$SocketType r3 = com.miniclip.network.JavaSocket.SocketType.TCP
            r2._socketType = r3
        L_0x008b:
            java.lang.String r3 = r9.toLowerCase()
            int r4 = r3.hashCode()
            r6 = -901912090(0xffffffffca3de9e6, float:-3111545.5)
            if (r4 == r6) goto L_0x00c5
            r6 = -901912032(0xffffffffca3dea20, float:-3111560.0)
            if (r4 == r6) goto L_0x00bb
            r6 = -901911937(0xffffffffca3dea7f, float:-3111583.8)
            if (r4 == r6) goto L_0x00b1
            r6 = 3387192(0x33af38, float:4.746467E-39)
            if (r4 == r6) goto L_0x00a8
            goto L_0x00cf
        L_0x00a8:
            java.lang.String r4 = "none"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00cf
            goto L_0x00d0
        L_0x00b1:
            java.lang.String r4 = "size64"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00cf
            r5 = 3
            goto L_0x00d0
        L_0x00bb:
            java.lang.String r4 = "size32"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00cf
            r5 = r0
            goto L_0x00d0
        L_0x00c5:
            java.lang.String r4 = "size16"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00cf
            r5 = r8
            goto L_0x00d0
        L_0x00cf:
            r5 = r1
        L_0x00d0:
            switch(r5) {
                case 0: goto L_0x00f9;
                case 1: goto L_0x00f4;
                case 2: goto L_0x00ef;
                case 3: goto L_0x00ea;
                default: goto L_0x00d3;
            }
        L_0x00d3:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Invalid Packet Size Type: "
            r4.append(r5)
            r4.append(r9)
            java.lang.String r4 = r4.toString()
            r3.<init>(r4)
            throw r3
        L_0x00ea:
            com.miniclip.network.JavaSocket$PacketSizeHeaderType r3 = com.miniclip.network.JavaSocket.PacketSizeHeaderType.Size64
            r2._packetSizeHeaderType = r3
            goto L_0x00fd
        L_0x00ef:
            com.miniclip.network.JavaSocket$PacketSizeHeaderType r3 = com.miniclip.network.JavaSocket.PacketSizeHeaderType.Size32
            r2._packetSizeHeaderType = r3
            goto L_0x00fd
        L_0x00f4:
            com.miniclip.network.JavaSocket$PacketSizeHeaderType r3 = com.miniclip.network.JavaSocket.PacketSizeHeaderType.Size16
            r2._packetSizeHeaderType = r3
            goto L_0x00fd
        L_0x00f9:
            com.miniclip.network.JavaSocket$PacketSizeHeaderType r3 = com.miniclip.network.JavaSocket.PacketSizeHeaderType.None
            r2._packetSizeHeaderType = r3
        L_0x00fd:
            java.lang.Thread r3 = new java.lang.Thread
            com.miniclip.network.JavaSocket$1 r4 = new com.miniclip.network.JavaSocket$1
            r4.<init>()
            r3.<init>(r4)
            r2._sendThread = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.miniclip.network.JavaSocket.<init>(java.lang.String, java.lang.String, int, long, int, java.lang.String):void");
    }

    private boolean connectUDP() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    InetAddress unused = JavaSocket.this._host = InetAddress.getByName(JavaSocket.this._hostAddress);
                    DatagramSocket unused2 = JavaSocket.this._datagramSocket = new DatagramSocket();
                    byte[] bArr = new byte[JavaSocket.this._workBufferSize];
                    boolean unused3 = JavaSocket.this._isConnected = true;
                    JavaSocket.onConnect(JavaSocket.this._nativeObject);
                    JavaSocket.this._sendThread.start();
                    while (true) {
                        JavaSocket.this._datagramSocket.setSoTimeout(GamesStatusCodes.STATUS_REQUEST_UPDATE_PARTIAL_SUCCESS);
                        DatagramPacket datagramPacket = new DatagramPacket(bArr, bArr.length, JavaSocket.this._host, JavaSocket.this._hostPort);
                        JavaSocket.this._datagramSocket.receive(datagramPacket);
                        JavaSocket.this.onDataPartial(datagramPacket.getLength(), datagramPacket.getData());
                    }
                } catch (Exception e) {
                    try {
                        e.printStackTrace();
                        boolean unused4 = JavaSocket.this._isConnected = false;
                        synchronized (JavaSocket.this._sendThread) {
                            JavaSocket.this._sendThread.notify();
                            JavaSocket.onDisconnect(JavaSocket.this._nativeObject, 0, "");
                            if (JavaSocket.this._datagramSocket != null) {
                                JavaSocket.this._datagramSocket.close();
                            }
                        }
                    } catch (Throwable th) {
                        boolean unused5 = JavaSocket.this._isConnected = false;
                        synchronized (JavaSocket.this._sendThread) {
                            JavaSocket.this._sendThread.notify();
                            JavaSocket.onDisconnect(JavaSocket.this._nativeObject, 0, "");
                            if (JavaSocket.this._datagramSocket != null) {
                                JavaSocket.this._datagramSocket.close();
                            }
                            throw th;
                        }
                    }
                }
            }
        }).start();
        return true;
    }

    private boolean connectTCP() {
        new Thread(new Runnable() {
            /* JADX INFO: additional move instructions added (1) to help type inference */
            /* JADX WARNING: Removed duplicated region for block: B:46:0x00ee  */
            /* JADX WARNING: Removed duplicated region for block: B:96:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r9 = this;
                    r0 = 1
                    r1 = 0
                    r2 = 0
                    com.miniclip.network.JavaSocket r3 = com.miniclip.network.JavaSocket.this     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.lang.String r3 = r3._hostAddress     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.lang.String r4 = ":"
                    boolean r3 = r3.contains(r4)     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    if (r3 == 0) goto L_0x0021
                    com.miniclip.network.JavaSocket r3 = com.miniclip.network.JavaSocket.this     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    com.miniclip.network.JavaSocket r4 = com.miniclip.network.JavaSocket.this     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.lang.String r4 = r4._hostAddress     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.net.InetAddress r4 = java.net.Inet6Address.getByName(r4)     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.net.InetAddress unused = r3._host = r4     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    goto L_0x0030
                L_0x0021:
                    com.miniclip.network.JavaSocket r3 = com.miniclip.network.JavaSocket.this     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    com.miniclip.network.JavaSocket r4 = com.miniclip.network.JavaSocket.this     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.lang.String r4 = r4._hostAddress     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.net.InetAddress r4 = java.net.InetAddress.getByName(r4)     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.net.InetAddress unused = r3._host = r4     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                L_0x0030:
                    com.miniclip.network.JavaSocket r3 = com.miniclip.network.JavaSocket.this     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.lang.String r3 = r3._hostAddress     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.net.InetAddress.getAllByName(r3)     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    com.miniclip.network.JavaSocket r3 = com.miniclip.network.JavaSocket.this     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    com.miniclip.network.JavaSocket$SocketType r3 = r3._socketType     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    com.miniclip.network.JavaSocket$SocketType r4 = com.miniclip.network.JavaSocket.SocketType.TCPSSL     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    if (r3 != r4) goto L_0x008a
                    java.lang.String r3 = "TLS"
                    javax.net.ssl.SSLContext r3 = javax.net.ssl.SSLContext.getInstance(r3)     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.util.ArrayList r4 = com.miniclip.network.JavaSocket._certs     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    if (r4 == 0) goto L_0x006a
                    java.util.ArrayList r4 = com.miniclip.network.JavaSocket._certs     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    int r4 = r4.size()     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    if (r4 <= 0) goto L_0x006a
                    javax.net.ssl.TrustManager[] r4 = new javax.net.ssl.TrustManager[r0]     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    com.miniclip.network.JavaTrustManager r5 = new com.miniclip.network.JavaTrustManager     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.util.ArrayList r6 = com.miniclip.network.JavaSocket._certs     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    r5.<init>(r6)     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    r4[r1] = r5     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    r3.init(r2, r4, r2)     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    goto L_0x006d
                L_0x006a:
                    r3.init(r2, r2, r2)     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                L_0x006d:
                    javax.net.ssl.SSLSocketFactory r3 = r3.getSocketFactory()     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    com.miniclip.network.JavaSocket r4 = com.miniclip.network.JavaSocket.this     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.net.InetAddress r4 = r4._host     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    com.miniclip.network.JavaSocket r5 = com.miniclip.network.JavaSocket.this     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    int r5 = r5._hostPort     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.net.Socket r3 = r3.createSocket(r4, r5)     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    javax.net.ssl.SSLSocket r3 = (javax.net.ssl.SSLSocket) r3     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    r3.setUseClientMode(r0)     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    r3.startHandshake()     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    goto L_0x00a7
                L_0x008a:
                    com.miniclip.network.JavaSocket r3 = com.miniclip.network.JavaSocket.this     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    com.miniclip.network.JavaSocket$SocketType r3 = r3._socketType     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    com.miniclip.network.JavaSocket$SocketType r4 = com.miniclip.network.JavaSocket.SocketType.TCP     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    if (r3 != r4) goto L_0x00a6
                    java.net.Socket r3 = new java.net.Socket     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    com.miniclip.network.JavaSocket r4 = com.miniclip.network.JavaSocket.this     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    java.net.InetAddress r4 = r4._host     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    com.miniclip.network.JavaSocket r5 = com.miniclip.network.JavaSocket.this     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    int r5 = r5._hostPort     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    r3.<init>(r4, r5)     // Catch:{ UnknownHostException -> 0x00e6, IOException -> 0x00df, Exception -> 0x00d8 }
                    goto L_0x00a7
                L_0x00a6:
                    r3 = r2
                L_0x00a7:
                    java.io.InputStream r4 = r3.getInputStream()     // Catch:{ UnknownHostException -> 0x00d2, IOException -> 0x00cc, Exception -> 0x00c6 }
                    java.io.OutputStream r5 = r3.getOutputStream()     // Catch:{ UnknownHostException -> 0x00c0, IOException -> 0x00ba, Exception -> 0x00b4 }
                    r2 = r5
                    r5 = r4
                    r4 = r3
                    goto L_0x00ec
                L_0x00b4:
                    r5 = move-exception
                    r8 = r4
                    r4 = r3
                    r3 = r5
                    r5 = r8
                    goto L_0x00db
                L_0x00ba:
                    r5 = move-exception
                    r8 = r4
                    r4 = r3
                    r3 = r5
                    r5 = r8
                    goto L_0x00e2
                L_0x00c0:
                    r5 = move-exception
                    r8 = r4
                    r4 = r3
                    r3 = r5
                    r5 = r8
                    goto L_0x00e9
                L_0x00c6:
                    r4 = move-exception
                    r5 = r2
                    r8 = r4
                    r4 = r3
                    r3 = r8
                    goto L_0x00db
                L_0x00cc:
                    r4 = move-exception
                    r5 = r2
                    r8 = r4
                    r4 = r3
                    r3 = r8
                    goto L_0x00e2
                L_0x00d2:
                    r4 = move-exception
                    r5 = r2
                    r8 = r4
                    r4 = r3
                    r3 = r8
                    goto L_0x00e9
                L_0x00d8:
                    r3 = move-exception
                    r4 = r2
                    r5 = r4
                L_0x00db:
                    r3.printStackTrace()
                    goto L_0x00ec
                L_0x00df:
                    r3 = move-exception
                    r4 = r2
                    r5 = r4
                L_0x00e2:
                    r3.printStackTrace()
                    goto L_0x00ec
                L_0x00e6:
                    r3 = move-exception
                    r4 = r2
                    r5 = r4
                L_0x00e9:
                    r3.printStackTrace()
                L_0x00ec:
                    if (r4 == 0) goto L_0x0199
                    java.io.DataInputStream r3 = new java.io.DataInputStream
                    r3.<init>(r5)
                    com.miniclip.network.JavaSocket r4 = com.miniclip.network.JavaSocket.this
                    java.io.DataOutputStream r5 = new java.io.DataOutputStream
                    r5.<init>(r2)
                    java.io.DataOutputStream unused = r4._dout = r5
                    com.miniclip.network.JavaSocket r2 = com.miniclip.network.JavaSocket.this
                    boolean unused = r2._isConnected = r0
                    com.miniclip.network.JavaSocket r2 = com.miniclip.network.JavaSocket.this
                    long r4 = r2._nativeObject
                    com.miniclip.network.JavaSocket.onConnect(r4)
                    com.miniclip.network.JavaSocket r2 = com.miniclip.network.JavaSocket.this
                    java.lang.Thread r2 = r2._sendThread
                    r2.start()
                    com.miniclip.network.JavaSocket r2 = com.miniclip.network.JavaSocket.this     // Catch:{ IOException -> 0x015f }
                    int r2 = r2._workBufferSize     // Catch:{ IOException -> 0x015f }
                    byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x015f }
                L_0x011c:
                    int r4 = r3.read(r2)     // Catch:{ IOException -> 0x015f }
                    r5 = -1
                    if (r4 != r5) goto L_0x0124
                    goto L_0x0163
                L_0x0124:
                    int r6 = r3.available()     // Catch:{ IOException -> 0x015f }
                    if (r6 <= 0) goto L_0x0153
                    java.io.ByteArrayOutputStream r6 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x015f }
                    r6.<init>()     // Catch:{ IOException -> 0x015f }
                    r6.write(r2, r1, r4)     // Catch:{ IOException -> 0x015f }
                L_0x0132:
                    int r4 = r3.available()     // Catch:{ IOException -> 0x015f }
                    if (r4 <= 0) goto L_0x0146
                    int r4 = r3.read(r2)     // Catch:{ IOException -> 0x015f }
                    if (r4 <= 0) goto L_0x0142
                    r6.write(r2, r1, r4)     // Catch:{ IOException -> 0x015f }
                    goto L_0x0132
                L_0x0142:
                    if (r4 != r5) goto L_0x0132
                    r4 = r0
                    goto L_0x0147
                L_0x0146:
                    r4 = r1
                L_0x0147:
                    byte[] r5 = r6.toByteArray()     // Catch:{ IOException -> 0x015f }
                    int r6 = r6.size()     // Catch:{ IOException -> 0x015f }
                    r8 = r6
                    r6 = r4
                    r4 = r8
                    goto L_0x0155
                L_0x0153:
                    r6 = r1
                    r5 = r2
                L_0x0155:
                    if (r4 <= 0) goto L_0x015c
                    com.miniclip.network.JavaSocket r7 = com.miniclip.network.JavaSocket.this     // Catch:{ IOException -> 0x015f }
                    r7.onDataPartial(r4, r5)     // Catch:{ IOException -> 0x015f }
                L_0x015c:
                    if (r6 == 0) goto L_0x011c
                    goto L_0x0163
                L_0x015f:
                    r0 = move-exception
                    r0.printStackTrace()
                L_0x0163:
                    com.miniclip.network.JavaSocket r0 = com.miniclip.network.JavaSocket.this
                    boolean unused = r0._isConnected = r1
                    com.miniclip.network.JavaSocket r0 = com.miniclip.network.JavaSocket.this
                    java.lang.Thread r0 = r0._sendThread
                    monitor-enter(r0)
                    com.miniclip.network.JavaSocket r2 = com.miniclip.network.JavaSocket.this     // Catch:{ all -> 0x0196 }
                    java.lang.Thread r2 = r2._sendThread     // Catch:{ all -> 0x0196 }
                    r2.notify()     // Catch:{ all -> 0x0196 }
                    monitor-exit(r0)     // Catch:{ all -> 0x0196 }
                    com.miniclip.network.JavaSocket r0 = com.miniclip.network.JavaSocket.this
                    long r4 = r0._nativeObject
                    java.lang.String r0 = ""
                    com.miniclip.network.JavaSocket.onDisconnect(r4, r1, r0)
                    r3.close()     // Catch:{ IOException -> 0x0191 }
                    com.miniclip.network.JavaSocket r0 = com.miniclip.network.JavaSocket.this     // Catch:{ IOException -> 0x0191 }
                    java.io.DataOutputStream r0 = r0._dout     // Catch:{ IOException -> 0x0191 }
                    r0.close()     // Catch:{ IOException -> 0x0191 }
                    goto L_0x0199
                L_0x0191:
                    r0 = move-exception
                    r0.printStackTrace()
                    goto L_0x0199
                L_0x0196:
                    r1 = move-exception
                    monitor-exit(r0)     // Catch:{ all -> 0x0196 }
                    throw r1
                L_0x0199:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.miniclip.network.JavaSocket.AnonymousClass3.run():void");
            }
        }).start();
        return true;
    }

    public boolean connect() {
        if (this._socketType == SocketType.UDP) {
            return connectUDP();
        }
        return connectTCP();
    }

    public boolean isConnected() {
        return this._isConnected;
    }

    public void onDataPartial(int i, byte[] bArr) {
        int i2;
        if (this._packetSizeHeaderType != PacketSizeHeaderType.None) {
            switch (this._packetSizeHeaderType) {
                case Size16:
                    i2 = 2;
                    break;
                case Size32:
                    i2 = 4;
                    break;
                case Size64:
                    i2 = 8;
                    break;
                default:
                    i2 = 0;
                    break;
            }
            this._packetBuffer.write(bArr, 0, i);
            while (true) {
                if (this._packetSize == 0 && this._packetBuffer.size() >= i2) {
                    this._packetSize = ByteBuffer.wrap(this._packetBuffer.toByteArray()).getInt();
                }
                if (this._packetSize != 0 && this._packetBuffer.size() - i2 >= this._packetSize) {
                    onData(this._nativeObject, this._packetBuffer.toByteArray(), i2, this._packetSize);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    byteArrayOutputStream.write(this._packetBuffer.toByteArray(), this._packetSize + i2, this._packetBuffer.size() - (this._packetSize + i2));
                    this._packetBuffer = byteArrayOutputStream;
                    this._packetSize = 0;
                } else {
                    return;
                }
            }
        } else {
            onData(this._nativeObject, bArr, 0, i);
        }
    }

    /* access modifiers changed from: private */
    public void sendQueuedData() {
        synchronized (this._queuedData) {
            Iterator<byte[]> it = this._queuedData.iterator();
            while (it.hasNext()) {
                if (this._socketType == SocketType.UDP) {
                    sendDataUDP(it.next());
                } else {
                    sendDataTCP(it.next());
                }
            }
            this._queuedData.clear();
        }
    }

    private boolean sendDataUDP(byte[] bArr) {
        if (this._datagramSocket == null) {
            return false;
        }
        try {
            this._datagramSocket.send(new DatagramPacket(bArr, bArr.length, this._host, this._hostPort));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean sendDataTCP(byte[] bArr) {
        if (this._dout != null) {
            try {
                this._dout.write(bArr, 0, bArr.length);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean sendData(byte[] bArr) {
        if (this._packetSizeHeaderType != PacketSizeHeaderType.None) {
            ByteBuffer byteBuffer = null;
            switch (this._packetSizeHeaderType) {
                case Size16:
                    if (bArr.length <= 65535) {
                        byteBuffer = ByteBuffer.allocate(bArr.length + 2);
                        byteBuffer.putShort((short) bArr.length);
                        break;
                    } else {
                        return false;
                    }
                case Size32:
                    byteBuffer = ByteBuffer.allocate(bArr.length + 4);
                    byteBuffer.putInt(bArr.length);
                    break;
                case Size64:
                    byteBuffer = ByteBuffer.allocate(bArr.length + 8);
                    byteBuffer.putLong((long) bArr.length);
                    break;
            }
            byteBuffer.put(bArr);
            bArr = byteBuffer.array();
        }
        this._queuedData.add(bArr);
        synchronized (this._sendThread) {
            this._sendThread.notify();
        }
        return true;
    }

    public void setWorkBufferSize(int i) {
        this._workBufferSize = i;
    }

    public void setNativeObject(long j) {
        this._nativeObject = j;
    }

    public static boolean addTrustedCertificate(byte[] bArr) {
        if (_certs == null) {
            _certs = new ArrayList<>();
        }
        _certs.add(bArr);
        return true;
    }

    public static void clearTrustedCertificates() {
        if (_certs == null) {
            _certs = new ArrayList<>();
        }
        _certs.clear();
    }
}
