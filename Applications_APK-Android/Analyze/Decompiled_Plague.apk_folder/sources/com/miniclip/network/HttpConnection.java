package com.miniclip.network;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import com.miniclip.framework.Miniclip;
import com.miniclip.framework.ThreadingContext;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class HttpConnection {
    private static final int CORE_POOL_SIZE = (CPU_COUNT + 1);
    private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();
    public static final int HttpConnectionErrorBadURL = -1000;
    public static final int HttpConnectionErrorUnknown = -1;
    public static final int HttpConnectionErrorUnknownHost = -1003;
    private static final int KEEP_ALIVE = 1;
    private static final int MAXIMUM_POOL_SIZE = ((CPU_COUNT * 2) + 1);
    /* access modifiers changed from: private */
    public static final Executor THREAD_POOL_EXECUTOR = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, 1, TimeUnit.SECONDS, sPoolWorkQueue);
    private static int defaultTimeout = 60000;
    private static final BlockingQueue<Runnable> sPoolWorkQueue = new LinkedBlockingQueue();
    private boolean canceled;
    protected Integer connectionID;
    /* access modifiers changed from: private */
    public MCHttpURLConnectionTask connectionTask;
    protected HashMap<String, String> headerProperties;
    protected String httpCommand;
    protected boolean jniCall;
    protected HttpConnectionListener listener;
    protected int timeout;
    protected String urlBody;
    protected String urlString;

    /* access modifiers changed from: private */
    public static native void downloadComplete(int i, int i2, HashMap hashMap, int i3, byte[] bArr);

    /* access modifiers changed from: private */
    public static native void downloadFailed(int i, String str, int i2);

    public HttpConnection(String str, String str2, String str3, HttpConnectionListener httpConnectionListener, int i, HashMap hashMap) {
        this.jniCall = false;
        this.httpCommand = "GET";
        this.urlBody = "";
        this.canceled = false;
        this.connectionID = HttpConnectionsManager.sharedInstance().addConnection(this);
        this.urlString = str;
        this.httpCommand = str2;
        this.urlBody = str3;
        this.listener = httpConnectionListener;
        this.timeout = i;
        this.headerProperties = hashMap;
    }

    public HttpConnection(String str, String str2, String str3, HttpConnectionListener httpConnectionListener, int i) {
        this(str, str2, str3, httpConnectionListener, i, new HashMap());
    }

    public HttpConnection(String str, String str2) {
        this(str, str2, "", null, defaultTimeout, new HashMap());
    }

    public HttpConnection(String str, String str2, String str3) {
        this(str, str2, str3, null, defaultTimeout, new HashMap());
    }

    public HttpConnection(String str, String str2, HttpConnectionListener httpConnectionListener) {
        this(str, str2, "", httpConnectionListener, defaultTimeout, new HashMap());
    }

    public HttpConnection(String str, String str2, String str3, HttpConnectionListener httpConnectionListener) {
        this(str, str2, str3, httpConnectionListener, defaultTimeout, new HashMap());
    }

    private class MCHttpURLConnectionTask extends AsyncTask<String, Void, Boolean> {
        private int errorCode;
        /* access modifiers changed from: private */
        public HttpConnection owner;

        private MCHttpURLConnectionTask() {
            this.errorCode = -1;
        }

        /* JADX WARN: Failed to insert an additional move for type inference into block B:81:0x01db */
        /* JADX WARN: Failed to insert an additional move for type inference into block B:74:0x01cb */
        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Type inference failed for: r2v2 */
        /* JADX WARN: Type inference failed for: r2v3, types: [java.net.HttpURLConnection] */
        /* JADX WARN: Type inference failed for: r2v4, types: [java.net.HttpURLConnection] */
        /* JADX WARN: Type inference failed for: r2v5 */
        /* JADX WARN: Type inference failed for: r2v6 */
        /* JADX WARN: Type inference failed for: r2v7 */
        /* JADX WARN: Type inference failed for: r2v8, types: [byte[]] */
        /* JADX WARN: Type inference failed for: r2v33 */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:66:0x01bf, code lost:
            r11 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x01c1, code lost:
            r11 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x01c2, code lost:
            r2 = r1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:69:0x01c4, code lost:
            r11 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:0x01c5, code lost:
            r2 = r1;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:46:0x0135 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0046 A[Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }] */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0073 A[Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }, LOOP:0: B:17:0x006d->B:19:0x0073, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x008b A[Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00a1 A[Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }] */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0102 A[Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }] */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x0162 A[Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }] */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x018f A[Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }] */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x01a2 A[Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }] */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x01b3  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x01bb A[SYNTHETIC, Splitter:B:64:0x01bb] */
        /* JADX WARNING: Removed duplicated region for block: B:66:0x01bf A[ExcHandler: all (th java.lang.Throwable), Splitter:B:34:0x00eb] */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x01c4 A[ExcHandler: UnknownHostException (e java.net.UnknownHostException), Splitter:B:6:0x000f] */
        /* JADX WARNING: Removed duplicated region for block: B:77:0x01d4  */
        /* JADX WARNING: Removed duplicated region for block: B:87:0x01f2  */
        /* JADX WARNING: Removed duplicated region for block: B:97:0x014e A[EDGE_INSN: B:97:0x014e->B:51:0x014e ?: BREAK  , SYNTHETIC] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Boolean doInBackground(java.lang.String... r11) {
            /*
                r10 = this;
                r0 = 0
                java.net.URL r1 = new java.net.URL     // Catch:{ MalformedURLException -> 0x01f6 }
                r2 = r11[r0]     // Catch:{ MalformedURLException -> 0x01f6 }
                r1.<init>(r2)     // Catch:{ MalformedURLException -> 0x01f6 }
                r2 = 0
                java.net.URLConnection r1 = r1.openConnection()     // Catch:{ UnknownHostException -> 0x01d8, IOException -> 0x01ca }
                java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ UnknownHostException -> 0x01d8, IOException -> 0x01ca }
                com.miniclip.network.HttpConnection r3 = com.miniclip.network.HttpConnection.this     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r3 = r3.httpCommand     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r1.setRequestMethod(r3)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                com.miniclip.network.HttpConnection r3 = com.miniclip.network.HttpConnection.this     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                int r3 = r3.timeout     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r1.setReadTimeout(r3)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                com.miniclip.network.HttpConnection r3 = com.miniclip.network.HttpConnection.this     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                int r3 = r3.timeout     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r1.setConnectTimeout(r3)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r3 = 1
                r1.setDoInput(r3)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                com.miniclip.network.HttpConnection r4 = com.miniclip.network.HttpConnection.this     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r4 = r4.httpCommand     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r5 = "POST"
                boolean r4 = r4.equals(r5)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                if (r4 != 0) goto L_0x0043
                com.miniclip.network.HttpConnection r4 = com.miniclip.network.HttpConnection.this     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r4 = r4.httpCommand     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r5 = "PUT"
                boolean r4 = r4.equals(r5)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                if (r4 == 0) goto L_0x0041
                goto L_0x0043
            L_0x0041:
                r4 = r0
                goto L_0x0044
            L_0x0043:
                r4 = r3
            L_0x0044:
                if (r4 == 0) goto L_0x0061
                com.miniclip.network.HttpConnection r2 = com.miniclip.network.HttpConnection.this     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r2 = r2.urlBody     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r5 = "UTF-8"
                byte[] r2 = r2.getBytes(r5)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                int r5 = r2.length     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r6 = "Content-Type"
                java.lang.String r7 = "application/x-www-form-urlencoded"
                r1.setRequestProperty(r6, r7)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r6 = "Content-Length"
                java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r1.setRequestProperty(r6, r5)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
            L_0x0061:
                com.miniclip.network.HttpConnection r5 = com.miniclip.network.HttpConnection.this     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.util.HashMap<java.lang.String, java.lang.String> r5 = r5.headerProperties     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.util.Set r5 = r5.entrySet()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.util.Iterator r5 = r5.iterator()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
            L_0x006d:
                boolean r6 = r5.hasNext()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                if (r6 == 0) goto L_0x0089
                java.lang.Object r6 = r5.next()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.util.Map$Entry r6 = (java.util.Map.Entry) r6     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.Object r7 = r6.getKey()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r7 = (java.lang.String) r7     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.Object r6 = r6.getValue()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r6 = (java.lang.String) r6     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r1.setRequestProperty(r7, r6)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                goto L_0x006d
            L_0x0089:
                if (r4 == 0) goto L_0x0095
                r1.setDoOutput(r3)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.io.OutputStream r4 = r1.getOutputStream()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r4.write(r2)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
            L_0x0095:
                int r2 = r1.getResponseCode()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r10.errorCode = r2     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                int r2 = r10.errorCode     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r4 = 200(0xc8, float:2.8E-43)
                if (r2 == r4) goto L_0x00eb
                java.lang.String r2 = "HttpConnection"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r4.<init>()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r5 = "Server returned HTTP "
                r4.append(r5)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                int r5 = r10.errorCode     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r4.append(r5)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r5 = " "
                r4.append(r5)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r5 = r1.getResponseMessage()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r4.append(r5)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r4 = r4.toString()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                android.util.Log.i(r2, r4)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                int r2 = r10.errorCode     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r4 = 302(0x12e, float:4.23E-43)
                if (r2 == r4) goto L_0x00d7
                int r2 = r10.errorCode     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r4 = 301(0x12d, float:4.22E-43)
                if (r2 == r4) goto L_0x00d7
                int r2 = r10.errorCode     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r4 = 303(0x12f, float:4.25E-43)
                if (r2 != r4) goto L_0x00eb
            L_0x00d7:
                java.lang.String r11 = "Location"
                java.lang.String r11 = r1.getHeaderField(r11)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String[] r2 = new java.lang.String[r3]     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r2[r0] = r11     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.Boolean r11 = r10.doInBackground(r2)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                if (r1 == 0) goto L_0x00ea
                r1.disconnect()
            L_0x00ea:
                return r11
            L_0x00eb:
                java.util.Map r2 = r1.getHeaderFields()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.util.HashMap r4 = new java.util.HashMap     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r4.<init>()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.util.Set r2 = r2.entrySet()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.util.Iterator r2 = r2.iterator()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
            L_0x00fc:
                boolean r5 = r2.hasNext()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                if (r5 == 0) goto L_0x012b
                java.lang.Object r5 = r2.next()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.Object r6 = r5.getValue()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.util.List r6 = (java.util.List) r6     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                boolean r7 = r6.isEmpty()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                if (r7 != 0) goto L_0x00fc
                java.lang.Object r7 = r5.getKey()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                if (r7 != 0) goto L_0x011b
                goto L_0x00fc
            L_0x011b:
                java.lang.Object r5 = r5.getKey()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.util.Iterator r6 = r6.iterator()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.Object r6 = r6.next()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r4.put(r5, r6)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                goto L_0x00fc
            L_0x012b:
                java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0135, UnknownHostException -> 0x01c4, all -> 0x01bf }
                java.io.InputStream r5 = r1.getInputStream()     // Catch:{ IOException -> 0x0135, UnknownHostException -> 0x01c4, all -> 0x01bf }
                r2.<init>(r5)     // Catch:{ IOException -> 0x0135, UnknownHostException -> 0x01c4, all -> 0x01bf }
                goto L_0x013e
            L_0x0135:
                java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.io.InputStream r5 = r1.getErrorStream()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r2.<init>(r5)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
            L_0x013e:
                java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r5.<init>()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r6 = 1024(0x400, float:1.435E-42)
                byte[] r6 = new byte[r6]     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
            L_0x0147:
                int r7 = r2.read(r6)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r8 = -1
                if (r7 != r8) goto L_0x01bb
                byte[] r5 = r5.toByteArray()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                int r6 = r5.length     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r2.close()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                com.miniclip.network.HttpConnection r2 = com.miniclip.network.HttpConnection.this     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r2 = r2.httpCommand     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r7 = "GET"
                boolean r2 = r2.equals(r7)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                if (r2 != 0) goto L_0x0189
                java.lang.String r2 = new java.lang.String     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r7 = "UTF-8"
                r2.<init>(r5, r7)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r7 = "HttpConnection"
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r8.<init>()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r9 = "HttpConnection response from "
                r8.append(r9)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r11 = r11[r0]     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r8.append(r11)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r11 = ": "
                r8.append(r11)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r8.append(r2)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.String r11 = r8.toString()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                android.util.Log.d(r7, r11)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
            L_0x0189:
                com.miniclip.network.HttpConnection r11 = com.miniclip.network.HttpConnection.this     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                boolean r11 = r11.jniCall     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                if (r11 == 0) goto L_0x019c
                com.miniclip.network.HttpConnection r11 = com.miniclip.network.HttpConnection.this     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.Integer r11 = r11.connectionID     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                int r11 = r11.intValue()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                int r2 = r10.errorCode     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                com.miniclip.network.HttpConnection.downloadComplete(r11, r2, r4, r6, r5)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
            L_0x019c:
                com.miniclip.network.HttpConnection r11 = com.miniclip.network.HttpConnection.this     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                com.miniclip.network.HttpConnectionListener r11 = r11.listener     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                if (r11 == 0) goto L_0x01b1
                com.miniclip.network.HttpConnection r11 = com.miniclip.network.HttpConnection.this     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                com.miniclip.network.HttpConnectionListener r11 = r11.listener     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                com.miniclip.network.HttpConnection r2 = com.miniclip.network.HttpConnection.this     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                java.lang.Integer r2 = r2.connectionID     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                int r2 = r2.intValue()     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                r11.downloadComplete(r2, r6, r5)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
            L_0x01b1:
                if (r1 == 0) goto L_0x01b6
                r1.disconnect()
            L_0x01b6:
                java.lang.Boolean r11 = java.lang.Boolean.valueOf(r3)
                return r11
            L_0x01bb:
                r5.write(r6, r0, r7)     // Catch:{ UnknownHostException -> 0x01c4, IOException -> 0x01c1, all -> 0x01bf }
                goto L_0x0147
            L_0x01bf:
                r11 = move-exception
                goto L_0x01f0
            L_0x01c1:
                r11 = move-exception
                r2 = r1
                goto L_0x01cb
            L_0x01c4:
                r11 = move-exception
                r2 = r1
                goto L_0x01d9
            L_0x01c7:
                r11 = move-exception
                r1 = r2
                goto L_0x01f0
            L_0x01ca:
                r11 = move-exception
            L_0x01cb:
                r11.printStackTrace()     // Catch:{ all -> 0x01c7 }
                java.lang.Boolean r11 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x01c7 }
                if (r2 == 0) goto L_0x01d7
                r2.disconnect()
            L_0x01d7:
                return r11
            L_0x01d8:
                r11 = move-exception
            L_0x01d9:
                java.lang.String r1 = "HttpConnection"
                java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x01c7 }
                android.util.Log.e(r1, r11)     // Catch:{ all -> 0x01c7 }
                r11 = -1003(0xfffffffffffffc15, float:NaN)
                r10.errorCode = r11     // Catch:{ all -> 0x01c7 }
                java.lang.Boolean r11 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x01c7 }
                if (r2 == 0) goto L_0x01ef
                r2.disconnect()
            L_0x01ef:
                return r11
            L_0x01f0:
                if (r1 == 0) goto L_0x01f5
                r1.disconnect()
            L_0x01f5:
                throw r11
            L_0x01f6:
                r1 = move-exception
                r1.printStackTrace()
                java.lang.String r1 = "HttpConnection"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "Invalid URL: "
                r2.append(r3)
                r11 = r11[r0]
                r2.append(r11)
                java.lang.String r11 = r2.toString()
                android.util.Log.e(r1, r11)
                r11 = -1000(0xfffffffffffffc18, float:NaN)
                r10.errorCode = r11
                java.lang.Boolean r11 = java.lang.Boolean.valueOf(r0)
                return r11
            */
            throw new UnsupportedOperationException("Method not decompiled: com.miniclip.network.HttpConnection.MCHttpURLConnectionTask.doInBackground(java.lang.String[]):java.lang.Boolean");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            if (!bool.booleanValue()) {
                String str = "MCHttpURLConnectionTask failed to " + HttpConnection.this.httpCommand + " - " + HttpConnection.this.urlString;
                Log.w("HttpConnection", str);
                if (HttpConnection.this.jniCall) {
                    HttpConnection.downloadFailed(HttpConnection.this.connectionID.intValue(), str, this.errorCode);
                }
                if (HttpConnection.this.listener != null) {
                    HttpConnection.this.listener.downloadFailed(HttpConnection.this.connectionID.intValue(), str);
                    return;
                }
                return;
            }
            HttpConnectionsManager.sharedInstance().removeConnection(this.owner.connectionID);
        }
    }

    public Integer getConnectionID() {
        return this.connectionID;
    }

    public boolean start() {
        if (this.canceled) {
            Log.w("HttpConnection", "Not starting connection with ID " + this.connectionID + " - cancel was already called.");
            return false;
        }
        Miniclip.queueEvent(ThreadingContext.AndroidUi, new Runnable() {
            public void run() {
                MCHttpURLConnectionTask unused = HttpConnection.this.connectionTask = new MCHttpURLConnectionTask();
                HttpConnection unused2 = HttpConnection.this.connectionTask.owner = HttpConnection.this;
                if (Build.VERSION.SDK_INT >= 11) {
                    HttpConnection.this.connectionTask.executeOnExecutor(HttpConnection.THREAD_POOL_EXECUTOR, HttpConnection.this.urlString);
                    return;
                }
                HttpConnection.this.connectionTask.execute(HttpConnection.this.urlString);
            }
        });
        return true;
    }

    public boolean cancel() {
        this.canceled = true;
        Miniclip.queueEvent(ThreadingContext.AndroidUi, new Runnable() {
            public void run() {
                if (HttpConnection.this.connectionTask == null) {
                    Log.w("HttpConnection", "Failed to cancel connection with ID " + HttpConnection.this.connectionID + ": connection not started.");
                    return;
                }
                HttpConnection.this.connectionTask.cancel(true);
                HttpConnectionsManager.sharedInstance().removeConnection(HttpConnection.this.connectionID);
            }
        });
        return true;
    }

    private static int newConnection(String str, String str2, String str3, int i, HashMap hashMap) {
        return new HttpConnection(str, str2, str3, null, i, hashMap).connectionID.intValue();
    }

    private static boolean startConnection(int i) {
        HttpConnection connection = HttpConnectionsManager.sharedInstance().getConnection(Integer.valueOf(i));
        if (connection == null) {
            Log.w("HttpConnection", "Failed to start connection: could not find connection with ID " + i + "!");
            return false;
        }
        connection.jniCall = true;
        return connection.start();
    }

    private static boolean cancelConnection(int i) {
        HttpConnection connection = HttpConnectionsManager.sharedInstance().getConnection(Integer.valueOf(i));
        if (connection == null) {
            return false;
        }
        return connection.cancel();
    }
}
