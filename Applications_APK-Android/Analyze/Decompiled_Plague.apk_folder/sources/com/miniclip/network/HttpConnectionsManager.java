package com.miniclip.network;

import android.content.Context;
import java.util.HashMap;

public class HttpConnectionsManager {
    protected static HttpConnectionsManager instance = new HttpConnectionsManager();
    protected HashMap<Integer, HttpConnection> connections = new HashMap<>();
    public Context context;
    protected Integer lastAssignedID = 0;

    protected HttpConnectionsManager() {
    }

    public static HttpConnectionsManager sharedInstance() {
        return instance;
    }

    public Integer addConnection(HttpConnection httpConnection) {
        this.lastAssignedID = Integer.valueOf(this.lastAssignedID.intValue() + 1);
        this.connections.put(this.lastAssignedID, httpConnection);
        return this.lastAssignedID;
    }

    public HttpConnection getConnection(Integer num) {
        return this.connections.get(num);
    }

    public void removeConnection(Integer num) {
        this.connections.remove(num);
    }

    public String stateDescription() {
        return "HttpConnectionsManager: currently holding " + this.connections.size() + " connections.\n" + this.connections.toString();
    }

    private void reset() {
        this.connections = new HashMap<>();
        this.lastAssignedID = 0;
    }
}
