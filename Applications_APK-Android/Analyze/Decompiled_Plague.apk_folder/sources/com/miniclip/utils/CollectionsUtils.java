package com.miniclip.utils;

import java.util.Collections;

public class CollectionsUtils {
    public static <T> Iterable<T> emptyIfNull(Iterable<T> iterable) {
        return iterable == null ? Collections.emptyList() : iterable;
    }

    public static <T> T[] emptyIfNull(T[] tArr) {
        return tArr == null ? (Object[]) new Object[0] : tArr;
    }
}
