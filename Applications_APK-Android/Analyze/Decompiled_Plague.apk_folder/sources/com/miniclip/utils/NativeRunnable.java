package com.miniclip.utils;

public class NativeRunnable implements Runnable {
    private long id = 0;

    private static native void runNative(long j);

    public NativeRunnable(long j) {
        this.id = j;
    }

    public void run() {
        runNative(this.id);
    }
}
