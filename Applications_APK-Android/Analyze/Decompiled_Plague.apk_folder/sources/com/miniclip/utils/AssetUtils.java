package com.miniclip.utils;

import com.miniclip.framework.Miniclip;

public class AssetUtils {
    public static boolean isDirectoryInApk(String str) {
        try {
            if (Miniclip.getActivity().getAssets().list(str).length > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0054 A[SYNTHETIC, Splitter:B:17:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] checksum(java.lang.String r4, boolean r5, java.lang.String r6) {
        /*
            r0 = 0
            r1 = 0
            if (r5 == 0) goto L_0x0015
            android.app.Activity r5 = com.miniclip.framework.Miniclip.getActivity()     // Catch:{ Exception -> 0x0011 }
            android.content.res.AssetManager r5 = r5.getAssets()     // Catch:{ Exception -> 0x0011 }
            java.io.InputStream r4 = r5.open(r4)     // Catch:{ Exception -> 0x0011 }
            goto L_0x001b
        L_0x0011:
            r4 = move-exception
            r5 = r4
            r4 = r0
            goto L_0x0037
        L_0x0015:
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0011 }
            r5.<init>(r4)     // Catch:{ Exception -> 0x0011 }
            r4 = r5
        L_0x001b:
            java.security.MessageDigest r5 = java.security.MessageDigest.getInstance(r6)     // Catch:{ Exception -> 0x0036 }
            r5.reset()     // Catch:{ Exception -> 0x0036 }
            r6 = 32768(0x8000, float:4.5918E-41)
            byte[] r2 = new byte[r6]     // Catch:{ Exception -> 0x0036 }
        L_0x0027:
            int r3 = r4.read(r2, r1, r6)     // Catch:{ Exception -> 0x0036 }
            if (r3 <= 0) goto L_0x0031
            r5.update(r2, r1, r3)     // Catch:{ Exception -> 0x0036 }
            goto L_0x0027
        L_0x0031:
            byte[] r5 = r5.digest()     // Catch:{ Exception -> 0x0036 }
            goto L_0x0052
        L_0x0036:
            r5 = move-exception
        L_0x0037:
            java.io.PrintStream r6 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "md5Checksum: "
            r2.append(r3)
            java.lang.String r5 = r5.getMessage()
            r2.append(r5)
            java.lang.String r5 = r2.toString()
            r6.println(r5)
            r5 = r0
        L_0x0052:
            if (r4 == 0) goto L_0x0073
            r4.close()     // Catch:{ Exception -> 0x0058 }
            goto L_0x0073
        L_0x0058:
            r4 = move-exception
            java.io.PrintStream r6 = java.lang.System.err
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "md5Checksum: "
            r0.append(r2)
            java.lang.String r4 = r4.getMessage()
            r0.append(r4)
            java.lang.String r4 = r0.toString()
            r6.println(r4)
        L_0x0073:
            if (r5 == 0) goto L_0x0076
            goto L_0x0078
        L_0x0076:
            byte[] r5 = new byte[r1]
        L_0x0078:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.miniclip.utils.AssetUtils.checksum(java.lang.String, boolean, java.lang.String):byte[]");
    }

    public static byte[] md5Checksum(String str, boolean z) {
        return checksum(str, z, "MD5");
    }
}
