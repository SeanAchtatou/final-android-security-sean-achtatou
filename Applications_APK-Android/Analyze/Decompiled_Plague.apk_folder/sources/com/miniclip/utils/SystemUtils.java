package com.miniclip.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GoogleApiAvailability;
import com.miniclip.framework.Miniclip;
import com.miniclip.framework.ThreadingContext;
import com.tapjoy.TapjoyConstants;

public class SystemUtils {
    private static boolean initialized = false;
    /* access modifiers changed from: private */
    public static String uniqueID = "";

    public static void init() {
        if (!initialized) {
            initialized = true;
            Miniclip.queueEvent(ThreadingContext.Main, new Runnable() {
                public void run() {
                    SystemUtils.fetchID();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static void fetchID() {
        final Activity activity = Miniclip.getActivity();
        if (activity == null) {
            Log.e("MCServicesUtils", "Failed to get context!");
        } else if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(activity) == 0) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        String unused = SystemUtils.uniqueID = AdvertisingIdClient.getAdvertisingIdInfo(activity).getId();
                    } catch (Exception e) {
                        Log.e("MCServicesUtils", "Failed to get advertising info!");
                        String unused2 = SystemUtils.uniqueID = "0";
                        e.printStackTrace();
                    }
                }
            }).start();
        } else {
            uniqueID = Settings.Secure.getString(activity.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
        }
    }

    public static boolean canOpenURL(Context context, String str, String str2) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        intent.setPackage(str2);
        return context.getPackageManager().resolveActivity(intent, 65536) != null;
    }

    public static boolean isAppInstalled(String str) {
        try {
            Miniclip.getActivity().getPackageManager().getPackageInfo(str, 1);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public static String deviceArchitecture() {
        return System.getProperty("os.arch");
    }

    public static String deviceID() {
        init();
        return uniqueID;
    }
}
