package com.miniclip.appsharing;

import android.content.Intent;
import com.miniclip.framework.Miniclip;

public class AppSharing {
    public static boolean shareText(String str) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.TEXT", str);
        intent.setType("text/plain");
        Miniclip.getActivity().startActivity(Intent.createChooser(intent, "Share to..."));
        return true;
    }
}
