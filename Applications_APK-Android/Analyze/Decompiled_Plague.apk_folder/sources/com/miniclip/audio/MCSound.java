package com.miniclip.audio;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;
import java.io.File;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MCSound implements SoundPool.OnLoadCompleteListener {
    private static int MAX_SIMULTANEOUS_STREAMS_DEFAULT = 10;
    public static final int SOUND_LOOP_TIME = 0;
    public static final int SOUND_PRIORITY = 1;
    private static final int SOUND_QUALITY = 5;
    public static final float SOUND_RATE = 1.0f;
    private static final String TAG = "MCSound";
    private static final Random soundIdGenerator = new Random();
    private ExecutorService loadSoundExecutor = Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */
    public Context mContext;
    public float mLeftVolume;
    public float mRightVolume;
    /* access modifiers changed from: private */
    public SoundPool mSoundPool;
    final Map<String, SoundInfo> pathToSoundInfo = new ConcurrentHashMap();
    final Map<Integer, SoundInfo> sampleIdToSoundInfo = new ConcurrentHashMap();
    final Map<Integer, SoundInfo> soundIdToSoundInfo = new ConcurrentHashMap();

    private class PlayEffectInfo {
        public float leftGain;
        public int loopTime;
        public float pitch;
        public int priority;
        public float rightGain;

        public PlayEffectInfo(float f, float f2, int i, int i2, float f3) {
            this.leftGain = f;
            this.rightGain = f2;
            this.priority = i;
            this.loopTime = i2;
            this.pitch = f3;
        }
    }

    private class SoundInfo {
        public boolean loaded;
        public PlayEffectInfo playOnLoad;
        public boolean playing;
        public int sampleId;
        public int soundId;
        public int streamId;

        private SoundInfo() {
            this.soundId = -1;
            this.loaded = false;
            this.playing = false;
            this.sampleId = -1;
            this.streamId = -1;
            this.playOnLoad = null;
        }
    }

    public MCSound(Context context) {
        this.mContext = context;
        if (Build.MODEL.equals("GT-I9100") && Build.VERSION.SDK_INT < 14) {
            Log.i(TAG, "The phone is a " + Build.MODEL + ": enabling single stream sound pool");
            MAX_SIMULTANEOUS_STREAMS_DEFAULT = 1;
        }
        this.mSoundPool = new SoundPool(MAX_SIMULTANEOUS_STREAMS_DEFAULT, 3, 5);
        this.mSoundPool.setOnLoadCompleteListener(this);
        this.mLeftVolume = 0.5f;
        this.mRightVolume = 0.5f;
    }

    public int preloadEffect(String str) {
        synchronized (this) {
            SoundInfo soundInfo = this.pathToSoundInfo.get(str);
            if (soundInfo != null) {
                int i = soundInfo.soundId;
                return i;
            }
            SoundInfo soundInfo2 = new SoundInfo();
            do {
                soundInfo2.soundId = soundIdGenerator.nextInt(Integer.MAX_VALUE);
            } while (this.soundIdToSoundInfo.containsKey(Integer.valueOf(soundInfo2.soundId)));
            this.pathToSoundInfo.put(str, soundInfo2);
            this.soundIdToSoundInfo.put(Integer.valueOf(soundInfo2.soundId), soundInfo2);
            Log.d(TAG, "preloadEffect: loading: " + str);
            loadSound(str, soundInfo2);
            return soundInfo2.soundId;
        }
    }

    private void loadSound(final String str, final SoundInfo soundInfo) {
        this.loadSoundExecutor.execute(new Runnable() {
            public void run() {
                try {
                    String findAssetPath = MCAudio.findAssetPath(str);
                    if (findAssetPath != null) {
                        Log.d(MCSound.TAG, "loadSound from asset: " + findAssetPath);
                        soundInfo.sampleId = MCSound.this.mSoundPool.load(MCSound.this.mContext.getAssets().openFd(findAssetPath), 0);
                        MCSound.this.sampleIdToSoundInfo.put(Integer.valueOf(soundInfo.sampleId), soundInfo);
                        return;
                    }
                    File file = new File(str);
                    if (!file.exists() || file.isDirectory()) {
                        Log.w(MCSound.TAG, "loadSound failed for path: " + str);
                        return;
                    }
                    Log.d(MCSound.TAG, "loadSound from file: " + str);
                    soundInfo.sampleId = MCSound.this.mSoundPool.load(str, 0);
                    MCSound.this.sampleIdToSoundInfo.put(Integer.valueOf(soundInfo.sampleId), soundInfo);
                } catch (Exception e) {
                    Log.e(MCSound.TAG, "loadSound caught an exception for path: " + str + " - " + e.getMessage(), e);
                }
            }
        });
    }

    public void unloadEffect(String str) {
        SoundInfo soundInfo;
        synchronized (this) {
            soundInfo = this.pathToSoundInfo.get(str);
            if (soundInfo != null) {
                if (soundInfo.loaded) {
                    this.pathToSoundInfo.remove(str);
                    this.soundIdToSoundInfo.remove(Integer.valueOf(soundInfo.soundId));
                    this.sampleIdToSoundInfo.remove(Integer.valueOf(soundInfo.sampleId));
                } else {
                    soundInfo.playOnLoad = null;
                }
            }
        }
        if (soundInfo == null) {
            Log.e(TAG, "unloadEffect: Unknown sound path: " + str);
        } else if (soundInfo.loaded) {
            Log.d(TAG, "unloadEffect: " + str);
            this.mSoundPool.unload(soundInfo.streamId);
        } else {
            Log.w(TAG, "unloadEffect: Unable to unload a sound (was not loaded yet): " + str);
        }
    }

    public void playEffect(int i, float f, float f2, int i2, int i3, float f3) {
        SoundInfo soundInfo = this.soundIdToSoundInfo.get(Integer.valueOf(i));
        if (soundInfo == null) {
            Log.e(TAG, "playEffect: Unknown sound id: " + i);
            return;
        }
        if (soundInfo.playing) {
            this.mSoundPool.stop(soundInfo.streamId);
            soundInfo.playing = false;
        }
        if (soundInfo.loaded) {
            soundInfo.streamId = this.mSoundPool.play(soundInfo.sampleId, f * this.mLeftVolume, f2 * this.mRightVolume, i2, i3, f3);
            soundInfo.playing = true;
            return;
        }
        soundInfo.playOnLoad = new PlayEffectInfo(f, f2, i2, i3, f3);
    }

    public void setEffectPitch(int i, float f) {
        SoundInfo soundInfo = this.soundIdToSoundInfo.get(Integer.valueOf(i));
        if (soundInfo == null) {
            Log.e(TAG, "setEffectPitch: Unknown sound id: " + i);
            return;
        }
        float f2 = 0.5f;
        if (f >= 0.5f) {
            f2 = f > 2.0f ? 2.0f : f;
        }
        if (soundInfo.playing) {
            this.mSoundPool.setRate(soundInfo.streamId, f2);
        } else if (soundInfo.playOnLoad != null) {
            soundInfo.playOnLoad.pitch = f2;
        }
    }

    public void pauseEffect(int i) {
        SoundInfo soundInfo = this.soundIdToSoundInfo.get(Integer.valueOf(i));
        if (soundInfo == null) {
            Log.e(TAG, "pauseEffect: Unknown sound id: " + i);
        } else if (soundInfo.playing) {
            this.mSoundPool.pause(soundInfo.streamId);
        } else if (soundInfo.playOnLoad != null) {
            soundInfo.playOnLoad = null;
        }
    }

    public void stopEffect(int i) {
        SoundInfo soundInfo = this.soundIdToSoundInfo.get(Integer.valueOf(i));
        if (soundInfo == null) {
            Log.e(TAG, "stopEffect: Unknown sound id: " + i);
        } else if (soundInfo.playing) {
            this.mSoundPool.stop(soundInfo.streamId);
            soundInfo.playing = false;
        } else if (soundInfo.playOnLoad != null) {
            soundInfo.playOnLoad = null;
        }
    }

    public void setEffectLooping(int i, boolean z) {
        SoundInfo soundInfo = this.soundIdToSoundInfo.get(Integer.valueOf(i));
        if (soundInfo == null) {
            Log.e(TAG, "setEffectLooping: Unknown sound id: " + i);
            return;
        }
        int i2 = 0;
        if (soundInfo.playing) {
            SoundPool soundPool = this.mSoundPool;
            int i3 = soundInfo.streamId;
            if (z) {
                i2 = -1;
            }
            soundPool.setLoop(i3, i2);
        } else if (soundInfo.playOnLoad != null) {
            PlayEffectInfo playEffectInfo = soundInfo.playOnLoad;
            if (z) {
                i2 = -1;
            }
            playEffectInfo.loopTime = i2;
        }
    }

    public void setEffectGain(int i, float f) {
        SoundInfo soundInfo = this.soundIdToSoundInfo.get(Integer.valueOf(i));
        if (soundInfo == null) {
            Log.e(TAG, "setEffectGain: Unknown sound id: " + i);
        } else if (soundInfo.playing) {
            this.mSoundPool.setVolume(soundInfo.streamId, this.mLeftVolume * f, f * this.mRightVolume);
        } else if (soundInfo.playOnLoad != null) {
            soundInfo.playOnLoad.leftGain = f;
            soundInfo.playOnLoad.rightGain = f;
        }
    }

    public float getEffectsVolume() {
        AudioManager audioManager = (AudioManager) this.mContext.getSystemService("audio");
        return ((float) audioManager.getStreamVolume(3)) / ((float) audioManager.getStreamMaxVolume(3));
    }

    public void setEffectsVolume(float f) {
        this.mRightVolume = f;
        this.mLeftVolume = f;
    }

    public void pauseAllSounds() {
        try {
            this.mSoundPool.autoPause();
        } catch (NoSuchMethodError e) {
            e.printStackTrace();
        }
    }

    public void resumeAllSounds() {
        try {
            this.mSoundPool.autoResume();
        } catch (NoSuchMethodError e) {
            e.printStackTrace();
        }
    }

    public void onLoadComplete(SoundPool soundPool, int i, int i2) {
        SoundInfo soundInfo = this.sampleIdToSoundInfo.get(Integer.valueOf(i));
        if (soundInfo == null) {
            Log.e(TAG, "onLoadComplete: Unknown sample id: " + i);
        } else if (i2 != 0) {
            Log.e(TAG, "onLoadComplete: Unable to load sound: " + soundInfo.soundId);
        } else {
            soundInfo.loaded = true;
            if (soundInfo.playOnLoad != null) {
                playEffect(soundInfo.soundId, soundInfo.playOnLoad.leftGain, soundInfo.playOnLoad.rightGain, soundInfo.playOnLoad.priority, soundInfo.playOnLoad.loopTime, soundInfo.playOnLoad.pitch);
            }
        }
    }
}
