package com.miniclip.audio;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

public class MCMusic implements MediaPlayer.OnPreparedListener {
    private static final String TAG = "MCMusic";
    private MediaPlayer mBackgroundMediaPlayer;
    private Context mContext;
    private String mCurrentPath;
    private boolean mIsGamePaused = false;
    private boolean mIsPaused;
    private boolean mIsPrepared = false;
    private float mLeftVolume;
    private boolean mPlayAfterPrepared = false;
    private float mRightVolume;

    public MCMusic(Context context) {
        this.mContext = context;
        initData();
    }

    private void initData() {
        this.mLeftVolume = 0.5f;
        this.mRightVolume = 0.5f;
        this.mBackgroundMediaPlayer = null;
        this.mIsPaused = false;
        this.mCurrentPath = null;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.mIsPrepared = true;
        Log.i(TAG, "music finished loading " + mediaPlayer);
        if (mediaPlayer == this.mBackgroundMediaPlayer && this.mPlayAfterPrepared) {
            Log.i(TAG, "music is going to play");
            this.mBackgroundMediaPlayer.start();
            this.mIsPaused = false;
        }
    }

    public void playBackgroundMusic(String str, boolean z) {
        Log.i("SoccerStarsActivity", "playbackgroundmusic with path " + str + " " + z);
        if (this.mCurrentPath == null) {
            this.mBackgroundMediaPlayer = createMediaplayerFromAssets(str, true);
            this.mCurrentPath = str;
        } else if (!this.mCurrentPath.equals(str)) {
            if (this.mBackgroundMediaPlayer != null) {
                this.mBackgroundMediaPlayer.release();
            }
            this.mBackgroundMediaPlayer = createMediaplayerFromAssets(str, true);
            this.mCurrentPath = str;
            Log.e(TAG, "playBackgroundMusic: new music");
        }
        if (this.mBackgroundMediaPlayer == null) {
            Log.e(TAG, "playBackgroundMusic: background media player is null");
        } else {
            this.mBackgroundMediaPlayer.setLooping(z);
            if (this.mIsPrepared) {
                this.mBackgroundMediaPlayer.seekTo(0);
                this.mBackgroundMediaPlayer.start();
            }
        }
        if (this.mIsGamePaused) {
            pauseBackgroundMusic();
        }
    }

    public void stopBackgroundMusic() {
        if (this.mBackgroundMediaPlayer != null) {
            this.mBackgroundMediaPlayer.stop();
            this.mIsPaused = false;
        }
    }

    public void pauseBackgroundMusic() {
        if (this.mBackgroundMediaPlayer != null && this.mBackgroundMediaPlayer.isPlaying()) {
            this.mBackgroundMediaPlayer.pause();
            this.mIsPaused = true;
        }
        this.mIsGamePaused = true;
    }

    public void resumeBackgroundMusic() {
        if (this.mBackgroundMediaPlayer != null && this.mIsPaused) {
            this.mBackgroundMediaPlayer.start();
            this.mIsPaused = false;
        }
        this.mIsGamePaused = false;
    }

    public void rewindBackgroundMusic() {
        if (this.mBackgroundMediaPlayer != null) {
            this.mBackgroundMediaPlayer.stop();
            try {
                this.mBackgroundMediaPlayer.seekTo(0);
                this.mBackgroundMediaPlayer.start();
                this.mIsPaused = false;
            } catch (Exception unused) {
                Log.e(TAG, "rewindBackgroundMusic: error state");
            }
        }
    }

    public boolean isBackgroundMusicPlaying() {
        return this.mBackgroundMediaPlayer != null && this.mBackgroundMediaPlayer.isPlaying();
    }

    public float getBackgroundVolume() {
        if (this.mBackgroundMediaPlayer != null) {
            return (this.mLeftVolume + this.mRightVolume) / 2.0f;
        }
        return 0.0f;
    }

    public void setBackgroundVolume(float f) {
        this.mRightVolume = f;
        this.mLeftVolume = f;
        if (this.mBackgroundMediaPlayer != null) {
            this.mBackgroundMediaPlayer.setVolume(this.mLeftVolume, this.mRightVolume);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        return r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
        r8 = new java.io.FileInputStream(new java.io.File(r8));
        r6.reset();
        r6.setDataSource(r8.getFD());
        prepareMusicOnMediaPlayer(r6);
        r6.setVolume(r7.mLeftVolume, r7.mRightVolume);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0067, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0068, code lost:
        android.util.Log.e(com.miniclip.audio.MCMusic.TAG, "error: " + r8.getMessage(), r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0048 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.media.MediaPlayer createMediaplayerFromAssets(java.lang.String r8, boolean r9) {
        /*
            r7 = this;
            android.media.MediaPlayer r6 = new android.media.MediaPlayer
            r6.<init>()
            r7.mPlayAfterPrepared = r9
            java.lang.String r9 = com.miniclip.audio.MCAudio.findAssetPath(r8)     // Catch:{ Exception -> 0x0048 }
            if (r9 == 0) goto L_0x0083
            java.lang.String r0 = "MCMusic"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0048 }
            r1.<init>()     // Catch:{ Exception -> 0x0048 }
            java.lang.String r2 = "createMediaplayerFromAssets: asset: "
            r1.append(r2)     // Catch:{ Exception -> 0x0048 }
            r1.append(r9)     // Catch:{ Exception -> 0x0048 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0048 }
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x0048 }
            android.content.Context r0 = r7.mContext     // Catch:{ Exception -> 0x0048 }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ Exception -> 0x0048 }
            android.content.res.AssetFileDescriptor r9 = r0.openFd(r9)     // Catch:{ Exception -> 0x0048 }
            java.io.FileDescriptor r1 = r9.getFileDescriptor()     // Catch:{ Exception -> 0x0048 }
            long r2 = r9.getStartOffset()     // Catch:{ Exception -> 0x0048 }
            long r4 = r9.getLength()     // Catch:{ Exception -> 0x0048 }
            r0 = r6
            r0.setDataSource(r1, r2, r4)     // Catch:{ Exception -> 0x0048 }
            float r9 = r7.mLeftVolume     // Catch:{ Exception -> 0x0048 }
            float r0 = r7.mRightVolume     // Catch:{ Exception -> 0x0048 }
            r6.setVolume(r9, r0)     // Catch:{ Exception -> 0x0048 }
            r7.prepareMusicOnMediaPlayer(r6)     // Catch:{ Exception -> 0x0048 }
            goto L_0x0083
        L_0x0048:
            java.io.File r9 = new java.io.File     // Catch:{ Exception -> 0x0067 }
            r9.<init>(r8)     // Catch:{ Exception -> 0x0067 }
            java.io.FileInputStream r8 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0067 }
            r8.<init>(r9)     // Catch:{ Exception -> 0x0067 }
            r6.reset()     // Catch:{ Exception -> 0x0067 }
            java.io.FileDescriptor r8 = r8.getFD()     // Catch:{ Exception -> 0x0067 }
            r6.setDataSource(r8)     // Catch:{ Exception -> 0x0067 }
            r7.prepareMusicOnMediaPlayer(r6)     // Catch:{ Exception -> 0x0067 }
            float r8 = r7.mLeftVolume     // Catch:{ Exception -> 0x0067 }
            float r9 = r7.mRightVolume     // Catch:{ Exception -> 0x0067 }
            r6.setVolume(r8, r9)     // Catch:{ Exception -> 0x0067 }
            goto L_0x0083
        L_0x0067:
            r8 = move-exception
            r6 = 0
            java.lang.String r9 = "MCMusic"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "error: "
            r0.append(r1)
            java.lang.String r1 = r8.getMessage()
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r9, r0, r8)
        L_0x0083:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.miniclip.audio.MCMusic.createMediaplayerFromAssets(java.lang.String, boolean):android.media.MediaPlayer");
    }

    /* access modifiers changed from: package-private */
    public void prepareMusicOnMediaPlayer(MediaPlayer mediaPlayer) {
        if (this.mPlayAfterPrepared) {
            mediaPlayer.setOnPreparedListener(this);
            this.mIsPrepared = false;
            mediaPlayer.prepareAsync();
            return;
        }
        try {
            mediaPlayer.prepare();
        } catch (Exception e) {
            Log.e(TAG, "error: " + e.getMessage(), e);
        }
    }

    public int getDurationForSound(String str) {
        MediaPlayer createMediaplayerFromAssets = createMediaplayerFromAssets(str, false);
        if (createMediaplayerFromAssets == null) {
            return -1;
        }
        int duration = createMediaplayerFromAssets.getDuration();
        createMediaplayerFromAssets.release();
        return duration;
    }
}
