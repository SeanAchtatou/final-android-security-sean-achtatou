package com.miniclip.audio;

import com.miniclip.framework.AbstractActivityListener;
import com.miniclip.framework.MiniclipAndroidActivity;

public class MCAudio {
    private static final String TAG = "MCAudio";
    private static MiniclipAndroidActivity activity = null;
    private static MCMusic backgroundMusicPlayer = null;
    private static boolean isInitialized = false;
    private static MCSound soundPlayer;

    private static class MCAudioActivityListener extends AbstractActivityListener {
        private boolean hasWindowFocus;
        private boolean resumeOnFocus;

        private MCAudioActivityListener() {
            this.hasWindowFocus = false;
            this.resumeOnFocus = false;
        }

        public void onPause() {
            MCAudio.pauseBackgroundMusic();
            MCAudio.pauseAllSounds();
        }

        public void onWindowFocusChanged(boolean z) {
            this.hasWindowFocus = z;
            if (this.hasWindowFocus && this.resumeOnFocus) {
                this.resumeOnFocus = false;
                MCAudio.resumeBackgroundMusic();
                if (MCAudio.isInitialized()) {
                    MCAudio.resumeAllSounds();
                }
            }
        }

        public void onResume() {
            if (this.hasWindowFocus) {
                this.resumeOnFocus = false;
                MCAudio.resumeBackgroundMusic();
                MCAudio.resumeAllSounds();
                return;
            }
            this.resumeOnFocus = true;
        }
    }

    public static void init(MiniclipAndroidActivity miniclipAndroidActivity) {
        if (!isInitialized) {
            activity = miniclipAndroidActivity;
            backgroundMusicPlayer = new MCMusic(activity);
            soundPlayer = new MCSound(activity);
            activity.addListener(new MCAudioActivityListener());
            isInitialized = true;
        }
    }

    public static boolean isInitialized() {
        return isInitialized;
    }

    public static int getDurationForSound(String str) {
        return backgroundMusicPlayer.getDurationForSound(str);
    }

    public static void playBackgroundMusic(String str, boolean z) {
        backgroundMusicPlayer.playBackgroundMusic(str, z);
    }

    public static void stopBackgroundMusic() {
        backgroundMusicPlayer.stopBackgroundMusic();
    }

    public static void pauseBackgroundMusic() {
        backgroundMusicPlayer.pauseBackgroundMusic();
    }

    public static void resumeBackgroundMusic() {
        backgroundMusicPlayer.resumeBackgroundMusic();
    }

    public static void rewindBackgroundMusic() {
        backgroundMusicPlayer.rewindBackgroundMusic();
    }

    public static boolean isBackgroundMusicPlaying() {
        return backgroundMusicPlayer.isBackgroundMusicPlaying();
    }

    public static float getBackgroundMusicVolume() {
        return backgroundMusicPlayer.getBackgroundVolume();
    }

    public static void setBackgroundMusicVolume(float f) {
        backgroundMusicPlayer.setBackgroundVolume(f);
    }

    public static void playEffect(int i) {
        soundPlayer.playEffect(i, soundPlayer.mLeftVolume, soundPlayer.mRightVolume, 1, 0, 1.0f);
    }

    public static void playEffect(int i, int i2, float f) {
        soundPlayer.playEffect(i, soundPlayer.mLeftVolume, soundPlayer.mRightVolume, 1, i2, f);
    }

    public static void playEffect(int i, int i2, int i3, float f) {
        soundPlayer.playEffect(i, soundPlayer.mLeftVolume, soundPlayer.mRightVolume, i2, i3, f);
    }

    public static void playEffect(int i, float f, float f2, int i2, float f3) {
        soundPlayer.playEffect(i, f, f2, 1, i2, f3);
    }

    public static void playEffect(int i, float f, float f2, int i2, int i3, float f3) {
        soundPlayer.playEffect(i, f, f2, i2, i3, f3);
    }

    public static void pauseEffect(int i) {
        soundPlayer.pauseEffect(i);
    }

    public static void stopEffect(int i) {
        soundPlayer.stopEffect(i);
    }

    public static void setEffectLooping(int i, boolean z) {
        soundPlayer.setEffectLooping(i, z);
    }

    public static void setEffectGain(int i, float f) {
        soundPlayer.setEffectGain(i, f);
    }

    public static void setEffectPitch(int i, float f) {
        soundPlayer.setEffectPitch(i, f);
    }

    public static float getEffectsVolume() {
        return soundPlayer.getEffectsVolume();
    }

    public static void setEffectsVolume(float f) {
        soundPlayer.setEffectsVolume(f);
    }

    public static int preloadEffect(String str) {
        return soundPlayer.preloadEffect(str);
    }

    public static void unloadEffect(String str) {
        soundPlayer.unloadEffect(str);
    }

    public static void pauseAllSounds() {
        soundPlayer.pauseAllSounds();
    }

    public static void resumeAllSounds() {
        soundPlayer.resumeAllSounds();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:3|4|5) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0077, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0078, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:?, code lost:
        r0 = "unpack/" + new java.io.File(r3).getName();
        com.miniclip.audio.MCAudio.activity.getAssets().openFd(r0).close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x003e, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r3 = "unpack/" + new java.io.File(r3.substring(0, r3.length() - 3).concat("wav")).getName();
        com.miniclip.audio.MCAudio.activity.getAssets().openFd(r3).close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0017 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String findAssetPath(java.lang.String r3) {
        /*
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0017 }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0017 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x0017 }
            com.miniclip.framework.MiniclipAndroidActivity r1 = com.miniclip.audio.MCAudio.activity     // Catch:{ Exception -> 0x0017 }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ Exception -> 0x0017 }
            android.content.res.AssetFileDescriptor r1 = r1.openFd(r0)     // Catch:{ Exception -> 0x0017 }
            r1.close()     // Catch:{ Exception -> 0x0017 }
            return r0
        L_0x0017:
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x003f }
            r0.<init>(r3)     // Catch:{ Exception -> 0x003f }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x003f }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003f }
            r1.<init>()     // Catch:{ Exception -> 0x003f }
            java.lang.String r2 = "unpack/"
            r1.append(r2)     // Catch:{ Exception -> 0x003f }
            r1.append(r0)     // Catch:{ Exception -> 0x003f }
            java.lang.String r0 = r1.toString()     // Catch:{ Exception -> 0x003f }
            com.miniclip.framework.MiniclipAndroidActivity r1 = com.miniclip.audio.MCAudio.activity     // Catch:{ Exception -> 0x003f }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ Exception -> 0x003f }
            android.content.res.AssetFileDescriptor r1 = r1.openFd(r0)     // Catch:{ Exception -> 0x003f }
            r1.close()     // Catch:{ Exception -> 0x003f }
            return r0
        L_0x003f:
            r0 = 0
            int r1 = r3.length()     // Catch:{ Exception -> 0x0078 }
            int r1 = r1 + -3
            java.lang.String r3 = r3.substring(r0, r1)     // Catch:{ Exception -> 0x0078 }
            java.lang.String r0 = "wav"
            java.lang.String r3 = r3.concat(r0)     // Catch:{ Exception -> 0x0078 }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0078 }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0078 }
            java.lang.String r3 = r0.getName()     // Catch:{ Exception -> 0x0078 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0078 }
            r0.<init>()     // Catch:{ Exception -> 0x0078 }
            java.lang.String r1 = "unpack/"
            r0.append(r1)     // Catch:{ Exception -> 0x0078 }
            r0.append(r3)     // Catch:{ Exception -> 0x0078 }
            java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x0078 }
            com.miniclip.framework.MiniclipAndroidActivity r0 = com.miniclip.audio.MCAudio.activity     // Catch:{ Exception -> 0x0078 }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ Exception -> 0x0078 }
            android.content.res.AssetFileDescriptor r0 = r0.openFd(r3)     // Catch:{ Exception -> 0x0078 }
            r0.close()     // Catch:{ Exception -> 0x0078 }
            return r3
        L_0x0078:
            r3 = 0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.miniclip.audio.MCAudio.findAssetPath(java.lang.String):java.lang.String");
    }
}
