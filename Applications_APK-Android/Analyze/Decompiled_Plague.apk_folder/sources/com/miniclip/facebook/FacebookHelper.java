package com.miniclip.facebook;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionDefaultAudience;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.model.GraphObject;
import com.facebook.widget.WebDialog;
import com.miniclip.framework.AbstractActivityListener;
import com.miniclip.framework.MiniclipAndroidActivity;
import com.miniclip.framework.ThreadingContext;
import java.util.Arrays;
import java.util.List;

public class FacebookHelper {
    /* access modifiers changed from: private */
    public static MiniclipAndroidActivity activity = null;
    private static boolean isInitialized = false;
    protected static Context mContext = null;
    /* access modifiers changed from: private */
    public static boolean mFB_AuthenticationRequested = false;
    public static Session mFB_Session = null;
    /* access modifiers changed from: private */
    public static Handler mFacebookHandler = new Handler();
    public static SharedPreferences mPrefs = null;
    public static boolean mUSE_FACEBOOK = true;

    public static native void MfacebookCheckRequestsComplete(int i, int i2, byte[] bArr);

    public static native void MfacebookLikeComplete();

    public static native void MfacebookLoginComplete();

    public static native void MfacebookRequestComplete(int i, int i2, byte[] bArr);

    public static native void MfacebookShareCanceled();

    public static native void MfacebookShareComplete();

    private static class FacebookHelperActivityListener extends AbstractActivityListener {
        private FacebookHelperActivityListener() {
        }

        public void onActivityResult(int i, int i2, Intent intent) {
            super.onActivityResult(i, i2, intent);
            if (FacebookHelper.mUSE_FACEBOOK) {
                FacebookHelper.mFB_Session.onActivityResult(FacebookHelper.activity, i, i2, intent);
            }
        }
    }

    public static void init(MiniclipAndroidActivity miniclipAndroidActivity) {
        if (!isInitialized) {
            activity = miniclipAndroidActivity;
            mContext = activity;
            activity.addListener(new FacebookHelperActivityListener());
            if (mUSE_FACEBOOK) {
                mFB_Session = Session.openActiveSessionFromCache(mContext);
                if (mFB_Session == null) {
                    Log.i("cocojava", "FB: Cannot open session from cache");
                    mFB_Session = new Session(mContext);
                }
            }
        }
    }

    public static void SharedPreferences_setInt(String str, int i) {
        SharedPreferences.Editor edit = ((Activity) mContext).getSharedPreferences("GAME_INFO", 0).edit();
        edit.putInt(str, i);
        edit.commit();
    }

    public static int SharedPreferences_getInt(String str) {
        return ((Activity) mContext).getSharedPreferences("GAME_INFO", 0).getInt(str, 0);
    }

    public static void SharedPreferences_setString(String str, String str2) {
        SharedPreferences.Editor edit = ((Activity) mContext).getSharedPreferences("GAME_INFO", 0).edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public static String SharedPreferences_getString(String str) {
        return ((Activity) mContext).getSharedPreferences("GAME_INFO", 0).getString(str, "");
    }

    public static int faceBook_isSessionValid() {
        if (!mUSE_FACEBOOK) {
            return 0;
        }
        return mFB_Session.isOpened() ? 1 : 0;
    }

    public static String faceBook_getAccessToken() {
        if (!mUSE_FACEBOOK) {
            return "";
        }
        return mFB_Session.getAccessToken();
    }

    public static void faceBook_logout() {
        if (mUSE_FACEBOOK) {
            mFB_Session.closeAndClearTokenInformation();
            Session.setActiveSession(null);
        }
    }

    public static void faceBook_authorize(String str) {
        faceBook_authorizeAndRun(str, new Runnable() {
            public void run() {
            }
        });
    }

    public static void faceBook_authorizeAndRun(String str, final Runnable runnable) {
        if (mUSE_FACEBOOK) {
            mFB_AuthenticationRequested = true;
            mFB_Session = new Session(mContext);
            mPrefs = ((Activity) mContext).getPreferences(0);
            String string = mPrefs.getString("access_token", null);
            if (string == null) {
                final String[] split = str.split(",");
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    public void run() {
                        Session.OpenRequest callback;
                        if (FacebookHelper.mFB_Session != null && !FacebookHelper.mFB_Session.isOpened() && (callback = new Session.OpenRequest((Activity) FacebookHelper.mContext).setCallback((Session.StatusCallback) new Session.StatusCallback() {
                            public void call(Session session, SessionState sessionState, Exception exc) {
                                if (FacebookHelper.mFB_AuthenticationRequested && sessionState.isOpened()) {
                                    FacebookHelper.activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
                                        public void run() {
                                            boolean unused = FacebookHelper.mFB_AuthenticationRequested = false;
                                            FacebookHelper.MfacebookLoginComplete();
                                        }
                                    });
                                    FacebookHelper.mFacebookHandler.postDelayed(runnable, 500);
                                }
                            }
                        })) != null) {
                            callback.setDefaultAudience(SessionDefaultAudience.FRIENDS);
                            callback.setPermissions((List<String>) Arrays.asList(split));
                            callback.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
                            FacebookHelper.mFB_Session.openForRead(callback);
                            Session.setActiveSession(FacebookHelper.mFB_Session);
                        }
                    }
                });
                return;
            }
            SharedPreferences.Editor edit = mPrefs.edit();
            edit.putString("access_token", null);
            edit.commit();
            final AccessToken createFromExistingAccessToken = AccessToken.createFromExistingAccessToken(string, null, null, null, null);
            ((Activity) mContext).runOnUiThread(new Runnable() {
                public void run() {
                    FacebookHelper.mFB_Session.open(createFromExistingAccessToken, new Session.StatusCallback() {
                        public void call(Session session, SessionState sessionState, Exception exc) {
                        }
                    });
                    Session.setActiveSession(FacebookHelper.mFB_Session);
                    FacebookHelper.mFacebookHandler.postDelayed(runnable, 500);
                }
            });
        }
    }

    public static void faceBook_request(final String str, final int i) {
        if (mUSE_FACEBOOK) {
            ((Activity) mContext).runOnUiThread(new Runnable() {
                public void run() {
                    Request.executeGraphPathRequestAsync(FacebookHelper.mFB_Session, str, new Request.Callback() {
                        public void onCompleted(Response response) {
                            GraphObject graphObject = response.getGraphObject();
                            if (graphObject != null && graphObject.getInnerJSONObject() != null) {
                                final String jSONObject = graphObject.getInnerJSONObject().toString();
                                Log.i("Facebook: request: response: ", jSONObject);
                                FacebookHelper.activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
                                    public void run() {
                                        FacebookHelper.MfacebookRequestComplete(i, jSONObject.getBytes().length, jSONObject.getBytes());
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    }

    public static void faceBook_reauthorizeWithPublishPermissions(String str) {
        if (mUSE_FACEBOOK) {
            final String[] split = str.split(",");
            ((Activity) mContext).runOnUiThread(new Runnable() {
                public void run() {
                    if (FacebookHelper.mFB_Session != null && FacebookHelper.mFB_Session.getState().isOpened()) {
                        FacebookHelper.mFB_Session.requestNewPublishPermissions(new Session.NewPermissionsRequest((Activity) FacebookHelper.mContext, Arrays.asList(split)));
                    }
                }
            });
        }
    }

    public static void faceBook_sendRequest(final String str, String str2, String str3) {
        if (mUSE_FACEBOOK) {
            Log.i("FACEBOOK", "faceBook_send");
            final Bundle bundle = new Bundle();
            String[] split = str2.split("/;#");
            for (int i = 0; i < split.length / 2; i++) {
                int i2 = i * 2;
                bundle.putString(split[i2], split[i2 + 1]);
            }
            if (mFB_Session == null) {
                return;
            }
            if (!mFB_Session.getPermissions().contains("publish_actions")) {
                mFB_Session.requestNewPublishPermissions(new Session.NewPermissionsRequest((Activity) mContext, Arrays.asList("publish_actions")));
                return;
            }
            ((Activity) mContext).runOnUiThread(new Runnable() {
                public void run() {
                    new RequestAsyncTask(new Request(FacebookHelper.mFB_Session, str, bundle, HttpMethod.POST, new Request.Callback() {
                        public void onCompleted(Response response) {
                            Log.i("Facebook", "onComplete faceBook_sendRequest");
                        }
                    })).execute(new Void[0]);
                }
            });
        }
    }

    public static int faceBook_hasPermission(String str) {
        if (!mUSE_FACEBOOK) {
            return 0;
        }
        return mFB_Session.getPermissions().contains(str) ? 1 : 0;
    }

    public static void faceBook_dialogWithLogin(final String str, final String str2) {
        faceBook_authorizeAndRun("email,user_birthday", new Runnable() {
            public void run() {
                FacebookHelper.faceBook_dialog(str, str2);
            }
        });
    }

    public static void faceBook_dialog(final String str, String str2) {
        if (mUSE_FACEBOOK) {
            Log.i("FACEBOOK", "faceBook_dialog");
            final Bundle bundle = new Bundle();
            String[] split = str2.split("/;#");
            for (int i = 0; i < split.length / 2; i++) {
                int i2 = i * 2;
                bundle.putString(split[i2], split[i2 + 1]);
            }
            ((Activity) mContext).runOnUiThread(new Runnable() {
                public void run() {
                    ((WebDialog.Builder) new WebDialog.Builder((Activity) FacebookHelper.mContext, FacebookHelper.mFB_Session, str, bundle).setOnCompleteListener(new WebDialog.OnCompleteListener() {
                        public void onComplete(Bundle bundle, FacebookException facebookException) {
                            boolean z = false;
                            if (facebookException != null) {
                                boolean z2 = facebookException instanceof FacebookOperationCanceledException;
                            } else if (!(bundle.getString("request") == null && bundle.getString("post_id") == null)) {
                                z = true;
                            }
                            if (z) {
                                FacebookHelper.activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
                                    public void run() {
                                        FacebookHelper.MfacebookShareComplete();
                                    }
                                });
                            } else {
                                FacebookHelper.activity.queueEvent(ThreadingContext.GlThread, new Runnable() {
                                    public void run() {
                                        FacebookHelper.MfacebookShareCanceled();
                                    }
                                });
                            }
                        }
                    })).build().show();
                }
            });
        }
    }

    public static void faceBook_checkReceivedRequests(int i) {
        String SharedPreferences_getString = SharedPreferences_getString("FACEBOOK_RESQUESTIDS");
        if (SharedPreferences_getString == null || SharedPreferences_getString == "") {
            Log.i("Facebook", "No requestIds");
            return;
        }
        Log.i("Facebook", "Cached Request Ids: " + SharedPreferences_getString);
        if (mFB_Session == null || !mFB_Session.isOpened()) {
            Log.i("Facebook", "Facebook session closed");
            return;
        }
        Log.i("Facebook", "Facebook session opened : fetching requests");
        SharedPreferences_setString("FACEBOOK_RESQUESTIDS", "");
        for (String requestData : SharedPreferences_getString.split(",")) {
            getRequestData(requestData, i);
        }
    }

    private static void getRequestData(final String str, final int i) {
        ((Activity) mContext).runOnUiThread(new Runnable() {
            public void run() {
                Request.executeBatchAsync(new Request(FacebookHelper.mFB_Session, str, null, HttpMethod.GET, new Request.Callback() {
                    /* JADX WARNING: Removed duplicated region for block: B:14:0x005e  */
                    /* JADX WARNING: Removed duplicated region for block: B:16:? A[RETURN, SYNTHETIC] */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void onCompleted(com.facebook.Response r6) {
                        /*
                            r5 = this;
                            com.facebook.model.GraphObject r0 = r6.getGraphObject()
                            com.facebook.FacebookRequestError r6 = r6.getError()
                            java.lang.String r1 = "Incoming request"
                            r2 = 1
                            if (r0 == 0) goto L_0x0056
                            java.lang.String r3 = "data"
                            java.lang.Object r3 = r0.getProperty(r3)
                            if (r3 == 0) goto L_0x0051
                            java.lang.String r6 = "data"
                            java.lang.Object r6 = r0.getProperty(r6)     // Catch:{ Exception -> 0x004e }
                            java.lang.String r6 = (java.lang.String) r6     // Catch:{ Exception -> 0x004e }
                            java.lang.String r0 = "Facebook"
                            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004e }
                            r3.<init>()     // Catch:{ Exception -> 0x004e }
                            java.lang.String r4 = "Resquest: "
                            r3.append(r4)     // Catch:{ Exception -> 0x004e }
                            com.miniclip.facebook.FacebookHelper$9 r4 = com.miniclip.facebook.FacebookHelper.AnonymousClass9.this     // Catch:{ Exception -> 0x004e }
                            java.lang.String r4 = r2     // Catch:{ Exception -> 0x004e }
                            r3.append(r4)     // Catch:{ Exception -> 0x004e }
                            java.lang.String r4 = " data: "
                            r3.append(r4)     // Catch:{ Exception -> 0x004e }
                            r3.append(r6)     // Catch:{ Exception -> 0x004e }
                            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x004e }
                            android.util.Log.i(r0, r3)     // Catch:{ Exception -> 0x004e }
                            com.miniclip.framework.MiniclipAndroidActivity r0 = com.miniclip.facebook.FacebookHelper.activity     // Catch:{ Exception -> 0x004e }
                            com.miniclip.framework.ThreadingContext r3 = com.miniclip.framework.ThreadingContext.GlThread     // Catch:{ Exception -> 0x004e }
                            com.miniclip.facebook.FacebookHelper$9$1$1 r4 = new com.miniclip.facebook.FacebookHelper$9$1$1     // Catch:{ Exception -> 0x004e }
                            r4.<init>(r6)     // Catch:{ Exception -> 0x004e }
                            r0.queueEvent(r3, r4)     // Catch:{ Exception -> 0x004e }
                            goto L_0x0056
                        L_0x004e:
                            java.lang.String r1 = "Error getting request info"
                            goto L_0x0057
                        L_0x0051:
                            if (r6 == 0) goto L_0x0056
                            java.lang.String r1 = "Error getting request info"
                            goto L_0x0057
                        L_0x0056:
                            r2 = 0
                        L_0x0057:
                            java.lang.String r6 = "Facebook"
                            android.util.Log.i(r6, r1)
                            if (r2 != 0) goto L_0x0065
                            com.miniclip.facebook.FacebookHelper$9 r6 = com.miniclip.facebook.FacebookHelper.AnonymousClass9.this
                            java.lang.String r6 = r2
                            com.miniclip.facebook.FacebookHelper.deleteRequest(r6)
                        L_0x0065:
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.miniclip.facebook.FacebookHelper.AnonymousClass9.AnonymousClass1.onCompleted(com.facebook.Response):void");
                    }
                }));
            }
        });
    }

    /* access modifiers changed from: private */
    public static void deleteRequest(final String str) {
        ((Activity) mContext).runOnUiThread(new Runnable() {
            public void run() {
                Request.executeBatchAsync(new Request(FacebookHelper.mFB_Session, str, null, HttpMethod.DELETE, new Request.Callback() {
                    public void onCompleted(Response response) {
                        Log.i("Facebook", "Request Deleted: " + str);
                    }
                }));
            }
        });
    }

    public static void showHtmlDialog(final String str, final int i) {
        ((Activity) mContext).runOnUiThread(new Runnable() {
            public void run() {
                new HtmlDialog(FacebookHelper.mContext, str, i, new Facebook.DialogListener() {
                    public void onCancel() {
                    }

                    public void onComplete(Bundle bundle) {
                    }

                    public void onError(DialogError dialogError) {
                    }

                    public void onFacebookError(FacebookError facebookError) {
                    }
                }).show();
            }
        });
    }
}
