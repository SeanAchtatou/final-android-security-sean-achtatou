package com.miniclip.platform;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import com.miniclip.framework.AbstractActivityListener;
import com.miniclip.framework.Miniclip;
import com.miniclip.framework.ThreadingContext;
import java.io.File;
import java.io.FileFilter;
import java.util.Locale;
import java.util.regex.Pattern;

public class MCApplication extends AbstractActivityListener {
    public static final int FATAL_ERROR_INITIALIZATION = 1003;
    public static final int FATAL_ERROR_LIBRARY_LOAD = 1002;
    public static final int FATAL_ERROR_LOW_STORAGE = 1005;
    public static final int FATAL_ERROR_MULTIDEX_INSTALL = 1001;
    public static final int FATAL_ERROR_RESOURCE_LOADING = 1004;
    /* access modifiers changed from: private */
    public static Activity MCActivity = null;
    private static final String TAG = "MCApplication";
    /* access modifiers changed from: private */
    public static String currentIntentDataString = null;
    private static int fatalErrorCode = 0;
    private static boolean fatalErrorOccurred = false;
    private static boolean isInitialized = false;
    private static int numMemoryWarnings;
    private static RequestSelfPermissionCallback requestSelfPermissionResultCallback;
    /* access modifiers changed from: private */
    public static long requestSelfPermissionResultCallbackNative;

    public interface RequestSelfPermissionCallback {
        void response(boolean z);
    }

    /* access modifiers changed from: private */
    public static native void nativeLowMemory();

    /* access modifiers changed from: private */
    public static native void nativeNewIntentDataString(String str);

    private static native void nativeSetApplicationVersionNumber(String str);

    private static native void nativeSetCountryCode(String str);

    private static native void nativeSetLanguage(String str);

    /* access modifiers changed from: private */
    public static native void onRequestSelfPermissionResult(long j, boolean z);

    public static void init() {
        if (!isInitialized) {
            isInitialized = true;
            MCActivity = Miniclip.getActivity();
            currentIntentDataString = MCActivity.getIntent().getDataString();
            Miniclip.addListener(new MCApplication());
        }
    }

    public static String getApplicationVersionNumber() {
        try {
            PackageInfo packageInfo = MCActivity.getPackageManager().getPackageInfo(MCActivity.getPackageName(), 0);
            if (packageInfo != null) {
                return packageInfo.versionName;
            }
            return "";
        } catch (PackageManager.NameNotFoundException unused) {
            return "";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static void firstRun() {
        nativeSetCountryCode(Locale.getDefault().getCountry());
        if (Build.VERSION.SDK_INT >= 21) {
            nativeSetLanguage(Locale.getDefault().toLanguageTag());
        } else {
            nativeSetLanguage(Locale.getDefault().toString().replace('_', '-'));
        }
        nativeSetApplicationVersionNumber(getApplicationVersionNumber());
    }

    public static String getApplicationName() {
        return MCActivity.getString(MCActivity.getApplicationInfo().labelRes);
    }

    public static String getApplicationPackageName() {
        return MCActivity.getPackageName();
    }

    public static void signalFatalError(int i) {
        if (!fatalErrorOccurred) {
            fatalErrorOccurred = true;
            fatalErrorCode = i;
        }
    }

    public static int getFatalErrorCode() {
        if (fatalErrorOccurred) {
            return fatalErrorCode;
        }
        return 0;
    }

    public static boolean hasFatalErrorOccurred() {
        return fatalErrorOccurred;
    }

    public static int getCpuCoreCount() {
        try {
            return new File("/sys/devices/system/cpu/").listFiles(new FileFilter() {
                public boolean accept(File file) {
                    return Pattern.matches("cpu[0-9]", file.getName());
                }
            }).length;
        } catch (Exception unused) {
            return 1;
        }
    }

    public static int getApplicationVersionCode() {
        try {
            PackageInfo packageInfo = MCActivity.getPackageManager().getPackageInfo(MCActivity.getPackageName(), 0);
            if (packageInfo != null) {
                return packageInfo.versionCode;
            }
            return 0;
        } catch (PackageManager.NameNotFoundException unused) {
            return 0;
        }
    }

    public static String getStringValWithKeyFromMetadata(String str) {
        try {
            Bundle bundle = MCActivity.getPackageManager().getApplicationInfo(MCActivity.getPackageName(), 128).metaData;
            if (bundle != null) {
                return bundle.getString(str);
            }
            return null;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    public static String getIntentDataString() {
        return currentIntentDataString != null ? currentIntentDataString : "";
    }

    public static boolean isLargeScreen() {
        int i = MCActivity.getResources().getConfiguration().screenLayout & 15;
        return i == 3 || i == 4;
    }

    public static float getScreenDensity() {
        return MCActivity.getResources().getDisplayMetrics().density;
    }

    public static float getScreenDPI() {
        return MCActivity.getResources().getDisplayMetrics().ydpi;
    }

    @SuppressLint({"InlinedApi"})
    private static int getCurrentOrientation() {
        Display defaultDisplay = ((WindowManager) MCActivity.getSystemService("window")).getDefaultDisplay();
        if (Build.VERSION.SDK_INT <= 8) {
            return 0;
        }
        switch (defaultDisplay.getRotation()) {
            case 0:
            case 1:
            default:
                return 0;
            case 2:
            case 3:
                return 8;
        }
    }

    @SuppressLint({"InlinedApi"})
    public static void setAutoRotate(int i) {
        int i2;
        if (Build.VERSION.SDK_INT > 8) {
            i2 = i == 1 ? 6 : getCurrentOrientation();
        } else {
            i2 = 0;
        }
        MCActivity.setRequestedOrientation(i2);
    }

    public static boolean isDeviceOnline() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) MCActivity.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        return activeNetworkInfo.isConnectedOrConnecting();
    }

    public static boolean isDeviceRooted() {
        for (String str : new String[]{"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"}) {
            if (new File(str + "su").exists()) {
                return true;
            }
        }
        return false;
    }

    public static void requestSelfPermission(String str, String str2, RequestSelfPermissionCallback requestSelfPermissionCallback) {
        requestSelfPermission(str, str2, 0, requestSelfPermissionCallback);
    }

    public static void requestSelfPermission(String str, String str2, long j) {
        requestSelfPermission(str, str2, j, null);
    }

    public static void requestSelfPermission(final String str, final String str2, long j, RequestSelfPermissionCallback requestSelfPermissionCallback) {
        if (requestSelfPermissionResultCallback == null && requestSelfPermissionResultCallbackNative == 0) {
            requestSelfPermissionResultCallback = requestSelfPermissionCallback;
            requestSelfPermissionResultCallbackNative = j;
            if (Build.VERSION.SDK_INT < 23) {
                Log.i(TAG, "permission granted: Android OS < M");
                callOnRequestSelfPermissionResult(true);
            } else if (MCActivity.checkSelfPermission(str) == 0) {
                Log.i(TAG, "permission granted: already granted");
                callOnRequestSelfPermissionResult(true);
            } else if (MCActivity.shouldShowRequestPermissionRationale(str)) {
                Log.i(TAG, "show why need the permission: " + str);
                MCActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MCApplication.MCActivity);
                        builder.setMessage(str2);
                        builder.setCancelable(false);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Log.i(MCApplication.TAG, "ask for permission: " + str);
                                MCApplication.MCActivity.requestPermissions(new String[]{str}, 0);
                                dialogInterface.cancel();
                            }
                        });
                        builder.create().show();
                    }
                });
            } else {
                Log.i(TAG, "ask for permission: " + str);
                MCActivity.requestPermissions(new String[]{str}, 0);
            }
        } else {
            Log.i(TAG, "request aborted: we have one request already in progress");
        }
    }

    private static void callOnRequestSelfPermissionResult(final boolean z) {
        if (requestSelfPermissionResultCallback != null) {
            requestSelfPermissionResultCallback.response(z);
            requestSelfPermissionResultCallback = null;
            return;
        }
        Miniclip.queueEvent(ThreadingContext.Main, new Runnable() {
            public void run() {
                MCApplication.onRequestSelfPermissionResult(MCApplication.requestSelfPermissionResultCallbackNative, z);
                long unused = MCApplication.requestSelfPermissionResultCallbackNative = 0;
            }
        });
    }

    public static boolean checkSelfPermission(String str) {
        if (Build.VERSION.SDK_INT < 23 || MCActivity.checkSelfPermission(str) == 0) {
            return true;
        }
        return false;
    }

    private static boolean hasVibrator() {
        Vibrator vibrator = (Vibrator) MCActivity.getSystemService("vibrator");
        if (vibrator == null) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= 11) {
            return vibrator.hasVibrator();
        }
        return true;
    }

    public void onNewIntent(Intent intent) {
        Uri data = intent.getData();
        if (data != null) {
            currentIntentDataString = Uri.decode(data.toString()).trim();
            if (currentIntentDataString != null) {
                Miniclip.queueEvent(ThreadingContext.Main, new Runnable() {
                    public void run() {
                        MCApplication.nativeNewIntentDataString(MCApplication.currentIntentDataString);
                    }
                });
            }
        }
    }

    public void onLowMemory() {
        Log.i("MEMORY WARNING", "LOW MEMORY");
        numMemoryWarnings++;
        if (numMemoryWarnings >= 3) {
            numMemoryWarnings = 0;
            Miniclip.queueEvent(ThreadingContext.Main, new Runnable() {
                public void run() {
                    MCApplication.nativeLowMemory();
                }
            });
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Log.i(TAG, "onRequestPermissionsResult " + iArr[0]);
        if (requestSelfPermissionResultCallback == null && requestSelfPermissionResultCallbackNative == 0) {
            Log.e(TAG, "error: callback not defined");
        } else if (iArr[0] == 0) {
            Log.i(TAG, "permission granted");
            callOnRequestSelfPermissionResult(true);
        } else {
            Log.i(TAG, "permission not granted");
            callOnRequestSelfPermissionResult(false);
        }
    }
}
