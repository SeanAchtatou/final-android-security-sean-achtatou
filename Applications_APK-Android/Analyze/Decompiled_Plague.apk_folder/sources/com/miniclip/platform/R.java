package com.miniclip.platform;

public final class R {

    public static final class drawable {
        public static final int close_webpage_big = 2131099652;
        public static final int close_webpage_small = 2131099653;
    }

    public static final class id {
        public static final int close_webpage_clickable = 2131165200;
        public static final int progressBar1 = 2131165282;
        public static final int webview_with_webpage = 2131165296;
    }

    public static final class layout {
        public static final int progress_view = 2131296291;
    }
}
