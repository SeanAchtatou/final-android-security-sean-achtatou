package com.miniclip.platform;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.miniclip.framework.AbstractActivityListener;
import com.miniclip.framework.MiniclipAndroidActivity;
import com.miniclip.framework.ThreadingContext;
import com.miniclip.nativeJNI.ClearGLSurfaceView;
import com.miniclip.nativeJNI.ClearRenderer;
import com.miniclip.nativeJNI.GLSurfaceViewProfile;
import com.miniclip.nativeJNI.cocojava;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

public class MCViewManager {
    private static final int GL_VERSION_CODE = 1;
    private static final String TAG = "MCViewManager";
    /* access modifiers changed from: private */
    public static MiniclipAndroidActivity activity = null;
    /* access modifiers changed from: private */
    public static ClearRenderer clearRenderer = null;
    public static int displayHeight = 0;
    public static int displayWidth = 0;
    private static List<EGLComponentSizeParameters> eglParametersList = new ArrayList();
    /* access modifiers changed from: private */
    public static ClearGLSurfaceView glView = null;
    /* access modifiers changed from: private */
    public static RelativeLayout initView = null;
    private static MCViewManager instance = new MCViewManager();
    /* access modifiers changed from: private */
    public static boolean isFirstRun = true;
    private static boolean isInitialized = false;
    private static Set<MCViewManagerEventsListener> listeners = new HashSet();
    /* access modifiers changed from: private */
    public static RelativeLayout mainLayout;
    private static final List<Runnable> pendingEvents = new ArrayList();
    private static Handler screenFixHandler = new Handler();

    public interface MCViewManagerEventsListener {
        void onShowDefaultView(RelativeLayout relativeLayout);
    }

    private class MCViewManagerActivityListener extends AbstractActivityListener {
        private boolean hasWindowFocus;
        private boolean resumeOnFocus;

        private MCViewManagerActivityListener() {
            this.hasWindowFocus = false;
            this.resumeOnFocus = false;
        }

        public void onPause() {
            if (MCViewManager.glView != null) {
                MCViewManager.glView.onPause();
            }
        }

        public void onWindowFocusChanged(boolean z) {
            this.hasWindowFocus = z;
            if (this.hasWindowFocus && this.resumeOnFocus) {
                this.resumeOnFocus = false;
                if (MCViewManager.glView != null) {
                    MCViewManager.glView.onResume();
                }
            }
            if (this.hasWindowFocus) {
                MCViewManager.fixScreen();
            }
            if (this.hasWindowFocus && MCViewManager.isFirstRun) {
                boolean unused = MCViewManager.isFirstRun = false;
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        MCViewManager.activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
                            public void run() {
                                MCViewManager.this.showDefaultView();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        MCViewManager.activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
                                            public void run() {
                                                MCViewManager.this.firstRun();
                                            }
                                        });
                                    }
                                }, 50);
                            }
                        });
                    }
                }, 50);
            }
            if (this.hasWindowFocus && Build.VERSION.SDK_INT >= 19) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        MCViewManager.this.setSystemUiVisibility();
                    }
                }, 500);
            }
        }

        public void onResume() {
            if (this.hasWindowFocus) {
                this.resumeOnFocus = false;
                if (MCViewManager.glView != null) {
                    MCViewManager.glView.onResume();
                    return;
                }
                return;
            }
            this.resumeOnFocus = true;
        }
    }

    private static class EGLComponentSizeParameters {
        private int alphaSize;
        private int blueSize;
        private int depthSize;
        private int greenSize;
        private int redSize;
        private int stencilSize;

        EGLComponentSizeParameters(int i, int i2, int i3, int i4, int i5, int i6) {
            this.redSize = i;
            this.greenSize = i2;
            this.blueSize = i3;
            this.alphaSize = i4;
            this.depthSize = i5;
            this.stencilSize = i6;
        }

        /* access modifiers changed from: package-private */
        public GLSurfaceViewProfile.EGLConfigChooser createEGLConfigChooser(GLSurfaceViewProfile gLSurfaceViewProfile) {
            gLSurfaceViewProfile.getClass();
            return new GLSurfaceViewProfile.ComponentSizeChooser(this.redSize, this.greenSize, this.blueSize, this.alphaSize, this.depthSize, this.stencilSize);
        }
    }

    private class EGLMultipleConfigChooserAdapter implements GLSurfaceViewProfile.EGLConfigChooser {
        private List<GLSurfaceViewProfile.EGLConfigChooser> configChoosers;

        private EGLMultipleConfigChooserAdapter() {
            this.configChoosers = new ArrayList();
        }

        /* access modifiers changed from: package-private */
        public void addEGLConfigChooser(GLSurfaceViewProfile.EGLConfigChooser eGLConfigChooser) {
            this.configChoosers.add(eGLConfigChooser);
        }

        public EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay) {
            for (GLSurfaceViewProfile.EGLConfigChooser chooseConfig : this.configChoosers) {
                try {
                    EGLConfig chooseConfig2 = chooseConfig.chooseConfig(egl10, eGLDisplay);
                    if (chooseConfig2 != null) {
                        return chooseConfig2;
                    }
                } catch (Exception unused) {
                }
            }
            throw new IllegalArgumentException("No config chosen");
        }
    }

    private MCViewManager() {
    }

    public static void init(MiniclipAndroidActivity miniclipAndroidActivity) {
        instance.initImpl(miniclipAndroidActivity);
    }

    @TargetApi(19)
    private void initImpl(MiniclipAndroidActivity miniclipAndroidActivity) {
        if (!isInitialized) {
            activity = miniclipAndroidActivity;
            mainLayout = new RelativeLayout(activity);
            mainLayout.setBackgroundColor(-16777216);
            activity.setContentView(mainLayout);
            if (Build.VERSION.SDK_INT >= 19) {
                activity.getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                    public void onSystemUiVisibilityChange(int i) {
                        MCViewManager.this.setSystemUiVisibility();
                    }
                });
                setSystemUiVisibility();
            }
            Point point = new Point();
            if (Build.VERSION.SDK_INT >= 17) {
                getRealSizeOfDisplay(point);
            } else if (Build.VERSION.SDK_INT >= 13) {
                getSizeOfDisplay(point);
            } else {
                getMetricsOfDisplay(point);
            }
            if (cocojava.mINGAME_LANDSCAPE && point.y > point.x) {
                Log.i("Activity", "wrong proportions");
                int i = point.x;
                point.x = point.y;
                point.y = i;
            }
            displayWidth = point.x;
            displayHeight = point.y;
            mainLayout.setMinimumWidth(displayWidth);
            mainLayout.setMinimumHeight(displayHeight);
            activity.addListener(new MCViewManagerActivityListener());
            isInitialized = true;
        }
    }

    @TargetApi(17)
    private void getRealSizeOfDisplay(Point point) {
        activity.getWindowManager().getDefaultDisplay().getRealSize(point);
    }

    @TargetApi(13)
    private void getSizeOfDisplay(Point point) {
        activity.getWindowManager().getDefaultDisplay().getSize(point);
    }

    private void getMetricsOfDisplay(Point point) {
        DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();
        point.x = displayMetrics.widthPixels;
        point.y = displayMetrics.heightPixels;
    }

    /* access modifiers changed from: private */
    @TargetApi(11)
    public void setSystemUiVisibility() {
        activity.getWindow().getDecorView().setSystemUiVisibility(5894);
    }

    /* access modifiers changed from: private */
    public void showDefaultView() {
        initView = new RelativeLayout(activity);
        initView.setLayoutParams(new RelativeLayout.LayoutParams(displayWidth, displayHeight));
        initView.setPadding(0, 0, 0, 0);
        initView.setBackgroundColor(-16777216);
        onShowDefaultView(initView);
        mainLayout.addView(initView);
    }

    private void showDefaultViewAnimation() {
        float f = (float) displayHeight;
        RelativeLayout relativeLayout = new RelativeLayout(activity);
        int i = (int) (f / 2.0f);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i, i);
        layoutParams.addRule(14);
        layoutParams.addRule(12);
        relativeLayout.setLayoutParams(layoutParams);
        initView.addView(relativeLayout);
        if (cocojava.mSPINNING_ANIMATION) {
            try {
                ImageView imageView = new ImageView(activity);
                imageView.setImageDrawable(activity.getResources().getDrawable(activity.getResources().getIdentifier("spinning_ball", "drawable", activity.getPackageName())));
                int i2 = (int) (f / 12.0f);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(i2, i2);
                layoutParams2.addRule(13);
                imageView.setLayoutParams(layoutParams2);
                imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                relativeLayout.addView(imageView);
                ImageView imageView2 = new ImageView(activity);
                imageView2.setImageDrawable(activity.getResources().getDrawable(activity.getResources().getIdentifier("spinning_ball_effect", "drawable", activity.getPackageName())));
                int i3 = (int) (f / 8.0f);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(i3, i3);
                layoutParams3.addRule(13);
                imageView2.setLayoutParams(layoutParams3);
                imageView2.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                relativeLayout.addView(imageView2);
                RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 360.0f, (float) (i3 / 2), (float) (i3 / 2));
                rotateAnimation.setDuration(1000);
                rotateAnimation.setRepeatCount(-1);
                rotateAnimation.setInterpolator(new LinearInterpolator());
                rotateAnimation.setFillAfter(true);
                imageView2.startAnimation(rotateAnimation);
            } catch (Exception unused) {
                cocojava.displayLowStorageMessage(1004);
            }
        }
    }

    public static void setContentToGl() {
        activity.queueEvent(ThreadingContext.UiThread, new Runnable() {
            public void run() {
                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                alphaAnimation.setDuration(250);
                alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                        MCViewManager.mainLayout.removeView(MCViewManager.initView);
                    }
                });
                MCViewManager.initView.startAnimation(alphaAnimation);
            }
        });
    }

    public static void fixScreen() {
        AnonymousClass3 r0 = new Runnable() {
            public void run() {
                Log.i("cocojava", "fixScreen");
                if (MCViewManager.mainLayout != null) {
                    MCViewManager.mainLayout.requestLayout();
                }
            }
        };
        screenFixHandler.removeCallbacks(r0);
        screenFixHandler.postDelayed(r0, 1000);
    }

    /* access modifiers changed from: private */
    public void firstRun() {
        ArrayList<Runnable> arrayList;
        ((cocojava) activity).firstRun();
        if (!MCApplication.hasFatalErrorOccurred()) {
            Log.i(TAG, "firstRun");
            Log.i("Dim", String.format("width: %d, height: %d, density: %f", Integer.valueOf(displayWidth), Integer.valueOf(displayHeight), Float.valueOf(activity.getResources().getDisplayMetrics().density)));
            synchronized (pendingEvents) {
                glView = new ClearGLSurfaceView(activity);
                clearRenderer = new ClearRenderer();
                glView.setEGLContextClientVersion(1);
                GLSurfaceViewProfile.EGLConfigChooser createEGLConfigChooser = createEGLConfigChooser(glView);
                if (createEGLConfigChooser != null) {
                    glView.setEGLConfigChooser(createEGLConfigChooser);
                }
                glView.setRenderer(clearRenderer);
                arrayList = new ArrayList<>(pendingEvents);
                pendingEvents.clear();
            }
            mainLayout.addView(glView, 0);
            showDefaultViewAnimation();
            for (Runnable runOnGlThread : arrayList) {
                runOnGlThread(runOnGlThread);
            }
            arrayList.clear();
        }
    }

    private GLSurfaceViewProfile.EGLConfigChooser createEGLConfigChooser(GLSurfaceViewProfile gLSurfaceViewProfile) {
        if (eglParametersList == null || eglParametersList.isEmpty()) {
            return null;
        }
        EGLMultipleConfigChooserAdapter eGLMultipleConfigChooserAdapter = new EGLMultipleConfigChooserAdapter();
        for (EGLComponentSizeParameters createEGLConfigChooser : eglParametersList) {
            eGLMultipleConfigChooserAdapter.addEGLConfigChooser(createEGLConfigChooser.createEGLConfigChooser(gLSurfaceViewProfile));
        }
        return eGLMultipleConfigChooserAdapter;
    }

    public static void addEGLConfigOption(int i, int i2, int i3, int i4, int i5, int i6) {
        eglParametersList.add(new EGLComponentSizeParameters(i, i2, i3, i4, i5, i6));
    }

    public static RelativeLayout getMainLayout() {
        return mainLayout;
    }

    public static void runOnGlThread(final Runnable runnable) {
        synchronized (pendingEvents) {
            if (glView == null) {
                Log.d(TAG, "Tried to run on a null glView:");
                pendingEvents.add(runnable);
                return;
            }
            glView.queueEvent(new Runnable() {
                public void run() {
                    MCViewManager.clearRenderer.queueEvent(runnable);
                }
            });
        }
    }

    public static boolean addListener(MCViewManagerEventsListener mCViewManagerEventsListener) {
        return listeners.add(mCViewManagerEventsListener);
    }

    private void onShowDefaultView(RelativeLayout relativeLayout) {
        for (MCViewManagerEventsListener onShowDefaultView : listeners) {
            onShowDefaultView.onShowDefaultView(relativeLayout);
        }
    }
}
