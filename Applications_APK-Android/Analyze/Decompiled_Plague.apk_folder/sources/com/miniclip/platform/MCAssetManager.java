package com.miniclip.platform;

import android.util.Log;
import com.miniclip.framework.MiniclipAndroidActivity;

public class MCAssetManager {
    private static MiniclipAndroidActivity MCActivity = null;
    private static final String TAG = "MCAssetManager";

    private static native void nativeSetAPKPath(String str);

    private static native void nativeSetFilesPath(String str);

    public static void init(MiniclipAndroidActivity miniclipAndroidActivity) {
        MCActivity = miniclipAndroidActivity;
    }

    public static String getFilesPath() {
        return MCActivity.getFilesDir().getAbsolutePath();
    }

    public static String filePathToAssetsPath(String str) {
        String str2 = getFilesPath() + "/Contents/Resources/";
        if (!str.startsWith(str2)) {
            return null;
        }
        return "unpack/" + str.substring(str2.length());
    }

    public static void firstRun() {
        try {
            nativeSetAPKPath(MCActivity.getPackageManager().getApplicationInfo(MCActivity.getPackageName(), 0).sourceDir);
        } catch (Exception e) {
            e.printStackTrace();
        }
        nativeSetFilesPath(getFilesPath());
        Log.i(TAG, "Files Path: " + getFilesPath());
    }
}
