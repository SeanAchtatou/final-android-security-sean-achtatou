package com.miniclip.platform;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

public class MultiDexApplication extends Application {
    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        try {
            MultiDex.install(this);
        } catch (Throwable th) {
            th.printStackTrace();
            MCApplication.signalFatalError(1001);
        }
    }
}
