package com.miniclip.events;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.os.Build;
import android.os.Bundle;
import com.miniclip.framework.AbstractActivityListener;
import com.miniclip.framework.Miniclip;
import java.util.HashSet;
import java.util.Set;

public class EventDispatcher extends AbstractActivityListener {
    private static EventDispatcher instance = new EventDispatcher();

    /* access modifiers changed from: private */
    public static native void onAppEnterBackgroundNative();

    /* access modifiers changed from: private */
    public static native void onAppEnterForegroundNative();

    private static native void onPauseNative();

    private static native void onResumeNative();

    private static native void onStartNative();

    private static native void onStopNative();

    private EventDispatcher() {
    }

    public static void registerForApplicationEvents() {
        Miniclip.addListener(instance);
        if (Build.VERSION.SDK_INT >= 14) {
            Application application = Miniclip.getActivity().getApplication();
            EventDispatcher eventDispatcher = instance;
            eventDispatcher.getClass();
            application.registerActivityLifecycleCallbacks(new BackgroundForegroundDetector());
        }
    }

    public void onPause() {
        onPauseNative();
    }

    public void onResume() {
        onResumeNative();
    }

    public void onStart() {
        onStartNative();
    }

    public void onStop() {
        onStopNative();
    }

    @TargetApi(14)
    private class BackgroundForegroundDetector implements Application.ActivityLifecycleCallbacks {
        private Set<Activity> activeActivities = new HashSet();

        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivityPaused(Activity activity) {
        }

        public void onActivityResumed(Activity activity) {
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        BackgroundForegroundDetector() {
            this.activeActivities.add(Miniclip.getActivity());
        }

        public void onActivityStarted(Activity activity) {
            if (this.activeActivities.size() == 0) {
                EventDispatcher.onAppEnterForegroundNative();
            }
            this.activeActivities.add(activity);
        }

        public void onActivityStopped(Activity activity) {
            this.activeActivities.remove(activity);
            if (this.activeActivities.size() == 0) {
                EventDispatcher.onAppEnterBackgroundNative();
            }
        }
    }
}
