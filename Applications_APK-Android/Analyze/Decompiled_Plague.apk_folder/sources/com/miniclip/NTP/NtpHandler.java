package com.miniclip.NTP;

import android.os.Handler;
import android.util.Log;
import com.miniclip.framework.ThreadingContext;
import com.miniclip.nativeJNI.CocoJNI;
import com.miniclip.nativeJNI.cocojava;
import java.io.IOException;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.text.DecimalFormat;

public class NtpHandler {
    private Handler _handler = new Handler();
    /* access modifiers changed from: private */
    public int _status = 0;

    private interface NtpResponce {
        void failure();

        void success(double d);
    }

    public double getOffsetFromServer(String str) throws IOException {
        DatagramSocket datagramSocket = new DatagramSocket();
        InetAddress byName = InetAddress.getByName(str);
        byte[] byteArray = new NtpMessage().toByteArray();
        DatagramPacket datagramPacket = new DatagramPacket(byteArray, byteArray.length, byName, 123);
        NtpMessage.encodeTimestamp(datagramPacket.getData(), 40, (((double) System.currentTimeMillis()) / 1000.0d) + 2.2089888E9d);
        datagramSocket.send(datagramPacket);
        System.out.println("NTP request sent, waiting for response...\n");
        DatagramPacket datagramPacket2 = new DatagramPacket(byteArray, byteArray.length);
        datagramSocket.receive(datagramPacket2);
        double currentTimeMillis = (((double) System.currentTimeMillis()) / 1000.0d) + 2.2089888E9d;
        NtpMessage ntpMessage = new NtpMessage(datagramPacket2.getData());
        double d = (currentTimeMillis - ntpMessage.originateTimestamp) - (ntpMessage.transmitTimestamp - ntpMessage.receiveTimestamp);
        double d2 = ((ntpMessage.receiveTimestamp - ntpMessage.originateTimestamp) + (ntpMessage.transmitTimestamp - currentTimeMillis)) / 2.0d;
        PrintStream printStream = System.out;
        printStream.println("NTP server: " + str);
        System.out.println(ntpMessage.toString());
        PrintStream printStream2 = System.out;
        printStream2.println("Dest. timestamp:     " + NtpMessage.timestampToString(currentTimeMillis));
        PrintStream printStream3 = System.out;
        printStream3.println("Round-trip delay: " + new DecimalFormat("0.00").format(d * 1000.0d) + " ms");
        PrintStream printStream4 = System.out;
        printStream4.println("Local clock offset: " + new DecimalFormat("0.00").format(1000.0d * d2) + " ms");
        datagramSocket.close();
        return d2;
    }

    public void getOffsetFromServerAsync(final String str, final NtpResponce ntpResponce) {
        new Thread() {
            public void run() {
                try {
                    ntpResponce.success(NtpHandler.this.getOffsetFromServer(str));
                } catch (IOException e) {
                    e.printStackTrace();
                    ntpResponce.failure();
                }
            }
        }.start();
    }

    public void getOffsetFromServerListAsync(String str, final int i, int i2) {
        Log.i("NtpHandler", "getOffsetFromServerListAsync1");
        if (this._status == 0) {
            this._status = 1;
            String[] split = str.split(",");
            for (String offsetFromServerAsync : split) {
                getOffsetFromServerAsync(offsetFromServerAsync, new NtpResponce() {
                    public void failure() {
                    }

                    public void success(final double d) {
                        if (NtpHandler.this._status == 1) {
                            cocojava.mContext.queueEvent(ThreadingContext.GlThread, new Runnable() {
                                public void run() {
                                    CocoJNI.MnetworkTimeResponce(i, d, 0);
                                }
                            });
                        }
                        int unused = NtpHandler.this._status = 2;
                    }
                });
            }
            this._handler.postDelayed(new Runnable() {
                public void run() {
                    if (NtpHandler.this._status == 1) {
                        cocojava.mContext.queueEvent(ThreadingContext.GlThread, new Runnable() {
                            public void run() {
                                CocoJNI.MnetworkTimeResponce(i, 0.0d, 1);
                            }
                        });
                    }
                    int unused = NtpHandler.this._status = 0;
                }
            }, (long) i2);
        }
    }
}
