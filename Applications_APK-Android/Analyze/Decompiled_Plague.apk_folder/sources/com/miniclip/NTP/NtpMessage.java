package com.miniclip.NTP;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NtpMessage {
    public byte leapIndicator;
    public byte mode;
    public double originateTimestamp;
    public byte pollInterval;
    public byte precision;
    public double receiveTimestamp;
    public byte[] referenceIdentifier;
    public double referenceTimestamp;
    public double rootDelay;
    public double rootDispersion;
    public short stratum;
    public double transmitTimestamp;
    public byte version;

    public static short unsignedByteToShort(byte b) {
        return (b & 128) == 128 ? (short) (128 + (b & Byte.MAX_VALUE)) : (short) b;
    }

    public NtpMessage(byte[] bArr) {
        this.leapIndicator = 0;
        this.version = 3;
        this.mode = 0;
        this.stratum = 0;
        this.pollInterval = 0;
        this.precision = 0;
        this.rootDelay = 0.0d;
        this.rootDispersion = 0.0d;
        this.referenceIdentifier = new byte[]{0, 0, 0, 0};
        this.referenceTimestamp = 0.0d;
        this.originateTimestamp = 0.0d;
        this.receiveTimestamp = 0.0d;
        this.transmitTimestamp = 0.0d;
        this.leapIndicator = (byte) ((bArr[0] >> 6) & 3);
        this.version = (byte) ((bArr[0] >> 3) & 7);
        this.mode = (byte) (bArr[0] & 7);
        this.stratum = unsignedByteToShort(bArr[1]);
        this.pollInterval = bArr[2];
        this.precision = bArr[3];
        this.rootDelay = (((double) bArr[4]) * 256.0d) + ((double) unsignedByteToShort(bArr[5])) + (((double) unsignedByteToShort(bArr[6])) / 256.0d) + (((double) unsignedByteToShort(bArr[7])) / 65536.0d);
        this.rootDispersion = (((double) unsignedByteToShort(bArr[8])) * 256.0d) + ((double) unsignedByteToShort(bArr[9])) + (((double) unsignedByteToShort(bArr[10])) / 256.0d) + (((double) unsignedByteToShort(bArr[11])) / 65536.0d);
        this.referenceIdentifier[0] = bArr[12];
        this.referenceIdentifier[1] = bArr[13];
        this.referenceIdentifier[2] = bArr[14];
        this.referenceIdentifier[3] = bArr[15];
        this.referenceTimestamp = decodeTimestamp(bArr, 16);
        this.originateTimestamp = decodeTimestamp(bArr, 24);
        this.receiveTimestamp = decodeTimestamp(bArr, 32);
        this.transmitTimestamp = decodeTimestamp(bArr, 40);
    }

    public NtpMessage() {
        this.leapIndicator = 0;
        this.version = 3;
        this.mode = 0;
        this.stratum = 0;
        this.pollInterval = 0;
        this.precision = 0;
        this.rootDelay = 0.0d;
        this.rootDispersion = 0.0d;
        this.referenceIdentifier = new byte[]{0, 0, 0, 0};
        this.referenceTimestamp = 0.0d;
        this.originateTimestamp = 0.0d;
        this.receiveTimestamp = 0.0d;
        this.transmitTimestamp = 0.0d;
        this.mode = 3;
        this.transmitTimestamp = (((double) System.currentTimeMillis()) / 1000.0d) + 2.2089888E9d;
    }

    public byte[] toByteArray() {
        byte[] bArr = new byte[48];
        bArr[0] = (byte) ((this.leapIndicator << 6) | (this.version << 3) | this.mode);
        bArr[1] = (byte) this.stratum;
        bArr[2] = this.pollInterval;
        bArr[3] = this.precision;
        int i = (int) (this.rootDelay * 65536.0d);
        bArr[4] = (byte) ((i >> 24) & 255);
        bArr[5] = (byte) ((i >> 16) & 255);
        bArr[6] = (byte) ((i >> 8) & 255);
        bArr[7] = (byte) (i & 255);
        long j = (long) (this.rootDispersion * 65536.0d);
        bArr[8] = (byte) ((int) ((j >> 24) & 255));
        bArr[9] = (byte) ((int) ((j >> 16) & 255));
        bArr[10] = (byte) ((int) ((j >> 8) & 255));
        bArr[11] = (byte) ((int) (j & 255));
        bArr[12] = this.referenceIdentifier[0];
        bArr[13] = this.referenceIdentifier[1];
        bArr[14] = this.referenceIdentifier[2];
        bArr[15] = this.referenceIdentifier[3];
        encodeTimestamp(bArr, 16, this.referenceTimestamp);
        encodeTimestamp(bArr, 24, this.originateTimestamp);
        encodeTimestamp(bArr, 32, this.receiveTimestamp);
        encodeTimestamp(bArr, 40, this.transmitTimestamp);
        return bArr;
    }

    public String toString() {
        String format = new DecimalFormat("0.#E0").format(Math.pow(2.0d, (double) this.precision));
        return "Leap indicator: " + ((int) this.leapIndicator) + "\n" + "Version: " + ((int) this.version) + "\n" + "Mode: " + ((int) this.mode) + "\n" + "Stratum: " + ((int) this.stratum) + "\n" + "Poll: " + ((int) this.pollInterval) + "\n" + "Precision: " + ((int) this.precision) + " (" + format + " seconds)\n" + "Root delay: " + new DecimalFormat("0.00").format(this.rootDelay * 1000.0d) + " ms\n" + "Root dispersion: " + new DecimalFormat("0.00").format(this.rootDispersion * 1000.0d) + " ms\n" + "Reference identifier: " + referenceIdentifierToString(this.referenceIdentifier, this.stratum, this.version) + "\n" + "Reference timestamp: " + timestampToString(this.referenceTimestamp) + "\n" + "Originate timestamp: " + timestampToString(this.originateTimestamp) + "\n" + "Receive timestamp:   " + timestampToString(this.receiveTimestamp) + "\n" + "Transmit timestamp:  " + timestampToString(this.transmitTimestamp);
    }

    public static double decodeTimestamp(byte[] bArr, int i) {
        double d = 0.0d;
        for (int i2 = 0; i2 < 8; i2++) {
            d += ((double) unsignedByteToShort(bArr[i + i2])) * Math.pow(2.0d, (double) ((3 - i2) * 8));
        }
        return d;
    }

    public static void encodeTimestamp(byte[] bArr, int i, double d) {
        for (int i2 = 0; i2 < 8; i2++) {
            double pow = Math.pow(2.0d, (double) ((3 - i2) * 8));
            int i3 = i + i2;
            bArr[i3] = (byte) ((int) (d / pow));
            d -= ((double) unsignedByteToShort(bArr[i3])) * pow;
        }
        bArr[7] = (byte) ((int) (Math.random() * 255.0d));
    }

    public static String timestampToString(double d) {
        if (d == 0.0d) {
            return "0";
        }
        String format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").format(new Date((long) ((d - 2.2089888E9d) * 1000.0d)));
        String format2 = new DecimalFormat(".000000").format(d - ((double) ((long) d)));
        return format + format2;
    }

    public static String referenceIdentifierToString(byte[] bArr, short s, byte b) {
        if (s == 0 || s == 1) {
            return new String(bArr);
        }
        if (b == 3) {
            return ((int) unsignedByteToShort(bArr[0])) + "." + ((int) unsignedByteToShort(bArr[1])) + "." + ((int) unsignedByteToShort(bArr[2])) + "." + ((int) unsignedByteToShort(bArr[3]));
        } else if (b != 4) {
            return "";
        } else {
            return "" + ((((double) unsignedByteToShort(bArr[0])) / 256.0d) + (((double) unsignedByteToShort(bArr[1])) / 65536.0d) + (((double) unsignedByteToShort(bArr[2])) / 1.6777216E7d) + (((double) unsignedByteToShort(bArr[3])) / 4.294967296E9d));
        }
    }
}
