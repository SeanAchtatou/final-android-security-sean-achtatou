package com.miniclip.clipboard;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import java.io.PrintStream;

public class Clipboard {
    private static ClipboardManager clipboardManager;

    public static void init(Activity activity) {
        try {
            clipboardManager = (ClipboardManager) activity.getSystemService("clipboard");
        } catch (Exception e) {
            PrintStream printStream = System.err;
            printStream.println("Clipboard: " + e.getMessage());
        }
    }

    public static boolean isSupported() {
        if (Build.VERSION.SDK_INT < 11) {
            Log.e("Clipboard", "Unsupported API version");
            return false;
        } else if (clipboardManager != null) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean copyText(String str) {
        if (Build.VERSION.SDK_INT < 11) {
            Log.e("Clipboard", "Unsupported API version");
            return false;
        } else if (clipboardManager == null) {
            Log.e("Clipboard", "No ClipboardManager instance");
            return false;
        } else {
            clipboardManager.setPrimaryClip(ClipData.newPlainText("clipboard text", str));
            return true;
        }
    }

    public static boolean hasText() {
        if (Build.VERSION.SDK_INT < 11) {
            Log.e("Clipboard", "Unsupported API version");
            return false;
        } else if (clipboardManager == null) {
            Log.e("Clipboard", "No ClipboardManager instance");
            return false;
        } else {
            ClipDescription primaryClipDescription = clipboardManager.getPrimaryClipDescription();
            if (primaryClipDescription == null || !primaryClipDescription.hasMimeType("text/plain")) {
                return false;
            }
            return true;
        }
    }

    public static String pasteText() {
        if (Build.VERSION.SDK_INT < 11) {
            Log.e("Clipboard", "Unsupported API version");
            return "";
        } else if (clipboardManager == null) {
            Log.e("Clipboard", "No ClipboardManager instance");
            return "";
        } else {
            ClipData primaryClip = clipboardManager.getPrimaryClip();
            if (primaryClip == null || primaryClip.getItemCount() == 0) {
                return "";
            }
            ClipData.Item itemAt = primaryClip.getItemAt(0);
            CharSequence text = itemAt.getText();
            if (text != null) {
                return text.toString();
            }
            Uri uri = itemAt.getUri();
            if (uri != null) {
                return uri.toString();
            }
            Log.e("Clipboard", "No valid text on the clipboard");
            return "";
        }
    }
}
