package com.miniclip.filesystem;

import android.content.res.AssetFileDescriptor;
import android.os.ParcelFileDescriptor;
import com.google.android.gms.drive.DriveFile;
import com.miniclip.framework.Miniclip;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileManager {
    public static final int STORAGE_TYPE_BUNDLE = 0;
    public static final int STORAGE_TYPE_SAVE = 1;
    public static final int STORAGE_TYPE_TEMP = 2;

    public static AssetFileDescriptor getAssetFileDescriptor(int i, String str) throws IOException {
        String str2;
        switch (i) {
            case 0:
                return Miniclip.getActivity().getAssets().openFd(str);
            case 1:
                str2 = Miniclip.getActivity().getFilesDir().getAbsolutePath() + "/Contents/Documents/";
                break;
            case 2:
                str2 = Miniclip.getActivity().getFilesDir().getAbsolutePath() + "/Contents/Caches/";
                break;
            default:
                throw new FileNotFoundException("Unknown storage type: " + i);
        }
        File file = new File(str2 + str);
        return new AssetFileDescriptor(ParcelFileDescriptor.open(file, DriveFile.MODE_READ_ONLY), 0, file.length());
    }
}
