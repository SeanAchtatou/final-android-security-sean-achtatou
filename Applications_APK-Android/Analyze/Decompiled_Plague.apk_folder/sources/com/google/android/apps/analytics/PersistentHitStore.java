package com.google.android.apps.analytics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.tapjoy.TapjoyConstants;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

class PersistentHitStore implements HitStore {
    private static final String ACCOUNT_ID = "account_id";
    private static final String ACTION = "action";
    private static final String CATEGORY = "category";
    /* access modifiers changed from: private */
    public static final String CREATE_CUSTOM_VARIABLES_TABLE = ("CREATE TABLE custom_variables (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", CUSTOMVAR_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' INTEGER NOT NULL,", CUSTOMVAR_INDEX) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_NAME) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_VALUE) + String.format(" '%s' INTEGER NOT NULL);", CUSTOMVAR_SCOPE));
    /* access modifiers changed from: private */
    public static final String CREATE_CUSTOM_VAR_CACHE_TABLE = ("CREATE TABLE IF NOT EXISTS custom_var_cache (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", CUSTOMVAR_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' INTEGER NOT NULL,", CUSTOMVAR_INDEX) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_NAME) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_VALUE) + String.format(" '%s' INTEGER NOT NULL);", CUSTOMVAR_SCOPE));
    /* access modifiers changed from: private */
    public static final String CREATE_EVENTS_TABLE = ("CREATE TABLE events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", EVENT_ID) + String.format(" '%s' INTEGER NOT NULL,", USER_ID) + String.format(" '%s' CHAR(256) NOT NULL,", ACCOUNT_ID) + String.format(" '%s' INTEGER NOT NULL,", RANDOM_VAL) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_FIRST) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_PREVIOUS) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_CURRENT) + String.format(" '%s' INTEGER NOT NULL,", VISITS) + String.format(" '%s' CHAR(256) NOT NULL,", CATEGORY) + String.format(" '%s' CHAR(256) NOT NULL,", "action") + String.format(" '%s' CHAR(256), ", LABEL) + String.format(" '%s' INTEGER,", VALUE) + String.format(" '%s' INTEGER,", SCREEN_WIDTH) + String.format(" '%s' INTEGER);", SCREEN_HEIGHT));
    /* access modifiers changed from: private */
    public static final String CREATE_HITS_TABLE;
    private static final String CREATE_INSTALL_REFERRER_TABLE = "CREATE TABLE install_referrer (referrer TEXT PRIMARY KEY NOT NULL);";
    /* access modifiers changed from: private */
    public static final String CREATE_ITEM_EVENTS_TABLE = ("CREATE TABLE item_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", ITEM_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' TEXT NOT NULL,", ORDER_ID) + String.format(" '%s' TEXT NOT NULL,", ITEM_SKU) + String.format(" '%s' TEXT,", ITEM_NAME) + String.format(" '%s' TEXT,", ITEM_CATEGORY) + String.format(" '%s' TEXT NOT NULL,", ITEM_PRICE) + String.format(" '%s' TEXT NOT NULL);", ITEM_COUNT));
    private static final String CREATE_REFERRER_TABLE = "CREATE TABLE IF NOT EXISTS referrer (referrer TEXT PRIMARY KEY NOT NULL,timestamp_referrer INTEGER NOT NULL,referrer_visit INTEGER NOT NULL DEFAULT 1,referrer_index INTEGER NOT NULL DEFAULT 1);";
    /* access modifiers changed from: private */
    public static final String CREATE_SESSION_TABLE;
    /* access modifiers changed from: private */
    public static final String CREATE_TRANSACTION_EVENTS_TABLE = ("CREATE TABLE transaction_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", TRANSACTION_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' TEXT NOT NULL,", ORDER_ID) + String.format(" '%s' TEXT,", STORE_NAME) + String.format(" '%s' TEXT NOT NULL,", TOTAL_COST) + String.format(" '%s' TEXT,", TOTAL_TAX) + String.format(" '%s' TEXT);", SHIPPING_COST));
    private static final String CUSTOMVAR_ID = "cv_id";
    private static final String CUSTOMVAR_INDEX = "cv_index";
    private static final String CUSTOMVAR_NAME = "cv_name";
    private static final String CUSTOMVAR_SCOPE = "cv_scope";
    private static final String CUSTOMVAR_VALUE = "cv_value";
    private static final String CUSTOM_VARIABLE_COLUMN_TYPE = "CHAR(64) NOT NULL";
    private static final String DATABASE_NAME = "google_analytics.db";
    private static final int DATABASE_VERSION = 5;
    private static final String EVENT_ID = "event_id";
    private static final String HIT_ID = "hit_id";
    private static final String HIT_STRING = "hit_string";
    private static final String HIT_TIMESTAMP = "hit_time";
    private static final String ITEM_CATEGORY = "item_category";
    private static final String ITEM_COUNT = "item_count";
    private static final String ITEM_ID = "item_id";
    private static final String ITEM_NAME = "item_name";
    private static final String ITEM_PRICE = "item_price";
    private static final String ITEM_SKU = "item_sku";
    private static final String LABEL = "label";
    private static final int MAX_HITS = 1000;
    private static final String ORDER_ID = "order_id";
    private static final String RANDOM_VAL = "random_val";
    static final String REFERRER = "referrer";
    static final String REFERRER_COLUMN = "referrer";
    static final String REFERRER_INDEX = "referrer_index";
    static final String REFERRER_VISIT = "referrer_visit";
    private static final String SCREEN_HEIGHT = "screen_height";
    private static final String SCREEN_WIDTH = "screen_width";
    private static final String SHIPPING_COST = "tran_shippingcost";
    private static final String STORE_ID = "store_id";
    private static final String STORE_NAME = "tran_storename";
    private static final String TIMESTAMP_CURRENT = "timestamp_current";
    private static final String TIMESTAMP_FIRST = "timestamp_first";
    private static final String TIMESTAMP_PREVIOUS = "timestamp_previous";
    static final String TIMESTAMP_REFERRER = "timestamp_referrer";
    private static final String TOTAL_COST = "tran_totalcost";
    private static final String TOTAL_TAX = "tran_totaltax";
    private static final String TRANSACTION_ID = "tran_id";
    private static final String USER_ID = "user_id";
    private static final String VALUE = "value";
    private static final String VISITS = "visits";
    private boolean anonymizeIp;
    private DataBaseHelper databaseHelper;
    private volatile int numStoredHits;
    private Random random;
    private int sampleRate;
    private boolean sessionStarted;
    private int storeId;
    private long timestampCurrent;
    private long timestampFirst;
    private long timestampPrevious;
    private boolean useStoredVisitorVars;
    /* access modifiers changed from: private */
    public CustomVariableBuffer visitorCVCache;
    private int visits;

    static class DataBaseHelper extends SQLiteOpenHelper {
        private final int databaseVersion;
        private final PersistentHitStore store;

        public DataBaseHelper(Context context, PersistentHitStore persistentHitStore) {
            this(context, PersistentHitStore.DATABASE_NAME, 5, persistentHitStore);
        }

        DataBaseHelper(Context context, String str, int i, PersistentHitStore persistentHitStore) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, i);
            this.databaseVersion = i;
            this.store = persistentHitStore;
        }

        public DataBaseHelper(Context context, String str, PersistentHitStore persistentHitStore) {
            this(context, str, 5, persistentHitStore);
        }

        private void createECommerceTables(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS transaction_events;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_TRANSACTION_EVENTS_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS item_events;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_ITEM_EVENTS_TABLE);
        }

        private void createHitTable(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS hits;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_HITS_TABLE);
        }

        private void createReferrerTable(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS referrer;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_REFERRER_TABLE);
        }

        /* JADX WARNING: Removed duplicated region for block: B:38:0x00ca  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x00f4  */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x0105  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x010e  */
        /* JADX WARNING: Removed duplicated region for block: B:71:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:72:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void fixReferrerTable(android.database.sqlite.SQLiteDatabase r16) {
            /*
                r15 = this;
                r1 = 0
                java.lang.String r3 = "referrer"
                r4 = 0
                r5 = 0
                r6 = 0
                r7 = 0
                r8 = 0
                r9 = 0
                r2 = r16
                android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00de }
                java.lang.String[] r3 = r2.getColumnNames()     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                r4 = 0
                r5 = r4
                r6 = r5
            L_0x0016:
                int r7 = r3.length     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                r8 = 1
                if (r4 >= r7) goto L_0x0034
                r7 = r3[r4]     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.String r9 = "referrer_index"
                boolean r7 = r7.equals(r9)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                if (r7 == 0) goto L_0x0026
                r5 = r8
                goto L_0x0031
            L_0x0026:
                r7 = r3[r4]     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.String r9 = "referrer_visit"
                boolean r7 = r7.equals(r9)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                if (r7 == 0) goto L_0x0031
                r6 = r8
            L_0x0031:
                int r4 = r4 + 1
                goto L_0x0016
            L_0x0034:
                if (r5 == 0) goto L_0x003d
                if (r6 != 0) goto L_0x0039
                goto L_0x003d
            L_0x0039:
                r5 = r16
                goto L_0x00c8
            L_0x003d:
                boolean r3 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                if (r3 == 0) goto L_0x007d
                java.lang.String r3 = "referrer_visit"
                int r3 = r2.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.String r4 = "referrer_index"
                int r4 = r2.getColumnIndex(r4)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                com.google.android.apps.analytics.Referrer r5 = new com.google.android.apps.analytics.Referrer     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.String r6 = "referrer"
                int r6 = r2.getColumnIndex(r6)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.String r10 = r2.getString(r6)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.String r6 = "timestamp_referrer"
                int r6 = r2.getColumnIndex(r6)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                long r11 = r2.getLong(r6)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                r6 = -1
                if (r3 != r6) goto L_0x006a
                r13 = r8
                goto L_0x006f
            L_0x006a:
                int r3 = r2.getInt(r3)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                r13 = r3
            L_0x006f:
                if (r4 != r6) goto L_0x0073
            L_0x0071:
                r14 = r8
                goto L_0x0078
            L_0x0073:
                int r8 = r2.getInt(r4)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                goto L_0x0071
            L_0x0078:
                r9 = r5
                r9.<init>(r10, r11, r13, r14)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                goto L_0x007e
            L_0x007d:
                r5 = r1
            L_0x007e:
                r16.beginTransaction()     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                r15.createReferrerTable(r16)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                if (r5 == 0) goto L_0x00c3
                android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                r3.<init>()     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.String r4 = "referrer"
                java.lang.String r6 = r5.getReferrerString()     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                r3.put(r4, r6)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.String r4 = "timestamp_referrer"
                long r6 = r5.getTimeStamp()     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                r3.put(r4, r6)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.String r4 = "referrer_visit"
                int r6 = r5.getVisit()     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                r3.put(r4, r6)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.String r4 = "referrer_index"
                int r5 = r5.getIndex()     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                r3.put(r4, r5)     // Catch:{ SQLiteException -> 0x00da, all -> 0x00d6 }
                java.lang.String r4 = "referrer"
                r5 = r16
                r5.insert(r4, r1, r3)     // Catch:{ SQLiteException -> 0x00d4 }
                goto L_0x00c5
            L_0x00c3:
                r5 = r16
            L_0x00c5:
                r16.setTransactionSuccessful()     // Catch:{ SQLiteException -> 0x00d4 }
            L_0x00c8:
                if (r2 == 0) goto L_0x00cd
                r2.close()
            L_0x00cd:
                boolean r1 = r16.inTransaction()
                if (r1 == 0) goto L_0x0100
                goto L_0x00fd
            L_0x00d4:
                r0 = move-exception
                goto L_0x00e8
            L_0x00d6:
                r0 = move-exception
                r5 = r16
                goto L_0x00e2
            L_0x00da:
                r0 = move-exception
                r5 = r16
                goto L_0x00e8
            L_0x00de:
                r0 = move-exception
                r5 = r16
                r2 = r1
            L_0x00e2:
                r1 = r0
                goto L_0x0103
            L_0x00e4:
                r0 = move-exception
                r5 = r16
                r2 = r1
            L_0x00e8:
                r1 = r0
                java.lang.String r3 = "GoogleAnalyticsTracker"
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0101 }
                android.util.Log.e(r3, r1)     // Catch:{ all -> 0x0101 }
                if (r2 == 0) goto L_0x00f7
                r2.close()
            L_0x00f7:
                boolean r1 = r16.inTransaction()
                if (r1 == 0) goto L_0x0100
            L_0x00fd:
                boolean unused = com.google.android.apps.analytics.PersistentHitStore.endTransaction(r16)
            L_0x0100:
                return
            L_0x0101:
                r0 = move-exception
                goto L_0x00e2
            L_0x0103:
                if (r2 == 0) goto L_0x0108
                r2.close()
            L_0x0108:
                boolean r2 = r16.inTransaction()
                if (r2 == 0) goto L_0x0111
                boolean unused = com.google.android.apps.analytics.PersistentHitStore.endTransaction(r16)
            L_0x0111:
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.DataBaseHelper.fixReferrerTable(android.database.sqlite.SQLiteDatabase):void");
        }

        private void migrateEventsToHits(SQLiteDatabase sQLiteDatabase, int i) {
            this.store.loadExistingSession(sQLiteDatabase);
            CustomVariableBuffer unused = this.store.visitorCVCache = this.store.getVisitorVarBuffer(sQLiteDatabase);
            Event[] peekEvents = this.store.peekEvents(1000, sQLiteDatabase, i);
            for (Event access$800 : peekEvents) {
                this.store.putEvent(access$800, sQLiteDatabase, false);
            }
            sQLiteDatabase.execSQL("DELETE from events;");
            sQLiteDatabase.execSQL("DELETE from item_events;");
            sQLiteDatabase.execSQL("DELETE from transaction_events;");
            sQLiteDatabase.execSQL("DELETE from custom_variables;");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0093  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0098  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x00a0  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x00a5  */
        /* JADX WARNING: Removed duplicated region for block: B:44:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void migratePreV4Referrer(android.database.sqlite.SQLiteDatabase r21) {
            /*
                r20 = this;
                r1 = 0
                java.lang.String r3 = "install_referrer"
                r10 = 1
                java.lang.String[] r4 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0084, all -> 0x007f }
                java.lang.String r2 = "referrer"
                r11 = 0
                r4[r11] = r2     // Catch:{ SQLiteException -> 0x0084, all -> 0x007f }
                r5 = 0
                r6 = 0
                r7 = 0
                r8 = 0
                r9 = 0
                r2 = r21
                android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ SQLiteException -> 0x0084, all -> 0x007f }
                r3 = 0
                boolean r5 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x007c, all -> 0x0079 }
                if (r5 == 0) goto L_0x006e
                java.lang.String r5 = r2.getString(r11)     // Catch:{ SQLiteException -> 0x007c, all -> 0x0079 }
                java.lang.String r13 = "session"
                r14 = 0
                r15 = 0
                r16 = 0
                r17 = 0
                r18 = 0
                r19 = 0
                r12 = r21
                android.database.Cursor r6 = r12.query(r13, r14, r15, r16, r17, r18, r19)     // Catch:{ SQLiteException -> 0x007c, all -> 0x0079 }
                boolean r7 = r6.moveToFirst()     // Catch:{ SQLiteException -> 0x006c }
                if (r7 == 0) goto L_0x003e
                long r3 = r6.getLong(r11)     // Catch:{ SQLiteException -> 0x006c }
            L_0x003e:
                android.content.ContentValues r7 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x006c }
                r7.<init>()     // Catch:{ SQLiteException -> 0x006c }
                java.lang.String r8 = "referrer"
                r7.put(r8, r5)     // Catch:{ SQLiteException -> 0x006c }
                java.lang.String r5 = "timestamp_referrer"
                java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x006c }
                r7.put(r5, r3)     // Catch:{ SQLiteException -> 0x006c }
                java.lang.String r3 = "referrer_visit"
                java.lang.Integer r4 = java.lang.Integer.valueOf(r10)     // Catch:{ SQLiteException -> 0x006c }
                r7.put(r3, r4)     // Catch:{ SQLiteException -> 0x006c }
                java.lang.String r3 = "referrer_index"
                java.lang.Integer r4 = java.lang.Integer.valueOf(r10)     // Catch:{ SQLiteException -> 0x006c }
                r7.put(r3, r4)     // Catch:{ SQLiteException -> 0x006c }
                java.lang.String r3 = "referrer"
                r4 = r21
                r4.insert(r3, r1, r7)     // Catch:{ SQLiteException -> 0x006c }
                r1 = r6
                goto L_0x006e
            L_0x006c:
                r0 = move-exception
                goto L_0x0087
            L_0x006e:
                if (r2 == 0) goto L_0x0073
                r2.close()
            L_0x0073:
                if (r1 == 0) goto L_0x009b
                r1.close()
                return
            L_0x0079:
                r0 = move-exception
                r6 = r1
                goto L_0x0082
            L_0x007c:
                r0 = move-exception
                r6 = r1
                goto L_0x0087
            L_0x007f:
                r0 = move-exception
                r2 = r1
                r6 = r2
            L_0x0082:
                r1 = r0
                goto L_0x009e
            L_0x0084:
                r0 = move-exception
                r2 = r1
                r6 = r2
            L_0x0087:
                r1 = r0
                java.lang.String r3 = "GoogleAnalyticsTracker"
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x009c }
                android.util.Log.e(r3, r1)     // Catch:{ all -> 0x009c }
                if (r2 == 0) goto L_0x0096
                r2.close()
            L_0x0096:
                if (r6 == 0) goto L_0x009b
                r6.close()
            L_0x009b:
                return
            L_0x009c:
                r0 = move-exception
                goto L_0x0082
            L_0x009e:
                if (r2 == 0) goto L_0x00a3
                r2.close()
            L_0x00a3:
                if (r6 == 0) goto L_0x00a8
                r6.close()
            L_0x00a8:
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.DataBaseHelper.migratePreV4Referrer(android.database.sqlite.SQLiteDatabase):void");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        /* access modifiers changed from: package-private */
        public void createCustomVariableTables(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS custom_variables;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_CUSTOM_VARIABLES_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS custom_var_cache;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_CUSTOM_VAR_CACHE_TABLE);
            for (int i = 1; i <= 5; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(PersistentHitStore.EVENT_ID, (Integer) 0);
                contentValues.put(PersistentHitStore.CUSTOMVAR_INDEX, Integer.valueOf(i));
                contentValues.put(PersistentHitStore.CUSTOMVAR_NAME, "");
                contentValues.put(PersistentHitStore.CUSTOMVAR_SCOPE, (Integer) 3);
                contentValues.put(PersistentHitStore.CUSTOMVAR_VALUE, "");
                sQLiteDatabase.insert("custom_var_cache", PersistentHitStore.EVENT_ID, contentValues);
            }
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS events;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_EVENTS_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS install_referrer;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_INSTALL_REFERRER_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS session;");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_SESSION_TABLE);
            if (this.databaseVersion > 1) {
                createCustomVariableTables(sQLiteDatabase);
            }
            if (this.databaseVersion > 2) {
                createECommerceTables(sQLiteDatabase);
            }
            if (this.databaseVersion > 3) {
                createHitTable(sQLiteDatabase);
                createReferrerTable(sQLiteDatabase);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            Log.w(GoogleAnalyticsTracker.LOG_TAG, "Downgrading database version from " + i + " to " + i2 + " not recommended.");
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_REFERRER_TABLE);
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_HITS_TABLE);
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_CUSTOM_VAR_CACHE_TABLE);
            sQLiteDatabase.execSQL(PersistentHitStore.CREATE_SESSION_TABLE);
            HashSet hashSet = new HashSet();
            Cursor query = sQLiteDatabase.query("custom_var_cache", null, null, null, null, null, null, null);
            while (query.moveToNext()) {
                try {
                    hashSet.add(Integer.valueOf(query.getInt(query.getColumnIndex(PersistentHitStore.CUSTOMVAR_INDEX))));
                } catch (SQLiteException e) {
                    Log.e(GoogleAnalyticsTracker.LOG_TAG, "Error on downgrade: " + e.toString());
                } catch (Throwable th) {
                    query.close();
                    throw th;
                }
            }
            query.close();
            for (int i3 = 1; i3 <= 5; i3++) {
                try {
                    if (!hashSet.contains(Integer.valueOf(i3))) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(PersistentHitStore.EVENT_ID, (Integer) 0);
                        contentValues.put(PersistentHitStore.CUSTOMVAR_INDEX, Integer.valueOf(i3));
                        contentValues.put(PersistentHitStore.CUSTOMVAR_NAME, "");
                        contentValues.put(PersistentHitStore.CUSTOMVAR_SCOPE, (Integer) 3);
                        contentValues.put(PersistentHitStore.CUSTOMVAR_VALUE, "");
                        sQLiteDatabase.insert("custom_var_cache", PersistentHitStore.EVENT_ID, contentValues);
                    }
                } catch (SQLiteException e2) {
                    Log.e(GoogleAnalyticsTracker.LOG_TAG, "Error inserting custom variable on downgrade: " + e2.toString());
                }
            }
        }

        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (sQLiteDatabase.isReadOnly()) {
                Log.w(GoogleAnalyticsTracker.LOG_TAG, "Warning: Need to update database, but it's read only.");
            } else {
                fixReferrerTable(sQLiteDatabase);
            }
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            if (i > i2) {
                onDowngrade(sQLiteDatabase, i, i2);
                return;
            }
            if (i < 2 && i2 > 1) {
                createCustomVariableTables(sQLiteDatabase);
            }
            if (i < 3 && i2 > 2) {
                createECommerceTables(sQLiteDatabase);
            }
            if (i < 4 && i2 > 3) {
                createHitTable(sQLiteDatabase);
                createReferrerTable(sQLiteDatabase);
                migrateEventsToHits(sQLiteDatabase, i);
                migratePreV4Referrer(sQLiteDatabase);
            }
        }
    }

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS session (");
        sb.append(String.format(" '%s' INTEGER PRIMARY KEY,", TIMESTAMP_FIRST));
        sb.append(String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_PREVIOUS));
        sb.append(String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_CURRENT));
        sb.append(String.format(" '%s' INTEGER NOT NULL,", VISITS));
        sb.append(String.format(" '%s' INTEGER NOT NULL);", STORE_ID));
        CREATE_SESSION_TABLE = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("CREATE TABLE IF NOT EXISTS hits (");
        sb2.append(String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", HIT_ID));
        sb2.append(String.format(" '%s' TEXT NOT NULL,", HIT_STRING));
        sb2.append(String.format(" '%s' INTEGER NOT NULL);", HIT_TIMESTAMP));
        CREATE_HITS_TABLE = sb2.toString();
    }

    PersistentHitStore(Context context) {
        this(context, DATABASE_NAME, 5);
    }

    PersistentHitStore(Context context, String str) {
        this(context, str, 5);
    }

    PersistentHitStore(Context context, String str, int i) {
        this.sampleRate = 100;
        this.random = new Random();
        this.databaseHelper = new DataBaseHelper(context, str, i, this);
        loadExistingSession();
        this.visitorCVCache = getVisitorVarBuffer();
    }

    PersistentHitStore(DataBaseHelper dataBaseHelper) {
        this.sampleRate = 100;
        this.random = new Random();
        this.databaseHelper = dataBaseHelper;
        loadExistingSession();
        this.visitorCVCache = getVisitorVarBuffer();
    }

    /* access modifiers changed from: private */
    public static boolean endTransaction(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.endTransaction();
            return true;
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, "exception ending transaction:" + e.toString());
            return false;
        }
    }

    static String formatReferrer(String str) {
        if (str == null) {
            return null;
        }
        if (!str.contains("=")) {
            if (str.contains("%3D")) {
                try {
                    str = URLDecoder.decode(str, "UTF-8");
                } catch (UnsupportedEncodingException unused) {
                }
            }
            return null;
        }
        Map<String, String> parseURLParameters = Utils.parseURLParameters(str);
        boolean z = parseURLParameters.get("utm_campaign") != null;
        boolean z2 = parseURLParameters.get("utm_medium") != null;
        boolean z3 = parseURLParameters.get("utm_source") != null;
        if ((parseURLParameters.get("gclid") != null) || (z && z2 && z3)) {
            String[][] strArr = {new String[]{"utmcid", parseURLParameters.get("utm_id")}, new String[]{"utmcsr", parseURLParameters.get("utm_source")}, new String[]{"utmgclid", parseURLParameters.get("gclid")}, new String[]{"utmccn", parseURLParameters.get("utm_campaign")}, new String[]{"utmcmd", parseURLParameters.get("utm_medium")}, new String[]{"utmctr", parseURLParameters.get("utm_term")}, new String[]{"utmcct", parseURLParameters.get("utm_content")}};
            StringBuilder sb = new StringBuilder();
            boolean z4 = true;
            for (int i = 0; i < strArr.length; i++) {
                if (strArr[i][1] != null) {
                    String replace = strArr[i][1].replace("+", "%20").replace(" ", "%20");
                    if (z4) {
                        z4 = false;
                    } else {
                        sb.append("|");
                    }
                    sb.append(strArr[i][0]);
                    sb.append("=");
                    sb.append(replace);
                }
            }
            return sb.toString();
        }
        Log.w(GoogleAnalyticsTracker.LOG_TAG, "Badly formatted referrer missing campaign, medium and source or click ID");
        return null;
    }

    private Referrer getAndUpdateReferrer(SQLiteDatabase sQLiteDatabase) {
        Referrer readCurrentReferrer = readCurrentReferrer(sQLiteDatabase);
        if (readCurrentReferrer == null) {
            return null;
        }
        if (readCurrentReferrer.getTimeStamp() != 0) {
            return readCurrentReferrer;
        }
        int index = readCurrentReferrer.getIndex();
        String referrerString = readCurrentReferrer.getReferrerString();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TapjoyConstants.TJC_REFERRER, referrerString);
        contentValues.put(TIMESTAMP_REFERRER, Long.valueOf(this.timestampCurrent));
        contentValues.put(REFERRER_VISIT, Integer.valueOf(this.visits));
        contentValues.put(REFERRER_INDEX, Integer.valueOf(index));
        if (setReferrerDatabase(sQLiteDatabase, contentValues)) {
            return new Referrer(referrerString, this.timestampCurrent, this.visits, index);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void putEvent(Event event, SQLiteDatabase sQLiteDatabase, boolean z) {
        if (!event.isSessionInitialized()) {
            event.setRandomVal(this.random.nextInt(Integer.MAX_VALUE));
            event.setTimestampFirst((int) this.timestampFirst);
            event.setTimestampPrevious((int) this.timestampPrevious);
            event.setTimestampCurrent((int) this.timestampCurrent);
            event.setVisits(this.visits);
        }
        event.setAnonymizeIp(this.anonymizeIp);
        if (event.getUserId() == -1) {
            event.setUserId(this.storeId);
        }
        putCustomVariables(event, sQLiteDatabase);
        Referrer andUpdateReferrer = getAndUpdateReferrer(sQLiteDatabase);
        String[] split = event.accountId.split(",");
        if (split.length == 1) {
            writeEventToDatabase(event, andUpdateReferrer, sQLiteDatabase, z);
            return;
        }
        for (String event2 : split) {
            writeEventToDatabase(new Event(event, event2), andUpdateReferrer, sQLiteDatabase, z);
        }
    }

    private boolean setReferrerDatabase(SQLiteDatabase sQLiteDatabase, ContentValues contentValues) {
        try {
            sQLiteDatabase.beginTransaction();
            sQLiteDatabase.delete(TapjoyConstants.TJC_REFERRER, null, null);
            sQLiteDatabase.insert(TapjoyConstants.TJC_REFERRER, null, contentValues);
            sQLiteDatabase.setTransactionSuccessful();
            return !sQLiteDatabase.inTransaction() || endTransaction(sQLiteDatabase);
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
            if (!sQLiteDatabase.inTransaction() || !endTransaction(sQLiteDatabase)) {
            }
            return false;
        } catch (Throwable th) {
            if (sQLiteDatabase.inTransaction() && !endTransaction(sQLiteDatabase)) {
                return false;
            }
            throw th;
        }
    }

    public void clearReferrer() {
        try {
            this.databaseHelper.getWritableDatabase().delete(TapjoyConstants.TJC_REFERRER, null, null);
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }

    public synchronized void deleteHit(long j) {
        try {
            this.numStoredHits -= this.databaseHelper.getWritableDatabase().delete("hits", "hit_id = ?", new String[]{Long.toString(j)});
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
        return;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0074  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.CustomVariableBuffer getCustomVariables(long r11, android.database.sqlite.SQLiteDatabase r13) {
        /*
            r10 = this;
            com.google.android.apps.analytics.CustomVariableBuffer r0 = new com.google.android.apps.analytics.CustomVariableBuffer
            r0.<init>()
            r1 = 0
            java.lang.String r3 = "custom_variables"
            r4 = 0
            java.lang.String r5 = "event_id= ?"
            r2 = 1
            java.lang.String[] r6 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x0062 }
            r2 = 0
            java.lang.String r11 = java.lang.Long.toString(r11)     // Catch:{ SQLiteException -> 0x0062 }
            r6[r2] = r11     // Catch:{ SQLiteException -> 0x0062 }
            r7 = 0
            r8 = 0
            r9 = 0
            r2 = r13
            android.database.Cursor r11 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ SQLiteException -> 0x0062 }
        L_0x001d:
            boolean r12 = r11.moveToNext()     // Catch:{ SQLiteException -> 0x005c, all -> 0x005a }
            if (r12 == 0) goto L_0x0054
            com.google.android.apps.analytics.CustomVariable r12 = new com.google.android.apps.analytics.CustomVariable     // Catch:{ SQLiteException -> 0x005c, all -> 0x005a }
            java.lang.String r13 = "cv_index"
            int r13 = r11.getColumnIndex(r13)     // Catch:{ SQLiteException -> 0x005c, all -> 0x005a }
            int r13 = r11.getInt(r13)     // Catch:{ SQLiteException -> 0x005c, all -> 0x005a }
            java.lang.String r1 = "cv_name"
            int r1 = r11.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x005c, all -> 0x005a }
            java.lang.String r1 = r11.getString(r1)     // Catch:{ SQLiteException -> 0x005c, all -> 0x005a }
            java.lang.String r2 = "cv_value"
            int r2 = r11.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x005c, all -> 0x005a }
            java.lang.String r2 = r11.getString(r2)     // Catch:{ SQLiteException -> 0x005c, all -> 0x005a }
            java.lang.String r3 = "cv_scope"
            int r3 = r11.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x005c, all -> 0x005a }
            int r3 = r11.getInt(r3)     // Catch:{ SQLiteException -> 0x005c, all -> 0x005a }
            r12.<init>(r13, r1, r2, r3)     // Catch:{ SQLiteException -> 0x005c, all -> 0x005a }
            r0.setCustomVariable(r12)     // Catch:{ SQLiteException -> 0x005c, all -> 0x005a }
            goto L_0x001d
        L_0x0054:
            if (r11 == 0) goto L_0x0071
            r11.close()
            return r0
        L_0x005a:
            r12 = move-exception
            goto L_0x0072
        L_0x005c:
            r12 = move-exception
            r1 = r11
            goto L_0x0063
        L_0x005f:
            r12 = move-exception
            r11 = r1
            goto L_0x0072
        L_0x0062:
            r12 = move-exception
        L_0x0063:
            java.lang.String r11 = "GoogleAnalyticsTracker"
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x005f }
            android.util.Log.e(r11, r12)     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x0071
            r1.close()
        L_0x0071:
            return r0
        L_0x0072:
            if (r11 == 0) goto L_0x0077
            r11.close()
        L_0x0077:
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getCustomVariables(long, android.database.sqlite.SQLiteDatabase):com.google.android.apps.analytics.CustomVariableBuffer");
    }

    /* access modifiers changed from: package-private */
    public DataBaseHelper getDatabaseHelper() {
        return this.databaseHelper;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0072, code lost:
        if (r10 != null) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0085, code lost:
        if (r10 != null) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0087, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008a, code lost:
        return null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Item getItem(long r10, android.database.sqlite.SQLiteDatabase r12) {
        /*
            r9 = this;
            r0 = 0
            java.lang.String r2 = "item_events"
            r3 = 0
            java.lang.String r4 = "event_id= ?"
            r1 = 1
            java.lang.String[] r5 = new java.lang.String[r1]     // Catch:{ SQLiteException -> 0x007a, all -> 0x0077 }
            r1 = 0
            java.lang.String r10 = java.lang.Long.toString(r10)     // Catch:{ SQLiteException -> 0x007a, all -> 0x0077 }
            r5[r1] = r10     // Catch:{ SQLiteException -> 0x007a, all -> 0x0077 }
            r6 = 0
            r7 = 0
            r8 = 0
            r1 = r12
            android.database.Cursor r10 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x007a, all -> 0x0077 }
            boolean r11 = r10.moveToFirst()     // Catch:{ SQLiteException -> 0x0075 }
            if (r11 == 0) goto L_0x0072
            com.google.android.apps.analytics.Item$Builder r11 = new com.google.android.apps.analytics.Item$Builder     // Catch:{ SQLiteException -> 0x0075 }
            java.lang.String r12 = "order_id"
            int r12 = r10.getColumnIndex(r12)     // Catch:{ SQLiteException -> 0x0075 }
            java.lang.String r2 = r10.getString(r12)     // Catch:{ SQLiteException -> 0x0075 }
            java.lang.String r12 = "item_sku"
            int r12 = r10.getColumnIndex(r12)     // Catch:{ SQLiteException -> 0x0075 }
            java.lang.String r3 = r10.getString(r12)     // Catch:{ SQLiteException -> 0x0075 }
            java.lang.String r12 = "item_price"
            int r12 = r10.getColumnIndex(r12)     // Catch:{ SQLiteException -> 0x0075 }
            double r4 = r10.getDouble(r12)     // Catch:{ SQLiteException -> 0x0075 }
            java.lang.String r12 = "item_count"
            int r12 = r10.getColumnIndex(r12)     // Catch:{ SQLiteException -> 0x0075 }
            long r6 = r10.getLong(r12)     // Catch:{ SQLiteException -> 0x0075 }
            r1 = r11
            r1.<init>(r2, r3, r4, r6)     // Catch:{ SQLiteException -> 0x0075 }
            java.lang.String r12 = "item_name"
            int r12 = r10.getColumnIndex(r12)     // Catch:{ SQLiteException -> 0x0075 }
            java.lang.String r12 = r10.getString(r12)     // Catch:{ SQLiteException -> 0x0075 }
            com.google.android.apps.analytics.Item$Builder r11 = r11.setItemName(r12)     // Catch:{ SQLiteException -> 0x0075 }
            java.lang.String r12 = "item_category"
            int r12 = r10.getColumnIndex(r12)     // Catch:{ SQLiteException -> 0x0075 }
            java.lang.String r12 = r10.getString(r12)     // Catch:{ SQLiteException -> 0x0075 }
            com.google.android.apps.analytics.Item$Builder r11 = r11.setItemCategory(r12)     // Catch:{ SQLiteException -> 0x0075 }
            com.google.android.apps.analytics.Item r11 = r11.build()     // Catch:{ SQLiteException -> 0x0075 }
            if (r10 == 0) goto L_0x0071
            r10.close()
        L_0x0071:
            return r11
        L_0x0072:
            if (r10 == 0) goto L_0x008a
            goto L_0x0087
        L_0x0075:
            r11 = move-exception
            goto L_0x007c
        L_0x0077:
            r11 = move-exception
            r10 = r0
            goto L_0x008c
        L_0x007a:
            r11 = move-exception
            r10 = r0
        L_0x007c:
            java.lang.String r12 = "GoogleAnalyticsTracker"
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x008b }
            android.util.Log.e(r12, r11)     // Catch:{ all -> 0x008b }
            if (r10 == 0) goto L_0x008a
        L_0x0087:
            r10.close()
        L_0x008a:
            return r0
        L_0x008b:
            r11 = move-exception
        L_0x008c:
            if (r10 == 0) goto L_0x0091
            r10.close()
        L_0x0091:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getItem(long, android.database.sqlite.SQLiteDatabase):com.google.android.apps.analytics.Item");
    }

    public int getNumStoredHits() {
        return this.numStoredHits;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x003d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getNumStoredHitsFromDb() {
        /*
            r6 = this;
            r0 = 0
            r1 = 0
            com.google.android.apps.analytics.PersistentHitStore$DataBaseHelper r2 = r6.databaseHelper     // Catch:{ SQLiteException -> 0x002b }
            android.database.sqlite.SQLiteDatabase r2 = r2.getReadableDatabase()     // Catch:{ SQLiteException -> 0x002b }
            java.lang.String r3 = "SELECT COUNT(*) from hits"
            android.database.Cursor r2 = r2.rawQuery(r3, r0)     // Catch:{ SQLiteException -> 0x002b }
            boolean r0 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x0024, all -> 0x0020 }
            if (r0 == 0) goto L_0x001a
            long r3 = r2.getLong(r1)     // Catch:{ SQLiteException -> 0x0024, all -> 0x0020 }
            int r0 = (int) r3
            r1 = r0
        L_0x001a:
            if (r2 == 0) goto L_0x003a
            r2.close()
            return r1
        L_0x0020:
            r0 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x003b
        L_0x0024:
            r0 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x002c
        L_0x0029:
            r1 = move-exception
            goto L_0x003b
        L_0x002b:
            r2 = move-exception
        L_0x002c:
            java.lang.String r3 = "GoogleAnalyticsTracker"
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0029 }
            android.util.Log.e(r3, r2)     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x003a
            r0.close()
        L_0x003a:
            return r1
        L_0x003b:
            if (r0 == 0) goto L_0x0040
            r0.close()
        L_0x0040:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getNumStoredHitsFromDb():int");
    }

    public Referrer getReferrer() {
        try {
            return readCurrentReferrer(this.databaseHelper.getReadableDatabase());
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
            return null;
        }
    }

    public String getSessionId() {
        if (!this.sessionStarted) {
            return null;
        }
        return Integer.toString((int) this.timestampCurrent);
    }

    public int getStoreId() {
        return this.storeId;
    }

    /* access modifiers changed from: package-private */
    public long getTimestampCurrent() {
        return this.timestampCurrent;
    }

    /* access modifiers changed from: package-private */
    public long getTimestampFirst() {
        return this.timestampFirst;
    }

    /* access modifiers changed from: package-private */
    public long getTimestampPrevious() {
        return this.timestampPrevious;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x006b, code lost:
        if (r10 != null) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x007e, code lost:
        if (r10 != null) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0080, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0083, code lost:
        return null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0087  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Transaction getTransaction(long r10, android.database.sqlite.SQLiteDatabase r12) {
        /*
            r9 = this;
            r0 = 0
            java.lang.String r2 = "transaction_events"
            r3 = 0
            java.lang.String r4 = "event_id= ?"
            r1 = 1
            java.lang.String[] r5 = new java.lang.String[r1]     // Catch:{ SQLiteException -> 0x0073, all -> 0x0070 }
            r1 = 0
            java.lang.String r10 = java.lang.Long.toString(r10)     // Catch:{ SQLiteException -> 0x0073, all -> 0x0070 }
            r5[r1] = r10     // Catch:{ SQLiteException -> 0x0073, all -> 0x0070 }
            r6 = 0
            r7 = 0
            r8 = 0
            r1 = r12
            android.database.Cursor r10 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x0073, all -> 0x0070 }
            boolean r11 = r10.moveToFirst()     // Catch:{ SQLiteException -> 0x006e }
            if (r11 == 0) goto L_0x006b
            com.google.android.apps.analytics.Transaction$Builder r11 = new com.google.android.apps.analytics.Transaction$Builder     // Catch:{ SQLiteException -> 0x006e }
            java.lang.String r12 = "order_id"
            int r12 = r10.getColumnIndex(r12)     // Catch:{ SQLiteException -> 0x006e }
            java.lang.String r12 = r10.getString(r12)     // Catch:{ SQLiteException -> 0x006e }
            java.lang.String r1 = "tran_totalcost"
            int r1 = r10.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x006e }
            double r1 = r10.getDouble(r1)     // Catch:{ SQLiteException -> 0x006e }
            r11.<init>(r12, r1)     // Catch:{ SQLiteException -> 0x006e }
            java.lang.String r12 = "tran_storename"
            int r12 = r10.getColumnIndex(r12)     // Catch:{ SQLiteException -> 0x006e }
            java.lang.String r12 = r10.getString(r12)     // Catch:{ SQLiteException -> 0x006e }
            com.google.android.apps.analytics.Transaction$Builder r11 = r11.setStoreName(r12)     // Catch:{ SQLiteException -> 0x006e }
            java.lang.String r12 = "tran_totaltax"
            int r12 = r10.getColumnIndex(r12)     // Catch:{ SQLiteException -> 0x006e }
            double r1 = r10.getDouble(r12)     // Catch:{ SQLiteException -> 0x006e }
            com.google.android.apps.analytics.Transaction$Builder r11 = r11.setTotalTax(r1)     // Catch:{ SQLiteException -> 0x006e }
            java.lang.String r12 = "tran_shippingcost"
            int r12 = r10.getColumnIndex(r12)     // Catch:{ SQLiteException -> 0x006e }
            double r1 = r10.getDouble(r12)     // Catch:{ SQLiteException -> 0x006e }
            com.google.android.apps.analytics.Transaction$Builder r11 = r11.setShippingCost(r1)     // Catch:{ SQLiteException -> 0x006e }
            com.google.android.apps.analytics.Transaction r11 = r11.build()     // Catch:{ SQLiteException -> 0x006e }
            if (r10 == 0) goto L_0x006a
            r10.close()
        L_0x006a:
            return r11
        L_0x006b:
            if (r10 == 0) goto L_0x0083
            goto L_0x0080
        L_0x006e:
            r11 = move-exception
            goto L_0x0075
        L_0x0070:
            r11 = move-exception
            r10 = r0
            goto L_0x0085
        L_0x0073:
            r11 = move-exception
            r10 = r0
        L_0x0075:
            java.lang.String r12 = "GoogleAnalyticsTracker"
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x0084 }
            android.util.Log.e(r12, r11)     // Catch:{ all -> 0x0084 }
            if (r10 == 0) goto L_0x0083
        L_0x0080:
            r10.close()
        L_0x0083:
            return r0
        L_0x0084:
            r11 = move-exception
        L_0x0085:
            if (r10 == 0) goto L_0x008a
            r10.close()
        L_0x008a:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getTransaction(long, android.database.sqlite.SQLiteDatabase):com.google.android.apps.analytics.Transaction");
    }

    public String getVisitorCustomVar(int i) {
        CustomVariable customVariableAt = this.visitorCVCache.getCustomVariableAt(i);
        if (customVariableAt == null || customVariableAt.getScope() != 1) {
            return null;
        }
        return customVariableAt.getValue();
    }

    public String getVisitorId() {
        if (!this.sessionStarted) {
            return null;
        }
        return String.format("%d.%d", Integer.valueOf(this.storeId), Long.valueOf(this.timestampFirst));
    }

    /* access modifiers changed from: package-private */
    public CustomVariableBuffer getVisitorVarBuffer() {
        try {
            return getVisitorVarBuffer(this.databaseHelper.getReadableDatabase());
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
            return new CustomVariableBuffer();
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0069, code lost:
        if (r12 != null) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x006b, code lost:
        r12.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006e, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0054, code lost:
        if (r12 != null) goto L_0x006b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0072  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.CustomVariableBuffer getVisitorVarBuffer(android.database.sqlite.SQLiteDatabase r12) {
        /*
            r11 = this;
            com.google.android.apps.analytics.CustomVariableBuffer r0 = new com.google.android.apps.analytics.CustomVariableBuffer
            r0.<init>()
            r1 = 0
            java.lang.String r3 = "custom_var_cache"
            r4 = 0
            java.lang.String r5 = "cv_scope= ?"
            r2 = 1
            java.lang.String[] r6 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x005c, all -> 0x0059 }
            r7 = 0
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ SQLiteException -> 0x005c, all -> 0x0059 }
            r6[r7] = r2     // Catch:{ SQLiteException -> 0x005c, all -> 0x0059 }
            r7 = 0
            r8 = 0
            r9 = 0
            r2 = r12
            android.database.Cursor r12 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ SQLiteException -> 0x005c, all -> 0x0059 }
        L_0x001d:
            boolean r1 = r12.moveToNext()     // Catch:{ SQLiteException -> 0x0057 }
            if (r1 == 0) goto L_0x0054
            com.google.android.apps.analytics.CustomVariable r1 = new com.google.android.apps.analytics.CustomVariable     // Catch:{ SQLiteException -> 0x0057 }
            java.lang.String r2 = "cv_index"
            int r2 = r12.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0057 }
            int r2 = r12.getInt(r2)     // Catch:{ SQLiteException -> 0x0057 }
            java.lang.String r3 = "cv_name"
            int r3 = r12.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x0057 }
            java.lang.String r3 = r12.getString(r3)     // Catch:{ SQLiteException -> 0x0057 }
            java.lang.String r4 = "cv_value"
            int r4 = r12.getColumnIndex(r4)     // Catch:{ SQLiteException -> 0x0057 }
            java.lang.String r4 = r12.getString(r4)     // Catch:{ SQLiteException -> 0x0057 }
            java.lang.String r5 = "cv_scope"
            int r5 = r12.getColumnIndex(r5)     // Catch:{ SQLiteException -> 0x0057 }
            int r5 = r12.getInt(r5)     // Catch:{ SQLiteException -> 0x0057 }
            r1.<init>(r2, r3, r4, r5)     // Catch:{ SQLiteException -> 0x0057 }
            r0.setCustomVariable(r1)     // Catch:{ SQLiteException -> 0x0057 }
            goto L_0x001d
        L_0x0054:
            if (r12 == 0) goto L_0x006e
            goto L_0x006b
        L_0x0057:
            r1 = move-exception
            goto L_0x0060
        L_0x0059:
            r0 = move-exception
            r12 = r1
            goto L_0x0070
        L_0x005c:
            r12 = move-exception
            r10 = r1
            r1 = r12
            r12 = r10
        L_0x0060:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x006f }
            android.util.Log.e(r2, r1)     // Catch:{ all -> 0x006f }
            if (r12 == 0) goto L_0x006e
        L_0x006b:
            r12.close()
        L_0x006e:
            return r0
        L_0x006f:
            r0 = move-exception
        L_0x0070:
            if (r12 == 0) goto L_0x0075
            r12.close()
        L_0x0075:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.getVisitorVarBuffer(android.database.sqlite.SQLiteDatabase):com.google.android.apps.analytics.CustomVariableBuffer");
    }

    /* access modifiers changed from: package-private */
    public int getVisits() {
        return this.visits;
    }

    public void loadExistingSession() {
        try {
            loadExistingSession(this.databaseHelper.getWritableDatabase());
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
     arg types: [java.lang.String, long]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadExistingSession(android.database.sqlite.SQLiteDatabase r10) {
        /*
            r9 = this;
            r0 = 0
            java.lang.String r2 = "session"
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r1 = r10
            android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x00ad }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r3 = 1
            r4 = 0
            r5 = 0
            if (r2 == 0) goto L_0x0053
            long r7 = r1.getLong(r4)     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r9.timestampFirst = r7     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            long r7 = r1.getLong(r3)     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r9.timestampPrevious = r7     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r0 = 2
            long r7 = r1.getLong(r0)     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r9.timestampCurrent = r7     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r0 = 3
            int r0 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r9.visits = r0     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r0 = 4
            int r0 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r9.storeId = r0     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            com.google.android.apps.analytics.Referrer r10 = r9.readCurrentReferrer(r10)     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            long r7 = r9.timestampFirst     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            int r0 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x004e
            if (r10 == 0) goto L_0x004f
            long r7 = r10.getTimeStamp()     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            int r10 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r10 == 0) goto L_0x004e
            goto L_0x004f
        L_0x004e:
            r3 = r4
        L_0x004f:
            r9.sessionStarted = r3     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r0 = r1
            goto L_0x00a2
        L_0x0053:
            r9.sessionStarted = r4     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r9.useStoredVisitorVars = r3     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            java.security.SecureRandom r2 = new java.security.SecureRandom     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r2.<init>()     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            int r2 = r2.nextInt()     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r3 = 2147483647(0x7fffffff, float:NaN)
            r2 = r2 & r3
            r9.storeId = r2     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            r1.close()     // Catch:{ SQLiteException -> 0x00a8, all -> 0x00a5 }
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x00ad }
            r1.<init>()     // Catch:{ SQLiteException -> 0x00ad }
            java.lang.String r2 = "timestamp_first"
            java.lang.Long r3 = java.lang.Long.valueOf(r5)     // Catch:{ SQLiteException -> 0x00ad }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x00ad }
            java.lang.String r2 = "timestamp_previous"
            java.lang.Long r3 = java.lang.Long.valueOf(r5)     // Catch:{ SQLiteException -> 0x00ad }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x00ad }
            java.lang.String r2 = "timestamp_current"
            java.lang.Long r3 = java.lang.Long.valueOf(r5)     // Catch:{ SQLiteException -> 0x00ad }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x00ad }
            java.lang.String r2 = "visits"
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)     // Catch:{ SQLiteException -> 0x00ad }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x00ad }
            java.lang.String r2 = "store_id"
            int r3 = r9.storeId     // Catch:{ SQLiteException -> 0x00ad }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x00ad }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x00ad }
            java.lang.String r2 = "session"
            r10.insert(r2, r0, r1)     // Catch:{ SQLiteException -> 0x00ad }
        L_0x00a2:
            if (r0 == 0) goto L_0x00bc
            goto L_0x00b9
        L_0x00a5:
            r10 = move-exception
            r0 = r1
            goto L_0x00bd
        L_0x00a8:
            r10 = move-exception
            r0 = r1
            goto L_0x00ae
        L_0x00ab:
            r10 = move-exception
            goto L_0x00bd
        L_0x00ad:
            r10 = move-exception
        L_0x00ae:
            java.lang.String r1 = "GoogleAnalyticsTracker"
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x00ab }
            android.util.Log.e(r1, r10)     // Catch:{ all -> 0x00ab }
            if (r0 == 0) goto L_0x00bc
        L_0x00b9:
            r0.close()
        L_0x00bc:
            return
        L_0x00bd:
            if (r0 == 0) goto L_0x00c2
            r0.close()
        L_0x00c2:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.loadExistingSession(android.database.sqlite.SQLiteDatabase):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x011d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Event[] peekEvents(int r30, android.database.sqlite.SQLiteDatabase r31, int r32) {
        /*
            r29 = this;
            r1 = r29
            r11 = r31
            java.util.ArrayList r12 = new java.util.ArrayList
            r12.<init>()
            r13 = 0
            r14 = 0
            java.lang.String r3 = "events"
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "event_id"
            java.lang.String r10 = java.lang.Integer.toString(r30)     // Catch:{ SQLiteException -> 0x0108 }
            r2 = r11
            android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x0108 }
        L_0x001d:
            boolean r3 = r2.moveToNext()     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            if (r3 == 0) goto L_0x00ed
            com.google.android.apps.analytics.Event r3 = new com.google.android.apps.analytics.Event     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            long r15 = r2.getLong(r13)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4 = 2
            java.lang.String r17 = r2.getString(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4 = 3
            int r18 = r2.getInt(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4 = 4
            int r19 = r2.getInt(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4 = 5
            int r20 = r2.getInt(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4 = 6
            int r21 = r2.getInt(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4 = 7
            int r22 = r2.getInt(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4 = 8
            java.lang.String r23 = r2.getString(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4 = 9
            java.lang.String r24 = r2.getString(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4 = 10
            java.lang.String r25 = r2.getString(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4 = 11
            int r26 = r2.getInt(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4 = 12
            int r27 = r2.getInt(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4 = 13
            int r28 = r2.getInt(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r14 = r3
            r14.<init>(r15, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4 = 1
            int r5 = r2.getInt(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r3.setUserId(r5)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            java.lang.String r5 = "event_id"
            int r5 = r2.getColumnIndex(r5)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            long r5 = r2.getLong(r5)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            java.lang.String r7 = "__##GOOGLETRANSACTION##__"
            java.lang.String r8 = r3.category     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            boolean r7 = r7.equals(r8)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            if (r7 == 0) goto L_0x00ad
            com.google.android.apps.analytics.Transaction r4 = r1.getTransaction(r5, r11)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            if (r4 != 0) goto L_0x00a7
            java.lang.String r7 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r8.<init>()     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            java.lang.String r9 = "missing expected transaction for event "
            r8.append(r9)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r8.append(r5)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            java.lang.String r5 = r8.toString()     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            android.util.Log.w(r7, r5)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
        L_0x00a7:
            r3.setTransaction(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
        L_0x00aa:
            r7 = r32
            goto L_0x00e8
        L_0x00ad:
            java.lang.String r7 = "__##GOOGLEITEM##__"
            java.lang.String r8 = r3.category     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            boolean r7 = r7.equals(r8)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            if (r7 == 0) goto L_0x00d7
            com.google.android.apps.analytics.Item r4 = r1.getItem(r5, r11)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            if (r4 != 0) goto L_0x00d3
            java.lang.String r7 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r8.<init>()     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            java.lang.String r9 = "missing expected item for event "
            r8.append(r9)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r8.append(r5)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            java.lang.String r5 = r8.toString()     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            android.util.Log.w(r7, r5)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
        L_0x00d3:
            r3.setItem(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            goto L_0x00aa
        L_0x00d7:
            r7 = r32
            if (r7 <= r4) goto L_0x00e0
            com.google.android.apps.analytics.CustomVariableBuffer r4 = r1.getCustomVariables(r5, r11)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            goto L_0x00e5
        L_0x00e0:
            com.google.android.apps.analytics.CustomVariableBuffer r4 = new com.google.android.apps.analytics.CustomVariableBuffer     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            r4.<init>()     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
        L_0x00e5:
            r3.setCustomVariableBuffer(r4)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
        L_0x00e8:
            r12.add(r3)     // Catch:{ SQLiteException -> 0x0102, all -> 0x00ff }
            goto L_0x001d
        L_0x00ed:
            if (r2 == 0) goto L_0x00f2
            r2.close()
        L_0x00f2:
            int r2 = r12.size()
            com.google.android.apps.analytics.Event[] r2 = new com.google.android.apps.analytics.Event[r2]
            java.lang.Object[] r2 = r12.toArray(r2)
            com.google.android.apps.analytics.Event[] r2 = (com.google.android.apps.analytics.Event[]) r2
            return r2
        L_0x00ff:
            r0 = move-exception
            r14 = r2
            goto L_0x0106
        L_0x0102:
            r0 = move-exception
            r14 = r2
            goto L_0x0109
        L_0x0105:
            r0 = move-exception
        L_0x0106:
            r2 = r0
            goto L_0x011b
        L_0x0108:
            r0 = move-exception
        L_0x0109:
            r2 = r0
            java.lang.String r3 = "GoogleAnalyticsTracker"
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0105 }
            android.util.Log.e(r3, r2)     // Catch:{ all -> 0x0105 }
            com.google.android.apps.analytics.Event[] r2 = new com.google.android.apps.analytics.Event[r13]     // Catch:{ all -> 0x0105 }
            if (r14 == 0) goto L_0x011a
            r14.close()
        L_0x011a:
            return r2
        L_0x011b:
            if (r14 == 0) goto L_0x0120
            r14.close()
        L_0x0120:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.peekEvents(int, android.database.sqlite.SQLiteDatabase, int):com.google.android.apps.analytics.Event[]");
    }

    public Hit[] peekHits() {
        return peekHits(1000);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0064  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Hit[] peekHits(int r14) {
        /*
            r13 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
            r2 = 0
            com.google.android.apps.analytics.PersistentHitStore$DataBaseHelper r3 = r13.databaseHelper     // Catch:{ SQLiteException -> 0x0050 }
            android.database.sqlite.SQLiteDatabase r4 = r3.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0050 }
            java.lang.String r5 = "hits"
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            java.lang.String r11 = "hit_id"
            java.lang.String r12 = java.lang.Integer.toString(r14)     // Catch:{ SQLiteException -> 0x0050 }
            android.database.Cursor r14 = r4.query(r5, r6, r7, r8, r9, r10, r11, r12)     // Catch:{ SQLiteException -> 0x0050 }
        L_0x001e:
            boolean r2 = r14.moveToNext()     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            if (r2 == 0) goto L_0x0036
            com.google.android.apps.analytics.Hit r2 = new com.google.android.apps.analytics.Hit     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            r3 = 1
            java.lang.String r3 = r14.getString(r3)     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            long r4 = r14.getLong(r1)     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            r2.<init>(r3, r4)     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            r0.add(r2)     // Catch:{ SQLiteException -> 0x004b, all -> 0x0048 }
            goto L_0x001e
        L_0x0036:
            if (r14 == 0) goto L_0x003b
            r14.close()
        L_0x003b:
            int r14 = r0.size()
            com.google.android.apps.analytics.Hit[] r14 = new com.google.android.apps.analytics.Hit[r14]
            java.lang.Object[] r14 = r0.toArray(r14)
            com.google.android.apps.analytics.Hit[] r14 = (com.google.android.apps.analytics.Hit[]) r14
            return r14
        L_0x0048:
            r0 = move-exception
            r2 = r14
            goto L_0x0062
        L_0x004b:
            r0 = move-exception
            r2 = r14
            goto L_0x0051
        L_0x004e:
            r0 = move-exception
            goto L_0x0062
        L_0x0050:
            r0 = move-exception
        L_0x0051:
            java.lang.String r14 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x004e }
            android.util.Log.e(r14, r0)     // Catch:{ all -> 0x004e }
            com.google.android.apps.analytics.Hit[] r14 = new com.google.android.apps.analytics.Hit[r1]     // Catch:{ all -> 0x004e }
            if (r2 == 0) goto L_0x0061
            r2.close()
        L_0x0061:
            return r14
        L_0x0062:
            if (r2 == 0) goto L_0x0067
            r2.close()
        L_0x0067:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.peekHits(int):com.google.android.apps.analytics.Hit[]");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: package-private */
    public void putCustomVariables(Event event, SQLiteDatabase sQLiteDatabase) {
        if (!"__##GOOGLEITEM##__".equals(event.category) && !"__##GOOGLETRANSACTION##__".equals(event.category)) {
            try {
                CustomVariableBuffer customVariableBuffer = event.getCustomVariableBuffer();
                if (this.useStoredVisitorVars) {
                    if (customVariableBuffer == null) {
                        customVariableBuffer = new CustomVariableBuffer();
                        event.setCustomVariableBuffer(customVariableBuffer);
                    }
                    for (int i = 1; i <= 5; i++) {
                        CustomVariable customVariableAt = this.visitorCVCache.getCustomVariableAt(i);
                        CustomVariable customVariableAt2 = customVariableBuffer.getCustomVariableAt(i);
                        if (customVariableAt != null && customVariableAt2 == null) {
                            customVariableBuffer.setCustomVariable(customVariableAt);
                        }
                    }
                    this.useStoredVisitorVars = false;
                }
                if (customVariableBuffer != null) {
                    for (int i2 = 1; i2 <= 5; i2++) {
                        if (!customVariableBuffer.isIndexAvailable(i2)) {
                            CustomVariable customVariableAt3 = customVariableBuffer.getCustomVariableAt(i2);
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(EVENT_ID, (Integer) 0);
                            contentValues.put(CUSTOMVAR_INDEX, Integer.valueOf(customVariableAt3.getIndex()));
                            contentValues.put(CUSTOMVAR_NAME, customVariableAt3.getName());
                            contentValues.put(CUSTOMVAR_SCOPE, Integer.valueOf(customVariableAt3.getScope()));
                            contentValues.put(CUSTOMVAR_VALUE, customVariableAt3.getValue());
                            sQLiteDatabase.update("custom_var_cache", contentValues, "cv_index = ?", new String[]{Integer.toString(customVariableAt3.getIndex())});
                            if (customVariableAt3.getScope() == 1) {
                                this.visitorCVCache.setCustomVariable(customVariableAt3);
                            } else {
                                this.visitorCVCache.clearCustomVariableAt(customVariableAt3.getIndex());
                            }
                        }
                    }
                }
            } catch (SQLiteException e) {
                Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0057, code lost:
        if (r0.inTransaction() != false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005d, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0087, code lost:
        if (r0.inTransaction() != false) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0089, code lost:
        endTransaction(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x008c, code lost:
        throw r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x008d, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ad, code lost:
        throw r5;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:23:0x0053, B:30:0x0062] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void putEvent(com.google.android.apps.analytics.Event r5) {
        /*
            r4 = this;
            int r0 = r4.numStoredHits
            r1 = 1000(0x3e8, float:1.401E-42)
            if (r0 < r1) goto L_0x000e
            java.lang.String r5 = "GoogleAnalyticsTracker"
            java.lang.String r0 = "Store full. Not storing last event."
            android.util.Log.w(r5, r0)
            return
        L_0x000e:
            int r0 = r4.sampleRate
            r1 = 100
            if (r0 == r1) goto L_0x003b
            int r0 = r5.getUserId()
            r2 = -1
            if (r0 != r2) goto L_0x001e
            int r0 = r4.storeId
            goto L_0x0022
        L_0x001e:
            int r0 = r5.getUserId()
        L_0x0022:
            int r0 = r0 % 10000
            int r2 = r4.sampleRate
            int r2 = r2 * r1
            if (r0 < r2) goto L_0x003b
            com.google.android.apps.analytics.GoogleAnalyticsTracker r5 = com.google.android.apps.analytics.GoogleAnalyticsTracker.getInstance()
            boolean r5 = r5.getDebug()
            if (r5 == 0) goto L_0x003a
            java.lang.String r5 = "GoogleAnalyticsTracker"
            java.lang.String r0 = "User has been sampled out. Aborting hit."
            android.util.Log.v(r5, r0)
        L_0x003a:
            return
        L_0x003b:
            monitor-enter(r4)
            com.google.android.apps.analytics.PersistentHitStore$DataBaseHelper r0 = r4.databaseHelper     // Catch:{ SQLiteException -> 0x008f }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ SQLiteException -> 0x008f }
            r0.beginTransaction()     // Catch:{ SQLiteException -> 0x005f }
            boolean r1 = r4.sessionStarted     // Catch:{ SQLiteException -> 0x005f }
            if (r1 != 0) goto L_0x004c
            r4.storeUpdatedSession(r0)     // Catch:{ SQLiteException -> 0x005f }
        L_0x004c:
            r1 = 1
            r4.putEvent(r5, r0, r1)     // Catch:{ SQLiteException -> 0x005f }
            r0.setTransactionSuccessful()     // Catch:{ SQLiteException -> 0x005f }
            boolean r5 = r0.inTransaction()     // Catch:{ all -> 0x008d }
            if (r5 == 0) goto L_0x0081
        L_0x0059:
            endTransaction(r0)     // Catch:{ all -> 0x008d }
            goto L_0x0081
        L_0x005d:
            r5 = move-exception
            goto L_0x0083
        L_0x005f:
            r5 = move-exception
            java.lang.String r1 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x005d }
            r2.<init>()     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "putEventOuter:"
            r2.append(r3)     // Catch:{ all -> 0x005d }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x005d }
            r2.append(r5)     // Catch:{ all -> 0x005d }
            java.lang.String r5 = r2.toString()     // Catch:{ all -> 0x005d }
            android.util.Log.e(r1, r5)     // Catch:{ all -> 0x005d }
            boolean r5 = r0.inTransaction()     // Catch:{ all -> 0x008d }
            if (r5 == 0) goto L_0x0081
            goto L_0x0059
        L_0x0081:
            monitor-exit(r4)     // Catch:{ all -> 0x008d }
            return
        L_0x0083:
            boolean r1 = r0.inTransaction()     // Catch:{ all -> 0x008d }
            if (r1 == 0) goto L_0x008c
            endTransaction(r0)     // Catch:{ all -> 0x008d }
        L_0x008c:
            throw r5     // Catch:{ all -> 0x008d }
        L_0x008d:
            r5 = move-exception
            goto L_0x00ac
        L_0x008f:
            r5 = move-exception
            java.lang.String r0 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
            r1.<init>()     // Catch:{ all -> 0x008d }
            java.lang.String r2 = "Can't get db: "
            r1.append(r2)     // Catch:{ all -> 0x008d }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x008d }
            r1.append(r5)     // Catch:{ all -> 0x008d }
            java.lang.String r5 = r1.toString()     // Catch:{ all -> 0x008d }
            android.util.Log.e(r0, r5)     // Catch:{ all -> 0x008d }
            monitor-exit(r4)     // Catch:{ all -> 0x008d }
            return
        L_0x00ac:
            monitor-exit(r4)     // Catch:{ all -> 0x008d }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.putEvent(com.google.android.apps.analytics.Event):void");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Referrer readCurrentReferrer(android.database.sqlite.SQLiteDatabase r11) {
        /*
            r10 = this;
            r0 = 0
            java.lang.String r2 = "referrer"
            r1 = 4
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ SQLiteException -> 0x0066, all -> 0x0061 }
            r1 = 0
            java.lang.String r4 = "referrer"
            r3[r1] = r4     // Catch:{ SQLiteException -> 0x0066, all -> 0x0061 }
            r1 = 1
            java.lang.String r4 = "timestamp_referrer"
            r3[r1] = r4     // Catch:{ SQLiteException -> 0x0066, all -> 0x0061 }
            r1 = 2
            java.lang.String r4 = "referrer_visit"
            r3[r1] = r4     // Catch:{ SQLiteException -> 0x0066, all -> 0x0061 }
            r1 = 3
            java.lang.String r4 = "referrer_index"
            r3[r1] = r4     // Catch:{ SQLiteException -> 0x0066, all -> 0x0061 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r1 = r11
            android.database.Cursor r11 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x0066, all -> 0x0061 }
            boolean r1 = r11.moveToFirst()     // Catch:{ SQLiteException -> 0x005f }
            if (r1 == 0) goto L_0x0059
            java.lang.String r1 = "timestamp_referrer"
            int r1 = r11.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x005f }
            long r4 = r11.getLong(r1)     // Catch:{ SQLiteException -> 0x005f }
            java.lang.String r1 = "referrer_visit"
            int r1 = r11.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x005f }
            int r6 = r11.getInt(r1)     // Catch:{ SQLiteException -> 0x005f }
            java.lang.String r1 = "referrer_index"
            int r1 = r11.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x005f }
            int r7 = r11.getInt(r1)     // Catch:{ SQLiteException -> 0x005f }
            java.lang.String r1 = "referrer"
            int r1 = r11.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x005f }
            java.lang.String r3 = r11.getString(r1)     // Catch:{ SQLiteException -> 0x005f }
            com.google.android.apps.analytics.Referrer r1 = new com.google.android.apps.analytics.Referrer     // Catch:{ SQLiteException -> 0x005f }
            r2 = r1
            r2.<init>(r3, r4, r6, r7)     // Catch:{ SQLiteException -> 0x005f }
            r0 = r1
        L_0x0059:
            if (r11 == 0) goto L_0x005e
            r11.close()
        L_0x005e:
            return r0
        L_0x005f:
            r1 = move-exception
            goto L_0x0068
        L_0x0061:
            r11 = move-exception
            r9 = r0
            r0 = r11
            r11 = r9
            goto L_0x0078
        L_0x0066:
            r1 = move-exception
            r11 = r0
        L_0x0068:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0077 }
            android.util.Log.e(r2, r1)     // Catch:{ all -> 0x0077 }
            if (r11 == 0) goto L_0x0076
            r11.close()
        L_0x0076:
            return r0
        L_0x0077:
            r0 = move-exception
        L_0x0078:
            if (r11 == 0) goto L_0x007d
            r11.close()
        L_0x007d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentHitStore.readCurrentReferrer(android.database.sqlite.SQLiteDatabase):com.google.android.apps.analytics.Referrer");
    }

    public void setAnonymizeIp(boolean z) {
        this.anonymizeIp = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
     arg types: [java.lang.String, long]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean setReferrer(String str) {
        long j;
        String formatReferrer = formatReferrer(str);
        if (formatReferrer == null) {
            return false;
        }
        try {
            SQLiteDatabase writableDatabase = this.databaseHelper.getWritableDatabase();
            Referrer readCurrentReferrer = readCurrentReferrer(writableDatabase);
            ContentValues contentValues = new ContentValues();
            contentValues.put(TapjoyConstants.TJC_REFERRER, formatReferrer);
            contentValues.put(TIMESTAMP_REFERRER, (Long) 0L);
            contentValues.put(REFERRER_VISIT, (Integer) 0);
            if (readCurrentReferrer != null) {
                long index = (long) readCurrentReferrer.getIndex();
                j = readCurrentReferrer.getTimeStamp() > 0 ? index + 1 : index;
            } else {
                j = 1;
            }
            contentValues.put(REFERRER_INDEX, Long.valueOf(j));
            if (!setReferrerDatabase(writableDatabase, contentValues)) {
                return false;
            }
            startNewVisit();
            return true;
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
            return false;
        }
    }

    public void setSampleRate(int i) {
        this.sampleRate = i;
    }

    public synchronized void startNewVisit() {
        this.sessionStarted = false;
        this.useStoredVisitorVars = true;
        this.numStoredHits = getNumStoredHitsFromDb();
    }

    /* access modifiers changed from: package-private */
    public void storeUpdatedSession(SQLiteDatabase sQLiteDatabase) {
        SQLiteDatabase writableDatabase = this.databaseHelper.getWritableDatabase();
        writableDatabase.delete("session", null, null);
        if (this.timestampFirst == 0) {
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            this.timestampFirst = currentTimeMillis;
            this.timestampPrevious = currentTimeMillis;
            this.timestampCurrent = currentTimeMillis;
            this.visits = 1;
        } else {
            this.timestampPrevious = this.timestampCurrent;
            this.timestampCurrent = System.currentTimeMillis() / 1000;
            if (this.timestampCurrent == this.timestampPrevious) {
                this.timestampCurrent++;
            }
            this.visits++;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(TIMESTAMP_FIRST, Long.valueOf(this.timestampFirst));
        contentValues.put(TIMESTAMP_PREVIOUS, Long.valueOf(this.timestampPrevious));
        contentValues.put(TIMESTAMP_CURRENT, Long.valueOf(this.timestampCurrent));
        contentValues.put(VISITS, Integer.valueOf(this.visits));
        contentValues.put(STORE_ID, Integer.valueOf(this.storeId));
        writableDatabase.insert("session", null, contentValues);
        this.sessionStarted = true;
    }

    /* access modifiers changed from: package-private */
    public void writeEventToDatabase(Event event, Referrer referrer, SQLiteDatabase sQLiteDatabase, boolean z) throws SQLiteException {
        ContentValues contentValues = new ContentValues();
        contentValues.put(HIT_STRING, HitBuilder.constructHitRequestPath(event, referrer));
        contentValues.put(HIT_TIMESTAMP, Long.valueOf(z ? System.currentTimeMillis() : 0));
        sQLiteDatabase.insert("hits", null, contentValues);
        this.numStoredHits++;
    }
}
