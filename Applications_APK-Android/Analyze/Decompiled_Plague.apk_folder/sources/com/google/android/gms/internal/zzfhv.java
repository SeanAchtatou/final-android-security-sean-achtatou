package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfhv extends zzfhe<zzfhv> {
    public String mimeType = null;
    public Integer zzphs = null;
    public byte[] zzpiu = null;

    public zzfhv() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzap */
    public final zzfhv zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 8) {
                try {
                    int zzctv = zzfhb.zzctv();
                    switch (zzctv) {
                        default:
                            StringBuilder sb = new StringBuilder(36);
                            sb.append(zzctv);
                            sb.append(" is not a valid enum Type");
                            throw new IllegalArgumentException(sb.toString());
                        case 0:
                        case 1:
                            this.zzphs = Integer.valueOf(zzctv);
                            continue;
                    }
                } catch (IllegalArgumentException unused) {
                    zzfhb.zzlv(zzfhb.getPosition());
                    zza(zzfhb, zzcts);
                }
            } else if (zzcts == 18) {
                this.mimeType = zzfhb.readString();
            } else if (zzcts == 26) {
                this.zzpiu = zzfhb.readBytes();
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzphs != null) {
            zzfhc.zzaa(1, this.zzphs.intValue());
        }
        if (this.mimeType != null) {
            zzfhc.zzn(2, this.mimeType);
        }
        if (this.zzpiu != null) {
            zzfhc.zzc(3, this.zzpiu);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzphs != null) {
            zzo += zzfhc.zzad(1, this.zzphs.intValue());
        }
        if (this.mimeType != null) {
            zzo += zzfhc.zzo(2, this.mimeType);
        }
        return this.zzpiu != null ? zzo + zzfhc.zzd(3, this.zzpiu) : zzo;
    }
}
