package com.google.android.gms.internal;

import java.io.IOException;

public final class zzdqm extends zzfee<zzdqm, zza> implements zzffk {
    private static volatile zzffm<zzdqm> zzbas;
    /* access modifiers changed from: private */
    public static final zzdqm zzlqx;
    private int zzlqb;
    private zzfdh zzlqj = zzfdh.zzpal;

    public static final class zza extends zzfef<zzdqm, zza> implements zzffk {
        private zza() {
            super(zzdqm.zzlqx);
        }

        /* synthetic */ zza(zzdqn zzdqn) {
            this();
        }

        public final zza zzfm(int i) {
            zzcvi();
            ((zzdqm) this.zzpbv).setVersion(0);
            return this;
        }

        public final zza zzu(zzfdh zzfdh) {
            zzcvi();
            ((zzdqm) this.zzpbv).zzj(zzfdh);
            return this;
        }
    }

    static {
        zzdqm zzdqm = new zzdqm();
        zzlqx = zzdqm;
        zzdqm.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdqm.zzpbs.zzbim();
    }

    private zzdqm() {
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzlqb = i;
    }

    public static zza zzbmo() {
        zzdqm zzdqm = zzlqx;
        zzfef zzfef = (zzfef) zzdqm.zza(zzfem.zzpch, (Object) null, (Object) null);
        zzfef.zza((zzfee) zzdqm);
        return (zza) zzfef;
    }

    /* access modifiers changed from: private */
    public final void zzj(zzfdh zzfdh) {
        if (zzfdh == null) {
            throw new NullPointerException();
        }
        this.zzlqj = zzfdh;
    }

    public static zzdqm zzt(zzfdh zzfdh) throws zzfew {
        return (zzdqm) zzfee.zza(zzlqx, zzfdh);
    }

    public final int getVersion() {
        return this.zzlqb;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        boolean z = true;
        boolean z2 = false;
        switch (zzdqn.zzbaq[i - 1]) {
            case 1:
                return new zzdqm();
            case 2:
                return zzlqx;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdqm zzdqm = (zzdqm) obj2;
                this.zzlqb = zzfen.zza(this.zzlqb != 0, this.zzlqb, zzdqm.zzlqb != 0, zzdqm.zzlqb);
                boolean z3 = this.zzlqj != zzfdh.zzpal;
                zzfdh zzfdh = this.zzlqj;
                if (zzdqm.zzlqj == zzfdh.zzpal) {
                    z = false;
                }
                this.zzlqj = zzfen.zza(z3, zzfdh, z, zzdqm.zzlqj);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                if (((zzfea) obj2) != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzlqb = zzfdq.zzcub();
                                } else if (zzcts == 18) {
                                    this.zzlqj = zzfdq.zzcua();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdqm.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlqx);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlqx;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlqb != 0) {
            zzfdv.zzab(1, this.zzlqb);
        }
        if (!this.zzlqj.isEmpty()) {
            zzfdv.zza(2, this.zzlqj);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzfdh zzblt() {
        return this.zzlqj;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlqb != 0) {
            i2 = 0 + zzfdv.zzae(1, this.zzlqb);
        }
        if (!this.zzlqj.isEmpty()) {
            i2 += zzfdv.zzb(2, this.zzlqj);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
