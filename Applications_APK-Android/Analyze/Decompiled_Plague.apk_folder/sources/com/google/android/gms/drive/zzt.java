package com.google.android.gms.drive;

import com.google.android.gms.drive.ExecutionOptions;

public final class zzt extends ExecutionOptions.Builder {
    private boolean zzghf = true;

    public final /* synthetic */ ExecutionOptions build() {
        zzanv();
        return new zzr(this.zzgha, this.zzghb, this.zzghc, this.zzghf);
    }

    public final /* synthetic */ ExecutionOptions.Builder setConflictStrategy(int i) {
        super.setConflictStrategy(i);
        return this;
    }

    public final /* synthetic */ ExecutionOptions.Builder setNotifyOnCompletion(boolean z) {
        super.setNotifyOnCompletion(z);
        return this;
    }

    public final /* synthetic */ ExecutionOptions.Builder setTrackingTag(String str) {
        super.setTrackingTag(str);
        return this;
    }
}
