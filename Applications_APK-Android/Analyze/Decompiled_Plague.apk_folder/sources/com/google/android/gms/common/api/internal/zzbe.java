package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.internal.zzaf;

final class zzbe implements zzaf {
    private /* synthetic */ zzbd zzfpp;

    zzbe(zzbd zzbd) {
        this.zzfpp = zzbd;
    }

    public final boolean isConnected() {
        return this.zzfpp.isConnected();
    }

    public final Bundle zzaew() {
        return null;
    }
}
