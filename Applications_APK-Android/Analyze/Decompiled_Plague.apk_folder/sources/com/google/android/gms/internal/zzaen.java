package com.google.android.gms.internal;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class zzaen implements ThreadFactory {
    private final AtomicInteger zzcvo = new AtomicInteger(1);

    zzaen(zzael zzael) {
    }

    public final Thread newThread(Runnable runnable) {
        int andIncrement = this.zzcvo.getAndIncrement();
        StringBuilder sb = new StringBuilder(42);
        sb.append("AdWorker(SCION_TASK_EXECUTOR) #");
        sb.append(andIncrement);
        return new Thread(runnable, sb.toString());
    }
}
