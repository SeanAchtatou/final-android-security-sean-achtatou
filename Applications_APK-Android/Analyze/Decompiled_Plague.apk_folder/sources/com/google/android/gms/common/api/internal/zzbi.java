package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

final class zzbi extends Handler {
    private /* synthetic */ zzbd zzfpp;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbi(zzbd zzbd, Looper looper) {
        super(looper);
        this.zzfpp = zzbd;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.zzfpp.zzahx();
                return;
            case 2:
                this.zzfpp.resume();
                return;
            default:
                int i = message.what;
                StringBuilder sb = new StringBuilder(31);
                sb.append("Unknown message id: ");
                sb.append(i);
                Log.w("GoogleApiClientImpl", sb.toString());
                return;
        }
    }
}
