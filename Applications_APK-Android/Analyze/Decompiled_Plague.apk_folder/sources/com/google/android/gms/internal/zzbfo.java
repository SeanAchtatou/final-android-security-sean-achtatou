package com.google.android.gms.internal;

import android.os.Parcelable;

public final class zzbfo implements Parcelable.Creator<zzbfl> {
    /* JADX WARN: Type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r14) {
        /*
            r13 = this;
            int r0 = com.google.android.gms.internal.zzbek.zzd(r14)
            r1 = 0
            r2 = 0
            r9 = r1
            r11 = r9
            r12 = r11
            r4 = r2
            r5 = r4
            r6 = r5
            r7 = r6
            r8 = r7
            r10 = r8
        L_0x000f:
            int r1 = r14.dataPosition()
            if (r1 >= r0) goto L_0x0056
            int r1 = r14.readInt()
            r2 = 65535(0xffff, float:9.1834E-41)
            r2 = r2 & r1
            switch(r2) {
                case 1: goto L_0x0051;
                case 2: goto L_0x004c;
                case 3: goto L_0x0047;
                case 4: goto L_0x0042;
                case 5: goto L_0x003d;
                case 6: goto L_0x0038;
                case 7: goto L_0x0033;
                case 8: goto L_0x002e;
                case 9: goto L_0x0024;
                default: goto L_0x0020;
            }
        L_0x0020:
            com.google.android.gms.internal.zzbek.zzb(r14, r1)
            goto L_0x000f
        L_0x0024:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzbfe> r2 = com.google.android.gms.internal.zzbfe.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r14, r1, r2)
            r12 = r1
            com.google.android.gms.internal.zzbfe r12 = (com.google.android.gms.internal.zzbfe) r12
            goto L_0x000f
        L_0x002e:
            java.lang.String r11 = com.google.android.gms.internal.zzbek.zzq(r14, r1)
            goto L_0x000f
        L_0x0033:
            int r10 = com.google.android.gms.internal.zzbek.zzg(r14, r1)
            goto L_0x000f
        L_0x0038:
            java.lang.String r9 = com.google.android.gms.internal.zzbek.zzq(r14, r1)
            goto L_0x000f
        L_0x003d:
            boolean r8 = com.google.android.gms.internal.zzbek.zzc(r14, r1)
            goto L_0x000f
        L_0x0042:
            int r7 = com.google.android.gms.internal.zzbek.zzg(r14, r1)
            goto L_0x000f
        L_0x0047:
            boolean r6 = com.google.android.gms.internal.zzbek.zzc(r14, r1)
            goto L_0x000f
        L_0x004c:
            int r5 = com.google.android.gms.internal.zzbek.zzg(r14, r1)
            goto L_0x000f
        L_0x0051:
            int r4 = com.google.android.gms.internal.zzbek.zzg(r14, r1)
            goto L_0x000f
        L_0x0056:
            com.google.android.gms.internal.zzbek.zzaf(r14, r0)
            com.google.android.gms.internal.zzbfl r14 = new com.google.android.gms.internal.zzbfl
            r3 = r14
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzbfo.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbfl[i];
    }
}
