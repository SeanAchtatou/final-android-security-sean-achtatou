package com.google.android.gms.internal;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Process;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzm;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.common.util.zzx;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.ChangeListener;
import com.google.android.gms.drive.events.DriveEventService;
import com.google.android.gms.drive.events.zzd;
import com.google.android.gms.drive.events.zzj;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class zzbll extends zzab<zzbpf> {
    private final String zzdyu;
    private final Bundle zzggl;
    protected final boolean zzgkn;
    private volatile DriveId zzgko;
    private volatile DriveId zzgkp;
    private volatile boolean zzgkq = false;
    private GoogleApiClient.ConnectionCallbacks zzgkr;
    private Map<DriveId, Map<ChangeListener, zzbov>> zzgks = new HashMap();
    private Map<zzd, zzbov> zzgkt = new HashMap();
    private Map<DriveId, Map<Object, zzbov>> zzgku = new HashMap();
    private Map<DriveId, Map<Object, zzbov>> zzgkv = new HashMap();

    public zzbll(Context context, Looper looper, zzr zzr, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener, Bundle bundle) {
        super(context, looper, 11, zzr, connectionCallbacks, onConnectionFailedListener);
        this.zzdyu = zzr.zzakm();
        this.zzgkr = connectionCallbacks;
        this.zzggl = bundle;
        Intent intent = new Intent(DriveEventService.ACTION_HANDLE_EVENT);
        intent.setPackage(context.getPackageName());
        List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(intent, 0);
        switch (queryIntentServices.size()) {
            case 0:
                this.zzgkn = false;
                return;
            case 1:
                ServiceInfo serviceInfo = queryIntentServices.get(0).serviceInfo;
                if (!serviceInfo.exported) {
                    String str = serviceInfo.name;
                    StringBuilder sb = new StringBuilder(60 + String.valueOf(str).length());
                    sb.append("Drive event service ");
                    sb.append(str);
                    sb.append(" must be exported in AndroidManifest.xml");
                    throw new IllegalStateException(sb.toString());
                }
                this.zzgkn = true;
                return;
            default:
                String action = intent.getAction();
                StringBuilder sb2 = new StringBuilder(72 + String.valueOf(action).length());
                sb2.append("AndroidManifest.xml can only define one service that handles the ");
                sb2.append(action);
                sb2.append(" action");
                throw new IllegalStateException(sb2.toString());
        }
    }

    public final void disconnect() {
        if (isConnected()) {
            try {
                ((zzbpf) zzakb()).zza(new zzbkn());
            } catch (RemoteException unused) {
            }
        }
        super.disconnect();
        synchronized (this.zzgks) {
            this.zzgks.clear();
        }
        synchronized (this.zzgkt) {
            this.zzgkt.clear();
        }
        synchronized (this.zzgku) {
            this.zzgku.clear();
        }
        synchronized (this.zzgkv) {
            this.zzgkv.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    public final PendingResult<Status> zza(GoogleApiClient googleApiClient, DriveId driveId, ChangeListener changeListener) {
        zzbq.checkArgument(zzj.zza(1, driveId));
        zzbq.checkNotNull(changeListener, "listener");
        zzbq.zza(isConnected(), (Object) "Client must be connected");
        synchronized (this.zzgks) {
            Map map = this.zzgks.get(driveId);
            if (map == null) {
                map = new HashMap();
                this.zzgks.put(driveId, map);
            }
            zzbov zzbov = (zzbov) map.get(changeListener);
            if (zzbov == null) {
                zzbov = new zzbov(getLooper(), getContext(), 1, changeListener);
                map.put(changeListener, zzbov);
            } else if (zzbov.zzcw(1)) {
                zzblf zzblf = new zzblf(googleApiClient, Status.zzfko);
                return zzblf;
            }
            zzbov.zzcv(1);
            zzm zze = googleApiClient.zze(new zzblm(this, googleApiClient, new zzbjt(1, driveId), zzbov));
            return zze;
        }
    }

    /* access modifiers changed from: protected */
    public final void zza(int i, IBinder iBinder, Bundle bundle, int i2) {
        if (bundle != null) {
            bundle.setClassLoader(getClass().getClassLoader());
            this.zzgko = (DriveId) bundle.getParcelable("com.google.android.gms.drive.root_id");
            this.zzgkp = (DriveId) bundle.getParcelable("com.google.android.gms.drive.appdata_id");
            this.zzgkq = true;
        }
        super.zza(i, iBinder, bundle, i2);
    }

    /* access modifiers changed from: protected */
    public final Bundle zzaae() {
        String packageName = getContext().getPackageName();
        zzbq.checkNotNull(packageName);
        zzbq.checkState(!zzakv().zzakk().isEmpty());
        Bundle bundle = new Bundle();
        if (!packageName.equals(this.zzdyu)) {
            bundle.putString("proxy_package_name", this.zzdyu);
        }
        bundle.putAll(this.zzggl);
        return bundle;
    }

    public final boolean zzaam() {
        return !getContext().getPackageName().equals(this.zzdyu) || !zzx.zzf(getContext(), Process.myUid());
    }

    public final boolean zzakc() {
        return true;
    }

    public final DriveId zzaon() {
        return this.zzgko;
    }

    public final DriveId zzaoo() {
        return this.zzgkp;
    }

    public final boolean zzaop() {
        return this.zzgkq;
    }

    public final boolean zzaoq() {
        return this.zzgkn;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    public final PendingResult<Status> zzb(GoogleApiClient googleApiClient, DriveId driveId, ChangeListener changeListener) {
        zzbq.checkArgument(zzj.zza(1, driveId));
        zzbq.zza(isConnected(), (Object) "Client must be connected");
        zzbq.checkNotNull(changeListener, "listener");
        synchronized (this.zzgks) {
            Map map = this.zzgks.get(driveId);
            if (map == null) {
                zzblf zzblf = new zzblf(googleApiClient, Status.zzfko);
                return zzblf;
            }
            zzbov zzbov = (zzbov) map.remove(changeListener);
            if (zzbov == null) {
                zzblf zzblf2 = new zzblf(googleApiClient, Status.zzfko);
                return zzblf2;
            }
            if (map.isEmpty()) {
                this.zzgks.remove(driveId);
            }
            zzm zze = googleApiClient.zze(new zzbln(this, googleApiClient, new zzbrd(driveId, 1), zzbov));
            return zze;
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ IInterface zzd(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.drive.internal.IDriveService");
        return queryLocalInterface instanceof zzbpf ? (zzbpf) queryLocalInterface : new zzbpg(iBinder);
    }

    /* access modifiers changed from: protected */
    public final String zzhf() {
        return "com.google.android.gms.drive.ApiService.START";
    }

    /* access modifiers changed from: protected */
    public final String zzhg() {
        return "com.google.android.gms.drive.internal.IDriveService";
    }
}
