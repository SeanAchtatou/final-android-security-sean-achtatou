package com.google.android.gms.games.video;

import android.os.Bundle;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzbq;

public final class CaptureState {
    private final boolean zzarc;
    private final boolean zzesn;
    private final boolean zzhyd;
    private final int zzhye;
    private final int zzhyf;

    private CaptureState(boolean z, int i, int i2, boolean z2, boolean z3) {
        zzbq.checkArgument(VideoConfiguration.isValidCaptureMode(i, true));
        zzbq.checkArgument(VideoConfiguration.isValidQualityLevel(i2, true));
        this.zzhyd = z;
        this.zzhye = i;
        this.zzhyf = i2;
        this.zzesn = z2;
        this.zzarc = z3;
    }

    public static CaptureState zzp(Bundle bundle) {
        if (bundle == null || bundle.get("IsCapturing") == null) {
            return null;
        }
        return new CaptureState(bundle.getBoolean("IsCapturing", false), bundle.getInt("CaptureMode", -1), bundle.getInt("CaptureQuality", -1), bundle.getBoolean("IsOverlayVisible", false), bundle.getBoolean("IsPaused", false));
    }

    public final int getCaptureMode() {
        return this.zzhye;
    }

    public final int getCaptureQuality() {
        return this.zzhyf;
    }

    public final boolean isCapturing() {
        return this.zzhyd;
    }

    public final boolean isOverlayVisible() {
        return this.zzesn;
    }

    public final boolean isPaused() {
        return this.zzarc;
    }

    public final String toString() {
        return zzbg.zzw(this).zzg("IsCapturing", Boolean.valueOf(this.zzhyd)).zzg("CaptureMode", Integer.valueOf(this.zzhye)).zzg("CaptureQuality", Integer.valueOf(this.zzhyf)).zzg("IsOverlayVisible", Boolean.valueOf(this.zzesn)).zzg("IsPaused", Boolean.valueOf(this.zzarc)).toString();
    }
}
