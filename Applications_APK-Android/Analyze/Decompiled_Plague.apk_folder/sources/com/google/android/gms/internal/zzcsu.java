package com.google.android.gms.internal;

import android.database.ContentObserver;
import android.os.Handler;
import java.util.Map;

final class zzcsu extends ContentObserver {
    private /* synthetic */ zzcss zzjts;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcsu(zzcss zzcss, Handler handler) {
        super(null);
        this.zzjts = zzcss;
    }

    public final void onChange(boolean z) {
        synchronized (this.zzjts.zzjtp) {
            Map unused = this.zzjts.zzjtq = null;
        }
    }
}
