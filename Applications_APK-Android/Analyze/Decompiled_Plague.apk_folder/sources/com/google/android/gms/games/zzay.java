package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.Players;

final class zzay implements zzbo<Players.LoadPlayersResult, Player> {
    zzay() {
    }

    private static Player zza(@Nullable Players.LoadPlayersResult loadPlayersResult) {
        if (loadPlayersResult == null) {
            return null;
        }
        PlayerBuffer players = loadPlayersResult.getPlayers();
        if (players != null) {
            try {
                if (players.getCount() > 0) {
                    return (Player) ((Player) players.get(0)).freeze();
                }
            } finally {
                if (players != null) {
                    players.release();
                }
            }
        }
        if (players != null) {
            players.release();
        }
        return null;
    }

    public final /* synthetic */ Object zzb(@Nullable Result result) {
        return zza((Players.LoadPlayersResult) result);
    }
}
