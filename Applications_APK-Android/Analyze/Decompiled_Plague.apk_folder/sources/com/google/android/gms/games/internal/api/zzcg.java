package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;

final class zzcg extends zzcj {
    private /* synthetic */ Snapshot zzhld;
    private /* synthetic */ SnapshotMetadataChange zzhqq;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcg(zzcd zzcd, GoogleApiClient googleApiClient, Snapshot snapshot, SnapshotMetadataChange snapshotMetadataChange) {
        super(googleApiClient, null);
        this.zzhld = snapshot;
        this.zzhqq = snapshotMetadataChange;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhld, this.zzhqq);
    }
}
