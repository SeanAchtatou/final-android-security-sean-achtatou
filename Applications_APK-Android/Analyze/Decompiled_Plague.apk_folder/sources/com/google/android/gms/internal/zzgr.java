package com.google.android.gms.internal;

final class zzgr implements Runnable {
    private /* synthetic */ zzgq zzayi;

    zzgr(zzgq zzgq) {
        this.zzayi = zzgq;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzgq.zza(com.google.android.gms.internal.zzgq, boolean):boolean
     arg types: [com.google.android.gms.internal.zzgq, int]
     candidates:
      com.google.android.gms.internal.zzgq.zza(android.app.Application, android.content.Context):void
      com.google.android.gms.internal.zzgq.zza(com.google.android.gms.internal.zzgq, boolean):boolean */
    public final void run() {
        synchronized (this.zzayi.mLock) {
            if (!this.zzayi.zzayc || !this.zzayi.zzayd) {
                zzafj.zzbw("App is still foreground");
            } else {
                boolean unused = this.zzayi.zzayc = false;
                zzafj.zzbw("App went background");
                for (zzgs zzg : this.zzayi.zzaye) {
                    try {
                        zzg.zzg(false);
                    } catch (Exception e) {
                        zzafj.zzb("OnForegroundStateChangedListener threw exception.", e);
                    }
                }
            }
        }
    }
}
