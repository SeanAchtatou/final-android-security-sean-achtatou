package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@zzzb
public final class zzajg {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, java.util.concurrent.Future):void
     arg types: [com.google.android.gms.internal.zzajy, com.google.android.gms.internal.zzajp<V>]
     candidates:
      com.google.android.gms.internal.zzajg.zza(java.util.concurrent.Future, java.lang.Object):T
      com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, com.google.android.gms.internal.zzajy):void
      com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, java.util.concurrent.Future):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, com.google.android.gms.internal.zzajy):void
     arg types: [com.google.android.gms.internal.zzajp<V>, com.google.android.gms.internal.zzajy]
     candidates:
      com.google.android.gms.internal.zzajg.zza(java.util.concurrent.Future, java.lang.Object):T
      com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, java.util.concurrent.Future):void
      com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, com.google.android.gms.internal.zzajy):void */
    public static <V> zzajp<V> zza(zzajp<V> zzajp, long j, TimeUnit timeUnit, ScheduledExecutorService scheduledExecutorService) {
        zzajy zzajy = new zzajy();
        zza((zzajp) zzajy, (Future) zzajp);
        scheduledExecutorService.schedule(new zzajk(zzajy), j, timeUnit);
        zza((zzajp) zzajp, zzajy);
        return zzajy;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, java.util.concurrent.Future):void
     arg types: [com.google.android.gms.internal.zzajy, com.google.android.gms.internal.zzajp]
     candidates:
      com.google.android.gms.internal.zzajg.zza(java.util.concurrent.Future, java.lang.Object):T
      com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, com.google.android.gms.internal.zzajy):void
      com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, java.util.concurrent.Future):void */
    public static <A, B> zzajp<B> zza(zzajp zzajp, zzajb zzajb, Executor executor) {
        zzajy zzajy = new zzajy();
        zzajp.zza(new zzajj(zzajy, zzajb, zzajp), executor);
        zza((zzajp) zzajy, (Future) zzajp);
        return zzajy;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, java.util.concurrent.Future):void
     arg types: [com.google.android.gms.internal.zzajy, com.google.android.gms.internal.zzajp]
     candidates:
      com.google.android.gms.internal.zzajg.zza(java.util.concurrent.Future, java.lang.Object):T
      com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, com.google.android.gms.internal.zzajy):void
      com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, java.util.concurrent.Future):void */
    public static <A, B> zzajp<B> zza(zzajp zzajp, zzajc zzajc, Executor executor) {
        zzajy zzajy = new zzajy();
        zzajp.zza(new zzaji(zzajy, zzajc, zzajp), executor);
        zza((zzajp) zzajy, (Future) zzajp);
        return zzajy;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, java.util.concurrent.Future):void
     arg types: [com.google.android.gms.internal.zzajy, com.google.android.gms.internal.zzajp<? extends V>]
     candidates:
      com.google.android.gms.internal.zzajg.zza(java.util.concurrent.Future, java.lang.Object):T
      com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, com.google.android.gms.internal.zzajy):void
      com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, java.util.concurrent.Future):void */
    public static <V, X extends Throwable> zzajp<V> zza(zzajp<? extends V> zzajp, Class<X> cls, zzajb<? super X, ? extends V> zzajb, Executor executor) {
        zzajy zzajy = new zzajy();
        zza((zzajp) zzajy, (Future) zzajp);
        zzajp.zza(new zzajl(zzajy, zzajp, cls, zzajb, executor), zzaju.zzdcu);
        return zzajy;
    }

    public static <T> T zza(Future<T> future, T t) {
        try {
            return future.get(((Long) zzbs.zzep().zzd(zzmq.zzblr)).longValue(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e = e;
            future.cancel(true);
            zzafj.zzc("InterruptedException caught while resolving future.", e);
            Thread.currentThread().interrupt();
            zzbs.zzeg().zza(e, "Futures.resolveFuture");
            return t;
        } catch (Exception e2) {
            e = e2;
            future.cancel(true);
            zzafj.zzb("Error waiting for future.", e);
            zzbs.zzeg().zza(e, "Futures.resolveFuture");
            return t;
        }
    }

    public static <T> T zza(Future<T> future, T t, long j, TimeUnit timeUnit) {
        try {
            return future.get(j, timeUnit);
        } catch (InterruptedException e) {
            e = e;
            future.cancel(true);
            zzafj.zzc("InterruptedException caught while resolving future.", e);
            Thread.currentThread().interrupt();
            zzbs.zzeg().zza(e, "Futures.resolveFuture");
            return t;
        } catch (Exception e2) {
            e = e2;
            future.cancel(true);
            zzafj.zzb("Error waiting for future.", e);
            zzbs.zzeg().zza(e, "Futures.resolveFuture");
            return t;
        }
    }

    public static <V> void zza(zzajp zzajp, zzajd zzajd, Executor executor) {
        zzajp.zza(new zzajh(zzajd, zzajp), executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, java.util.concurrent.Future):void
     arg types: [com.google.android.gms.internal.zzajy<V>, com.google.android.gms.internal.zzajp<? extends V>]
     candidates:
      com.google.android.gms.internal.zzajg.zza(java.util.concurrent.Future, java.lang.Object):T
      com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, com.google.android.gms.internal.zzajy):void
      com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajp, java.util.concurrent.Future):void */
    private static <V> void zza(zzajp<? extends V> zzajp, zzajy<V> zzajy) {
        zza((zzajp) zzajy, (Future) zzajp);
        zzajp.zza(new zzajm(zzajy, zzajp), zzaju.zzdcu);
    }

    private static <A, B> void zza(zzajp<A> zzajp, Future<B> future) {
        zzajp.zza(new zzajn(zzajp, future), zzaju.zzdcu);
    }

    static final /* synthetic */ void zza(zzajy zzajy, zzajb zzajb, zzajp zzajp) {
        if (!zzajy.isCancelled()) {
            try {
                zza(zzajb.zzc(zzajp.get()), zzajy);
            } catch (CancellationException unused) {
                zzajy.cancel(true);
            } catch (ExecutionException e) {
                zzajy.setException(e.getCause());
            } catch (InterruptedException e2) {
                Thread.currentThread().interrupt();
                zzajy.setException(e2);
            } catch (Exception e3) {
                zzajy.setException(e3);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001e  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static final /* synthetic */ void zza(com.google.android.gms.internal.zzajy r1, com.google.android.gms.internal.zzajp r2, java.lang.Class r3, com.google.android.gms.internal.zzajb r4, java.util.concurrent.Executor r5) {
        /*
            java.lang.Object r2 = r2.get()     // Catch:{ ExecutionException -> 0x0013, InterruptedException -> 0x000a, Exception -> 0x0008 }
            r1.set(r2)     // Catch:{ ExecutionException -> 0x0013, InterruptedException -> 0x000a, Exception -> 0x0008 }
            return
        L_0x0008:
            r2 = move-exception
            goto L_0x0018
        L_0x000a:
            r2 = move-exception
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
            goto L_0x0018
        L_0x0013:
            r2 = move-exception
            java.lang.Throwable r2 = r2.getCause()
        L_0x0018:
            boolean r3 = r3.isInstance(r2)
            if (r3 == 0) goto L_0x002a
            com.google.android.gms.internal.zzajo r2 = zzi(r2)
            com.google.android.gms.internal.zzajp r2 = zza(r2, r4, r5)
            zza(r2, r1)
            return
        L_0x002a:
            r1.setException(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzajg.zza(com.google.android.gms.internal.zzajy, com.google.android.gms.internal.zzajp, java.lang.Class, com.google.android.gms.internal.zzajb, java.util.concurrent.Executor):void");
    }

    public static <T> zzajo<T> zzi(T t) {
        return new zzajo<>(t);
    }
}
