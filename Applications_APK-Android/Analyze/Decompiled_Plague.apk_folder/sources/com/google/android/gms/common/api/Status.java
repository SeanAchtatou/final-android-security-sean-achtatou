package com.google.android.gms.common.api;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.IntentSender;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class Status extends zzbej implements Result, ReflectedParcelable {
    public static final Parcelable.Creator<Status> CREATOR = new zzg();
    public static final Status zzfko = new Status(0);
    public static final Status zzfkp = new Status(14);
    public static final Status zzfkq = new Status(8);
    public static final Status zzfkr = new Status(15);
    public static final Status zzfks = new Status(16);
    public static final Status zzfkt = new Status(17);
    private static Status zzfku = new Status(18);
    private int zzdzm;
    @Nullable
    private final PendingIntent zzebp;
    private final int zzfcq;
    @Nullable
    private final String zzfhz;

    public Status(int i) {
        this(i, null);
    }

    Status(int i, int i2, @Nullable String str, @Nullable PendingIntent pendingIntent) {
        this.zzdzm = i;
        this.zzfcq = i2;
        this.zzfhz = str;
        this.zzebp = pendingIntent;
    }

    public Status(int i, @Nullable String str) {
        this(1, i, str, null);
    }

    public Status(int i, @Nullable String str, @Nullable PendingIntent pendingIntent) {
        this(1, i, str, pendingIntent);
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof Status)) {
            return false;
        }
        Status status = (Status) obj;
        return this.zzdzm == status.zzdzm && this.zzfcq == status.zzfcq && zzbg.equal(this.zzfhz, status.zzfhz) && zzbg.equal(this.zzebp, status.zzebp);
    }

    public final PendingIntent getResolution() {
        return this.zzebp;
    }

    public final Status getStatus() {
        return this;
    }

    public final int getStatusCode() {
        return this.zzfcq;
    }

    @Nullable
    public final String getStatusMessage() {
        return this.zzfhz;
    }

    public final boolean hasResolution() {
        return this.zzebp != null;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.zzdzm), Integer.valueOf(this.zzfcq), this.zzfhz, this.zzebp});
    }

    public final boolean isCanceled() {
        return this.zzfcq == 16;
    }

    public final boolean isInterrupted() {
        return this.zzfcq == 14;
    }

    public final boolean isSuccess() {
        return this.zzfcq <= 0;
    }

    public final void startResolutionForResult(Activity activity, int i) throws IntentSender.SendIntentException {
        if (hasResolution()) {
            activity.startIntentSenderForResult(this.zzebp.getIntentSender(), i, null, 0, 0, 0);
        }
    }

    public final String toString() {
        return zzbg.zzw(this).zzg("statusCode", zzagk()).zzg("resolution", this.zzebp).toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.app.PendingIntent, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 1, getStatusCode());
        zzbem.zza(parcel, 2, getStatusMessage(), false);
        zzbem.zza(parcel, 3, (Parcelable) this.zzebp, i, false);
        zzbem.zzc(parcel, 1000, this.zzdzm);
        zzbem.zzai(parcel, zze);
    }

    public final String zzagk() {
        return this.zzfhz != null ? this.zzfhz : CommonStatusCodes.getStatusCodeString(this.zzfcq);
    }
}
