package com.google.android.gms.internal;

import java.io.IOException;

public final class zzij extends zzfhe<zzij> {
    public Integer zzbbk = null;

    public zzij() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzg */
    public final zzij zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 56) {
                try {
                    int zzcuh = zzfhb.zzcuh();
                    switch (zzcuh) {
                        default:
                            StringBuilder sb = new StringBuilder(43);
                            sb.append(zzcuh);
                            sb.append(" is not a valid enum AdInitiater");
                            throw new IllegalArgumentException(sb.toString());
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            this.zzbbk = Integer.valueOf(zzcuh);
                            continue;
                    }
                } catch (IllegalArgumentException unused) {
                    zzfhb.zzlv(zzfhb.getPosition());
                    zza(zzfhb, zzcts);
                }
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzbbk != null) {
            zzfhc.zzaa(7, this.zzbbk.intValue());
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        return this.zzbbk != null ? zzo + zzfhc.zzad(7, this.zzbbk.intValue()) : zzo;
    }
}
