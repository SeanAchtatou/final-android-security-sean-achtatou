package com.google.android.gms.common.api.internal;

abstract class zzbm {
    private final zzbk zzfqb;

    protected zzbm(zzbk zzbk) {
        this.zzfqb = zzbk;
    }

    /* access modifiers changed from: protected */
    public abstract void zzahp();

    public final void zzc(zzbl zzbl) {
        zzbl.zzfmy.lock();
        try {
            if (zzbl.zzfpx == this.zzfqb) {
                zzahp();
            }
        } finally {
            zzbl.zzfmy.unlock();
        }
    }
}
