package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzkl extends zzed implements zzkj {
    zzkl(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IClientApi");
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzjw createAdLoaderBuilder(com.google.android.gms.dynamic.IObjectWrapper r2, java.lang.String r3, com.google.android.gms.internal.zzuc r4, int r5) throws android.os.RemoteException {
        /*
            r1 = this;
            android.os.Parcel r0 = r1.zzaz()
            com.google.android.gms.internal.zzef.zza(r0, r2)
            r0.writeString(r3)
            com.google.android.gms.internal.zzef.zza(r0, r4)
            r0.writeInt(r5)
            r2 = 3
            android.os.Parcel r2 = r1.zza(r2, r0)
            android.os.IBinder r3 = r2.readStrongBinder()
            if (r3 != 0) goto L_0x001d
            r3 = 0
            goto L_0x0031
        L_0x001d:
            java.lang.String r4 = "com.google.android.gms.ads.internal.client.IAdLoaderBuilder"
            android.os.IInterface r4 = r3.queryLocalInterface(r4)
            boolean r5 = r4 instanceof com.google.android.gms.internal.zzjw
            if (r5 == 0) goto L_0x002b
            r3 = r4
            com.google.android.gms.internal.zzjw r3 = (com.google.android.gms.internal.zzjw) r3
            goto L_0x0031
        L_0x002b:
            com.google.android.gms.internal.zzjy r4 = new com.google.android.gms.internal.zzjy
            r4.<init>(r3)
            r3 = r4
        L_0x0031:
            r2.recycle()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzkl.createAdLoaderBuilder(com.google.android.gms.dynamic.IObjectWrapper, java.lang.String, com.google.android.gms.internal.zzuc, int):com.google.android.gms.internal.zzjw");
    }

    public final zzwj createAdOverlay(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        Parcel zza = zza(8, zzaz);
        zzwj zzu = zzwk.zzu(zza.readStrongBinder());
        zza.recycle();
        return zzu;
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzkb createBannerAdManager(com.google.android.gms.dynamic.IObjectWrapper r2, com.google.android.gms.internal.zziw r3, java.lang.String r4, com.google.android.gms.internal.zzuc r5, int r6) throws android.os.RemoteException {
        /*
            r1 = this;
            android.os.Parcel r0 = r1.zzaz()
            com.google.android.gms.internal.zzef.zza(r0, r2)
            com.google.android.gms.internal.zzef.zza(r0, r3)
            r0.writeString(r4)
            com.google.android.gms.internal.zzef.zza(r0, r5)
            r0.writeInt(r6)
            r2 = 1
            android.os.Parcel r2 = r1.zza(r2, r0)
            android.os.IBinder r3 = r2.readStrongBinder()
            if (r3 != 0) goto L_0x0020
            r3 = 0
            goto L_0x0034
        L_0x0020:
            java.lang.String r4 = "com.google.android.gms.ads.internal.client.IAdManager"
            android.os.IInterface r4 = r3.queryLocalInterface(r4)
            boolean r5 = r4 instanceof com.google.android.gms.internal.zzkb
            if (r5 == 0) goto L_0x002e
            r3 = r4
            com.google.android.gms.internal.zzkb r3 = (com.google.android.gms.internal.zzkb) r3
            goto L_0x0034
        L_0x002e:
            com.google.android.gms.internal.zzkd r4 = new com.google.android.gms.internal.zzkd
            r4.<init>(r3)
            r3 = r4
        L_0x0034:
            r2.recycle()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzkl.createBannerAdManager(com.google.android.gms.dynamic.IObjectWrapper, com.google.android.gms.internal.zziw, java.lang.String, com.google.android.gms.internal.zzuc, int):com.google.android.gms.internal.zzkb");
    }

    public final zzwt createInAppPurchaseManager(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        Parcel zza = zza(7, zzaz);
        zzwt zzw = zzwu.zzw(zza.readStrongBinder());
        zza.recycle();
        return zzw;
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzkb createInterstitialAdManager(com.google.android.gms.dynamic.IObjectWrapper r2, com.google.android.gms.internal.zziw r3, java.lang.String r4, com.google.android.gms.internal.zzuc r5, int r6) throws android.os.RemoteException {
        /*
            r1 = this;
            android.os.Parcel r0 = r1.zzaz()
            com.google.android.gms.internal.zzef.zza(r0, r2)
            com.google.android.gms.internal.zzef.zza(r0, r3)
            r0.writeString(r4)
            com.google.android.gms.internal.zzef.zza(r0, r5)
            r0.writeInt(r6)
            r2 = 2
            android.os.Parcel r2 = r1.zza(r2, r0)
            android.os.IBinder r3 = r2.readStrongBinder()
            if (r3 != 0) goto L_0x0020
            r3 = 0
            goto L_0x0034
        L_0x0020:
            java.lang.String r4 = "com.google.android.gms.ads.internal.client.IAdManager"
            android.os.IInterface r4 = r3.queryLocalInterface(r4)
            boolean r5 = r4 instanceof com.google.android.gms.internal.zzkb
            if (r5 == 0) goto L_0x002e
            r3 = r4
            com.google.android.gms.internal.zzkb r3 = (com.google.android.gms.internal.zzkb) r3
            goto L_0x0034
        L_0x002e:
            com.google.android.gms.internal.zzkd r4 = new com.google.android.gms.internal.zzkd
            r4.<init>(r3)
            r3 = r4
        L_0x0034:
            r2.recycle()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzkl.createInterstitialAdManager(com.google.android.gms.dynamic.IObjectWrapper, com.google.android.gms.internal.zziw, java.lang.String, com.google.android.gms.internal.zzuc, int):com.google.android.gms.internal.zzkb");
    }

    public final zzpc createNativeAdViewDelegate(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        zzef.zza(zzaz, iObjectWrapper2);
        Parcel zza = zza(5, zzaz);
        zzpc zzl = zzpd.zzl(zza.readStrongBinder());
        zza.recycle();
        return zzl;
    }

    public final zzph createNativeAdViewHolderDelegate(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        zzef.zza(zzaz, iObjectWrapper2);
        zzef.zza(zzaz, iObjectWrapper3);
        Parcel zza = zza(11, zzaz);
        zzph zzm = zzpi.zzm(zza.readStrongBinder());
        zza.recycle();
        return zzm;
    }

    public final zzacq createRewardedVideoAd(IObjectWrapper iObjectWrapper, zzuc zzuc, int i) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        zzef.zza(zzaz, zzuc);
        zzaz.writeInt(i);
        Parcel zza = zza(6, zzaz);
        zzacq zzy = zzacr.zzy(zza.readStrongBinder());
        zza.recycle();
        return zzy;
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzkb createSearchAdManager(com.google.android.gms.dynamic.IObjectWrapper r2, com.google.android.gms.internal.zziw r3, java.lang.String r4, int r5) throws android.os.RemoteException {
        /*
            r1 = this;
            android.os.Parcel r0 = r1.zzaz()
            com.google.android.gms.internal.zzef.zza(r0, r2)
            com.google.android.gms.internal.zzef.zza(r0, r3)
            r0.writeString(r4)
            r0.writeInt(r5)
            r2 = 10
            android.os.Parcel r2 = r1.zza(r2, r0)
            android.os.IBinder r3 = r2.readStrongBinder()
            if (r3 != 0) goto L_0x001e
            r3 = 0
            goto L_0x0032
        L_0x001e:
            java.lang.String r4 = "com.google.android.gms.ads.internal.client.IAdManager"
            android.os.IInterface r4 = r3.queryLocalInterface(r4)
            boolean r5 = r4 instanceof com.google.android.gms.internal.zzkb
            if (r5 == 0) goto L_0x002c
            r3 = r4
            com.google.android.gms.internal.zzkb r3 = (com.google.android.gms.internal.zzkb) r3
            goto L_0x0032
        L_0x002c:
            com.google.android.gms.internal.zzkd r4 = new com.google.android.gms.internal.zzkd
            r4.<init>(r3)
            r3 = r4
        L_0x0032:
            r2.recycle()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzkl.createSearchAdManager(com.google.android.gms.dynamic.IObjectWrapper, com.google.android.gms.internal.zziw, java.lang.String, int):com.google.android.gms.internal.zzkb");
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzkp getMobileAdsSettingsManager(com.google.android.gms.dynamic.IObjectWrapper r4) throws android.os.RemoteException {
        /*
            r3 = this;
            android.os.Parcel r0 = r3.zzaz()
            com.google.android.gms.internal.zzef.zza(r0, r4)
            r4 = 4
            android.os.Parcel r4 = r3.zza(r4, r0)
            android.os.IBinder r0 = r4.readStrongBinder()
            if (r0 != 0) goto L_0x0014
            r0 = 0
            goto L_0x0028
        L_0x0014:
            java.lang.String r1 = "com.google.android.gms.ads.internal.client.IMobileAdsSettingManager"
            android.os.IInterface r1 = r0.queryLocalInterface(r1)
            boolean r2 = r1 instanceof com.google.android.gms.internal.zzkp
            if (r2 == 0) goto L_0x0022
            r0 = r1
            com.google.android.gms.internal.zzkp r0 = (com.google.android.gms.internal.zzkp) r0
            goto L_0x0028
        L_0x0022:
            com.google.android.gms.internal.zzkr r1 = new com.google.android.gms.internal.zzkr
            r1.<init>(r0)
            r0 = r1
        L_0x0028:
            r4.recycle()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzkl.getMobileAdsSettingsManager(com.google.android.gms.dynamic.IObjectWrapper):com.google.android.gms.internal.zzkp");
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzkp getMobileAdsSettingsManagerWithClientJarVersion(com.google.android.gms.dynamic.IObjectWrapper r3, int r4) throws android.os.RemoteException {
        /*
            r2 = this;
            android.os.Parcel r0 = r2.zzaz()
            com.google.android.gms.internal.zzef.zza(r0, r3)
            r0.writeInt(r4)
            r3 = 9
            android.os.Parcel r3 = r2.zza(r3, r0)
            android.os.IBinder r4 = r3.readStrongBinder()
            if (r4 != 0) goto L_0x0018
            r4 = 0
            goto L_0x002c
        L_0x0018:
            java.lang.String r0 = "com.google.android.gms.ads.internal.client.IMobileAdsSettingManager"
            android.os.IInterface r0 = r4.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.internal.zzkp
            if (r1 == 0) goto L_0x0026
            r4 = r0
            com.google.android.gms.internal.zzkp r4 = (com.google.android.gms.internal.zzkp) r4
            goto L_0x002c
        L_0x0026:
            com.google.android.gms.internal.zzkr r0 = new com.google.android.gms.internal.zzkr
            r0.<init>(r4)
            r4 = r0
        L_0x002c:
            r3.recycle()
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzkl.getMobileAdsSettingsManagerWithClientJarVersion(com.google.android.gms.dynamic.IObjectWrapper, int):com.google.android.gms.internal.zzkp");
    }
}
