package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrk;
import java.security.GeneralSecurityException;

public final class zzdpf implements zzdos<zzdoo> {
    zzdpf() {
    }

    private static zzdoo zzd(zzfdh zzfdh) throws GeneralSecurityException {
        try {
            return new zzdrz(zzdqg.zzq(zzfdh).zzblt().toByteArray());
        } catch (zzfew unused) {
            throw new GeneralSecurityException("expected AesGcmKey proto");
        }
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.AesGcmKey";
    }

    public final /* synthetic */ Object zza(zzfdh zzfdh) throws GeneralSecurityException {
        return zzd(zzfdh);
    }

    public final /* synthetic */ Object zza(zzffi zzffi) throws GeneralSecurityException {
        if (!(zzffi instanceof zzdqg)) {
            throw new GeneralSecurityException("expected AesGcmKey proto");
        }
        zzdqg zzdqg = (zzdqg) zzffi;
        zzdte.zzt(zzdqg.getVersion(), 0);
        zzdte.zzgc(zzdqg.zzblt().size());
        return new zzdrz(zzdqg.zzblt().toByteArray());
    }

    public final zzffi zzb(zzfdh zzfdh) throws GeneralSecurityException {
        try {
            return zzb(zzdqi.zzs(zzfdh));
        } catch (zzfew e) {
            throw new GeneralSecurityException("expected serialized AesGcmKeyFormat proto", e);
        }
    }

    public final zzffi zzb(zzffi zzffi) throws GeneralSecurityException {
        if (!(zzffi instanceof zzdqi)) {
            throw new GeneralSecurityException("expected AesGcmKeyFormat proto");
        }
        zzdqi zzdqi = (zzdqi) zzffi;
        zzdte.zzgc(zzdqi.getKeySize());
        return zzdqg.zzbmi().zzr(zzfdh.zzay(zzdtd.zzgb(zzdqi.getKeySize()))).zzb(zzdqi.zzbmk()).zzfl(0).zzcvk();
    }

    public final zzdrk zzc(zzfdh zzfdh) throws GeneralSecurityException {
        return (zzdrk) zzdrk.zzbnv().zzoa("type.googleapis.com/google.crypto.tink.AesGcmKey").zzaa(((zzdqg) zzb(zzfdh)).toByteString()).zzb(zzdrk.zzb.SYMMETRIC).zzcvk();
    }
}
