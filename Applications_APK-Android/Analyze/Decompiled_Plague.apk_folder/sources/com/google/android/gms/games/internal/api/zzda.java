package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzda extends zzdj {
    private /* synthetic */ String zzhkj;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzda(zzcw zzcw, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient, null);
        this.zzhkj = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zzd(this, this.zzhkj);
    }
}
