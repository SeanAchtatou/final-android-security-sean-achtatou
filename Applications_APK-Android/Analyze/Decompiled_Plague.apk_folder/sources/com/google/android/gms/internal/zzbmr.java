package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.drive.DrivePreferencesApi;

final class zzbmr extends zzbjv {
    private final zzn<DrivePreferencesApi.FileUploadPreferencesResult> zzfzc;
    private /* synthetic */ zzbmo zzglx;

    private zzbmr(zzbmo zzbmo, zzn<DrivePreferencesApi.FileUploadPreferencesResult> zzn) {
        this.zzglx = zzbmo;
        this.zzfzc = zzn;
    }

    /* synthetic */ zzbmr(zzbmo zzbmo, zzn zzn, zzbmp zzbmp) {
        this(zzbmo, zzn);
    }

    public final void onError(Status status) throws RemoteException {
        this.zzfzc.setResult(new zzbms(this.zzglx, status, null, null));
    }

    public final void zza(zzbpu zzbpu) throws RemoteException {
        this.zzfzc.setResult(new zzbms(this.zzglx, Status.zzfko, zzbpu.zzgnx, null));
    }
}
