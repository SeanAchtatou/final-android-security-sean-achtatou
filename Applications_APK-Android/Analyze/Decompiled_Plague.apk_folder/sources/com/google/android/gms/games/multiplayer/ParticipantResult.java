package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.internal.zzbem;

public final class ParticipantResult extends zzc {
    public static final Parcelable.Creator<ParticipantResult> CREATOR = new zzd();
    public static final int MATCH_RESULT_DISAGREED = 5;
    public static final int MATCH_RESULT_DISCONNECT = 4;
    public static final int MATCH_RESULT_LOSS = 1;
    public static final int MATCH_RESULT_NONE = 3;
    public static final int MATCH_RESULT_TIE = 2;
    public static final int MATCH_RESULT_UNINITIALIZED = -1;
    public static final int MATCH_RESULT_WIN = 0;
    public static final int PLACING_UNINITIALIZED = -1;
    private final String zzhog;
    private final int zzhuz;
    private final int zzhva;

    public ParticipantResult(String str, int i, int i2) {
        boolean z;
        this.zzhog = (String) zzbq.checkNotNull(str);
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                z = true;
                break;
            default:
                z = false;
                break;
        }
        zzbq.checkState(z);
        this.zzhuz = i;
        this.zzhva = i2;
    }

    public final String getParticipantId() {
        return this.zzhog;
    }

    public final int getPlacing() {
        return this.zzhva;
    }

    public final int getResult() {
        return this.zzhuz;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, getParticipantId(), false);
        zzbem.zzc(parcel, 2, getResult());
        zzbem.zzc(parcel, 3, getPlacing());
        zzbem.zzai(parcel, zze);
    }
}
