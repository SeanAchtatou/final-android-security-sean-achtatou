package com.google.android.gms.ads.internal;

import android.webkit.CookieManager;
import com.google.android.gms.internal.zzmq;
import java.util.concurrent.Callable;

final class zze implements Callable<String> {
    private /* synthetic */ zzd zzand;

    zze(zzd zzd) {
        this.zzand = zzd;
    }

    public final /* synthetic */ Object call() throws Exception {
        CookieManager zzax;
        return (!((Boolean) zzbs.zzep().zzd(zzmq.zzboh)).booleanValue() || (zzax = zzbs.zzee().zzax(this.zzand.zzamt.zzaif)) == null) ? "" : zzax.getCookie("googleads.g.doubleclick.net");
    }
}
