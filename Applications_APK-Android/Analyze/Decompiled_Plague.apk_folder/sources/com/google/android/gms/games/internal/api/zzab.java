package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.GameBuffer;
import com.google.android.gms.games.GamesMetadata;

final class zzab implements GamesMetadata.LoadGamesResult {
    private /* synthetic */ Status zzekv;

    zzab(zzaa zzaa, Status status) {
        this.zzekv = status;
    }

    public final GameBuffer getGames() {
        return new GameBuffer(DataHolder.zzca(14));
    }

    public final Status getStatus() {
        return this.zzekv;
    }

    public final void release() {
    }
}
