package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzc;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.games.internal.player.zzb;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class PlayerEntity extends GamesDowngradeableSafeParcel implements Player {
    public static final Parcelable.Creator<PlayerEntity> CREATOR = new zza();
    private final String mName;
    private String zzedu;
    private final String zzejt;
    private String zzfcj;
    private final Uri zzhgv;
    private final Uri zzhgw;
    private final String zzhhg;
    private final String zzhhh;
    private final long zzhja;
    private final int zzhjb;
    private final long zzhjc;
    private final zzb zzhjd;
    private final PlayerLevelInfo zzhje;
    private final boolean zzhjf;
    private final boolean zzhjg;
    private final String zzhjh;
    private final Uri zzhji;
    private final String zzhjj;
    private final Uri zzhjk;
    private final String zzhjl;
    private final int zzhjm;
    private final long zzhjn;
    private final boolean zzhjo;

    static final class zza extends zzap {
        zza() {
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return createFromParcel(parcel);
        }

        public final PlayerEntity zzj(Parcel parcel) {
            if (PlayerEntity.zze(PlayerEntity.zzaku()) || PlayerEntity.zzgc(PlayerEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            return new PlayerEntity(readString, readString2, readString3 == null ? null : Uri.parse(readString3), readString4 == null ? null : Uri.parse(readString4), parcel.readLong(), -1, -1, null, null, null, null, null, true, false, parcel.readString(), parcel.readString(), null, null, null, null, -1, -1, false);
        }
    }

    public PlayerEntity(Player player) {
        this(player, true);
    }

    private PlayerEntity(Player player, boolean z) {
        this.zzfcj = player.getPlayerId();
        this.zzedu = player.getDisplayName();
        this.zzhgv = player.getIconImageUri();
        this.zzhhg = player.getIconImageUrl();
        this.zzhgw = player.getHiResImageUri();
        this.zzhhh = player.getHiResImageUrl();
        this.zzhja = player.getRetrievedTimestamp();
        this.zzhjb = player.zzaqz();
        this.zzhjc = player.getLastPlayedWithTimestamp();
        this.zzejt = player.getTitle();
        this.zzhjf = player.zzara();
        com.google.android.gms.games.internal.player.zza zzarb = player.zzarb();
        this.zzhjd = zzarb == null ? null : new zzb(zzarb);
        this.zzhje = player.getLevelInfo();
        this.zzhjg = player.zzaqy();
        this.zzhjh = player.zzaqx();
        this.mName = player.getName();
        this.zzhji = player.getBannerImageLandscapeUri();
        this.zzhjj = player.getBannerImageLandscapeUrl();
        this.zzhjk = player.getBannerImagePortraitUri();
        this.zzhjl = player.getBannerImagePortraitUrl();
        this.zzhjm = player.zzarc();
        this.zzhjn = player.zzard();
        this.zzhjo = player.isMuted();
        zzc.zzu(this.zzfcj);
        zzc.zzu(this.zzedu);
        zzc.checkState(this.zzhja > 0);
    }

    PlayerEntity(String str, String str2, Uri uri, Uri uri2, long j, int i, long j2, String str3, String str4, String str5, zzb zzb, PlayerLevelInfo playerLevelInfo, boolean z, boolean z2, String str6, String str7, Uri uri3, String str8, Uri uri4, String str9, int i2, long j3, boolean z3) {
        this.zzfcj = str;
        this.zzedu = str2;
        this.zzhgv = uri;
        this.zzhhg = str3;
        this.zzhgw = uri2;
        this.zzhhh = str4;
        this.zzhja = j;
        this.zzhjb = i;
        this.zzhjc = j2;
        this.zzejt = str5;
        this.zzhjf = z;
        this.zzhjd = zzb;
        this.zzhje = playerLevelInfo;
        this.zzhjg = z2;
        this.zzhjh = str6;
        this.mName = str7;
        this.zzhji = uri3;
        this.zzhjj = str8;
        this.zzhjk = uri4;
        this.zzhjl = str9;
        this.zzhjm = i2;
        this.zzhjn = j3;
        this.zzhjo = z3;
    }

    static int zza(Player player) {
        return Arrays.hashCode(new Object[]{player.getPlayerId(), player.getDisplayName(), Boolean.valueOf(player.zzaqy()), player.getIconImageUri(), player.getHiResImageUri(), Long.valueOf(player.getRetrievedTimestamp()), player.getTitle(), player.getLevelInfo(), player.zzaqx(), player.getName(), player.getBannerImageLandscapeUri(), player.getBannerImagePortraitUri(), Integer.valueOf(player.zzarc()), Long.valueOf(player.zzard()), Boolean.valueOf(player.isMuted())});
    }

    static boolean zza(Player player, Object obj) {
        if (!(obj instanceof Player)) {
            return false;
        }
        if (player == obj) {
            return true;
        }
        Player player2 = (Player) obj;
        return zzbg.equal(player2.getPlayerId(), player.getPlayerId()) && zzbg.equal(player2.getDisplayName(), player.getDisplayName()) && zzbg.equal(Boolean.valueOf(player2.zzaqy()), Boolean.valueOf(player.zzaqy())) && zzbg.equal(player2.getIconImageUri(), player.getIconImageUri()) && zzbg.equal(player2.getHiResImageUri(), player.getHiResImageUri()) && zzbg.equal(Long.valueOf(player2.getRetrievedTimestamp()), Long.valueOf(player.getRetrievedTimestamp())) && zzbg.equal(player2.getTitle(), player.getTitle()) && zzbg.equal(player2.getLevelInfo(), player.getLevelInfo()) && zzbg.equal(player2.zzaqx(), player.zzaqx()) && zzbg.equal(player2.getName(), player.getName()) && zzbg.equal(player2.getBannerImageLandscapeUri(), player.getBannerImageLandscapeUri()) && zzbg.equal(player2.getBannerImagePortraitUri(), player.getBannerImagePortraitUri()) && zzbg.equal(Integer.valueOf(player2.zzarc()), Integer.valueOf(player.zzarc())) && zzbg.equal(Long.valueOf(player2.zzard()), Long.valueOf(player.zzard())) && zzbg.equal(Boolean.valueOf(player2.isMuted()), Boolean.valueOf(player.isMuted()));
    }

    static String zzb(Player player) {
        return zzbg.zzw(player).zzg("PlayerId", player.getPlayerId()).zzg("DisplayName", player.getDisplayName()).zzg("HasDebugAccess", Boolean.valueOf(player.zzaqy())).zzg("IconImageUri", player.getIconImageUri()).zzg("IconImageUrl", player.getIconImageUrl()).zzg("HiResImageUri", player.getHiResImageUri()).zzg("HiResImageUrl", player.getHiResImageUrl()).zzg("RetrievedTimestamp", Long.valueOf(player.getRetrievedTimestamp())).zzg("Title", player.getTitle()).zzg("LevelInfo", player.getLevelInfo()).zzg("GamerTag", player.zzaqx()).zzg("Name", player.getName()).zzg("BannerImageLandscapeUri", player.getBannerImageLandscapeUri()).zzg("BannerImageLandscapeUrl", player.getBannerImageLandscapeUrl()).zzg("BannerImagePortraitUri", player.getBannerImagePortraitUri()).zzg("BannerImagePortraitUrl", player.getBannerImagePortraitUrl()).zzg("GamerFriendStatus", Integer.valueOf(player.zzarc())).zzg("GamerFriendUpdateTimestamp", Long.valueOf(player.zzard())).zzg("IsMuted", Boolean.valueOf(player.isMuted())).toString();
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    public final Player freeze() {
        return this;
    }

    public final Uri getBannerImageLandscapeUri() {
        return this.zzhji;
    }

    public final String getBannerImageLandscapeUrl() {
        return this.zzhjj;
    }

    public final Uri getBannerImagePortraitUri() {
        return this.zzhjk;
    }

    public final String getBannerImagePortraitUrl() {
        return this.zzhjl;
    }

    public final String getDisplayName() {
        return this.zzedu;
    }

    public final void getDisplayName(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.zzedu, charArrayBuffer);
    }

    public final Uri getHiResImageUri() {
        return this.zzhgw;
    }

    public final String getHiResImageUrl() {
        return this.zzhhh;
    }

    public final Uri getIconImageUri() {
        return this.zzhgv;
    }

    public final String getIconImageUrl() {
        return this.zzhhg;
    }

    public final long getLastPlayedWithTimestamp() {
        return this.zzhjc;
    }

    public final PlayerLevelInfo getLevelInfo() {
        return this.zzhje;
    }

    public final String getName() {
        return this.mName;
    }

    public final String getPlayerId() {
        return this.zzfcj;
    }

    public final long getRetrievedTimestamp() {
        return this.zzhja;
    }

    public final String getTitle() {
        return this.zzejt;
    }

    public final void getTitle(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.zzejt, charArrayBuffer);
    }

    public final boolean hasHiResImage() {
        return getHiResImageUri() != null;
    }

    public final boolean hasIconImage() {
        return getIconImageUri() != null;
    }

    public final int hashCode() {
        return zza(this);
    }

    public final boolean isDataValid() {
        return true;
    }

    public final boolean isMuted() {
        return this.zzhjo;
    }

    public final String toString() {
        return zzb(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.internal.player.zzb, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.PlayerLevelInfo, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, getPlayerId(), false);
        zzbem.zza(parcel, 2, getDisplayName(), false);
        zzbem.zza(parcel, 3, (Parcelable) getIconImageUri(), i, false);
        zzbem.zza(parcel, 4, (Parcelable) getHiResImageUri(), i, false);
        zzbem.zza(parcel, 5, getRetrievedTimestamp());
        zzbem.zzc(parcel, 6, this.zzhjb);
        zzbem.zza(parcel, 7, getLastPlayedWithTimestamp());
        zzbem.zza(parcel, 8, getIconImageUrl(), false);
        zzbem.zza(parcel, 9, getHiResImageUrl(), false);
        zzbem.zza(parcel, 14, getTitle(), false);
        zzbem.zza(parcel, 15, (Parcelable) this.zzhjd, i, false);
        zzbem.zza(parcel, 16, (Parcelable) getLevelInfo(), i, false);
        zzbem.zza(parcel, 18, this.zzhjf);
        zzbem.zza(parcel, 19, this.zzhjg);
        zzbem.zza(parcel, 20, this.zzhjh, false);
        zzbem.zza(parcel, 21, this.mName, false);
        zzbem.zza(parcel, 22, (Parcelable) getBannerImageLandscapeUri(), i, false);
        zzbem.zza(parcel, 23, getBannerImageLandscapeUrl(), false);
        zzbem.zza(parcel, 24, (Parcelable) getBannerImagePortraitUri(), i, false);
        zzbem.zza(parcel, 25, getBannerImagePortraitUrl(), false);
        zzbem.zzc(parcel, 26, this.zzhjm);
        zzbem.zza(parcel, 27, this.zzhjn);
        zzbem.zza(parcel, 28, this.zzhjo);
        zzbem.zzai(parcel, zze);
    }

    public final String zzaqx() {
        return this.zzhjh;
    }

    public final boolean zzaqy() {
        return this.zzhjg;
    }

    public final int zzaqz() {
        return this.zzhjb;
    }

    public final boolean zzara() {
        return this.zzhjf;
    }

    public final com.google.android.gms.games.internal.player.zza zzarb() {
        return this.zzhjd;
    }

    public final int zzarc() {
        return this.zzhjm;
    }

    public final long zzard() {
        return this.zzhjn;
    }
}
