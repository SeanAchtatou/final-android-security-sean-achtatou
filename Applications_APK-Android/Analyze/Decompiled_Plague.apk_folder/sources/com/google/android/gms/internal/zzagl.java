package com.google.android.gms.internal;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@zzzb
public final class zzagl {
    public static final ThreadPoolExecutor zzcyx = new ThreadPoolExecutor(20, 20, 1, TimeUnit.MINUTES, new LinkedBlockingQueue(), zzcb("Default"));
    private static final ThreadPoolExecutor zzcyy = new ThreadPoolExecutor(5, 5, 1, TimeUnit.MINUTES, new LinkedBlockingQueue(), zzcb("Loader"));

    static {
        zzcyx.allowCoreThreadTimeOut(true);
        zzcyy.allowCoreThreadTimeOut(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzagl.zza(java.util.concurrent.ExecutorService, java.util.concurrent.Callable):com.google.android.gms.internal.zzajp<T>
     arg types: [java.util.concurrent.ThreadPoolExecutor, java.lang.Object]
     candidates:
      com.google.android.gms.internal.zzagl.zza(int, java.lang.Runnable):com.google.android.gms.internal.zzajp<java.lang.Void>
      com.google.android.gms.internal.zzagl.zza(java.util.concurrent.ExecutorService, java.util.concurrent.Callable):com.google.android.gms.internal.zzajp<T> */
    public static zzajp<Void> zza(int i, Runnable runnable) {
        ThreadPoolExecutor threadPoolExecutor;
        Object zzagn;
        if (i == 1) {
            threadPoolExecutor = zzcyy;
            zzagn = new zzagm(runnable);
        } else {
            threadPoolExecutor = zzcyx;
            zzagn = new zzagn(runnable);
        }
        return zza((ExecutorService) threadPoolExecutor, (Callable) zzagn);
    }

    public static zzajp<Void> zza(Runnable runnable) {
        return zza(0, runnable);
    }

    public static <T> zzajp<T> zza(ExecutorService executorService, Callable<T> callable) {
        zzajy zzajy = new zzajy();
        try {
            zzajy.zza(new zzagp(zzajy, executorService.submit(new zzago(zzajy, callable))), zzaju.zzdct);
            return zzajy;
        } catch (RejectedExecutionException e) {
            zzafj.zzc("Thread execution is rejected.", e);
            zzajy.setException(e);
            return zzajy;
        }
    }

    private static ThreadFactory zzcb(String str) {
        return new zzagq(str);
    }
}
