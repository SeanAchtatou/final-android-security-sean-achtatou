package com.google.android.gms.internal;

import java.io.IOException;

public final class zzdrt extends zzfee<zzdrt, zza> implements zzffk {
    private static volatile zzffm<zzdrt> zzbas;
    /* access modifiers changed from: private */
    public static final zzdrt zzltq;
    private int zzltg;
    private int zzlth;
    private zzfev<zzb> zzltp = zzcvf();

    public static final class zza extends zzfef<zzdrt, zza> implements zzffk {
        private zza() {
            super(zzdrt.zzltq);
        }

        /* synthetic */ zza(zzdru zzdru) {
            this();
        }

        public final zza zzb(zzb zzb) {
            zzcvi();
            ((zzdrt) this.zzpbv).zza(zzb);
            return this;
        }

        public final zza zzfu(int i) {
            zzcvi();
            ((zzdrt) this.zzpbv).zzft(i);
            return this;
        }
    }

    public static final class zzb extends zzfee<zzb, zza> implements zzffk {
        private static volatile zzffm<zzb> zzbas;
        /* access modifiers changed from: private */
        public static final zzb zzltr;
        private String zzlso = "";
        private int zzltl;
        private int zzltm;
        private int zzltn;

        public static final class zza extends zzfef<zzb, zza> implements zzffk {
            private zza() {
                super(zzb.zzltr);
            }

            /* synthetic */ zza(zzdru zzdru) {
                this();
            }

            public final zza zzb(zzdrn zzdrn) {
                zzcvi();
                ((zzb) this.zzpbv).zza(zzdrn);
                return this;
            }

            public final zza zzb(zzdrv zzdrv) {
                zzcvi();
                ((zzb) this.zzpbv).zza(zzdrv);
                return this;
            }

            public final zza zzfw(int i) {
                zzcvi();
                ((zzb) this.zzpbv).zzfv(i);
                return this;
            }

            public final zza zzob(String str) {
                zzcvi();
                ((zzb) this.zzpbv).zznz(str);
                return this;
            }
        }

        static {
            zzb zzb = new zzb();
            zzltr = zzb;
            zzb.zza(zzfem.zzpcf, (Object) null, (Object) null);
            zzb.zzpbs.zzbim();
        }

        private zzb() {
        }

        /* access modifiers changed from: private */
        public final void zza(zzdrn zzdrn) {
            if (zzdrn == null) {
                throw new NullPointerException();
            }
            this.zzltl = zzdrn.zzhn();
        }

        /* access modifiers changed from: private */
        public final void zza(zzdrv zzdrv) {
            if (zzdrv == null) {
                throw new NullPointerException();
            }
            this.zzltn = zzdrv.zzhn();
        }

        public static zza zzboo() {
            zzb zzb = zzltr;
            zzfef zzfef = (zzfef) zzb.zza(zzfem.zzpch, (Object) null, (Object) null);
            zzfef.zza((zzfee) zzb);
            return (zza) zzfef;
        }

        public static zzb zzbop() {
            return zzltr;
        }

        /* access modifiers changed from: private */
        public final void zzfv(int i) {
            this.zzltm = i;
        }

        /* access modifiers changed from: private */
        public final void zznz(String str) {
            if (str == null) {
                throw new NullPointerException();
            }
            this.zzlso = str;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            boolean z = true;
            boolean z2 = false;
            switch (zzdru.zzbaq[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return zzltr;
                case 3:
                    return null;
                case 4:
                    return new zza(null);
                case 5:
                    zzfen zzfen = (zzfen) obj;
                    zzb zzb = (zzb) obj2;
                    this.zzlso = zzfen.zza(!this.zzlso.isEmpty(), this.zzlso, !zzb.zzlso.isEmpty(), zzb.zzlso);
                    this.zzltl = zzfen.zza(this.zzltl != 0, this.zzltl, zzb.zzltl != 0, zzb.zzltl);
                    this.zzltm = zzfen.zza(this.zzltm != 0, this.zzltm, zzb.zzltm != 0, zzb.zzltm);
                    boolean z3 = this.zzltn != 0;
                    int i2 = this.zzltn;
                    if (zzb.zzltn == 0) {
                        z = false;
                    }
                    this.zzltn = zzfen.zza(z3, i2, z, zzb.zzltn);
                    return this;
                case 6:
                    zzfdq zzfdq = (zzfdq) obj;
                    if (((zzfea) obj2) != null) {
                        while (!z2) {
                            try {
                                int zzcts = zzfdq.zzcts();
                                if (zzcts != 0) {
                                    if (zzcts == 10) {
                                        this.zzlso = zzfdq.zzctz();
                                    } else if (zzcts == 16) {
                                        this.zzltl = zzfdq.zzcuc();
                                    } else if (zzcts == 24) {
                                        this.zzltm = zzfdq.zzcub();
                                    } else if (zzcts == 32) {
                                        this.zzltn = zzfdq.zzcuc();
                                    } else if (!zza(zzcts, zzfdq)) {
                                    }
                                }
                                z2 = true;
                            } catch (zzfew e) {
                                throw new RuntimeException(e.zzh(this));
                            } catch (IOException e2) {
                                throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                            }
                        }
                        break;
                    } else {
                        throw new NullPointerException();
                    }
                case 7:
                    break;
                case 8:
                    if (zzbas == null) {
                        synchronized (zzb.class) {
                            if (zzbas == null) {
                                zzbas = new zzfeg(zzltr);
                            }
                        }
                    }
                    return zzbas;
                default:
                    throw new UnsupportedOperationException();
            }
            return zzltr;
        }

        public final void zza(zzfdv zzfdv) throws IOException {
            if (!this.zzlso.isEmpty()) {
                zzfdv.zzn(1, this.zzlso);
            }
            if (this.zzltl != zzdrn.UNKNOWN_STATUS.zzhn()) {
                zzfdv.zzaa(2, this.zzltl);
            }
            if (this.zzltm != 0) {
                zzfdv.zzab(3, this.zzltm);
            }
            if (this.zzltn != zzdrv.UNKNOWN_PREFIX.zzhn()) {
                zzfdv.zzaa(4, this.zzltn);
            }
            this.zzpbs.zza(zzfdv);
        }

        public final int zzhl() {
            int i = this.zzpbt;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (!this.zzlso.isEmpty()) {
                i2 = 0 + zzfdv.zzo(1, this.zzlso);
            }
            if (this.zzltl != zzdrn.UNKNOWN_STATUS.zzhn()) {
                i2 += zzfdv.zzag(2, this.zzltl);
            }
            if (this.zzltm != 0) {
                i2 += zzfdv.zzae(3, this.zzltm);
            }
            if (this.zzltn != zzdrv.UNKNOWN_PREFIX.zzhn()) {
                i2 += zzfdv.zzag(4, this.zzltn);
            }
            int zzhl = i2 + this.zzpbs.zzhl();
            this.zzpbt = zzhl;
            return zzhl;
        }
    }

    static {
        zzdrt zzdrt = new zzdrt();
        zzltq = zzdrt;
        zzdrt.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdrt.zzpbs.zzbim();
    }

    private zzdrt() {
    }

    /* access modifiers changed from: private */
    public final void zza(zzb zzb2) {
        if (zzb2 == null) {
            throw new NullPointerException();
        }
        if (!this.zzltp.zzcth()) {
            zzfev<zzb> zzfev = this.zzltp;
            int size = zzfev.size();
            this.zzltp = zzfev.zzln(size == 0 ? 10 : size << 1);
        }
        this.zzltp.add(zzb2);
    }

    public static zza zzbol() {
        zzdrt zzdrt = zzltq;
        zzfef zzfef = (zzfef) zzdrt.zza(zzfem.zzpch, (Object) null, (Object) null);
        zzfef.zza((zzfee) zzdrt);
        return (zza) zzfef;
    }

    public static zzdrt zzbom() {
        return zzltq;
    }

    /* access modifiers changed from: private */
    public final void zzft(int i) {
        this.zzlth = i;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        boolean z = true;
        boolean z2 = false;
        switch (zzdru.zzbaq[i - 1]) {
            case 1:
                return new zzdrt();
            case 2:
                return zzltq;
            case 3:
                this.zzltp.zzbim();
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdrt zzdrt = (zzdrt) obj2;
                boolean z3 = this.zzlth != 0;
                int i2 = this.zzlth;
                if (zzdrt.zzlth == 0) {
                    z = false;
                }
                this.zzlth = zzfen.zza(z3, i2, z, zzdrt.zzlth);
                this.zzltp = zzfen.zza(this.zzltp, zzdrt.zzltp);
                if (zzfen == zzfel.zzpcb) {
                    this.zzltg |= zzdrt.zzltg;
                }
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzlth = zzfdq.zzcub();
                                } else if (zzcts == 18) {
                                    if (!this.zzltp.zzcth()) {
                                        zzfev<zzb> zzfev = this.zzltp;
                                        int size = zzfev.size();
                                        this.zzltp = zzfev.zzln(size == 0 ? 10 : size << 1);
                                    }
                                    this.zzltp.add((zzb) zzfdq.zza(zzb.zzbop(), zzfea));
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdrt.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzltq);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzltq;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlth != 0) {
            zzfdv.zzab(1, this.zzlth);
        }
        for (int i = 0; i < this.zzltp.size(); i++) {
            zzfdv.zza(2, this.zzltp.get(i));
        }
        this.zzpbs.zza(zzfdv);
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int zzae = this.zzlth != 0 ? zzfdv.zzae(1, this.zzlth) + 0 : 0;
        for (int i2 = 0; i2 < this.zzltp.size(); i2++) {
            zzae += zzfdv.zzb(2, this.zzltp.get(i2));
        }
        int zzhl = zzae + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
