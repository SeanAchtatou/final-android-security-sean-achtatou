package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.DriveId;

final class zzblp extends zzblk {
    private /* synthetic */ DriveId zzgkz;
    private /* synthetic */ int zzgla = 1;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzblp(zzbll zzbll, GoogleApiClient googleApiClient, DriveId driveId, int i) {
        super(googleApiClient);
        this.zzgkz = driveId;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbrd(this.zzgkz, this.zzgla), (zzbpj) null, (String) null, new zzbrj(this));
    }
}
