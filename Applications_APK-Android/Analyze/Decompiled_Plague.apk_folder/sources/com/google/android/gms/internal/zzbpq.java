package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.zza;
import com.google.android.gms.drive.zzy;
import java.util.List;

public final class zzbpq extends zzy {
    public static final Parcelable.Creator<zzbpq> CREATOR = new zzbpr();
    private DataHolder zzgns;
    private List<DriveId> zzgnt;
    private zza zzgnu;
    private boolean zzgnv;

    public zzbpq(DataHolder dataHolder, List<DriveId> list, zza zza, boolean z) {
        this.zzgns = dataHolder;
        this.zzgnt = list;
        this.zzgnu = zza;
        this.zzgnv = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.data.DataHolder, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.zza, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* access modifiers changed from: protected */
    public final void zzaj(Parcel parcel, int i) {
        int i2 = i | 1;
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, (Parcelable) this.zzgns, i2, false);
        zzbem.zzc(parcel, 3, this.zzgnt, false);
        zzbem.zza(parcel, 4, (Parcelable) this.zzgnu, i2, false);
        zzbem.zza(parcel, 5, this.zzgnv);
        zzbem.zzai(parcel, zze);
    }
}
