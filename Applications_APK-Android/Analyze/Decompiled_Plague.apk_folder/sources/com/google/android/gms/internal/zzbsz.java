package com.google.android.gms.internal;

import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.zzb;
import com.google.android.gms.drive.metadata.internal.zzh;

public final class zzbsz {
    public static final MetadataField<Integer> zzgrq = new zzh("contentAvailability", 4300000);
    public static final MetadataField<Boolean> zzgrr = new zzb("isPinnable", 4300000);
}
