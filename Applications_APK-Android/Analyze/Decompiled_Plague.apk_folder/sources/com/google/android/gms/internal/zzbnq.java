package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.ExecutionOptions;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.zzo;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbnq extends zzbny<DriveFile> {
    private /* synthetic */ DriveFolder zzgml;
    private /* synthetic */ ExecutionOptions zzgmm;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbnq(zzbmu zzbmu, MetadataChangeSet metadataChangeSet, DriveContents driveContents, DriveFolder driveFolder, ExecutionOptions executionOptions) {
        super(metadataChangeSet, driveContents);
        this.zzgml = driveFolder;
        this.zzgmm = executionOptions;
    }

    /* access modifiers changed from: protected */
    public final void zza(zzbll zzbll, TaskCompletionSource<DriveFile> taskCompletionSource) throws RemoteException {
        ((zzbpf) zzbll.zzakb()).zza(new zzbkg(this.zzgml.getDriveId(), this.zzgms.zzanz(), this.zzgmt, this.zzgjv, this.zzgmq), new zzbru(taskCompletionSource));
    }

    /* access modifiers changed from: protected */
    public final zzo zzaos() {
        return zzo.zza(this.zzgmm);
    }
}
