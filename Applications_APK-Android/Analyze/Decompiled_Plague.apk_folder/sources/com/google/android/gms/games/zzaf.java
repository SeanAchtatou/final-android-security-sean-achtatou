package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.LeaderboardsClient;
import com.google.android.gms.games.leaderboard.Leaderboard;
import com.google.android.gms.games.leaderboard.Leaderboards;

final class zzaf implements zzbo<Leaderboards.LoadScoresResult, LeaderboardsClient.LeaderboardScores> {
    zzaf() {
    }

    public final /* synthetic */ Object zzb(@Nullable Result result) {
        Leaderboards.LoadScoresResult loadScoresResult = (Leaderboards.LoadScoresResult) result;
        Leaderboard leaderboard = null;
        if (loadScoresResult == null) {
            return null;
        }
        if (loadScoresResult.getLeaderboard() != null) {
            leaderboard = (Leaderboard) loadScoresResult.getLeaderboard().freeze();
        }
        return new LeaderboardsClient.LeaderboardScores(leaderboard, loadScoresResult.getScores());
    }
}
