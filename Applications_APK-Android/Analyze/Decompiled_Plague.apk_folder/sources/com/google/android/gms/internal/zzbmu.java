package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.common.api.internal.zzcn;
import com.google.android.gms.common.internal.zzbj;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.ExecutionOptions;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.events.ListenerToken;
import com.google.android.gms.drive.events.OnChangeListener;
import com.google.android.gms.drive.events.OpenFileCallback;
import com.google.android.gms.drive.events.zzj;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.zzr;
import com.google.android.gms.drive.zzt;
import com.google.android.gms.tasks.Task;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public final class zzbmu extends DriveResourceClient {
    private static final AtomicInteger zzglz = new AtomicInteger();
    private final DriveApi zzglb = new zzbkp();

    public zzbmu(@NonNull Activity activity, @Nullable Drive.zza zza) {
        super(activity, zza);
    }

    public zzbmu(@NonNull Context context, @Nullable Drive.zza zza) {
        super(context, zza);
    }

    static final /* synthetic */ ListenerToken zza(zzcl zzcl, Task task) throws Exception {
        if (task.isSuccessful()) {
            return new zzbjq(zzcl.zzajc());
        }
        throw task.getException();
    }

    static final /* synthetic */ ListenerToken zza(zzbjq zzbjq, Task task) throws Exception {
        if (task.isSuccessful()) {
            return zzbjq;
        }
        throw task.getException();
    }

    private static void zzcu(int i) {
        if (i != 268435456 && i != 536870912 && i != 805306368) {
            throw new IllegalArgumentException("Invalid openMode provided");
        }
    }

    public final Task<ListenerToken> addChangeListener(@NonNull DriveResource driveResource, @NonNull OnChangeListener onChangeListener) {
        zzbq.checkNotNull(driveResource.getDriveId());
        zzbq.checkNotNull(onChangeListener, "listener");
        zzbnz zzbnz = new zzbnz(this, onChangeListener, driveResource.getDriveId());
        int incrementAndGet = zzglz.incrementAndGet();
        StringBuilder sb = new StringBuilder(27);
        sb.append("OnChangeListener");
        sb.append(incrementAndGet);
        zzcl zza = zza(zzbnz, sb.toString());
        return zza(new zzbnd(this, zza, driveResource, zzbnz), new zzbne(this, zza.zzajc(), driveResource, zzbnz)).continueWith(new zzbmw(zza));
    }

    public final Task<Void> addChangeSubscription(@NonNull DriveResource driveResource) {
        zzbq.checkNotNull(driveResource.getDriveId());
        zzbq.checkArgument(zzj.zza(1, driveResource.getDriveId()));
        return zzb(new zzbnf(this, driveResource));
    }

    public final Task<Boolean> cancelOpenFileCallback(@NonNull ListenerToken listenerToken) {
        if (listenerToken instanceof zzbjq) {
            return zza(((zzbjq) listenerToken).zzaom());
        }
        throw new IllegalArgumentException("Unrecognized ListenerToken");
    }

    public final Task<Void> commitContents(@NonNull DriveContents driveContents, @Nullable MetadataChangeSet metadataChangeSet) {
        return commitContents(driveContents, metadataChangeSet, (zzr) new zzt().build());
    }

    public final Task<Void> commitContents(@NonNull DriveContents driveContents, @Nullable MetadataChangeSet metadataChangeSet, @NonNull ExecutionOptions executionOptions) {
        zzbq.checkNotNull(executionOptions, "Execution options cannot be null.");
        boolean z = true;
        zzbq.checkArgument(!driveContents.zzanr(), "DriveContents is already closed");
        if (driveContents.getMode() == 268435456) {
            z = false;
        }
        zzbq.checkArgument(z, "Cannot commit contents opened in MODE_READ_ONLY.");
        zzbq.checkNotNull(driveContents.getDriveId(), "Only DriveContents obtained through DriveFile.open can be committed.");
        zzr zzb = zzr.zzb(executionOptions);
        if (!ExecutionOptions.zzcr(zzb.zzanu()) || driveContents.zzanp().zzanh()) {
            if (metadataChangeSet == null) {
                metadataChangeSet = MetadataChangeSet.zzghi;
            }
            return zzb(new zzbnn(this, zzb, driveContents, metadataChangeSet));
        }
        throw new IllegalStateException("DriveContents must be valid for conflict detection.");
    }

    public final Task<DriveContents> createContents() {
        return zzb(new zzbnk(this));
    }

    public final Task<DriveFile> createFile(@NonNull DriveFolder driveFolder, @NonNull MetadataChangeSet metadataChangeSet, @Nullable DriveContents driveContents) {
        return zzb(new zzbnp(this, metadataChangeSet, driveContents, driveFolder));
    }

    public final Task<DriveFile> createFile(@NonNull DriveFolder driveFolder, @NonNull MetadataChangeSet metadataChangeSet, @Nullable DriveContents driveContents, @NonNull ExecutionOptions executionOptions) {
        zzbq.checkNotNull(executionOptions, "executionOptions cannot be null");
        return zzb(new zzbnq(this, metadataChangeSet, driveContents, driveFolder, executionOptions));
    }

    public final Task<DriveFolder> createFolder(@NonNull DriveFolder driveFolder, @NonNull MetadataChangeSet metadataChangeSet) {
        zzbq.checkNotNull(metadataChangeSet, "MetadataChangeSet must be provided.");
        if (metadataChangeSet.getMimeType() == null || metadataChangeSet.getMimeType().equals(DriveFolder.MIME_TYPE)) {
            return zzb(new zzbnr(this, metadataChangeSet, driveFolder));
        }
        throw new IllegalArgumentException("The mimetype must be of type application/vnd.google-apps.folder");
    }

    public final Task<Void> delete(@NonNull DriveResource driveResource) {
        zzbq.checkNotNull(driveResource.getDriveId());
        return zzb(new zzbnw(this, driveResource));
    }

    public final Task<Void> discardContents(@NonNull DriveContents driveContents) {
        zzbq.checkArgument(!driveContents.zzanr(), "DriveContents is already closed");
        driveContents.zzanq();
        return zzb(new zzbno(this, driveContents));
    }

    public final Task<DriveFolder> getAppFolder() {
        return zza(new zzbnl(this));
    }

    public final Task<Metadata> getMetadata(@NonNull DriveResource driveResource) {
        zzbq.checkNotNull(driveResource.getDriveId());
        return zza(new zzbns(this, driveResource));
    }

    public final Task<DriveFolder> getRootFolder() {
        return zza(new zzbna(this));
    }

    public final Task<MetadataBuffer> listChildren(@NonNull DriveFolder driveFolder) {
        return zzbj.zza(this.zzglb.query(zzagb(), zzbmf.zza((Query) null, driveFolder.getDriveId())), zzbmy.zzglc);
    }

    public final Task<MetadataBuffer> listParents(@NonNull DriveResource driveResource) {
        zzbq.checkNotNull(driveResource.getDriveId());
        return zza(new zzbnu(this, driveResource));
    }

    public final Task<DriveContents> openFile(@NonNull DriveFile driveFile, int i) {
        zzcu(i);
        return zza(new zzbnh(this, driveFile, i));
    }

    public final Task<ListenerToken> openFile(@NonNull DriveFile driveFile, int i, @NonNull OpenFileCallback openFileCallback) {
        zzcu(i);
        int incrementAndGet = zzglz.incrementAndGet();
        StringBuilder sb = new StringBuilder(27);
        sb.append("OpenFileCallback");
        sb.append(incrementAndGet);
        zzcl zza = zza(openFileCallback, sb.toString());
        zzcn zzajc = zza.zzajc();
        zzbjq zzbjq = new zzbjq(zzajc);
        return zza(new zzbni(this, zza, driveFile, i, zzbjq, zza), new zzbnj(this, zzajc, zzbjq)).continueWith(new zzbmx(zzbjq));
    }

    public final Task<MetadataBuffer> query(@NonNull Query query) {
        return zzbj.zza(this.zzglb.query(zzagb(), query), zzbmv.zzglc);
    }

    public final Task<MetadataBuffer> queryChildren(@NonNull DriveFolder driveFolder, @NonNull Query query) {
        return zzbj.zza(this.zzglb.query(zzagb(), zzbmf.zza(query, driveFolder.getDriveId())), zzbmz.zzglc);
    }

    public final Task<Boolean> removeChangeListener(@NonNull ListenerToken listenerToken) {
        zzbq.checkNotNull(listenerToken, "Token is required to unregister listener.");
        if (listenerToken instanceof zzbjq) {
            return zza(((zzbjq) listenerToken).zzaom());
        }
        throw new IllegalStateException("Could not recover key from ListenerToken");
    }

    public final Task<Void> removeChangeSubscription(@NonNull DriveResource driveResource) {
        zzbq.checkNotNull(driveResource.getDriveId());
        zzbq.checkArgument(zzj.zza(1, driveResource.getDriveId()));
        return zzb(new zzbng(this, driveResource));
    }

    public final Task<DriveContents> reopenContentsForWrite(@NonNull DriveContents driveContents) {
        boolean z = true;
        zzbq.checkArgument(!driveContents.zzanr(), "DriveContents is already closed");
        if (driveContents.getMode() != 268435456) {
            z = false;
        }
        zzbq.checkArgument(z, "This method can only be called on contents that are currently opened in MODE_READ_ONLY.");
        driveContents.zzanq();
        return zza(new zzbnm(this, driveContents));
    }

    public final Task<Void> setParents(@NonNull DriveResource driveResource, @NonNull Set<DriveId> set) {
        zzbq.checkNotNull(driveResource.getDriveId());
        zzbq.checkNotNull(set);
        return zzb(new zzbnv(this, driveResource, new ArrayList(set)));
    }

    public final Task<Void> trash(@NonNull DriveResource driveResource) {
        zzbq.checkNotNull(driveResource.getDriveId());
        return zzb(new zzbnb(this, driveResource));
    }

    public final Task<Void> untrash(@NonNull DriveResource driveResource) {
        zzbq.checkNotNull(driveResource.getDriveId());
        return zzb(new zzbnc(this, driveResource));
    }

    public final Task<Metadata> updateMetadata(@NonNull DriveResource driveResource, @NonNull MetadataChangeSet metadataChangeSet) {
        zzbq.checkNotNull(driveResource.getDriveId());
        zzbq.checkNotNull(metadataChangeSet);
        return zzb(new zzbnt(this, metadataChangeSet, driveResource));
    }
}
