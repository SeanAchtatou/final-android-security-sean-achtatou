package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.IInterface;
import com.google.android.gms.common.api.Api;

public final class zzbz<T extends IInterface> extends zzab<T> {
    public static Api.zzg<T> zzalg() {
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: protected */
    public final T zzd(IBinder iBinder) {
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: protected */
    public final String zzhf() {
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: protected */
    public final String zzhg() {
        throw new NoSuchMethodError();
    }
}
