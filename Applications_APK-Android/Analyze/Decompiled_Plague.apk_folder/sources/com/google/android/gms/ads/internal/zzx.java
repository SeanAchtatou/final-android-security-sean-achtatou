package com.google.android.gms.ads.internal;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewTreeObserver;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzaeh;
import com.google.android.gms.internal.zzaek;
import com.google.android.gms.internal.zzaeu;
import com.google.android.gms.internal.zzaev;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzagr;
import com.google.android.gms.internal.zzaiy;
import com.google.android.gms.internal.zzama;
import com.google.android.gms.internal.zzamj;
import com.google.android.gms.internal.zzamm;
import com.google.android.gms.internal.zzanp;
import com.google.android.gms.internal.zzfy;
import com.google.android.gms.internal.zzis;
import com.google.android.gms.internal.zziw;
import com.google.android.gms.internal.zzjk;
import com.google.android.gms.internal.zzku;
import com.google.android.gms.internal.zzuc;
import com.google.android.gms.internal.zzuo;
import com.google.android.gms.internal.zzur;
import com.google.android.gms.internal.zzzb;
import com.tapjoy.TapjoyAuctionFlags;
import java.lang.ref.WeakReference;
import java.util.List;

@zzzb
public final class zzx extends zzi implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener {
    private boolean zzals;
    private WeakReference<Object> zzanx = new WeakReference<>(null);

    public zzx(Context context, zziw zziw, String str, zzuc zzuc, zzaiy zzaiy, zzv zzv) {
        super(context, zziw, str, zzuc, zzaiy, zzv);
    }

    private final boolean zzd(@Nullable zzaeu zzaeu, zzaeu zzaeu2) {
        if (zzaeu2.zzcng) {
            View zze = zzaq.zze(zzaeu2);
            if (zze == null) {
                zzafj.zzco("Could not get mediation view");
                return false;
            }
            View nextView = this.zzamt.zzate.getNextView();
            if (nextView != null) {
                if (nextView instanceof zzama) {
                    ((zzama) nextView).destroy();
                }
                this.zzamt.zzate.removeView(nextView);
            }
            if (!zzaq.zzf(zzaeu2)) {
                try {
                    if (zzbs.zzfa().zzs(this.zzamt.zzaif)) {
                        new zzfy(this.zzamt.zzaif, zze).zza(new zzaek(this.zzamt.zzaif, this.zzamt.zzatb));
                    }
                    if (zzaeu2.zzcvu != null) {
                        this.zzamt.zzate.setMinimumWidth(zzaeu2.zzcvu.widthPixels);
                        this.zzamt.zzate.setMinimumHeight(zzaeu2.zzcvu.heightPixels);
                    }
                    zzb(zze);
                } catch (Exception e) {
                    zzbs.zzeg().zza(e, "BannerAdManager.swapViews");
                    zzafj.zzc("Could not add mediation view to view hierarchy.", e);
                    return false;
                }
            }
        } else if (!(zzaeu2.zzcvu == null || zzaeu2.zzchj == null)) {
            zzaeu2.zzchj.zza(zzanp.zzc(zzaeu2.zzcvu));
            this.zzamt.zzate.removeAllViews();
            this.zzamt.zzate.setMinimumWidth(zzaeu2.zzcvu.widthPixels);
            this.zzamt.zzate.setMinimumHeight(zzaeu2.zzcvu.heightPixels);
            zzama zzama = zzaeu2.zzchj;
            if (zzama == null) {
                throw null;
            }
            zzb((View) zzama);
        }
        if (this.zzamt.zzate.getChildCount() > 1) {
            this.zzamt.zzate.showNext();
        }
        if (zzaeu != null) {
            View nextView2 = this.zzamt.zzate.getNextView();
            if (nextView2 instanceof zzama) {
                ((zzama) nextView2).destroy();
            } else if (nextView2 != null) {
                this.zzamt.zzate.removeView(nextView2);
            }
            this.zzamt.zzff();
        }
        this.zzamt.zzate.setVisibility(0);
        return true;
    }

    @Nullable
    public final zzku getVideoController() {
        zzbq.zzfz("getVideoController must be called from the main thread.");
        if (this.zzamt.zzati == null || this.zzamt.zzati.zzchj == null) {
            return null;
        }
        return this.zzamt.zzati.zzchj.zzrx();
    }

    public final void onGlobalLayout() {
        zzd(this.zzamt.zzati);
    }

    public final void onScrollChanged() {
        zzd(this.zzamt.zzati);
    }

    public final void setManualImpressionsEnabled(boolean z) {
        zzbq.zzfz("setManualImpressionsEnabled must be called from the main thread.");
        this.zzals = z;
    }

    public final void showInterstitial() {
        throw new IllegalStateException("Interstitial is NOT supported by BannerAdManager.");
    }

    /* access modifiers changed from: protected */
    public final zzama zza(zzaev zzaev, @Nullable zzw zzw, @Nullable zzaeh zzaeh) throws zzamm {
        zziw zziw;
        AdSize adSize;
        if (this.zzamt.zzath.zzbdc == null && this.zzamt.zzath.zzbde) {
            zzbt zzbt = this.zzamt;
            if (zzaev.zzcwe.zzbde) {
                zziw = this.zzamt.zzath;
            } else {
                String str = zzaev.zzcwe.zzcnj;
                if (str != null) {
                    String[] split = str.split("[xX]");
                    split[0] = split[0].trim();
                    split[1] = split[1].trim();
                    adSize = new AdSize(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
                } else {
                    adSize = this.zzamt.zzath.zzhq();
                }
                zziw = new zziw(this.zzamt.zzaif, adSize);
            }
            zzbt.zzath = zziw;
        }
        return super.zza(zzaev, zzw, zzaeh);
    }

    /* access modifiers changed from: protected */
    public final void zza(@Nullable zzaeu zzaeu, boolean z) {
        View view;
        super.zza(zzaeu, z);
        if (zzaq.zzf(zzaeu)) {
            zzab zzab = new zzab(this);
            if (zzaeu != null && zzaq.zzf(zzaeu)) {
                zzama zzama = zzaeu.zzchj;
                if (zzama == null) {
                    view = null;
                } else if (zzama == null) {
                    throw null;
                } else {
                    view = (View) zzama;
                }
                if (view == null) {
                    zzafj.zzco("AdWebView is null");
                    return;
                }
                try {
                    List<String> list = zzaeu.zzcdd != null ? zzaeu.zzcdd.zzcbq : null;
                    if (list != null) {
                        if (!list.isEmpty()) {
                            zzuo zzly = zzaeu.zzcde != null ? zzaeu.zzcde.zzly() : null;
                            zzur zzlz = zzaeu.zzcde != null ? zzaeu.zzcde.zzlz() : null;
                            if (list.contains("2") && zzly != null) {
                                zzly.zzi(zzn.zzy(view));
                                if (!zzly.getOverrideImpressionRecording()) {
                                    zzly.recordImpression();
                                }
                                zzama.zzsq().zza("/nativeExpressViewClicked", zzaq.zza(zzly, (zzur) null, zzab));
                                return;
                            } else if (!list.contains(TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE) || zzlz == null) {
                                zzafj.zzco("No matching template id and mapper");
                                return;
                            } else {
                                zzlz.zzi(zzn.zzy(view));
                                if (!zzlz.getOverrideImpressionRecording()) {
                                    zzlz.recordImpression();
                                }
                                zzama.zzsq().zza("/nativeExpressViewClicked", zzaq.zza((zzuo) null, zzlz, zzab));
                                return;
                            }
                        }
                    }
                    zzafj.zzco("No template ids present in mediation response");
                } catch (RemoteException e) {
                    zzafj.zzc("Error occurred while recording impression and registering for clicks", e);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzakg.zza(android.view.View, android.view.ViewTreeObserver$OnGlobalLayoutListener):void
     arg types: [com.google.android.gms.ads.internal.zzbu, com.google.android.gms.ads.internal.zzx]
     candidates:
      com.google.android.gms.internal.zzakg.zza(android.view.View, android.view.ViewTreeObserver$OnScrollChangedListener):void
      com.google.android.gms.internal.zzakg.zza(android.view.View, android.view.ViewTreeObserver$OnGlobalLayoutListener):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzakg.zza(android.view.View, android.view.ViewTreeObserver$OnScrollChangedListener):void
     arg types: [com.google.android.gms.ads.internal.zzbu, com.google.android.gms.ads.internal.zzx]
     candidates:
      com.google.android.gms.internal.zzakg.zza(android.view.View, android.view.ViewTreeObserver$OnGlobalLayoutListener):void
      com.google.android.gms.internal.zzakg.zza(android.view.View, android.view.ViewTreeObserver$OnScrollChangedListener):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, boolean):void
     arg types: [com.google.android.gms.internal.zzaeu, int]
     candidates:
      com.google.android.gms.ads.internal.zzx.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zzi.zza(com.google.android.gms.internal.zzaev, com.google.android.gms.internal.zznd):void
      com.google.android.gms.ads.internal.zzi.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzpu, java.lang.String):void
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaaa, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzis, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzaev, com.google.android.gms.internal.zznd):void
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzww, java.lang.String):void
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzis, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.internal.zzkb.zza(com.google.android.gms.internal.zzww, java.lang.String):void
      com.google.android.gms.internal.zzto.zza(com.google.android.gms.internal.zzpu, java.lang.String):void
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0079, code lost:
        if (((java.lang.Boolean) com.google.android.gms.ads.internal.zzbs.zzep().zzd(com.google.android.gms.internal.zzmq.zzbmr)).booleanValue() != false) goto L_0x007b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(@android.support.annotation.Nullable com.google.android.gms.internal.zzaeu r4, com.google.android.gms.internal.zzaeu r5) {
        /*
            r3 = this;
            boolean r0 = super.zza(r4, r5)
            r1 = 0
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            com.google.android.gms.ads.internal.zzbt r0 = r3.zzamt
            boolean r0 = r0.zzfg()
            if (r0 == 0) goto L_0x0025
            boolean r4 = r3.zzd(r4, r5)
            if (r4 != 0) goto L_0x0025
            com.google.android.gms.internal.zzib r4 = r5.zzcwc
            if (r4 == 0) goto L_0x0021
            com.google.android.gms.internal.zzib r4 = r5.zzcwc
            com.google.android.gms.internal.zzid$zza$zzb r5 = com.google.android.gms.internal.zzid.zza.zzb.AD_FAILED_TO_LOAD
            r4.zza(r5)
        L_0x0021:
            r3.zzi(r1)
            return r1
        L_0x0025:
            boolean r4 = r5.zzcny
            r0 = 0
            if (r4 == 0) goto L_0x0061
            r3.zzd(r5)
            com.google.android.gms.ads.internal.zzbs.zzez()
            com.google.android.gms.ads.internal.zzbt r4 = r3.zzamt
            com.google.android.gms.ads.internal.zzbu r4 = r4.zzate
            com.google.android.gms.internal.zzakg.zza(r4, r3)
            com.google.android.gms.ads.internal.zzbs.zzez()
            com.google.android.gms.ads.internal.zzbt r4 = r3.zzamt
            com.google.android.gms.ads.internal.zzbu r4 = r4.zzate
            com.google.android.gms.internal.zzakg.zza(r4, r3)
            boolean r4 = r5.zzcvr
            if (r4 != 0) goto L_0x007e
            com.google.android.gms.ads.internal.zzy r4 = new com.google.android.gms.ads.internal.zzy
            r4.<init>(r3)
            com.google.android.gms.internal.zzama r1 = r5.zzchj
            if (r1 == 0) goto L_0x0055
            com.google.android.gms.internal.zzama r1 = r5.zzchj
            com.google.android.gms.internal.zzamb r1 = r1.zzsq()
            goto L_0x0056
        L_0x0055:
            r1 = r0
        L_0x0056:
            if (r1 == 0) goto L_0x007e
            com.google.android.gms.ads.internal.zzz r2 = new com.google.android.gms.ads.internal.zzz
            r2.<init>(r3, r5, r4)
            r1.zza(r2)
            goto L_0x007e
        L_0x0061:
            com.google.android.gms.ads.internal.zzbt r4 = r3.zzamt
            boolean r4 = r4.zzfh()
            if (r4 == 0) goto L_0x007b
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r4 = com.google.android.gms.internal.zzmq.zzbmr
            com.google.android.gms.internal.zzmo r2 = com.google.android.gms.ads.internal.zzbs.zzep()
            java.lang.Object r4 = r2.zzd(r4)
            java.lang.Boolean r4 = (java.lang.Boolean) r4
            boolean r4 = r4.booleanValue()
            if (r4 == 0) goto L_0x007e
        L_0x007b:
            r3.zza(r5, r1)
        L_0x007e:
            com.google.android.gms.internal.zzama r4 = r5.zzchj
            if (r4 == 0) goto L_0x00a2
            com.google.android.gms.internal.zzama r4 = r5.zzchj
            com.google.android.gms.internal.zzamr r4 = r4.zzrx()
            com.google.android.gms.internal.zzama r1 = r5.zzchj
            com.google.android.gms.internal.zzamb r1 = r1.zzsq()
            if (r1 == 0) goto L_0x0093
            r1.zztk()
        L_0x0093:
            com.google.android.gms.ads.internal.zzbt r1 = r3.zzamt
            com.google.android.gms.internal.zzma r1 = r1.zzatu
            if (r1 == 0) goto L_0x00a2
            if (r4 == 0) goto L_0x00a2
            com.google.android.gms.ads.internal.zzbt r1 = r3.zzamt
            com.google.android.gms.internal.zzma r1 = r1.zzatu
            r4.zzb(r1)
        L_0x00a2:
            com.google.android.gms.ads.internal.zzbt r4 = r3.zzamt
            boolean r4 = r4.zzfg()
            if (r4 == 0) goto L_0x0116
            com.google.android.gms.internal.zzama r4 = r5.zzchj
            if (r4 == 0) goto L_0x012d
            org.json.JSONObject r4 = r5.zzcvq
            if (r4 == 0) goto L_0x00bb
            com.google.android.gms.internal.zzfb r4 = r3.zzamv
            com.google.android.gms.ads.internal.zzbt r1 = r3.zzamt
            com.google.android.gms.internal.zziw r1 = r1.zzath
            r4.zza(r1, r5)
        L_0x00bb:
            com.google.android.gms.internal.zzfy r4 = new com.google.android.gms.internal.zzfy
            com.google.android.gms.ads.internal.zzbt r1 = r3.zzamt
            android.content.Context r1 = r1.zzaif
            com.google.android.gms.internal.zzama r2 = r5.zzchj
            if (r2 != 0) goto L_0x00c6
            throw r0
        L_0x00c6:
            android.view.View r2 = (android.view.View) r2
            r4.<init>(r1, r2)
            com.google.android.gms.internal.zzael r0 = com.google.android.gms.ads.internal.zzbs.zzfa()
            com.google.android.gms.ads.internal.zzbt r1 = r3.zzamt
            android.content.Context r1 = r1.zzaif
            boolean r0 = r0.zzs(r1)
            if (r0 == 0) goto L_0x00fb
            com.google.android.gms.internal.zzis r0 = r5.zzclo
            boolean r0 = zza(r0)
            if (r0 == 0) goto L_0x00fb
            com.google.android.gms.ads.internal.zzbt r0 = r3.zzamt
            java.lang.String r0 = r0.zzatb
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00fb
            com.google.android.gms.internal.zzaek r0 = new com.google.android.gms.internal.zzaek
            com.google.android.gms.ads.internal.zzbt r1 = r3.zzamt
            android.content.Context r1 = r1.zzaif
            com.google.android.gms.ads.internal.zzbt r2 = r3.zzamt
            java.lang.String r2 = r2.zzatb
            r0.<init>(r1, r2)
            r4.zza(r0)
        L_0x00fb:
            boolean r0 = r5.zzfr()
            if (r0 == 0) goto L_0x0107
            com.google.android.gms.internal.zzama r5 = r5.zzchj
            r4.zza(r5)
            goto L_0x012d
        L_0x0107:
            com.google.android.gms.internal.zzama r0 = r5.zzchj
            com.google.android.gms.internal.zzamb r0 = r0.zzsq()
            com.google.android.gms.ads.internal.zzaa r1 = new com.google.android.gms.ads.internal.zzaa
            r1.<init>(r3, r4, r5)
            r0.zza(r1)
            goto L_0x012d
        L_0x0116:
            com.google.android.gms.ads.internal.zzbt r4 = r3.zzamt
            android.view.View r4 = r4.zzaud
            if (r4 == 0) goto L_0x012d
            org.json.JSONObject r4 = r5.zzcvq
            if (r4 == 0) goto L_0x012d
            com.google.android.gms.internal.zzfb r4 = r3.zzamv
            com.google.android.gms.ads.internal.zzbt r0 = r3.zzamt
            com.google.android.gms.internal.zziw r0 = r0.zzath
            com.google.android.gms.ads.internal.zzbt r1 = r3.zzamt
            android.view.View r1 = r1.zzaud
            r4.zza(r0, r5, r1)
        L_0x012d:
            r4 = 1
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.zzx.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean");
    }

    public final boolean zzb(zzis zzis) {
        zzx zzx = this;
        zzis zzis2 = zzis;
        if (zzis2.zzbca != zzx.zzals) {
            int i = zzis2.versionCode;
            long j = zzis2.zzbbv;
            Bundle bundle = zzis2.extras;
            int i2 = zzis2.zzbbw;
            List<String> list = zzis2.zzbbx;
            boolean z = zzis2.zzbby;
            int i3 = zzis2.zzbbz;
            zzis2 = new zzis(i, j, bundle, i2, list, z, i3, zzis2.zzbca || zzx.zzals, zzis2.zzbcb, zzis2.zzbcc, zzis2.zzbcd, zzis2.zzbce, zzis2.zzbcf, zzis2.zzbcg, zzis2.zzbch, zzis2.zzbci, zzis2.zzbcj, zzis2.zzbck);
            zzx = this;
        }
        return super.zzb(zzis2);
    }

    /* access modifiers changed from: protected */
    public final boolean zzbz() {
        boolean z;
        zzbs.zzec();
        if (!zzagr.zzd(this.zzamt.zzaif, this.zzamt.zzaif.getPackageName(), "android.permission.INTERNET")) {
            zzjk.zzhx().zza(this.zzamt.zzate, this.zzamt.zzath, "Missing internet permission in AndroidManifest.xml.", "Missing internet permission in AndroidManifest.xml. You must have the following declaration: <uses-permission android:name=\"android.permission.INTERNET\" />");
            z = false;
        } else {
            z = true;
        }
        zzbs.zzec();
        if (!zzagr.zzag(this.zzamt.zzaif)) {
            zzjk.zzhx().zza(this.zzamt.zzate, this.zzamt.zzath, "Missing AdActivity with android:configChanges in AndroidManifest.xml.", "Missing AdActivity with android:configChanges in AndroidManifest.xml. You must have the following declaration within the <application> element: <activity android:name=\"com.google.android.gms.ads.AdActivity\" android:configChanges=\"keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize\" />");
            z = false;
        }
        if (!z && this.zzamt.zzate != null) {
            this.zzamt.zzate.setVisibility(0);
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, boolean):void
     arg types: [com.google.android.gms.internal.zzaeu, int]
     candidates:
      com.google.android.gms.ads.internal.zzx.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zzi.zza(com.google.android.gms.internal.zzaev, com.google.android.gms.internal.zznd):void
      com.google.android.gms.ads.internal.zzi.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzpu, java.lang.String):void
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaaa, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzis, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzaev, com.google.android.gms.internal.zznd):void
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzww, java.lang.String):void
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzis, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.internal.zzkb.zza(com.google.android.gms.internal.zzww, java.lang.String):void
      com.google.android.gms.internal.zzto.zza(com.google.android.gms.internal.zzpu, java.lang.String):void
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, boolean):void */
    /* access modifiers changed from: package-private */
    public final void zzd(@Nullable zzaeu zzaeu) {
        if (zzaeu != null && !zzaeu.zzcvr && this.zzamt.zzate != null && zzbs.zzec().zza(this.zzamt.zzate, this.zzamt.zzaif) && this.zzamt.zzate.getGlobalVisibleRect(new Rect(), null)) {
            if (!(zzaeu == null || zzaeu.zzchj == null || zzaeu.zzchj.zzsq() == null)) {
                zzaeu.zzchj.zzsq().zza((zzamj) null);
            }
            zza(zzaeu, false);
            zzaeu.zzcvr = true;
        }
    }
}
