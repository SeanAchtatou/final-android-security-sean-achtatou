package com.google.android.gms.internal;

abstract class zzfdn extends zzfdh {
    zzfdn() {
    }

    /* access modifiers changed from: package-private */
    public abstract boolean zza(zzfdh zzfdh, int i, int i2);

    /* access modifiers changed from: protected */
    public final int zzctm() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public final boolean zzctn() {
        return true;
    }
}
