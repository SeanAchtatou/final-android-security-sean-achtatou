package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbe extends zzac<Void> {
    private /* synthetic */ String zzhkj;

    zzbe(RealTimeMultiplayerClient realTimeMultiplayerClient, String str) {
        this.zzhkj = str;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException {
        gamesClientImpl.zzq(this.zzhkj, 0);
        taskCompletionSource.setResult(null);
    }
}
