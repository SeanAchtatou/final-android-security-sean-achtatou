package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.support.annotation.BinderThread;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;

public final class zzn extends zze {
    private /* synthetic */ zzd zzfwg;
    private IBinder zzfwk;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    @BinderThread
    public zzn(zzd zzd, int i, IBinder iBinder, Bundle bundle) {
        super(zzd, i, bundle);
        this.zzfwg = zzd;
        this.zzfwk = iBinder;
    }

    /* access modifiers changed from: protected */
    public final boolean zzakf() {
        try {
            String interfaceDescriptor = this.zzfwk.getInterfaceDescriptor();
            if (!this.zzfwg.zzhg().equals(interfaceDescriptor)) {
                String zzhg = this.zzfwg.zzhg();
                StringBuilder sb = new StringBuilder(34 + String.valueOf(zzhg).length() + String.valueOf(interfaceDescriptor).length());
                sb.append("service descriptor mismatch: ");
                sb.append(zzhg);
                sb.append(" vs. ");
                sb.append(interfaceDescriptor);
                Log.e("GmsClient", sb.toString());
                return false;
            }
            IInterface zzd = this.zzfwg.zzd(this.zzfwk);
            if (zzd == null) {
                return false;
            }
            if (!this.zzfwg.zza(2, 4, zzd) && !this.zzfwg.zza(3, 4, zzd)) {
                return false;
            }
            ConnectionResult unused = this.zzfwg.zzfwb = (ConnectionResult) null;
            Bundle zzaew = this.zzfwg.zzaew();
            if (this.zzfwg.zzfvx != null) {
                this.zzfwg.zzfvx.onConnected(zzaew);
            }
            return true;
        } catch (RemoteException unused2) {
            Log.w("GmsClient", "service probably died");
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final void zzj(ConnectionResult connectionResult) {
        if (this.zzfwg.zzfvy != null) {
            this.zzfwg.zzfvy.onConnectionFailed(connectionResult);
        }
        this.zzfwg.onConnectionFailed(connectionResult);
    }
}
