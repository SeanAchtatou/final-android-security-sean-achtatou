package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import com.google.android.gms.internal.zzbjr;
import java.util.Arrays;

public final class zzn extends zzbej implements DriveEvent {
    public static final Parcelable.Creator<zzn> CREATOR = new zzo();
    private zzbjr zzgjc;

    public zzn(zzbjr zzbjr) {
        this.zzgjc = zzbjr;
    }

    public final boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return zzbg.equal(this.zzgjc, ((zzn) obj).zzgjc);
    }

    public final int getType() {
        return 8;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.zzgjc});
    }

    public final String toString() {
        return String.format("TransferProgressEvent[%s]", this.zzgjc.toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.zzbjr, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, (Parcelable) this.zzgjc, i, false);
        zzbem.zzai(parcel, zze);
    }

    public final zzbjr zzaol() {
        return this.zzgjc;
    }
}
