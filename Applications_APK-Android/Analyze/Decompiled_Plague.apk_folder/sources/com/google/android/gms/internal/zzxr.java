package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzba;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.games.GamesStatusCodes;
import com.tapjoy.TJAdUnitConstants;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzb
public final class zzxr implements Callable<zzaeu> {
    private static long zzciq = 10;
    private final Context mContext;
    private int mErrorCode;
    private final Object mLock = new Object();
    private final zznd zzamo;
    private final zzyg zzaqq;
    private final zzcs zzbta;
    private final zzaev zzchv;
    private final zzahy zzcir;
    /* access modifiers changed from: private */
    public final zzba zzcis;
    private boolean zzcit;
    private List<String> zzciu;
    private JSONObject zzciv;
    private String zzciw;

    public zzxr(Context context, zzba zzba, zzahy zzahy, zzcs zzcs, zzaev zzaev, zznd zznd) {
        this.mContext = context;
        this.zzcis = zzba;
        this.zzcir = zzahy;
        this.zzchv = zzaev;
        this.zzbta = zzcs;
        this.zzamo = zznd;
        this.zzaqq = zzba.zzdk();
        this.zzcit = false;
        this.mErrorCode = -2;
        this.zzciu = null;
        this.zzciw = null;
    }

    /* JADX INFO: finally extract failed */
    private final zzaeu zza(zzoc zzoc) {
        int i;
        synchronized (this.mLock) {
            try {
                int i2 = this.mErrorCode;
                if (zzoc == null && this.mErrorCode == -2) {
                    i2 = 0;
                }
                i = i2;
            } catch (Throwable th) {
                while (true) {
                    throw th;
                }
            }
        }
        zzoc zzoc2 = i != -2 ? null : zzoc;
        zzis zzis = this.zzchv.zzcpe.zzclo;
        List<String> list = this.zzchv.zzcwe.zzcbv;
        List<String> list2 = this.zzchv.zzcwe.zzcbw;
        List<String> list3 = this.zzciu;
        int i3 = this.zzchv.zzcwe.orientation;
        long j = this.zzchv.zzcwe.zzccb;
        String str = this.zzchv.zzcpe.zzclr;
        zziw zziw = this.zzchv.zzath;
        long j2 = this.zzchv.zzcwe.zzcnf;
        List<String> list4 = list;
        long j3 = this.zzchv.zzcvw;
        return new zzaeu(zzis, null, list4, i, list2, list3, i3, j, str, false, null, null, null, null, null, 0, zziw, j2, j3, this.zzchv.zzcvx, this.zzchv.zzcwe.zzcnl, this.zzciv, zzoc2, null, null, null, this.zzchv.zzcwe.zzcny, this.zzchv.zzcwe.zzcnz, null, this.zzchv.zzcwe.zzcby, this.zzciw, this.zzchv.zzcwc, this.zzchv.zzcwe.zzapy, this.zzchv.zzcwd);
    }

    private final zzajp<zznr> zza(JSONObject jSONObject, boolean z, boolean z2) throws JSONException {
        String string = z ? jSONObject.getString(TJAdUnitConstants.String.URL) : jSONObject.optString(TJAdUnitConstants.String.URL);
        double optDouble = jSONObject.optDouble("scale", 1.0d);
        boolean optBoolean = jSONObject.optBoolean("is_transparent", true);
        if (!TextUtils.isEmpty(string)) {
            return z2 ? zzajg.zzi(new zznr(null, Uri.parse(string), optDouble)) : this.zzcir.zza(string, new zzxv(this, z, optDouble, optBoolean, string));
        }
        zzd(0, z);
        return zzajg.zzi(null);
    }

    static zzama zzb(zzajp<zzama> zzajp) {
        try {
            return (zzama) zzajp.get((long) ((Integer) zzbs.zzep().zzd(zzmq.zzbna)).intValue(), TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            zzafj.zzc("InterruptedException occurred while waiting for video to load", e);
            Thread.currentThread().interrupt();
            return null;
        } catch (CancellationException | ExecutionException | TimeoutException e2) {
            zzafj.zzc("Exception occurred while waiting for video to load", e2);
            return null;
        }
    }

    private static Integer zzb(JSONObject jSONObject, String str) {
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject(str);
            return Integer.valueOf(Color.rgb(jSONObject2.getInt("r"), jSONObject2.getInt("g"), jSONObject2.getInt("b")));
        } catch (JSONException unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public final void zzc(zzpu zzpu, String str) {
        try {
            zzqe zzr = this.zzcis.zzr(zzpu.getCustomTemplateId());
            if (zzr != null) {
                zzr.zzb(zzpu, str);
            }
        } catch (RemoteException e) {
            StringBuilder sb = new StringBuilder(40 + String.valueOf(str).length());
            sb.append("Failed to call onCustomClick for asset ");
            sb.append(str);
            sb.append(".");
            zzafj.zzc(sb.toString(), e);
        }
    }

    private static String[] zzd(JSONObject jSONObject, String str) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        if (optJSONArray == null) {
            return null;
        }
        String[] strArr = new String[optJSONArray.length()];
        for (int i = 0; i < optJSONArray.length(); i++) {
            strArr[i] = optJSONArray.getString(i);
        }
        return strArr;
    }

    private static <V> zzajp<List<V>> zzk(List<zzajp<V>> list) {
        zzajy zzajy = new zzajy();
        int size = list.size();
        AtomicInteger atomicInteger = new AtomicInteger(0);
        for (zzajp<V> zza : list) {
            zza.zza(new zzxw(atomicInteger, size, zzajy, list), zzagl.zzcyx);
        }
        return zzajy;
    }

    /* access modifiers changed from: private */
    public static <V> List<V> zzl(List<zzajp<V>> list) throws ExecutionException, InterruptedException {
        ArrayList arrayList = new ArrayList();
        for (zzajp<V> zzajp : list) {
            Object obj = zzajp.get();
            if (obj != null) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0054 A[Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0102 A[Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0103 A[Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x014b A[Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0178  */
    /* renamed from: zzna */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzaeu call() {
        /*
            r13 = this;
            r0 = 0
            r1 = 0
            com.google.android.gms.ads.internal.zzba r2 = r13.zzcis     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            java.lang.String r11 = r2.getUuid()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            boolean r2 = r13.zznb()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            if (r2 != 0) goto L_0x004d
            com.google.android.gms.internal.zzajy r2 = new com.google.android.gms.internal.zzajy     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r2.<init>()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzxq r2 = new com.google.android.gms.internal.zzxq     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r2.<init>()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzaev r3 = r13.zzchv     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzaad r3 = r3.zzcwe     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            java.lang.String r3 = r3.body     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r2.<init>(r3)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzyg r3 = r13.zzaqq     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzajp r2 = r3.zzi(r2)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            long r3 = com.google.android.gms.internal.zzxr.zzciq     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            java.lang.Object r2 = r2.get(r3, r5)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            org.json.JSONObject r2 = (org.json.JSONObject) r2     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            java.lang.String r3 = "success"
            boolean r3 = r2.optBoolean(r3, r1)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            if (r3 == 0) goto L_0x004d
            java.lang.String r3 = "json"
            org.json.JSONObject r2 = r2.getJSONObject(r3)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            java.lang.String r3 = "ads"
            org.json.JSONArray r2 = r2.optJSONArray(r3)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            org.json.JSONObject r2 = r2.getJSONObject(r1)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r8 = r2
            goto L_0x004e
        L_0x004d:
            r8 = r0
        L_0x004e:
            boolean r2 = r13.zznb()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            if (r2 != 0) goto L_0x00f7
            if (r8 != 0) goto L_0x0058
            goto L_0x00f7
        L_0x0058:
            java.lang.String r2 = "template_id"
            java.lang.String r2 = r8.getString(r2)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzaev r3 = r13.zzchv     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzzz r3 = r3.zzcpe     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzom r3 = r3.zzatt     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            if (r3 == 0) goto L_0x006f
            com.google.android.gms.internal.zzaev r3 = r13.zzchv     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzzz r3 = r3.zzcpe     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzom r3 = r3.zzatt     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            boolean r3 = r3.zzbtj     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            goto L_0x0070
        L_0x006f:
            r3 = r1
        L_0x0070:
            com.google.android.gms.internal.zzaev r4 = r13.zzchv     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzzz r4 = r4.zzcpe     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzom r4 = r4.zzatt     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            if (r4 == 0) goto L_0x0081
            com.google.android.gms.internal.zzaev r4 = r13.zzchv     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzzz r4 = r4.zzcpe     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzom r4 = r4.zzatt     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            boolean r4 = r4.zzbtl     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            goto L_0x0082
        L_0x0081:
            r4 = r1
        L_0x0082:
            java.lang.String r5 = "2"
            boolean r5 = r5.equals(r2)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            if (r5 == 0) goto L_0x0094
            com.google.android.gms.internal.zzyh r2 = new com.google.android.gms.internal.zzyh     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzaev r5 = r13.zzchv     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            boolean r5 = r5.zzcwd     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r2.<init>(r3, r4, r5)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            goto L_0x00f8
        L_0x0094:
            java.lang.String r5 = "1"
            boolean r5 = r5.equals(r2)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            if (r5 == 0) goto L_0x00a6
            com.google.android.gms.internal.zzyi r2 = new com.google.android.gms.internal.zzyi     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzaev r5 = r13.zzchv     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            boolean r5 = r5.zzcwd     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r2.<init>(r3, r4, r5)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            goto L_0x00f8
        L_0x00a6:
            java.lang.String r4 = "3"
            boolean r2 = r4.equals(r2)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            if (r2 == 0) goto L_0x00f4
            java.lang.String r2 = "custom_template_id"
            java.lang.String r2 = r8.getString(r2)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzajy r4 = new com.google.android.gms.internal.zzajy     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r4.<init>()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            android.os.Handler r5 = com.google.android.gms.internal.zzagr.zzczc     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzxs r6 = new com.google.android.gms.internal.zzxs     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r6.<init>(r13, r4, r2)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r5.post(r6)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            long r5 = com.google.android.gms.internal.zzxr.zzciq     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            java.lang.Object r2 = r4.get(r5, r2)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            if (r2 == 0) goto L_0x00d3
            com.google.android.gms.internal.zzyj r2 = new com.google.android.gms.internal.zzyj     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r2.<init>(r3)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            goto L_0x00f8
        L_0x00d3:
            java.lang.String r2 = "No handler for custom template: "
            java.lang.String r3 = "custom_template_id"
            java.lang.String r3 = r8.getString(r3)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            int r4 = r3.length()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            if (r4 == 0) goto L_0x00ea
            java.lang.String r2 = r2.concat(r3)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            goto L_0x00f0
        L_0x00ea:
            java.lang.String r3 = new java.lang.String     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r3.<init>(r2)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r2 = r3
        L_0x00f0:
            com.google.android.gms.internal.zzafj.e(r2)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            goto L_0x00f7
        L_0x00f4:
            r13.zzz(r1)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
        L_0x00f7:
            r2 = r0
        L_0x00f8:
            boolean r3 = r13.zznb()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            if (r3 != 0) goto L_0x0146
            if (r2 == 0) goto L_0x0146
            if (r8 != 0) goto L_0x0103
            goto L_0x0146
        L_0x0103:
            java.lang.String r3 = "tracking_urls_and_actions"
            org.json.JSONObject r3 = r8.getJSONObject(r3)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            java.lang.String r4 = "impression_tracking_urls"
            java.lang.String[] r4 = zzd(r3, r4)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            if (r4 != 0) goto L_0x0113
            r4 = r0
            goto L_0x0117
        L_0x0113:
            java.util.List r4 = java.util.Arrays.asList(r4)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
        L_0x0117:
            r13.zzciu = r4     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            java.lang.String r4 = "active_view"
            org.json.JSONObject r3 = r3.optJSONObject(r4)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r13.zzciv = r3     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            java.lang.String r3 = "debug_signals"
            java.lang.String r3 = r8.optString(r3)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r13.zzciw = r3     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzoc r2 = r2.zza(r13, r8)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzoe r12 = new com.google.android.gms.internal.zzoe     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            android.content.Context r4 = r13.mContext     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.ads.internal.zzba r5 = r13.zzcis     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzyg r6 = r13.zzaqq     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzcs r7 = r13.zzbta     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzaev r3 = r13.zzchv     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzzz r3 = r3.zzcpe     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzaiy r10 = r3.zzatd     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r3 = r12
            r9 = r2
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r2.zzb(r12)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            goto L_0x0147
        L_0x0146:
            r2 = r0
        L_0x0147:
            boolean r3 = r2 instanceof com.google.android.gms.internal.zznw     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            if (r3 == 0) goto L_0x0161
            r3 = r2
            com.google.android.gms.internal.zznw r3 = (com.google.android.gms.internal.zznw) r3     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzxq r4 = new com.google.android.gms.internal.zzxq     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r4.<init>()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzxt r5 = new com.google.android.gms.internal.zzxt     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r5.<init>(r13, r3)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            r4.zzcip = r5     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            com.google.android.gms.internal.zzyg r3 = r13.zzaqq     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            java.lang.String r4 = "/nativeAdCustomClick"
            r3.zza(r4, r5)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
        L_0x0161:
            com.google.android.gms.internal.zzaeu r2 = r13.zza(r2)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0174, JSONException -> 0x016e, TimeoutException -> 0x016a, Exception -> 0x0166 }
            return r2
        L_0x0166:
            r2 = move-exception
            java.lang.String r3 = "Error occured while doing native ads initialization."
            goto L_0x0171
        L_0x016a:
            r2 = move-exception
            java.lang.String r3 = "Timeout when loading native ad."
            goto L_0x0171
        L_0x016e:
            r2 = move-exception
            java.lang.String r3 = "Malformed native JSON response."
        L_0x0171:
            com.google.android.gms.internal.zzafj.zzc(r3, r2)
        L_0x0174:
            boolean r2 = r13.zzcit
            if (r2 != 0) goto L_0x017b
            r13.zzz(r1)
        L_0x017b:
            com.google.android.gms.internal.zzaeu r0 = r13.zza(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzxr.call():com.google.android.gms.internal.zzaeu");
    }

    private final boolean zznb() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzcit;
        }
        return z;
    }

    private final void zzz(int i) {
        synchronized (this.mLock) {
            this.zzcit = true;
            this.mErrorCode = i;
        }
    }

    public final zzajp<zznr> zza(JSONObject jSONObject, String str, boolean z, boolean z2) throws JSONException {
        JSONObject jSONObject2 = z ? jSONObject.getJSONObject(str) : jSONObject.optJSONObject(str);
        if (jSONObject2 == null) {
            jSONObject2 = new JSONObject();
        }
        return zza(jSONObject2, z, z2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzxr.zza(org.json.JSONObject, boolean, boolean):com.google.android.gms.internal.zzajp<com.google.android.gms.internal.zznr>
     arg types: [org.json.JSONObject, int, boolean]
     candidates:
      com.google.android.gms.internal.zzxr.zza(com.google.android.gms.internal.zzxr, com.google.android.gms.internal.zzpu, java.lang.String):void
      com.google.android.gms.internal.zzxr.zza(org.json.JSONObject, java.lang.String, boolean):java.util.concurrent.Future<com.google.android.gms.internal.zznr>
      com.google.android.gms.internal.zzxr.zza(org.json.JSONObject, boolean, boolean):com.google.android.gms.internal.zzajp<com.google.android.gms.internal.zznr> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzxr.zzd(int, boolean):void
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.zzxr.zzd(org.json.JSONObject, java.lang.String):java.lang.String[]
      com.google.android.gms.internal.zzxr.zzd(int, boolean):void */
    public final List<zzajp<zznr>> zza(JSONObject jSONObject, String str, boolean z, boolean z2, boolean z3) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        ArrayList arrayList = new ArrayList();
        if (optJSONArray == null || optJSONArray.length() == 0) {
            zzd(0, false);
            return arrayList;
        }
        int length = z3 ? optJSONArray.length() : 1;
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
            if (jSONObject2 == null) {
                jSONObject2 = new JSONObject();
            }
            arrayList.add(zza(jSONObject2, false, z2));
        }
        return arrayList;
    }

    public final Future<zznr> zza(JSONObject jSONObject, String str, boolean z) throws JSONException {
        JSONObject jSONObject2 = jSONObject.getJSONObject(str);
        boolean optBoolean = jSONObject2.optBoolean("require", true);
        if (jSONObject2 == null) {
            jSONObject2 = new JSONObject();
        }
        return zza(jSONObject2, optBoolean, z);
    }

    public final zzajp<zzama> zzc(JSONObject jSONObject, String str) throws JSONException {
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        if (optJSONObject == null) {
            return zzajg.zzi(null);
        }
        if (TextUtils.isEmpty(optJSONObject.optString("vast_xml"))) {
            zzafj.zzco("Required field 'vast_xml' is missing");
            return zzajg.zzi(null);
        }
        zzxy zzxy = new zzxy(this.mContext, this.zzbta, this.zzchv, this.zzamo, this.zzcis);
        zzajy zzajy = new zzajy();
        zzbs.zzec();
        zzagr.runOnUiThread(new zzxz(zzxy, optJSONObject, zzajy));
        return zzajy;
    }

    public final void zzd(int i, boolean z) {
        if (z) {
            zzz(i);
        }
    }

    public final zzajp<zznp> zzh(JSONObject jSONObject) throws JSONException {
        JSONObject optJSONObject = jSONObject.optJSONObject("attribution");
        if (optJSONObject == null) {
            return zzajg.zzi(null);
        }
        String optString = optJSONObject.optString("text");
        int optInt = optJSONObject.optInt("text_size", -1);
        Integer zzb = zzb(optJSONObject, "text_color");
        Integer zzb2 = zzb(optJSONObject, "bg_color");
        int optInt2 = optJSONObject.optInt("animation_ms", 1000);
        int optInt3 = optJSONObject.optInt("presentation_ms", GamesStatusCodes.STATUS_SNAPSHOT_NOT_FOUND);
        int i = (this.zzchv.zzcpe.zzatt == null || this.zzchv.zzcpe.zzatt.versionCode < 2) ? 1 : this.zzchv.zzcpe.zzatt.zzbtm;
        boolean optBoolean = optJSONObject.optBoolean("allow_pub_rendering");
        List arrayList = new ArrayList();
        if (optJSONObject.optJSONArray("images") != null) {
            arrayList = zza(optJSONObject, "images", false, false, true);
        } else {
            arrayList.add(zza(optJSONObject, "image", false, false));
        }
        return zzajg.zza(zzk(arrayList), new zzxu(this, optString, zzb2, zzb, optInt, optInt3, optInt2, i, optBoolean), zzagl.zzcyx);
    }
}
