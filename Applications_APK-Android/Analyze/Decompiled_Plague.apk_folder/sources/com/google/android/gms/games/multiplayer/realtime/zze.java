package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import com.google.android.gms.internal.zzbek;
import java.util.ArrayList;

public class zze implements Parcelable.Creator<RoomEntity> {
    public /* synthetic */ Object[] newArray(int i) {
        return new RoomEntity[i];
    }

    /* renamed from: zzm */
    public RoomEntity createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int zzd = zzbek.zzd(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        Bundle bundle = null;
        ArrayList arrayList = null;
        long j = 0;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    str = zzbek.zzq(parcel2, readInt);
                    break;
                case 2:
                    str2 = zzbek.zzq(parcel2, readInt);
                    break;
                case 3:
                    j = zzbek.zzi(parcel2, readInt);
                    break;
                case 4:
                    i = zzbek.zzg(parcel2, readInt);
                    break;
                case 5:
                    str3 = zzbek.zzq(parcel2, readInt);
                    break;
                case 6:
                    i2 = zzbek.zzg(parcel2, readInt);
                    break;
                case 7:
                    bundle = zzbek.zzs(parcel2, readInt);
                    break;
                case 8:
                    arrayList = zzbek.zzc(parcel2, readInt, ParticipantEntity.CREATOR);
                    break;
                case 9:
                    i3 = zzbek.zzg(parcel2, readInt);
                    break;
                default:
                    zzbek.zzb(parcel2, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel2, zzd);
        return new RoomEntity(str, str2, j, i, str3, i2, bundle, arrayList, i3);
    }
}
