package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbkn extends zzbej {
    public static final Parcelable.Creator<zzbkn> CREATOR = new zzbko();

    public final void writeToParcel(Parcel parcel, int i) {
        zzbem.zzai(parcel, zzbem.zze(parcel));
    }
}
