package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.support.annotation.BinderThread;
import android.support.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;

public final class zzo extends zze {
    private /* synthetic */ zzd zzfwg;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    @BinderThread
    public zzo(zzd zzd, @Nullable int i, Bundle bundle) {
        super(zzd, i, null);
        this.zzfwg = zzd;
    }

    /* access modifiers changed from: protected */
    public final boolean zzakf() {
        this.zzfwg.zzfvs.zzf(ConnectionResult.zzfhy);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void zzj(ConnectionResult connectionResult) {
        this.zzfwg.zzfvs.zzf(connectionResult);
        this.zzfwg.onConnectionFailed(connectionResult);
    }
}
