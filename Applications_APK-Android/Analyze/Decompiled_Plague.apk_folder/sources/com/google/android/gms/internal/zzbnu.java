package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzdf;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbnu extends zzdf<zzbll, MetadataBuffer> {
    private /* synthetic */ DriveResource zzgmc;

    zzbnu(zzbmu zzbmu, DriveResource driveResource) {
        this.zzgmc = driveResource;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbpo(this.zzgmc.getDriveId()), new zzbrx(taskCompletionSource));
    }
}
