package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.DriveFile;

final class zzbkt extends zzbky {
    private /* synthetic */ int zzgki = DriveFile.MODE_WRITE_ONLY;

    zzbkt(zzbkp zzbkp, GoogleApiClient googleApiClient, int i) {
        super(googleApiClient);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbkb(this.zzgki), new zzbkw(this));
    }
}
