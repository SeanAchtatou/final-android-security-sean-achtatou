package com.google.android.gms.internal;

import android.os.Parcelable;

public final class zzbjs implements Parcelable.Creator<zzbjr> {
    /* JADX WARN: Type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r14) {
        /*
            r13 = this;
            int r0 = com.google.android.gms.internal.zzbek.zzd(r14)
            r1 = 0
            r3 = 0
            r4 = 0
            r9 = r1
            r11 = r9
            r6 = r3
            r8 = r6
            r7 = r4
        L_0x000d:
            int r1 = r14.dataPosition()
            if (r1 >= r0) goto L_0x0040
            int r1 = r14.readInt()
            r2 = 65535(0xffff, float:9.1834E-41)
            r2 = r2 & r1
            switch(r2) {
                case 2: goto L_0x003b;
                case 3: goto L_0x0031;
                case 4: goto L_0x002c;
                case 5: goto L_0x0027;
                case 6: goto L_0x0022;
                default: goto L_0x001e;
            }
        L_0x001e:
            com.google.android.gms.internal.zzbek.zzb(r14, r1)
            goto L_0x000d
        L_0x0022:
            long r11 = com.google.android.gms.internal.zzbek.zzi(r14, r1)
            goto L_0x000d
        L_0x0027:
            long r9 = com.google.android.gms.internal.zzbek.zzi(r14, r1)
            goto L_0x000d
        L_0x002c:
            int r8 = com.google.android.gms.internal.zzbek.zzg(r14, r1)
            goto L_0x000d
        L_0x0031:
            android.os.Parcelable$Creator<com.google.android.gms.drive.DriveId> r2 = com.google.android.gms.drive.DriveId.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r14, r1, r2)
            r7 = r1
            com.google.android.gms.drive.DriveId r7 = (com.google.android.gms.drive.DriveId) r7
            goto L_0x000d
        L_0x003b:
            int r6 = com.google.android.gms.internal.zzbek.zzg(r14, r1)
            goto L_0x000d
        L_0x0040:
            com.google.android.gms.internal.zzbek.zzaf(r14, r0)
            com.google.android.gms.internal.zzbjr r14 = new com.google.android.gms.internal.zzbjr
            r5 = r14
            r5.<init>(r6, r7, r8, r9, r11)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzbjs.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbjr[i];
    }
}
