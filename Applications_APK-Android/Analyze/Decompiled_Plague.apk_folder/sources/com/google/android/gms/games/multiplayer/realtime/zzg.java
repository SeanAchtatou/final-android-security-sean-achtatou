package com.google.android.gms.games.multiplayer.realtime;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

public final class zzg implements zzh {
    private final RoomUpdateCallback zzhvh;
    private final RoomStatusUpdateCallback zzhvi;
    private final OnRealTimeMessageReceivedListener zzhvs;

    public zzg(@NonNull RoomUpdateCallback roomUpdateCallback, @Nullable RoomStatusUpdateCallback roomStatusUpdateCallback, @Nullable OnRealTimeMessageReceivedListener onRealTimeMessageReceivedListener) {
        this.zzhvh = roomUpdateCallback;
        this.zzhvi = roomStatusUpdateCallback;
        this.zzhvs = onRealTimeMessageReceivedListener;
    }

    public final void onConnectedToRoom(@Nullable Room room) {
        if (this.zzhvi != null) {
            this.zzhvi.onConnectedToRoom(room);
        }
    }

    public final void onDisconnectedFromRoom(@Nullable Room room) {
        if (this.zzhvi != null) {
            this.zzhvi.onDisconnectedFromRoom(room);
        }
    }

    public final void onJoinedRoom(int i, @Nullable Room room) {
        if (this.zzhvh != null) {
            this.zzhvh.onJoinedRoom(i, room);
        }
    }

    public final void onLeftRoom(int i, @NonNull String str) {
        if (this.zzhvh != null) {
            this.zzhvh.onLeftRoom(i, str);
        }
    }

    public final void onP2PConnected(@NonNull String str) {
        if (this.zzhvi != null) {
            this.zzhvi.onP2PConnected(str);
        }
    }

    public final void onP2PDisconnected(@NonNull String str) {
        if (this.zzhvi != null) {
            this.zzhvi.onP2PDisconnected(str);
        }
    }

    public final void onPeerDeclined(@Nullable Room room, @NonNull List<String> list) {
        if (this.zzhvi != null) {
            this.zzhvi.onPeerDeclined(room, list);
        }
    }

    public final void onPeerInvitedToRoom(@Nullable Room room, @NonNull List<String> list) {
        if (this.zzhvi != null) {
            this.zzhvi.onPeerInvitedToRoom(room, list);
        }
    }

    public final void onPeerJoined(@Nullable Room room, @NonNull List<String> list) {
        if (this.zzhvi != null) {
            this.zzhvi.onPeerJoined(room, list);
        }
    }

    public final void onPeerLeft(@Nullable Room room, @NonNull List<String> list) {
        if (this.zzhvi != null) {
            this.zzhvi.onPeerLeft(room, list);
        }
    }

    public final void onPeersConnected(@Nullable Room room, @NonNull List<String> list) {
        if (this.zzhvi != null) {
            this.zzhvi.onPeersConnected(room, list);
        }
    }

    public final void onPeersDisconnected(@Nullable Room room, @NonNull List<String> list) {
        if (this.zzhvi != null) {
            this.zzhvi.onPeersDisconnected(room, list);
        }
    }

    public final void onRealTimeMessageReceived(@NonNull RealTimeMessage realTimeMessage) {
        if (this.zzhvs != null) {
            this.zzhvs.onRealTimeMessageReceived(realTimeMessage);
        }
    }

    public final void onRoomAutoMatching(@Nullable Room room) {
        if (this.zzhvi != null) {
            this.zzhvi.onRoomAutoMatching(room);
        }
    }

    public final void onRoomConnected(int i, @Nullable Room room) {
        if (this.zzhvh != null) {
            this.zzhvh.onRoomConnected(i, room);
        }
    }

    public final void onRoomConnecting(@Nullable Room room) {
        if (this.zzhvi != null) {
            this.zzhvi.onRoomConnecting(room);
        }
    }

    public final void onRoomCreated(int i, @Nullable Room room) {
        if (this.zzhvh != null) {
            this.zzhvh.onRoomCreated(i, room);
        }
    }
}
