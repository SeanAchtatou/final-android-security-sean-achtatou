package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.drive.events.DriveEvent;
import com.google.android.gms.drive.events.zzb;
import com.google.android.gms.drive.events.zzl;
import com.google.android.gms.drive.events.zzn;
import com.google.android.gms.drive.events.zzr;

public final class zzbqa extends zzbej {
    public static final Parcelable.Creator<zzbqa> CREATOR = new zzbqb();
    private int zzgas;
    private ChangeEvent zzgoc;
    private CompletionEvent zzgod;
    private zzl zzgoe;
    private zzb zzgof;
    private zzr zzgog;
    private zzn zzgoh;

    zzbqa(int i, ChangeEvent changeEvent, CompletionEvent completionEvent, zzl zzl, zzb zzb, zzr zzr, zzn zzn) {
        this.zzgas = i;
        this.zzgoc = changeEvent;
        this.zzgod = completionEvent;
        this.zzgoe = zzl;
        this.zzgof = zzb;
        this.zzgog = zzr;
        this.zzgoh = zzn;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.events.ChangeEvent, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.events.CompletionEvent, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.events.zzl, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.events.zzb, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.events.zzr, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.events.zzn, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 2, this.zzgas);
        zzbem.zza(parcel, 3, (Parcelable) this.zzgoc, i, false);
        zzbem.zza(parcel, 5, (Parcelable) this.zzgod, i, false);
        zzbem.zza(parcel, 6, (Parcelable) this.zzgoe, i, false);
        zzbem.zza(parcel, 7, (Parcelable) this.zzgof, i, false);
        zzbem.zza(parcel, 9, (Parcelable) this.zzgog, i, false);
        zzbem.zza(parcel, 10, (Parcelable) this.zzgoh, i, false);
        zzbem.zzai(parcel, zze);
    }

    public final DriveEvent zzaou() {
        switch (this.zzgas) {
            case 1:
                return this.zzgoc;
            case 2:
                return this.zzgod;
            case 3:
                return this.zzgoe;
            case 4:
                return this.zzgof;
            case 5:
            case 6:
            default:
                int i = this.zzgas;
                StringBuilder sb = new StringBuilder(33);
                sb.append("Unexpected event type ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
            case 7:
                return this.zzgog;
            case 8:
                return this.zzgoh;
        }
    }
}
