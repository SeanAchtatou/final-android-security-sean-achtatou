package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrk;
import java.security.GeneralSecurityException;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class zzdpc implements zzdos<zzdoo> {
    private static final Logger logger = Logger.getLogger(zzdpc.class.getName());

    static {
        try {
            zzdpa.zzlpq.zza("type.googleapis.com/google.crypto.tink.AesCtrKey", new zzdpd());
            zzdpp.zzbli();
        } catch (GeneralSecurityException e) {
            Logger logger2 = logger;
            Level level = Level.SEVERE;
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(30 + String.valueOf(valueOf).length());
            sb.append("cannot register key managers: ");
            sb.append(valueOf);
            logger2.logp(level, "com.google.crypto.tink.aead.AesCtrHmacAeadKeyManager", "<clinit>", sb.toString());
        }
    }

    zzdpc() {
    }

    /* access modifiers changed from: private */
    /* renamed from: zzd */
    public final zzdoo zza(zzfdh zzfdh) throws GeneralSecurityException {
        try {
            zzdpq zzh = zzdpq.zzh(zzfdh);
            if (!(zzh instanceof zzdpq)) {
                throw new GeneralSecurityException("expected AesCtrHmacAeadKey proto");
            }
            zzdpq zzdpq = zzh;
            zzdte.zzt(zzdpq.getVersion(), 0);
            return new zzdsq((zzdtb) zzdpa.zzlpq.zzb("type.googleapis.com/google.crypto.tink.AesCtrKey", zzdpq.zzbll()), (zzdou) zzdpa.zzlpq.zzb("type.googleapis.com/google.crypto.tink.HmacKey", zzdpq.zzblm()), zzdpq.zzblm().zzbni().zzbnp());
        } catch (zzfew e) {
            throw new GeneralSecurityException("expected serialized AesCtrHmacAeadKey proto", e);
        }
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey";
    }

    public final /* synthetic */ Object zza(zzffi zzffi) throws GeneralSecurityException {
        if (!(zzffi instanceof zzdpq)) {
            throw new GeneralSecurityException("expected AesCtrHmacAeadKey proto");
        }
        zzdpq zzdpq = (zzdpq) zzffi;
        zzdte.zzt(zzdpq.getVersion(), 0);
        return new zzdsq((zzdtb) zzdpa.zzlpq.zzb("type.googleapis.com/google.crypto.tink.AesCtrKey", zzdpq.zzbll()), (zzdou) zzdpa.zzlpq.zzb("type.googleapis.com/google.crypto.tink.HmacKey", zzdpq.zzblm()), zzdpq.zzblm().zzbni().zzbnp());
    }

    public final zzffi zzb(zzfdh zzfdh) throws GeneralSecurityException {
        try {
            return zzb(zzdps.zzi(zzfdh));
        } catch (zzfew e) {
            throw new GeneralSecurityException("expected serialized AesCtrHmacAeadKeyFormat proto", e);
        }
    }

    public final zzffi zzb(zzffi zzffi) throws GeneralSecurityException {
        if (!(zzffi instanceof zzdps)) {
            throw new GeneralSecurityException("expected AesCtrHmacAeadKeyFormat proto");
        }
        zzdps zzdps = (zzdps) zzffi;
        return zzdpq.zzbln().zzb((zzdpu) zzdpa.zzlpq.zza("type.googleapis.com/google.crypto.tink.AesCtrKey", zzdps.zzblp())).zzb((zzdre) zzdpa.zzlpq.zza("type.googleapis.com/google.crypto.tink.HmacKey", zzdps.zzblq())).zzfi(0).zzcvk();
    }

    public final zzdrk zzc(zzfdh zzfdh) throws GeneralSecurityException {
        return (zzdrk) zzdrk.zzbnv().zzoa("type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey").zzaa(((zzdpq) zzb(zzfdh)).toByteString()).zzb(zzdrk.zzb.SYMMETRIC).zzcvk();
    }
}
