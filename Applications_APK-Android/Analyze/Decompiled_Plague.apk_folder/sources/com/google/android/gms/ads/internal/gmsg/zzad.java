package com.google.android.gms.ads.internal.gmsg;

import android.text.TextUtils;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzzb;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@zzzb
public final class zzad implements zzt<Object> {
    private final Object mLock = new Object();
    private final Map<String, zzae> zzbws = new HashMap();

    public final void zza(Object obj, Map<String, String> map) {
        String str = map.get("id");
        String str2 = map.get("fail");
        String str3 = map.get("fail_reason");
        String str4 = map.get("result");
        synchronized (this.mLock) {
            zzae remove = this.zzbws.remove(str);
            if (remove == null) {
                String valueOf = String.valueOf(str);
                zzafj.zzco(valueOf.length() != 0 ? "Received result for unexpected method invocation: ".concat(valueOf) : new String("Received result for unexpected method invocation: "));
            } else if (!TextUtils.isEmpty(str2)) {
                remove.zzat(str3);
            } else if (str4 == null) {
                remove.zze(null);
            } else {
                try {
                    remove.zze(new JSONObject(str4));
                } catch (JSONException e) {
                    remove.zzat(e.getMessage());
                }
            }
        }
    }

    public final void zza(String str, zzae zzae) {
        synchronized (this.mLock) {
            this.zzbws.put(str, zzae);
        }
    }
}
