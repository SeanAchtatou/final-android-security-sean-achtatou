package com.google.android.gms.internal;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzr;

final class zzcvz extends Api.zza<zzcwl, zzcwc> {
    zzcvz() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzcwl.<init>(android.content.Context, android.os.Looper, boolean, com.google.android.gms.common.internal.zzr, com.google.android.gms.internal.zzcwc, com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks, com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener):void
     arg types: [android.content.Context, android.os.Looper, int, com.google.android.gms.common.internal.zzr, com.google.android.gms.internal.zzcwc, com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks, com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener]
     candidates:
      com.google.android.gms.internal.zzcwl.<init>(android.content.Context, android.os.Looper, boolean, com.google.android.gms.common.internal.zzr, android.os.Bundle, com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks, com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener):void
      com.google.android.gms.internal.zzcwl.<init>(android.content.Context, android.os.Looper, boolean, com.google.android.gms.common.internal.zzr, com.google.android.gms.internal.zzcwc, com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks, com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener):void */
    public final /* synthetic */ Api.zze zza(Context context, Looper looper, zzr zzr, Object obj, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        zzcwc zzcwc = (zzcwc) obj;
        if (zzcwc == null) {
            zzcwc = zzcwc.zzjyz;
        }
        return new zzcwl(context, looper, true, zzr, zzcwc, connectionCallbacks, onConnectionFailedListener);
    }
}
