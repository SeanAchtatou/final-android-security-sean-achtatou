package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.common.api.internal.zzct;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbnd extends zzct<zzbll, zzbnz> {
    private /* synthetic */ DriveResource zzgmc;
    private /* synthetic */ zzbnz zzgmd;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbnd(zzbmu zzbmu, zzcl zzcl, DriveResource driveResource, zzbnz zzbnz) {
        super(zzcl);
        this.zzgmc = driveResource;
        this.zzgmd = zzbnz;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zzb(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbjt(1, this.zzgmc.getDriveId()), this.zzgmd.zzgmw, (String) null, new zzbsa(taskCompletionSource));
    }
}
