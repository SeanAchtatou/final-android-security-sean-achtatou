package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class zzbqk implements Parcelable.Creator<zzbqj> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        MetadataBundle metadataBundle = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                zzbek.zzb(parcel, readInt);
            } else {
                metadataBundle = (MetadataBundle) zzbek.zza(parcel, readInt, MetadataBundle.CREATOR);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbqj(metadataBundle);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbqj[i];
    }
}
