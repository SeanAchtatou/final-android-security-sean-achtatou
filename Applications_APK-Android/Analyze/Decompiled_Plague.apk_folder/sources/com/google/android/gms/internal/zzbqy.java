package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.query.internal.FilterHolder;

public final class zzbqy implements Parcelable.Creator<zzbqx> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        String str = null;
        String[] strArr = null;
        DriveId driveId = null;
        FilterHolder filterHolder = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = zzbek.zzq(parcel, readInt);
                    break;
                case 3:
                    strArr = zzbek.zzaa(parcel, readInt);
                    break;
                case 4:
                    driveId = (DriveId) zzbek.zza(parcel, readInt, DriveId.CREATOR);
                    break;
                case 5:
                    filterHolder = (FilterHolder) zzbek.zza(parcel, readInt, FilterHolder.CREATOR);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbqx(str, strArr, driveId, filterHolder);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbqx[i];
    }
}
