package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.TransformedResult;
import com.google.android.gms.common.internal.zzaq;
import com.google.android.gms.common.internal.zzbq;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public abstract class zzs<R extends Result> extends PendingResult<R> {
    static final ThreadLocal<Boolean> zzfly = new zzt();
    private Status mStatus;
    private boolean zzaj;
    private final CountDownLatch zzaoi;
    /* access modifiers changed from: private */
    public R zzfkk;
    private final Object zzflz;
    private zzu<R> zzfma;
    private WeakReference<GoogleApiClient> zzfmb;
    private final ArrayList<PendingResult.zza> zzfmc;
    private ResultCallback<? super R> zzfmd;
    private final AtomicReference<zzdo> zzfme;
    private zzv zzfmf;
    private volatile boolean zzfmg;
    private boolean zzfmh;
    private zzaq zzfmi;
    private volatile zzdi<R> zzfmj;
    private boolean zzfmk;

    @Deprecated
    zzs() {
        this.zzflz = new Object();
        this.zzaoi = new CountDownLatch(1);
        this.zzfmc = new ArrayList<>();
        this.zzfme = new AtomicReference<>();
        this.zzfmk = false;
        this.zzfma = new zzu<>(Looper.getMainLooper());
        this.zzfmb = new WeakReference<>(null);
    }

    @Deprecated
    protected zzs(Looper looper) {
        this.zzflz = new Object();
        this.zzaoi = new CountDownLatch(1);
        this.zzfmc = new ArrayList<>();
        this.zzfme = new AtomicReference<>();
        this.zzfmk = false;
        this.zzfma = new zzu<>(looper);
        this.zzfmb = new WeakReference<>(null);
    }

    protected zzs(GoogleApiClient googleApiClient) {
        this.zzflz = new Object();
        this.zzaoi = new CountDownLatch(1);
        this.zzfmc = new ArrayList<>();
        this.zzfme = new AtomicReference<>();
        this.zzfmk = false;
        this.zzfma = new zzu<>(googleApiClient != null ? googleApiClient.getLooper() : Looper.getMainLooper());
        this.zzfmb = new WeakReference<>(googleApiClient);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    private final R get() {
        R r;
        synchronized (this.zzflz) {
            zzbq.zza(!this.zzfmg, (Object) "Result has already been consumed.");
            zzbq.zza(isReady(), (Object) "Result is not ready.");
            r = this.zzfkk;
            this.zzfkk = null;
            this.zzfmd = null;
            this.zzfmg = true;
        }
        zzdo andSet = this.zzfme.getAndSet(null);
        if (andSet != null) {
            andSet.zzc(this);
        }
        return r;
    }

    private final void zzc(R r) {
        this.zzfkk = r;
        this.zzfmi = null;
        this.zzaoi.countDown();
        this.mStatus = this.zzfkk.getStatus();
        if (this.zzaj) {
            this.zzfmd = null;
        } else if (this.zzfmd != null) {
            this.zzfma.removeMessages(2);
            this.zzfma.zza(this.zzfmd, get());
        } else if (this.zzfkk instanceof Releasable) {
            this.zzfmf = new zzv(this, null);
        }
        ArrayList arrayList = this.zzfmc;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList.get(i);
            i++;
            ((PendingResult.zza) obj).zzr(this.mStatus);
        }
        this.zzfmc.clear();
    }

    public static void zzd(Result result) {
        if (result instanceof Releasable) {
            try {
                ((Releasable) result).release();
            } catch (RuntimeException e) {
                String valueOf = String.valueOf(result);
                StringBuilder sb = new StringBuilder(18 + String.valueOf(valueOf).length());
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("BasePendingResult", sb.toString(), e);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final R await() {
        boolean z = false;
        zzbq.zza(Looper.myLooper() != Looper.getMainLooper(), (Object) "await must not be called on the UI thread");
        zzbq.zza(!this.zzfmg, (Object) "Result has already been consumed");
        if (this.zzfmj == null) {
            z = true;
        }
        zzbq.zza(z, (Object) "Cannot await if then() has been called.");
        try {
            this.zzaoi.await();
        } catch (InterruptedException unused) {
            zzv(Status.zzfkp);
        }
        zzbq.zza(isReady(), (Object) "Result is not ready.");
        return get();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final R await(long j, TimeUnit timeUnit) {
        boolean z = false;
        zzbq.zza(j <= 0 || Looper.myLooper() != Looper.getMainLooper(), (Object) "await must not be called on the UI thread when time is greater than zero.");
        zzbq.zza(!this.zzfmg, (Object) "Result has already been consumed.");
        if (this.zzfmj == null) {
            z = true;
        }
        zzbq.zza(z, (Object) "Cannot await if then() has been called.");
        try {
            if (!this.zzaoi.await(j, timeUnit)) {
                zzv(Status.zzfkr);
            }
        } catch (InterruptedException unused) {
            zzv(Status.zzfkp);
        }
        zzbq.zza(isReady(), (Object) "Result is not ready.");
        return get();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:8|(2:10|11)|12|13|14|15) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0029, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0015 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void cancel() {
        /*
            r2 = this;
            java.lang.Object r0 = r2.zzflz
            monitor-enter(r0)
            boolean r1 = r2.zzaj     // Catch:{ all -> 0x002a }
            if (r1 != 0) goto L_0x0028
            boolean r1 = r2.zzfmg     // Catch:{ all -> 0x002a }
            if (r1 == 0) goto L_0x000c
            goto L_0x0028
        L_0x000c:
            com.google.android.gms.common.internal.zzaq r1 = r2.zzfmi     // Catch:{ all -> 0x002a }
            if (r1 == 0) goto L_0x0015
            com.google.android.gms.common.internal.zzaq r1 = r2.zzfmi     // Catch:{ RemoteException -> 0x0015 }
            r1.cancel()     // Catch:{ RemoteException -> 0x0015 }
        L_0x0015:
            R r1 = r2.zzfkk     // Catch:{ all -> 0x002a }
            zzd(r1)     // Catch:{ all -> 0x002a }
            r1 = 1
            r2.zzaj = r1     // Catch:{ all -> 0x002a }
            com.google.android.gms.common.api.Status r1 = com.google.android.gms.common.api.Status.zzfks     // Catch:{ all -> 0x002a }
            com.google.android.gms.common.api.Result r1 = r2.zzb(r1)     // Catch:{ all -> 0x002a }
            r2.zzc(r1)     // Catch:{ all -> 0x002a }
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return
        L_0x0028:
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return
        L_0x002a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzs.cancel():void");
    }

    public boolean isCanceled() {
        boolean z;
        synchronized (this.zzflz) {
            z = this.zzaj;
        }
        return z;
    }

    public final boolean isReady() {
        return this.zzaoi.getCount() == 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final void setResult(R r) {
        synchronized (this.zzflz) {
            if (this.zzfmh || this.zzaj) {
                zzd(r);
                return;
            }
            isReady();
            zzbq.zza(!isReady(), (Object) "Results have already been set");
            zzbq.zza(!this.zzfmg, (Object) "Result has already been consumed");
            zzc(r);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003b, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void setResultCallback(com.google.android.gms.common.api.ResultCallback<? super R> r5) {
        /*
            r4 = this;
            java.lang.Object r0 = r4.zzflz
            monitor-enter(r0)
            if (r5 != 0) goto L_0x000c
            r5 = 0
            r4.zzfmd = r5     // Catch:{ all -> 0x000a }
            monitor-exit(r0)     // Catch:{ all -> 0x000a }
            return
        L_0x000a:
            r5 = move-exception
            goto L_0x003c
        L_0x000c:
            boolean r1 = r4.zzfmg     // Catch:{ all -> 0x000a }
            r2 = 1
            r1 = r1 ^ r2
            java.lang.String r3 = "Result has already been consumed."
            com.google.android.gms.common.internal.zzbq.zza(r1, r3)     // Catch:{ all -> 0x000a }
            com.google.android.gms.common.api.internal.zzdi<R> r1 = r4.zzfmj     // Catch:{ all -> 0x000a }
            if (r1 != 0) goto L_0x001a
            goto L_0x001b
        L_0x001a:
            r2 = 0
        L_0x001b:
            java.lang.String r1 = "Cannot set callbacks if then() has been called."
            com.google.android.gms.common.internal.zzbq.zza(r2, r1)     // Catch:{ all -> 0x000a }
            boolean r1 = r4.isCanceled()     // Catch:{ all -> 0x000a }
            if (r1 == 0) goto L_0x0028
            monitor-exit(r0)     // Catch:{ all -> 0x000a }
            return
        L_0x0028:
            boolean r1 = r4.isReady()     // Catch:{ all -> 0x000a }
            if (r1 == 0) goto L_0x0038
            com.google.android.gms.common.api.internal.zzu<R> r1 = r4.zzfma     // Catch:{ all -> 0x000a }
            com.google.android.gms.common.api.Result r2 = r4.get()     // Catch:{ all -> 0x000a }
            r1.zza(r5, r2)     // Catch:{ all -> 0x000a }
            goto L_0x003a
        L_0x0038:
            r4.zzfmd = r5     // Catch:{ all -> 0x000a }
        L_0x003a:
            monitor-exit(r0)     // Catch:{ all -> 0x000a }
            return
        L_0x003c:
            monitor-exit(r0)     // Catch:{ all -> 0x000a }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzs.setResultCallback(com.google.android.gms.common.api.ResultCallback):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0049, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void setResultCallback(com.google.android.gms.common.api.ResultCallback<? super R> r5, long r6, java.util.concurrent.TimeUnit r8) {
        /*
            r4 = this;
            java.lang.Object r0 = r4.zzflz
            monitor-enter(r0)
            if (r5 != 0) goto L_0x000c
            r5 = 0
            r4.zzfmd = r5     // Catch:{ all -> 0x000a }
            monitor-exit(r0)     // Catch:{ all -> 0x000a }
            return
        L_0x000a:
            r5 = move-exception
            goto L_0x004a
        L_0x000c:
            boolean r1 = r4.zzfmg     // Catch:{ all -> 0x000a }
            r2 = 1
            r1 = r1 ^ r2
            java.lang.String r3 = "Result has already been consumed."
            com.google.android.gms.common.internal.zzbq.zza(r1, r3)     // Catch:{ all -> 0x000a }
            com.google.android.gms.common.api.internal.zzdi<R> r1 = r4.zzfmj     // Catch:{ all -> 0x000a }
            if (r1 != 0) goto L_0x001a
            goto L_0x001b
        L_0x001a:
            r2 = 0
        L_0x001b:
            java.lang.String r1 = "Cannot set callbacks if then() has been called."
            com.google.android.gms.common.internal.zzbq.zza(r2, r1)     // Catch:{ all -> 0x000a }
            boolean r1 = r4.isCanceled()     // Catch:{ all -> 0x000a }
            if (r1 == 0) goto L_0x0028
            monitor-exit(r0)     // Catch:{ all -> 0x000a }
            return
        L_0x0028:
            boolean r1 = r4.isReady()     // Catch:{ all -> 0x000a }
            if (r1 == 0) goto L_0x0038
            com.google.android.gms.common.api.internal.zzu<R> r6 = r4.zzfma     // Catch:{ all -> 0x000a }
            com.google.android.gms.common.api.Result r7 = r4.get()     // Catch:{ all -> 0x000a }
            r6.zza(r5, r7)     // Catch:{ all -> 0x000a }
            goto L_0x0048
        L_0x0038:
            r4.zzfmd = r5     // Catch:{ all -> 0x000a }
            com.google.android.gms.common.api.internal.zzu<R> r5 = r4.zzfma     // Catch:{ all -> 0x000a }
            long r6 = r8.toMillis(r6)     // Catch:{ all -> 0x000a }
            r8 = 2
            android.os.Message r8 = r5.obtainMessage(r8, r4)     // Catch:{ all -> 0x000a }
            r5.sendMessageDelayed(r8, r6)     // Catch:{ all -> 0x000a }
        L_0x0048:
            monitor-exit(r0)     // Catch:{ all -> 0x000a }
            return
        L_0x004a:
            monitor-exit(r0)     // Catch:{ all -> 0x000a }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzs.setResultCallback(com.google.android.gms.common.api.ResultCallback, long, java.util.concurrent.TimeUnit):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public <S extends Result> TransformedResult<S> then(ResultTransform<? super R, ? extends S> resultTransform) {
        TransformedResult<S> then;
        zzbq.zza(!this.zzfmg, (Object) "Result has already been consumed.");
        synchronized (this.zzflz) {
            boolean z = false;
            zzbq.zza(this.zzfmj == null, (Object) "Cannot call then() twice.");
            if (this.zzfmd == null) {
                z = true;
            }
            zzbq.zza(z, (Object) "Cannot call then() if callbacks are set.");
            zzbq.zza(!this.zzaj, (Object) "Cannot call then() if result was canceled.");
            this.zzfmk = true;
            this.zzfmj = new zzdi<>(this.zzfmb);
            then = this.zzfmj.then(resultTransform);
            if (isReady()) {
                this.zzfma.zza(this.zzfmj, get());
            } else {
                this.zzfmd = this.zzfmj;
            }
        }
        return then;
    }

    public final void zza(PendingResult.zza zza) {
        zzbq.checkArgument(zza != null, "Callback cannot be null.");
        synchronized (this.zzflz) {
            if (isReady()) {
                zza.zzr(this.mStatus);
            } else {
                this.zzfmc.add(zza);
            }
        }
    }

    public final void zza(zzdo zzdo) {
        this.zzfme.set(zzdo);
    }

    /* access modifiers changed from: protected */
    public final void zza(zzaq zzaq) {
        synchronized (this.zzflz) {
            this.zzfmi = zzaq;
        }
    }

    public final Integer zzagi() {
        return null;
    }

    public final boolean zzagv() {
        boolean isCanceled;
        synchronized (this.zzflz) {
            if (this.zzfmb.get() == null || !this.zzfmk) {
                cancel();
            }
            isCanceled = isCanceled();
        }
        return isCanceled;
    }

    public final void zzagw() {
        this.zzfmk = this.zzfmk || zzfly.get().booleanValue();
    }

    /* access modifiers changed from: protected */
    @NonNull
    public abstract R zzb(Status status);

    public final void zzv(Status status) {
        synchronized (this.zzflz) {
            if (!isReady()) {
                setResult(zzb(status));
                this.zzfmh = true;
            }
        }
    }
}
