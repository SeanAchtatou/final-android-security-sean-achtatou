package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.multiplayer.ParticipantResult;

final class zzdc extends zzdr {
    private /* synthetic */ String zzhlu;
    private /* synthetic */ byte[] zzhqw;
    private /* synthetic */ ParticipantResult[] zzhqy;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdc(zzcw zzcw, GoogleApiClient googleApiClient, String str, byte[] bArr, ParticipantResult[] participantResultArr) {
        super(googleApiClient, null);
        this.zzhlu = str;
        this.zzhqw = bArr;
        this.zzhqy = participantResultArr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhlu, this.zzhqw, this.zzhqy);
    }
}
