package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbfe extends zzbej {
    public static final Parcelable.Creator<zzbfe> CREATOR = new zzbff();
    private int zzdzm;
    private final zzbfg zzfze;

    zzbfe(int i, zzbfg zzbfg) {
        this.zzdzm = i;
        this.zzfze = zzbfg;
    }

    private zzbfe(zzbfg zzbfg) {
        this.zzdzm = 1;
        this.zzfze = zzbfg;
    }

    public static zzbfe zza(zzbfm<?, ?> zzbfm) {
        if (zzbfm instanceof zzbfg) {
            return new zzbfe((zzbfg) zzbfm);
        }
        throw new IllegalArgumentException("Unsupported safe parcelable field converter class.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.zzbfg, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 1, this.zzdzm);
        zzbem.zza(parcel, 2, (Parcelable) this.zzfze, i, false);
        zzbem.zzai(parcel, zze);
    }

    public final zzbfm<?, ?> zzalh() {
        if (this.zzfze != null) {
            return this.zzfze;
        }
        throw new IllegalStateException("There was no converter wrapped in this ConverterWrapper.");
    }
}
