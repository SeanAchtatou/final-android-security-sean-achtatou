package com.google.android.gms.internal;

import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.internal.zzt;

public final class zzbsj extends zzt implements SearchableMetadataField<String> {
    public zzbsj(int i) {
        super("mimeType", 4100000);
    }
}
