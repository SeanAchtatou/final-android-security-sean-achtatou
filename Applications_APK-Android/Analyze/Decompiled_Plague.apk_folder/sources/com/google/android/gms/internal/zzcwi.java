package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;

public abstract class zzcwi extends zzee implements zzcwh {
    public zzcwi() {
        attachInterface(this, "com.google.android.gms.signin.internal.ISignInCallbacks");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        Parcelable.Creator creator;
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 3:
                zzef.zza(parcel, ConnectionResult.CREATOR);
                creator = zzcwe.CREATOR;
                zzef.zza(parcel, creator);
                break;
            case 4:
            case 6:
                creator = Status.CREATOR;
                zzef.zza(parcel, creator);
                break;
            case 5:
            default:
                return false;
            case 7:
                zzef.zza(parcel, Status.CREATOR);
                creator = GoogleSignInAccount.CREATOR;
                zzef.zza(parcel, creator);
                break;
            case 8:
                zzb((zzcwo) zzef.zza(parcel, zzcwo.CREATOR));
                break;
        }
        parcel2.writeNoException();
        return true;
    }
}
