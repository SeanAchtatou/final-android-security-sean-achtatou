package com.google.android.gms.common.internal;

import android.support.annotation.NonNull;

public final class zzam {
    @NonNull
    private final String mPackageName;
    private final int zzfxr = 129;
    @NonNull
    private final String zzfyg;
    private final boolean zzfyh = false;

    public zzam(@NonNull String str, @NonNull String str2, boolean z, int i) {
        this.mPackageName = str;
        this.zzfyg = str2;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public final String getPackageName() {
        return this.mPackageName;
    }

    /* access modifiers changed from: package-private */
    public final int zzaky() {
        return this.zzfxr;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public final String zzalc() {
        return this.zzfyg;
    }
}
