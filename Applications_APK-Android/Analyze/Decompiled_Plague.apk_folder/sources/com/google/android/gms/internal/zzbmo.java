package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DrivePreferencesApi;
import com.google.android.gms.drive.FileUploadPreferences;

@Deprecated
public final class zzbmo implements DrivePreferencesApi {
    public final PendingResult<DrivePreferencesApi.FileUploadPreferencesResult> getFileUploadPreferences(GoogleApiClient googleApiClient) {
        return googleApiClient.zzd(new zzbmp(this, googleApiClient));
    }

    public final PendingResult<Status> setFileUploadPreferences(GoogleApiClient googleApiClient, FileUploadPreferences fileUploadPreferences) {
        if (fileUploadPreferences instanceof zzboz) {
            return googleApiClient.zze(new zzbmq(this, googleApiClient, (zzboz) fileUploadPreferences));
        }
        throw new IllegalArgumentException("Invalid preference value");
    }
}
