package com.google.android.gms.internal;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.ads.mediation.MediationAdapter;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import java.util.List;

@zzzb
public final class zzvb<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> extends zzug {
    private final MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> zzceg;
    private final NETWORK_EXTRAS zzceh;

    public zzvb(MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter, NETWORK_EXTRAS network_extras) {
        this.zzceg = mediationAdapter;
        this.zzceh = network_extras;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: SERVER_PARAMETERS
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private final SERVER_PARAMETERS zza(java.lang.String r3, int r4, java.lang.String r5) throws android.os.RemoteException {
        /*
            r2 = this;
            if (r3 == 0) goto L_0x002a
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0028 }
            r4.<init>(r3)     // Catch:{ Throwable -> 0x0028 }
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ Throwable -> 0x0028 }
            int r5 = r4.length()     // Catch:{ Throwable -> 0x0028 }
            r3.<init>(r5)     // Catch:{ Throwable -> 0x0028 }
            java.util.Iterator r5 = r4.keys()     // Catch:{ Throwable -> 0x0028 }
        L_0x0014:
            boolean r0 = r5.hasNext()     // Catch:{ Throwable -> 0x0028 }
            if (r0 == 0) goto L_0x0030
            java.lang.Object r0 = r5.next()     // Catch:{ Throwable -> 0x0028 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Throwable -> 0x0028 }
            java.lang.String r1 = r4.getString(r0)     // Catch:{ Throwable -> 0x0028 }
            r3.put(r0, r1)     // Catch:{ Throwable -> 0x0028 }
            goto L_0x0014
        L_0x0028:
            r3 = move-exception
            goto L_0x0044
        L_0x002a:
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ Throwable -> 0x0028 }
            r4 = 0
            r3.<init>(r4)     // Catch:{ Throwable -> 0x0028 }
        L_0x0030:
            com.google.ads.mediation.MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> r4 = r2.zzceg     // Catch:{ Throwable -> 0x0028 }
            java.lang.Class r4 = r4.getServerParametersType()     // Catch:{ Throwable -> 0x0028 }
            r5 = 0
            if (r4 == 0) goto L_0x0043
            java.lang.Object r4 = r4.newInstance()     // Catch:{ Throwable -> 0x0028 }
            r5 = r4
            com.google.ads.mediation.MediationServerParameters r5 = (com.google.ads.mediation.MediationServerParameters) r5     // Catch:{ Throwable -> 0x0028 }
            r5.load(r3)     // Catch:{ Throwable -> 0x0028 }
        L_0x0043:
            return r5
        L_0x0044:
            java.lang.String r4 = "Could not get MediationServerParameters."
            com.google.android.gms.internal.zzaiw.zzc(r4, r3)
            android.os.RemoteException r3 = new android.os.RemoteException
            r3.<init>()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzvb.zza(java.lang.String, int, java.lang.String):com.google.ads.mediation.MediationServerParameters");
    }

    private static boolean zzn(zzis zzis) {
        if (zzis.zzbby) {
            return true;
        }
        zzjk.zzhx();
        return zzais.zzqs();
    }

    public final void destroy() throws RemoteException {
        try {
            this.zzceg.destroy();
        } catch (Throwable th) {
            zzaiw.zzc("Could not destroy adapter.", th);
            throw new RemoteException();
        }
    }

    public final Bundle getInterstitialAdapterInfo() {
        return new Bundle();
    }

    public final zzku getVideoController() {
        return null;
    }

    public final IObjectWrapper getView() throws RemoteException {
        if (!(this.zzceg instanceof MediationBannerAdapter)) {
            String valueOf = String.valueOf(this.zzceg.getClass().getCanonicalName());
            zzaiw.zzco(valueOf.length() != 0 ? "MediationAdapter is not a MediationBannerAdapter: ".concat(valueOf) : new String("MediationAdapter is not a MediationBannerAdapter: "));
            throw new RemoteException();
        }
        try {
            return zzn.zzy(((MediationBannerAdapter) this.zzceg).getBannerView());
        } catch (Throwable th) {
            zzaiw.zzc("Could not get banner view from adapter.", th);
            throw new RemoteException();
        }
    }

    public final boolean isInitialized() {
        return true;
    }

    public final void pause() throws RemoteException {
        throw new RemoteException();
    }

    public final void resume() throws RemoteException {
        throw new RemoteException();
    }

    public final void setImmersiveMode(boolean z) {
    }

    public final void showInterstitial() throws RemoteException {
        if (!(this.zzceg instanceof MediationInterstitialAdapter)) {
            String valueOf = String.valueOf(this.zzceg.getClass().getCanonicalName());
            zzaiw.zzco(valueOf.length() != 0 ? "MediationAdapter is not a MediationInterstitialAdapter: ".concat(valueOf) : new String("MediationAdapter is not a MediationInterstitialAdapter: "));
            throw new RemoteException();
        }
        zzaiw.zzbw("Showing interstitial from adapter.");
        try {
            ((MediationInterstitialAdapter) this.zzceg).showInterstitial();
        } catch (Throwable th) {
            zzaiw.zzc("Could not show interstitial from adapter.", th);
            throw new RemoteException();
        }
    }

    public final void showVideo() {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzads zzads, List<String> list) {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzis zzis, String str, zzads zzads, String str2) throws RemoteException {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzis zzis, String str, zzui zzui) throws RemoteException {
        zza(iObjectWrapper, zzis, str, (String) null, zzui);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzis zzis, String str, String str2, zzui zzui) throws RemoteException {
        if (!(this.zzceg instanceof MediationInterstitialAdapter)) {
            String valueOf = String.valueOf(this.zzceg.getClass().getCanonicalName());
            zzaiw.zzco(valueOf.length() != 0 ? "MediationAdapter is not a MediationInterstitialAdapter: ".concat(valueOf) : new String("MediationAdapter is not a MediationInterstitialAdapter: "));
            throw new RemoteException();
        }
        zzaiw.zzbw("Requesting interstitial ad from adapter.");
        try {
            ((MediationInterstitialAdapter) this.zzceg).requestInterstitialAd(new zzvc(zzui), (Activity) zzn.zzx(iObjectWrapper), zza(str, zzis.zzbbz, str2), zzvo.zza(zzis, zzn(zzis)), this.zzceh);
        } catch (Throwable th) {
            zzaiw.zzc("Could not request interstitial ad from adapter.", th);
            throw new RemoteException();
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzis zzis, String str, String str2, zzui zzui, zzom zzom, List<String> list) {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zziw zziw, zzis zzis, String str, zzui zzui) throws RemoteException {
        zza(iObjectWrapper, zziw, zzis, str, null, zzui);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zziw zziw, zzis zzis, String str, String str2, zzui zzui) throws RemoteException {
        if (!(this.zzceg instanceof MediationBannerAdapter)) {
            String valueOf = String.valueOf(this.zzceg.getClass().getCanonicalName());
            zzaiw.zzco(valueOf.length() != 0 ? "MediationAdapter is not a MediationBannerAdapter: ".concat(valueOf) : new String("MediationAdapter is not a MediationBannerAdapter: "));
            throw new RemoteException();
        }
        zzaiw.zzbw("Requesting banner ad from adapter.");
        try {
            ((MediationBannerAdapter) this.zzceg).requestBannerAd(new zzvc(zzui), (Activity) zzn.zzx(iObjectWrapper), zza(str, zzis.zzbbz, str2), zzvo.zzb(zziw), zzvo.zza(zzis, zzn(zzis)), this.zzceh);
        } catch (Throwable th) {
            zzaiw.zzc("Could not request banner ad from adapter.", th);
            throw new RemoteException();
        }
    }

    public final void zza(zzis zzis, String str, String str2) {
    }

    public final void zzc(zzis zzis, String str) {
    }

    public final void zzg(IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    public final zzuo zzly() {
        return null;
    }

    public final zzur zzlz() {
        return null;
    }

    public final Bundle zzma() {
        return new Bundle();
    }

    public final Bundle zzmb() {
        return new Bundle();
    }

    public final boolean zzmc() {
        return false;
    }

    public final zzpu zzmd() {
        return null;
    }
}
