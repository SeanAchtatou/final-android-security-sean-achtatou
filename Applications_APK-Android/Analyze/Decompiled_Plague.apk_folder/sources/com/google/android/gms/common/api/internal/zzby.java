package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import java.util.Collections;

final class zzby implements Runnable {
    private /* synthetic */ ConnectionResult zzfqy;
    private /* synthetic */ zzbx zzfrb;

    zzby(zzbx zzbx, ConnectionResult connectionResult) {
        this.zzfrb = zzbx;
        this.zzfqy = connectionResult;
    }

    public final void run() {
        if (this.zzfqy.isSuccess()) {
            boolean unused = this.zzfrb.zzfra = true;
            if (this.zzfrb.zzfnb.zzaam()) {
                this.zzfrb.zzaiu();
            } else {
                this.zzfrb.zzfnb.zza(null, Collections.emptySet());
            }
        } else {
            ((zzbr) this.zzfrb.zzfqo.zzfne.get(this.zzfrb.zzfjl)).onConnectionFailed(this.zzfqy);
        }
    }
}
