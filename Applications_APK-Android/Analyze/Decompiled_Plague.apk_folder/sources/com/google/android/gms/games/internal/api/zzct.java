package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzct extends zzcu {
    private /* synthetic */ boolean zzhpv;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzct(zzcs zzcs, GoogleApiClient googleApiClient, boolean z) {
        super(googleApiClient, null);
        this.zzhpv = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zze(this, this.zzhpv);
    }
}
