package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbek;

public final class zzy implements Parcelable.Creator<zzx> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        String str = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 1) {
                zzbek.zzb(parcel, readInt);
            } else {
                str = zzbek.zzq(parcel, readInt);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzx(str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzx[i];
    }
}
