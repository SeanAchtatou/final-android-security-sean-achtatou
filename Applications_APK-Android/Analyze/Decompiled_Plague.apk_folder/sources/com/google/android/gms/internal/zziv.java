package com.google.android.gms.internal;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.search.SearchAdRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

@zzzb
public final class zziv {
    public static final zziv zzbcz = new zziv();

    protected zziv() {
    }

    public static zzis zza(Context context, zzlc zzlc) {
        List list;
        Context context2;
        String str;
        zzlc zzlc2 = zzlc;
        Date birthday = zzlc.getBirthday();
        long time = birthday != null ? birthday.getTime() : -1;
        String contentUrl = zzlc.getContentUrl();
        int gender = zzlc.getGender();
        Set<String> keywords = zzlc.getKeywords();
        if (!keywords.isEmpty()) {
            list = Collections.unmodifiableList(new ArrayList(keywords));
            context2 = context;
        } else {
            context2 = context;
            list = null;
        }
        boolean isTestDevice = zzlc2.isTestDevice(context2);
        int zzii = zzlc.zzii();
        Location location = zzlc.getLocation();
        Bundle networkExtrasBundle = zzlc2.getNetworkExtrasBundle(AdMobAdapter.class);
        boolean manualImpressionsEnabled = zzlc.getManualImpressionsEnabled();
        String publisherProvidedId = zzlc.getPublisherProvidedId();
        SearchAdRequest zzif = zzlc.zzif();
        zzlw zzlw = zzif != null ? new zzlw(zzif) : null;
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            String packageName = applicationContext.getPackageName();
            zzjk.zzhx();
            str = zzais.zza(Thread.currentThread().getStackTrace(), packageName);
        } else {
            str = null;
        }
        return new zzis(7, time, networkExtrasBundle, gender, list, isTestDevice, zzii, manualImpressionsEnabled, publisherProvidedId, zzlw, location, contentUrl, zzlc.zzih(), zzlc.getCustomTargeting(), Collections.unmodifiableList(new ArrayList(zzlc.zzij())), zzlc.zzie(), str, zzlc.isDesignedForFamilies());
    }
}
