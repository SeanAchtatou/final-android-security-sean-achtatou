package com.google.android.gms.internal;

public final class zzdox<P> {
    private final P zzlpj;
    private final zzdta zzlpk;
    private final zzdrn zzlpl;
    private final zzdrv zzlpm;

    public zzdox(P p, byte[] bArr, zzdrn zzdrn, zzdrv zzdrv) {
        this.zzlpj = p;
        this.zzlpk = zzdta.zzak(bArr);
        this.zzlpl = zzdrn;
        this.zzlpm = zzdrv;
    }

    public final P zzblg() {
        return this.zzlpj;
    }

    public final byte[] zzblh() {
        if (this.zzlpk == null) {
            return null;
        }
        return this.zzlpk.getBytes();
    }
}
