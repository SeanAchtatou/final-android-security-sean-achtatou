package com.google.android.gms.internal;

import java.util.Arrays;
import java.util.Stack;

final class zzffr {
    private final Stack<zzfdh> zzpdm;

    private zzffr() {
        this.zzpdm = new Stack<>();
    }

    private final void zzao(zzfdh zzfdh) {
        while (!zzfdh.zzctn()) {
            if (zzfdh instanceof zzffp) {
                zzffp zzffp = (zzffp) zzfdh;
                zzao(zzffp.zzpdi);
                zzfdh = zzffp.zzpdj;
            } else {
                String valueOf = String.valueOf(zzfdh.getClass());
                StringBuilder sb = new StringBuilder(49 + String.valueOf(valueOf).length());
                sb.append("Has a new type of ByteString been created? Found ");
                sb.append(valueOf);
                throw new IllegalArgumentException(sb.toString());
            }
        }
        int zzlo = zzlo(zzfdh.size());
        int i = zzffp.zzpdg[zzlo + 1];
        if (this.zzpdm.isEmpty() || this.zzpdm.peek().size() >= i) {
            this.zzpdm.push(zzfdh);
            return;
        }
        int i2 = zzffp.zzpdg[zzlo];
        zzfdh pop = this.zzpdm.pop();
        while (!this.zzpdm.isEmpty() && this.zzpdm.peek().size() < i2) {
            pop = new zzffp(this.zzpdm.pop(), pop);
        }
        zzffp zzffp2 = new zzffp(pop, zzfdh);
        while (!this.zzpdm.isEmpty()) {
            if (this.zzpdm.peek().size() >= zzffp.zzpdg[zzlo(zzffp2.size()) + 1]) {
                break;
            }
            zzffp2 = new zzffp(this.zzpdm.pop(), zzffp2);
        }
        this.zzpdm.push(zzffp2);
    }

    /* access modifiers changed from: private */
    public final zzfdh zzc(zzfdh zzfdh, zzfdh zzfdh2) {
        zzao(zzfdh);
        zzao(zzfdh2);
        zzfdh pop = this.zzpdm.pop();
        while (!this.zzpdm.isEmpty()) {
            pop = new zzffp(this.zzpdm.pop(), pop);
        }
        return pop;
    }

    private static int zzlo(int i) {
        int binarySearch = Arrays.binarySearch(zzffp.zzpdg, i);
        return binarySearch < 0 ? (-(binarySearch + 1)) - 1 : binarySearch;
    }
}
