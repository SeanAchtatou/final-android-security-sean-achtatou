package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class zzb extends zzc implements zza {
    public static final Parcelable.Creator<zzb> CREATOR = new zzc();
    private final String zzhrs;
    private final String zzhrt;
    private final long zzhru;
    private final Uri zzhrv;
    private final Uri zzhrw;
    private final Uri zzhrx;

    public zzb(zza zza) {
        this.zzhrs = zza.zzatj();
        this.zzhrt = zza.zzatk();
        this.zzhru = zza.zzatl();
        this.zzhrv = zza.zzatm();
        this.zzhrw = zza.zzatn();
        this.zzhrx = zza.zzato();
    }

    zzb(String str, String str2, long j, Uri uri, Uri uri2, Uri uri3) {
        this.zzhrs = str;
        this.zzhrt = str2;
        this.zzhru = j;
        this.zzhrv = uri;
        this.zzhrw = uri2;
        this.zzhrx = uri3;
    }

    static int zza(zza zza) {
        return Arrays.hashCode(new Object[]{zza.zzatj(), zza.zzatk(), Long.valueOf(zza.zzatl()), zza.zzatm(), zza.zzatn(), zza.zzato()});
    }

    static boolean zza(zza zza, Object obj) {
        if (!(obj instanceof zza)) {
            return false;
        }
        if (zza == obj) {
            return true;
        }
        zza zza2 = (zza) obj;
        return zzbg.equal(zza2.zzatj(), zza.zzatj()) && zzbg.equal(zza2.zzatk(), zza.zzatk()) && zzbg.equal(Long.valueOf(zza2.zzatl()), Long.valueOf(zza.zzatl())) && zzbg.equal(zza2.zzatm(), zza.zzatm()) && zzbg.equal(zza2.zzatn(), zza.zzatn()) && zzbg.equal(zza2.zzato(), zza.zzato());
    }

    static String zzb(zza zza) {
        return zzbg.zzw(zza).zzg("GameId", zza.zzatj()).zzg("GameName", zza.zzatk()).zzg("ActivityTimestampMillis", Long.valueOf(zza.zzatl())).zzg("GameIconUri", zza.zzatm()).zzg("GameHiResUri", zza.zzatn()).zzg("GameFeaturedUri", zza.zzato()).toString();
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    public final /* bridge */ /* synthetic */ Object freeze() {
        if (this != null) {
            return this;
        }
        throw null;
    }

    public final int hashCode() {
        return zza(this);
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String toString() {
        return zzb(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, this.zzhrs, false);
        zzbem.zza(parcel, 2, this.zzhrt, false);
        zzbem.zza(parcel, 3, this.zzhru);
        zzbem.zza(parcel, 4, (Parcelable) this.zzhrv, i, false);
        zzbem.zza(parcel, 5, (Parcelable) this.zzhrw, i, false);
        zzbem.zza(parcel, 6, (Parcelable) this.zzhrx, i, false);
        zzbem.zzai(parcel, zze);
    }

    public final String zzatj() {
        return this.zzhrs;
    }

    public final String zzatk() {
        return this.zzhrt;
    }

    public final long zzatl() {
        return this.zzhru;
    }

    public final Uri zzatm() {
        return this.zzhrv;
    }

    public final Uri zzatn() {
        return this.zzhrw;
    }

    public final Uri zzato() {
        return this.zzhrx;
    }
}
