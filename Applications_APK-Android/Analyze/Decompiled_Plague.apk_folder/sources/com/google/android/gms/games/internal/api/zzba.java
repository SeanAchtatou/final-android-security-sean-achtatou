package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzba extends zzbh {
    private /* synthetic */ String zzfcc;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzba(zzaz zzaz, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient);
        this.zzfcc = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.Players$LoadPlayersResult>, java.lang.String, boolean):void
     arg types: [com.google.android.gms.games.internal.api.zzba, java.lang.String, int]
     candidates:
      com.google.android.gms.games.internal.GamesClientImpl.zza(byte[], java.lang.String, java.lang.String[]):int
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer$LoadMatchesResult>, int, int[]):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.snapshot.Snapshots$CommitSnapshotResult>, com.google.android.gms.games.snapshot.Snapshot, com.google.android.gms.games.snapshot.SnapshotMetadataChange):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.achievement.Achievements$UpdateAchievementResult>, java.lang.String, int):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer$LeaveMatchResult>, java.lang.String, java.lang.String):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.event.Events$LoadEventsResult>, boolean, java.lang.String[]):void
      com.google.android.gms.common.internal.zzd.zza(com.google.android.gms.common.internal.zzd, int, android.os.IInterface):void
      com.google.android.gms.common.internal.zzd.zza(int, int, android.os.IInterface):boolean
      com.google.android.gms.common.internal.zzd.zza(int, android.os.Bundle, int):void
      com.google.android.gms.common.internal.zzd.zza(com.google.android.gms.common.internal.zzj, int, android.app.PendingIntent):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.Players$LoadPlayersResult>, java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza((zzn<Players.LoadPlayersResult>) this, this.zzfcc, false);
    }
}
