package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import com.google.android.gms.internal.zzbsb;
import com.google.android.gms.internal.zzfhk;

public class zza extends zzbej {
    public static final Parcelable.Creator<zza> CREATOR = new zzb();
    private long zzgfs;
    private long zzgft;
    private long zzgfu;
    private volatile String zzgfv = null;

    public zza(long j, long j2, long j3) {
        boolean z = false;
        zzbq.checkArgument(j != -1);
        zzbq.checkArgument(j2 != -1);
        zzbq.checkArgument(j3 != -1 ? true : z);
        this.zzgfs = j;
        this.zzgft = j2;
        this.zzgfu = j3;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != zza.class) {
            return false;
        }
        zza zza = (zza) obj;
        return zza.zzgft == this.zzgft && zza.zzgfu == this.zzgfu && zza.zzgfs == this.zzgfs;
    }

    public int hashCode() {
        String valueOf = String.valueOf(this.zzgfs);
        String valueOf2 = String.valueOf(this.zzgft);
        String valueOf3 = String.valueOf(this.zzgfu);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length());
        sb.append(valueOf);
        sb.append(valueOf2);
        sb.append(valueOf3);
        return sb.toString().hashCode();
    }

    public String toString() {
        if (this.zzgfv == null) {
            zzbsb zzbsb = new zzbsb();
            zzbsb.versionCode = 1;
            zzbsb.sequenceNumber = this.zzgfs;
            zzbsb.zzgox = this.zzgft;
            zzbsb.zzgoy = this.zzgfu;
            String encodeToString = Base64.encodeToString(zzfhk.zzc(zzbsb), 10);
            String valueOf = String.valueOf("ChangeSequenceNumber:");
            String valueOf2 = String.valueOf(encodeToString);
            this.zzgfv = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        }
        return this.zzgfv;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, this.zzgfs);
        zzbem.zza(parcel, 3, this.zzgft);
        zzbem.zza(parcel, 4, this.zzgfu);
        zzbem.zzai(parcel, zze);
    }
}
