package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.support.annotation.Nullable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzan;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzp;
import com.google.android.gms.common.internal.zzr;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public final class Api<O extends ApiOptions> {
    private final String mName;
    private final zza<?, O> zzfiy;
    private final zzh<?, O> zzfiz = null;
    private final zzf<?> zzfja;
    private final zzi<?> zzfjb;

    public interface ApiOptions {

        public interface HasAccountOptions extends HasOptions, NotRequiredOptions {
            Account getAccount();
        }

        public interface HasGoogleSignInAccountOptions extends HasOptions {
            GoogleSignInAccount getGoogleSignInAccount();
        }

        public interface HasOptions extends ApiOptions {
        }

        public static final class NoOptions implements NotRequiredOptions {
            private NoOptions() {
            }
        }

        public interface NotRequiredOptions extends ApiOptions {
        }

        public interface Optional extends HasOptions, NotRequiredOptions {
        }
    }

    public static abstract class zza<T extends zze, O> extends zzd<T, O> {
        public abstract T zza(Context context, Looper looper, zzr zzr, O o, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener);
    }

    public interface zzb {
    }

    public static class zzc<C extends zzb> {
    }

    public static abstract class zzd<T extends zzb, O> {
        public int getPriority() {
            return Integer.MAX_VALUE;
        }

        public List<Scope> zzq(O o) {
            return Collections.emptyList();
        }
    }

    public interface zze extends zzb {
        void disconnect();

        void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

        Intent getSignInIntent();

        boolean isConnected();

        boolean isConnecting();

        void zza(zzan zzan, Set<Scope> set);

        void zza(zzj zzj);

        void zza(zzp zzp);

        boolean zzaam();

        boolean zzaaw();

        boolean zzafu();

        @Nullable
        IBinder zzafv();
    }

    public static final class zzf<C extends zze> extends zzc<C> {
    }

    public interface zzg<T extends IInterface> extends zzb {
    }

    public static abstract class zzh<T extends zzg, O> extends zzd<T, O> {
    }

    public static final class zzi<C extends zzg> extends zzc<C> {
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [com.google.android.gms.common.api.Api$zza<?, O>, com.google.android.gms.common.api.Api$zza<C, O>, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r4v0, types: [com.google.android.gms.common.api.Api$zzf<C>, com.google.android.gms.common.api.Api$zzf<?>, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <C extends com.google.android.gms.common.api.Api.zze> Api(java.lang.String r2, com.google.android.gms.common.api.Api.zza<C, O> r3, com.google.android.gms.common.api.Api.zzf<C> r4) {
        /*
            r1 = this;
            r1.<init>()
            java.lang.String r0 = "Cannot construct an Api with a null ClientBuilder"
            com.google.android.gms.common.internal.zzbq.checkNotNull(r3, r0)
            java.lang.String r0 = "Cannot construct an Api with a null ClientKey"
            com.google.android.gms.common.internal.zzbq.checkNotNull(r4, r0)
            r1.mName = r2
            r1.zzfiy = r3
            r2 = 0
            r1.zzfiz = r2
            r1.zzfja = r4
            r1.zzfjb = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.Api.<init>(java.lang.String, com.google.android.gms.common.api.Api$zza, com.google.android.gms.common.api.Api$zzf):void");
    }

    public final String getName() {
        return this.mName;
    }

    public final zzd<?, O> zzafr() {
        return this.zzfiy;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final zza<?, O> zzafs() {
        zzbq.zza(this.zzfiy != null, (Object) "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder");
        return this.zzfiy;
    }

    public final zzc<?> zzaft() {
        if (this.zzfja != null) {
            return this.zzfja;
        }
        throw new IllegalStateException("This API was constructed with null client keys. This should not be possible.");
    }
}
