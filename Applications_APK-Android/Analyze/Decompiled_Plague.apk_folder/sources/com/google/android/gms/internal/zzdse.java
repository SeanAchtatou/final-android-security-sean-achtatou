package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

final class zzdse {
    private zzdsa zzlui;
    private int[] zzluj;
    private int[] zzluk;
    private int[] zzlul = new int[16];
    private int zzlum = 0;
    private boolean zzlun;

    zzdse(zzdsa zzdsa, byte[] bArr, int i) {
        this.zzlui = zzdsa;
        this.zzluj = zzdsa.zzd(bArr, i);
        this.zzluk = zzdsa.zzf(this.zzluj);
        this.zzlun = false;
    }

    /* access modifiers changed from: package-private */
    public final int[] zzbos() {
        this.zzlun = true;
        System.arraycopy(this.zzluk, this.zzlum, this.zzlul, 0, 16 - this.zzlum);
        this.zzlui.zzh(this.zzluj);
        this.zzluk = this.zzlui.zzf(this.zzluj);
        System.arraycopy(this.zzluk, 0, this.zzlul, 16 - this.zzlum, this.zzlum);
        return this.zzlul;
    }

    /* access modifiers changed from: package-private */
    public final byte[] zzfy(int i) {
        if (this.zzlun) {
            throw new IllegalStateException("first can only be called once and before next().");
        }
        this.zzlun = true;
        this.zzlum = 8;
        ByteBuffer order = ByteBuffer.allocate(32).order(ByteOrder.LITTLE_ENDIAN);
        order.asIntBuffer().put(this.zzluk, 0, 8);
        return order.array();
    }
}
