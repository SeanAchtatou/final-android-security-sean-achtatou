package com.google.android.gms.internal;

import java.util.Arrays;

final class zzfhm {
    final int tag;
    final byte[] zzjkl;

    zzfhm(int i, byte[] bArr) {
        this.tag = i;
        this.zzjkl = bArr;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfhm)) {
            return false;
        }
        zzfhm zzfhm = (zzfhm) obj;
        return this.tag == zzfhm.tag && Arrays.equals(this.zzjkl, zzfhm.zzjkl);
    }

    public final int hashCode() {
        return ((527 + this.tag) * 31) + Arrays.hashCode(this.zzjkl);
    }
}
