package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzbm extends zzbs {
    private /* synthetic */ boolean zzhpv;
    private /* synthetic */ int zzhpz;
    private /* synthetic */ int[] zzhqi;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbm(zzbj zzbj, GoogleApiClient googleApiClient, int[] iArr, int i, boolean z) {
        super(googleApiClient, null);
        this.zzhqi = iArr;
        this.zzhpz = i;
        this.zzhpv = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhqi, this.zzhpz, this.zzhpv);
    }
}
