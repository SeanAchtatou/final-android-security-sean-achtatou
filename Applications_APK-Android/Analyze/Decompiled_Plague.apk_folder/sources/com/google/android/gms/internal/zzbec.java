package com.google.android.gms.internal;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;

public final class zzbec extends Drawable implements Drawable.Callback {
    private int mAlpha;
    private int mFrom;
    private long zzdss;
    private boolean zzfup;
    private int zzfuu;
    private int zzfuv;
    private int zzfuw;
    private int zzfux;
    private boolean zzfuy;
    private zzbeg zzfuz;
    private Drawable zzfva;
    private Drawable zzfvb;
    private boolean zzfvc;
    private boolean zzfvd;
    private boolean zzfve;
    private int zzfvf;

    public zzbec(Drawable drawable, Drawable drawable2) {
        this(null);
        drawable = drawable == null ? zzbee.zzfvg : drawable;
        this.zzfva = drawable;
        drawable.setCallback(this);
        zzbeg zzbeg = this.zzfuz;
        zzbeg.zzfvi = drawable.getChangingConfigurations() | zzbeg.zzfvi;
        drawable2 = drawable2 == null ? zzbee.zzfvg : drawable2;
        this.zzfvb = drawable2;
        drawable2.setCallback(this);
        zzbeg zzbeg2 = this.zzfuz;
        zzbeg2.zzfvi = drawable2.getChangingConfigurations() | zzbeg2.zzfvi;
    }

    zzbec(zzbeg zzbeg) {
        this.zzfuu = 0;
        this.zzfuw = 255;
        this.mAlpha = 0;
        this.zzfup = true;
        this.zzfuz = new zzbeg(zzbeg);
    }

    private final boolean canConstantState() {
        if (!this.zzfvc) {
            this.zzfvd = (this.zzfva.getConstantState() == null || this.zzfvb.getConstantState() == null) ? false : true;
            this.zzfvc = true;
        }
        return this.zzfvd;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public final void draw(Canvas canvas) {
        boolean z = true;
        switch (this.zzfuu) {
            case 1:
                this.zzdss = SystemClock.uptimeMillis();
                this.zzfuu = 2;
                z = false;
                break;
            case 2:
                if (this.zzdss >= 0) {
                    float uptimeMillis = ((float) (SystemClock.uptimeMillis() - this.zzdss)) / ((float) this.zzfux);
                    if (uptimeMillis < 1.0f) {
                        z = false;
                    }
                    if (z) {
                        this.zzfuu = 0;
                    }
                    this.mAlpha = (int) (0.0f + (((float) this.zzfuv) * Math.min(uptimeMillis, 1.0f)));
                    break;
                }
                break;
        }
        int i = this.mAlpha;
        boolean z2 = this.zzfup;
        Drawable drawable = this.zzfva;
        Drawable drawable2 = this.zzfvb;
        if (z) {
            if (!z2 || i == 0) {
                drawable.draw(canvas);
            }
            if (i == this.zzfuw) {
                drawable2.setAlpha(this.zzfuw);
                drawable2.draw(canvas);
                return;
            }
            return;
        }
        if (z2) {
            drawable.setAlpha(this.zzfuw - i);
        }
        drawable.draw(canvas);
        if (z2) {
            drawable.setAlpha(this.zzfuw);
        }
        if (i > 0) {
            drawable2.setAlpha(i);
            drawable2.draw(canvas);
            drawable2.setAlpha(this.zzfuw);
        }
        invalidateSelf();
    }

    public final int getChangingConfigurations() {
        return super.getChangingConfigurations() | this.zzfuz.mChangingConfigurations | this.zzfuz.zzfvi;
    }

    public final Drawable.ConstantState getConstantState() {
        if (!canConstantState()) {
            return null;
        }
        this.zzfuz.mChangingConfigurations = getChangingConfigurations();
        return this.zzfuz;
    }

    public final int getIntrinsicHeight() {
        return Math.max(this.zzfva.getIntrinsicHeight(), this.zzfvb.getIntrinsicHeight());
    }

    public final int getIntrinsicWidth() {
        return Math.max(this.zzfva.getIntrinsicWidth(), this.zzfvb.getIntrinsicWidth());
    }

    public final int getOpacity() {
        if (!this.zzfve) {
            this.zzfvf = Drawable.resolveOpacity(this.zzfva.getOpacity(), this.zzfvb.getOpacity());
            this.zzfve = true;
        }
        return this.zzfvf;
    }

    public final void invalidateDrawable(Drawable drawable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    public final Drawable mutate() {
        if (!this.zzfuy && super.mutate() == this) {
            if (!canConstantState()) {
                throw new IllegalStateException("One or more children of this LayerDrawable does not have constant state; this drawable cannot be mutated.");
            }
            this.zzfva.mutate();
            this.zzfvb.mutate();
            this.zzfuy = true;
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public final void onBoundsChange(Rect rect) {
        this.zzfva.setBounds(rect);
        this.zzfvb.setBounds(rect);
    }

    public final void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j);
        }
    }

    public final void setAlpha(int i) {
        if (this.mAlpha == this.zzfuw) {
            this.mAlpha = i;
        }
        this.zzfuw = i;
        invalidateSelf();
    }

    public final void setColorFilter(ColorFilter colorFilter) {
        this.zzfva.setColorFilter(colorFilter);
        this.zzfvb.setColorFilter(colorFilter);
    }

    public final void startTransition(int i) {
        this.mFrom = 0;
        this.zzfuv = this.zzfuw;
        this.mAlpha = 0;
        this.zzfux = 250;
        this.zzfuu = 1;
        invalidateSelf();
    }

    public final void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }

    public final Drawable zzajs() {
        return this.zzfvb;
    }
}
