package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.internal.zzbse;
import com.google.android.gms.internal.zzbsp;
import com.google.android.gms.internal.zzbsr;
import com.google.android.gms.internal.zzbsz;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class zzf {
    private static final Map<String, MetadataField<?>> zzgpl = new HashMap();
    private static final Map<String, zzg> zzgpm = new HashMap();

    static {
        zzb(zzbse.zzgpr);
        zzb(zzbse.zzgqx);
        zzb(zzbse.zzgqo);
        zzb(zzbse.zzgqv);
        zzb(zzbse.zzgqy);
        zzb(zzbse.zzgqe);
        zzb(zzbse.zzgqd);
        zzb(zzbse.zzgqf);
        zzb(zzbse.zzgqg);
        zzb(zzbse.zzgqh);
        zzb(zzbse.zzgqb);
        zzb(zzbse.zzgqj);
        zzb(zzbse.zzgqk);
        zzb(zzbse.zzgql);
        zzb(zzbse.zzgqt);
        zzb(zzbse.zzgps);
        zzb(zzbse.zzgqq);
        zzb(zzbse.zzgpu);
        zzb(zzbse.zzgqc);
        zzb(zzbse.zzgpv);
        zzb(zzbse.zzgpw);
        zzb(zzbse.zzgpx);
        zzb(zzbse.zzgpy);
        zzb(zzbse.zzgqn);
        zzb(zzbse.zzgqi);
        zzb(zzbse.zzgqp);
        zzb(zzbse.zzgqr);
        zzb(zzbse.zzgqs);
        zzb(zzbse.zzgqu);
        zzb(zzbse.zzgqz);
        zzb(zzbse.zzgra);
        zzb(zzbse.zzgqa);
        zzb(zzbse.zzgpz);
        zzb(zzbse.zzgqw);
        zzb(zzbse.zzgqm);
        zzb(zzbse.zzgpt);
        zzb(zzbse.zzgrb);
        zzb(zzbse.zzgrc);
        zzb(zzbse.zzgrd);
        zzb(zzbse.zzgre);
        zzb(zzbse.zzgrf);
        zzb(zzbse.zzgrg);
        zzb(zzbse.zzgrh);
        zzb(zzbsr.zzgrj);
        zzb(zzbsr.zzgrl);
        zzb(zzbsr.zzgrm);
        zzb(zzbsr.zzgrn);
        zzb(zzbsr.zzgrk);
        zzb(zzbsr.zzgro);
        zzb(zzbsz.zzgrq);
        zzb(zzbsz.zzgrr);
        zza(zzo.zzgpq);
        zza(zzbsp.zzgri);
    }

    private static void zza(zzg zzg) {
        if (zzgpm.put(zzg.zzapc(), zzg) != null) {
            String zzapc = zzg.zzapc();
            StringBuilder sb = new StringBuilder(46 + String.valueOf(zzapc).length());
            sb.append("A cleaner for key ");
            sb.append(zzapc);
            sb.append(" has already been registered");
            throw new IllegalStateException(sb.toString());
        }
    }

    public static Collection<MetadataField<?>> zzapb() {
        return Collections.unmodifiableCollection(zzgpl.values());
    }

    public static void zzb(DataHolder dataHolder) {
        for (zzg zzc : zzgpm.values()) {
            zzc.zzc(dataHolder);
        }
    }

    private static void zzb(MetadataField<?> metadataField) {
        if (zzgpl.containsKey(metadataField.getName())) {
            String valueOf = String.valueOf(metadataField.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Duplicate field name registered: ".concat(valueOf) : new String("Duplicate field name registered: "));
        } else {
            zzgpl.put(metadataField.getName(), metadataField);
        }
    }

    public static MetadataField<?> zzgs(String str) {
        return zzgpl.get(str);
    }
}
