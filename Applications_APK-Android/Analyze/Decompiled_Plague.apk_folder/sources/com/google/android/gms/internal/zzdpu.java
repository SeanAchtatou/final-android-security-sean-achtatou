package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdpy;
import java.io.IOException;

public final class zzdpu extends zzfee<zzdpu, zza> implements zzffk {
    private static volatile zzffm<zzdpu> zzbas;
    /* access modifiers changed from: private */
    public static final zzdpu zzlqk;
    private int zzlqb;
    private zzdpy zzlqi;
    private zzfdh zzlqj = zzfdh.zzpal;

    public static final class zza extends zzfef<zzdpu, zza> implements zzffk {
        private zza() {
            super(zzdpu.zzlqk);
        }

        /* synthetic */ zza(zzdpv zzdpv) {
            this();
        }

        public final zza zzc(zzdpy zzdpy) {
            zzcvi();
            ((zzdpu) this.zzpbv).zzb(zzdpy);
            return this;
        }

        public final zza zzfj(int i) {
            zzcvi();
            ((zzdpu) this.zzpbv).setVersion(0);
            return this;
        }

        public final zza zzl(zzfdh zzfdh) {
            zzcvi();
            ((zzdpu) this.zzpbv).zzj(zzfdh);
            return this;
        }
    }

    static {
        zzdpu zzdpu = new zzdpu();
        zzlqk = zzdpu;
        zzdpu.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdpu.zzpbs.zzbim();
    }

    private zzdpu() {
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzlqb = i;
    }

    /* access modifiers changed from: private */
    public final void zzb(zzdpy zzdpy) {
        if (zzdpy == null) {
            throw new NullPointerException();
        }
        this.zzlqi = zzdpy;
    }

    public static zza zzblu() {
        zzdpu zzdpu = zzlqk;
        zzfef zzfef = (zzfef) zzdpu.zza(zzfem.zzpch, (Object) null, (Object) null);
        zzfef.zza((zzfee) zzdpu);
        return (zza) zzfef;
    }

    public static zzdpu zzblv() {
        return zzlqk;
    }

    /* access modifiers changed from: private */
    public final void zzj(zzfdh zzfdh) {
        if (zzfdh == null) {
            throw new NullPointerException();
        }
        this.zzlqj = zzfdh;
    }

    public static zzdpu zzk(zzfdh zzfdh) throws zzfew {
        return (zzdpu) zzfee.zza(zzlqk, zzfdh);
    }

    public final int getVersion() {
        return this.zzlqb;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdpy.zza zza2;
        boolean z = true;
        boolean z2 = false;
        switch (zzdpv.zzbaq[i - 1]) {
            case 1:
                return new zzdpu();
            case 2:
                return zzlqk;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdpu zzdpu = (zzdpu) obj2;
                this.zzlqb = zzfen.zza(this.zzlqb != 0, this.zzlqb, zzdpu.zzlqb != 0, zzdpu.zzlqb);
                this.zzlqi = (zzdpy) zzfen.zza(this.zzlqi, zzdpu.zzlqi);
                boolean z3 = this.zzlqj != zzfdh.zzpal;
                zzfdh zzfdh = this.zzlqj;
                if (zzdpu.zzlqj == zzfdh.zzpal) {
                    z = false;
                }
                this.zzlqj = zzfen.zza(z3, zzfdh, z, zzdpu.zzlqj);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzlqb = zzfdq.zzcub();
                                } else if (zzcts == 18) {
                                    if (this.zzlqi != null) {
                                        zzdpy zzdpy = this.zzlqi;
                                        zzfef zzfef = (zzfef) zzdpy.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdpy);
                                        zza2 = (zzdpy.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlqi = (zzdpy) zzfdq.zza(zzdpy.zzbma(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlqi);
                                        this.zzlqi = (zzdpy) zza2.zzcvj();
                                    }
                                } else if (zzcts == 26) {
                                    this.zzlqj = zzfdq.zzcua();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdpu.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlqk);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlqk;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlqb != 0) {
            zzfdv.zzab(1, this.zzlqb);
        }
        if (this.zzlqi != null) {
            zzfdv.zza(2, this.zzlqi == null ? zzdpy.zzbma() : this.zzlqi);
        }
        if (!this.zzlqj.isEmpty()) {
            zzfdv.zza(3, this.zzlqj);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzdpy zzbls() {
        return this.zzlqi == null ? zzdpy.zzbma() : this.zzlqi;
    }

    public final zzfdh zzblt() {
        return this.zzlqj;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlqb != 0) {
            i2 = 0 + zzfdv.zzae(1, this.zzlqb);
        }
        if (this.zzlqi != null) {
            i2 += zzfdv.zzb(2, this.zzlqi == null ? zzdpy.zzbma() : this.zzlqi);
        }
        if (!this.zzlqj.isEmpty()) {
            i2 += zzfdv.zzb(3, this.zzlqj);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
