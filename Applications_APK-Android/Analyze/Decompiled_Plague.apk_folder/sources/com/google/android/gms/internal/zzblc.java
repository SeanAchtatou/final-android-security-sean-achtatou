package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.MetadataBuffer;

public final class zzblc implements DriveApi.MetadataBufferResult {
    private final Status mStatus;
    private final MetadataBuffer zzgkk;
    private final boolean zzgkl;

    public zzblc(Status status, MetadataBuffer metadataBuffer, boolean z) {
        this.mStatus = status;
        this.zzgkk = metadataBuffer;
        this.zzgkl = z;
    }

    public final MetadataBuffer getMetadataBuffer() {
        return this.zzgkk;
    }

    public final Status getStatus() {
        return this.mStatus;
    }

    public final void release() {
        if (this.zzgkk != null) {
            this.zzgkk.release();
        }
    }
}
