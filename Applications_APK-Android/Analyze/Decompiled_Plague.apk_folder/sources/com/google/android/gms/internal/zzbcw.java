package com.google.android.gms.internal;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzr;

final class zzbcw extends Api.zza<zzbdl, Api.ApiOptions.NoOptions> {
    zzbcw() {
    }

    public final /* synthetic */ Api.zze zza(Context context, Looper looper, zzr zzr, Object obj, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        return new zzbdl(context, looper, zzr, connectionCallbacks, onConnectionFailedListener);
    }
}
