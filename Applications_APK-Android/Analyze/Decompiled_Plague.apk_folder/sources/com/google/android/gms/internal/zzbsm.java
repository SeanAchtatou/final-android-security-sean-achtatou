package com.google.android.gms.internal;

import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.internal.zzb;

public final class zzbsm extends zzb implements SearchableMetadataField<Boolean> {
    public zzbsm(String str, int i) {
        super(str, 4100000);
    }
}
