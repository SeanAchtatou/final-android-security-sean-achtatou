package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.zzama;
import com.tapjoy.TapjoyAuctionFlags;
import java.util.Map;

final class zzm implements zzt<zzama> {
    zzm() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        ((zzama) obj).zzad(TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE.equals(map.get("custom_close")));
    }
}
