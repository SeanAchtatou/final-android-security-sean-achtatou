package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfde extends zzfee<zzfde, zza> implements zzffk {
    private static volatile zzffm<zzfde> zzbas;
    /* access modifiers changed from: private */
    public static final zzfde zzpak;
    private String zzlso = "";
    private zzfdh zzlsp = zzfdh.zzpal;

    public static final class zza extends zzfef<zzfde, zza> implements zzffk {
        private zza() {
            super(zzfde.zzpak);
        }

        /* synthetic */ zza(zzfdf zzfdf) {
            this();
        }
    }

    static {
        zzfde zzfde = new zzfde();
        zzpak = zzfde;
        zzfde.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzfde.zzpbs.zzbim();
    }

    private zzfde() {
    }

    public static zzfde zzctj() {
        return zzpak;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        boolean z;
        boolean z2 = true;
        switch (zzfdf.zzbaq[i - 1]) {
            case 1:
                return new zzfde();
            case 2:
                return zzpak;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzfde zzfde = (zzfde) obj2;
                this.zzlso = zzfen.zza(!this.zzlso.isEmpty(), this.zzlso, !zzfde.zzlso.isEmpty(), zzfde.zzlso);
                boolean z3 = this.zzlsp != zzfdh.zzpal;
                zzfdh zzfdh = this.zzlsp;
                if (zzfde.zzlsp == zzfdh.zzpal) {
                    z2 = false;
                }
                this.zzlsp = zzfen.zza(z3, zzfdh, z2, zzfde.zzlsp);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                if (((zzfea) obj2) != null) {
                    boolean z4 = false;
                    while (!z4) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 10) {
                                    this.zzlso = zzfdq.zzctz();
                                } else if (zzcts != 18) {
                                    if ((zzcts & 7) == 4) {
                                        z = false;
                                    } else {
                                        if (this.zzpbs == zzfgi.zzcwu()) {
                                            this.zzpbs = zzfgi.zzcwv();
                                        }
                                        z = this.zzpbs.zzb(zzcts, zzfdq);
                                    }
                                    if (!z) {
                                    }
                                } else {
                                    this.zzlsp = zzfdq.zzcua();
                                }
                            }
                            z4 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzfde.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzpak);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzpak;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (!this.zzlso.isEmpty()) {
            zzfdv.zzn(1, this.zzlso);
        }
        if (!this.zzlsp.isEmpty()) {
            zzfdv.zza(2, this.zzlsp);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (!this.zzlso.isEmpty()) {
            i2 = 0 + zzfdv.zzo(1, this.zzlso);
        }
        if (!this.zzlsp.isEmpty()) {
            i2 += zzfdv.zzb(2, this.zzlsp);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
