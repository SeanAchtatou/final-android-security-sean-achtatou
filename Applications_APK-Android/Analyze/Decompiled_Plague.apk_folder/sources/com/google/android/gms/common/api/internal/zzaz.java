package com.google.android.gms.common.api.internal;

import com.google.android.gms.internal.zzcwo;

final class zzaz extends zzbm {
    private /* synthetic */ zzar zzfoy;
    private /* synthetic */ zzcwo zzfoz;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzaz(zzay zzay, zzbk zzbk, zzar zzar, zzcwo zzcwo) {
        super(zzbk);
        this.zzfoy = zzar;
        this.zzfoz = zzcwo;
    }

    public final void zzahp() {
        this.zzfoy.zza(this.zzfoz);
    }
}
