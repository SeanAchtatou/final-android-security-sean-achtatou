package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public abstract class zzup extends zzee implements zzuo {
    public zzup() {
        attachInterface(this, "com.google.android.gms.ads.internal.mediation.client.INativeAppInstallAdMapper");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        String str;
        IInterface iInterface;
        boolean z;
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 2:
                str = getHeadline();
                parcel2.writeNoException();
                parcel2.writeString(str);
                return true;
            case 3:
                List images = getImages();
                parcel2.writeNoException();
                parcel2.writeList(images);
                return true;
            case 4:
                str = getBody();
                parcel2.writeNoException();
                parcel2.writeString(str);
                return true;
            case 5:
                iInterface = zzjm();
                parcel2.writeNoException();
                zzef.zza(parcel2, iInterface);
                return true;
            case 6:
                str = getCallToAction();
                parcel2.writeNoException();
                parcel2.writeString(str);
                return true;
            case 7:
                double starRating = getStarRating();
                parcel2.writeNoException();
                parcel2.writeDouble(starRating);
                return true;
            case 8:
                str = getStore();
                parcel2.writeNoException();
                parcel2.writeString(str);
                return true;
            case 9:
                str = getPrice();
                parcel2.writeNoException();
                parcel2.writeString(str);
                return true;
            case 10:
                recordImpression();
                parcel2.writeNoException();
                return true;
            case 11:
                zzh(IObjectWrapper.zza.zzap(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 12:
                zzi(IObjectWrapper.zza.zzap(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 13:
                z = getOverrideImpressionRecording();
                parcel2.writeNoException();
                zzef.zza(parcel2, z);
                return true;
            case 14:
                z = getOverrideClickHandling();
                parcel2.writeNoException();
                zzef.zza(parcel2, z);
                return true;
            case 15:
                Bundle extras = getExtras();
                parcel2.writeNoException();
                zzef.zzb(parcel2, extras);
                return true;
            case 16:
                zzj(IObjectWrapper.zza.zzap(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 17:
                iInterface = getVideoController();
                parcel2.writeNoException();
                zzef.zza(parcel2, iInterface);
                return true;
            case 18:
                iInterface = zzme();
                parcel2.writeNoException();
                zzef.zza(parcel2, iInterface);
                return true;
            case 19:
                iInterface = zzjs();
                parcel2.writeNoException();
                zzef.zza(parcel2, iInterface);
                return true;
            case 20:
                iInterface = zzmf();
                parcel2.writeNoException();
                zzef.zza(parcel2, iInterface);
                return true;
            case 21:
                iInterface = zzjr();
                parcel2.writeNoException();
                zzef.zza(parcel2, iInterface);
                return true;
            default:
                return false;
        }
    }
}
