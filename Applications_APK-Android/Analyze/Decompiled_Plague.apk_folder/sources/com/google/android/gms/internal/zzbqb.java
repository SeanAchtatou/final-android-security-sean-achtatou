package com.google.android.gms.internal;

import android.os.Parcelable;

public final class zzbqb implements Parcelable.Creator<zzbqa> {
    /* JADX WARN: Type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r1v5, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r1v6, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r1v7, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r1v8, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r12) {
        /*
            r11 = this;
            int r0 = com.google.android.gms.internal.zzbek.zzd(r12)
            r1 = 0
            r2 = 0
            r5 = r1
            r6 = r5
            r7 = r6
            r8 = r7
            r9 = r8
            r10 = r9
            r4 = r2
        L_0x000d:
            int r1 = r12.dataPosition()
            if (r1 >= r0) goto L_0x0063
            int r1 = r12.readInt()
            r2 = 65535(0xffff, float:9.1834E-41)
            r2 = r2 & r1
            switch(r2) {
                case 2: goto L_0x005e;
                case 3: goto L_0x0054;
                case 4: goto L_0x001e;
                case 5: goto L_0x004a;
                case 6: goto L_0x0040;
                case 7: goto L_0x0036;
                case 8: goto L_0x001e;
                case 9: goto L_0x002c;
                case 10: goto L_0x0022;
                default: goto L_0x001e;
            }
        L_0x001e:
            com.google.android.gms.internal.zzbek.zzb(r12, r1)
            goto L_0x000d
        L_0x0022:
            android.os.Parcelable$Creator<com.google.android.gms.drive.events.zzn> r2 = com.google.android.gms.drive.events.zzn.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r12, r1, r2)
            r10 = r1
            com.google.android.gms.drive.events.zzn r10 = (com.google.android.gms.drive.events.zzn) r10
            goto L_0x000d
        L_0x002c:
            android.os.Parcelable$Creator<com.google.android.gms.drive.events.zzr> r2 = com.google.android.gms.drive.events.zzr.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r12, r1, r2)
            r9 = r1
            com.google.android.gms.drive.events.zzr r9 = (com.google.android.gms.drive.events.zzr) r9
            goto L_0x000d
        L_0x0036:
            android.os.Parcelable$Creator<com.google.android.gms.drive.events.zzb> r2 = com.google.android.gms.drive.events.zzb.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r12, r1, r2)
            r8 = r1
            com.google.android.gms.drive.events.zzb r8 = (com.google.android.gms.drive.events.zzb) r8
            goto L_0x000d
        L_0x0040:
            android.os.Parcelable$Creator<com.google.android.gms.drive.events.zzl> r2 = com.google.android.gms.drive.events.zzl.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r12, r1, r2)
            r7 = r1
            com.google.android.gms.drive.events.zzl r7 = (com.google.android.gms.drive.events.zzl) r7
            goto L_0x000d
        L_0x004a:
            android.os.Parcelable$Creator<com.google.android.gms.drive.events.CompletionEvent> r2 = com.google.android.gms.drive.events.CompletionEvent.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r12, r1, r2)
            r6 = r1
            com.google.android.gms.drive.events.CompletionEvent r6 = (com.google.android.gms.drive.events.CompletionEvent) r6
            goto L_0x000d
        L_0x0054:
            android.os.Parcelable$Creator<com.google.android.gms.drive.events.ChangeEvent> r2 = com.google.android.gms.drive.events.ChangeEvent.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r12, r1, r2)
            r5 = r1
            com.google.android.gms.drive.events.ChangeEvent r5 = (com.google.android.gms.drive.events.ChangeEvent) r5
            goto L_0x000d
        L_0x005e:
            int r4 = com.google.android.gms.internal.zzbek.zzg(r12, r1)
            goto L_0x000d
        L_0x0063:
            com.google.android.gms.internal.zzbek.zzaf(r12, r0)
            com.google.android.gms.internal.zzbqa r12 = new com.google.android.gms.internal.zzbqa
            r3 = r12
            r3.<init>(r4, r5, r6, r7, r8, r9, r10)
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzbqb.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbqa[i];
    }
}
