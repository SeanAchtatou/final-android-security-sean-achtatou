package com.google.android.gms.internal;

final /* synthetic */ class zzdsj {
    static final /* synthetic */ int[] zzluo = new int[zzdsl.values().length];
    static final /* synthetic */ int[] zzlup = new int[zzdsk.values().length];

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x001f */
    static {
        /*
            com.google.android.gms.internal.zzdsk[] r0 = com.google.android.gms.internal.zzdsk.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.google.android.gms.internal.zzdsj.zzlup = r0
            r0 = 1
            int[] r1 = com.google.android.gms.internal.zzdsj.zzlup     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.google.android.gms.internal.zzdsk r2 = com.google.android.gms.internal.zzdsk.NIST_P256     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            r1 = 2
            int[] r2 = com.google.android.gms.internal.zzdsj.zzlup     // Catch:{ NoSuchFieldError -> 0x001f }
            com.google.android.gms.internal.zzdsk r3 = com.google.android.gms.internal.zzdsk.NIST_P384     // Catch:{ NoSuchFieldError -> 0x001f }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            int[] r2 = com.google.android.gms.internal.zzdsj.zzlup     // Catch:{ NoSuchFieldError -> 0x002a }
            com.google.android.gms.internal.zzdsk r3 = com.google.android.gms.internal.zzdsk.NIST_P521     // Catch:{ NoSuchFieldError -> 0x002a }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r4 = 3
            r2[r3] = r4     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            com.google.android.gms.internal.zzdsl[] r2 = com.google.android.gms.internal.zzdsl.values()
            int r2 = r2.length
            int[] r2 = new int[r2]
            com.google.android.gms.internal.zzdsj.zzluo = r2
            int[] r2 = com.google.android.gms.internal.zzdsj.zzluo     // Catch:{ NoSuchFieldError -> 0x003d }
            com.google.android.gms.internal.zzdsl r3 = com.google.android.gms.internal.zzdsl.UNCOMPRESSED     // Catch:{ NoSuchFieldError -> 0x003d }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x003d }
            r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x003d }
        L_0x003d:
            int[] r0 = com.google.android.gms.internal.zzdsj.zzluo     // Catch:{ NoSuchFieldError -> 0x0047 }
            com.google.android.gms.internal.zzdsl r2 = com.google.android.gms.internal.zzdsl.COMPRESSED     // Catch:{ NoSuchFieldError -> 0x0047 }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
            r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x0047 }
        L_0x0047:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdsj.<clinit>():void");
    }
}
