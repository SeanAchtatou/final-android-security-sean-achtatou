package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.internal.api.zzp;
import com.google.android.gms.games.internal.zzg;
import com.google.android.gms.games.internal.zzn;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotContents;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.SnapshotMetadataBuffer;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.Snapshots;
import com.google.android.gms.tasks.Task;

public class SnapshotsClient extends zzp {
    public static final int DISPLAY_LIMIT_NONE = -1;
    public static final String EXTRA_SNAPSHOT_METADATA = "com.google.android.gms.games.SNAPSHOT_METADATA";
    public static final String EXTRA_SNAPSHOT_NEW = "com.google.android.gms.games.SNAPSHOT_NEW";
    public static final int RESOLUTION_POLICY_HIGHEST_PROGRESS = 4;
    public static final int RESOLUTION_POLICY_LAST_KNOWN_GOOD = 2;
    public static final int RESOLUTION_POLICY_LONGEST_PLAYTIME = 1;
    public static final int RESOLUTION_POLICY_MANUAL = -1;
    public static final int RESOLUTION_POLICY_MOST_RECENTLY_MODIFIED = 3;
    private static final zzn<Snapshots.OpenSnapshotResult> zzhks = new zzbx();
    private static final zzbo<Snapshots.DeleteSnapshotResult, String> zzhkt = new zzby();
    private static final zzbo<Snapshots.CommitSnapshotResult, SnapshotMetadata> zzhku = new zzbz();
    private static final zzbo<Snapshots.OpenSnapshotResult, Snapshots.OpenSnapshotResult> zzhkv = new zzca();
    private static final com.google.android.gms.games.internal.zzp zzhkw = new zzcb();
    private static final zzbo<Snapshots.OpenSnapshotResult, DataOrConflict<Snapshot>> zzhkx = new zzbs();
    private static final zzbo<Snapshots.LoadSnapshotsResult, SnapshotMetadataBuffer> zzhky = new zzbt();

    public static class DataOrConflict<T> {
        private final T data;
        private final SnapshotConflict zzhle;

        DataOrConflict(@Nullable T t, @Nullable SnapshotConflict snapshotConflict) {
            this.data = t;
            this.zzhle = snapshotConflict;
        }

        @Nullable
        public SnapshotConflict getConflict() {
            if (isConflict()) {
                return this.zzhle;
            }
            throw new IllegalStateException("getConflict called when there is no conflict.");
        }

        @Nullable
        public T getData() {
            if (!isConflict()) {
                return this.data;
            }
            throw new IllegalStateException("getData called when there is a conflict.");
        }

        public boolean isConflict() {
            return this.zzhle != null;
        }
    }

    public static class SnapshotConflict {
        private final Snapshot zzhlf;
        private final String zzhlg;
        private final Snapshot zzhlh;
        private final SnapshotContents zzhli;

        SnapshotConflict(@NonNull Snapshot snapshot, @NonNull String str, @NonNull Snapshot snapshot2, @NonNull SnapshotContents snapshotContents) {
            this.zzhlf = snapshot;
            this.zzhlg = str;
            this.zzhlh = snapshot2;
            this.zzhli = snapshotContents;
        }

        public String getConflictId() {
            return this.zzhlg;
        }

        public Snapshot getConflictingSnapshot() {
            return this.zzhlh;
        }

        public SnapshotContents getResolutionSnapshotContents() {
            return this.zzhli;
        }

        public Snapshot getSnapshot() {
            return this.zzhlf;
        }
    }

    public static class SnapshotContentUnavailableApiException extends ApiException {
        protected final SnapshotMetadata metadata;

        SnapshotContentUnavailableApiException(@NonNull Status status, @NonNull SnapshotMetadata snapshotMetadata) {
            super(status);
            this.metadata = snapshotMetadata;
        }

        public SnapshotMetadata getSnapshotMetadata() {
            return this.metadata;
        }
    }

    SnapshotsClient(@NonNull Activity activity, @NonNull Games.GamesOptions gamesOptions) {
        super(activity, gamesOptions);
    }

    SnapshotsClient(@NonNull Context context, @NonNull Games.GamesOptions gamesOptions) {
        super(context, gamesOptions);
    }

    @Nullable
    public static SnapshotMetadata getSnapshotFromBundle(@NonNull Bundle bundle) {
        return Games.Snapshots.getSnapshotFromBundle(bundle);
    }

    private static Task<DataOrConflict<Snapshot>> zze(@NonNull PendingResult<Snapshots.OpenSnapshotResult> pendingResult) {
        return zzg.zza(pendingResult, zzhkw, zzhkx, zzhkv, zzhks);
    }

    public Task<SnapshotMetadata> commitAndClose(@NonNull Snapshot snapshot, @NonNull SnapshotMetadataChange snapshotMetadataChange) {
        return zzg.zza(Games.Snapshots.commitAndClose(zzagb(), snapshot, snapshotMetadataChange), zzhku);
    }

    public Task<String> delete(@NonNull SnapshotMetadata snapshotMetadata) {
        return zzg.zza(Games.Snapshots.delete(zzagb(), snapshotMetadata), zzhkt);
    }

    public Task<Void> discardAndClose(@NonNull Snapshot snapshot) {
        return zzb(new zzbw(this, snapshot));
    }

    public Task<Integer> getMaxCoverImageSize() {
        return zza(new zzbu(this));
    }

    public Task<Integer> getMaxDataSize() {
        return zza(new zzbr(this));
    }

    public Task<Intent> getSelectSnapshotIntent(@NonNull String str, boolean z, boolean z2, int i) {
        return zza(new zzbv(this, str, z, z2, i));
    }

    public Task<AnnotatedData<SnapshotMetadataBuffer>> load(boolean z) {
        return zzg.zzc(Games.Snapshots.load(zzagb(), z), zzhky);
    }

    public Task<DataOrConflict<Snapshot>> open(@NonNull SnapshotMetadata snapshotMetadata) {
        return zze(Games.Snapshots.open(zzagb(), snapshotMetadata));
    }

    public Task<DataOrConflict<Snapshot>> open(@NonNull SnapshotMetadata snapshotMetadata, int i) {
        return zze(Games.Snapshots.open(zzagb(), snapshotMetadata, i));
    }

    public Task<DataOrConflict<Snapshot>> open(@NonNull String str, boolean z) {
        return zze(Games.Snapshots.open(zzagb(), str, z));
    }

    public Task<DataOrConflict<Snapshot>> open(@NonNull String str, boolean z, int i) {
        return zze(Games.Snapshots.open(zzagb(), str, z, i));
    }

    public Task<DataOrConflict<Snapshot>> resolveConflict(@NonNull String str, @NonNull Snapshot snapshot) {
        return zze(Games.Snapshots.resolveConflict(zzagb(), str, snapshot));
    }

    public Task<DataOrConflict<Snapshot>> resolveConflict(@NonNull String str, @NonNull String str2, @NonNull SnapshotMetadataChange snapshotMetadataChange, @NonNull SnapshotContents snapshotContents) {
        return zze(Games.Snapshots.resolveConflict(zzagb(), str, str2, snapshotMetadataChange, snapshotContents));
    }
}
