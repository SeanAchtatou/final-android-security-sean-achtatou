package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzbdp extends IInterface {
    void zza(zzbdn zzbdn, zzbde zzbde) throws RemoteException;
}
