package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DrivePreferencesApi;

abstract class zzbmt extends zzblj<DrivePreferencesApi.FileUploadPreferencesResult> {
    private /* synthetic */ zzbmo zzglx;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzbmt(zzbmo zzbmo, GoogleApiClient googleApiClient) {
        super(googleApiClient);
        this.zzglx = zzbmo;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Result zzb(Status status) {
        return new zzbms(this.zzglx, status, null, null);
    }
}
