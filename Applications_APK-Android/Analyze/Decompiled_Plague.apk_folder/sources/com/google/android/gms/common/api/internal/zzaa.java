package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;

final class zzaa implements zzcg {
    private /* synthetic */ zzy zzfna;

    private zzaa(zzy zzy) {
        this.zzfna = zzy;
    }

    /* synthetic */ zzaa(zzy zzy, zzz zzz) {
        this(zzy);
    }

    public final void zzc(@NonNull ConnectionResult connectionResult) {
        this.zzfna.zzfmy.lock();
        try {
            ConnectionResult unused = this.zzfna.zzfmv = connectionResult;
            this.zzfna.zzagz();
        } finally {
            this.zzfna.zzfmy.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.zzy.zza(com.google.android.gms.common.api.internal.zzy, boolean):boolean
     arg types: [com.google.android.gms.common.api.internal.zzy, int]
     candidates:
      com.google.android.gms.common.api.internal.zzy.zza(com.google.android.gms.common.api.internal.zzy, com.google.android.gms.common.ConnectionResult):com.google.android.gms.common.ConnectionResult
      com.google.android.gms.common.api.internal.zzy.zza(com.google.android.gms.common.api.internal.zzy, android.os.Bundle):void
      com.google.android.gms.common.api.internal.zzy.zza(com.google.android.gms.common.api.internal.zzy, boolean):boolean */
    public final void zzf(int i, boolean z) {
        this.zzfna.zzfmy.lock();
        try {
            if (!this.zzfna.zzfmx && this.zzfna.zzfmw != null) {
                if (this.zzfna.zzfmw.isSuccess()) {
                    boolean unused = this.zzfna.zzfmx = true;
                    this.zzfna.zzfmq.onConnectionSuspended(i);
                }
            }
            boolean unused2 = this.zzfna.zzfmx = false;
            this.zzfna.zze(i, z);
        } finally {
            this.zzfna.zzfmy.unlock();
        }
    }

    public final void zzj(@Nullable Bundle bundle) {
        this.zzfna.zzfmy.lock();
        try {
            this.zzfna.zzi(bundle);
            ConnectionResult unused = this.zzfna.zzfmv = ConnectionResult.zzfhy;
            this.zzfna.zzagz();
        } finally {
            this.zzfna.zzfmy.unlock();
        }
    }
}
