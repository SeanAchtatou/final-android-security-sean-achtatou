package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzbpj extends IInterface {
    void zzc(zzbqa zzbqa) throws RemoteException;
}
