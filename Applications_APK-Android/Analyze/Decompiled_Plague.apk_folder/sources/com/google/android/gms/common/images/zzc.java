package com.google.android.gms.common.images;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.internal.zzbec;
import com.google.android.gms.internal.zzbeh;
import java.lang.ref.WeakReference;

public final class zzc extends zza {
    private WeakReference<ImageView> zzfus;

    public zzc(ImageView imageView, int i) {
        super(null, i);
        com.google.android.gms.common.internal.zzc.zzu(imageView);
        this.zzfus = new WeakReference<>(imageView);
    }

    public zzc(ImageView imageView, Uri uri) {
        super(uri, 0);
        com.google.android.gms.common.internal.zzc.zzu(imageView);
        this.zzfus = new WeakReference<>(imageView);
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzc)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        ImageView imageView = this.zzfus.get();
        ImageView imageView2 = ((zzc) obj).zzfus.get();
        return (imageView2 == null || imageView == null || !zzbg.equal(imageView2, imageView)) ? false : true;
    }

    public final int hashCode() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public final void zza(Drawable drawable, boolean z, boolean z2, boolean z3) {
        ImageView imageView = this.zzfus.get();
        if (imageView != null) {
            int i = 0;
            boolean z4 = !z2 && !z3;
            if (z4 && (imageView instanceof zzbeh)) {
                int zzaju = zzbeh.zzaju();
                if (this.zzfun != 0 && zzaju == this.zzfun) {
                    return;
                }
            }
            boolean zzc = zzc(z, z2);
            Uri uri = null;
            if (zzc) {
                Drawable drawable2 = imageView.getDrawable();
                if (drawable2 == null) {
                    drawable2 = null;
                } else if (drawable2 instanceof zzbec) {
                    drawable2 = ((zzbec) drawable2).zzajs();
                }
                drawable = new zzbec(drawable2, drawable);
            }
            imageView.setImageDrawable(drawable);
            if (imageView instanceof zzbeh) {
                if (z3) {
                    uri = this.zzful.uri;
                }
                zzbeh.zzn(uri);
                if (z4) {
                    i = this.zzfun;
                }
                zzbeh.zzcd(i);
            }
            if (zzc) {
                ((zzbec) drawable).startTransition(250);
            }
        }
    }
}
