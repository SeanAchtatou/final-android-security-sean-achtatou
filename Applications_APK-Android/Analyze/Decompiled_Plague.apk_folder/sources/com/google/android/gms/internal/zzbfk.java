package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzb;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.common.util.zzp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class zzbfk {
    protected static <O, I> I zza(zzbfl<I, O> zzbfl, Object obj) {
        return zzbfl.zzfzt != null ? zzbfl.convertBack(obj) : obj;
    }

    private static void zza(StringBuilder sb, zzbfl zzbfl, Object obj) {
        String str;
        if (zzbfl.zzfzk == 11) {
            str = ((zzbfk) zzbfl.zzfzq.cast(obj)).toString();
        } else if (zzbfl.zzfzk == 7) {
            sb.append("\"");
            sb.append(zzo.zzgm((String) obj));
            str = "\"";
        } else {
            sb.append(obj);
            return;
        }
        sb.append(str);
    }

    private static void zza(StringBuilder sb, zzbfl zzbfl, ArrayList<Object> arrayList) {
        sb.append("[");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append(",");
            }
            Object obj = arrayList.get(i);
            if (obj != null) {
                zza(sb, zzbfl, obj);
            }
        }
        sb.append("]");
    }

    public String toString() {
        String str;
        String str2;
        Map<String, zzbfl<?, ?>> zzaaj = zzaaj();
        StringBuilder sb = new StringBuilder(100);
        for (String next : zzaaj.keySet()) {
            zzbfl zzbfl = zzaaj.get(next);
            if (zza(zzbfl)) {
                Object zza = zza(zzbfl, zzb(zzbfl));
                sb.append(sb.length() == 0 ? "{" : ",");
                sb.append("\"");
                sb.append(next);
                sb.append("\":");
                if (zza != null) {
                    switch (zzbfl.zzfzm) {
                        case 8:
                            sb.append("\"");
                            str = zzb.zzj((byte[]) zza);
                            sb.append(str);
                            str2 = "\"";
                            break;
                        case 9:
                            sb.append("\"");
                            str = zzb.zzk((byte[]) zza);
                            sb.append(str);
                            str2 = "\"";
                            break;
                        case 10:
                            zzp.zza(sb, (HashMap) zza);
                            break;
                        default:
                            if (!zzbfl.zzfzl) {
                                zza(sb, zzbfl, zza);
                                break;
                            } else {
                                zza(sb, zzbfl, (ArrayList<Object>) ((ArrayList) zza));
                                break;
                            }
                    }
                } else {
                    str2 = "null";
                }
                sb.append(str2);
            }
        }
        sb.append(sb.length() > 0 ? "}" : "{}");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public boolean zza(zzbfl zzbfl) {
        if (zzbfl.zzfzm != 11) {
            return zzgk(zzbfl.zzfzo);
        }
        if (zzbfl.zzfzn) {
            String str = zzbfl.zzfzo;
            throw new UnsupportedOperationException("Concrete type arrays not supported");
        }
        String str2 = zzbfl.zzfzo;
        throw new UnsupportedOperationException("Concrete types not supported");
    }

    public abstract Map<String, zzbfl<?, ?>> zzaaj();

    /* access modifiers changed from: protected */
    public Object zzb(zzbfl zzbfl) {
        String str = zzbfl.zzfzo;
        if (zzbfl.zzfzq == null) {
            return zzgj(zzbfl.zzfzo);
        }
        zzgj(zzbfl.zzfzo);
        zzbq.zza(true, "Concrete field shouldn't be value object: %s", zzbfl.zzfzo);
        boolean z = zzbfl.zzfzn;
        try {
            char upperCase = Character.toUpperCase(str.charAt(0));
            String substring = str.substring(1);
            StringBuilder sb = new StringBuilder(4 + String.valueOf(substring).length());
            sb.append("get");
            sb.append(upperCase);
            sb.append(substring);
            return getClass().getMethod(sb.toString(), new Class[0]).invoke(this, new Object[0]);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: protected */
    public abstract Object zzgj(String str);

    /* access modifiers changed from: protected */
    public abstract boolean zzgk(String str);
}
