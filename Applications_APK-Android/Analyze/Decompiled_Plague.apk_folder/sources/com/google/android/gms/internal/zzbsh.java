package com.google.android.gms.internal;

import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;

public final class zzbsh extends zzbsp implements SearchableMetadataField<AppVisibleCustomProperties> {
    public zzbsh(int i) {
        super(5000000);
    }
}
