package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DrivePreferencesApi;
import com.google.android.gms.drive.FileUploadPreferences;

final class zzbms implements DrivePreferencesApi.FileUploadPreferencesResult {
    private final Status mStatus;
    private final FileUploadPreferences zzgly;

    private zzbms(zzbmo zzbmo, Status status, FileUploadPreferences fileUploadPreferences) {
        this.mStatus = status;
        this.zzgly = fileUploadPreferences;
    }

    /* synthetic */ zzbms(zzbmo zzbmo, Status status, FileUploadPreferences fileUploadPreferences, zzbmp zzbmp) {
        this(zzbmo, status, fileUploadPreferences);
    }

    public final FileUploadPreferences getFileUploadPreferences() {
        return this.zzgly;
    }

    public final Status getStatus() {
        return this.mStatus;
    }
}
