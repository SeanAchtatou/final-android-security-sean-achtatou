package com.google.android.gms.internal;

public final class zzdsp {
    private final zzdta zzlve;
    private final zzdta zzlvf;

    public zzdsp(byte[] bArr, byte[] bArr2) {
        this.zzlve = zzdta.zzak(bArr);
        this.zzlvf = zzdta.zzak(bArr2);
    }

    public final byte[] zzbot() {
        if (this.zzlve == null) {
            return null;
        }
        return this.zzlve.getBytes();
    }

    public final byte[] zzbou() {
        if (this.zzlvf == null) {
            return null;
        }
        return this.zzlvf.getBytes();
    }
}
