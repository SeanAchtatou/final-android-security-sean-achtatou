package com.google.android.gms.internal;

import java.security.GeneralSecurityException;

public final class zzdpp {
    public static void zzbli() throws GeneralSecurityException {
        zzdpn zzdpn = new zzdpn();
        zzdpa.zzlpq.zza(zzdpn.getKeyType(), zzdpn);
    }
}
