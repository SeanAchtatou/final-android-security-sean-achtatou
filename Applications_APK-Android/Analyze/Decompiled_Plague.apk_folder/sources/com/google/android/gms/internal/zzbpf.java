package com.google.android.gms.internal;

import android.content.IntentSender;
import android.os.IInterface;
import android.os.RemoteException;

public interface zzbpf extends IInterface {
    IntentSender zza(zzbke zzbke) throws RemoteException;

    IntentSender zza(zzbqx zzbqx) throws RemoteException;

    zzbot zza(zzbqu zzbqu, zzbph zzbph) throws RemoteException;

    void zza(zzbjt zzbjt, zzbpj zzbpj, String str, zzbph zzbph) throws RemoteException;

    void zza(zzbjw zzbjw, zzbph zzbph) throws RemoteException;

    void zza(zzbjy zzbjy, zzbph zzbph) throws RemoteException;

    void zza(zzbkb zzbkb, zzbph zzbph) throws RemoteException;

    void zza(zzbkg zzbkg, zzbph zzbph) throws RemoteException;

    void zza(zzbki zzbki, zzbph zzbph) throws RemoteException;

    void zza(zzbkl zzbkl, zzbph zzbph) throws RemoteException;

    void zza(zzbkn zzbkn) throws RemoteException;

    void zza(zzbpb zzbpb, zzbph zzbph) throws RemoteException;

    void zza(zzbph zzbph) throws RemoteException;

    void zza(zzbpo zzbpo, zzbph zzbph) throws RemoteException;

    void zza(zzbrb zzbrb, zzbph zzbph) throws RemoteException;

    void zza(zzbrd zzbrd, zzbpj zzbpj, String str, zzbph zzbph) throws RemoteException;

    void zza(zzbrf zzbrf, zzbph zzbph) throws RemoteException;

    void zza(zzbrh zzbrh, zzbph zzbph) throws RemoteException;

    void zza(zzbrm zzbrm, zzbph zzbph) throws RemoteException;

    void zza(zzbro zzbro, zzbph zzbph) throws RemoteException;

    void zza(zzbrq zzbrq, zzbph zzbph) throws RemoteException;

    void zzb(zzbph zzbph) throws RemoteException;
}
