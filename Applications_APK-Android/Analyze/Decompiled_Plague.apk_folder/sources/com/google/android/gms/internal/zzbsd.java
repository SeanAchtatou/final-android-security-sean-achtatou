package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbsd extends zzfhe<zzbsd> {
    public long zzgox = -1;
    public long zzgpa = -1;

    public zzbsd() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzbsd)) {
            return false;
        }
        zzbsd zzbsd = (zzbsd) obj;
        if (this.zzgpa == zzbsd.zzgpa && this.zzgox == zzbsd.zzgox) {
            return (this.zzpgy == null || this.zzpgy.isEmpty()) ? zzbsd.zzpgy == null || zzbsd.zzpgy.isEmpty() : this.zzpgy.equals(zzbsd.zzpgy);
        }
        return false;
    }

    public final int hashCode() {
        return ((((((527 + getClass().getName().hashCode()) * 31) + ((int) (this.zzgpa ^ (this.zzgpa >>> 32)))) * 31) + ((int) (this.zzgox ^ (this.zzgox >>> 32)))) * 31) + ((this.zzpgy == null || this.zzpgy.isEmpty()) ? 0 : this.zzpgy.hashCode());
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 8) {
                long zzcum = zzfhb.zzcum();
                this.zzgpa = (zzcum >>> 1) ^ (-(zzcum & 1));
            } else if (zzcts == 16) {
                long zzcum2 = zzfhb.zzcum();
                this.zzgox = (zzcum2 >>> 1) ^ (-(zzcum2 & 1));
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        zzfhc.zzg(1, this.zzgpa);
        zzfhc.zzg(2, this.zzgox);
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        return super.zzo() + zzfhc.zzh(1, this.zzgpa) + zzfhc.zzh(2, this.zzgox);
    }
}
