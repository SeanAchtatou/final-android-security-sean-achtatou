package com.google.android.gms.internal;

import com.google.android.gms.common.api.PendingResults;
import com.google.android.gms.common.api.Status;
import java.util.ArrayList;
import java.util.TimeZone;

public final class zzbcx {
    private String zzfgj;
    private int zzfgk;
    private String zzfgl;
    private String zzfgm;
    private int zzfgo;
    private final zzbcz zzfgt;
    private ArrayList<Integer> zzfgu;
    private ArrayList<String> zzfgv;
    private ArrayList<Integer> zzfgw;
    private ArrayList<zzcsv> zzfgx;
    private ArrayList<byte[]> zzfgy;
    private boolean zzfgz;
    private final zzfii zzfha;
    private boolean zzfhb;
    private /* synthetic */ zzbcv zzfhc;

    private zzbcx(zzbcv zzbcv, byte[] bArr) {
        this(zzbcv, bArr, (zzbcz) null);
    }

    /* synthetic */ zzbcx(zzbcv zzbcv, byte[] bArr, zzbcw zzbcw) {
        this(zzbcv, bArr);
    }

    private zzbcx(zzbcv zzbcv, byte[] bArr, zzbcz zzbcz) {
        this.zzfhc = zzbcv;
        this.zzfgk = this.zzfhc.zzfgk;
        this.zzfgj = this.zzfhc.zzfgj;
        zzbcv zzbcv2 = this.zzfhc;
        this.zzfgl = null;
        zzbcv zzbcv3 = this.zzfhc;
        this.zzfgm = null;
        this.zzfgo = 0;
        this.zzfgu = null;
        this.zzfgv = null;
        this.zzfgw = null;
        this.zzfgx = null;
        this.zzfgy = null;
        this.zzfgz = true;
        this.zzfha = new zzfii();
        this.zzfhb = false;
        this.zzfgl = null;
        this.zzfgm = null;
        this.zzfha.zzpkg = zzbcv.zzfgq.currentTimeMillis();
        this.zzfha.zzpkh = zzbcv.zzfgq.elapsedRealtime();
        zzfii zzfii = this.zzfha;
        zzbda unused = zzbcv.zzfgr;
        zzfii.zzpks = (long) (TimeZone.getDefault().getOffset(this.zzfha.zzpkg) / 1000);
        if (bArr != null) {
            this.zzfha.zzpkn = bArr;
        }
        this.zzfgt = null;
    }

    public final void zzbf() {
        if (this.zzfhb) {
            throw new IllegalStateException("do not reuse LogEventBuilder");
        }
        this.zzfhb = true;
        zzbde zzbde = new zzbde(new zzbdt(this.zzfhc.packageName, this.zzfhc.zzfgi, this.zzfgk, this.zzfgj, this.zzfgl, this.zzfgm, this.zzfhc.zzfgn, 0), this.zzfha, null, null, zzbcv.zzb((ArrayList<Integer>) null), null, zzbcv.zzb((ArrayList<Integer>) null), null, null, this.zzfgz);
        zzbdt zzbdt = zzbde.zzfhf;
        if (this.zzfhc.zzfgs.zzg(zzbdt.zzfgj, zzbdt.zzfgk)) {
            this.zzfhc.zzfgp.zza(zzbde);
        } else {
            PendingResults.immediatePendingResult(Status.zzfko);
        }
    }
}
