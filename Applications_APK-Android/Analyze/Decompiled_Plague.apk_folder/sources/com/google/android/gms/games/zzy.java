package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.zzq;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzy extends zzq<OnInvitationReceivedListener> {
    private /* synthetic */ zzcl zzhik;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzy(InvitationsClient invitationsClient, zzcl zzcl, zzcl zzcl2) {
        super(zzcl);
        this.zzhik = zzcl2;
    }

    /* access modifiers changed from: protected */
    public final void zzb(GamesClientImpl gamesClientImpl, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException, SecurityException {
        gamesClientImpl.zze(this.zzhik);
        taskCompletionSource.setResult(null);
    }
}
