package com.google.android.gms.internal;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.drive.DriveApi;

final /* synthetic */ class zzblr implements zzbo {
    static final zzbo zzglc = new zzblr();

    private zzblr() {
    }

    public final Object zzb(Result result) {
        return ((DriveApi.DriveIdResult) result).getDriveId();
    }
}
