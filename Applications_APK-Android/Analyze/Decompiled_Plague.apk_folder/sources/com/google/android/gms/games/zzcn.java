package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.zzq;
import com.google.android.gms.games.multiplayer.turnbased.OnTurnBasedMatchUpdateReceivedListener;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzcn extends zzq<OnTurnBasedMatchUpdateReceivedListener> {
    private /* synthetic */ zzcl zzhik;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcn(TurnBasedMultiplayerClient turnBasedMultiplayerClient, zzcl zzcl, zzcl zzcl2) {
        super(zzcl);
        this.zzhik = zzcl2;
    }

    /* access modifiers changed from: protected */
    public final void zzb(GamesClientImpl gamesClientImpl, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException, SecurityException {
        gamesClientImpl.zzg(this.zzhik);
        taskCompletionSource.setResult(null);
    }
}
