package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzbz;
import com.google.android.gms.internal.zzcwb;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public final class zzbr<O extends Api.ApiOptions> implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, zzx {
    private final zzh<O> zzfjl;
    /* access modifiers changed from: private */
    public final Api.zze zzfnb;
    private boolean zzfpc;
    final /* synthetic */ zzbp zzfqo;
    private final Queue<zza> zzfqp = new LinkedList();
    private final Api.zzb zzfqq;
    private final zzah zzfqr;
    private final Set<zzj> zzfqs = new HashSet();
    private final Map<zzcn<?>, zzcu> zzfqt = new HashMap();
    private final int zzfqu;
    private final zzcy zzfqv;
    private ConnectionResult zzfqw = null;

    @WorkerThread
    public zzbr(zzbp zzbp, GoogleApi<O> googleApi) {
        this.zzfqo = zzbp;
        this.zzfnb = googleApi.zza(zzbp.mHandler.getLooper(), this);
        this.zzfqq = this.zzfnb instanceof zzbz ? zzbz.zzalg() : this.zzfnb;
        this.zzfjl = googleApi.zzaga();
        this.zzfqr = new zzah();
        this.zzfqu = googleApi.getInstanceId();
        if (this.zzfnb.zzaam()) {
            this.zzfqv = googleApi.zza(zzbp.mContext, zzbp.mHandler);
        } else {
            this.zzfqv = null;
        }
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public final void zzaik() {
        zzain();
        zzi(ConnectionResult.zzfhy);
        zzaip();
        for (zzcu zzcu : this.zzfqt.values()) {
            try {
                zzcu.zzfkw.zzb(this.zzfqq, new TaskCompletionSource());
            } catch (DeadObjectException unused) {
                onConnectionSuspended(1);
                this.zzfnb.disconnect();
            } catch (RemoteException unused2) {
            }
        }
        while (this.zzfnb.isConnected() && !this.zzfqp.isEmpty()) {
            zzb(this.zzfqp.remove());
        }
        zzaiq();
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public final void zzail() {
        zzain();
        this.zzfpc = true;
        this.zzfqr.zzahk();
        this.zzfqo.mHandler.sendMessageDelayed(Message.obtain(this.zzfqo.mHandler, 9, this.zzfjl), this.zzfqo.zzfpe);
        this.zzfqo.mHandler.sendMessageDelayed(Message.obtain(this.zzfqo.mHandler, 11, this.zzfjl), this.zzfqo.zzfpd);
        int unused = this.zzfqo.zzfqi = -1;
    }

    @WorkerThread
    private final void zzaip() {
        if (this.zzfpc) {
            this.zzfqo.mHandler.removeMessages(11, this.zzfjl);
            this.zzfqo.mHandler.removeMessages(9, this.zzfjl);
            this.zzfpc = false;
        }
    }

    private final void zzaiq() {
        this.zzfqo.mHandler.removeMessages(12, this.zzfjl);
        this.zzfqo.mHandler.sendMessageDelayed(this.zzfqo.mHandler.obtainMessage(12, this.zzfjl), this.zzfqo.zzfqg);
    }

    @WorkerThread
    private final void zzb(zza zza) {
        zza.zza(this.zzfqr, zzaam());
        try {
            zza.zza(this);
        } catch (DeadObjectException unused) {
            onConnectionSuspended(1);
            this.zzfnb.disconnect();
        }
    }

    @WorkerThread
    private final void zzi(ConnectionResult connectionResult) {
        for (zzj zza : this.zzfqs) {
            zza.zza(this.zzfjl, connectionResult);
        }
        this.zzfqs.clear();
    }

    @WorkerThread
    public final void connect() {
        zzbq.zza(this.zzfqo.mHandler);
        if (!this.zzfnb.isConnected() && !this.zzfnb.isConnecting()) {
            if (this.zzfnb.zzafu() && this.zzfqo.zzfqi != 0) {
                int unused = this.zzfqo.zzfqi = this.zzfqo.zzfke.isGooglePlayServicesAvailable(this.zzfqo.mContext);
                if (this.zzfqo.zzfqi != 0) {
                    onConnectionFailed(new ConnectionResult(this.zzfqo.zzfqi, null));
                    return;
                }
            }
            zzbx zzbx = new zzbx(this.zzfqo, this.zzfnb, this.zzfjl);
            if (this.zzfnb.zzaam()) {
                this.zzfqv.zza(zzbx);
            }
            this.zzfnb.zza(zzbx);
        }
    }

    public final int getInstanceId() {
        return this.zzfqu;
    }

    /* access modifiers changed from: package-private */
    public final boolean isConnected() {
        return this.zzfnb.isConnected();
    }

    public final void onConnected(@Nullable Bundle bundle) {
        if (Looper.myLooper() == this.zzfqo.mHandler.getLooper()) {
            zzaik();
        } else {
            this.zzfqo.mHandler.post(new zzbs(this));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0069, code lost:
        if (r4.zzfqo.zzc(r5, r4.zzfqu) != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0071, code lost:
        if (r5.getErrorCode() != 18) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0073, code lost:
        r4.zzfpc = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0078, code lost:
        if (r4.zzfpc == false) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007a, code lost:
        r4.zzfqo.mHandler.sendMessageDelayed(android.os.Message.obtain(r4.zzfqo.mHandler, 9, r4.zzfjl), r4.zzfqo.zzfpe);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0097, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0098, code lost:
        r1 = r4.zzfjl.zzagl();
        r3 = new java.lang.StringBuilder(38 + java.lang.String.valueOf(r1).length());
        r3.append("API: ");
        r3.append(r1);
        r3.append(" is not available on this device.");
        zzw(new com.google.android.gms.common.api.Status(17, r3.toString()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        return;
     */
    @android.support.annotation.WorkerThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onConnectionFailed(@android.support.annotation.NonNull com.google.android.gms.common.ConnectionResult r5) {
        /*
            r4 = this;
            com.google.android.gms.common.api.internal.zzbp r0 = r4.zzfqo
            android.os.Handler r0 = r0.mHandler
            com.google.android.gms.common.internal.zzbq.zza(r0)
            com.google.android.gms.common.api.internal.zzcy r0 = r4.zzfqv
            if (r0 == 0) goto L_0x0012
            com.google.android.gms.common.api.internal.zzcy r0 = r4.zzfqv
            r0.zzaje()
        L_0x0012:
            r4.zzain()
            com.google.android.gms.common.api.internal.zzbp r0 = r4.zzfqo
            r1 = -1
            int unused = r0.zzfqi = r1
            r4.zzi(r5)
            int r0 = r5.getErrorCode()
            r1 = 4
            if (r0 != r1) goto L_0x002d
            com.google.android.gms.common.api.Status r5 = com.google.android.gms.common.api.internal.zzbp.zzfqf
            r4.zzw(r5)
            return
        L_0x002d:
            java.util.Queue<com.google.android.gms.common.api.internal.zza> r0 = r4.zzfqp
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0038
            r4.zzfqw = r5
            return
        L_0x0038:
            java.lang.Object r0 = com.google.android.gms.common.api.internal.zzbp.sLock
            monitor-enter(r0)
            com.google.android.gms.common.api.internal.zzbp r1 = r4.zzfqo     // Catch:{ all -> 0x00ca }
            com.google.android.gms.common.api.internal.zzak r1 = r1.zzfql     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x0060
            com.google.android.gms.common.api.internal.zzbp r1 = r4.zzfqo     // Catch:{ all -> 0x00ca }
            java.util.Set r1 = r1.zzfqm     // Catch:{ all -> 0x00ca }
            com.google.android.gms.common.api.internal.zzh<O> r2 = r4.zzfjl     // Catch:{ all -> 0x00ca }
            boolean r1 = r1.contains(r2)     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x0060
            com.google.android.gms.common.api.internal.zzbp r1 = r4.zzfqo     // Catch:{ all -> 0x00ca }
            com.google.android.gms.common.api.internal.zzak r1 = r1.zzfql     // Catch:{ all -> 0x00ca }
            int r2 = r4.zzfqu     // Catch:{ all -> 0x00ca }
            r1.zzb(r5, r2)     // Catch:{ all -> 0x00ca }
            monitor-exit(r0)     // Catch:{ all -> 0x00ca }
            return
        L_0x0060:
            monitor-exit(r0)     // Catch:{ all -> 0x00ca }
            com.google.android.gms.common.api.internal.zzbp r0 = r4.zzfqo
            int r1 = r4.zzfqu
            boolean r0 = r0.zzc(r5, r1)
            if (r0 != 0) goto L_0x00c9
            int r5 = r5.getErrorCode()
            r0 = 18
            if (r5 != r0) goto L_0x0076
            r5 = 1
            r4.zzfpc = r5
        L_0x0076:
            boolean r5 = r4.zzfpc
            if (r5 == 0) goto L_0x0098
            com.google.android.gms.common.api.internal.zzbp r5 = r4.zzfqo
            android.os.Handler r5 = r5.mHandler
            com.google.android.gms.common.api.internal.zzbp r0 = r4.zzfqo
            android.os.Handler r0 = r0.mHandler
            r1 = 9
            com.google.android.gms.common.api.internal.zzh<O> r2 = r4.zzfjl
            android.os.Message r0 = android.os.Message.obtain(r0, r1, r2)
            com.google.android.gms.common.api.internal.zzbp r1 = r4.zzfqo
            long r1 = r1.zzfpe
            r5.sendMessageDelayed(r0, r1)
            return
        L_0x0098:
            com.google.android.gms.common.api.Status r5 = new com.google.android.gms.common.api.Status
            r0 = 17
            com.google.android.gms.common.api.internal.zzh<O> r1 = r4.zzfjl
            java.lang.String r1 = r1.zzagl()
            r2 = 38
            java.lang.String r3 = java.lang.String.valueOf(r1)
            int r3 = r3.length()
            int r2 = r2 + r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r2 = "API: "
            r3.append(r2)
            r3.append(r1)
            java.lang.String r1 = " is not available on this device."
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r5.<init>(r0, r1)
            r4.zzw(r5)
        L_0x00c9:
            return
        L_0x00ca:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00ca }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzbr.onConnectionFailed(com.google.android.gms.common.ConnectionResult):void");
    }

    public final void onConnectionSuspended(int i) {
        if (Looper.myLooper() == this.zzfqo.mHandler.getLooper()) {
            zzail();
        } else {
            this.zzfqo.mHandler.post(new zzbt(this));
        }
    }

    @WorkerThread
    public final void resume() {
        zzbq.zza(this.zzfqo.mHandler);
        if (this.zzfpc) {
            connect();
        }
    }

    @WorkerThread
    public final void signOut() {
        zzbq.zza(this.zzfqo.mHandler);
        zzw(zzbp.zzfqe);
        this.zzfqr.zzahj();
        for (zzcn zzf : (zzcn[]) this.zzfqt.keySet().toArray(new zzcn[this.zzfqt.size()])) {
            zza(new zzf(zzf, new TaskCompletionSource()));
        }
        zzi(new ConnectionResult(4));
        this.zzfnb.zza(new zzbv(this));
    }

    public final void zza(ConnectionResult connectionResult, Api<?> api, boolean z) {
        if (Looper.myLooper() == this.zzfqo.mHandler.getLooper()) {
            onConnectionFailed(connectionResult);
        } else {
            this.zzfqo.mHandler.post(new zzbu(this, connectionResult));
        }
    }

    @WorkerThread
    public final void zza(zza zza) {
        zzbq.zza(this.zzfqo.mHandler);
        if (this.zzfnb.isConnected()) {
            zzb(zza);
            zzaiq();
            return;
        }
        this.zzfqp.add(zza);
        if (this.zzfqw == null || !this.zzfqw.hasResolution()) {
            connect();
        } else {
            onConnectionFailed(this.zzfqw);
        }
    }

    @WorkerThread
    public final void zza(zzj zzj) {
        zzbq.zza(this.zzfqo.mHandler);
        this.zzfqs.add(zzj);
    }

    public final boolean zzaam() {
        return this.zzfnb.zzaam();
    }

    public final Api.zze zzahd() {
        return this.zzfnb;
    }

    @WorkerThread
    public final void zzahx() {
        zzbq.zza(this.zzfqo.mHandler);
        if (this.zzfpc) {
            zzaip();
            zzw(this.zzfqo.zzfke.isGooglePlayServicesAvailable(this.zzfqo.mContext) == 18 ? new Status(8, "Connection timed out while waiting for Google Play services update to complete.") : new Status(8, "API failed to connect while resuming due to an unknown error."));
            this.zzfnb.disconnect();
        }
    }

    public final Map<zzcn<?>, zzcu> zzaim() {
        return this.zzfqt;
    }

    @WorkerThread
    public final void zzain() {
        zzbq.zza(this.zzfqo.mHandler);
        this.zzfqw = null;
    }

    @WorkerThread
    public final ConnectionResult zzaio() {
        zzbq.zza(this.zzfqo.mHandler);
        return this.zzfqw;
    }

    @WorkerThread
    public final void zzair() {
        zzbq.zza(this.zzfqo.mHandler);
        if (this.zzfnb.isConnected() && this.zzfqt.size() == 0) {
            if (this.zzfqr.zzahi()) {
                zzaiq();
            } else {
                this.zzfnb.disconnect();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final zzcwb zzais() {
        if (this.zzfqv == null) {
            return null;
        }
        return this.zzfqv.zzais();
    }

    @WorkerThread
    public final void zzh(@NonNull ConnectionResult connectionResult) {
        zzbq.zza(this.zzfqo.mHandler);
        this.zzfnb.disconnect();
        onConnectionFailed(connectionResult);
    }

    @WorkerThread
    public final void zzw(Status status) {
        zzbq.zza(this.zzfqo.mHandler);
        for (zza zzs : this.zzfqp) {
            zzs.zzs(status);
        }
        this.zzfqp.clear();
    }
}
