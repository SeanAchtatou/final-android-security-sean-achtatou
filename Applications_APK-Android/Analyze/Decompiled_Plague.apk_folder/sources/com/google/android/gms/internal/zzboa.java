package com.google.android.gms.internal;

import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.ChangeListener;
import com.google.android.gms.drive.events.OnChangeListener;

final /* synthetic */ class zzboa implements ChangeListener {
    private final OnChangeListener zzgmx;

    private zzboa(OnChangeListener onChangeListener) {
        this.zzgmx = onChangeListener;
    }

    static ChangeListener zza(OnChangeListener onChangeListener) {
        return new zzboa(onChangeListener);
    }

    public final void onChange(ChangeEvent changeEvent) {
        this.zzgmx.onChange(changeEvent);
    }
}
