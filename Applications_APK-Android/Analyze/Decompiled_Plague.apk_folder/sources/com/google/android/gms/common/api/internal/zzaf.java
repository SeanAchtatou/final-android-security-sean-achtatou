package com.google.android.gms.common.api.internal;

import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.AvailabilityException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import java.util.Collections;
import java.util.Map;

final class zzaf implements OnCompleteListener<Void> {
    private /* synthetic */ zzad zzfns;

    private zzaf(zzad zzad) {
        this.zzfns = zzad;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.zzad.zza(com.google.android.gms.common.api.internal.zzad, boolean):boolean
     arg types: [com.google.android.gms.common.api.internal.zzad, int]
     candidates:
      com.google.android.gms.common.api.internal.zzad.zza(com.google.android.gms.common.api.internal.zzad, com.google.android.gms.common.ConnectionResult):com.google.android.gms.common.ConnectionResult
      com.google.android.gms.common.api.internal.zzad.zza(com.google.android.gms.common.api.internal.zzad, java.util.Map):java.util.Map
      com.google.android.gms.common.api.internal.zzad.zza(com.google.android.gms.common.api.internal.zzac<?>, com.google.android.gms.common.ConnectionResult):boolean
      com.google.android.gms.common.api.internal.zzad.zza(com.google.android.gms.common.api.internal.zzad, boolean):boolean */
    public final void onComplete(@NonNull Task<Void> task) {
        zzad zzad;
        ConnectionResult connectionResult;
        Map zzd;
        this.zzfns.zzfmy.lock();
        try {
            if (this.zzfns.zzfnn) {
                if (task.isSuccessful()) {
                    Map unused = this.zzfns.zzfno = new ArrayMap(this.zzfns.zzfne.size());
                    for (zzac zzaga : this.zzfns.zzfne.values()) {
                        this.zzfns.zzfno.put(zzaga.zzaga(), ConnectionResult.zzfhy);
                    }
                } else {
                    if (task.getException() instanceof AvailabilityException) {
                        AvailabilityException availabilityException = (AvailabilityException) task.getException();
                        if (this.zzfns.zzfnl) {
                            Map unused2 = this.zzfns.zzfno = new ArrayMap(this.zzfns.zzfne.size());
                            for (zzac zzac : this.zzfns.zzfne.values()) {
                                zzh zzaga2 = zzac.zzaga();
                                ConnectionResult connectionResult2 = availabilityException.getConnectionResult(zzac);
                                if (this.zzfns.zza(zzac, connectionResult2)) {
                                    zzd = this.zzfns.zzfno;
                                    connectionResult2 = new ConnectionResult(16);
                                } else {
                                    zzd = this.zzfns.zzfno;
                                }
                                zzd.put(zzaga2, connectionResult2);
                            }
                        } else {
                            Map unused3 = this.zzfns.zzfno = availabilityException.zzafw();
                        }
                        zzad = this.zzfns;
                        connectionResult = this.zzfns.zzahh();
                    } else {
                        Log.e("ConnectionlessGAC", "Unexpected availability exception", task.getException());
                        Map unused4 = this.zzfns.zzfno = Collections.emptyMap();
                        zzad = this.zzfns;
                        connectionResult = new ConnectionResult(8);
                    }
                    ConnectionResult unused5 = zzad.zzfnr = connectionResult;
                }
                if (this.zzfns.zzfnp != null) {
                    this.zzfns.zzfno.putAll(this.zzfns.zzfnp);
                    ConnectionResult unused6 = this.zzfns.zzfnr = this.zzfns.zzahh();
                }
                if (this.zzfns.zzfnr == null) {
                    this.zzfns.zzahf();
                    this.zzfns.zzahg();
                } else {
                    boolean unused7 = this.zzfns.zzfnn = false;
                    this.zzfns.zzfnh.zzc(this.zzfns.zzfnr);
                }
                this.zzfns.zzfnj.signalAll();
            }
        } finally {
            this.zzfns.zzfmy.unlock();
        }
    }
}
