package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzblo extends zzblk {
    private /* synthetic */ zzbjt zzgkw;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzblo(zzbll zzbll, GoogleApiClient googleApiClient, zzbjt zzbjt) {
        super(googleApiClient);
        this.zzgkw = zzbjt;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(this.zzgkw, (zzbpj) null, (String) null, new zzbrj(this));
    }
}
