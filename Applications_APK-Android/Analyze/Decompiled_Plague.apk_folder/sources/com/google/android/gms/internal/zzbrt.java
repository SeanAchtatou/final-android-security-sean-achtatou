package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzbrt extends zzbrs<Boolean> {
    public zzbrt(TaskCompletionSource<Boolean> taskCompletionSource) {
        super(taskCompletionSource);
    }

    public final void onSuccess() throws RemoteException {
        zzaox().setResult(true);
    }
}
