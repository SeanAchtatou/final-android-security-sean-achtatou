package com.google.android.gms.drive.query;

import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.SearchableCollectionMetadataField;
import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.SearchableOrderedMetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.internal.zzbse;
import com.google.android.gms.internal.zzbsr;
import java.util.Date;

public class SearchableField {
    public static final SearchableMetadataField<Boolean> IS_PINNED = zzbse.zzgqg;
    public static final SearchableOrderedMetadataField<Date> LAST_VIEWED_BY_ME = zzbsr.zzgrk;
    public static final SearchableMetadataField<String> MIME_TYPE = zzbse.zzgqo;
    public static final SearchableOrderedMetadataField<Date> MODIFIED_DATE = zzbsr.zzgrl;
    public static final SearchableCollectionMetadataField<DriveId> PARENTS = zzbse.zzgqt;
    public static final SearchableMetadataField<Boolean> STARRED = zzbse.zzgqv;
    public static final SearchableMetadataField<String> TITLE = zzbse.zzgqx;
    public static final SearchableMetadataField<Boolean> TRASHED = zzbse.zzgqy;
    public static final SearchableOrderedMetadataField<Date> zzgrz = zzbsr.zzgrn;
    public static final SearchableMetadataField<AppVisibleCustomProperties> zzgsa = zzbse.zzgpt;
}
