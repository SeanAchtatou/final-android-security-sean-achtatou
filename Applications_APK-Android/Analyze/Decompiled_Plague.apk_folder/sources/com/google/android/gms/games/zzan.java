package com.google.android.gms.games;

import android.support.annotation.NonNull;
import com.google.android.gms.games.internal.zzo;
import com.google.android.gms.games.leaderboard.Leaderboards;

final class zzan implements zzo<Leaderboards.LeaderboardMetadataResult> {
    zzan() {
    }

    public final /* synthetic */ void release(@NonNull Object obj) {
        Leaderboards.LeaderboardMetadataResult leaderboardMetadataResult = (Leaderboards.LeaderboardMetadataResult) obj;
        if (leaderboardMetadataResult.getLeaderboards() != null) {
            leaderboardMetadataResult.getLeaderboards().release();
        }
    }
}
