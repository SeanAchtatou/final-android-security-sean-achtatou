package com.google.android.gms.drive.events;

import com.google.android.gms.internal.zzben;

public interface DriveEvent extends zzben {
    int getType();
}
