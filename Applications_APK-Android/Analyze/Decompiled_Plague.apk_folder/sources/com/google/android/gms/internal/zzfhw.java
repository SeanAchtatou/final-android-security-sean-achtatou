package com.google.android.gms.internal;

import android.support.v4.view.MotionEventCompat;
import java.io.IOException;

public final class zzfhw extends zzfhe<zzfhw> {
    private static volatile zzfhw[] zzpiv;
    public String url = null;
    public Integer zzjhb = null;
    public zzfhr zzpiw = null;
    private zzfht zzpix = null;
    private Integer zzpiy = null;
    private int[] zzpiz = zzfhn.zzphl;
    private String zzpja = null;
    public Integer zzpjb = null;
    public String[] zzpjc = zzfhn.EMPTY_STRING_ARRAY;

    public zzfhw() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzaq */
    public final zzfhw zza(zzfhb zzfhb) throws IOException {
        zzfhk zzfhk;
        while (true) {
            int zzcts = zzfhb.zzcts();
            switch (zzcts) {
                case 0:
                    return this;
                case 8:
                    this.zzjhb = Integer.valueOf(zzfhb.zzctv());
                    continue;
                case 18:
                    this.url = zzfhb.readString();
                    continue;
                case MotionEventCompat.AXIS_SCROLL:
                    if (this.zzpiw == null) {
                        this.zzpiw = new zzfhr();
                    }
                    zzfhk = this.zzpiw;
                    break;
                case MotionEventCompat.AXIS_GENERIC_3:
                    if (this.zzpix == null) {
                        this.zzpix = new zzfht();
                    }
                    zzfhk = this.zzpix;
                    break;
                case MotionEventCompat.AXIS_GENERIC_9:
                    this.zzpiy = Integer.valueOf(zzfhb.zzctv());
                    continue;
                case 48:
                    int zzb = zzfhn.zzb(zzfhb, 48);
                    int length = this.zzpiz == null ? 0 : this.zzpiz.length;
                    int[] iArr = new int[(zzb + length)];
                    if (length != 0) {
                        System.arraycopy(this.zzpiz, 0, iArr, 0, length);
                    }
                    while (length < iArr.length - 1) {
                        iArr[length] = zzfhb.zzctv();
                        zzfhb.zzcts();
                        length++;
                    }
                    iArr[length] = zzfhb.zzctv();
                    this.zzpiz = iArr;
                    continue;
                case 50:
                    int zzki = zzfhb.zzki(zzfhb.zzcuh());
                    int position = zzfhb.getPosition();
                    int i = 0;
                    while (zzfhb.zzcuj() > 0) {
                        zzfhb.zzctv();
                        i++;
                    }
                    zzfhb.zzlv(position);
                    int length2 = this.zzpiz == null ? 0 : this.zzpiz.length;
                    int[] iArr2 = new int[(i + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.zzpiz, 0, iArr2, 0, length2);
                    }
                    while (length2 < iArr2.length) {
                        iArr2[length2] = zzfhb.zzctv();
                        length2++;
                    }
                    this.zzpiz = iArr2;
                    zzfhb.zzkj(zzki);
                    continue;
                case 58:
                    this.zzpja = zzfhb.readString();
                    continue;
                case 64:
                    try {
                        int zzctv = zzfhb.zzctv();
                        switch (zzctv) {
                            default:
                                StringBuilder sb = new StringBuilder(46);
                                sb.append(zzctv);
                                sb.append(" is not a valid enum AdResourceType");
                                throw new IllegalArgumentException(sb.toString());
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                                this.zzpjb = Integer.valueOf(zzctv);
                                continue;
                                continue;
                        }
                    } catch (IllegalArgumentException unused) {
                        zzfhb.zzlv(zzfhb.getPosition());
                        zza(zzfhb, zzcts);
                    }
                case 74:
                    int zzb2 = zzfhn.zzb(zzfhb, 74);
                    int length3 = this.zzpjc == null ? 0 : this.zzpjc.length;
                    String[] strArr = new String[(zzb2 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.zzpjc, 0, strArr, 0, length3);
                    }
                    while (length3 < strArr.length - 1) {
                        strArr[length3] = zzfhb.readString();
                        zzfhb.zzcts();
                        length3++;
                    }
                    strArr[length3] = zzfhb.readString();
                    this.zzpjc = strArr;
                    continue;
                default:
                    if (!super.zza(zzfhb, zzcts)) {
                        return this;
                    }
                    continue;
            }
            zzfhb.zza(zzfhk);
        }
    }

    public static zzfhw[] zzcxn() {
        if (zzpiv == null) {
            synchronized (zzfhi.zzphg) {
                if (zzpiv == null) {
                    zzpiv = new zzfhw[0];
                }
            }
        }
        return zzpiv;
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        zzfhc.zzaa(1, this.zzjhb.intValue());
        if (this.url != null) {
            zzfhc.zzn(2, this.url);
        }
        if (this.zzpiw != null) {
            zzfhc.zza(3, this.zzpiw);
        }
        if (this.zzpix != null) {
            zzfhc.zza(4, this.zzpix);
        }
        if (this.zzpiy != null) {
            zzfhc.zzaa(5, this.zzpiy.intValue());
        }
        if (this.zzpiz != null && this.zzpiz.length > 0) {
            for (int zzaa : this.zzpiz) {
                zzfhc.zzaa(6, zzaa);
            }
        }
        if (this.zzpja != null) {
            zzfhc.zzn(7, this.zzpja);
        }
        if (this.zzpjb != null) {
            zzfhc.zzaa(8, this.zzpjb.intValue());
        }
        if (this.zzpjc != null && this.zzpjc.length > 0) {
            for (String str : this.zzpjc) {
                if (str != null) {
                    zzfhc.zzn(9, str);
                }
            }
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo() + zzfhc.zzad(1, this.zzjhb.intValue());
        if (this.url != null) {
            zzo += zzfhc.zzo(2, this.url);
        }
        if (this.zzpiw != null) {
            zzo += zzfhc.zzb(3, this.zzpiw);
        }
        if (this.zzpix != null) {
            zzo += zzfhc.zzb(4, this.zzpix);
        }
        if (this.zzpiy != null) {
            zzo += zzfhc.zzad(5, this.zzpiy.intValue());
        }
        if (this.zzpiz != null && this.zzpiz.length > 0) {
            int i = 0;
            for (int zzkx : this.zzpiz) {
                i += zzfhc.zzkx(zzkx);
            }
            zzo = zzo + i + (this.zzpiz.length * 1);
        }
        if (this.zzpja != null) {
            zzo += zzfhc.zzo(7, this.zzpja);
        }
        if (this.zzpjb != null) {
            zzo += zzfhc.zzad(8, this.zzpjb.intValue());
        }
        if (this.zzpjc == null || this.zzpjc.length <= 0) {
            return zzo;
        }
        int i2 = 0;
        int i3 = 0;
        for (String str : this.zzpjc) {
            if (str != null) {
                i3++;
                i2 += zzfhc.zztd(str);
            }
        }
        return zzo + i2 + (1 * i3);
    }
}
