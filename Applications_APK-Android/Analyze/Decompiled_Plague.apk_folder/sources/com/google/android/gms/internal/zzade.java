package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.zzbs;

@zzzb
public final class zzade extends zzafh implements zzadk, zzadn {
    /* access modifiers changed from: private */
    public final Context mContext;
    private int mErrorCode = 3;
    private final Object mLock;
    /* access modifiers changed from: private */
    public final String zzcco;
    private final zzaev zzchv;
    private final zzadr zzctm;
    private final zzadn zzctn;
    /* access modifiers changed from: private */
    public final String zzcto;
    private final zztm zzctp;
    private final long zzctq;
    private int zzctr = 0;
    private zzadh zzcts;

    public zzade(Context context, String str, String str2, zztm zztm, zzaev zzaev, zzadr zzadr, zzadn zzadn, long j) {
        this.mContext = context;
        this.zzcco = str;
        this.zzcto = str2;
        this.zzctp = zztm;
        this.zzchv = zzaev;
        this.zzctm = zzadr;
        this.mLock = new Object();
        this.zzctn = zzadn;
        this.zzctq = j;
    }

    /* access modifiers changed from: private */
    public final void zza(zzis zzis, zzuf zzuf) {
        this.zzctm.zzod().zza((zzadn) this);
        try {
            if ("com.google.ads.mediation.admob.AdMobAdapter".equals(this.zzcco)) {
                zzuf.zza(zzis, this.zzcto, this.zzctp.zzcbb);
            } else {
                zzuf.zzc(zzis, this.zzcto);
            }
        } catch (RemoteException e) {
            zzafj.zzc("Fail to load ad from adapter.", e);
            zza(this.zzcco, 0);
        }
    }

    private final boolean zze(long j) {
        int i;
        long elapsedRealtime = this.zzctq - (zzbs.zzei().elapsedRealtime() - j);
        if (elapsedRealtime <= 0) {
            i = 4;
        } else {
            try {
                this.mLock.wait(elapsedRealtime);
                return true;
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                i = 5;
            }
        }
        this.mErrorCode = i;
        return false;
    }

    public final void onStop() {
    }

    public final void zza(String str, int i) {
        synchronized (this.mLock) {
            this.zzctr = 2;
            this.mErrorCode = i;
            this.mLock.notify();
        }
    }

    public final void zzaa(int i) {
        zza(this.zzcco, 0);
    }

    public final void zzbq(String str) {
        synchronized (this.mLock) {
            this.zzctr = 1;
            this.mLock.notify();
        }
    }

    public final void zzdg() {
        Handler handler;
        Runnable zzadg;
        if (this.zzctm != null && this.zzctm.zzod() != null && this.zzctm.zzoc() != null) {
            zzadm zzod = this.zzctm.zzod();
            zzod.zza((zzadn) null);
            zzod.zza((zzadk) this);
            zzis zzis = this.zzchv.zzcpe.zzclo;
            zzuf zzoc = this.zzctm.zzoc();
            try {
                if (zzoc.isInitialized()) {
                    handler = zzais.zzdbs;
                    zzadg = new zzadf(this, zzis, zzoc);
                } else {
                    handler = zzais.zzdbs;
                    zzadg = new zzadg(this, zzoc, zzis, zzod);
                }
                handler.post(zzadg);
            } catch (RemoteException e) {
                zzafj.zzc("Fail to check if adapter is initialized.", e);
                zza(this.zzcco, 0);
            }
            long elapsedRealtime = zzbs.zzei().elapsedRealtime();
            while (true) {
                synchronized (this.mLock) {
                    if (this.zzctr != 0) {
                        this.zzcts = new zzadj().zzf(zzbs.zzei().elapsedRealtime() - elapsedRealtime).zzab(1 == this.zzctr ? 6 : this.mErrorCode).zzbr(this.zzcco).zzbs(this.zzctp.zzcbe).zzoa();
                    } else if (!zze(elapsedRealtime)) {
                        this.zzcts = new zzadj().zzab(this.mErrorCode).zzf(zzbs.zzei().elapsedRealtime() - elapsedRealtime).zzbr(this.zzcco).zzbs(this.zzctp.zzcbe).zzoa();
                        break;
                    }
                }
            }
            zzod.zza((zzadn) null);
            zzod.zza((zzadk) null);
            if (this.zzctr == 1) {
                this.zzctn.zzbq(this.zzcco);
            } else {
                this.zzctn.zza(this.zzcco, this.mErrorCode);
            }
        }
    }

    public final zzadh zznx() {
        zzadh zzadh;
        synchronized (this.mLock) {
            zzadh = this.zzcts;
        }
        return zzadh;
    }

    public final zztm zzny() {
        return this.zzctp;
    }

    public final void zznz() {
        zza(this.zzchv.zzcpe.zzclo, this.zzctm.zzoc());
    }
}
