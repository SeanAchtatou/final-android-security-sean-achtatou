package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.snapshot.Snapshots;

final class zzca implements zzbo<Snapshots.OpenSnapshotResult, Snapshots.OpenSnapshotResult> {
    zzca() {
    }

    public final /* synthetic */ Object zzb(@Nullable Result result) {
        return (Snapshots.OpenSnapshotResult) result;
    }
}
