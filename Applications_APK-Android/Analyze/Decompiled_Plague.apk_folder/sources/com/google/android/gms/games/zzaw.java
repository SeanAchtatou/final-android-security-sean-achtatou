package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.Players;

final class zzaw implements zzbo<Players.LoadPlayersResult, PlayerBuffer> {
    zzaw() {
    }

    public final /* synthetic */ Object zzb(@Nullable Result result) {
        Players.LoadPlayersResult loadPlayersResult = (Players.LoadPlayersResult) result;
        if (loadPlayersResult == null) {
            return null;
        }
        return loadPlayersResult.getPlayers();
    }
}
