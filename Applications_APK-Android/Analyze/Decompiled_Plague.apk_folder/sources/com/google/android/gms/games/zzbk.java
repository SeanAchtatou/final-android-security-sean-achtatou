package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.zzq;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.zzh;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbk extends zzq<zzh> {
    private /* synthetic */ zzcl zzhik;
    private /* synthetic */ RoomConfig zzhkn;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbk(RealTimeMultiplayerClient realTimeMultiplayerClient, zzcl zzcl, zzcl zzcl2, RoomConfig roomConfig) {
        super(zzcl);
        this.zzhik = zzcl2;
        this.zzhkn = roomConfig;
    }

    /* access modifiers changed from: protected */
    public final void zzb(GamesClientImpl gamesClientImpl, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException, SecurityException {
        gamesClientImpl.zzc(this.zzhik, this.zzhik, this.zzhik, this.zzhkn);
        taskCompletionSource.setResult(null);
    }
}
