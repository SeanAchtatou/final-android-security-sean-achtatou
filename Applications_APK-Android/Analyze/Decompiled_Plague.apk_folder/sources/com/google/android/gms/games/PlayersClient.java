package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.internal.api.zzp;
import com.google.android.gms.games.internal.zzg;
import com.google.android.gms.games.internal.zzo;
import com.google.android.gms.tasks.Task;

public class PlayersClient extends zzp {
    private static final zzbo<Players.LoadPlayersResult, PlayerBuffer> zzhjz = new zzaw();
    private static final zzo<Players.LoadPlayersResult> zzhka = new zzax();
    private static final zzbo<Players.LoadPlayersResult, Player> zzhkb = new zzay();

    PlayersClient(@NonNull Activity activity, @NonNull Games.GamesOptions gamesOptions) {
        super(activity, gamesOptions);
    }

    PlayersClient(@NonNull Context context, @NonNull Games.GamesOptions gamesOptions) {
        super(context, gamesOptions);
    }

    public Task<Intent> getCompareProfileIntent(@NonNull Player player) {
        return zza(new zzav(this, player));
    }

    public Task<Player> getCurrentPlayer() {
        return zza(new zzau(this));
    }

    public Task<String> getCurrentPlayerId() {
        return zza(new zzat(this));
    }

    public Task<AnnotatedData<PlayerBuffer>> loadMoreRecentlyPlayedWithPlayers(@IntRange(from = 1, to = 25) int i) {
        return zzg.zzc(Games.Players.loadMoreRecentlyPlayedWithPlayers(zzagb(), i), zzhjz);
    }

    public Task<AnnotatedData<Player>> loadPlayer(@NonNull String str) {
        return loadPlayer(str, false);
    }

    public Task<AnnotatedData<Player>> loadPlayer(@NonNull String str, boolean z) {
        return zzg.zza(Games.Players.loadPlayer(zzagb(), str, z), zzhkb, zzhka);
    }

    public Task<AnnotatedData<PlayerBuffer>> loadRecentlyPlayedWithPlayers(@IntRange(from = 1, to = 25) int i, boolean z) {
        return zzg.zzc(Games.Players.loadRecentlyPlayedWithPlayers(zzagb(), i, z), zzhjz);
    }
}
