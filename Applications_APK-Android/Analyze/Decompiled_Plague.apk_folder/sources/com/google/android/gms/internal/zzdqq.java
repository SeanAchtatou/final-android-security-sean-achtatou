package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrp;
import java.io.IOException;

public final class zzdqq extends zzfee<zzdqq, zza> implements zzffk {
    private static volatile zzffm<zzdqq> zzbas;
    /* access modifiers changed from: private */
    public static final zzdqq zzlre;
    private zzdrp zzlrd;

    public static final class zza extends zzfef<zzdqq, zza> implements zzffk {
        private zza() {
            super(zzdqq.zzlre);
        }

        /* synthetic */ zza(zzdqr zzdqr) {
            this();
        }
    }

    static {
        zzdqq zzdqq = new zzdqq();
        zzlre = zzdqq;
        zzdqq.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdqq.zzpbs.zzbim();
    }

    private zzdqq() {
    }

    public static zzdqq zzbmr() {
        return zzlre;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdrp.zza zza2;
        switch (zzdqr.zzbaq[i - 1]) {
            case 1:
                return new zzdqq();
            case 2:
                return zzlre;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                this.zzlrd = (zzdrp) ((zzfen) obj).zza(this.zzlrd, ((zzdqq) obj2).zzlrd);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    boolean z = false;
                    while (!z) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 18) {
                                    if (this.zzlrd != null) {
                                        zzdrp zzdrp = this.zzlrd;
                                        zzfef zzfef = (zzfef) zzdrp.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdrp);
                                        zza2 = (zzdrp.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlrd = (zzdrp) zzfdq.zza(zzdrp.zzbny(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlrd);
                                        this.zzlrd = (zzdrp) zza2.zzcvj();
                                    }
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdqq.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlre);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlre;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlrd != null) {
            zzfdv.zza(2, this.zzlrd == null ? zzdrp.zzbny() : this.zzlrd);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzdrp zzbmq() {
        return this.zzlrd == null ? zzdrp.zzbny() : this.zzlrd;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlrd != null) {
            i2 = 0 + zzfdv.zzb(2, this.zzlrd == null ? zzdrp.zzbny() : this.zzlrd);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
