package com.google.android.gms.drive;

@Deprecated
public final class zzo extends ExecutionOptions {
    private String zzghd;
    private String zzghe;

    private zzo(String str, boolean z, String str2, String str3, int i) {
        super(str, z, i);
        this.zzghd = str2;
        this.zzghe = str3;
    }

    public static zzo zza(ExecutionOptions executionOptions) {
        zzq zzq = new zzq();
        if (executionOptions != null) {
            if (executionOptions.zzanu() != 0) {
                throw new IllegalStateException("May not set a conflict strategy for new file creation.");
            }
            String zzans = executionOptions.zzans();
            if (zzans != null) {
                zzq.setTrackingTag(zzans);
            }
            zzq.setNotifyOnCompletion(executionOptions.zzant());
        }
        return (zzo) zzq.build();
    }

    public final String zzanw() {
        return this.zzghd;
    }

    public final String zzanx() {
        return this.zzghe;
    }
}
