package com.google.android.gms.internal;

import android.graphics.drawable.Drawable;

final class zzbeg extends Drawable.ConstantState {
    int mChangingConfigurations;
    int zzfvi;

    zzbeg(zzbeg zzbeg) {
        if (zzbeg != null) {
            this.mChangingConfigurations = zzbeg.mChangingConfigurations;
            this.zzfvi = zzbeg.zzfvi;
        }
    }

    public final int getChangingConfigurations() {
        return this.mChangingConfigurations;
    }

    public final Drawable newDrawable() {
        return new zzbec(this);
    }
}
