package com.google.android.gms.internal;

import java.util.concurrent.ExecutionException;

final /* synthetic */ class zzajm implements Runnable {
    private final zzajy zzbxh;
    private final zzajp zzdcf;

    zzajm(zzajy zzajy, zzajp zzajp) {
        this.zzbxh = zzajy;
        this.zzdcf = zzajp;
    }

    public final void run() {
        Throwable e;
        zzajy zzajy = this.zzbxh;
        try {
            zzajy.set(this.zzdcf.get());
        } catch (ExecutionException e2) {
            e = e2.getCause();
            zzajy.setException(e);
        } catch (InterruptedException e3) {
            e = e3;
            Thread.currentThread().interrupt();
            zzajy.setException(e);
        } catch (Exception e4) {
            zzajy.setException(e4);
        }
    }
}
