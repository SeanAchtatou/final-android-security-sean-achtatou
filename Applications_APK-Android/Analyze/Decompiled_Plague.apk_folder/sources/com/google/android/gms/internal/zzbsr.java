package com.google.android.gms.internal;

public final class zzbsr {
    public static final zzbss zzgrj = new zzbss("created", 4100000);
    public static final zzbst zzgrk = new zzbst("lastOpenedTime", 4300000);
    public static final zzbsv zzgrl = new zzbsv("modified", 4100000);
    public static final zzbsu zzgrm = new zzbsu("modifiedByMe", 4100000);
    public static final zzbsx zzgrn = new zzbsx("sharedWithMe", 4100000);
    public static final zzbsw zzgro = new zzbsw("recency", 8000000);
}
