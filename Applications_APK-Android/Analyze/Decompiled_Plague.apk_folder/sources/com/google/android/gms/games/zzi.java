package com.google.android.gms.games;

import com.google.android.gms.games.Games;
import java.util.Collections;
import java.util.List;

final class zzi extends Games.zzb {
    zzi() {
        super(null);
    }

    public final /* synthetic */ List zzq(Object obj) {
        return Collections.singletonList(Games.SCOPE_GAMES);
    }
}
