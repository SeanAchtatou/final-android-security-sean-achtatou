package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdpu;
import com.google.android.gms.internal.zzdqg;
import com.google.android.gms.internal.zzdre;
import java.security.GeneralSecurityException;
import java.util.Arrays;

public final class zzdpl implements zzdsm {
    private final int zzlpt;
    private final int zzlpu;
    private zzdqg zzlpv;
    private zzdpq zzlpw;
    private int zzlpx;

    zzdpl(zzdrp zzdrp) throws GeneralSecurityException {
        String zzbns = zzdrp.zzbns();
        if (zzbns.equals("type.googleapis.com/google.crypto.tink.AesGcmKey")) {
            try {
                zzdqi zzs = zzdqi.zzs(zzdrp.zzbnt());
                this.zzlpt = zzdpm.zzlpy;
                this.zzlpv = (zzdqg) zzdpa.zzlpq.zzb(zzdrp);
                this.zzlpu = zzs.getKeySize();
            } catch (zzfew e) {
                throw new GeneralSecurityException("invalid KeyFormat protobuf, expected AesGcmKeyFormat", e);
            }
        } else if (zzbns.equals("type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey")) {
            try {
                zzdps zzi = zzdps.zzi(zzdrp.zzbnt());
                this.zzlpt = zzdpm.zzlpz;
                this.zzlpw = (zzdpq) zzdpa.zzlpq.zzb(zzdrp);
                this.zzlpx = zzi.zzblp().getKeySize();
                this.zzlpu = this.zzlpx + zzi.zzblq().getKeySize();
            } catch (zzfew e2) {
                throw new GeneralSecurityException("invalid KeyFormat protobuf, expected AesGcmKeyFormat", e2);
            }
        } else {
            String valueOf = String.valueOf(zzbns);
            throw new GeneralSecurityException(valueOf.length() != 0 ? "unsupported AEAD DEM key type: ".concat(valueOf) : new String("unsupported AEAD DEM key type: "));
        }
    }

    public final zzdoo zzad(byte[] bArr) throws GeneralSecurityException {
        zzffi zzffi;
        zzdpa zzdpa;
        String str;
        if (this.zzlpt == zzdpm.zzlpy) {
            zzffi = (zzdqg) ((zzdqg.zza) zzdqg.zzbmi().zza((zzfee) this.zzlpv)).zzr(zzfdh.zzay(bArr)).zzcvk();
            zzdpa = zzdpa.zzlpq;
            str = "type.googleapis.com/google.crypto.tink.AesGcmKey";
        } else if (this.zzlpt == zzdpm.zzlpz) {
            byte[] copyOfRange = Arrays.copyOfRange(bArr, 0, this.zzlpx);
            byte[] copyOfRange2 = Arrays.copyOfRange(bArr, this.zzlpx, this.zzlpu);
            zzffi = (zzdpq) zzdpq.zzbln().zzfi(this.zzlpw.getVersion()).zzb((zzdpu) ((zzdpu.zza) zzdpu.zzblu().zza((zzfee) this.zzlpw.zzbll())).zzl(zzfdh.zzay(copyOfRange)).zzcvk()).zzb((zzdre) ((zzdre.zza) zzdre.zzbnj().zza((zzfee) this.zzlpw.zzblm())).zzx(zzfdh.zzay(copyOfRange2)).zzcvk()).zzcvk();
            zzdpa = zzdpa.zzlpq;
            str = "type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey";
        } else {
            throw new GeneralSecurityException("unknown DEM key type");
        }
        return (zzdoo) zzdpa.zzb(str, zzffi);
    }

    public final int zzblk() {
        return this.zzlpu;
    }
}
