package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveResource;

abstract class zzbor extends zzblj<DriveResource.MetadataResult> {
    private zzbor(zzbog zzbog, GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* synthetic */ zzbor(zzbog zzbog, GoogleApiClient googleApiClient, zzboh zzboh) {
        this(zzbog, googleApiClient);
    }

    public final /* synthetic */ Result zzb(Status status) {
        return new zzboq(status, null);
    }
}
