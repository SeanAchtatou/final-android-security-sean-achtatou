package com.google.android.gms.common.api.internal;

final class zzv {
    private /* synthetic */ zzs zzfml;

    private zzv(zzs zzs) {
        this.zzfml = zzs;
    }

    /* synthetic */ zzv(zzs zzs, zzt zzt) {
        this(zzs);
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        zzs.zzd(this.zzfml.zzfkk);
        super.finalize();
    }
}
