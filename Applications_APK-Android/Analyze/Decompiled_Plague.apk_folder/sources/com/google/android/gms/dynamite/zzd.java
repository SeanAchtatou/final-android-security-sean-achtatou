package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

final class zzd implements DynamiteModule.zzd {
    zzd() {
    }

    public final zzj zza(Context context, String str, zzi zzi) throws DynamiteModule.zzc {
        int i;
        zzj zzj = new zzj();
        zzj.zzgum = zzi.zzab(context, str);
        zzj.zzgun = zzi.zzc(context, str, true);
        if (zzj.zzgum == 0 && zzj.zzgun == 0) {
            i = 0;
        } else if (zzj.zzgum >= zzj.zzgun) {
            i = -1;
        } else {
            zzj.zzguo = 1;
            return zzj;
        }
        zzj.zzguo = i;
        return zzj;
    }
}
