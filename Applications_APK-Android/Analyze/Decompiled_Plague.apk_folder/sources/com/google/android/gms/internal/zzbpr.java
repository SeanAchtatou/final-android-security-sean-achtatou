package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.zza;
import java.util.ArrayList;

public final class zzbpr implements Parcelable.Creator<zzbpq> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        DataHolder dataHolder = null;
        zza zza = null;
        boolean z = false;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    dataHolder = (DataHolder) zzbek.zza(parcel, readInt, DataHolder.CREATOR);
                    break;
                case 3:
                    arrayList = zzbek.zzc(parcel, readInt, DriveId.CREATOR);
                    break;
                case 4:
                    zza = (zza) zzbek.zza(parcel, readInt, zza.CREATOR);
                    break;
                case 5:
                    z = zzbek.zzc(parcel, readInt);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbpq(dataHolder, arrayList, zza, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbpq[i];
    }
}
