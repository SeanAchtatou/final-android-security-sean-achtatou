package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.common.api.internal.zzcp;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.internal.api.zzp;
import com.google.android.gms.games.internal.zzg;
import com.google.android.gms.games.multiplayer.InvitationBuffer;
import com.google.android.gms.games.multiplayer.InvitationCallback;
import com.google.android.gms.games.multiplayer.Invitations;
import com.google.android.gms.tasks.Task;

public class InvitationsClient extends zzp {
    private static final zzbo<Invitations.LoadInvitationsResult, InvitationBuffer> zzhij = new zzaa();

    InvitationsClient(@NonNull Activity activity, @NonNull Games.GamesOptions gamesOptions) {
        super(activity, gamesOptions);
    }

    InvitationsClient(@NonNull Context context, @NonNull Games.GamesOptions gamesOptions) {
        super(context, gamesOptions);
    }

    public Task<Intent> getInvitationInboxIntent() {
        return zza(new zzx(this));
    }

    public Task<AnnotatedData<InvitationBuffer>> loadInvitations() {
        return loadInvitations(0);
    }

    public Task<AnnotatedData<InvitationBuffer>> loadInvitations(int i) {
        return zzg.zzc(Games.Invitations.loadInvitations(zzagb(), i), zzhij);
    }

    public Task<Void> registerInvitationCallback(@NonNull InvitationCallback invitationCallback) {
        zzcl zza = zza(invitationCallback, InvitationCallback.class.getSimpleName());
        return zza(new zzy(this, zza, zza), new zzz(this, zza.zzajc()));
    }

    public Task<Boolean> unregisterInvitationCallback(@NonNull InvitationCallback invitationCallback) {
        return zza(zzcp.zzb(invitationCallback, InvitationCallback.class.getSimpleName()));
    }
}
