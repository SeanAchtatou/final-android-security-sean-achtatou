package com.google.android.gms.games;

import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzcp extends zzac<Intent> {
    private /* synthetic */ int zzhkk;
    private /* synthetic */ int zzhkl;
    private /* synthetic */ boolean zzhkm;

    zzcp(TurnBasedMultiplayerClient turnBasedMultiplayerClient, int i, int i2, boolean z) {
        this.zzhkk = i;
        this.zzhkl = i2;
        this.zzhkm = z;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<Intent> taskCompletionSource) throws RemoteException {
        taskCompletionSource.setResult(gamesClientImpl.zzb(this.zzhkk, this.zzhkl, this.zzhkm));
    }
}
