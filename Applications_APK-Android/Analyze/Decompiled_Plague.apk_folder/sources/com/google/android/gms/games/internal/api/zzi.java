package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzi extends zzm {
    private /* synthetic */ String val$id;
    private /* synthetic */ int zzhpw;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzi(zza zza, String str, GoogleApiClient googleApiClient, String str2, int i) {
        super(str, googleApiClient);
        this.val$id = str2;
        this.zzhpw = i;
    }

    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zzb((zzn<Achievements.UpdateAchievementResult>) null, this.val$id, this.zzhpw);
    }
}
