package com.google.android.gms.games.internal.experience;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class ExperienceEventEntity extends zzc implements ExperienceEvent {
    public static final Parcelable.Creator<ExperienceEventEntity> CREATOR = new zza();
    private final int zzefe;
    private final Uri zzhgv;
    private final String zzhhg;
    private final String zzhrk;
    private final GameEntity zzhrl;
    private final String zzhrm;
    private final String zzhrn;
    private final long zzhro;
    private final long zzhrp;
    private final long zzhrq;
    private final int zzhrr;

    ExperienceEventEntity(String str, GameEntity gameEntity, String str2, String str3, String str4, Uri uri, long j, long j2, long j3, int i, int i2) {
        this.zzhrk = str;
        this.zzhrl = gameEntity;
        this.zzhrm = str2;
        this.zzhrn = str3;
        this.zzhhg = str4;
        this.zzhgv = uri;
        this.zzhro = j;
        this.zzhrp = j2;
        this.zzhrq = j3;
        this.zzefe = i;
        this.zzhrr = i2;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof ExperienceEvent)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        ExperienceEvent experienceEvent = (ExperienceEvent) obj;
        return zzbg.equal(experienceEvent.zzatc(), zzatc()) && zzbg.equal(experienceEvent.getGame(), getGame()) && zzbg.equal(experienceEvent.zzatd(), zzatd()) && zzbg.equal(experienceEvent.zzate(), zzate()) && zzbg.equal(experienceEvent.getIconImageUrl(), getIconImageUrl()) && zzbg.equal(experienceEvent.getIconImageUri(), getIconImageUri()) && zzbg.equal(Long.valueOf(experienceEvent.zzatf()), Long.valueOf(zzatf())) && zzbg.equal(Long.valueOf(experienceEvent.zzatg()), Long.valueOf(zzatg())) && zzbg.equal(Long.valueOf(experienceEvent.zzath()), Long.valueOf(zzath())) && zzbg.equal(Integer.valueOf(experienceEvent.getType()), Integer.valueOf(getType())) && zzbg.equal(Integer.valueOf(experienceEvent.zzati()), Integer.valueOf(zzati()));
    }

    public final /* bridge */ /* synthetic */ Object freeze() {
        if (this != null) {
            return this;
        }
        throw null;
    }

    public final Game getGame() {
        return this.zzhrl;
    }

    public final Uri getIconImageUri() {
        return this.zzhgv;
    }

    public final String getIconImageUrl() {
        return this.zzhhg;
    }

    public final int getType() {
        return this.zzefe;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{zzatc(), getGame(), zzatd(), zzate(), getIconImageUrl(), getIconImageUri(), Long.valueOf(zzatf()), Long.valueOf(zzatg()), Long.valueOf(zzath()), Integer.valueOf(getType()), Integer.valueOf(zzati())});
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String toString() {
        return zzbg.zzw(this).zzg("ExperienceId", zzatc()).zzg("Game", getGame()).zzg("DisplayTitle", zzatd()).zzg("DisplayDescription", zzate()).zzg("IconImageUrl", getIconImageUrl()).zzg("IconImageUri", getIconImageUri()).zzg("CreatedTimestamp", Long.valueOf(zzatf())).zzg("XpEarned", Long.valueOf(zzatg())).zzg("CurrentXp", Long.valueOf(zzath())).zzg("Type", Integer.valueOf(getType())).zzg("NewLevel", Integer.valueOf(zzati())).toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.GameEntity, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, this.zzhrk, false);
        zzbem.zza(parcel, 2, (Parcelable) this.zzhrl, i, false);
        zzbem.zza(parcel, 3, this.zzhrm, false);
        zzbem.zza(parcel, 4, this.zzhrn, false);
        zzbem.zza(parcel, 5, getIconImageUrl(), false);
        zzbem.zza(parcel, 6, (Parcelable) this.zzhgv, i, false);
        zzbem.zza(parcel, 7, this.zzhro);
        zzbem.zza(parcel, 8, this.zzhrp);
        zzbem.zza(parcel, 9, this.zzhrq);
        zzbem.zzc(parcel, 10, this.zzefe);
        zzbem.zzc(parcel, 11, this.zzhrr);
        zzbem.zzai(parcel, zze);
    }

    public final String zzatc() {
        return this.zzhrk;
    }

    public final String zzatd() {
        return this.zzhrm;
    }

    public final String zzate() {
        return this.zzhrn;
    }

    public final long zzatf() {
        return this.zzhro;
    }

    public final long zzatg() {
        return this.zzhrp;
    }

    public final long zzath() {
        return this.zzhrq;
    }

    public final int zzati() {
        return this.zzhrr;
    }
}
