package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Build;
import android.os.UserManager;

public abstract class zzctg<T> {
    private static Context zzaif = null;
    private static boolean zzccv = false;
    private static final Object zzjuh = new Object();
    private final zzctn zzjui;
    final String zzjuj;
    private final String zzjuk;
    private final T zzjul;
    private T zzjum;

    private zzctg(zzctn zzctn, String str, T t) {
        this.zzjum = null;
        if (zzctn.zzjuq == null && zzctn.zzjur == null) {
            throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
        } else if (zzctn.zzjuq == null || zzctn.zzjur == null) {
            this.zzjui = zzctn;
            this.zzjuk = zzctn.zzkq(str);
            String valueOf = String.valueOf(zzctn.zzjut);
            String valueOf2 = String.valueOf(str);
            this.zzjuj = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
            this.zzjul = t;
        } else {
            throw new IllegalArgumentException("Must pass one of SharedPreferences file name or ContentProvider URI");
        }
    }

    /* synthetic */ zzctg(zzctn zzctn, String str, Object obj, zzctk zzctk) {
        this(zzctn, str, obj);
    }

    /* access modifiers changed from: private */
    public static zzctg<String> zza(zzctn zzctn, String str, String str2) {
        return new zzctl(zzctn, str, str2);
    }

    static <V> V zza(zzctm<V> zzctm) {
        long clearCallingIdentity;
        try {
            return zzctm.zzbcd();
        } catch (SecurityException unused) {
            clearCallingIdentity = Binder.clearCallingIdentity();
            V zzbcd = zzctm.zzbcd();
            Binder.restoreCallingIdentity(clearCallingIdentity);
            return zzbcd;
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(clearCallingIdentity);
            throw th;
        }
    }

    @TargetApi(24)
    private final T zzbce() {
        if (!((Boolean) zza(zzctj.zzjup)).booleanValue()) {
            if (this.zzjui.zzjur != null) {
                String str = (String) zza(new zzcth(this, zzcss.zza(zzaif.getContentResolver(), this.zzjui.zzjur)));
                if (str != null) {
                    return zzkn(str);
                }
            } else if (this.zzjui.zzjuq == null || (Build.VERSION.SDK_INT >= 24 && !zzaif.isDeviceProtectedStorage() && !((UserManager) zzaif.getSystemService(UserManager.class)).isUserUnlocked())) {
                return null;
            } else {
                SharedPreferences sharedPreferences = zzaif.getSharedPreferences(this.zzjui.zzjuq, 0);
                if (sharedPreferences.contains(this.zzjuj)) {
                    return zzb(sharedPreferences);
                }
            }
        }
        return null;
    }

    private final T zzbcf() {
        String str;
        if (this.zzjuk == null || (str = (String) zza(new zzcti(this))) == null) {
            return null;
        }
        return zzkn(str);
    }

    public static void zzdw(Context context) {
        if (zzaif == null) {
            synchronized (zzjuh) {
                if (Build.VERSION.SDK_INT < 24 || !context.isDeviceProtectedStorage()) {
                    Context applicationContext = context.getApplicationContext();
                    if (applicationContext != null) {
                        context = applicationContext;
                    }
                }
                zzaif = context;
            }
            zzccv = false;
        }
    }

    public final T get() {
        if (zzaif == null) {
            throw new IllegalStateException("Must call PhenotypeFlag.init() first");
        }
        if (this.zzjui.zzjuv) {
            T zzbcf = zzbcf();
            if (zzbcf != null) {
                return zzbcf;
            }
            T zzbce = zzbce();
            if (zzbce != null) {
                return zzbce;
            }
        } else {
            T zzbce2 = zzbce();
            if (zzbce2 != null) {
                return zzbce2;
            }
            T zzbcf2 = zzbcf();
            if (zzbcf2 != null) {
                return zzbcf2;
            }
        }
        return this.zzjul;
    }

    public abstract T zzb(SharedPreferences sharedPreferences);

    /* access modifiers changed from: package-private */
    public final /* synthetic */ String zzbch() {
        return zzdld.zza(zzaif.getContentResolver(), this.zzjuk, (String) null);
    }

    public abstract T zzkn(String str);
}
