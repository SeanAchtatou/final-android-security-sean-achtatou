package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.api.GoogleApiClient;
import java.util.concurrent.atomic.AtomicReference;

final class zzbf implements GoogleApiClient.ConnectionCallbacks {
    private /* synthetic */ zzbd zzfpp;
    private /* synthetic */ AtomicReference zzfpq;
    private /* synthetic */ zzdc zzfpr;

    zzbf(zzbd zzbd, AtomicReference atomicReference, zzdc zzdc) {
        this.zzfpp = zzbd;
        this.zzfpq = atomicReference;
        this.zzfpr = zzdc;
    }

    public final void onConnected(Bundle bundle) {
        this.zzfpp.zza((GoogleApiClient) this.zzfpq.get(), this.zzfpr, true);
    }

    public final void onConnectionSuspended(int i) {
    }
}
