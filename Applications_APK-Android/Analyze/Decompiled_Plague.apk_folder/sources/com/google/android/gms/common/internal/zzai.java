package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Message;
import android.support.v4.os.EnvironmentCompat;
import android.util.Log;
import com.google.android.gms.common.stats.zza;
import java.util.HashMap;

final class zzai extends zzag implements Handler.Callback {
    /* access modifiers changed from: private */
    public final Context mApplicationContext;
    /* access modifiers changed from: private */
    public final Handler mHandler;
    /* access modifiers changed from: private */
    public final HashMap<zzah, zzaj> zzfxs = new HashMap<>();
    /* access modifiers changed from: private */
    public final zza zzfxt;
    private final long zzfxu;
    /* access modifiers changed from: private */
    public final long zzfxv;

    zzai(Context context) {
        this.mApplicationContext = context.getApplicationContext();
        this.mHandler = new Handler(context.getMainLooper(), this);
        this.zzfxt = zza.zzalq();
        this.zzfxu = 5000;
        this.zzfxv = 300000;
    }

    public final boolean handleMessage(Message message) {
        switch (message.what) {
            case 0:
                synchronized (this.zzfxs) {
                    zzah zzah = (zzah) message.obj;
                    zzaj zzaj = this.zzfxs.get(zzah);
                    if (zzaj != null && zzaj.zzala()) {
                        if (zzaj.isBound()) {
                            zzaj.zzge("GmsClientSupervisor");
                        }
                        this.zzfxs.remove(zzah);
                    }
                }
                return true;
            case 1:
                synchronized (this.zzfxs) {
                    zzah zzah2 = (zzah) message.obj;
                    zzaj zzaj2 = this.zzfxs.get(zzah2);
                    if (zzaj2 != null && zzaj2.getState() == 3) {
                        String valueOf = String.valueOf(zzah2);
                        StringBuilder sb = new StringBuilder(47 + String.valueOf(valueOf).length());
                        sb.append("Timeout waiting for ServiceConnection callback ");
                        sb.append(valueOf);
                        Log.wtf("GmsClientSupervisor", sb.toString(), new Exception());
                        ComponentName componentName = zzaj2.getComponentName();
                        if (componentName == null) {
                            componentName = zzah2.getComponentName();
                        }
                        if (componentName == null) {
                            componentName = new ComponentName(zzah2.getPackage(), EnvironmentCompat.MEDIA_UNKNOWN);
                        }
                        zzaj2.onServiceDisconnected(componentName);
                    }
                }
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public final boolean zza(zzah zzah, ServiceConnection serviceConnection, String str) {
        boolean isBound;
        zzbq.checkNotNull(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.zzfxs) {
            zzaj zzaj = this.zzfxs.get(zzah);
            if (zzaj != null) {
                this.mHandler.removeMessages(0, zzah);
                if (!zzaj.zza(serviceConnection)) {
                    zzaj.zza(serviceConnection, str);
                    switch (zzaj.getState()) {
                        case 1:
                            serviceConnection.onServiceConnected(zzaj.getComponentName(), zzaj.getBinder());
                            break;
                        case 2:
                            zzaj.zzgd(str);
                            break;
                    }
                } else {
                    String valueOf = String.valueOf(zzah);
                    StringBuilder sb = new StringBuilder(81 + String.valueOf(valueOf).length());
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            } else {
                zzaj = new zzaj(this, zzah);
                zzaj.zza(serviceConnection, str);
                zzaj.zzgd(str);
                this.zzfxs.put(zzah, zzaj);
            }
            isBound = zzaj.isBound();
        }
        return isBound;
    }

    /* access modifiers changed from: protected */
    public final void zzb(zzah zzah, ServiceConnection serviceConnection, String str) {
        zzbq.checkNotNull(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.zzfxs) {
            zzaj zzaj = this.zzfxs.get(zzah);
            if (zzaj == null) {
                String valueOf = String.valueOf(zzah);
                StringBuilder sb = new StringBuilder(50 + String.valueOf(valueOf).length());
                sb.append("Nonexistent connection status for service config: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            } else if (!zzaj.zza(serviceConnection)) {
                String valueOf2 = String.valueOf(zzah);
                StringBuilder sb2 = new StringBuilder(76 + String.valueOf(valueOf2).length());
                sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            } else {
                zzaj.zzb(serviceConnection, str);
                if (zzaj.zzala()) {
                    this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(0, zzah), this.zzfxu);
                }
            }
        }
    }
}
