package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdri;
import java.io.IOException;

public final class zzdrg extends zzfee<zzdrg, zza> implements zzffk {
    private static volatile zzffm<zzdrg> zzbas;
    /* access modifiers changed from: private */
    public static final zzdrg zzlsk;
    private int zzlql;
    private zzdri zzlsi;

    public static final class zza extends zzfef<zzdrg, zza> implements zzffk {
        private zza() {
            super(zzdrg.zzlsk);
        }

        /* synthetic */ zza(zzdrh zzdrh) {
            this();
        }
    }

    static {
        zzdrg zzdrg = new zzdrg();
        zzlsk = zzdrg;
        zzdrg.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdrg.zzpbs.zzbim();
    }

    private zzdrg() {
    }

    public static zzdrg zzbnm() {
        return zzlsk;
    }

    public static zzdrg zzy(zzfdh zzfdh) throws zzfew {
        return (zzdrg) zzfee.zza(zzlsk, zzfdh);
    }

    public final int getKeySize() {
        return this.zzlql;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdri.zza zza2;
        boolean z = false;
        switch (zzdrh.zzbaq[i - 1]) {
            case 1:
                return new zzdrg();
            case 2:
                return zzlsk;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdrg zzdrg = (zzdrg) obj2;
                this.zzlsi = (zzdri) zzfen.zza(this.zzlsi, zzdrg.zzlsi);
                boolean z2 = this.zzlql != 0;
                int i2 = this.zzlql;
                if (zzdrg.zzlql != 0) {
                    z = true;
                }
                this.zzlql = zzfen.zza(z2, i2, z, zzdrg.zzlql);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 10) {
                                    if (this.zzlsi != null) {
                                        zzdri zzdri = this.zzlsi;
                                        zzfef zzfef = (zzfef) zzdri.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdri);
                                        zza2 = (zzdri.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlsi = (zzdri) zzfdq.zza(zzdri.zzbnq(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlsi);
                                        this.zzlsi = (zzdri) zza2.zzcvj();
                                    }
                                } else if (zzcts == 16) {
                                    this.zzlql = zzfdq.zzcub();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdrg.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlsk);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlsk;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlsi != null) {
            zzfdv.zza(1, this.zzlsi == null ? zzdri.zzbnq() : this.zzlsi);
        }
        if (this.zzlql != 0) {
            zzfdv.zzab(2, this.zzlql);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzdri zzbni() {
        return this.zzlsi == null ? zzdri.zzbnq() : this.zzlsi;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlsi != null) {
            i2 = 0 + zzfdv.zzb(1, this.zzlsi == null ? zzdri.zzbnq() : this.zzlsi);
        }
        if (this.zzlql != 0) {
            i2 += zzfdv.zzae(2, this.zzlql);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
