package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

final class zzbn extends Handler {
    private /* synthetic */ zzbl zzfqc;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbn(zzbl zzbl, Looper looper) {
        super(looper);
        this.zzfqc = zzbl;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                ((zzbm) message.obj).zzc(this.zzfqc);
                return;
            case 2:
                throw ((RuntimeException) message.obj);
            default:
                int i = message.what;
                StringBuilder sb = new StringBuilder(31);
                sb.append("Unknown message id: ");
                sb.append(i);
                Log.w("GACStateManager", sb.toString());
                return;
        }
    }
}
