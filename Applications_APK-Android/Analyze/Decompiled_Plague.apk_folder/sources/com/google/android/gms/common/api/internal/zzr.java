package com.google.android.gms.common.api.internal;

import android.app.Dialog;

final class zzr extends zzcb {
    private /* synthetic */ Dialog zzflw;
    private /* synthetic */ zzq zzflx;

    zzr(zzq zzq, Dialog dialog) {
        this.zzflx = zzq;
        this.zzflw = dialog;
    }

    public final void zzagu() {
        this.zzflx.zzflv.zzagr();
        if (this.zzflw.isShowing()) {
            this.zzflw.dismiss();
        }
    }
}
