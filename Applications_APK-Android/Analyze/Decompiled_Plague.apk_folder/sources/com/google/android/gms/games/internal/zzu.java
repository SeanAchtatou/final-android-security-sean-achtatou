package com.google.android.gms.games.internal;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzu extends IInterface {
    zzy zzare() throws RemoteException;
}
