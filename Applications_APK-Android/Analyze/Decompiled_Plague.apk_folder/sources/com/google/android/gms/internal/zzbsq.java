package com.google.android.gms.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.internal.zzg;

final class zzbsq implements zzg {
    zzbsq() {
    }

    public final String zzapc() {
        return "customPropertiesExtraHolder";
    }

    public final void zzc(DataHolder dataHolder) {
        zzbsp.zzd(dataHolder);
    }
}
