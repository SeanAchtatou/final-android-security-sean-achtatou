package com.google.android.gms.internal;

import java.util.concurrent.atomic.AtomicReference;

public abstract class zzcae {
    private final AtomicReference<zzcac> zzhrj = new AtomicReference<>();

    public final void flush() {
        zzcac zzcac = this.zzhrj.get();
        if (zzcac != null) {
            zzcac.flush();
        }
    }

    /* access modifiers changed from: protected */
    public abstract zzcac zzast();

    public final void zzp(String str, int i) {
        zzcac zzcac = this.zzhrj.get();
        if (zzcac == null) {
            zzcac = zzast();
            if (!this.zzhrj.compareAndSet(null, zzcac)) {
                zzcac = this.zzhrj.get();
            }
        }
        zzcac.zzv(str, i);
    }
}
