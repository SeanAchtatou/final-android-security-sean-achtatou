package com.google.android.gms.internal;

import android.os.Parcelable;

public final class zzaab implements Parcelable.Creator<zzzz> {
    /* JADX WARN: Type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v5, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v6, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v7, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v8, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v9, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r64) {
        /*
            r63 = this;
            r0 = r64
            int r1 = com.google.android.gms.internal.zzbek.zzd(r64)
            r2 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r28 = r2
            r35 = r28
            r26 = r4
            r38 = r26
            r8 = r5
            r20 = r8
            r23 = r20
            r24 = r23
            r25 = r24
            r39 = r25
            r40 = r39
            r41 = r40
            r42 = r41
            r43 = r42
            r46 = r43
            r47 = r46
            r51 = r47
            r56 = r51
            r60 = r56
            r61 = r60
            r62 = r61
            r9 = r6
            r10 = r9
            r11 = r10
            r12 = r11
            r13 = r12
            r14 = r13
            r15 = r14
            r16 = r15
            r17 = r16
            r18 = r17
            r19 = r18
            r21 = r19
            r22 = r21
            r27 = r22
            r30 = r27
            r31 = r30
            r32 = r31
            r33 = r32
            r34 = r33
            r37 = r34
            r44 = r37
            r45 = r44
            r48 = r45
            r49 = r48
            r50 = r49
            r52 = r50
            r53 = r52
            r54 = r53
            r55 = r54
            r57 = r55
            r58 = r57
            r59 = r58
        L_0x006d:
            int r2 = r64.dataPosition()
            if (r2 >= r1) goto L_0x01d2
            int r2 = r64.readInt()
            r3 = 65535(0xffff, float:9.1834E-41)
            r3 = r3 & r2
            switch(r3) {
                case 1: goto L_0x01cc;
                case 2: goto L_0x01c6;
                case 3: goto L_0x01bb;
                case 4: goto L_0x01b0;
                case 5: goto L_0x01aa;
                case 6: goto L_0x019f;
                case 7: goto L_0x0194;
                case 8: goto L_0x018e;
                case 9: goto L_0x0188;
                case 10: goto L_0x0182;
                case 11: goto L_0x0176;
                case 12: goto L_0x0170;
                case 13: goto L_0x016a;
                case 14: goto L_0x0164;
                case 15: goto L_0x015e;
                case 16: goto L_0x0158;
                case 17: goto L_0x007e;
                case 18: goto L_0x0152;
                case 19: goto L_0x014c;
                case 20: goto L_0x0146;
                case 21: goto L_0x0140;
                case 22: goto L_0x007e;
                case 23: goto L_0x007e;
                case 24: goto L_0x007e;
                case 25: goto L_0x013a;
                case 26: goto L_0x0134;
                case 27: goto L_0x012e;
                case 28: goto L_0x0128;
                case 29: goto L_0x011c;
                case 30: goto L_0x0116;
                case 31: goto L_0x0110;
                case 32: goto L_0x007e;
                case 33: goto L_0x010a;
                case 34: goto L_0x0104;
                case 35: goto L_0x00fe;
                case 36: goto L_0x00f8;
                case 37: goto L_0x00f2;
                case 38: goto L_0x00ec;
                case 39: goto L_0x00e7;
                case 40: goto L_0x00e2;
                case 41: goto L_0x00dd;
                case 42: goto L_0x00d8;
                case 43: goto L_0x00d3;
                case 44: goto L_0x00ce;
                case 45: goto L_0x00c9;
                case 46: goto L_0x00be;
                case 47: goto L_0x00b9;
                case 48: goto L_0x00b4;
                case 49: goto L_0x00af;
                case 50: goto L_0x00aa;
                case 51: goto L_0x00a5;
                case 52: goto L_0x00a0;
                case 53: goto L_0x009b;
                case 54: goto L_0x0096;
                case 55: goto L_0x0091;
                case 56: goto L_0x008c;
                case 57: goto L_0x0087;
                case 58: goto L_0x0082;
                default: goto L_0x007e;
            }
        L_0x007e:
            com.google.android.gms.internal.zzbek.zzb(r0, r2)
            goto L_0x006d
        L_0x0082:
            boolean r62 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x006d
        L_0x0087:
            boolean r61 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x006d
        L_0x008c:
            int r60 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x006d
        L_0x0091:
            java.util.ArrayList r59 = com.google.android.gms.internal.zzbek.zzac(r0, r2)
            goto L_0x006d
        L_0x0096:
            java.lang.String r58 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x009b:
            java.util.ArrayList r57 = com.google.android.gms.internal.zzbek.zzab(r0, r2)
            goto L_0x006d
        L_0x00a0:
            boolean r56 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x006d
        L_0x00a5:
            java.lang.String r55 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x00aa:
            java.lang.String r54 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x00af:
            java.lang.String r53 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x00b4:
            android.os.Bundle r52 = com.google.android.gms.internal.zzbek.zzs(r0, r2)
            goto L_0x006d
        L_0x00b9:
            boolean r51 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x006d
        L_0x00be:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzla> r3 = com.google.android.gms.internal.zzla.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r50 = r2
            com.google.android.gms.internal.zzla r50 = (com.google.android.gms.internal.zzla) r50
            goto L_0x006d
        L_0x00c9:
            java.lang.String r49 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x00ce:
            android.os.Bundle r48 = com.google.android.gms.internal.zzbek.zzs(r0, r2)
            goto L_0x006d
        L_0x00d3:
            int r47 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x006d
        L_0x00d8:
            boolean r46 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x006d
        L_0x00dd:
            java.lang.String r45 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x00e2:
            boolean r39 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x006d
        L_0x00e7:
            java.lang.String r44 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x00ec:
            boolean r43 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x006d
        L_0x00f2:
            boolean r42 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x006d
        L_0x00f8:
            int r41 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x006d
        L_0x00fe:
            int r40 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x006d
        L_0x0104:
            float r38 = com.google.android.gms.internal.zzbek.zzl(r0, r2)
            goto L_0x006d
        L_0x010a:
            java.lang.String r37 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x0110:
            long r35 = com.google.android.gms.internal.zzbek.zzi(r0, r2)
            goto L_0x006d
        L_0x0116:
            java.util.ArrayList r34 = com.google.android.gms.internal.zzbek.zzac(r0, r2)
            goto L_0x006d
        L_0x011c:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzom> r3 = com.google.android.gms.internal.zzom.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r33 = r2
            com.google.android.gms.internal.zzom r33 = (com.google.android.gms.internal.zzom) r33
            goto L_0x006d
        L_0x0128:
            java.lang.String r32 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x012e:
            java.util.ArrayList r31 = com.google.android.gms.internal.zzbek.zzac(r0, r2)
            goto L_0x006d
        L_0x0134:
            java.lang.String r30 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x013a:
            long r28 = com.google.android.gms.internal.zzbek.zzi(r0, r2)
            goto L_0x006d
        L_0x0140:
            java.lang.String r27 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x0146:
            float r26 = com.google.android.gms.internal.zzbek.zzl(r0, r2)
            goto L_0x006d
        L_0x014c:
            int r25 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x006d
        L_0x0152:
            int r24 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x006d
        L_0x0158:
            boolean r23 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x006d
        L_0x015e:
            android.os.Bundle r22 = com.google.android.gms.internal.zzbek.zzs(r0, r2)
            goto L_0x006d
        L_0x0164:
            java.util.ArrayList r21 = com.google.android.gms.internal.zzbek.zzac(r0, r2)
            goto L_0x006d
        L_0x016a:
            int r20 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x006d
        L_0x0170:
            android.os.Bundle r19 = com.google.android.gms.internal.zzbek.zzs(r0, r2)
            goto L_0x006d
        L_0x0176:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzaiy> r3 = com.google.android.gms.internal.zzaiy.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r18 = r2
            com.google.android.gms.internal.zzaiy r18 = (com.google.android.gms.internal.zzaiy) r18
            goto L_0x006d
        L_0x0182:
            java.lang.String r17 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x0188:
            java.lang.String r16 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x018e:
            java.lang.String r15 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x0194:
            android.os.Parcelable$Creator r3 = android.content.pm.PackageInfo.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r14 = r2
            android.content.pm.PackageInfo r14 = (android.content.pm.PackageInfo) r14
            goto L_0x006d
        L_0x019f:
            android.os.Parcelable$Creator r3 = android.content.pm.ApplicationInfo.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r13 = r2
            android.content.pm.ApplicationInfo r13 = (android.content.pm.ApplicationInfo) r13
            goto L_0x006d
        L_0x01aa:
            java.lang.String r12 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x006d
        L_0x01b0:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zziw> r3 = com.google.android.gms.internal.zziw.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r11 = r2
            com.google.android.gms.internal.zziw r11 = (com.google.android.gms.internal.zziw) r11
            goto L_0x006d
        L_0x01bb:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzis> r3 = com.google.android.gms.internal.zzis.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r10 = r2
            com.google.android.gms.internal.zzis r10 = (com.google.android.gms.internal.zzis) r10
            goto L_0x006d
        L_0x01c6:
            android.os.Bundle r9 = com.google.android.gms.internal.zzbek.zzs(r0, r2)
            goto L_0x006d
        L_0x01cc:
            int r8 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x006d
        L_0x01d2:
            com.google.android.gms.internal.zzbek.zzaf(r0, r1)
            com.google.android.gms.internal.zzzz r0 = new com.google.android.gms.internal.zzzz
            r7 = r0
            r7.<init>(r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r30, r31, r32, r33, r34, r35, r37, r38, r39, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaab.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzzz[i];
    }
}
