package com.google.android.gms.tasks;

import android.support.annotation.NonNull;
import java.util.concurrent.Executor;

final class zza<TResult, TContinuationResult> implements zzk<TResult> {
    private final Executor zzkcc;
    /* access modifiers changed from: private */
    public final Continuation<TResult, TContinuationResult> zzkrf;
    /* access modifiers changed from: private */
    public final zzn<TContinuationResult> zzkrg;

    public zza(@NonNull Executor executor, @NonNull Continuation<TResult, TContinuationResult> continuation, @NonNull zzn<TContinuationResult> zzn) {
        this.zzkcc = executor;
        this.zzkrf = continuation;
        this.zzkrg = zzn;
    }

    public final void cancel() {
        throw new UnsupportedOperationException();
    }

    public final void onComplete(@NonNull Task<TResult> task) {
        this.zzkcc.execute(new zzb(this, task));
    }
}
