package com.google.android.gms.ads.internal.js;

import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.common.util.zzr;

final /* synthetic */ class zzh implements zzr {
    private final zzt zzbzc;

    zzh(zzt zzt) {
        this.zzbzc = zzt;
    }

    public final boolean apply(Object obj) {
        zzt zzt = (zzt) obj;
        return (zzt instanceof zzn) && zzn.zza((zzn) zzt).equals(this.zzbzc);
    }
}
