package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbq extends zzac<Integer> {
    private /* synthetic */ zzcl zzgmh;
    private /* synthetic */ byte[] zzhkf;
    private /* synthetic */ String zzhkg;
    private /* synthetic */ String zzhkh;

    zzbq(RealTimeMultiplayerClient realTimeMultiplayerClient, zzcl zzcl, byte[] bArr, String str, String str2) {
        this.zzgmh = zzcl;
        this.zzhkf = bArr;
        this.zzhkg = str;
        this.zzhkh = str2;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<Integer> taskCompletionSource) throws RemoteException {
        Integer valueOf = Integer.valueOf(gamesClientImpl.zza(this.zzgmh, this.zzhkf, this.zzhkg, this.zzhkh));
        if (valueOf.intValue() == -1) {
            taskCompletionSource.trySetException(zzb.zzy(GamesClientStatusCodes.zzdg(GamesClientStatusCodes.REAL_TIME_MESSAGE_SEND_FAILED)));
        } else {
            taskCompletionSource.setResult(valueOf);
        }
    }
}
