package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.common.api.internal.zzcp;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.internal.api.zzp;
import com.google.android.gms.games.internal.zzg;
import com.google.android.gms.games.internal.zzn;
import com.google.android.gms.games.internal.zzo;
import com.google.android.gms.games.multiplayer.ParticipantResult;
import com.google.android.gms.games.multiplayer.turnbased.LoadMatchesResponse;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchUpdateCallback;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;
import com.google.android.gms.tasks.Task;
import java.util.List;

public class TurnBasedMultiplayerClient extends zzp {
    private static final zzn<TurnBasedMatch> zzhlj = new zzcu();
    private static final zzbo<TurnBasedMultiplayer.LoadMatchesResult, LoadMatchesResponse> zzhlk = new zzcd();
    private static final zzo<TurnBasedMultiplayer.LoadMatchesResult> zzhll = new zzce();
    private static final zzbo<TurnBasedMultiplayer.LoadMatchResult, TurnBasedMatch> zzhlm = new zzcf();
    private static final zzbo<TurnBasedMultiplayer.CancelMatchResult, String> zzhln = new zzcg();
    private static final com.google.android.gms.games.internal.zzp zzhlo = new zzch();
    private static final zzbo<TurnBasedMultiplayer.LeaveMatchResult, Void> zzhlp = new zzci();
    private static final zzbo<TurnBasedMultiplayer.LeaveMatchResult, TurnBasedMatch> zzhlq = new zzcj();
    private static final com.google.android.gms.games.internal.zzp zzhlr = new zzck();
    private static final zzbo<TurnBasedMultiplayer.UpdateMatchResult, TurnBasedMatch> zzhls = new zzcl();
    private static final zzbo<TurnBasedMultiplayer.InitiateMatchResult, TurnBasedMatch> zzhlt = new zzcm();

    public static class MatchOutOfDateApiException extends ApiException {
        protected final TurnBasedMatch match;

        MatchOutOfDateApiException(@NonNull Status status, @NonNull TurnBasedMatch turnBasedMatch) {
            super(status);
            this.match = turnBasedMatch;
        }

        public TurnBasedMatch getMatch() {
            return this.match;
        }
    }

    TurnBasedMultiplayerClient(@NonNull Activity activity, @NonNull Games.GamesOptions gamesOptions) {
        super(activity, gamesOptions);
    }

    TurnBasedMultiplayerClient(@NonNull Context context, @NonNull Games.GamesOptions gamesOptions) {
        super(context, gamesOptions);
    }

    private static Task<Void> zzf(@NonNull PendingResult<TurnBasedMultiplayer.LeaveMatchResult> pendingResult) {
        return zzg.zza(pendingResult, zzhlo, zzhlp, zzhlq, zzhlj);
    }

    private static Task<TurnBasedMatch> zzg(@NonNull PendingResult<TurnBasedMultiplayer.UpdateMatchResult> pendingResult) {
        return zzg.zza(pendingResult, zzhlr, zzhls, zzhls, zzhlj);
    }

    public Task<TurnBasedMatch> acceptInvitation(@NonNull String str) {
        return zzg.zza(Games.TurnBasedMultiplayer.acceptInvitation(zzagb(), str), zzhlt);
    }

    public Task<String> cancelMatch(@NonNull String str) {
        return zzg.zza(Games.TurnBasedMultiplayer.cancelMatch(zzagb(), str), zzhln);
    }

    public Task<TurnBasedMatch> createMatch(@NonNull TurnBasedMatchConfig turnBasedMatchConfig) {
        return zzg.zza(Games.TurnBasedMultiplayer.createMatch(zzagb(), turnBasedMatchConfig), zzhlt);
    }

    public Task<Void> declineInvitation(@NonNull String str) {
        return zzb(new zzcq(this, str));
    }

    public Task<Void> dismissInvitation(@NonNull String str) {
        return zzb(new zzcr(this, str));
    }

    public Task<Void> dismissMatch(@NonNull String str) {
        return zzb(new zzct(this, str));
    }

    public Task<TurnBasedMatch> finishMatch(@NonNull String str) {
        return zzg(Games.TurnBasedMultiplayer.finishMatch(zzagb(), str));
    }

    public Task<TurnBasedMatch> finishMatch(@NonNull String str, @Nullable byte[] bArr, @Nullable List<ParticipantResult> list) {
        return zzg(Games.TurnBasedMultiplayer.finishMatch(zzagb(), str, bArr, list));
    }

    public Task<TurnBasedMatch> finishMatch(@NonNull String str, @Nullable byte[] bArr, @Nullable ParticipantResult... participantResultArr) {
        return zzg(Games.TurnBasedMultiplayer.finishMatch(zzagb(), str, bArr, participantResultArr));
    }

    public Task<Intent> getInboxIntent() {
        return zza(new zzcc(this));
    }

    public Task<Integer> getMaxMatchDataSize() {
        return zza(new zzcs(this));
    }

    public Task<Intent> getSelectOpponentsIntent(@IntRange(from = 1) int i, @IntRange(from = 1) int i2) {
        return getSelectOpponentsIntent(i, i2, true);
    }

    public Task<Intent> getSelectOpponentsIntent(@IntRange(from = 1) int i, @IntRange(from = 1) int i2, boolean z) {
        return zza(new zzcp(this, i, i2, z));
    }

    public Task<Void> leaveMatch(@NonNull String str) {
        return zzf(Games.TurnBasedMultiplayer.leaveMatch(zzagb(), str));
    }

    public Task<Void> leaveMatchDuringTurn(@NonNull String str, @Nullable String str2) {
        return zzf(Games.TurnBasedMultiplayer.leaveMatchDuringTurn(zzagb(), str, str2));
    }

    public Task<AnnotatedData<TurnBasedMatch>> loadMatch(@NonNull String str) {
        return zzg.zzb(Games.TurnBasedMultiplayer.loadMatch(zzagb(), str), zzhlm);
    }

    public Task<AnnotatedData<LoadMatchesResponse>> loadMatchesByStatus(int i, @NonNull int[] iArr) {
        return zzg.zza(Games.TurnBasedMultiplayer.loadMatchesByStatus(zzagb(), i, iArr), zzhlk, zzhll);
    }

    public Task<AnnotatedData<LoadMatchesResponse>> loadMatchesByStatus(@NonNull int[] iArr) {
        return zzg.zza(Games.TurnBasedMultiplayer.loadMatchesByStatus(zzagb(), iArr), zzhlk, zzhll);
    }

    public Task<Void> registerTurnBasedMatchUpdateCallback(@NonNull TurnBasedMatchUpdateCallback turnBasedMatchUpdateCallback) {
        zzcl zza = zza(turnBasedMatchUpdateCallback, TurnBasedMatchUpdateCallback.class.getSimpleName());
        return zza(new zzcn(this, zza, zza), new zzco(this, zza.zzajc()));
    }

    public Task<TurnBasedMatch> rematch(@NonNull String str) {
        return zzg.zza(Games.TurnBasedMultiplayer.rematch(zzagb(), str), zzhlt);
    }

    public Task<TurnBasedMatch> takeTurn(@NonNull String str, @Nullable byte[] bArr, @Nullable String str2) {
        return zzg(Games.TurnBasedMultiplayer.takeTurn(zzagb(), str, bArr, str2));
    }

    public Task<TurnBasedMatch> takeTurn(@NonNull String str, @Nullable byte[] bArr, @Nullable String str2, @Nullable List<ParticipantResult> list) {
        return zzg(Games.TurnBasedMultiplayer.takeTurn(zzagb(), str, bArr, str2, list));
    }

    public Task<TurnBasedMatch> takeTurn(@NonNull String str, @Nullable byte[] bArr, @Nullable String str2, @Nullable ParticipantResult... participantResultArr) {
        return zzg(Games.TurnBasedMultiplayer.takeTurn(zzagb(), str, bArr, str2, participantResultArr));
    }

    public Task<Boolean> unregisterTurnBasedMatchUpdateCallback(@NonNull TurnBasedMatchUpdateCallback turnBasedMatchUpdateCallback) {
        return zza(zzcp.zzb(turnBasedMatchUpdateCallback, TurnBasedMatchUpdateCallback.class.getSimpleName()));
    }
}
