package com.google.android.gms.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.DriveSpace;
import com.google.android.gms.drive.metadata.internal.zzl;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public final class zzbsl extends zzl<DriveSpace> {
    public zzbsl(int i) {
        super("spaces", Arrays.asList("inDriveSpace", "isAppData", "inGooglePhotosSpace"), Collections.emptySet(), 7000000);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzc(DataHolder dataHolder, int i, int i2) {
        return zzc(dataHolder, i, i2);
    }

    /* access modifiers changed from: protected */
    public final Collection<DriveSpace> zzd(DataHolder dataHolder, int i, int i2) {
        ArrayList arrayList = new ArrayList();
        if (dataHolder.zze("inDriveSpace", i, i2)) {
            arrayList.add(DriveSpace.zzggu);
        }
        if (dataHolder.zze("isAppData", i, i2)) {
            arrayList.add(DriveSpace.zzggv);
        }
        if (dataHolder.zze("inGooglePhotosSpace", i, i2)) {
            arrayList.add(DriveSpace.zzggw);
        }
        return arrayList;
    }
}
