package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.drive.events.ListenerToken;
import com.google.android.gms.drive.events.OpenFileCallback;

final class zzbob extends zzbjv {
    private /* synthetic */ zzbmu zzgmi;
    private final ListenerToken zzgmy;
    private final zzcl<OpenFileCallback> zzgmz;

    zzbob(zzbmu zzbmu, ListenerToken listenerToken, zzcl<OpenFileCallback> zzcl) {
        this.zzgmi = zzbmu;
        this.zzgmy = listenerToken;
        this.zzgmz = zzcl;
    }

    private final void zza(zzbnx<OpenFileCallback> zzbnx) {
        this.zzgmz.zza(new zzbof(this, zzbnx));
    }

    public final void onError(Status status) throws RemoteException {
        zza(new zzboc(this, status));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(Status status, OpenFileCallback openFileCallback) {
        openFileCallback.onError(zzb.zzy(status));
        this.zzgmi.cancelOpenFileCallback(this.zzgmy);
    }

    public final void zza(zzbps zzbps) throws RemoteException {
        zza(new zzboe(this, zzbps));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(zzbps zzbps, OpenFileCallback openFileCallback) {
        openFileCallback.onContents(new zzblv(zzbps.zzglf));
        this.zzgmi.cancelOpenFileCallback(this.zzgmy);
    }

    public final void zza(zzbpw zzbpw) throws RemoteException {
        zza(new zzbod(zzbpw));
    }
}
