package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import java.util.Locale;

public final class ChangeEvent extends zzbej implements ResourceEvent {
    public static final Parcelable.Creator<ChangeEvent> CREATOR = new zza();
    private DriveId zzgfy;
    private int zzgig;

    public ChangeEvent(DriveId driveId, int i) {
        this.zzgfy = driveId;
        this.zzgig = i;
    }

    public final DriveId getDriveId() {
        return this.zzgfy;
    }

    public final int getType() {
        return 1;
    }

    public final boolean hasBeenDeleted() {
        return (this.zzgig & 4) != 0;
    }

    public final boolean hasContentChanged() {
        return (this.zzgig & 2) != 0;
    }

    public final boolean hasMetadataChanged() {
        return (this.zzgig & 1) != 0;
    }

    public final String toString() {
        return String.format(Locale.US, "ChangeEvent [id=%s,changeFlags=%x]", this.zzgfy, Integer.valueOf(this.zzgig));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.DriveId, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, (Parcelable) this.zzgfy, i, false);
        zzbem.zzc(parcel, 3, this.zzgig);
        zzbem.zzai(parcel, zze);
    }
}
