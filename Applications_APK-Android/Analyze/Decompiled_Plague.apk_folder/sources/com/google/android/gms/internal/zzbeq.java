package com.google.android.gms.internal;

import com.google.android.gms.common.api.Api;

public final class zzbeq {
    public static final Api<Api.ApiOptions.NoOptions> API = new Api<>("Common.API", zzdyi, zzdyh);
    public static final Api.zzf<zzbey> zzdyh = new Api.zzf<>();
    private static final Api.zza<zzbey, Api.ApiOptions.NoOptions> zzdyi = new zzber();
    public static final zzbes zzfzb = new zzbet();
}
