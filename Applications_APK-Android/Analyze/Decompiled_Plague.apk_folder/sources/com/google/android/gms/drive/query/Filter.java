package com.google.android.gms.drive.query;

import com.google.android.gms.drive.query.internal.zzj;
import com.google.android.gms.internal.zzben;

public interface Filter extends zzben {
    <T> T zza(zzj<T> zzj);
}
