package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.ads.mediation.admob.AdMobAdapter;
import java.util.Iterator;
import java.util.List;

@zzzb
public final class zzxl extends zzxb {
    private final zznd zzamo;
    private zzuc zzanb;
    /* access modifiers changed from: private */
    public final zzama zzbwq;
    private zztn zzccq;
    private zztl zzcih;
    protected zztt zzcii;
    /* access modifiers changed from: private */
    public boolean zzcij;

    zzxl(Context context, zzaev zzaev, zzuc zzuc, zzxg zzxg, zznd zznd, zzama zzama) {
        super(context, zzaev, zzxg);
        this.zzanb = zzuc;
        this.zzccq = zzaev.zzcvs;
        this.zzamo = zznd;
        this.zzbwq = zzama;
    }

    private static String zzj(List<zztt> list) {
        String str = "";
        if (list == null) {
            return str.toString();
        }
        Iterator<zztt> it = list.iterator();
        while (true) {
            int i = 0;
            if (!it.hasNext()) {
                return str.substring(0, Math.max(0, str.length() - 1));
            }
            zztt next = it.next();
            if (!(next == null || next.zzcdd == null || TextUtils.isEmpty(next.zzcdd.zzcbe))) {
                String valueOf = String.valueOf(str);
                String str2 = next.zzcdd.zzcbe;
                switch (next.zzcdc) {
                    case -1:
                        i = 4;
                        break;
                    case 0:
                        break;
                    case 1:
                        i = 1;
                        break;
                    case 2:
                    default:
                        i = 6;
                        break;
                    case 3:
                        i = 2;
                        break;
                    case 4:
                        i = 3;
                        break;
                    case 5:
                        i = 5;
                        break;
                }
                long j = next.zzcdi;
                StringBuilder sb = new StringBuilder(33 + String.valueOf(str2).length());
                sb.append(str2);
                sb.append(".");
                sb.append(i);
                sb.append(".");
                sb.append(j);
                String sb2 = sb.toString();
                StringBuilder sb3 = new StringBuilder(1 + String.valueOf(valueOf).length() + String.valueOf(sb2).length());
                sb3.append(valueOf);
                sb3.append(sb2);
                sb3.append("_");
                str = sb3.toString();
            }
        }
    }

    public final void onStop() {
        synchronized (this.zzchy) {
            super.onStop();
            if (this.zzcih != null) {
                this.zzcih.cancel();
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v3, types: [com.google.android.gms.internal.zztl] */
    /* JADX WARN: Type inference failed for: r18v2, types: [com.google.android.gms.internal.zztz] */
    /* JADX WARN: Type inference failed for: r5v5, types: [com.google.android.gms.internal.zztw] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzd(long r33) throws com.google.android.gms.internal.zzxe {
        /*
            r32 = this;
            r1 = r32
            java.lang.Object r2 = r1.zzchy
            monitor-enter(r2)
            com.google.android.gms.internal.zztn r3 = r1.zzccq     // Catch:{ all -> 0x0177 }
            int r3 = r3.zzcce     // Catch:{ all -> 0x0177 }
            r4 = -1
            if (r3 == r4) goto L_0x0043
            com.google.android.gms.internal.zztw r3 = new com.google.android.gms.internal.zztw     // Catch:{ all -> 0x0177 }
            android.content.Context r6 = r1.mContext     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzaev r4 = r1.zzchv     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzzz r7 = r4.zzcpe     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzuc r8 = r1.zzanb     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zztn r9 = r1.zzccq     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzaad r4 = r1.zzchw     // Catch:{ all -> 0x0177 }
            boolean r10 = r4.zzbdd     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzaad r4 = r1.zzchw     // Catch:{ all -> 0x0177 }
            boolean r11 = r4.zzbdf     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzaad r4 = r1.zzchw     // Catch:{ all -> 0x0177 }
            java.lang.String r12 = r4.zzcoa     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzmg<java.lang.Long> r4 = com.google.android.gms.internal.zzmq.zzblt     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzmo r5 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ all -> 0x0177 }
            java.lang.Object r4 = r5.zzd(r4)     // Catch:{ all -> 0x0177 }
            java.lang.Long r4 = (java.lang.Long) r4     // Catch:{ all -> 0x0177 }
            long r15 = r4.longValue()     // Catch:{ all -> 0x0177 }
            r17 = 2
            com.google.android.gms.internal.zzaev r4 = r1.zzchv     // Catch:{ all -> 0x0177 }
            boolean r4 = r4.zzcwd     // Catch:{ all -> 0x0177 }
            r5 = r3
            r13 = r33
            r18 = r4
            r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r15, r17, r18)     // Catch:{ all -> 0x0177 }
            goto L_0x008a
        L_0x0043:
            com.google.android.gms.internal.zztz r3 = new com.google.android.gms.internal.zztz     // Catch:{ all -> 0x0177 }
            android.content.Context r4 = r1.mContext     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzaev r5 = r1.zzchv     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzzz r5 = r5.zzcpe     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzuc r6 = r1.zzanb     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zztn r7 = r1.zzccq     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzaad r8 = r1.zzchw     // Catch:{ all -> 0x0177 }
            boolean r8 = r8.zzbdd     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzaad r9 = r1.zzchw     // Catch:{ all -> 0x0177 }
            boolean r9 = r9.zzbdf     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzaad r10 = r1.zzchw     // Catch:{ all -> 0x0177 }
            java.lang.String r10 = r10.zzcoa     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzmg<java.lang.Long> r11 = com.google.android.gms.internal.zzmq.zzblt     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzmo r12 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ all -> 0x0177 }
            java.lang.Object r11 = r12.zzd(r11)     // Catch:{ all -> 0x0177 }
            java.lang.Long r11 = (java.lang.Long) r11     // Catch:{ all -> 0x0177 }
            long r28 = r11.longValue()     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zznd r11 = r1.zzamo     // Catch:{ all -> 0x0177 }
            com.google.android.gms.internal.zzaev r12 = r1.zzchv     // Catch:{ all -> 0x0177 }
            boolean r12 = r12.zzcwd     // Catch:{ all -> 0x0177 }
            r18 = r3
            r19 = r4
            r20 = r5
            r21 = r6
            r22 = r7
            r23 = r8
            r24 = r9
            r25 = r10
            r26 = r33
            r30 = r11
            r31 = r12
            r18.<init>(r19, r20, r21, r22, r23, r24, r25, r26, r28, r30, r31)     // Catch:{ all -> 0x0177 }
        L_0x008a:
            r1.zzcih = r3     // Catch:{ all -> 0x0177 }
            monitor-exit(r2)     // Catch:{ all -> 0x0177 }
            java.util.ArrayList r2 = new java.util.ArrayList
            com.google.android.gms.internal.zztn r3 = r1.zzccq
            java.util.List<com.google.android.gms.internal.zztm> r3 = r3.zzcbt
            r2.<init>(r3)
            com.google.android.gms.internal.zzaev r3 = r1.zzchv
            com.google.android.gms.internal.zzzz r3 = r3.zzcpe
            com.google.android.gms.internal.zzis r3 = r3.zzclo
            android.os.Bundle r3 = r3.zzbcf
            java.lang.String r4 = "com.google.ads.mediation.admob.AdMobAdapter"
            r5 = 0
            if (r3 == 0) goto L_0x00b0
            android.os.Bundle r3 = r3.getBundle(r4)
            if (r3 == 0) goto L_0x00b0
            java.lang.String r6 = "_skipMediation"
            boolean r3 = r3.getBoolean(r6)
            goto L_0x00b1
        L_0x00b0:
            r3 = r5
        L_0x00b1:
            if (r3 == 0) goto L_0x00cf
            java.util.ListIterator r3 = r2.listIterator()
        L_0x00b7:
            boolean r6 = r3.hasNext()
            if (r6 == 0) goto L_0x00cf
            java.lang.Object r6 = r3.next()
            com.google.android.gms.internal.zztm r6 = (com.google.android.gms.internal.zztm) r6
            java.util.List<java.lang.String> r6 = r6.zzcbd
            boolean r6 = r6.contains(r4)
            if (r6 != 0) goto L_0x00b7
            r3.remove()
            goto L_0x00b7
        L_0x00cf:
            com.google.android.gms.internal.zztl r3 = r1.zzcih
            com.google.android.gms.internal.zztt r2 = r3.zzg(r2)
            r1.zzcii = r2
            com.google.android.gms.internal.zztt r2 = r1.zzcii
            int r2 = r2.zzcdc
            switch(r2) {
                case 0: goto L_0x0104;
                case 1: goto L_0x00fb;
                default: goto L_0x00de;
            }
        L_0x00de:
            com.google.android.gms.internal.zzxe r2 = new com.google.android.gms.internal.zzxe
            com.google.android.gms.internal.zztt r3 = r1.zzcii
            int r3 = r3.zzcdc
            r4 = 40
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>(r4)
            java.lang.String r4 = "Unexpected mediation result: "
            r6.append(r4)
            r6.append(r3)
            java.lang.String r3 = r6.toString()
            r2.<init>(r3, r5)
            throw r2
        L_0x00fb:
            com.google.android.gms.internal.zzxe r2 = new com.google.android.gms.internal.zzxe
            java.lang.String r3 = "No fill from any mediation ad networks."
            r4 = 3
            r2.<init>(r3, r4)
            throw r2
        L_0x0104:
            com.google.android.gms.internal.zztt r2 = r1.zzcii
            com.google.android.gms.internal.zztm r2 = r2.zzcdd
            if (r2 == 0) goto L_0x0176
            com.google.android.gms.internal.zztt r2 = r1.zzcii
            com.google.android.gms.internal.zztm r2 = r2.zzcdd
            java.lang.String r2 = r2.zzcbn
            if (r2 == 0) goto L_0x0176
            java.util.concurrent.CountDownLatch r2 = new java.util.concurrent.CountDownLatch
            r3 = 1
            r2.<init>(r3)
            android.os.Handler r3 = com.google.android.gms.internal.zzagr.zzczc
            com.google.android.gms.internal.zzxm r4 = new com.google.android.gms.internal.zzxm
            r4.<init>(r1, r2)
            r3.post(r4)
            r3 = 10
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ InterruptedException -> 0x014e }
            r2.await(r3, r6)     // Catch:{ InterruptedException -> 0x014e }
            java.lang.Object r2 = r1.zzchy
            monitor-enter(r2)
            boolean r3 = r1.zzcij     // Catch:{ all -> 0x014a }
            if (r3 != 0) goto L_0x0138
            com.google.android.gms.internal.zzxe r3 = new com.google.android.gms.internal.zzxe     // Catch:{ all -> 0x014a }
            java.lang.String r4 = "View could not be prepared"
            r3.<init>(r4, r5)     // Catch:{ all -> 0x014a }
            throw r3     // Catch:{ all -> 0x014a }
        L_0x0138:
            com.google.android.gms.internal.zzama r3 = r1.zzbwq     // Catch:{ all -> 0x014a }
            boolean r3 = r3.isDestroyed()     // Catch:{ all -> 0x014a }
            if (r3 == 0) goto L_0x0148
            com.google.android.gms.internal.zzxe r3 = new com.google.android.gms.internal.zzxe     // Catch:{ all -> 0x014a }
            java.lang.String r4 = "Assets not loaded, web view is destroyed"
            r3.<init>(r4, r5)     // Catch:{ all -> 0x014a }
            throw r3     // Catch:{ all -> 0x014a }
        L_0x0148:
            monitor-exit(r2)     // Catch:{ all -> 0x014a }
            return
        L_0x014a:
            r0 = move-exception
            r3 = r0
            monitor-exit(r2)     // Catch:{ all -> 0x014a }
            throw r3
        L_0x014e:
            r0 = move-exception
            r2 = r0
            com.google.android.gms.internal.zzxe r3 = new com.google.android.gms.internal.zzxe
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r4 = 38
            java.lang.String r6 = java.lang.String.valueOf(r2)
            int r6 = r6.length()
            int r4 = r4 + r6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>(r4)
            java.lang.String r4 = "Interrupted while waiting for latch : "
            r6.append(r4)
            r6.append(r2)
            java.lang.String r2 = r6.toString()
            r3.<init>(r2, r5)
            throw r3
        L_0x0176:
            return
        L_0x0177:
            r0 = move-exception
            r3 = r0
            monitor-exit(r2)     // Catch:{ all -> 0x0177 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzxl.zzd(long):void");
    }

    /* access modifiers changed from: protected */
    public final zzaeu zzy(int i) {
        zzzz zzzz = this.zzchv.zzcpe;
        zzis zzis = zzzz.zzclo;
        zzama zzama = this.zzbwq;
        List<String> list = this.zzchw.zzcbv;
        List<String> list2 = this.zzchw.zzcbw;
        List<String> list3 = this.zzchw.zzcni;
        int i2 = this.zzchw.orientation;
        long j = this.zzchw.zzccb;
        String str = zzzz.zzclr;
        boolean z = this.zzchw.zzcng;
        zztm zztm = this.zzcii != null ? this.zzcii.zzcdd : null;
        zzuf zzuf = this.zzcii != null ? this.zzcii.zzcde : null;
        return new zzaeu(zzis, zzama, list, i, list2, list3, i2, j, str, z, zztm, zzuf, this.zzcii != null ? this.zzcii.zzcdf : AdMobAdapter.class.getName(), this.zzccq, this.zzcii != null ? this.zzcii.zzcdg : null, this.zzchw.zzcnh, this.zzchv.zzath, this.zzchw.zzcnf, this.zzchv.zzcvw, this.zzchw.zzcnk, this.zzchw.zzcnl, this.zzchv.zzcvq, null, this.zzchw.zzcnv, this.zzchw.zzcnw, this.zzchw.zzcnx, this.zzccq != null ? this.zzccq.zzccg : false, this.zzchw.zzcnz, this.zzcih != null ? zzj(this.zzcih.zzlo()) : null, this.zzchw.zzcby, this.zzchw.zzcoc, this.zzchv.zzcwc, this.zzchw.zzapy, this.zzchv.zzcwd);
    }
}
