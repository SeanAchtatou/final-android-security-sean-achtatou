package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;

public final class zzcj extends Fragment implements zzci {
    private static WeakHashMap<Activity, WeakReference<zzcj>> zzfrk = new WeakHashMap<>();
    /* access modifiers changed from: private */
    public int zzbzn = 0;
    private Map<String, LifecycleCallback> zzfrl = new ArrayMap();
    /* access modifiers changed from: private */
    public Bundle zzfrm;

    public static zzcj zzo(Activity activity) {
        zzcj zzcj;
        WeakReference weakReference = zzfrk.get(activity);
        if (weakReference != null && (zzcj = (zzcj) weakReference.get()) != null) {
            return zzcj;
        }
        try {
            zzcj zzcj2 = (zzcj) activity.getFragmentManager().findFragmentByTag("LifecycleFragmentImpl");
            if (zzcj2 == null || zzcj2.isRemoving()) {
                zzcj2 = new zzcj();
                activity.getFragmentManager().beginTransaction().add(zzcj2, "LifecycleFragmentImpl").commitAllowingStateLoss();
            }
            zzfrk.put(activity, new WeakReference(zzcj2));
            return zzcj2;
        } catch (ClassCastException e) {
            throw new IllegalStateException("Fragment with tag LifecycleFragmentImpl is not a LifecycleFragmentImpl", e);
        }
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (LifecycleCallback dump : this.zzfrl.values()) {
            dump.dump(str, fileDescriptor, printWriter, strArr);
        }
    }

    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (LifecycleCallback onActivityResult : this.zzfrl.values()) {
            onActivityResult.onActivityResult(i, i2, intent);
        }
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.zzbzn = 1;
        this.zzfrm = bundle;
        for (Map.Entry next : this.zzfrl.entrySet()) {
            ((LifecycleCallback) next.getValue()).onCreate(bundle != null ? bundle.getBundle((String) next.getKey()) : null);
        }
    }

    public final void onDestroy() {
        super.onDestroy();
        this.zzbzn = 5;
        for (LifecycleCallback onDestroy : this.zzfrl.values()) {
            onDestroy.onDestroy();
        }
    }

    public final void onResume() {
        super.onResume();
        this.zzbzn = 3;
        for (LifecycleCallback onResume : this.zzfrl.values()) {
            onResume.onResume();
        }
    }

    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Map.Entry next : this.zzfrl.entrySet()) {
                Bundle bundle2 = new Bundle();
                ((LifecycleCallback) next.getValue()).onSaveInstanceState(bundle2);
                bundle.putBundle((String) next.getKey(), bundle2);
            }
        }
    }

    public final void onStart() {
        super.onStart();
        this.zzbzn = 2;
        for (LifecycleCallback onStart : this.zzfrl.values()) {
            onStart.onStart();
        }
    }

    public final void onStop() {
        super.onStop();
        this.zzbzn = 4;
        for (LifecycleCallback onStop : this.zzfrl.values()) {
            onStop.onStop();
        }
    }

    public final <T extends LifecycleCallback> T zza(String str, Class<T> cls) {
        return (LifecycleCallback) cls.cast(this.zzfrl.get(str));
    }

    public final void zza(String str, @NonNull LifecycleCallback lifecycleCallback) {
        if (!this.zzfrl.containsKey(str)) {
            this.zzfrl.put(str, lifecycleCallback);
            if (this.zzbzn > 0) {
                new Handler(Looper.getMainLooper()).post(new zzck(this, lifecycleCallback, str));
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder(59 + String.valueOf(str).length());
        sb.append("LifecycleCallback with tag ");
        sb.append(str);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }

    public final Activity zzajb() {
        return getActivity();
    }
}
