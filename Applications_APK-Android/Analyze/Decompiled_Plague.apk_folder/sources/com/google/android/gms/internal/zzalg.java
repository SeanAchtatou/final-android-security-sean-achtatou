package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.util.Log;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.concurrent.CountDownLatch;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

@zzzb
@TargetApi(14)
public final class zzalg extends Thread implements SurfaceTexture.OnFrameAvailableListener, zzalf {
    private static final float[] zzdfl = {-1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f};
    private int zzakw;
    private int zzakx;
    private final float[] zzdfi;
    private final zzald zzdfm;
    private final float[] zzdfn;
    private final float[] zzdfo;
    private final float[] zzdfp;
    private final float[] zzdfq;
    private final float[] zzdfr;
    private final float[] zzdfs;
    private float zzdft;
    private float zzdfu;
    private float zzdfv;
    private SurfaceTexture zzdfw;
    private SurfaceTexture zzdfx;
    private int zzdfy;
    private int zzdfz;
    private int zzdga;
    private FloatBuffer zzdgb = ByteBuffer.allocateDirect(zzdfl.length << 2).order(ByteOrder.nativeOrder()).asFloatBuffer();
    private final CountDownLatch zzdgc;
    private final Object zzdgd;
    private EGL10 zzdge;
    private EGLDisplay zzdgf;
    private EGLContext zzdgg;
    private EGLSurface zzdgh;
    private volatile boolean zzdgi;
    private volatile boolean zzdgj;

    public zzalg(Context context) {
        super("SphericalVideoProcessor");
        this.zzdgb.put(zzdfl).position(0);
        this.zzdfi = new float[9];
        this.zzdfn = new float[9];
        this.zzdfo = new float[9];
        this.zzdfp = new float[9];
        this.zzdfq = new float[9];
        this.zzdfr = new float[9];
        this.zzdfs = new float[9];
        this.zzdft = Float.NaN;
        this.zzdfm = new zzald(context);
        this.zzdfm.zza(this);
        this.zzdgc = new CountDownLatch(1);
        this.zzdgd = new Object();
    }

    private static void zza(float[] fArr, float f) {
        fArr[0] = 1.0f;
        fArr[1] = 0.0f;
        fArr[2] = 0.0f;
        fArr[3] = 0.0f;
        double d = (double) f;
        fArr[4] = (float) Math.cos(d);
        fArr[5] = (float) (-Math.sin(d));
        fArr[6] = 0.0f;
        fArr[7] = (float) Math.sin(d);
        fArr[8] = (float) Math.cos(d);
    }

    private static void zza(float[] fArr, float[] fArr2, float[] fArr3) {
        fArr[0] = (fArr2[0] * fArr3[0]) + (fArr2[1] * fArr3[3]) + (fArr2[2] * fArr3[6]);
        fArr[1] = (fArr2[0] * fArr3[1]) + (fArr2[1] * fArr3[4]) + (fArr2[2] * fArr3[7]);
        fArr[2] = (fArr2[0] * fArr3[2]) + (fArr2[1] * fArr3[5]) + (fArr2[2] * fArr3[8]);
        fArr[3] = (fArr2[3] * fArr3[0]) + (fArr2[4] * fArr3[3]) + (fArr2[5] * fArr3[6]);
        fArr[4] = (fArr2[3] * fArr3[1]) + (fArr2[4] * fArr3[4]) + (fArr2[5] * fArr3[7]);
        fArr[5] = (fArr2[3] * fArr3[2]) + (fArr2[4] * fArr3[5]) + (fArr2[5] * fArr3[8]);
        fArr[6] = (fArr2[6] * fArr3[0]) + (fArr2[7] * fArr3[3]) + (fArr2[8] * fArr3[6]);
        fArr[7] = (fArr2[6] * fArr3[1]) + (fArr2[7] * fArr3[4]) + (fArr2[8] * fArr3[7]);
        fArr[8] = (fArr2[6] * fArr3[2]) + (fArr2[7] * fArr3[5]) + (fArr2[8] * fArr3[8]);
    }

    private static void zzb(float[] fArr, float f) {
        double d = (double) f;
        fArr[0] = (float) Math.cos(d);
        fArr[1] = (float) (-Math.sin(d));
        fArr[2] = 0.0f;
        fArr[3] = (float) Math.sin(d);
        fArr[4] = (float) Math.cos(d);
        fArr[5] = 0.0f;
        fArr[6] = 0.0f;
        fArr[7] = 0.0f;
        fArr[8] = 1.0f;
    }

    private static void zzcq(String str) {
        int glGetError = GLES20.glGetError();
        if (glGetError != 0) {
            StringBuilder sb = new StringBuilder(21 + String.valueOf(str).length());
            sb.append(str);
            sb.append(": glError ");
            sb.append(glGetError);
            Log.e("SphericalVideoRenderer", sb.toString());
        }
    }

    private static int zzd(int i, String str) {
        int glCreateShader = GLES20.glCreateShader(i);
        zzcq("createShader");
        if (glCreateShader == 0) {
            return glCreateShader;
        }
        GLES20.glShaderSource(glCreateShader, str);
        zzcq("shaderSource");
        GLES20.glCompileShader(glCreateShader);
        zzcq("compileShader");
        int[] iArr = new int[1];
        GLES20.glGetShaderiv(glCreateShader, 35713, iArr, 0);
        zzcq("getShaderiv");
        if (iArr[0] != 0) {
            return glCreateShader;
        }
        StringBuilder sb = new StringBuilder(37);
        sb.append("Could not compile shader ");
        sb.append(i);
        sb.append(":");
        Log.e("SphericalVideoRenderer", sb.toString());
        Log.e("SphericalVideoRenderer", GLES20.glGetShaderInfoLog(glCreateShader));
        GLES20.glDeleteShader(glCreateShader);
        zzcq("deleteShader");
        return 0;
    }

    private final void zzru() {
        float f;
        float[] fArr;
        while (this.zzdga > 0) {
            this.zzdfw.updateTexImage();
            this.zzdga--;
        }
        if (this.zzdfm.zza(this.zzdfi)) {
            if (Float.isNaN(this.zzdft)) {
                float[] fArr2 = this.zzdfi;
                float[] fArr3 = {0.0f, 1.0f, 0.0f};
                float[] fArr4 = {(fArr2[0] * fArr3[0]) + (fArr2[1] * fArr3[1]) + (fArr2[2] * fArr3[2]), (fArr2[3] * fArr3[0]) + (fArr2[4] * fArr3[1]) + (fArr2[5] * fArr3[2]), (fArr2[6] * fArr3[0]) + (fArr2[7] * fArr3[1]) + (fArr2[8] * fArr3[2])};
                this.zzdft = -(((float) Math.atan2((double) fArr4[1], (double) fArr4[0])) - 1.5707964f);
            }
            fArr = this.zzdfr;
            f = this.zzdft + this.zzdfu;
        } else {
            zza(this.zzdfi, -1.5707964f);
            fArr = this.zzdfr;
            f = this.zzdfu;
        }
        zzb(fArr, f);
        zza(this.zzdfn, 1.5707964f);
        zza(this.zzdfo, this.zzdfr, this.zzdfn);
        zza(this.zzdfp, this.zzdfi, this.zzdfo);
        zza(this.zzdfq, this.zzdfv);
        zza(this.zzdfs, this.zzdfq, this.zzdfp);
        GLES20.glUniformMatrix3fv(this.zzdfz, 1, false, this.zzdfs, 0);
        GLES20.glDrawArrays(5, 0, 4);
        zzcq("drawArrays");
        GLES20.glFinish();
        this.zzdge.eglSwapBuffers(this.zzdgf, this.zzdgh);
    }

    private final boolean zzrv() {
        boolean z = false;
        if (!(this.zzdgh == null || this.zzdgh == EGL10.EGL_NO_SURFACE)) {
            z = this.zzdge.eglDestroySurface(this.zzdgf, this.zzdgh) | this.zzdge.eglMakeCurrent(this.zzdgf, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT) | false;
            this.zzdgh = null;
        }
        if (this.zzdgg != null) {
            z |= this.zzdge.eglDestroyContext(this.zzdgf, this.zzdgg);
            this.zzdgg = null;
        }
        if (this.zzdgf == null) {
            return z;
        }
        boolean eglTerminate = z | this.zzdge.eglTerminate(this.zzdgf);
        this.zzdgf = null;
        return eglTerminate;
    }

    public final void onFrameAvailable(SurfaceTexture surfaceTexture) {
        this.zzdga++;
        synchronized (this.zzdgd) {
            this.zzdgd.notifyAll();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.opengl.GLES20.glVertexAttribPointer(int, int, int, boolean, int, java.nio.Buffer):void}
     arg types: [int, int, int, int, int, java.nio.FloatBuffer]
     candidates:
      ClspMth{android.opengl.GLES20.glVertexAttribPointer(int, int, int, boolean, int, int):void}
      ClspMth{android.opengl.GLES20.glVertexAttribPointer(int, int, int, boolean, int, java.nio.Buffer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x02d5  */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x02da  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01e0  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01e2  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01e5 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r12 = this;
            android.graphics.SurfaceTexture r0 = r12.zzdfx
            if (r0 != 0) goto L_0x000f
            java.lang.String r0 = "SphericalVideoProcessor started with no output texture."
            com.google.android.gms.internal.zzafj.e(r0)
            java.util.concurrent.CountDownLatch r0 = r12.zzdgc
            r0.countDown()
            return
        L_0x000f:
            javax.microedition.khronos.egl.EGL r0 = javax.microedition.khronos.egl.EGLContext.getEGL()
            javax.microedition.khronos.egl.EGL10 r0 = (javax.microedition.khronos.egl.EGL10) r0
            r12.zzdge = r0
            javax.microedition.khronos.egl.EGL10 r0 = r12.zzdge
            java.lang.Object r1 = javax.microedition.khronos.egl.EGL10.EGL_DEFAULT_DISPLAY
            javax.microedition.khronos.egl.EGLDisplay r0 = r0.eglGetDisplay(r1)
            r12.zzdgf = r0
            javax.microedition.khronos.egl.EGLDisplay r0 = r12.zzdgf
            javax.microedition.khronos.egl.EGLDisplay r1 = javax.microedition.khronos.egl.EGL10.EGL_NO_DISPLAY
            r2 = 0
            r3 = 1
            r4 = 0
            if (r0 != r1) goto L_0x002d
        L_0x002a:
            r0 = r4
            goto L_0x00a4
        L_0x002d:
            r0 = 2
            int[] r0 = new int[r0]
            javax.microedition.khronos.egl.EGL10 r1 = r12.zzdge
            javax.microedition.khronos.egl.EGLDisplay r5 = r12.zzdgf
            boolean r0 = r1.eglInitialize(r5, r0)
            if (r0 != 0) goto L_0x003b
            goto L_0x002a
        L_0x003b:
            int[] r0 = new int[r3]
            javax.microedition.khronos.egl.EGLConfig[] r1 = new javax.microedition.khronos.egl.EGLConfig[r3]
            r5 = 11
            int[] r7 = new int[r5]
            r7 = {12352, 4, 12324, 8, 12323, 8, 12322, 8, 12325, 16, 12344} // fill-array
            javax.microedition.khronos.egl.EGL10 r5 = r12.zzdge
            javax.microedition.khronos.egl.EGLDisplay r6 = r12.zzdgf
            r9 = 1
            r8 = r1
            r10 = r0
            boolean r5 = r5.eglChooseConfig(r6, r7, r8, r9, r10)
            if (r5 == 0) goto L_0x005a
            r0 = r0[r4]
            if (r0 <= 0) goto L_0x005a
            r0 = r1[r4]
            goto L_0x005b
        L_0x005a:
            r0 = r2
        L_0x005b:
            if (r0 != 0) goto L_0x005e
            goto L_0x002a
        L_0x005e:
            r1 = 3
            int[] r1 = new int[r1]
            r1 = {12440, 2, 12344} // fill-array
            javax.microedition.khronos.egl.EGL10 r5 = r12.zzdge
            javax.microedition.khronos.egl.EGLDisplay r6 = r12.zzdgf
            javax.microedition.khronos.egl.EGLContext r7 = javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT
            javax.microedition.khronos.egl.EGLContext r1 = r5.eglCreateContext(r6, r0, r7, r1)
            r12.zzdgg = r1
            javax.microedition.khronos.egl.EGLContext r1 = r12.zzdgg
            if (r1 == 0) goto L_0x002a
            javax.microedition.khronos.egl.EGLContext r1 = r12.zzdgg
            javax.microedition.khronos.egl.EGLContext r5 = javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT
            if (r1 != r5) goto L_0x007b
            goto L_0x002a
        L_0x007b:
            javax.microedition.khronos.egl.EGL10 r1 = r12.zzdge
            javax.microedition.khronos.egl.EGLDisplay r5 = r12.zzdgf
            android.graphics.SurfaceTexture r6 = r12.zzdfx
            javax.microedition.khronos.egl.EGLSurface r0 = r1.eglCreateWindowSurface(r5, r0, r6, r2)
            r12.zzdgh = r0
            javax.microedition.khronos.egl.EGLSurface r0 = r12.zzdgh
            if (r0 == 0) goto L_0x002a
            javax.microedition.khronos.egl.EGLSurface r0 = r12.zzdgh
            javax.microedition.khronos.egl.EGLSurface r1 = javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE
            if (r0 != r1) goto L_0x0092
            goto L_0x002a
        L_0x0092:
            javax.microedition.khronos.egl.EGL10 r0 = r12.zzdge
            javax.microedition.khronos.egl.EGLDisplay r1 = r12.zzdgf
            javax.microedition.khronos.egl.EGLSurface r5 = r12.zzdgh
            javax.microedition.khronos.egl.EGLSurface r6 = r12.zzdgh
            javax.microedition.khronos.egl.EGLContext r7 = r12.zzdgg
            boolean r0 = r0.eglMakeCurrent(r1, r5, r6, r7)
            if (r0 != 0) goto L_0x00a3
            goto L_0x002a
        L_0x00a3:
            r0 = r3
        L_0x00a4:
            r1 = 35633(0x8b31, float:4.9932E-41)
            com.google.android.gms.internal.zzmg<java.lang.String> r5 = com.google.android.gms.internal.zzmq.zzbkv
            com.google.android.gms.internal.zzmo r6 = com.google.android.gms.ads.internal.zzbs.zzep()
            java.lang.Object r6 = r6.zzd(r5)
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r7 = r5.zzip()
            boolean r6 = r6.equals(r7)
            if (r6 != 0) goto L_0x00c8
            com.google.android.gms.internal.zzmo r6 = com.google.android.gms.ads.internal.zzbs.zzep()
            java.lang.Object r5 = r6.zzd(r5)
            java.lang.String r5 = (java.lang.String) r5
            goto L_0x00ca
        L_0x00c8:
            java.lang.String r5 = "attribute highp vec3 aPosition;varying vec3 pos;void main() {  gl_Position = vec4(aPosition, 1.0);  pos = aPosition;}"
        L_0x00ca:
            int r1 = zzd(r1, r5)
            if (r1 != 0) goto L_0x00d3
        L_0x00d0:
            r6 = r4
            goto L_0x0155
        L_0x00d3:
            r5 = 35632(0x8b30, float:4.9931E-41)
            com.google.android.gms.internal.zzmg<java.lang.String> r6 = com.google.android.gms.internal.zzmq.zzbkw
            com.google.android.gms.internal.zzmo r7 = com.google.android.gms.ads.internal.zzbs.zzep()
            java.lang.Object r7 = r7.zzd(r6)
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r8 = r6.zzip()
            boolean r7 = r7.equals(r8)
            if (r7 != 0) goto L_0x00f7
            com.google.android.gms.internal.zzmo r7 = com.google.android.gms.ads.internal.zzbs.zzep()
            java.lang.Object r6 = r7.zzd(r6)
            java.lang.String r6 = (java.lang.String) r6
            goto L_0x00f9
        L_0x00f7:
            java.lang.String r6 = "#extension GL_OES_EGL_image_external : require\n#define INV_PI 0.3183\nprecision highp float;varying vec3 pos;uniform samplerExternalOES uSplr;uniform mat3 uVMat;uniform float uFOVx;uniform float uFOVy;void main() {  vec3 ray = vec3(pos.x * tan(uFOVx), pos.y * tan(uFOVy), -1);  ray = (uVMat * ray).xyz;  ray = normalize(ray);  vec2 texCrd = vec2(    0.5 + atan(ray.x, - ray.z) * INV_PI * 0.5, acos(ray.y) * INV_PI);  gl_FragColor = vec4(texture2D(uSplr, texCrd).xyz, 1.0);}"
        L_0x00f9:
            int r5 = zzd(r5, r6)
            if (r5 != 0) goto L_0x0100
            goto L_0x00d0
        L_0x0100:
            int r6 = android.opengl.GLES20.glCreateProgram()
            java.lang.String r7 = "createProgram"
            zzcq(r7)
            if (r6 == 0) goto L_0x0155
            android.opengl.GLES20.glAttachShader(r6, r1)
            java.lang.String r1 = "attachShader"
            zzcq(r1)
            android.opengl.GLES20.glAttachShader(r6, r5)
            java.lang.String r1 = "attachShader"
            zzcq(r1)
            android.opengl.GLES20.glLinkProgram(r6)
            java.lang.String r1 = "linkProgram"
            zzcq(r1)
            int[] r1 = new int[r3]
            r5 = 35714(0x8b82, float:5.0046E-41)
            android.opengl.GLES20.glGetProgramiv(r6, r5, r1, r4)
            java.lang.String r5 = "getProgramiv"
            zzcq(r5)
            r1 = r1[r4]
            if (r1 == r3) goto L_0x014d
            java.lang.String r1 = "SphericalVideoRenderer"
            java.lang.String r5 = "Could not link program: "
            android.util.Log.e(r1, r5)
            java.lang.String r1 = "SphericalVideoRenderer"
            java.lang.String r5 = android.opengl.GLES20.glGetProgramInfoLog(r6)
            android.util.Log.e(r1, r5)
            android.opengl.GLES20.glDeleteProgram(r6)
            java.lang.String r1 = "deleteProgram"
            zzcq(r1)
            goto L_0x00d0
        L_0x014d:
            android.opengl.GLES20.glValidateProgram(r6)
            java.lang.String r1 = "validateProgram"
            zzcq(r1)
        L_0x0155:
            r12.zzdfy = r6
            int r1 = r12.zzdfy
            android.opengl.GLES20.glUseProgram(r1)
            java.lang.String r1 = "useProgram"
            zzcq(r1)
            int r1 = r12.zzdfy
            java.lang.String r5 = "aPosition"
            int r1 = android.opengl.GLES20.glGetAttribLocation(r1, r5)
            r7 = 3
            r8 = 5126(0x1406, float:7.183E-42)
            r9 = 0
            r10 = 12
            java.nio.FloatBuffer r11 = r12.zzdgb
            r6 = r1
            android.opengl.GLES20.glVertexAttribPointer(r6, r7, r8, r9, r10, r11)
            java.lang.String r5 = "vertexAttribPointer"
            zzcq(r5)
            android.opengl.GLES20.glEnableVertexAttribArray(r1)
            java.lang.String r1 = "enableVertexAttribArray"
            zzcq(r1)
            int[] r1 = new int[r3]
            android.opengl.GLES20.glGenTextures(r3, r1, r4)
            java.lang.String r5 = "genTextures"
            zzcq(r5)
            r1 = r1[r4]
            r5 = 36197(0x8d65, float:5.0723E-41)
            android.opengl.GLES20.glBindTexture(r5, r1)
            java.lang.String r6 = "bindTextures"
            zzcq(r6)
            r6 = 10240(0x2800, float:1.4349E-41)
            r7 = 9729(0x2601, float:1.3633E-41)
            android.opengl.GLES20.glTexParameteri(r5, r6, r7)
            java.lang.String r6 = "texParameteri"
            zzcq(r6)
            r6 = 10241(0x2801, float:1.435E-41)
            android.opengl.GLES20.glTexParameteri(r5, r6, r7)
            java.lang.String r6 = "texParameteri"
            zzcq(r6)
            r6 = 10242(0x2802, float:1.4352E-41)
            r7 = 33071(0x812f, float:4.6342E-41)
            android.opengl.GLES20.glTexParameteri(r5, r6, r7)
            java.lang.String r6 = "texParameteri"
            zzcq(r6)
            r6 = 10243(0x2803, float:1.4354E-41)
            android.opengl.GLES20.glTexParameteri(r5, r6, r7)
            java.lang.String r5 = "texParameteri"
            zzcq(r5)
            int r5 = r12.zzdfy
            java.lang.String r6 = "uVMat"
            int r5 = android.opengl.GLES20.glGetUniformLocation(r5, r6)
            r12.zzdfz = r5
            r5 = 9
            float[] r5 = new float[r5]
            r5 = {1065353216, 0, 0, 0, 1065353216, 0, 0, 0, 1065353216} // fill-array
            int r6 = r12.zzdfz
            android.opengl.GLES20.glUniformMatrix3fv(r6, r3, r4, r5, r4)
            int r5 = r12.zzdfy
            if (r5 == 0) goto L_0x01e2
            r5 = r3
            goto L_0x01e3
        L_0x01e2:
            r5 = r4
        L_0x01e3:
            if (r0 == 0) goto L_0x02bf
            if (r5 != 0) goto L_0x01e9
            goto L_0x02bf
        L_0x01e9:
            android.graphics.SurfaceTexture r0 = new android.graphics.SurfaceTexture
            r0.<init>(r1)
            r12.zzdfw = r0
            android.graphics.SurfaceTexture r0 = r12.zzdfw
            r0.setOnFrameAvailableListener(r12)
            java.util.concurrent.CountDownLatch r0 = r12.zzdgc
            r0.countDown()
            com.google.android.gms.internal.zzald r0 = r12.zzdfm
            r0.start()
            r12.zzdgi = r3     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
        L_0x0201:
            boolean r0 = r12.zzdgj     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            if (r0 != 0) goto L_0x0269
            r12.zzru()     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            boolean r0 = r12.zzdgi     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            if (r0 == 0) goto L_0x0250
            int r0 = r12.zzakw     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            int r1 = r12.zzakx     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            android.opengl.GLES20.glViewport(r4, r4, r0, r1)     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            java.lang.String r0 = "viewport"
            zzcq(r0)     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            int r0 = r12.zzdfy     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            java.lang.String r1 = "uFOVx"
            int r0 = android.opengl.GLES20.glGetUniformLocation(r0, r1)     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            int r1 = r12.zzdfy     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            java.lang.String r3 = "uFOVy"
            int r1 = android.opengl.GLES20.glGetUniformLocation(r1, r3)     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            int r3 = r12.zzakw     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            int r5 = r12.zzakx     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            r6 = 1063216883(0x3f5f66f3, float:0.87266463)
            if (r3 <= r5) goto L_0x0240
            android.opengl.GLES20.glUniform1f(r0, r6)     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            int r0 = r12.zzakx     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            float r0 = (float) r0     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            float r6 = r6 * r0
            int r0 = r12.zzakw     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            float r0 = (float) r0     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            float r6 = r6 / r0
            android.opengl.GLES20.glUniform1f(r1, r6)     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            goto L_0x024e
        L_0x0240:
            int r3 = r12.zzakw     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            float r3 = (float) r3     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            float r3 = r3 * r6
            int r5 = r12.zzakx     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            float r5 = (float) r5     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            float r3 = r3 / r5
            android.opengl.GLES20.glUniform1f(r0, r3)     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
            android.opengl.GLES20.glUniform1f(r1, r6)     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
        L_0x024e:
            r12.zzdgi = r4     // Catch:{ IllegalStateException -> 0x029a, Throwable -> 0x027b }
        L_0x0250:
            java.lang.Object r0 = r12.zzdgd     // Catch:{ InterruptedException -> 0x0201 }
            monitor-enter(r0)     // Catch:{ InterruptedException -> 0x0201 }
            boolean r1 = r12.zzdgj     // Catch:{ all -> 0x0266 }
            if (r1 != 0) goto L_0x0264
            boolean r1 = r12.zzdgi     // Catch:{ all -> 0x0266 }
            if (r1 != 0) goto L_0x0264
            int r1 = r12.zzdga     // Catch:{ all -> 0x0266 }
            if (r1 != 0) goto L_0x0264
            java.lang.Object r1 = r12.zzdgd     // Catch:{ all -> 0x0266 }
            r1.wait()     // Catch:{ all -> 0x0266 }
        L_0x0264:
            monitor-exit(r0)     // Catch:{ all -> 0x0266 }
            goto L_0x0201
        L_0x0266:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0266 }
            throw r1     // Catch:{ InterruptedException -> 0x0201 }
        L_0x0269:
            com.google.android.gms.internal.zzald r0 = r12.zzdfm
            r0.stop()
            android.graphics.SurfaceTexture r0 = r12.zzdfw
            r0.setOnFrameAvailableListener(r2)
            r12.zzdfw = r2
            r12.zzrv()
            return
        L_0x0279:
            r0 = move-exception
            goto L_0x02af
        L_0x027b:
            r0 = move-exception
            java.lang.String r1 = "SphericalVideoProcessor died."
            com.google.android.gms.internal.zzafj.zzb(r1, r0)     // Catch:{ all -> 0x0279 }
            com.google.android.gms.internal.zzaez r1 = com.google.android.gms.ads.internal.zzbs.zzeg()     // Catch:{ all -> 0x0279 }
            java.lang.String r3 = "SphericalVideoProcessor.run.2"
            r1.zza(r0, r3)     // Catch:{ all -> 0x0279 }
            com.google.android.gms.internal.zzald r0 = r12.zzdfm
            r0.stop()
            android.graphics.SurfaceTexture r0 = r12.zzdfw
            r0.setOnFrameAvailableListener(r2)
            r12.zzdfw = r2
            r12.zzrv()
            return
        L_0x029a:
            java.lang.String r0 = "SphericalVideoProcessor halted unexpectedly."
            com.google.android.gms.internal.zzafj.zzco(r0)     // Catch:{ all -> 0x0279 }
            com.google.android.gms.internal.zzald r0 = r12.zzdfm
            r0.stop()
            android.graphics.SurfaceTexture r0 = r12.zzdfw
            r0.setOnFrameAvailableListener(r2)
            r12.zzdfw = r2
            r12.zzrv()
            return
        L_0x02af:
            com.google.android.gms.internal.zzald r1 = r12.zzdfm
            r1.stop()
            android.graphics.SurfaceTexture r1 = r12.zzdfw
            r1.setOnFrameAvailableListener(r2)
            r12.zzdfw = r2
            r12.zzrv()
            throw r0
        L_0x02bf:
            javax.microedition.khronos.egl.EGL10 r0 = r12.zzdge
            int r0 = r0.eglGetError()
            java.lang.String r0 = android.opengl.GLUtils.getEGLErrorString(r0)
            java.lang.String r1 = "EGL initialization failed: "
            java.lang.String r0 = java.lang.String.valueOf(r0)
            int r2 = r0.length()
            if (r2 == 0) goto L_0x02da
            java.lang.String r0 = r1.concat(r0)
            goto L_0x02df
        L_0x02da:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r1)
        L_0x02df:
            com.google.android.gms.internal.zzafj.e(r0)
            com.google.android.gms.internal.zzaez r1 = com.google.android.gms.ads.internal.zzbs.zzeg()
            java.lang.Throwable r2 = new java.lang.Throwable
            r2.<init>(r0)
            java.lang.String r0 = "SphericalVideoProcessor.run.1"
            r1.zza(r2, r0)
            r12.zzrv()
            java.util.concurrent.CountDownLatch r0 = r12.zzdgc
            r0.countDown()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzalg.run():void");
    }

    public final void zza(SurfaceTexture surfaceTexture, int i, int i2) {
        this.zzakw = i;
        this.zzakx = i2;
        this.zzdfx = surfaceTexture;
    }

    public final void zzb(float f, float f2) {
        float f3;
        float f4;
        int i;
        if (this.zzakw > this.zzakx) {
            f3 = (f * 1.7453293f) / ((float) this.zzakw);
            f4 = 1.7453293f * f2;
            i = this.zzakw;
        } else {
            f3 = (f * 1.7453293f) / ((float) this.zzakx);
            f4 = 1.7453293f * f2;
            i = this.zzakx;
        }
        float f5 = f4 / ((float) i);
        this.zzdfu -= f3;
        this.zzdfv -= f5;
        if (this.zzdfv < -1.5707964f) {
            this.zzdfv = -1.5707964f;
        }
        if (this.zzdfv > 1.5707964f) {
            this.zzdfv = 1.5707964f;
        }
    }

    public final void zzh(int i, int i2) {
        synchronized (this.zzdgd) {
            this.zzakw = i;
            this.zzakx = i2;
            this.zzdgi = true;
            this.zzdgd.notifyAll();
        }
    }

    public final void zzms() {
        synchronized (this.zzdgd) {
            this.zzdgd.notifyAll();
        }
    }

    public final void zzrs() {
        synchronized (this.zzdgd) {
            this.zzdgj = true;
            this.zzdfx = null;
            this.zzdgd.notifyAll();
        }
    }

    public final SurfaceTexture zzrt() {
        if (this.zzdfx == null) {
            return null;
        }
        try {
            this.zzdgc.await();
        } catch (InterruptedException unused) {
        }
        return this.zzdfw;
    }
}
