package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzbrv extends zzbrs<DriveFolder> {
    public zzbrv(TaskCompletionSource<DriveFolder> taskCompletionSource) {
        super(taskCompletionSource);
    }

    public final void zza(zzbpy zzbpy) throws RemoteException {
        zzaox().setResult(zzbpy.getDriveId().asDriveFolder());
    }
}
