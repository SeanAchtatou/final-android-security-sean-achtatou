package com.google.android.gms.common.api.internal;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArraySet;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzcwb;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.tapjoy.TapjoyConstants;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public final class zzbp implements Handler.Callback {
    /* access modifiers changed from: private */
    public static final Object sLock = new Object();
    public static final Status zzfqe = new Status(4, "Sign-out occurred while this API call was in progress.");
    /* access modifiers changed from: private */
    public static final Status zzfqf = new Status(4, "The user must be signed in to make this API call.");
    private static zzbp zzfqh;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final Handler mHandler;
    /* access modifiers changed from: private */
    public final GoogleApiAvailability zzfke;
    /* access modifiers changed from: private */
    public final Map<zzh<?>, zzbr<?>> zzfne = new ConcurrentHashMap(5, 0.75f, 1);
    /* access modifiers changed from: private */
    public long zzfpd = 120000;
    /* access modifiers changed from: private */
    public long zzfpe = 5000;
    /* access modifiers changed from: private */
    public long zzfqg = TapjoyConstants.TIMER_INCREMENT;
    /* access modifiers changed from: private */
    public int zzfqi = -1;
    private final AtomicInteger zzfqj = new AtomicInteger(1);
    private final AtomicInteger zzfqk = new AtomicInteger(0);
    /* access modifiers changed from: private */
    public zzak zzfql = null;
    /* access modifiers changed from: private */
    public final Set<zzh<?>> zzfqm = new ArraySet();
    private final Set<zzh<?>> zzfqn = new ArraySet();

    private zzbp(Context context, Looper looper, GoogleApiAvailability googleApiAvailability) {
        this.mContext = context;
        this.mHandler = new Handler(looper, this);
        this.zzfke = googleApiAvailability;
        this.mHandler.sendMessage(this.mHandler.obtainMessage(6));
    }

    public static zzbp zzaie() {
        zzbp zzbp;
        synchronized (sLock) {
            zzbq.checkNotNull(zzfqh, "Must guarantee manager is non-null before using getInstance");
            zzbp = zzfqh;
        }
        return zzbp;
    }

    public static void zzaif() {
        synchronized (sLock) {
            if (zzfqh != null) {
                zzbp zzbp = zzfqh;
                zzbp.zzfqk.incrementAndGet();
                zzbp.mHandler.sendMessageAtFrontOfQueue(zzbp.mHandler.obtainMessage(10));
            }
        }
    }

    @WorkerThread
    private final void zzaih() {
        for (zzh<?> remove : this.zzfqn) {
            this.zzfne.remove(remove).signOut();
        }
        this.zzfqn.clear();
    }

    @WorkerThread
    private final void zzb(GoogleApi<?> googleApi) {
        zzh<?> zzaga = googleApi.zzaga();
        zzbr zzbr = this.zzfne.get(zzaga);
        if (zzbr == null) {
            zzbr = new zzbr(this, googleApi);
            this.zzfne.put(zzaga, zzbr);
        }
        if (zzbr.zzaam()) {
            this.zzfqn.add(zzaga);
        }
        zzbr.connect();
    }

    public static zzbp zzch(Context context) {
        zzbp zzbp;
        synchronized (sLock) {
            if (zzfqh == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                zzfqh = new zzbp(context.getApplicationContext(), handlerThread.getLooper(), GoogleApiAvailability.getInstance());
            }
            zzbp = zzfqh;
        }
        return zzbp;
    }

    @WorkerThread
    public final boolean handleMessage(Message message) {
        ConnectionResult zzaio;
        long j = 300000;
        switch (message.what) {
            case 1:
                if (((Boolean) message.obj).booleanValue()) {
                    j = TapjoyConstants.TIMER_INCREMENT;
                }
                this.zzfqg = j;
                this.mHandler.removeMessages(12);
                for (zzh<?> obtainMessage : this.zzfne.keySet()) {
                    this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(12, obtainMessage), this.zzfqg);
                }
                break;
            case 2:
                zzj zzj = (zzj) message.obj;
                for (zzh next : zzj.zzagn()) {
                    zzbr zzbr = this.zzfne.get(next);
                    if (zzbr == null) {
                        zzj.zza(next, new ConnectionResult(13));
                        return true;
                    }
                    if (zzbr.isConnected()) {
                        zzaio = ConnectionResult.zzfhy;
                    } else if (zzbr.zzaio() != null) {
                        zzaio = zzbr.zzaio();
                    } else {
                        zzbr.zza(zzj);
                    }
                    zzj.zza(next, zzaio);
                }
                break;
            case 3:
                for (zzbr next2 : this.zzfne.values()) {
                    next2.zzain();
                    next2.connect();
                }
                break;
            case 4:
            case 8:
            case 13:
                zzcs zzcs = (zzcs) message.obj;
                zzbr zzbr2 = this.zzfne.get(zzcs.zzfrx.zzaga());
                if (zzbr2 == null) {
                    zzb(zzcs.zzfrx);
                    zzbr2 = this.zzfne.get(zzcs.zzfrx.zzaga());
                }
                if (!zzbr2.zzaam() || this.zzfqk.get() == zzcs.zzfrw) {
                    zzbr2.zza(zzcs.zzfrv);
                    return true;
                }
                zzcs.zzfrv.zzs(zzfqe);
                zzbr2.signOut();
                return true;
            case 5:
                int i = message.arg1;
                ConnectionResult connectionResult = (ConnectionResult) message.obj;
                zzbr zzbr3 = null;
                Iterator<zzbr<?>> it = this.zzfne.values().iterator();
                while (true) {
                    if (it.hasNext()) {
                        zzbr next3 = it.next();
                        if (next3.getInstanceId() == i) {
                            zzbr3 = next3;
                        }
                    }
                }
                if (zzbr3 != null) {
                    String errorString = this.zzfke.getErrorString(connectionResult.getErrorCode());
                    String errorMessage = connectionResult.getErrorMessage();
                    StringBuilder sb = new StringBuilder(69 + String.valueOf(errorString).length() + String.valueOf(errorMessage).length());
                    sb.append("Error resolution was canceled by the user, original error message: ");
                    sb.append(errorString);
                    sb.append(": ");
                    sb.append(errorMessage);
                    zzbr3.zzw(new Status(17, sb.toString()));
                    return true;
                }
                StringBuilder sb2 = new StringBuilder(76);
                sb2.append("Could not find API instance ");
                sb2.append(i);
                sb2.append(" while trying to fail enqueued calls.");
                Log.wtf("GoogleApiManager", sb2.toString(), new Exception());
                return true;
            case 6:
                if (this.mContext.getApplicationContext() instanceof Application) {
                    zzk.zza((Application) this.mContext.getApplicationContext());
                    zzk.zzagp().zza(new zzbq(this));
                    if (!zzk.zzagp().zzbd(true)) {
                        this.zzfqg = 300000;
                        return true;
                    }
                }
                break;
            case 7:
                zzb((GoogleApi) message.obj);
                return true;
            case 9:
                if (this.zzfne.containsKey(message.obj)) {
                    this.zzfne.get(message.obj).resume();
                    return true;
                }
                break;
            case 10:
                zzaih();
                return true;
            case 11:
                if (this.zzfne.containsKey(message.obj)) {
                    this.zzfne.get(message.obj).zzahx();
                    return true;
                }
                break;
            case 12:
                if (this.zzfne.containsKey(message.obj)) {
                    this.zzfne.get(message.obj).zzair();
                    return true;
                }
                break;
            default:
                int i2 = message.what;
                StringBuilder sb3 = new StringBuilder(31);
                sb3.append("Unknown message id: ");
                sb3.append(i2);
                Log.w("GoogleApiManager", sb3.toString());
                return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final PendingIntent zza(zzh<?> zzh, int i) {
        zzcwb zzais;
        zzbr zzbr = this.zzfne.get(zzh);
        if (zzbr == null || (zzais = zzbr.zzais()) == null) {
            return null;
        }
        return PendingIntent.getActivity(this.mContext, i, zzais.getSignInIntent(), 134217728);
    }

    public final <O extends Api.ApiOptions> Task<Boolean> zza(@NonNull GoogleApi googleApi, @NonNull zzcn<?> zzcn) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.mHandler.sendMessage(this.mHandler.obtainMessage(13, new zzcs(new zzf(zzcn, taskCompletionSource), this.zzfqk.get(), googleApi)));
        return taskCompletionSource.getTask();
    }

    public final <O extends Api.ApiOptions> Task<Void> zza(@NonNull GoogleApi googleApi, @NonNull zzct<Api.zzb, ?> zzct, @NonNull zzdp<Api.zzb, ?> zzdp) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.mHandler.sendMessage(this.mHandler.obtainMessage(8, new zzcs(new zzd(new zzcu(zzct, zzdp), taskCompletionSource), this.zzfqk.get(), googleApi)));
        return taskCompletionSource.getTask();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x003a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x000f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.tasks.Task<java.lang.Void> zza(java.lang.Iterable<? extends com.google.android.gms.common.api.GoogleApi<?>> r4) {
        /*
            r3 = this;
            com.google.android.gms.common.api.internal.zzj r0 = new com.google.android.gms.common.api.internal.zzj
            r0.<init>(r4)
            java.util.Iterator r4 = r4.iterator()
        L_0x0009:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x003a
            java.lang.Object r1 = r4.next()
            com.google.android.gms.common.api.GoogleApi r1 = (com.google.android.gms.common.api.GoogleApi) r1
            java.util.Map<com.google.android.gms.common.api.internal.zzh<?>, com.google.android.gms.common.api.internal.zzbr<?>> r2 = r3.zzfne
            com.google.android.gms.common.api.internal.zzh r1 = r1.zzaga()
            java.lang.Object r1 = r2.get(r1)
            com.google.android.gms.common.api.internal.zzbr r1 = (com.google.android.gms.common.api.internal.zzbr) r1
            if (r1 == 0) goto L_0x0029
            boolean r1 = r1.isConnected()
            if (r1 != 0) goto L_0x0009
        L_0x0029:
            android.os.Handler r4 = r3.mHandler
            android.os.Handler r1 = r3.mHandler
            r2 = 2
            android.os.Message r1 = r1.obtainMessage(r2, r0)
            r4.sendMessage(r1)
        L_0x0035:
            com.google.android.gms.tasks.Task r4 = r0.getTask()
            return r4
        L_0x003a:
            r0.zzago()
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzbp.zza(java.lang.Iterable):com.google.android.gms.tasks.Task");
    }

    public final void zza(ConnectionResult connectionResult, int i) {
        if (!zzc(connectionResult, i)) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(5, i, 0, connectionResult));
        }
    }

    public final void zza(GoogleApi<?> googleApi) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(7, googleApi));
    }

    public final <O extends Api.ApiOptions, TResult> void zza(GoogleApi<O> googleApi, int i, zzdf<Api.zzb, TResult> zzdf, TaskCompletionSource<TResult> taskCompletionSource, zzdb zzdb) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(4, new zzcs(new zze(i, zzdf, taskCompletionSource, zzdb), this.zzfqk.get(), googleApi)));
    }

    public final <O extends Api.ApiOptions> void zza(GoogleApi googleApi, int i, zzm<? extends Result, Api.zzb> zzm) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(4, new zzcs(new zzc(i, zzm), this.zzfqk.get(), googleApi)));
    }

    public final void zza(@NonNull zzak zzak) {
        synchronized (sLock) {
            if (this.zzfql != zzak) {
                this.zzfql = zzak;
                this.zzfqm.clear();
                this.zzfqm.addAll(zzak.zzahl());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzagf() {
        this.zzfqk.incrementAndGet();
        this.mHandler.sendMessage(this.mHandler.obtainMessage(10));
    }

    public final void zzagm() {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(3));
    }

    public final int zzaig() {
        return this.zzfqj.getAndIncrement();
    }

    /* access modifiers changed from: package-private */
    public final void zzb(@NonNull zzak zzak) {
        synchronized (sLock) {
            if (this.zzfql == zzak) {
                this.zzfql = null;
                this.zzfqm.clear();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean zzc(ConnectionResult connectionResult, int i) {
        return this.zzfke.zza(this.mContext, connectionResult, i);
    }
}
