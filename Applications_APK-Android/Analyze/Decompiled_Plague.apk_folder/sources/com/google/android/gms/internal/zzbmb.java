package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzar;
import com.google.android.gms.drive.DriveFile;

final class zzbmb extends zzbky {
    private /* synthetic */ int zzgki;
    private /* synthetic */ DriveFile.DownloadProgressListener zzgll;
    private /* synthetic */ zzbma zzglm;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbmb(zzbma zzbma, GoogleApiClient googleApiClient, int i, DriveFile.DownloadProgressListener downloadProgressListener) {
        super(googleApiClient);
        this.zzglm = zzbma;
        this.zzgki = i;
        this.zzgll = downloadProgressListener;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        zza(zzar.zzak(((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbqu(this.zzglm.getDriveId(), this.zzgki, 0), new zzbqw(this, this.zzgll)).zzgni));
    }
}
