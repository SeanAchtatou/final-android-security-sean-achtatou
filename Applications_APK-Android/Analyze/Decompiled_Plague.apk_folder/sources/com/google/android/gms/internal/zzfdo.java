package com.google.android.gms.internal;

import java.io.IOException;

class zzfdo extends zzfdn {
    protected final byte[] zzjkl;

    zzfdo(byte[] bArr) {
        this.zzjkl = bArr;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfdh) || size() != ((zzfdh) obj).size()) {
            return false;
        }
        if (size() == 0) {
            return true;
        }
        if (!(obj instanceof zzfdo)) {
            return obj.equals(this);
        }
        zzfdo zzfdo = (zzfdo) obj;
        int zzcto = zzcto();
        int zzcto2 = zzfdo.zzcto();
        if (zzcto == 0 || zzcto2 == 0 || zzcto == zzcto2) {
            return zza(zzfdo, 0, size());
        }
        return false;
    }

    public int size() {
        return this.zzjkl.length;
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzfdg zzfdg) throws IOException {
        zzfdg.zzd(this.zzjkl, zzctp(), size());
    }

    /* access modifiers changed from: package-private */
    public final boolean zza(zzfdh zzfdh, int i, int i2) {
        if (i2 > zzfdh.size()) {
            int size = size();
            StringBuilder sb = new StringBuilder(40);
            sb.append("Length too large: ");
            sb.append(i2);
            sb.append(size);
            throw new IllegalArgumentException(sb.toString());
        }
        int i3 = i + i2;
        if (i3 > zzfdh.size()) {
            int size2 = zzfdh.size();
            StringBuilder sb2 = new StringBuilder(59);
            sb2.append("Ran off end of other: ");
            sb2.append(i);
            sb2.append(", ");
            sb2.append(i2);
            sb2.append(", ");
            sb2.append(size2);
            throw new IllegalArgumentException(sb2.toString());
        } else if (!(zzfdh instanceof zzfdo)) {
            return zzfdh.zzx(i, i3).equals(zzx(0, i2));
        } else {
            zzfdo zzfdo = (zzfdo) zzfdh;
            byte[] bArr = this.zzjkl;
            byte[] bArr2 = zzfdo.zzjkl;
            int zzctp = zzctp() + i2;
            int zzctp2 = zzctp();
            int zzctp3 = zzfdo.zzctp() + i;
            while (zzctp2 < zzctp) {
                if (bArr[zzctp2] != bArr2[zzctp3]) {
                    return false;
                }
                zzctp2++;
                zzctp3++;
            }
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void zzb(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.zzjkl, i, bArr, i2, i3);
    }

    public final zzfdq zzctl() {
        return zzfdq.zzb(this.zzjkl, zzctp(), size(), true);
    }

    /* access modifiers changed from: protected */
    public int zzctp() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public final int zzg(int i, int i2, int i3) {
        return zzfer.zza(i, this.zzjkl, zzctp() + i2, i3);
    }

    public byte zzkd(int i) {
        return this.zzjkl[i];
    }

    public final zzfdh zzx(int i, int i2) {
        int zzh = zzh(i, i2, size());
        return zzh == 0 ? zzfdh.zzpal : new zzfdk(this.zzjkl, zzctp() + i, zzh);
    }
}
