package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbek;
import com.google.android.gms.internal.zzbjr;

public final class zzo implements Parcelable.Creator<zzn> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        zzbjr zzbjr = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                zzbek.zzb(parcel, readInt);
            } else {
                zzbjr = (zzbjr) zzbek.zza(parcel, readInt, zzbjr.CREATOR);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzn(zzbjr);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzn[i];
    }
}
