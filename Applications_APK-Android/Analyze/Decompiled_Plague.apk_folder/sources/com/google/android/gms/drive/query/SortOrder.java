package com.google.android.gms.drive.query;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.drive.query.internal.zzf;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SortOrder extends zzbej {
    public static final Parcelable.Creator<SortOrder> CREATOR = new zzc();
    private List<zzf> zzgsb;
    private boolean zzgsc;

    public static class Builder {
        private final List<zzf> zzgsb = new ArrayList();
        private boolean zzgsc = false;

        public Builder addSortAscending(SortableMetadataField sortableMetadataField) {
            this.zzgsb.add(new zzf(sortableMetadataField.getName(), true));
            return this;
        }

        public Builder addSortDescending(SortableMetadataField sortableMetadataField) {
            this.zzgsb.add(new zzf(sortableMetadataField.getName(), false));
            return this;
        }

        public SortOrder build() {
            return new SortOrder(this.zzgsb, false);
        }
    }

    SortOrder(List<zzf> list, boolean z) {
        this.zzgsb = list;
        this.zzgsc = z;
    }

    public String toString() {
        return String.format(Locale.US, "SortOrder[%s, %s]", TextUtils.join(",", this.zzgsb), Boolean.valueOf(this.zzgsc));
    }

    public void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 1, this.zzgsb, false);
        zzbem.zza(parcel, 2, this.zzgsc);
        zzbem.zzai(parcel, zze);
    }
}
