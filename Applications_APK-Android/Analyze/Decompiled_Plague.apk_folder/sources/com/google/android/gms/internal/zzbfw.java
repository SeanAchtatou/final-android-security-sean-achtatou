package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbfw implements Parcelable.Creator<zzbfv> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        Parcel parcel2 = null;
        int i = 0;
        zzbfq zzbfq = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbek.zzg(parcel, readInt);
                    break;
                case 2:
                    parcel2 = zzbek.zzad(parcel, readInt);
                    break;
                case 3:
                    zzbfq = (zzbfq) zzbek.zza(parcel, readInt, zzbfq.CREATOR);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbfv(i, parcel2, zzbfq);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbfv[i];
    }
}
