package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrk;
import java.io.IOException;
import java.util.List;

public final class zzdrr extends zzfee<zzdrr, zza> implements zzffk {
    private static volatile zzffm<zzdrr> zzbas;
    /* access modifiers changed from: private */
    public static final zzdrr zzltj;
    private int zzltg;
    private int zzlth;
    private zzfev<zzb> zzlti = zzcvf();

    public static final class zza extends zzfef<zzdrr, zza> implements zzffk {
        private zza() {
            super(zzdrr.zzltj);
        }

        /* synthetic */ zza(zzdrs zzdrs) {
            this();
        }
    }

    public static final class zzb extends zzfee<zzb, zza> implements zzffk {
        private static volatile zzffm<zzb> zzbas;
        /* access modifiers changed from: private */
        public static final zzb zzlto;
        private zzdrk zzltk;
        private int zzltl;
        private int zzltm;
        private int zzltn;

        public static final class zza extends zzfef<zzb, zza> implements zzffk {
            private zza() {
                super(zzb.zzlto);
            }

            /* synthetic */ zza(zzdrs zzdrs) {
                this();
            }
        }

        static {
            zzb zzb = new zzb();
            zzlto = zzb;
            zzb.zza(zzfem.zzpcf, (Object) null, (Object) null);
            zzb.zzpbs.zzbim();
        }

        private zzb() {
        }

        public static zzb zzboj() {
            return zzlto;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            zzdrk.zza zza2;
            boolean z = true;
            boolean z2 = false;
            switch (zzdrs.zzbaq[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return zzlto;
                case 3:
                    return null;
                case 4:
                    return new zza(null);
                case 5:
                    zzfen zzfen = (zzfen) obj;
                    zzb zzb = (zzb) obj2;
                    this.zzltk = (zzdrk) zzfen.zza(this.zzltk, zzb.zzltk);
                    this.zzltl = zzfen.zza(this.zzltl != 0, this.zzltl, zzb.zzltl != 0, zzb.zzltl);
                    this.zzltm = zzfen.zza(this.zzltm != 0, this.zzltm, zzb.zzltm != 0, zzb.zzltm);
                    boolean z3 = this.zzltn != 0;
                    int i2 = this.zzltn;
                    if (zzb.zzltn == 0) {
                        z = false;
                    }
                    this.zzltn = zzfen.zza(z3, i2, z, zzb.zzltn);
                    return this;
                case 6:
                    zzfdq zzfdq = (zzfdq) obj;
                    zzfea zzfea = (zzfea) obj2;
                    if (zzfea != null) {
                        while (!z2) {
                            try {
                                int zzcts = zzfdq.zzcts();
                                if (zzcts != 0) {
                                    if (zzcts == 10) {
                                        if (this.zzltk != null) {
                                            zzdrk zzdrk = this.zzltk;
                                            zzfef zzfef = (zzfef) zzdrk.zza(zzfem.zzpch, (Object) null, (Object) null);
                                            zzfef.zza((zzfee) zzdrk);
                                            zza2 = (zzdrk.zza) zzfef;
                                        } else {
                                            zza2 = null;
                                        }
                                        this.zzltk = (zzdrk) zzfdq.zza(zzdrk.zzbnw(), zzfea);
                                        if (zza2 != null) {
                                            zza2.zza((zzfee) this.zzltk);
                                            this.zzltk = (zzdrk) zza2.zzcvj();
                                        }
                                    } else if (zzcts == 16) {
                                        this.zzltl = zzfdq.zzcuc();
                                    } else if (zzcts == 24) {
                                        this.zzltm = zzfdq.zzcub();
                                    } else if (zzcts == 32) {
                                        this.zzltn = zzfdq.zzcuc();
                                    } else if (!zza(zzcts, zzfdq)) {
                                    }
                                }
                                z2 = true;
                            } catch (zzfew e) {
                                throw new RuntimeException(e.zzh(this));
                            } catch (IOException e2) {
                                throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                            }
                        }
                        break;
                    } else {
                        throw new NullPointerException();
                    }
                case 7:
                    break;
                case 8:
                    if (zzbas == null) {
                        synchronized (zzb.class) {
                            if (zzbas == null) {
                                zzbas = new zzfeg(zzlto);
                            }
                        }
                    }
                    return zzbas;
                default:
                    throw new UnsupportedOperationException();
            }
            return zzlto;
        }

        public final void zza(zzfdv zzfdv) throws IOException {
            if (this.zzltk != null) {
                zzfdv.zza(1, this.zzltk == null ? zzdrk.zzbnw() : this.zzltk);
            }
            if (this.zzltl != zzdrn.UNKNOWN_STATUS.zzhn()) {
                zzfdv.zzaa(2, this.zzltl);
            }
            if (this.zzltm != 0) {
                zzfdv.zzab(3, this.zzltm);
            }
            if (this.zzltn != zzdrv.UNKNOWN_PREFIX.zzhn()) {
                zzfdv.zzaa(4, this.zzltn);
            }
            this.zzpbs.zza(zzfdv);
        }

        public final boolean zzboe() {
            return this.zzltk != null;
        }

        public final zzdrk zzbof() {
            return this.zzltk == null ? zzdrk.zzbnw() : this.zzltk;
        }

        public final zzdrn zzbog() {
            zzdrn zzfs = zzdrn.zzfs(this.zzltl);
            return zzfs == null ? zzdrn.UNRECOGNIZED : zzfs;
        }

        public final int zzboh() {
            return this.zzltm;
        }

        public final zzdrv zzboi() {
            zzdrv zzfx = zzdrv.zzfx(this.zzltn);
            return zzfx == null ? zzdrv.UNRECOGNIZED : zzfx;
        }

        public final int zzhl() {
            int i = this.zzpbt;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (this.zzltk != null) {
                i2 = 0 + zzfdv.zzb(1, this.zzltk == null ? zzdrk.zzbnw() : this.zzltk);
            }
            if (this.zzltl != zzdrn.UNKNOWN_STATUS.zzhn()) {
                i2 += zzfdv.zzag(2, this.zzltl);
            }
            if (this.zzltm != 0) {
                i2 += zzfdv.zzae(3, this.zzltm);
            }
            if (this.zzltn != zzdrv.UNKNOWN_PREFIX.zzhn()) {
                i2 += zzfdv.zzag(4, this.zzltn);
            }
            int zzhl = i2 + this.zzpbs.zzhl();
            this.zzpbt = zzhl;
            return zzhl;
        }
    }

    static {
        zzdrr zzdrr = new zzdrr();
        zzltj = zzdrr;
        zzdrr.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdrr.zzpbs.zzbim();
    }

    private zzdrr() {
    }

    public static zzdrr zzae(byte[] bArr) throws zzfew {
        return (zzdrr) zzfee.zza(zzltj, bArr);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        boolean z = true;
        boolean z2 = false;
        switch (zzdrs.zzbaq[i - 1]) {
            case 1:
                return new zzdrr();
            case 2:
                return zzltj;
            case 3:
                this.zzlti.zzbim();
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdrr zzdrr = (zzdrr) obj2;
                boolean z3 = this.zzlth != 0;
                int i2 = this.zzlth;
                if (zzdrr.zzlth == 0) {
                    z = false;
                }
                this.zzlth = zzfen.zza(z3, i2, z, zzdrr.zzlth);
                this.zzlti = zzfen.zza(this.zzlti, zzdrr.zzlti);
                if (zzfen == zzfel.zzpcb) {
                    this.zzltg |= zzdrr.zzltg;
                }
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzlth = zzfdq.zzcub();
                                } else if (zzcts == 18) {
                                    if (!this.zzlti.zzcth()) {
                                        zzfev<zzb> zzfev = this.zzlti;
                                        int size = zzfev.size();
                                        this.zzlti = zzfev.zzln(size == 0 ? 10 : size << 1);
                                    }
                                    this.zzlti.add((zzb) zzfdq.zza(zzb.zzboj(), zzfea));
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdrr.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzltj);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzltj;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlth != 0) {
            zzfdv.zzab(1, this.zzlth);
        }
        for (int i = 0; i < this.zzlti.size(); i++) {
            zzfdv.zza(2, this.zzlti.get(i));
        }
        this.zzpbs.zza(zzfdv);
    }

    public final int zzboa() {
        return this.zzlth;
    }

    public final List<zzb> zzbob() {
        return this.zzlti;
    }

    public final int zzboc() {
        return this.zzlti.size();
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int zzae = this.zzlth != 0 ? zzfdv.zzae(1, this.zzlth) + 0 : 0;
        for (int i2 = 0; i2 < this.zzlti.size(); i2++) {
            zzae += zzfdv.zzb(2, this.zzlti.get(i2));
        }
        int zzhl = zzae + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
