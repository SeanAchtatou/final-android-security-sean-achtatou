package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.internal.player.zza;
import com.google.android.gms.games.internal.player.zzd;
import com.google.android.gms.games.internal.player.zze;

public final class PlayerRef extends zzc implements Player {
    private final PlayerLevelInfo zzhje;
    private final zze zzhjw;
    private final zzd zzhjx;

    public PlayerRef(DataHolder dataHolder, int i) {
        this(dataHolder, i, null);
    }

    public PlayerRef(DataHolder dataHolder, int i, String str) {
        super(dataHolder, i);
        PlayerLevelInfo playerLevelInfo;
        this.zzhjw = new zze(str);
        this.zzhjx = new zzd(dataHolder, i, this.zzhjw);
        if (!zzfx(this.zzhjw.zzhsh) && getLong(this.zzhjw.zzhsh) != -1) {
            int integer = getInteger(this.zzhjw.zzhsi);
            int integer2 = getInteger(this.zzhjw.zzhsl);
            PlayerLevel playerLevel = new PlayerLevel(integer, getLong(this.zzhjw.zzhsj), getLong(this.zzhjw.zzhsk));
            playerLevelInfo = new PlayerLevelInfo(getLong(this.zzhjw.zzhsh), getLong(this.zzhjw.zzhsn), playerLevel, integer != integer2 ? new PlayerLevel(integer2, getLong(this.zzhjw.zzhsk), getLong(this.zzhjw.zzhsm)) : playerLevel);
        } else {
            playerLevelInfo = null;
        }
        this.zzhje = playerLevelInfo;
    }

    public final int describeContents() {
        return 0;
    }

    public final boolean equals(Object obj) {
        return PlayerEntity.zza(this, obj);
    }

    public final /* synthetic */ Object freeze() {
        return new PlayerEntity(this);
    }

    public final Uri getBannerImageLandscapeUri() {
        return zzfw(this.zzhjw.zzhsy);
    }

    public final String getBannerImageLandscapeUrl() {
        return getString(this.zzhjw.zzhsz);
    }

    public final Uri getBannerImagePortraitUri() {
        return zzfw(this.zzhjw.zzhta);
    }

    public final String getBannerImagePortraitUrl() {
        return getString(this.zzhjw.zzhtb);
    }

    public final String getDisplayName() {
        return getString(this.zzhjw.zzhrz);
    }

    public final void getDisplayName(CharArrayBuffer charArrayBuffer) {
        zza(this.zzhjw.zzhrz, charArrayBuffer);
    }

    public final Uri getHiResImageUri() {
        return zzfw(this.zzhjw.zzhsc);
    }

    public final String getHiResImageUrl() {
        return getString(this.zzhjw.zzhsd);
    }

    public final Uri getIconImageUri() {
        return zzfw(this.zzhjw.zzhsa);
    }

    public final String getIconImageUrl() {
        return getString(this.zzhjw.zzhsb);
    }

    public final long getLastPlayedWithTimestamp() {
        if (!zzfv(this.zzhjw.zzhsg) || zzfx(this.zzhjw.zzhsg)) {
            return -1;
        }
        return getLong(this.zzhjw.zzhsg);
    }

    public final PlayerLevelInfo getLevelInfo() {
        return this.zzhje;
    }

    public final String getName() {
        return getString(this.zzhjw.name);
    }

    public final String getPlayerId() {
        return getString(this.zzhjw.zzhry);
    }

    public final long getRetrievedTimestamp() {
        return getLong(this.zzhjw.zzhse);
    }

    public final String getTitle() {
        return getString(this.zzhjw.title);
    }

    public final void getTitle(CharArrayBuffer charArrayBuffer) {
        zza(this.zzhjw.title, charArrayBuffer);
    }

    public final boolean hasHiResImage() {
        return getHiResImageUri() != null;
    }

    public final boolean hasIconImage() {
        return getIconImageUri() != null;
    }

    public final int hashCode() {
        return PlayerEntity.zza(this);
    }

    public final boolean isMuted() {
        return getBoolean(this.zzhjw.zzhte);
    }

    public final String toString() {
        return PlayerEntity.zzb(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((PlayerEntity) ((Player) freeze())).writeToParcel(parcel, i);
    }

    public final String zzaqx() {
        return getString(this.zzhjw.zzhsx);
    }

    public final boolean zzaqy() {
        return getBoolean(this.zzhjw.zzhsw);
    }

    public final int zzaqz() {
        return getInteger(this.zzhjw.zzhsf);
    }

    public final boolean zzara() {
        return getBoolean(this.zzhjw.zzhsp);
    }

    public final zza zzarb() {
        if (zzfx(this.zzhjw.zzhsq)) {
            return null;
        }
        return this.zzhjx;
    }

    public final int zzarc() {
        return getInteger(this.zzhjw.zzhtc);
    }

    public final long zzard() {
        return getLong(this.zzhjw.zzhtd);
    }
}
