package com.google.android.gms.internal;

import android.os.Parcelable;

public final class zzbkf implements Parcelable.Creator<zzbke> {
    /* JADX WARN: Type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r10) {
        /*
            r9 = this;
            int r0 = com.google.android.gms.internal.zzbek.zzd(r10)
            r1 = 0
            r2 = 0
            r4 = r1
            r6 = r4
            r7 = r6
            r8 = r7
            r5 = r2
        L_0x000b:
            int r1 = r10.dataPosition()
            if (r1 >= r0) goto L_0x0043
            int r1 = r10.readInt()
            r2 = 65535(0xffff, float:9.1834E-41)
            r2 = r2 & r1
            switch(r2) {
                case 2: goto L_0x0039;
                case 3: goto L_0x0034;
                case 4: goto L_0x002f;
                case 5: goto L_0x0025;
                case 6: goto L_0x0020;
                default: goto L_0x001c;
            }
        L_0x001c:
            com.google.android.gms.internal.zzbek.zzb(r10, r1)
            goto L_0x000b
        L_0x0020:
            java.lang.Integer r8 = com.google.android.gms.internal.zzbek.zzh(r10, r1)
            goto L_0x000b
        L_0x0025:
            android.os.Parcelable$Creator<com.google.android.gms.drive.DriveId> r2 = com.google.android.gms.drive.DriveId.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r10, r1, r2)
            r7 = r1
            com.google.android.gms.drive.DriveId r7 = (com.google.android.gms.drive.DriveId) r7
            goto L_0x000b
        L_0x002f:
            java.lang.String r6 = com.google.android.gms.internal.zzbek.zzq(r10, r1)
            goto L_0x000b
        L_0x0034:
            int r5 = com.google.android.gms.internal.zzbek.zzg(r10, r1)
            goto L_0x000b
        L_0x0039:
            android.os.Parcelable$Creator<com.google.android.gms.drive.metadata.internal.MetadataBundle> r2 = com.google.android.gms.drive.metadata.internal.MetadataBundle.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r10, r1, r2)
            r4 = r1
            com.google.android.gms.drive.metadata.internal.MetadataBundle r4 = (com.google.android.gms.drive.metadata.internal.MetadataBundle) r4
            goto L_0x000b
        L_0x0043:
            com.google.android.gms.internal.zzbek.zzaf(r10, r0)
            com.google.android.gms.internal.zzbke r10 = new com.google.android.gms.internal.zzbke
            r3 = r10
            r3.<init>(r4, r5, r6, r7, r8)
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzbkf.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbke[i];
    }
}
