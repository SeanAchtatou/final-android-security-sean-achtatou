package com.google.android.gms.internal;

public final class zzbzy {
    private static zzbzy zzhga;
    private final zzbzt zzhgb = new zzbzt();
    private final zzbzu zzhgc = new zzbzu();

    static {
        zzbzy zzbzy = new zzbzy();
        synchronized (zzbzy.class) {
            zzhga = zzbzy;
        }
    }

    private zzbzy() {
    }

    private static zzbzy zzaqo() {
        zzbzy zzbzy;
        synchronized (zzbzy.class) {
            zzbzy = zzhga;
        }
        return zzbzy;
    }

    public static zzbzt zzaqp() {
        return zzaqo().zzhgb;
    }

    public static zzbzu zzaqq() {
        return zzaqo().zzhgc;
    }
}
