package com.google.android.gms.common.api.internal;

final class zzz implements Runnable {
    private /* synthetic */ zzy zzfna;

    zzz(zzy zzy) {
        this.zzfna = zzy;
    }

    public final void run() {
        this.zzfna.zzfmy.lock();
        try {
            this.zzfna.zzagz();
        } finally {
            this.zzfna.zzfmy.unlock();
        }
    }
}
