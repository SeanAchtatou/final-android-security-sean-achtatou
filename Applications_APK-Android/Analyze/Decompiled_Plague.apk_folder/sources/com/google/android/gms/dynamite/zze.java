package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

final class zze implements DynamiteModule.zzd {
    zze() {
    }

    public final zzj zza(Context context, String str, zzi zzi) throws DynamiteModule.zzc {
        zzj zzj = new zzj();
        zzj.zzgum = zzi.zzab(context, str);
        zzj.zzgun = zzj.zzgum != 0 ? zzi.zzc(context, str, false) : zzi.zzc(context, str, true);
        if (zzj.zzgum == 0 && zzj.zzgun == 0) {
            zzj.zzguo = 0;
            return zzj;
        } else if (zzj.zzgum >= zzj.zzgun) {
            zzj.zzguo = -1;
            return zzj;
        } else {
            zzj.zzguo = 1;
            return zzj;
        }
    }
}
