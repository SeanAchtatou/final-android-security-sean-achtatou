package com.google.android.gms.games.leaderboard;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import java.util.ArrayList;
import java.util.Arrays;

public final class LeaderboardEntity implements Leaderboard {
    private final String zzedu;
    private final Uri zzhgv;
    private final String zzhhg;
    private final String zzhtj;
    private final int zzhtk;
    private final ArrayList<zzb> zzhtl;
    private final Game zzhtm;

    public LeaderboardEntity(Leaderboard leaderboard) {
        this.zzhtj = leaderboard.getLeaderboardId();
        this.zzedu = leaderboard.getDisplayName();
        this.zzhgv = leaderboard.getIconImageUri();
        this.zzhhg = leaderboard.getIconImageUrl();
        this.zzhtk = leaderboard.getScoreOrder();
        Game game = leaderboard.getGame();
        this.zzhtm = game == null ? null : new GameEntity(game);
        ArrayList<LeaderboardVariant> variants = leaderboard.getVariants();
        int size = variants.size();
        this.zzhtl = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            this.zzhtl.add((zzb) variants.get(i).freeze());
        }
    }

    static int zza(Leaderboard leaderboard) {
        return Arrays.hashCode(new Object[]{leaderboard.getLeaderboardId(), leaderboard.getDisplayName(), leaderboard.getIconImageUri(), Integer.valueOf(leaderboard.getScoreOrder()), leaderboard.getVariants()});
    }

    static boolean zza(Leaderboard leaderboard, Object obj) {
        if (!(obj instanceof Leaderboard)) {
            return false;
        }
        if (leaderboard == obj) {
            return true;
        }
        Leaderboard leaderboard2 = (Leaderboard) obj;
        return zzbg.equal(leaderboard2.getLeaderboardId(), leaderboard.getLeaderboardId()) && zzbg.equal(leaderboard2.getDisplayName(), leaderboard.getDisplayName()) && zzbg.equal(leaderboard2.getIconImageUri(), leaderboard.getIconImageUri()) && zzbg.equal(Integer.valueOf(leaderboard2.getScoreOrder()), Integer.valueOf(leaderboard.getScoreOrder())) && zzbg.equal(leaderboard2.getVariants(), leaderboard.getVariants());
    }

    static String zzb(Leaderboard leaderboard) {
        return zzbg.zzw(leaderboard).zzg("LeaderboardId", leaderboard.getLeaderboardId()).zzg("DisplayName", leaderboard.getDisplayName()).zzg("IconImageUri", leaderboard.getIconImageUri()).zzg("IconImageUrl", leaderboard.getIconImageUrl()).zzg("ScoreOrder", Integer.valueOf(leaderboard.getScoreOrder())).zzg("Variants", leaderboard.getVariants()).toString();
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    public final /* bridge */ /* synthetic */ Object freeze() {
        if (this != null) {
            return this;
        }
        throw null;
    }

    public final String getDisplayName() {
        return this.zzedu;
    }

    public final void getDisplayName(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.zzedu, charArrayBuffer);
    }

    public final Game getGame() {
        return this.zzhtm;
    }

    public final Uri getIconImageUri() {
        return this.zzhgv;
    }

    public final String getIconImageUrl() {
        return this.zzhhg;
    }

    public final String getLeaderboardId() {
        return this.zzhtj;
    }

    public final int getScoreOrder() {
        return this.zzhtk;
    }

    public final ArrayList<LeaderboardVariant> getVariants() {
        return new ArrayList<>(this.zzhtl);
    }

    public final int hashCode() {
        return zza(this);
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String toString() {
        return zzb(this);
    }
}
