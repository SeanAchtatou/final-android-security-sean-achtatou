package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzcf implements zzbo<TurnBasedMultiplayer.LoadMatchResult, TurnBasedMatch> {
    zzcf() {
    }

    public final /* synthetic */ Object zzb(@Nullable Result result) {
        TurnBasedMatch match;
        TurnBasedMultiplayer.LoadMatchResult loadMatchResult = (TurnBasedMultiplayer.LoadMatchResult) result;
        if (loadMatchResult == null || (match = loadMatchResult.getMatch()) == null) {
            return null;
        }
        return (TurnBasedMatch) match.freeze();
    }
}
