package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzam extends zzat {
    private /* synthetic */ String zzhis;
    private /* synthetic */ boolean zzhpv;
    private /* synthetic */ int zzhqa;
    private /* synthetic */ int zzhqb;
    private /* synthetic */ int zzhqc;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzam(zzah zzah, GoogleApiClient googleApiClient, String str, int i, int i2, int i3, boolean z) {
        super(googleApiClient, null);
        this.zzhis = str;
        this.zzhqa = i;
        this.zzhqb = i2;
        this.zzhqc = i3;
        this.zzhpv = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zzb(this, this.zzhis, this.zzhqa, this.zzhqb, this.zzhqc, this.zzhpv);
    }
}
