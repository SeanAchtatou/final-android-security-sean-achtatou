package com.google.android.gms.games.multiplayer;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class ParticipantEntity extends GamesDowngradeableSafeParcel implements Participant {
    public static final Parcelable.Creator<ParticipantEntity> CREATOR = new zza();
    private final int zzbzn;
    private final String zzedu;
    private final int zzele;
    private final Uri zzhgv;
    private final Uri zzhgw;
    private final String zzhhg;
    private final String zzhhh;
    private final PlayerEntity zzhmf;
    private final String zzhog;
    private final String zzhuv;
    private final boolean zzhuw;
    private final ParticipantResult zzhux;

    static final class zza extends zzc {
        zza() {
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return createFromParcel(parcel);
        }

        public final ParticipantEntity zzl(Parcel parcel) {
            if (ParticipantEntity.zze(ParticipantEntity.zzaku()) || ParticipantEntity.zzgc(ParticipantEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            Uri parse = readString3 == null ? null : Uri.parse(readString3);
            String readString4 = parcel.readString();
            Uri parse2 = readString4 == null ? null : Uri.parse(readString4);
            int readInt = parcel.readInt();
            String readString5 = parcel.readString();
            boolean z = false;
            boolean z2 = parcel.readInt() > 0;
            if (parcel.readInt() > 0) {
                z = true;
            }
            return new ParticipantEntity(readString, readString2, parse, parse2, readInt, readString5, z2, z ? PlayerEntity.CREATOR.createFromParcel(parcel) : null, 7, null, null, null);
        }
    }

    public ParticipantEntity(Participant participant) {
        this.zzhog = participant.getParticipantId();
        this.zzedu = participant.getDisplayName();
        this.zzhgv = participant.getIconImageUri();
        this.zzhgw = participant.getHiResImageUri();
        this.zzbzn = participant.getStatus();
        this.zzhuv = participant.zzatv();
        this.zzhuw = participant.isConnectedToRoom();
        Player player = participant.getPlayer();
        this.zzhmf = player == null ? null : new PlayerEntity(player);
        this.zzele = participant.getCapabilities();
        this.zzhux = participant.getResult();
        this.zzhhg = participant.getIconImageUrl();
        this.zzhhh = participant.getHiResImageUrl();
    }

    ParticipantEntity(String str, String str2, Uri uri, Uri uri2, int i, String str3, boolean z, PlayerEntity playerEntity, int i2, ParticipantResult participantResult, String str4, String str5) {
        this.zzhog = str;
        this.zzedu = str2;
        this.zzhgv = uri;
        this.zzhgw = uri2;
        this.zzbzn = i;
        this.zzhuv = str3;
        this.zzhuw = z;
        this.zzhmf = playerEntity;
        this.zzele = i2;
        this.zzhux = participantResult;
        this.zzhhg = str4;
        this.zzhhh = str5;
    }

    static int zza(Participant participant) {
        return Arrays.hashCode(new Object[]{participant.getPlayer(), Integer.valueOf(participant.getStatus()), participant.zzatv(), Boolean.valueOf(participant.isConnectedToRoom()), participant.getDisplayName(), participant.getIconImageUri(), participant.getHiResImageUri(), Integer.valueOf(participant.getCapabilities()), participant.getResult(), participant.getParticipantId()});
    }

    static boolean zza(Participant participant, Object obj) {
        if (!(obj instanceof Participant)) {
            return false;
        }
        if (participant == obj) {
            return true;
        }
        Participant participant2 = (Participant) obj;
        return zzbg.equal(participant2.getPlayer(), participant.getPlayer()) && zzbg.equal(Integer.valueOf(participant2.getStatus()), Integer.valueOf(participant.getStatus())) && zzbg.equal(participant2.zzatv(), participant.zzatv()) && zzbg.equal(Boolean.valueOf(participant2.isConnectedToRoom()), Boolean.valueOf(participant.isConnectedToRoom())) && zzbg.equal(participant2.getDisplayName(), participant.getDisplayName()) && zzbg.equal(participant2.getIconImageUri(), participant.getIconImageUri()) && zzbg.equal(participant2.getHiResImageUri(), participant.getHiResImageUri()) && zzbg.equal(Integer.valueOf(participant2.getCapabilities()), Integer.valueOf(participant.getCapabilities())) && zzbg.equal(participant2.getResult(), participant.getResult()) && zzbg.equal(participant2.getParticipantId(), participant.getParticipantId());
    }

    static String zzb(Participant participant) {
        return zzbg.zzw(participant).zzg("ParticipantId", participant.getParticipantId()).zzg("Player", participant.getPlayer()).zzg("Status", Integer.valueOf(participant.getStatus())).zzg("ClientAddress", participant.zzatv()).zzg("ConnectedToRoom", Boolean.valueOf(participant.isConnectedToRoom())).zzg("DisplayName", participant.getDisplayName()).zzg("IconImage", participant.getIconImageUri()).zzg("IconImageUrl", participant.getIconImageUrl()).zzg("HiResImage", participant.getHiResImageUri()).zzg("HiResImageUrl", participant.getHiResImageUrl()).zzg("Capabilities", Integer.valueOf(participant.getCapabilities())).zzg("Result", participant.getResult()).toString();
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    public final Participant freeze() {
        return this;
    }

    public final int getCapabilities() {
        return this.zzele;
    }

    public final String getDisplayName() {
        return this.zzhmf == null ? this.zzedu : this.zzhmf.getDisplayName();
    }

    public final void getDisplayName(CharArrayBuffer charArrayBuffer) {
        if (this.zzhmf == null) {
            zzg.zzb(this.zzedu, charArrayBuffer);
        } else {
            this.zzhmf.getDisplayName(charArrayBuffer);
        }
    }

    public final Uri getHiResImageUri() {
        return this.zzhmf == null ? this.zzhgw : this.zzhmf.getHiResImageUri();
    }

    public final String getHiResImageUrl() {
        return this.zzhmf == null ? this.zzhhh : this.zzhmf.getHiResImageUrl();
    }

    public final Uri getIconImageUri() {
        return this.zzhmf == null ? this.zzhgv : this.zzhmf.getIconImageUri();
    }

    public final String getIconImageUrl() {
        return this.zzhmf == null ? this.zzhhg : this.zzhmf.getIconImageUrl();
    }

    public final String getParticipantId() {
        return this.zzhog;
    }

    public final Player getPlayer() {
        return this.zzhmf;
    }

    public final ParticipantResult getResult() {
        return this.zzhux;
    }

    public final int getStatus() {
        return this.zzbzn;
    }

    public final int hashCode() {
        return zza(this);
    }

    public final boolean isConnectedToRoom() {
        return this.zzhuw;
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String toString() {
        return zzb(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.Player, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.multiplayer.ParticipantResult, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, getParticipantId(), false);
        zzbem.zza(parcel, 2, getDisplayName(), false);
        zzbem.zza(parcel, 3, (Parcelable) getIconImageUri(), i, false);
        zzbem.zza(parcel, 4, (Parcelable) getHiResImageUri(), i, false);
        zzbem.zzc(parcel, 5, getStatus());
        zzbem.zza(parcel, 6, this.zzhuv, false);
        zzbem.zza(parcel, 7, isConnectedToRoom());
        zzbem.zza(parcel, 8, (Parcelable) getPlayer(), i, false);
        zzbem.zzc(parcel, 9, this.zzele);
        zzbem.zza(parcel, 10, (Parcelable) getResult(), i, false);
        zzbem.zza(parcel, 11, getIconImageUrl(), false);
        zzbem.zza(parcel, 12, getHiResImageUrl(), false);
        zzbem.zzai(parcel, zze);
    }

    public final String zzatv() {
        return this.zzhuv;
    }
}
