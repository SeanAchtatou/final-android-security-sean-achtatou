package com.google.android.gms.internal;

public abstract class zzaai extends zzee implements zzaah {
    public zzaai() {
        attachInterface(this, "com.google.android.gms.ads.internal.request.IAdRequestService");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [com.google.android.gms.internal.zzaan] */
    /* JADX WARN: Type inference failed for: r1v6, types: [com.google.android.gms.internal.zzaak] */
    /* JADX WARN: Type inference failed for: r1v11 */
    /* JADX WARN: Type inference failed for: r1v12 */
    /* JADX WARN: Type inference failed for: r1v13 */
    /* JADX WARN: Type inference failed for: r1v14 */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTransact(int r3, android.os.Parcel r4, android.os.Parcel r5, int r6) throws android.os.RemoteException {
        /*
            r2 = this;
            boolean r6 = r2.zza(r3, r4, r5, r6)
            r0 = 1
            if (r6 == 0) goto L_0x0008
            return r0
        L_0x0008:
            r6 = 4
            r1 = 0
            if (r3 == r6) goto L_0x004d
            switch(r3) {
                case 1: goto L_0x003a;
                case 2: goto L_0x0011;
                default: goto L_0x000f;
            }
        L_0x000f:
            r3 = 0
            return r3
        L_0x0011:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzzz> r3 = com.google.android.gms.internal.zzzz.CREATOR
            android.os.Parcelable r3 = com.google.android.gms.internal.zzef.zza(r4, r3)
            com.google.android.gms.internal.zzzz r3 = (com.google.android.gms.internal.zzzz) r3
            android.os.IBinder r4 = r4.readStrongBinder()
            if (r4 != 0) goto L_0x0020
            goto L_0x0033
        L_0x0020:
            java.lang.String r6 = "com.google.android.gms.ads.internal.request.IAdResponseListener"
            android.os.IInterface r6 = r4.queryLocalInterface(r6)
            boolean r1 = r6 instanceof com.google.android.gms.internal.zzaak
            if (r1 == 0) goto L_0x002e
            r1 = r6
            com.google.android.gms.internal.zzaak r1 = (com.google.android.gms.internal.zzaak) r1
            goto L_0x0033
        L_0x002e:
            com.google.android.gms.internal.zzaam r1 = new com.google.android.gms.internal.zzaam
            r1.<init>(r4)
        L_0x0033:
            r2.zza(r3, r1)
        L_0x0036:
            r5.writeNoException()
            return r0
        L_0x003a:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzzz> r3 = com.google.android.gms.internal.zzzz.CREATOR
            android.os.Parcelable r3 = com.google.android.gms.internal.zzef.zza(r4, r3)
            com.google.android.gms.internal.zzzz r3 = (com.google.android.gms.internal.zzzz) r3
            com.google.android.gms.internal.zzaad r3 = r2.zzb(r3)
            r5.writeNoException()
            com.google.android.gms.internal.zzef.zzb(r5, r3)
            return r0
        L_0x004d:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzaas> r3 = com.google.android.gms.internal.zzaas.CREATOR
            android.os.Parcelable r3 = com.google.android.gms.internal.zzef.zza(r4, r3)
            com.google.android.gms.internal.zzaas r3 = (com.google.android.gms.internal.zzaas) r3
            android.os.IBinder r4 = r4.readStrongBinder()
            if (r4 != 0) goto L_0x005c
            goto L_0x006f
        L_0x005c:
            java.lang.String r6 = "com.google.android.gms.ads.internal.request.INonagonStreamingResponseListener"
            android.os.IInterface r6 = r4.queryLocalInterface(r6)
            boolean r1 = r6 instanceof com.google.android.gms.internal.zzaan
            if (r1 == 0) goto L_0x006a
            r1 = r6
            com.google.android.gms.internal.zzaan r1 = (com.google.android.gms.internal.zzaan) r1
            goto L_0x006f
        L_0x006a:
            com.google.android.gms.internal.zzaao r1 = new com.google.android.gms.internal.zzaao
            r1.<init>(r4)
        L_0x006f:
            r2.zza(r3, r1)
            goto L_0x0036
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaai.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
