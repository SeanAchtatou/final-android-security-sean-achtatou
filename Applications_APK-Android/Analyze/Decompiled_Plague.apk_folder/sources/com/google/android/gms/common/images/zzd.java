package com.google.android.gms.common.images;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.google.android.gms.common.images.ImageManager;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzc;
import java.lang.ref.WeakReference;
import java.util.Arrays;

public final class zzd extends zza {
    private WeakReference<ImageManager.OnImageLoadedListener> zzfut;

    public zzd(ImageManager.OnImageLoadedListener onImageLoadedListener, Uri uri) {
        super(uri, 0);
        zzc.zzu(onImageLoadedListener);
        this.zzfut = new WeakReference<>(onImageLoadedListener);
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzd)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        zzd zzd = (zzd) obj;
        ImageManager.OnImageLoadedListener onImageLoadedListener = this.zzfut.get();
        ImageManager.OnImageLoadedListener onImageLoadedListener2 = zzd.zzfut.get();
        return onImageLoadedListener2 != null && onImageLoadedListener != null && zzbg.equal(onImageLoadedListener2, onImageLoadedListener) && zzbg.equal(zzd.zzful, this.zzful);
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.zzful});
    }

    /* access modifiers changed from: protected */
    public final void zza(Drawable drawable, boolean z, boolean z2, boolean z3) {
        ImageManager.OnImageLoadedListener onImageLoadedListener;
        if (!z2 && (onImageLoadedListener = this.zzfut.get()) != null) {
            onImageLoadedListener.onImageLoaded(this.zzful.uri, drawable, z3);
        }
    }
}
