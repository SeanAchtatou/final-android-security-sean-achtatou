package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfed;

public interface zzfed<T extends zzfed<T>> extends Comparable<T> {
    zzfgr zzcvb();

    boolean zzcvc();
}
