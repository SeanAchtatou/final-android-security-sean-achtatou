package com.google.android.gms.internal;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.internal.zzbs;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

@zzzb
public final class zzabh extends zzaai {
    private static final Object sLock = new Object();
    private static zzabh zzcpt;
    private final Context mContext;
    private final zzabg zzcpu;

    private zzabh(Context context, zzabg zzabg) {
        this.mContext = context;
        this.zzcpu = zzabg;
    }

    private static zzaad zza(Context context, zzabg zzabg, zzzz zzzz) {
        char c;
        String string;
        Context context2 = context;
        zzabg zzabg2 = zzabg;
        zzzz zzzz2 = zzzz;
        zzafj.zzbw("Starting ad request from service using: google.afma.request.getAdDictionary");
        zznd zznd = new zznd(((Boolean) zzbs.zzep().zzd(zzmq.zzbhx)).booleanValue(), "load_ad", zzzz2.zzath.zzbda);
        if (zzzz2.versionCode > 10 && zzzz2.zzcme != -1) {
            zznd.zza(zznd.zzc(zzzz2.zzcme), "cts");
        }
        ScheduledExecutorService newSingleThreadScheduledExecutor = Executors.newSingleThreadScheduledExecutor();
        zznb zziz = zznd.zziz();
        zzajp zza = zzajg.zza(zzabg2.zzcpq.zzj(context2), ((Long) zzbs.zzep().zzd(zzmq.zzbod)).longValue(), TimeUnit.MILLISECONDS, newSingleThreadScheduledExecutor);
        zzajp zza2 = zzajg.zza(zzabg2.zzcpp.zzq(context2), ((Long) zzbs.zzep().zzd(zzmq.zzbln)).longValue(), TimeUnit.MILLISECONDS, newSingleThreadScheduledExecutor);
        zzajp<String> zzbx = zzabg2.zzcpk.zzbx(zzzz2.zzclp.packageName);
        zzajp<String> zza3 = zzabg2.zzcpr.zza(zzzz2.zzclq, zzzz2.zzclp);
        Future<zzabu> zzp = zzbs.zzem().zzp(context2);
        zzajp zzi = zzajg.zzi(null);
        Bundle bundle = zzzz2.zzclo.extras;
        boolean z = (bundle == null || bundle.getString("_ad") == null) ? false : true;
        if (zzzz2.zzcmk && !z) {
            zzi = zzabg2.zzcpn.zza(zzzz2.applicationInfo);
        }
        zznb zznb = zziz;
        zzajp zza4 = zzajg.zza(zzi, ((Long) zzbs.zzep().zzd(zzmq.zzbnm)).longValue(), TimeUnit.MILLISECONDS, newSingleThreadScheduledExecutor);
        Future zzi2 = zzajg.zzi(null);
        if (((Boolean) zzbs.zzep().zzd(zzmq.zzbjt)).booleanValue()) {
            zzi2 = zzajg.zza(zzabg2.zzcpr.zzac(context2), ((Long) zzbs.zzep().zzd(zzmq.zzbju)).longValue(), TimeUnit.MILLISECONDS, newSingleThreadScheduledExecutor);
        }
        Bundle bundle2 = (zzzz2.versionCode < 4 || zzzz2.zzclv == null) ? null : zzzz2.zzclv;
        ((Boolean) zzbs.zzep().zzd(zzmq.zzbin)).booleanValue();
        zzbs.zzec();
        if (zzagr.zzd(context2, context.getPackageName(), "android.permission.ACCESS_NETWORK_STATE") && ((ConnectivityManager) context2.getSystemService("connectivity")).getActiveNetworkInfo() == null) {
            zzafj.zzbw("Device is offline.");
        }
        String uuid = zzzz2.versionCode >= 7 ? zzzz2.zzcmb : UUID.randomUUID().toString();
        new zzabn(context2, uuid, zzzz2.applicationInfo.packageName);
        if (zzzz2.zzclo.extras != null && (string = zzzz2.zzclo.extras.getString("_ad")) != null) {
            return zzabm.zza(context2, zzzz2, string);
        }
        List<String> zze = zzabg2.zzcpl.zze(zzzz2.zzcmc);
        ScheduledExecutorService scheduledExecutorService = newSingleThreadScheduledExecutor;
        String str = uuid;
        zznd zznd2 = zznd;
        Bundle bundle3 = (Bundle) zzajg.zza(zza, (Object) null, ((Long) zzbs.zzep().zzd(zzmq.zzbod)).longValue(), TimeUnit.MILLISECONDS);
        zzace zzace = (zzace) zzajg.zza(zza2, (Object) null);
        Location location = (Location) zzajg.zza(zza4, (Object) null);
        AdvertisingIdClient.Info info = (AdvertisingIdClient.Info) zzajg.zza(zzi2, (Object) null);
        String str2 = (String) zzajg.zza(zza3, (Object) null);
        String str3 = (String) zzajg.zza(zzbx, (Object) null);
        zzabu zzabu = (zzabu) zzajg.zza(zzp, (Object) null);
        if (zzabu == null) {
            zzafj.zzco("Error fetching device info. This is not recoverable.");
            return new zzaad(0);
        }
        zzabf zzabf = new zzabf();
        zzabf.zzcpe = zzzz2;
        zzabf.zzcpf = zzabu;
        zzabf.zzcpb = zzace;
        zzabf.zzbcd = location;
        zzabf.zzcpa = bundle3;
        zzabf.zzclq = str2;
        zzabf.zzcpd = info;
        if (zze == null) {
            zzabf.zzcmc.clear();
        }
        zzabf.zzcmc = zze;
        zzabf.zzclv = bundle2;
        zzabf.zzcpc = str3;
        zzabf.zzcpg = zzabg2.zzcpj.zzf(context2);
        zzabf.zzcph = zzabg2.zzcph;
        JSONObject zza5 = zzabm.zza(context2, zzabf);
        if (zza5 == null) {
            return new zzaad(0);
        }
        if (zzzz2.versionCode < 7) {
            try {
                zza5.put("request_id", str);
            } catch (JSONException unused) {
            }
        }
        zza5.toString();
        zznb zznb2 = zznb;
        zznd zznd3 = zznd2;
        zznd3.zza(zznb2, "arc");
        zznd3.zziz();
        zzajp zza6 = zzajg.zza(zzajg.zza(zzabg2.zzcps.zznk().zzg(zza5), zzabi.zzcpv, Executors.newSingleThreadScheduledExecutor()), 10, TimeUnit.SECONDS, scheduledExecutorService);
        zzajp<Void> zznt = zzabg2.zzcpm.zznt();
        if (zznt != null) {
            zzaje.zza(zznt, "AdRequestServiceImpl.loadAd.flags");
        }
        zzabt zzabt = (zzabt) zzajg.zza(zza6, (Object) null);
        if (zzabt == null) {
            return new zzaad(0);
        }
        if (zzabt.getErrorCode() != -2) {
            return new zzaad(zzabt.getErrorCode());
        }
        zznd3.zzjc();
        zzaad zza7 = !TextUtils.isEmpty(zzabt.zznq()) ? zzabm.zza(context2, zzzz2, zzabt.zznq()) : null;
        if (zza7 == null && !TextUtils.isEmpty(zzabt.getUrl())) {
            zza7 = zza(zzzz2, context2, zzzz2.zzatd.zzcp, zzabt.getUrl(), str3, zzabt, zznd3, zzabg2);
        }
        if (zza7 == null) {
            c = 0;
            zza7 = new zzaad(0);
        } else {
            c = 0;
        }
        String[] strArr = new String[1];
        strArr[c] = "tts";
        zznd3.zza(zznb2, strArr);
        zza7.zzcnt = zznd3.zzja();
        return zza7;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00bf, code lost:
        r6 = r6.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        r9 = new java.io.InputStreamReader(r11.getInputStream());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        com.google.android.gms.ads.internal.zzbs.zzec();
        r10 = com.google.android.gms.internal.zzagr.zza(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        com.google.android.gms.common.util.zzn.closeQuietly(r9);
        zza(r6, r3, r10, r1);
        r5.zza(r6, r3, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00dc, code lost:
        if (r2 == null) goto L_0x00e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00de, code lost:
        r2.zza(r4, "ufe");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00e9, code lost:
        r1 = r5.zza(r7, r23.zznr());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        r11.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00f4, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00f5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f6, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00f8, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00f9, code lost:
        r1 = r0;
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        com.google.android.gms.common.util.zzn.closeQuietly(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00fe, code lost:
        throw r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.internal.zzaad zza(com.google.android.gms.internal.zzzz r18, android.content.Context r19, java.lang.String r20, java.lang.String r21, java.lang.String r22, com.google.android.gms.internal.zzabt r23, com.google.android.gms.internal.zznd r24, com.google.android.gms.internal.zzabg r25) {
        /*
            r1 = r18
            r2 = r24
            if (r2 == 0) goto L_0x000b
            com.google.android.gms.internal.zznb r4 = r24.zziz()
            goto L_0x000c
        L_0x000b:
            r4 = 0
        L_0x000c:
            com.google.android.gms.internal.zzabr r5 = new com.google.android.gms.internal.zzabr     // Catch:{ IOException -> 0x0181 }
            java.lang.String r6 = r23.zznn()     // Catch:{ IOException -> 0x0181 }
            r5.<init>(r1, r6)     // Catch:{ IOException -> 0x0181 }
            java.lang.String r6 = "AdRequestServiceImpl: Sending request: "
            java.lang.String r7 = java.lang.String.valueOf(r21)     // Catch:{ IOException -> 0x0181 }
            int r8 = r7.length()     // Catch:{ IOException -> 0x0181 }
            if (r8 == 0) goto L_0x0026
            java.lang.String r6 = r6.concat(r7)     // Catch:{ IOException -> 0x0181 }
            goto L_0x002c
        L_0x0026:
            java.lang.String r7 = new java.lang.String     // Catch:{ IOException -> 0x0181 }
            r7.<init>(r6)     // Catch:{ IOException -> 0x0181 }
            r6 = r7
        L_0x002c:
            com.google.android.gms.internal.zzafj.zzbw(r6)     // Catch:{ IOException -> 0x0181 }
            java.net.URL r6 = new java.net.URL     // Catch:{ IOException -> 0x0181 }
            r7 = r21
            r6.<init>(r7)     // Catch:{ IOException -> 0x0181 }
            com.google.android.gms.common.util.zzd r7 = com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ IOException -> 0x0181 }
            long r7 = r7.elapsedRealtime()     // Catch:{ IOException -> 0x0181 }
            r9 = 0
            r10 = r9
        L_0x0040:
            java.net.URLConnection r11 = r6.openConnection()     // Catch:{ IOException -> 0x0181 }
            java.net.HttpURLConnection r11 = (java.net.HttpURLConnection) r11     // Catch:{ IOException -> 0x0181 }
            com.google.android.gms.internal.zzagr r12 = com.google.android.gms.ads.internal.zzbs.zzec()     // Catch:{ all -> 0x017b }
            r13 = r19
            r14 = r20
            r12.zza(r13, r14, r9, r11)     // Catch:{ all -> 0x017b }
            boolean r12 = android.text.TextUtils.isEmpty(r22)     // Catch:{ all -> 0x017b }
            if (r12 != 0) goto L_0x0065
            boolean r12 = r23.zznp()     // Catch:{ all -> 0x017b }
            if (r12 == 0) goto L_0x0065
            java.lang.String r12 = "x-afma-drt-cookie"
            r15 = r22
            r11.addRequestProperty(r12, r15)     // Catch:{ all -> 0x017b }
            goto L_0x0067
        L_0x0065:
            r15 = r22
        L_0x0067:
            java.lang.String r12 = r1.zzcml     // Catch:{ all -> 0x017b }
            boolean r16 = android.text.TextUtils.isEmpty(r12)     // Catch:{ all -> 0x017b }
            if (r16 != 0) goto L_0x0079
            java.lang.String r3 = "Sending webview cookie in ad request header."
            com.google.android.gms.internal.zzafj.zzbw(r3)     // Catch:{ all -> 0x017b }
            java.lang.String r3 = "Cookie"
            r11.addRequestProperty(r3, r12)     // Catch:{ all -> 0x017b }
        L_0x0079:
            r3 = 1
            if (r23 == 0) goto L_0x00af
            java.lang.String r9 = r23.zzno()     // Catch:{ all -> 0x017b }
            boolean r9 = android.text.TextUtils.isEmpty(r9)     // Catch:{ all -> 0x017b }
            if (r9 != 0) goto L_0x00af
            r11.setDoOutput(r3)     // Catch:{ all -> 0x017b }
            java.lang.String r9 = r23.zzno()     // Catch:{ all -> 0x017b }
            byte[] r9 = r9.getBytes()     // Catch:{ all -> 0x017b }
            int r3 = r9.length     // Catch:{ all -> 0x017b }
            r11.setFixedLengthStreamingMode(r3)     // Catch:{ all -> 0x017b }
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x00a8 }
            java.io.OutputStream r1 = r11.getOutputStream()     // Catch:{ all -> 0x00a8 }
            r3.<init>(r1)     // Catch:{ all -> 0x00a8 }
            r3.write(r9)     // Catch:{ all -> 0x00a5 }
            com.google.android.gms.common.util.zzn.closeQuietly(r3)     // Catch:{ all -> 0x017b }
            goto L_0x00af
        L_0x00a5:
            r0 = move-exception
            r1 = r0
            goto L_0x00ab
        L_0x00a8:
            r0 = move-exception
            r1 = r0
            r3 = 0
        L_0x00ab:
            com.google.android.gms.common.util.zzn.closeQuietly(r3)     // Catch:{ all -> 0x017b }
            throw r1     // Catch:{ all -> 0x017b }
        L_0x00af:
            int r1 = r11.getResponseCode()     // Catch:{ all -> 0x017b }
            java.util.Map r3 = r11.getHeaderFields()     // Catch:{ all -> 0x017b }
            r9 = 200(0xc8, float:2.8E-43)
            r13 = 300(0x12c, float:4.2E-43)
            if (r1 < r9) goto L_0x00ff
            if (r1 >= r13) goto L_0x00ff
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x017b }
            java.io.InputStreamReader r9 = new java.io.InputStreamReader     // Catch:{ all -> 0x00f8 }
            java.io.InputStream r10 = r11.getInputStream()     // Catch:{ all -> 0x00f8 }
            r9.<init>(r10)     // Catch:{ all -> 0x00f8 }
            com.google.android.gms.ads.internal.zzbs.zzec()     // Catch:{ all -> 0x00f5 }
            java.lang.String r10 = com.google.android.gms.internal.zzagr.zza(r9)     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.common.util.zzn.closeQuietly(r9)     // Catch:{ all -> 0x017b }
            zza(r6, r3, r10, r1)     // Catch:{ all -> 0x017b }
            r5.zza(r6, r3, r10)     // Catch:{ all -> 0x017b }
            if (r2 == 0) goto L_0x00e9
            r1 = 1
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ all -> 0x017b }
            java.lang.String r3 = "ufe"
            r6 = 0
            r1[r6] = r3     // Catch:{ all -> 0x017b }
            r2.zza(r4, r1)     // Catch:{ all -> 0x017b }
        L_0x00e9:
            boolean r1 = r23.zznr()     // Catch:{ all -> 0x017b }
            com.google.android.gms.internal.zzaad r1 = r5.zza(r7, r1)     // Catch:{ all -> 0x017b }
            r11.disconnect()     // Catch:{ IOException -> 0x0181 }
            return r1
        L_0x00f5:
            r0 = move-exception
            r1 = r0
            goto L_0x00fb
        L_0x00f8:
            r0 = move-exception
            r1 = r0
            r9 = 0
        L_0x00fb:
            com.google.android.gms.common.util.zzn.closeQuietly(r9)     // Catch:{ all -> 0x017b }
            throw r1     // Catch:{ all -> 0x017b }
        L_0x00ff:
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x017b }
            r9 = 0
            zza(r6, r3, r9, r1)     // Catch:{ all -> 0x017b }
            if (r1 < r13) goto L_0x015b
            r6 = 400(0x190, float:5.6E-43)
            if (r1 >= r6) goto L_0x015b
            java.lang.String r1 = "Location"
            java.lang.String r1 = r11.getHeaderField(r1)     // Catch:{ all -> 0x017b }
            boolean r6 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x017b }
            if (r6 == 0) goto L_0x0128
            java.lang.String r1 = "No location header to follow redirect."
            com.google.android.gms.internal.zzafj.zzco(r1)     // Catch:{ all -> 0x017b }
            com.google.android.gms.internal.zzaad r1 = new com.google.android.gms.internal.zzaad     // Catch:{ all -> 0x017b }
            r2 = 0
            r1.<init>(r2)     // Catch:{ all -> 0x017b }
            r11.disconnect()     // Catch:{ IOException -> 0x0181 }
            return r1
        L_0x0128:
            java.net.URL r6 = new java.net.URL     // Catch:{ all -> 0x017b }
            r6.<init>(r1)     // Catch:{ all -> 0x017b }
            r1 = 1
            int r10 = r10 + r1
            com.google.android.gms.internal.zzmg<java.lang.Integer> r1 = com.google.android.gms.internal.zzmq.zzbpm     // Catch:{ all -> 0x017b }
            com.google.android.gms.internal.zzmo r13 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ all -> 0x017b }
            java.lang.Object r1 = r13.zzd(r1)     // Catch:{ all -> 0x017b }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ all -> 0x017b }
            int r1 = r1.intValue()     // Catch:{ all -> 0x017b }
            if (r10 <= r1) goto L_0x0150
            java.lang.String r1 = "Too many redirects."
            com.google.android.gms.internal.zzafj.zzco(r1)     // Catch:{ all -> 0x017b }
            com.google.android.gms.internal.zzaad r1 = new com.google.android.gms.internal.zzaad     // Catch:{ all -> 0x017b }
            r2 = 0
            r1.<init>(r2)     // Catch:{ all -> 0x017b }
            r11.disconnect()     // Catch:{ IOException -> 0x0181 }
            return r1
        L_0x0150:
            r5.zzn(r3)     // Catch:{ all -> 0x017b }
            r11.disconnect()     // Catch:{ IOException -> 0x0181 }
            r1 = r18
            r9 = 0
            goto L_0x0040
        L_0x015b:
            r2 = 46
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x017b }
            r3.<init>(r2)     // Catch:{ all -> 0x017b }
            java.lang.String r2 = "Received error HTTP response code: "
            r3.append(r2)     // Catch:{ all -> 0x017b }
            r3.append(r1)     // Catch:{ all -> 0x017b }
            java.lang.String r1 = r3.toString()     // Catch:{ all -> 0x017b }
            com.google.android.gms.internal.zzafj.zzco(r1)     // Catch:{ all -> 0x017b }
            com.google.android.gms.internal.zzaad r1 = new com.google.android.gms.internal.zzaad     // Catch:{ all -> 0x017b }
            r2 = 0
            r1.<init>(r2)     // Catch:{ all -> 0x017b }
            r11.disconnect()     // Catch:{ IOException -> 0x0181 }
            return r1
        L_0x017b:
            r0 = move-exception
            r1 = r0
            r11.disconnect()     // Catch:{ IOException -> 0x0181 }
            throw r1     // Catch:{ IOException -> 0x0181 }
        L_0x0181:
            r0 = move-exception
            r1 = r0
            java.lang.String r2 = "Error while connecting to ad server: "
            java.lang.String r1 = r1.getMessage()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r3 = r1.length()
            if (r3 == 0) goto L_0x0198
            java.lang.String r1 = r2.concat(r1)
            goto L_0x019d
        L_0x0198:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r2)
        L_0x019d:
            com.google.android.gms.internal.zzafj.zzco(r1)
            com.google.android.gms.internal.zzaad r1 = new com.google.android.gms.internal.zzaad
            r2 = 2
            r1.<init>(r2)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzabh.zza(com.google.android.gms.internal.zzzz, android.content.Context, java.lang.String, java.lang.String, java.lang.String, com.google.android.gms.internal.zzabt, com.google.android.gms.internal.zznd, com.google.android.gms.internal.zzabg):com.google.android.gms.internal.zzaad");
    }

    public static zzabh zza(Context context, zzabg zzabg) {
        zzabh zzabh;
        synchronized (sLock) {
            if (zzcpt == null) {
                if (context.getApplicationContext() != null) {
                    context = context.getApplicationContext();
                }
                zzmq.initialize(context);
                zzcpt = new zzabh(context, zzabg);
            }
            zzabh = zzcpt;
        }
        return zzabh;
    }

    private static void zza(String str, Map<String, List<String>> map, String str2, int i) {
        if (zzafj.zzae(2)) {
            StringBuilder sb = new StringBuilder(39 + String.valueOf(str).length());
            sb.append("Http Response: {\n  URL:\n    ");
            sb.append(str);
            sb.append("\n  Headers:");
            zzafj.v(sb.toString());
            if (map != null) {
                for (String next : map.keySet()) {
                    StringBuilder sb2 = new StringBuilder(5 + String.valueOf(next).length());
                    sb2.append("    ");
                    sb2.append(next);
                    sb2.append(":");
                    zzafj.v(sb2.toString());
                    for (String valueOf : map.get(next)) {
                        String valueOf2 = String.valueOf(valueOf);
                        zzafj.v(valueOf2.length() != 0 ? "      ".concat(valueOf2) : new String("      "));
                    }
                }
            }
            zzafj.v("  Body:");
            if (str2 != null) {
                int i2 = 0;
                while (i2 < Math.min(str2.length(), 100000)) {
                    int i3 = i2 + 1000;
                    zzafj.v(str2.substring(i2, Math.min(str2.length(), i3)));
                    i2 = i3;
                }
            } else {
                zzafj.v("    null");
            }
            StringBuilder sb3 = new StringBuilder(34);
            sb3.append("  Response Code:\n    ");
            sb3.append(i);
            sb3.append("\n}");
            zzafj.v(sb3.toString());
        }
    }

    public final void zza(zzaas zzaas, zzaan zzaan) {
        zzafj.v("Nonagon code path entered in octagon");
        throw new IllegalArgumentException();
    }

    public final void zza(zzzz zzzz, zzaak zzaak) {
        zzbs.zzeg().zzd(this.mContext, zzzz.zzatd);
        zzajp<Void> zza = zzagl.zza(new zzabj(this, zzzz, zzaak));
        zzbs.zzet().zzqm();
        zzbs.zzet().getHandler().postDelayed(new zzabk(this, zza), 60000);
    }

    public final zzaad zzb(zzzz zzzz) {
        return zza(this.mContext, this.zzcpu, zzzz);
    }
}
