package com.google.android.gms.games.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzcn;
import com.google.android.gms.common.api.internal.zzdp;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class zzr<L> extends zzdp<GamesClientImpl, L> {
    protected zzr(zzcn<L> zzcn) {
        super(zzcn);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zzc(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        try {
            zzc((GamesClientImpl) zzb, (TaskCompletionSource<Boolean>) taskCompletionSource);
        } catch (SecurityException e) {
            taskCompletionSource.trySetException(e);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void zzc(GamesClientImpl gamesClientImpl, TaskCompletionSource<Boolean> taskCompletionSource) throws RemoteException;
}
