package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzbmq extends zzblk {
    private /* synthetic */ zzboz zzgkh;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbmq(zzbmo zzbmo, GoogleApiClient googleApiClient, zzboz zzboz) {
        super(googleApiClient);
        this.zzgkh = zzboz;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbrf(this.zzgkh), new zzbrj(this));
    }
}
