package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class zzct<A extends Api.zzb, L> {
    private final zzcl<L> zzfry;

    protected zzct(zzcl<L> zzcl) {
        this.zzfry = zzcl;
    }

    public final zzcn<L> zzajc() {
        return this.zzfry.zzajc();
    }

    public final void zzajd() {
        this.zzfry.clear();
    }

    /* access modifiers changed from: protected */
    public abstract void zzb(A a, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException;
}
