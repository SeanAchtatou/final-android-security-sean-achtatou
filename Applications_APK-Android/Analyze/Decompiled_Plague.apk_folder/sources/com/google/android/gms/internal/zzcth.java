package com.google.android.gms.internal;

final /* synthetic */ class zzcth implements zzctm {
    private final zzctg zzjun;
    private final zzcss zzjuo;

    zzcth(zzctg zzctg, zzcss zzcss) {
        this.zzjun = zzctg;
        this.zzjuo = zzcss;
    }

    public final Object zzbcd() {
        return this.zzjuo.zzbca().get(this.zzjun.zzjuj);
    }
}
