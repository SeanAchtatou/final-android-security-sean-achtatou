package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzby extends zzbz {
    private /* synthetic */ int zzhpz;
    private /* synthetic */ int zzhql;
    private /* synthetic */ int zzhqm;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzby(zzbv zzbv, GoogleApiClient googleApiClient, int i, int i2, int i3) {
        super(googleApiClient, null);
        this.zzhql = i;
        this.zzhqm = i2;
        this.zzhpz = i3;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhql, this.zzhqm, this.zzhpz);
    }
}
