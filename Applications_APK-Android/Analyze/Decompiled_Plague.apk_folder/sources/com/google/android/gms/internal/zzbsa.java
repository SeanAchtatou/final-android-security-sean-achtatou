package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzdg;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzbsa extends zzbrs<Void> {
    public zzbsa(TaskCompletionSource<Void> taskCompletionSource) {
        super(taskCompletionSource);
    }

    public final void onSuccess() throws RemoteException {
        zzdg.zza(Status.zzfko, null, zzaox());
    }
}
