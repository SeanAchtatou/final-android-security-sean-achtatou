package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.common.zze;
import com.google.android.gms.internal.zzcwb;
import com.google.android.gms.internal.zzcwc;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

final class zzy implements zzcf {
    private final Context mContext;
    private final Looper zzakm;
    private final zzbd zzfmo;
    /* access modifiers changed from: private */
    public final zzbl zzfmp;
    /* access modifiers changed from: private */
    public final zzbl zzfmq;
    private final Map<Api.zzc<?>, zzbl> zzfmr;
    private final Set<zzcx> zzfms = Collections.newSetFromMap(new WeakHashMap());
    private final Api.zze zzfmt;
    private Bundle zzfmu;
    /* access modifiers changed from: private */
    public ConnectionResult zzfmv = null;
    /* access modifiers changed from: private */
    public ConnectionResult zzfmw = null;
    /* access modifiers changed from: private */
    public boolean zzfmx = false;
    /* access modifiers changed from: private */
    public final Lock zzfmy;
    private int zzfmz = 0;

    private zzy(Context context, zzbd zzbd, Lock lock, Looper looper, zze zze, Map<Api.zzc<?>, Api.zze> map, Map<Api.zzc<?>, Api.zze> map2, zzr zzr, Api.zza<? extends zzcwb, zzcwc> zza, Api.zze zze2, ArrayList<zzw> arrayList, ArrayList<zzw> arrayList2, Map<Api<?>, Boolean> map3, Map<Api<?>, Boolean> map4) {
        Context context2 = context;
        this.mContext = context2;
        this.zzfmo = zzbd;
        Lock lock2 = lock;
        this.zzfmy = lock2;
        Looper looper2 = looper;
        this.zzakm = looper2;
        this.zzfmt = zze2;
        Context context3 = context2;
        Lock lock3 = lock2;
        zze zze3 = zze;
        zzbl zzbl = r3;
        zzbl zzbl2 = new zzbl(context3, this.zzfmo, lock3, looper2, zze3, map2, null, map4, null, arrayList2, new zzaa(this, null));
        this.zzfmp = zzbl;
        this.zzfmq = new zzbl(context3, this.zzfmo, lock3, looper, zze3, map, zzr, map3, zza, arrayList, new zzab(this, null));
        ArrayMap arrayMap = new ArrayMap();
        for (Api.zzc<?> put : map2.keySet()) {
            arrayMap.put(put, this.zzfmp);
        }
        for (Api.zzc<?> put2 : map.keySet()) {
            arrayMap.put(put2, this.zzfmq);
        }
        this.zzfmr = Collections.unmodifiableMap(arrayMap);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public static zzy zza(Context context, zzbd zzbd, Lock lock, Looper looper, zze zze, Map<Api.zzc<?>, Api.zze> map, zzr zzr, Map<Api<?>, Boolean> map2, Api.zza<? extends zzcwb, zzcwc> zza, ArrayList<zzw> arrayList) {
        Map<Api<?>, Boolean> map3 = map2;
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        Api.zze zze2 = null;
        for (Map.Entry next : map.entrySet()) {
            Api.zze zze3 = (Api.zze) next.getValue();
            if (zze3.zzaaw()) {
                zze2 = zze3;
            }
            if (zze3.zzaam()) {
                arrayMap.put((Api.zzc) next.getKey(), zze3);
            } else {
                arrayMap2.put((Api.zzc) next.getKey(), zze3);
            }
        }
        zzbq.zza(!arrayMap.isEmpty(), (Object) "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        ArrayMap arrayMap3 = new ArrayMap();
        ArrayMap arrayMap4 = new ArrayMap();
        for (Api next2 : map2.keySet()) {
            Api.zzc<?> zzaft = next2.zzaft();
            if (arrayMap.containsKey(zzaft)) {
                arrayMap3.put(next2, map3.get(next2));
            } else if (arrayMap2.containsKey(zzaft)) {
                arrayMap4.put(next2, map3.get(next2));
            } else {
                throw new IllegalStateException("Each API in the isOptionalMap must have a corresponding client in the clients map.");
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = arrayList;
        int size = arrayList4.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList4.get(i);
            i++;
            zzw zzw = (zzw) obj;
            if (arrayMap3.containsKey(zzw.zzffv)) {
                arrayList2.add(zzw);
            } else if (arrayMap4.containsKey(zzw.zzffv)) {
                arrayList3.add(zzw);
            } else {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the isOptionalMap");
            }
        }
        return new zzy(context, zzbd, lock, looper, zze, arrayMap, arrayMap2, zzr, zza, zze2, arrayList2, arrayList3, arrayMap3, arrayMap4);
    }

    private final void zza(ConnectionResult connectionResult) {
        switch (this.zzfmz) {
            case 2:
                this.zzfmo.zzc(connectionResult);
            case 1:
                zzaha();
                break;
            default:
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                break;
        }
        this.zzfmz = 0;
    }

    /* access modifiers changed from: private */
    public final void zzagz() {
        if (zzb(this.zzfmv)) {
            if (zzb(this.zzfmw) || zzahb()) {
                switch (this.zzfmz) {
                    case 2:
                        this.zzfmo.zzj(this.zzfmu);
                    case 1:
                        zzaha();
                        break;
                    default:
                        Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                        break;
                }
                this.zzfmz = 0;
            } else if (this.zzfmw == null) {
            } else {
                if (this.zzfmz == 1) {
                    zzaha();
                    return;
                }
                zza(this.zzfmw);
                this.zzfmp.disconnect();
            }
        } else if (this.zzfmv != null && zzb(this.zzfmw)) {
            this.zzfmq.disconnect();
            zza(this.zzfmv);
        } else if (this.zzfmv != null && this.zzfmw != null) {
            ConnectionResult connectionResult = this.zzfmv;
            if (this.zzfmq.zzfpz < this.zzfmp.zzfpz) {
                connectionResult = this.zzfmw;
            }
            zza(connectionResult);
        }
    }

    private final void zzaha() {
        for (zzcx zzaav : this.zzfms) {
            zzaav.zzaav();
        }
        this.zzfms.clear();
    }

    private final boolean zzahb() {
        return this.zzfmw != null && this.zzfmw.getErrorCode() == 4;
    }

    @Nullable
    private final PendingIntent zzahc() {
        if (this.zzfmt == null) {
            return null;
        }
        return PendingIntent.getActivity(this.mContext, System.identityHashCode(this.zzfmo), this.zzfmt.getSignInIntent(), 134217728);
    }

    private static boolean zzb(ConnectionResult connectionResult) {
        return connectionResult != null && connectionResult.isSuccess();
    }

    /* access modifiers changed from: private */
    public final void zze(int i, boolean z) {
        this.zzfmo.zzf(i, z);
        this.zzfmw = null;
        this.zzfmv = null;
    }

    private final boolean zzf(zzm<? extends Result, ? extends Api.zzb> zzm) {
        Api.zzc<? extends Api.zzb> zzaft = zzm.zzaft();
        zzbq.checkArgument(this.zzfmr.containsKey(zzaft), "GoogleApiClient is not configured to use the API required for this call.");
        return this.zzfmr.get(zzaft).equals(this.zzfmq);
    }

    /* access modifiers changed from: private */
    public final void zzi(Bundle bundle) {
        if (this.zzfmu == null) {
            this.zzfmu = bundle;
        } else if (bundle != null) {
            this.zzfmu.putAll(bundle);
        }
    }

    public final ConnectionResult blockingConnect() {
        throw new UnsupportedOperationException();
    }

    public final ConnectionResult blockingConnect(long j, @NonNull TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public final void connect() {
        this.zzfmz = 2;
        this.zzfmx = false;
        this.zzfmw = null;
        this.zzfmv = null;
        this.zzfmp.connect();
        this.zzfmq.connect();
    }

    public final void disconnect() {
        this.zzfmw = null;
        this.zzfmv = null;
        this.zzfmz = 0;
        this.zzfmp.disconnect();
        this.zzfmq.disconnect();
        zzaha();
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append((CharSequence) "authClient").println(":");
        this.zzfmq.dump(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        printWriter.append((CharSequence) str).append((CharSequence) "anonClient").println(":");
        this.zzfmp.dump(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
    }

    @Nullable
    public final ConnectionResult getConnectionResult(@NonNull Api<?> api) {
        return this.zzfmr.get(api.zzaft()).equals(this.zzfmq) ? zzahb() ? new ConnectionResult(4, zzahc()) : this.zzfmq.getConnectionResult(api) : this.zzfmp.getConnectionResult(api);
    }

    public final boolean isConnected() {
        this.zzfmy.lock();
        try {
            boolean z = true;
            if (!this.zzfmp.isConnected() || (!this.zzfmq.isConnected() && !zzahb() && this.zzfmz != 1)) {
                z = false;
            }
            return z;
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final boolean isConnecting() {
        this.zzfmy.lock();
        try {
            return this.zzfmz == 2;
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final boolean zza(zzcx zzcx) {
        this.zzfmy.lock();
        try {
            if ((isConnecting() || isConnected()) && !this.zzfmq.isConnected()) {
                this.zzfms.add(zzcx);
                if (this.zzfmz == 0) {
                    this.zzfmz = 1;
                }
                this.zzfmw = null;
                this.zzfmq.connect();
                return true;
            }
            this.zzfmy.unlock();
            return false;
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final void zzagf() {
        this.zzfmy.lock();
        try {
            boolean isConnecting = isConnecting();
            this.zzfmq.disconnect();
            this.zzfmw = new ConnectionResult(4);
            if (isConnecting) {
                new Handler(this.zzakm).post(new zzz(this));
            } else {
                zzaha();
            }
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final void zzagy() {
        this.zzfmp.zzagy();
        this.zzfmq.zzagy();
    }

    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T zzd(@NonNull zzm zzm) {
        if (!zzf(zzm)) {
            return this.zzfmp.zzd(zzm);
        }
        if (!zzahb()) {
            return this.zzfmq.zzd(zzm);
        }
        zzm.zzu(new Status(4, null, zzahc()));
        return zzm;
    }

    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T zze(@NonNull zzm zzm) {
        if (!zzf(zzm)) {
            return this.zzfmp.zze(zzm);
        }
        if (!zzahb()) {
            return this.zzfmq.zze(zzm);
        }
        zzm.zzu(new Status(4, null, zzahc()));
        return zzm;
    }
}
