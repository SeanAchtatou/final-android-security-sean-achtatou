package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbdu implements Parcelable.Creator<zzbdt> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        int i = 0;
        int i2 = 0;
        boolean z = false;
        int i3 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        boolean z2 = true;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = zzbek.zzq(parcel, readInt);
                    break;
                case 3:
                    i = zzbek.zzg(parcel, readInt);
                    break;
                case 4:
                    i2 = zzbek.zzg(parcel, readInt);
                    break;
                case 5:
                    str2 = zzbek.zzq(parcel, readInt);
                    break;
                case 6:
                    str3 = zzbek.zzq(parcel, readInt);
                    break;
                case 7:
                    z2 = zzbek.zzc(parcel, readInt);
                    break;
                case 8:
                    str4 = zzbek.zzq(parcel, readInt);
                    break;
                case 9:
                    z = zzbek.zzc(parcel, readInt);
                    break;
                case 10:
                    i3 = zzbek.zzg(parcel, readInt);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbdt(str, i, i2, str2, str3, z2, str4, z, i3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbdt[i];
    }
}
