package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.zzbq;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONException;

public final class zzz {
    private static final Lock zzefu = new ReentrantLock();
    private static zzz zzefv;
    private final Lock zzefw = new ReentrantLock();
    private final SharedPreferences zzefx;

    private zzz(Context context) {
        this.zzefx = context.getSharedPreferences("com.google.android.gms.signin", 0);
    }

    public static zzz zzbr(Context context) {
        zzbq.checkNotNull(context);
        zzefu.lock();
        try {
            if (zzefv == null) {
                zzefv = new zzz(context.getApplicationContext());
            }
            return zzefv;
        } finally {
            zzefu.unlock();
        }
    }

    private final GoogleSignInAccount zzeq(String str) {
        String zzes;
        if (!TextUtils.isEmpty(str) && (zzes = zzes(zzp("googleSignInAccount", str))) != null) {
            try {
                return GoogleSignInAccount.zzen(zzes);
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    private final GoogleSignInOptions zzer(String str) {
        String zzes;
        if (!TextUtils.isEmpty(str) && (zzes = zzes(zzp("googleSignInOptions", str))) != null) {
            try {
                return GoogleSignInOptions.zzeo(zzes);
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    private final String zzes(String str) {
        this.zzefw.lock();
        try {
            return this.zzefx.getString(str, null);
        } finally {
            this.zzefw.unlock();
        }
    }

    private final void zzet(String str) {
        this.zzefw.lock();
        try {
            this.zzefx.edit().remove(str).apply();
        } finally {
            this.zzefw.unlock();
        }
    }

    private static String zzp(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + String.valueOf(":").length() + String.valueOf(str2).length());
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        return sb.toString();
    }

    public final void clear() {
        this.zzefw.lock();
        try {
            this.zzefx.edit().clear().apply();
        } finally {
            this.zzefw.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(GoogleSignInAccount googleSignInAccount, GoogleSignInOptions googleSignInOptions) {
        zzbq.checkNotNull(googleSignInAccount);
        zzbq.checkNotNull(googleSignInOptions);
        String zzaao = googleSignInAccount.zzaao();
        zzo(zzp("googleSignInAccount", zzaao), googleSignInAccount.zzaap());
        zzo(zzp("googleSignInOptions", zzaao), googleSignInOptions.zzaat());
    }

    public final GoogleSignInAccount zzabg() {
        return zzeq(zzes("defaultGoogleSignInAccount"));
    }

    public final GoogleSignInOptions zzabh() {
        return zzer(zzes("defaultGoogleSignInAccount"));
    }

    public final void zzabi() {
        String zzes = zzes("defaultGoogleSignInAccount");
        zzet("defaultGoogleSignInAccount");
        if (!TextUtils.isEmpty(zzes)) {
            zzet(zzp("googleSignInAccount", zzes));
            zzet(zzp("googleSignInOptions", zzes));
        }
    }

    /* access modifiers changed from: protected */
    public final void zzo(String str, String str2) {
        this.zzefw.lock();
        try {
            this.zzefx.edit().putString(str, str2).apply();
        } finally {
            this.zzefw.unlock();
        }
    }
}
