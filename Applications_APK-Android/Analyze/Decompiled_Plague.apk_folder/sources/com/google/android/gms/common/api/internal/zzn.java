package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;

public interface zzn<R> {
    void setResult(Object obj);

    void zzu(Status status);
}
