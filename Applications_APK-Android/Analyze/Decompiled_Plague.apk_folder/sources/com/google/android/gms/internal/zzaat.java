package com.google.android.gms.internal;

import android.os.Parcelable;

public final class zzaat implements Parcelable.Creator<zzaas> {
    /* JADX WARN: Type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r1v5, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r13) {
        /*
            r12 = this;
            int r0 = com.google.android.gms.internal.zzbek.zzd(r13)
            r1 = 0
            r2 = 0
            r4 = r1
            r5 = r4
            r6 = r5
            r7 = r6
            r8 = r7
            r9 = r8
            r10 = r9
            r11 = r2
        L_0x000e:
            int r1 = r13.dataPosition()
            if (r1 >= r0) goto L_0x005a
            int r1 = r13.readInt()
            r2 = 65535(0xffff, float:9.1834E-41)
            r2 = r2 & r1
            switch(r2) {
                case 1: goto L_0x0055;
                case 2: goto L_0x004b;
                case 3: goto L_0x0041;
                case 4: goto L_0x003c;
                case 5: goto L_0x0037;
                case 6: goto L_0x002d;
                case 7: goto L_0x0028;
                case 8: goto L_0x0023;
                default: goto L_0x001f;
            }
        L_0x001f:
            com.google.android.gms.internal.zzbek.zzb(r13, r1)
            goto L_0x000e
        L_0x0023:
            boolean r11 = com.google.android.gms.internal.zzbek.zzc(r13, r1)
            goto L_0x000e
        L_0x0028:
            java.lang.String r10 = com.google.android.gms.internal.zzbek.zzq(r13, r1)
            goto L_0x000e
        L_0x002d:
            android.os.Parcelable$Creator r2 = android.content.pm.PackageInfo.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r13, r1, r2)
            r9 = r1
            android.content.pm.PackageInfo r9 = (android.content.pm.PackageInfo) r9
            goto L_0x000e
        L_0x0037:
            java.util.ArrayList r8 = com.google.android.gms.internal.zzbek.zzac(r13, r1)
            goto L_0x000e
        L_0x003c:
            java.lang.String r7 = com.google.android.gms.internal.zzbek.zzq(r13, r1)
            goto L_0x000e
        L_0x0041:
            android.os.Parcelable$Creator r2 = android.content.pm.ApplicationInfo.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r13, r1, r2)
            r6 = r1
            android.content.pm.ApplicationInfo r6 = (android.content.pm.ApplicationInfo) r6
            goto L_0x000e
        L_0x004b:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzaiy> r2 = com.google.android.gms.internal.zzaiy.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r13, r1, r2)
            r5 = r1
            com.google.android.gms.internal.zzaiy r5 = (com.google.android.gms.internal.zzaiy) r5
            goto L_0x000e
        L_0x0055:
            android.os.Bundle r4 = com.google.android.gms.internal.zzbek.zzs(r13, r1)
            goto L_0x000e
        L_0x005a:
            com.google.android.gms.internal.zzbek.zzaf(r13, r0)
            com.google.android.gms.internal.zzaas r13 = new com.google.android.gms.internal.zzaas
            r3 = r13
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11)
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaat.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzaas[i];
    }
}
