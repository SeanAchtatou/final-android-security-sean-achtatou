package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzdf;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.List;

final class zzbnv extends zzdf<zzbll, Void> {
    private /* synthetic */ DriveResource zzgmc;
    private /* synthetic */ List zzgmn;

    zzbnv(zzbmu zzbmu, DriveResource driveResource, List list) {
        this.zzgmc = driveResource;
        this.zzgmn = list;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbrh(this.zzgmc.getDriveId(), this.zzgmn), new zzbsa(taskCompletionSource));
    }
}
