package com.google.android.gms.common.internal;

import android.util.Log;

public abstract class zzi<TListener> {
    private TListener zzfrq;
    private /* synthetic */ zzd zzfwg;
    private boolean zzfwh = false;

    public zzi(zzd zzd, TListener tlistener) {
        this.zzfwg = zzd;
        this.zzfrq = tlistener;
    }

    public final void removeListener() {
        synchronized (this) {
            this.zzfrq = null;
        }
    }

    public final void unregister() {
        removeListener();
        synchronized (this.zzfwg.zzfvu) {
            this.zzfwg.zzfvu.remove(this);
        }
    }

    public final void zzakg() {
        TListener tlistener;
        synchronized (this) {
            tlistener = this.zzfrq;
            if (this.zzfwh) {
                String valueOf = String.valueOf(this);
                StringBuilder sb = new StringBuilder(47 + String.valueOf(valueOf).length());
                sb.append("Callback proxy ");
                sb.append(valueOf);
                sb.append(" being reused. This is not safe.");
                Log.w("GmsClient", sb.toString());
            }
        }
        if (tlistener != null) {
            try {
                zzv(tlistener);
            } catch (RuntimeException e) {
                throw e;
            }
        }
        synchronized (this) {
            this.zzfwh = true;
        }
        unregister();
    }

    /* access modifiers changed from: protected */
    public abstract void zzv(TListener tlistener);
}
