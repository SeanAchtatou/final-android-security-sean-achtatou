package com.google.android.gms.drive.events;

import android.os.Looper;
import com.google.android.gms.drive.events.DriveEventService;
import java.util.concurrent.CountDownLatch;

final class zzh extends Thread {
    private /* synthetic */ CountDownLatch zzgiy;
    private /* synthetic */ DriveEventService zzgiz;

    zzh(DriveEventService driveEventService, CountDownLatch countDownLatch) {
        this.zzgiz = driveEventService;
        this.zzgiy = countDownLatch;
    }

    public final void run() {
        try {
            Looper.prepare();
            this.zzgiz.zzgiv = new DriveEventService.zza();
            this.zzgiz.zzgiw = false;
            this.zzgiy.countDown();
            DriveEventService.zzggp.zzu("DriveEventService", "Bound and starting loop");
            Looper.loop();
            DriveEventService.zzggp.zzu("DriveEventService", "Finished loop");
        } finally {
            if (this.zzgiz.zzgiu != null) {
                this.zzgiz.zzgiu.countDown();
            }
        }
    }
}
