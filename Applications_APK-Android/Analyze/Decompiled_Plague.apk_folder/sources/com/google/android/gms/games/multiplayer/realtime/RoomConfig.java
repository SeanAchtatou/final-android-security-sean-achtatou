package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.games.multiplayer.Multiplayer;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class RoomConfig {

    public static final class Builder {
        int zzhus;
        @Deprecated
        final RoomUpdateListener zzhve;
        @Deprecated
        RoomStatusUpdateListener zzhvf;
        @Deprecated
        RealTimeMessageReceivedListener zzhvg;
        final RoomUpdateCallback zzhvh;
        RoomStatusUpdateCallback zzhvi;
        OnRealTimeMessageReceivedListener zzhvj;
        String zzhvk;
        ArrayList<String> zzhvl;
        Bundle zzhvm;

        private Builder(@NonNull RoomUpdateCallback roomUpdateCallback) {
            this.zzhvk = null;
            this.zzhus = -1;
            this.zzhvl = new ArrayList<>();
            this.zzhvh = (RoomUpdateCallback) zzbq.checkNotNull(roomUpdateCallback, "Must provide a RoomUpdateCallback");
            this.zzhve = null;
        }

        @Deprecated
        private Builder(RoomUpdateListener roomUpdateListener) {
            this.zzhvk = null;
            this.zzhus = -1;
            this.zzhvl = new ArrayList<>();
            this.zzhve = (RoomUpdateListener) zzbq.checkNotNull(roomUpdateListener, "Must provide a RoomUpdateListener");
            this.zzhvh = null;
        }

        public final Builder addPlayersToInvite(@NonNull ArrayList<String> arrayList) {
            zzbq.checkNotNull(arrayList);
            this.zzhvl.addAll(arrayList);
            return this;
        }

        public final Builder addPlayersToInvite(@NonNull String... strArr) {
            zzbq.checkNotNull(strArr);
            this.zzhvl.addAll(Arrays.asList(strArr));
            return this;
        }

        public final RoomConfig build() {
            return new zzd(this);
        }

        public final Builder setAutoMatchCriteria(Bundle bundle) {
            this.zzhvm = bundle;
            return this;
        }

        public final Builder setInvitationIdToAccept(String str) {
            zzbq.checkNotNull(str);
            this.zzhvk = str;
            return this;
        }

        @Deprecated
        public final Builder setMessageReceivedListener(RealTimeMessageReceivedListener realTimeMessageReceivedListener) {
            this.zzhvg = realTimeMessageReceivedListener;
            return this;
        }

        public final Builder setOnMessageReceivedListener(@NonNull OnRealTimeMessageReceivedListener onRealTimeMessageReceivedListener) {
            this.zzhvj = onRealTimeMessageReceivedListener;
            return this;
        }

        public final Builder setRoomStatusUpdateCallback(@Nullable RoomStatusUpdateCallback roomStatusUpdateCallback) {
            this.zzhvi = roomStatusUpdateCallback;
            return this;
        }

        @Deprecated
        public final Builder setRoomStatusUpdateListener(RoomStatusUpdateListener roomStatusUpdateListener) {
            this.zzhvf = roomStatusUpdateListener;
            return this;
        }

        public final Builder setVariant(int i) {
            zzbq.checkArgument(i == -1 || i > 0, "Variant must be a positive integer or Room.ROOM_VARIANT_ANY");
            this.zzhus = i;
            return this;
        }
    }

    protected RoomConfig() {
    }

    public static Builder builder(@NonNull RoomUpdateCallback roomUpdateCallback) {
        return new Builder(roomUpdateCallback);
    }

    @Deprecated
    public static Builder builder(RoomUpdateListener roomUpdateListener) {
        return new Builder(roomUpdateListener);
    }

    public static Bundle createAutoMatchCriteria(int i, int i2, long j) {
        Bundle bundle = new Bundle();
        bundle.putInt(Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, i);
        bundle.putInt(Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, i2);
        bundle.putLong(Multiplayer.EXTRA_EXCLUSIVE_BIT_MASK, j);
        return bundle;
    }

    public abstract Bundle getAutoMatchCriteria();

    public abstract String getInvitationId();

    public abstract String[] getInvitedPlayerIds();

    @Deprecated
    public abstract RealTimeMessageReceivedListener getMessageReceivedListener();

    public abstract OnRealTimeMessageReceivedListener getOnMessageReceivedListener();

    public abstract RoomStatusUpdateCallback getRoomStatusUpdateCallback();

    @Deprecated
    public abstract RoomStatusUpdateListener getRoomStatusUpdateListener();

    public abstract RoomUpdateCallback getRoomUpdateCallback();

    @Deprecated
    public abstract RoomUpdateListener getRoomUpdateListener();

    public abstract int getVariant();

    public abstract zzh zzatw();
}
