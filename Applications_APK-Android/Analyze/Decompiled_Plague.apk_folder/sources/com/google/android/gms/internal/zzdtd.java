package com.google.android.gms.internal;

import java.security.SecureRandom;

public final class zzdtd {
    private static final SecureRandom zzlvy = new SecureRandom();

    public static byte[] zzgb(int i) {
        byte[] bArr = new byte[i];
        zzlvy.nextBytes(bArr);
        return bArr;
    }
}
