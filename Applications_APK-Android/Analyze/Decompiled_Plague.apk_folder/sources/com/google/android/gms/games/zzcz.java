package com.google.android.gms.games;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.video.Videos;

final class zzcz implements zzbo<Videos.CaptureAvailableResult, Boolean> {
    zzcz() {
    }

    public final /* synthetic */ Object zzb(Result result) {
        Videos.CaptureAvailableResult captureAvailableResult = (Videos.CaptureAvailableResult) result;
        return Boolean.valueOf(captureAvailableResult == null ? false : captureAvailableResult.isAvailable());
    }
}
