package com.google.android.gms.internal;

final class zzfek implements zzfen {
    int zzpca = 0;

    zzfek() {
    }

    public final double zza(boolean z, double d, boolean z2, double d2) {
        this.zzpca = (53 * this.zzpca) + zzfer.zzdd(Double.doubleToLongBits(d));
        return d;
    }

    public final int zza(boolean z, int i, boolean z2, int i2) {
        this.zzpca = (53 * this.zzpca) + i;
        return i;
    }

    public final long zza(boolean z, long j, boolean z2, long j2) {
        this.zzpca = (53 * this.zzpca) + zzfer.zzdd(j);
        return j;
    }

    public final zzfdh zza(boolean z, zzfdh zzfdh, boolean z2, zzfdh zzfdh2) {
        this.zzpca = (53 * this.zzpca) + zzfdh.hashCode();
        return zzfdh;
    }

    public final zzfeu zza(zzfeu zzfeu, zzfeu zzfeu2) {
        this.zzpca = (53 * this.zzpca) + zzfeu.hashCode();
        return zzfeu;
    }

    public final <T> zzfev<T> zza(zzfev zzfev, zzfev zzfev2) {
        this.zzpca = (53 * this.zzpca) + zzfev.hashCode();
        return zzfev;
    }

    public final <K, V> zzffh<K, V> zza(zzffh zzffh, zzffh zzffh2) {
        this.zzpca = (53 * this.zzpca) + zzffh.hashCode();
        return zzffh;
    }

    public final <T extends zzffi> T zza(zzffi zzffi, zzffi zzffi2) {
        int i;
        if (zzffi == null) {
            i = 37;
        } else if (zzffi instanceof zzfee) {
            zzfee zzfee = (zzfee) zzffi;
            if (zzfee.zzpaf == 0) {
                int i2 = this.zzpca;
                this.zzpca = 0;
                zzfee.zza(zzfem.zzpcd, this, zzfee);
                zzfee.zzpbs = zza(zzfee.zzpbs, zzfee.zzpbs);
                zzfee.zzpaf = this.zzpca;
                this.zzpca = i2;
            }
            i = zzfee.zzpaf;
        } else {
            i = zzffi.hashCode();
        }
        this.zzpca = (53 * this.zzpca) + i;
        return zzffi;
    }

    public final zzfgi zza(zzfgi zzfgi, zzfgi zzfgi2) {
        this.zzpca = (53 * this.zzpca) + zzfgi.hashCode();
        return zzfgi;
    }

    public final Object zza(boolean z, Object obj, Object obj2) {
        this.zzpca = (53 * this.zzpca) + zzfer.zzdc(((Boolean) obj).booleanValue());
        return obj;
    }

    public final String zza(boolean z, String str, boolean z2, String str2) {
        this.zzpca = (53 * this.zzpca) + str.hashCode();
        return str;
    }

    public final boolean zza(boolean z, boolean z2, boolean z3, boolean z4) {
        this.zzpca = (53 * this.zzpca) + zzfer.zzdc(z2);
        return z2;
    }

    public final Object zzb(boolean z, Object obj, Object obj2) {
        this.zzpca = (53 * this.zzpca) + ((Integer) obj).intValue();
        return obj;
    }

    public final Object zzc(boolean z, Object obj, Object obj2) {
        this.zzpca = (53 * this.zzpca) + zzfer.zzdd(Double.doubleToLongBits(((Double) obj).doubleValue()));
        return obj;
    }

    public final Object zzd(boolean z, Object obj, Object obj2) {
        this.zzpca = (53 * this.zzpca) + zzfer.zzdd(((Long) obj).longValue());
        return obj;
    }

    public final void zzdb(boolean z) {
        if (z) {
            throw new IllegalStateException();
        }
    }

    public final Object zze(boolean z, Object obj, Object obj2) {
        this.zzpca = (53 * this.zzpca) + obj.hashCode();
        return obj;
    }

    public final Object zzf(boolean z, Object obj, Object obj2) {
        this.zzpca = (53 * this.zzpca) + obj.hashCode();
        return obj;
    }

    public final Object zzg(boolean z, Object obj, Object obj2) {
        return zza((zzffi) obj, (zzffi) obj2);
    }
}
