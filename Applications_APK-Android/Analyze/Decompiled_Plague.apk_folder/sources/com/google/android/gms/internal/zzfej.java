package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfej;

public abstract class zzfej<MessageType extends zzfej<MessageType, BuilderType>, BuilderType> extends zzfee<MessageType, BuilderType> implements zzffk {
    protected zzfeb<Object> zzpbz = zzfeb.zzcva();
}
