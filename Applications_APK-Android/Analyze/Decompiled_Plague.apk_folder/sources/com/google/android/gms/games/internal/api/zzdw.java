package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzdw extends zzdx {
    private /* synthetic */ int zzhrc;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdw(zzdt zzdt, GoogleApiClient googleApiClient, int i) {
        super(googleApiClient, null);
        this.zzhrc = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zzb(this, this.zzhrc);
    }
}
