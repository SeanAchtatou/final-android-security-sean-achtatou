package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzba;
import java.util.concurrent.Future;

@zzzb
public final class zzxn extends zzafh {
    private final Object mLock;
    /* access modifiers changed from: private */
    public final zzxg zzchu;
    private final zzaev zzchv;
    private final zzaad zzchw;
    private final zzxr zzcil;
    private Future<zzaeu> zzcim;

    public zzxn(Context context, zzba zzba, zzaev zzaev, zzcs zzcs, zzxg zzxg, zznd zznd) {
        this(zzaev, zzxg, new zzxr(context, zzba, new zzahy(context), zzcs, zzaev, zznd));
    }

    private zzxn(zzaev zzaev, zzxg zzxg, zzxr zzxr) {
        this.mLock = new Object();
        this.zzchv = zzaev;
        this.zzchw = zzaev.zzcwe;
        this.zzchu = zzxg;
        this.zzcil = zzxr;
    }

    public final void onStop() {
        synchronized (this.mLock) {
            if (this.zzcim != null) {
                this.zzcim.cancel(true);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0039  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzdg() {
        /*
            r53 = this;
            r1 = r53
            r2 = 0
            r3 = 0
            java.lang.Object r4 = r1.mLock     // Catch:{ TimeoutException -> 0x0027, InterruptedException | CancellationException | ExecutionException -> 0x0033 }
            monitor-enter(r4)     // Catch:{ TimeoutException -> 0x0027, InterruptedException | CancellationException | ExecutionException -> 0x0033 }
            com.google.android.gms.internal.zzxr r5 = r1.zzcil     // Catch:{ all -> 0x0023 }
            java.util.concurrent.ThreadPoolExecutor r6 = com.google.android.gms.internal.zzagl.zzcyx     // Catch:{ all -> 0x0023 }
            com.google.android.gms.internal.zzajp r5 = com.google.android.gms.internal.zzagl.zza(r6, r5)     // Catch:{ all -> 0x0023 }
            r1.zzcim = r5     // Catch:{ all -> 0x0023 }
            monitor-exit(r4)     // Catch:{ all -> 0x0023 }
            java.util.concurrent.Future<com.google.android.gms.internal.zzaeu> r4 = r1.zzcim     // Catch:{ TimeoutException -> 0x0027, InterruptedException | CancellationException | ExecutionException -> 0x0033 }
            r5 = 60000(0xea60, double:2.9644E-319)
            java.util.concurrent.TimeUnit r7 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ TimeoutException -> 0x0027, InterruptedException | CancellationException | ExecutionException -> 0x0033 }
            java.lang.Object r4 = r4.get(r5, r7)     // Catch:{ TimeoutException -> 0x0027, InterruptedException | CancellationException | ExecutionException -> 0x0033 }
            com.google.android.gms.internal.zzaeu r4 = (com.google.android.gms.internal.zzaeu) r4     // Catch:{ TimeoutException -> 0x0027, InterruptedException | CancellationException | ExecutionException -> 0x0033 }
            r2 = -2
            r8 = r2
            r3 = r4
            goto L_0x0034
        L_0x0023:
            r0 = move-exception
            r5 = r0
            monitor-exit(r4)     // Catch:{ all -> 0x0023 }
            throw r5     // Catch:{ TimeoutException -> 0x0027, InterruptedException | CancellationException | ExecutionException -> 0x0033 }
        L_0x0027:
            java.lang.String r2 = "Timed out waiting for native ad."
            com.google.android.gms.internal.zzafj.zzco(r2)
            r2 = 2
            java.util.concurrent.Future<com.google.android.gms.internal.zzaeu> r4 = r1.zzcim
            r5 = 1
            r4.cancel(r5)
        L_0x0033:
            r8 = r2
        L_0x0034:
            if (r3 == 0) goto L_0x0039
            r2 = r3
            goto L_0x00cc
        L_0x0039:
            com.google.android.gms.internal.zzaeu r2 = new com.google.android.gms.internal.zzaeu
            com.google.android.gms.internal.zzaev r3 = r1.zzchv
            com.google.android.gms.internal.zzzz r3 = r3.zzcpe
            com.google.android.gms.internal.zzis r5 = r3.zzclo
            com.google.android.gms.internal.zzaad r3 = r1.zzchw
            int r11 = r3.orientation
            com.google.android.gms.internal.zzaad r3 = r1.zzchw
            long r12 = r3.zzccb
            com.google.android.gms.internal.zzaev r3 = r1.zzchv
            com.google.android.gms.internal.zzzz r3 = r3.zzcpe
            java.lang.String r14 = r3.zzclr
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            com.google.android.gms.internal.zzaad r3 = r1.zzchw
            long r3 = r3.zzcnh
            com.google.android.gms.internal.zzaev r15 = r1.zzchv
            com.google.android.gms.internal.zziw r15 = r15.zzath
            com.google.android.gms.internal.zzaad r10 = r1.zzchw
            r44 = r11
            long r10 = r10.zzcnf
            com.google.android.gms.internal.zzaev r9 = r1.zzchv
            r45 = r10
            long r10 = r9.zzcvw
            com.google.android.gms.internal.zzaad r9 = r1.zzchw
            r47 = r10
            long r10 = r9.zzcnk
            com.google.android.gms.internal.zzaad r9 = r1.zzchw
            java.lang.String r9 = r9.zzcnl
            com.google.android.gms.internal.zzaev r7 = r1.zzchv
            org.json.JSONObject r7 = r7.zzcvq
            r32 = 0
            r33 = 0
            r34 = 0
            r35 = 0
            com.google.android.gms.internal.zzaev r6 = r1.zzchv
            com.google.android.gms.internal.zzaad r6 = r6.zzcwe
            boolean r6 = r6.zzcny
            r49 = r3
            com.google.android.gms.internal.zzaev r3 = r1.zzchv
            com.google.android.gms.internal.zzaad r3 = r3.zzcwe
            com.google.android.gms.internal.zzaaf r3 = r3.zzcnz
            r38 = 0
            r39 = 0
            com.google.android.gms.internal.zzaad r4 = r1.zzchw
            java.lang.String r4 = r4.zzcoc
            r51 = r4
            com.google.android.gms.internal.zzaev r4 = r1.zzchv
            com.google.android.gms.internal.zzib r4 = r4.zzcwc
            r52 = r4
            com.google.android.gms.internal.zzaev r4 = r1.zzchv
            com.google.android.gms.internal.zzaad r4 = r4.zzcwe
            boolean r4 = r4.zzapy
            r43 = 0
            r42 = r4
            r21 = r49
            r40 = r51
            r41 = r52
            r4 = r2
            r36 = r6
            r6 = 0
            r31 = r7
            r7 = 0
            r30 = r9
            r9 = 0
            r28 = r10
            r24 = r45
            r26 = r47
            r10 = 0
            r11 = r44
            r23 = r15
            r15 = 0
            r37 = r3
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r14, r15, r16, r17, r18, r19, r20, r21, r23, r24, r26, r28, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41, r42, r43)
        L_0x00cc:
            android.os.Handler r3 = com.google.android.gms.internal.zzagr.zzczc
            com.google.android.gms.internal.zzxo r4 = new com.google.android.gms.internal.zzxo
            r4.<init>(r1, r2)
            r3.post(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzxn.zzdg():void");
    }
}
