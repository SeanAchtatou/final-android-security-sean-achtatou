package com.google.android.gms.games;

import com.google.android.gms.common.api.internal.zzdg;
import com.google.android.gms.games.internal.zza;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbg extends zza {
    private /* synthetic */ TaskCompletionSource zzels;

    zzbg(zzbf zzbf, TaskCompletionSource taskCompletionSource) {
        this.zzels = taskCompletionSource;
    }

    public final void onLeftRoom(int i, String str) {
        zzdg.zza(GamesClientStatusCodes.zzdg(GamesClientStatusCodes.zzdh(i)), str, this.zzels);
    }
}
