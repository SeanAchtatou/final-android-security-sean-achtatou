package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.zzq;
import com.google.android.gms.games.video.Videos;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzcx extends zzq<Videos.CaptureOverlayStateListener> {
    private /* synthetic */ zzcl zzhik;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcx(VideosClient videosClient, zzcl zzcl, zzcl zzcl2) {
        super(zzcl);
        this.zzhik = zzcl2;
    }

    /* access modifiers changed from: protected */
    public final void zzb(GamesClientImpl gamesClientImpl, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException, SecurityException {
        gamesClientImpl.zzk(this.zzhik);
        taskCompletionSource.setResult(null);
    }
}
