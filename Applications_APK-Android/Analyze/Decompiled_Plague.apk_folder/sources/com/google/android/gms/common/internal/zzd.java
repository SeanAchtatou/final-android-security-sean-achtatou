package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.zzc;
import com.google.android.gms.common.zze;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class zzd<T extends IInterface> {
    private static String[] zzfwe = {"service_esmobile", "service_googleme"};
    private final Context mContext;
    final Handler mHandler;
    private final Object mLock;
    private final Looper zzakm;
    private final zze zzfni;
    private int zzfvj;
    private long zzfvk;
    private long zzfvl;
    private int zzfvm;
    private long zzfvn;
    private zzam zzfvo;
    private final zzag zzfvp;
    /* access modifiers changed from: private */
    public final Object zzfvq;
    /* access modifiers changed from: private */
    public zzay zzfvr;
    protected zzj zzfvs;
    private T zzfvt;
    /* access modifiers changed from: private */
    public final ArrayList<zzi<?>> zzfvu;
    private zzl zzfvv;
    private int zzfvw;
    /* access modifiers changed from: private */
    public final zzf zzfvx;
    /* access modifiers changed from: private */
    public final zzg zzfvy;
    private final int zzfvz;
    private final String zzfwa;
    /* access modifiers changed from: private */
    public ConnectionResult zzfwb;
    /* access modifiers changed from: private */
    public boolean zzfwc;
    protected AtomicInteger zzfwd;

    protected zzd(Context context, Looper looper, int i, zzf zzf, zzg zzg, String str) {
        this(context, looper, zzag.zzcl(context), zze.zzafm(), i, (zzf) zzbq.checkNotNull(zzf), (zzg) zzbq.checkNotNull(zzg), null);
    }

    protected zzd(Context context, Looper looper, zzag zzag, zze zze, int i, zzf zzf, zzg zzg, String str) {
        this.mLock = new Object();
        this.zzfvq = new Object();
        this.zzfvu = new ArrayList<>();
        this.zzfvw = 1;
        this.zzfwb = null;
        this.zzfwc = false;
        this.zzfwd = new AtomicInteger(0);
        this.mContext = (Context) zzbq.checkNotNull(context, "Context must not be null");
        this.zzakm = (Looper) zzbq.checkNotNull(looper, "Looper must not be null");
        this.zzfvp = (zzag) zzbq.checkNotNull(zzag, "Supervisor must not be null");
        this.zzfni = (zze) zzbq.checkNotNull(zze, "API availability must not be null");
        this.mHandler = new zzh(this, looper);
        this.zzfvz = i;
        this.zzfvx = zzf;
        this.zzfvy = zzg;
        this.zzfwa = str;
    }

    /* access modifiers changed from: private */
    public final void zza(int i, T t) {
        boolean z = true;
        if ((i == 4) != (t != null)) {
            z = false;
        }
        zzbq.checkArgument(z);
        synchronized (this.mLock) {
            this.zzfvw = i;
            this.zzfvt = t;
            switch (i) {
                case 1:
                    if (this.zzfvv != null) {
                        this.zzfvp.zza(zzhf(), zzajv(), 129, this.zzfvv, zzajw());
                        this.zzfvv = null;
                        break;
                    }
                    break;
                case 2:
                case 3:
                    if (!(this.zzfvv == null || this.zzfvo == null)) {
                        String zzalc = this.zzfvo.zzalc();
                        String packageName = this.zzfvo.getPackageName();
                        StringBuilder sb = new StringBuilder(70 + String.valueOf(zzalc).length() + String.valueOf(packageName).length());
                        sb.append("Calling connect() while still connected, missing disconnect() for ");
                        sb.append(zzalc);
                        sb.append(" on ");
                        sb.append(packageName);
                        Log.e("GmsClient", sb.toString());
                        this.zzfvp.zza(this.zzfvo.zzalc(), this.zzfvo.getPackageName(), this.zzfvo.zzaky(), this.zzfvv, zzajw());
                        this.zzfwd.incrementAndGet();
                    }
                    this.zzfvv = new zzl(this, this.zzfwd.get());
                    this.zzfvo = new zzam(zzajv(), zzhf(), false, 129);
                    if (!this.zzfvp.zza(new zzah(this.zzfvo.zzalc(), this.zzfvo.getPackageName(), this.zzfvo.zzaky()), this.zzfvv, zzajw())) {
                        String zzalc2 = this.zzfvo.zzalc();
                        String packageName2 = this.zzfvo.getPackageName();
                        StringBuilder sb2 = new StringBuilder(34 + String.valueOf(zzalc2).length() + String.valueOf(packageName2).length());
                        sb2.append("unable to connect to service: ");
                        sb2.append(zzalc2);
                        sb2.append(" on ");
                        sb2.append(packageName2);
                        Log.e("GmsClient", sb2.toString());
                        zza(16, (Bundle) null, this.zzfwd.get());
                        break;
                    }
                    break;
                case 4:
                    zza((IInterface) t);
                    break;
            }
        }
    }

    /* access modifiers changed from: private */
    public final boolean zza(int i, int i2, T t) {
        synchronized (this.mLock) {
            if (this.zzfvw != i) {
                return false;
            }
            zza(i2, t);
            return true;
        }
    }

    @Nullable
    private final String zzajw() {
        return this.zzfwa == null ? this.mContext.getClass().getName() : this.zzfwa;
    }

    private final boolean zzajy() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzfvw == 3;
        }
        return z;
    }

    /* access modifiers changed from: private */
    public final boolean zzake() {
        if (this.zzfwc || TextUtils.isEmpty(zzhg()) || TextUtils.isEmpty(null)) {
            return false;
        }
        try {
            Class.forName(zzhg());
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public final void zzcf(int i) {
        int i2;
        if (zzajy()) {
            i2 = 5;
            this.zzfwc = true;
        } else {
            i2 = 4;
        }
        this.mHandler.sendMessage(this.mHandler.obtainMessage(i2, this.zzfwd.get(), 16));
    }

    public void disconnect() {
        this.zzfwd.incrementAndGet();
        synchronized (this.zzfvu) {
            int size = this.zzfvu.size();
            for (int i = 0; i < size; i++) {
                this.zzfvu.get(i).removeListener();
            }
            this.zzfvu.clear();
        }
        synchronized (this.zzfvq) {
            this.zzfvr = null;
        }
        zza(1, (IInterface) null);
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int i;
        T t;
        zzay zzay;
        String str2;
        String str3;
        synchronized (this.mLock) {
            i = this.zzfvw;
            t = this.zzfvt;
        }
        synchronized (this.zzfvq) {
            zzay = this.zzfvr;
        }
        printWriter.append((CharSequence) str).append((CharSequence) "mConnectState=");
        switch (i) {
            case 1:
                str2 = "DISCONNECTED";
                break;
            case 2:
                str2 = "REMOTE_CONNECTING";
                break;
            case 3:
                str2 = "LOCAL_CONNECTING";
                break;
            case 4:
                str2 = "CONNECTED";
                break;
            case 5:
                str2 = "DISCONNECTING";
                break;
            default:
                str2 = "UNKNOWN";
                break;
        }
        printWriter.print(str2);
        printWriter.append((CharSequence) " mService=");
        if (t == null) {
            printWriter.append((CharSequence) "null");
        } else {
            printWriter.append((CharSequence) zzhg()).append((CharSequence) "@").append((CharSequence) Integer.toHexString(System.identityHashCode(t.asBinder())));
        }
        printWriter.append((CharSequence) " mServiceBroker=");
        if (zzay == null) {
            printWriter.println("null");
        } else {
            printWriter.append((CharSequence) "IGmsServiceBroker@").println(Integer.toHexString(System.identityHashCode(zzay.asBinder())));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        if (this.zzfvl > 0) {
            PrintWriter append = printWriter.append((CharSequence) str).append((CharSequence) "lastConnectedTime=");
            long j = this.zzfvl;
            String format = simpleDateFormat.format(new Date(this.zzfvl));
            StringBuilder sb = new StringBuilder(String.valueOf(format).length() + 21);
            sb.append(j);
            sb.append(" ");
            sb.append(format);
            append.println(sb.toString());
        }
        if (this.zzfvk > 0) {
            printWriter.append((CharSequence) str).append((CharSequence) "lastSuspendedCause=");
            switch (this.zzfvj) {
                case 1:
                    str3 = "CAUSE_SERVICE_DISCONNECTED";
                    break;
                case 2:
                    str3 = "CAUSE_NETWORK_LOST";
                    break;
                default:
                    str3 = String.valueOf(this.zzfvj);
                    break;
            }
            printWriter.append((CharSequence) str3);
            PrintWriter append2 = printWriter.append((CharSequence) " lastSuspendedTime=");
            long j2 = this.zzfvk;
            String format2 = simpleDateFormat.format(new Date(this.zzfvk));
            StringBuilder sb2 = new StringBuilder(String.valueOf(format2).length() + 21);
            sb2.append(j2);
            sb2.append(" ");
            sb2.append(format2);
            append2.println(sb2.toString());
        }
        if (this.zzfvn > 0) {
            printWriter.append((CharSequence) str).append((CharSequence) "lastFailedStatus=").append((CharSequence) CommonStatusCodes.getStatusCodeString(this.zzfvm));
            PrintWriter append3 = printWriter.append((CharSequence) " lastFailedTime=");
            long j3 = this.zzfvn;
            String format3 = simpleDateFormat.format(new Date(this.zzfvn));
            StringBuilder sb3 = new StringBuilder(21 + String.valueOf(format3).length());
            sb3.append(j3);
            sb3.append(" ");
            sb3.append(format3);
            append3.println(sb3.toString());
        }
    }

    public Account getAccount() {
        return null;
    }

    public final Context getContext() {
        return this.mContext;
    }

    public final Looper getLooper() {
        return this.zzakm;
    }

    public Intent getSignInIntent() {
        throw new UnsupportedOperationException("Not a sign in API");
    }

    public final boolean isConnected() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzfvw == 4;
        }
        return z;
    }

    public final boolean isConnecting() {
        boolean z;
        synchronized (this.mLock) {
            if (this.zzfvw != 2) {
                if (this.zzfvw != 3) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void onConnectionFailed(ConnectionResult connectionResult) {
        this.zzfvm = connectionResult.getErrorCode();
        this.zzfvn = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void onConnectionSuspended(int i) {
        this.zzfvj = i;
        this.zzfvk = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    public final void zza(int i, @Nullable Bundle bundle, int i2) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(7, i2, -1, new zzo(this, i, null)));
    }

    /* access modifiers changed from: protected */
    public void zza(int i, IBinder iBinder, Bundle bundle, int i2) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(1, i2, -1, new zzn(this, i, iBinder, bundle)));
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void zza(@NonNull IInterface iInterface) {
        this.zzfvl = System.currentTimeMillis();
    }

    @WorkerThread
    public final void zza(zzan zzan, Set<Scope> set) {
        Bundle zzaae = zzaae();
        zzz zzz = new zzz(this.zzfvz);
        zzz.zzfwz = this.mContext.getPackageName();
        zzz.zzfxc = zzaae;
        if (set != null) {
            zzz.zzfxb = (Scope[]) set.toArray(new Scope[set.size()]);
        }
        if (zzaam()) {
            zzz.zzfxd = getAccount() != null ? getAccount() : new Account("<<default account>>", "com.google");
            if (zzan != null) {
                zzz.zzfxa = zzan.asBinder();
            }
        } else if (zzakc()) {
            zzz.zzfxd = getAccount();
        }
        zzz.zzfxe = zzajz();
        try {
            synchronized (this.zzfvq) {
                if (this.zzfvr != null) {
                    this.zzfvr.zza(new zzk(this, this.zzfwd.get()), zzz);
                } else {
                    Log.w("GmsClient", "mServiceBroker is null, client disconnected");
                }
            }
        } catch (DeadObjectException e) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e);
            zzce(1);
        } catch (SecurityException e2) {
            throw e2;
        } catch (RemoteException | RuntimeException e3) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e3);
            zza(8, (IBinder) null, (Bundle) null, this.zzfwd.get());
        }
    }

    public void zza(@NonNull zzj zzj) {
        this.zzfvs = (zzj) zzbq.checkNotNull(zzj, "Connection progress callbacks cannot be null.");
        zza(2, (IInterface) null);
    }

    /* access modifiers changed from: protected */
    public final void zza(@NonNull zzj zzj, int i, @Nullable PendingIntent pendingIntent) {
        this.zzfvs = (zzj) zzbq.checkNotNull(zzj, "Connection progress callbacks cannot be null.");
        this.mHandler.sendMessage(this.mHandler.obtainMessage(3, this.zzfwd.get(), i, pendingIntent));
    }

    public void zza(@NonNull zzp zzp) {
        zzp.zzait();
    }

    /* access modifiers changed from: protected */
    public Bundle zzaae() {
        return new Bundle();
    }

    public boolean zzaam() {
        return false;
    }

    public boolean zzaaw() {
        return false;
    }

    public Bundle zzaew() {
        return null;
    }

    public boolean zzafu() {
        return true;
    }

    @Nullable
    public final IBinder zzafv() {
        synchronized (this.zzfvq) {
            if (this.zzfvr == null) {
                return null;
            }
            IBinder asBinder = this.zzfvr.asBinder();
            return asBinder;
        }
    }

    /* access modifiers changed from: protected */
    public String zzajv() {
        return "com.google.android.gms";
    }

    public final void zzajx() {
        int isGooglePlayServicesAvailable = this.zzfni.isGooglePlayServicesAvailable(this.mContext);
        if (isGooglePlayServicesAvailable != 0) {
            zza(1, (IInterface) null);
            zza(new zzm(this), isGooglePlayServicesAvailable, (PendingIntent) null);
            return;
        }
        zza(new zzm(this));
    }

    public zzc[] zzajz() {
        return new zzc[0];
    }

    /* access modifiers changed from: protected */
    public final void zzaka() {
        if (!isConnected()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final T zzakb() throws DeadObjectException {
        T t;
        synchronized (this.mLock) {
            if (this.zzfvw == 5) {
                throw new DeadObjectException();
            }
            zzaka();
            zzbq.zza(this.zzfvt != null, (Object) "Client is connected but service is null");
            t = this.zzfvt;
        }
        return t;
    }

    public boolean zzakc() {
        return false;
    }

    /* access modifiers changed from: protected */
    public Set<Scope> zzakd() {
        return Collections.EMPTY_SET;
    }

    public final void zzce(int i) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(6, this.zzfwd.get(), i));
    }

    /* access modifiers changed from: protected */
    @Nullable
    public abstract T zzd(IBinder iBinder);

    /* access modifiers changed from: protected */
    @NonNull
    public abstract String zzhf();

    /* access modifiers changed from: protected */
    @NonNull
    public abstract String zzhg();
}
