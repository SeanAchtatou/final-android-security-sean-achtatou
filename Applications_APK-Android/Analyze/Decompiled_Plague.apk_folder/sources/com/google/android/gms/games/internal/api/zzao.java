package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzao extends zzav {
    private /* synthetic */ String zzhis;
    private /* synthetic */ long zzhiv;
    private /* synthetic */ String zzhiw;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzao(zzah zzah, GoogleApiClient googleApiClient, String str, long j, String str2) {
        super(googleApiClient);
        this.zzhis = str;
        this.zzhiv = j;
        this.zzhiw = str2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhis, this.zzhiv, this.zzhiw);
    }
}
