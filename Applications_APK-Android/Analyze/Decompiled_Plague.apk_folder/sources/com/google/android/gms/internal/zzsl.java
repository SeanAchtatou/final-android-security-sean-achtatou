package com.google.android.gms.internal;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.util.Base64;
import com.google.android.gms.ads.internal.zzbs;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@zzzb
public final class zzsl {
    private final Map<zzsm, zzsn> zzbxw = new HashMap();
    private final LinkedList<zzsm> zzbxx = new LinkedList<>();
    @Nullable
    private zzri zzbxy;

    private static void zza(String str, zzsm zzsm) {
        if (zzafj.zzae(2)) {
            zzafj.v(String.format(str, zzsm));
        }
    }

    private static String[] zzaw(String str) {
        try {
            String[] split = str.split("\u0000");
            for (int i = 0; i < split.length; i++) {
                split[i] = new String(Base64.decode(split[i], 0), "UTF-8");
            }
            return split;
        } catch (UnsupportedEncodingException unused) {
            return new String[0];
        }
    }

    private static boolean zzax(String str) {
        try {
            return Pattern.matches((String) zzbs.zzep().zzd(zzmq.zzbkn), str);
        } catch (RuntimeException e) {
            zzbs.zzeg().zza(e, "InterstitialAdPool.isExcludedAdUnit");
            return false;
        }
    }

    private static String zzay(String str) {
        try {
            Matcher matcher = Pattern.compile("([^/]+/[0-9]+).*").matcher(str);
            if (matcher.matches()) {
                return matcher.group(1);
            }
        } catch (RuntimeException unused) {
        }
        return str;
    }

    private static void zzc(Bundle bundle, String str) {
        while (true) {
            String[] split = str.split("/", 2);
            if (split.length != 0) {
                String str2 = split[0];
                if (split.length == 1) {
                    bundle.remove(str2);
                    return;
                }
                bundle = bundle.getBundle(str2);
                if (bundle != null) {
                    str = split[1];
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    static Set<String> zzi(zzis zzis) {
        HashSet hashSet = new HashSet();
        hashSet.addAll(zzis.extras.keySet());
        Bundle bundle = zzis.zzbcf.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
        if (bundle != null) {
            hashSet.addAll(bundle.keySet());
        }
        return hashSet;
    }

    static zzis zzj(zzis zzis) {
        zzis zzl = zzl(zzis);
        Bundle bundle = zzl.zzbcf.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
        if (bundle != null) {
            bundle.putBoolean("_skipMediation", true);
        }
        zzl.extras.putBoolean("_skipMediation", true);
        return zzl;
    }

    private static zzis zzk(zzis zzis) {
        zzis zzl = zzl(zzis);
        for (String str : ((String) zzbs.zzep().zzd(zzmq.zzbkj)).split(",")) {
            zzc(zzl.zzbcf, str);
            if (str.startsWith("com.google.ads.mediation.admob.AdMobAdapter/")) {
                zzc(zzl.extras, str.replaceFirst("com.google.ads.mediation.admob.AdMobAdapter/", ""));
            }
        }
        return zzl;
    }

    private final String zzkq() {
        try {
            StringBuilder sb = new StringBuilder();
            Iterator<zzsm> it = this.zzbxx.iterator();
            while (it.hasNext()) {
                sb.append(Base64.encodeToString(it.next().toString().getBytes("UTF-8"), 0));
                if (it.hasNext()) {
                    sb.append("\u0000");
                }
            }
            return sb.toString();
        } catch (UnsupportedEncodingException unused) {
            return "";
        }
    }

    private static zzis zzl(zzis zzis) {
        Parcel obtain = Parcel.obtain();
        zzis.writeToParcel(obtain, 0);
        obtain.setDataPosition(0);
        zzis createFromParcel = zzis.CREATOR.createFromParcel(obtain);
        obtain.recycle();
        if (((Boolean) zzbs.zzep().zzd(zzmq.zzbjy)).booleanValue()) {
            zzis.zzh(createFromParcel);
        }
        return createFromParcel;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public final zzso zza(zzis zzis, String str) {
        if (zzax(str)) {
            return null;
        }
        int i = new zzabv(this.zzbxy.getApplicationContext()).zzns().zzcsg;
        zzis zzk = zzk(zzis);
        String zzay = zzay(str);
        zzsm zzsm = new zzsm(zzk, zzay, i);
        zzsn zzsn = this.zzbxw.get(zzsm);
        if (zzsn == null) {
            zza("Interstitial pool created at %s.", zzsm);
            zzsn = new zzsn(zzk, zzay, i);
            this.zzbxw.put(zzsm, zzsn);
        }
        this.zzbxx.remove(zzsm);
        this.zzbxx.add(zzsm);
        zzsn.zzku();
        while (true) {
            if (this.zzbxx.size() <= ((Integer) zzbs.zzep().zzd(zzmq.zzbkk)).intValue()) {
                break;
            }
            zzsm remove = this.zzbxx.remove();
            zzsn zzsn2 = this.zzbxw.get(remove);
            zza("Evicting interstitial queue for %s.", remove);
            while (zzsn2.size() > 0) {
                zzso zzm = zzsn2.zzm(null);
                if (zzm.zzbyh) {
                    zzsp.zzkw().zzky();
                }
                zzm.zzbyd.zzde();
            }
            this.zzbxw.remove(remove);
        }
        while (zzsn.size() > 0) {
            zzso zzm2 = zzsn.zzm(zzk);
            if (zzm2.zzbyh) {
                if (zzbs.zzei().currentTimeMillis() - zzm2.zzbyg > 1000 * ((long) ((Integer) zzbs.zzep().zzd(zzmq.zzbkm)).intValue())) {
                    zza("Expired interstitial at %s.", zzsm);
                    zzsp.zzkw().zzkx();
                }
            }
            String str2 = zzm2.zzbye != null ? " (inline) " : " ";
            StringBuilder sb = new StringBuilder(34 + String.valueOf(str2).length());
            sb.append("Pooled interstitial");
            sb.append(str2);
            sb.append("returned at %s.");
            zza(sb.toString(), zzsm);
            return zzm2;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzri zzri) {
        if (this.zzbxy == null) {
            this.zzbxy = zzri.zzko();
            if (this.zzbxy != null) {
                SharedPreferences sharedPreferences = this.zzbxy.getApplicationContext().getSharedPreferences("com.google.android.gms.ads.internal.interstitial.InterstitialAdPool", 0);
                while (this.zzbxx.size() > 0) {
                    zzsm remove = this.zzbxx.remove();
                    zzsn zzsn = this.zzbxw.get(remove);
                    zza("Flushing interstitial queue for %s.", remove);
                    while (zzsn.size() > 0) {
                        zzsn.zzm(null).zzbyd.zzde();
                    }
                    this.zzbxw.remove(remove);
                }
                try {
                    HashMap hashMap = new HashMap();
                    for (Map.Entry next : sharedPreferences.getAll().entrySet()) {
                        if (!((String) next.getKey()).equals("PoolKeys")) {
                            zzsr zzaz = zzsr.zzaz((String) next.getValue());
                            zzsm zzsm = new zzsm(zzaz.zzara, zzaz.zzaou, zzaz.zzbyb);
                            if (!this.zzbxw.containsKey(zzsm)) {
                                this.zzbxw.put(zzsm, new zzsn(zzaz.zzara, zzaz.zzaou, zzaz.zzbyb));
                                hashMap.put(zzsm.toString(), zzsm);
                                zza("Restored interstitial queue for %s.", zzsm);
                            }
                        }
                    }
                    for (String str : zzaw(sharedPreferences.getString("PoolKeys", ""))) {
                        zzsm zzsm2 = (zzsm) hashMap.get(str);
                        if (this.zzbxw.containsKey(zzsm2)) {
                            this.zzbxx.add(zzsm2);
                        }
                    }
                } catch (IOException | RuntimeException e) {
                    zzbs.zzeg().zza(e, "InterstitialAdPool.restore");
                    zzafj.zzc("Malformed preferences value for InterstitialAdPool.", e);
                    this.zzbxw.clear();
                    this.zzbxx.clear();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzb(zzis zzis, String str) {
        if (this.zzbxy != null) {
            int i = new zzabv(this.zzbxy.getApplicationContext()).zzns().zzcsg;
            zzis zzk = zzk(zzis);
            String zzay = zzay(str);
            zzsm zzsm = new zzsm(zzk, zzay, i);
            zzsn zzsn = this.zzbxw.get(zzsm);
            if (zzsn == null) {
                zza("Interstitial pool created at %s.", zzsm);
                zzsn = new zzsn(zzk, zzay, i);
                this.zzbxw.put(zzsm, zzsn);
            }
            zzsn.zza(this.zzbxy, zzis);
            zzsn.zzku();
            zza("Inline entry added to the queue at %s.", zzsm);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzkp() {
        int size;
        int zzks;
        if (this.zzbxy != null) {
            for (Map.Entry next : this.zzbxw.entrySet()) {
                zzsm zzsm = (zzsm) next.getKey();
                zzsn zzsn = (zzsn) next.getValue();
                if (zzafj.zzae(2) && (zzks = zzsn.zzks()) < (size = zzsn.size())) {
                    zzafj.v(String.format("Loading %s/%s pooled interstitials for %s.", Integer.valueOf(size - zzks), Integer.valueOf(size), zzsm));
                }
                int zzkt = 0 + zzsn.zzkt();
                while (true) {
                    if (zzsn.size() >= ((Integer) zzbs.zzep().zzd(zzmq.zzbkl)).intValue()) {
                        break;
                    }
                    zza("Pooling and loading one new interstitial for %s.", zzsm);
                    if (zzsn.zzb(this.zzbxy)) {
                        zzkt++;
                    }
                }
                zzsp.zzkw().zzu(zzkt);
            }
            if (this.zzbxy != null) {
                SharedPreferences.Editor edit = this.zzbxy.getApplicationContext().getSharedPreferences("com.google.android.gms.ads.internal.interstitial.InterstitialAdPool", 0).edit();
                edit.clear();
                for (Map.Entry next2 : this.zzbxw.entrySet()) {
                    zzsm zzsm2 = (zzsm) next2.getKey();
                    zzsn zzsn2 = (zzsn) next2.getValue();
                    if (zzsn2.zzkv()) {
                        edit.putString(zzsm2.toString(), new zzsr(zzsn2).zzlf());
                        zza("Saved interstitial queue for %s.", zzsm2);
                    }
                }
                edit.putString("PoolKeys", zzkq());
                edit.apply();
            }
        }
    }
}
