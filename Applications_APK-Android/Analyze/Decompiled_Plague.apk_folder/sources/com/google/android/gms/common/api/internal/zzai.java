package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

final class zzai implements PendingResult.zza {
    private /* synthetic */ zzs zzfnw;
    private /* synthetic */ zzah zzfnx;

    zzai(zzah zzah, zzs zzs) {
        this.zzfnx = zzah;
        this.zzfnw = zzs;
    }

    public final void zzr(Status status) {
        this.zzfnx.zzfnu.remove(this.zzfnw);
    }
}
