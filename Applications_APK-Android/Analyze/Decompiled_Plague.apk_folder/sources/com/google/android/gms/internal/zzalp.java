package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.zzbs;
import com.mopub.mobileads.VastIconXmlManager;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyAuctionFlags;
import java.util.Map;

@zzzb
public final class zzalp implements zzt<zzali> {
    public final /* synthetic */ void zza(Object obj, Map map) {
        zzali zzali = (zzali) obj;
        if (((Boolean) zzbs.zzep().zzd(zzmq.zzblk)).booleanValue()) {
            zzamr zzrx = zzali.zzrx();
            if (zzrx == null) {
                try {
                    zzamr zzamr = new zzamr(zzali, Float.parseFloat((String) map.get(VastIconXmlManager.DURATION)), TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE.equals(map.get("customControlsAllowed")), TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE.equals(map.get("clickToExpandAllowed")));
                    zzali.zza(zzamr);
                    zzrx = zzamr;
                } catch (NullPointerException | NumberFormatException e) {
                    zzafj.zzb("Unable to parse videoMeta message.", e);
                    zzbs.zzeg().zza(e, "VideoMetaGmsgHandler.onGmsg");
                    return;
                }
            }
            boolean equals = TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE.equals(map.get("muted"));
            float parseFloat = Float.parseFloat((String) map.get(TJAdUnitConstants.String.VIDEO_CURRENT_TIME));
            int parseInt = Integer.parseInt((String) map.get("playbackState"));
            if (parseInt < 0 || 3 < parseInt) {
                parseInt = 0;
            }
            String str = (String) map.get("aspectRatio");
            float parseFloat2 = TextUtils.isEmpty(str) ? 0.0f : Float.parseFloat(str);
            if (zzafj.zzae(3)) {
                StringBuilder sb = new StringBuilder(79 + String.valueOf(str).length());
                sb.append("Video Meta GMSG: isMuted : ");
                sb.append(equals);
                sb.append(" , playbackState : ");
                sb.append(parseInt);
                sb.append(" , aspectRatio : ");
                sb.append(str);
                zzafj.zzbw(sb.toString());
            }
            zzrx.zza(parseFloat, parseInt, equals, parseFloat2);
        }
    }
}
