package com.google.android.gms.games.internal;

import android.os.Binder;
import android.view.View;

public class zzaa {
    protected GamesClientImpl zzhpq;
    protected zzac zzhpr;

    private zzaa(GamesClientImpl gamesClientImpl, int i) {
        this.zzhpq = gamesClientImpl;
        zzdl(i);
    }

    public void zzasz() {
        this.zzhpq.zza(this.zzhpr.zzhps, this.zzhpr.zzata());
    }

    /* access modifiers changed from: protected */
    public void zzdl(int i) {
        this.zzhpr = new zzac(i, new Binder());
    }

    public void zzv(View view) {
    }
}
