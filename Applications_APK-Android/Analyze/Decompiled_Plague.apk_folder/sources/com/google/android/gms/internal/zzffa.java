package com.google.android.gms.internal;

import java.util.Map;

final class zzffa<K> implements Map.Entry<K, Object> {
    private Map.Entry<K, zzfey> zzpcv;

    private zzffa(Map.Entry<K, zzfey> entry) {
        this.zzpcv = entry;
    }

    public final K getKey() {
        return this.zzpcv.getKey();
    }

    public final Object getValue() {
        if (this.zzpcv.getValue() == null) {
            return null;
        }
        return zzfey.zzcwa();
    }

    public final Object setValue(Object obj) {
        if (obj instanceof zzffi) {
            return this.zzpcv.getValue().zzj((zzffi) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
