package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.zzy;

public final class zzbqe extends zzy {
    public static final Parcelable.Creator<zzbqe> CREATOR = new zzbqf();
    final boolean zzgkl;
    final DataHolder zzgoj;

    public zzbqe(DataHolder dataHolder, boolean z) {
        this.zzgoj = dataHolder;
        this.zzgkl = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.data.DataHolder, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* access modifiers changed from: protected */
    public final void zzaj(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, (Parcelable) this.zzgoj, i, false);
        zzbem.zza(parcel, 3, this.zzgkl);
        zzbem.zzai(parcel, zze);
    }
}
