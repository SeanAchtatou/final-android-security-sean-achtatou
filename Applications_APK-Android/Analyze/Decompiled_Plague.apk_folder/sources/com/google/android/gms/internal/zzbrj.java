package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;

public final class zzbrj extends zzbjv {
    private final zzn<Status> zzfzc;

    public zzbrj(zzn<Status> zzn) {
        this.zzfzc = zzn;
    }

    public final void onError(Status status) throws RemoteException {
        this.zzfzc.setResult(status);
    }

    public final void onSuccess() throws RemoteException {
        this.zzfzc.setResult(Status.zzfko);
    }
}
