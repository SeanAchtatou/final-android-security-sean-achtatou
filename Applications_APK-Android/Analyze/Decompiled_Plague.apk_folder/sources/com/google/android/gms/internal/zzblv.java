package com.google.android.gms.internal;

import android.os.ParcelFileDescriptor;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzal;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzn;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.ExecutionOptions;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.zzc;
import com.google.android.gms.drive.zzr;
import com.google.android.gms.drive.zzt;
import java.io.InputStream;
import java.io.OutputStream;

public final class zzblv implements DriveContents {
    /* access modifiers changed from: private */
    public static final zzal zzggp = new zzal("DriveContentsImpl", "");
    private boolean mClosed = false;
    /* access modifiers changed from: private */
    public final zzc zzglf;
    private boolean zzglg = false;
    private boolean zzglh = false;

    public zzblv(zzc zzc) {
        this.zzglf = (zzc) zzbq.checkNotNull(zzc);
    }

    private final PendingResult<Status> zza(GoogleApiClient googleApiClient, MetadataChangeSet metadataChangeSet, zzr zzr) {
        if (zzr == null) {
            zzr = (zzr) new zzt().build();
        }
        if (this.zzglf.getMode() == 268435456) {
            throw new IllegalStateException("Cannot commit contents opened with MODE_READ_ONLY");
        } else if (!ExecutionOptions.zzcr(zzr.zzanu()) || this.zzglf.zzanh()) {
            zzr.zzf(googleApiClient);
            if (this.mClosed) {
                throw new IllegalStateException("DriveContents already closed.");
            } else if (getDriveId() == null) {
                throw new IllegalStateException("Only DriveContents obtained through DriveFile.open can be committed.");
            } else {
                if (metadataChangeSet == null) {
                    metadataChangeSet = MetadataChangeSet.zzghi;
                }
                zzanq();
                return googleApiClient.zze(new zzblx(this, googleApiClient, metadataChangeSet, zzr));
            }
        } else {
            throw new IllegalStateException("DriveContents must be valid for conflict detection.");
        }
    }

    public final PendingResult<Status> commit(GoogleApiClient googleApiClient, MetadataChangeSet metadataChangeSet) {
        return zza(googleApiClient, metadataChangeSet, null);
    }

    public final PendingResult<Status> commit(GoogleApiClient googleApiClient, MetadataChangeSet metadataChangeSet, ExecutionOptions executionOptions) {
        return zza(googleApiClient, metadataChangeSet, executionOptions == null ? null : zzr.zzb(executionOptions));
    }

    public final void discard(GoogleApiClient googleApiClient) {
        if (this.mClosed) {
            throw new IllegalStateException("DriveContents already closed.");
        }
        zzanq();
        ((zzblz) googleApiClient.zze(new zzblz(this, googleApiClient))).setResultCallback(new zzbly(this));
    }

    public final DriveId getDriveId() {
        return this.zzglf.getDriveId();
    }

    public final InputStream getInputStream() {
        if (this.mClosed) {
            throw new IllegalStateException("Contents have been closed, cannot access the input stream.");
        } else if (this.zzglf.getMode() != 268435456) {
            throw new IllegalStateException("getInputStream() can only be used with contents opened with MODE_READ_ONLY.");
        } else if (this.zzglg) {
            throw new IllegalStateException("getInputStream() can only be called once per Contents instance.");
        } else {
            this.zzglg = true;
            return this.zzglf.getInputStream();
        }
    }

    public final int getMode() {
        return this.zzglf.getMode();
    }

    public final OutputStream getOutputStream() {
        if (this.mClosed) {
            throw new IllegalStateException("Contents have been closed, cannot access the output stream.");
        } else if (this.zzglf.getMode() != 536870912) {
            throw new IllegalStateException("getOutputStream() can only be used with contents opened with MODE_WRITE_ONLY.");
        } else if (this.zzglh) {
            throw new IllegalStateException("getOutputStream() can only be called once per Contents instance.");
        } else {
            this.zzglh = true;
            return this.zzglf.getOutputStream();
        }
    }

    public final ParcelFileDescriptor getParcelFileDescriptor() {
        if (!this.mClosed) {
            return this.zzglf.getParcelFileDescriptor();
        }
        throw new IllegalStateException("Contents have been closed, cannot access the output stream.");
    }

    public final PendingResult<DriveApi.DriveContentsResult> reopenForWrite(GoogleApiClient googleApiClient) {
        if (this.mClosed) {
            throw new IllegalStateException("DriveContents already closed.");
        } else if (this.zzglf.getMode() != 268435456) {
            throw new IllegalStateException("reopenForWrite can only be used with DriveContents opened with MODE_READ_ONLY.");
        } else {
            zzanq();
            return googleApiClient.zzd(new zzblw(this, googleApiClient));
        }
    }

    public final zzc zzanp() {
        return this.zzglf;
    }

    public final void zzanq() {
        zzn.zza(this.zzglf.getParcelFileDescriptor());
        this.mClosed = true;
    }

    public final boolean zzanr() {
        return this.mClosed;
    }
}
