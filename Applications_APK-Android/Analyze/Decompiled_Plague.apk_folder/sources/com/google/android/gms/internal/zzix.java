package com.google.android.gms.internal;

import android.os.Parcelable;

public final class zzix implements Parcelable.Creator<zziw> {
    /* JADX WARN: Type inference failed for: r1v3, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r15) {
        /*
            r14 = this;
            int r0 = com.google.android.gms.internal.zzbek.zzd(r15)
            r1 = 0
            r2 = 0
            r4 = r1
            r10 = r4
            r5 = r2
            r6 = r5
            r7 = r6
            r8 = r7
            r9 = r8
            r11 = r9
            r12 = r11
            r13 = r12
        L_0x0010:
            int r1 = r15.dataPosition()
            if (r1 >= r0) goto L_0x005c
            int r1 = r15.readInt()
            r2 = 65535(0xffff, float:9.1834E-41)
            r2 = r2 & r1
            switch(r2) {
                case 2: goto L_0x0057;
                case 3: goto L_0x0052;
                case 4: goto L_0x004d;
                case 5: goto L_0x0048;
                case 6: goto L_0x0043;
                case 7: goto L_0x003e;
                case 8: goto L_0x0034;
                case 9: goto L_0x002f;
                case 10: goto L_0x002a;
                case 11: goto L_0x0025;
                default: goto L_0x0021;
            }
        L_0x0021:
            com.google.android.gms.internal.zzbek.zzb(r15, r1)
            goto L_0x0010
        L_0x0025:
            boolean r13 = com.google.android.gms.internal.zzbek.zzc(r15, r1)
            goto L_0x0010
        L_0x002a:
            boolean r12 = com.google.android.gms.internal.zzbek.zzc(r15, r1)
            goto L_0x0010
        L_0x002f:
            boolean r11 = com.google.android.gms.internal.zzbek.zzc(r15, r1)
            goto L_0x0010
        L_0x0034:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zziw> r2 = com.google.android.gms.internal.zziw.CREATOR
            java.lang.Object[] r1 = com.google.android.gms.internal.zzbek.zzb(r15, r1, r2)
            r10 = r1
            com.google.android.gms.internal.zziw[] r10 = (com.google.android.gms.internal.zziw[]) r10
            goto L_0x0010
        L_0x003e:
            int r9 = com.google.android.gms.internal.zzbek.zzg(r15, r1)
            goto L_0x0010
        L_0x0043:
            int r8 = com.google.android.gms.internal.zzbek.zzg(r15, r1)
            goto L_0x0010
        L_0x0048:
            boolean r7 = com.google.android.gms.internal.zzbek.zzc(r15, r1)
            goto L_0x0010
        L_0x004d:
            int r6 = com.google.android.gms.internal.zzbek.zzg(r15, r1)
            goto L_0x0010
        L_0x0052:
            int r5 = com.google.android.gms.internal.zzbek.zzg(r15, r1)
            goto L_0x0010
        L_0x0057:
            java.lang.String r4 = com.google.android.gms.internal.zzbek.zzq(r15, r1)
            goto L_0x0010
        L_0x005c:
            com.google.android.gms.internal.zzbek.zzaf(r15, r0)
            com.google.android.gms.internal.zziw r15 = new com.google.android.gms.internal.zziw
            r3 = r15
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            return r15
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzix.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zziw[i];
    }
}
