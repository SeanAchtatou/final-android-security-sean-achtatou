package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.Context;
import android.graphics.Rect;
import android.os.PowerManager;
import android.os.Process;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.util.zzq;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@zzzb
@TargetApi(14)
public final class zzgt extends Thread {
    private final Object mLock;
    private boolean mStarted = false;
    private final int zzaxi;
    private final int zzaxk;
    private boolean zzayj = false;
    private final zzgo zzayk;
    private final zzyz zzayl;
    private final int zzaym;
    private final int zzayn;
    private final int zzayo;
    private final int zzayp;
    private final int zzayq;
    private final int zzayr;
    private final String zzays;
    private final boolean zzayt;
    private boolean zzbk = false;

    public zzgt(zzgo zzgo, zzyz zzyz) {
        this.zzayk = zzgo;
        this.zzayl = zzyz;
        this.mLock = new Object();
        this.zzaxi = ((Integer) zzbs.zzep().zzd(zzmq.zzbib)).intValue();
        this.zzayn = ((Integer) zzbs.zzep().zzd(zzmq.zzbic)).intValue();
        this.zzaxk = ((Integer) zzbs.zzep().zzd(zzmq.zzbid)).intValue();
        this.zzayo = ((Integer) zzbs.zzep().zzd(zzmq.zzbie)).intValue();
        this.zzayp = ((Integer) zzbs.zzep().zzd(zzmq.zzbih)).intValue();
        this.zzayq = ((Integer) zzbs.zzep().zzd(zzmq.zzbij)).intValue();
        this.zzayr = ((Integer) zzbs.zzep().zzd(zzmq.zzbik)).intValue();
        this.zzaym = ((Integer) zzbs.zzep().zzd(zzmq.zzbif)).intValue();
        this.zzays = (String) zzbs.zzep().zzd(zzmq.zzbim);
        this.zzayt = ((Boolean) zzbs.zzep().zzd(zzmq.zzbio)).booleanValue();
        setName("ContentFetchTask");
    }

    private final zzgx zza(@Nullable View view, zzgn zzgn) {
        boolean z;
        if (view == null) {
            return new zzgx(this, 0, 0);
        }
        Context context = zzbs.zzef().getContext();
        if (context != null) {
            String str = (String) view.getTag(context.getResources().getIdentifier((String) zzbs.zzep().zzd(zzmq.zzbil), "id", context.getPackageName()));
            if (!TextUtils.isEmpty(this.zzays) && str != null && str.equals(this.zzays)) {
                return new zzgx(this, 0, 0);
            }
        }
        boolean globalVisibleRect = view.getGlobalVisibleRect(new Rect());
        if ((view instanceof TextView) && !(view instanceof EditText)) {
            CharSequence text = ((TextView) view).getText();
            if (TextUtils.isEmpty(text)) {
                return new zzgx(this, 0, 0);
            }
            zzgn.zzb(text.toString(), globalVisibleRect, view.getX(), view.getY(), (float) view.getWidth(), (float) view.getHeight());
            return new zzgx(this, 1, 0);
        } else if ((view instanceof WebView) && !(view instanceof zzama)) {
            zzgn.zzgm();
            WebView webView = (WebView) view;
            if (!zzq.zzalz()) {
                z = false;
            } else {
                zzgn.zzgm();
                webView.post(new zzgv(this, zzgn, webView, globalVisibleRect));
                z = true;
            }
            return z ? new zzgx(this, 0, 1) : new zzgx(this, 0, 0);
        } else if (!(view instanceof ViewGroup)) {
            return new zzgx(this, 0, 0);
        } else {
            ViewGroup viewGroup = (ViewGroup) view;
            int i = 0;
            int i2 = 0;
            for (int i3 = 0; i3 < viewGroup.getChildCount(); i3++) {
                zzgx zza = zza(viewGroup.getChildAt(i3), zzgn);
                i += zza.zzazb;
                i2 += zza.zzazc;
            }
            return new zzgx(this, i, i2);
        }
    }

    private static boolean zzgr() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        try {
            Context context = zzbs.zzef().getContext();
            if (context == null) {
                return false;
            }
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService("keyguard");
            if (activityManager == null || keyguardManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null) {
                return false;
            }
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (Process.myPid() == next.pid) {
                    if (next.importance != 100 || keyguardManager.inKeyguardRestrictedInputMode()) {
                        return false;
                    }
                    PowerManager powerManager = (PowerManager) context.getSystemService("power");
                    return powerManager == null ? false : powerManager.isScreenOn();
                }
            }
            return false;
        } catch (Throwable th) {
            zzbs.zzeg().zza(th, "ContentFetchTask.isInForeground");
            return false;
        }
    }

    private final void zzgt() {
        synchronized (this.mLock) {
            this.zzayj = true;
            boolean z = this.zzayj;
            StringBuilder sb = new StringBuilder(42);
            sb.append("ContentFetchThread: paused, mPause = ");
            sb.append(z);
            zzafj.zzbw(sb.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0068, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0069, code lost:
        com.google.android.gms.internal.zzafj.zzb("Error in ContentFetchTask", r0);
        r4.zzayl.zza(r0, "ContentFetchTask.run");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0076, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0077, code lost:
        com.google.android.gms.internal.zzafj.zzb("Error in ContentFetchTask", r0);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:30:0x007f */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0076 A[ExcHandler: InterruptedException (r0v1 'e' java.lang.InterruptedException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007f A[LOOP:1: B:30:0x007f->B:42:0x007f, LOOP_START, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r4 = this;
        L_0x0000:
            boolean r0 = zzgr()     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
            if (r0 == 0) goto L_0x0059
            com.google.android.gms.internal.zzgp r0 = com.google.android.gms.ads.internal.zzbs.zzef()     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
            android.app.Activity r0 = r0.getActivity()     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
            if (r0 != 0) goto L_0x0019
            java.lang.String r0 = "ContentFetchThread: no activity. Sleeping."
            com.google.android.gms.internal.zzafj.zzbw(r0)     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
        L_0x0015:
            r4.zzgt()     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
            goto L_0x005f
        L_0x0019:
            if (r0 == 0) goto L_0x005f
            r1 = 0
            android.view.Window r2 = r0.getWindow()     // Catch:{ Exception -> 0x003d, InterruptedException -> 0x0076 }
            if (r2 == 0) goto L_0x004c
            android.view.Window r2 = r0.getWindow()     // Catch:{ Exception -> 0x003d, InterruptedException -> 0x0076 }
            android.view.View r2 = r2.getDecorView()     // Catch:{ Exception -> 0x003d, InterruptedException -> 0x0076 }
            if (r2 == 0) goto L_0x004c
            android.view.Window r0 = r0.getWindow()     // Catch:{ Exception -> 0x003d, InterruptedException -> 0x0076 }
            android.view.View r0 = r0.getDecorView()     // Catch:{ Exception -> 0x003d, InterruptedException -> 0x0076 }
            r2 = 16908290(0x1020002, float:2.3877235E-38)
            android.view.View r0 = r0.findViewById(r2)     // Catch:{ Exception -> 0x003d, InterruptedException -> 0x0076 }
            r1 = r0
            goto L_0x004c
        L_0x003d:
            r0 = move-exception
            com.google.android.gms.internal.zzaez r2 = com.google.android.gms.ads.internal.zzbs.zzeg()     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
            java.lang.String r3 = "ContentFetchTask.extractContent"
            r2.zza(r0, r3)     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
            java.lang.String r0 = "Failed getting root view of activity. Content not extracted."
            com.google.android.gms.internal.zzafj.zzbw(r0)     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
        L_0x004c:
            if (r1 == 0) goto L_0x005f
            if (r1 == 0) goto L_0x005f
            com.google.android.gms.internal.zzgu r0 = new com.google.android.gms.internal.zzgu     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
            r0.<init>(r4, r1)     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
            r1.post(r0)     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
            goto L_0x005f
        L_0x0059:
            java.lang.String r0 = "ContentFetchTask: sleeping"
            com.google.android.gms.internal.zzafj.zzbw(r0)     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
            goto L_0x0015
        L_0x005f:
            int r0 = r4.zzaym     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
            int r0 = r0 * 1000
            long r0 = (long) r0     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x0076, Exception -> 0x0068 }
            goto L_0x007c
        L_0x0068:
            r0 = move-exception
            java.lang.String r1 = "Error in ContentFetchTask"
            com.google.android.gms.internal.zzafj.zzb(r1, r0)
            com.google.android.gms.internal.zzyz r1 = r4.zzayl
            java.lang.String r2 = "ContentFetchTask.run"
            r1.zza(r0, r2)
            goto L_0x007c
        L_0x0076:
            r0 = move-exception
            java.lang.String r1 = "Error in ContentFetchTask"
            com.google.android.gms.internal.zzafj.zzb(r1, r0)
        L_0x007c:
            java.lang.Object r0 = r4.mLock
            monitor-enter(r0)
        L_0x007f:
            boolean r1 = r4.zzayj     // Catch:{ all -> 0x0091 }
            if (r1 == 0) goto L_0x008e
            java.lang.String r1 = "ContentFetchTask: waiting"
            com.google.android.gms.internal.zzafj.zzbw(r1)     // Catch:{ InterruptedException -> 0x007f }
            java.lang.Object r1 = r4.mLock     // Catch:{ InterruptedException -> 0x007f }
            r1.wait()     // Catch:{ InterruptedException -> 0x007f }
            goto L_0x007f
        L_0x008e:
            monitor-exit(r0)     // Catch:{ all -> 0x0091 }
            goto L_0x0000
        L_0x0091:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0091 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzgt.run():void");
    }

    public final void wakeup() {
        synchronized (this.mLock) {
            this.zzayj = false;
            this.mLock.notifyAll();
            zzafj.zzbw("ContentFetchThread: wakeup");
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzgn zzgn, WebView webView, String str, boolean z) {
        zzgn.zzgl();
        try {
            if (!TextUtils.isEmpty(str)) {
                String optString = new JSONObject(str).optString("text");
                if (this.zzayt || TextUtils.isEmpty(webView.getTitle())) {
                    zzgn.zza(optString, z, webView.getX(), webView.getY(), (float) webView.getWidth(), (float) webView.getHeight());
                } else {
                    String title = webView.getTitle();
                    StringBuilder sb = new StringBuilder(1 + String.valueOf(title).length() + String.valueOf(optString).length());
                    sb.append(title);
                    sb.append("\n");
                    sb.append(optString);
                    zzgn.zza(sb.toString(), z, webView.getX(), webView.getY(), (float) webView.getWidth(), (float) webView.getHeight());
                }
            }
            if (zzgn.zzgg()) {
                this.zzayk.zzb(zzgn);
            }
        } catch (JSONException unused) {
            zzafj.zzbw("Json string may be malformed.");
        } catch (Throwable th) {
            zzafj.zza("Failed to get webview content.", th);
            this.zzayl.zza(th, "ContentFetchTask.processWebViewContent");
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzf(View view) {
        try {
            zzgn zzgn = new zzgn(this.zzaxi, this.zzayn, this.zzaxk, this.zzayo, this.zzayp, this.zzayq, this.zzayr);
            zzgx zza = zza(view, zzgn);
            zzgn.zzgn();
            if (zza.zzazb != 0 || zza.zzazc != 0) {
                if (zza.zzazc != 0 || zzgn.zzgo() != 0) {
                    if (zza.zzazc != 0 || !this.zzayk.zza(zzgn)) {
                        this.zzayk.zzc(zzgn);
                    }
                }
            }
        } catch (Exception e) {
            zzafj.zzb("Exception in fetchContentOnUIThread", e);
            this.zzayl.zza(e, "ContentFetchTask.fetchContent");
        }
    }

    public final void zzgq() {
        synchronized (this.mLock) {
            if (this.mStarted) {
                zzafj.zzbw("Content hash thread already started, quiting...");
                return;
            }
            this.mStarted = true;
            start();
        }
    }

    public final zzgn zzgs() {
        return this.zzayk.zzgp();
    }

    public final boolean zzgu() {
        return this.zzayj;
    }
}
