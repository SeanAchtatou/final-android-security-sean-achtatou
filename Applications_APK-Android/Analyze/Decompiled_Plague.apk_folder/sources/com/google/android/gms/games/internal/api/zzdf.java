package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzdf extends zzdh {
    private /* synthetic */ String zzhlu;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdf(zzcw zzcw, String str, GoogleApiClient googleApiClient, String str2) {
        super(str, googleApiClient);
        this.zzhlu = str2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zzf(this, this.zzhlu);
    }
}
