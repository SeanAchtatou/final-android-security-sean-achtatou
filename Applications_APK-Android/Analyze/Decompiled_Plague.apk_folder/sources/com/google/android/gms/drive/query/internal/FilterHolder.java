package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.drive.query.Filter;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;

public class FilterHolder extends zzbej implements ReflectedParcelable {
    public static final Parcelable.Creator<FilterHolder> CREATOR = new zzh();
    private final Filter zzghm;
    private zzb<?> zzgsi;
    private zzd zzgsj;
    private zzr zzgsk;
    private zzv zzgsl;
    private zzp<?> zzgsm;
    private zzt zzgsn;
    private zzn zzgso;
    private zzl zzgsp;
    private zzz zzgsq;

    public FilterHolder(Filter filter) {
        zzbq.checkNotNull(filter, "Null filter.");
        zzz zzz = null;
        this.zzgsi = filter instanceof zzb ? (zzb) filter : null;
        this.zzgsj = filter instanceof zzd ? (zzd) filter : null;
        this.zzgsk = filter instanceof zzr ? (zzr) filter : null;
        this.zzgsl = filter instanceof zzv ? (zzv) filter : null;
        this.zzgsm = filter instanceof zzp ? (zzp) filter : null;
        this.zzgsn = filter instanceof zzt ? (zzt) filter : null;
        this.zzgso = filter instanceof zzn ? (zzn) filter : null;
        this.zzgsp = filter instanceof zzl ? (zzl) filter : null;
        this.zzgsq = filter instanceof zzz ? (zzz) filter : zzz;
        if (this.zzgsi == null && this.zzgsj == null && this.zzgsk == null && this.zzgsl == null && this.zzgsm == null && this.zzgsn == null && this.zzgso == null && this.zzgsp == null && this.zzgsq == null) {
            throw new IllegalArgumentException("Invalid filter type.");
        }
        this.zzghm = filter;
    }

    FilterHolder(zzb<?> zzb, zzd zzd, zzr zzr, zzv zzv, zzp<?> zzp, zzt zzt, zzn<?> zzn, zzl zzl, zzz zzz) {
        Filter filter;
        this.zzgsi = zzb;
        this.zzgsj = zzd;
        this.zzgsk = zzr;
        this.zzgsl = zzv;
        this.zzgsm = zzp;
        this.zzgsn = zzt;
        this.zzgso = zzn;
        this.zzgsp = zzl;
        this.zzgsq = zzz;
        if (this.zzgsi != null) {
            filter = this.zzgsi;
        } else if (this.zzgsj != null) {
            filter = this.zzgsj;
        } else if (this.zzgsk != null) {
            filter = this.zzgsk;
        } else if (this.zzgsl != null) {
            filter = this.zzgsl;
        } else if (this.zzgsm != null) {
            filter = this.zzgsm;
        } else if (this.zzgsn != null) {
            filter = this.zzgsn;
        } else if (this.zzgso != null) {
            filter = this.zzgso;
        } else if (this.zzgsp != null) {
            filter = this.zzgsp;
        } else if (this.zzgsq != null) {
            filter = this.zzgsq;
        } else {
            throw new IllegalArgumentException("At least one filter must be set.");
        }
        this.zzghm = filter;
    }

    public final Filter getFilter() {
        return this.zzghm;
    }

    public String toString() {
        return String.format("FilterHolder[%s]", this.zzghm);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.zzb<?>, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.zzd, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.zzr, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.zzv, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.zzp<?>, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.zzt, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.zzn, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.zzl, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.zzz, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, (Parcelable) this.zzgsi, i, false);
        zzbem.zza(parcel, 2, (Parcelable) this.zzgsj, i, false);
        zzbem.zza(parcel, 3, (Parcelable) this.zzgsk, i, false);
        zzbem.zza(parcel, 4, (Parcelable) this.zzgsl, i, false);
        zzbem.zza(parcel, 5, (Parcelable) this.zzgsm, i, false);
        zzbem.zza(parcel, 6, (Parcelable) this.zzgsn, i, false);
        zzbem.zza(parcel, 7, (Parcelable) this.zzgso, i, false);
        zzbem.zza(parcel, 8, (Parcelable) this.zzgsp, i, false);
        zzbem.zza(parcel, 9, (Parcelable) this.zzgsq, i, false);
        zzbem.zzai(parcel, zze);
    }
}
