package com.google.android.gms.internal;

import java.security.GeneralSecurityException;

public final class zzdpi {
    public static void zzbli() throws GeneralSecurityException {
        zzdpb.zzbli();
        zzdpp.zzbli();
        zzdph zzdph = new zzdph();
        zzdpa.zzlpq.zza(zzdph.getKeyType(), zzdph);
    }
}
