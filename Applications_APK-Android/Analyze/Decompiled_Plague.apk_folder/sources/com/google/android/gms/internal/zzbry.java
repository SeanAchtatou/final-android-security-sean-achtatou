package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzbry extends zzbrs<Metadata> {
    public zzbry(TaskCompletionSource<Metadata> taskCompletionSource) {
        super(taskCompletionSource);
    }

    public final void zza(zzbqj zzbqj) throws RemoteException {
        zzaox().setResult(new zzbkk(zzbqj.zzaow()));
    }
}
