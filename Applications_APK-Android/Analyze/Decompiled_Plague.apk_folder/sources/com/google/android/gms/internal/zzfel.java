package com.google.android.gms.internal;

public final class zzfel implements zzfen {
    public static final zzfel zzpcb = new zzfel();

    private zzfel() {
    }

    public final double zza(boolean z, double d, boolean z2, double d2) {
        return z2 ? d2 : d;
    }

    public final int zza(boolean z, int i, boolean z2, int i2) {
        return z2 ? i2 : i;
    }

    public final long zza(boolean z, long j, boolean z2, long j2) {
        return z2 ? j2 : j;
    }

    public final zzfdh zza(boolean z, zzfdh zzfdh, boolean z2, zzfdh zzfdh2) {
        return z2 ? zzfdh2 : zzfdh;
    }

    public final zzfeu zza(zzfeu zzfeu, zzfeu zzfeu2) {
        int size = zzfeu.size();
        int size2 = zzfeu2.size();
        if (size > 0 && size2 > 0) {
            if (!zzfeu.zzcth()) {
                zzfeu = zzfeu.zzlj(size2 + size);
            }
            zzfeu.addAll(zzfeu2);
        }
        return size > 0 ? zzfeu : zzfeu2;
    }

    public final <T> zzfev<T> zza(zzfev<T> zzfev, zzfev<T> zzfev2) {
        int size = zzfev.size();
        int size2 = zzfev2.size();
        if (size > 0 && size2 > 0) {
            if (!zzfev.zzcth()) {
                zzfev = zzfev.zzln(size2 + size);
            }
            zzfev.addAll(zzfev2);
        }
        return size > 0 ? zzfev : zzfev2;
    }

    public final <K, V> zzffh<K, V> zza(zzffh<K, V> zzffh, zzffh<K, V> zzffh2) {
        if (!zzffh2.isEmpty()) {
            if (!zzffh.isMutable()) {
                zzffh = zzffh.zzcwd();
            }
            zzffh.zza(zzffh2);
        }
        return zzffh;
    }

    public final <T extends zzffi> T zza(T t, T t2) {
        return (t == null || t2 == null) ? t != null ? t : t2 : t.zzcvg().zzc(t2).zzcvm();
    }

    public final zzfgi zza(zzfgi zzfgi, zzfgi zzfgi2) {
        return zzfgi2 == zzfgi.zzcwu() ? zzfgi : zzfgi.zzb(zzfgi, zzfgi2);
    }

    public final Object zza(boolean z, Object obj, Object obj2) {
        return obj2;
    }

    public final String zza(boolean z, String str, boolean z2, String str2) {
        return z2 ? str2 : str;
    }

    public final boolean zza(boolean z, boolean z2, boolean z3, boolean z4) {
        return z3 ? z4 : z2;
    }

    public final Object zzb(boolean z, Object obj, Object obj2) {
        return obj2;
    }

    public final Object zzc(boolean z, Object obj, Object obj2) {
        return obj2;
    }

    public final Object zzd(boolean z, Object obj, Object obj2) {
        return obj2;
    }

    public final void zzdb(boolean z) {
    }

    public final Object zze(boolean z, Object obj, Object obj2) {
        return obj2;
    }

    public final Object zzf(boolean z, Object obj, Object obj2) {
        return obj2;
    }

    public final Object zzg(boolean z, Object obj, Object obj2) {
        return z ? zza((zzffi) obj, (zzffi) obj2) : obj2;
    }
}
