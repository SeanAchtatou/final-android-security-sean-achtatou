package com.google.android.gms.games;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class PlayerLevel extends zzc {
    public static final Parcelable.Creator<PlayerLevel> CREATOR = new zzaq();
    private final int zzhjp;
    private final long zzhjq;
    private final long zzhjr;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public PlayerLevel(int i, long j, long j2) {
        boolean z = false;
        zzbq.zza(j >= 0, (Object) "Min XP must be positive!");
        zzbq.zza(j2 > j ? true : z, (Object) "Max XP must be more than min XP!");
        this.zzhjp = i;
        this.zzhjq = j;
        this.zzhjr = j2;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof PlayerLevel)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        PlayerLevel playerLevel = (PlayerLevel) obj;
        return zzbg.equal(Integer.valueOf(playerLevel.getLevelNumber()), Integer.valueOf(getLevelNumber())) && zzbg.equal(Long.valueOf(playerLevel.getMinXp()), Long.valueOf(getMinXp())) && zzbg.equal(Long.valueOf(playerLevel.getMaxXp()), Long.valueOf(getMaxXp()));
    }

    public final int getLevelNumber() {
        return this.zzhjp;
    }

    public final long getMaxXp() {
        return this.zzhjr;
    }

    public final long getMinXp() {
        return this.zzhjq;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.zzhjp), Long.valueOf(this.zzhjq), Long.valueOf(this.zzhjr)});
    }

    public final String toString() {
        return zzbg.zzw(this).zzg("LevelNumber", Integer.valueOf(getLevelNumber())).zzg("MinXp", Long.valueOf(getMinXp())).zzg("MaxXp", Long.valueOf(getMaxXp())).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 1, getLevelNumber());
        zzbem.zza(parcel, 2, getMinXp());
        zzbem.zza(parcel, 3, getMaxXp());
        zzbem.zzai(parcel, zze);
    }
}
