package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.drive.DriveSpace;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public final class zze extends zzbej {
    public static final Parcelable.Creator<zze> CREATOR = new zzf();
    private int zzdzm;
    private int zzgii;
    private boolean zzgij;
    private List<DriveSpace> zzgik;
    private final Set<DriveSpace> zzgil;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    zze(int i, int i2, boolean z, List<DriveSpace> list) {
        this(i, i2, z, list, list == null ? null : new HashSet(list));
    }

    private zze(int i, int i2, boolean z, List<DriveSpace> list, @NonNull Set<DriveSpace> set) {
        this.zzdzm = i;
        this.zzgii = i2;
        this.zzgij = z;
        this.zzgik = list;
        this.zzgil = set;
    }

    public final boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        zze zze = (zze) obj;
        return zzbg.equal(this.zzgil, zze.zzgil) && this.zzgii == zze.zzgii && this.zzgij == zze.zzgij;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.zzgil, Integer.valueOf(this.zzgii), Boolean.valueOf(this.zzgij)});
    }

    public final String toString() {
        return String.format(Locale.US, "ChangesAvailableOptions[ChangesSizeLimit=%d, Repeats=%s, Spaces=%s]", Integer.valueOf(this.zzgii), Boolean.valueOf(this.zzgij), this.zzgik);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 1, this.zzdzm);
        zzbem.zzc(parcel, 2, this.zzgii);
        zzbem.zza(parcel, 3, this.zzgij);
        zzbem.zzc(parcel, 4, this.zzgik, false);
        zzbem.zzai(parcel, zze);
    }
}
