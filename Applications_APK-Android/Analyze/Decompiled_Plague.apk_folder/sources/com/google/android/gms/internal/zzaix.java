package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@zzzb
public final class zzaix implements zzaiv {
    @Nullable
    private final String zzcxh;

    public zzaix() {
        this(null);
    }

    public zzaix(@Nullable String str) {
        this.zzcxh = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzais.zza(boolean, java.net.HttpURLConnection, java.lang.String):void
     arg types: [int, java.net.HttpURLConnection, java.lang.String]
     candidates:
      com.google.android.gms.internal.zzais.zza(android.view.ViewGroup, com.google.android.gms.internal.zziw, java.lang.String):void
      com.google.android.gms.internal.zzais.zza(boolean, java.net.HttpURLConnection, java.lang.String):void */
    @WorkerThread
    public final void zzch(String str) {
        StringBuilder sb;
        String message;
        HttpURLConnection httpURLConnection;
        try {
            String valueOf = String.valueOf(str);
            zzaiw.zzbw(valueOf.length() != 0 ? "Pinging URL: ".concat(valueOf) : new String("Pinging URL: "));
            httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            zzjk.zzhx();
            zzais.zza(true, httpURLConnection, this.zzcxh);
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode < 200 || responseCode >= 300) {
                StringBuilder sb2 = new StringBuilder(65 + String.valueOf(str).length());
                sb2.append("Received non-success response code ");
                sb2.append(responseCode);
                sb2.append(" from pinging URL: ");
                sb2.append(str);
                zzaiw.zzco(sb2.toString());
            }
            httpURLConnection.disconnect();
        } catch (IndexOutOfBoundsException e) {
            String message2 = e.getMessage();
            sb = new StringBuilder(32 + String.valueOf(str).length() + String.valueOf(message2).length());
            sb.append("Error while parsing ping URL: ");
            sb.append(str);
            sb.append(". ");
            sb.append(message2);
            zzaiw.zzco(sb.toString());
        } catch (IOException e2) {
            message = e2.getMessage();
            sb = new StringBuilder(27 + String.valueOf(str).length() + String.valueOf(message).length());
            sb.append("Error while pinging URL: ");
            sb.append(str);
            sb.append(". ");
            sb.append(message);
            zzaiw.zzco(sb.toString());
        } catch (RuntimeException e3) {
            message = e3.getMessage();
            sb = new StringBuilder(27 + String.valueOf(str).length() + String.valueOf(message).length());
            sb.append("Error while pinging URL: ");
            sb.append(str);
            sb.append(". ");
            sb.append(message);
            zzaiw.zzco(sb.toString());
        } catch (Throwable th) {
            httpURLConnection.disconnect();
            throw th;
        }
    }
}
