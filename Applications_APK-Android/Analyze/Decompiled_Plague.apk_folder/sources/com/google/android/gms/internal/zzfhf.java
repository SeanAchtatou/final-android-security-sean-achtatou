package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfhe;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public final class zzfhf<M extends zzfhe<M>, T> {
    public final int tag;
    private int type;
    protected final Class<T> zznan;
    private zzfee<?, ?> zzpbu;
    protected final boolean zzpgz;

    private zzfhf(int i, Class<T> cls, int i2, boolean z) {
        this(11, cls, null, i2, false);
    }

    private zzfhf(int i, Class<T> cls, zzfee<?, ?> zzfee, int i2, boolean z) {
        this.type = i;
        this.zznan = cls;
        this.tag = i2;
        this.zzpgz = false;
        this.zzpbu = null;
    }

    public static <M extends zzfhe<M>, T extends zzfhk> zzfhf<M, T> zza(int i, Class<T> cls, long j) {
        return new zzfhf<>(11, cls, (int) j, false);
    }

    private final Object zzan(zzfhb zzfhb) {
        Class componentType = this.zzpgz ? this.zznan.getComponentType() : this.zznan;
        try {
            switch (this.type) {
                case 10:
                    zzfhk zzfhk = (zzfhk) componentType.newInstance();
                    zzfhb.zza(zzfhk, this.tag >>> 3);
                    return zzfhk;
                case 11:
                    zzfhk zzfhk2 = (zzfhk) componentType.newInstance();
                    zzfhb.zza(zzfhk2);
                    return zzfhk2;
                default:
                    int i = this.type;
                    StringBuilder sb = new StringBuilder(24);
                    sb.append("Unknown type ");
                    sb.append(i);
                    throw new IllegalArgumentException(sb.toString());
            }
        } catch (InstantiationException e) {
            String valueOf = String.valueOf(componentType);
            StringBuilder sb2 = new StringBuilder(33 + String.valueOf(valueOf).length());
            sb2.append("Error creating instance of class ");
            sb2.append(valueOf);
            throw new IllegalArgumentException(sb2.toString(), e);
        } catch (IllegalAccessException e2) {
            String valueOf2 = String.valueOf(componentType);
            StringBuilder sb3 = new StringBuilder(33 + String.valueOf(valueOf2).length());
            sb3.append("Error creating instance of class ");
            sb3.append(valueOf2);
            throw new IllegalArgumentException(sb3.toString(), e2);
        } catch (IOException e3) {
            throw new IllegalArgumentException("Error reading extension field", e3);
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzfhf)) {
            return false;
        }
        zzfhf zzfhf = (zzfhf) obj;
        return this.type == zzfhf.type && this.zznan == zzfhf.zznan && this.tag == zzfhf.tag && this.zzpgz == zzfhf.zzpgz;
    }

    public final int hashCode() {
        return ((((((1147 + this.type) * 31) + this.zznan.hashCode()) * 31) + this.tag) * 31) + (this.zzpgz ? 1 : 0);
    }

    /* access modifiers changed from: protected */
    public final void zza(Object obj, zzfhc zzfhc) {
        try {
            zzfhc.zzlx(this.tag);
            switch (this.type) {
                case 10:
                    ((zzfhk) obj).zza(zzfhc);
                    zzfhc.zzz(this.tag >>> 3, 4);
                    return;
                case 11:
                    zzfhc.zzb((zzfhk) obj);
                    return;
                default:
                    int i = this.type;
                    StringBuilder sb = new StringBuilder(24);
                    sb.append("Unknown type ");
                    sb.append(i);
                    throw new IllegalArgumentException(sb.toString());
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public final T zzbp(List<zzfhm> list) {
        if (list == null) {
            return null;
        }
        if (this.zzpgz) {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < list.size(); i++) {
                zzfhm zzfhm = list.get(i);
                if (zzfhm.zzjkl.length != 0) {
                    arrayList.add(zzan(zzfhb.zzbd(zzfhm.zzjkl)));
                }
            }
            int size = arrayList.size();
            if (size == 0) {
                return null;
            }
            T cast = this.zznan.cast(Array.newInstance(this.zznan.getComponentType(), size));
            for (int i2 = 0; i2 < size; i2++) {
                Array.set(cast, i2, arrayList.get(i2));
            }
            return cast;
        } else if (list.isEmpty()) {
            return null;
        } else {
            return this.zznan.cast(zzan(zzfhb.zzbd(list.get(list.size() - 1).zzjkl)));
        }
    }

    /* access modifiers changed from: protected */
    public final int zzcn(Object obj) {
        int i = this.tag >>> 3;
        switch (this.type) {
            case 10:
                return (zzfhc.zzkw(i) << 1) + ((zzfhk) obj).zzhl();
            case 11:
                return zzfhc.zzb(i, (zzfhk) obj);
            default:
                int i2 = this.type;
                StringBuilder sb = new StringBuilder(24);
                sb.append("Unknown type ");
                sb.append(i2);
                throw new IllegalArgumentException(sb.toString());
        }
    }
}
