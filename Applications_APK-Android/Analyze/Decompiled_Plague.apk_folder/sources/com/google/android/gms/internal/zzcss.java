package com.google.android.gms.internal;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class zzcss {
    private static String[] zzhth = {"key", "value"};
    private static final ConcurrentHashMap<Uri, zzcss> zzjtm = new ConcurrentHashMap<>();
    private final Uri uri;
    private final ContentResolver zzjtn;
    private final ContentObserver zzjto;
    /* access modifiers changed from: private */
    public final Object zzjtp = new Object();
    /* access modifiers changed from: private */
    public volatile Map<String, String> zzjtq;

    private zzcss(ContentResolver contentResolver, Uri uri2) {
        this.zzjtn = contentResolver;
        this.uri = uri2;
        this.zzjto = new zzcsu(this, null);
    }

    public static zzcss zza(ContentResolver contentResolver, Uri uri2) {
        zzcss zzcss = zzjtm.get(uri2);
        if (zzcss != null) {
            return zzcss;
        }
        zzcss zzcss2 = new zzcss(contentResolver, uri2);
        zzcss putIfAbsent = zzjtm.putIfAbsent(uri2, zzcss2);
        if (putIfAbsent != null) {
            return putIfAbsent;
        }
        zzcss2.zzjtn.registerContentObserver(zzcss2.uri, false, zzcss2.zzjto);
        return zzcss2;
    }

    private final Map<String, String> zzbcb() {
        HashMap hashMap = new HashMap();
        Cursor query = this.zzjtn.query(this.uri, zzhth, null, null, null);
        if (query == null) {
            return hashMap;
        }
        while (query.moveToNext()) {
            try {
                hashMap.put(query.getString(0), query.getString(1));
            } finally {
                query.close();
            }
        }
        return hashMap;
    }

    public final Map<String, String> zzbca() {
        Map<String, String> map;
        Map<String, String> zzbcb = ((Boolean) zzctg.zza(new zzcst(this))).booleanValue() ? zzbcb() : this.zzjtq;
        if (zzbcb != null) {
            return zzbcb;
        }
        synchronized (this.zzjtp) {
            map = this.zzjtq;
            if (map == null) {
                map = zzbcb();
                this.zzjtq = map;
            }
        }
        return map;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzdld.zza(android.content.ContentResolver, java.lang.String, boolean):boolean
     arg types: [android.content.ContentResolver, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzdld.zza(java.util.HashMap, java.lang.String, java.lang.Object):T
      com.google.android.gms.internal.zzdld.zza(android.content.ContentResolver, java.lang.String, java.lang.String):java.lang.String
      com.google.android.gms.internal.zzdld.zza(java.lang.Object, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.zzdld.zza(android.content.ContentResolver, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public final /* synthetic */ Boolean zzbcc() {
        return Boolean.valueOf(zzdld.zza(this.zzjtn, "gms:phenotype:phenotype_flag:debug_disable_caching", false));
    }
}
