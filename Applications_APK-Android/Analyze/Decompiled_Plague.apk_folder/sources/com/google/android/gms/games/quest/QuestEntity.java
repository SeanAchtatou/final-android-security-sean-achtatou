package com.google.android.gms.games.quest;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.internal.zzbem;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Deprecated
public final class QuestEntity extends zzc implements Quest {
    public static final Parcelable.Creator<QuestEntity> CREATOR = new zzc();
    private final String mName;
    private final int mState;
    private final String zzdou;
    private final int zzefe;
    private final long zzhmi;
    private final GameEntity zzhrl;
    private final String zzhwl;
    private final long zzhwm;
    private final Uri zzhwn;
    private final String zzhwo;
    private final long zzhwp;
    private final Uri zzhwq;
    private final String zzhwr;
    private final long zzhws;
    private final long zzhwt;
    private final ArrayList<MilestoneEntity> zzhwu;

    QuestEntity(GameEntity gameEntity, String str, long j, Uri uri, String str2, String str3, long j2, long j3, Uri uri2, String str4, String str5, long j4, long j5, int i, int i2, ArrayList<MilestoneEntity> arrayList) {
        this.zzhrl = gameEntity;
        this.zzhwl = str;
        this.zzhwm = j;
        this.zzhwn = uri;
        this.zzhwo = str2;
        this.zzdou = str3;
        this.zzhwp = j2;
        this.zzhmi = j3;
        this.zzhwq = uri2;
        this.zzhwr = str4;
        this.mName = str5;
        this.zzhws = j4;
        this.zzhwt = j5;
        this.mState = i;
        this.zzefe = i2;
        this.zzhwu = arrayList;
    }

    public QuestEntity(Quest quest) {
        this.zzhrl = new GameEntity(quest.getGame());
        this.zzhwl = quest.getQuestId();
        this.zzhwm = quest.getAcceptedTimestamp();
        this.zzdou = quest.getDescription();
        this.zzhwn = quest.getBannerImageUri();
        this.zzhwo = quest.getBannerImageUrl();
        this.zzhwp = quest.getEndTimestamp();
        this.zzhwq = quest.getIconImageUri();
        this.zzhwr = quest.getIconImageUrl();
        this.zzhmi = quest.getLastUpdatedTimestamp();
        this.mName = quest.getName();
        this.zzhws = quest.zzatz();
        this.zzhwt = quest.getStartTimestamp();
        this.mState = quest.getState();
        this.zzefe = quest.getType();
        List<Milestone> zzaty = quest.zzaty();
        int size = zzaty.size();
        this.zzhwu = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            this.zzhwu.add((MilestoneEntity) zzaty.get(i).freeze());
        }
    }

    static int zza(Quest quest) {
        return Arrays.hashCode(new Object[]{quest.getGame(), quest.getQuestId(), Long.valueOf(quest.getAcceptedTimestamp()), quest.getBannerImageUri(), quest.getDescription(), Long.valueOf(quest.getEndTimestamp()), quest.getIconImageUri(), Long.valueOf(quest.getLastUpdatedTimestamp()), quest.zzaty(), quest.getName(), Long.valueOf(quest.zzatz()), Long.valueOf(quest.getStartTimestamp()), Integer.valueOf(quest.getState())});
    }

    static boolean zza(Quest quest, Object obj) {
        if (!(obj instanceof Quest)) {
            return false;
        }
        if (quest == obj) {
            return true;
        }
        Quest quest2 = (Quest) obj;
        return zzbg.equal(quest2.getGame(), quest.getGame()) && zzbg.equal(quest2.getQuestId(), quest.getQuestId()) && zzbg.equal(Long.valueOf(quest2.getAcceptedTimestamp()), Long.valueOf(quest.getAcceptedTimestamp())) && zzbg.equal(quest2.getBannerImageUri(), quest.getBannerImageUri()) && zzbg.equal(quest2.getDescription(), quest.getDescription()) && zzbg.equal(Long.valueOf(quest2.getEndTimestamp()), Long.valueOf(quest.getEndTimestamp())) && zzbg.equal(quest2.getIconImageUri(), quest.getIconImageUri()) && zzbg.equal(Long.valueOf(quest2.getLastUpdatedTimestamp()), Long.valueOf(quest.getLastUpdatedTimestamp())) && zzbg.equal(quest2.zzaty(), quest.zzaty()) && zzbg.equal(quest2.getName(), quest.getName()) && zzbg.equal(Long.valueOf(quest2.zzatz()), Long.valueOf(quest.zzatz())) && zzbg.equal(Long.valueOf(quest2.getStartTimestamp()), Long.valueOf(quest.getStartTimestamp())) && zzbg.equal(Integer.valueOf(quest2.getState()), Integer.valueOf(quest.getState()));
    }

    static String zzb(Quest quest) {
        return zzbg.zzw(quest).zzg("Game", quest.getGame()).zzg("QuestId", quest.getQuestId()).zzg("AcceptedTimestamp", Long.valueOf(quest.getAcceptedTimestamp())).zzg("BannerImageUri", quest.getBannerImageUri()).zzg("BannerImageUrl", quest.getBannerImageUrl()).zzg("Description", quest.getDescription()).zzg("EndTimestamp", Long.valueOf(quest.getEndTimestamp())).zzg("IconImageUri", quest.getIconImageUri()).zzg("IconImageUrl", quest.getIconImageUrl()).zzg("LastUpdatedTimestamp", Long.valueOf(quest.getLastUpdatedTimestamp())).zzg("Milestones", quest.zzaty()).zzg("Name", quest.getName()).zzg("NotifyTimestamp", Long.valueOf(quest.zzatz())).zzg("StartTimestamp", Long.valueOf(quest.getStartTimestamp())).zzg("State", Integer.valueOf(quest.getState())).toString();
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    public final Quest freeze() {
        return this;
    }

    public final long getAcceptedTimestamp() {
        return this.zzhwm;
    }

    public final Uri getBannerImageUri() {
        return this.zzhwn;
    }

    public final String getBannerImageUrl() {
        return this.zzhwo;
    }

    public final Milestone getCurrentMilestone() {
        return zzaty().get(0);
    }

    public final String getDescription() {
        return this.zzdou;
    }

    public final void getDescription(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.zzdou, charArrayBuffer);
    }

    public final long getEndTimestamp() {
        return this.zzhwp;
    }

    public final Game getGame() {
        return this.zzhrl;
    }

    public final Uri getIconImageUri() {
        return this.zzhwq;
    }

    public final String getIconImageUrl() {
        return this.zzhwr;
    }

    public final long getLastUpdatedTimestamp() {
        return this.zzhmi;
    }

    public final String getName() {
        return this.mName;
    }

    public final void getName(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.mName, charArrayBuffer);
    }

    public final String getQuestId() {
        return this.zzhwl;
    }

    public final long getStartTimestamp() {
        return this.zzhwt;
    }

    public final int getState() {
        return this.mState;
    }

    public final int getType() {
        return this.zzefe;
    }

    public final int hashCode() {
        return zza(this);
    }

    public final boolean isDataValid() {
        return true;
    }

    public final boolean isEndingSoon() {
        return this.zzhws <= System.currentTimeMillis() + TapjoyConstants.SESSION_ID_INACTIVITY_TIME;
    }

    public final String toString() {
        return zzb(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.Game, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, (Parcelable) getGame(), i, false);
        zzbem.zza(parcel, 2, getQuestId(), false);
        zzbem.zza(parcel, 3, getAcceptedTimestamp());
        zzbem.zza(parcel, 4, (Parcelable) getBannerImageUri(), i, false);
        zzbem.zza(parcel, 5, getBannerImageUrl(), false);
        zzbem.zza(parcel, 6, getDescription(), false);
        zzbem.zza(parcel, 7, getEndTimestamp());
        zzbem.zza(parcel, 8, getLastUpdatedTimestamp());
        zzbem.zza(parcel, 9, (Parcelable) getIconImageUri(), i, false);
        zzbem.zza(parcel, 10, getIconImageUrl(), false);
        zzbem.zza(parcel, 12, getName(), false);
        zzbem.zza(parcel, 13, this.zzhws);
        zzbem.zza(parcel, 14, getStartTimestamp());
        zzbem.zzc(parcel, 15, getState());
        zzbem.zzc(parcel, 16, this.zzefe);
        zzbem.zzc(parcel, 17, zzaty(), false);
        zzbem.zzai(parcel, zze);
    }

    public final List<Milestone> zzaty() {
        return new ArrayList(this.zzhwu);
    }

    public final long zzatz() {
        return this.zzhws;
    }
}
