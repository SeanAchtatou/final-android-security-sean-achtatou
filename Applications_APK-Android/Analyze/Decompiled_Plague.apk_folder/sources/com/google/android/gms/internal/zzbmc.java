package com.google.android.gms.internal;

import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.drive.DriveFile;

final class zzbmc implements DriveFile.DownloadProgressListener {
    private final zzcl<DriveFile.DownloadProgressListener> zzgln;

    public zzbmc(zzcl<DriveFile.DownloadProgressListener> zzcl) {
        this.zzgln = zzcl;
    }

    public final void onProgress(long j, long j2) {
        this.zzgln.zza(new zzbmd(this, j, j2));
    }
}
