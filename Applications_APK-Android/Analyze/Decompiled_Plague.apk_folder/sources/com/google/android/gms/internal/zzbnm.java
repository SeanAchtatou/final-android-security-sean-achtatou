package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzdf;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbnm extends zzdf<zzbll, DriveContents> {
    private /* synthetic */ DriveContents zzgmj;

    zzbnm(zzbmu zzbmu, DriveContents driveContents) {
        this.zzgmj = driveContents;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbqu(this.zzgmj.getDriveId(), DriveFile.MODE_WRITE_ONLY, this.zzgmj.zzanp().getRequestId()), new zzbrz(taskCompletionSource));
    }
}
