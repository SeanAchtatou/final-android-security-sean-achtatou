package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.internal.zzbq;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

@zzzb
public abstract class zzwz implements zzahi<Void>, zzamf {
    protected final Context mContext;
    protected final zzama zzbwq;
    private zzxg zzchu;
    private zzaev zzchv;
    protected zzaad zzchw;
    private Runnable zzchx;
    private Object zzchy = new Object();
    /* access modifiers changed from: private */
    public AtomicBoolean zzchz = new AtomicBoolean(true);

    protected zzwz(Context context, zzaev zzaev, zzama zzama, zzxg zzxg) {
        this.mContext = context;
        this.zzchv = zzaev;
        this.zzchw = this.zzchv.zzcwe;
        this.zzbwq = zzama;
        this.zzchu = zzxg;
    }

    public void cancel() {
        if (this.zzchz.getAndSet(false)) {
            this.zzbwq.stopLoading();
            zzbs.zzee();
            zzagw.zzh(this.zzbwq);
            zzx(-1);
            zzagr.zzczc.removeCallbacks(this.zzchx);
        }
    }

    public final void zza(zzama zzama, boolean z) {
        zzafj.zzbw("WebView finished loading.");
        int i = 0;
        if (this.zzchz.getAndSet(false)) {
            if (z) {
                i = -2;
            }
            zzx(i);
            zzagr.zzczc.removeCallbacks(this.zzchx);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void zzmw();

    public final /* synthetic */ Object zzmx() {
        zzbq.zzfz("Webview render task needs to be called on UI thread.");
        this.zzchx = new zzxa(this);
        zzagr.zzczc.postDelayed(this.zzchx, ((Long) zzbs.zzep().zzd(zzmq.zzblt)).longValue());
        zzmw();
        return null;
    }

    /* access modifiers changed from: protected */
    public void zzx(int i) {
        zzaeu zzaeu;
        int i2 = i;
        if (i2 != -2) {
            this.zzchw = new zzaad(i2, this.zzchw.zzccb);
        }
        this.zzbwq.zzsk();
        zzxg zzxg = this.zzchu;
        zzzz zzzz = this.zzchv.zzcpe;
        zzis zzis = zzzz.zzclo;
        zzama zzama = this.zzbwq;
        List<String> list = this.zzchw.zzcbv;
        List<String> list2 = this.zzchw.zzcbw;
        List<String> list3 = this.zzchw.zzcni;
        int i3 = this.zzchw.orientation;
        long j = this.zzchw.zzccb;
        String str = zzzz.zzclr;
        boolean z = this.zzchw.zzcng;
        long j2 = this.zzchw.zzcnh;
        zziw zziw = this.zzchv.zzath;
        long j3 = j2;
        long j4 = this.zzchw.zzcnf;
        long j5 = this.zzchv.zzcvw;
        long j6 = this.zzchw.zzcnk;
        String str2 = this.zzchw.zzcnl;
        JSONObject jSONObject = this.zzchv.zzcvq;
        zzadw zzadw = this.zzchw.zzcnv;
        List<String> list4 = this.zzchw.zzcnw;
        List<String> list5 = this.zzchw.zzcnx;
        boolean z2 = this.zzchw.zzcny;
        zzaaf zzaaf = this.zzchw.zzcnz;
        List<String> list6 = this.zzchw.zzcby;
        String str3 = this.zzchw.zzcoc;
        zzib zzib = this.zzchv.zzcwc;
        String str4 = str2;
        long j7 = j3;
        long j8 = j4;
        long j9 = j5;
        long j10 = j6;
        new zzaeu(zzis, zzama, list, i2, list2, list3, i3, j, str, z, null, null, null, null, null, j7, zziw, j8, j9, j10, str4, jSONObject, null, zzadw, list4, list5, z2, zzaaf, null, list6, str3, zzib, this.zzchv.zzcwe.zzapy, this.zzchv.zzcwd);
        zzxg.zzb(zzaeu);
    }
}
