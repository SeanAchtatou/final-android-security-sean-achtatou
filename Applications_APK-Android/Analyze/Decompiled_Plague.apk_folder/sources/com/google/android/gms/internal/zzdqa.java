package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdqe;
import java.io.IOException;

public final class zzdqa extends zzfee<zzdqa, zza> implements zzffk {
    private static volatile zzffm<zzdqa> zzbas;
    /* access modifiers changed from: private */
    public static final zzdqa zzlqq;
    private int zzlqb;
    private zzfdh zzlqj = zzfdh.zzpal;
    private zzdqe zzlqp;

    public static final class zza extends zzfef<zzdqa, zza> implements zzffk {
        private zza() {
            super(zzdqa.zzlqq);
        }

        /* synthetic */ zza(zzdqb zzdqb) {
            this();
        }

        public final zza zzb(zzdqe zzdqe) {
            zzcvi();
            ((zzdqa) this.zzpbv).zza(zzdqe);
            return this;
        }

        public final zza zzfk(int i) {
            zzcvi();
            ((zzdqa) this.zzpbv).setVersion(0);
            return this;
        }

        public final zza zzo(zzfdh zzfdh) {
            zzcvi();
            ((zzdqa) this.zzpbv).zzj(zzfdh);
            return this;
        }
    }

    static {
        zzdqa zzdqa = new zzdqa();
        zzlqq = zzdqa;
        zzdqa.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdqa.zzpbs.zzbim();
    }

    private zzdqa() {
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzlqb = i;
    }

    /* access modifiers changed from: private */
    public final void zza(zzdqe zzdqe) {
        if (zzdqe == null) {
            throw new NullPointerException();
        }
        this.zzlqp = zzdqe;
    }

    public static zza zzbmd() {
        zzdqa zzdqa = zzlqq;
        zzfef zzfef = (zzfef) zzdqa.zza(zzfem.zzpch, (Object) null, (Object) null);
        zzfef.zza((zzfee) zzdqa);
        return (zza) zzfef;
    }

    /* access modifiers changed from: private */
    public final void zzj(zzfdh zzfdh) {
        if (zzfdh == null) {
            throw new NullPointerException();
        }
        this.zzlqj = zzfdh;
    }

    public static zzdqa zzn(zzfdh zzfdh) throws zzfew {
        return (zzdqa) zzfee.zza(zzlqq, zzfdh);
    }

    public final int getVersion() {
        return this.zzlqb;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdqe.zza zza2;
        boolean z = true;
        boolean z2 = false;
        switch (zzdqb.zzbaq[i - 1]) {
            case 1:
                return new zzdqa();
            case 2:
                return zzlqq;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdqa zzdqa = (zzdqa) obj2;
                this.zzlqb = zzfen.zza(this.zzlqb != 0, this.zzlqb, zzdqa.zzlqb != 0, zzdqa.zzlqb);
                this.zzlqp = (zzdqe) zzfen.zza(this.zzlqp, zzdqa.zzlqp);
                boolean z3 = this.zzlqj != zzfdh.zzpal;
                zzfdh zzfdh = this.zzlqj;
                if (zzdqa.zzlqj == zzfdh.zzpal) {
                    z = false;
                }
                this.zzlqj = zzfen.zza(z3, zzfdh, z, zzdqa.zzlqj);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzlqb = zzfdq.zzcub();
                                } else if (zzcts == 18) {
                                    if (this.zzlqp != null) {
                                        zzdqe zzdqe = this.zzlqp;
                                        zzfef zzfef = (zzfef) zzdqe.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdqe);
                                        zza2 = (zzdqe.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlqp = (zzdqe) zzfdq.zza(zzdqe.zzbmg(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlqp);
                                        this.zzlqp = (zzdqe) zza2.zzcvj();
                                    }
                                } else if (zzcts == 26) {
                                    this.zzlqj = zzfdq.zzcua();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdqa.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlqq);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlqq;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlqb != 0) {
            zzfdv.zzab(1, this.zzlqb);
        }
        if (this.zzlqp != null) {
            zzfdv.zza(2, this.zzlqp == null ? zzdqe.zzbmg() : this.zzlqp);
        }
        if (!this.zzlqj.isEmpty()) {
            zzfdv.zza(3, this.zzlqj);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzfdh zzblt() {
        return this.zzlqj;
    }

    public final zzdqe zzbmc() {
        return this.zzlqp == null ? zzdqe.zzbmg() : this.zzlqp;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlqb != 0) {
            i2 = 0 + zzfdv.zzae(1, this.zzlqb);
        }
        if (this.zzlqp != null) {
            i2 += zzfdv.zzb(2, this.zzlqp == null ? zzdqe.zzbmg() : this.zzlqp);
        }
        if (!this.zzlqj.isEmpty()) {
            i2 += zzfdv.zzb(3, this.zzlqj);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
