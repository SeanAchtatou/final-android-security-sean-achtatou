package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbs;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@zzzb
public final class zztz implements zztl {
    private final Context mContext;
    private final Object mLock = new Object();
    private final long mStartTime;
    private final zznd zzamo;
    private final zzuc zzanb;
    private final boolean zzauu;
    private final zztn zzccq;
    private final boolean zzccu;
    private final boolean zzccv;
    private final zzzz zzcdj;
    private final long zzcdk;
    private boolean zzcdm = false;
    private final String zzcdo;
    private List<zztt> zzcdp = new ArrayList();
    private zztq zzcdt;

    public zztz(Context context, zzzz zzzz, zzuc zzuc, zztn zztn, boolean z, boolean z2, String str, long j, long j2, zznd zznd, boolean z3) {
        this.mContext = context;
        this.zzcdj = zzzz;
        this.zzanb = zzuc;
        this.zzccq = zztn;
        this.zzauu = z;
        this.zzccu = z2;
        this.zzcdo = str;
        this.mStartTime = j;
        this.zzcdk = j2;
        this.zzamo = zznd;
        this.zzccv = z3;
    }

    public final void cancel() {
        synchronized (this.mLock) {
            this.zzcdm = true;
            if (this.zzcdt != null) {
                this.zzcdt.cancel();
            }
        }
    }

    public final zztt zzg(List<zztm> list) {
        Object obj;
        Throwable th;
        zztq zztq;
        zztt zztt;
        zzafj.zzbw("Starting mediation.");
        ArrayList arrayList = new ArrayList();
        zznb zziz = this.zzamo.zziz();
        zziw zziw = this.zzcdj.zzath;
        int[] iArr = new int[2];
        if (zziw.zzbdc != null) {
            zzbs.zzew();
            if (zztv.zza(this.zzcdo, iArr)) {
                int i = iArr[0];
                int i2 = iArr[1];
                zziw[] zziwArr = zziw.zzbdc;
                int length = zziwArr.length;
                int i3 = 0;
                while (true) {
                    if (i3 >= length) {
                        break;
                    }
                    zziw zziw2 = zziwArr[i3];
                    if (i == zziw2.width && i2 == zziw2.height) {
                        zziw = zziw2;
                        break;
                    }
                    i3++;
                }
            }
        }
        Iterator<zztm> it = list.iterator();
        while (it.hasNext()) {
            zztm next = it.next();
            String valueOf = String.valueOf(next.zzcbc);
            zzafj.zzcn(valueOf.length() != 0 ? "Trying mediation network: ".concat(valueOf) : new String("Trying mediation network: "));
            Iterator<String> it2 = next.zzcbd.iterator();
            while (true) {
                if (it2.hasNext()) {
                    String next2 = it2.next();
                    zznb zziz2 = this.zzamo.zziz();
                    Object obj2 = this.mLock;
                    synchronized (obj2) {
                        try {
                            if (this.zzcdm) {
                                try {
                                    zztt = new zztt(-1);
                                } catch (Throwable th2) {
                                    th = th2;
                                    obj = obj2;
                                    throw th;
                                }
                            } else {
                                Iterator<zztm> it3 = it;
                                Iterator<String> it4 = it2;
                                zznb zznb = zziz;
                                zznb zznb2 = zziz2;
                                ArrayList arrayList2 = arrayList;
                                zztq zztq2 = zztq;
                                obj = obj2;
                                try {
                                    zztq = new zztq(this.mContext, next2, this.zzanb, this.zzccq, next, this.zzcdj.zzclo, zziw, this.zzcdj.zzatd, this.zzauu, this.zzccu, this.zzcdj.zzatt, this.zzcdj.zzaub, this.zzcdj.zzcmd, this.zzcdj.zzcmy, this.zzccv);
                                    this.zzcdt = zztq2;
                                    zztt zza = this.zzcdt.zza(this.mStartTime, this.zzcdk);
                                    this.zzcdp.add(zza);
                                    if (zza.zzcdc == 0) {
                                        zzafj.zzbw("Adapter succeeded.");
                                        this.zzamo.zzf("mediation_network_succeed", next2);
                                        ArrayList arrayList3 = arrayList2;
                                        if (!arrayList3.isEmpty()) {
                                            this.zzamo.zzf("mediation_networks_fail", TextUtils.join(",", arrayList3));
                                        }
                                        this.zzamo.zza(zznb2, "mls");
                                        this.zzamo.zza(zznb, "ttm");
                                        return zza;
                                    }
                                    zznb zznb3 = zznb;
                                    ArrayList arrayList4 = arrayList2;
                                    arrayList4.add(next2);
                                    this.zzamo.zza(zznb2, "mlf");
                                    if (zza.zzcde != null) {
                                        zzagr.zzczc.post(new zzua(this, zza));
                                    }
                                    arrayList = arrayList4;
                                    zziz = zznb3;
                                    it = it3;
                                    it2 = it4;
                                } catch (Throwable th3) {
                                    th = th3;
                                    th = th;
                                    throw th;
                                }
                            }
                        } catch (Throwable th4) {
                            th = th4;
                            obj = obj2;
                            th = th;
                            throw th;
                        }
                    }
                    return zztt;
                }
            }
        }
        ArrayList arrayList5 = arrayList;
        if (!arrayList5.isEmpty()) {
            this.zzamo.zzf("mediation_networks_fail", TextUtils.join(",", arrayList5));
        }
        return new zztt(1);
    }

    public final List<zztt> zzlo() {
        return this.zzcdp;
    }
}
