package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.video.Videos;

final class zzdy implements Videos.CaptureAvailableResult {
    private /* synthetic */ Status zzekv;

    zzdy(zzdx zzdx, Status status) {
        this.zzekv = status;
    }

    public final Status getStatus() {
        return this.zzekv;
    }

    public final boolean isAvailable() {
        return false;
    }
}
