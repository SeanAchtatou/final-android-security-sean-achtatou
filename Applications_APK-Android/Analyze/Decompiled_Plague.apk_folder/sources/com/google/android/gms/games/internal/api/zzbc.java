package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzbc extends zzbh {
    private /* synthetic */ boolean zzhpv;
    private /* synthetic */ int zzhqf;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbc(zzaz zzaz, GoogleApiClient googleApiClient, int i, boolean z) {
        super(googleApiClient);
        this.zzhqf = i;
        this.zzhpv = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.Players$LoadPlayersResult>, int, boolean, boolean):void
     arg types: [com.google.android.gms.games.internal.api.zzbc, int, int, boolean]
     candidates:
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzcl<com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer$ReliableMessageSentCallback>, byte[], java.lang.String, java.lang.String):int
      com.google.android.gms.games.internal.GamesClientImpl.zza(java.lang.String, boolean, boolean, int):android.content.Intent
      com.google.android.gms.games.internal.GamesClientImpl.zza(int, android.os.IBinder, android.os.Bundle, int):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzcl<? extends com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener>, com.google.android.gms.common.api.internal.zzcl<? extends com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener>, com.google.android.gms.common.api.internal.zzcl<? extends com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener>, com.google.android.gms.games.multiplayer.realtime.RoomConfig):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.request.Requests$LoadRequestsResult>, int, int, int):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.leaderboard.Leaderboards$LoadScoresResult>, com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer, int, int):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.leaderboard.Leaderboards$SubmitScoreResult>, java.lang.String, long, java.lang.String):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.snapshot.Snapshots$OpenSnapshotResult>, java.lang.String, boolean, int):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer$UpdateMatchResult>, java.lang.String, byte[], com.google.android.gms.games.multiplayer.ParticipantResult[]):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.quest.Quests$LoadQuestsResult>, int[], int, boolean):void
      com.google.android.gms.common.internal.zzd.zza(com.google.android.gms.common.internal.zzd, int, int, android.os.IInterface):boolean
      com.google.android.gms.common.internal.zzd.zza(int, android.os.IBinder, android.os.Bundle, int):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.Players$LoadPlayersResult>, int, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza((zzn<Players.LoadPlayersResult>) this, this.zzhqf, false, this.zzhpv);
    }
}
