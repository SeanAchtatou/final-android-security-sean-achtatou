package com.google.android.gms.internal;

final class zzffg<K, V> {
    public final V zzjul;
    public final zzfgr zzpda;
    public final K zzpdb;
    public final zzfgr zzpdc;

    public zzffg(zzfgr zzfgr, K k, zzfgr zzfgr2, V v) {
        this.zzpda = zzfgr;
        this.zzpdb = k;
        this.zzpdc = zzfgr2;
        this.zzjul = v;
    }
}
