package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzcj implements zzbo<TurnBasedMultiplayer.LeaveMatchResult, TurnBasedMatch> {
    zzcj() {
    }

    public final /* synthetic */ Object zzb(@Nullable Result result) {
        TurnBasedMatch match;
        TurnBasedMultiplayer.LeaveMatchResult leaveMatchResult = (TurnBasedMultiplayer.LeaveMatchResult) result;
        if (leaveMatchResult == null || (match = leaveMatchResult.getMatch()) == null) {
            return null;
        }
        return (TurnBasedMatch) match.freeze();
    }
}
