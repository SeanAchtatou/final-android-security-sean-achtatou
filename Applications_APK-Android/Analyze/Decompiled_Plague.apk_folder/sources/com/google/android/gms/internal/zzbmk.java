package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;

final class zzbmk implements DriveFolder.DriveFileResult {
    private final Status mStatus;
    private final DriveFile zzglv;

    public zzbmk(Status status, DriveFile driveFile) {
        this.mStatus = status;
        this.zzglv = driveFile;
    }

    public final DriveFile getDriveFile() {
        return this.zzglv;
    }

    public final Status getStatus() {
        return this.mStatus;
    }
}
