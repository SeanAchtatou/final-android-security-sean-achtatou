package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class zzbrl implements Parcelable.Creator<zzbrk> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        ArrayList<String> arrayList = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                zzbek.zzb(parcel, readInt);
            } else {
                arrayList = zzbek.zzac(parcel, readInt);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbrk(arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbrk[i];
    }
}
