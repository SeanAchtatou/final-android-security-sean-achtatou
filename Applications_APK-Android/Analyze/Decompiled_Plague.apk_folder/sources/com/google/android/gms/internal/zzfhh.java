package com.google.android.gms.internal;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

final class zzfhh implements Cloneable {
    private Object value;
    private zzfhf<?, ?> zzphe;
    private List<zzfhm> zzphf = new ArrayList();

    zzfhh() {
    }

    private final byte[] toByteArray() throws IOException {
        byte[] bArr = new byte[zzo()];
        zza(zzfhc.zzbe(bArr));
        return bArr;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzcxg */
    public zzfhh clone() {
        Object clone;
        zzfhh zzfhh = new zzfhh();
        try {
            zzfhh.zzphe = this.zzphe;
            if (this.zzphf == null) {
                zzfhh.zzphf = null;
            } else {
                zzfhh.zzphf.addAll(this.zzphf);
            }
            if (this.value != null) {
                if (this.value instanceof zzfhk) {
                    clone = (zzfhk) ((zzfhk) this.value).clone();
                } else if (this.value instanceof byte[]) {
                    clone = ((byte[]) this.value).clone();
                } else {
                    int i = 0;
                    if (this.value instanceof byte[][]) {
                        byte[][] bArr = (byte[][]) this.value;
                        byte[][] bArr2 = new byte[bArr.length][];
                        zzfhh.value = bArr2;
                        while (i < bArr.length) {
                            bArr2[i] = (byte[]) bArr[i].clone();
                            i++;
                        }
                    } else if (this.value instanceof boolean[]) {
                        clone = ((boolean[]) this.value).clone();
                    } else if (this.value instanceof int[]) {
                        clone = ((int[]) this.value).clone();
                    } else if (this.value instanceof long[]) {
                        clone = ((long[]) this.value).clone();
                    } else if (this.value instanceof float[]) {
                        clone = ((float[]) this.value).clone();
                    } else if (this.value instanceof double[]) {
                        clone = ((double[]) this.value).clone();
                    } else if (this.value instanceof zzfhk[]) {
                        zzfhk[] zzfhkArr = (zzfhk[]) this.value;
                        zzfhk[] zzfhkArr2 = new zzfhk[zzfhkArr.length];
                        zzfhh.value = zzfhkArr2;
                        while (i < zzfhkArr.length) {
                            zzfhkArr2[i] = (zzfhk) zzfhkArr[i].clone();
                            i++;
                        }
                    }
                }
                zzfhh.value = clone;
                return zzfhh;
            }
            return zzfhh;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfhh)) {
            return false;
        }
        zzfhh zzfhh = (zzfhh) obj;
        if (this.value == null || zzfhh.value == null) {
            if (this.zzphf != null && zzfhh.zzphf != null) {
                return this.zzphf.equals(zzfhh.zzphf);
            }
            try {
                return Arrays.equals(toByteArray(), zzfhh.toByteArray());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else if (this.zzphe != zzfhh.zzphe) {
            return false;
        } else {
            return !this.zzphe.zznan.isArray() ? this.value.equals(zzfhh.value) : this.value instanceof byte[] ? Arrays.equals((byte[]) this.value, (byte[]) zzfhh.value) : this.value instanceof int[] ? Arrays.equals((int[]) this.value, (int[]) zzfhh.value) : this.value instanceof long[] ? Arrays.equals((long[]) this.value, (long[]) zzfhh.value) : this.value instanceof float[] ? Arrays.equals((float[]) this.value, (float[]) zzfhh.value) : this.value instanceof double[] ? Arrays.equals((double[]) this.value, (double[]) zzfhh.value) : this.value instanceof boolean[] ? Arrays.equals((boolean[]) this.value, (boolean[]) zzfhh.value) : Arrays.deepEquals((Object[]) this.value, (Object[]) zzfhh.value);
        }
    }

    public final int hashCode() {
        try {
            return 527 + Arrays.hashCode(toByteArray());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.value != null) {
            zzfhf<?, ?> zzfhf = this.zzphe;
            Object obj = this.value;
            if (zzfhf.zzpgz) {
                int length = Array.getLength(obj);
                for (int i = 0; i < length; i++) {
                    Object obj2 = Array.get(obj, i);
                    if (obj2 != null) {
                        zzfhf.zza(obj2, zzfhc);
                    }
                }
                return;
            }
            zzfhf.zza(obj, zzfhc);
            return;
        }
        for (zzfhm next : this.zzphf) {
            zzfhc.zzlx(next.tag);
            zzfhc.zzbg(next.zzjkl);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzfhm zzfhm) {
        this.zzphf.add(zzfhm);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.google.android.gms.internal.zzfhf, com.google.android.gms.internal.zzfhf<?, T>, com.google.android.gms.internal.zzfhf<?, ?>, java.lang.Object] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    final <T> T zzb(com.google.android.gms.internal.zzfhf<?, T> r2) {
        /*
            r1 = this;
            java.lang.Object r0 = r1.value
            if (r0 == 0) goto L_0x0014
            com.google.android.gms.internal.zzfhf<?, ?> r0 = r1.zzphe
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0021
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r0 = "Tried to getExtension with a different Extension."
            r2.<init>(r0)
            throw r2
        L_0x0014:
            r1.zzphe = r2
            java.util.List<com.google.android.gms.internal.zzfhm> r0 = r1.zzphf
            java.lang.Object r2 = r2.zzbp(r0)
            r1.value = r2
            r2 = 0
            r1.zzphf = r2
        L_0x0021:
            java.lang.Object r2 = r1.value
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfhh.zzb(com.google.android.gms.internal.zzfhf):java.lang.Object");
    }

    /* access modifiers changed from: package-private */
    public final int zzo() {
        int i;
        if (this.value != null) {
            zzfhf<?, ?> zzfhf = this.zzphe;
            Object obj = this.value;
            if (!zzfhf.zzpgz) {
                return zzfhf.zzcn(obj);
            }
            int length = Array.getLength(obj);
            i = 0;
            for (int i2 = 0; i2 < length; i2++) {
                if (Array.get(obj, i2) != null) {
                    i += zzfhf.zzcn(Array.get(obj, i2));
                }
            }
        } else {
            i = 0;
            for (zzfhm next : this.zzphf) {
                i += zzfhc.zzly(next.tag) + 0 + next.zzjkl.length;
            }
        }
        return i;
    }
}
