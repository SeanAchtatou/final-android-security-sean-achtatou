package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;

public final class zzb extends TurnBasedMatchConfig {
    private final int zzhus;
    private final Bundle zzhvm;
    private final String[] zzhvo;
    private final int zzhvx;

    zzb(TurnBasedMatchConfig.Builder builder) {
        this.zzhus = builder.zzhus;
        this.zzhvx = builder.zzhvx;
        this.zzhvm = builder.zzhvm;
        this.zzhvo = (String[]) builder.zzhvl.toArray(new String[builder.zzhvl.size()]);
    }

    public final Bundle getAutoMatchCriteria() {
        return this.zzhvm;
    }

    public final String[] getInvitedPlayerIds() {
        return this.zzhvo;
    }

    public final int getVariant() {
        return this.zzhus;
    }

    public final int zzatx() {
        return this.zzhvx;
    }
}
