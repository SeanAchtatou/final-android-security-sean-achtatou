package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfhe;
import java.io.IOException;

public abstract class zzfhe<M extends zzfhe<M>> extends zzfhk {
    protected zzfhg zzpgy;

    public final <T> T zza(zzfhf zzfhf) {
        zzfhh zzlz;
        if (this.zzpgy == null || (zzlz = this.zzpgy.zzlz(zzfhf.tag >>> 3)) == null) {
            return null;
        }
        return zzlz.zzb(zzfhf);
    }

    public void zza(zzfhc zzfhc) throws IOException {
        if (this.zzpgy != null) {
            for (int i = 0; i < this.zzpgy.size(); i++) {
                this.zzpgy.zzma(i).zza(zzfhc);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final boolean zza(zzfhb zzfhb, int i) throws IOException {
        int position = zzfhb.getPosition();
        if (!zzfhb.zzkg(i)) {
            return false;
        }
        int i2 = i >>> 3;
        zzfhm zzfhm = new zzfhm(i, zzfhb.zzal(position, zzfhb.getPosition() - position));
        zzfhh zzfhh = null;
        if (this.zzpgy == null) {
            this.zzpgy = new zzfhg();
        } else {
            zzfhh = this.zzpgy.zzlz(i2);
        }
        if (zzfhh == null) {
            zzfhh = new zzfhh();
            this.zzpgy.zza(i2, zzfhh);
        }
        zzfhh.zza(zzfhm);
        return true;
    }

    /* renamed from: zzcxe */
    public M clone() throws CloneNotSupportedException {
        M m = (zzfhe) super.clone();
        zzfhi.zza(this, m);
        return m;
    }

    public /* synthetic */ zzfhk zzcxf() throws CloneNotSupportedException {
        return (zzfhe) clone();
    }

    /* access modifiers changed from: protected */
    public int zzo() {
        if (this.zzpgy == null) {
            return 0;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.zzpgy.size(); i2++) {
            i += this.zzpgy.zzma(i2).zzo();
        }
        return i;
    }
}
