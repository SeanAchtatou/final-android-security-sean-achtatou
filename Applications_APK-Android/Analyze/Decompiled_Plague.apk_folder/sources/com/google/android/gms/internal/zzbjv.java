package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;

public class zzbjv extends zzbpi {
    public void onError(Status status) throws RemoteException {
    }

    public void onSuccess() throws RemoteException {
    }

    public void zza(zzbps zzbps) throws RemoteException {
    }

    public void zza(zzbpu zzbpu) throws RemoteException {
    }

    public void zza(zzbpw zzbpw) throws RemoteException {
    }

    public void zza(zzbpy zzbpy) throws RemoteException {
    }

    public void zza(zzbqe zzbqe) throws RemoteException {
    }

    public void zza(zzbqg zzbqg) throws RemoteException {
    }

    public void zza(zzbqj zzbqj) throws RemoteException {
    }

    public void zza(zzbql zzbql) throws RemoteException {
    }
}
