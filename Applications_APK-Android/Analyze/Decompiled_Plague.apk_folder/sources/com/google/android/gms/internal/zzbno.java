package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzdf;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbno extends zzdf<zzbll, Void> {
    private /* synthetic */ DriveContents zzgmj;

    zzbno(zzbmu zzbmu, DriveContents driveContents) {
        this.zzgmj = driveContents;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbjy(this.zzgmj.zzanp().getRequestId(), false), new zzbsa(taskCompletionSource));
    }
}
