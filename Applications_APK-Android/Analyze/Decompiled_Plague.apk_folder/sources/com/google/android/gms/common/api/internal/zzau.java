package com.google.android.gms.common.api.internal;

import android.support.annotation.WorkerThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.zzj;
import java.util.Iterator;
import java.util.Map;

final class zzau extends zzbb {
    final /* synthetic */ zzar zzfor;
    private final Map<Api.zze, zzat> zzfot;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzau(zzar zzar, Map<Api.zze, zzat> map) {
        super(zzar, null);
        this.zzfor = zzar;
        this.zzfot = map;
    }

    @WorkerThread
    public final void zzahp() {
        boolean z;
        Iterator<Api.zze> it = this.zzfot.keySet().iterator();
        boolean z2 = true;
        int i = 0;
        boolean z3 = true;
        boolean z4 = false;
        while (true) {
            if (!it.hasNext()) {
                z = false;
                z2 = z4;
                break;
            }
            Api.zze next = it.next();
            if (!next.zzafu()) {
                z3 = false;
            } else if (!this.zzfot.get(next).zzfmm) {
                z = true;
                break;
            } else {
                z4 = true;
            }
        }
        if (z2) {
            i = this.zzfor.zzfni.isGooglePlayServicesAvailable(this.zzfor.mContext);
        }
        if (i == 0 || (!z && !z3)) {
            if (this.zzfor.zzfol) {
                this.zzfor.zzfoj.connect();
            }
            for (Api.zze next2 : this.zzfot.keySet()) {
                zzj zzj = this.zzfot.get(next2);
                if (!next2.zzafu() || i == 0) {
                    next2.zza(zzj);
                } else {
                    this.zzfor.zzfob.zza(new zzaw(this, this.zzfor, zzj));
                }
            }
            return;
        }
        this.zzfor.zzfob.zza(new zzav(this, this.zzfor, new ConnectionResult(i, null)));
    }
}
