package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrk;
import java.security.GeneralSecurityException;
import javax.crypto.spec.SecretKeySpec;

public final class zzdpn implements zzdos<zzdou> {
    zzdpn() {
    }

    private static void zza(zzdri zzdri) throws GeneralSecurityException {
        if (zzdri.zzbnp() < 10) {
            throw new GeneralSecurityException("tag size too small");
        }
        switch (zzdpo.zzlpn[zzdri.zzbno().ordinal()]) {
            case 1:
                if (zzdri.zzbnp() > 20) {
                    throw new GeneralSecurityException("tag size too big");
                }
                return;
            case 2:
                if (zzdri.zzbnp() > 32) {
                    throw new GeneralSecurityException("tag size too big");
                }
                return;
            case 3:
                if (zzdri.zzbnp() > 64) {
                    throw new GeneralSecurityException("tag size too big");
                }
                return;
            default:
                throw new GeneralSecurityException("unknown hash type");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: zzg */
    public final zzdou zza(zzfdh zzfdh) throws GeneralSecurityException {
        zzdtc zzdtc;
        try {
            zzdre zzw = zzdre.zzw(zzfdh);
            if (!(zzw instanceof zzdre)) {
                throw new GeneralSecurityException("expected HmacKey proto");
            }
            zzdre zzdre = zzw;
            zzdte.zzt(zzdre.getVersion(), 0);
            if (zzdre.zzblt().size() < 16) {
                throw new GeneralSecurityException("key too short");
            }
            zza(zzdre.zzbni());
            zzdrc zzbno = zzdre.zzbni().zzbno();
            SecretKeySpec secretKeySpec = new SecretKeySpec(zzdre.zzblt().toByteArray(), "HMAC");
            int zzbnp = zzdre.zzbni().zzbnp();
            switch (zzdpo.zzlpn[zzbno.ordinal()]) {
                case 1:
                    zzdtc = new zzdtc("HMACSHA1", secretKeySpec, zzbnp);
                    break;
                case 2:
                    zzdtc = new zzdtc("HMACSHA256", secretKeySpec, zzbnp);
                    break;
                case 3:
                    zzdtc = new zzdtc("HMACSHA512", secretKeySpec, zzbnp);
                    break;
                default:
                    throw new GeneralSecurityException("unknown hash");
            }
            return zzdtc;
        } catch (zzfew e) {
            throw new GeneralSecurityException("expected serialized HmacKey proto", e);
        }
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.HmacKey";
    }

    public final /* synthetic */ Object zza(zzffi zzffi) throws GeneralSecurityException {
        if (!(zzffi instanceof zzdre)) {
            throw new GeneralSecurityException("expected HmacKey proto");
        }
        zzdre zzdre = (zzdre) zzffi;
        zzdte.zzt(zzdre.getVersion(), 0);
        if (zzdre.zzblt().size() < 16) {
            throw new GeneralSecurityException("key too short");
        }
        zza(zzdre.zzbni());
        zzdrc zzbno = zzdre.zzbni().zzbno();
        SecretKeySpec secretKeySpec = new SecretKeySpec(zzdre.zzblt().toByteArray(), "HMAC");
        int zzbnp = zzdre.zzbni().zzbnp();
        switch (zzdpo.zzlpn[zzbno.ordinal()]) {
            case 1:
                return new zzdtc("HMACSHA1", secretKeySpec, zzbnp);
            case 2:
                return new zzdtc("HMACSHA256", secretKeySpec, zzbnp);
            case 3:
                return new zzdtc("HMACSHA512", secretKeySpec, zzbnp);
            default:
                throw new GeneralSecurityException("unknown hash");
        }
    }

    public final zzffi zzb(zzfdh zzfdh) throws GeneralSecurityException {
        try {
            return zzb(zzdrg.zzy(zzfdh));
        } catch (zzfew e) {
            throw new GeneralSecurityException("expected serialized HmacKeyFormat proto", e);
        }
    }

    public final zzffi zzb(zzffi zzffi) throws GeneralSecurityException {
        if (!(zzffi instanceof zzdrg)) {
            throw new GeneralSecurityException("expected HmacKeyFormat proto");
        }
        zzdrg zzdrg = (zzdrg) zzffi;
        if (zzdrg.getKeySize() < 16) {
            throw new GeneralSecurityException("key too short");
        }
        zza(zzdrg.zzbni());
        return zzdre.zzbnj().zzfq(0).zzc(zzdrg.zzbni()).zzx(zzfdh.zzay(zzdtd.zzgb(zzdrg.getKeySize()))).zzcvk();
    }

    public final zzdrk zzc(zzfdh zzfdh) throws GeneralSecurityException {
        return (zzdrk) zzdrk.zzbnv().zzoa("type.googleapis.com/google.crypto.tink.HmacKey").zzaa(((zzdre) zzb(zzfdh)).toByteString()).zzb(zzdrk.zzb.SYMMETRIC).zzcvk();
    }
}
