package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzb;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.common.util.zzp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class zzbfv extends zzbfn {
    public static final Parcelable.Creator<zzbfv> CREATOR = new zzbfw();
    private final String mClassName;
    private final int zzdzm;
    private final zzbfq zzfzs;
    private final Parcel zzfzz;
    private final int zzgaa = 2;
    private int zzgab;
    private int zzgac;

    zzbfv(int i, Parcel parcel, zzbfq zzbfq) {
        this.zzdzm = i;
        this.zzfzz = (Parcel) zzbq.checkNotNull(parcel);
        this.zzfzs = zzbfq;
        this.mClassName = this.zzfzs == null ? null : this.zzfzs.zzaln();
        this.zzgab = 2;
    }

    private static void zza(StringBuilder sb, int i, Object obj) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                sb.append(obj);
                return;
            case 7:
                sb.append("\"");
                sb.append(zzo.zzgm(obj.toString()));
                sb.append("\"");
                return;
            case 8:
                sb.append("\"");
                sb.append(zzb.zzj((byte[]) obj));
                sb.append("\"");
                return;
            case 9:
                sb.append("\"");
                sb.append(zzb.zzk((byte[]) obj));
                sb.append("\"");
                return;
            case 10:
                zzp.zza(sb, (HashMap) obj);
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                StringBuilder sb2 = new StringBuilder(26);
                sb2.append("Unknown type = ");
                sb2.append(i);
                throw new IllegalArgumentException(sb2.toString());
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.math.BigInteger[]] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.util.zza.zza(java.lang.StringBuilder, java.lang.Object[]):void
     arg types: [java.lang.StringBuilder, double[]]
     candidates:
      com.google.android.gms.common.util.zza.zza(java.lang.StringBuilder, double[]):void
      com.google.android.gms.common.util.zza.zza(java.lang.StringBuilder, float[]):void
      com.google.android.gms.common.util.zza.zza(java.lang.StringBuilder, long[]):void
      com.google.android.gms.common.util.zza.zza(java.lang.StringBuilder, java.lang.String[]):void
      com.google.android.gms.common.util.zza.zza(java.lang.StringBuilder, boolean[]):void
      com.google.android.gms.common.util.zza.zza(java.lang.Object[], java.lang.Object[]):T[]
      com.google.android.gms.common.util.zza.zza(java.lang.StringBuilder, java.lang.Object[]):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zza(java.lang.StringBuilder r6, com.google.android.gms.internal.zzbfl<?, ?> r7, android.os.Parcel r8, int r9) {
        /*
            r5 = this;
            boolean r0 = r7.zzfzn
            r1 = 0
            if (r0 == 0) goto L_0x00cb
            java.lang.String r0 = "["
            r6.append(r0)
            int r0 = r7.zzfzm
            r2 = 0
            switch(r0) {
                case 0: goto L_0x00ab;
                case 1: goto L_0x0082;
                case 2: goto L_0x007a;
                case 3: goto L_0x0072;
                case 4: goto L_0x005b;
                case 5: goto L_0x0052;
                case 6: goto L_0x0049;
                case 7: goto L_0x0040;
                case 8: goto L_0x0038;
                case 9: goto L_0x0038;
                case 10: goto L_0x0038;
                case 11: goto L_0x0018;
                default: goto L_0x0010;
            }
        L_0x0010:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "Unknown field type out."
            r6.<init>(r7)
            throw r6
        L_0x0018:
            android.os.Parcel[] r8 = com.google.android.gms.internal.zzbek.zzae(r8, r9)
            int r9 = r8.length
            r0 = r1
        L_0x001e:
            if (r0 >= r9) goto L_0x00c5
            if (r0 <= 0) goto L_0x0027
            java.lang.String r2 = ","
            r6.append(r2)
        L_0x0027:
            r2 = r8[r0]
            r2.setDataPosition(r1)
            java.util.Map r2 = r7.zzall()
            r3 = r8[r0]
            r5.zza(r6, r2, r3)
            int r0 = r0 + 1
            goto L_0x001e
        L_0x0038:
            java.lang.UnsupportedOperationException r6 = new java.lang.UnsupportedOperationException
            java.lang.String r7 = "List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported"
            r6.<init>(r7)
            throw r6
        L_0x0040:
            java.lang.String[] r7 = com.google.android.gms.internal.zzbek.zzaa(r8, r9)
            com.google.android.gms.common.util.zza.zza(r6, r7)
            goto L_0x00c5
        L_0x0049:
            boolean[] r7 = com.google.android.gms.internal.zzbek.zzv(r8, r9)
            com.google.android.gms.common.util.zza.zza(r6, r7)
            goto L_0x00c5
        L_0x0052:
            java.math.BigDecimal[] r7 = com.google.android.gms.internal.zzbek.zzz(r8, r9)
            com.google.android.gms.common.util.zza.zza(r6, r7)
            goto L_0x00c5
        L_0x005b:
            int r7 = com.google.android.gms.internal.zzbek.zza(r8, r9)
            int r9 = r8.dataPosition()
            if (r7 != 0) goto L_0x0066
            goto L_0x006e
        L_0x0066:
            double[] r2 = r8.createDoubleArray()
            int r9 = r9 + r7
            r8.setDataPosition(r9)
        L_0x006e:
            com.google.android.gms.common.util.zza.zza(r6, r2)
            goto L_0x00c5
        L_0x0072:
            float[] r7 = com.google.android.gms.internal.zzbek.zzy(r8, r9)
            com.google.android.gms.common.util.zza.zza(r6, r7)
            goto L_0x00c5
        L_0x007a:
            long[] r7 = com.google.android.gms.internal.zzbek.zzx(r8, r9)
            com.google.android.gms.common.util.zza.zza(r6, r7)
            goto L_0x00c5
        L_0x0082:
            int r7 = com.google.android.gms.internal.zzbek.zza(r8, r9)
            int r9 = r8.dataPosition()
            if (r7 != 0) goto L_0x008d
            goto L_0x00a7
        L_0x008d:
            int r0 = r8.readInt()
            java.math.BigInteger[] r2 = new java.math.BigInteger[r0]
        L_0x0093:
            if (r1 >= r0) goto L_0x00a3
            java.math.BigInteger r3 = new java.math.BigInteger
            byte[] r4 = r8.createByteArray()
            r3.<init>(r4)
            r2[r1] = r3
            int r1 = r1 + 1
            goto L_0x0093
        L_0x00a3:
            int r9 = r9 + r7
            r8.setDataPosition(r9)
        L_0x00a7:
            com.google.android.gms.common.util.zza.zza(r6, r2)
            goto L_0x00c5
        L_0x00ab:
            int[] r7 = com.google.android.gms.internal.zzbek.zzw(r8, r9)
            int r8 = r7.length
        L_0x00b0:
            if (r1 >= r8) goto L_0x00c5
            if (r1 == 0) goto L_0x00b9
            java.lang.String r9 = ","
            r6.append(r9)
        L_0x00b9:
            r9 = r7[r1]
            java.lang.String r9 = java.lang.Integer.toString(r9)
            r6.append(r9)
            int r1 = r1 + 1
            goto L_0x00b0
        L_0x00c5:
            java.lang.String r7 = "]"
            r6.append(r7)
            return
        L_0x00cb:
            int r0 = r7.zzfzm
            switch(r0) {
                case 0: goto L_0x01b0;
                case 1: goto L_0x01a8;
                case 2: goto L_0x01a0;
                case 3: goto L_0x0198;
                case 4: goto L_0x0190;
                case 5: goto L_0x0188;
                case 6: goto L_0x0180;
                case 7: goto L_0x016a;
                case 8: goto L_0x0154;
                case 9: goto L_0x013e;
                case 10: goto L_0x00e7;
                case 11: goto L_0x00d8;
                default: goto L_0x00d0;
            }
        L_0x00d0:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "Unknown field type out"
            r6.<init>(r7)
            throw r6
        L_0x00d8:
            android.os.Parcel r8 = com.google.android.gms.internal.zzbek.zzad(r8, r9)
            r8.setDataPosition(r1)
            java.util.Map r7 = r7.zzall()
            r5.zza(r6, r7, r8)
            return
        L_0x00e7:
            android.os.Bundle r7 = com.google.android.gms.internal.zzbek.zzs(r8, r9)
            java.util.Set r8 = r7.keySet()
            r8.size()
            java.lang.String r9 = "{"
            r6.append(r9)
            java.util.Iterator r8 = r8.iterator()
            r9 = 1
        L_0x00fc:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x0138
            java.lang.Object r0 = r8.next()
            java.lang.String r0 = (java.lang.String) r0
            if (r9 != 0) goto L_0x010f
            java.lang.String r9 = ","
            r6.append(r9)
        L_0x010f:
            java.lang.String r9 = "\""
            r6.append(r9)
            r6.append(r0)
            java.lang.String r9 = "\""
            r6.append(r9)
            java.lang.String r9 = ":"
            r6.append(r9)
            java.lang.String r9 = "\""
            r6.append(r9)
            java.lang.String r9 = r7.getString(r0)
            java.lang.String r9 = com.google.android.gms.common.util.zzo.zzgm(r9)
            r6.append(r9)
            java.lang.String r9 = "\""
            r6.append(r9)
            r9 = r1
            goto L_0x00fc
        L_0x0138:
            java.lang.String r7 = "}"
            r6.append(r7)
            return
        L_0x013e:
            byte[] r7 = com.google.android.gms.internal.zzbek.zzt(r8, r9)
            java.lang.String r8 = "\""
            r6.append(r8)
            java.lang.String r7 = com.google.android.gms.common.util.zzb.zzk(r7)
            r6.append(r7)
            java.lang.String r7 = "\""
            r6.append(r7)
            return
        L_0x0154:
            byte[] r7 = com.google.android.gms.internal.zzbek.zzt(r8, r9)
            java.lang.String r8 = "\""
            r6.append(r8)
            java.lang.String r7 = com.google.android.gms.common.util.zzb.zzj(r7)
            r6.append(r7)
            java.lang.String r7 = "\""
            r6.append(r7)
            return
        L_0x016a:
            java.lang.String r7 = com.google.android.gms.internal.zzbek.zzq(r8, r9)
            java.lang.String r8 = "\""
            r6.append(r8)
            java.lang.String r7 = com.google.android.gms.common.util.zzo.zzgm(r7)
            r6.append(r7)
            java.lang.String r7 = "\""
            r6.append(r7)
            return
        L_0x0180:
            boolean r7 = com.google.android.gms.internal.zzbek.zzc(r8, r9)
            r6.append(r7)
            return
        L_0x0188:
            java.math.BigDecimal r7 = com.google.android.gms.internal.zzbek.zzp(r8, r9)
            r6.append(r7)
            return
        L_0x0190:
            double r7 = com.google.android.gms.internal.zzbek.zzn(r8, r9)
            r6.append(r7)
            return
        L_0x0198:
            float r7 = com.google.android.gms.internal.zzbek.zzl(r8, r9)
            r6.append(r7)
            return
        L_0x01a0:
            long r7 = com.google.android.gms.internal.zzbek.zzi(r8, r9)
            r6.append(r7)
            return
        L_0x01a8:
            java.math.BigInteger r7 = com.google.android.gms.internal.zzbek.zzk(r8, r9)
            r6.append(r7)
            return
        L_0x01b0:
            int r7 = com.google.android.gms.internal.zzbek.zzg(r8, r9)
            r6.append(r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzbfv.zza(java.lang.StringBuilder, com.google.android.gms.internal.zzbfl, android.os.Parcel, int):void");
    }

    private final void zza(StringBuilder sb, Map<String, zzbfl<?, ?>> map, Parcel parcel) {
        Object obj;
        SparseArray sparseArray = new SparseArray();
        for (Map.Entry next : map.entrySet()) {
            sparseArray.put(((zzbfl) next.getValue()).zzfzp, next);
        }
        sb.append('{');
        int zzd = zzbek.zzd(parcel);
        boolean z = false;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            Map.Entry entry = (Map.Entry) sparseArray.get(65535 & readInt);
            if (entry != null) {
                if (z) {
                    sb.append(",");
                }
                zzbfl zzbfl = (zzbfl) entry.getValue();
                sb.append("\"");
                sb.append((String) entry.getKey());
                sb.append("\":");
                if (zzbfl.zzalk()) {
                    switch (zzbfl.zzfzm) {
                        case 0:
                            obj = Integer.valueOf(zzbek.zzg(parcel, readInt));
                            break;
                        case 1:
                            obj = zzbek.zzk(parcel, readInt);
                            break;
                        case 2:
                            obj = Long.valueOf(zzbek.zzi(parcel, readInt));
                            break;
                        case 3:
                            obj = Float.valueOf(zzbek.zzl(parcel, readInt));
                            break;
                        case 4:
                            obj = Double.valueOf(zzbek.zzn(parcel, readInt));
                            break;
                        case 5:
                            obj = zzbek.zzp(parcel, readInt);
                            break;
                        case 6:
                            obj = Boolean.valueOf(zzbek.zzc(parcel, readInt));
                            break;
                        case 7:
                            obj = zzbek.zzq(parcel, readInt);
                            break;
                        case 8:
                        case 9:
                            obj = zzbek.zzt(parcel, readInt);
                            break;
                        case 10:
                            obj = zzl(zzbek.zzs(parcel, readInt));
                            break;
                        case 11:
                            throw new IllegalArgumentException("Method does not accept concrete type.");
                        default:
                            int i = zzbfl.zzfzm;
                            StringBuilder sb2 = new StringBuilder(36);
                            sb2.append("Unknown field out type = ");
                            sb2.append(i);
                            throw new IllegalArgumentException(sb2.toString());
                    }
                    zzb(sb, zzbfl, zza(zzbfl, obj));
                } else {
                    zza(sb, zzbfl, parcel, readInt);
                }
                z = true;
            }
        }
        if (parcel.dataPosition() != zzd) {
            StringBuilder sb3 = new StringBuilder(37);
            sb3.append("Overread allowed size end=");
            sb3.append(zzd);
            throw new zzbel(sb3.toString(), parcel);
        }
        sb.append('}');
    }

    private Parcel zzalp() {
        switch (this.zzgab) {
            case 0:
                this.zzgac = zzbem.zze(this.zzfzz);
            case 1:
                zzbem.zzai(this.zzfzz, this.zzgac);
                this.zzgab = 2;
                break;
        }
        return this.zzfzz;
    }

    private final void zzb(StringBuilder sb, zzbfl<?, ?> zzbfl, Object obj) {
        if (zzbfl.zzfzl) {
            ArrayList arrayList = (ArrayList) obj;
            sb.append("[");
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                if (i != 0) {
                    sb.append(",");
                }
                zza(sb, zzbfl.zzfzk, arrayList.get(i));
            }
            sb.append("]");
            return;
        }
        zza(sb, zzbfl.zzfzk, obj);
    }

    private static HashMap<String, String> zzl(Bundle bundle) {
        HashMap<String, String> hashMap = new HashMap<>();
        for (String next : bundle.keySet()) {
            hashMap.put(next, bundle.getString(next));
        }
        return hashMap;
    }

    public String toString() {
        zzbq.checkNotNull(this.zzfzs, "Cannot convert to JSON on client side.");
        Parcel zzalp = zzalp();
        zzalp.setDataPosition(0);
        StringBuilder sb = new StringBuilder(100);
        zza(sb, this.zzfzs.zzgl(this.mClassName), zzalp);
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
     arg types: [android.os.Parcel, int, android.os.Parcel, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.zzbfq, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        zzbfq zzbfq;
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 1, this.zzdzm);
        zzbem.zza(parcel, 2, zzalp(), false);
        switch (this.zzgaa) {
            case 0:
                zzbfq = null;
                break;
            case 1:
            case 2:
                zzbfq = this.zzfzs;
                break;
            default:
                int i2 = this.zzgaa;
                StringBuilder sb = new StringBuilder(34);
                sb.append("Invalid creation type: ");
                sb.append(i2);
                throw new IllegalStateException(sb.toString());
        }
        zzbem.zza(parcel, 3, (Parcelable) zzbfq, i, false);
        zzbem.zzai(parcel, zze);
    }

    public final Map<String, zzbfl<?, ?>> zzaaj() {
        if (this.zzfzs == null) {
            return null;
        }
        return this.zzfzs.zzgl(this.mClassName);
    }

    public final Object zzgj(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    public final boolean zzgk(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }
}
