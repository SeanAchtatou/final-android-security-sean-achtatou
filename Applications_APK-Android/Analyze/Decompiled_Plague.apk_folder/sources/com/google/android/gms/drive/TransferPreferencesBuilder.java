package com.google.android.gms.drive;

import java.util.Arrays;

public class TransferPreferencesBuilder {
    public static final TransferPreferences DEFAULT_PREFERENCES = new zza(1, true, 256);
    private int zzghy;
    private boolean zzghz;
    private int zzgia;

    static class zza implements TransferPreferences {
        private final int zzghy;
        private final boolean zzghz;
        private final int zzgia;

        zza(int i, boolean z, int i2) {
            this.zzghy = i;
            this.zzghz = z;
            this.zzgia = i2;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            zza zza = (zza) obj;
            return zza.zzghy == this.zzghy && zza.zzghz == this.zzghz && zza.zzgia == this.zzgia;
        }

        public final int getBatteryUsagePreference() {
            return this.zzgia;
        }

        public final int getNetworkPreference() {
            return this.zzghy;
        }

        public final int hashCode() {
            return Arrays.hashCode(new Object[]{Integer.valueOf(this.zzghy), Boolean.valueOf(this.zzghz), Integer.valueOf(this.zzgia)});
        }

        public final boolean isRoamingAllowed() {
            return this.zzghz;
        }

        public final String toString() {
            return String.format("NetworkPreference: %s, IsRoamingAllowed %s, BatteryUsagePreference %s", Integer.valueOf(this.zzghy), Boolean.valueOf(this.zzghz), Integer.valueOf(this.zzgia));
        }
    }

    public TransferPreferencesBuilder() {
        this(DEFAULT_PREFERENCES);
    }

    public TransferPreferencesBuilder(FileUploadPreferences fileUploadPreferences) {
        this.zzghy = fileUploadPreferences.getNetworkTypePreference();
        this.zzghz = fileUploadPreferences.isRoamingAllowed();
        this.zzgia = fileUploadPreferences.getBatteryUsagePreference();
    }

    public TransferPreferencesBuilder(TransferPreferences transferPreferences) {
        this.zzghy = transferPreferences.getNetworkPreference();
        this.zzghz = transferPreferences.isRoamingAllowed();
        this.zzgia = transferPreferences.getBatteryUsagePreference();
    }

    public TransferPreferences build() {
        return new zza(this.zzghy, this.zzghz, this.zzgia);
    }

    public TransferPreferencesBuilder setBatteryUsagePreference(int i) {
        this.zzgia = i;
        return this;
    }

    public TransferPreferencesBuilder setIsRoamingAllowed(boolean z) {
        this.zzghz = z;
        return this;
    }

    public TransferPreferencesBuilder setNetworkPreference(int i) {
        this.zzghy = i;
        return this;
    }
}
