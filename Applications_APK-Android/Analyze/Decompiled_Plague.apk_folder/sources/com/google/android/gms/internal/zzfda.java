package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfcz;
import com.google.android.gms.internal.zzfda;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class zzfda<MessageType extends zzfcz<MessageType, BuilderType>, BuilderType extends zzfda<MessageType, BuilderType>> implements zzffj {
    protected static <T> void zza(Iterable iterable, List list) {
        zzfer.checkNotNull(iterable);
        if (iterable instanceof zzffd) {
            List<?> zzcwb = ((zzffd) iterable).zzcwb();
            zzffd zzffd = (zzffd) list;
            int size = list.size();
            for (Object next : zzcwb) {
                if (next == null) {
                    StringBuilder sb = new StringBuilder(37);
                    sb.append("Element at index ");
                    sb.append(zzffd.size() - size);
                    sb.append(" is null.");
                    String sb2 = sb.toString();
                    for (int size2 = zzffd.size() - 1; size2 >= size; size2--) {
                        zzffd.remove(size2);
                    }
                    throw new NullPointerException(sb2);
                } else if (!(next instanceof zzfdh)) {
                    zzffd.add((String) next);
                }
            }
        } else if (iterable instanceof zzffn) {
            list.addAll((Collection) iterable);
        } else {
            zzb(iterable, list);
        }
    }

    private static <T> void zzb(Iterable iterable, List list) {
        if ((list instanceof ArrayList) && (iterable instanceof Collection)) {
            ((ArrayList) list).ensureCapacity(list.size() + ((Collection) iterable).size());
        }
        int size = list.size();
        for (Object next : iterable) {
            if (next == null) {
                StringBuilder sb = new StringBuilder(37);
                sb.append("Element at index ");
                sb.append(list.size() - size);
                sb.append(" is null.");
                String sb2 = sb.toString();
                for (int size2 = list.size() - 1; size2 >= size; size2--) {
                    list.remove(size2);
                }
                throw new NullPointerException(sb2);
            }
            list.add(next);
        }
    }

    /* access modifiers changed from: protected */
    public abstract BuilderType zza(zzfcz zzfcz);

    /* renamed from: zza */
    public abstract BuilderType zzb(zzfdq zzfdq, zzfea zzfea) throws IOException;

    public final /* synthetic */ zzffj zzc(zzffi zzffi) {
        if (zzcvh().getClass().isInstance(zzffi)) {
            return zza((zzfcz) zzffi);
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }

    /* renamed from: zzctg */
    public abstract BuilderType clone();
}
