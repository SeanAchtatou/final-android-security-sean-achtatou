package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.zzi;
import java.util.Collections;
import java.util.List;

public final class zzbpw extends zzbej {
    public static final Parcelable.Creator<zzbpw> CREATOR = new zzbpx();
    private static final List<zzi> zzgny = Collections.emptyList();
    private int zzbzn;
    final long zzgnz;
    final long zzgoa;
    private List<zzi> zzgob;

    public zzbpw(long j, long j2, int i, List<zzi> list) {
        this.zzgnz = j;
        this.zzgoa = j2;
        this.zzbzn = i;
        this.zzgob = list;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, this.zzgnz);
        zzbem.zza(parcel, 3, this.zzgoa);
        zzbem.zzc(parcel, 4, this.zzbzn);
        zzbem.zzc(parcel, 5, this.zzgob, false);
        zzbem.zzai(parcel, zze);
    }
}
