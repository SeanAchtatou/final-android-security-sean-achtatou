package com.google.android.gms.games.snapshot;

import android.os.Parcelable;

public final class zzd implements Parcelable.Creator<zze> {
    /* JADX WARN: Type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r9) {
        /*
            r8 = this;
            int r0 = com.google.android.gms.internal.zzbek.zzd(r9)
            r1 = 0
            r3 = r1
            r4 = r3
            r5 = r4
            r6 = r5
            r7 = r6
        L_0x000a:
            int r1 = r9.dataPosition()
            if (r1 >= r0) goto L_0x0042
            int r1 = r9.readInt()
            r2 = 65535(0xffff, float:9.1834E-41)
            r2 = r2 & r1
            switch(r2) {
                case 1: goto L_0x003d;
                case 2: goto L_0x0038;
                case 3: goto L_0x001b;
                case 4: goto L_0x002e;
                case 5: goto L_0x0024;
                case 6: goto L_0x001f;
                default: goto L_0x001b;
            }
        L_0x001b:
            com.google.android.gms.internal.zzbek.zzb(r9, r1)
            goto L_0x000a
        L_0x001f:
            java.lang.Long r7 = com.google.android.gms.internal.zzbek.zzj(r9, r1)
            goto L_0x000a
        L_0x0024:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.BitmapTeleporter> r2 = com.google.android.gms.common.data.BitmapTeleporter.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r9, r1, r2)
            r5 = r1
            com.google.android.gms.common.data.BitmapTeleporter r5 = (com.google.android.gms.common.data.BitmapTeleporter) r5
            goto L_0x000a
        L_0x002e:
            android.os.Parcelable$Creator r2 = android.net.Uri.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r9, r1, r2)
            r6 = r1
            android.net.Uri r6 = (android.net.Uri) r6
            goto L_0x000a
        L_0x0038:
            java.lang.Long r4 = com.google.android.gms.internal.zzbek.zzj(r9, r1)
            goto L_0x000a
        L_0x003d:
            java.lang.String r3 = com.google.android.gms.internal.zzbek.zzq(r9, r1)
            goto L_0x000a
        L_0x0042:
            com.google.android.gms.internal.zzbek.zzaf(r9, r0)
            com.google.android.gms.games.snapshot.zze r9 = new com.google.android.gms.games.snapshot.zze
            r2 = r9
            r2.<init>(r3, r4, r5, r6, r7)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.games.snapshot.zzd.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zze[i];
    }
}
