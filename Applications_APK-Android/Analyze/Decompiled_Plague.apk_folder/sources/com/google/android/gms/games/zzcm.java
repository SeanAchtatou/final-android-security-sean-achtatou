package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzcm implements zzbo<TurnBasedMultiplayer.InitiateMatchResult, TurnBasedMatch> {
    zzcm() {
    }

    public final /* synthetic */ Object zzb(@Nullable Result result) {
        TurnBasedMatch match;
        TurnBasedMultiplayer.InitiateMatchResult initiateMatchResult = (TurnBasedMultiplayer.InitiateMatchResult) result;
        if (initiateMatchResult == null || (match = initiateMatchResult.getMatch()) == null) {
            return null;
        }
        return (TurnBasedMatch) match.freeze();
    }
}
