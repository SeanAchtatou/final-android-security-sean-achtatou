package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzcz extends zzdj {
    private /* synthetic */ String zzhlu;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcz(zzcw zzcw, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient, null);
        this.zzhlu = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zzc(this, this.zzhlu);
    }
}
