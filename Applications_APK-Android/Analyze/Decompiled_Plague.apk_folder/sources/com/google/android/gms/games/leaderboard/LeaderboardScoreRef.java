package com.google.android.gms.games.leaderboard;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;

public final class LeaderboardScoreRef extends zzc implements LeaderboardScore {
    private final PlayerRef zzhub;

    LeaderboardScoreRef(DataHolder dataHolder, int i) {
        super(dataHolder, i);
        this.zzhub = new PlayerRef(dataHolder, i);
    }

    public final boolean equals(Object obj) {
        return LeaderboardScoreEntity.zza(this, obj);
    }

    public final /* synthetic */ Object freeze() {
        return new LeaderboardScoreEntity(this);
    }

    public final String getDisplayRank() {
        return getString("display_rank");
    }

    public final void getDisplayRank(CharArrayBuffer charArrayBuffer) {
        zza("display_rank", charArrayBuffer);
    }

    public final String getDisplayScore() {
        return getString("display_score");
    }

    public final void getDisplayScore(CharArrayBuffer charArrayBuffer) {
        zza("display_score", charArrayBuffer);
    }

    public final long getRank() {
        return getLong("rank");
    }

    public final long getRawScore() {
        return getLong("raw_score");
    }

    public final Player getScoreHolder() {
        if (zzfx("external_player_id")) {
            return null;
        }
        return this.zzhub;
    }

    public final String getScoreHolderDisplayName() {
        return zzfx("external_player_id") ? getString("default_display_name") : this.zzhub.getDisplayName();
    }

    public final void getScoreHolderDisplayName(CharArrayBuffer charArrayBuffer) {
        if (zzfx("external_player_id")) {
            zza("default_display_name", charArrayBuffer);
        } else {
            this.zzhub.getDisplayName(charArrayBuffer);
        }
    }

    public final Uri getScoreHolderHiResImageUri() {
        if (zzfx("external_player_id")) {
            return null;
        }
        return this.zzhub.getHiResImageUri();
    }

    public final String getScoreHolderHiResImageUrl() {
        if (zzfx("external_player_id")) {
            return null;
        }
        return this.zzhub.getHiResImageUrl();
    }

    public final Uri getScoreHolderIconImageUri() {
        return zzfx("external_player_id") ? zzfw("default_display_image_uri") : this.zzhub.getIconImageUri();
    }

    public final String getScoreHolderIconImageUrl() {
        return zzfx("external_player_id") ? getString("default_display_image_url") : this.zzhub.getIconImageUrl();
    }

    public final String getScoreTag() {
        return getString("score_tag");
    }

    public final long getTimestampMillis() {
        return getLong("achieved_timestamp");
    }

    public final int hashCode() {
        return LeaderboardScoreEntity.zza(this);
    }

    public final String toString() {
        return LeaderboardScoreEntity.zzb(this);
    }
}
