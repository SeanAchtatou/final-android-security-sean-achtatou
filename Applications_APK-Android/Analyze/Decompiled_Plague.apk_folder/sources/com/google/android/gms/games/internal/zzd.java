package com.google.android.gms.games.internal;

import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.internal.zzcac;
import com.google.android.gms.internal.zzcae;

final class zzd extends zzcae {
    private /* synthetic */ GamesClientImpl zzhmx;

    zzd(GamesClientImpl gamesClientImpl) {
        this.zzhmx = gamesClientImpl;
    }

    public final zzcac zzast() {
        return new GamesClientImpl.zzv();
    }
}
