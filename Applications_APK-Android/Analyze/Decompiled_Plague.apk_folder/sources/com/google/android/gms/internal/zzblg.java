package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.TransferPreferencesBuilder;

final class zzblg extends zzbjv {
    private final zzn<DriveApi.zza> zzfzc;

    private zzblg(zzn<DriveApi.zza> zzn) {
        this.zzfzc = zzn;
    }

    /* synthetic */ zzblg(zzn zzn, zzbkq zzbkq) {
        this(zzn);
    }

    public final void onError(Status status) throws RemoteException {
        this.zzfzc.setResult(new zzblh(status, null, null));
    }

    public final void zza(zzbpu zzbpu) throws RemoteException {
        this.zzfzc.setResult(new zzblh(Status.zzfko, new TransferPreferencesBuilder(zzbpu.zzgnx).build(), null));
    }

    public final void zza(zzbql zzbql) throws RemoteException {
        this.zzfzc.setResult(new zzblh(Status.zzfko, zzbql.zzgol, null));
    }
}
