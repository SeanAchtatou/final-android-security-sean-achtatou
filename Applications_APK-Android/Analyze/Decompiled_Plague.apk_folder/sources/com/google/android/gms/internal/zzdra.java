package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrt;
import java.io.IOException;

public final class zzdra extends zzfee<zzdra, zza> implements zzffk {
    private static volatile zzffm<zzdra> zzbas;
    /* access modifiers changed from: private */
    public static final zzdra zzlsa;
    private zzfdh zzlry = zzfdh.zzpal;
    private zzdrt zzlrz;

    public static final class zza extends zzfef<zzdra, zza> implements zzffk {
        private zza() {
            super(zzdra.zzlsa);
        }

        /* synthetic */ zza(zzdrb zzdrb) {
            this();
        }
    }

    static {
        zzdra zzdra = new zzdra();
        zzlsa = zzdra;
        zzdra.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdra.zzpbs.zzbim();
    }

    private zzdra() {
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdrt.zza zza2;
        boolean z = false;
        switch (zzdrb.zzbaq[i - 1]) {
            case 1:
                return new zzdra();
            case 2:
                return zzlsa;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdra zzdra = (zzdra) obj2;
                boolean z2 = this.zzlry != zzfdh.zzpal;
                zzfdh zzfdh = this.zzlry;
                if (zzdra.zzlry != zzfdh.zzpal) {
                    z = true;
                }
                this.zzlry = zzfen.zza(z2, zzfdh, z, zzdra.zzlry);
                this.zzlrz = (zzdrt) zzfen.zza(this.zzlrz, zzdra.zzlrz);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 18) {
                                    this.zzlry = zzfdq.zzcua();
                                } else if (zzcts == 26) {
                                    if (this.zzlrz != null) {
                                        zzdrt zzdrt = this.zzlrz;
                                        zzfef zzfef = (zzfef) zzdrt.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdrt);
                                        zza2 = (zzdrt.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlrz = (zzdrt) zzfdq.zza(zzdrt.zzbom(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlrz);
                                        this.zzlrz = (zzdrt) zza2.zzcvj();
                                    }
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdra.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlsa);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlsa;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (!this.zzlry.isEmpty()) {
            zzfdv.zza(2, this.zzlry);
        }
        if (this.zzlrz != null) {
            zzfdv.zza(3, this.zzlrz == null ? zzdrt.zzbom() : this.zzlrz);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (!this.zzlry.isEmpty()) {
            i2 = 0 + zzfdv.zzb(2, this.zzlry);
        }
        if (this.zzlrz != null) {
            i2 += zzfdv.zzb(3, this.zzlrz == null ? zzdrt.zzbom() : this.zzlrz);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
