package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.MetadataChangeSet;

final class zzbmh extends zzbmn {
    private /* synthetic */ MetadataChangeSet zzglq;
    private /* synthetic */ zzbmf zzglu;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbmh(zzbmf zzbmf, GoogleApiClient googleApiClient, MetadataChangeSet metadataChangeSet) {
        super(googleApiClient);
        this.zzglu = zzbmf;
        this.zzglq = metadataChangeSet;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        zzbll zzbll = (zzbll) zzb;
        this.zzglq.zzanz().setContext(zzbll.getContext());
        ((zzbpf) zzbll.zzakb()).zza(new zzbki(this.zzglu.getDriveId(), this.zzglq.zzanz()), new zzbmj(this));
    }
}
