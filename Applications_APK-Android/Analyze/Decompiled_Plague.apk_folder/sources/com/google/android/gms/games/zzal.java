package com.google.android.gms.games;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.leaderboard.LeaderboardBuffer;
import com.google.android.gms.games.leaderboard.Leaderboards;

final class zzal implements zzbo<Leaderboards.LeaderboardMetadataResult, LeaderboardBuffer> {
    zzal() {
    }

    public final /* synthetic */ Object zzb(Result result) {
        Leaderboards.LeaderboardMetadataResult leaderboardMetadataResult = (Leaderboards.LeaderboardMetadataResult) result;
        if (leaderboardMetadataResult == null) {
            return null;
        }
        return leaderboardMetadataResult.getLeaderboards();
    }
}
