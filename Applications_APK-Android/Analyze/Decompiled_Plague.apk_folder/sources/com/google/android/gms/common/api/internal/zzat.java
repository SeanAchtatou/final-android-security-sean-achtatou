package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzj;
import java.lang.ref.WeakReference;

final class zzat implements zzj {
    private final Api<?> zzffv;
    /* access modifiers changed from: private */
    public final boolean zzfmm;
    private final WeakReference<zzar> zzfos;

    public zzat(zzar zzar, Api<?> api, boolean z) {
        this.zzfos = new WeakReference<>(zzar);
        this.zzffv = api;
        this.zzfmm = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final void zzf(@NonNull ConnectionResult connectionResult) {
        zzar zzar = this.zzfos.get();
        if (zzar != null) {
            zzbq.zza(Looper.myLooper() == zzar.zzfob.zzfmo.getLooper(), (Object) "onReportServiceBinding must be called on the GoogleApiClient handler thread");
            zzar.zzfmy.lock();
            try {
                if (zzar.zzbt(0)) {
                    if (!connectionResult.isSuccess()) {
                        zzar.zzb(connectionResult, this.zzffv, this.zzfmm);
                    }
                    if (zzar.zzahq()) {
                        zzar.zzahr();
                    }
                }
            } finally {
                zzar.zzfmy.unlock();
            }
        }
    }
}
