package com.google.android.gms.games;

import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzaz extends zzac<Intent> {
    private /* synthetic */ Room zzhkd;
    private /* synthetic */ int zzhke;

    zzaz(RealTimeMultiplayerClient realTimeMultiplayerClient, Room room, int i) {
        this.zzhkd = room;
        this.zzhke = i;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<Intent> taskCompletionSource) throws RemoteException {
        taskCompletionSource.setResult(gamesClientImpl.zza(this.zzhkd, this.zzhke));
    }
}
