package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zze<TResult> extends zza {
    private final TaskCompletionSource<TResult> zzeay;
    private final zzdf<Api.zzb, TResult> zzfky;
    private final zzdb zzfkz;

    public zze(int i, zzdf<Api.zzb, TResult> zzdf, TaskCompletionSource<TResult> taskCompletionSource, zzdb zzdb) {
        super(i);
        this.zzeay = taskCompletionSource;
        this.zzfky = zzdf;
        this.zzfkz = zzdb;
    }

    public final void zza(@NonNull zzah zzah, boolean z) {
        zzah.zza(this.zzeay, z);
    }

    public final void zza(zzbr<?> zzbr) throws DeadObjectException {
        try {
            this.zzfky.zza(zzbr.zzahd(), this.zzeay);
        } catch (DeadObjectException e) {
            throw e;
        } catch (RemoteException e2) {
            zzs(zza.zza(e2));
        }
    }

    public final void zzs(@NonNull Status status) {
        this.zzeay.trySetException(this.zzfkz.zzt(status));
    }
}
