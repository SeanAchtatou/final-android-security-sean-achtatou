package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;

public class zzbrs<T> extends zzbjv {
    private TaskCompletionSource<T> zzgow;

    zzbrs(TaskCompletionSource<T> taskCompletionSource) {
        this.zzgow = taskCompletionSource;
    }

    public final void onError(Status status) throws RemoteException {
        this.zzgow.setException(new ApiException(status));
    }

    public final TaskCompletionSource<T> zzaox() {
        return this.zzgow;
    }
}
