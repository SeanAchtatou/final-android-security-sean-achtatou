package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzaaa;
import com.google.android.gms.internal.zzaau;
import com.google.android.gms.internal.zzaeu;
import com.google.android.gms.internal.zzaey;
import com.google.android.gms.internal.zzafh;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzagr;
import com.google.android.gms.internal.zzagw;
import com.google.android.gms.internal.zzaiy;
import com.google.android.gms.internal.zzama;
import com.google.android.gms.internal.zzgt;
import com.google.android.gms.internal.zzid;
import com.google.android.gms.internal.zzig;
import com.google.android.gms.internal.zzis;
import com.google.android.gms.internal.zziw;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.internal.zznd;
import com.google.android.gms.internal.zzpu;
import com.google.android.gms.internal.zzqe;
import com.google.android.gms.internal.zzto;
import com.google.android.gms.internal.zztv;
import com.google.android.gms.internal.zzuc;
import com.google.android.gms.internal.zzzb;
import com.google.android.gms.internal.zzze;
import com.tapjoy.TapjoyConstants;
import org.json.JSONException;
import org.json.JSONObject;

@zzzb
public abstract class zzd extends zza implements zzn, zzbl, zzto {
    protected final zzuc zzanb;
    private transient boolean zzanc;

    public zzd(Context context, zziw zziw, String str, zzuc zzuc, zzaiy zzaiy, zzv zzv) {
        this(new zzbt(context, zziw, str, zzaiy), zzuc, null, zzv);
    }

    private zzd(zzbt zzbt, zzuc zzuc, @Nullable zzbi zzbi, zzv zzv) {
        super(zzbt, null, zzv);
        this.zzanb = zzuc;
        this.zzanc = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0148  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0163  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0166  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0171  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0180  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.internal.zzaaa zza(com.google.android.gms.internal.zzis r62, android.os.Bundle r63, com.google.android.gms.internal.zzaey r64, int r65) {
        /*
            r61 = this;
            r0 = r61
            com.google.android.gms.ads.internal.zzbt r1 = r0.zzamt
            android.content.Context r1 = r1.zzaif
            android.content.pm.ApplicationInfo r7 = r1.getApplicationInfo()
            r1 = 0
            r2 = 0
            com.google.android.gms.ads.internal.zzbt r3 = r0.zzamt     // Catch:{ NameNotFoundException -> 0x001c }
            android.content.Context r3 = r3.zzaif     // Catch:{ NameNotFoundException -> 0x001c }
            com.google.android.gms.internal.zzbgb r3 = com.google.android.gms.internal.zzbgc.zzcy(r3)     // Catch:{ NameNotFoundException -> 0x001c }
            java.lang.String r4 = r7.packageName     // Catch:{ NameNotFoundException -> 0x001c }
            android.content.pm.PackageInfo r3 = r3.getPackageInfo(r4, r2)     // Catch:{ NameNotFoundException -> 0x001c }
            r8 = r3
            goto L_0x001d
        L_0x001c:
            r8 = r1
        L_0x001d:
            com.google.android.gms.ads.internal.zzbt r3 = r0.zzamt
            android.content.Context r3 = r3.zzaif
            android.content.res.Resources r3 = r3.getResources()
            android.util.DisplayMetrics r3 = r3.getDisplayMetrics()
            com.google.android.gms.ads.internal.zzbt r4 = r0.zzamt
            com.google.android.gms.ads.internal.zzbu r4 = r4.zzate
            if (r4 == 0) goto L_0x0094
            com.google.android.gms.ads.internal.zzbt r4 = r0.zzamt
            com.google.android.gms.ads.internal.zzbu r4 = r4.zzate
            android.view.ViewParent r4 = r4.getParent()
            if (r4 == 0) goto L_0x0094
            r4 = 2
            int[] r4 = new int[r4]
            com.google.android.gms.ads.internal.zzbt r5 = r0.zzamt
            com.google.android.gms.ads.internal.zzbu r5 = r5.zzate
            r5.getLocationOnScreen(r4)
            r5 = r4[r2]
            r6 = 1
            r4 = r4[r6]
            com.google.android.gms.ads.internal.zzbt r9 = r0.zzamt
            com.google.android.gms.ads.internal.zzbu r9 = r9.zzate
            int r9 = r9.getWidth()
            com.google.android.gms.ads.internal.zzbt r10 = r0.zzamt
            com.google.android.gms.ads.internal.zzbu r10 = r10.zzate
            int r10 = r10.getHeight()
            com.google.android.gms.ads.internal.zzbt r11 = r0.zzamt
            com.google.android.gms.ads.internal.zzbu r11 = r11.zzate
            boolean r11 = r11.isShown()
            if (r11 == 0) goto L_0x0073
            int r11 = r5 + r9
            if (r11 <= 0) goto L_0x0073
            int r11 = r4 + r10
            if (r11 <= 0) goto L_0x0073
            int r11 = r3.widthPixels
            if (r5 > r11) goto L_0x0073
            int r11 = r3.heightPixels
            if (r4 > r11) goto L_0x0073
            goto L_0x0074
        L_0x0073:
            r6 = r2
        L_0x0074:
            android.os.Bundle r11 = new android.os.Bundle
            r12 = 5
            r11.<init>(r12)
            java.lang.String r12 = "x"
            r11.putInt(r12, r5)
            java.lang.String r5 = "y"
            r11.putInt(r5, r4)
            java.lang.String r4 = "width"
            r11.putInt(r4, r9)
            java.lang.String r4 = "height"
            r11.putInt(r4, r10)
            java.lang.String r4 = "visible"
            r11.putInt(r4, r6)
            goto L_0x0095
        L_0x0094:
            r11 = r1
        L_0x0095:
            com.google.android.gms.internal.zzaez r4 = com.google.android.gms.ads.internal.zzbs.zzeg()
            java.lang.String r9 = r4.zzou()
            com.google.android.gms.ads.internal.zzbt r4 = r0.zzamt
            com.google.android.gms.internal.zzaew r5 = new com.google.android.gms.internal.zzaew
            com.google.android.gms.ads.internal.zzbt r6 = r0.zzamt
            java.lang.String r6 = r6.zzatb
            r5.<init>(r9, r6)
            r4.zzatk = r5
            com.google.android.gms.ads.internal.zzbt r4 = r0.zzamt
            com.google.android.gms.internal.zzaew r4 = r4.zzatk
            r5 = r62
            r4.zzo(r5)
            com.google.android.gms.ads.internal.zzbs.zzec()
            com.google.android.gms.ads.internal.zzbt r4 = r0.zzamt
            android.content.Context r4 = r4.zzaif
            com.google.android.gms.ads.internal.zzbt r6 = r0.zzamt
            com.google.android.gms.ads.internal.zzbu r6 = r6.zzate
            com.google.android.gms.ads.internal.zzbt r10 = r0.zzamt
            com.google.android.gms.internal.zziw r10 = r10.zzath
            java.lang.String r20 = com.google.android.gms.internal.zzagr.zza(r4, r6, r10)
            r12 = 0
            com.google.android.gms.ads.internal.zzbt r4 = r0.zzamt
            com.google.android.gms.internal.zzkm r4 = r4.zzato
            if (r4 == 0) goto L_0x00de
            com.google.android.gms.ads.internal.zzbt r4 = r0.zzamt     // Catch:{ RemoteException -> 0x00d9 }
            com.google.android.gms.internal.zzkm r4 = r4.zzato     // Catch:{ RemoteException -> 0x00d9 }
            long r14 = r4.getValue()     // Catch:{ RemoteException -> 0x00d9 }
            r21 = r14
            goto L_0x00e0
        L_0x00d9:
            java.lang.String r4 = "Cannot get correlation id, default to 0."
            com.google.android.gms.internal.zzafj.zzco(r4)
        L_0x00de:
            r21 = r12
        L_0x00e0:
            java.util.UUID r4 = java.util.UUID.randomUUID()
            java.lang.String r23 = r4.toString()
            com.google.android.gms.internal.zzaez r4 = com.google.android.gms.ads.internal.zzbs.zzeg()
            com.google.android.gms.ads.internal.zzbt r6 = r0.zzamt
            android.content.Context r6 = r6.zzaif
            android.os.Bundle r12 = r4.zza(r6, r0, r9)
            java.util.ArrayList r14 = new java.util.ArrayList
            r14.<init>()
            java.util.ArrayList r15 = new java.util.ArrayList
            r15.<init>()
            r4 = r2
        L_0x00ff:
            com.google.android.gms.ads.internal.zzbt r6 = r0.zzamt
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.internal.zzqh> r6 = r6.zzats
            int r6 = r6.size()
            if (r4 >= r6) goto L_0x0130
            com.google.android.gms.ads.internal.zzbt r6 = r0.zzamt
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.internal.zzqh> r6 = r6.zzats
            java.lang.Object r6 = r6.keyAt(r4)
            java.lang.String r6 = (java.lang.String) r6
            r14.add(r6)
            com.google.android.gms.ads.internal.zzbt r10 = r0.zzamt
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.internal.zzqe> r10 = r10.zzatr
            boolean r10 = r10.containsKey(r6)
            if (r10 == 0) goto L_0x012d
            com.google.android.gms.ads.internal.zzbt r10 = r0.zzamt
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.internal.zzqe> r10 = r10.zzatr
            java.lang.Object r10 = r10.get(r6)
            if (r10 == 0) goto L_0x012d
            r15.add(r6)
        L_0x012d:
            int r4 = r4 + 1
            goto L_0x00ff
        L_0x0130:
            com.google.android.gms.ads.internal.zze r4 = new com.google.android.gms.ads.internal.zze
            r4.<init>(r0)
            java.util.concurrent.ThreadPoolExecutor r6 = com.google.android.gms.internal.zzagl.zzcyx
            com.google.android.gms.internal.zzajp r34 = com.google.android.gms.internal.zzagl.zza(r6, r4)
            com.google.android.gms.ads.internal.zzf r4 = new com.google.android.gms.ads.internal.zzf
            r4.<init>(r0)
            java.util.concurrent.ThreadPoolExecutor r6 = com.google.android.gms.internal.zzagl.zzcyx
            com.google.android.gms.internal.zzajp r44 = com.google.android.gms.internal.zzagl.zza(r6, r4)
            if (r64 == 0) goto L_0x014f
            java.lang.String r4 = r64.zzoq()
            r35 = r4
            goto L_0x0151
        L_0x014f:
            r35 = r1
        L_0x0151:
            com.google.android.gms.ads.internal.zzbt r4 = r0.zzamt
            java.util.List<java.lang.String> r4 = r4.zzaub
            if (r4 == 0) goto L_0x0198
            com.google.android.gms.ads.internal.zzbt r4 = r0.zzamt
            java.util.List<java.lang.String> r4 = r4.zzaub
            int r4 = r4.size()
            if (r4 <= 0) goto L_0x0198
            if (r8 == 0) goto L_0x0166
            int r4 = r8.versionCode
            goto L_0x0167
        L_0x0166:
            r4 = r2
        L_0x0167:
            com.google.android.gms.internal.zzaez r6 = com.google.android.gms.ads.internal.zzbs.zzeg()
            int r6 = r6.zzpe()
            if (r4 <= r6) goto L_0x0180
            com.google.android.gms.internal.zzaez r6 = com.google.android.gms.ads.internal.zzbs.zzeg()
            r6.zzpl()
            com.google.android.gms.internal.zzaez r6 = com.google.android.gms.ads.internal.zzbs.zzeg()
            r6.zzac(r4)
            goto L_0x0198
        L_0x0180:
            com.google.android.gms.internal.zzaez r4 = com.google.android.gms.ads.internal.zzbs.zzeg()
            org.json.JSONObject r4 = r4.zzpk()
            if (r4 == 0) goto L_0x0198
            com.google.android.gms.ads.internal.zzbt r6 = r0.zzamt
            java.lang.String r6 = r6.zzatb
            org.json.JSONArray r4 = r4.optJSONArray(r6)
            if (r4 == 0) goto L_0x0198
            java.lang.String r1 = r4.toString()
        L_0x0198:
            r46 = r1
            com.google.android.gms.internal.zzaaa r1 = new com.google.android.gms.internal.zzaaa
            com.google.android.gms.ads.internal.zzbt r4 = r0.zzamt
            com.google.android.gms.internal.zziw r6 = r4.zzath
            com.google.android.gms.ads.internal.zzbt r4 = r0.zzamt
            java.lang.String r10 = r4.zzatb
            java.lang.String r13 = com.google.android.gms.internal.zzjk.zzhz()
            com.google.android.gms.ads.internal.zzbt r4 = r0.zzamt
            com.google.android.gms.internal.zzaiy r4 = r4.zzatd
            com.google.android.gms.ads.internal.zzbt r2 = r0.zzamt
            java.util.List<java.lang.String> r2 = r2.zzaub
            r51 = r2
            com.google.android.gms.internal.zzaez r2 = com.google.android.gms.ads.internal.zzbs.zzeg()
            boolean r16 = r2.zzox()
            int r2 = r3.widthPixels
            r52 = r15
            int r15 = r3.heightPixels
            float r3 = r3.density
            java.util.List r24 = com.google.android.gms.internal.zzmq.zziq()
            r53 = r2
            com.google.android.gms.ads.internal.zzbt r2 = r0.zzamt
            java.lang.String r2 = r2.zzata
            r54 = r2
            com.google.android.gms.ads.internal.zzbt r2 = r0.zzamt
            com.google.android.gms.internal.zzom r2 = r2.zzatt
            r55 = r2
            com.google.android.gms.ads.internal.zzbt r2 = r0.zzamt
            java.lang.String r27 = r2.zzfi()
            com.google.android.gms.ads.internal.zzbs.zzec()
            float r28 = com.google.android.gms.internal.zzagr.zzdh()
            com.google.android.gms.ads.internal.zzbs.zzec()
            boolean r29 = com.google.android.gms.internal.zzagr.zzdi()
            com.google.android.gms.ads.internal.zzbs.zzec()
            com.google.android.gms.ads.internal.zzbt r2 = r0.zzamt
            android.content.Context r2 = r2.zzaif
            int r30 = com.google.android.gms.internal.zzagr.zzaq(r2)
            com.google.android.gms.ads.internal.zzbs.zzec()
            com.google.android.gms.ads.internal.zzbt r2 = r0.zzamt
            com.google.android.gms.ads.internal.zzbu r2 = r2.zzate
            int r31 = com.google.android.gms.internal.zzagr.zzr(r2)
            com.google.android.gms.ads.internal.zzbt r2 = r0.zzamt
            android.content.Context r2 = r2.zzaif
            boolean r2 = r2 instanceof android.app.Activity
            r56 = r2
            com.google.android.gms.internal.zzaez r2 = com.google.android.gms.ads.internal.zzbs.zzeg()
            boolean r33 = r2.zzpb()
            com.google.android.gms.internal.zzaez r2 = com.google.android.gms.ads.internal.zzbs.zzeg()
            boolean r36 = r2.zzpg()
            com.google.android.gms.internal.zzals r2 = com.google.android.gms.ads.internal.zzbs.zzey()
            int r37 = r2.zzsi()
            com.google.android.gms.ads.internal.zzbs.zzec()
            android.os.Bundle r38 = com.google.android.gms.internal.zzagr.zzpy()
            com.google.android.gms.internal.zzahp r2 = com.google.android.gms.ads.internal.zzbs.zzel()
            java.lang.String r39 = r2.zzqj()
            com.google.android.gms.ads.internal.zzbt r2 = r0.zzamt
            com.google.android.gms.internal.zzla r2 = r2.zzatv
            r57 = r2
            com.google.android.gms.internal.zzahp r2 = com.google.android.gms.ads.internal.zzbs.zzel()
            boolean r41 = r2.zzqk()
            com.google.android.gms.internal.zzsp r2 = com.google.android.gms.internal.zzsp.zzkw()
            android.os.Bundle r42 = r2.asBundle()
            com.google.android.gms.ads.internal.zzbs.zzeg()
            com.google.android.gms.ads.internal.zzbt r2 = r0.zzamt
            android.content.Context r2 = r2.zzaif
            r58 = r3
            com.google.android.gms.ads.internal.zzbt r3 = r0.zzamt
            java.lang.String r3 = r3.zzatb
            r59 = r4
            java.lang.String r4 = "admob"
            r60 = r15
            r15 = 0
            android.content.SharedPreferences r2 = r2.getSharedPreferences(r4, r15)
            java.util.Set r4 = java.util.Collections.emptySet()
            java.lang.String r15 = "never_pool_slots"
            java.util.Set r2 = r2.getStringSet(r15, r4)
            boolean r43 = r2.contains(r3)
            com.google.android.gms.ads.internal.zzbt r2 = r0.zzamt
            java.util.List<java.lang.Integer> r15 = r2.zzatx
            com.google.android.gms.ads.internal.zzbt r2 = r0.zzamt
            android.content.Context r2 = r2.zzaif
            com.google.android.gms.internal.zzbgb r2 = com.google.android.gms.internal.zzbgc.zzcy(r2)
            boolean r49 = r2.zzami()
            com.google.android.gms.internal.zzaez r2 = com.google.android.gms.ads.internal.zzbs.zzeg()
            boolean r50 = r2.zzph()
            r17 = r51
            r18 = r53
            r25 = r54
            r26 = r55
            r32 = r56
            r40 = r57
            r2 = r1
            r19 = r58
            r3 = r11
            r11 = r59
            r4 = r5
            r5 = r6
            r6 = r10
            r10 = r13
            r13 = r17
            r48 = r15
            r47 = r52
            r45 = r60
            r15 = r63
            r17 = r18
            r18 = r45
            r45 = r48
            r48 = r65
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzis, android.os.Bundle, com.google.android.gms.internal.zzaey, int):com.google.android.gms.internal.zzaaa");
    }

    @Nullable
    static String zzc(zzaeu zzaeu) {
        if (zzaeu == null) {
            return null;
        }
        String str = zzaeu.zzcdf;
        if (("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(str) || "com.google.ads.mediation.customevent.CustomEventAdapter".equals(str)) && zzaeu.zzcdd != null) {
            try {
                return new JSONObject(zzaeu.zzcdd.zzcbk).getString("class_name");
            } catch (NullPointerException | JSONException unused) {
            }
        }
        return str;
    }

    @Nullable
    public final String getMediationAdapterClassName() {
        if (this.zzamt.zzati == null) {
            return null;
        }
        return this.zzamt.zzati.zzcdf;
    }

    public void onAdClicked() {
        if (this.zzamt.zzati == null) {
            zzafj.zzco("Ad state was null when trying to ping click URLs.");
            return;
        }
        if (!(this.zzamt.zzati.zzcvs == null || this.zzamt.zzati.zzcvs.zzcbv == null)) {
            zzbs.zzew();
            zztv.zza(this.zzamt.zzaif, this.zzamt.zzatd.zzcp, this.zzamt.zzati, this.zzamt.zzatb, false, zzb(this.zzamt.zzati.zzcvs.zzcbv));
        }
        if (!(this.zzamt.zzati.zzcdd == null || this.zzamt.zzati.zzcdd.zzcbg == null)) {
            zzbs.zzew();
            zztv.zza(this.zzamt.zzaif, this.zzamt.zzatd.zzcp, this.zzamt.zzati, this.zzamt.zzatb, false, this.zzamt.zzati.zzcdd.zzcbg);
        }
        super.onAdClicked();
    }

    public final void onPause() {
        this.zzamv.zzj(this.zzamt.zzati);
    }

    public final void onResume() {
        this.zzamv.zzk(this.zzamt.zzati);
    }

    public void pause() {
        zzbq.zzfz("pause must be called on the main UI thread.");
        if (!(this.zzamt.zzati == null || this.zzamt.zzati.zzchj == null || !this.zzamt.zzfg())) {
            zzbs.zzee();
            zzagw.zzh(this.zzamt.zzati.zzchj);
        }
        if (!(this.zzamt.zzati == null || this.zzamt.zzati.zzcde == null)) {
            try {
                this.zzamt.zzati.zzcde.pause();
            } catch (RemoteException unused) {
                zzafj.zzco("Could not pause mediation adapter.");
            }
        }
        this.zzamv.zzj(this.zzamt.zzati);
        this.zzams.pause();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, boolean):void
     arg types: [com.google.android.gms.internal.zzaeu, int]
     candidates:
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzpu, java.lang.String):void
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaaa, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzis, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzaev, com.google.android.gms.internal.zznd):void
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzww, java.lang.String):void
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzis, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.internal.zzkb.zza(com.google.android.gms.internal.zzww, java.lang.String):void
      com.google.android.gms.internal.zzto.zza(com.google.android.gms.internal.zzpu, java.lang.String):void
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, boolean):void */
    public final void recordImpression() {
        zza(this.zzamt.zzati, false);
    }

    public void resume() {
        zzbq.zzfz("resume must be called on the main UI thread.");
        zzama zzama = (this.zzamt.zzati == null || this.zzamt.zzati.zzchj == null) ? null : this.zzamt.zzati.zzchj;
        if (zzama != null && this.zzamt.zzfg()) {
            zzbs.zzee();
            zzagw.zzi(this.zzamt.zzati.zzchj);
        }
        if (!(this.zzamt.zzati == null || this.zzamt.zzati.zzcde == null)) {
            try {
                this.zzamt.zzati.zzcde.resume();
            } catch (RemoteException unused) {
                zzafj.zzco("Could not resume mediation adapter.");
            }
        }
        if (zzama == null || !zzama.zzsw()) {
            this.zzams.resume();
        }
        this.zzamv.zzk(this.zzamt.zzati);
    }

    public void showInterstitial() {
        zzafj.zzco("showInterstitial is not supported for current ad type");
    }

    /* access modifiers changed from: protected */
    public void zza(@Nullable zzaeu zzaeu, boolean z) {
        if (zzaeu == null) {
            zzafj.zzco("Ad state was null when trying to ping impression URLs.");
            return;
        }
        if (zzaeu == null) {
            zzafj.zzco("Ad state was null when trying to ping impression URLs.");
        } else {
            zzafj.zzbw("Pinging Impression URLs.");
            if (this.zzamt.zzatk != null) {
                this.zzamt.zzatk.zzoi();
            }
            zzaeu.zzcwc.zza(zzid.zza.zzb.AD_IMPRESSION);
            if (zzaeu.zzcbw != null && !zzaeu.zzcvz) {
                zzbs.zzec();
                zzagr.zza(this.zzamt.zzaif, this.zzamt.zzatd.zzcp, zzb(zzaeu.zzcbw));
                zzaeu.zzcvz = true;
            }
        }
        if (!(zzaeu.zzcvs == null || zzaeu.zzcvs.zzcbw == null)) {
            zzbs.zzew();
            zztv.zza(this.zzamt.zzaif, this.zzamt.zzatd.zzcp, zzaeu, this.zzamt.zzatb, z, zzb(zzaeu.zzcvs.zzcbw));
        }
        if (zzaeu.zzcdd != null && zzaeu.zzcdd.zzcbh != null) {
            zzbs.zzew();
            zztv.zza(this.zzamt.zzaif, this.zzamt.zzatd.zzcp, zzaeu, this.zzamt.zzatb, z, zzaeu.zzcdd.zzcbh);
        }
    }

    public final void zza(zzpu zzpu, String str) {
        String str2;
        zzqe zzqe = null;
        if (zzpu != null) {
            try {
                str2 = zzpu.getCustomTemplateId();
            } catch (RemoteException e) {
                zzafj.zzc("Unable to call onCustomClick.", e);
                return;
            }
        } else {
            str2 = null;
        }
        if (!(this.zzamt.zzatr == null || str2 == null)) {
            zzqe = this.zzamt.zzatr.get(str2);
        }
        if (zzqe == null) {
            zzafj.zzco("Mediation adapter invoked onCustomClick but no listeners were set.");
        } else {
            zzqe.zzb(zzpu, str);
        }
    }

    public final boolean zza(zzaaa zzaaa, zznd zznd) {
        this.zzamo = zznd;
        zznd.zzf("seq_num", zzaaa.zzclr);
        zznd.zzf("request_id", zzaaa.zzcmb);
        zznd.zzf(TapjoyConstants.TJC_SESSION_ID, zzaaa.zzcls);
        if (zzaaa.zzclp != null) {
            zznd.zzf(TapjoyConstants.TJC_APP_VERSION_NAME, String.valueOf(zzaaa.zzclp.versionCode));
        }
        zzbt zzbt = this.zzamt;
        zzbs.zzdy();
        Context context = this.zzamt.zzaif;
        zzig zzig = this.zzamw.zzant;
        zzafh zzaau = zzaaa.zzclo.extras.getBundle("sdk_less_server_data") != null ? new zzaau(context, zzaaa, this, zzig) : new zzze(context, zzaaa, this, zzig);
        zzaau.zzps();
        zzbt.zzatf = zzaau;
        return true;
    }

    /* access modifiers changed from: package-private */
    public final boolean zza(zzaeu zzaeu) {
        zzis zzis;
        boolean z = false;
        if (this.zzamu != null) {
            zzis = this.zzamu;
            this.zzamu = null;
        } else {
            zzis = zzaeu.zzclo;
            if (zzis.extras != null) {
                z = zzis.extras.getBoolean("_noRefresh", false);
            }
        }
        return zza(zzis, zzaeu, z);
    }

    /* access modifiers changed from: protected */
    public boolean zza(@Nullable zzaeu zzaeu, zzaeu zzaeu2) {
        int i;
        if (!(zzaeu == null || zzaeu.zzcdg == null)) {
            zzaeu.zzcdg.zza((zzto) null);
        }
        if (zzaeu2.zzcdg != null) {
            zzaeu2.zzcdg.zza(this);
        }
        int i2 = 0;
        if (zzaeu2.zzcvs != null) {
            i2 = zzaeu2.zzcvs.zzccj;
            i = zzaeu2.zzcvs.zzcck;
        } else {
            i = 0;
        }
        this.zzamt.zzauc.zze(i2, i);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean zza(zzis zzis, zzaeu zzaeu, boolean z) {
        zzbi zzbi;
        long j;
        if (!z && this.zzamt.zzfg()) {
            if (zzaeu.zzccb > 0) {
                zzbi = this.zzams;
                j = zzaeu.zzccb;
            } else if (zzaeu.zzcvs != null && zzaeu.zzcvs.zzccb > 0) {
                zzbi = this.zzams;
                j = zzaeu.zzcvs.zzccb;
            } else if (!zzaeu.zzcng && zzaeu.errorCode == 2) {
                this.zzams.zzg(zzis);
            }
            zzbi.zza(zzis, j);
        }
        return this.zzams.zzdr();
    }

    public boolean zza(zzis zzis, zznd zznd) {
        return zza(zzis, zznd, 1);
    }

    public final boolean zza(zzis zzis, zznd zznd, int i) {
        zzaey zzaey;
        if (!zzbz()) {
            return false;
        }
        zzbs.zzec();
        zzgt zzad = zzbs.zzeg().zzad(this.zzamt.zzaif);
        String str = null;
        Bundle zza = zzad == null ? null : zzagr.zza(zzad);
        this.zzams.cancel();
        this.zzamt.zzaue = 0;
        if (((Boolean) zzbs.zzep().zzd(zzmq.zzbnq)).booleanValue()) {
            zzaey = zzbs.zzeg().zzpj();
            zzac zzej = zzbs.zzej();
            Context context = this.zzamt.zzaif;
            zzaiy zzaiy = this.zzamt.zzatd;
            String str2 = this.zzamt.zzatb;
            if (zzaey != null) {
                str = zzaey.getAppId();
            }
            zzej.zza(context, zzaiy, false, zzaey, str, str2, null);
        } else {
            zzaey = null;
        }
        return zza(zza(zzis, zza, zzaey, i), zznd);
    }

    public final void zzb(zzaeu zzaeu) {
        super.zzb(zzaeu);
        if (zzaeu.zzcdd != null) {
            zzafj.zzbw("Disable the debug gesture detector on the mediation ad frame.");
            if (this.zzamt.zzate != null) {
                this.zzamt.zzate.zzfm();
            }
            zzafj.zzbw("Pinging network fill URLs.");
            zzbs.zzew();
            zztv.zza(this.zzamt.zzaif, this.zzamt.zzatd.zzcp, zzaeu, this.zzamt.zzatb, false, zzaeu.zzcdd.zzcbj);
            if (!(zzaeu.zzcvs == null || zzaeu.zzcvs.zzcby == null || zzaeu.zzcvs.zzcby.size() <= 0)) {
                zzafj.zzbw("Pinging urls remotely");
                zzbs.zzec().zza(this.zzamt.zzaif, zzaeu.zzcvs.zzcby);
            }
        } else {
            zzafj.zzbw("Enable the debug gesture detector on the admob ad frame.");
            if (this.zzamt.zzate != null) {
                this.zzamt.zzate.zzfl();
            }
        }
        if (zzaeu.errorCode == 3 && zzaeu.zzcvs != null && zzaeu.zzcvs.zzcbx != null) {
            zzafj.zzbw("Pinging no fill URLs.");
            zzbs.zzew();
            zztv.zza(this.zzamt.zzaif, this.zzamt.zzatd.zzcp, zzaeu, this.zzamt.zzatb, false, zzaeu.zzcvs.zzcbx);
        }
    }

    /* access modifiers changed from: protected */
    public boolean zzbz() {
        zzbs.zzec();
        if (!zzagr.zzd(this.zzamt.zzaif, this.zzamt.zzaif.getPackageName(), "android.permission.INTERNET")) {
            return false;
        }
        zzbs.zzec();
        return zzagr.zzag(this.zzamt.zzaif);
    }

    public final void zzc(String str, String str2) {
        onAppEvent(str, str2);
    }

    /* access modifiers changed from: protected */
    public final boolean zzc(zzis zzis) {
        return super.zzc(zzis) && !this.zzanc;
    }

    public void zzca() {
        this.zzanc = false;
        zzbp();
        this.zzamt.zzatk.zzok();
    }

    public void zzcb() {
        this.zzanc = true;
        zzbr();
    }

    public void zzcc() {
        zzafj.zzco("Mediated ad does not support onVideoEnd callback");
    }

    public void zzcd() {
        onAdClicked();
    }

    public final void zzce() {
        zzca();
    }

    public final void zzcf() {
        zzbq();
    }

    public final void zzcg() {
        zzcb();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, boolean):void
     arg types: [com.google.android.gms.internal.zzaeu, int]
     candidates:
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzpu, java.lang.String):void
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaaa, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzis, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzaev, com.google.android.gms.internal.zznd):void
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzww, java.lang.String):void
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzis, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.internal.zzkb.zza(com.google.android.gms.internal.zzww, java.lang.String):void
      com.google.android.gms.internal.zzto.zza(com.google.android.gms.internal.zzpu, java.lang.String):void
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, boolean):void */
    public final void zzch() {
        if (this.zzamt.zzati != null) {
            String str = this.zzamt.zzati.zzcdf;
            StringBuilder sb = new StringBuilder(74 + String.valueOf(str).length());
            sb.append("Mediation adapter ");
            sb.append(str);
            sb.append(" refreshed, but mediation adapters should never refresh.");
            zzafj.zzco(sb.toString());
        }
        zza(this.zzamt.zzati, true);
        zzbs();
    }

    public void zzci() {
        recordImpression();
    }

    @Nullable
    public final String zzcj() {
        if (this.zzamt.zzati == null) {
            return null;
        }
        return zzc(this.zzamt.zzati);
    }

    public final void zzck() {
        zzbs.zzec();
        zzagr.runOnUiThread(new zzg(this));
    }

    public final void zzcl() {
        zzbs.zzec();
        zzagr.runOnUiThread(new zzh(this));
    }
}
