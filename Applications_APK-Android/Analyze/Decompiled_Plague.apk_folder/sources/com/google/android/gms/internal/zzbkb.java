package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbq;

public final class zzbkb extends zzbej {
    public static final Parcelable.Creator<zzbkb> CREATOR = new zzbkc();
    private int zzgfx;

    public zzbkb(int i) {
        zzbq.checkArgument(i == 536870912 || i == 805306368, "Cannot create a new read-only contents!");
        this.zzgfx = i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 2, this.zzgfx);
        zzbem.zzai(parcel, zze);
    }
}
