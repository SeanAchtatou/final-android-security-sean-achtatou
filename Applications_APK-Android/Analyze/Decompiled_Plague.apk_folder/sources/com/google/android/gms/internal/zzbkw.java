package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.drive.DriveApi;

final class zzbkw extends zzbjv {
    private final zzn<DriveApi.DriveContentsResult> zzfzc;

    zzbkw(zzn<DriveApi.DriveContentsResult> zzn) {
        this.zzfzc = zzn;
    }

    public final void onError(Status status) throws RemoteException {
        this.zzfzc.setResult(new zzbkx(status, null));
    }

    public final void zza(zzbps zzbps) throws RemoteException {
        this.zzfzc.setResult(new zzbkx(Status.zzfko, new zzblv(zzbps.zzglf)));
    }
}
