package com.google.android.gms.ads.internal.gmsg;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import com.google.android.gms.ads.internal.js.zza;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzagr;
import com.google.android.gms.internal.zzaik;
import com.google.android.gms.internal.zzali;
import com.google.android.gms.internal.zzalo;
import com.google.android.gms.internal.zzalp;
import com.google.android.gms.internal.zzama;
import com.google.android.gms.internal.zzamv;
import com.google.android.gms.internal.zzanl;
import com.google.android.gms.internal.zzanm;
import com.google.android.gms.internal.zzann;
import com.google.android.gms.internal.zzcs;
import com.google.android.gms.internal.zzct;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.internal.zzzb;
import com.mopub.common.Constants;
import com.tapjoy.TJAdUnitConstants;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzb
public final class zzd {
    private static zzt<Object> zzbvb = new zzj();
    public static final zzt<zzama> zzbvc = zze.zzbvu;
    public static final zzt<zzama> zzbvd = zzf.zzbvu;
    public static final zzt<zzama> zzbve = zzg.zzbvu;
    public static final zzt<zzama> zzbvf = new zzl();
    public static final zzt<zzama> zzbvg = new zzm();
    public static final zzt<zzama> zzbvh = zzh.zzbvu;
    public static final zzt<Object> zzbvi = new zzn();
    public static final zzt<zzama> zzbvj = new zzo();
    public static final zzt<zzama> zzbvk = zzi.zzbvu;
    public static final zzt<zzama> zzbvl = new zzp();
    public static final zzt<zzama> zzbvm = new zzq();
    public static final zzt<zzali> zzbvn = new zzalo();
    public static final zzt<zzali> zzbvo = new zzalp();
    public static final zzt<zzama> zzbvp = new zzc();
    public static final zzad zzbvq = new zzad();
    public static final zzt<zzama> zzbvr = new zzr();
    public static final zzt<zzama> zzbvs = new zzs();
    public static final zzt<zzama> zzbvt = new zzk();

    static final /* synthetic */ void zza(zza zza, Map map) {
        String str = (String) map.get("u");
        if (str == null) {
            zzafj.zzco("URL missing from click GMSG.");
            return;
        }
        Uri parse = Uri.parse(str);
        try {
            zzcs zzss = ((zzanl) zza).zzss();
            if (zzss != null && zzss.zzb(parse)) {
                Context context = ((zzamv) zza).getContext();
                zzann zzann = (zzann) zza;
                if (zzann == null) {
                    throw null;
                }
                parse = zzss.zza(parse, context, (View) zzann);
            }
        } catch (zzct unused) {
            String valueOf = String.valueOf(str);
            zzafj.zzco(valueOf.length() != 0 ? "Unable to append parameter to URL: ".concat(valueOf) : new String("Unable to append parameter to URL: "));
        }
        zzamv zzamv = (zzamv) zza;
        if ((((Boolean) zzbs.zzep().zzd(zzmq.zzbjg)).booleanValue() && zzbs.zzfa().zzr(zzamv.getContext())) && TextUtils.isEmpty(parse.getQueryParameter("fbs_aeid"))) {
            String zzz = zzbs.zzfa().zzz(zzamv.getContext());
            zzbs.zzec();
            parse = zzagr.zzb(parse.toString(), "fbs_aeid", zzz);
            zzbs.zzfa().zze(zzamv.getContext(), zzz);
        }
        new zzaik(zzamv.getContext(), ((zzanm) zza).zzsb().zzcp, parse.toString()).zzps();
    }

    static final /* synthetic */ void zza(zzamv zzamv, Map map) {
        String str = (String) map.get("u");
        if (str == null) {
            zzafj.zzco("URL missing from httpTrack GMSG.");
        } else {
            new zzaik(zzamv.getContext(), ((zzanm) zzamv).zzsb().zzcp, str).zzps();
        }
    }

    static final /* synthetic */ void zza(zzanl zzanl, Map map) {
        String str = (String) map.get("tx");
        String str2 = (String) map.get("ty");
        String str3 = (String) map.get("td");
        try {
            int parseInt = Integer.parseInt(str);
            int parseInt2 = Integer.parseInt(str2);
            int parseInt3 = Integer.parseInt(str3);
            zzcs zzss = zzanl.zzss();
            if (zzss != null) {
                zzss.zzad().zza(parseInt, parseInt2, parseInt3);
            }
        } catch (NumberFormatException unused) {
            zzafj.zzco("Could not parse touch parameters from gmsg.");
        }
    }

    static final /* synthetic */ void zzb(zzamv zzamv, Map map) {
        String str;
        PackageManager packageManager = zzamv.getContext().getPackageManager();
        try {
            try {
                JSONArray jSONArray = new JSONObject((String) map.get(TJAdUnitConstants.String.DATA)).getJSONArray("intents");
                JSONObject jSONObject = new JSONObject();
                for (int i = 0; i < jSONArray.length(); i++) {
                    try {
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                        String optString = jSONObject2.optString("id");
                        String optString2 = jSONObject2.optString("u");
                        String optString3 = jSONObject2.optString("i");
                        String optString4 = jSONObject2.optString("m");
                        String optString5 = jSONObject2.optString("p");
                        String optString6 = jSONObject2.optString("c");
                        jSONObject2.optString("f");
                        jSONObject2.optString("e");
                        String optString7 = jSONObject2.optString("intent_url");
                        Intent intent = null;
                        if (!TextUtils.isEmpty(optString7)) {
                            try {
                                intent = Intent.parseUri(optString7, 0);
                            } catch (URISyntaxException e) {
                                String valueOf = String.valueOf(optString7);
                                zzafj.zzb(valueOf.length() != 0 ? "Error parsing the url: ".concat(valueOf) : new String("Error parsing the url: "), e);
                            }
                        }
                        boolean z = true;
                        if (intent == null) {
                            intent = new Intent();
                            if (!TextUtils.isEmpty(optString2)) {
                                intent.setData(Uri.parse(optString2));
                            }
                            if (!TextUtils.isEmpty(optString3)) {
                                intent.setAction(optString3);
                            }
                            if (!TextUtils.isEmpty(optString4)) {
                                intent.setType(optString4);
                            }
                            if (!TextUtils.isEmpty(optString5)) {
                                intent.setPackage(optString5);
                            }
                            if (!TextUtils.isEmpty(optString6)) {
                                String[] split = optString6.split("/", 2);
                                if (split.length == 2) {
                                    intent.setComponent(new ComponentName(split[0], split[1]));
                                }
                            }
                        }
                        if (packageManager.resolveActivity(intent, 65536) == null) {
                            z = false;
                        }
                        try {
                            jSONObject.put(optString, z);
                        } catch (JSONException e2) {
                            e = e2;
                            str = "Error constructing openable urls response.";
                        }
                    } catch (JSONException e3) {
                        e = e3;
                        str = "Error parsing the intent data.";
                        zzafj.zzb(str, e);
                    }
                }
                ((zza) zzamv).zza("openableIntents", jSONObject);
            } catch (JSONException unused) {
                ((zza) zzamv).zza("openableIntents", new JSONObject());
            }
        } catch (JSONException unused2) {
            ((zza) zzamv).zza("openableIntents", new JSONObject());
        }
    }

    static final /* synthetic */ void zzc(zzamv zzamv, Map map) {
        String str = (String) map.get(Constants.VIDEO_TRACKING_URLS_KEY);
        if (TextUtils.isEmpty(str)) {
            zzafj.zzco("URLs missing in canOpenURLs GMSG.");
            return;
        }
        String[] split = str.split(",");
        HashMap hashMap = new HashMap();
        PackageManager packageManager = zzamv.getContext().getPackageManager();
        for (String str2 : split) {
            String[] split2 = str2.split(";", 2);
            boolean z = true;
            if (packageManager.resolveActivity(new Intent(split2.length > 1 ? split2[1].trim() : "android.intent.action.VIEW", Uri.parse(split2[0].trim())), 65536) == null) {
                z = false;
            }
            hashMap.put(str2, Boolean.valueOf(z));
        }
        ((zza) zzamv).zza("openableURLs", hashMap);
    }
}
