package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzbru extends zzbrs<DriveFile> {
    public zzbru(TaskCompletionSource<DriveFile> taskCompletionSource) {
        super(taskCompletionSource);
    }

    public final void zza(zzbpy zzbpy) throws RemoteException {
        zzaox().setResult(zzbpy.getDriveId().asDriveFile());
    }
}
