package com.google.android.gms.internal;

import android.content.Context;

@zzzb
public abstract class zzxb extends zzafh {
    protected final Context mContext;
    protected final Object mLock = new Object();
    protected final zzxg zzchu;
    protected final zzaev zzchv;
    protected zzaad zzchw;
    protected final Object zzchy = new Object();

    protected zzxb(Context context, zzaev zzaev, zzxg zzxg) {
        super(true);
        this.mContext = context;
        this.zzchv = zzaev;
        this.zzchw = zzaev.zzcwe;
        this.zzchu = zzxg;
    }

    public void onStop() {
    }

    /* access modifiers changed from: protected */
    public abstract void zzd(long j) throws zzxe;

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0033 A[Catch:{ zzxe -> 0x0014 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003b A[Catch:{ zzxe -> 0x0014 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzdg() {
        /*
            r5 = this;
            java.lang.Object r0 = r5.mLock
            monitor-enter(r0)
            java.lang.String r1 = "AdRendererBackgroundTask started."
            com.google.android.gms.internal.zzafj.zzbw(r1)     // Catch:{ all -> 0x0060 }
            com.google.android.gms.internal.zzaev r1 = r5.zzchv     // Catch:{ all -> 0x0060 }
            int r1 = r1.errorCode     // Catch:{ all -> 0x0060 }
            long r2 = android.os.SystemClock.elapsedRealtime()     // Catch:{ zzxe -> 0x0014 }
            r5.zzd(r2)     // Catch:{ zzxe -> 0x0014 }
            goto L_0x0050
        L_0x0014:
            r1 = move-exception
            int r2 = r1.getErrorCode()     // Catch:{ all -> 0x0060 }
            r3 = 3
            if (r2 == r3) goto L_0x0028
            r3 = -1
            if (r2 != r3) goto L_0x0020
            goto L_0x0028
        L_0x0020:
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0060 }
            com.google.android.gms.internal.zzafj.zzco(r1)     // Catch:{ all -> 0x0060 }
            goto L_0x002f
        L_0x0028:
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0060 }
            com.google.android.gms.internal.zzafj.zzcn(r1)     // Catch:{ all -> 0x0060 }
        L_0x002f:
            com.google.android.gms.internal.zzaad r1 = r5.zzchw     // Catch:{ all -> 0x0060 }
            if (r1 != 0) goto L_0x003b
            com.google.android.gms.internal.zzaad r1 = new com.google.android.gms.internal.zzaad     // Catch:{ all -> 0x0060 }
            r1.<init>(r2)     // Catch:{ all -> 0x0060 }
        L_0x0038:
            r5.zzchw = r1     // Catch:{ all -> 0x0060 }
            goto L_0x0045
        L_0x003b:
            com.google.android.gms.internal.zzaad r1 = new com.google.android.gms.internal.zzaad     // Catch:{ all -> 0x0060 }
            com.google.android.gms.internal.zzaad r3 = r5.zzchw     // Catch:{ all -> 0x0060 }
            long r3 = r3.zzccb     // Catch:{ all -> 0x0060 }
            r1.<init>(r2, r3)     // Catch:{ all -> 0x0060 }
            goto L_0x0038
        L_0x0045:
            android.os.Handler r1 = com.google.android.gms.internal.zzagr.zzczc     // Catch:{ all -> 0x0060 }
            com.google.android.gms.internal.zzxc r3 = new com.google.android.gms.internal.zzxc     // Catch:{ all -> 0x0060 }
            r3.<init>(r5)     // Catch:{ all -> 0x0060 }
            r1.post(r3)     // Catch:{ all -> 0x0060 }
            r1 = r2
        L_0x0050:
            com.google.android.gms.internal.zzaeu r1 = r5.zzy(r1)     // Catch:{ all -> 0x0060 }
            android.os.Handler r2 = com.google.android.gms.internal.zzagr.zzczc     // Catch:{ all -> 0x0060 }
            com.google.android.gms.internal.zzxd r3 = new com.google.android.gms.internal.zzxd     // Catch:{ all -> 0x0060 }
            r3.<init>(r5, r1)     // Catch:{ all -> 0x0060 }
            r2.post(r3)     // Catch:{ all -> 0x0060 }
            monitor-exit(r0)     // Catch:{ all -> 0x0060 }
            return
        L_0x0060:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0060 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzxb.zzdg():void");
    }

    /* access modifiers changed from: protected */
    public abstract zzaeu zzy(int i);
}
