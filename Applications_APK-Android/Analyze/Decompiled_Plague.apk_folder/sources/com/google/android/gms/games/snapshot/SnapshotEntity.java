package com.google.android.gms.games.snapshot;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class SnapshotEntity extends zzc implements Snapshot {
    public static final Parcelable.Creator<SnapshotEntity> CREATOR = new zzc();
    private final SnapshotMetadataEntity zzhxa;
    private final zza zzhxb;

    public SnapshotEntity(SnapshotMetadata snapshotMetadata, zza zza) {
        this.zzhxa = new SnapshotMetadataEntity(snapshotMetadata);
        this.zzhxb = zza;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof Snapshot)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Snapshot snapshot = (Snapshot) obj;
        return zzbg.equal(snapshot.getMetadata(), getMetadata()) && zzbg.equal(snapshot.getSnapshotContents(), getSnapshotContents());
    }

    public final Snapshot freeze() {
        return this;
    }

    public final SnapshotMetadata getMetadata() {
        return this.zzhxa;
    }

    public final SnapshotContents getSnapshotContents() {
        if (this.zzhxb.isClosed()) {
            return null;
        }
        return this.zzhxb;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{getMetadata(), getSnapshotContents()});
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String toString() {
        return zzbg.zzw(this).zzg("Metadata", getMetadata()).zzg("HasContents", Boolean.valueOf(getSnapshotContents() != null)).toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.snapshot.SnapshotMetadata, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.snapshot.SnapshotContents, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, (Parcelable) getMetadata(), i, false);
        zzbem.zza(parcel, 3, (Parcelable) getSnapshotContents(), i, false);
        zzbem.zzai(parcel, zze);
    }
}
