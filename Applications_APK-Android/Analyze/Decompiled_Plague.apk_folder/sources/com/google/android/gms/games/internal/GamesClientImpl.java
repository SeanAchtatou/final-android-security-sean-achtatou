package com.google.android.gms.games.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.BinderWrapper;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameBuffer;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesClientStatusCodes;
import com.google.android.gms.games.GamesMetadata;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerBuffer;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.event.EventBuffer;
import com.google.android.gms.games.event.Events;
import com.google.android.gms.games.leaderboard.Leaderboard;
import com.google.android.gms.games.leaderboard.LeaderboardBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardEntity;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardScoreEntity;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.InvitationBuffer;
import com.google.android.gms.games.multiplayer.Invitations;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.ParticipantResult;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomEntity;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.android.gms.games.multiplayer.turnbased.LoadMatchesResponse;
import com.google.android.gms.games.multiplayer.turnbased.OnTurnBasedMatchUpdateReceivedListener;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchBuffer;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;
import com.google.android.gms.games.quest.Milestone;
import com.google.android.gms.games.quest.Quest;
import com.google.android.gms.games.quest.QuestBuffer;
import com.google.android.gms.games.quest.QuestEntity;
import com.google.android.gms.games.quest.QuestUpdateListener;
import com.google.android.gms.games.quest.Quests;
import com.google.android.gms.games.request.GameRequest;
import com.google.android.gms.games.request.GameRequestBuffer;
import com.google.android.gms.games.request.OnRequestReceivedListener;
import com.google.android.gms.games.request.Requests;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotContents;
import com.google.android.gms.games.snapshot.SnapshotEntity;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.SnapshotMetadataBuffer;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.SnapshotMetadataEntity;
import com.google.android.gms.games.snapshot.Snapshots;
import com.google.android.gms.games.stats.PlayerStats;
import com.google.android.gms.games.stats.PlayerStatsBuffer;
import com.google.android.gms.games.stats.Stats;
import com.google.android.gms.games.video.CaptureState;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.games.video.Videos;
import com.google.android.gms.internal.zzcac;
import com.google.android.gms.internal.zzcae;
import com.google.android.gms.internal.zzcaf;
import com.google.android.gms.internal.zzcwl;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GamesClientImpl extends com.google.android.gms.common.internal.zzab<zzw> {
    private zzcae zzhmm = new zzd(this);
    private final String zzhmn;
    private PlayerEntity zzhmo;
    private GameEntity zzhmp;
    private final zzaa zzhmq;
    private boolean zzhmr = false;
    private final Binder zzhms;
    private final long zzhmt;
    private final Games.GamesOptions zzhmu;
    private boolean zzhmv = false;
    private Bundle zzhmw;

    static final class CaptureStreamingUrlResultImpl implements Videos.CaptureStreamingUrlResult {
        private final String zzad;

        public final Status getStatus() {
            throw new NoSuchMethodError();
        }

        public final String getUrl() {
            return this.zzad;
        }
    }

    static abstract class zza extends zzc {
        private final ArrayList<String> zzhmz = new ArrayList<>();

        zza(DataHolder dataHolder, String[] strArr) {
            super(dataHolder);
            for (String add : strArr) {
                this.zzhmz.add(add);
            }
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            zza(roomStatusUpdateListener, room, this.zzhmz);
        }

        /* access modifiers changed from: protected */
        public abstract void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList);
    }

    static final class zzaa extends zzcr implements TurnBasedMultiplayer.InitiateMatchResult {
        zzaa(DataHolder dataHolder) {
            super(dataHolder);
        }
    }

    static final class zzab extends zza {
        private final com.google.android.gms.common.api.internal.zzcl<OnInvitationReceivedListener> zzgln;

        zzab(com.google.android.gms.common.api.internal.zzcl<OnInvitationReceivedListener> zzcl) {
            this.zzgln = zzcl;
        }

        public final void onInvitationRemoved(String str) {
            this.zzgln.zza(new zzad(str));
        }

        public final void zzn(DataHolder dataHolder) {
            InvitationBuffer invitationBuffer = new InvitationBuffer(dataHolder);
            try {
                Invitation invitation = invitationBuffer.getCount() > 0 ? (Invitation) ((Invitation) invitationBuffer.get(0)).freeze() : null;
                if (invitation != null) {
                    this.zzgln.zza(new zzac(invitation));
                }
            } finally {
                invitationBuffer.release();
            }
        }
    }

    static final class zzac implements com.google.android.gms.common.api.internal.zzco<OnInvitationReceivedListener> {
        private final Invitation zzhnk;

        zzac(Invitation invitation) {
            this.zzhnk = invitation;
        }

        public final void zzahn() {
        }

        public final /* synthetic */ void zzt(Object obj) {
            ((OnInvitationReceivedListener) obj).onInvitationReceived(this.zzhnk);
        }
    }

    static final class zzad implements com.google.android.gms.common.api.internal.zzco<OnInvitationReceivedListener> {
        private final String zzdyn;

        zzad(String str) {
            this.zzdyn = str;
        }

        public final void zzahn() {
        }

        public final /* synthetic */ void zzt(Object obj) {
            ((OnInvitationReceivedListener) obj).onInvitationRemoved(this.zzdyn);
        }
    }

    static final class zzae extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Invitations.LoadInvitationsResult> zzfzc;

        zzae(com.google.android.gms.common.api.internal.zzn<Invitations.LoadInvitationsResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzm(DataHolder dataHolder) {
            this.zzfzc.setResult(new zzao(dataHolder));
        }
    }

    static final class zzaf extends zzb {
        public zzaf(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomUpdateListener roomUpdateListener, Room room, int i) {
            roomUpdateListener.onJoinedRoom(i, room);
        }
    }

    static final class zzag extends zzw implements Leaderboards.LeaderboardMetadataResult {
        private final LeaderboardBuffer zzhnl;

        zzag(DataHolder dataHolder) {
            super(dataHolder);
            this.zzhnl = new LeaderboardBuffer(dataHolder);
        }

        public final LeaderboardBuffer getLeaderboards() {
            return this.zzhnl;
        }
    }

    static final class zzah extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Leaderboards.LoadScoresResult> zzfzc;

        zzah(com.google.android.gms.common.api.internal.zzn<Leaderboards.LoadScoresResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zza(DataHolder dataHolder, DataHolder dataHolder2) {
            this.zzfzc.setResult(new zzaw(dataHolder, dataHolder2));
        }
    }

    static final class zzai extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Leaderboards.LeaderboardMetadataResult> zzfzc;

        zzai(com.google.android.gms.common.api.internal.zzn<Leaderboards.LeaderboardMetadataResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzh(DataHolder dataHolder) {
            this.zzfzc.setResult(new zzag(dataHolder));
        }
    }

    static final class zzaj extends zzcr implements TurnBasedMultiplayer.LeaveMatchResult {
        zzaj(DataHolder dataHolder) {
            super(dataHolder);
        }
    }

    static final class zzak implements com.google.android.gms.common.api.internal.zzco<RoomUpdateListener> {
        private final int zzfcq;
        private final String zzhnm;

        zzak(int i, String str) {
            this.zzfcq = i;
            this.zzhnm = str;
        }

        public final void zzahn() {
        }

        public final /* synthetic */ void zzt(Object obj) {
            ((RoomUpdateListener) obj).onLeftRoom(this.zzfcq, this.zzhnm);
        }
    }

    static final class zzal extends zzw implements Achievements.LoadAchievementsResult {
        private final AchievementBuffer zzhnn;

        zzal(DataHolder dataHolder) {
            super(dataHolder);
            this.zzhnn = new AchievementBuffer(dataHolder);
        }

        public final AchievementBuffer getAchievements() {
            return this.zzhnn;
        }
    }

    static final class zzam extends zzw implements Events.LoadEventsResult {
        private final EventBuffer zzhno;

        zzam(DataHolder dataHolder) {
            super(dataHolder);
            this.zzhno = new EventBuffer(dataHolder);
        }

        public final EventBuffer getEvents() {
            return this.zzhno;
        }
    }

    static final class zzan extends zzw implements GamesMetadata.LoadGamesResult {
        private final GameBuffer zzhnp;

        zzan(DataHolder dataHolder) {
            super(dataHolder);
            this.zzhnp = new GameBuffer(dataHolder);
        }

        public final GameBuffer getGames() {
            return this.zzhnp;
        }
    }

    static final class zzao extends zzw implements Invitations.LoadInvitationsResult {
        private final InvitationBuffer zzhnq;

        zzao(DataHolder dataHolder) {
            super(dataHolder);
            this.zzhnq = new InvitationBuffer(dataHolder);
        }

        public final InvitationBuffer getInvitations() {
            return this.zzhnq;
        }
    }

    static final class zzap extends zzcr implements TurnBasedMultiplayer.LoadMatchResult {
        zzap(DataHolder dataHolder) {
            super(dataHolder);
        }
    }

    static final class zzaq implements TurnBasedMultiplayer.LoadMatchesResult {
        private final Status mStatus;
        private final LoadMatchesResponse zzhnr;

        zzaq(Status status, Bundle bundle) {
            this.mStatus = status;
            this.zzhnr = new LoadMatchesResponse(bundle);
        }

        public final LoadMatchesResponse getMatches() {
            return this.zzhnr;
        }

        public final Status getStatus() {
            return this.mStatus;
        }

        public final void release() {
            this.zzhnr.release();
        }
    }

    static final class zzar extends zzw implements Leaderboards.LoadPlayerScoreResult {
        private final LeaderboardScoreEntity zzhns;

        zzar(DataHolder dataHolder) {
            super(dataHolder);
            LeaderboardScoreBuffer leaderboardScoreBuffer = new LeaderboardScoreBuffer(dataHolder);
            try {
                if (leaderboardScoreBuffer.getCount() > 0) {
                    this.zzhns = (LeaderboardScoreEntity) ((LeaderboardScore) leaderboardScoreBuffer.get(0)).freeze();
                } else {
                    this.zzhns = null;
                }
            } finally {
                leaderboardScoreBuffer.release();
            }
        }

        public final LeaderboardScore getScore() {
            return this.zzhns;
        }
    }

    static final class zzas extends zzw implements Stats.LoadPlayerStatsResult {
        private final PlayerStats zzhnt;

        zzas(DataHolder dataHolder) {
            super(dataHolder);
            PlayerStatsBuffer playerStatsBuffer = new PlayerStatsBuffer(dataHolder);
            try {
                if (playerStatsBuffer.getCount() > 0) {
                    this.zzhnt = new com.google.android.gms.games.stats.zza((PlayerStats) playerStatsBuffer.get(0));
                } else {
                    this.zzhnt = null;
                }
            } finally {
                playerStatsBuffer.release();
            }
        }

        public final PlayerStats getPlayerStats() {
            return this.zzhnt;
        }
    }

    static final class zzat extends zzw implements Players.LoadPlayersResult {
        private final PlayerBuffer zzhnu;

        zzat(DataHolder dataHolder) {
            super(dataHolder);
            this.zzhnu = new PlayerBuffer(dataHolder);
        }

        public final PlayerBuffer getPlayers() {
            return this.zzhnu;
        }
    }

    static final class zzau extends zzw implements Quests.LoadQuestsResult {
        private final DataHolder zzfnz;

        zzau(DataHolder dataHolder) {
            super(dataHolder);
            this.zzfnz = dataHolder;
        }

        public final QuestBuffer getQuests() {
            return new QuestBuffer(this.zzfnz);
        }
    }

    static final class zzav implements Requests.LoadRequestsResult {
        private final Status mStatus;
        private final Bundle zzhnv;

        zzav(Status status, Bundle bundle) {
            this.mStatus = status;
            this.zzhnv = bundle;
        }

        public final GameRequestBuffer getRequests(int i) {
            String str;
            switch (i) {
                case 1:
                    str = "GIFT";
                    break;
                case 2:
                    str = "WISH";
                    break;
                default:
                    StringBuilder sb = new StringBuilder(33);
                    sb.append("Unknown request type: ");
                    sb.append(i);
                    zzf.zzw("RequestType", sb.toString());
                    str = "UNKNOWN_TYPE";
                    break;
            }
            if (!this.zzhnv.containsKey(str)) {
                return null;
            }
            return new GameRequestBuffer((DataHolder) this.zzhnv.get(str));
        }

        public final Status getStatus() {
            return this.mStatus;
        }

        public final void release() {
            for (String parcelable : this.zzhnv.keySet()) {
                DataHolder dataHolder = (DataHolder) this.zzhnv.getParcelable(parcelable);
                if (dataHolder != null) {
                    dataHolder.close();
                }
            }
        }
    }

    static final class zzaw extends zzw implements Leaderboards.LoadScoresResult {
        private final LeaderboardEntity zzhnw;
        private final LeaderboardScoreBuffer zzhnx;

        /* JADX INFO: finally extract failed */
        zzaw(DataHolder dataHolder, DataHolder dataHolder2) {
            super(dataHolder2);
            LeaderboardBuffer leaderboardBuffer = new LeaderboardBuffer(dataHolder);
            try {
                if (leaderboardBuffer.getCount() > 0) {
                    this.zzhnw = (LeaderboardEntity) ((Leaderboard) leaderboardBuffer.get(0)).freeze();
                } else {
                    this.zzhnw = null;
                }
                leaderboardBuffer.release();
                this.zzhnx = new LeaderboardScoreBuffer(dataHolder2);
            } catch (Throwable th) {
                leaderboardBuffer.release();
                throw th;
            }
        }

        public final Leaderboard getLeaderboard() {
            return this.zzhnw;
        }

        public final LeaderboardScoreBuffer getScores() {
            return this.zzhnx;
        }
    }

    static final class zzax extends zzw implements Snapshots.LoadSnapshotsResult {
        zzax(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final SnapshotMetadataBuffer getSnapshots() {
            return new SnapshotMetadataBuffer(this.zzfnz);
        }
    }

    static final class zzay implements com.google.android.gms.common.api.internal.zzco<OnTurnBasedMatchUpdateReceivedListener> {
        private final String zzhny;

        zzay(String str) {
            this.zzhny = str;
        }

        public final void zzahn() {
        }

        public final /* synthetic */ void zzt(Object obj) {
            ((OnTurnBasedMatchUpdateReceivedListener) obj).onTurnBasedMatchRemoved(this.zzhny);
        }
    }

    static final class zzaz extends zza {
        private final com.google.android.gms.common.api.internal.zzcl<OnTurnBasedMatchUpdateReceivedListener> zzgln;

        zzaz(com.google.android.gms.common.api.internal.zzcl<OnTurnBasedMatchUpdateReceivedListener> zzcl) {
            this.zzgln = zzcl;
        }

        public final void onTurnBasedMatchRemoved(String str) {
            this.zzgln.zza(new zzay(str));
        }

        public final void zzt(DataHolder dataHolder) {
            TurnBasedMatchBuffer turnBasedMatchBuffer = new TurnBasedMatchBuffer(dataHolder);
            try {
                TurnBasedMatch turnBasedMatch = turnBasedMatchBuffer.getCount() > 0 ? (TurnBasedMatch) ((TurnBasedMatch) turnBasedMatchBuffer.get(0)).freeze() : null;
                if (turnBasedMatch != null) {
                    this.zzgln.zza(new zzba(turnBasedMatch));
                }
            } finally {
                turnBasedMatchBuffer.release();
            }
        }
    }

    static abstract class zzb extends com.google.android.gms.common.api.internal.zzal<RoomUpdateListener> {
        zzb(DataHolder dataHolder) {
            super(dataHolder);
        }

        /* access modifiers changed from: protected */
        public abstract void zza(RoomUpdateListener roomUpdateListener, Room room, int i);

        /* access modifiers changed from: protected */
        public final /* synthetic */ void zza(Object obj, DataHolder dataHolder) {
            zza((RoomUpdateListener) obj, GamesClientImpl.zzak(dataHolder), dataHolder.getStatusCode());
        }
    }

    static final class zzba implements com.google.android.gms.common.api.internal.zzco<OnTurnBasedMatchUpdateReceivedListener> {
        private final TurnBasedMatch zzhnz;

        zzba(TurnBasedMatch turnBasedMatch) {
            this.zzhnz = turnBasedMatch;
        }

        public final void zzahn() {
        }

        public final /* synthetic */ void zzt(Object obj) {
            ((OnTurnBasedMatchUpdateReceivedListener) obj).onTurnBasedMatchReceived(this.zzhnz);
        }
    }

    static final class zzbb implements com.google.android.gms.common.api.internal.zzco<RealTimeMessageReceivedListener> {
        private final RealTimeMessage zzhoa;

        zzbb(RealTimeMessage realTimeMessage) {
            this.zzhoa = realTimeMessage;
        }

        public final void zzahn() {
        }

        public final /* synthetic */ void zzt(Object obj) {
            ((RealTimeMessageReceivedListener) obj).onRealTimeMessageReceived(this.zzhoa);
        }
    }

    static final class zzbc extends zzw implements Snapshots.OpenSnapshotResult {
        private final Snapshot zzhob;
        private final String zzhoc;
        private final Snapshot zzhod;
        private final com.google.android.gms.drive.zzc zzhoe;
        private final SnapshotContents zzhof;

        zzbc(DataHolder dataHolder, com.google.android.gms.drive.zzc zzc) {
            this(dataHolder, null, zzc, null, null);
        }

        /* JADX INFO: finally extract failed */
        zzbc(DataHolder dataHolder, String str, com.google.android.gms.drive.zzc zzc, com.google.android.gms.drive.zzc zzc2, com.google.android.gms.drive.zzc zzc3) {
            super(dataHolder);
            SnapshotMetadataBuffer snapshotMetadataBuffer = new SnapshotMetadataBuffer(dataHolder);
            try {
                if (snapshotMetadataBuffer.getCount() == 0) {
                    this.zzhob = null;
                } else {
                    boolean z = true;
                    if (snapshotMetadataBuffer.getCount() == 1) {
                        if (dataHolder.getStatusCode() == 4004) {
                            z = false;
                        }
                        com.google.android.gms.common.internal.zzc.checkState(z);
                        this.zzhob = new SnapshotEntity(new SnapshotMetadataEntity((SnapshotMetadata) snapshotMetadataBuffer.get(0)), new com.google.android.gms.games.snapshot.zza(zzc));
                    } else {
                        this.zzhob = new SnapshotEntity(new SnapshotMetadataEntity((SnapshotMetadata) snapshotMetadataBuffer.get(0)), new com.google.android.gms.games.snapshot.zza(zzc));
                        this.zzhod = new SnapshotEntity(new SnapshotMetadataEntity((SnapshotMetadata) snapshotMetadataBuffer.get(1)), new com.google.android.gms.games.snapshot.zza(zzc2));
                        snapshotMetadataBuffer.release();
                        this.zzhoc = str;
                        this.zzhoe = zzc3;
                        this.zzhof = new com.google.android.gms.games.snapshot.zza(zzc3);
                    }
                }
                this.zzhod = null;
                snapshotMetadataBuffer.release();
                this.zzhoc = str;
                this.zzhoe = zzc3;
                this.zzhof = new com.google.android.gms.games.snapshot.zza(zzc3);
            } catch (Throwable th) {
                snapshotMetadataBuffer.release();
                throw th;
            }
        }

        public final String getConflictId() {
            return this.zzhoc;
        }

        public final Snapshot getConflictingSnapshot() {
            return this.zzhod;
        }

        public final SnapshotContents getResolutionSnapshotContents() {
            return this.zzhof;
        }

        public final Snapshot getSnapshot() {
            return this.zzhob;
        }
    }

    static final class zzbd implements com.google.android.gms.common.api.internal.zzco<RoomStatusUpdateListener> {
        private final String zzhog;

        zzbd(String str) {
            this.zzhog = str;
        }

        public final void zzahn() {
        }

        public final /* synthetic */ void zzt(Object obj) {
            ((RoomStatusUpdateListener) obj).onP2PConnected(this.zzhog);
        }
    }

    static final class zzbe implements com.google.android.gms.common.api.internal.zzco<RoomStatusUpdateListener> {
        private final String zzhog;

        zzbe(String str) {
            this.zzhog = str;
        }

        public final void zzahn() {
        }

        public final /* synthetic */ void zzt(Object obj) {
            ((RoomStatusUpdateListener) obj).onP2PDisconnected(this.zzhog);
        }
    }

    static final class zzbf extends zza {
        zzbf(DataHolder dataHolder, String[] strArr) {
            super(dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeersConnected(room, arrayList);
        }
    }

    static final class zzbg extends zza {
        zzbg(DataHolder dataHolder, String[] strArr) {
            super(dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeerDeclined(room, arrayList);
        }
    }

    static final class zzbh extends zza {
        zzbh(DataHolder dataHolder, String[] strArr) {
            super(dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeersDisconnected(room, arrayList);
        }
    }

    static final class zzbi extends zza {
        zzbi(DataHolder dataHolder, String[] strArr) {
            super(dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeerInvitedToRoom(room, arrayList);
        }
    }

    static final class zzbj extends zza {
        zzbj(DataHolder dataHolder, String[] strArr) {
            super(dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeerJoined(room, arrayList);
        }
    }

    static final class zzbk extends zza {
        zzbk(DataHolder dataHolder, String[] strArr) {
            super(dataHolder, strArr);
        }

        /* access modifiers changed from: protected */
        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            roomStatusUpdateListener.onPeerLeft(room, arrayList);
        }
    }

    static final class zzbl extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Leaderboards.LoadPlayerScoreResult> zzfzc;

        zzbl(com.google.android.gms.common.api.internal.zzn<Leaderboards.LoadPlayerScoreResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzab(DataHolder dataHolder) {
            this.zzfzc.setResult(new zzar(dataHolder));
        }
    }

    static final class zzbm extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Stats.LoadPlayerStatsResult> zzfzc;

        public zzbm(com.google.android.gms.common.api.internal.zzn<Stats.LoadPlayerStatsResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzaj(DataHolder dataHolder) {
            this.zzfzc.setResult(new zzas(dataHolder));
        }
    }

    static final class zzbn extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Players.LoadPlayersResult> zzfzc;

        zzbn(com.google.android.gms.common.api.internal.zzn<Players.LoadPlayersResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzj(DataHolder dataHolder) {
            this.zzfzc.setResult(new zzat(dataHolder));
        }

        public final void zzk(DataHolder dataHolder) {
            this.zzfzc.setResult(new zzat(dataHolder));
        }
    }

    static final class zzbo extends zzb {
        private final zzaa zzhmq;

        public zzbo(zzaa zzaa) {
            this.zzhmq = zzaa;
        }

        public final zzy zzare() {
            return new zzy(this.zzhmq.zzhpr);
        }
    }

    static final class zzbp extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Quests.AcceptQuestResult> zzhoh;

        public zzbp(com.google.android.gms.common.api.internal.zzn<Quests.AcceptQuestResult> zzn) {
            this.zzhoh = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzag(DataHolder dataHolder) {
            this.zzhoh.setResult(new zzd(dataHolder));
        }
    }

    static final class zzbq implements com.google.android.gms.common.api.internal.zzco<QuestUpdateListener> {
        private final Quest zzhna;

        zzbq(Quest quest) {
            this.zzhna = quest;
        }

        public final void zzahn() {
        }

        public final /* synthetic */ void zzt(Object obj) {
            ((QuestUpdateListener) obj).onQuestCompleted(this.zzhna);
        }
    }

    static final class zzbr extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Quests.ClaimMilestoneResult> zzhoi;
        private final String zzhoj;

        public zzbr(com.google.android.gms.common.api.internal.zzn<Quests.ClaimMilestoneResult> zzn, String str) {
            this.zzhoi = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
            this.zzhoj = (String) com.google.android.gms.common.internal.zzbq.checkNotNull(str, "MilestoneId must not be null");
        }

        public final void zzaf(DataHolder dataHolder) {
            this.zzhoi.setResult(new zzp(dataHolder, this.zzhoj));
        }
    }

    static final class zzbs extends zza {
        private final com.google.android.gms.common.api.internal.zzcl<QuestUpdateListener> zzgln;

        zzbs(com.google.android.gms.common.api.internal.zzcl<QuestUpdateListener> zzcl) {
            this.zzgln = zzcl;
        }

        private static Quest zzam(DataHolder dataHolder) {
            QuestBuffer questBuffer = new QuestBuffer(dataHolder);
            try {
                return questBuffer.getCount() > 0 ? (Quest) ((Quest) questBuffer.get(0)).freeze() : null;
            } finally {
                questBuffer.release();
            }
        }

        public final void zzah(DataHolder dataHolder) {
            Quest zzam = zzam(dataHolder);
            if (zzam != null) {
                this.zzgln.zza(new zzbq(zzam));
            }
        }
    }

    static final class zzbt extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Quests.LoadQuestsResult> zzhok;

        public zzbt(com.google.android.gms.common.api.internal.zzn<Quests.LoadQuestsResult> zzn) {
            this.zzhok = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzai(DataHolder dataHolder) {
            this.zzhok.setResult(new zzau(dataHolder));
        }
    }

    static final class zzbu implements com.google.android.gms.common.api.internal.zzco<RealTimeMultiplayer.ReliableMessageSentCallback> {
        private final int zzfcq;
        private final String zzhol;
        private final int zzhom;

        zzbu(int i, int i2, String str) {
            this.zzfcq = i;
            this.zzhom = i2;
            this.zzhol = str;
        }

        public final void zzahn() {
        }

        public final /* synthetic */ void zzt(Object obj) {
            RealTimeMultiplayer.ReliableMessageSentCallback reliableMessageSentCallback = (RealTimeMultiplayer.ReliableMessageSentCallback) obj;
            if (reliableMessageSentCallback != null) {
                reliableMessageSentCallback.onRealTimeMessageSent(this.zzfcq, this.zzhom, this.zzhol);
            }
        }
    }

    static final class zzbv extends zza {
        private com.google.android.gms.common.api.internal.zzcl<RealTimeMultiplayer.ReliableMessageSentCallback> zzhon;

        public zzbv(com.google.android.gms.common.api.internal.zzcl<RealTimeMultiplayer.ReliableMessageSentCallback> zzcl) {
            this.zzhon = zzcl;
        }

        public final void zzb(int i, int i2, String str) {
            if (this.zzhon != null) {
                this.zzhon.zza(new zzbu(i, i2, str));
            }
        }
    }

    static final class zzbw extends zza {
        private final com.google.android.gms.common.api.internal.zzcl<OnRequestReceivedListener> zzgln;

        zzbw(com.google.android.gms.common.api.internal.zzcl<OnRequestReceivedListener> zzcl) {
            this.zzgln = zzcl;
        }

        public final void onRequestRemoved(String str) {
            this.zzgln.zza(new zzby(str));
        }

        public final void zzo(DataHolder dataHolder) {
            GameRequestBuffer gameRequestBuffer = new GameRequestBuffer(dataHolder);
            try {
                GameRequest gameRequest = gameRequestBuffer.getCount() > 0 ? (GameRequest) ((GameRequest) gameRequestBuffer.get(0)).freeze() : null;
                if (gameRequest != null) {
                    this.zzgln.zza(new zzbx(gameRequest));
                }
            } finally {
                gameRequestBuffer.release();
            }
        }
    }

    static final class zzbx implements com.google.android.gms.common.api.internal.zzco<OnRequestReceivedListener> {
        private final GameRequest zzhoo;

        zzbx(GameRequest gameRequest) {
            this.zzhoo = gameRequest;
        }

        public final void zzahn() {
        }

        public final /* synthetic */ void zzt(Object obj) {
            ((OnRequestReceivedListener) obj).onRequestReceived(this.zzhoo);
        }
    }

    static final class zzby implements com.google.android.gms.common.api.internal.zzco<OnRequestReceivedListener> {
        private final String zzcqb;

        zzby(String str) {
            this.zzcqb = str;
        }

        public final void zzahn() {
        }

        public final /* synthetic */ void zzt(Object obj) {
            ((OnRequestReceivedListener) obj).onRequestRemoved(this.zzcqb);
        }
    }

    static final class zzbz extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Requests.LoadRequestsResult> zzhop;

        public zzbz(com.google.android.gms.common.api.internal.zzn<Requests.LoadRequestsResult> zzn) {
            this.zzhop = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzc(int i, Bundle bundle) {
            bundle.setClassLoader(getClass().getClassLoader());
            this.zzhop.setResult(new zzav(GamesStatusCodes.zzdg(i), bundle));
        }
    }

    static abstract class zzc extends com.google.android.gms.common.api.internal.zzal<RoomStatusUpdateListener> {
        zzc(DataHolder dataHolder) {
            super(dataHolder);
        }

        /* access modifiers changed from: protected */
        public abstract void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room);

        /* access modifiers changed from: protected */
        public final /* synthetic */ void zza(Object obj, DataHolder dataHolder) {
            zza((RoomStatusUpdateListener) obj, GamesClientImpl.zzak(dataHolder));
        }
    }

    static final class zzca extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Requests.UpdateRequestsResult> zzhoq;

        public zzca(com.google.android.gms.common.api.internal.zzn<Requests.UpdateRequestsResult> zzn) {
            this.zzhoq = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzac(DataHolder dataHolder) {
            this.zzhoq.setResult(new zzcw(dataHolder));
        }
    }

    static final class zzcb extends zzc {
        zzcb(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            roomStatusUpdateListener.onRoomAutoMatching(room);
        }
    }

    static final class zzcc extends zza {
        private final com.google.android.gms.common.api.internal.zzcl<? extends RoomUpdateListener> zzhor;
        private final com.google.android.gms.common.api.internal.zzcl<? extends RoomStatusUpdateListener> zzhos;
        private final com.google.android.gms.common.api.internal.zzcl<? extends RealTimeMessageReceivedListener> zzhot;

        public zzcc(com.google.android.gms.common.api.internal.zzcl<? extends RoomUpdateListener> zzcl) {
            this.zzhor = (com.google.android.gms.common.api.internal.zzcl) com.google.android.gms.common.internal.zzbq.checkNotNull(zzcl, "Callbacks must not be null");
            this.zzhos = null;
            this.zzhot = null;
        }

        public zzcc(com.google.android.gms.common.api.internal.zzcl<? extends RoomUpdateListener> zzcl, com.google.android.gms.common.api.internal.zzcl<? extends RoomStatusUpdateListener> zzcl2, com.google.android.gms.common.api.internal.zzcl<? extends RealTimeMessageReceivedListener> zzcl3) {
            this.zzhor = (com.google.android.gms.common.api.internal.zzcl) com.google.android.gms.common.internal.zzbq.checkNotNull(zzcl, "Callbacks must not be null");
            this.zzhos = zzcl2;
            this.zzhot = zzcl3;
        }

        public final void onLeftRoom(int i, String str) {
            this.zzhor.zza(new zzak(i, str));
        }

        public final void onP2PConnected(String str) {
            if (this.zzhos != null) {
                this.zzhos.zza(new zzbd(str));
            }
        }

        public final void onP2PDisconnected(String str) {
            if (this.zzhos != null) {
                this.zzhos.zza(new zzbe(str));
            }
        }

        public final void onRealTimeMessageReceived(RealTimeMessage realTimeMessage) {
            if (this.zzhot != null) {
                this.zzhot.zza(new zzbb(realTimeMessage));
            }
        }

        public final void zza(DataHolder dataHolder, String[] strArr) {
            if (this.zzhos != null) {
                this.zzhos.zza(new zzbi(dataHolder, strArr));
            }
        }

        public final void zzaa(DataHolder dataHolder) {
            if (this.zzhos != null) {
                this.zzhos.zza(new zzt(dataHolder));
            }
        }

        public final void zzb(DataHolder dataHolder, String[] strArr) {
            if (this.zzhos != null) {
                this.zzhos.zza(new zzbj(dataHolder, strArr));
            }
        }

        public final void zzc(DataHolder dataHolder, String[] strArr) {
            if (this.zzhos != null) {
                this.zzhos.zza(new zzbk(dataHolder, strArr));
            }
        }

        public final void zzd(DataHolder dataHolder, String[] strArr) {
            if (this.zzhos != null) {
                this.zzhos.zza(new zzbg(dataHolder, strArr));
            }
        }

        public final void zze(DataHolder dataHolder, String[] strArr) {
            if (this.zzhos != null) {
                this.zzhos.zza(new zzbf(dataHolder, strArr));
            }
        }

        public final void zzf(DataHolder dataHolder, String[] strArr) {
            if (this.zzhos != null) {
                this.zzhos.zza(new zzbh(dataHolder, strArr));
            }
        }

        public final void zzu(DataHolder dataHolder) {
            this.zzhor.zza(new zzcf(dataHolder));
        }

        public final void zzv(DataHolder dataHolder) {
            this.zzhor.zza(new zzaf(dataHolder));
        }

        public final void zzw(DataHolder dataHolder) {
            if (this.zzhos != null) {
                this.zzhos.zza(new zzce(dataHolder));
            }
        }

        public final void zzx(DataHolder dataHolder) {
            if (this.zzhos != null) {
                this.zzhos.zza(new zzcb(dataHolder));
            }
        }

        public final void zzy(DataHolder dataHolder) {
            this.zzhor.zza(new zzcd(dataHolder));
        }

        public final void zzz(DataHolder dataHolder) {
            if (this.zzhos != null) {
                this.zzhos.zza(new zzr(dataHolder));
            }
        }
    }

    static final class zzcd extends zzb {
        zzcd(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomUpdateListener roomUpdateListener, Room room, int i) {
            roomUpdateListener.onRoomConnected(i, room);
        }
    }

    static final class zzce extends zzc {
        zzce(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            roomStatusUpdateListener.onRoomConnecting(room);
        }
    }

    static final class zzcf extends zzb {
        public zzcf(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomUpdateListener roomUpdateListener, Room room, int i) {
            roomUpdateListener.onRoomCreated(i, room);
        }
    }

    static final class zzcg extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Status> zzfzc;

        public zzcg(com.google.android.gms.common.api.internal.zzn<Status> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzait() {
            this.zzfzc.setResult(GamesStatusCodes.zzdg(0));
        }
    }

    static final class zzch extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Snapshots.CommitSnapshotResult> zzhou;

        public zzch(com.google.android.gms.common.api.internal.zzn<Snapshots.CommitSnapshotResult> zzn) {
            this.zzhou = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzae(DataHolder dataHolder) {
            this.zzhou.setResult(new zzq(dataHolder));
        }
    }

    static final class zzci extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Snapshots.DeleteSnapshotResult> zzfzc;

        public zzci(com.google.android.gms.common.api.internal.zzn<Snapshots.DeleteSnapshotResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzj(int i, String str) {
            this.zzfzc.setResult(new zzs(i, str));
        }
    }

    static final class zzcj extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Snapshots.OpenSnapshotResult> zzhov;

        public zzcj(com.google.android.gms.common.api.internal.zzn<Snapshots.OpenSnapshotResult> zzn) {
            this.zzhov = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zza(DataHolder dataHolder, com.google.android.gms.drive.zzc zzc) {
            this.zzhov.setResult(new zzbc(dataHolder, zzc));
        }

        public final void zza(DataHolder dataHolder, String str, com.google.android.gms.drive.zzc zzc, com.google.android.gms.drive.zzc zzc2, com.google.android.gms.drive.zzc zzc3) {
            this.zzhov.setResult(new zzbc(dataHolder, str, zzc, zzc2, zzc3));
        }
    }

    static final class zzck extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Snapshots.LoadSnapshotsResult> zzhow;

        public zzck(com.google.android.gms.common.api.internal.zzn<Snapshots.LoadSnapshotsResult> zzn) {
            this.zzhow = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzad(DataHolder dataHolder) {
            this.zzhow.setResult(new zzax(dataHolder));
        }
    }

    static final class zzcl extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Leaderboards.SubmitScoreResult> zzfzc;

        public zzcl(com.google.android.gms.common.api.internal.zzn<Leaderboards.SubmitScoreResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzi(DataHolder dataHolder) {
            this.zzfzc.setResult(new zzcm(dataHolder));
        }
    }

    static final class zzcm extends zzw implements Leaderboards.SubmitScoreResult {
        private final ScoreSubmissionData zzhox;

        public zzcm(DataHolder dataHolder) {
            super(dataHolder);
            try {
                this.zzhox = new ScoreSubmissionData(dataHolder);
            } finally {
                dataHolder.close();
            }
        }

        public final ScoreSubmissionData getScoreData() {
            return this.zzhox;
        }
    }

    static final class zzcn extends zza {
        private final com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.CancelMatchResult> zzhoy;

        public zzcn(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.CancelMatchResult> zzn) {
            this.zzhoy = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzi(int i, String str) {
            this.zzhoy.setResult(new zzg(GamesStatusCodes.zzdg(i), str));
        }
    }

    static final class zzco extends zza {
        private final com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.InitiateMatchResult> zzhoz;

        public zzco(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.InitiateMatchResult> zzn) {
            this.zzhoz = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzq(DataHolder dataHolder) {
            this.zzhoz.setResult(new zzaa(dataHolder));
        }
    }

    static final class zzcp extends zza {
        private final com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.LeaveMatchResult> zzhpa;

        public zzcp(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.LeaveMatchResult> zzn) {
            this.zzhpa = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzs(DataHolder dataHolder) {
            this.zzhpa.setResult(new zzaj(dataHolder));
        }
    }

    static final class zzcq extends zza {
        private final com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.LoadMatchResult> zzhpb;

        public zzcq(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.LoadMatchResult> zzn) {
            this.zzhpb = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzp(DataHolder dataHolder) {
            this.zzhpb.setResult(new zzap(dataHolder));
        }
    }

    static abstract class zzcr extends zzw {
        private TurnBasedMatch zzhnz;

        zzcr(DataHolder dataHolder) {
            super(dataHolder);
            TurnBasedMatchBuffer turnBasedMatchBuffer = new TurnBasedMatchBuffer(dataHolder);
            try {
                if (turnBasedMatchBuffer.getCount() > 0) {
                    this.zzhnz = (TurnBasedMatch) ((TurnBasedMatch) turnBasedMatchBuffer.get(0)).freeze();
                } else {
                    this.zzhnz = null;
                }
            } finally {
                turnBasedMatchBuffer.release();
            }
        }

        public TurnBasedMatch getMatch() {
            return this.zzhnz;
        }
    }

    static final class zzcs extends zza {
        private final com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.UpdateMatchResult> zzhpc;

        public zzcs(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.UpdateMatchResult> zzn) {
            this.zzhpc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzr(DataHolder dataHolder) {
            this.zzhpc.setResult(new zzcv(dataHolder));
        }
    }

    static final class zzct extends zza {
        private final com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.LoadMatchesResult> zzhpd;

        public zzct(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.LoadMatchesResult> zzn) {
            this.zzhpd = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzb(int i, Bundle bundle) {
            bundle.setClassLoader(getClass().getClassLoader());
            this.zzhpd.setResult(new zzaq(GamesStatusCodes.zzdg(i), bundle));
        }
    }

    static final class zzcu implements Achievements.UpdateAchievementResult {
        private final Status mStatus;
        private final String zzhly;

        zzcu(int i, String str) {
            this.mStatus = GamesStatusCodes.zzdg(i);
            this.zzhly = str;
        }

        public final String getAchievementId() {
            return this.zzhly;
        }

        public final Status getStatus() {
            return this.mStatus;
        }
    }

    static final class zzcv extends zzcr implements TurnBasedMultiplayer.UpdateMatchResult {
        zzcv(DataHolder dataHolder) {
            super(dataHolder);
        }
    }

    static final class zzcw extends zzw implements Requests.UpdateRequestsResult {
        private final zzcaf zzhpe;

        zzcw(DataHolder dataHolder) {
            super(dataHolder);
            this.zzhpe = zzcaf.zzan(dataHolder);
        }

        public final Set<String> getRequestIds() {
            return this.zzhpe.getRequestIds();
        }

        public final int getRequestOutcome(String str) {
            return this.zzhpe.getRequestOutcome(str);
        }
    }

    static final class zzd extends zzw implements Quests.AcceptQuestResult {
        private final Quest zzhna;

        zzd(DataHolder dataHolder) {
            super(dataHolder);
            QuestBuffer questBuffer = new QuestBuffer(dataHolder);
            try {
                if (questBuffer.getCount() > 0) {
                    this.zzhna = new QuestEntity((Quest) questBuffer.get(0));
                } else {
                    this.zzhna = null;
                }
            } finally {
                questBuffer.release();
            }
        }

        public final Quest getQuest() {
            return this.zzhna;
        }
    }

    static final class zze extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Achievements.UpdateAchievementResult> zzfzc;

        zze(com.google.android.gms.common.api.internal.zzn<Achievements.UpdateAchievementResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzh(int i, String str) {
            this.zzfzc.setResult(new zzcu(i, str));
        }
    }

    static final class zzf extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Achievements.LoadAchievementsResult> zzfzc;

        zzf(com.google.android.gms.common.api.internal.zzn<Achievements.LoadAchievementsResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzf(DataHolder dataHolder) {
            this.zzfzc.setResult(new zzal(dataHolder));
        }
    }

    static final class zzg implements TurnBasedMultiplayer.CancelMatchResult {
        private final Status mStatus;
        private final String zzhnb;

        zzg(Status status, String str) {
            this.mStatus = status;
            this.zzhnb = str;
        }

        public final String getMatchId() {
            return this.zzhnb;
        }

        public final Status getStatus() {
            return this.mStatus;
        }
    }

    static final class zzh extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Videos.CaptureAvailableResult> zzfzc;

        zzh(com.google.android.gms.common.api.internal.zzn<Videos.CaptureAvailableResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzi(int i, boolean z) {
            this.zzfzc.setResult(new zzi(new Status(i), z));
        }
    }

    static final class zzi implements Videos.CaptureAvailableResult {
        private final Status mStatus;
        private final boolean zzhnc;

        zzi(Status status, boolean z) {
            this.mStatus = status;
            this.zzhnc = z;
        }

        public final Status getStatus() {
            return this.mStatus;
        }

        public final boolean isAvailable() {
            return this.zzhnc;
        }
    }

    static final class zzj extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Videos.CaptureCapabilitiesResult> zzfzc;

        zzj(com.google.android.gms.common.api.internal.zzn<Videos.CaptureCapabilitiesResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zza(int i, VideoCapabilities videoCapabilities) {
            this.zzfzc.setResult(new zzk(new Status(i), videoCapabilities));
        }
    }

    static final class zzk implements Videos.CaptureCapabilitiesResult {
        private final Status mStatus;
        private final VideoCapabilities zzhnd;

        zzk(Status status, VideoCapabilities videoCapabilities) {
            this.mStatus = status;
            this.zzhnd = videoCapabilities;
        }

        public final VideoCapabilities getCapabilities() {
            return this.zzhnd;
        }

        public final Status getStatus() {
            return this.mStatus;
        }
    }

    static final class zzl extends zza {
        private final com.google.android.gms.common.api.internal.zzcl<Videos.CaptureOverlayStateListener> zzgln;

        zzl(com.google.android.gms.common.api.internal.zzcl<Videos.CaptureOverlayStateListener> zzcl) {
            this.zzgln = (com.google.android.gms.common.api.internal.zzcl) com.google.android.gms.common.internal.zzbq.checkNotNull(zzcl, "Callback must not be null");
        }

        public final void onCaptureOverlayStateChanged(int i) {
            this.zzgln.zza(new zzm(i));
        }
    }

    static final class zzm implements com.google.android.gms.common.api.internal.zzco<Videos.CaptureOverlayStateListener> {
        private final int zzhne;

        zzm(int i) {
            this.zzhne = i;
        }

        public final void zzahn() {
        }

        public final /* synthetic */ void zzt(Object obj) {
            ((Videos.CaptureOverlayStateListener) obj).onCaptureOverlayStateChanged(this.zzhne);
        }
    }

    static final class zzn extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Videos.CaptureStateResult> zzfzc;

        public zzn(com.google.android.gms.common.api.internal.zzn<Videos.CaptureStateResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzd(int i, Bundle bundle) {
            this.zzfzc.setResult(new zzo(new Status(i), CaptureState.zzp(bundle)));
        }
    }

    static final class zzo implements Videos.CaptureStateResult {
        private final Status mStatus;
        private final CaptureState zzhnf;

        zzo(Status status, CaptureState captureState) {
            this.mStatus = status;
            this.zzhnf = captureState;
        }

        public final CaptureState getCaptureState() {
            return this.zzhnf;
        }

        public final Status getStatus() {
            return this.mStatus;
        }
    }

    static final class zzp extends zzw implements Quests.ClaimMilestoneResult {
        private final Quest zzhna;
        private final Milestone zzhng;

        zzp(DataHolder dataHolder, String str) {
            super(dataHolder);
            QuestBuffer questBuffer = new QuestBuffer(dataHolder);
            try {
                if (questBuffer.getCount() > 0) {
                    this.zzhna = new QuestEntity((Quest) questBuffer.get(0));
                    List<Milestone> zzaty = this.zzhna.zzaty();
                    int size = zzaty.size();
                    for (int i = 0; i < size; i++) {
                        if (zzaty.get(i).getMilestoneId().equals(str)) {
                            this.zzhng = zzaty.get(i);
                            return;
                        }
                    }
                    this.zzhng = null;
                } else {
                    this.zzhng = null;
                    this.zzhna = null;
                }
                questBuffer.release();
            } finally {
                questBuffer.release();
            }
        }

        public final Milestone getMilestone() {
            return this.zzhng;
        }

        public final Quest getQuest() {
            return this.zzhna;
        }
    }

    static final class zzq extends zzw implements Snapshots.CommitSnapshotResult {
        private final SnapshotMetadata zzhnh;

        zzq(DataHolder dataHolder) {
            super(dataHolder);
            SnapshotMetadataBuffer snapshotMetadataBuffer = new SnapshotMetadataBuffer(dataHolder);
            try {
                if (snapshotMetadataBuffer.getCount() > 0) {
                    this.zzhnh = new SnapshotMetadataEntity((SnapshotMetadata) snapshotMetadataBuffer.get(0));
                } else {
                    this.zzhnh = null;
                }
            } finally {
                snapshotMetadataBuffer.release();
            }
        }

        public final SnapshotMetadata getSnapshotMetadata() {
            return this.zzhnh;
        }
    }

    static final class zzr extends zzc {
        zzr(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            roomStatusUpdateListener.onConnectedToRoom(room);
        }
    }

    static final class zzs implements Snapshots.DeleteSnapshotResult {
        private final Status mStatus;
        private final String zzhni;

        zzs(int i, String str) {
            this.mStatus = GamesStatusCodes.zzdg(i);
            this.zzhni = str;
        }

        public final String getSnapshotId() {
            return this.zzhni;
        }

        public final Status getStatus() {
            return this.mStatus;
        }
    }

    static final class zzt extends zzc {
        zzt(DataHolder dataHolder) {
            super(dataHolder);
        }

        public final void zza(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            roomStatusUpdateListener.onDisconnectedFromRoom(room);
        }
    }

    static final class zzu extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Events.LoadEventsResult> zzfzc;

        zzu(com.google.android.gms.common.api.internal.zzn<Events.LoadEventsResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzg(DataHolder dataHolder) {
            this.zzfzc.setResult(new zzam(dataHolder));
        }
    }

    class zzv extends zzcac {
        public zzv() {
            super(GamesClientImpl.this.getContext().getMainLooper(), 1000);
        }

        /* access modifiers changed from: protected */
        public final void zzu(String str, int i) {
            try {
                if (GamesClientImpl.this.isConnected()) {
                    ((zzw) GamesClientImpl.this.zzakb()).zzp(str, i);
                    return;
                }
                StringBuilder sb = new StringBuilder(89 + String.valueOf(str).length());
                sb.append("Unable to increment event ");
                sb.append(str);
                sb.append(" by ");
                sb.append(i);
                sb.append(" because the games client is no longer connected");
                zzf.zzw("GamesClientImpl", sb.toString());
            } catch (RemoteException e) {
                GamesClientImpl.zzd(e);
            } catch (SecurityException e2) {
                GamesClientImpl.zza(e2);
            }
        }
    }

    static abstract class zzw extends com.google.android.gms.common.api.internal.zzam {
        protected zzw(DataHolder dataHolder) {
            super(dataHolder, GamesStatusCodes.zzdg(dataHolder.getStatusCode()));
        }
    }

    static final class zzx extends zza {
        private final com.google.android.gms.common.api.internal.zzn<GamesMetadata.LoadGamesResult> zzfzc;

        zzx(com.google.android.gms.common.api.internal.zzn<GamesMetadata.LoadGamesResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzl(DataHolder dataHolder) {
            this.zzfzc.setResult(new zzan(dataHolder));
        }
    }

    static final class zzy extends zza {
        private final com.google.android.gms.common.api.internal.zzn<Games.GetServerAuthCodeResult> zzfzc;

        public zzy(com.google.android.gms.common.api.internal.zzn<Games.GetServerAuthCodeResult> zzn) {
            this.zzfzc = (com.google.android.gms.common.api.internal.zzn) com.google.android.gms.common.internal.zzbq.checkNotNull(zzn, "Holder must not be null");
        }

        public final void zzg(int i, String str) {
            this.zzfzc.setResult(new zzz(GamesStatusCodes.zzdg(i), str));
        }
    }

    static final class zzz implements Games.GetServerAuthCodeResult {
        private final Status mStatus;
        private final String zzhnj;

        zzz(Status status, String str) {
            this.mStatus = status;
            this.zzhnj = str;
        }

        public final String getCode() {
            return this.zzhnj;
        }

        public final Status getStatus() {
            return this.mStatus;
        }
    }

    public GamesClientImpl(Context context, Looper looper, com.google.android.gms.common.internal.zzr zzr2, Games.GamesOptions gamesOptions, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 1, zzr2, connectionCallbacks, onConnectionFailedListener);
        this.zzhmn = zzr2.zzakm();
        this.zzhms = new Binder();
        this.zzhmq = new zzad(this, zzr2.zzaki());
        this.zzhmt = (long) hashCode();
        this.zzhmu = gamesOptions;
        if (this.zzhmu.zzhic) {
            return;
        }
        if (zzr2.zzako() != null || (context instanceof Activity)) {
            zzu(zzr2.zzako());
        }
    }

    private static <R> void zza(com.google.android.gms.common.api.internal.zzn<R> zzn2, SecurityException securityException) {
        if (zzn2 != null) {
            zzn2.zzu(GamesClientStatusCodes.zzdg(4));
        }
    }

    /* access modifiers changed from: private */
    public static void zza(SecurityException securityException) {
        zzf.zzd("GamesClientImpl", "Is player signed out?", securityException);
    }

    /* access modifiers changed from: private */
    public static Room zzak(DataHolder dataHolder) {
        com.google.android.gms.games.multiplayer.realtime.zzb zzb2 = new com.google.android.gms.games.multiplayer.realtime.zzb(dataHolder);
        try {
            return zzb2.getCount() > 0 ? (Room) ((Room) zzb2.get(0)).freeze() : null;
        } finally {
            zzb2.release();
        }
    }

    /* access modifiers changed from: private */
    public static void zzd(RemoteException remoteException) {
        zzf.zzc("GamesClientImpl", "service died", remoteException);
    }

    public final void disconnect() {
        this.zzhmr = false;
        if (isConnected()) {
            try {
                zzw zzw2 = (zzw) zzakb();
                zzw2.zzass();
                this.zzhmm.flush();
                zzw2.zzab(this.zzhmt);
            } catch (RemoteException unused) {
                zzf.zzv("GamesClientImpl", "Failed to notify client disconnect.");
            }
        }
        super.disconnect();
    }

    public final String getAppId() throws RemoteException {
        return ((zzw) zzakb()).getAppId();
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        super.onConnectionFailed(connectionResult);
        this.zzhmr = false;
    }

    public final int zza(com.google.android.gms.common.api.internal.zzcl<RealTimeMultiplayer.ReliableMessageSentCallback> zzcl2, byte[] bArr, String str, String str2) throws RemoteException {
        return ((zzw) zzakb()).zza(new zzbv(zzcl2), bArr, str, str2);
    }

    public final int zza(byte[] bArr, String str, String[] strArr) {
        com.google.android.gms.common.internal.zzbq.checkNotNull(strArr, "Participant IDs must not be null");
        try {
            com.google.android.gms.common.internal.zzbq.checkNotNull(strArr, "Participant IDs must not be null");
            return ((zzw) zzakb()).zzb(bArr, str, strArr);
        } catch (RemoteException e) {
            zzd(e);
            return -1;
        }
    }

    public final Intent zza(int i, byte[] bArr, int i2, Bitmap bitmap, String str) {
        try {
            Intent zza2 = ((zzw) zzakb()).zza(i, bArr, i2, str);
            com.google.android.gms.common.internal.zzbq.checkNotNull(bitmap, "Must provide a non null icon");
            zza2.putExtra("com.google.android.gms.games.REQUEST_ITEM_ICON", bitmap);
            return zza2;
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final Intent zza(PlayerEntity playerEntity) throws RemoteException {
        return ((zzw) zzakb()).zza(playerEntity);
    }

    public final Intent zza(Room room, int i) throws RemoteException {
        return ((zzw) zzakb()).zza((RoomEntity) room.freeze(), i);
    }

    public final Intent zza(String str, boolean z, boolean z2, int i) throws RemoteException {
        return ((zzw) zzakb()).zza(str, z, z2, i);
    }

    /* access modifiers changed from: protected */
    public final void zza(int i, IBinder iBinder, Bundle bundle, int i2) {
        if (i == 0 && bundle != null) {
            bundle.setClassLoader(GamesClientImpl.class.getClassLoader());
            this.zzhmr = bundle.getBoolean("show_welcome_popup");
            this.zzhmv = this.zzhmr;
            this.zzhmo = (PlayerEntity) bundle.getParcelable("com.google.android.gms.games.current_player");
            this.zzhmp = (GameEntity) bundle.getParcelable("com.google.android.gms.games.current_game");
        }
        super.zza(i, iBinder, bundle, i2);
    }

    public final void zza(IBinder iBinder, Bundle bundle) {
        if (isConnected()) {
            try {
                ((zzw) zzakb()).zza(iBinder, bundle);
            } catch (RemoteException e) {
                zzd(e);
            }
        }
    }

    public final /* synthetic */ void zza(@NonNull IInterface iInterface) {
        zzw zzw2 = (zzw) iInterface;
        super.zza(zzw2);
        if (this.zzhmr) {
            this.zzhmq.zzasz();
            this.zzhmr = false;
        }
        if (!this.zzhmu.zzhhu && !this.zzhmu.zzhic) {
            try {
                zzw2.zza(new zzbo(this.zzhmq), this.zzhmt);
            } catch (RemoteException e) {
                zzd(e);
            }
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzcl<? extends RoomUpdateListener> zzcl2, com.google.android.gms.common.api.internal.zzcl<? extends RoomStatusUpdateListener> zzcl3, com.google.android.gms.common.api.internal.zzcl<? extends RealTimeMessageReceivedListener> zzcl4, RoomConfig roomConfig) throws RemoteException {
        ((zzw) zzakb()).zza(new zzcc(zzcl2, zzcl3, zzcl4), this.zzhms, roomConfig.getVariant(), roomConfig.getInvitedPlayerIds(), roomConfig.getAutoMatchCriteria(), false, this.zzhmt);
    }

    public final void zza(com.google.android.gms.common.api.internal.zzcl<? extends RoomUpdateListener> zzcl2, String str) {
        try {
            ((zzw) zzakb()).zza(new zzcc(zzcl2), str);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, int):void
     arg types: [com.google.android.gms.games.internal.GamesClientImpl$zzae, int]
     candidates:
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.multiplayer.realtime.RoomEntity, int):android.content.Intent
      com.google.android.gms.games.internal.zzw.zza(android.os.IBinder, android.os.Bundle):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, long):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, boolean):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String[]):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzu, long):void
      com.google.android.gms.games.internal.zzw.zza(java.lang.String, com.google.android.gms.games.internal.zzs):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, int):void */
    public final void zza(com.google.android.gms.common.api.internal.zzn<Invitations.LoadInvitationsResult> zzn2, int i) throws RemoteException {
        try {
            ((zzw) zzakb()).zza((zzs) new zzae(zzn2), i);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<Requests.LoadRequestsResult> zzn2, int i, int i2, int i3) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(new zzbz(zzn2), i, i2, i3);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, int, boolean, boolean):void
     arg types: [com.google.android.gms.games.internal.GamesClientImpl$zzbn, int, boolean, boolean]
     candidates:
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, byte[], java.lang.String, java.lang.String):int
      com.google.android.gms.games.internal.zzw.zza(int, byte[], int, java.lang.String):android.content.Intent
      com.google.android.gms.games.internal.zzw.zza(java.lang.String, boolean, boolean, int):android.content.Intent
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, int, int, int):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, android.os.Bundle, int, int):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String, long, java.lang.String):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String, android.os.IBinder, android.os.Bundle):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String, com.google.android.gms.games.snapshot.zze, com.google.android.gms.drive.zzc):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String, boolean, int):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String, byte[], com.google.android.gms.games.multiplayer.ParticipantResult[]):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, int[], int, boolean):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, int, boolean, boolean):void */
    public final void zza(com.google.android.gms.common.api.internal.zzn<Players.LoadPlayersResult> zzn2, int i, boolean z, boolean z2) throws RemoteException {
        try {
            ((zzw) zzakb()).zza((zzs) new zzbn(zzn2), i, z, z2);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.LoadMatchesResult> zzn2, int i, int[] iArr) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(new zzct(zzn2), i, iArr);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<Leaderboards.LoadScoresResult> zzn2, LeaderboardScoreBuffer leaderboardScoreBuffer, int i, int i2) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(new zzah(zzn2), leaderboardScoreBuffer.zzatr().asBundle(), i, i2);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.InitiateMatchResult> zzn2, TurnBasedMatchConfig turnBasedMatchConfig) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(new zzco(zzn2), turnBasedMatchConfig.getVariant(), turnBasedMatchConfig.zzatx(), turnBasedMatchConfig.getInvitedPlayerIds(), turnBasedMatchConfig.getAutoMatchCriteria());
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final void zza(com.google.android.gms.common.api.internal.zzn<Snapshots.CommitSnapshotResult> zzn2, Snapshot snapshot, SnapshotMetadataChange snapshotMetadataChange) throws RemoteException {
        SnapshotContents snapshotContents = snapshot.getSnapshotContents();
        com.google.android.gms.common.internal.zzbq.zza(!snapshotContents.isClosed(), (Object) "Snapshot already closed");
        BitmapTeleporter zzaua = snapshotMetadataChange.zzaua();
        if (zzaua != null) {
            zzaua.setTempDir(getContext().getCacheDir());
        }
        com.google.android.gms.drive.zzc zzanp = snapshotContents.zzanp();
        snapshotContents.close();
        try {
            ((zzw) zzakb()).zza(new zzch(zzn2), snapshot.getMetadata().getSnapshotId(), (com.google.android.gms.games.snapshot.zze) snapshotMetadataChange, zzanp);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<Achievements.UpdateAchievementResult> zzn2, String str) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(zzn2 == null ? null : new zze(zzn2), str, this.zzhmq.zzhpr.zzhps, this.zzhmq.zzhpr.zzata());
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<Achievements.UpdateAchievementResult> zzn2, String str, int i) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(zzn2 == null ? null : new zze(zzn2), str, i, this.zzhmq.zzhpr.zzhps, this.zzhmq.zzhpr.zzata());
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<Leaderboards.LoadScoresResult> zzn2, String str, int i, int i2, int i3, boolean z) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(new zzah(zzn2), str, i, i2, i3, z);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<Players.LoadPlayersResult> zzn2, String str, int i, boolean z, boolean z2) throws RemoteException {
        if (((str.hashCode() == 156408498 && str.equals("played_with")) ? (char) 0 : 65535) != 0) {
            String valueOf = String.valueOf(str);
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Invalid player collection: ".concat(valueOf) : new String("Invalid player collection: "));
        }
        try {
            ((zzw) zzakb()).zza(new zzbn(zzn2), str, i, z, z2);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<Leaderboards.SubmitScoreResult> zzn2, String str, long j, String str2) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(zzn2 == null ? null : new zzcl(zzn2), str, j, str2);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.LeaveMatchResult> zzn2, String str, String str2) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(new zzcp(zzn2), str, str2);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<Leaderboards.LoadPlayerScoreResult> zzn2, String str, String str2, int i, int i2) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(new zzbl(zzn2), (String) null, str2, i, i2);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final void zza(com.google.android.gms.common.api.internal.zzn<Snapshots.OpenSnapshotResult> zzn2, String str, String str2, SnapshotMetadataChange snapshotMetadataChange, SnapshotContents snapshotContents) throws RemoteException {
        com.google.android.gms.common.internal.zzbq.zza(!snapshotContents.isClosed(), (Object) "SnapshotContents already closed");
        BitmapTeleporter zzaua = snapshotMetadataChange.zzaua();
        if (zzaua != null) {
            zzaua.setTempDir(getContext().getCacheDir());
        }
        com.google.android.gms.drive.zzc zzanp = snapshotContents.zzanp();
        snapshotContents.close();
        try {
            ((zzw) zzakb()).zza(new zzcj(zzn2), str, str2, (com.google.android.gms.games.snapshot.zze) snapshotMetadataChange, zzanp);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<Players.LoadPlayersResult> zzn2, String str, boolean z) throws RemoteException {
        try {
            ((zzw) zzakb()).zzb(new zzbn(zzn2), str, z);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<Snapshots.OpenSnapshotResult> zzn2, String str, boolean z, int i) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(new zzcj(zzn2), str, z, i);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.UpdateMatchResult> zzn2, String str, byte[] bArr, String str2, ParticipantResult[] participantResultArr) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(new zzcs(zzn2), str, bArr, str2, participantResultArr);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.UpdateMatchResult> zzn2, String str, byte[] bArr, ParticipantResult[] participantResultArr) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(new zzcs(zzn2), str, bArr, participantResultArr);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzw.zzc(com.google.android.gms.games.internal.zzs, boolean):void
     arg types: [com.google.android.gms.games.internal.GamesClientImpl$zzbn, boolean]
     candidates:
      com.google.android.gms.games.internal.zzw.zzc(com.google.android.gms.games.internal.zzs, long):void
      com.google.android.gms.games.internal.zzw.zzc(com.google.android.gms.games.internal.zzs, java.lang.String):void
      com.google.android.gms.games.internal.zzw.zzc(com.google.android.gms.games.internal.zzs, boolean):void */
    public final void zza(com.google.android.gms.common.api.internal.zzn<Players.LoadPlayersResult> zzn2, boolean z) throws RemoteException {
        try {
            ((zzw) zzakb()).zzc((zzs) new zzbn(zzn2), z);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<Events.LoadEventsResult> zzn2, boolean z, String... strArr) throws RemoteException {
        this.zzhmm.flush();
        try {
            ((zzw) zzakb()).zza(new zzu(zzn2), z, strArr);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<Quests.LoadQuestsResult> zzn2, int[] iArr, int i, boolean z) throws RemoteException {
        this.zzhmm.flush();
        try {
            ((zzw) zzakb()).zza(new zzbt(zzn2), iArr, i, z);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.api.internal.zzn<Requests.UpdateRequestsResult> zzn2, String[] strArr) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(new zzca(zzn2), strArr);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zza(com.google.android.gms.common.internal.zzj zzj2) {
        this.zzhmo = null;
        this.zzhmp = null;
        super.zza(zzj2);
    }

    public final void zza(@NonNull com.google.android.gms.common.internal.zzp zzp2) {
        try {
            zzg(new zze(this, zzp2));
        } catch (RemoteException unused) {
            zzp2.zzait();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final void zza(Snapshot snapshot) throws RemoteException {
        SnapshotContents snapshotContents = snapshot.getSnapshotContents();
        com.google.android.gms.common.internal.zzbq.zza(!snapshotContents.isClosed(), (Object) "Snapshot already closed");
        com.google.android.gms.drive.zzc zzanp = snapshotContents.zzanp();
        snapshotContents.close();
        ((zzw) zzakb()).zza(zzanp);
    }

    /* access modifiers changed from: protected */
    public final Bundle zzaae() {
        String locale = getContext().getResources().getConfiguration().locale.toString();
        Bundle zzaqw = this.zzhmu.zzaqw();
        zzaqw.putString("com.google.android.gms.games.key.gamePackageName", this.zzhmn);
        zzaqw.putString("com.google.android.gms.games.key.desiredLocale", locale);
        zzaqw.putParcelable("com.google.android.gms.games.key.popupWindowToken", new BinderWrapper(this.zzhmq.zzhpr.zzhps));
        zzaqw.putInt("com.google.android.gms.games.key.API_VERSION", 6);
        zzaqw.putBundle("com.google.android.gms.games.key.signInOptions", zzcwl.zza(zzakv()));
        return zzaqw;
    }

    public final boolean zzaam() {
        return true;
    }

    public final Bundle zzaew() {
        try {
            Bundle zzaew = ((zzw) zzakb()).zzaew();
            if (zzaew != null) {
                zzaew.setClassLoader(GamesClientImpl.class.getClassLoader());
                this.zzhmw = zzaew;
            }
            return zzaew;
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    @Nullable
    public final Bundle zzarf() {
        Bundle zzaew = zzaew();
        if (zzaew == null) {
            zzaew = this.zzhmw;
        }
        this.zzhmw = null;
        return zzaew;
    }

    public final String zzarg() throws RemoteException {
        return ((zzw) zzakb()).zzarg();
    }

    public final String zzarh() {
        try {
            return zzarg();
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    /* JADX INFO: finally extract failed */
    public final Player zzari() throws RemoteException {
        zzaka();
        synchronized (this) {
            if (this.zzhmo == null) {
                PlayerBuffer playerBuffer = new PlayerBuffer(((zzw) zzakb()).zzasv());
                try {
                    if (playerBuffer.getCount() > 0) {
                        this.zzhmo = (PlayerEntity) ((Player) playerBuffer.get(0)).freeze();
                    }
                    playerBuffer.release();
                } catch (Throwable th) {
                    playerBuffer.release();
                    throw th;
                }
            }
        }
        return this.zzhmo;
    }

    public final Player zzarj() {
        try {
            return zzari();
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    /* JADX INFO: finally extract failed */
    public final Game zzark() throws RemoteException {
        zzaka();
        synchronized (this) {
            if (this.zzhmp == null) {
                GameBuffer gameBuffer = new GameBuffer(((zzw) zzakb()).zzasw());
                try {
                    if (gameBuffer.getCount() > 0) {
                        this.zzhmp = (GameEntity) ((Game) gameBuffer.get(0)).freeze();
                    }
                    gameBuffer.release();
                } catch (Throwable th) {
                    gameBuffer.release();
                    throw th;
                }
            }
        }
        return this.zzhmp;
    }

    public final Game zzarl() {
        try {
            return zzark();
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final Intent zzarm() throws RemoteException {
        return ((zzw) zzakb()).zzarm();
    }

    public final Intent zzarn() {
        try {
            return zzarm();
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final Intent zzaro() {
        try {
            return ((zzw) zzakb()).zzaro();
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final Intent zzarp() {
        try {
            return ((zzw) zzakb()).zzarp();
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final Intent zzarq() {
        try {
            return ((zzw) zzakb()).zzarq();
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final void zzarr() throws RemoteException {
        ((zzw) zzakb()).zzac(this.zzhmt);
    }

    public final void zzars() {
        try {
            zzarr();
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzart() throws RemoteException {
        ((zzw) zzakb()).zzad(this.zzhmt);
    }

    public final void zzaru() {
        try {
            zzart();
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzarv() {
        try {
            ((zzw) zzakb()).zzaf(this.zzhmt);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzarw() {
        try {
            ((zzw) zzakb()).zzae(this.zzhmt);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final Intent zzarx() {
        try {
            return ((zzw) zzakb()).zzasx();
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final Intent zzary() throws RemoteException {
        return ((zzw) zzakb()).zzary();
    }

    public final Intent zzarz() {
        try {
            return zzary();
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final int zzasa() throws RemoteException {
        return ((zzw) zzakb()).zzasa();
    }

    public final int zzasb() {
        try {
            return zzasa();
        } catch (RemoteException e) {
            zzd(e);
            return 4368;
        }
    }

    public final String zzasc() {
        try {
            return getAppId();
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final int zzasd() throws RemoteException {
        return ((zzw) zzakb()).zzasd();
    }

    public final int zzase() {
        try {
            return zzasd();
        } catch (RemoteException e) {
            zzd(e);
            return -1;
        }
    }

    public final Intent zzasf() {
        try {
            return ((zzw) zzakb()).zzasf();
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final int zzasg() {
        try {
            return ((zzw) zzakb()).zzasg();
        } catch (RemoteException e) {
            zzd(e);
            return -1;
        }
    }

    public final int zzash() {
        try {
            return ((zzw) zzakb()).zzash();
        } catch (RemoteException e) {
            zzd(e);
            return -1;
        }
    }

    public final int zzasi() throws RemoteException {
        return ((zzw) zzakb()).zzasi();
    }

    public final int zzasj() {
        try {
            return zzasi();
        } catch (RemoteException e) {
            zzd(e);
            return -1;
        }
    }

    public final int zzask() throws RemoteException {
        return ((zzw) zzakb()).zzask();
    }

    public final int zzasl() {
        try {
            return zzask();
        } catch (RemoteException e) {
            zzd(e);
            return -1;
        }
    }

    public final Intent zzasm() throws RemoteException {
        return ((zzw) zzakb()).zzasy();
    }

    public final Intent zzasn() {
        try {
            return zzasm();
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final boolean zzaso() throws RemoteException {
        return ((zzw) zzakb()).zzaso();
    }

    public final boolean zzasp() {
        try {
            return zzaso();
        } catch (RemoteException e) {
            zzd(e);
            return false;
        }
    }

    public final void zzasq() throws RemoteException {
        ((zzw) zzakb()).zzag(this.zzhmt);
    }

    public final void zzasr() {
        try {
            zzasq();
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzass() {
        if (isConnected()) {
            try {
                ((zzw) zzakb()).zzass();
            } catch (RemoteException e) {
                zzd(e);
            }
        }
    }

    public final int zzb(com.google.android.gms.common.api.internal.zzcl<RealTimeMultiplayer.ReliableMessageSentCallback> zzcl2, byte[] bArr, String str, String str2) {
        try {
            return zza(zzcl2, bArr, str, str2);
        } catch (RemoteException e) {
            zzd(e);
            return -1;
        }
    }

    public final Intent zzb(int i, int i2, boolean z) throws RemoteException {
        return ((zzw) zzakb()).zzb(i, i2, z);
    }

    public final Intent zzb(PlayerEntity playerEntity) {
        try {
            return zza(playerEntity);
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final Intent zzb(Room room, int i) {
        try {
            return zza(room, i);
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final Intent zzb(String str, boolean z, boolean z2, int i) {
        try {
            return zza(str, z, z2, i);
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final Set<Scope> zzb(Set<Scope> set) {
        HashSet hashSet = new HashSet(set);
        boolean contains = set.contains(Games.SCOPE_GAMES);
        boolean contains2 = set.contains(Games.SCOPE_GAMES_LITE);
        if (set.contains(Games.zzhho)) {
            com.google.android.gms.common.internal.zzbq.zza(!contains, "Cannot have both %s and %s!", Scopes.GAMES, "https://www.googleapis.com/auth/games.firstparty");
            return hashSet;
        }
        com.google.android.gms.common.internal.zzbq.zza(contains || contains2, "Games APIs requires %s function.", "https://www.googleapis.com/auth/games_lite");
        if (contains2 && contains) {
            hashSet.remove(Games.SCOPE_GAMES_LITE);
        }
        return hashSet;
    }

    public final void zzb(com.google.android.gms.common.api.internal.zzcl<? extends RoomUpdateListener> zzcl2, com.google.android.gms.common.api.internal.zzcl<? extends RoomStatusUpdateListener> zzcl3, com.google.android.gms.common.api.internal.zzcl<? extends RealTimeMessageReceivedListener> zzcl4, RoomConfig roomConfig) {
        try {
            zza(zzcl2, zzcl3, zzcl4, roomConfig);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzw.zzb(com.google.android.gms.games.internal.zzs, int):void
     arg types: [com.google.android.gms.games.internal.GamesClientImpl$zzh, int]
     candidates:
      com.google.android.gms.games.internal.zzw.zzb(com.google.android.gms.games.internal.zzs, long):void
      com.google.android.gms.games.internal.zzw.zzb(com.google.android.gms.games.internal.zzs, java.lang.String):void
      com.google.android.gms.games.internal.zzw.zzb(com.google.android.gms.games.internal.zzs, boolean):void
      com.google.android.gms.games.internal.zzw.zzb(com.google.android.gms.games.internal.zzs, java.lang.String[]):void
      com.google.android.gms.games.internal.zzw.zzb(com.google.android.gms.games.internal.zzs, int):void */
    public final void zzb(com.google.android.gms.common.api.internal.zzn<Videos.CaptureAvailableResult> zzn2, int i) throws RemoteException {
        try {
            ((zzw) zzakb()).zzb((zzs) new zzh(zzn2), i);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzb(com.google.android.gms.common.api.internal.zzn<Achievements.UpdateAchievementResult> zzn2, String str) throws RemoteException {
        try {
            ((zzw) zzakb()).zzb(zzn2 == null ? null : new zze(zzn2), str, this.zzhmq.zzhpr.zzhps, this.zzhmq.zzhpr.zzata());
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzb(com.google.android.gms.common.api.internal.zzn<Achievements.UpdateAchievementResult> zzn2, String str, int i) throws RemoteException {
        try {
            ((zzw) zzakb()).zzb(zzn2 == null ? null : new zze(zzn2), str, i, this.zzhmq.zzhpr.zzhps, this.zzhmq.zzhpr.zzata());
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzb(com.google.android.gms.common.api.internal.zzn<Leaderboards.LoadScoresResult> zzn2, String str, int i, int i2, int i3, boolean z) throws RemoteException {
        try {
            ((zzw) zzakb()).zzb(new zzah(zzn2), str, i, i2, i3, z);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzb(com.google.android.gms.common.api.internal.zzn<Quests.ClaimMilestoneResult> zzn2, String str, String str2) throws RemoteException {
        this.zzhmm.flush();
        try {
            ((zzw) zzakb()).zzb(new zzbr(zzn2, str2), str, str2);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzb(com.google.android.gms.common.api.internal.zzn<Leaderboards.LeaderboardMetadataResult> zzn2, String str, boolean z) throws RemoteException {
        try {
            ((zzw) zzakb()).zza(new zzai(zzn2), str, z);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzw.zzb(com.google.android.gms.games.internal.zzs, boolean):void
     arg types: [com.google.android.gms.games.internal.GamesClientImpl$zzai, boolean]
     candidates:
      com.google.android.gms.games.internal.zzw.zzb(com.google.android.gms.games.internal.zzs, int):void
      com.google.android.gms.games.internal.zzw.zzb(com.google.android.gms.games.internal.zzs, long):void
      com.google.android.gms.games.internal.zzw.zzb(com.google.android.gms.games.internal.zzs, java.lang.String):void
      com.google.android.gms.games.internal.zzw.zzb(com.google.android.gms.games.internal.zzs, java.lang.String[]):void
      com.google.android.gms.games.internal.zzw.zzb(com.google.android.gms.games.internal.zzs, boolean):void */
    public final void zzb(com.google.android.gms.common.api.internal.zzn<Leaderboards.LeaderboardMetadataResult> zzn2, boolean z) throws RemoteException {
        try {
            ((zzw) zzakb()).zzb((zzs) new zzai(zzn2), z);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzb(com.google.android.gms.common.api.internal.zzn<Quests.LoadQuestsResult> zzn2, boolean z, String[] strArr) throws RemoteException {
        this.zzhmm.flush();
        try {
            ((zzw) zzakb()).zza(new zzbt(zzn2), strArr, z);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzb(com.google.android.gms.common.api.internal.zzn<Requests.UpdateRequestsResult> zzn2, String[] strArr) throws RemoteException {
        try {
            ((zzw) zzakb()).zzb(new zzca(zzn2), strArr);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzb(Snapshot snapshot) {
        try {
            zza(snapshot);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzb(String str, com.google.android.gms.common.api.internal.zzn<Games.GetServerAuthCodeResult> zzn2) throws RemoteException {
        com.google.android.gms.common.internal.zzbq.zzh(str, "Please provide a valid serverClientId");
        try {
            ((zzw) zzakb()).zza(str, new zzy(zzn2));
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final String zzbg(boolean z) throws RemoteException {
        return this.zzhmo != null ? this.zzhmo.getPlayerId() : ((zzw) zzakb()).zzasu();
    }

    public final String zzbh(boolean z) {
        try {
            return zzbg(true);
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final Intent zzc(int i, int i2, boolean z) {
        try {
            return zzb(i, i2, z);
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, android.os.IBinder, java.lang.String, boolean, long):void
     arg types: [com.google.android.gms.games.internal.GamesClientImpl$zzcc, android.os.Binder, java.lang.String, int, long]
     candidates:
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, int, int, java.lang.String[], android.os.Bundle):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String, int, android.os.IBinder, android.os.Bundle):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String, int, boolean, boolean):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String, java.lang.String, int, int):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String, java.lang.String, com.google.android.gms.games.snapshot.zze, com.google.android.gms.drive.zzc):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String, byte[], java.lang.String, com.google.android.gms.games.multiplayer.ParticipantResult[]):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, android.os.IBinder, java.lang.String, boolean, long):void */
    public final void zzc(com.google.android.gms.common.api.internal.zzcl<? extends RoomUpdateListener> zzcl2, com.google.android.gms.common.api.internal.zzcl<? extends RoomStatusUpdateListener> zzcl3, com.google.android.gms.common.api.internal.zzcl<? extends RealTimeMessageReceivedListener> zzcl4, RoomConfig roomConfig) throws RemoteException {
        ((zzw) zzakb()).zza((zzs) new zzcc(zzcl2, zzcl3, zzcl4), (IBinder) this.zzhms, roomConfig.getInvitationId(), false, this.zzhmt);
    }

    public final void zzc(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.InitiateMatchResult> zzn2, String str) throws RemoteException {
        try {
            ((zzw) zzakb()).zzb(new zzco(zzn2), str);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, boolean):void
     arg types: [com.google.android.gms.games.internal.GamesClientImpl$zzf, boolean]
     candidates:
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.multiplayer.realtime.RoomEntity, int):android.content.Intent
      com.google.android.gms.games.internal.zzw.zza(android.os.IBinder, android.os.Bundle):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, int):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, long):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, java.lang.String[]):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzu, long):void
      com.google.android.gms.games.internal.zzw.zza(java.lang.String, com.google.android.gms.games.internal.zzs):void
      com.google.android.gms.games.internal.zzw.zza(com.google.android.gms.games.internal.zzs, boolean):void */
    public final void zzc(com.google.android.gms.common.api.internal.zzn<Achievements.LoadAchievementsResult> zzn2, boolean z) throws RemoteException {
        try {
            ((zzw) zzakb()).zza((zzs) new zzf(zzn2), z);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final int zzd(byte[] bArr, String str) throws RemoteException {
        return ((zzw) zzakb()).zzb(bArr, str, (String[]) null);
    }

    public final Intent zzd(int i, int i2, boolean z) throws RemoteException {
        return ((zzw) zzakb()).zzd(i, i2, z);
    }

    public final Intent zzd(int[] iArr) {
        try {
            return ((zzw) zzakb()).zzd(iArr);
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ IInterface zzd(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.games.internal.IGamesService");
        return queryLocalInterface instanceof zzw ? (zzw) queryLocalInterface : new zzx(iBinder);
    }

    public final void zzd(com.google.android.gms.common.api.internal.zzcl<? extends RoomUpdateListener> zzcl2, com.google.android.gms.common.api.internal.zzcl<? extends RoomStatusUpdateListener> zzcl3, com.google.android.gms.common.api.internal.zzcl<? extends RealTimeMessageReceivedListener> zzcl4, RoomConfig roomConfig) {
        try {
            zzc(zzcl2, zzcl3, zzcl4, roomConfig);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzd(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.InitiateMatchResult> zzn2, String str) throws RemoteException {
        try {
            ((zzw) zzakb()).zzc(new zzco(zzn2), str);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzw.zze(com.google.android.gms.games.internal.zzs, boolean):void
     arg types: [com.google.android.gms.games.internal.GamesClientImpl$zzu, boolean]
     candidates:
      com.google.android.gms.games.internal.zzw.zze(com.google.android.gms.games.internal.zzs, long):void
      com.google.android.gms.games.internal.zzw.zze(com.google.android.gms.games.internal.zzs, java.lang.String):void
      com.google.android.gms.games.internal.zzw.zze(com.google.android.gms.games.internal.zzs, boolean):void */
    public final void zzd(com.google.android.gms.common.api.internal.zzn<Events.LoadEventsResult> zzn2, boolean z) throws RemoteException {
        this.zzhmm.flush();
        try {
            ((zzw) zzakb()).zze((zzs) new zzu(zzn2), z);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzdi(int i) {
        this.zzhmq.zzhpr.gravity = i;
    }

    public final void zzdj(int i) throws RemoteException {
        ((zzw) zzakb()).zzdj(i);
    }

    public final void zzdk(int i) {
        try {
            zzdj(i);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final int zze(byte[] bArr, String str) {
        try {
            return zzd(bArr, str);
        } catch (RemoteException e) {
            zzd(e);
            return -1;
        }
    }

    public final Intent zze(int i, int i2, boolean z) {
        try {
            return zzd(i, i2, z);
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final void zze(com.google.android.gms.common.api.internal.zzcl<OnInvitationReceivedListener> zzcl2) throws RemoteException {
        ((zzw) zzakb()).zza(new zzab(zzcl2), this.zzhmt);
    }

    public final void zze(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.LeaveMatchResult> zzn2, String str) throws RemoteException {
        try {
            ((zzw) zzakb()).zze(new zzcp(zzn2), str);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zze(com.google.android.gms.common.api.internal.zzn<Stats.LoadPlayerStatsResult> zzn2, boolean z) throws RemoteException {
        try {
            ((zzw) zzakb()).zzf(new zzbm(zzn2), z);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzf(com.google.android.gms.common.api.internal.zzcl<OnInvitationReceivedListener> zzcl2) {
        try {
            zze(zzcl2);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzf(com.google.android.gms.common.api.internal.zzn<GamesMetadata.LoadGamesResult> zzn2) throws RemoteException {
        try {
            ((zzw) zzakb()).zzb(new zzx(zzn2));
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzf(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.CancelMatchResult> zzn2, String str) throws RemoteException {
        try {
            ((zzw) zzakb()).zzd(new zzcn(zzn2), str);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzw.zzd(com.google.android.gms.games.internal.zzs, boolean):void
     arg types: [com.google.android.gms.games.internal.GamesClientImpl$zzck, boolean]
     candidates:
      com.google.android.gms.games.internal.zzw.zzd(com.google.android.gms.games.internal.zzs, long):void
      com.google.android.gms.games.internal.zzw.zzd(com.google.android.gms.games.internal.zzs, java.lang.String):void
      com.google.android.gms.games.internal.zzw.zzd(com.google.android.gms.games.internal.zzs, boolean):void */
    public final void zzf(com.google.android.gms.common.api.internal.zzn<Snapshots.LoadSnapshotsResult> zzn2, boolean z) throws RemoteException {
        try {
            ((zzw) zzakb()).zzd((zzs) new zzck(zzn2), z);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzg(com.google.android.gms.common.api.internal.zzcl<OnTurnBasedMatchUpdateReceivedListener> zzcl2) throws RemoteException {
        ((zzw) zzakb()).zzb(new zzaz(zzcl2), this.zzhmt);
    }

    public final void zzg(com.google.android.gms.common.api.internal.zzn<Status> zzn2) throws RemoteException {
        this.zzhmm.flush();
        try {
            ((zzw) zzakb()).zza(new zzcg(zzn2));
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzg(com.google.android.gms.common.api.internal.zzn<TurnBasedMultiplayer.LoadMatchResult> zzn2, String str) throws RemoteException {
        try {
            ((zzw) zzakb()).zzf(new zzcq(zzn2), str);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzh(com.google.android.gms.common.api.internal.zzcl<OnTurnBasedMatchUpdateReceivedListener> zzcl2) {
        try {
            zzg(zzcl2);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzh(com.google.android.gms.common.api.internal.zzn<Videos.CaptureCapabilitiesResult> zzn2) throws RemoteException {
        try {
            ((zzw) zzakb()).zzc(new zzj(zzn2));
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzh(com.google.android.gms.common.api.internal.zzn<Quests.AcceptQuestResult> zzn2, String str) throws RemoteException {
        this.zzhmm.flush();
        try {
            ((zzw) zzakb()).zzh(new zzbp(zzn2), str);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    /* access modifiers changed from: protected */
    public final String zzhf() {
        return "com.google.android.gms.games.service.START";
    }

    /* access modifiers changed from: protected */
    public final String zzhg() {
        return "com.google.android.gms.games.internal.IGamesService";
    }

    public final void zzhk(String str) throws RemoteException {
        ((zzw) zzakb()).zzho(str);
    }

    public final void zzhl(String str) {
        try {
            zzhk(str);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final Intent zzhm(String str) {
        try {
            return ((zzw) zzakb()).zzhm(str);
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final void zzhn(String str) {
        try {
            ((zzw) zzakb()).zza(str, this.zzhmq.zzhpr.zzhps, this.zzhmq.zzhpr.zzata());
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzi(com.google.android.gms.common.api.internal.zzcl<QuestUpdateListener> zzcl2) {
        try {
            ((zzw) zzakb()).zzd(new zzbs(zzcl2), this.zzhmt);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzi(com.google.android.gms.common.api.internal.zzn<Videos.CaptureStateResult> zzn2) throws RemoteException {
        try {
            ((zzw) zzakb()).zzd(new zzn(zzn2));
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final void zzi(com.google.android.gms.common.api.internal.zzn<Snapshots.DeleteSnapshotResult> zzn2, String str) throws RemoteException {
        try {
            ((zzw) zzakb()).zzg(new zzci(zzn2), str);
        } catch (SecurityException e) {
            zza(zzn2, e);
        }
    }

    public final Intent zzj(String str, int i, int i2) {
        try {
            return ((zzw) zzakb()).zzk(str, i, i2);
        } catch (RemoteException e) {
            zzd(e);
            return null;
        }
    }

    public final void zzj(com.google.android.gms.common.api.internal.zzcl<OnRequestReceivedListener> zzcl2) {
        try {
            ((zzw) zzakb()).zzc(new zzbw(zzcl2), this.zzhmt);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzk(com.google.android.gms.common.api.internal.zzcl<Videos.CaptureOverlayStateListener> zzcl2) throws RemoteException {
        ((zzw) zzakb()).zze(new zzl(zzcl2), this.zzhmt);
    }

    public final void zzl(com.google.android.gms.common.api.internal.zzcl<Videos.CaptureOverlayStateListener> zzcl2) {
        try {
            zzk(zzcl2);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzp(String str, int i) {
        this.zzhmm.zzp(str, i);
    }

    public final void zzq(String str, int i) throws RemoteException {
        ((zzw) zzakb()).zzq(str, i);
    }

    public final void zzr(String str, int i) {
        try {
            zzq(str, i);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzs(String str, int i) throws RemoteException {
        ((zzw) zzakb()).zzs(str, i);
    }

    public final void zzt(String str, int i) {
        try {
            zzs(str, i);
        } catch (RemoteException e) {
            zzd(e);
        }
    }

    public final void zzu(View view) {
        this.zzhmq.zzv(view);
    }
}
