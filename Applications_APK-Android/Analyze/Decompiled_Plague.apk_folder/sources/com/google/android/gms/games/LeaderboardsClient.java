package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.internal.api.zzp;
import com.google.android.gms.games.internal.zzg;
import com.google.android.gms.games.internal.zzo;
import com.google.android.gms.games.leaderboard.Leaderboard;
import com.google.android.gms.games.leaderboard.LeaderboardBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;
import com.google.android.gms.tasks.Task;

public class LeaderboardsClient extends zzp {
    private static final zzbo<Leaderboards.LeaderboardMetadataResult, LeaderboardBuffer> zzhil = new zzal();
    private static final zzbo<Leaderboards.LeaderboardMetadataResult, Leaderboard> zzhim = new zzam();
    private static final zzo<Leaderboards.LeaderboardMetadataResult> zzhin = new zzan();
    private static final zzbo<Leaderboards.LoadPlayerScoreResult, LeaderboardScore> zzhio = new zzac();
    private static final com.google.android.gms.games.internal.zzp zzhip = new zzad();
    private static final zzbo<Leaderboards.SubmitScoreResult, ScoreSubmissionData> zzhiq = new zzae();
    private static final zzbo<Leaderboards.LoadScoresResult, LeaderboardScores> zzhir = new zzaf();

    public static class LeaderboardScores implements Releasable {
        private final Leaderboard zzhix;
        private final LeaderboardScoreBuffer zzhiy;

        LeaderboardScores(@Nullable Leaderboard leaderboard, @NonNull LeaderboardScoreBuffer leaderboardScoreBuffer) {
            this.zzhix = leaderboard;
            this.zzhiy = leaderboardScoreBuffer;
        }

        @Nullable
        public Leaderboard getLeaderboard() {
            return this.zzhix;
        }

        @NonNull
        public LeaderboardScoreBuffer getScores() {
            return this.zzhiy;
        }

        public void release() {
            if (this.zzhiy != null) {
                this.zzhiy.release();
            }
        }
    }

    LeaderboardsClient(@NonNull Activity activity, @NonNull Games.GamesOptions gamesOptions) {
        super(activity, gamesOptions);
    }

    LeaderboardsClient(@NonNull Context context, @NonNull Games.GamesOptions gamesOptions) {
        super(context, gamesOptions);
    }

    public Task<Intent> getAllLeaderboardsIntent() {
        return zza(new zzab(this));
    }

    public Task<Intent> getLeaderboardIntent(@NonNull String str) {
        return zza(new zzag(this, str));
    }

    public Task<Intent> getLeaderboardIntent(@NonNull String str, int i) {
        return zza(new zzah(this, str, i));
    }

    public Task<Intent> getLeaderboardIntent(@NonNull String str, int i, int i2) {
        return zza(new zzai(this, str, i, i2));
    }

    public Task<AnnotatedData<LeaderboardScore>> loadCurrentPlayerLeaderboardScore(@NonNull String str, int i, int i2) {
        return zzg.zzb(Games.Leaderboards.loadCurrentPlayerLeaderboardScore(zzagb(), str, i, i2), zzhio);
    }

    public Task<AnnotatedData<Leaderboard>> loadLeaderboardMetadata(@NonNull String str, boolean z) {
        return zzg.zza(Games.Leaderboards.loadLeaderboardMetadata(zzagb(), str, z), zzhim, zzhin);
    }

    public Task<AnnotatedData<LeaderboardBuffer>> loadLeaderboardMetadata(boolean z) {
        return zzg.zzc(Games.Leaderboards.loadLeaderboardMetadata(zzagb(), z), zzhil);
    }

    public Task<AnnotatedData<LeaderboardScores>> loadMoreScores(@NonNull LeaderboardScoreBuffer leaderboardScoreBuffer, @IntRange(from = 1, to = 25) int i, int i2) {
        return zzg.zzc(Games.Leaderboards.loadMoreScores(zzagb(), leaderboardScoreBuffer, i, i2), zzhir);
    }

    public Task<AnnotatedData<LeaderboardScores>> loadPlayerCenteredScores(@NonNull String str, int i, int i2, @IntRange(from = 1, to = 25) int i3) {
        return zzg.zzc(Games.Leaderboards.loadPlayerCenteredScores(zzagb(), str, i, i2, i3), zzhir);
    }

    public Task<AnnotatedData<LeaderboardScores>> loadPlayerCenteredScores(@NonNull String str, int i, int i2, @IntRange(from = 1, to = 25) int i3, boolean z) {
        return zzg.zzc(Games.Leaderboards.loadPlayerCenteredScores(zzagb(), str, i, i2, i3, z), zzhir);
    }

    public Task<AnnotatedData<LeaderboardScores>> loadTopScores(@NonNull String str, int i, int i2, @IntRange(from = 1, to = 25) int i3) {
        return zzg.zzc(Games.Leaderboards.loadTopScores(zzagb(), str, i, i2, i3), zzhir);
    }

    public Task<AnnotatedData<LeaderboardScores>> loadTopScores(@NonNull String str, int i, int i2, @IntRange(from = 1, to = 25) int i3, boolean z) {
        return zzg.zzc(Games.Leaderboards.loadTopScores(zzagb(), str, i, i2, i3, z), zzhir);
    }

    public void submitScore(@NonNull String str, long j) {
        zzb(new zzaj(this, str, j));
    }

    public void submitScore(@NonNull String str, long j, @NonNull String str2) {
        zzb(new zzak(this, str, j, str2));
    }

    public Task<ScoreSubmissionData> submitScoreImmediate(@NonNull String str, long j) {
        return zzg.zza(Games.Leaderboards.submitScoreImmediate(zzagb(), str, j), zzhip, zzhiq);
    }

    public Task<ScoreSubmissionData> submitScoreImmediate(@NonNull String str, long j, @NonNull String str2) {
        return zzg.zza(Games.Leaderboards.submitScoreImmediate(zzagb(), str, j, str2), zzhip, zzhiq);
    }
}
