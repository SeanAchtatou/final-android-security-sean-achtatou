package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.zzcn;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.zzr;
import com.google.android.gms.games.video.Videos;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzcy extends zzr<Videos.CaptureOverlayStateListener> {
    zzcy(VideosClient videosClient, zzcn zzcn) {
        super(zzcn);
    }

    /* access modifiers changed from: protected */
    public final void zzc(GamesClientImpl gamesClientImpl, TaskCompletionSource<Boolean> taskCompletionSource) throws RemoteException, SecurityException {
        gamesClientImpl.zzasq();
        taskCompletionSource.setResult(true);
    }
}
