package com.google.android.gms.drive.query.internal;

import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.zzb;
import com.google.android.gms.drive.query.Filter;
import java.util.List;

public final class zzk implements zzj<Boolean> {
    private Boolean zzgsr = false;

    private zzk() {
    }

    public static boolean zza(Filter filter) {
        if (filter == null) {
            return false;
        }
        return ((Boolean) filter.zza(new zzk())).booleanValue();
    }

    public final /* synthetic */ Object zza(zzb zzb, Object obj) {
        return this.zzgsr;
    }

    public final /* synthetic */ Object zza(zzx zzx, MetadataField metadataField, Object obj) {
        return this.zzgsr;
    }

    public final /* synthetic */ Object zza(zzx zzx, List list) {
        return this.zzgsr;
    }

    public final /* synthetic */ Object zzapi() {
        return this.zzgsr;
    }

    public final /* synthetic */ Object zzapj() {
        return this.zzgsr;
    }

    public final /* synthetic */ Object zzd(MetadataField metadataField, Object obj) {
        return this.zzgsr;
    }

    public final /* synthetic */ Object zze(MetadataField metadataField) {
        return this.zzgsr;
    }

    public final /* synthetic */ Object zzgv(String str) {
        if (!str.isEmpty()) {
            this.zzgsr = true;
        }
        return this.zzgsr;
    }

    public final /* synthetic */ Object zzx(Object obj) {
        return this.zzgsr;
    }
}
