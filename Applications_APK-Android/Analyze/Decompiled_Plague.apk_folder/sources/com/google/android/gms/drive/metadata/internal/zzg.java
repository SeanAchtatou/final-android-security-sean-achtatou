package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.common.data.DataHolder;

public interface zzg {
    String zzapc();

    void zzc(DataHolder dataHolder);
}
