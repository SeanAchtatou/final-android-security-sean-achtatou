package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;

final class zzan extends zzat {
    private /* synthetic */ int zzhqc;
    private /* synthetic */ LeaderboardScoreBuffer zzhqd;
    private /* synthetic */ int zzhqe;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzan(zzah zzah, GoogleApiClient googleApiClient, LeaderboardScoreBuffer leaderboardScoreBuffer, int i, int i2) {
        super(googleApiClient, null);
        this.zzhqd = leaderboardScoreBuffer;
        this.zzhqc = i;
        this.zzhqe = i2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhqd, this.zzhqc, this.zzhqe);
    }
}
