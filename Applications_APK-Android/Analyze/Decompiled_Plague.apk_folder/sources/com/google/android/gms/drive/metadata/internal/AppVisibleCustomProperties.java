package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class AppVisibleCustomProperties extends zzbej implements ReflectedParcelable, Iterable<zzc> {
    public static final Parcelable.Creator<AppVisibleCustomProperties> CREATOR = new zza();
    public static final AppVisibleCustomProperties zzgph = new zza().zzapa();
    private List<zzc> zzgpi;

    public static class zza {
        private final Map<CustomPropertyKey, zzc> zzgpj = new HashMap();

        public final zza zza(CustomPropertyKey customPropertyKey, String str) {
            zzbq.checkNotNull(customPropertyKey, "key");
            this.zzgpj.put(customPropertyKey, new zzc(customPropertyKey, str));
            return this;
        }

        public final zza zza(zzc zzc) {
            zzbq.checkNotNull(zzc, "property");
            this.zzgpj.put(zzc.zzgpk, zzc);
            return this;
        }

        public final AppVisibleCustomProperties zzapa() {
            return new AppVisibleCustomProperties(this.zzgpj.values());
        }
    }

    AppVisibleCustomProperties(Collection<zzc> collection) {
        zzbq.checkNotNull(collection);
        this.zzgpi = new ArrayList(collection);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        return zzaoz().equals(((AppVisibleCustomProperties) obj).zzaoz());
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.zzgpi});
    }

    public final Iterator<zzc> iterator() {
        return this.zzgpi.iterator();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 2, this.zzgpi, false);
        zzbem.zzai(parcel, zze);
    }

    public final Map<CustomPropertyKey, String> zzaoz() {
        HashMap hashMap = new HashMap(this.zzgpi.size());
        for (zzc next : this.zzgpi) {
            hashMap.put(next.zzgpk, next.mValue);
        }
        return Collections.unmodifiableMap(hashMap);
    }
}
