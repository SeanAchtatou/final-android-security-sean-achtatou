package com.google.android.gms.internal;

interface zzbnx<T> {
    void accept(T t);
}
