package com.google.android.gms.dynamic;

final class zzh implements zzi {
    private /* synthetic */ zza zzgtn;

    zzh(zza zza) {
        this.zzgtn = zza;
    }

    public final int getState() {
        return 5;
    }

    public final void zzb(LifecycleDelegate lifecycleDelegate) {
        this.zzgtn.zzgtj.onResume();
    }
}
