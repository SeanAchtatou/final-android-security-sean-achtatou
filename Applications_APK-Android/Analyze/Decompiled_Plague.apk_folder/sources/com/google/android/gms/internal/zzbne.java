package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzcn;
import com.google.android.gms.common.api.internal.zzdp;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbne extends zzdp<zzbll, zzbnz> {
    private /* synthetic */ DriveResource zzgmc;
    private /* synthetic */ zzbnz zzgmd;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbne(zzbmu zzbmu, zzcn zzcn, DriveResource driveResource, zzbnz zzbnz) {
        super(zzcn);
        this.zzgmc = driveResource;
        this.zzgmd = zzbnz;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zzc(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbrd(this.zzgmc.getDriveId(), 1), this.zzgmd.zzgmw, (String) null, new zzbrt(taskCompletionSource));
    }
}
