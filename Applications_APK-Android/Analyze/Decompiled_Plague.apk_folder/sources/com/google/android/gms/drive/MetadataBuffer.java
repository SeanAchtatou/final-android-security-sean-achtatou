package com.google.android.gms.drive;

import com.google.android.gms.common.data.AbstractDataBuffer;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.metadata.internal.zzf;
import com.google.android.gms.internal.zzbkk;
import com.google.android.gms.internal.zzbse;

public final class MetadataBuffer extends AbstractDataBuffer<Metadata> {
    private zza zzghg;

    static class zza extends Metadata {
        private final DataHolder zzfnz;
        private final int zzfte;
        /* access modifiers changed from: private */
        public final int zzghh;

        public zza(DataHolder dataHolder, int i) {
            this.zzfnz = dataHolder;
            this.zzghh = i;
            this.zzfte = dataHolder.zzbz(i);
        }

        public final /* synthetic */ Object freeze() {
            MetadataBundle zzapd = MetadataBundle.zzapd();
            for (MetadataField<BitmapTeleporter> next : zzf.zzapb()) {
                if (next != zzbse.zzgqw) {
                    next.zza(this.zzfnz, zzapd, this.zzghh, this.zzfte);
                }
            }
            return new zzbkk(zzapd);
        }

        public final boolean isDataValid() {
            return !this.zzfnz.isClosed();
        }

        public final <T> T zza(MetadataField<T> metadataField) {
            return metadataField.zza(this.zzfnz, this.zzghh, this.zzfte);
        }
    }

    public MetadataBuffer(DataHolder dataHolder) {
        super(dataHolder);
        dataHolder.zzafx().setClassLoader(MetadataBuffer.class.getClassLoader());
    }

    public final Metadata get(int i) {
        zza zza2 = this.zzghg;
        if (zza2 != null && zza2.zzghh == i) {
            return zza2;
        }
        zza zza3 = new zza(this.zzfnz, i);
        this.zzghg = zza3;
        return zza3;
    }

    @Deprecated
    public final String getNextPageToken() {
        return null;
    }

    public final void release() {
        if (this.zzfnz != null) {
            zzf.zzb(this.zzfnz);
        }
        super.release();
    }
}
