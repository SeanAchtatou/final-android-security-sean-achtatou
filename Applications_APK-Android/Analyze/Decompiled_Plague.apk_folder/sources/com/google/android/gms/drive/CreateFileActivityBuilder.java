package com.google.android.gms.drive;

import android.content.IntentSender;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbkd;
import com.google.android.gms.internal.zzblv;

@Deprecated
public class CreateFileActivityBuilder {
    public static final String EXTRA_RESPONSE_DRIVE_ID = "response_drive_id";
    private final zzbkd zzgga = new zzbkd(0);
    private DriveContents zzggb;
    private boolean zzggc;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public IntentSender build(GoogleApiClient googleApiClient) {
        zzbq.zza(googleApiClient.isConnected(), (Object) "Client must be connected");
        zzanm();
        return this.zzgga.build(googleApiClient);
    }

    /* access modifiers changed from: package-private */
    public final int getRequestId() {
        return this.zzgga.getRequestId();
    }

    public CreateFileActivityBuilder setActivityStartFolder(DriveId driveId) {
        this.zzgga.zza(driveId);
        return this;
    }

    public CreateFileActivityBuilder setActivityTitle(String str) {
        this.zzgga.setActivityTitle(str);
        return this;
    }

    public CreateFileActivityBuilder setInitialDriveContents(DriveContents driveContents) {
        if (driveContents == null) {
            this.zzgga.zzct(1);
        } else if (!(driveContents instanceof zzblv)) {
            throw new IllegalArgumentException("Only DriveContents obtained from the Drive API are accepted.");
        } else if (driveContents.getDriveId() != null) {
            throw new IllegalArgumentException("Only DriveContents obtained through DriveApi.newDriveContents are accepted for file creation.");
        } else if (driveContents.zzanr()) {
            throw new IllegalArgumentException("DriveContents are already closed.");
        } else {
            this.zzgga.zzct(driveContents.zzanp().zzgfw);
            this.zzggb = driveContents;
        }
        this.zzggc = true;
        return this;
    }

    public CreateFileActivityBuilder setInitialMetadata(MetadataChangeSet metadataChangeSet) {
        this.zzgga.zza(metadataChangeSet);
        return this;
    }

    /* access modifiers changed from: package-private */
    public final MetadataChangeSet zzani() {
        return this.zzgga.zzani();
    }

    /* access modifiers changed from: package-private */
    public final DriveId zzanj() {
        return this.zzgga.zzanj();
    }

    /* access modifiers changed from: package-private */
    public final String zzank() {
        return this.zzgga.zzank();
    }

    /* access modifiers changed from: package-private */
    public final int zzanl() {
        zzbkd zzbkd = this.zzgga;
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    public final void zzanm() {
        zzbq.zza(this.zzggc, (Object) "Must call setInitialDriveContents.");
        if (this.zzggb != null) {
            this.zzggb.zzanq();
        }
        this.zzgga.zzanm();
    }
}
