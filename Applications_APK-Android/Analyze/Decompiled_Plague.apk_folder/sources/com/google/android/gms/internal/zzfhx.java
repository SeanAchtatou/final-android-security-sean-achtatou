package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfhx extends zzfhe<zzfhx> {
    public String zzpjd = null;
    public Long zzpje = null;
    public Boolean zzpjf = null;

    public zzfhx() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 10) {
                this.zzpjd = zzfhb.readString();
            } else if (zzcts == 16) {
                this.zzpje = Long.valueOf(zzfhb.zzctu());
            } else if (zzcts == 24) {
                this.zzpjf = Boolean.valueOf(zzfhb.zzcty());
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzpjd != null) {
            zzfhc.zzn(1, this.zzpjd);
        }
        if (this.zzpje != null) {
            zzfhc.zzf(2, this.zzpje.longValue());
        }
        if (this.zzpjf != null) {
            zzfhc.zzl(3, this.zzpjf.booleanValue());
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzpjd != null) {
            zzo += zzfhc.zzo(1, this.zzpjd);
        }
        if (this.zzpje != null) {
            zzo += zzfhc.zzc(2, this.zzpje.longValue());
        }
        if (this.zzpjf == null) {
            return zzo;
        }
        this.zzpjf.booleanValue();
        return zzo + zzfhc.zzkw(3) + 1;
    }
}
