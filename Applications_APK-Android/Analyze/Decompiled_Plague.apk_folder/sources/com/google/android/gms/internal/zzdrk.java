package com.google.android.gms.internal;

import java.io.IOException;

public final class zzdrk extends zzfee<zzdrk, zza> implements zzffk {
    private static volatile zzffm<zzdrk> zzbas;
    /* access modifiers changed from: private */
    public static final zzdrk zzlsr;
    private String zzlso = "";
    private zzfdh zzlsp = zzfdh.zzpal;
    private int zzlsq;

    public static final class zza extends zzfef<zzdrk, zza> implements zzffk {
        private zza() {
            super(zzdrk.zzlsr);
        }

        /* synthetic */ zza(zzdrl zzdrl) {
            this();
        }

        public final zza zzaa(zzfdh zzfdh) {
            zzcvi();
            ((zzdrk) this.zzpbv).zzz(zzfdh);
            return this;
        }

        public final zza zzb(zzb zzb) {
            zzcvi();
            ((zzdrk) this.zzpbv).zza(zzb);
            return this;
        }

        public final zza zzoa(String str) {
            zzcvi();
            ((zzdrk) this.zzpbv).zznz(str);
            return this;
        }
    }

    public enum zzb implements zzfes {
        UNKNOWN_KEYMATERIAL(0),
        SYMMETRIC(1),
        ASYMMETRIC_PRIVATE(2),
        ASYMMETRIC_PUBLIC(3),
        REMOTE(4),
        UNRECOGNIZED(-1);
        
        private static final zzfet<zzb> zzbbb = new zzdrm();
        private final int value;

        private zzb(int i) {
            this.value = i;
        }

        public static zzb zzfr(int i) {
            switch (i) {
                case 0:
                    return UNKNOWN_KEYMATERIAL;
                case 1:
                    return SYMMETRIC;
                case 2:
                    return ASYMMETRIC_PRIVATE;
                case 3:
                    return ASYMMETRIC_PUBLIC;
                case 4:
                    return REMOTE;
                default:
                    return null;
            }
        }

        public final int zzhn() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }
    }

    static {
        zzdrk zzdrk = new zzdrk();
        zzlsr = zzdrk;
        zzdrk.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdrk.zzpbs.zzbim();
    }

    private zzdrk() {
    }

    /* access modifiers changed from: private */
    public final void zza(zzb zzb2) {
        if (zzb2 == null) {
            throw new NullPointerException();
        }
        this.zzlsq = zzb2.zzhn();
    }

    public static zza zzbnv() {
        zzdrk zzdrk = zzlsr;
        zzfef zzfef = (zzfef) zzdrk.zza(zzfem.zzpch, (Object) null, (Object) null);
        zzfef.zza((zzfee) zzdrk);
        return (zza) zzfef;
    }

    public static zzdrk zzbnw() {
        return zzlsr;
    }

    /* access modifiers changed from: private */
    public final void zznz(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.zzlso = str;
    }

    /* access modifiers changed from: private */
    public final void zzz(zzfdh zzfdh) {
        if (zzfdh == null) {
            throw new NullPointerException();
        }
        this.zzlsp = zzfdh;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        boolean z = true;
        boolean z2 = false;
        switch (zzdrl.zzbaq[i - 1]) {
            case 1:
                return new zzdrk();
            case 2:
                return zzlsr;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdrk zzdrk = (zzdrk) obj2;
                this.zzlso = zzfen.zza(!this.zzlso.isEmpty(), this.zzlso, !zzdrk.zzlso.isEmpty(), zzdrk.zzlso);
                this.zzlsp = zzfen.zza(this.zzlsp != zzfdh.zzpal, this.zzlsp, zzdrk.zzlsp != zzfdh.zzpal, zzdrk.zzlsp);
                boolean z3 = this.zzlsq != 0;
                int i2 = this.zzlsq;
                if (zzdrk.zzlsq == 0) {
                    z = false;
                }
                this.zzlsq = zzfen.zza(z3, i2, z, zzdrk.zzlsq);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                if (((zzfea) obj2) != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 10) {
                                    this.zzlso = zzfdq.zzctz();
                                } else if (zzcts == 18) {
                                    this.zzlsp = zzfdq.zzcua();
                                } else if (zzcts == 24) {
                                    this.zzlsq = zzfdq.zzcuc();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdrk.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlsr);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlsr;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (!this.zzlso.isEmpty()) {
            zzfdv.zzn(1, this.zzlso);
        }
        if (!this.zzlsp.isEmpty()) {
            zzfdv.zza(2, this.zzlsp);
        }
        if (this.zzlsq != zzb.UNKNOWN_KEYMATERIAL.zzhn()) {
            zzfdv.zzaa(3, this.zzlsq);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final String zzbns() {
        return this.zzlso;
    }

    public final zzfdh zzbnt() {
        return this.zzlsp;
    }

    public final zzb zzbnu() {
        zzb zzfr = zzb.zzfr(this.zzlsq);
        return zzfr == null ? zzb.UNRECOGNIZED : zzfr;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (!this.zzlso.isEmpty()) {
            i2 = 0 + zzfdv.zzo(1, this.zzlso);
        }
        if (!this.zzlsp.isEmpty()) {
            i2 += zzfdv.zzb(2, this.zzlsp);
        }
        if (this.zzlsq != zzb.UNKNOWN_KEYMATERIAL.zzhn()) {
            i2 += zzfdv.zzag(3, this.zzlsq);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
