package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.TransferPreferences;

final class zzblh implements DriveApi.zza {
    private final Status mStatus;
    private final TransferPreferences zzgkm;

    private zzblh(Status status, TransferPreferences transferPreferences) {
        this.mStatus = status;
        this.zzgkm = transferPreferences;
    }

    /* synthetic */ zzblh(Status status, TransferPreferences transferPreferences, zzbkq zzbkq) {
        this(status, transferPreferences);
    }

    public final Status getStatus() {
        return this.mStatus;
    }

    public final TransferPreferences zzano() {
        return this.zzgkm;
    }
}
