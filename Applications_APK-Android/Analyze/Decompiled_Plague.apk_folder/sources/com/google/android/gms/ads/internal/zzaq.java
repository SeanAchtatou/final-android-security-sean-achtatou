package com.google.android.gms.ads.internal;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzaeu;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzama;
import com.google.android.gms.internal.zzoy;
import com.google.android.gms.internal.zzoz;
import com.google.android.gms.internal.zzuo;
import com.google.android.gms.internal.zzur;
import com.google.android.gms.internal.zzzb;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

@zzzb
public final class zzaq {
    static zzt<zzama> zza(@Nullable zzuo zzuo, @Nullable zzur zzur, zzab zzab) {
        return new zzav(zzuo, zzab, zzur);
    }

    private static String zza(@Nullable Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (bitmap == null) {
            zzafj.zzco("Bitmap is null. Returning empty string");
            return "";
        }
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        String encodeToString = Base64.encodeToString(byteArrayOutputStream.toByteArray(), 0);
        String valueOf = String.valueOf("data:image/png;base64,");
        String valueOf2 = String.valueOf(encodeToString);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    static String zza(@Nullable zzoy zzoy) {
        if (zzoy == null) {
            zzafj.zzco("Image is null. Returning empty string");
            return "";
        }
        try {
            Uri uri = zzoy.getUri();
            if (uri != null) {
                return uri.toString();
            }
        } catch (RemoteException unused) {
            zzafj.zzco("Unable to get image uri. Trying data uri next");
        }
        return zzb(zzoy);
    }

    /* access modifiers changed from: private */
    public static JSONObject zza(@Nullable Bundle bundle, String str) throws JSONException {
        String valueOf;
        String str2;
        JSONObject jSONObject = new JSONObject();
        if (bundle == null || TextUtils.isEmpty(str)) {
            return jSONObject;
        }
        JSONObject jSONObject2 = new JSONObject(str);
        Iterator<String> keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            if (bundle.containsKey(next)) {
                if ("image".equals(jSONObject2.getString(next))) {
                    Object obj = bundle.get(next);
                    if (obj instanceof Bitmap) {
                        valueOf = zza((Bitmap) obj);
                    } else {
                        str2 = "Invalid type. An image type extra should return a bitmap";
                        zzafj.zzco(str2);
                    }
                } else if (bundle.get(next) instanceof Bitmap) {
                    str2 = "Invalid asset type. Bitmap should be returned only for image type";
                    zzafj.zzco(str2);
                } else {
                    valueOf = String.valueOf(bundle.get(next));
                }
                jSONObject.put(next, valueOf);
            }
        }
        return jSONObject;
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x0140  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean zza(com.google.android.gms.internal.zzama r26, com.google.android.gms.internal.zztt r27, java.util.concurrent.CountDownLatch r28) {
        /*
            r1 = r26
            r2 = r27
            r7 = r28
            r3 = 0
            r8 = 0
            if (r1 != 0) goto L_0x0013
            throw r3     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
        L_0x000b:
            r0 = move-exception
            r1 = r0
            goto L_0x0135
        L_0x000f:
            r0 = move-exception
            r1 = r0
            goto L_0x0139
        L_0x0013:
            r4 = r1
            android.view.View r4 = (android.view.View) r4     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            if (r4 != 0) goto L_0x001f
            java.lang.String r1 = "AdWebView is null"
        L_0x001a:
            com.google.android.gms.internal.zzafj.zzco(r1)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            goto L_0x013e
        L_0x001f:
            r5 = 4
            r4.setVisibility(r5)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.internal.zztm r4 = r2.zzcdd     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.util.List<java.lang.String> r4 = r4.zzcbq     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            if (r4 == 0) goto L_0x0131
            boolean r5 = r4.isEmpty()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            if (r5 == 0) goto L_0x0031
            goto L_0x0131
        L_0x0031:
            com.google.android.gms.internal.zzamb r5 = r26.zzsq()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r6 = "/nativeExpressAssetsLoaded"
            com.google.android.gms.ads.internal.zzat r9 = new com.google.android.gms.ads.internal.zzat     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            r9.<init>(r7)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            r5.zza(r6, r9)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.internal.zzamb r5 = r26.zzsq()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r6 = "/nativeExpressAssetsLoadingFailed"
            com.google.android.gms.ads.internal.zzau r9 = new com.google.android.gms.ads.internal.zzau     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            r9.<init>(r7)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            r5.zza(r6, r9)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.internal.zzuf r5 = r2.zzcde     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.internal.zzuo r5 = r5.zzly()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.internal.zzuf r6 = r2.zzcde     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.internal.zzur r6 = r6.zzlz()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r9 = "2"
            boolean r9 = r4.contains(r9)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            if (r9 == 0) goto L_0x00ba
            if (r5 == 0) goto L_0x00ba
            com.google.android.gms.internal.zzns r4 = new com.google.android.gms.internal.zzns     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r11 = r5.getHeadline()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.util.List r12 = r5.getImages()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r13 = r5.getBody()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.internal.zzoy r14 = r5.zzjm()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r15 = r5.getCallToAction()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            double r16 = r5.getStarRating()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r18 = r5.getStore()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r19 = r5.getPrice()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            r20 = 0
            android.os.Bundle r21 = r5.getExtras()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            r22 = 0
            com.google.android.gms.dynamic.IObjectWrapper r6 = r5.zzmf()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            if (r6 == 0) goto L_0x009d
            com.google.android.gms.dynamic.IObjectWrapper r3 = r5.zzmf()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.Object r3 = com.google.android.gms.dynamic.zzn.zzx(r3)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            android.view.View r3 = (android.view.View) r3     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
        L_0x009d:
            r23 = r3
            com.google.android.gms.dynamic.IObjectWrapper r24 = r5.zzjr()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            r25 = 0
            r10 = r4
            r10.<init>(r11, r12, r13, r14, r15, r16, r18, r19, r20, r21, r22, r23, r24, r25)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.internal.zztm r3 = r2.zzcdd     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r3 = r3.zzcbp     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.internal.zzamb r5 = r26.zzsq()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.ads.internal.zzar r6 = new com.google.android.gms.ads.internal.zzar     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            r6.<init>(r4, r3, r1)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
        L_0x00b6:
            r5.zza(r6)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            goto L_0x0110
        L_0x00ba:
            java.lang.String r5 = "1"
            boolean r4 = r4.contains(r5)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            if (r4 == 0) goto L_0x012d
            if (r6 == 0) goto L_0x012d
            com.google.android.gms.internal.zznu r4 = new com.google.android.gms.internal.zznu     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r10 = r6.getHeadline()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.util.List r11 = r6.getImages()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r12 = r6.getBody()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.internal.zzoy r13 = r6.zzjt()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r14 = r6.getCallToAction()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r15 = r6.getAdvertiser()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            r16 = 0
            android.os.Bundle r17 = r6.getExtras()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            r18 = 0
            com.google.android.gms.dynamic.IObjectWrapper r5 = r6.zzmf()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            if (r5 == 0) goto L_0x00f6
            com.google.android.gms.dynamic.IObjectWrapper r3 = r6.zzmf()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.Object r3 = com.google.android.gms.dynamic.zzn.zzx(r3)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            android.view.View r3 = (android.view.View) r3     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
        L_0x00f6:
            r19 = r3
            com.google.android.gms.dynamic.IObjectWrapper r20 = r6.zzjr()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            r21 = 0
            r9 = r4
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.internal.zztm r3 = r2.zzcdd     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r3 = r3.zzcbp     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.internal.zzamb r5 = r26.zzsq()     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.ads.internal.zzas r6 = new com.google.android.gms.ads.internal.zzas     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            r6.<init>(r4, r3, r1)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            goto L_0x00b6
        L_0x0110:
            com.google.android.gms.internal.zztm r3 = r2.zzcdd     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r3 = r3.zzcbn     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            com.google.android.gms.internal.zztm r2 = r2.zzcdd     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            java.lang.String r2 = r2.zzcbo     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            if (r2 == 0) goto L_0x0123
            java.lang.String r4 = "text/html"
            java.lang.String r5 = "UTF-8"
            r6 = 0
            r1.loadDataWithBaseURL(r2, r3, r4, r5, r6)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
            goto L_0x012a
        L_0x0123:
            java.lang.String r2 = "text/html"
            java.lang.String r4 = "UTF-8"
            r1.loadData(r3, r2, r4)     // Catch:{ RemoteException -> 0x000f, RuntimeException -> 0x000b }
        L_0x012a:
            r1 = 1
            r8 = r1
            goto L_0x013e
        L_0x012d:
            java.lang.String r1 = "No matching template id and mapper"
            goto L_0x001a
        L_0x0131:
            java.lang.String r1 = "No template ids present in mediation response"
            goto L_0x001a
        L_0x0135:
            r28.countDown()
            throw r1
        L_0x0139:
            java.lang.String r2 = "Unable to invoke load assets"
            com.google.android.gms.internal.zzafj.zzc(r2, r1)
        L_0x013e:
            if (r8 != 0) goto L_0x0143
            r28.countDown()
        L_0x0143:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.zzaq.zza(com.google.android.gms.internal.zzama, com.google.android.gms.internal.zztt, java.util.concurrent.CountDownLatch):boolean");
    }

    private static String zzb(zzoy zzoy) {
        try {
            IObjectWrapper zzjl = zzoy.zzjl();
            if (zzjl == null) {
                zzafj.zzco("Drawable is null. Returning empty string");
                return "";
            }
            Drawable drawable = (Drawable) zzn.zzx(zzjl);
            if (drawable instanceof BitmapDrawable) {
                return zza(((BitmapDrawable) drawable).getBitmap());
            }
            zzafj.zzco("Drawable is not an instance of BitmapDrawable. Returning empty string");
            return "";
        } catch (RemoteException unused) {
            zzafj.zzco("Unable to get drawable. Returning empty string");
            return "";
        }
    }

    /* access modifiers changed from: private */
    public static void zzc(zzama zzama) {
        View.OnClickListener onClickListener = zzama.getOnClickListener();
        if (onClickListener == null) {
            return;
        }
        if (zzama == null) {
            throw null;
        }
        onClickListener.onClick((View) zzama);
    }

    /* access modifiers changed from: private */
    @Nullable
    public static zzoy zzd(Object obj) {
        if (obj instanceof IBinder) {
            return zzoz.zzk((IBinder) obj);
        }
        return null;
    }

    @Nullable
    public static View zze(@Nullable zzaeu zzaeu) {
        if (zzaeu == null) {
            zzafj.e("AdState is null");
            return null;
        } else if (!zzf(zzaeu) || zzaeu.zzchj == null) {
            try {
                IObjectWrapper view = zzaeu.zzcde != null ? zzaeu.zzcde.getView() : null;
                if (view != null) {
                    return (View) zzn.zzx(view);
                }
                zzafj.zzco("View in mediation adapter is null.");
                return null;
            } catch (RemoteException e) {
                zzafj.zzc("Could not get View from mediation adapter.", e);
                return null;
            }
        } else {
            zzama zzama = zzaeu.zzchj;
            if (zzama != null) {
                return (View) zzama;
            }
            throw null;
        }
    }

    public static boolean zzf(@Nullable zzaeu zzaeu) {
        return (zzaeu == null || !zzaeu.zzcng || zzaeu.zzcdd == null || zzaeu.zzcdd.zzcbn == null) ? false : true;
    }
}
