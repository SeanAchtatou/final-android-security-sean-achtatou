package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;

public interface zzbph extends IInterface {
    void onError(Status status) throws RemoteException;

    void onSuccess() throws RemoteException;

    void zza(zzbps zzbps) throws RemoteException;

    void zza(zzbpu zzbpu) throws RemoteException;

    void zza(zzbpw zzbpw) throws RemoteException;

    void zza(zzbpy zzbpy) throws RemoteException;

    void zza(zzbqe zzbqe) throws RemoteException;

    void zza(zzbqg zzbqg) throws RemoteException;

    void zza(zzbqj zzbqj) throws RemoteException;

    void zza(zzbql zzbql) throws RemoteException;
}
