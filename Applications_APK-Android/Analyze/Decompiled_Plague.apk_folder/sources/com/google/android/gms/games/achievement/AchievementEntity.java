package com.google.android.gms.games.achievement;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzbi;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class AchievementEntity extends zzc implements Achievement {
    public static final Parcelable.Creator<AchievementEntity> CREATOR = new zza();
    private final String mName;
    private final int mState;
    private final String zzdou;
    private final int zzefe;
    private final String zzhly;
    private final Uri zzhlz;
    private final String zzhma;
    private final Uri zzhmb;
    private final String zzhmc;
    private final int zzhmd;
    private final String zzhme;
    private final PlayerEntity zzhmf;
    private final int zzhmg;
    private final String zzhmh;
    private final long zzhmi;
    private final long zzhmj;

    public AchievementEntity(Achievement achievement) {
        this.zzhly = achievement.getAchievementId();
        this.zzefe = achievement.getType();
        this.mName = achievement.getName();
        this.zzdou = achievement.getDescription();
        this.zzhlz = achievement.getUnlockedImageUri();
        this.zzhma = achievement.getUnlockedImageUrl();
        this.zzhmb = achievement.getRevealedImageUri();
        this.zzhmc = achievement.getRevealedImageUrl();
        this.zzhmf = (PlayerEntity) achievement.getPlayer().freeze();
        this.mState = achievement.getState();
        this.zzhmi = achievement.getLastUpdatedTimestamp();
        this.zzhmj = achievement.getXpValue();
        if (achievement.getType() == 1) {
            this.zzhmd = achievement.getTotalSteps();
            this.zzhme = achievement.getFormattedTotalSteps();
            this.zzhmg = achievement.getCurrentSteps();
            this.zzhmh = achievement.getFormattedCurrentSteps();
        } else {
            this.zzhmd = 0;
            this.zzhme = null;
            this.zzhmg = 0;
            this.zzhmh = null;
        }
        com.google.android.gms.common.internal.zzc.zzu(this.zzhly);
        com.google.android.gms.common.internal.zzc.zzu(this.zzdou);
    }

    AchievementEntity(String str, int i, String str2, String str3, Uri uri, String str4, Uri uri2, String str5, int i2, String str6, PlayerEntity playerEntity, int i3, int i4, String str7, long j, long j2) {
        this.zzhly = str;
        this.zzefe = i;
        this.mName = str2;
        this.zzdou = str3;
        this.zzhlz = uri;
        this.zzhma = str4;
        this.zzhmb = uri2;
        this.zzhmc = str5;
        this.zzhmd = i2;
        this.zzhme = str6;
        this.zzhmf = playerEntity;
        this.mState = i3;
        this.zzhmg = i4;
        this.zzhmh = str7;
        this.zzhmi = j;
        this.zzhmj = j2;
    }

    static String zza(Achievement achievement) {
        zzbi zzg = zzbg.zzw(achievement).zzg("Id", achievement.getAchievementId()).zzg("Type", Integer.valueOf(achievement.getType())).zzg("Name", achievement.getName()).zzg("Description", achievement.getDescription()).zzg("Player", achievement.getPlayer()).zzg("State", Integer.valueOf(achievement.getState()));
        if (achievement.getType() == 1) {
            zzg.zzg("CurrentSteps", Integer.valueOf(achievement.getCurrentSteps()));
            zzg.zzg("TotalSteps", Integer.valueOf(achievement.getTotalSteps()));
        }
        return zzg.toString();
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (!(obj instanceof Achievement)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Achievement achievement = (Achievement) obj;
        if (getType() == 1) {
            z2 = zzbg.equal(Integer.valueOf(achievement.getCurrentSteps()), Integer.valueOf(getCurrentSteps()));
            z = zzbg.equal(Integer.valueOf(achievement.getTotalSteps()), Integer.valueOf(getTotalSteps()));
        } else {
            z2 = true;
            z = true;
        }
        return zzbg.equal(achievement.getAchievementId(), getAchievementId()) && zzbg.equal(achievement.getName(), getName()) && zzbg.equal(Integer.valueOf(achievement.getType()), Integer.valueOf(getType())) && zzbg.equal(achievement.getDescription(), getDescription()) && zzbg.equal(Long.valueOf(achievement.getXpValue()), Long.valueOf(getXpValue())) && zzbg.equal(Integer.valueOf(achievement.getState()), Integer.valueOf(getState())) && zzbg.equal(Long.valueOf(achievement.getLastUpdatedTimestamp()), Long.valueOf(getLastUpdatedTimestamp())) && zzbg.equal(achievement.getPlayer(), getPlayer()) && z2 && z;
    }

    public final Achievement freeze() {
        return this;
    }

    public final String getAchievementId() {
        return this.zzhly;
    }

    public final int getCurrentSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        com.google.android.gms.common.internal.zzc.checkState(z);
        return this.zzhmg;
    }

    public final String getDescription() {
        return this.zzdou;
    }

    public final void getDescription(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.zzdou, charArrayBuffer);
    }

    public final String getFormattedCurrentSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        com.google.android.gms.common.internal.zzc.checkState(z);
        return this.zzhmh;
    }

    public final void getFormattedCurrentSteps(CharArrayBuffer charArrayBuffer) {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        com.google.android.gms.common.internal.zzc.checkState(z);
        zzg.zzb(this.zzhmh, charArrayBuffer);
    }

    public final String getFormattedTotalSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        com.google.android.gms.common.internal.zzc.checkState(z);
        return this.zzhme;
    }

    public final void getFormattedTotalSteps(CharArrayBuffer charArrayBuffer) {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        com.google.android.gms.common.internal.zzc.checkState(z);
        zzg.zzb(this.zzhme, charArrayBuffer);
    }

    public final long getLastUpdatedTimestamp() {
        return this.zzhmi;
    }

    public final String getName() {
        return this.mName;
    }

    public final void getName(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.mName, charArrayBuffer);
    }

    public final Player getPlayer() {
        return this.zzhmf;
    }

    public final Uri getRevealedImageUri() {
        return this.zzhmb;
    }

    public final String getRevealedImageUrl() {
        return this.zzhmc;
    }

    public final int getState() {
        return this.mState;
    }

    public final int getTotalSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        com.google.android.gms.common.internal.zzc.checkState(z);
        return this.zzhmd;
    }

    public final int getType() {
        return this.zzefe;
    }

    public final Uri getUnlockedImageUri() {
        return this.zzhlz;
    }

    public final String getUnlockedImageUrl() {
        return this.zzhma;
    }

    public final long getXpValue() {
        return this.zzhmj;
    }

    public final int hashCode() {
        int i;
        int i2;
        if (getType() == 1) {
            i2 = getCurrentSteps();
            i = getTotalSteps();
        } else {
            i2 = 0;
            i = 0;
        }
        return Arrays.hashCode(new Object[]{getAchievementId(), getName(), Integer.valueOf(getType()), getDescription(), Long.valueOf(getXpValue()), Integer.valueOf(getState()), Long.valueOf(getLastUpdatedTimestamp()), getPlayer(), Integer.valueOf(i2), Integer.valueOf(i)});
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String toString() {
        return zza(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.Player, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, getAchievementId(), false);
        zzbem.zzc(parcel, 2, getType());
        zzbem.zza(parcel, 3, getName(), false);
        zzbem.zza(parcel, 4, getDescription(), false);
        zzbem.zza(parcel, 5, (Parcelable) getUnlockedImageUri(), i, false);
        zzbem.zza(parcel, 6, getUnlockedImageUrl(), false);
        zzbem.zza(parcel, 7, (Parcelable) getRevealedImageUri(), i, false);
        zzbem.zza(parcel, 8, getRevealedImageUrl(), false);
        zzbem.zzc(parcel, 9, this.zzhmd);
        zzbem.zza(parcel, 10, this.zzhme, false);
        zzbem.zza(parcel, 11, (Parcelable) getPlayer(), i, false);
        zzbem.zzc(parcel, 12, getState());
        zzbem.zzc(parcel, 13, this.zzhmg);
        zzbem.zza(parcel, 14, this.zzhmh, false);
        zzbem.zza(parcel, 15, getLastUpdatedTimestamp());
        zzbem.zza(parcel, 16, getXpValue());
        zzbem.zzai(parcel, zze);
    }
}
