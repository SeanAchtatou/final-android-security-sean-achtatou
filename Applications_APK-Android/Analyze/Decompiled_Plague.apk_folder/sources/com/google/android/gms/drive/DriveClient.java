package com.google.android.gms.drive;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.tasks.Task;

public abstract class DriveClient extends GoogleApi<Drive.zza> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$zza):void
     arg types: [android.app.Activity, com.google.android.gms.common.api.Api<com.google.android.gms.drive.Drive$zza>, com.google.android.gms.drive.Drive$zza, com.google.android.gms.common.api.GoogleApi$zza]
     candidates:
      com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.internal.zzdb):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.content.Context, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$zza):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.content.Context, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.internal.zzdb):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$zza):void */
    public DriveClient(@NonNull Activity activity, @Nullable Drive.zza zza) {
        super(activity, (Api) Drive.zzggi, (Api.ApiOptions) zza, GoogleApi.zza.zzfjp);
    }

    public DriveClient(@NonNull Context context, @NonNull Drive.zza zza) {
        super(context, Drive.zzggi, zza, GoogleApi.zza.zzfjp);
    }

    public abstract Task<DriveId> getDriveId(@NonNull String str);

    public abstract Task<TransferPreferences> getUploadPreferences();

    public abstract Task<IntentSender> newCreateFileActivityIntentSender(CreateFileActivityOptions createFileActivityOptions);

    public abstract Task<IntentSender> newOpenFileActivityIntentSender(OpenFileActivityOptions openFileActivityOptions);

    public abstract Task<Void> requestSync();

    public abstract Task<Void> setUploadPreferences(@NonNull TransferPreferences transferPreferences);
}
