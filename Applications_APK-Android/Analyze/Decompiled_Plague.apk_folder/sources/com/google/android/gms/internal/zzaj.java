package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public final class zzaj implements zzam {
    private HttpClient zzca;

    public zzaj(HttpClient httpClient) {
        this.zzca = httpClient;
    }

    private static void zza(HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase, zzp<?> zzp) throws zza {
        byte[] zzg = zzp.zzg();
        if (zzg != null) {
            httpEntityEnclosingRequestBase.setEntity(new ByteArrayEntity(zzg));
        }
    }

    private static void zza(HttpUriRequest httpUriRequest, Map<String, String> map) {
        for (String next : map.keySet()) {
            httpUriRequest.setHeader(next, map.get(next));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzaj.zza(org.apache.http.client.methods.HttpEntityEnclosingRequestBase, com.google.android.gms.internal.zzp<?>):void
     arg types: [com.google.android.gms.internal.zzak, com.google.android.gms.internal.zzp<?>]
     candidates:
      com.google.android.gms.internal.zzaj.zza(org.apache.http.client.methods.HttpUriRequest, java.util.Map<java.lang.String, java.lang.String>):void
      com.google.android.gms.internal.zzaj.zza(com.google.android.gms.internal.zzp<?>, java.util.Map<java.lang.String, java.lang.String>):org.apache.http.HttpResponse
      com.google.android.gms.internal.zzam.zza(com.google.android.gms.internal.zzp<?>, java.util.Map<java.lang.String, java.lang.String>):org.apache.http.HttpResponse
      com.google.android.gms.internal.zzaj.zza(org.apache.http.client.methods.HttpEntityEnclosingRequestBase, com.google.android.gms.internal.zzp<?>):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzaj.zza(org.apache.http.client.methods.HttpUriRequest, java.util.Map<java.lang.String, java.lang.String>):void
     arg types: [com.google.android.gms.internal.zzak, java.util.Map<java.lang.String, java.lang.String>]
     candidates:
      com.google.android.gms.internal.zzaj.zza(org.apache.http.client.methods.HttpEntityEnclosingRequestBase, com.google.android.gms.internal.zzp<?>):void
      com.google.android.gms.internal.zzaj.zza(com.google.android.gms.internal.zzp<?>, java.util.Map<java.lang.String, java.lang.String>):org.apache.http.HttpResponse
      com.google.android.gms.internal.zzam.zza(com.google.android.gms.internal.zzp<?>, java.util.Map<java.lang.String, java.lang.String>):org.apache.http.HttpResponse
      com.google.android.gms.internal.zzaj.zza(org.apache.http.client.methods.HttpUriRequest, java.util.Map<java.lang.String, java.lang.String>):void */
    public final HttpResponse zza(zzp<?> zzp, Map<String, String> map) throws IOException, zza {
        zzak zzak;
        switch (zzp.getMethod()) {
            case -1:
                zzak = new HttpGet(zzp.getUrl());
                break;
            case 0:
                zzak = new HttpGet(zzp.getUrl());
                break;
            case 1:
                zzak = new HttpPost(zzp.getUrl());
                zzak.addHeader("Content-Type", zzp.zzf());
                zza((HttpEntityEnclosingRequestBase) zzak, zzp);
                break;
            case 2:
                zzak = new HttpPut(zzp.getUrl());
                zzak.addHeader("Content-Type", zzp.zzf());
                zza((HttpEntityEnclosingRequestBase) zzak, zzp);
                break;
            case 3:
                zzak = new HttpDelete(zzp.getUrl());
                break;
            case 4:
                zzak = new HttpHead(zzp.getUrl());
                break;
            case 5:
                zzak = new HttpOptions(zzp.getUrl());
                break;
            case 6:
                zzak = new HttpTrace(zzp.getUrl());
                break;
            case 7:
                zzak = new zzak(zzp.getUrl());
                zzak.addHeader("Content-Type", zzp.zzf());
                zza((HttpEntityEnclosingRequestBase) zzak, zzp);
                break;
            default:
                throw new IllegalStateException("Unknown request method.");
        }
        zza((HttpUriRequest) zzak, map);
        zza((HttpUriRequest) zzak, zzp.getHeaders());
        HttpParams params = zzak.getParams();
        int zzi = zzp.zzi();
        HttpConnectionParams.setConnectionTimeout(params, 5000);
        HttpConnectionParams.setSoTimeout(params, zzi);
        return this.zzca.execute(zzak);
    }
}
