package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.drive.DriveId;
import java.util.Arrays;

public final class zzbjp {
    private final int zzbzn;
    private final DriveId zzgfy;
    private final int zzgjd;

    public zzbjp(zzbjr zzbjr) {
        this.zzgfy = zzbjr.zzgfy;
        this.zzgjd = zzbjr.zzgjd;
        this.zzbzn = zzbjr.zzbzn;
    }

    public final boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        zzbjp zzbjp = (zzbjp) obj;
        return zzbg.equal(this.zzgfy, zzbjp.zzgfy) && this.zzgjd == zzbjp.zzgjd && this.zzbzn == zzbjp.zzbzn;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.zzgfy, Integer.valueOf(this.zzgjd), Integer.valueOf(this.zzbzn)});
    }

    public final String toString() {
        return String.format("FileTransferState[TransferType: %d, DriveId: %s, status: %d]", Integer.valueOf(this.zzgjd), this.zzgfy, Integer.valueOf(this.zzbzn));
    }
}
