package com.google.android.gms.internal;

import java.io.IOException;

public final class zzba extends zzfhe<zzba> {
    public byte[] data = null;
    public byte[] zzge = null;
    public byte[] zzgf = null;
    public byte[] zzgg = null;

    public zzba() {
        this.zzpai = -1;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 10) {
                this.data = zzfhb.readBytes();
            } else if (zzcts == 18) {
                this.zzge = zzfhb.readBytes();
            } else if (zzcts == 26) {
                this.zzgf = zzfhb.readBytes();
            } else if (zzcts == 34) {
                this.zzgg = zzfhb.readBytes();
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.data != null) {
            zzfhc.zzc(1, this.data);
        }
        if (this.zzge != null) {
            zzfhc.zzc(2, this.zzge);
        }
        if (this.zzgf != null) {
            zzfhc.zzc(3, this.zzgf);
        }
        if (this.zzgg != null) {
            zzfhc.zzc(4, this.zzgg);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.data != null) {
            zzo += zzfhc.zzd(1, this.data);
        }
        if (this.zzge != null) {
            zzo += zzfhc.zzd(2, this.zzge);
        }
        if (this.zzgf != null) {
            zzo += zzfhc.zzd(3, this.zzgf);
        }
        return this.zzgg != null ? zzo + zzfhc.zzd(4, this.zzgg) : zzo;
    }
}
