package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzfhk {
    protected volatile int zzpai = -1;

    public static final <T extends zzfhk> T zza(zzfhk zzfhk, byte[] bArr) throws zzfhj {
        return zza(zzfhk, bArr, 0, bArr.length);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private static <T extends com.google.android.gms.internal.zzfhk> T zza(T r0, byte[] r1, int r2, int r3) throws com.google.android.gms.internal.zzfhj {
        /*
            r2 = 0
            com.google.android.gms.internal.zzfhb r1 = com.google.android.gms.internal.zzfhb.zzn(r1, r2, r3)     // Catch:{ zzfhj -> 0x0015, IOException -> 0x000c }
            r0.zza(r1)     // Catch:{ zzfhj -> 0x0015, IOException -> 0x000c }
            r1.zzkf(r2)     // Catch:{ zzfhj -> 0x0015, IOException -> 0x000c }
            return r0
        L_0x000c:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r2 = "Reading from a byte array threw an IOException (should never happen)."
            r1.<init>(r2, r0)
            throw r1
        L_0x0015:
            r0 = move-exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfhk.zza(com.google.android.gms.internal.zzfhk, byte[], int, int):com.google.android.gms.internal.zzfhk");
    }

    public static final byte[] zzc(zzfhk zzfhk) {
        byte[] bArr = new byte[zzfhk.zzhl()];
        try {
            zzfhc zzo = zzfhc.zzo(bArr, 0, bArr.length);
            zzfhk.zza(zzo);
            zzo.zzcus();
            return bArr;
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public String toString() {
        return zzfhl.zzd(this);
    }

    public abstract zzfhk zza(zzfhb zzfhb) throws IOException;

    public void zza(zzfhc zzfhc) throws IOException {
    }

    /* renamed from: zzcxf */
    public zzfhk clone() throws CloneNotSupportedException {
        return (zzfhk) super.clone();
    }

    public final int zzcxl() {
        if (this.zzpai < 0) {
            zzhl();
        }
        return this.zzpai;
    }

    public final int zzhl() {
        int zzo = zzo();
        this.zzpai = zzo;
        return zzo;
    }

    /* access modifiers changed from: protected */
    public int zzo() {
        return 0;
    }
}
