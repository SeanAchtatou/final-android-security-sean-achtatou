package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;

public final class zzjy extends zzed implements zzjw {
    zzjy(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
    }

    public final void zza(PublisherAdViewOptions publisherAdViewOptions) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, publisherAdViewOptions);
        zzb(9, zzaz);
    }

    public final void zza(zzom zzom) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzom);
        zzb(6, zzaz);
    }

    public final void zza(zzpy zzpy) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzpy);
        zzb(3, zzaz);
    }

    public final void zza(zzqb zzqb) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzqb);
        zzb(4, zzaz);
    }

    public final void zza(zzqk zzqk, zziw zziw) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzqk);
        zzef.zza(zzaz, zziw);
        zzb(8, zzaz);
    }

    public final void zza(String str, zzqh zzqh, zzqe zzqe) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeString(str);
        zzef.zza(zzaz, zzqh);
        zzef.zza(zzaz, zzqe);
        zzb(5, zzaz);
    }

    public final void zzb(zzjq zzjq) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzjq);
        zzb(2, zzaz);
    }

    public final void zzb(zzkm zzkm) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzkm);
        zzb(7, zzaz);
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzjt zzdc() throws android.os.RemoteException {
        /*
            r4 = this;
            android.os.Parcel r0 = r4.zzaz()
            r1 = 1
            android.os.Parcel r0 = r4.zza(r1, r0)
            android.os.IBinder r1 = r0.readStrongBinder()
            if (r1 != 0) goto L_0x0011
            r1 = 0
            goto L_0x0025
        L_0x0011:
            java.lang.String r2 = "com.google.android.gms.ads.internal.client.IAdLoader"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r3 = r2 instanceof com.google.android.gms.internal.zzjt
            if (r3 == 0) goto L_0x001f
            r1 = r2
            com.google.android.gms.internal.zzjt r1 = (com.google.android.gms.internal.zzjt) r1
            goto L_0x0025
        L_0x001f:
            com.google.android.gms.internal.zzjv r2 = new com.google.android.gms.internal.zzjv
            r2.<init>(r1)
            r1 = r2
        L_0x0025:
            r0.recycle()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzjy.zzdc():com.google.android.gms.internal.zzjt");
    }
}
