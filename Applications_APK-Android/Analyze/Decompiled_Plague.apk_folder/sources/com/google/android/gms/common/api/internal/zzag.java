package com.google.android.gms.common.api.internal;

import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.AvailabilityException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import java.util.Collections;
import java.util.Map;

final class zzag implements OnCompleteListener<Void> {
    private /* synthetic */ zzad zzfns;
    private zzcx zzfnt;

    zzag(zzad zzad, zzcx zzcx) {
        this.zzfns = zzad;
        this.zzfnt = zzcx;
    }

    /* access modifiers changed from: package-private */
    public final void cancel() {
        this.zzfnt.zzaav();
    }

    public final void onComplete(@NonNull Task<Void> task) {
        zzcx zzcx;
        Map zzg;
        this.zzfns.zzfmy.lock();
        try {
            if (!this.zzfns.zzfnn) {
                zzcx = this.zzfnt;
            } else {
                if (task.isSuccessful()) {
                    Map unused = this.zzfns.zzfnp = new ArrayMap(this.zzfns.zzfnf.size());
                    for (zzac zzaga : this.zzfns.zzfnf.values()) {
                        this.zzfns.zzfnp.put(zzaga.zzaga(), ConnectionResult.zzfhy);
                    }
                } else if (task.getException() instanceof AvailabilityException) {
                    AvailabilityException availabilityException = (AvailabilityException) task.getException();
                    if (this.zzfns.zzfnl) {
                        Map unused2 = this.zzfns.zzfnp = new ArrayMap(this.zzfns.zzfnf.size());
                        for (zzac zzac : this.zzfns.zzfnf.values()) {
                            zzh zzaga2 = zzac.zzaga();
                            ConnectionResult connectionResult = availabilityException.getConnectionResult(zzac);
                            if (this.zzfns.zza(zzac, connectionResult)) {
                                zzg = this.zzfns.zzfnp;
                                connectionResult = new ConnectionResult(16);
                            } else {
                                zzg = this.zzfns.zzfnp;
                            }
                            zzg.put(zzaga2, connectionResult);
                        }
                    } else {
                        Map unused3 = this.zzfns.zzfnp = availabilityException.zzafw();
                    }
                } else {
                    Log.e("ConnectionlessGAC", "Unexpected availability exception", task.getException());
                    Map unused4 = this.zzfns.zzfnp = Collections.emptyMap();
                }
                if (this.zzfns.isConnected()) {
                    this.zzfns.zzfno.putAll(this.zzfns.zzfnp);
                    if (this.zzfns.zzahh() == null) {
                        this.zzfns.zzahf();
                        this.zzfns.zzahg();
                        this.zzfns.zzfnj.signalAll();
                    }
                }
                zzcx = this.zzfnt;
            }
            zzcx.zzaav();
        } finally {
            this.zzfns.zzfmy.unlock();
        }
    }
}
