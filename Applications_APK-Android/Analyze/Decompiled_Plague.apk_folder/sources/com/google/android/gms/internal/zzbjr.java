package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.drive.DriveId;
import java.util.Arrays;

public final class zzbjr extends zzbej {
    public static final Parcelable.Creator<zzbjr> CREATOR = new zzbjs();
    final int zzbzn;
    final DriveId zzgfy;
    final int zzgjd;
    final long zzgjg;
    final long zzgjh;

    public zzbjr(int i, DriveId driveId, int i2, long j, long j2) {
        this.zzgjd = i;
        this.zzgfy = driveId;
        this.zzbzn = i2;
        this.zzgjg = j;
        this.zzgjh = j2;
    }

    public final boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        zzbjr zzbjr = (zzbjr) obj;
        return this.zzgjd == zzbjr.zzgjd && zzbg.equal(this.zzgfy, zzbjr.zzgfy) && this.zzbzn == zzbjr.zzbzn && this.zzgjg == zzbjr.zzgjg && this.zzgjh == zzbjr.zzgjh;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.zzgjd), this.zzgfy, Integer.valueOf(this.zzbzn), Long.valueOf(this.zzgjg), Long.valueOf(this.zzgjh)});
    }

    public final String toString() {
        return String.format("TransferProgressData[TransferType: %d, DriveId: %s, status: %d, bytes transferred: %d, total bytes: %d]", Integer.valueOf(this.zzgjd), this.zzgfy, Integer.valueOf(this.zzbzn), Long.valueOf(this.zzgjg), Long.valueOf(this.zzgjh));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.DriveId, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 2, this.zzgjd);
        zzbem.zza(parcel, 3, (Parcelable) this.zzgfy, i, false);
        zzbem.zzc(parcel, 4, this.zzbzn);
        zzbem.zza(parcel, 5, this.zzgjg);
        zzbem.zza(parcel, 6, this.zzgjh);
        zzbem.zzai(parcel, zze);
    }
}
