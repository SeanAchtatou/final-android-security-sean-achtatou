package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdqk;
import java.io.IOException;

public final class zzdqi extends zzfee<zzdqi, zza> implements zzffk {
    private static volatile zzffm<zzdqi> zzbas;
    /* access modifiers changed from: private */
    public static final zzdqi zzlqv;
    private int zzlql;
    private zzdqk zzlqt;

    public static final class zza extends zzfef<zzdqi, zza> implements zzffk {
        private zza() {
            super(zzdqi.zzlqv);
        }

        /* synthetic */ zza(zzdqj zzdqj) {
            this();
        }
    }

    static {
        zzdqi zzdqi = new zzdqi();
        zzlqv = zzdqi;
        zzdqi.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdqi.zzpbs.zzbim();
    }

    private zzdqi() {
    }

    public static zzdqi zzs(zzfdh zzfdh) throws zzfew {
        return (zzdqi) zzfee.zza(zzlqv, zzfdh);
    }

    public final int getKeySize() {
        return this.zzlql;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdqk.zza zza2;
        boolean z = false;
        switch (zzdqj.zzbaq[i - 1]) {
            case 1:
                return new zzdqi();
            case 2:
                return zzlqv;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdqi zzdqi = (zzdqi) obj2;
                this.zzlqt = (zzdqk) zzfen.zza(this.zzlqt, zzdqi.zzlqt);
                boolean z2 = this.zzlql != 0;
                int i2 = this.zzlql;
                if (zzdqi.zzlql != 0) {
                    z = true;
                }
                this.zzlql = zzfen.zza(z2, i2, z, zzdqi.zzlql);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 10) {
                                    if (this.zzlqt != null) {
                                        zzdqk zzdqk = this.zzlqt;
                                        zzfef zzfef = (zzfef) zzdqk.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdqk);
                                        zza2 = (zzdqk.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlqt = (zzdqk) zzfdq.zza(zzdqk.zzbmm(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlqt);
                                        this.zzlqt = (zzdqk) zza2.zzcvj();
                                    }
                                } else if (zzcts == 16) {
                                    this.zzlql = zzfdq.zzcub();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdqi.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlqv);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlqv;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlqt != null) {
            zzfdv.zza(1, this.zzlqt == null ? zzdqk.zzbmm() : this.zzlqt);
        }
        if (this.zzlql != 0) {
            zzfdv.zzab(2, this.zzlql);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzdqk zzbmk() {
        return this.zzlqt == null ? zzdqk.zzbmm() : this.zzlqt;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlqt != null) {
            i2 = 0 + zzfdv.zzb(1, this.zzlqt == null ? zzdqk.zzbmm() : this.zzlqt);
        }
        if (this.zzlql != 0) {
            i2 += zzfdv.zzae(2, this.zzlql);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
