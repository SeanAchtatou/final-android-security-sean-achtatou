package com.google.android.gms.drive;

import android.os.Parcel;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbej;

public abstract class zzy extends zzbej {
    private volatile transient boolean zzgif = false;

    public void writeToParcel(Parcel parcel, int i) {
        zzbq.checkState(!this.zzgif);
        this.zzgif = true;
        zzaj(parcel, i);
    }

    /* access modifiers changed from: protected */
    public abstract void zzaj(Parcel parcel, int i);
}
