package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.DriveId;

final class zzbku extends zzblb {
    private /* synthetic */ String zzgkj;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbku(zzbkp zzbkp, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient);
        this.zzgkj = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbpb(DriveId.zzgq(this.zzgkj), false), new zzbkz(this));
    }
}
