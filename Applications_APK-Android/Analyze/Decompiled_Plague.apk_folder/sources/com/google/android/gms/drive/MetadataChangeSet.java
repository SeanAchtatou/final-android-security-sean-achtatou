package com.google.android.gms.drive;

import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.internal.zzbse;
import com.google.android.gms.internal.zzbsr;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

public final class MetadataChangeSet {
    public static final int CUSTOM_PROPERTY_SIZE_LIMIT_BYTES = 124;
    public static final int INDEXABLE_TEXT_SIZE_LIMIT_BYTES = 131072;
    public static final int MAX_PRIVATE_PROPERTIES_PER_RESOURCE_PER_APP = 30;
    public static final int MAX_PUBLIC_PROPERTIES_PER_RESOURCE = 30;
    public static final int MAX_TOTAL_PROPERTIES_PER_RESOURCE = 100;
    public static final MetadataChangeSet zzghi = new MetadataChangeSet(MetadataBundle.zzapd());
    private final MetadataBundle zzghj;

    public static class Builder {
        private final MetadataBundle zzghj = MetadataBundle.zzapd();
        private AppVisibleCustomProperties.zza zzghk;

        private final AppVisibleCustomProperties.zza zzaoa() {
            if (this.zzghk == null) {
                this.zzghk = new AppVisibleCustomProperties.zza();
            }
            return this.zzghk;
        }

        private static int zzgr(String str) {
            if (str == null) {
                return 0;
            }
            return str.getBytes().length;
        }

        private static void zzi(String str, int i, int i2) {
            zzbq.checkArgument(i2 <= i, String.format("%s must be no more than %d bytes, but is %d bytes.", str, Integer.valueOf(i), Integer.valueOf(i2)));
        }

        public MetadataChangeSet build() {
            if (this.zzghk != null) {
                this.zzghj.zzc(zzbse.zzgpt, this.zzghk.zzapa());
            }
            return new MetadataChangeSet(this.zzghj);
        }

        public Builder deleteCustomProperty(CustomPropertyKey customPropertyKey) {
            zzbq.checkNotNull(customPropertyKey, "key");
            zzaoa().zza(customPropertyKey, null);
            return this;
        }

        public Builder setCustomProperty(CustomPropertyKey customPropertyKey, String str) {
            zzbq.checkNotNull(customPropertyKey, "key");
            zzbq.checkNotNull(str, "value");
            zzi("The total size of key string and value string of a custom property", MetadataChangeSet.CUSTOM_PROPERTY_SIZE_LIMIT_BYTES, zzgr(customPropertyKey.getKey()) + zzgr(str));
            zzaoa().zza(customPropertyKey, str);
            return this;
        }

        public Builder setDescription(String str) {
            this.zzghj.zzc(zzbse.zzgpu, str);
            return this;
        }

        public Builder setIndexableText(String str) {
            zzi("Indexable text size", 131072, zzgr(str));
            this.zzghj.zzc(zzbse.zzgqa, str);
            return this;
        }

        public Builder setLastViewedByMeDate(Date date) {
            this.zzghj.zzc(zzbsr.zzgrk, date);
            return this;
        }

        public Builder setMimeType(@NonNull String str) {
            zzbq.checkNotNull(str);
            this.zzghj.zzc(zzbse.zzgqo, str);
            return this;
        }

        public Builder setPinned(boolean z) {
            this.zzghj.zzc(zzbse.zzgqg, Boolean.valueOf(z));
            return this;
        }

        public Builder setStarred(boolean z) {
            this.zzghj.zzc(zzbse.zzgqv, Boolean.valueOf(z));
            return this;
        }

        public Builder setTitle(@NonNull String str) {
            zzbq.checkNotNull(str, "Title cannot be null.");
            this.zzghj.zzc(zzbse.zzgqx, str);
            return this;
        }

        public Builder setViewed() {
            this.zzghj.zzc(zzbse.zzgqn, true);
            return this;
        }

        @Deprecated
        public Builder setViewed(boolean z) {
            if (z) {
                this.zzghj.zzc(zzbse.zzgqn, true);
                return this;
            }
            if (this.zzghj.zzd(zzbse.zzgqn)) {
                this.zzghj.zzc(zzbse.zzgqn);
            }
            return this;
        }
    }

    public MetadataChangeSet(MetadataBundle metadataBundle) {
        this.zzghj = metadataBundle.zzape();
    }

    public final Map<CustomPropertyKey, String> getCustomPropertyChangeMap() {
        AppVisibleCustomProperties appVisibleCustomProperties = (AppVisibleCustomProperties) this.zzghj.zza(zzbse.zzgpt);
        return appVisibleCustomProperties == null ? Collections.emptyMap() : appVisibleCustomProperties.zzaoz();
    }

    public final String getDescription() {
        return (String) this.zzghj.zza(zzbse.zzgpu);
    }

    public final String getIndexableText() {
        return (String) this.zzghj.zza(zzbse.zzgqa);
    }

    public final Date getLastViewedByMeDate() {
        return (Date) this.zzghj.zza(zzbsr.zzgrk);
    }

    public final String getMimeType() {
        return (String) this.zzghj.zza(zzbse.zzgqo);
    }

    public final String getTitle() {
        return (String) this.zzghj.zza(zzbse.zzgqx);
    }

    public final Boolean isPinned() {
        return (Boolean) this.zzghj.zza(zzbse.zzgqg);
    }

    public final Boolean isStarred() {
        return (Boolean) this.zzghj.zza(zzbse.zzgqv);
    }

    public final Boolean isViewed() {
        return (Boolean) this.zzghj.zza(zzbse.zzgqn);
    }

    public final <T> MetadataChangeSet zza(MetadataField<T> metadataField, T t) {
        MetadataChangeSet metadataChangeSet = new MetadataChangeSet(this.zzghj);
        metadataChangeSet.zzghj.zzc(metadataField, t);
        return metadataChangeSet;
    }

    public final MetadataBundle zzanz() {
        return this.zzghj;
    }
}
