package com.google.android.gms.internal;

import java.io.IOException;
import java.util.ArrayList;

final class zzffp extends zzfdh {
    /* access modifiers changed from: private */
    public static final int[] zzpdg;
    private final int zzpdh;
    /* access modifiers changed from: private */
    public final zzfdh zzpdi;
    /* access modifiers changed from: private */
    public final zzfdh zzpdj;
    private final int zzpdk;
    private final int zzpdl;

    static {
        ArrayList arrayList = new ArrayList();
        int i = 1;
        int i2 = 1;
        while (i > 0) {
            arrayList.add(Integer.valueOf(i));
            int i3 = i2 + i;
            i2 = i;
            i = i3;
        }
        arrayList.add(Integer.MAX_VALUE);
        zzpdg = new int[arrayList.size()];
        for (int i4 = 0; i4 < zzpdg.length; i4++) {
            zzpdg[i4] = ((Integer) arrayList.get(i4)).intValue();
        }
    }

    private zzffp(zzfdh zzfdh, zzfdh zzfdh2) {
        this.zzpdi = zzfdh;
        this.zzpdj = zzfdh2;
        this.zzpdk = zzfdh.size();
        this.zzpdh = this.zzpdk + zzfdh2.size();
        this.zzpdl = Math.max(zzfdh.zzctm(), zzfdh2.zzctm()) + 1;
    }

    static zzfdh zza(zzfdh zzfdh, zzfdh zzfdh2) {
        if (zzfdh2.size() == 0) {
            return zzfdh;
        }
        if (zzfdh.size() == 0) {
            return zzfdh2;
        }
        int size = zzfdh.size() + zzfdh2.size();
        if (size < 128) {
            return zzb(zzfdh, zzfdh2);
        }
        if (zzfdh instanceof zzffp) {
            zzffp zzffp = (zzffp) zzfdh;
            if (zzffp.zzpdj.size() + zzfdh2.size() < 128) {
                return new zzffp(zzffp.zzpdi, zzb(zzffp.zzpdj, zzfdh2));
            } else if (zzffp.zzpdi.zzctm() > zzffp.zzpdj.zzctm() && zzffp.zzctm() > zzfdh2.zzctm()) {
                return new zzffp(zzffp.zzpdi, new zzffp(zzffp.zzpdj, zzfdh2));
            }
        }
        return size >= zzpdg[Math.max(zzfdh.zzctm(), zzfdh2.zzctm()) + 1] ? new zzffp(zzfdh, zzfdh2) : new zzffr().zzc(zzfdh, zzfdh2);
    }

    private static zzfdh zzb(zzfdh zzfdh, zzfdh zzfdh2) {
        int size = zzfdh.size();
        int size2 = zzfdh2.size();
        byte[] bArr = new byte[(size + size2)];
        zzfdh.zza(bArr, 0, 0, size);
        zzfdh2.zza(bArr, 0, size, size2);
        return zzfdh.zzaz(bArr);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfdh)) {
            return false;
        }
        zzfdh zzfdh = (zzfdh) obj;
        if (this.zzpdh != zzfdh.size()) {
            return false;
        }
        if (this.zzpdh == 0) {
            return true;
        }
        int zzcto = zzcto();
        int zzcto2 = zzfdh.zzcto();
        if (zzcto != 0 && zzcto2 != 0 && zzcto != zzcto2) {
            return false;
        }
        zzffs zzffs = new zzffs(this);
        zzfdn zzfdn = (zzfdn) zzffs.next();
        zzffs zzffs2 = new zzffs(zzfdh);
        zzfdn zzfdn2 = (zzfdn) zzffs2.next();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int size = zzfdn.size() - i;
            int size2 = zzfdn2.size() - i2;
            int min = Math.min(size, size2);
            if (!(i == 0 ? zzfdn.zza(zzfdn2, i2, min) : zzfdn2.zza(zzfdn, i, min))) {
                return false;
            }
            i3 += min;
            if (i3 < this.zzpdh) {
                if (min == size) {
                    zzfdn = (zzfdn) zzffs.next();
                    i = 0;
                } else {
                    i += min;
                }
                if (min == size2) {
                    zzfdn2 = (zzfdn) zzffs2.next();
                    i2 = 0;
                } else {
                    i2 += min;
                }
            } else if (i3 == this.zzpdh) {
                return true;
            } else {
                throw new IllegalStateException();
            }
        }
    }

    public final int size() {
        return this.zzpdh;
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzfdg zzfdg) throws IOException {
        this.zzpdi.zza(zzfdg);
        this.zzpdj.zza(zzfdg);
    }

    /* access modifiers changed from: protected */
    public final void zzb(byte[] bArr, int i, int i2, int i3) {
        if (i + i3 <= this.zzpdk) {
            this.zzpdi.zzb(bArr, i, i2, i3);
        } else if (i >= this.zzpdk) {
            this.zzpdj.zzb(bArr, i - this.zzpdk, i2, i3);
        } else {
            int i4 = this.zzpdk - i;
            this.zzpdi.zzb(bArr, i, i2, i4);
            this.zzpdj.zzb(bArr, 0, i2 + i4, i3 - i4);
        }
    }

    public final zzfdq zzctl() {
        return zzfdq.zzi(new zzfft(this));
    }

    /* access modifiers changed from: protected */
    public final int zzctm() {
        return this.zzpdl;
    }

    /* access modifiers changed from: protected */
    public final boolean zzctn() {
        return this.zzpdh >= zzpdg[this.zzpdl];
    }

    /* access modifiers changed from: protected */
    public final int zzg(int i, int i2, int i3) {
        if (i2 + i3 <= this.zzpdk) {
            return this.zzpdi.zzg(i, i2, i3);
        }
        if (i2 >= this.zzpdk) {
            return this.zzpdj.zzg(i, i2 - this.zzpdk, i3);
        }
        int i4 = this.zzpdk - i2;
        return this.zzpdj.zzg(this.zzpdi.zzg(i, i2, i4), 0, i3 - i4);
    }

    public final byte zzkd(int i) {
        zzfdh zzfdh;
        zzy(i, this.zzpdh);
        if (i < this.zzpdk) {
            zzfdh = this.zzpdi;
        } else {
            zzfdh = this.zzpdj;
            i -= this.zzpdk;
        }
        return zzfdh.zzkd(i);
    }

    public final zzfdh zzx(int i, int i2) {
        zzfdh zzfdh;
        int zzh = zzh(i, i2, this.zzpdh);
        if (zzh == 0) {
            return zzfdh.zzpal;
        }
        if (zzh == this.zzpdh) {
            return this;
        }
        if (i2 <= this.zzpdk) {
            zzfdh = this.zzpdi;
        } else if (i >= this.zzpdk) {
            zzfdh = this.zzpdj;
            i -= this.zzpdk;
            i2 -= this.zzpdk;
        } else {
            zzfdh zzfdh2 = this.zzpdi;
            return new zzffp(zzfdh2.zzx(i, zzfdh2.size()), this.zzpdj.zzx(0, i2 - this.zzpdk));
        }
        return zzfdh.zzx(i, i2);
    }
}
