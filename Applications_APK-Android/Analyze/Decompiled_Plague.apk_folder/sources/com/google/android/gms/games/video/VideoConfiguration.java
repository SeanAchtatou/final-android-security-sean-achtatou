package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class VideoConfiguration extends zzbej {
    public static final int CAPTURE_MODE_FILE = 0;
    public static final int CAPTURE_MODE_STREAM = 1;
    public static final int CAPTURE_MODE_UNKNOWN = -1;
    public static final Parcelable.Creator<VideoConfiguration> CREATOR = new zzb();
    public static final int NUM_CAPTURE_MODE = 2;
    public static final int NUM_QUALITY_LEVEL = 4;
    public static final int QUALITY_LEVEL_FULLHD = 3;
    public static final int QUALITY_LEVEL_HD = 1;
    public static final int QUALITY_LEVEL_SD = 0;
    public static final int QUALITY_LEVEL_UNKNOWN = -1;
    public static final int QUALITY_LEVEL_XHD = 2;
    private final int zzhye;
    private final int zzhyl;
    private final String zzhym;
    private final String zzhyn;
    private final String zzhyo;
    private final String zzhyp;
    private final boolean zzhyq;

    public static final class Builder {
        private int zzhye;
        private int zzhyl;
        private String zzhym = null;
        private String zzhyn = null;
        private String zzhyo = null;
        private String zzhyp = null;
        private boolean zzhyq = true;

        public Builder(int i, int i2) {
            this.zzhyl = i;
            this.zzhye = i2;
        }

        public final VideoConfiguration build() {
            return new VideoConfiguration(this.zzhyl, this.zzhye, null, null, null, null, this.zzhyq);
        }

        public final Builder setCaptureMode(int i) {
            this.zzhye = i;
            return this;
        }

        public final Builder setQualityLevel(int i) {
            this.zzhyl = i;
            return this;
        }
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface ValidCaptureModes {
    }

    public VideoConfiguration(int i, int i2, String str, String str2, String str3, String str4, boolean z) {
        zzbq.checkArgument(isValidQualityLevel(i, false));
        zzbq.checkArgument(isValidCaptureMode(i2, false));
        this.zzhyl = i;
        this.zzhye = i2;
        this.zzhyq = z;
        boolean z2 = true;
        if (i2 == 1) {
            this.zzhyn = str2;
            this.zzhym = str;
            this.zzhyo = str3;
            this.zzhyp = str4;
            return;
        }
        zzbq.checkArgument(str2 == null, "Stream key should be null when not streaming");
        zzbq.checkArgument(str == null, "Stream url should be null when not streaming");
        zzbq.checkArgument(str3 == null, "Stream title should be null when not streaming");
        zzbq.checkArgument(str4 != null ? false : z2, "Stream description should be null when not streaming");
        this.zzhyn = null;
        this.zzhym = null;
        this.zzhyo = null;
        this.zzhyp = null;
    }

    public static boolean isValidCaptureMode(int i, boolean z) {
        switch (i) {
            case -1:
                return z;
            case 0:
            case 1:
                return true;
            default:
                return false;
        }
    }

    public static boolean isValidQualityLevel(int i, boolean z) {
        switch (i) {
            case -1:
                return z;
            case 0:
            case 1:
            case 2:
            case 3:
                return true;
            default:
                return false;
        }
    }

    public final int getCaptureMode() {
        return this.zzhye;
    }

    public final int getQualityLevel() {
        return this.zzhyl;
    }

    public final String getStreamUrl() {
        return this.zzhym;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 1, getQualityLevel());
        zzbem.zzc(parcel, 2, getCaptureMode());
        zzbem.zza(parcel, 3, getStreamUrl(), false);
        zzbem.zza(parcel, 4, this.zzhyn, false);
        zzbem.zza(parcel, 5, this.zzhyo, false);
        zzbem.zza(parcel, 6, this.zzhyp, false);
        zzbem.zza(parcel, 7, this.zzhyq);
        zzbem.zzai(parcel, zze);
    }
}
