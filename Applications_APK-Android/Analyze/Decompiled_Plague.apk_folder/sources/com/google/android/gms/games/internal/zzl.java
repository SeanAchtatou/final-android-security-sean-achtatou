package com.google.android.gms.games.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.AnnotatedData;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.TimeUnit;

final class zzl implements PendingResult.zza {
    private /* synthetic */ PendingResult zzfym;
    private /* synthetic */ TaskCompletionSource zzfyn;
    private /* synthetic */ zzbo zzhpj;

    zzl(zzbo zzbo, PendingResult pendingResult, TaskCompletionSource taskCompletionSource) {
        this.zzhpj = zzbo;
        this.zzfym = pendingResult;
        this.zzfyn = taskCompletionSource;
    }

    public final void zzr(@NonNull Status status) {
        boolean z = status.getStatusCode() == 3;
        Releasable releasable = (Releasable) this.zzhpj.zzb(this.zzfym.await(0, TimeUnit.MILLISECONDS));
        if (status.isSuccess() || z) {
            this.zzfyn.setResult(new AnnotatedData(releasable, z));
            return;
        }
        if (releasable != null) {
            releasable.release();
        }
        this.zzfyn.setException(zzb.zzy(zzg.zzah(status)));
    }
}
