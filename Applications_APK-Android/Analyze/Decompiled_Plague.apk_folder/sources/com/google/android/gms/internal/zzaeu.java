package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import org.json.JSONObject;

@zzzb
public final class zzaeu {
    public final int errorCode;
    public final int orientation;
    public final boolean zzapy;
    public final List<String> zzcbv;
    public final List<String> zzcbw;
    @Nullable
    public final List<String> zzcby;
    public final long zzccb;
    @Nullable
    public final zztm zzcdd;
    @Nullable
    public final zzuf zzcde;
    @Nullable
    public final String zzcdf;
    @Nullable
    public final zztp zzcdg;
    @Nullable
    public final zzama zzchj;
    public final zzis zzclo;
    public final String zzclr;
    private long zzcnf;
    public final boolean zzcng;
    private long zzcnh;
    public final List<String> zzcni;
    public final String zzcnl;
    @Nullable
    public final zzadw zzcnv;
    @Nullable
    public final List<String> zzcnx;
    public final boolean zzcny;
    private zzaaf zzcnz;
    public final String zzcoc;
    public final JSONObject zzcvq;
    public boolean zzcvr;
    public final zztn zzcvs;
    @Nullable
    public final String zzcvt;
    public final zziw zzcvu;
    @Nullable
    public final List<String> zzcvv;
    public final long zzcvw;
    public final long zzcvx;
    @Nullable
    public final zzoc zzcvy;
    public boolean zzcvz;
    public boolean zzcwa;
    public boolean zzcwb;
    public final zzib zzcwc;
    public final boolean zzcwd;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzaeu(com.google.android.gms.internal.zzaev r59, @android.support.annotation.Nullable com.google.android.gms.internal.zzama r60, @android.support.annotation.Nullable com.google.android.gms.internal.zztm r61, @android.support.annotation.Nullable com.google.android.gms.internal.zzuf r62, @android.support.annotation.Nullable java.lang.String r63, @android.support.annotation.Nullable com.google.android.gms.internal.zztp r64, @android.support.annotation.Nullable com.google.android.gms.internal.zzoc r65, @android.support.annotation.Nullable java.lang.String r66) {
        /*
            r58 = this;
            r0 = r59
            com.google.android.gms.internal.zzzz r1 = r0.zzcpe
            com.google.android.gms.internal.zzis r3 = r1.zzclo
            com.google.android.gms.internal.zzaad r1 = r0.zzcwe
            java.util.List<java.lang.String> r5 = r1.zzcbv
            int r6 = r0.errorCode
            com.google.android.gms.internal.zzaad r1 = r0.zzcwe
            java.util.List<java.lang.String> r7 = r1.zzcbw
            com.google.android.gms.internal.zzaad r1 = r0.zzcwe
            java.util.List<java.lang.String> r8 = r1.zzcni
            com.google.android.gms.internal.zzaad r1 = r0.zzcwe
            int r9 = r1.orientation
            com.google.android.gms.internal.zzaad r1 = r0.zzcwe
            long r10 = r1.zzccb
            com.google.android.gms.internal.zzzz r1 = r0.zzcpe
            java.lang.String r12 = r1.zzclr
            com.google.android.gms.internal.zzaad r1 = r0.zzcwe
            boolean r13 = r1.zzcng
            com.google.android.gms.internal.zztn r1 = r0.zzcvs
            com.google.android.gms.internal.zzaad r2 = r0.zzcwe
            long r14 = r2.zzcnh
            com.google.android.gms.internal.zziw r4 = r0.zzath
            com.google.android.gms.internal.zzaad r2 = r0.zzcwe
            r42 = r14
            long r14 = r2.zzcnf
            r44 = r14
            long r14 = r0.zzcvw
            r46 = r14
            long r14 = r0.zzcvx
            com.google.android.gms.internal.zzaad r2 = r0.zzcwe
            java.lang.String r2 = r2.zzcnl
            r48 = r14
            org.json.JSONObject r15 = r0.zzcvq
            com.google.android.gms.internal.zzaad r14 = r0.zzcwe
            com.google.android.gms.internal.zzadw r14 = r14.zzcnv
            r50 = r2
            com.google.android.gms.internal.zzaad r2 = r0.zzcwe
            java.util.List<java.lang.String> r2 = r2.zzcnw
            r51 = r2
            com.google.android.gms.internal.zzaad r2 = r0.zzcwe
            java.util.List<java.lang.String> r2 = r2.zzcnw
            r52 = r2
            com.google.android.gms.internal.zzaad r2 = r0.zzcwe
            boolean r2 = r2.zzcny
            r53 = r2
            com.google.android.gms.internal.zzaad r2 = r0.zzcwe
            com.google.android.gms.internal.zzaaf r2 = r2.zzcnz
            r54 = r2
            com.google.android.gms.internal.zzaad r2 = r0.zzcwe
            java.util.List<java.lang.String> r2 = r2.zzcby
            r55 = r2
            com.google.android.gms.internal.zzaad r2 = r0.zzcwe
            java.lang.String r2 = r2.zzcoc
            r56 = r15
            com.google.android.gms.internal.zzib r15 = r0.zzcwc
            r57 = r2
            com.google.android.gms.internal.zzaad r2 = r0.zzcwe
            boolean r2 = r2.zzapy
            boolean r0 = r0.zzcwd
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r30 = 0
            r36 = 0
            r40 = r2
            r28 = r50
            r32 = r51
            r33 = r52
            r34 = r53
            r35 = r54
            r37 = r55
            r38 = r57
            r2 = r58
            r21 = r4
            r4 = r16
            r31 = r14
            r22 = r42
            r24 = r44
            r26 = r46
            r41 = r48
            r14 = r17
            r39 = r15
            r29 = r56
            r15 = r18
            r16 = r19
            r17 = r1
            r18 = r20
            r19 = r22
            r22 = r24
            r24 = r26
            r26 = r41
            r41 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r12, r13, r14, r15, r16, r17, r18, r19, r21, r22, r24, r26, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaeu.<init>(com.google.android.gms.internal.zzaev, com.google.android.gms.internal.zzama, com.google.android.gms.internal.zztm, com.google.android.gms.internal.zzuf, java.lang.String, com.google.android.gms.internal.zztp, com.google.android.gms.internal.zzoc, java.lang.String):void");
    }

    public zzaeu(zzis zzis, @Nullable zzama zzama, List<String> list, int i, List<String> list2, List<String> list3, int i2, long j, String str, boolean z, @Nullable zztm zztm, @Nullable zzuf zzuf, @Nullable String str2, zztn zztn, @Nullable zztp zztp, long j2, zziw zziw, long j3, long j4, long j5, String str3, JSONObject jSONObject, @Nullable zzoc zzoc, zzadw zzadw, List<String> list4, List<String> list5, boolean z2, zzaaf zzaaf, @Nullable String str4, List<String> list6, String str5, zzib zzib, boolean z3, boolean z4) {
        this.zzcvz = false;
        this.zzcwa = false;
        this.zzcwb = false;
        this.zzclo = zzis;
        this.zzchj = zzama;
        this.zzcbv = zzp(list);
        this.errorCode = i;
        this.zzcbw = zzp(list2);
        this.zzcni = zzp(list3);
        this.orientation = i2;
        this.zzccb = j;
        this.zzclr = str;
        this.zzcng = z;
        this.zzcdd = zztm;
        this.zzcde = zzuf;
        this.zzcdf = str2;
        this.zzcvs = zztn;
        this.zzcdg = zztp;
        this.zzcnh = j2;
        this.zzcvu = zziw;
        this.zzcnf = j3;
        this.zzcvw = j4;
        this.zzcvx = j5;
        this.zzcnl = str3;
        this.zzcvq = jSONObject;
        this.zzcvy = zzoc;
        this.zzcnv = zzadw;
        this.zzcvv = zzp(list4);
        this.zzcnx = zzp(list5);
        this.zzcny = z2;
        this.zzcnz = zzaaf;
        this.zzcvt = str4;
        this.zzcby = zzp(list6);
        this.zzcoc = str5;
        this.zzcwc = zzib;
        this.zzapy = z3;
        this.zzcwd = z4;
    }

    @Nullable
    private static <T> List<T> zzp(@Nullable List<T> list) {
        if (list == null) {
            return null;
        }
        return Collections.unmodifiableList(list);
    }

    public final boolean zzfr() {
        if (this.zzchj == null || this.zzchj.zzsq() == null) {
            return false;
        }
        return this.zzchj.zzsq().zzfr();
    }
}
