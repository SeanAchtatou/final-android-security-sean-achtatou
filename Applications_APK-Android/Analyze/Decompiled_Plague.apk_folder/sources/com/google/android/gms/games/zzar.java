package com.google.android.gms.games;

import android.os.Parcelable;

public final class zzar implements Parcelable.Creator<PlayerLevelInfo> {
    /* JADX WARN: Type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r12) {
        /*
            r11 = this;
            int r0 = com.google.android.gms.internal.zzbek.zzd(r12)
            r1 = 0
            r2 = 0
            r9 = r1
            r10 = r9
            r5 = r2
            r7 = r5
        L_0x000b:
            int r1 = r12.dataPosition()
            if (r1 >= r0) goto L_0x003e
            int r1 = r12.readInt()
            r2 = 65535(0xffff, float:9.1834E-41)
            r2 = r2 & r1
            switch(r2) {
                case 1: goto L_0x0039;
                case 2: goto L_0x0034;
                case 3: goto L_0x002a;
                case 4: goto L_0x0020;
                default: goto L_0x001c;
            }
        L_0x001c:
            com.google.android.gms.internal.zzbek.zzb(r12, r1)
            goto L_0x000b
        L_0x0020:
            android.os.Parcelable$Creator<com.google.android.gms.games.PlayerLevel> r2 = com.google.android.gms.games.PlayerLevel.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r12, r1, r2)
            r10 = r1
            com.google.android.gms.games.PlayerLevel r10 = (com.google.android.gms.games.PlayerLevel) r10
            goto L_0x000b
        L_0x002a:
            android.os.Parcelable$Creator<com.google.android.gms.games.PlayerLevel> r2 = com.google.android.gms.games.PlayerLevel.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r12, r1, r2)
            r9 = r1
            com.google.android.gms.games.PlayerLevel r9 = (com.google.android.gms.games.PlayerLevel) r9
            goto L_0x000b
        L_0x0034:
            long r7 = com.google.android.gms.internal.zzbek.zzi(r12, r1)
            goto L_0x000b
        L_0x0039:
            long r5 = com.google.android.gms.internal.zzbek.zzi(r12, r1)
            goto L_0x000b
        L_0x003e:
            com.google.android.gms.internal.zzbek.zzaf(r12, r0)
            com.google.android.gms.games.PlayerLevelInfo r12 = new com.google.android.gms.games.PlayerLevelInfo
            r4 = r12
            r4.<init>(r5, r7, r9, r10)
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.games.zzar.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new PlayerLevelInfo[i];
    }
}
