package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfik extends zzfhe<zzfik> implements Cloneable {
    private int zzpla = -1;
    private int zzplb = 0;

    public zzfik() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzas */
    public final zzfik zza(zzfhb zzfhb) throws IOException {
        int i;
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 8) {
                i = zzfhb.getPosition();
                int zzctv = zzfhb.zzctv();
                switch (zzctv) {
                    default:
                        StringBuilder sb = new StringBuilder(43);
                        sb.append(zzctv);
                        sb.append(" is not a valid enum NetworkType");
                        throw new IllegalArgumentException(sb.toString());
                    case -1:
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                    case 16:
                    case 17:
                        this.zzpla = zzctv;
                        continue;
                }
            } else if (zzcts == 16) {
                i = zzfhb.getPosition();
                try {
                    int zzctv2 = zzfhb.zzctv();
                    if (zzctv2 != 100) {
                        switch (zzctv2) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                            case 10:
                            case 11:
                            case 12:
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                                break;
                            default:
                                StringBuilder sb2 = new StringBuilder(45);
                                sb2.append(zzctv2);
                                sb2.append(" is not a valid enum MobileSubtype");
                                throw new IllegalArgumentException(sb2.toString());
                        }
                    }
                    this.zzplb = zzctv2;
                } catch (IllegalArgumentException unused) {
                    zzfhb.zzlv(i);
                    zza(zzfhb, zzcts);
                }
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: zzcyb */
    public zzfik clone() {
        try {
            return (zzfik) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfik)) {
            return false;
        }
        zzfik zzfik = (zzfik) obj;
        if (this.zzpla == zzfik.zzpla && this.zzplb == zzfik.zzplb) {
            return (this.zzpgy == null || this.zzpgy.isEmpty()) ? zzfik.zzpgy == null || zzfik.zzpgy.isEmpty() : this.zzpgy.equals(zzfik.zzpgy);
        }
        return false;
    }

    public final int hashCode() {
        return ((((((527 + getClass().getName().hashCode()) * 31) + this.zzpla) * 31) + this.zzplb) * 31) + ((this.zzpgy == null || this.zzpgy.isEmpty()) ? 0 : this.zzpgy.hashCode());
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzpla != -1) {
            zzfhc.zzaa(1, this.zzpla);
        }
        if (this.zzplb != 0) {
            zzfhc.zzaa(2, this.zzplb);
        }
        super.zza(zzfhc);
    }

    public final /* synthetic */ zzfhe zzcxe() throws CloneNotSupportedException {
        return (zzfik) clone();
    }

    public final /* synthetic */ zzfhk zzcxf() throws CloneNotSupportedException {
        return (zzfik) clone();
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzpla != -1) {
            zzo += zzfhc.zzad(1, this.zzpla);
        }
        return this.zzplb != 0 ? zzo + zzfhc.zzad(2, this.zzplb) : zzo;
    }
}
