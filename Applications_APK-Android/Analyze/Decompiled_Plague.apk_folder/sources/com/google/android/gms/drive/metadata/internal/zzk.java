package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.drive.DriveFolder;

public final class zzk {
    private String zzgpo;

    private zzk(String str) {
        this.zzgpo = str.toLowerCase();
    }

    public static zzk zzgt(String str) {
        zzbq.checkArgument(str == null || !str.isEmpty());
        if (str == null) {
            return null;
        }
        return new zzk(str);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        return this.zzgpo.equals(((zzk) obj).zzgpo);
    }

    public final int hashCode() {
        return this.zzgpo.hashCode();
    }

    public final boolean isFolder() {
        return this.zzgpo.equals(DriveFolder.MIME_TYPE);
    }

    public final String toString() {
        return this.zzgpo;
    }

    public final boolean zzapg() {
        return this.zzgpo.startsWith("application/vnd.google-apps");
    }
}
