package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdpw;
import com.google.android.gms.internal.zzdrg;
import java.io.IOException;

public final class zzdps extends zzfee<zzdps, zza> implements zzffk {
    private static volatile zzffm<zzdps> zzbas;
    /* access modifiers changed from: private */
    public static final zzdps zzlqh;
    private zzdpw zzlqf;
    private zzdrg zzlqg;

    public static final class zza extends zzfef<zzdps, zza> implements zzffk {
        private zza() {
            super(zzdps.zzlqh);
        }

        /* synthetic */ zza(zzdpt zzdpt) {
            this();
        }
    }

    static {
        zzdps zzdps = new zzdps();
        zzlqh = zzdps;
        zzdps.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdps.zzpbs.zzbim();
    }

    private zzdps() {
    }

    public static zzdps zzi(zzfdh zzfdh) throws zzfew {
        return (zzdps) zzfee.zza(zzlqh, zzfdh);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdpw.zza zza2;
        zzdrg.zza zza3;
        switch (zzdpt.zzbaq[i - 1]) {
            case 1:
                return new zzdps();
            case 2:
                return zzlqh;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdps zzdps = (zzdps) obj2;
                this.zzlqf = (zzdpw) zzfen.zza(this.zzlqf, zzdps.zzlqf);
                this.zzlqg = (zzdrg) zzfen.zza(this.zzlqg, zzdps.zzlqg);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    boolean z = false;
                    while (!z) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 10) {
                                    if (this.zzlqf != null) {
                                        zzdpw zzdpw = this.zzlqf;
                                        zzfef zzfef = (zzfef) zzdpw.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdpw);
                                        zza2 = (zzdpw.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlqf = (zzdpw) zzfdq.zza(zzdpw.zzblx(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlqf);
                                        this.zzlqf = (zzdpw) zza2.zzcvj();
                                    }
                                } else if (zzcts == 18) {
                                    if (this.zzlqg != null) {
                                        zzdrg zzdrg = this.zzlqg;
                                        zzfef zzfef2 = (zzfef) zzdrg.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef2.zza((zzfee) zzdrg);
                                        zza3 = (zzdrg.zza) zzfef2;
                                    } else {
                                        zza3 = null;
                                    }
                                    this.zzlqg = (zzdrg) zzfdq.zza(zzdrg.zzbnm(), zzfea);
                                    if (zza3 != null) {
                                        zza3.zza((zzfee) this.zzlqg);
                                        this.zzlqg = (zzdrg) zza3.zzcvj();
                                    }
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdps.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlqh);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlqh;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlqf != null) {
            zzfdv.zza(1, this.zzlqf == null ? zzdpw.zzblx() : this.zzlqf);
        }
        if (this.zzlqg != null) {
            zzfdv.zza(2, this.zzlqg == null ? zzdrg.zzbnm() : this.zzlqg);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzdpw zzblp() {
        return this.zzlqf == null ? zzdpw.zzblx() : this.zzlqf;
    }

    public final zzdrg zzblq() {
        return this.zzlqg == null ? zzdrg.zzbnm() : this.zzlqg;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlqf != null) {
            i2 = 0 + zzfdv.zzb(1, this.zzlqf == null ? zzdpw.zzblx() : this.zzlqf);
        }
        if (this.zzlqg != null) {
            i2 += zzfdv.zzb(2, this.zzlqg == null ? zzdrg.zzbnm() : this.zzlqg);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
