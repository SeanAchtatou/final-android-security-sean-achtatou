package com.google.android.gms.common;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.zzba;
import com.google.android.gms.common.internal.zzbb;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.dynamite.DynamiteModule;

final class zzf {
    private static zzba zzfie;
    private static final Object zzfif = new Object();
    private static Context zzfig;

    static boolean zza(String str, zzg zzg) {
        return zza(str, zzg, false);
    }

    private static boolean zza(String str, zzg zzg, boolean z) {
        if (!zzafn()) {
            return false;
        }
        zzbq.checkNotNull(zzfig);
        try {
            return zzfie.zza(new zzm(str, zzg, z), zzn.zzy(zzfig.getPackageManager()));
        } catch (RemoteException e) {
            Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
            return false;
        }
    }

    private static boolean zzafn() {
        if (zzfie != null) {
            return true;
        }
        zzbq.checkNotNull(zzfig);
        synchronized (zzfif) {
            if (zzfie == null) {
                try {
                    zzfie = zzbb.zzam(DynamiteModule.zza(zzfig, DynamiteModule.zzguf, "com.google.android.gms.googlecertificates").zzgw("com.google.android.gms.common.GoogleCertificatesImpl"));
                } catch (DynamiteModule.zzc e) {
                    Log.e("GoogleCertificates", "Failed to load com.google.android.gms.googlecertificates", e);
                    return false;
                }
            }
        }
        return true;
    }

    static boolean zzb(String str, zzg zzg) {
        return zza(str, zzg, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0019, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized void zzce(android.content.Context r2) {
        /*
            java.lang.Class<com.google.android.gms.common.zzf> r0 = com.google.android.gms.common.zzf.class
            monitor-enter(r0)
            android.content.Context r1 = com.google.android.gms.common.zzf.zzfig     // Catch:{ all -> 0x001a }
            if (r1 != 0) goto L_0x0011
            if (r2 == 0) goto L_0x0018
            android.content.Context r2 = r2.getApplicationContext()     // Catch:{ all -> 0x001a }
            com.google.android.gms.common.zzf.zzfig = r2     // Catch:{ all -> 0x001a }
            monitor-exit(r0)
            return
        L_0x0011:
            java.lang.String r2 = "GoogleCertificates"
            java.lang.String r1 = "GoogleCertificates has been initialized already"
            android.util.Log.w(r2, r1)     // Catch:{ all -> 0x001a }
        L_0x0018:
            monitor-exit(r0)
            return
        L_0x001a:
            r2 = move-exception
            monitor-exit(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.zzf.zzce(android.content.Context):void");
    }
}
