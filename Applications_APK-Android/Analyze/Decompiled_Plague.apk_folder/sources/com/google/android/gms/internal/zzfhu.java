package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfhu extends zzfhe<zzfhu> {
    private byte[] zzpip = null;
    private Integer zzpis = null;
    private byte[] zzpit = null;

    public zzfhu() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 8) {
                this.zzpis = Integer.valueOf(zzfhb.zzctv());
            } else if (zzcts == 18) {
                this.zzpit = zzfhb.readBytes();
            } else if (zzcts == 26) {
                this.zzpip = zzfhb.readBytes();
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzpis != null) {
            zzfhc.zzaa(1, this.zzpis.intValue());
        }
        if (this.zzpit != null) {
            zzfhc.zzc(2, this.zzpit);
        }
        if (this.zzpip != null) {
            zzfhc.zzc(3, this.zzpip);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzpis != null) {
            zzo += zzfhc.zzad(1, this.zzpis.intValue());
        }
        if (this.zzpit != null) {
            zzo += zzfhc.zzd(2, this.zzpit);
        }
        return this.zzpip != null ? zzo + zzfhc.zzd(3, this.zzpip) : zzo;
    }
}
