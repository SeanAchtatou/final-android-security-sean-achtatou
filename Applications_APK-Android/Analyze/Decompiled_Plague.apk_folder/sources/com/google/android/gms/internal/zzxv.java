package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Looper;
import android.os.SystemClock;
import com.google.android.gms.common.util.zzq;
import com.miniclip.input.MCInput;
import java.io.InputStream;

final class zzxv implements zzaie<zznr> {
    private /* synthetic */ String zzcfm;
    private /* synthetic */ zzxr zzciz;
    private /* synthetic */ boolean zzcjj;
    private /* synthetic */ double zzcjk;
    private /* synthetic */ boolean zzcjl;

    zzxv(zzxr zzxr, boolean z, double d, boolean z2, String str) {
        this.zzciz = zzxr;
        this.zzcjj = z;
        this.zzcjk = d;
        this.zzcjl = z2;
        this.zzcfm = str;
    }

    /* access modifiers changed from: private */
    @TargetApi(19)
    /* renamed from: zzd */
    public final zznr zze(InputStream inputStream) {
        Bitmap bitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inDensity = (int) (160.0d * this.zzcjk);
        if (!this.zzcjl) {
            options.inPreferredConfig = Bitmap.Config.RGB_565;
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        try {
            bitmap = BitmapFactory.decodeStream(inputStream, null, options);
        } catch (Exception e) {
            zzafj.zzb("Error grabbing image.", e);
            bitmap = null;
        }
        if (bitmap == null) {
            this.zzciz.zzd(2, this.zzcjj);
            return null;
        }
        long uptimeMillis2 = SystemClock.uptimeMillis();
        if (zzq.zzalz() && zzafj.zzpt()) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            int allocationByteCount = bitmap.getAllocationByteCount();
            long j = uptimeMillis2 - uptimeMillis;
            boolean z = Looper.getMainLooper().getThread() == Thread.currentThread();
            StringBuilder sb = new StringBuilder((int) MCInput.KEYCODE_BUTTON_START);
            sb.append("Decoded image w: ");
            sb.append(width);
            sb.append(" h:");
            sb.append(height);
            sb.append(" bytes: ");
            sb.append(allocationByteCount);
            sb.append(" time: ");
            sb.append(j);
            sb.append(" on ui thread: ");
            sb.append(z);
            zzafj.v(sb.toString());
        }
        return new zznr(new BitmapDrawable(Resources.getSystem(), bitmap), Uri.parse(this.zzcfm), this.zzcjk);
    }

    public final /* synthetic */ Object zznc() {
        this.zzciz.zzd(2, this.zzcjj);
        return null;
    }
}
