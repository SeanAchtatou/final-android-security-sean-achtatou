package com.google.android.gms.common.api;

import com.google.android.gms.common.api.internal.zzs;
import java.util.ArrayList;
import java.util.List;

public final class Batch extends zzs<BatchResult> {
    /* access modifiers changed from: private */
    public final Object mLock;
    /* access modifiers changed from: private */
    public int zzfjd;
    /* access modifiers changed from: private */
    public boolean zzfje;
    /* access modifiers changed from: private */
    public boolean zzfjf;
    /* access modifiers changed from: private */
    public final PendingResult<?>[] zzfjg;

    public static final class Builder {
        private GoogleApiClient zzerl;
        private List<PendingResult<?>> zzfji = new ArrayList();

        public Builder(GoogleApiClient googleApiClient) {
            this.zzerl = googleApiClient;
        }

        public final <R extends Result> BatchResultToken<R> add(PendingResult<R> pendingResult) {
            BatchResultToken<R> batchResultToken = new BatchResultToken<>(this.zzfji.size());
            this.zzfji.add(pendingResult);
            return batchResultToken;
        }

        public final Batch build() {
            return new Batch(this.zzfji, this.zzerl, null);
        }
    }

    private Batch(List<PendingResult<?>> list, GoogleApiClient googleApiClient) {
        super(googleApiClient);
        this.mLock = new Object();
        this.zzfjd = list.size();
        this.zzfjg = new PendingResult[this.zzfjd];
        if (list.isEmpty()) {
            setResult(new BatchResult(Status.zzfko, this.zzfjg));
            return;
        }
        for (int i = 0; i < list.size(); i++) {
            PendingResult<?> pendingResult = list.get(i);
            this.zzfjg[i] = pendingResult;
            pendingResult.zza(new zza(this));
        }
    }

    /* synthetic */ Batch(List list, GoogleApiClient googleApiClient, zza zza) {
        this(list, googleApiClient);
    }

    static /* synthetic */ int zzb(Batch batch) {
        int i = batch.zzfjd;
        batch.zzfjd = i - 1;
        return i;
    }

    public final void cancel() {
        super.cancel();
        for (PendingResult<?> cancel : this.zzfjg) {
            cancel.cancel();
        }
    }

    /* renamed from: createFailedResult */
    public final BatchResult zzb(Status status) {
        return new BatchResult(status, this.zzfjg);
    }
}
