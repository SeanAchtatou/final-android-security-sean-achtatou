package com.google.android.gms.internal;

import android.os.Bundle;
import android.support.v4.util.LongSparseArray;
import android.util.SparseArray;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.drive.metadata.internal.zzc;
import com.google.android.gms.drive.metadata.internal.zzg;
import com.google.android.gms.drive.metadata.internal.zzm;
import java.util.Arrays;

public class zzbsp extends zzm<AppVisibleCustomProperties> {
    public static final zzg zzgri = new zzbsq();

    public zzbsp(int i) {
        super("customProperties", Arrays.asList("hasCustomProperties", "sqlId"), Arrays.asList("customPropertiesExtra", "customPropertiesExtraHolder"), 5000000);
    }

    /* access modifiers changed from: private */
    public static void zzd(DataHolder dataHolder) {
        Bundle zzafx = dataHolder.zzafx();
        if (zzafx != null) {
            synchronized (dataHolder) {
                DataHolder dataHolder2 = (DataHolder) zzafx.getParcelable("customPropertiesExtraHolder");
                if (dataHolder2 != null) {
                    dataHolder2.close();
                    zzafx.remove("customPropertiesExtraHolder");
                }
            }
        }
    }

    /* JADX INFO: finally extract failed */
    private static AppVisibleCustomProperties zzf(DataHolder dataHolder, int i, int i2) {
        DataHolder dataHolder2;
        DataHolder dataHolder3 = dataHolder;
        Bundle zzafx = dataHolder.zzafx();
        SparseArray sparseParcelableArray = zzafx.getSparseParcelableArray("customPropertiesExtra");
        if (sparseParcelableArray == null) {
            if (zzafx.getParcelable("customPropertiesExtraHolder") != null) {
                synchronized (dataHolder) {
                    try {
                        dataHolder2 = (DataHolder) dataHolder.zzafx().getParcelable("customPropertiesExtraHolder");
                        if (dataHolder2 != null) {
                            Bundle zzafx2 = dataHolder2.zzafx();
                            String string = zzafx2.getString("entryIdColumn");
                            String string2 = zzafx2.getString("keyColumn");
                            String string3 = zzafx2.getString("visibilityColumn");
                            String string4 = zzafx2.getString("valueColumn");
                            LongSparseArray longSparseArray = new LongSparseArray();
                            int i3 = 0;
                            while (i3 < dataHolder2.getCount()) {
                                int zzbz = dataHolder2.zzbz(i3);
                                long zzb = dataHolder2.zzb(string, i3, zzbz);
                                String zzd = dataHolder2.zzd(string2, i3, zzbz);
                                int zzc = dataHolder2.zzc(string3, i3, zzbz);
                                String str = string4;
                                zzc zzc2 = new zzc(new CustomPropertyKey(zzd, zzc), dataHolder2.zzd(string4, i3, zzbz));
                                AppVisibleCustomProperties.zza zza = (AppVisibleCustomProperties.zza) longSparseArray.get(zzb);
                                if (zza == null) {
                                    zza = new AppVisibleCustomProperties.zza();
                                    longSparseArray.put(zzb, zza);
                                }
                                zza.zza(zzc2);
                                i3++;
                                string4 = str;
                            }
                            SparseArray sparseArray = new SparseArray();
                            for (int i4 = 0; i4 < dataHolder.getCount(); i4++) {
                                AppVisibleCustomProperties.zza zza2 = (AppVisibleCustomProperties.zza) longSparseArray.get(dataHolder3.zzb("sqlId", i4, dataHolder3.zzbz(i4)));
                                if (zza2 != null) {
                                    sparseArray.append(i4, zza2.zzapa());
                                }
                            }
                            dataHolder.zzafx().putSparseParcelableArray("customPropertiesExtra", sparseArray);
                            dataHolder2.close();
                            dataHolder.zzafx().remove("customPropertiesExtraHolder");
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                sparseParcelableArray = zzafx.getSparseParcelableArray("customPropertiesExtra");
            }
            if (sparseParcelableArray == null) {
                return AppVisibleCustomProperties.zzgph;
            }
        }
        return (AppVisibleCustomProperties) sparseParcelableArray.get(i, AppVisibleCustomProperties.zzgph);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzc(DataHolder dataHolder, int i, int i2) {
        return zzf(dataHolder, i, i2);
    }
}
