package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdri;
import java.io.IOException;

public final class zzdre extends zzfee<zzdre, zza> implements zzffk {
    private static volatile zzffm<zzdre> zzbas;
    /* access modifiers changed from: private */
    public static final zzdre zzlsj;
    private int zzlqb;
    private zzfdh zzlqj = zzfdh.zzpal;
    private zzdri zzlsi;

    public static final class zza extends zzfef<zzdre, zza> implements zzffk {
        private zza() {
            super(zzdre.zzlsj);
        }

        /* synthetic */ zza(zzdrf zzdrf) {
            this();
        }

        public final zza zzc(zzdri zzdri) {
            zzcvi();
            ((zzdre) this.zzpbv).zzb(zzdri);
            return this;
        }

        public final zza zzfq(int i) {
            zzcvi();
            ((zzdre) this.zzpbv).setVersion(0);
            return this;
        }

        public final zza zzx(zzfdh zzfdh) {
            zzcvi();
            ((zzdre) this.zzpbv).zzj(zzfdh);
            return this;
        }
    }

    static {
        zzdre zzdre = new zzdre();
        zzlsj = zzdre;
        zzdre.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdre.zzpbs.zzbim();
    }

    private zzdre() {
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzlqb = i;
    }

    /* access modifiers changed from: private */
    public final void zzb(zzdri zzdri) {
        if (zzdri == null) {
            throw new NullPointerException();
        }
        this.zzlsi = zzdri;
    }

    public static zza zzbnj() {
        zzdre zzdre = zzlsj;
        zzfef zzfef = (zzfef) zzdre.zza(zzfem.zzpch, (Object) null, (Object) null);
        zzfef.zza((zzfee) zzdre);
        return (zza) zzfef;
    }

    public static zzdre zzbnk() {
        return zzlsj;
    }

    /* access modifiers changed from: private */
    public final void zzj(zzfdh zzfdh) {
        if (zzfdh == null) {
            throw new NullPointerException();
        }
        this.zzlqj = zzfdh;
    }

    public static zzdre zzw(zzfdh zzfdh) throws zzfew {
        return (zzdre) zzfee.zza(zzlsj, zzfdh);
    }

    public final int getVersion() {
        return this.zzlqb;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdri.zza zza2;
        boolean z = true;
        boolean z2 = false;
        switch (zzdrf.zzbaq[i - 1]) {
            case 1:
                return new zzdre();
            case 2:
                return zzlsj;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdre zzdre = (zzdre) obj2;
                this.zzlqb = zzfen.zza(this.zzlqb != 0, this.zzlqb, zzdre.zzlqb != 0, zzdre.zzlqb);
                this.zzlsi = (zzdri) zzfen.zza(this.zzlsi, zzdre.zzlsi);
                boolean z3 = this.zzlqj != zzfdh.zzpal;
                zzfdh zzfdh = this.zzlqj;
                if (zzdre.zzlqj == zzfdh.zzpal) {
                    z = false;
                }
                this.zzlqj = zzfen.zza(z3, zzfdh, z, zzdre.zzlqj);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzlqb = zzfdq.zzcub();
                                } else if (zzcts == 18) {
                                    if (this.zzlsi != null) {
                                        zzdri zzdri = this.zzlsi;
                                        zzfef zzfef = (zzfef) zzdri.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdri);
                                        zza2 = (zzdri.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlsi = (zzdri) zzfdq.zza(zzdri.zzbnq(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlsi);
                                        this.zzlsi = (zzdri) zza2.zzcvj();
                                    }
                                } else if (zzcts == 26) {
                                    this.zzlqj = zzfdq.zzcua();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdre.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlsj);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlsj;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlqb != 0) {
            zzfdv.zzab(1, this.zzlqb);
        }
        if (this.zzlsi != null) {
            zzfdv.zza(2, this.zzlsi == null ? zzdri.zzbnq() : this.zzlsi);
        }
        if (!this.zzlqj.isEmpty()) {
            zzfdv.zza(3, this.zzlqj);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzfdh zzblt() {
        return this.zzlqj;
    }

    public final zzdri zzbni() {
        return this.zzlsi == null ? zzdri.zzbnq() : this.zzlsi;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlqb != 0) {
            i2 = 0 + zzfdv.zzae(1, this.zzlqb);
        }
        if (this.zzlsi != null) {
            i2 += zzfdv.zzb(2, this.zzlsi == null ? zzdri.zzbnq() : this.zzlsi);
        }
        if (!this.zzlqj.isEmpty()) {
            i2 += zzfdv.zzb(3, this.zzlqj);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
