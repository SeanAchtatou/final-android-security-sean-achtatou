package com.google.android.gms.drive;

import android.text.TextUtils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.internal.zzbll;
import java.util.Arrays;

public class ExecutionOptions {
    public static final int CONFLICT_STRATEGY_KEEP_REMOTE = 1;
    public static final int CONFLICT_STRATEGY_OVERWRITE_REMOTE = 0;
    public static final int MAX_TRACKING_TAG_STRING_LENGTH = 65536;
    private final String zzgha;
    private final boolean zzghb;
    private final int zzghc;

    public static class Builder {
        protected String zzgha;
        protected boolean zzghb;
        protected int zzghc = 0;

        public ExecutionOptions build() {
            zzanv();
            return new ExecutionOptions(this.zzgha, this.zzghb, this.zzghc);
        }

        public Builder setConflictStrategy(int i) {
            boolean z;
            switch (i) {
                case 0:
                case 1:
                    z = true;
                    break;
                default:
                    z = false;
                    break;
            }
            if (!z) {
                StringBuilder sb = new StringBuilder(53);
                sb.append("Unrecognized value for conflict strategy: ");
                sb.append(i);
                throw new IllegalArgumentException(sb.toString());
            }
            this.zzghc = i;
            return this;
        }

        public Builder setNotifyOnCompletion(boolean z) {
            this.zzghb = z;
            return this;
        }

        public Builder setTrackingTag(String str) {
            if (!(!TextUtils.isEmpty(str) && str.length() <= 65536)) {
                throw new IllegalArgumentException(String.format("trackingTag must not be null nor empty, and the length must be <= the maximum length (%s)", 65536));
            }
            this.zzgha = str;
            return this;
        }

        /* access modifiers changed from: protected */
        public final void zzanv() {
            if (this.zzghc == 1 && !this.zzghb) {
                throw new IllegalStateException("Cannot use CONFLICT_STRATEGY_KEEP_REMOTE without requesting completion notifications");
            }
        }
    }

    public ExecutionOptions(String str, boolean z, int i) {
        this.zzgha = str;
        this.zzghb = z;
        this.zzghc = i;
    }

    public static boolean zzcr(int i) {
        return i == 1;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        ExecutionOptions executionOptions = (ExecutionOptions) obj;
        return zzbg.equal(this.zzgha, executionOptions.zzgha) && this.zzghc == executionOptions.zzghc && this.zzghb == executionOptions.zzghb;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.zzgha, Integer.valueOf(this.zzghc), Boolean.valueOf(this.zzghb)});
    }

    public final void zza(zzbll zzbll) {
        if (this.zzghb && !zzbll.zzaoq()) {
            throw new IllegalStateException("Application must define an exported DriveEventService subclass in AndroidManifest.xml to be notified on completion");
        }
    }

    public final String zzans() {
        return this.zzgha;
    }

    public final boolean zzant() {
        return this.zzghb;
    }

    public final int zzanu() {
        return this.zzghc;
    }

    @Deprecated
    public final void zzf(GoogleApiClient googleApiClient) {
        zza((zzbll) googleApiClient.zza(Drive.zzdyh));
    }
}
