package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbem;

public final class zzt extends zza {
    public static final Parcelable.Creator<zzt> CREATOR = new zzu();

    public final void writeToParcel(Parcel parcel, int i) {
        zzbem.zzai(parcel, zzbem.zze(parcel));
    }

    public final <F> F zza(zzj<F> zzj) {
        return zzj.zzapj();
    }
}
