package com.google.android.gms.internal;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.support.v4.os.EnvironmentCompat;
import com.mopub.common.Constants;
import com.tapjoy.TapjoyConstants;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@zzzb
public class zzyv implements zzyz {
    private static final Object sLock = new Object();
    private static zzyz zzckp;
    private final Context mContext;
    private final ExecutorService zzaih = Executors.newCachedThreadPool();
    private final zzaiy zzaqi;
    private final Object zzckq = new Object();
    private final WeakHashMap<Thread, Boolean> zzckr = new WeakHashMap<>();

    protected zzyv(Context context) {
        this.mContext = context;
        this.zzaqi = zzaiy.zzqv();
        Thread thread = Looper.getMainLooper().getThread();
        if (thread != null) {
            synchronized (this.zzckq) {
                this.zzckr.put(thread, true);
            }
            thread.setUncaughtExceptionHandler(new zzyx(this, thread.getUncaughtExceptionHandler()));
        }
        Thread.setDefaultUncaughtExceptionHandler(new zzyw(this, Thread.getDefaultUncaughtExceptionHandler()));
    }

    public static zzyz zzk(Context context) {
        synchronized (sLock) {
            if (zzckp == null) {
                zzckp = new zzza();
            }
        }
        return zzckp;
    }

    private final void zzn(List<String> list) {
        for (String zzyy : list) {
            this.zzaih.submit(new zzyy(this, new zzaix(), zzyy));
        }
    }

    /* access modifiers changed from: protected */
    public Uri.Builder zza(String str, String str2, String str3, int i) {
        boolean z;
        try {
            z = zzbgc.zzcy(this.mContext).zzami();
        } catch (Throwable th) {
            zzaiw.zzb("Error fetching instant app info", th);
            z = false;
        }
        String str4 = EnvironmentCompat.MEDIA_UNKNOWN;
        try {
            str4 = this.mContext.getApplicationContext().getPackageName();
        } catch (Throwable unused) {
            zzaiw.zzco("Cannot obtain package name, proceeding.");
        }
        Uri.Builder appendQueryParameter = new Uri.Builder().scheme(Constants.HTTPS).path("//pagead2.googlesyndication.com/pagead/gen_204").appendQueryParameter("is_aia", Boolean.toString(z)).appendQueryParameter("id", "gmob-apps-report-exception").appendQueryParameter("os", Build.VERSION.RELEASE).appendQueryParameter("api", String.valueOf(Build.VERSION.SDK_INT));
        String str5 = Build.MANUFACTURER;
        String str6 = Build.MODEL;
        if (!str6.startsWith(str5)) {
            StringBuilder sb = new StringBuilder(1 + String.valueOf(str5).length() + String.valueOf(str6).length());
            sb.append(str5);
            sb.append(" ");
            sb.append(str6);
            str6 = sb.toString();
        }
        return appendQueryParameter.appendQueryParameter(TapjoyConstants.TJC_NOTIFICATION_DEVICE_PREFIX, str6).appendQueryParameter("js", this.zzaqi.zzcp).appendQueryParameter("appid", str4).appendQueryParameter("exceptiontype", str).appendQueryParameter("stacktrace", str2).appendQueryParameter("exceptionkey", str3).appendQueryParameter("cl", "175987007").appendQueryParameter("rc", "dev").appendQueryParameter(TapjoyConstants.TJC_SESSION_ID, zzjk.zzhz()).appendQueryParameter("sampling_rate", Integer.toString(i));
    }

    /* access modifiers changed from: protected */
    public final void zza(Thread thread, Throwable th) {
        boolean z;
        boolean z2 = false;
        if (th != null) {
            boolean z3 = false;
            boolean z4 = false;
            Throwable th2 = th;
            while (th2 != null) {
                boolean z5 = z4;
                boolean z6 = z3;
                for (StackTraceElement stackTraceElement : th2.getStackTrace()) {
                    if (zzais.zzcm(stackTraceElement.getClassName())) {
                        z6 = true;
                    }
                    if (getClass().getName().equals(stackTraceElement.getClassName())) {
                        z5 = true;
                    }
                }
                th2 = th2.getCause();
                z3 = z6;
                z4 = z5;
            }
            if (z3 && !z4) {
                z = true;
                if (z && zzais.zzc(th) != null) {
                    String name = th.getClass().getName();
                    StringWriter stringWriter = new StringWriter();
                    zzdtf.zza(th, new PrintWriter(stringWriter));
                    String stringWriter2 = stringWriter.toString();
                    if (Math.random() < 1.0d) {
                        z2 = true;
                    }
                    if (z2) {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(zza(name, stringWriter2, "", 1).toString());
                        zzn(arrayList);
                        return;
                    }
                    return;
                }
            }
        }
        z = false;
        if (z) {
        }
    }

    public final void zza(Throwable th, String str) {
        if (zzais.zzc(th) != null) {
            String name = th.getClass().getName();
            StringWriter stringWriter = new StringWriter();
            zzdtf.zza(th, new PrintWriter(stringWriter));
            String stringWriter2 = stringWriter.toString();
            if (Math.random() < 1.0d) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(zza(name, stringWriter2, str, 1).toString());
                zzn(arrayList);
            }
        }
    }
}
