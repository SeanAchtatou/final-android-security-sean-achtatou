package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

public final class zzbrk extends zzbej {
    public static final Parcelable.Creator<zzbrk> CREATOR = new zzbrl();
    private final List<String> zzgov;

    public zzbrk(List<String> list) {
        this.zzgov = list;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzb(parcel, 2, this.zzgov, false);
        zzbem.zzai(parcel, zze);
    }
}
