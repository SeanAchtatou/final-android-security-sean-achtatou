package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.zzr;

final class zzblx extends zzblk {
    private /* synthetic */ zzblv zzgli;
    private /* synthetic */ MetadataChangeSet zzglj;
    private /* synthetic */ zzr zzglk;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzblx(zzblv zzblv, GoogleApiClient googleApiClient, MetadataChangeSet metadataChangeSet, zzr zzr) {
        super(googleApiClient);
        this.zzgli = zzblv;
        this.zzglj = metadataChangeSet;
        this.zzglk = zzr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        zzbll zzbll = (zzbll) zzb;
        this.zzglj.zzanz().setContext(zzbll.getContext());
        ((zzbpf) zzbll.zzakb()).zza(new zzbjw(this.zzgli.zzglf.getDriveId(), this.zzglj.zzanz(), this.zzgli.zzglf.getRequestId(), this.zzgli.zzglf.zzanh(), this.zzglk), new zzbrj(this));
    }
}
