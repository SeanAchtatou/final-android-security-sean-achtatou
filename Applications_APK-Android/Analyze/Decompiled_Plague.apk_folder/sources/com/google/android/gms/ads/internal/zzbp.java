package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.zzcr;
import com.google.android.gms.internal.zzcs;
import java.util.concurrent.Callable;

final class zzbp implements Callable<zzcs> {
    private /* synthetic */ zzbm zzarm;

    zzbp(zzbm zzbm) {
        this.zzarm = zzbm;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzcr.zza(java.lang.String, android.content.Context, boolean):com.google.android.gms.internal.zzcr
     arg types: [java.lang.String, android.content.Context, int]
     candidates:
      com.google.android.gms.internal.zzcr.zza(com.google.android.gms.internal.zzda, com.google.android.gms.internal.zzaw, com.google.android.gms.internal.zzat):java.util.List<java.util.concurrent.Callable<java.lang.Void>>
      com.google.android.gms.internal.zzcq.zza(com.google.android.gms.internal.zzda, android.view.MotionEvent, android.util.DisplayMetrics):com.google.android.gms.internal.zzde
      com.google.android.gms.internal.zzcq.zza(com.google.android.gms.internal.zzda, com.google.android.gms.internal.zzaw, com.google.android.gms.internal.zzat):java.util.List<java.util.concurrent.Callable<java.lang.Void>>
      com.google.android.gms.internal.zzcp.zza(android.content.Context, java.lang.String, android.view.View):java.lang.String
      com.google.android.gms.internal.zzcp.zza(int, int, int):void
      com.google.android.gms.internal.zzco.zza(android.content.Context, java.lang.String, android.view.View):java.lang.String
      com.google.android.gms.internal.zzco.zza(int, int, int):void
      com.google.android.gms.internal.zzcr.zza(java.lang.String, android.content.Context, boolean):com.google.android.gms.internal.zzcr */
    public final /* synthetic */ Object call() throws Exception {
        return new zzcs(zzcr.zza(this.zzarm.zzaov.zzcp, this.zzarm.mContext, false));
    }
}
