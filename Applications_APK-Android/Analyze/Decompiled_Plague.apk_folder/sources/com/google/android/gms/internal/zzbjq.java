package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.zzcn;
import com.google.android.gms.common.internal.zzaq;
import com.google.android.gms.drive.events.ListenerToken;

public final class zzbjq implements ListenerToken {
    private final zzcn zzgji;
    private zzaq zzgjj = null;

    public zzbjq(zzcn zzcn) {
        this.zzgji = zzcn;
    }

    public final boolean cancel() {
        if (this.zzgjj == null) {
            return false;
        }
        try {
            this.zzgjj.cancel();
            return true;
        } catch (RemoteException unused) {
            return false;
        }
    }

    public final void zza(zzaq zzaq) {
        this.zzgjj = zzaq;
    }

    public final zzcn zzaom() {
        return this.zzgji;
    }
}
