package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.drive.DriveResource;

final class zzbop extends zzbjv {
    private final zzn<DriveResource.MetadataResult> zzfzc;

    public zzbop(zzn<DriveResource.MetadataResult> zzn) {
        this.zzfzc = zzn;
    }

    public final void onError(Status status) throws RemoteException {
        this.zzfzc.setResult(new zzboq(status, null));
    }

    public final void zza(zzbqj zzbqj) throws RemoteException {
        this.zzfzc.setResult(new zzboq(Status.zzfko, new zzbkk(zzbqj.zzgjz)));
    }
}
