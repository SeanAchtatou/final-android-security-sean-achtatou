package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzkd extends zzed implements zzkb {
    zzkd(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdManager");
    }

    public final void destroy() throws RemoteException {
        zzb(2, zzaz());
    }

    public final String getAdUnitId() throws RemoteException {
        Parcel zza = zza(31, zzaz());
        String readString = zza.readString();
        zza.recycle();
        return readString;
    }

    public final String getMediationAdapterClassName() throws RemoteException {
        Parcel zza = zza(18, zzaz());
        String readString = zza.readString();
        zza.recycle();
        return readString;
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzku getVideoController() throws android.os.RemoteException {
        /*
            r4 = this;
            android.os.Parcel r0 = r4.zzaz()
            r1 = 26
            android.os.Parcel r0 = r4.zza(r1, r0)
            android.os.IBinder r1 = r0.readStrongBinder()
            if (r1 != 0) goto L_0x0012
            r1 = 0
            goto L_0x0026
        L_0x0012:
            java.lang.String r2 = "com.google.android.gms.ads.internal.client.IVideoController"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r3 = r2 instanceof com.google.android.gms.internal.zzku
            if (r3 == 0) goto L_0x0020
            r1 = r2
            com.google.android.gms.internal.zzku r1 = (com.google.android.gms.internal.zzku) r1
            goto L_0x0026
        L_0x0020:
            com.google.android.gms.internal.zzkw r2 = new com.google.android.gms.internal.zzkw
            r2.<init>(r1)
            r1 = r2
        L_0x0026:
            r0.recycle()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzkd.getVideoController():com.google.android.gms.internal.zzku");
    }

    public final boolean isLoading() throws RemoteException {
        Parcel zza = zza(23, zzaz());
        boolean zza2 = zzef.zza(zza);
        zza.recycle();
        return zza2;
    }

    public final boolean isReady() throws RemoteException {
        Parcel zza = zza(3, zzaz());
        boolean zza2 = zzef.zza(zza);
        zza.recycle();
        return zza2;
    }

    public final void pause() throws RemoteException {
        zzb(5, zzaz());
    }

    public final void resume() throws RemoteException {
        zzb(6, zzaz());
    }

    public final void setImmersiveMode(boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, z);
        zzb(34, zzaz);
    }

    public final void setManualImpressionsEnabled(boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, z);
        zzb(22, zzaz);
    }

    public final void setUserId(String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeString(str);
        zzb(25, zzaz);
    }

    public final void showInterstitial() throws RemoteException {
        zzb(9, zzaz());
    }

    public final void stopLoading() throws RemoteException {
        zzb(10, zzaz());
    }

    public final void zza(zzacv zzacv) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzacv);
        zzb(24, zzaz);
    }

    public final void zza(zziw zziw) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zziw);
        zzb(13, zzaz);
    }

    public final void zza(zzjn zzjn) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzjn);
        zzb(20, zzaz);
    }

    public final void zza(zzjq zzjq) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzjq);
        zzb(7, zzaz);
    }

    public final void zza(zzkg zzkg) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzkg);
        zzb(8, zzaz);
    }

    public final void zza(zzkm zzkm) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzkm);
        zzb(21, zzaz);
    }

    public final void zza(zzla zzla) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzla);
        zzb(30, zzaz);
    }

    public final void zza(zzma zzma) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzma);
        zzb(29, zzaz);
    }

    public final void zza(zznj zznj) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zznj);
        zzb(19, zzaz);
    }

    public final void zza(zzwq zzwq) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzwq);
        zzb(14, zzaz);
    }

    public final void zza(zzww zzww, String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzww);
        zzaz.writeString(str);
        zzb(15, zzaz);
    }

    public final boolean zzb(zzis zzis) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzis);
        Parcel zza = zza(4, zzaz);
        boolean zza2 = zzef.zza(zza);
        zza.recycle();
        return zza2;
    }

    public final IObjectWrapper zzbl() throws RemoteException {
        Parcel zza = zza(1, zzaz());
        IObjectWrapper zzap = IObjectWrapper.zza.zzap(zza.readStrongBinder());
        zza.recycle();
        return zzap;
    }

    public final zziw zzbm() throws RemoteException {
        Parcel zza = zza(12, zzaz());
        zziw zziw = (zziw) zzef.zza(zza, zziw.CREATOR);
        zza.recycle();
        return zziw;
    }

    public final void zzbo() throws RemoteException {
        zzb(11, zzaz());
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzkg zzbx() throws android.os.RemoteException {
        /*
            r4 = this;
            android.os.Parcel r0 = r4.zzaz()
            r1 = 32
            android.os.Parcel r0 = r4.zza(r1, r0)
            android.os.IBinder r1 = r0.readStrongBinder()
            if (r1 != 0) goto L_0x0012
            r1 = 0
            goto L_0x0026
        L_0x0012:
            java.lang.String r2 = "com.google.android.gms.ads.internal.client.IAppEventListener"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r3 = r2 instanceof com.google.android.gms.internal.zzkg
            if (r3 == 0) goto L_0x0020
            r1 = r2
            com.google.android.gms.internal.zzkg r1 = (com.google.android.gms.internal.zzkg) r1
            goto L_0x0026
        L_0x0020:
            com.google.android.gms.internal.zzki r2 = new com.google.android.gms.internal.zzki
            r2.<init>(r1)
            r1 = r2
        L_0x0026:
            r0.recycle()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzkd.zzbx():com.google.android.gms.internal.zzkg");
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzjq zzby() throws android.os.RemoteException {
        /*
            r4 = this;
            android.os.Parcel r0 = r4.zzaz()
            r1 = 33
            android.os.Parcel r0 = r4.zza(r1, r0)
            android.os.IBinder r1 = r0.readStrongBinder()
            if (r1 != 0) goto L_0x0012
            r1 = 0
            goto L_0x0026
        L_0x0012:
            java.lang.String r2 = "com.google.android.gms.ads.internal.client.IAdListener"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r3 = r2 instanceof com.google.android.gms.internal.zzjq
            if (r3 == 0) goto L_0x0020
            r1 = r2
            com.google.android.gms.internal.zzjq r1 = (com.google.android.gms.internal.zzjq) r1
            goto L_0x0026
        L_0x0020:
            com.google.android.gms.internal.zzjs r2 = new com.google.android.gms.internal.zzjs
            r2.<init>(r1)
            r1 = r2
        L_0x0026:
            r0.recycle()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzkd.zzby():com.google.android.gms.internal.zzjq");
    }

    public final String zzcj() throws RemoteException {
        Parcel zza = zza(35, zzaz());
        String readString = zza.readString();
        zza.recycle();
        return readString;
    }
}
