package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfhs extends zzfhe<zzfhs> {
    private byte[] zzpin = null;
    private byte[] zzpio = null;
    private byte[] zzpip = null;

    public zzfhs() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 10) {
                this.zzpin = zzfhb.readBytes();
            } else if (zzcts == 18) {
                this.zzpio = zzfhb.readBytes();
            } else if (zzcts == 26) {
                this.zzpip = zzfhb.readBytes();
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzpin != null) {
            zzfhc.zzc(1, this.zzpin);
        }
        if (this.zzpio != null) {
            zzfhc.zzc(2, this.zzpio);
        }
        if (this.zzpip != null) {
            zzfhc.zzc(3, this.zzpip);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzpin != null) {
            zzo += zzfhc.zzd(1, this.zzpin);
        }
        if (this.zzpio != null) {
            zzo += zzfhc.zzd(2, this.zzpio);
        }
        return this.zzpip != null ? zzo + zzfhc.zzd(3, this.zzpip) : zzo;
    }
}
