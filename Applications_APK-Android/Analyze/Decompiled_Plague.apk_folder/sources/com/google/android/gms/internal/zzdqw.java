package com.google.android.gms.internal;

import java.io.IOException;

public final class zzdqw extends zzfee<zzdqw, zza> implements zzffk {
    private static volatile zzffm<zzdqw> zzbas;
    /* access modifiers changed from: private */
    public static final zzdqw zzlrq;
    private int zzlrn;
    private int zzlro;
    private zzfdh zzlrp = zzfdh.zzpal;

    public static final class zza extends zzfef<zzdqw, zza> implements zzffk {
        private zza() {
            super(zzdqw.zzlrq);
        }

        /* synthetic */ zza(zzdqx zzdqx) {
            this();
        }
    }

    static {
        zzdqw zzdqw = new zzdqw();
        zzlrq = zzdqw;
        zzdqw.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdqw.zzpbs.zzbim();
    }

    private zzdqw() {
    }

    public static zzdqw zzbnf() {
        return zzlrq;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        boolean z = true;
        boolean z2 = false;
        switch (zzdqx.zzbaq[i - 1]) {
            case 1:
                return new zzdqw();
            case 2:
                return zzlrq;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdqw zzdqw = (zzdqw) obj2;
                this.zzlrn = zzfen.zza(this.zzlrn != 0, this.zzlrn, zzdqw.zzlrn != 0, zzdqw.zzlrn);
                this.zzlro = zzfen.zza(this.zzlro != 0, this.zzlro, zzdqw.zzlro != 0, zzdqw.zzlro);
                boolean z3 = this.zzlrp != zzfdh.zzpal;
                zzfdh zzfdh = this.zzlrp;
                if (zzdqw.zzlrp == zzfdh.zzpal) {
                    z = false;
                }
                this.zzlrp = zzfen.zza(z3, zzfdh, z, zzdqw.zzlrp);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                if (((zzfea) obj2) != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzlrn = zzfdq.zzcuc();
                                } else if (zzcts == 16) {
                                    this.zzlro = zzfdq.zzcuc();
                                } else if (zzcts == 90) {
                                    this.zzlrp = zzfdq.zzcua();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdqw.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlrq);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlrq;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlrn != zzdqy.UNKNOWN_CURVE.zzhn()) {
            zzfdv.zzaa(1, this.zzlrn);
        }
        if (this.zzlro != zzdrc.UNKNOWN_HASH.zzhn()) {
            zzfdv.zzaa(2, this.zzlro);
        }
        if (!this.zzlrp.isEmpty()) {
            zzfdv.zza(11, this.zzlrp);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzdqy zzbnc() {
        zzdqy zzfo = zzdqy.zzfo(this.zzlrn);
        return zzfo == null ? zzdqy.UNRECOGNIZED : zzfo;
    }

    public final zzdrc zzbnd() {
        zzdrc zzfp = zzdrc.zzfp(this.zzlro);
        return zzfp == null ? zzdrc.UNRECOGNIZED : zzfp;
    }

    public final zzfdh zzbne() {
        return this.zzlrp;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlrn != zzdqy.UNKNOWN_CURVE.zzhn()) {
            i2 = 0 + zzfdv.zzag(1, this.zzlrn);
        }
        if (this.zzlro != zzdrc.UNKNOWN_HASH.zzhn()) {
            i2 += zzfdv.zzag(2, this.zzlro);
        }
        if (!this.zzlrp.isEmpty()) {
            i2 += zzfdv.zzb(11, this.zzlrp);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
