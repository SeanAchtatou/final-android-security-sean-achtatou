package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArrayMap;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzbd;
import com.google.android.gms.common.api.internal.zzch;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.common.api.internal.zzcx;
import com.google.android.gms.common.api.internal.zzdi;
import com.google.android.gms.common.api.internal.zzi;
import com.google.android.gms.common.api.internal.zzm;
import com.google.android.gms.common.api.internal.zzw;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.common.internal.zzt;
import com.google.android.gms.internal.zzcvy;
import com.google.android.gms.internal.zzcwb;
import com.google.android.gms.internal.zzcwc;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public abstract class GoogleApiClient {
    public static final int SIGN_IN_MODE_OPTIONAL = 2;
    public static final int SIGN_IN_MODE_REQUIRED = 1;
    /* access modifiers changed from: private */
    public static final Set<GoogleApiClient> zzfjt = Collections.newSetFromMap(new WeakHashMap());

    public static final class Builder {
        private final Context mContext;
        private Looper zzakm;
        private String zzdyu;
        private Account zzdzb;
        private final Set<Scope> zzfju;
        private final Set<Scope> zzfjv;
        private int zzfjw;
        private View zzfjx;
        private String zzfjy;
        private final Map<Api<?>, zzt> zzfjz;
        private final Map<Api<?>, Api.ApiOptions> zzfka;
        private zzch zzfkb;
        private int zzfkc;
        private OnConnectionFailedListener zzfkd;
        private GoogleApiAvailability zzfke;
        private Api.zza<? extends zzcwb, zzcwc> zzfkf;
        private final ArrayList<ConnectionCallbacks> zzfkg;
        private final ArrayList<OnConnectionFailedListener> zzfkh;
        private boolean zzfki;

        public Builder(@NonNull Context context) {
            this.zzfju = new HashSet();
            this.zzfjv = new HashSet();
            this.zzfjz = new ArrayMap();
            this.zzfka = new ArrayMap();
            this.zzfkc = -1;
            this.zzfke = GoogleApiAvailability.getInstance();
            this.zzfkf = zzcvy.zzdyi;
            this.zzfkg = new ArrayList<>();
            this.zzfkh = new ArrayList<>();
            this.zzfki = false;
            this.mContext = context;
            this.zzakm = context.getMainLooper();
            this.zzdyu = context.getPackageName();
            this.zzfjy = context.getClass().getName();
        }

        public Builder(@NonNull Context context, @NonNull ConnectionCallbacks connectionCallbacks, @NonNull OnConnectionFailedListener onConnectionFailedListener) {
            this(context);
            zzbq.checkNotNull(connectionCallbacks, "Must provide a connected listener");
            this.zzfkg.add(connectionCallbacks);
            zzbq.checkNotNull(onConnectionFailedListener, "Must provide a connection failed listener");
            this.zzfkh.add(onConnectionFailedListener);
        }

        private final <O extends Api.ApiOptions> void zza(Api<O> api, O o, Scope... scopeArr) {
            HashSet hashSet = new HashSet(api.zzafr().zzq(o));
            for (Scope add : scopeArr) {
                hashSet.add(add);
            }
            this.zzfjz.put(api, new zzt(hashSet));
        }

        public final Builder addApi(@NonNull Api<? extends Api.ApiOptions.NotRequiredOptions> api) {
            zzbq.checkNotNull(api, "Api must not be null");
            this.zzfka.put(api, null);
            List<Scope> zzq = api.zzafr().zzq(null);
            this.zzfjv.addAll(zzq);
            this.zzfju.addAll(zzq);
            return this;
        }

        public final <O extends Api.ApiOptions.HasOptions> Builder addApi(@NonNull Api<O> api, @NonNull O o) {
            zzbq.checkNotNull(api, "Api must not be null");
            zzbq.checkNotNull(o, "Null options are not permitted for this Api");
            this.zzfka.put(api, o);
            List<Scope> zzq = api.zzafr().zzq(o);
            this.zzfjv.addAll(zzq);
            this.zzfju.addAll(zzq);
            return this;
        }

        public final <O extends Api.ApiOptions.HasOptions> Builder addApiIfAvailable(@NonNull Api<O> api, @NonNull O o, Scope... scopeArr) {
            zzbq.checkNotNull(api, "Api must not be null");
            zzbq.checkNotNull(o, "Null options are not permitted for this Api");
            this.zzfka.put(api, o);
            zza(api, o, scopeArr);
            return this;
        }

        public final Builder addApiIfAvailable(@NonNull Api<? extends Api.ApiOptions.NotRequiredOptions> api, Scope... scopeArr) {
            zzbq.checkNotNull(api, "Api must not be null");
            this.zzfka.put(api, null);
            zza(api, null, scopeArr);
            return this;
        }

        public final Builder addConnectionCallbacks(@NonNull ConnectionCallbacks connectionCallbacks) {
            zzbq.checkNotNull(connectionCallbacks, "Listener must not be null");
            this.zzfkg.add(connectionCallbacks);
            return this;
        }

        public final Builder addOnConnectionFailedListener(@NonNull OnConnectionFailedListener onConnectionFailedListener) {
            zzbq.checkNotNull(onConnectionFailedListener, "Listener must not be null");
            this.zzfkh.add(onConnectionFailedListener);
            return this;
        }

        public final Builder addScope(@NonNull Scope scope) {
            zzbq.checkNotNull(scope, "Scope must not be null");
            this.zzfju.add(scope);
            return this;
        }

        public final GoogleApiClient build() {
            boolean z;
            boolean z2 = true;
            zzbq.checkArgument(!this.zzfka.isEmpty(), "must call addApi() to add at least one API");
            zzr zzagh = zzagh();
            Api api = null;
            Map<Api<?>, zzt> zzakl = zzagh.zzakl();
            ArrayMap arrayMap = new ArrayMap();
            ArrayMap arrayMap2 = new ArrayMap();
            ArrayList arrayList = new ArrayList();
            Iterator<Api<?>> it = this.zzfka.keySet().iterator();
            boolean z3 = false;
            while (it.hasNext()) {
                Api next = it.next();
                Api.ApiOptions apiOptions = this.zzfka.get(next);
                boolean z4 = zzakl.get(next) != null ? z2 : false;
                arrayMap.put(next, Boolean.valueOf(z4));
                zzw zzw = new zzw(next, z4);
                arrayList.add(zzw);
                Api.zza zzafs = next.zzafs();
                Api.zza zza = zzafs;
                zzw zzw2 = zzw;
                Map<Api<?>, zzt> map = zzakl;
                Api api2 = next;
                Iterator<Api<?>> it2 = it;
                Api.zze zza2 = zzafs.zza(this.mContext, this.zzakm, zzagh, apiOptions, zzw2, zzw2);
                arrayMap2.put(api2.zzaft(), zza2);
                if (zza.getPriority() == 1) {
                    z3 = apiOptions != null;
                }
                if (zza2.zzaaw()) {
                    if (api != null) {
                        String name = api2.getName();
                        String name2 = api.getName();
                        StringBuilder sb = new StringBuilder(21 + String.valueOf(name).length() + String.valueOf(name2).length());
                        sb.append(name);
                        sb.append(" cannot be used with ");
                        sb.append(name2);
                        throw new IllegalStateException(sb.toString());
                    }
                    api = api2;
                }
                zzakl = map;
                it = it2;
                z2 = true;
            }
            if (api == null) {
                z = true;
            } else if (z3) {
                String name3 = api.getName();
                StringBuilder sb2 = new StringBuilder(82 + String.valueOf(name3).length());
                sb2.append("With using ");
                sb2.append(name3);
                sb2.append(", GamesOptions can only be specified within GoogleSignInOptions.Builder");
                throw new IllegalStateException(sb2.toString());
            } else {
                z = true;
                zzbq.zza(this.zzdzb == null, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", api.getName());
                zzbq.zza(this.zzfju.equals(this.zzfjv), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", api.getName());
            }
            zzbd zzbd = new zzbd(this.mContext, new ReentrantLock(), this.zzakm, zzagh, this.zzfke, this.zzfkf, arrayMap, this.zzfkg, this.zzfkh, arrayMap2, this.zzfkc, zzbd.zza(arrayMap2.values(), z), arrayList, false);
            synchronized (GoogleApiClient.zzfjt) {
                try {
                    GoogleApiClient.zzfjt.add(zzbd);
                } catch (Throwable th) {
                    while (true) {
                        throw th;
                    }
                }
            }
            if (this.zzfkc >= 0) {
                zzi.zza(this.zzfkb).zza(this.zzfkc, zzbd, this.zzfkd);
            }
            return zzbd;
        }

        public final Builder enableAutoManage(@NonNull FragmentActivity fragmentActivity, int i, @Nullable OnConnectionFailedListener onConnectionFailedListener) {
            zzch zzch = new zzch(fragmentActivity);
            zzbq.checkArgument(i >= 0, "clientId must be non-negative");
            this.zzfkc = i;
            this.zzfkd = onConnectionFailedListener;
            this.zzfkb = zzch;
            return this;
        }

        public final Builder enableAutoManage(@NonNull FragmentActivity fragmentActivity, @Nullable OnConnectionFailedListener onConnectionFailedListener) {
            return enableAutoManage(fragmentActivity, 0, onConnectionFailedListener);
        }

        public final Builder setAccountName(String str) {
            this.zzdzb = str == null ? null : new Account(str, "com.google");
            return this;
        }

        public final Builder setGravityForPopups(int i) {
            this.zzfjw = i;
            return this;
        }

        public final Builder setHandler(@NonNull Handler handler) {
            zzbq.checkNotNull(handler, "Handler must not be null");
            this.zzakm = handler.getLooper();
            return this;
        }

        public final Builder setViewForPopups(@NonNull View view) {
            zzbq.checkNotNull(view, "View must not be null");
            this.zzfjx = view;
            return this;
        }

        public final Builder useDefaultAccount() {
            return setAccountName("<<default account>>");
        }

        public final zzr zzagh() {
            zzcwc zzcwc = zzcwc.zzjyz;
            if (this.zzfka.containsKey(zzcvy.API)) {
                zzcwc = (zzcwc) this.zzfka.get(zzcvy.API);
            }
            return new zzr(this.zzdzb, this.zzfju, this.zzfjz, this.zzfjw, this.zzfjx, this.zzdyu, this.zzfjy, zzcwc);
        }
    }

    public interface ConnectionCallbacks {
        public static final int CAUSE_NETWORK_LOST = 2;
        public static final int CAUSE_SERVICE_DISCONNECTED = 1;

        void onConnected(@Nullable Bundle bundle);

        void onConnectionSuspended(int i);
    }

    public interface OnConnectionFailedListener {
        void onConnectionFailed(@NonNull ConnectionResult connectionResult);
    }

    public static void dumpAll(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        synchronized (zzfjt) {
            int i = 0;
            String concat = String.valueOf(str).concat("  ");
            for (GoogleApiClient dump : zzfjt) {
                printWriter.append((CharSequence) str).append((CharSequence) "GoogleApiClient#").println(i);
                dump.dump(concat, fileDescriptor, printWriter, strArr);
                i++;
            }
        }
    }

    public static Set<GoogleApiClient> zzage() {
        Set<GoogleApiClient> set;
        synchronized (zzfjt) {
            set = zzfjt;
        }
        return set;
    }

    public abstract ConnectionResult blockingConnect();

    public abstract ConnectionResult blockingConnect(long j, @NonNull TimeUnit timeUnit);

    public abstract PendingResult<Status> clearDefaultAccountAndReconnect();

    public abstract void connect();

    public void connect(int i) {
        throw new UnsupportedOperationException();
    }

    public abstract void disconnect();

    public abstract void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @NonNull
    public abstract ConnectionResult getConnectionResult(@NonNull Api<?> api);

    public Context getContext() {
        throw new UnsupportedOperationException();
    }

    public Looper getLooper() {
        throw new UnsupportedOperationException();
    }

    public abstract boolean hasConnectedApi(@NonNull Api<?> api);

    public abstract boolean isConnected();

    public abstract boolean isConnecting();

    public abstract boolean isConnectionCallbacksRegistered(@NonNull ConnectionCallbacks connectionCallbacks);

    public abstract boolean isConnectionFailedListenerRegistered(@NonNull OnConnectionFailedListener onConnectionFailedListener);

    public abstract void reconnect();

    public abstract void registerConnectionCallbacks(@NonNull ConnectionCallbacks connectionCallbacks);

    public abstract void registerConnectionFailedListener(@NonNull OnConnectionFailedListener onConnectionFailedListener);

    public abstract void stopAutoManage(@NonNull FragmentActivity fragmentActivity);

    public abstract void unregisterConnectionCallbacks(@NonNull ConnectionCallbacks connectionCallbacks);

    public abstract void unregisterConnectionFailedListener(@NonNull OnConnectionFailedListener onConnectionFailedListener);

    @NonNull
    public <C extends Api.zze> C zza(@NonNull Api.zzc<C> zzc) {
        throw new UnsupportedOperationException();
    }

    public void zza(zzdi zzdi) {
        throw new UnsupportedOperationException();
    }

    public boolean zza(@NonNull Api<?> api) {
        throw new UnsupportedOperationException();
    }

    public boolean zza(zzcx zzcx) {
        throw new UnsupportedOperationException();
    }

    public void zzagf() {
        throw new UnsupportedOperationException();
    }

    public void zzb(zzdi zzdi) {
        throw new UnsupportedOperationException();
    }

    public <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T zzd(@NonNull T t) {
        throw new UnsupportedOperationException();
    }

    public <A extends Api.zzb, T extends zzm<? extends Result, A>> T zze(@NonNull T t) {
        throw new UnsupportedOperationException();
    }

    public <L> zzcl<L> zzs(@NonNull L l) {
        throw new UnsupportedOperationException();
    }
}
