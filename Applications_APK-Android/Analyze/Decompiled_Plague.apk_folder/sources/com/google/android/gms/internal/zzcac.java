package com.google.android.gms.internal;

import android.os.Handler;
import android.os.Looper;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class zzcac {
    private Object zzhrd = new Object();
    private Handler zzhre;
    private boolean zzhrf;
    private HashMap<String, AtomicInteger> zzhrg;
    private int zzhrh;

    public zzcac(Looper looper, int i) {
        this.zzhre = new Handler(looper);
        this.zzhrg = new HashMap<>();
        this.zzhrh = 1000;
    }

    /* access modifiers changed from: private */
    public final void zzatb() {
        synchronized (this.zzhrd) {
            this.zzhrf = false;
            flush();
        }
    }

    public final void flush() {
        synchronized (this.zzhrd) {
            for (Map.Entry next : this.zzhrg.entrySet()) {
                zzu((String) next.getKey(), ((AtomicInteger) next.getValue()).get());
            }
            this.zzhrg.clear();
        }
    }

    /* access modifiers changed from: protected */
    public abstract void zzu(String str, int i);

    public final void zzv(String str, int i) {
        synchronized (this.zzhrd) {
            if (!this.zzhrf) {
                this.zzhrf = true;
                this.zzhre.postDelayed(new zzcad(this), (long) this.zzhrh);
            }
            AtomicInteger atomicInteger = this.zzhrg.get(str);
            if (atomicInteger == null) {
                atomicInteger = new AtomicInteger();
                this.zzhrg.put(str, atomicInteger);
            }
            atomicInteger.addAndGet(i);
        }
    }
}
