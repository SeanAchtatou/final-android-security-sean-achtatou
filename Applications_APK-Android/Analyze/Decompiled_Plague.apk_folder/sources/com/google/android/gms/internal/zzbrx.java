package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzbrx extends zzbrs<MetadataBuffer> {
    public zzbrx(TaskCompletionSource<MetadataBuffer> taskCompletionSource) {
        super(taskCompletionSource);
    }

    public final void zza(zzbqg zzbqg) throws RemoteException {
        zzaox().setResult(new MetadataBuffer(zzbqg.zzaov()));
    }
}
