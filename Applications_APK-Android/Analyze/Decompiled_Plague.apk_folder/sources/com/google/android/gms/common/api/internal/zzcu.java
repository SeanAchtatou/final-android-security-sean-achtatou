package com.google.android.gms.common.api.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.Api;

public final class zzcu {
    public final zzct<Api.zzb, ?> zzfkw;
    public final zzdp<Api.zzb, ?> zzfkx;

    public zzcu(@NonNull zzct<Api.zzb, ?> zzct, @NonNull zzdp<Api.zzb, ?> zzdp) {
        this.zzfkw = zzct;
        this.zzfkx = zzdp;
    }
}
