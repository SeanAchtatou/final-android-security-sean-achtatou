package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzdg extends zzdp {
    private /* synthetic */ int zzhqz;
    private /* synthetic */ int[] zzhra;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdg(zzcw zzcw, GoogleApiClient googleApiClient, int i, int[] iArr) {
        super(googleApiClient, null);
        this.zzhqz = i;
        this.zzhra = iArr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhqz, this.zzhra);
    }
}
