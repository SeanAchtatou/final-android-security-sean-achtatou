package com.google.android.gms.common.api.internal;

import android.os.IBinder;
import com.google.android.gms.common.api.zze;
import java.lang.ref.WeakReference;
import java.util.NoSuchElementException;

final class zzdn implements IBinder.DeathRecipient, zzdo {
    private final WeakReference<zzs<?>> zzfsr;
    private final WeakReference<zze> zzfss;
    private final WeakReference<IBinder> zzfst;

    private zzdn(zzs<?> zzs, zze zze, IBinder iBinder) {
        this.zzfss = new WeakReference<>(zze);
        this.zzfsr = new WeakReference<>(zzs);
        this.zzfst = new WeakReference<>(iBinder);
    }

    /* synthetic */ zzdn(zzs zzs, zze zze, IBinder iBinder, zzdm zzdm) {
        this(zzs, null, iBinder);
    }

    private final void zzajj() {
        zzs zzs = this.zzfsr.get();
        zze zze = this.zzfss.get();
        if (!(zze == null || zzs == null)) {
            zze.remove(zzs.zzagi().intValue());
        }
        IBinder iBinder = this.zzfst.get();
        if (iBinder != null) {
            try {
                iBinder.unlinkToDeath(this, 0);
            } catch (NoSuchElementException unused) {
            }
        }
    }

    public final void binderDied() {
        zzajj();
    }

    public final void zzc(zzs<?> zzs) {
        zzajj();
    }
}
