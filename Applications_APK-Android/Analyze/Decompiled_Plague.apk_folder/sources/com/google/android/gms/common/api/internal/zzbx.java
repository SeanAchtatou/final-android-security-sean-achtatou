package com.google.android.gms.common.api.internal;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzan;
import com.google.android.gms.common.internal.zzj;
import java.util.Set;

final class zzbx implements zzda, zzj {
    private Set<Scope> zzees = null;
    /* access modifiers changed from: private */
    public final zzh<?> zzfjl;
    /* access modifiers changed from: private */
    public final Api.zze zzfnb;
    private zzan zzfon = null;
    final /* synthetic */ zzbp zzfqo;
    /* access modifiers changed from: private */
    public boolean zzfra = false;

    public zzbx(zzbp zzbp, Api.zze zze, zzh<?> zzh) {
        this.zzfqo = zzbp;
        this.zzfnb = zze;
        this.zzfjl = zzh;
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public final void zzaiu() {
        if (this.zzfra && this.zzfon != null) {
            this.zzfnb.zza(this.zzfon, this.zzees);
        }
    }

    @WorkerThread
    public final void zzb(zzan zzan, Set<Scope> set) {
        if (zzan == null || set == null) {
            Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", new Exception());
            zzh(new ConnectionResult(4));
            return;
        }
        this.zzfon = zzan;
        this.zzees = set;
        zzaiu();
    }

    public final void zzf(@NonNull ConnectionResult connectionResult) {
        this.zzfqo.mHandler.post(new zzby(this, connectionResult));
    }

    @WorkerThread
    public final void zzh(ConnectionResult connectionResult) {
        ((zzbr) this.zzfqo.zzfne.get(this.zzfjl)).zzh(connectionResult);
    }
}
