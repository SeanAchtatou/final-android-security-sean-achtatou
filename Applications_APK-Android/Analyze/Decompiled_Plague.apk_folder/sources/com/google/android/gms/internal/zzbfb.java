package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzbfb extends IInterface {
    void zza(zzbez zzbez) throws RemoteException;
}
