package com.google.android.gms.common.api.internal;

import android.support.annotation.WorkerThread;
import com.google.android.gms.common.api.Api;
import java.util.ArrayList;

final class zzax extends zzbb {
    private /* synthetic */ zzar zzfor;
    private final ArrayList<Api.zze> zzfox;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzax(zzar zzar, ArrayList<Api.zze> arrayList) {
        super(zzar, null);
        this.zzfor = zzar;
        this.zzfox = arrayList;
    }

    @WorkerThread
    public final void zzahp() {
        this.zzfor.zzfob.zzfmo.zzfpi = this.zzfor.zzahv();
        ArrayList arrayList = this.zzfox;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList.get(i);
            i++;
            ((Api.zze) obj).zza(this.zzfor.zzfon, this.zzfor.zzfob.zzfmo.zzfpi);
        }
    }
}
