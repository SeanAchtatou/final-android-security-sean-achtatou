package com.google.android.gms.internal;

final class zzfgc {
    static String zzaq(zzfdh zzfdh) {
        String str;
        char c;
        zzfgd zzfgd = new zzfgd(zzfdh);
        StringBuilder sb = new StringBuilder(zzfgd.size());
        for (int i = 0; i < zzfgd.size(); i++) {
            byte zzkd = zzfgd.zzkd(i);
            if (zzkd == 34) {
                str = "\\\"";
            } else if (zzkd == 39) {
                str = "\\'";
            } else if (zzkd != 92) {
                switch (zzkd) {
                    case 7:
                        str = "\\a";
                        break;
                    case 8:
                        str = "\\b";
                        break;
                    case 9:
                        str = "\\t";
                        break;
                    case 10:
                        str = "\\n";
                        break;
                    case 11:
                        str = "\\v";
                        break;
                    case 12:
                        str = "\\f";
                        break;
                    case 13:
                        str = "\\r";
                        break;
                    default:
                        if (zzkd < 32 || zzkd > 126) {
                            sb.append('\\');
                            sb.append((char) (((zzkd >>> 6) & 3) + 48));
                            sb.append((char) (((zzkd >>> 3) & 7) + 48));
                            c = (char) (48 + (zzkd & 7));
                        } else {
                            c = (char) zzkd;
                        }
                        sb.append(c);
                        continue;
                        break;
                }
            } else {
                str = "\\\\";
            }
            sb.append(str);
        }
        return sb.toString();
    }
}
