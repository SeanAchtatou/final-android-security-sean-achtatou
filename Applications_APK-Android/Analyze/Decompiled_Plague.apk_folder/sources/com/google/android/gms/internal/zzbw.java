package com.google.android.gms.internal;

final class zzbw {
    static zzdor zzyu;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbr.zza(java.lang.String, boolean):byte[]
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbr.zza(byte[], boolean):java.lang.String
      com.google.android.gms.internal.zzbr.zza(java.lang.String, boolean):byte[] */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static boolean zzz() {
        /*
            com.google.android.gms.internal.zzdor r0 = com.google.android.gms.internal.zzbw.zzyu
            r1 = 1
            if (r0 == 0) goto L_0x0006
            return r1
        L_0x0006:
            com.google.android.gms.internal.zzmg<java.lang.String> r0 = com.google.android.gms.internal.zzmq.zzbmb
            com.google.android.gms.internal.zzmo r2 = com.google.android.gms.ads.internal.zzbs.zzep()
            java.lang.Object r0 = r2.zzd(r0)
            java.lang.String r0 = (java.lang.String) r0
            int r2 = r0.length()
            r3 = 0
            if (r2 != 0) goto L_0x001a
            return r3
        L_0x001a:
            byte[] r0 = com.google.android.gms.internal.zzbr.zza(r0, r1)     // Catch:{ IllegalArgumentException -> 0x0030 }
            com.google.android.gms.internal.zzdot r0 = com.google.android.gms.internal.zzdov.zzac(r0)     // Catch:{  }
            com.google.android.gms.internal.zzdpi.zzbli()     // Catch:{  }
            com.google.android.gms.internal.zzdor r0 = com.google.android.gms.internal.zzdpj.zza(r0)     // Catch:{  }
            com.google.android.gms.internal.zzbw.zzyu = r0     // Catch:{  }
            com.google.android.gms.internal.zzdor r0 = com.google.android.gms.internal.zzbw.zzyu
            if (r0 == 0) goto L_0x0030
            return r1
        L_0x0030:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzbw.zzz():boolean");
    }
}
