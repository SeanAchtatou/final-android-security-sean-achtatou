package com.google.android.gms.internal;

import android.content.Context;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Pair;
import com.google.android.gms.common.internal.zzal;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.drive.events.DriveEvent;
import com.google.android.gms.drive.events.zzi;
import java.util.ArrayList;
import java.util.List;

public final class zzbov extends zzbpk {
    /* access modifiers changed from: private */
    public static final zzal zzggp = new zzal("EventCallback", "");
    private final int zzgas = 1;
    private final zzi zzgnj;
    private final zzbox zzgnk;
    private final List<Integer> zzgnl = new ArrayList();

    public zzbov(Looper looper, Context context, int i, zzi zzi) {
        this.zzgnj = zzi;
        this.zzgnk = new zzbox(looper, context);
    }

    public final void zzc(zzbqa zzbqa) throws RemoteException {
        DriveEvent zzaou = zzbqa.zzaou();
        zzbq.checkState(this.zzgas == zzaou.getType());
        zzbq.checkState(this.zzgnl.contains(Integer.valueOf(zzaou.getType())));
        zzbox zzbox = this.zzgnk;
        zzbox.sendMessage(zzbox.obtainMessage(1, new Pair(this.zzgnj, zzaou)));
    }

    public final void zzcv(int i) {
        this.zzgnl.add(1);
    }

    public final boolean zzcw(int i) {
        return this.zzgnl.contains(1);
    }
}
