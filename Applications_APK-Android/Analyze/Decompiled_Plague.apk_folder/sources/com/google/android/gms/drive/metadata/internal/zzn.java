package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbek;
import java.util.ArrayList;

public final class zzn implements Parcelable.Creator<ParentDriveIdSet> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        ArrayList arrayList = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                zzbek.zzb(parcel, readInt);
            } else {
                arrayList = zzbek.zzc(parcel, readInt, zzq.CREATOR);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new ParentDriveIdSet(arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new ParentDriveIdSet[i];
    }
}
