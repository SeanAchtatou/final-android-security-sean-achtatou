package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.common.api.internal.zzct;
import com.google.android.gms.common.internal.zzar;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.events.OpenFileCallback;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbni extends zzct<zzbll, OpenFileCallback> {
    private /* synthetic */ DriveFile zzgme;
    private /* synthetic */ int zzgmf;
    private /* synthetic */ zzbjq zzgmg;
    private /* synthetic */ zzcl zzgmh;
    private /* synthetic */ zzbmu zzgmi;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbni(zzbmu zzbmu, zzcl zzcl, DriveFile driveFile, int i, zzbjq zzbjq, zzcl zzcl2) {
        super(zzcl);
        this.zzgmi = zzbmu;
        this.zzgme = driveFile;
        this.zzgmf = i;
        this.zzgmg = zzbjq;
        this.zzgmh = zzcl2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zzb(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        this.zzgmg.zza(zzar.zzak(((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbqu(this.zzgme.getDriveId(), this.zzgmf, 0), new zzbob(this.zzgmi, this.zzgmg, this.zzgmh)).zzgni));
        taskCompletionSource.setResult(null);
    }
}
