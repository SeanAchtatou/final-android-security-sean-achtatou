package com.google.android.gms.internal;

import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.drive.metadata.internal.zzi;

public final class zzbsk extends zzi implements SortableMetadataField<Long> {
    public zzbsk(String str, int i) {
        super(str, 4300000);
    }
}
