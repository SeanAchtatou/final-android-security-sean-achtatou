package com.google.android.gms.internal;

import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzm;

final class zzbdj extends zzm<Status, zzbdl> {
    private final zzbde zzfhm;

    zzbdj(zzbde zzbde, GoogleApiClient googleApiClient) {
        super(zzbcv.API, googleApiClient);
        this.zzfhm = zzbde;
    }

    public final /* bridge */ /* synthetic */ void setResult(Object obj) {
        super.setResult((Result) ((Status) obj));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        zzbdl zzbdl = (zzbdl) zzb;
        zzbdk zzbdk = new zzbdk(this);
        try {
            zzbde zzbde = this.zzfhm;
            if (zzbde.zzfgt != null && zzbde.zzfha.zzpkn.length == 0) {
                zzbde.zzfha.zzpkn = zzbde.zzfgt.zzafk();
            }
            if (zzbde.zzfhl != null && zzbde.zzfha.zzpku.length == 0) {
                zzbde.zzfha.zzpku = zzbde.zzfhl.zzafk();
            }
            zzbde.zzfhg = zzfhk.zzc(zzbde.zzfha);
            ((zzbdp) zzbdl.zzakb()).zza(zzbdk, this.zzfhm);
        } catch (RuntimeException e) {
            Log.e("ClearcutLoggerApiImpl", "derived ClearcutLogger.MessageProducer ", e);
            zzu(new Status(10, "MessageProducer"));
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Result zzb(Status status) {
        return status;
    }
}
