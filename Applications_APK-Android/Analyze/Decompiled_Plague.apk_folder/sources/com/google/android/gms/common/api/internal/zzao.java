package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;

public final class zzao implements zzbk {
    /* access modifiers changed from: private */
    public final zzbl zzfob;
    private boolean zzfoc = false;

    public zzao(zzbl zzbl) {
        this.zzfob = zzbl;
    }

    public final void begin() {
    }

    public final void connect() {
        if (this.zzfoc) {
            this.zzfoc = false;
            this.zzfob.zza(new zzaq(this, this));
        }
    }

    public final boolean disconnect() {
        if (this.zzfoc) {
            return false;
        }
        if (this.zzfob.zzfmo.zzahz()) {
            this.zzfoc = true;
            for (zzdi zzajg : this.zzfob.zzfmo.zzfpm) {
                zzajg.zzajg();
            }
            return false;
        }
        this.zzfob.zzg(null);
        return true;
    }

    public final void onConnected(Bundle bundle) {
    }

    public final void onConnectionSuspended(int i) {
        this.zzfob.zzg(null);
        this.zzfob.zzfqa.zzf(i, this.zzfoc);
    }

    public final void zza(ConnectionResult connectionResult, Api<?> api, boolean z) {
    }

    /* access modifiers changed from: package-private */
    public final void zzaho() {
        if (this.zzfoc) {
            this.zzfoc = false;
            this.zzfob.zzfmo.zzfpn.release();
            disconnect();
        }
    }

    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T zzd(T t) {
        return zze(t);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final <A extends com.google.android.gms.common.api.Api.zzb, T extends com.google.android.gms.common.api.internal.zzm<? extends com.google.android.gms.common.api.Result, A>> T zze(T r4) {
        /*
            r3 = this;
            com.google.android.gms.common.api.internal.zzbl r0 = r3.zzfob     // Catch:{ DeadObjectException -> 0x0049 }
            com.google.android.gms.common.api.internal.zzbd r0 = r0.zzfmo     // Catch:{ DeadObjectException -> 0x0049 }
            com.google.android.gms.common.api.internal.zzdl r0 = r0.zzfpn     // Catch:{ DeadObjectException -> 0x0049 }
            r0.zzb(r4)     // Catch:{ DeadObjectException -> 0x0049 }
            com.google.android.gms.common.api.internal.zzbl r0 = r3.zzfob     // Catch:{ DeadObjectException -> 0x0049 }
            com.google.android.gms.common.api.internal.zzbd r0 = r0.zzfmo     // Catch:{ DeadObjectException -> 0x0049 }
            com.google.android.gms.common.api.Api$zzc r1 = r4.zzaft()     // Catch:{ DeadObjectException -> 0x0049 }
            java.util.Map<com.google.android.gms.common.api.Api$zzc<?>, com.google.android.gms.common.api.Api$zze> r0 = r0.zzfph     // Catch:{ DeadObjectException -> 0x0049 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ DeadObjectException -> 0x0049 }
            com.google.android.gms.common.api.Api$zze r0 = (com.google.android.gms.common.api.Api.zze) r0     // Catch:{ DeadObjectException -> 0x0049 }
            java.lang.String r1 = "Appropriate Api was not requested."
            com.google.android.gms.common.internal.zzbq.checkNotNull(r0, r1)     // Catch:{ DeadObjectException -> 0x0049 }
            boolean r1 = r0.isConnected()     // Catch:{ DeadObjectException -> 0x0049 }
            if (r1 != 0) goto L_0x003d
            com.google.android.gms.common.api.internal.zzbl r1 = r3.zzfob     // Catch:{ DeadObjectException -> 0x0049 }
            java.util.Map<com.google.android.gms.common.api.Api$zzc<?>, com.google.android.gms.common.ConnectionResult> r1 = r1.zzfpw     // Catch:{ DeadObjectException -> 0x0049 }
            com.google.android.gms.common.api.Api$zzc r2 = r4.zzaft()     // Catch:{ DeadObjectException -> 0x0049 }
            boolean r1 = r1.containsKey(r2)     // Catch:{ DeadObjectException -> 0x0049 }
            if (r1 == 0) goto L_0x003d
            com.google.android.gms.common.api.Status r0 = new com.google.android.gms.common.api.Status     // Catch:{ DeadObjectException -> 0x0049 }
            r1 = 17
            r0.<init>(r1)     // Catch:{ DeadObjectException -> 0x0049 }
            r4.zzu(r0)     // Catch:{ DeadObjectException -> 0x0049 }
            return r4
        L_0x003d:
            boolean r1 = r0 instanceof com.google.android.gms.common.internal.zzbz     // Catch:{ DeadObjectException -> 0x0049 }
            if (r1 == 0) goto L_0x0045
            com.google.android.gms.common.api.Api$zzg r0 = com.google.android.gms.common.internal.zzbz.zzalg()     // Catch:{ DeadObjectException -> 0x0049 }
        L_0x0045:
            r4.zzb(r0)     // Catch:{ DeadObjectException -> 0x0049 }
            return r4
        L_0x0049:
            com.google.android.gms.common.api.internal.zzbl r0 = r3.zzfob
            com.google.android.gms.common.api.internal.zzap r1 = new com.google.android.gms.common.api.internal.zzap
            r1.<init>(r3, r3)
            r0.zza(r1)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzao.zze(com.google.android.gms.common.api.internal.zzm):com.google.android.gms.common.api.internal.zzm");
    }
}
