package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfee;

public final class zzfeg<T extends zzfee<T, ?>> extends zzfdc<T> {
    private T zzpbu;

    public zzfeg(T t) {
        this.zzpbu = t;
    }

    public final /* synthetic */ Object zze(zzfdq zzfdq, zzfea zzfea) throws zzfew {
        return zzfee.zza(this.zzpbu, zzfdq, zzfea);
    }
}
