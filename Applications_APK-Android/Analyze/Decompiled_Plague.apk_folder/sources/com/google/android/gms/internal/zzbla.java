package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveId;

final class zzbla implements DriveApi.DriveIdResult {
    private final Status mStatus;
    private final DriveId zzgfy;

    public zzbla(Status status, DriveId driveId) {
        this.mStatus = status;
        this.zzgfy = driveId;
    }

    public final DriveId getDriveId() {
        return this.zzgfy;
    }

    public final Status getStatus() {
        return this.mStatus;
    }
}
