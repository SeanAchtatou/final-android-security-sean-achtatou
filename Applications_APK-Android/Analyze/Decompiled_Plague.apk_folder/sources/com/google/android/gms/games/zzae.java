package com.google.android.gms.games;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;

final class zzae implements zzbo<Leaderboards.SubmitScoreResult, ScoreSubmissionData> {
    zzae() {
    }

    public final /* synthetic */ Object zzb(Result result) {
        Leaderboards.SubmitScoreResult submitScoreResult = (Leaderboards.SubmitScoreResult) result;
        if (submitScoreResult == null) {
            return null;
        }
        return submitScoreResult.getScoreData();
    }
}
