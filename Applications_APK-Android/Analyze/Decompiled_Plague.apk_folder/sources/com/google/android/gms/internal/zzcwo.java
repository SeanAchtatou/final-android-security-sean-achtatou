package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzbt;

public final class zzcwo extends zzbej {
    public static final Parcelable.Creator<zzcwo> CREATOR = new zzcwp();
    private int zzdzm;
    private final ConnectionResult zzflt;
    private final zzbt zzjzj;

    public zzcwo(int i) {
        this(new ConnectionResult(8, null), null);
    }

    zzcwo(int i, ConnectionResult connectionResult, zzbt zzbt) {
        this.zzdzm = i;
        this.zzflt = connectionResult;
        this.zzjzj = zzbt;
    }

    private zzcwo(ConnectionResult connectionResult, zzbt zzbt) {
        this(1, connectionResult, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.ConnectionResult, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.internal.zzbt, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 1, this.zzdzm);
        zzbem.zza(parcel, 2, (Parcelable) this.zzflt, i, false);
        zzbem.zza(parcel, 3, (Parcelable) this.zzjzj, i, false);
        zzbem.zzai(parcel, zze);
    }

    public final ConnectionResult zzagt() {
        return this.zzflt;
    }

    public final zzbt zzbcw() {
        return this.zzjzj;
    }
}
