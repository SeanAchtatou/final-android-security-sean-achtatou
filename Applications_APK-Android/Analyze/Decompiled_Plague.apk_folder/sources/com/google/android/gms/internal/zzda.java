package com.google.android.gms.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.VisibleForTesting;
import android.support.v4.widget.ExploreByTouchHelper;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class zzda {
    private static final String TAG = "zzda";
    private volatile boolean zzahk = false;
    protected Context zzaif;
    private Context zzaig;
    private ExecutorService zzaih;
    private DexClassLoader zzaii;
    private zzcu zzaij;
    private byte[] zzaik;
    private volatile AdvertisingIdClient zzail = null;
    private Future zzaim = null;
    /* access modifiers changed from: private */
    public volatile zzaw zzain = null;
    private Future zzaio = null;
    private zzcm zzaip;
    private boolean zzaiq = false;
    private boolean zzair = false;
    private Map<Pair<String, String>, zzea> zzais;
    private boolean zzait = false;
    /* access modifiers changed from: private */
    public boolean zzaiu = true;
    private boolean zzaiv = false;

    final class zza extends BroadcastReceiver {
        private zza() {
        }

        /* synthetic */ zza(zzda zzda, zzdb zzdb) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.zzda.zza(com.google.android.gms.internal.zzda, boolean):boolean
         arg types: [com.google.android.gms.internal.zzda, int]
         candidates:
          com.google.android.gms.internal.zzda.zza(com.google.android.gms.internal.zzda, com.google.android.gms.internal.zzaw):com.google.android.gms.internal.zzaw
          com.google.android.gms.internal.zzda.zza(java.io.File, java.lang.String):void
          com.google.android.gms.internal.zzda.zza(int, com.google.android.gms.internal.zzaw):boolean
          com.google.android.gms.internal.zzda.zza(java.lang.String, java.lang.String):java.lang.reflect.Method
          com.google.android.gms.internal.zzda.zza(int, boolean):void
          com.google.android.gms.internal.zzda.zza(com.google.android.gms.internal.zzda, boolean):boolean */
        public final void onReceive(Context context, Intent intent) {
            if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
                boolean unused = zzda.this.zzaiu = true;
            } else if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                boolean unused2 = zzda.this.zzaiu = false;
            }
        }
    }

    private zzda(Context context) {
        this.zzaif = context;
        this.zzaig = context.getApplicationContext();
        this.zzais = new HashMap();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzda.zza(int, boolean):void
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.zzda.zza(com.google.android.gms.internal.zzda, com.google.android.gms.internal.zzaw):com.google.android.gms.internal.zzaw
      com.google.android.gms.internal.zzda.zza(java.io.File, java.lang.String):void
      com.google.android.gms.internal.zzda.zza(int, com.google.android.gms.internal.zzaw):boolean
      com.google.android.gms.internal.zzda.zza(com.google.android.gms.internal.zzda, boolean):boolean
      com.google.android.gms.internal.zzda.zza(java.lang.String, java.lang.String):java.lang.reflect.Method
      com.google.android.gms.internal.zzda.zza(int, boolean):void */
    /* JADX WARNING: Can't wrap try/catch for region: R(18:1|2|(1:4)|5|6|7|8|(1:10)(1:11)|12|(1:14)(1:15)|16|17|18|(2:20|(2:22|23))|24|25|26|(3:27|28|(12:34|(1:36)|37|38|39|40|41|42|43|(2:47|(1:49)(1:50))|51|52)(2:32|33))) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0046 */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004f A[Catch:{ zzcv -> 0x0162, zzcx -> 0x0169 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ab A[Catch:{ all -> 0x012f, FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0109 A[Catch:{ zzcv -> 0x0162, zzcx -> 0x0169 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x010a A[Catch:{ zzcv -> 0x0162, zzcx -> 0x0169 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.internal.zzda zza(android.content.Context r8, java.lang.String r9, java.lang.String r10, boolean r11) {
        /*
            com.google.android.gms.internal.zzda r0 = new com.google.android.gms.internal.zzda
            r0.<init>(r8)
            java.util.concurrent.ExecutorService r8 = java.util.concurrent.Executors.newCachedThreadPool()     // Catch:{ zzcx -> 0x0169 }
            r0.zzaih = r8     // Catch:{ zzcx -> 0x0169 }
            r0.zzahk = r11     // Catch:{ zzcx -> 0x0169 }
            if (r11 == 0) goto L_0x001c
            java.util.concurrent.ExecutorService r8 = r0.zzaih     // Catch:{ zzcx -> 0x0169 }
            com.google.android.gms.internal.zzdb r11 = new com.google.android.gms.internal.zzdb     // Catch:{ zzcx -> 0x0169 }
            r11.<init>(r0)     // Catch:{ zzcx -> 0x0169 }
            java.util.concurrent.Future r8 = r8.submit(r11)     // Catch:{ zzcx -> 0x0169 }
            r0.zzaim = r8     // Catch:{ zzcx -> 0x0169 }
        L_0x001c:
            java.util.concurrent.ExecutorService r8 = r0.zzaih     // Catch:{ zzcx -> 0x0169 }
            com.google.android.gms.internal.zzdd r11 = new com.google.android.gms.internal.zzdd     // Catch:{ zzcx -> 0x0169 }
            r11.<init>(r0)     // Catch:{ zzcx -> 0x0169 }
            r8.execute(r11)     // Catch:{ zzcx -> 0x0169 }
            r8 = 0
            r11 = 1
            com.google.android.gms.common.zze r1 = com.google.android.gms.common.zze.zzafm()     // Catch:{ Throwable -> 0x0046 }
            android.content.Context r2 = r0.zzaif     // Catch:{ Throwable -> 0x0046 }
            int r2 = com.google.android.gms.common.zze.zzcd(r2)     // Catch:{ Throwable -> 0x0046 }
            if (r2 <= 0) goto L_0x0036
            r2 = r11
            goto L_0x0037
        L_0x0036:
            r2 = r8
        L_0x0037:
            r0.zzaiq = r2     // Catch:{ Throwable -> 0x0046 }
            android.content.Context r2 = r0.zzaif     // Catch:{ Throwable -> 0x0046 }
            int r1 = r1.isGooglePlayServicesAvailable(r2)     // Catch:{ Throwable -> 0x0046 }
            if (r1 != 0) goto L_0x0043
            r1 = r11
            goto L_0x0044
        L_0x0043:
            r1 = r8
        L_0x0044:
            r0.zzair = r1     // Catch:{ Throwable -> 0x0046 }
        L_0x0046:
            r0.zza(r8, r11)     // Catch:{ zzcx -> 0x0169 }
            boolean r1 = com.google.android.gms.internal.zzdf.zzas()     // Catch:{ zzcx -> 0x0169 }
            if (r1 == 0) goto L_0x0069
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r1 = com.google.android.gms.internal.zzmq.zzbmd     // Catch:{ zzcx -> 0x0169 }
            com.google.android.gms.internal.zzmo r2 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ zzcx -> 0x0169 }
            java.lang.Object r1 = r2.zzd(r1)     // Catch:{ zzcx -> 0x0169 }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ zzcx -> 0x0169 }
            boolean r1 = r1.booleanValue()     // Catch:{ zzcx -> 0x0169 }
            if (r1 == 0) goto L_0x0069
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException     // Catch:{ zzcx -> 0x0169 }
            java.lang.String r9 = "Task Context initialization must not be called from the UI thread."
            r8.<init>(r9)     // Catch:{ zzcx -> 0x0169 }
            throw r8     // Catch:{ zzcx -> 0x0169 }
        L_0x0069:
            com.google.android.gms.internal.zzcu r1 = new com.google.android.gms.internal.zzcu     // Catch:{ zzcx -> 0x0169 }
            r2 = 0
            r1.<init>(r2)     // Catch:{ zzcx -> 0x0169 }
            r0.zzaij = r1     // Catch:{ zzcx -> 0x0169 }
            com.google.android.gms.internal.zzcu r1 = r0.zzaij     // Catch:{ zzcv -> 0x0162 }
            byte[] r9 = r1.zzk(r9)     // Catch:{ zzcv -> 0x0162 }
            r0.zzaik = r9     // Catch:{ zzcv -> 0x0162 }
            android.content.Context r9 = r0.zzaif     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            java.io.File r9 = r9.getCacheDir()     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            if (r9 != 0) goto L_0x0091
            android.content.Context r9 = r0.zzaif     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            java.lang.String r1 = "dex"
            java.io.File r9 = r9.getDir(r1, r8)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            if (r9 != 0) goto L_0x0091
            com.google.android.gms.internal.zzcx r8 = new com.google.android.gms.internal.zzcx     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r8.<init>()     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            throw r8     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
        L_0x0091:
            java.lang.String r1 = "1501670890290"
            java.io.File r3 = new java.io.File     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            java.lang.String r4 = "%s/%s.jar"
            r5 = 2
            java.lang.Object[] r6 = new java.lang.Object[r5]     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r6[r8] = r9     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r6[r11] = r1     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            java.lang.String r4 = java.lang.String.format(r4, r6)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r3.<init>(r4)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            boolean r4 = r3.exists()     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            if (r4 != 0) goto L_0x00c2
            com.google.android.gms.internal.zzcu r4 = r0.zzaij     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            byte[] r6 = r0.zzaik     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            byte[] r10 = r4.zzb(r6, r10)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r3.createNewFile()     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r4.<init>(r3)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            int r6 = r10.length     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r4.write(r10, r8, r6)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r4.close()     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
        L_0x00c2:
            r0.zzb(r9, r1)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            dalvik.system.DexClassLoader r10 = new dalvik.system.DexClassLoader     // Catch:{ all -> 0x012f }
            java.lang.String r4 = r3.getAbsolutePath()     // Catch:{ all -> 0x012f }
            java.lang.String r6 = r9.getAbsolutePath()     // Catch:{ all -> 0x012f }
            android.content.Context r7 = r0.zzaif     // Catch:{ all -> 0x012f }
            java.lang.ClassLoader r7 = r7.getClassLoader()     // Catch:{ all -> 0x012f }
            r10.<init>(r4, r6, r2, r7)     // Catch:{ all -> 0x012f }
            r0.zzaii = r10     // Catch:{ all -> 0x012f }
            zzb(r3)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r0.zza(r9, r1)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            java.lang.String r10 = "%s/%s.dex"
            java.lang.Object[] r3 = new java.lang.Object[r5]     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r3[r8] = r9     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r3[r11] = r1     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            java.lang.String r8 = java.lang.String.format(r10, r3)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            zzl(r8)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r8 = com.google.android.gms.internal.zzmq.zzblx     // Catch:{ zzcx -> 0x0169 }
            com.google.android.gms.internal.zzmo r9 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ zzcx -> 0x0169 }
            java.lang.Object r8 = r9.zzd(r8)     // Catch:{ zzcx -> 0x0169 }
            java.lang.Boolean r8 = (java.lang.Boolean) r8     // Catch:{ zzcx -> 0x0169 }
            boolean r8 = r8.booleanValue()     // Catch:{ zzcx -> 0x0169 }
            if (r8 == 0) goto L_0x0125
            boolean r8 = r0.zzaiv     // Catch:{ zzcx -> 0x0169 }
            if (r8 != 0) goto L_0x0125
            android.content.Context r8 = r0.zzaig     // Catch:{ zzcx -> 0x0169 }
            if (r8 != 0) goto L_0x010a
            goto L_0x0125
        L_0x010a:
            android.content.IntentFilter r8 = new android.content.IntentFilter     // Catch:{ zzcx -> 0x0169 }
            r8.<init>()     // Catch:{ zzcx -> 0x0169 }
            java.lang.String r9 = "android.intent.action.USER_PRESENT"
            r8.addAction(r9)     // Catch:{ zzcx -> 0x0169 }
            java.lang.String r9 = "android.intent.action.SCREEN_OFF"
            r8.addAction(r9)     // Catch:{ zzcx -> 0x0169 }
            android.content.Context r9 = r0.zzaig     // Catch:{ zzcx -> 0x0169 }
            com.google.android.gms.internal.zzda$zza r10 = new com.google.android.gms.internal.zzda$zza     // Catch:{ zzcx -> 0x0169 }
            r10.<init>(r0, r2)     // Catch:{ zzcx -> 0x0169 }
            r9.registerReceiver(r10, r8)     // Catch:{ zzcx -> 0x0169 }
            r0.zzaiv = r11     // Catch:{ zzcx -> 0x0169 }
        L_0x0125:
            com.google.android.gms.internal.zzcm r8 = new com.google.android.gms.internal.zzcm     // Catch:{ zzcx -> 0x0169 }
            r8.<init>(r0)     // Catch:{ zzcx -> 0x0169 }
            r0.zzaip = r8     // Catch:{ zzcx -> 0x0169 }
            r0.zzait = r11     // Catch:{ zzcx -> 0x0169 }
            return r0
        L_0x012f:
            r10 = move-exception
            zzb(r3)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r0.zza(r9, r1)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            java.lang.String r2 = "%s/%s.dex"
            java.lang.Object[] r3 = new java.lang.Object[r5]     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r3[r8] = r9     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            r3[r11] = r1     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            java.lang.String r8 = java.lang.String.format(r2, r3)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            zzl(r8)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
            throw r10     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0154, zzcv -> 0x014d, NullPointerException -> 0x0146 }
        L_0x0146:
            r8 = move-exception
            com.google.android.gms.internal.zzcx r9 = new com.google.android.gms.internal.zzcx     // Catch:{ zzcx -> 0x0169 }
            r9.<init>(r8)     // Catch:{ zzcx -> 0x0169 }
            throw r9     // Catch:{ zzcx -> 0x0169 }
        L_0x014d:
            r8 = move-exception
            com.google.android.gms.internal.zzcx r9 = new com.google.android.gms.internal.zzcx     // Catch:{ zzcx -> 0x0169 }
            r9.<init>(r8)     // Catch:{ zzcx -> 0x0169 }
            throw r9     // Catch:{ zzcx -> 0x0169 }
        L_0x0154:
            r8 = move-exception
            com.google.android.gms.internal.zzcx r9 = new com.google.android.gms.internal.zzcx     // Catch:{ zzcx -> 0x0169 }
            r9.<init>(r8)     // Catch:{ zzcx -> 0x0169 }
            throw r9     // Catch:{ zzcx -> 0x0169 }
        L_0x015b:
            r8 = move-exception
            com.google.android.gms.internal.zzcx r9 = new com.google.android.gms.internal.zzcx     // Catch:{ zzcx -> 0x0169 }
            r9.<init>(r8)     // Catch:{ zzcx -> 0x0169 }
            throw r9     // Catch:{ zzcx -> 0x0169 }
        L_0x0162:
            r8 = move-exception
            com.google.android.gms.internal.zzcx r9 = new com.google.android.gms.internal.zzcx     // Catch:{ zzcx -> 0x0169 }
            r9.<init>(r8)     // Catch:{ zzcx -> 0x0169 }
            throw r9     // Catch:{ zzcx -> 0x0169 }
        L_0x0169:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzda.zza(android.content.Context, java.lang.String, java.lang.String, boolean):com.google.android.gms.internal.zzda");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(10:20|21|22|23|24|25|26|27|28|30) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x0091 */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00a3 A[SYNTHETIC, Splitter:B:39:0x00a3] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a8 A[SYNTHETIC, Splitter:B:43:0x00a8] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00b2 A[SYNTHETIC, Splitter:B:52:0x00b2] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00b7 A[SYNTHETIC, Splitter:B:56:0x00b7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zza(java.io.File r8, java.lang.String r9) {
        /*
            r7 = this;
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "%s/%s.tmp"
            r2 = 2
            java.lang.Object[] r3 = new java.lang.Object[r2]
            r4 = 0
            r3[r4] = r8
            r5 = 1
            r3[r5] = r9
            java.lang.String r1 = java.lang.String.format(r1, r3)
            r0.<init>(r1)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x001b
            return
        L_0x001b:
            java.io.File r1 = new java.io.File
            java.lang.String r3 = "%s/%s.dex"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r4] = r8
            r2[r5] = r9
            java.lang.String r8 = java.lang.String.format(r3, r2)
            r1.<init>(r8)
            boolean r8 = r1.exists()
            if (r8 != 0) goto L_0x0033
            return
        L_0x0033:
            long r2 = r1.length()
            r5 = 0
            int r8 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r8 > 0) goto L_0x003e
            return
        L_0x003e:
            int r8 = (int) r2
            byte[] r8 = new byte[r8]
            r2 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00af, all -> 0x009f }
            r3.<init>(r1)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00af, all -> 0x009f }
            int r5 = r3.read(r8)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            if (r5 > 0) goto L_0x0054
            r3.close()     // Catch:{ IOException -> 0x0050 }
        L_0x0050:
            zzb(r1)
            return
        L_0x0054:
            com.google.android.gms.internal.zzba r5 = new com.google.android.gms.internal.zzba     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            r5.<init>()     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            java.lang.String r6 = android.os.Build.VERSION.SDK     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            byte[] r6 = r6.getBytes()     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            r5.zzgg = r6     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            byte[] r9 = r9.getBytes()     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            r5.zzgf = r9     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            com.google.android.gms.internal.zzcu r9 = r7.zzaij     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            byte[] r6 = r7.zzaik     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            java.lang.String r8 = r9.zzc(r6, r8)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            byte[] r8 = r8.getBytes()     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            r5.data = r8     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            byte[] r8 = com.google.android.gms.internal.zzbt.zzb(r8)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            r5.zzge = r8     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            r0.createNewFile()     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            r8.<init>(r0)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b0, all -> 0x009d }
            byte[] r9 = com.google.android.gms.internal.zzfhk.zzc(r5)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x009b, all -> 0x0098 }
            int r0 = r9.length     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x009b, all -> 0x0098 }
            r8.write(r9, r4, r0)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x009b, all -> 0x0098 }
            r8.close()     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x009b, all -> 0x0098 }
            r3.close()     // Catch:{ IOException -> 0x0091 }
        L_0x0091:
            r8.close()     // Catch:{ IOException -> 0x0094 }
        L_0x0094:
            zzb(r1)
            return
        L_0x0098:
            r9 = move-exception
            r2 = r8
            goto L_0x00a1
        L_0x009b:
            r2 = r8
            goto L_0x00b0
        L_0x009d:
            r9 = move-exception
            goto L_0x00a1
        L_0x009f:
            r9 = move-exception
            r3 = r2
        L_0x00a1:
            if (r3 == 0) goto L_0x00a6
            r3.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x00a6:
            if (r2 == 0) goto L_0x00ab
            r2.close()     // Catch:{ IOException -> 0x00ab }
        L_0x00ab:
            zzb(r1)
            throw r9
        L_0x00af:
            r3 = r2
        L_0x00b0:
            if (r3 == 0) goto L_0x00b5
            r3.close()     // Catch:{ IOException -> 0x00b5 }
        L_0x00b5:
            if (r2 == 0) goto L_0x00ba
            r2.close()     // Catch:{ IOException -> 0x00ba }
        L_0x00ba:
            zzb(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzda.zza(java.io.File, java.lang.String):void");
    }

    /* access modifiers changed from: private */
    public static boolean zza(int i, zzaw zzaw) {
        if (i >= 4) {
            return false;
        }
        if (zzaw == null) {
            return true;
        }
        if (((Boolean) zzbs.zzep().zzd(zzmq.zzbmg)).booleanValue() && (zzaw.zzcq == null || zzaw.zzcq.equals("0000000000000000000000000000000000000000000000000000000000000000"))) {
            return true;
        }
        if (((Boolean) zzbs.zzep().zzd(zzmq.zzbmh)).booleanValue()) {
            return zzaw.zzfb == null || zzaw.zzfb.zzfz == null || zzaw.zzfb.zzfz.longValue() == -2;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public final void zzao() {
        try {
            if (this.zzail == null && this.zzaig != null) {
                AdvertisingIdClient advertisingIdClient = new AdvertisingIdClient(this.zzaig);
                advertisingIdClient.start();
                this.zzail = advertisingIdClient;
            }
        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException unused) {
            this.zzail = null;
        }
    }

    @VisibleForTesting
    private final zzaw zzap() {
        try {
            return zzcaj.zzn(this.zzaif, this.zzaif.getPackageName(), Integer.toString(this.zzaif.getPackageManager().getPackageInfo(this.zzaif.getPackageName(), 0).versionCode));
        } catch (Throwable unused) {
            return null;
        }
    }

    private static void zzb(File file) {
        if (!file.exists()) {
            Log.d(TAG, String.format("File %s not found. No need for deletion", file.getAbsolutePath()));
            return;
        }
        file.delete();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:29|30|31|32|33|34|35|36) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:34:0x00b1 */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00c7 A[SYNTHETIC, Splitter:B:52:0x00c7] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00cc A[SYNTHETIC, Splitter:B:56:0x00cc] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00d3 A[SYNTHETIC, Splitter:B:64:0x00d3] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00d8 A[SYNTHETIC, Splitter:B:68:0x00d8] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean zzb(java.io.File r10, java.lang.String r11) {
        /*
            r9 = this;
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "%s/%s.tmp"
            r2 = 2
            java.lang.Object[] r3 = new java.lang.Object[r2]
            r4 = 0
            r3[r4] = r10
            r5 = 1
            r3[r5] = r11
            java.lang.String r1 = java.lang.String.format(r1, r3)
            r0.<init>(r1)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x001b
            return r4
        L_0x001b:
            java.io.File r1 = new java.io.File
            java.lang.String r3 = "%s/%s.dex"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r4] = r10
            r2[r5] = r11
            java.lang.String r10 = java.lang.String.format(r3, r2)
            r1.<init>(r10)
            boolean r10 = r1.exists()
            if (r10 == 0) goto L_0x0033
            return r4
        L_0x0033:
            r10 = 0
            long r2 = r0.length()     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d0, all -> 0x00c3 }
            r6 = 0
            int r8 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r8 > 0) goto L_0x0042
            zzb(r0)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d0, all -> 0x00c3 }
            return r4
        L_0x0042:
            int r2 = (int) r2     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d0, all -> 0x00c3 }
            byte[] r2 = new byte[r2]     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d0, all -> 0x00c3 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d0, all -> 0x00c3 }
            r3.<init>(r0)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d0, all -> 0x00c3 }
            int r6 = r3.read(r2)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            if (r6 > 0) goto L_0x005e
            java.lang.String r11 = com.google.android.gms.internal.zzda.TAG     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            java.lang.String r1 = "Cannot read the cache data."
            android.util.Log.d(r11, r1)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            zzb(r0)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            r3.close()     // Catch:{ IOException -> 0x005d }
        L_0x005d:
            return r4
        L_0x005e:
            com.google.android.gms.internal.zzba r6 = new com.google.android.gms.internal.zzba     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            r6.<init>()     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            com.google.android.gms.internal.zzfhk r2 = com.google.android.gms.internal.zzfhk.zza(r6, r2)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            com.google.android.gms.internal.zzba r2 = (com.google.android.gms.internal.zzba) r2     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            java.lang.String r6 = new java.lang.String     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            byte[] r7 = r2.zzgf     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            r6.<init>(r7)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            boolean r11 = r11.equals(r6)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            if (r11 == 0) goto L_0x00ba
            byte[] r11 = r2.zzge     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            byte[] r6 = r2.data     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            byte[] r6 = com.google.android.gms.internal.zzbt.zzb(r6)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            boolean r11 = java.util.Arrays.equals(r11, r6)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            if (r11 == 0) goto L_0x00ba
            byte[] r11 = r2.zzgg     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            java.lang.String r6 = android.os.Build.VERSION.SDK     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            byte[] r6 = r6.getBytes()     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            boolean r11 = java.util.Arrays.equals(r11, r6)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            if (r11 != 0) goto L_0x0093
            goto L_0x00ba
        L_0x0093:
            com.google.android.gms.internal.zzcu r11 = r9.zzaij     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            byte[] r0 = r9.zzaik     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            java.lang.String r6 = new java.lang.String     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            byte[] r2 = r2.data     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            r6.<init>(r2)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            byte[] r11 = r11.zzb(r0, r6)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            r1.createNewFile()     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            r0.<init>(r1)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            int r10 = r11.length     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b8, all -> 0x00b5 }
            r0.write(r11, r4, r10)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00b8, all -> 0x00b5 }
            r3.close()     // Catch:{ IOException -> 0x00b1 }
        L_0x00b1:
            r0.close()     // Catch:{ IOException -> 0x00b4 }
        L_0x00b4:
            return r5
        L_0x00b5:
            r11 = move-exception
            r10 = r0
            goto L_0x00c5
        L_0x00b8:
            r10 = r0
            goto L_0x00d1
        L_0x00ba:
            zzb(r0)     // Catch:{ zzcv | IOException | NoSuchAlgorithmException -> 0x00d1, all -> 0x00c1 }
            r3.close()     // Catch:{ IOException -> 0x00c0 }
        L_0x00c0:
            return r4
        L_0x00c1:
            r11 = move-exception
            goto L_0x00c5
        L_0x00c3:
            r11 = move-exception
            r3 = r10
        L_0x00c5:
            if (r3 == 0) goto L_0x00ca
            r3.close()     // Catch:{ IOException -> 0x00ca }
        L_0x00ca:
            if (r10 == 0) goto L_0x00cf
            r10.close()     // Catch:{ IOException -> 0x00cf }
        L_0x00cf:
            throw r11
        L_0x00d0:
            r3 = r10
        L_0x00d1:
            if (r3 == 0) goto L_0x00d6
            r3.close()     // Catch:{ IOException -> 0x00d6 }
        L_0x00d6:
            if (r10 == 0) goto L_0x00db
            r10.close()     // Catch:{ IOException -> 0x00db }
        L_0x00db:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzda.zzb(java.io.File, java.lang.String):boolean");
    }

    private static void zzl(String str) {
        zzb(new File(str));
    }

    public final Context getApplicationContext() {
        return this.zzaig;
    }

    public final Context getContext() {
        return this.zzaif;
    }

    public final boolean isInitialized() {
        return this.zzait;
    }

    public final Method zza(String str, String str2) {
        zzea zzea = this.zzais.get(new Pair(str, str2));
        if (zzea == null) {
            return null;
        }
        return zzea.zzay();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public final void zza(int i, boolean z) {
        if (this.zzair) {
            Future<?> submit = this.zzaih.submit(new zzdc(this, i, z));
            if (i == 0) {
                this.zzaio = submit;
            }
        }
    }

    public final boolean zza(String str, String str2, Class<?>... clsArr) {
        if (this.zzais.containsKey(new Pair(str, str2))) {
            return false;
        }
        this.zzais.put(new Pair(str, str2), new zzea(this, str, str2, clsArr));
        return true;
    }

    public final int zzaa() {
        return this.zzaip != null ? zzcm.zzaa() : ExploreByTouchHelper.INVALID_ID;
    }

    public final ExecutorService zzae() {
        return this.zzaih;
    }

    public final DexClassLoader zzaf() {
        return this.zzaii;
    }

    public final zzcu zzag() {
        return this.zzaij;
    }

    public final byte[] zzah() {
        return this.zzaik;
    }

    public final boolean zzai() {
        return this.zzaiq;
    }

    public final zzcm zzaj() {
        return this.zzaip;
    }

    public final boolean zzak() {
        return this.zzair;
    }

    public final boolean zzal() {
        return this.zzaiu;
    }

    public final zzaw zzam() {
        return this.zzain;
    }

    public final Future zzan() {
        return this.zzaio;
    }

    public final AdvertisingIdClient zzaq() {
        if (!this.zzahk) {
            return null;
        }
        if (this.zzail != null) {
            return this.zzail;
        }
        if (this.zzaim != null) {
            try {
                this.zzaim.get(2000, TimeUnit.MILLISECONDS);
                this.zzaim = null;
            } catch (InterruptedException | ExecutionException unused) {
            } catch (TimeoutException unused2) {
                this.zzaim.cancel(true);
            }
        }
        return this.zzail;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public final zzaw zzb(int i, boolean z) {
        if (i > 0 && z) {
            try {
                Thread.sleep((long) (i * 1000));
            } catch (InterruptedException unused) {
            }
        }
        return zzap();
    }
}
