package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.query.Query;

public final class zzbrc implements Parcelable.Creator<zzbrb> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        Query query = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                zzbek.zzb(parcel, readInt);
            } else {
                query = (Query) zzbek.zza(parcel, readInt, Query.CREATOR);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbrb(query);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbrb[i];
    }
}
