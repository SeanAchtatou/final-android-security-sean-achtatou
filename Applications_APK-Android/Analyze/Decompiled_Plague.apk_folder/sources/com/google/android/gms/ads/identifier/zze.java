package com.google.android.gms.ads.identifier;

import android.net.Uri;
import android.util.Log;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public final class zze {
    /* JADX INFO: additional move instructions added (1) to help type inference */
    public final void zzc(Map<String, String> map) {
        String str;
        String message;
        StringBuilder sb;
        String str2;
        Exception exc;
        HttpURLConnection httpURLConnection;
        Uri.Builder buildUpon = Uri.parse("https://pagead2.googlesyndication.com/pagead/gen_204?id=gmob-apps").buildUpon();
        for (String next : map.keySet()) {
            buildUpon.appendQueryParameter(next, map.get(next));
        }
        String uri = buildUpon.build().toString();
        try {
            httpURLConnection = (HttpURLConnection) new URL(uri).openConnection();
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode < 200 || responseCode >= 300) {
                StringBuilder sb2 = new StringBuilder(65 + String.valueOf(uri).length());
                sb2.append("Received non-success response code ");
                sb2.append(responseCode);
                sb2.append(" from pinging URL: ");
                sb2.append(uri);
                Log.w("HttpUrlPinger", sb2.toString());
            }
            httpURLConnection.disconnect();
        } catch (IndexOutOfBoundsException e) {
            str = "HttpUrlPinger";
            message = e.getMessage();
            sb = new StringBuilder(32 + String.valueOf(uri).length() + String.valueOf(message).length());
            str2 = "Error while parsing ping URL: ";
            exc = e;
            sb.append(str2);
            sb.append(uri);
            sb.append(". ");
            sb.append(message);
            Log.w(str, sb.toString(), exc);
        } catch (IOException | RuntimeException e2) {
            str = "HttpUrlPinger";
            message = e2.getMessage();
            sb = new StringBuilder(27 + String.valueOf(uri).length() + String.valueOf(message).length());
            str2 = "Error while pinging URL: ";
            exc = e2;
            sb.append(str2);
            sb.append(uri);
            sb.append(". ");
            sb.append(message);
            Log.w(str, sb.toString(), exc);
        } catch (Throwable th) {
            httpURLConnection.disconnect();
            throw th;
        }
    }
}
