package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzcl implements zzbo<TurnBasedMultiplayer.UpdateMatchResult, TurnBasedMatch> {
    zzcl() {
    }

    public final /* synthetic */ Object zzb(@Nullable Result result) {
        TurnBasedMatch match;
        TurnBasedMultiplayer.UpdateMatchResult updateMatchResult = (TurnBasedMultiplayer.UpdateMatchResult) result;
        if (updateMatchResult == null || (match = updateMatchResult.getMatch()) == null) {
            return null;
        }
        return (TurnBasedMatch) match.freeze();
    }
}
