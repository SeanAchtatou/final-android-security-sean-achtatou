package com.google.android.gms.internal;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;

final class zzbdk extends zzbdi {
    private /* synthetic */ zzbdj zzfhn;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbdk(zzbdj zzbdj) {
        super();
        this.zzfhn = zzbdj;
    }

    public final void zzo(Status status) {
        this.zzfhn.setResult((Result) status);
    }
}
