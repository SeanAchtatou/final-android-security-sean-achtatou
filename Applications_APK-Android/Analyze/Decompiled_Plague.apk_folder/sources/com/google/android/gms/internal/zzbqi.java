package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbqi extends zzbej {
    public static final Parcelable.Creator<zzbqi> CREATOR = new zzbqn();
    private boolean zzaqh;

    public zzbqi(boolean z) {
        this.zzaqh = z;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, this.zzaqh);
        zzbem.zzai(parcel, zze);
    }
}
