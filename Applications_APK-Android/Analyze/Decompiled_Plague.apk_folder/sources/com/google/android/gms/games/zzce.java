package com.google.android.gms.games;

import android.support.annotation.NonNull;
import com.google.android.gms.games.internal.zzo;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzce implements zzo<TurnBasedMultiplayer.LoadMatchesResult> {
    zzce() {
    }

    public final /* synthetic */ void release(@NonNull Object obj) {
        TurnBasedMultiplayer.LoadMatchesResult loadMatchesResult = (TurnBasedMultiplayer.LoadMatchesResult) obj;
        if (loadMatchesResult.getMatches() != null) {
            loadMatchesResult.getMatches().release();
        }
    }
}
