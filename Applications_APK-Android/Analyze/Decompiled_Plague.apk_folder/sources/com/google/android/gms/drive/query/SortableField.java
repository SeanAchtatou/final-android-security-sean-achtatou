package com.google.android.gms.drive.query;

import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.internal.zzbse;
import com.google.android.gms.internal.zzbsr;
import java.util.Date;

public class SortableField {
    public static final SortableMetadataField<Date> CREATED_DATE = zzbsr.zzgrj;
    public static final SortableMetadataField<Date> LAST_VIEWED_BY_ME = zzbsr.zzgrk;
    public static final SortableMetadataField<Date> MODIFIED_BY_ME_DATE = zzbsr.zzgrm;
    public static final SortableMetadataField<Date> MODIFIED_DATE = zzbsr.zzgrl;
    public static final SortableMetadataField<Long> QUOTA_USED = zzbse.zzgqu;
    public static final SortableMetadataField<Date> SHARED_WITH_ME_DATE = zzbsr.zzgrn;
    public static final SortableMetadataField<String> TITLE = zzbse.zzgqx;
    private static SortableMetadataField<Date> zzgsd = zzbsr.zzgro;
}
