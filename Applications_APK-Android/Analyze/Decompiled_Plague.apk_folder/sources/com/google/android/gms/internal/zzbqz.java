package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.TransferPreferences;

public final class zzbqz extends zzbej implements TransferPreferences {
    public static final Parcelable.Creator<zzbqz> CREATOR = new zzbra();
    private boolean zzghz;
    private int zzgia;
    private int zzgnn;

    zzbqz(int i, int i2, boolean z) {
        this.zzgnn = i;
        this.zzgia = i2;
        this.zzghz = z;
    }

    public final int getBatteryUsagePreference() {
        return this.zzgia;
    }

    public final int getNetworkPreference() {
        return this.zzgnn;
    }

    public final boolean isRoamingAllowed() {
        return this.zzghz;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 2, this.zzgnn);
        zzbem.zzc(parcel, 3, this.zzgia);
        zzbem.zza(parcel, 4, this.zzghz);
        zzbem.zzai(parcel, zze);
    }
}
