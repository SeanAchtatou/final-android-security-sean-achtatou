package com.google.android.gms.games.request;

import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameRef;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;
import com.tapjoy.TJAdUnitConstants;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public final class zzb extends zzc implements GameRequest {
    private final int zzhtn;

    public zzb(DataHolder dataHolder, int i, int i2) {
        super(dataHolder, i);
        this.zzhtn = i2;
    }

    public final int describeContents() {
        return 0;
    }

    public final boolean equals(Object obj) {
        return GameRequestEntity.zza(this, obj);
    }

    public final /* synthetic */ Object freeze() {
        return new GameRequestEntity(this);
    }

    public final long getCreationTimestamp() {
        return getLong("creation_timestamp");
    }

    public final byte[] getData() {
        return getByteArray(TJAdUnitConstants.String.DATA);
    }

    public final long getExpirationTimestamp() {
        return getLong("expiration_timestamp");
    }

    public final Game getGame() {
        return new GameRef(this.zzfnz, this.zzftd);
    }

    public final int getRecipientStatus(String str) {
        for (int i = this.zzftd; i < this.zzftd + this.zzhtn; i++) {
            int zzbz = this.zzfnz.zzbz(i);
            if (this.zzfnz.zzd("recipient_external_player_id", i, zzbz).equals(str)) {
                return this.zzfnz.zzc("recipient_status", i, zzbz);
            }
        }
        return -1;
    }

    public final List<Player> getRecipients() {
        ArrayList arrayList = new ArrayList(this.zzhtn);
        for (int i = 0; i < this.zzhtn; i++) {
            arrayList.add(new PlayerRef(this.zzfnz, this.zzftd + i, "recipient_"));
        }
        return arrayList;
    }

    public final String getRequestId() {
        return getString("external_request_id");
    }

    public final Player getSender() {
        return new PlayerRef(this.zzfnz, this.zzftd, "sender_");
    }

    public final int getStatus() {
        return getInteger("status");
    }

    public final int getType() {
        return getInteger("type");
    }

    public final int hashCode() {
        return GameRequestEntity.zza(this);
    }

    public final boolean isConsumed(String str) {
        return getRecipientStatus(str) == 1;
    }

    public final String toString() {
        return GameRequestEntity.zzc(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((GameRequestEntity) ((GameRequest) freeze())).writeToParcel(parcel, i);
    }
}
