package com.google.android.gms.internal;

import java.io.IOException;

public final class zzdri extends zzfee<zzdri, zza> implements zzffk {
    private static volatile zzffm<zzdri> zzbas;
    /* access modifiers changed from: private */
    public static final zzdri zzlsn;
    private int zzlsl;
    private int zzlsm;

    public static final class zza extends zzfef<zzdri, zza> implements zzffk {
        private zza() {
            super(zzdri.zzlsn);
        }

        /* synthetic */ zza(zzdrj zzdrj) {
            this();
        }
    }

    static {
        zzdri zzdri = new zzdri();
        zzlsn = zzdri;
        zzdri.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdri.zzpbs.zzbim();
    }

    private zzdri() {
    }

    public static zzdri zzbnq() {
        return zzlsn;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        boolean z = true;
        boolean z2 = false;
        switch (zzdrj.zzbaq[i - 1]) {
            case 1:
                return new zzdri();
            case 2:
                return zzlsn;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdri zzdri = (zzdri) obj2;
                this.zzlsl = zzfen.zza(this.zzlsl != 0, this.zzlsl, zzdri.zzlsl != 0, zzdri.zzlsl);
                boolean z3 = this.zzlsm != 0;
                int i2 = this.zzlsm;
                if (zzdri.zzlsm == 0) {
                    z = false;
                }
                this.zzlsm = zzfen.zza(z3, i2, z, zzdri.zzlsm);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                if (((zzfea) obj2) != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzlsl = zzfdq.zzcuc();
                                } else if (zzcts == 16) {
                                    this.zzlsm = zzfdq.zzcub();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdri.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlsn);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlsn;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlsl != zzdrc.UNKNOWN_HASH.zzhn()) {
            zzfdv.zzaa(1, this.zzlsl);
        }
        if (this.zzlsm != 0) {
            zzfdv.zzab(2, this.zzlsm);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzdrc zzbno() {
        zzdrc zzfp = zzdrc.zzfp(this.zzlsl);
        return zzfp == null ? zzdrc.UNRECOGNIZED : zzfp;
    }

    public final int zzbnp() {
        return this.zzlsm;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlsl != zzdrc.UNKNOWN_HASH.zzhn()) {
            i2 = 0 + zzfdv.zzag(1, this.zzlsl);
        }
        if (this.zzlsm != 0) {
            i2 += zzfdv.zzae(2, this.zzlsm);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
