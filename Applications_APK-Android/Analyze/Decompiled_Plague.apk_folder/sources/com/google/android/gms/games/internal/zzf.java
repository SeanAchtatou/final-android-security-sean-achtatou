package com.google.android.gms.games.internal;

import com.google.android.gms.common.internal.zzal;
import com.google.android.gms.internal.zzbdv;

public final class zzf {
    private static final zzal zzhpf = new zzal("Games");
    private static final zzbdv<Boolean> zzhpg = zzbdv.zze("games.play_games_dogfood", false);

    public static void zzb(String str, String str2, Throwable th) {
        zzhpf.zzb(str, str2, th);
    }

    public static void zzc(String str, String str2, Throwable th) {
        zzhpf.zzc(str, str2, th);
    }

    public static void zzd(String str, String str2, Throwable th) {
        zzhpf.zzd(str, str2, th);
    }

    public static void zzv(String str, String str2) {
        zzhpf.zzv(str, str2);
    }

    public static void zzw(String str, String str2) {
        zzhpf.zzw(str, str2);
    }
}
