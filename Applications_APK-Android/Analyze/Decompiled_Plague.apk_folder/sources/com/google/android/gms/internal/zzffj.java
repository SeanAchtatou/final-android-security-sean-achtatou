package com.google.android.gms.internal;

import java.io.IOException;

public interface zzffj extends zzffk, Cloneable {
    zzffj zzb(zzfdq zzfdq, zzfea zzfea) throws IOException;

    zzffj zzc(zzffi zzffi);

    zzffi zzcvl();

    zzffi zzcvm();
}
