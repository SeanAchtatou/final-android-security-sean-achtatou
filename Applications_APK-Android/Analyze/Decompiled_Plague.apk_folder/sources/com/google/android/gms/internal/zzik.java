package com.google.android.gms.internal;

import java.io.IOException;

public final class zzik extends zzfhe<zzik> {
    public String zzbbl = null;
    public zzim zzbbm = null;

    public zzik() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 10) {
                this.zzbbl = zzfhb.readString();
            } else if (zzcts == 34) {
                if (this.zzbbm == null) {
                    this.zzbbm = new zzim();
                }
                zzfhb.zza(this.zzbbm);
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzbbl != null) {
            zzfhc.zzn(1, this.zzbbl);
        }
        if (this.zzbbm != null) {
            zzfhc.zza(4, this.zzbbm);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzbbl != null) {
            zzo += zzfhc.zzo(1, this.zzbbl);
        }
        return this.zzbbm != null ? zzo + zzfhc.zzb(4, this.zzbbm) : zzo;
    }
}
