package com.google.android.gms.games.snapshot;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzn;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.games.internal.zzf;
import com.google.android.gms.internal.zzbem;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public final class zza extends zzc implements SnapshotContents {
    public static final Parcelable.Creator<zza> CREATOR = new zzb();
    private static final Object zzhwz = new Object();
    private com.google.android.gms.drive.zzc zzglf;

    public zza(com.google.android.gms.drive.zzc zzc) {
        this.zzglf = zzc;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    private final boolean zza(int i, byte[] bArr, int i2, int i3, boolean z) {
        zzbq.zza(!isClosed(), (Object) "Must provide a previously opened SnapshotContents");
        synchronized (zzhwz) {
            FileOutputStream fileOutputStream = new FileOutputStream(this.zzglf.getParcelFileDescriptor().getFileDescriptor());
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            try {
                FileChannel channel = fileOutputStream.getChannel();
                channel.position((long) i);
                bufferedOutputStream.write(bArr, i2, i3);
                if (z) {
                    channel.truncate((long) bArr.length);
                }
                bufferedOutputStream.flush();
            } catch (IOException e) {
                zzf.zzb("SnapshotContentsEntity", "Failed to write snapshot data", e);
                return false;
            }
        }
        return true;
    }

    public final void close() {
        this.zzglf = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final ParcelFileDescriptor getParcelFileDescriptor() {
        zzbq.zza(!isClosed(), (Object) "Cannot mutate closed contents!");
        return this.zzglf.getParcelFileDescriptor();
    }

    public final boolean isClosed() {
        return this.zzglf == null;
    }

    public final boolean modifyBytes(int i, byte[] bArr, int i2, int i3) {
        return zza(i, bArr, i2, bArr.length, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final byte[] readFully() throws IOException {
        byte[] zza;
        zzbq.zza(!isClosed(), (Object) "Must provide a previously opened Snapshot");
        synchronized (zzhwz) {
            FileInputStream fileInputStream = new FileInputStream(this.zzglf.getParcelFileDescriptor().getFileDescriptor());
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            try {
                fileInputStream.getChannel().position(0L);
                zza = zzn.zza(bufferedInputStream, false);
                fileInputStream.getChannel().position(0L);
            } catch (IOException e) {
                zzf.zzc("SnapshotContentsEntity", "Failed to read snapshot data", e);
                throw e;
            }
        }
        return zza;
    }

    public final boolean writeBytes(byte[] bArr) {
        return zza(0, bArr, 0, bArr.length, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.zzc, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, (Parcelable) this.zzglf, i, false);
        zzbem.zzai(parcel, zze);
    }

    public final com.google.android.gms.drive.zzc zzanp() {
        return this.zzglf;
    }
}
