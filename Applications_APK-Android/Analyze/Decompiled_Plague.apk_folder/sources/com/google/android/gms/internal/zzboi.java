package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzboi extends zzbld {
    private /* synthetic */ zzbog zzgng;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzboi(zzbog zzbog, GoogleApiClient googleApiClient) {
        super(googleApiClient);
        this.zzgng = zzbog;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbpo(this.zzgng.zzgfy), new zzboo(this));
    }
}
