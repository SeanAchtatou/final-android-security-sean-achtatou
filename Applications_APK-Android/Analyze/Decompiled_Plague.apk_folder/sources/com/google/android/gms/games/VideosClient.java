package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.common.api.internal.zzcp;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.internal.api.zzp;
import com.google.android.gms.games.internal.zzg;
import com.google.android.gms.games.video.CaptureState;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.games.video.Videos;
import com.google.android.gms.tasks.Task;

public class VideosClient extends zzp {
    public static final int CAPTURE_OVERLAY_STATE_CAPTURE_STARTED = 2;
    public static final int CAPTURE_OVERLAY_STATE_CAPTURE_STOPPED = 3;
    public static final int CAPTURE_OVERLAY_STATE_DISMISSED = 4;
    public static final int CAPTURE_OVERLAY_STATE_SHOWN = 1;
    private static final zzbo<Videos.CaptureAvailableResult, Boolean> zzhlv = new zzcz();
    private static final zzbo<Videos.CaptureStateResult, CaptureState> zzhlw = new zzda();
    private static final zzbo<Videos.CaptureCapabilitiesResult, VideoCapabilities> zzhlx = new zzdb();

    public interface OnCaptureOverlayStateListener extends Videos.CaptureOverlayStateListener {
        void onCaptureOverlayStateChanged(int i);
    }

    VideosClient(@NonNull Activity activity, @NonNull Games.GamesOptions gamesOptions) {
        super(activity, gamesOptions);
    }

    VideosClient(@NonNull Context context, @NonNull Games.GamesOptions gamesOptions) {
        super(context, gamesOptions);
    }

    public Task<VideoCapabilities> getCaptureCapabilities() {
        return zzg.zza(Games.Videos.getCaptureCapabilities(zzagb()), zzhlx);
    }

    public Task<Intent> getCaptureOverlayIntent() {
        return zza(new zzcv(this));
    }

    public Task<CaptureState> getCaptureState() {
        return zzg.zza(Games.Videos.getCaptureState(zzagb()), zzhlw);
    }

    public Task<Boolean> isCaptureAvailable(int i) {
        return zzg.zza(Games.Videos.isCaptureAvailable(zzagb(), i), zzhlv);
    }

    public Task<Boolean> isCaptureSupported() {
        return zza(new zzcw(this));
    }

    public Task<Void> registerOnCaptureOverlayStateChangedListener(@NonNull OnCaptureOverlayStateListener onCaptureOverlayStateListener) {
        zzcl zza = zza(onCaptureOverlayStateListener, OnCaptureOverlayStateListener.class.getSimpleName());
        return zza(new zzcx(this, zza, zza), new zzcy(this, zza.zzajc()));
    }

    public Task<Boolean> unregisterOnCaptureOverlayStateChangedListener(@NonNull OnCaptureOverlayStateListener onCaptureOverlayStateListener) {
        return zza(zzcp.zzb(onCaptureOverlayStateListener, OnCaptureOverlayStateListener.class.getSimpleName()));
    }
}
