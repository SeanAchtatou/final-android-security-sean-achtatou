package com.google.android.gms.games.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.Status;

final class zzh implements zzp {
    zzh() {
    }

    public final boolean zzag(@NonNull Status status) {
        return status.isSuccess();
    }
}
