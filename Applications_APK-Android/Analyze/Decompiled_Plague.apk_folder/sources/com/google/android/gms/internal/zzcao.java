package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzcao extends zzbej {
    public static final Parcelable.Creator<zzcao> CREATOR = new zzcap();
    private int versionCode;
    private zzaw zzhyw = null;
    private byte[] zzhyx;

    zzcao(int i, byte[] bArr) {
        this.versionCode = i;
        this.zzhyx = bArr;
        zzamx();
    }

    private final void zzamx() {
        if (this.zzhyw == null && this.zzhyx != null) {
            return;
        }
        if (this.zzhyw != null && this.zzhyx == null) {
            return;
        }
        if (this.zzhyw != null && this.zzhyx != null) {
            throw new IllegalStateException("Invalid internal representation - full");
        } else if (this.zzhyw == null && this.zzhyx == null) {
            throw new IllegalStateException("Invalid internal representation - empty");
        } else {
            throw new IllegalStateException("Impossible");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
     arg types: [android.os.Parcel, int, byte[], int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 1, this.versionCode);
        zzbem.zza(parcel, 2, this.zzhyx != null ? this.zzhyx : zzfhk.zzc(this.zzhyw), false);
        zzbem.zzai(parcel, zze);
    }

    public final zzaw zzauf() {
        if (!(this.zzhyw != null)) {
            try {
                this.zzhyw = (zzaw) zzfhk.zza(new zzaw(), this.zzhyx);
                this.zzhyx = null;
            } catch (zzfhj e) {
                throw new IllegalStateException(e);
            }
        }
        zzamx();
        return this.zzhyw;
    }
}
