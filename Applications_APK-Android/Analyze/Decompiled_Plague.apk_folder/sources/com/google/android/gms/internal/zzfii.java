package com.google.android.gms.internal;

import android.support.v4.view.MotionEventCompat;
import com.millennialmedia.internal.AdPlacementReporter;
import com.miniclip.input.MCInput;
import java.io.IOException;
import java.util.Arrays;

public final class zzfii extends zzfhe<zzfii> implements Cloneable {
    private String tag = "";
    private int zzakb = 0;
    private boolean zzmnq = false;
    private zzfik zzoan = null;
    public long zzpkg = 0;
    public long zzpkh = 0;
    private long zzpki = 0;
    private int zzpkj = 0;
    private zzfij[] zzpkk = zzfij.zzcxz();
    private byte[] zzpkl = zzfhn.zzphr;
    private zzfig zzpkm = null;
    public byte[] zzpkn = zzfhn.zzphr;
    private String zzpko = "";
    private String zzpkp = "";
    private zzfif zzpkq = null;
    private String zzpkr = "";
    public long zzpks = 180000;
    private zzfih zzpkt = null;
    public byte[] zzpku = zzfhn.zzphr;
    private String zzpkv = "";
    private int zzpkw = 0;
    private int[] zzpkx = zzfhn.zzphl;
    private long zzpky = 0;

    public zzfii() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzar */
    public final zzfii zza(zzfhb zzfhb) throws IOException {
        zzfhk zzfhk;
        while (true) {
            int zzcts = zzfhb.zzcts();
            switch (zzcts) {
                case 0:
                    return this;
                case 8:
                    this.zzpkg = zzfhb.zzctu();
                    continue;
                case 18:
                    this.tag = zzfhb.readString();
                    continue;
                case MotionEventCompat.AXIS_SCROLL:
                    int zzb = zzfhn.zzb(zzfhb, 26);
                    int length = this.zzpkk == null ? 0 : this.zzpkk.length;
                    zzfij[] zzfijArr = new zzfij[(zzb + length)];
                    if (length != 0) {
                        System.arraycopy(this.zzpkk, 0, zzfijArr, 0, length);
                    }
                    while (length < zzfijArr.length - 1) {
                        zzfijArr[length] = new zzfij();
                        zzfhb.zza(zzfijArr[length]);
                        zzfhb.zzcts();
                        length++;
                    }
                    zzfijArr[length] = new zzfij();
                    zzfhb.zza(zzfijArr[length]);
                    this.zzpkk = zzfijArr;
                    continue;
                case MotionEventCompat.AXIS_GENERIC_3:
                    this.zzpkl = zzfhb.readBytes();
                    continue;
                case 50:
                    this.zzpkn = zzfhb.readBytes();
                    continue;
                case 58:
                    if (this.zzpkq == null) {
                        this.zzpkq = new zzfif();
                    }
                    zzfhk = this.zzpkq;
                    break;
                case 66:
                    this.zzpko = zzfhb.readString();
                    continue;
                case 74:
                    if (this.zzpkm == null) {
                        this.zzpkm = new zzfig();
                    }
                    zzfhk = this.zzpkm;
                    break;
                case 80:
                    this.zzmnq = zzfhb.zzcty();
                    continue;
                case 88:
                    this.zzpkj = zzfhb.zzctv();
                    continue;
                case MCInput.KEYCODE_BUTTON_A /*96*/:
                    this.zzakb = zzfhb.zzctv();
                    continue;
                case MCInput.KEYCODE_BUTTON_THUMBL /*106*/:
                    this.zzpkp = zzfhb.readString();
                    continue;
                case AdPlacementReporter.PLAYLIST_REPLACED_IN_CACHE /*114*/:
                    this.zzpkr = zzfhb.readString();
                    continue;
                case 120:
                    this.zzpks = zzfhb.zzcug();
                    continue;
                case 130:
                    if (this.zzpkt == null) {
                        this.zzpkt = new zzfih();
                    }
                    zzfhk = this.zzpkt;
                    break;
                case 136:
                    this.zzpkh = zzfhb.zzctu();
                    continue;
                case 146:
                    this.zzpku = zzfhb.readBytes();
                    continue;
                case 152:
                    try {
                        int zzctv = zzfhb.zzctv();
                        switch (zzctv) {
                            default:
                                StringBuilder sb = new StringBuilder(45);
                                sb.append(zzctv);
                                sb.append(" is not a valid enum InternalEvent");
                                throw new IllegalArgumentException(sb.toString());
                            case 0:
                            case 1:
                            case 2:
                                this.zzpkw = zzctv;
                                continue;
                                continue;
                        }
                    } catch (IllegalArgumentException unused) {
                        zzfhb.zzlv(zzfhb.getPosition());
                        zza(zzfhb, zzcts);
                    }
                case 160:
                    int zzb2 = zzfhn.zzb(zzfhb, 160);
                    int length2 = this.zzpkx == null ? 0 : this.zzpkx.length;
                    int[] iArr = new int[(zzb2 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.zzpkx, 0, iArr, 0, length2);
                    }
                    while (length2 < iArr.length - 1) {
                        iArr[length2] = zzfhb.zzctv();
                        zzfhb.zzcts();
                        length2++;
                    }
                    iArr[length2] = zzfhb.zzctv();
                    this.zzpkx = iArr;
                    continue;
                case 162:
                    int zzki = zzfhb.zzki(zzfhb.zzcuh());
                    int position = zzfhb.getPosition();
                    int i = 0;
                    while (zzfhb.zzcuj() > 0) {
                        zzfhb.zzctv();
                        i++;
                    }
                    zzfhb.zzlv(position);
                    int length3 = this.zzpkx == null ? 0 : this.zzpkx.length;
                    int[] iArr2 = new int[(i + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.zzpkx, 0, iArr2, 0, length3);
                    }
                    while (length3 < iArr2.length) {
                        iArr2[length3] = zzfhb.zzctv();
                        length3++;
                    }
                    this.zzpkx = iArr2;
                    zzfhb.zzkj(zzki);
                    continue;
                case 168:
                    this.zzpki = zzfhb.zzctu();
                    continue;
                case 176:
                    this.zzpky = zzfhb.zzctu();
                    continue;
                case 186:
                    if (this.zzoan == null) {
                        this.zzoan = new zzfik();
                    }
                    zzfhk = this.zzoan;
                    break;
                case 194:
                    this.zzpkv = zzfhb.readString();
                    continue;
                default:
                    if (!super.zza(zzfhb, zzcts)) {
                        return this;
                    }
                    continue;
            }
            zzfhb.zza(zzfhk);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: zzcxy */
    public final zzfii clone() {
        try {
            zzfii zzfii = (zzfii) super.clone();
            if (this.zzpkk != null && this.zzpkk.length > 0) {
                zzfii.zzpkk = new zzfij[this.zzpkk.length];
                for (int i = 0; i < this.zzpkk.length; i++) {
                    if (this.zzpkk[i] != null) {
                        zzfii.zzpkk[i] = (zzfij) this.zzpkk[i].clone();
                    }
                }
            }
            if (this.zzpkm != null) {
                zzfii.zzpkm = (zzfig) this.zzpkm.clone();
            }
            if (this.zzpkq != null) {
                zzfii.zzpkq = (zzfif) this.zzpkq.clone();
            }
            if (this.zzpkt != null) {
                zzfii.zzpkt = (zzfih) this.zzpkt.clone();
            }
            if (this.zzpkx != null && this.zzpkx.length > 0) {
                zzfii.zzpkx = (int[]) this.zzpkx.clone();
            }
            if (this.zzoan != null) {
                zzfii.zzoan = (zzfik) this.zzoan.clone();
            }
            return zzfii;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfii)) {
            return false;
        }
        zzfii zzfii = (zzfii) obj;
        if (this.zzpkg != zzfii.zzpkg || this.zzpkh != zzfii.zzpkh || this.zzpki != zzfii.zzpki) {
            return false;
        }
        if (this.tag == null) {
            if (zzfii.tag != null) {
                return false;
            }
        } else if (!this.tag.equals(zzfii.tag)) {
            return false;
        }
        if (this.zzpkj != zzfii.zzpkj || this.zzakb != zzfii.zzakb || this.zzmnq != zzfii.zzmnq || !zzfhi.equals(this.zzpkk, zzfii.zzpkk) || !Arrays.equals(this.zzpkl, zzfii.zzpkl)) {
            return false;
        }
        if (this.zzpkm == null) {
            if (zzfii.zzpkm != null) {
                return false;
            }
        } else if (!this.zzpkm.equals(zzfii.zzpkm)) {
            return false;
        }
        if (!Arrays.equals(this.zzpkn, zzfii.zzpkn)) {
            return false;
        }
        if (this.zzpko == null) {
            if (zzfii.zzpko != null) {
                return false;
            }
        } else if (!this.zzpko.equals(zzfii.zzpko)) {
            return false;
        }
        if (this.zzpkp == null) {
            if (zzfii.zzpkp != null) {
                return false;
            }
        } else if (!this.zzpkp.equals(zzfii.zzpkp)) {
            return false;
        }
        if (this.zzpkq == null) {
            if (zzfii.zzpkq != null) {
                return false;
            }
        } else if (!this.zzpkq.equals(zzfii.zzpkq)) {
            return false;
        }
        if (this.zzpkr == null) {
            if (zzfii.zzpkr != null) {
                return false;
            }
        } else if (!this.zzpkr.equals(zzfii.zzpkr)) {
            return false;
        }
        if (this.zzpks != zzfii.zzpks) {
            return false;
        }
        if (this.zzpkt == null) {
            if (zzfii.zzpkt != null) {
                return false;
            }
        } else if (!this.zzpkt.equals(zzfii.zzpkt)) {
            return false;
        }
        if (!Arrays.equals(this.zzpku, zzfii.zzpku)) {
            return false;
        }
        if (this.zzpkv == null) {
            if (zzfii.zzpkv != null) {
                return false;
            }
        } else if (!this.zzpkv.equals(zzfii.zzpkv)) {
            return false;
        }
        if (this.zzpkw != zzfii.zzpkw || !zzfhi.equals(this.zzpkx, zzfii.zzpkx) || this.zzpky != zzfii.zzpky) {
            return false;
        }
        if (this.zzoan == null) {
            if (zzfii.zzoan != null) {
                return false;
            }
        } else if (!this.zzoan.equals(zzfii.zzoan)) {
            return false;
        }
        return (this.zzpgy == null || this.zzpgy.isEmpty()) ? zzfii.zzpgy == null || zzfii.zzpgy.isEmpty() : this.zzpgy.equals(zzfii.zzpgy);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((((527 + getClass().getName().hashCode()) * 31) + ((int) (this.zzpkg ^ (this.zzpkg >>> 32)))) * 31) + ((int) (this.zzpkh ^ (this.zzpkh >>> 32)))) * 31) + ((int) (this.zzpki ^ (this.zzpki >>> 32)))) * 31) + (this.tag == null ? 0 : this.tag.hashCode())) * 31) + this.zzpkj) * 31) + this.zzakb) * 31) + (this.zzmnq ? 1231 : 1237)) * 31) + zzfhi.hashCode(this.zzpkk)) * 31) + Arrays.hashCode(this.zzpkl);
        zzfig zzfig = this.zzpkm;
        int hashCode2 = (((((((hashCode * 31) + (zzfig == null ? 0 : zzfig.hashCode())) * 31) + Arrays.hashCode(this.zzpkn)) * 31) + (this.zzpko == null ? 0 : this.zzpko.hashCode())) * 31) + (this.zzpkp == null ? 0 : this.zzpkp.hashCode());
        zzfif zzfif = this.zzpkq;
        int hashCode3 = (((((hashCode2 * 31) + (zzfif == null ? 0 : zzfif.hashCode())) * 31) + (this.zzpkr == null ? 0 : this.zzpkr.hashCode())) * 31) + ((int) (this.zzpks ^ (this.zzpks >>> 32)));
        zzfih zzfih = this.zzpkt;
        int hashCode4 = (((((((((((hashCode3 * 31) + (zzfih == null ? 0 : zzfih.hashCode())) * 31) + Arrays.hashCode(this.zzpku)) * 31) + (this.zzpkv == null ? 0 : this.zzpkv.hashCode())) * 31) + this.zzpkw) * 31) + zzfhi.hashCode(this.zzpkx)) * 31) + ((int) (this.zzpky ^ (this.zzpky >>> 32)));
        zzfik zzfik = this.zzoan;
        int hashCode5 = ((hashCode4 * 31) + (zzfik == null ? 0 : zzfik.hashCode())) * 31;
        if (this.zzpgy != null && !this.zzpgy.isEmpty()) {
            i = this.zzpgy.hashCode();
        }
        return hashCode5 + i;
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzpkg != 0) {
            zzfhc.zzf(1, this.zzpkg);
        }
        if (this.tag != null && !this.tag.equals("")) {
            zzfhc.zzn(2, this.tag);
        }
        if (this.zzpkk != null && this.zzpkk.length > 0) {
            for (zzfij zzfij : this.zzpkk) {
                if (zzfij != null) {
                    zzfhc.zza(3, zzfij);
                }
            }
        }
        if (!Arrays.equals(this.zzpkl, zzfhn.zzphr)) {
            zzfhc.zzc(4, this.zzpkl);
        }
        if (!Arrays.equals(this.zzpkn, zzfhn.zzphr)) {
            zzfhc.zzc(6, this.zzpkn);
        }
        if (this.zzpkq != null) {
            zzfhc.zza(7, this.zzpkq);
        }
        if (this.zzpko != null && !this.zzpko.equals("")) {
            zzfhc.zzn(8, this.zzpko);
        }
        if (this.zzpkm != null) {
            zzfhc.zza(9, this.zzpkm);
        }
        if (this.zzmnq) {
            zzfhc.zzl(10, this.zzmnq);
        }
        if (this.zzpkj != 0) {
            zzfhc.zzaa(11, this.zzpkj);
        }
        if (this.zzakb != 0) {
            zzfhc.zzaa(12, this.zzakb);
        }
        if (this.zzpkp != null && !this.zzpkp.equals("")) {
            zzfhc.zzn(13, this.zzpkp);
        }
        if (this.zzpkr != null && !this.zzpkr.equals("")) {
            zzfhc.zzn(14, this.zzpkr);
        }
        if (this.zzpks != 180000) {
            zzfhc.zzg(15, this.zzpks);
        }
        if (this.zzpkt != null) {
            zzfhc.zza(16, this.zzpkt);
        }
        if (this.zzpkh != 0) {
            zzfhc.zzf(17, this.zzpkh);
        }
        if (!Arrays.equals(this.zzpku, zzfhn.zzphr)) {
            zzfhc.zzc(18, this.zzpku);
        }
        if (this.zzpkw != 0) {
            zzfhc.zzaa(19, this.zzpkw);
        }
        if (this.zzpkx != null && this.zzpkx.length > 0) {
            for (int zzaa : this.zzpkx) {
                zzfhc.zzaa(20, zzaa);
            }
        }
        if (this.zzpki != 0) {
            zzfhc.zzf(21, this.zzpki);
        }
        if (this.zzpky != 0) {
            zzfhc.zzf(22, this.zzpky);
        }
        if (this.zzoan != null) {
            zzfhc.zza(23, this.zzoan);
        }
        if (this.zzpkv != null && !this.zzpkv.equals("")) {
            zzfhc.zzn(24, this.zzpkv);
        }
        super.zza(zzfhc);
    }

    public final /* synthetic */ zzfhe zzcxe() throws CloneNotSupportedException {
        return (zzfii) clone();
    }

    public final /* synthetic */ zzfhk zzcxf() throws CloneNotSupportedException {
        return (zzfii) clone();
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzpkg != 0) {
            zzo += zzfhc.zzc(1, this.zzpkg);
        }
        if (this.tag != null && !this.tag.equals("")) {
            zzo += zzfhc.zzo(2, this.tag);
        }
        if (this.zzpkk != null && this.zzpkk.length > 0) {
            int i = zzo;
            for (zzfij zzfij : this.zzpkk) {
                if (zzfij != null) {
                    i += zzfhc.zzb(3, zzfij);
                }
            }
            zzo = i;
        }
        if (!Arrays.equals(this.zzpkl, zzfhn.zzphr)) {
            zzo += zzfhc.zzd(4, this.zzpkl);
        }
        if (!Arrays.equals(this.zzpkn, zzfhn.zzphr)) {
            zzo += zzfhc.zzd(6, this.zzpkn);
        }
        if (this.zzpkq != null) {
            zzo += zzfhc.zzb(7, this.zzpkq);
        }
        if (this.zzpko != null && !this.zzpko.equals("")) {
            zzo += zzfhc.zzo(8, this.zzpko);
        }
        if (this.zzpkm != null) {
            zzo += zzfhc.zzb(9, this.zzpkm);
        }
        if (this.zzmnq) {
            zzo += zzfhc.zzkw(10) + 1;
        }
        if (this.zzpkj != 0) {
            zzo += zzfhc.zzad(11, this.zzpkj);
        }
        if (this.zzakb != 0) {
            zzo += zzfhc.zzad(12, this.zzakb);
        }
        if (this.zzpkp != null && !this.zzpkp.equals("")) {
            zzo += zzfhc.zzo(13, this.zzpkp);
        }
        if (this.zzpkr != null && !this.zzpkr.equals("")) {
            zzo += zzfhc.zzo(14, this.zzpkr);
        }
        if (this.zzpks != 180000) {
            zzo += zzfhc.zzh(15, this.zzpks);
        }
        if (this.zzpkt != null) {
            zzo += zzfhc.zzb(16, this.zzpkt);
        }
        if (this.zzpkh != 0) {
            zzo += zzfhc.zzc(17, this.zzpkh);
        }
        if (!Arrays.equals(this.zzpku, zzfhn.zzphr)) {
            zzo += zzfhc.zzd(18, this.zzpku);
        }
        if (this.zzpkw != 0) {
            zzo += zzfhc.zzad(19, this.zzpkw);
        }
        if (this.zzpkx != null && this.zzpkx.length > 0) {
            int i2 = 0;
            for (int zzkx : this.zzpkx) {
                i2 += zzfhc.zzkx(zzkx);
            }
            zzo = zzo + i2 + (2 * this.zzpkx.length);
        }
        if (this.zzpki != 0) {
            zzo += zzfhc.zzc(21, this.zzpki);
        }
        if (this.zzpky != 0) {
            zzo += zzfhc.zzc(22, this.zzpky);
        }
        if (this.zzoan != null) {
            zzo += zzfhc.zzb(23, this.zzoan);
        }
        return (this.zzpkv == null || this.zzpkv.equals("")) ? zzo : zzo + zzfhc.zzo(24, this.zzpkv);
    }
}
