package com.google.android.gms.drive.events;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import com.google.android.gms.common.internal.zzal;
import com.google.android.gms.common.util.zzx;
import com.google.android.gms.internal.zzbpk;
import com.google.android.gms.internal.zzbqa;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class DriveEventService extends Service implements ChangeListener, CompletionListener, zzd, zzi {
    public static final String ACTION_HANDLE_EVENT = "com.google.android.gms.drive.events.HANDLE_EVENT";
    /* access modifiers changed from: private */
    public static final zzal zzggp = new zzal("DriveEventService", "");
    private final String mName;
    /* access modifiers changed from: private */
    public CountDownLatch zzgiu;
    zza zzgiv;
    boolean zzgiw;
    private int zzgix;

    final class zza extends Handler {
        zza() {
        }

        /* access modifiers changed from: private */
        public final Message zzaoh() {
            return obtainMessage(2);
        }

        /* access modifiers changed from: private */
        public final Message zzb(zzbqa zzbqa) {
            return obtainMessage(1, zzbqa);
        }

        public final void handleMessage(Message message) {
            DriveEventService.zzggp.zzb("DriveEventService", "handleMessage message type: %s", Integer.valueOf(message.what));
            switch (message.what) {
                case 1:
                    DriveEventService.this.zza((zzbqa) message.obj);
                    return;
                case 2:
                    getLooper().quit();
                    return;
                default:
                    DriveEventService.zzggp.zzc("DriveEventService", "Unexpected message type: %s", Integer.valueOf(message.what));
                    return;
            }
        }
    }

    final class zzb extends zzbpk {
        zzb() {
        }

        public final void zzc(zzbqa zzbqa) throws RemoteException {
            synchronized (DriveEventService.this) {
                DriveEventService.zzggp.zzb("DriveEventService", "onEvent: %s", zzbqa);
                DriveEventService.this.zzaof();
                if (DriveEventService.this.zzgiv != null) {
                    DriveEventService.this.zzgiv.sendMessage(DriveEventService.this.zzgiv.zzb(zzbqa));
                } else {
                    DriveEventService.zzggp.zzw("DriveEventService", "Receiving event before initialize is completed.");
                }
            }
        }
    }

    protected DriveEventService() {
        this(DriveEventService.class.getSimpleName());
    }

    protected DriveEventService(String str) {
        this.zzgiw = false;
        this.zzgix = -1;
        this.mName = str;
    }

    /* access modifiers changed from: private */
    public final void zza(zzbqa zzbqa) {
        DriveEvent zzaou = zzbqa.zzaou();
        zzggp.zzb("DriveEventService", "handleEventMessage: %s", zzaou);
        try {
            int type = zzaou.getType();
            if (type == 4) {
                zza((zzb) zzaou);
            } else if (type != 7) {
                switch (type) {
                    case 1:
                        onChange((ChangeEvent) zzaou);
                        return;
                    case 2:
                        onCompletion((CompletionEvent) zzaou);
                        return;
                    default:
                        zzggp.zzc("DriveEventService", "Unhandled event: %s", zzaou);
                        return;
                }
            } else {
                zzggp.zzc("DriveEventService", "Unhandled transfer state event in %s: %s", this.mName, (zzr) zzaou);
            }
        } catch (Exception e) {
            zzggp.zzd("DriveEventService", String.format("Error handling event in %s", this.mName), e);
        }
    }

    /* access modifiers changed from: private */
    public final void zzaof() throws SecurityException {
        int callingUid = getCallingUid();
        if (callingUid != this.zzgix) {
            if (zzx.zzf(this, callingUid)) {
                this.zzgix = callingUid;
                return;
            }
            throw new SecurityException("Caller is not GooglePlayServices");
        }
    }

    /* access modifiers changed from: protected */
    public int getCallingUid() {
        return Binder.getCallingUid();
    }

    public final synchronized IBinder onBind(Intent intent) {
        if (!ACTION_HANDLE_EVENT.equals(intent.getAction())) {
            return null;
        }
        if (this.zzgiv == null && !this.zzgiw) {
            this.zzgiw = true;
            CountDownLatch countDownLatch = new CountDownLatch(1);
            this.zzgiu = new CountDownLatch(1);
            new zzh(this, countDownLatch).start();
            try {
                if (!countDownLatch.await(5000, TimeUnit.MILLISECONDS)) {
                    zzggp.zzw("DriveEventService", "Failed to synchronously initialize event handler.");
                }
            } catch (InterruptedException e) {
                throw new RuntimeException("Unable to start event handler", e);
            }
        }
        return new zzb().asBinder();
    }

    public void onChange(ChangeEvent changeEvent) {
        zzggp.zzc("DriveEventService", "Unhandled change event in %s: %s", this.mName, changeEvent);
    }

    public void onCompletion(CompletionEvent completionEvent) {
        zzggp.zzc("DriveEventService", "Unhandled completion event in %s: %s", this.mName, completionEvent);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:4|5|6|(1:8)|9|10) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0031 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onDestroy() {
        /*
            r5 = this;
            monitor-enter(r5)
            com.google.android.gms.common.internal.zzal r0 = com.google.android.gms.drive.events.DriveEventService.zzggp     // Catch:{ all -> 0x0038 }
            java.lang.String r1 = "DriveEventService"
            java.lang.String r2 = "onDestroy"
            r0.zzu(r1, r2)     // Catch:{ all -> 0x0038 }
            com.google.android.gms.drive.events.DriveEventService$zza r0 = r5.zzgiv     // Catch:{ all -> 0x0038 }
            if (r0 == 0) goto L_0x0033
            com.google.android.gms.drive.events.DriveEventService$zza r0 = r5.zzgiv     // Catch:{ all -> 0x0038 }
            android.os.Message r0 = r0.zzaoh()     // Catch:{ all -> 0x0038 }
            com.google.android.gms.drive.events.DriveEventService$zza r1 = r5.zzgiv     // Catch:{ all -> 0x0038 }
            r1.sendMessage(r0)     // Catch:{ all -> 0x0038 }
            r0 = 0
            r5.zzgiv = r0     // Catch:{ all -> 0x0038 }
            java.util.concurrent.CountDownLatch r1 = r5.zzgiu     // Catch:{ InterruptedException -> 0x0031 }
            r2 = 5000(0x1388, double:2.4703E-320)
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ InterruptedException -> 0x0031 }
            boolean r1 = r1.await(r2, r4)     // Catch:{ InterruptedException -> 0x0031 }
            if (r1 != 0) goto L_0x0031
            com.google.android.gms.common.internal.zzal r1 = com.google.android.gms.drive.events.DriveEventService.zzggp     // Catch:{ InterruptedException -> 0x0031 }
            java.lang.String r2 = "DriveEventService"
            java.lang.String r3 = "Failed to synchronously quit event handler. Will quit itself"
            r1.zzv(r2, r3)     // Catch:{ InterruptedException -> 0x0031 }
        L_0x0031:
            r5.zzgiu = r0     // Catch:{ all -> 0x0038 }
        L_0x0033:
            super.onDestroy()     // Catch:{ all -> 0x0038 }
            monitor-exit(r5)
            return
        L_0x0038:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.drive.events.DriveEventService.onDestroy():void");
    }

    public boolean onUnbind(Intent intent) {
        return true;
    }

    public final void zza(zzb zzb2) {
        zzggp.zzc("DriveEventService", "Unhandled changes available event in %s: %s", this.mName, zzb2);
    }
}
