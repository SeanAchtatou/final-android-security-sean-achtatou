package com.google.android.gms.drive;

import com.google.android.gms.drive.ExecutionOptions;

public final class zzq extends ExecutionOptions.Builder {
    public final /* synthetic */ ExecutionOptions build() {
        zzanv();
        return new zzo(this.zzgha, this.zzghb, this.zzghc);
    }

    public final /* synthetic */ ExecutionOptions.Builder setConflictStrategy(int i) {
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ ExecutionOptions.Builder setNotifyOnCompletion(boolean z) {
        super.setNotifyOnCompletion(z);
        return this;
    }

    public final /* synthetic */ ExecutionOptions.Builder setTrackingTag(String str) {
        super.setTrackingTag(str);
        return this;
    }
}
