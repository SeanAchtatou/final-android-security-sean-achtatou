package com.google.android.gms.games.internal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.AnnotatedData;
import com.google.android.gms.games.GamesClientStatusCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzg {
    private static zzp zzhph = new zzh();

    public static <R, PendingR extends Result> Task<R> zza(@NonNull PendingResult<PendingR> pendingResult, @NonNull zzbo<PendingR, R> zzbo) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        pendingResult.zza(new zzj(pendingResult, taskCompletionSource, zzbo));
        return taskCompletionSource.getTask();
    }

    public static <R, PendingR extends Result, ExceptionData> Task<AnnotatedData<R>> zza(@NonNull PendingResult pendingResult, @NonNull zzbo zzbo, @Nullable zzo zzo) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        pendingResult.zza(new zzk(pendingResult, taskCompletionSource, zzbo, zzo));
        return taskCompletionSource.getTask();
    }

    public static <R, PendingR extends Result, ExceptionData> Task<R> zza(@NonNull PendingResult pendingResult, @NonNull zzp zzp, @NonNull zzbo zzbo) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        pendingResult.zza(new zzm(zzp, pendingResult, taskCompletionSource, zzbo));
        return taskCompletionSource.getTask();
    }

    public static <R, PendingR extends Result, ExceptionData> Task<R> zza(@NonNull PendingResult<PendingR> pendingResult, @NonNull zzp zzp, @NonNull zzbo<PendingR, R> zzbo, @NonNull zzbo<PendingR, ExceptionData> zzbo2, @NonNull zzn<ExceptionData> zzn) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        pendingResult.zza(new zzi(pendingResult, zzp, taskCompletionSource, zzbo, zzbo2, zzn));
        return taskCompletionSource.getTask();
    }

    /* access modifiers changed from: private */
    public static Status zzah(@NonNull Status status) {
        int zzdh = GamesClientStatusCodes.zzdh(status.getStatusCode());
        return zzdh != status.getStatusCode() ? GamesStatusCodes.getStatusString(status.getStatusCode()).equals(status.getStatusMessage()) ? GamesClientStatusCodes.zzdg(zzdh) : new Status(zzdh, status.getStatusMessage()) : status;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [com.google.android.gms.common.api.PendingResult, com.google.android.gms.common.api.PendingResult<PendingR>] */
    /* JADX WARN: Type inference failed for: r2v0, types: [com.google.android.gms.common.internal.zzbo<PendingR, R>, com.google.android.gms.common.internal.zzbo] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <R, PendingR extends com.google.android.gms.common.api.Result, ExceptionData> com.google.android.gms.tasks.Task<com.google.android.gms.games.AnnotatedData<R>> zzb(@android.support.annotation.NonNull com.google.android.gms.common.api.PendingResult<PendingR> r1, @android.support.annotation.NonNull com.google.android.gms.common.internal.zzbo<PendingR, R> r2) {
        /*
            r0 = 0
            com.google.android.gms.tasks.Task r1 = zza(r1, r2, r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.games.internal.zzg.zzb(com.google.android.gms.common.api.PendingResult, com.google.android.gms.common.internal.zzbo):com.google.android.gms.tasks.Task");
    }

    public static <R extends Releasable, PendingR extends Result, ExceptionData> Task<AnnotatedData<R>> zzc(@NonNull PendingResult<PendingR> pendingResult, @NonNull zzbo<PendingR, R> zzbo) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        pendingResult.zza(new zzl(zzbo, pendingResult, taskCompletionSource));
        return taskCompletionSource.getTask();
    }
}
