package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzbmp extends zzbmt {
    private /* synthetic */ zzbmo zzglx;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbmp(zzbmo zzbmo, GoogleApiClient googleApiClient) {
        super(zzbmo, googleApiClient);
        this.zzglx = zzbmo;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zzb(new zzbmr(this.zzglx, this, null));
    }
}
