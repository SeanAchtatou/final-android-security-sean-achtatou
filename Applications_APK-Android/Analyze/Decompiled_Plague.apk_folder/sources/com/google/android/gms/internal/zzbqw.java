package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveFile;

final class zzbqw extends zzbjv {
    private final zzn<DriveApi.DriveContentsResult> zzfzc;
    private final DriveFile.DownloadProgressListener zzgoq;

    zzbqw(zzn<DriveApi.DriveContentsResult> zzn, DriveFile.DownloadProgressListener downloadProgressListener) {
        this.zzfzc = zzn;
        this.zzgoq = downloadProgressListener;
    }

    public final void onError(Status status) throws RemoteException {
        this.zzfzc.setResult(new zzbkx(status, null));
    }

    public final void zza(zzbps zzbps) throws RemoteException {
        this.zzfzc.setResult(new zzbkx(zzbps.zzgnw ? new Status(-1) : Status.zzfko, new zzblv(zzbps.zzglf)));
    }

    public final void zza(zzbpw zzbpw) throws RemoteException {
        if (this.zzgoq != null) {
            this.zzgoq.onProgress(zzbpw.zzgnz, zzbpw.zzgoa);
        }
    }
}
