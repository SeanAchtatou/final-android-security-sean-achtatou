package com.google.android.gms.games;

import com.google.android.gms.common.api.internal.zzco;
import com.google.android.gms.games.multiplayer.realtime.zzh;

final class zzbo implements zzco<zzh> {
    private /* synthetic */ zzbn zzhkr;

    zzbo(zzbn zzbn) {
        this.zzhkr = zzbn;
    }

    public final void zzahn() {
        this.zzhkr.zzhkq.zzhkn.getRoomUpdateCallback().onLeftRoom(0, this.zzhkr.zzhkq.zzhkg);
    }

    public final /* synthetic */ void zzt(Object obj) {
        ((zzh) obj).onLeftRoom(0, this.zzhkr.zzhkq.zzhkg);
    }
}
