package com.google.android.gms.internal;

import java.io.IOException;

public final class zzim extends zzfhe<zzim> {
    public Integer zzbbq = null;
    public Integer zzbbr = null;
    public Integer zzbbs = null;

    public zzim() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 8) {
                this.zzbbq = Integer.valueOf(zzfhb.zzcuh());
            } else if (zzcts == 16) {
                this.zzbbr = Integer.valueOf(zzfhb.zzcuh());
            } else if (zzcts == 24) {
                this.zzbbs = Integer.valueOf(zzfhb.zzcuh());
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzbbq != null) {
            zzfhc.zzaa(1, this.zzbbq.intValue());
        }
        if (this.zzbbr != null) {
            zzfhc.zzaa(2, this.zzbbr.intValue());
        }
        if (this.zzbbs != null) {
            zzfhc.zzaa(3, this.zzbbs.intValue());
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzbbq != null) {
            zzo += zzfhc.zzad(1, this.zzbbq.intValue());
        }
        if (this.zzbbr != null) {
            zzo += zzfhc.zzad(2, this.zzbbr.intValue());
        }
        return this.zzbbs != null ? zzo + zzfhc.zzad(3, this.zzbbs.intValue()) : zzo;
    }
}
