package com.google.android.gms.drive;

@Deprecated
public final class zzr extends ExecutionOptions {
    private boolean zzghf;

    private zzr(String str, boolean z, int i, boolean z2) {
        super(str, z, i);
        this.zzghf = z2;
    }

    public static zzr zzb(ExecutionOptions executionOptions) {
        zzt zzt = new zzt();
        if (executionOptions != null) {
            zzt.setConflictStrategy(executionOptions.zzanu());
            zzt.setNotifyOnCompletion(executionOptions.zzant());
            String zzans = executionOptions.zzans();
            if (zzans != null) {
                zzt.setTrackingTag(zzans);
            }
        }
        return (zzr) zzt.build();
    }

    public final boolean zzany() {
        return this.zzghf;
    }
}
