package com.google.android.gms.games;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;

final class zzbm implements Continuation<Boolean, Task<Void>> {
    final /* synthetic */ String zzhkg;
    final /* synthetic */ RoomConfig zzhkn;
    final /* synthetic */ zzcl zzhko;
    private /* synthetic */ RealTimeMultiplayerClient zzhkp;

    zzbm(RealTimeMultiplayerClient realTimeMultiplayerClient, zzcl zzcl, String str, RoomConfig roomConfig) {
        this.zzhkp = realTimeMultiplayerClient;
        this.zzhko = zzcl;
        this.zzhkg = str;
        this.zzhkn = roomConfig;
    }

    public final /* synthetic */ Object then(@NonNull Task task) throws Exception {
        return this.zzhkp.zza(new zzbn(this));
    }
}
