package com.google.android.gms.ads.internal.js;

final class zzr implements Runnable {
    final /* synthetic */ zzq zzbzt;

    zzr(zzq zzq) {
        this.zzbzt = zzq;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0042, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r3 = this;
            com.google.android.gms.ads.internal.js.zzq r0 = r3.zzbzt
            com.google.android.gms.ads.internal.js.zzp r0 = r0.zzbzs
            com.google.android.gms.ads.internal.js.zzo r0 = r0.zzbzq
            java.lang.Object r0 = r0.mLock
            monitor-enter(r0)
            com.google.android.gms.ads.internal.js.zzq r1 = r3.zzbzt     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzp r1 = r1.zzbzs     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzaf r1 = r1.zzbzp     // Catch:{ all -> 0x0043 }
            int r1 = r1.getStatus()     // Catch:{ all -> 0x0043 }
            r2 = -1
            if (r1 == r2) goto L_0x0041
            com.google.android.gms.ads.internal.js.zzq r1 = r3.zzbzt     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzp r1 = r1.zzbzs     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzaf r1 = r1.zzbzp     // Catch:{ all -> 0x0043 }
            int r1 = r1.getStatus()     // Catch:{ all -> 0x0043 }
            r2 = 1
            if (r1 != r2) goto L_0x0026
            goto L_0x0041
        L_0x0026:
            com.google.android.gms.ads.internal.js.zzq r1 = r3.zzbzt     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzp r1 = r1.zzbzs     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzaf r1 = r1.zzbzp     // Catch:{ all -> 0x0043 }
            r1.reject()     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.zzbs.zzec()     // Catch:{ all -> 0x0043 }
            com.google.android.gms.ads.internal.js.zzs r1 = new com.google.android.gms.ads.internal.js.zzs     // Catch:{ all -> 0x0043 }
            r1.<init>(r3)     // Catch:{ all -> 0x0043 }
            com.google.android.gms.internal.zzagr.runOnUiThread(r1)     // Catch:{ all -> 0x0043 }
            java.lang.String r1 = "Could not receive loaded message in a timely manner. Rejecting."
            com.google.android.gms.internal.zzafj.v(r1)     // Catch:{ all -> 0x0043 }
            monitor-exit(r0)     // Catch:{ all -> 0x0043 }
            return
        L_0x0041:
            monitor-exit(r0)     // Catch:{ all -> 0x0043 }
            return
        L_0x0043:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0043 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.js.zzr.run():void");
    }
}
