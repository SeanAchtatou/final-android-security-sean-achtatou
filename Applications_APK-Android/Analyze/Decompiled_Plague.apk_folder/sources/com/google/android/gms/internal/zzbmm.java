package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveFolder;

final class zzbmm implements DriveFolder.DriveFolderResult {
    private final Status mStatus;
    private final DriveFolder zzglw;

    public zzbmm(Status status, DriveFolder driveFolder) {
        this.mStatus = status;
        this.zzglw = driveFolder;
    }

    public final DriveFolder getDriveFolder() {
        return this.zzglw;
    }

    public final Status getStatus() {
        return this.mStatus;
    }
}
