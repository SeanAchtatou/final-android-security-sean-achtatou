package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.zzo;

@zzzb
public final class zzaiy extends zzbej {
    public static final Parcelable.Creator<zzaiy> CREATOR = new zzaiz();
    public String zzcp;
    public int zzdbz;
    public int zzdca;
    public boolean zzdcb;
    public boolean zzdcc;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzaiy.<init>(int, int, boolean, boolean, boolean):void
     arg types: [int, int, boolean, int, int]
     candidates:
      com.google.android.gms.internal.zzaiy.<init>(java.lang.String, int, int, boolean, boolean):void
      com.google.android.gms.internal.zzaiy.<init>(int, int, boolean, boolean, boolean):void */
    public zzaiy(int i, int i2, boolean z) {
        this(i, i2, z, false, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzaiy.<init>(int, int, boolean, boolean, boolean):void
     arg types: [?, int, int, int, boolean]
     candidates:
      com.google.android.gms.internal.zzaiy.<init>(java.lang.String, int, int, boolean, boolean):void
      com.google.android.gms.internal.zzaiy.<init>(int, int, boolean, boolean, boolean):void */
    public zzaiy(int i, int i2, boolean z, boolean z2) {
        this((int) zzo.GOOGLE_PLAY_SERVICES_VERSION_CODE, i2, true, false, z2);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private zzaiy(int r10, int r11, boolean r12, boolean r13, boolean r14) {
        /*
            r9 = this;
            java.lang.String r13 = "afma-sdk-a-v"
            if (r12 == 0) goto L_0x0007
            java.lang.String r0 = "0"
            goto L_0x0009
        L_0x0007:
            java.lang.String r0 = "1"
        L_0x0009:
            r1 = 24
            java.lang.String r2 = java.lang.String.valueOf(r13)
            int r2 = r2.length()
            int r1 = r1 + r2
            java.lang.String r2 = java.lang.String.valueOf(r0)
            int r2 = r2.length()
            int r1 = r1 + r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r1)
            r2.append(r13)
            r2.append(r10)
            java.lang.String r13 = "."
            r2.append(r13)
            r2.append(r11)
            java.lang.String r13 = "."
            r2.append(r13)
            r2.append(r0)
            java.lang.String r4 = r2.toString()
            r3 = r9
            r5 = r10
            r6 = r11
            r7 = r12
            r8 = r14
            r3.<init>(r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaiy.<init>(int, int, boolean, boolean, boolean):void");
    }

    zzaiy(String str, int i, int i2, boolean z, boolean z2) {
        this.zzcp = str;
        this.zzdbz = i;
        this.zzdca = i2;
        this.zzdcb = z;
        this.zzdcc = z2;
    }

    public static zzaiy zzqv() {
        return new zzaiy(11720208, 11720208, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, this.zzcp, false);
        zzbem.zzc(parcel, 3, this.zzdbz);
        zzbem.zzc(parcel, 4, this.zzdca);
        zzbem.zza(parcel, 5, this.zzdcb);
        zzbem.zza(parcel, 6, this.zzdcc);
        zzbem.zzai(parcel, zze);
    }
}
