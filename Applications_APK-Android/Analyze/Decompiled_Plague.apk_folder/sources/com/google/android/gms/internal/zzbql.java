package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbql extends zzbej {
    public static final Parcelable.Creator<zzbql> CREATOR = new zzbqm();
    final zzbqz zzgol;

    zzbql(zzbqz zzbqz) {
        this.zzgol = zzbqz;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.zzbqz, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, (Parcelable) this.zzgol, i, false);
        zzbem.zzai(parcel, zze);
    }
}
