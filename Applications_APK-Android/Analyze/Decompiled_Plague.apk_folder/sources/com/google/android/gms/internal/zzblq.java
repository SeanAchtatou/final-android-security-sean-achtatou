package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.zzbj;
import com.google.android.gms.drive.CreateFileActivityOptions;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.OpenFileActivityOptions;
import com.google.android.gms.drive.TransferPreferences;
import com.google.android.gms.tasks.Task;

public final class zzblq extends DriveClient {
    private final DriveApi zzglb = new zzbkp();

    public zzblq(@NonNull Activity activity, @Nullable Drive.zza zza) {
        super(activity, zza);
    }

    public zzblq(@NonNull Context context, @Nullable Drive.zza zza) {
        super(context, zza);
    }

    public final Task<DriveId> getDriveId(@NonNull String str) {
        return zzbj.zza(this.zzglb.fetchDriveId(zzagb(), str), zzblr.zzglc);
    }

    public final Task<TransferPreferences> getUploadPreferences() {
        return zzbj.zza(this.zzglb.zze(zzagb()), zzbls.zzglc);
    }

    public final Task<IntentSender> newCreateFileActivityIntentSender(CreateFileActivityOptions createFileActivityOptions) {
        return zza(new zzblu(this, createFileActivityOptions));
    }

    public final Task<IntentSender> newOpenFileActivityIntentSender(OpenFileActivityOptions openFileActivityOptions) {
        return zza(new zzblt(this, openFileActivityOptions));
    }

    public final Task<Void> requestSync() {
        return zzbj.zzb(this.zzglb.requestSync(zzagb()));
    }

    public final Task<Void> setUploadPreferences(@NonNull TransferPreferences transferPreferences) {
        return zzbj.zzb(this.zzglb.zza(zzagb(), transferPreferences));
    }
}
