package com.google.android.gms.games.event;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class EventEntity extends zzc implements Event {
    public static final Parcelable.Creator<EventEntity> CREATOR = new zza();
    private final String mName;
    private final boolean zzavl;
    private final long zzdot;
    private final String zzdou;
    private final Uri zzhgv;
    private final String zzhhg;
    private final PlayerEntity zzhmf;
    private final String zzhmk;
    private final String zzhml;

    public EventEntity(Event event) {
        this.zzhmk = event.getEventId();
        this.mName = event.getName();
        this.zzdou = event.getDescription();
        this.zzhgv = event.getIconImageUri();
        this.zzhhg = event.getIconImageUrl();
        this.zzhmf = (PlayerEntity) event.getPlayer().freeze();
        this.zzdot = event.getValue();
        this.zzhml = event.getFormattedValue();
        this.zzavl = event.isVisible();
    }

    EventEntity(String str, String str2, String str3, Uri uri, String str4, Player player, long j, String str5, boolean z) {
        this.zzhmk = str;
        this.mName = str2;
        this.zzdou = str3;
        this.zzhgv = uri;
        this.zzhhg = str4;
        this.zzhmf = new PlayerEntity(player);
        this.zzdot = j;
        this.zzhml = str5;
        this.zzavl = z;
    }

    static int zza(Event event) {
        return Arrays.hashCode(new Object[]{event.getEventId(), event.getName(), event.getDescription(), event.getIconImageUri(), event.getIconImageUrl(), event.getPlayer(), Long.valueOf(event.getValue()), event.getFormattedValue(), Boolean.valueOf(event.isVisible())});
    }

    static boolean zza(Event event, Object obj) {
        if (!(obj instanceof Event)) {
            return false;
        }
        if (event == obj) {
            return true;
        }
        Event event2 = (Event) obj;
        return zzbg.equal(event2.getEventId(), event.getEventId()) && zzbg.equal(event2.getName(), event.getName()) && zzbg.equal(event2.getDescription(), event.getDescription()) && zzbg.equal(event2.getIconImageUri(), event.getIconImageUri()) && zzbg.equal(event2.getIconImageUrl(), event.getIconImageUrl()) && zzbg.equal(event2.getPlayer(), event.getPlayer()) && zzbg.equal(Long.valueOf(event2.getValue()), Long.valueOf(event.getValue())) && zzbg.equal(event2.getFormattedValue(), event.getFormattedValue()) && zzbg.equal(Boolean.valueOf(event2.isVisible()), Boolean.valueOf(event.isVisible()));
    }

    static String zzb(Event event) {
        return zzbg.zzw(event).zzg("Id", event.getEventId()).zzg("Name", event.getName()).zzg("Description", event.getDescription()).zzg("IconImageUri", event.getIconImageUri()).zzg("IconImageUrl", event.getIconImageUrl()).zzg("Player", event.getPlayer()).zzg("Value", Long.valueOf(event.getValue())).zzg("FormattedValue", event.getFormattedValue()).zzg("isVisible", Boolean.valueOf(event.isVisible())).toString();
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    public final Event freeze() {
        return this;
    }

    public final String getDescription() {
        return this.zzdou;
    }

    public final void getDescription(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.zzdou, charArrayBuffer);
    }

    public final String getEventId() {
        return this.zzhmk;
    }

    public final String getFormattedValue() {
        return this.zzhml;
    }

    public final void getFormattedValue(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.zzhml, charArrayBuffer);
    }

    public final Uri getIconImageUri() {
        return this.zzhgv;
    }

    public final String getIconImageUrl() {
        return this.zzhhg;
    }

    public final String getName() {
        return this.mName;
    }

    public final void getName(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.mName, charArrayBuffer);
    }

    public final Player getPlayer() {
        return this.zzhmf;
    }

    public final long getValue() {
        return this.zzdot;
    }

    public final int hashCode() {
        return zza(this);
    }

    public final boolean isDataValid() {
        return true;
    }

    public final boolean isVisible() {
        return this.zzavl;
    }

    public final String toString() {
        return zzb(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.Player, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, getEventId(), false);
        zzbem.zza(parcel, 2, getName(), false);
        zzbem.zza(parcel, 3, getDescription(), false);
        zzbem.zza(parcel, 4, (Parcelable) getIconImageUri(), i, false);
        zzbem.zza(parcel, 5, getIconImageUrl(), false);
        zzbem.zza(parcel, 6, (Parcelable) getPlayer(), i, false);
        zzbem.zza(parcel, 7, getValue());
        zzbem.zza(parcel, 8, getFormattedValue(), false);
        zzbem.zza(parcel, 9, isVisible());
        zzbem.zzai(parcel, zze);
    }
}
