package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzak extends zzar {
    private /* synthetic */ String zzhis;
    private /* synthetic */ int zzhqa;
    private /* synthetic */ int zzhqb;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzak(zzah zzah, GoogleApiClient googleApiClient, String str, int i, int i2) {
        super(googleApiClient, null);
        this.zzhis = str;
        this.zzhqa = i;
        this.zzhqb = i2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, (String) null, this.zzhis, this.zzhqa, this.zzhqb);
    }
}
