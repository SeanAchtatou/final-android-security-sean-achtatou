package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzae;
import com.google.android.gms.common.internal.zzaf;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.common.zze;
import com.google.android.gms.internal.zzbeq;
import com.google.android.gms.internal.zzcwb;
import com.google.android.gms.internal.zzcwc;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;

public final class zzbd extends GoogleApiClient implements zzcg {
    /* access modifiers changed from: private */
    public final Context mContext;
    private final Looper zzakm;
    private final int zzfkc;
    private final GoogleApiAvailability zzfke;
    private Api.zza<? extends zzcwb, zzcwc> zzfkf;
    private boolean zzfki;
    private final Lock zzfmy;
    private zzr zzfnd;
    private Map<Api<?>, Boolean> zzfng;
    final Queue<zzm<?, ?>> zzfnm = new LinkedList();
    private final zzae zzfpa;
    private zzcf zzfpb = null;
    private volatile boolean zzfpc;
    private long zzfpd = 120000;
    private long zzfpe = 5000;
    private final zzbi zzfpf;
    private zzca zzfpg;
    final Map<Api.zzc<?>, Api.zze> zzfph;
    Set<Scope> zzfpi = new HashSet();
    private final zzcp zzfpj = new zzcp();
    private final ArrayList<zzw> zzfpk;
    private Integer zzfpl = null;
    Set<zzdi> zzfpm = null;
    final zzdl zzfpn;
    private final zzaf zzfpo = new zzbe(this);

    public zzbd(Context context, Lock lock, Looper looper, zzr zzr, GoogleApiAvailability googleApiAvailability, Api.zza<? extends zzcwb, zzcwc> zza, Map<Api<?>, Boolean> map, List<GoogleApiClient.ConnectionCallbacks> list, List<GoogleApiClient.OnConnectionFailedListener> list2, Map<Api.zzc<?>, Api.zze> map2, int i, int i2, ArrayList<zzw> arrayList, boolean z) {
        Looper looper2 = looper;
        this.mContext = context;
        this.zzfmy = lock;
        this.zzfki = false;
        this.zzfpa = new zzae(looper2, this.zzfpo);
        this.zzakm = looper2;
        this.zzfpf = new zzbi(this, looper2);
        this.zzfke = googleApiAvailability;
        this.zzfkc = i;
        if (this.zzfkc >= 0) {
            this.zzfpl = Integer.valueOf(i2);
        }
        this.zzfng = map;
        this.zzfph = map2;
        this.zzfpk = arrayList;
        this.zzfpn = new zzdl(this.zzfph);
        for (GoogleApiClient.ConnectionCallbacks registerConnectionCallbacks : list) {
            this.zzfpa.registerConnectionCallbacks(registerConnectionCallbacks);
        }
        for (GoogleApiClient.OnConnectionFailedListener registerConnectionFailedListener : list2) {
            this.zzfpa.registerConnectionFailedListener(registerConnectionFailedListener);
        }
        this.zzfnd = zzr;
        this.zzfkf = zza;
    }

    /* access modifiers changed from: private */
    public final void resume() {
        this.zzfmy.lock();
        try {
            if (this.zzfpc) {
                zzahw();
            }
        } finally {
            this.zzfmy.unlock();
        }
    }

    public static int zza(Iterable<Api.zze> iterable, boolean z) {
        boolean z2 = false;
        boolean z3 = false;
        for (Api.zze next : iterable) {
            if (next.zzaam()) {
                z2 = true;
            }
            if (next.zzaaw()) {
                z3 = true;
            }
        }
        if (z2) {
            return (!z3 || !z) ? 1 : 2;
        }
        return 3;
    }

    /* access modifiers changed from: private */
    public final void zza(GoogleApiClient googleApiClient, zzdc zzdc, boolean z) {
        zzbeq.zzfzb.zzd(googleApiClient).setResultCallback(new zzbh(this, zzdc, z, googleApiClient));
    }

    private final void zzahw() {
        this.zzfpa.zzakx();
        this.zzfpb.connect();
    }

    /* access modifiers changed from: private */
    public final void zzahx() {
        this.zzfmy.lock();
        try {
            if (zzahy()) {
                zzahw();
            }
        } finally {
            this.zzfmy.unlock();
        }
    }

    private final void zzbv(int i) {
        if (this.zzfpl == null) {
            this.zzfpl = Integer.valueOf(i);
        } else if (this.zzfpl.intValue() != i) {
            String zzbw = zzbw(i);
            String zzbw2 = zzbw(this.zzfpl.intValue());
            StringBuilder sb = new StringBuilder(51 + String.valueOf(zzbw).length() + String.valueOf(zzbw2).length());
            sb.append("Cannot use sign-in mode: ");
            sb.append(zzbw);
            sb.append(". Mode was already set to ");
            sb.append(zzbw2);
            throw new IllegalStateException(sb.toString());
        }
        if (this.zzfpb == null) {
            boolean z = false;
            boolean z2 = false;
            for (Api.zze next : this.zzfph.values()) {
                if (next.zzaam()) {
                    z = true;
                }
                if (next.zzaaw()) {
                    z2 = true;
                }
            }
            switch (this.zzfpl.intValue()) {
                case 1:
                    if (!z) {
                        throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
                    } else if (z2) {
                        throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
                    }
                    break;
                case 2:
                    if (z) {
                        if (this.zzfki) {
                            this.zzfpb = new zzad(this.mContext, this.zzfmy, this.zzakm, this.zzfke, this.zzfph, this.zzfnd, this.zzfng, this.zzfkf, this.zzfpk, this, true);
                            return;
                        } else {
                            this.zzfpb = zzy.zza(this.mContext, this, this.zzfmy, this.zzakm, this.zzfke, this.zzfph, this.zzfnd, this.zzfng, this.zzfkf, this.zzfpk);
                            return;
                        }
                    }
                    break;
            }
            if (!this.zzfki || z2) {
                this.zzfpb = new zzbl(this.mContext, this, this.zzfmy, this.zzakm, this.zzfke, this.zzfph, this.zzfnd, this.zzfng, this.zzfkf, this.zzfpk, this);
            } else {
                this.zzfpb = new zzad(this.mContext, this.zzfmy, this.zzakm, this.zzfke, this.zzfph, this.zzfnd, this.zzfng, this.zzfkf, this.zzfpk, this, false);
            }
        }
    }

    private static String zzbw(int i) {
        switch (i) {
            case 1:
                return "SIGN_IN_MODE_REQUIRED";
            case 2:
                return "SIGN_IN_MODE_OPTIONAL";
            case 3:
                return "SIGN_IN_MODE_NONE";
            default:
                return "UNKNOWN";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final ConnectionResult blockingConnect() {
        boolean z = true;
        zzbq.zza(Looper.myLooper() != Looper.getMainLooper(), (Object) "blockingConnect must not be called on the UI thread");
        this.zzfmy.lock();
        try {
            if (this.zzfkc >= 0) {
                if (this.zzfpl == null) {
                    z = false;
                }
                zzbq.zza(z, (Object) "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.zzfpl == null) {
                this.zzfpl = Integer.valueOf(zza(this.zzfph.values(), false));
            } else if (this.zzfpl.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            zzbv(this.zzfpl.intValue());
            this.zzfpa.zzakx();
            return this.zzfpb.blockingConnect();
        } finally {
            this.zzfmy.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final ConnectionResult blockingConnect(long j, @NonNull TimeUnit timeUnit) {
        zzbq.zza(Looper.myLooper() != Looper.getMainLooper(), (Object) "blockingConnect must not be called on the UI thread");
        zzbq.checkNotNull(timeUnit, "TimeUnit must not be null");
        this.zzfmy.lock();
        try {
            if (this.zzfpl == null) {
                this.zzfpl = Integer.valueOf(zza(this.zzfph.values(), false));
            } else if (this.zzfpl.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            zzbv(this.zzfpl.intValue());
            this.zzfpa.zzakx();
            return this.zzfpb.blockingConnect(j, timeUnit);
        } finally {
            this.zzfmy.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final PendingResult<Status> clearDefaultAccountAndReconnect() {
        zzbq.zza(isConnected(), (Object) "GoogleApiClient is not connected yet.");
        zzbq.zza(this.zzfpl.intValue() != 2, (Object) "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API");
        zzdc zzdc = new zzdc(this);
        if (this.zzfph.containsKey(zzbeq.zzdyh)) {
            zza(this, zzdc, false);
            return zzdc;
        }
        AtomicReference atomicReference = new AtomicReference();
        GoogleApiClient build = new GoogleApiClient.Builder(this.mContext).addApi(zzbeq.API).addConnectionCallbacks(new zzbf(this, atomicReference, zzdc)).addOnConnectionFailedListener(new zzbg(this, zzdc)).setHandler(this.zzfpf).build();
        atomicReference.set(build);
        build.connect();
        return zzdc;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final void connect() {
        this.zzfmy.lock();
        try {
            boolean z = false;
            if (this.zzfkc >= 0) {
                if (this.zzfpl != null) {
                    z = true;
                }
                zzbq.zza(z, (Object) "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.zzfpl == null) {
                this.zzfpl = Integer.valueOf(zza(this.zzfph.values(), false));
            } else if (this.zzfpl.intValue() == 2) {
                throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            connect(this.zzfpl.intValue());
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final void connect(int i) {
        this.zzfmy.lock();
        boolean z = true;
        if (!(i == 3 || i == 1 || i == 2)) {
            z = false;
        }
        try {
            StringBuilder sb = new StringBuilder(33);
            sb.append("Illegal sign-in mode: ");
            sb.append(i);
            zzbq.checkArgument(z, sb.toString());
            zzbv(i);
            zzahw();
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final void disconnect() {
        this.zzfmy.lock();
        try {
            this.zzfpn.release();
            if (this.zzfpb != null) {
                this.zzfpb.disconnect();
            }
            this.zzfpj.release();
            for (zzm next : this.zzfnm) {
                next.zza((zzdo) null);
                next.cancel();
            }
            this.zzfnm.clear();
            if (this.zzfpb != null) {
                zzahy();
                this.zzfpa.zzakw();
            }
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append((CharSequence) "mContext=").println(this.mContext);
        printWriter.append((CharSequence) str).append((CharSequence) "mResuming=").print(this.zzfpc);
        printWriter.append((CharSequence) " mWorkQueue.size()=").print(this.zzfnm.size());
        printWriter.append((CharSequence) " mUnconsumedApiCalls.size()=").println(this.zzfpn.zzfso.size());
        if (this.zzfpb != null) {
            this.zzfpb.dump(str, fileDescriptor, printWriter, strArr);
        }
    }

    @NonNull
    public final ConnectionResult getConnectionResult(@NonNull Api<?> api) {
        ConnectionResult connectionResult;
        this.zzfmy.lock();
        try {
            if (!isConnected() && !this.zzfpc) {
                throw new IllegalStateException("Cannot invoke getConnectionResult unless GoogleApiClient is connected");
            } else if (this.zzfph.containsKey(api.zzaft())) {
                ConnectionResult connectionResult2 = this.zzfpb.getConnectionResult(api);
                if (connectionResult2 == null) {
                    if (this.zzfpc) {
                        connectionResult = ConnectionResult.zzfhy;
                    } else {
                        Log.w("GoogleApiClientImpl", zzaia());
                        Log.wtf("GoogleApiClientImpl", String.valueOf(api.getName()).concat(" requested in getConnectionResult is not connected but is not present in the failed  connections map"), new Exception());
                        connectionResult = new ConnectionResult(8, null);
                    }
                    return connectionResult;
                }
                this.zzfmy.unlock();
                return connectionResult2;
            } else {
                throw new IllegalArgumentException(String.valueOf(api.getName()).concat(" was never registered with GoogleApiClient"));
            }
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final Context getContext() {
        return this.mContext;
    }

    public final Looper getLooper() {
        return this.zzakm;
    }

    public final boolean hasConnectedApi(@NonNull Api<?> api) {
        Api.zze zze;
        return isConnected() && (zze = this.zzfph.get(api.zzaft())) != null && zze.isConnected();
    }

    public final boolean isConnected() {
        return this.zzfpb != null && this.zzfpb.isConnected();
    }

    public final boolean isConnecting() {
        return this.zzfpb != null && this.zzfpb.isConnecting();
    }

    public final boolean isConnectionCallbacksRegistered(@NonNull GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        return this.zzfpa.isConnectionCallbacksRegistered(connectionCallbacks);
    }

    public final boolean isConnectionFailedListenerRegistered(@NonNull GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        return this.zzfpa.isConnectionFailedListenerRegistered(onConnectionFailedListener);
    }

    public final void reconnect() {
        disconnect();
        connect();
    }

    public final void registerConnectionCallbacks(@NonNull GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        this.zzfpa.registerConnectionCallbacks(connectionCallbacks);
    }

    public final void registerConnectionFailedListener(@NonNull GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this.zzfpa.registerConnectionFailedListener(onConnectionFailedListener);
    }

    public final void stopAutoManage(@NonNull FragmentActivity fragmentActivity) {
        zzch zzch = new zzch(fragmentActivity);
        if (this.zzfkc >= 0) {
            zzi.zza(zzch).zzbr(this.zzfkc);
            return;
        }
        throw new IllegalStateException("Called stopAutoManage but automatic lifecycle management is not enabled.");
    }

    public final void unregisterConnectionCallbacks(@NonNull GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        this.zzfpa.unregisterConnectionCallbacks(connectionCallbacks);
    }

    public final void unregisterConnectionFailedListener(@NonNull GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this.zzfpa.unregisterConnectionFailedListener(onConnectionFailedListener);
    }

    @NonNull
    public final <C extends Api.zze> C zza(@NonNull Api.zzc<C> zzc) {
        C c = (Api.zze) this.zzfph.get(zzc);
        zzbq.checkNotNull(c, "Appropriate Api was not requested.");
        return c;
    }

    public final void zza(zzdi zzdi) {
        this.zzfmy.lock();
        try {
            if (this.zzfpm == null) {
                this.zzfpm = new HashSet();
            }
            this.zzfpm.add(zzdi);
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final boolean zza(@NonNull Api<?> api) {
        return this.zzfph.containsKey(api.zzaft());
    }

    public final boolean zza(zzcx zzcx) {
        return this.zzfpb != null && this.zzfpb.zza(zzcx);
    }

    public final void zzagf() {
        if (this.zzfpb != null) {
            this.zzfpb.zzagf();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean zzahy() {
        if (!this.zzfpc) {
            return false;
        }
        this.zzfpc = false;
        this.zzfpf.removeMessages(2);
        this.zzfpf.removeMessages(1);
        if (this.zzfpg != null) {
            this.zzfpg.unregister();
            this.zzfpg = null;
        }
        return true;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public final boolean zzahz() {
        this.zzfmy.lock();
        try {
            if (this.zzfpm == null) {
                this.zzfmy.unlock();
                return false;
            }
            boolean z = !this.zzfpm.isEmpty();
            this.zzfmy.unlock();
            return z;
        } catch (Throwable th) {
            this.zzfmy.unlock();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public final String zzaia() {
        StringWriter stringWriter = new StringWriter();
        dump("", null, new PrintWriter(stringWriter), null);
        return stringWriter.toString();
    }

    public final void zzb(zzdi zzdi) {
        String str;
        String str2;
        Exception exc;
        this.zzfmy.lock();
        try {
            if (this.zzfpm == null) {
                str = "GoogleApiClientImpl";
                str2 = "Attempted to remove pending transform when no transforms are registered.";
                exc = new Exception();
            } else if (!this.zzfpm.remove(zzdi)) {
                str = "GoogleApiClientImpl";
                str2 = "Failed to remove pending transform - this may lead to memory leaks!";
                exc = new Exception();
            } else {
                if (!zzahz()) {
                    this.zzfpb.zzagy();
                }
            }
            Log.wtf(str, str2, exc);
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final void zzc(ConnectionResult connectionResult) {
        if (!zze.zze(this.mContext, connectionResult.getErrorCode())) {
            zzahy();
        }
        if (!this.zzfpc) {
            this.zzfpa.zzk(connectionResult);
            this.zzfpa.zzakw();
        }
    }

    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T zzd(@NonNull T t) {
        zzbq.checkArgument(t.zzaft() != null, "This task can not be enqueued (it's probably a Batch or malformed)");
        boolean containsKey = this.zzfph.containsKey(t.zzaft());
        String name = t.zzafy() != null ? t.zzafy().getName() : "the API";
        StringBuilder sb = new StringBuilder(65 + String.valueOf(name).length());
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(name);
        sb.append(" required for this call.");
        zzbq.checkArgument(containsKey, sb.toString());
        this.zzfmy.lock();
        try {
            if (this.zzfpb == null) {
                this.zzfnm.add(t);
            } else {
                t = this.zzfpb.zzd(t);
            }
            return t;
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T zze(@NonNull T t) {
        zzbq.checkArgument(t.zzaft() != null, "This task can not be executed (it's probably a Batch or malformed)");
        boolean containsKey = this.zzfph.containsKey(t.zzaft());
        String name = t.zzafy() != null ? t.zzafy().getName() : "the API";
        StringBuilder sb = new StringBuilder(65 + String.valueOf(name).length());
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(name);
        sb.append(" required for this call.");
        zzbq.checkArgument(containsKey, sb.toString());
        this.zzfmy.lock();
        try {
            if (this.zzfpb == null) {
                throw new IllegalStateException("GoogleApiClient is not connected yet.");
            }
            if (this.zzfpc) {
                this.zzfnm.add(t);
                while (!this.zzfnm.isEmpty()) {
                    zzm remove = this.zzfnm.remove();
                    this.zzfpn.zzb(remove);
                    remove.zzu(Status.zzfkq);
                }
            } else {
                t = this.zzfpb.zze(t);
            }
            return t;
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final void zzf(int i, boolean z) {
        if (i == 1 && !z && !this.zzfpc) {
            this.zzfpc = true;
            if (this.zzfpg == null) {
                this.zzfpg = GoogleApiAvailability.zza(this.mContext.getApplicationContext(), new zzbj(this));
            }
            this.zzfpf.sendMessageDelayed(this.zzfpf.obtainMessage(1), this.zzfpd);
            this.zzfpf.sendMessageDelayed(this.zzfpf.obtainMessage(2), this.zzfpe);
        }
        this.zzfpn.zzaji();
        this.zzfpa.zzcg(i);
        this.zzfpa.zzakw();
        if (i == 2) {
            zzahw();
        }
    }

    public final void zzj(Bundle bundle) {
        while (!this.zzfnm.isEmpty()) {
            zze(this.zzfnm.remove());
        }
        this.zzfpa.zzk(bundle);
    }

    public final <L> zzcl<L> zzs(@NonNull L l) {
        this.zzfmy.lock();
        try {
            return this.zzfpj.zza(l, this.zzakm, "NO_TYPE");
        } finally {
            this.zzfmy.unlock();
        }
    }
}
