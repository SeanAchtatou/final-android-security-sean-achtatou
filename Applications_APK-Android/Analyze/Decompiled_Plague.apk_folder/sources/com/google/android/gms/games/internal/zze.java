package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.common.internal.zzp;

final class zze implements zzn<Status> {
    private /* synthetic */ zzp zzhmy;

    zze(GamesClientImpl gamesClientImpl, zzp zzp) {
        this.zzhmy = zzp;
    }

    public final /* synthetic */ void setResult(Object obj) {
        this.zzhmy.zzait();
    }

    public final void zzu(Status status) {
        this.zzhmy.zzait();
    }
}
