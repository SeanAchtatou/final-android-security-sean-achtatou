package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.Map;

final class zzfga implements Iterator<Map.Entry<K, V>> {
    private int pos;
    private /* synthetic */ zzffu zzped;
    private boolean zzpee;
    private Iterator<Map.Entry<K, V>> zzpef;

    private zzfga(zzffu zzffu) {
        this.zzped = zzffu;
        this.pos = -1;
    }

    /* synthetic */ zzfga(zzffu zzffu, zzffv zzffv) {
        this(zzffu);
    }

    private final Iterator<Map.Entry<K, V>> zzcwp() {
        if (this.zzpef == null) {
            this.zzpef = this.zzped.zzpdx.entrySet().iterator();
        }
        return this.zzpef;
    }

    public final boolean hasNext() {
        return this.pos + 1 < this.zzped.zzpdw.size() || zzcwp().hasNext();
    }

    public final /* synthetic */ Object next() {
        this.zzpee = true;
        int i = this.pos + 1;
        this.pos = i;
        return (Map.Entry) (i < this.zzped.zzpdw.size() ? this.zzped.zzpdw.get(this.pos) : zzcwp().next());
    }

    public final void remove() {
        if (!this.zzpee) {
            throw new IllegalStateException("remove() was called before next()");
        }
        this.zzpee = false;
        this.zzped.zzcwl();
        if (this.pos < this.zzped.zzpdw.size()) {
            zzffu zzffu = this.zzped;
            int i = this.pos;
            this.pos = i - 1;
            Object unused = zzffu.zzlr(i);
            return;
        }
        zzcwp().remove();
    }
}
