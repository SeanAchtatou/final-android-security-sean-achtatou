package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.DriveId;

public final class zzbpb extends zzbej {
    public static final Parcelable.Creator<zzbpb> CREATOR = new zzbpc();
    private DriveId zzgjm;
    private boolean zzgnp;

    public zzbpb(DriveId driveId, boolean z) {
        this.zzgjm = driveId;
        this.zzgnp = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.DriveId, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, (Parcelable) this.zzgjm, i, false);
        zzbem.zza(parcel, 3, this.zzgnp);
        zzbem.zzai(parcel, zze);
    }
}
