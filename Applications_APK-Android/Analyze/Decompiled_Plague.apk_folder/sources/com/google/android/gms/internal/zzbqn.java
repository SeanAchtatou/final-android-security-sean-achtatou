package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbqn implements Parcelable.Creator<zzbqi> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        boolean z = false;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                zzbek.zzb(parcel, readInt);
            } else {
                z = zzbek.zzc(parcel, readInt);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbqi(z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbqi[i];
    }
}
