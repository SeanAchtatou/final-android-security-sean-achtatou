package com.google.android.gms.internal;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@zzzb
public final class zzalz extends zzalt {
    private static final Set<String> zzdhv = Collections.synchronizedSet(new HashSet());
    private static final DecimalFormat zzdhw = new DecimalFormat("#,###");
    private File zzdhx;
    private boolean zzdhy;

    public zzalz(zzali zzali) {
        super(zzali);
        File cacheDir = this.mContext.getCacheDir();
        if (cacheDir == null) {
            zzafj.zzco("Context.getCacheDir() returned null");
            return;
        }
        this.zzdhx = new File(cacheDir, "admobVideoStreams");
        if (!this.zzdhx.isDirectory() && !this.zzdhx.mkdirs()) {
            String valueOf = String.valueOf(this.zzdhx.getAbsolutePath());
            zzafj.zzco(valueOf.length() != 0 ? "Could not create preload cache directory at ".concat(valueOf) : new String("Could not create preload cache directory at "));
            this.zzdhx = null;
        } else if (!this.zzdhx.setReadable(true, false) || !this.zzdhx.setExecutable(true, false)) {
            String valueOf2 = String.valueOf(this.zzdhx.getAbsolutePath());
            zzafj.zzco(valueOf2.length() != 0 ? "Could not set cache file permissions at ".concat(valueOf2) : new String("Could not set cache file permissions at "));
            this.zzdhx = null;
        }
    }

    private final File zzc(File file) {
        return new File(this.zzdhx, String.valueOf(file.getName()).concat(".done"));
    }

    public final void abort() {
        this.zzdhy = true;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:179|180|181) */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01f9, code lost:
        if (r7 <= r6) goto L_0x0251;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:?, code lost:
        r3 = new java.lang.StringBuilder((33 + java.lang.String.valueOf(r2).length()) + java.lang.String.valueOf(r31).length());
        r3.append("Content length ");
        r3.append(r2);
        r3.append(" exceeds limit at ");
        r3.append(r9);
        com.google.android.gms.internal.zzafj.zzco(r3.toString());
        r2 = java.lang.String.valueOf(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0235, code lost:
        if (r2.length() == 0) goto L_0x023c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0237, code lost:
        r1 = "File too big for full file cache. Size: ".concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x023c, code lost:
        r1 = new java.lang.String("File too big for full file cache. Size: ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0242, code lost:
        zza(r9, r12.getAbsolutePath(), "sizeExceeded", r1);
        com.google.android.gms.internal.zzalz.zzdhv.remove(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0250, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:?, code lost:
        r4 = new java.lang.StringBuilder((20 + java.lang.String.valueOf(r2).length()) + java.lang.String.valueOf(r31).length());
        r4.append("Caching ");
        r4.append(r2);
        r4.append(" bytes from ");
        r4.append(r9);
        com.google.android.gms.internal.zzafj.zzbw(r4.toString());
        r5 = java.nio.channels.Channels.newChannel(r1.getInputStream());
        r4 = new java.io.FileOutputStream(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:?, code lost:
        r3 = r4.getChannel();
        r2 = java.nio.ByteBuffer.allocate(1048576);
        r1 = com.google.android.gms.ads.internal.zzbs.zzei();
        r17 = r1.currentTimeMillis();
        r14 = new com.google.android.gms.internal.zzail(((java.lang.Long) com.google.android.gms.ads.internal.zzbs.zzep().zzd(com.google.android.gms.internal.zzmq.zzbgu)).longValue());
        r10 = ((java.lang.Long) com.google.android.gms.ads.internal.zzbs.zzep().zzd(com.google.android.gms.internal.zzmq.zzbgt)).longValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x02c5, code lost:
        r19 = r4;
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:?, code lost:
        r20 = r5.read(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x02cc, code lost:
        if (r20 < 0) goto L_0x03ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x02ce, code lost:
        r4 = r4 + r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x02d0, code lost:
        if (r4 <= r6) goto L_0x030e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x02d2, code lost:
        r1 = "sizeExceeded";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:?, code lost:
        r3 = java.lang.String.valueOf(java.lang.Integer.toString(r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x02e2, code lost:
        if (r3.length() == 0) goto L_0x02ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x02e4, code lost:
        r10 = "File too big for full file cache. Size: ".concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x02ef, code lost:
        r10 = new java.lang.String("File too big for full file cache. Size: ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x02f7, code lost:
        throw new java.io.IOException("stream cache file size limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x02f8, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x02f9, code lost:
        r2 = r1;
        r3 = r10;
        r10 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x02ff, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0300, code lost:
        r2 = r1;
        r10 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x030b, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:?, code lost:
        r2.flip();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x0315, code lost:
        if (r3.write(r2) > 0) goto L_0x0311;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x0317, code lost:
        r2.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0326, code lost:
        if ((r1.currentTimeMillis() - r17) <= (1000 * r10)) goto L_0x0357;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0328, code lost:
        r1 = "downloadTimeout";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:?, code lost:
        r2 = java.lang.Long.toString(r10);
        r4 = new java.lang.StringBuilder(29 + java.lang.String.valueOf(r2).length());
        r4.append("Timeout exceeded. Limit: ");
        r4.append(r2);
        r4.append(" sec");
        r10 = r4.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0356, code lost:
        throw new java.io.IOException("stream cache time limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0357, code lost:
        r25 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x035b, code lost:
        if (r8.zzdhy == false) goto L_0x0367;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x035d, code lost:
        r1 = "externalAbort";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0366, code lost:
        throw new java.io.IOException("abort requested");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x036b, code lost:
        if (r14.tryAcquire() == false) goto L_0x039e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x036d, code lost:
        r26 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x0377, code lost:
        r21 = r25;
        r22 = r2;
        r23 = r3;
        r29 = r19;
        r19 = r4;
        r20 = r5;
        r24 = r6;
        r25 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:?, code lost:
        com.google.android.gms.internal.zzais.zzdbs.post(new com.google.android.gms.internal.zzalu(r2, r9, r12.getAbsolutePath(), r19, r7, false));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x039c, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x039e, code lost:
        r22 = r2;
        r23 = r3;
        r20 = r5;
        r24 = r6;
        r26 = r10;
        r29 = r19;
        r21 = r25;
        r19 = r4;
        r25 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x03b0, code lost:
        r4 = r19;
        r5 = r20;
        r1 = r21;
        r2 = r22;
        r3 = r23;
        r6 = r24;
        r7 = r25;
        r10 = r26;
        r19 = r29;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x03c4, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x03c5, code lost:
        r29 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x03c7, code lost:
        r1 = r0;
        r2 = "error";
        r10 = r29;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x03ce, code lost:
        r1 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x03d8, code lost:
        if (com.google.android.gms.internal.zzafj.zzae(3) == false) goto L_0x0411;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x03da, code lost:
        r2 = com.google.android.gms.internal.zzalz.zzdhw.format((long) r4);
        r5 = new java.lang.StringBuilder((22 + java.lang.String.valueOf(r2).length()) + java.lang.String.valueOf(r31).length());
        r5.append("Preloaded ");
        r5.append(r2);
        r5.append(" bytes from ");
        r5.append(r9);
        com.google.android.gms.internal.zzafj.zzbw(r5.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x0411, code lost:
        r12.setReadable(true, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x041a, code lost:
        if (r13.isFile() == false) goto L_0x0424;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x041c, code lost:
        r13.setLastModified(java.lang.System.currentTimeMillis());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:?, code lost:
        r13.createNewFile();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:?, code lost:
        zza(r9, r12.getAbsolutePath(), r4);
        com.google.android.gms.internal.zzalz.zzdhv.remove(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x0433, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x0435, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x0437, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x0438, code lost:
        r1 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x043b, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x043c, code lost:
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x043d, code lost:
        r10 = r1;
        r2 = "error";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x0440, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x0443, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x0444, code lost:
        r1 = r0;
        r2 = "error";
        r3 = null;
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x044b, code lost:
        if ((r1 instanceof java.lang.RuntimeException) != false) goto L_0x044d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x044d, code lost:
        com.google.android.gms.ads.internal.zzbs.zzeg().zza(r1, "VideoStreamFullFileCache.preload");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x045b, code lost:
        if (r8.zzdhy == false) goto L_0x0482;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x045d, code lost:
        r4 = new java.lang.StringBuilder(26 + java.lang.String.valueOf(r31).length());
        r4.append("Preload aborted for URL \"");
        r4.append(r9);
        r4.append("\"");
        com.google.android.gms.internal.zzafj.zzcn(r4.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x0482, code lost:
        r5 = new java.lang.StringBuilder(25 + java.lang.String.valueOf(r31).length());
        r5.append("Preload failed for URL \"");
        r5.append(r9);
        r5.append("\"");
        com.google.android.gms.internal.zzafj.zzc(r5.toString(), r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x04b2, code lost:
        r4 = java.lang.String.valueOf(r12.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x04c0, code lost:
        if (r4.length() == 0) goto L_0x04c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x04c2, code lost:
        r1 = "Could not delete partial cache file at ".concat(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x04c7, code lost:
        r1 = new java.lang.String("Could not delete partial cache file at ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:209:0x04cd, code lost:
        com.google.android.gms.internal.zzafj.zzco(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:210:0x04d0, code lost:
        zza(r9, r12.getAbsolutePath(), r2, r3);
        com.google.android.gms.internal.zzalz.zzdhv.remove(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x04dd, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        com.google.android.gms.ads.internal.zzbs.zzeq();
        r1 = com.google.android.gms.internal.zzajz.zzb(r9, ((java.lang.Integer) com.google.android.gms.ads.internal.zzbs.zzep().zzd(com.google.android.gms.internal.zzmq.zzbgv)).intValue());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x014f, code lost:
        if ((r1 instanceof java.net.HttpURLConnection) == false) goto L_0x01b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        r2 = r1.getResponseCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x015a, code lost:
        if (r2 < 400) goto L_0x01b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        r4 = java.lang.String.valueOf(java.lang.Integer.toString(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x016c, code lost:
        if (r4.length() == 0) goto L_0x0173;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x016e, code lost:
        r3 = "HTTP request failed. Code: ".concat(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0178, code lost:
        r3 = new java.lang.String("HTTP request failed. Code: ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
        r6 = new java.lang.StringBuilder(32 + java.lang.String.valueOf(r31).length());
        r6.append("HTTP status code ");
        r6.append(r2);
        r6.append(" at ");
        r6.append(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01a2, code lost:
        throw new java.io.IOException(r6.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01a3, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01a4, code lost:
        r2 = "badUrl";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01a6, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01a7, code lost:
        r2 = "badUrl";
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01a9, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01ac, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01ad, code lost:
        r1 = r0;
        r3 = null;
        r2 = "error";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        r7 = r1.getContentLength();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01b7, code lost:
        if (r7 >= 0) goto L_0x01e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        r2 = java.lang.String.valueOf(r31);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01c3, code lost:
        if (r2.length() == 0) goto L_0x01ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01c5, code lost:
        r1 = "Stream cache aborted, missing content-length header at ".concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01ca, code lost:
        r1 = new java.lang.String("Stream cache aborted, missing content-length header at ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01d0, code lost:
        com.google.android.gms.internal.zzafj.zzco(r1);
        zza(r9, r12.getAbsolutePath(), "contentLengthMissing", null);
        com.google.android.gms.internal.zzalz.zzdhv.remove(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01e1, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:?, code lost:
        r2 = com.google.android.gms.internal.zzalz.zzdhw.format((long) r7);
        r6 = ((java.lang.Integer) com.google.android.gms.ads.internal.zzbs.zzep().zzd(com.google.android.gms.internal.zzmq.zzbgr)).intValue();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:179:0x0427 */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x044d  */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x045d  */
    /* JADX WARNING: Removed duplicated region for block: B:200:0x0482  */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x04c2  */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x04c7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zzcr(java.lang.String r31) {
        /*
            r30 = this;
            r8 = r30
            r9 = r31
            java.io.File r1 = r8.zzdhx
            r10 = 0
            r11 = 0
            if (r1 != 0) goto L_0x0010
            java.lang.String r1 = "noCacheDir"
        L_0x000c:
            r8.zza(r9, r10, r1, r10)
            return r11
        L_0x0010:
            java.io.File r1 = r8.zzdhx
            if (r1 != 0) goto L_0x0016
            r4 = r11
            goto L_0x0034
        L_0x0016:
            java.io.File r1 = r8.zzdhx
            java.io.File[] r1 = r1.listFiles()
            int r2 = r1.length
            r3 = r11
            r4 = r3
        L_0x001f:
            if (r3 >= r2) goto L_0x0034
            r5 = r1[r3]
            java.lang.String r5 = r5.getName()
            java.lang.String r6 = ".done"
            boolean r5 = r5.endsWith(r6)
            if (r5 != 0) goto L_0x0031
            int r4 = r4 + 1
        L_0x0031:
            int r3 = r3 + 1
            goto L_0x001f
        L_0x0034:
            com.google.android.gms.internal.zzmg<java.lang.Integer> r1 = com.google.android.gms.internal.zzmq.zzbgq
            com.google.android.gms.internal.zzmo r2 = com.google.android.gms.ads.internal.zzbs.zzep()
            java.lang.Object r1 = r2.zzd(r1)
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r1 = r1.intValue()
            if (r4 <= r1) goto L_0x0098
            java.io.File r1 = r8.zzdhx
            if (r1 != 0) goto L_0x004c
        L_0x004a:
            r1 = r11
            goto L_0x008d
        L_0x004c:
            r1 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            java.io.File r3 = r8.zzdhx
            java.io.File[] r3 = r3.listFiles()
            int r4 = r3.length
            r5 = r1
            r2 = r10
            r1 = r11
        L_0x005b:
            if (r1 >= r4) goto L_0x0078
            r7 = r3[r1]
            java.lang.String r12 = r7.getName()
            java.lang.String r13 = ".done"
            boolean r12 = r12.endsWith(r13)
            if (r12 != 0) goto L_0x0075
            long r12 = r7.lastModified()
            int r14 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
            if (r14 >= 0) goto L_0x0075
            r2 = r7
            r5 = r12
        L_0x0075:
            int r1 = r1 + 1
            goto L_0x005b
        L_0x0078:
            if (r2 == 0) goto L_0x004a
            boolean r1 = r2.delete()
            java.io.File r2 = r8.zzc(r2)
            boolean r3 = r2.isFile()
            if (r3 == 0) goto L_0x008d
            boolean r2 = r2.delete()
            r1 = r1 & r2
        L_0x008d:
            if (r1 != 0) goto L_0x0010
            java.lang.String r1 = "Unable to expire stream cache"
            com.google.android.gms.internal.zzafj.zzco(r1)
            java.lang.String r1 = "expireFailed"
            goto L_0x000c
        L_0x0098:
            com.google.android.gms.internal.zzjk.zzhx()
            java.lang.String r1 = com.google.android.gms.internal.zzais.zzcl(r31)
            java.io.File r12 = new java.io.File
            java.io.File r2 = r8.zzdhx
            r12.<init>(r2, r1)
            java.io.File r13 = r8.zzc(r12)
            boolean r1 = r12.isFile()
            r14 = 1
            if (r1 == 0) goto L_0x00de
            boolean r1 = r13.isFile()
            if (r1 == 0) goto L_0x00de
            long r1 = r12.length()
            int r1 = (int) r1
            java.lang.String r2 = "Stream cache hit at "
            java.lang.String r3 = java.lang.String.valueOf(r31)
            int r4 = r3.length()
            if (r4 == 0) goto L_0x00cd
            java.lang.String r2 = r2.concat(r3)
            goto L_0x00d3
        L_0x00cd:
            java.lang.String r3 = new java.lang.String
            r3.<init>(r2)
            r2 = r3
        L_0x00d3:
            com.google.android.gms.internal.zzafj.zzbw(r2)
            java.lang.String r2 = r12.getAbsolutePath()
            r8.zza(r9, r2, r1)
            return r14
        L_0x00de:
            java.io.File r1 = r8.zzdhx
            java.lang.String r1 = r1.getAbsolutePath()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            java.lang.String r2 = java.lang.String.valueOf(r31)
            int r3 = r2.length()
            if (r3 == 0) goto L_0x00f8
            java.lang.String r1 = r1.concat(r2)
            r15 = r1
            goto L_0x00fe
        L_0x00f8:
            java.lang.String r2 = new java.lang.String
            r2.<init>(r1)
            r15 = r2
        L_0x00fe:
            java.util.Set<java.lang.String> r1 = com.google.android.gms.internal.zzalz.zzdhv
            monitor-enter(r1)
            java.util.Set<java.lang.String> r2 = com.google.android.gms.internal.zzalz.zzdhv     // Catch:{ all -> 0x04de }
            boolean r2 = r2.contains(r15)     // Catch:{ all -> 0x04de }
            if (r2 == 0) goto L_0x012e
            java.lang.String r2 = "Stream cache already in progress at "
            java.lang.String r3 = java.lang.String.valueOf(r31)     // Catch:{ all -> 0x04de }
            int r4 = r3.length()     // Catch:{ all -> 0x04de }
            if (r4 == 0) goto L_0x011a
            java.lang.String r2 = r2.concat(r3)     // Catch:{ all -> 0x04de }
            goto L_0x0120
        L_0x011a:
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x04de }
            r3.<init>(r2)     // Catch:{ all -> 0x04de }
            r2 = r3
        L_0x0120:
            com.google.android.gms.internal.zzafj.zzco(r2)     // Catch:{ all -> 0x04de }
            java.lang.String r2 = r12.getAbsolutePath()     // Catch:{ all -> 0x04de }
            java.lang.String r3 = "inProgress"
            r8.zza(r9, r2, r3, r10)     // Catch:{ all -> 0x04de }
            monitor-exit(r1)     // Catch:{ all -> 0x04de }
            return r11
        L_0x012e:
            java.util.Set<java.lang.String> r2 = com.google.android.gms.internal.zzalz.zzdhv     // Catch:{ all -> 0x04de }
            r2.add(r15)     // Catch:{ all -> 0x04de }
            monitor-exit(r1)     // Catch:{ all -> 0x04de }
            java.lang.String r16 = "error"
            com.google.android.gms.ads.internal.zzbs.zzeq()     // Catch:{ IOException | RuntimeException -> 0x0443 }
            com.google.android.gms.internal.zzmg<java.lang.Integer> r1 = com.google.android.gms.internal.zzmq.zzbgv     // Catch:{ IOException | RuntimeException -> 0x0443 }
            com.google.android.gms.internal.zzmo r2 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ IOException | RuntimeException -> 0x0443 }
            java.lang.Object r1 = r2.zzd(r1)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ IOException | RuntimeException -> 0x0443 }
            int r1 = r1.intValue()     // Catch:{ IOException | RuntimeException -> 0x0443 }
            java.net.HttpURLConnection r1 = com.google.android.gms.internal.zzajz.zzb(r9, r1)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            boolean r2 = r1 instanceof java.net.HttpURLConnection     // Catch:{ IOException | RuntimeException -> 0x0443 }
            if (r2 == 0) goto L_0x01b3
            r2 = r1
            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ IOException | RuntimeException -> 0x01ac }
            int r2 = r2.getResponseCode()     // Catch:{ IOException | RuntimeException -> 0x01ac }
            r3 = 400(0x190, float:5.6E-43)
            if (r2 < r3) goto L_0x01b3
            java.lang.String r1 = "badUrl"
            java.lang.String r3 = "HTTP request failed. Code: "
            java.lang.String r4 = java.lang.Integer.toString(r2)     // Catch:{ IOException | RuntimeException -> 0x01a6 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ IOException | RuntimeException -> 0x01a6 }
            int r5 = r4.length()     // Catch:{ IOException | RuntimeException -> 0x01a6 }
            if (r5 == 0) goto L_0x0173
            java.lang.String r3 = r3.concat(r4)     // Catch:{ IOException | RuntimeException -> 0x01a6 }
            goto L_0x0179
        L_0x0173:
            java.lang.String r4 = new java.lang.String     // Catch:{ IOException | RuntimeException -> 0x01a6 }
            r4.<init>(r3)     // Catch:{ IOException | RuntimeException -> 0x01a6 }
            r3 = r4
        L_0x0179:
            java.io.IOException r4 = new java.io.IOException     // Catch:{ IOException | RuntimeException -> 0x01a3 }
            r5 = 32
            java.lang.String r6 = java.lang.String.valueOf(r31)     // Catch:{ IOException | RuntimeException -> 0x01a3 }
            int r6 = r6.length()     // Catch:{ IOException | RuntimeException -> 0x01a3 }
            int r5 = r5 + r6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IOException | RuntimeException -> 0x01a3 }
            r6.<init>(r5)     // Catch:{ IOException | RuntimeException -> 0x01a3 }
            java.lang.String r5 = "HTTP status code "
            r6.append(r5)     // Catch:{ IOException | RuntimeException -> 0x01a3 }
            r6.append(r2)     // Catch:{ IOException | RuntimeException -> 0x01a3 }
            java.lang.String r2 = " at "
            r6.append(r2)     // Catch:{ IOException | RuntimeException -> 0x01a3 }
            r6.append(r9)     // Catch:{ IOException | RuntimeException -> 0x01a3 }
            java.lang.String r2 = r6.toString()     // Catch:{ IOException | RuntimeException -> 0x01a3 }
            r4.<init>(r2)     // Catch:{ IOException | RuntimeException -> 0x01a3 }
            throw r4     // Catch:{ IOException | RuntimeException -> 0x01a3 }
        L_0x01a3:
            r0 = move-exception
            r2 = r1
            goto L_0x01a9
        L_0x01a6:
            r0 = move-exception
            r2 = r1
            r3 = r10
        L_0x01a9:
            r1 = r0
            goto L_0x0449
        L_0x01ac:
            r0 = move-exception
            r1 = r0
            r3 = r10
            r2 = r16
            goto L_0x0449
        L_0x01b3:
            int r7 = r1.getContentLength()     // Catch:{ IOException | RuntimeException -> 0x0443 }
            if (r7 >= 0) goto L_0x01e2
            java.lang.String r1 = "Stream cache aborted, missing content-length header at "
            java.lang.String r2 = java.lang.String.valueOf(r31)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            int r3 = r2.length()     // Catch:{ IOException | RuntimeException -> 0x01ac }
            if (r3 == 0) goto L_0x01ca
            java.lang.String r1 = r1.concat(r2)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            goto L_0x01d0
        L_0x01ca:
            java.lang.String r2 = new java.lang.String     // Catch:{ IOException | RuntimeException -> 0x01ac }
            r2.<init>(r1)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            r1 = r2
        L_0x01d0:
            com.google.android.gms.internal.zzafj.zzco(r1)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            java.lang.String r1 = r12.getAbsolutePath()     // Catch:{ IOException | RuntimeException -> 0x01ac }
            java.lang.String r2 = "contentLengthMissing"
            r8.zza(r9, r1, r2, r10)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            java.util.Set<java.lang.String> r1 = com.google.android.gms.internal.zzalz.zzdhv     // Catch:{ IOException | RuntimeException -> 0x01ac }
            r1.remove(r15)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            return r11
        L_0x01e2:
            java.text.DecimalFormat r2 = com.google.android.gms.internal.zzalz.zzdhw     // Catch:{ IOException | RuntimeException -> 0x0443 }
            long r3 = (long) r7     // Catch:{ IOException | RuntimeException -> 0x0443 }
            java.lang.String r2 = r2.format(r3)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            com.google.android.gms.internal.zzmg<java.lang.Integer> r3 = com.google.android.gms.internal.zzmq.zzbgr     // Catch:{ IOException | RuntimeException -> 0x0443 }
            com.google.android.gms.internal.zzmo r4 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ IOException | RuntimeException -> 0x0443 }
            java.lang.Object r3 = r4.zzd(r3)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ IOException | RuntimeException -> 0x0443 }
            int r6 = r3.intValue()     // Catch:{ IOException | RuntimeException -> 0x0443 }
            if (r7 <= r6) goto L_0x0251
            r1 = 33
            java.lang.String r3 = java.lang.String.valueOf(r2)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            int r3 = r3.length()     // Catch:{ IOException | RuntimeException -> 0x01ac }
            int r1 = r1 + r3
            java.lang.String r3 = java.lang.String.valueOf(r31)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            int r3 = r3.length()     // Catch:{ IOException | RuntimeException -> 0x01ac }
            int r1 = r1 + r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException | RuntimeException -> 0x01ac }
            r3.<init>(r1)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            java.lang.String r1 = "Content length "
            r3.append(r1)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            r3.append(r2)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            java.lang.String r1 = " exceeds limit at "
            r3.append(r1)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            r3.append(r9)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            java.lang.String r1 = r3.toString()     // Catch:{ IOException | RuntimeException -> 0x01ac }
            com.google.android.gms.internal.zzafj.zzco(r1)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            java.lang.String r1 = "File too big for full file cache. Size: "
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            int r3 = r2.length()     // Catch:{ IOException | RuntimeException -> 0x01ac }
            if (r3 == 0) goto L_0x023c
            java.lang.String r1 = r1.concat(r2)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            goto L_0x0242
        L_0x023c:
            java.lang.String r2 = new java.lang.String     // Catch:{ IOException | RuntimeException -> 0x01ac }
            r2.<init>(r1)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            r1 = r2
        L_0x0242:
            java.lang.String r2 = r12.getAbsolutePath()     // Catch:{ IOException | RuntimeException -> 0x01ac }
            java.lang.String r3 = "sizeExceeded"
            r8.zza(r9, r2, r3, r1)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            java.util.Set<java.lang.String> r1 = com.google.android.gms.internal.zzalz.zzdhv     // Catch:{ IOException | RuntimeException -> 0x01ac }
            r1.remove(r15)     // Catch:{ IOException | RuntimeException -> 0x01ac }
            return r11
        L_0x0251:
            r3 = 20
            java.lang.String r4 = java.lang.String.valueOf(r2)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            int r4 = r4.length()     // Catch:{ IOException | RuntimeException -> 0x0443 }
            int r3 = r3 + r4
            java.lang.String r4 = java.lang.String.valueOf(r31)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            int r4 = r4.length()     // Catch:{ IOException | RuntimeException -> 0x0443 }
            int r3 = r3 + r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException | RuntimeException -> 0x0443 }
            r4.<init>(r3)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            java.lang.String r3 = "Caching "
            r4.append(r3)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            r4.append(r2)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            java.lang.String r2 = " bytes from "
            r4.append(r2)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            r4.append(r9)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            java.lang.String r2 = r4.toString()     // Catch:{ IOException | RuntimeException -> 0x0443 }
            com.google.android.gms.internal.zzafj.zzbw(r2)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ IOException | RuntimeException -> 0x0443 }
            java.nio.channels.ReadableByteChannel r5 = java.nio.channels.Channels.newChannel(r1)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ IOException | RuntimeException -> 0x0443 }
            r4.<init>(r12)     // Catch:{ IOException | RuntimeException -> 0x0443 }
            java.nio.channels.FileChannel r3 = r4.getChannel()     // Catch:{ IOException | RuntimeException -> 0x043b }
            r1 = 1048576(0x100000, float:1.469368E-39)
            java.nio.ByteBuffer r2 = java.nio.ByteBuffer.allocate(r1)     // Catch:{ IOException | RuntimeException -> 0x043b }
            com.google.android.gms.common.util.zzd r1 = com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ IOException | RuntimeException -> 0x043b }
            long r17 = r1.currentTimeMillis()     // Catch:{ IOException | RuntimeException -> 0x043b }
            com.google.android.gms.internal.zzmg<java.lang.Long> r10 = com.google.android.gms.internal.zzmq.zzbgu     // Catch:{ IOException | RuntimeException -> 0x043b }
            com.google.android.gms.internal.zzmo r11 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ IOException | RuntimeException -> 0x043b }
            java.lang.Object r10 = r11.zzd(r10)     // Catch:{ IOException | RuntimeException -> 0x043b }
            java.lang.Long r10 = (java.lang.Long) r10     // Catch:{ IOException | RuntimeException -> 0x043b }
            long r10 = r10.longValue()     // Catch:{ IOException | RuntimeException -> 0x043b }
            com.google.android.gms.internal.zzail r14 = new com.google.android.gms.internal.zzail     // Catch:{ IOException | RuntimeException -> 0x043b }
            r14.<init>(r10)     // Catch:{ IOException | RuntimeException -> 0x043b }
            com.google.android.gms.internal.zzmg<java.lang.Long> r10 = com.google.android.gms.internal.zzmq.zzbgt     // Catch:{ IOException | RuntimeException -> 0x043b }
            com.google.android.gms.internal.zzmo r11 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ IOException | RuntimeException -> 0x043b }
            java.lang.Object r10 = r11.zzd(r10)     // Catch:{ IOException | RuntimeException -> 0x043b }
            java.lang.Long r10 = (java.lang.Long) r10     // Catch:{ IOException | RuntimeException -> 0x043b }
            long r10 = r10.longValue()     // Catch:{ IOException | RuntimeException -> 0x043b }
            r19 = r4
            r4 = 0
        L_0x02c8:
            int r20 = r5.read(r2)     // Catch:{ IOException | RuntimeException -> 0x0437 }
            if (r20 < 0) goto L_0x03ce
            int r4 = r4 + r20
            if (r4 <= r6) goto L_0x030e
            java.lang.String r1 = "sizeExceeded"
            java.lang.String r2 = "File too big for full file cache. Size: "
            java.lang.String r3 = java.lang.Integer.toString(r4)     // Catch:{ IOException | RuntimeException -> 0x02ff }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ IOException | RuntimeException -> 0x02ff }
            int r4 = r3.length()     // Catch:{ IOException | RuntimeException -> 0x02ff }
            if (r4 == 0) goto L_0x02ea
            java.lang.String r2 = r2.concat(r3)     // Catch:{ IOException | RuntimeException -> 0x02ff }
            r10 = r2
            goto L_0x02f0
        L_0x02ea:
            java.lang.String r3 = new java.lang.String     // Catch:{ IOException | RuntimeException -> 0x02ff }
            r3.<init>(r2)     // Catch:{ IOException | RuntimeException -> 0x02ff }
            r10 = r3
        L_0x02f0:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException | RuntimeException -> 0x02f8 }
            java.lang.String r3 = "stream cache file size limit exceeded"
            r2.<init>(r3)     // Catch:{ IOException | RuntimeException -> 0x02f8 }
            throw r2     // Catch:{ IOException | RuntimeException -> 0x02f8 }
        L_0x02f8:
            r0 = move-exception
            r2 = r1
            r3 = r10
            r10 = r19
            goto L_0x01a9
        L_0x02ff:
            r0 = move-exception
            r2 = r1
            r10 = r19
            goto L_0x0440
        L_0x0305:
            r0 = move-exception
            r1 = r0
            r2 = r16
            r10 = r19
        L_0x030b:
            r3 = 0
            goto L_0x0449
        L_0x030e:
            r2.flip()     // Catch:{ IOException | RuntimeException -> 0x03c4 }
        L_0x0311:
            int r20 = r3.write(r2)     // Catch:{ IOException | RuntimeException -> 0x03c4 }
            if (r20 > 0) goto L_0x0311
            r2.clear()     // Catch:{ IOException | RuntimeException -> 0x03c4 }
            long r20 = r1.currentTimeMillis()     // Catch:{ IOException | RuntimeException -> 0x03c4 }
            long r22 = r20 - r17
            r20 = 1000(0x3e8, double:4.94E-321)
            long r20 = r20 * r10
            int r24 = (r22 > r20 ? 1 : (r22 == r20 ? 0 : -1))
            if (r24 <= 0) goto L_0x0357
            java.lang.String r1 = "downloadTimeout"
            java.lang.String r2 = java.lang.Long.toString(r10)     // Catch:{ IOException | RuntimeException -> 0x02ff }
            r3 = 29
            java.lang.String r4 = java.lang.String.valueOf(r2)     // Catch:{ IOException | RuntimeException -> 0x02ff }
            int r4 = r4.length()     // Catch:{ IOException | RuntimeException -> 0x02ff }
            int r3 = r3 + r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException | RuntimeException -> 0x02ff }
            r4.<init>(r3)     // Catch:{ IOException | RuntimeException -> 0x02ff }
            java.lang.String r3 = "Timeout exceeded. Limit: "
            r4.append(r3)     // Catch:{ IOException | RuntimeException -> 0x02ff }
            r4.append(r2)     // Catch:{ IOException | RuntimeException -> 0x02ff }
            java.lang.String r2 = " sec"
            r4.append(r2)     // Catch:{ IOException | RuntimeException -> 0x02ff }
            java.lang.String r10 = r4.toString()     // Catch:{ IOException | RuntimeException -> 0x02ff }
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException | RuntimeException -> 0x02f8 }
            java.lang.String r3 = "stream cache time limit exceeded"
            r2.<init>(r3)     // Catch:{ IOException | RuntimeException -> 0x02f8 }
            throw r2     // Catch:{ IOException | RuntimeException -> 0x02f8 }
        L_0x0357:
            r25 = r1
            boolean r1 = r8.zzdhy     // Catch:{ IOException | RuntimeException -> 0x03c4 }
            if (r1 == 0) goto L_0x0367
            java.lang.String r1 = "externalAbort"
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException | RuntimeException -> 0x02ff }
            java.lang.String r3 = "abort requested"
            r2.<init>(r3)     // Catch:{ IOException | RuntimeException -> 0x02ff }
            throw r2     // Catch:{ IOException | RuntimeException -> 0x02ff }
        L_0x0367:
            boolean r1 = r14.tryAcquire()     // Catch:{ IOException | RuntimeException -> 0x03c4 }
            if (r1 == 0) goto L_0x039e
            java.lang.String r20 = r12.getAbsolutePath()     // Catch:{ IOException | RuntimeException -> 0x03c4 }
            android.os.Handler r1 = com.google.android.gms.internal.zzais.zzdbs     // Catch:{ IOException | RuntimeException -> 0x03c4 }
            r26 = r10
            com.google.android.gms.internal.zzalu r10 = new com.google.android.gms.internal.zzalu     // Catch:{ IOException | RuntimeException -> 0x03c4 }
            r11 = 0
            r28 = r1
            r21 = r25
            r1 = r10
            r22 = r2
            r2 = r8
            r23 = r3
            r3 = r9
            r29 = r19
            r19 = r4
            r4 = r20
            r20 = r5
            r5 = r19
            r24 = r6
            r6 = r7
            r25 = r7
            r7 = r11
            r1.<init>(r2, r3, r4, r5, r6, r7)     // Catch:{ IOException | RuntimeException -> 0x039c }
            r1 = r28
            r1.post(r10)     // Catch:{ IOException | RuntimeException -> 0x039c }
            goto L_0x03b0
        L_0x039c:
            r0 = move-exception
            goto L_0x03c7
        L_0x039e:
            r22 = r2
            r23 = r3
            r20 = r5
            r24 = r6
            r26 = r10
            r29 = r19
            r21 = r25
            r19 = r4
            r25 = r7
        L_0x03b0:
            r4 = r19
            r5 = r20
            r1 = r21
            r2 = r22
            r3 = r23
            r6 = r24
            r7 = r25
            r10 = r26
            r19 = r29
            goto L_0x02c8
        L_0x03c4:
            r0 = move-exception
            r29 = r19
        L_0x03c7:
            r1 = r0
            r2 = r16
            r10 = r29
            goto L_0x030b
        L_0x03ce:
            r1 = r19
            r1.close()     // Catch:{ IOException | RuntimeException -> 0x0435 }
            r2 = 3
            boolean r2 = com.google.android.gms.internal.zzafj.zzae(r2)     // Catch:{ IOException | RuntimeException -> 0x0435 }
            if (r2 == 0) goto L_0x0411
            java.text.DecimalFormat r2 = com.google.android.gms.internal.zzalz.zzdhw     // Catch:{ IOException | RuntimeException -> 0x0435 }
            long r5 = (long) r4     // Catch:{ IOException | RuntimeException -> 0x0435 }
            java.lang.String r2 = r2.format(r5)     // Catch:{ IOException | RuntimeException -> 0x0435 }
            r3 = 22
            java.lang.String r5 = java.lang.String.valueOf(r2)     // Catch:{ IOException | RuntimeException -> 0x0435 }
            int r5 = r5.length()     // Catch:{ IOException | RuntimeException -> 0x0435 }
            int r3 = r3 + r5
            java.lang.String r5 = java.lang.String.valueOf(r31)     // Catch:{ IOException | RuntimeException -> 0x0435 }
            int r5 = r5.length()     // Catch:{ IOException | RuntimeException -> 0x0435 }
            int r3 = r3 + r5
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException | RuntimeException -> 0x0435 }
            r5.<init>(r3)     // Catch:{ IOException | RuntimeException -> 0x0435 }
            java.lang.String r3 = "Preloaded "
            r5.append(r3)     // Catch:{ IOException | RuntimeException -> 0x0435 }
            r5.append(r2)     // Catch:{ IOException | RuntimeException -> 0x0435 }
            java.lang.String r2 = " bytes from "
            r5.append(r2)     // Catch:{ IOException | RuntimeException -> 0x0435 }
            r5.append(r9)     // Catch:{ IOException | RuntimeException -> 0x0435 }
            java.lang.String r2 = r5.toString()     // Catch:{ IOException | RuntimeException -> 0x0435 }
            com.google.android.gms.internal.zzafj.zzbw(r2)     // Catch:{ IOException | RuntimeException -> 0x0435 }
        L_0x0411:
            r2 = 0
            r3 = 1
            r12.setReadable(r3, r2)     // Catch:{ IOException | RuntimeException -> 0x0435 }
            boolean r2 = r13.isFile()     // Catch:{ IOException | RuntimeException -> 0x0435 }
            if (r2 == 0) goto L_0x0424
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ IOException | RuntimeException -> 0x0435 }
            r13.setLastModified(r2)     // Catch:{ IOException | RuntimeException -> 0x0435 }
            goto L_0x0427
        L_0x0424:
            r13.createNewFile()     // Catch:{ IOException -> 0x0427 }
        L_0x0427:
            java.lang.String r2 = r12.getAbsolutePath()     // Catch:{ IOException | RuntimeException -> 0x0435 }
            r8.zza(r9, r2, r4)     // Catch:{ IOException | RuntimeException -> 0x0435 }
            java.util.Set<java.lang.String> r2 = com.google.android.gms.internal.zzalz.zzdhv     // Catch:{ IOException | RuntimeException -> 0x0435 }
            r2.remove(r15)     // Catch:{ IOException | RuntimeException -> 0x0435 }
            r2 = 1
            return r2
        L_0x0435:
            r0 = move-exception
            goto L_0x043d
        L_0x0437:
            r0 = move-exception
            r1 = r19
            goto L_0x043d
        L_0x043b:
            r0 = move-exception
            r1 = r4
        L_0x043d:
            r10 = r1
            r2 = r16
        L_0x0440:
            r3 = 0
            goto L_0x01a9
        L_0x0443:
            r0 = move-exception
            r1 = r0
            r2 = r16
            r3 = 0
            r10 = 0
        L_0x0449:
            boolean r4 = r1 instanceof java.lang.RuntimeException
            if (r4 == 0) goto L_0x0456
            com.google.android.gms.internal.zzaez r4 = com.google.android.gms.ads.internal.zzbs.zzeg()
            java.lang.String r5 = "VideoStreamFullFileCache.preload"
            r4.zza(r1, r5)
        L_0x0456:
            r10.close()     // Catch:{ IOException | NullPointerException -> 0x0459 }
        L_0x0459:
            boolean r4 = r8.zzdhy
            if (r4 == 0) goto L_0x0482
            r1 = 26
            java.lang.String r4 = java.lang.String.valueOf(r31)
            int r4 = r4.length()
            int r1 = r1 + r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>(r1)
            java.lang.String r1 = "Preload aborted for URL \""
            r4.append(r1)
            r4.append(r9)
            java.lang.String r1 = "\""
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            com.google.android.gms.internal.zzafj.zzcn(r1)
            goto L_0x04a6
        L_0x0482:
            r4 = 25
            java.lang.String r5 = java.lang.String.valueOf(r31)
            int r5 = r5.length()
            int r4 = r4 + r5
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>(r4)
            java.lang.String r4 = "Preload failed for URL \""
            r5.append(r4)
            r5.append(r9)
            java.lang.String r4 = "\""
            r5.append(r4)
            java.lang.String r4 = r5.toString()
            com.google.android.gms.internal.zzafj.zzc(r4, r1)
        L_0x04a6:
            boolean r1 = r12.exists()
            if (r1 == 0) goto L_0x04d0
            boolean r1 = r12.delete()
            if (r1 != 0) goto L_0x04d0
            java.lang.String r1 = "Could not delete partial cache file at "
            java.lang.String r4 = r12.getAbsolutePath()
            java.lang.String r4 = java.lang.String.valueOf(r4)
            int r5 = r4.length()
            if (r5 == 0) goto L_0x04c7
            java.lang.String r1 = r1.concat(r4)
            goto L_0x04cd
        L_0x04c7:
            java.lang.String r4 = new java.lang.String
            r4.<init>(r1)
            r1 = r4
        L_0x04cd:
            com.google.android.gms.internal.zzafj.zzco(r1)
        L_0x04d0:
            java.lang.String r1 = r12.getAbsolutePath()
            r8.zza(r9, r1, r2, r3)
            java.util.Set<java.lang.String> r1 = com.google.android.gms.internal.zzalz.zzdhv
            r1.remove(r15)
            r1 = 0
            return r1
        L_0x04de:
            r0 = move-exception
            r2 = r0
            monitor-exit(r1)     // Catch:{ all -> 0x04de }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzalz.zzcr(java.lang.String):boolean");
    }
}
