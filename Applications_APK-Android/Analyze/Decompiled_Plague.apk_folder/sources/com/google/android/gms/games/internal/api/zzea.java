package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.games.video.Videos;

final class zzea implements Videos.CaptureCapabilitiesResult {
    private /* synthetic */ Status zzekv;

    zzea(zzdz zzdz, Status status) {
        this.zzekv = status;
    }

    public final VideoCapabilities getCapabilities() {
        return null;
    }

    public final Status getStatus() {
        return this.zzekv;
    }
}
