package com.google.android.gms;

public final class R {

    public static final class attr {
        public static final int adSize = 2130837504;
        public static final int adSizes = 2130837505;
        public static final int adUnitId = 2130837506;
        public static final int buttonSize = 2130837507;
        public static final int circleCrop = 2130837508;
        public static final int colorScheme = 2130837509;
        public static final int imageAspectRatio = 2130837524;
        public static final int imageAspectRatioAdjust = 2130837525;
        public static final int scopeUris = 2130837533;
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2130968581;
        public static final int common_google_signin_btn_text_dark_default = 2130968582;
        public static final int common_google_signin_btn_text_dark_disabled = 2130968583;
        public static final int common_google_signin_btn_text_dark_focused = 2130968584;
        public static final int common_google_signin_btn_text_dark_pressed = 2130968585;
        public static final int common_google_signin_btn_text_light = 2130968586;
        public static final int common_google_signin_btn_text_light_default = 2130968587;
        public static final int common_google_signin_btn_text_light_disabled = 2130968588;
        public static final int common_google_signin_btn_text_light_focused = 2130968589;
        public static final int common_google_signin_btn_text_light_pressed = 2130968590;
        public static final int common_google_signin_btn_tint = 2130968591;
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131099685;
        public static final int common_google_signin_btn_icon_dark = 2131099686;
        public static final int common_google_signin_btn_icon_dark_focused = 2131099687;
        public static final int common_google_signin_btn_icon_dark_normal = 2131099688;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131099689;
        public static final int common_google_signin_btn_icon_disabled = 2131099690;
        public static final int common_google_signin_btn_icon_light = 2131099691;
        public static final int common_google_signin_btn_icon_light_focused = 2131099692;
        public static final int common_google_signin_btn_icon_light_normal = 2131099693;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131099694;
        public static final int common_google_signin_btn_text_dark = 2131099695;
        public static final int common_google_signin_btn_text_dark_focused = 2131099696;
        public static final int common_google_signin_btn_text_dark_normal = 2131099697;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131099698;
        public static final int common_google_signin_btn_text_disabled = 2131099699;
        public static final int common_google_signin_btn_text_light = 2131099700;
        public static final int common_google_signin_btn_text_light_focused = 2131099701;
        public static final int common_google_signin_btn_text_light_normal = 2131099702;
        public static final int common_google_signin_btn_text_light_normal_background = 2131099703;
        public static final int googleg_disabled_color_18 = 2131099709;
        public static final int googleg_standard_color_18 = 2131099710;
    }

    public static final class id {
        public static final int adjust_height = 2131165193;
        public static final int adjust_width = 2131165194;
        public static final int auto = 2131165196;
        public static final int dark = 2131165221;
        public static final int icon_only = 2131165226;
        public static final int light = 2131165230;
        public static final int none = 2131165274;
        public static final int normal = 2131165275;
        public static final int standard = 2131165289;
        public static final int text = 2131165291;
        public static final int text2 = 2131165292;
        public static final int wide = 2131165297;
    }

    public static final class integer {
        public static final int google_play_services_version = 2131230721;
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131427365;
        public static final int common_google_play_services_enable_text = 2131427366;
        public static final int common_google_play_services_enable_title = 2131427367;
        public static final int common_google_play_services_install_button = 2131427368;
        public static final int common_google_play_services_install_text = 2131427369;
        public static final int common_google_play_services_install_title = 2131427370;
        public static final int common_google_play_services_notification_ticker = 2131427371;
        public static final int common_google_play_services_unknown_issue = 2131427372;
        public static final int common_google_play_services_unsupported_text = 2131427373;
        public static final int common_google_play_services_update_button = 2131427374;
        public static final int common_google_play_services_update_text = 2131427375;
        public static final int common_google_play_services_update_title = 2131427376;
        public static final int common_google_play_services_updating_text = 2131427377;
        public static final int common_google_play_services_wear_update_text = 2131427378;
        public static final int common_open_on_phone = 2131427379;
        public static final int common_signin_button_text = 2131427380;
        public static final int common_signin_button_text_long = 2131427381;
        public static final int s1 = 2131427415;
        public static final int s2 = 2131427416;
        public static final int s3 = 2131427417;
        public static final int s4 = 2131427418;
        public static final int s5 = 2131427419;
        public static final int s6 = 2131427420;
        public static final int s7 = 2131427421;
    }

    public static final class style {
        public static final int Theme_IAPTheme = 2131492877;
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.miniclip.plagueinc.R.attr.adSize, com.miniclip.plagueinc.R.attr.adSizes, com.miniclip.plagueinc.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] LoadingImageView = {com.miniclip.plagueinc.R.attr.circleCrop, com.miniclip.plagueinc.R.attr.imageAspectRatio, com.miniclip.plagueinc.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.miniclip.plagueinc.R.attr.buttonSize, com.miniclip.plagueinc.R.attr.colorScheme, com.miniclip.plagueinc.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
