package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.internal.zzan;

public interface zzcwj extends IInterface {
    void zza(zzan zzan, int i, boolean z) throws RemoteException;

    void zza(zzcwm zzcwm, zzcwh zzcwh) throws RemoteException;

    void zzeh(int i) throws RemoteException;
}
