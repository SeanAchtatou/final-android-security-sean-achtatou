package com.google.android.gms.internal;

public abstract class zzjx extends zzee implements zzjw {
    public zzjx() {
        attachInterface(this, "com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r5v2 */
    /* JADX WARN: Type inference failed for: r5v3, types: [com.google.android.gms.internal.zzjq] */
    /* JADX WARN: Type inference failed for: r5v10, types: [com.google.android.gms.internal.zzkm] */
    /* JADX WARN: Type inference failed for: r5v16 */
    /* JADX WARN: Type inference failed for: r5v17 */
    /* JADX WARN: Type inference failed for: r5v18 */
    /* JADX WARN: Type inference failed for: r5v19 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTransact(int r2, android.os.Parcel r3, android.os.Parcel r4, int r5) throws android.os.RemoteException {
        /*
            r1 = this;
            boolean r5 = r1.zza(r2, r3, r4, r5)
            r0 = 1
            if (r5 == 0) goto L_0x0008
            return r0
        L_0x0008:
            r5 = 0
            switch(r2) {
                case 1: goto L_0x00aa;
                case 2: goto L_0x008c;
                case 3: goto L_0x007d;
                case 4: goto L_0x0071;
                case 5: goto L_0x0059;
                case 6: goto L_0x004d;
                case 7: goto L_0x002f;
                case 8: goto L_0x001b;
                case 9: goto L_0x000e;
                default: goto L_0x000c;
            }
        L_0x000c:
            r2 = 0
            return r2
        L_0x000e:
            android.os.Parcelable$Creator<com.google.android.gms.ads.formats.PublisherAdViewOptions> r2 = com.google.android.gms.ads.formats.PublisherAdViewOptions.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzef.zza(r3, r2)
            com.google.android.gms.ads.formats.PublisherAdViewOptions r2 = (com.google.android.gms.ads.formats.PublisherAdViewOptions) r2
            r1.zza(r2)
            goto L_0x0088
        L_0x001b:
            android.os.IBinder r2 = r3.readStrongBinder()
            com.google.android.gms.internal.zzqk r2 = com.google.android.gms.internal.zzql.zzs(r2)
            android.os.Parcelable$Creator<com.google.android.gms.internal.zziw> r5 = com.google.android.gms.internal.zziw.CREATOR
            android.os.Parcelable r3 = com.google.android.gms.internal.zzef.zza(r3, r5)
            com.google.android.gms.internal.zziw r3 = (com.google.android.gms.internal.zziw) r3
            r1.zza(r2, r3)
            goto L_0x0088
        L_0x002f:
            android.os.IBinder r2 = r3.readStrongBinder()
            if (r2 != 0) goto L_0x0036
            goto L_0x0049
        L_0x0036:
            java.lang.String r3 = "com.google.android.gms.ads.internal.client.ICorrelationIdProvider"
            android.os.IInterface r3 = r2.queryLocalInterface(r3)
            boolean r5 = r3 instanceof com.google.android.gms.internal.zzkm
            if (r5 == 0) goto L_0x0044
            r5 = r3
            com.google.android.gms.internal.zzkm r5 = (com.google.android.gms.internal.zzkm) r5
            goto L_0x0049
        L_0x0044:
            com.google.android.gms.internal.zzko r5 = new com.google.android.gms.internal.zzko
            r5.<init>(r2)
        L_0x0049:
            r1.zzb(r5)
            goto L_0x0088
        L_0x004d:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzom> r2 = com.google.android.gms.internal.zzom.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzef.zza(r3, r2)
            com.google.android.gms.internal.zzom r2 = (com.google.android.gms.internal.zzom) r2
            r1.zza(r2)
            goto L_0x0088
        L_0x0059:
            java.lang.String r2 = r3.readString()
            android.os.IBinder r5 = r3.readStrongBinder()
            com.google.android.gms.internal.zzqh r5 = com.google.android.gms.internal.zzqi.zzr(r5)
            android.os.IBinder r3 = r3.readStrongBinder()
            com.google.android.gms.internal.zzqe r3 = com.google.android.gms.internal.zzqf.zzq(r3)
            r1.zza(r2, r5, r3)
            goto L_0x0088
        L_0x0071:
            android.os.IBinder r2 = r3.readStrongBinder()
            com.google.android.gms.internal.zzqb r2 = com.google.android.gms.internal.zzqc.zzp(r2)
            r1.zza(r2)
            goto L_0x0088
        L_0x007d:
            android.os.IBinder r2 = r3.readStrongBinder()
            com.google.android.gms.internal.zzpy r2 = com.google.android.gms.internal.zzpz.zzo(r2)
            r1.zza(r2)
        L_0x0088:
            r4.writeNoException()
            return r0
        L_0x008c:
            android.os.IBinder r2 = r3.readStrongBinder()
            if (r2 != 0) goto L_0x0093
            goto L_0x00a6
        L_0x0093:
            java.lang.String r3 = "com.google.android.gms.ads.internal.client.IAdListener"
            android.os.IInterface r3 = r2.queryLocalInterface(r3)
            boolean r5 = r3 instanceof com.google.android.gms.internal.zzjq
            if (r5 == 0) goto L_0x00a1
            r5 = r3
            com.google.android.gms.internal.zzjq r5 = (com.google.android.gms.internal.zzjq) r5
            goto L_0x00a6
        L_0x00a1:
            com.google.android.gms.internal.zzjs r5 = new com.google.android.gms.internal.zzjs
            r5.<init>(r2)
        L_0x00a6:
            r1.zzb(r5)
            goto L_0x0088
        L_0x00aa:
            com.google.android.gms.internal.zzjt r2 = r1.zzdc()
            r4.writeNoException()
            com.google.android.gms.internal.zzef.zza(r4, r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzjx.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
