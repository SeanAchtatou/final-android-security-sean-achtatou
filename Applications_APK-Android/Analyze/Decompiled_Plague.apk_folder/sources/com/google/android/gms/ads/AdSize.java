package com.google.android.gms.ads;

import android.content.Context;
import com.facebook.widget.ProfilePictureView;
import com.google.android.gms.internal.zzais;
import com.google.android.gms.internal.zziw;
import com.google.android.gms.internal.zzjk;

public final class AdSize {
    public static final int AUTO_HEIGHT = -2;
    public static final AdSize BANNER = new AdSize(320, 50, "320x50_mb");
    public static final AdSize FLUID = new AdSize(-3, -4, "fluid");
    public static final AdSize FULL_BANNER = new AdSize(468, 60, "468x60_as");
    public static final int FULL_WIDTH = -1;
    public static final AdSize LARGE_BANNER = new AdSize(320, 100, "320x100_as");
    public static final AdSize LEADERBOARD = new AdSize(728, 90, "728x90_as");
    public static final AdSize MEDIUM_RECTANGLE = new AdSize(300, 250, "300x250_as");
    public static final AdSize SEARCH = new AdSize(-3, 0, "search_v2");
    public static final AdSize SMART_BANNER = new AdSize(-1, -2, "smart_banner");
    public static final AdSize WIDE_SKYSCRAPER = new AdSize(160, 600, "160x600_as");
    public static final AdSize zzakv = new AdSize(50, 50, "50x50_mb");
    private final int zzakw;
    private final int zzakx;
    private final String zzaky;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AdSize(int r6, int r7) {
        /*
            r5 = this;
            r0 = -1
            if (r6 != r0) goto L_0x0006
            java.lang.String r0 = "FULL"
            goto L_0x000a
        L_0x0006:
            java.lang.String r0 = java.lang.String.valueOf(r6)
        L_0x000a:
            r1 = -2
            if (r7 != r1) goto L_0x0010
            java.lang.String r1 = "AUTO"
            goto L_0x0014
        L_0x0010:
            java.lang.String r1 = java.lang.String.valueOf(r7)
        L_0x0014:
            java.lang.String r2 = "_as"
            r3 = 1
            java.lang.String r4 = java.lang.String.valueOf(r0)
            int r4 = r4.length()
            int r3 = r3 + r4
            java.lang.String r4 = java.lang.String.valueOf(r1)
            int r4 = r4.length()
            int r3 = r3 + r4
            java.lang.String r4 = java.lang.String.valueOf(r2)
            int r4 = r4.length()
            int r3 = r3 + r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>(r3)
            r4.append(r0)
            java.lang.String r0 = "x"
            r4.append(r0)
            r4.append(r1)
            r4.append(r2)
            java.lang.String r0 = r4.toString()
            r5.<init>(r6, r7, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.AdSize.<init>(int, int):void");
    }

    AdSize(int i, int i2, String str) {
        if (i < 0 && i != -1 && i != -3) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Invalid width for AdSize: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 >= 0 || i2 == -2 || i2 == -4) {
            this.zzakw = i;
            this.zzakx = i2;
            this.zzaky = str;
        } else {
            StringBuilder sb2 = new StringBuilder(38);
            sb2.append("Invalid height for AdSize: ");
            sb2.append(i2);
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AdSize)) {
            return false;
        }
        AdSize adSize = (AdSize) obj;
        return this.zzakw == adSize.zzakw && this.zzakx == adSize.zzakx && this.zzaky.equals(adSize.zzaky);
    }

    public final int getHeight() {
        return this.zzakx;
    }

    public final int getHeightInPixels(Context context) {
        switch (this.zzakx) {
            case ProfilePictureView.LARGE:
            case -3:
                return -1;
            case -2:
                return zziw.zzc(context.getResources().getDisplayMetrics());
            default:
                zzjk.zzhx();
                return zzais.zzc(context, this.zzakx);
        }
    }

    public final int getWidth() {
        return this.zzakw;
    }

    public final int getWidthInPixels(Context context) {
        int i = this.zzakw;
        if (i == -1) {
            return zziw.zzb(context.getResources().getDisplayMetrics());
        }
        switch (i) {
            case ProfilePictureView.LARGE:
            case -3:
                return -1;
            default:
                zzjk.zzhx();
                return zzais.zzc(context, this.zzakw);
        }
    }

    public final int hashCode() {
        return this.zzaky.hashCode();
    }

    public final boolean isAutoHeight() {
        return this.zzakx == -2;
    }

    public final boolean isFluid() {
        return this.zzakw == -3 && this.zzakx == -4;
    }

    public final boolean isFullWidth() {
        return this.zzakw == -1;
    }

    public final String toString() {
        return this.zzaky;
    }
}
