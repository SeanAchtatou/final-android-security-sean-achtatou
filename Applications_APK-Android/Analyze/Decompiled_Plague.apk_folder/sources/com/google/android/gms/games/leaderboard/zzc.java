package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.data.DataHolder;

public final class zzc extends com.google.android.gms.common.data.zzc implements LeaderboardVariant {
    zzc(DataHolder dataHolder, int i) {
        super(dataHolder, i);
    }

    public final boolean equals(Object obj) {
        return zzb.zza(this, obj);
    }

    public final /* synthetic */ Object freeze() {
        return new zzb(this);
    }

    public final int getCollection() {
        return getInteger("collection");
    }

    public final String getDisplayPlayerRank() {
        return getString("player_display_rank");
    }

    public final String getDisplayPlayerScore() {
        return getString("player_display_score");
    }

    public final long getNumScores() {
        if (zzfx("total_scores")) {
            return -1;
        }
        return getLong("total_scores");
    }

    public final long getPlayerRank() {
        if (zzfx("player_rank")) {
            return -1;
        }
        return getLong("player_rank");
    }

    public final String getPlayerScoreTag() {
        return getString("player_score_tag");
    }

    public final long getRawPlayerScore() {
        if (zzfx("player_raw_score")) {
            return -1;
        }
        return getLong("player_raw_score");
    }

    public final int getTimeSpan() {
        return getInteger("timespan");
    }

    public final boolean hasPlayerInfo() {
        return !zzfx("player_raw_score");
    }

    public final int hashCode() {
        return zzb.zza(this);
    }

    public final String toString() {
        return zzb.zzb(this);
    }

    public final String zzats() {
        return getString("top_page_token_next");
    }

    public final String zzatt() {
        return getString("window_page_token_prev");
    }

    public final String zzatu() {
        return getString("window_page_token_next");
    }
}
