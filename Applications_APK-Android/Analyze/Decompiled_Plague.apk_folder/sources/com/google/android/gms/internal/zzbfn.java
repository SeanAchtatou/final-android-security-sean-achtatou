package com.google.android.gms.internal;

public abstract class zzbfn extends zzbfk implements zzben {
    public final int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!getClass().isInstance(obj)) {
            return false;
        }
        zzbfk zzbfk = (zzbfk) obj;
        for (zzbfl next : zzaaj().values()) {
            if (zza(next)) {
                if (!zzbfk.zza(next) || !zzb(next).equals(zzbfk.zzb(next))) {
                    return false;
                }
            } else if (zzbfk.zza(next)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        for (zzbfl next : zzaaj().values()) {
            if (zza(next)) {
                i = (i * 31) + zzb(next).hashCode();
            }
        }
        return i;
    }

    public Object zzgj(String str) {
        return null;
    }

    public boolean zzgk(String str) {
        return false;
    }
}
