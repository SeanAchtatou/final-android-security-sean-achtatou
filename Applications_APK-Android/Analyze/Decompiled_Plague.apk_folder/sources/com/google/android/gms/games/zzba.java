package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.games.internal.zzw;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzba extends zzac<Void> {
    private /* synthetic */ byte[] zzhkf;
    private /* synthetic */ String zzhkg;
    private /* synthetic */ String zzhkh;

    zzba(RealTimeMultiplayerClient realTimeMultiplayerClient, byte[] bArr, String str, String str2) {
        this.zzhkf = bArr;
        this.zzhkg = str;
        this.zzhkh = str2;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException {
        if (((zzw) gamesClientImpl.zzakb()).zzb(this.zzhkf, this.zzhkg, new String[]{this.zzhkh}) == 0) {
            taskCompletionSource.setResult(null);
        } else {
            taskCompletionSource.trySetException(zzb.zzy(GamesClientStatusCodes.zzdg(GamesClientStatusCodes.REAL_TIME_MESSAGE_SEND_FAILED)));
        }
    }
}
