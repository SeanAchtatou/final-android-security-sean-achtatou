package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbc extends zzfhe<zzbc> {
    public byte[] zzge = null;
    public byte[][] zzgj = zzfhn.zzphq;
    private Integer zzgk;
    public Integer zzgl;

    public zzbc() {
        this.zzpai = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzd */
    public final zzbc zza(zzfhb zzfhb) throws IOException {
        int i;
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 10) {
                int zzb = zzfhn.zzb(zzfhb, 10);
                int length = this.zzgj == null ? 0 : this.zzgj.length;
                byte[][] bArr = new byte[(zzb + length)][];
                if (length != 0) {
                    System.arraycopy(this.zzgj, 0, bArr, 0, length);
                }
                while (length < bArr.length - 1) {
                    bArr[length] = zzfhb.readBytes();
                    zzfhb.zzcts();
                    length++;
                }
                bArr[length] = zzfhb.readBytes();
                this.zzgj = bArr;
            } else if (zzcts == 18) {
                this.zzge = zzfhb.readBytes();
            } else if (zzcts == 24) {
                i = zzfhb.getPosition();
                int zzcuh = zzfhb.zzcuh();
                switch (zzcuh) {
                    default:
                        StringBuilder sb = new StringBuilder(41);
                        sb.append(zzcuh);
                        sb.append(" is not a valid enum ProtoName");
                        throw new IllegalArgumentException(sb.toString());
                    case 0:
                    case 1:
                        this.zzgk = Integer.valueOf(zzcuh);
                        continue;
                }
            } else if (zzcts == 32) {
                i = zzfhb.getPosition();
                try {
                    int zzcuh2 = zzfhb.zzcuh();
                    switch (zzcuh2) {
                        default:
                            StringBuilder sb2 = new StringBuilder(48);
                            sb2.append(zzcuh2);
                            sb2.append(" is not a valid enum EncryptionMethod");
                            throw new IllegalArgumentException(sb2.toString());
                        case 0:
                        case 1:
                        case 2:
                            this.zzgl = Integer.valueOf(zzcuh2);
                            continue;
                    }
                } catch (IllegalArgumentException unused) {
                    zzfhb.zzlv(i);
                    zza(zzfhb, zzcts);
                }
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzgj != null && this.zzgj.length > 0) {
            for (byte[] bArr : this.zzgj) {
                if (bArr != null) {
                    zzfhc.zzc(1, bArr);
                }
            }
        }
        if (this.zzge != null) {
            zzfhc.zzc(2, this.zzge);
        }
        if (this.zzgk != null) {
            zzfhc.zzaa(3, this.zzgk.intValue());
        }
        if (this.zzgl != null) {
            zzfhc.zzaa(4, this.zzgl.intValue());
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzgj != null && this.zzgj.length > 0) {
            int i = 0;
            int i2 = 0;
            for (byte[] bArr : this.zzgj) {
                if (bArr != null) {
                    i2++;
                    i += zzfhc.zzbf(bArr);
                }
            }
            zzo = zzo + i + (1 * i2);
        }
        if (this.zzge != null) {
            zzo += zzfhc.zzd(2, this.zzge);
        }
        if (this.zzgk != null) {
            zzo += zzfhc.zzad(3, this.zzgk.intValue());
        }
        return this.zzgl != null ? zzo + zzfhc.zzad(4, this.zzgl.intValue()) : zzo;
    }
}
