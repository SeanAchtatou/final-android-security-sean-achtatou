package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdpu;
import com.google.android.gms.internal.zzdre;
import java.io.IOException;

public final class zzdpq extends zzfee<zzdpq, zza> implements zzffk {
    private static volatile zzffm<zzdpq> zzbas;
    /* access modifiers changed from: private */
    public static final zzdpq zzlqe;
    private int zzlqb;
    private zzdpu zzlqc;
    private zzdre zzlqd;

    public static final class zza extends zzfef<zzdpq, zza> implements zzffk {
        private zza() {
            super(zzdpq.zzlqe);
        }

        /* synthetic */ zza(zzdpr zzdpr) {
            this();
        }

        public final zza zzb(zzdpu zzdpu) {
            zzcvi();
            ((zzdpq) this.zzpbv).zza(zzdpu);
            return this;
        }

        public final zza zzb(zzdre zzdre) {
            zzcvi();
            ((zzdpq) this.zzpbv).zza(zzdre);
            return this;
        }

        public final zza zzfi(int i) {
            zzcvi();
            ((zzdpq) this.zzpbv).setVersion(i);
            return this;
        }
    }

    static {
        zzdpq zzdpq = new zzdpq();
        zzlqe = zzdpq;
        zzdpq.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdpq.zzpbs.zzbim();
    }

    private zzdpq() {
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzlqb = i;
    }

    /* access modifiers changed from: private */
    public final void zza(zzdpu zzdpu) {
        if (zzdpu == null) {
            throw new NullPointerException();
        }
        this.zzlqc = zzdpu;
    }

    /* access modifiers changed from: private */
    public final void zza(zzdre zzdre) {
        if (zzdre == null) {
            throw new NullPointerException();
        }
        this.zzlqd = zzdre;
    }

    public static zza zzbln() {
        zzdpq zzdpq = zzlqe;
        zzfef zzfef = (zzfef) zzdpq.zza(zzfem.zzpch, (Object) null, (Object) null);
        zzfef.zza((zzfee) zzdpq);
        return (zza) zzfef;
    }

    public static zzdpq zzh(zzfdh zzfdh) throws zzfew {
        return (zzdpq) zzfee.zza(zzlqe, zzfdh);
    }

    public final int getVersion() {
        return this.zzlqb;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdpu.zza zza2;
        zzdre.zza zza3;
        boolean z = false;
        switch (zzdpr.zzbaq[i - 1]) {
            case 1:
                return new zzdpq();
            case 2:
                return zzlqe;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdpq zzdpq = (zzdpq) obj2;
                boolean z2 = this.zzlqb != 0;
                int i2 = this.zzlqb;
                if (zzdpq.zzlqb != 0) {
                    z = true;
                }
                this.zzlqb = zzfen.zza(z2, i2, z, zzdpq.zzlqb);
                this.zzlqc = (zzdpu) zzfen.zza(this.zzlqc, zzdpq.zzlqc);
                this.zzlqd = (zzdre) zzfen.zza(this.zzlqd, zzdpq.zzlqd);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzlqb = zzfdq.zzcub();
                                } else if (zzcts == 18) {
                                    if (this.zzlqc != null) {
                                        zzdpu zzdpu = this.zzlqc;
                                        zzfef zzfef = (zzfef) zzdpu.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdpu);
                                        zza2 = (zzdpu.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlqc = (zzdpu) zzfdq.zza(zzdpu.zzblv(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlqc);
                                        this.zzlqc = (zzdpu) zza2.zzcvj();
                                    }
                                } else if (zzcts == 26) {
                                    if (this.zzlqd != null) {
                                        zzdre zzdre = this.zzlqd;
                                        zzfef zzfef2 = (zzfef) zzdre.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef2.zza((zzfee) zzdre);
                                        zza3 = (zzdre.zza) zzfef2;
                                    } else {
                                        zza3 = null;
                                    }
                                    this.zzlqd = (zzdre) zzfdq.zza(zzdre.zzbnk(), zzfea);
                                    if (zza3 != null) {
                                        zza3.zza((zzfee) this.zzlqd);
                                        this.zzlqd = (zzdre) zza3.zzcvj();
                                    }
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdpq.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlqe);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlqe;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlqb != 0) {
            zzfdv.zzab(1, this.zzlqb);
        }
        if (this.zzlqc != null) {
            zzfdv.zza(2, this.zzlqc == null ? zzdpu.zzblv() : this.zzlqc);
        }
        if (this.zzlqd != null) {
            zzfdv.zza(3, this.zzlqd == null ? zzdre.zzbnk() : this.zzlqd);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzdpu zzbll() {
        return this.zzlqc == null ? zzdpu.zzblv() : this.zzlqc;
    }

    public final zzdre zzblm() {
        return this.zzlqd == null ? zzdre.zzbnk() : this.zzlqd;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlqb != 0) {
            i2 = 0 + zzfdv.zzae(1, this.zzlqb);
        }
        if (this.zzlqc != null) {
            i2 += zzfdv.zzb(2, this.zzlqc == null ? zzdpu.zzblv() : this.zzlqc);
        }
        if (this.zzlqd != null) {
            i2 += zzfdv.zzb(3, this.zzlqd == null ? zzdre.zzbnk() : this.zzlqd);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
