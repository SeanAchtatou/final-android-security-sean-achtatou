package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzdf;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbnh extends zzdf<zzbll, DriveContents> {
    private /* synthetic */ DriveFile zzgme;
    private /* synthetic */ int zzgmf;

    zzbnh(zzbmu zzbmu, DriveFile driveFile, int i) {
        this.zzgme = driveFile;
        this.zzgmf = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbqu(this.zzgme.getDriveId(), this.zzgmf, 0), new zzbrz(taskCompletionSource));
    }
}
