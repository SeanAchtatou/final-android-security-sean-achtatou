package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzba implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private /* synthetic */ zzar zzfor;

    private zzba(zzar zzar) {
        this.zzfor = zzar;
    }

    /* synthetic */ zzba(zzar zzar, zzas zzas) {
        this(zzar);
    }

    public final void onConnected(Bundle bundle) {
        this.zzfor.zzfoj.zza(new zzay(this.zzfor));
    }

    public final void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        this.zzfor.zzfmy.lock();
        try {
            if (this.zzfor.zzd(connectionResult)) {
                this.zzfor.zzaht();
                this.zzfor.zzahr();
            } else {
                this.zzfor.zze(connectionResult);
            }
        } finally {
            this.zzfor.zzfmy.unlock();
        }
    }

    public final void onConnectionSuspended(int i) {
    }
}
