package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzbon extends zzblk {
    private /* synthetic */ zzbog zzgng;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbon(zzbog zzbog, GoogleApiClient googleApiClient) {
        super(googleApiClient);
        this.zzgng = zzbog;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbro(this.zzgng.zzgfy), new zzbrj(this));
    }
}
