package com.google.android.gms.internal;

import android.support.v4.view.MotionEventCompat;
import com.millennialmedia.internal.AdPlacementReporter;
import com.miniclip.input.MCInput;
import com.mopub.mobileads.resource.DrawableConstants;
import java.io.IOException;

public final class zzax extends zzfhe<zzax> {
    private static volatile zzax[] zzfc;
    public Long zzdh = null;
    public Long zzdi = null;
    public Long zzfd = null;
    public Long zzfe = null;
    public Long zzff = null;
    public Long zzfg = null;
    public Integer zzfh;
    public Long zzfi = null;
    public Long zzfj = null;
    public Long zzfk = null;
    public Integer zzfl;
    public Long zzfm = null;
    public Long zzfn = null;
    public Long zzfo = null;
    public Long zzfp = null;
    public Long zzfq = null;
    public Long zzfr = null;
    public Long zzfs = null;
    public Long zzft = null;
    public Long zzfu = null;
    public Long zzfv = null;

    public zzax() {
        this.zzpai = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzc */
    public final zzax zza(zzfhb zzfhb) throws IOException {
        int i;
        while (true) {
            int zzcts = zzfhb.zzcts();
            switch (zzcts) {
                case 0:
                    return this;
                case 8:
                    this.zzdh = Long.valueOf(zzfhb.zzcum());
                    continue;
                case 16:
                    this.zzdi = Long.valueOf(zzfhb.zzcum());
                    continue;
                case 24:
                    this.zzfd = Long.valueOf(zzfhb.zzcum());
                    continue;
                case 32:
                    this.zzfe = Long.valueOf(zzfhb.zzcum());
                    continue;
                case MotionEventCompat.AXIS_GENERIC_9:
                    this.zzff = Long.valueOf(zzfhb.zzcum());
                    continue;
                case 48:
                    this.zzfg = Long.valueOf(zzfhb.zzcum());
                    continue;
                case DrawableConstants.CloseButton.WIDGET_HEIGHT_DIPS /*56*/:
                    i = zzfhb.getPosition();
                    this.zzfh = Integer.valueOf(zzav.zzd(zzfhb.zzcuh()));
                    continue;
                case 64:
                    this.zzfi = Long.valueOf(zzfhb.zzcum());
                    continue;
                case DrawableConstants.GradientStrip.GRADIENT_STRIP_HEIGHT_DIPS /*72*/:
                    this.zzfj = Long.valueOf(zzfhb.zzcum());
                    continue;
                case 80:
                    this.zzfk = Long.valueOf(zzfhb.zzcum());
                    continue;
                case 88:
                    i = zzfhb.getPosition();
                    try {
                        this.zzfl = Integer.valueOf(zzav.zzd(zzfhb.zzcuh()));
                        continue;
                    } catch (IllegalArgumentException unused) {
                        zzfhb.zzlv(i);
                        zza(zzfhb, zzcts);
                    }
                case MCInput.KEYCODE_BUTTON_A /*96*/:
                    this.zzfm = Long.valueOf(zzfhb.zzcum());
                    continue;
                case MCInput.KEYCODE_BUTTON_L2 /*104*/:
                    this.zzfn = Long.valueOf(zzfhb.zzcum());
                    continue;
                case AdPlacementReporter.PLAYLIST_LOADED_FROM_CACHE_DEMAND_ITEM_FAILED /*112*/:
                    this.zzfo = Long.valueOf(zzfhb.zzcum());
                    continue;
                case 120:
                    this.zzfp = Long.valueOf(zzfhb.zzcum());
                    continue;
                case 128:
                    this.zzfq = Long.valueOf(zzfhb.zzcum());
                    continue;
                case 136:
                    this.zzfr = Long.valueOf(zzfhb.zzcum());
                    continue;
                case 144:
                    this.zzfs = Long.valueOf(zzfhb.zzcum());
                    continue;
                case 152:
                    this.zzft = Long.valueOf(zzfhb.zzcum());
                    continue;
                case 160:
                    this.zzfu = Long.valueOf(zzfhb.zzcum());
                    continue;
                case 168:
                    this.zzfv = Long.valueOf(zzfhb.zzcum());
                    continue;
                default:
                    if (!super.zza(zzfhb, zzcts)) {
                        return this;
                    }
                    continue;
            }
            zzfhb.zzlv(i);
            zza(zzfhb, zzcts);
        }
    }

    public static zzax[] zzp() {
        if (zzfc == null) {
            synchronized (zzfhi.zzphg) {
                if (zzfc == null) {
                    zzfc = new zzax[0];
                }
            }
        }
        return zzfc;
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzdh != null) {
            zzfhc.zzf(1, this.zzdh.longValue());
        }
        if (this.zzdi != null) {
            zzfhc.zzf(2, this.zzdi.longValue());
        }
        if (this.zzfd != null) {
            zzfhc.zzf(3, this.zzfd.longValue());
        }
        if (this.zzfe != null) {
            zzfhc.zzf(4, this.zzfe.longValue());
        }
        if (this.zzff != null) {
            zzfhc.zzf(5, this.zzff.longValue());
        }
        if (this.zzfg != null) {
            zzfhc.zzf(6, this.zzfg.longValue());
        }
        if (this.zzfh != null) {
            zzfhc.zzaa(7, this.zzfh.intValue());
        }
        if (this.zzfi != null) {
            zzfhc.zzf(8, this.zzfi.longValue());
        }
        if (this.zzfj != null) {
            zzfhc.zzf(9, this.zzfj.longValue());
        }
        if (this.zzfk != null) {
            zzfhc.zzf(10, this.zzfk.longValue());
        }
        if (this.zzfl != null) {
            zzfhc.zzaa(11, this.zzfl.intValue());
        }
        if (this.zzfm != null) {
            zzfhc.zzf(12, this.zzfm.longValue());
        }
        if (this.zzfn != null) {
            zzfhc.zzf(13, this.zzfn.longValue());
        }
        if (this.zzfo != null) {
            zzfhc.zzf(14, this.zzfo.longValue());
        }
        if (this.zzfp != null) {
            zzfhc.zzf(15, this.zzfp.longValue());
        }
        if (this.zzfq != null) {
            zzfhc.zzf(16, this.zzfq.longValue());
        }
        if (this.zzfr != null) {
            zzfhc.zzf(17, this.zzfr.longValue());
        }
        if (this.zzfs != null) {
            zzfhc.zzf(18, this.zzfs.longValue());
        }
        if (this.zzft != null) {
            zzfhc.zzf(19, this.zzft.longValue());
        }
        if (this.zzfu != null) {
            zzfhc.zzf(20, this.zzfu.longValue());
        }
        if (this.zzfv != null) {
            zzfhc.zzf(21, this.zzfv.longValue());
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzdh != null) {
            zzo += zzfhc.zzc(1, this.zzdh.longValue());
        }
        if (this.zzdi != null) {
            zzo += zzfhc.zzc(2, this.zzdi.longValue());
        }
        if (this.zzfd != null) {
            zzo += zzfhc.zzc(3, this.zzfd.longValue());
        }
        if (this.zzfe != null) {
            zzo += zzfhc.zzc(4, this.zzfe.longValue());
        }
        if (this.zzff != null) {
            zzo += zzfhc.zzc(5, this.zzff.longValue());
        }
        if (this.zzfg != null) {
            zzo += zzfhc.zzc(6, this.zzfg.longValue());
        }
        if (this.zzfh != null) {
            zzo += zzfhc.zzad(7, this.zzfh.intValue());
        }
        if (this.zzfi != null) {
            zzo += zzfhc.zzc(8, this.zzfi.longValue());
        }
        if (this.zzfj != null) {
            zzo += zzfhc.zzc(9, this.zzfj.longValue());
        }
        if (this.zzfk != null) {
            zzo += zzfhc.zzc(10, this.zzfk.longValue());
        }
        if (this.zzfl != null) {
            zzo += zzfhc.zzad(11, this.zzfl.intValue());
        }
        if (this.zzfm != null) {
            zzo += zzfhc.zzc(12, this.zzfm.longValue());
        }
        if (this.zzfn != null) {
            zzo += zzfhc.zzc(13, this.zzfn.longValue());
        }
        if (this.zzfo != null) {
            zzo += zzfhc.zzc(14, this.zzfo.longValue());
        }
        if (this.zzfp != null) {
            zzo += zzfhc.zzc(15, this.zzfp.longValue());
        }
        if (this.zzfq != null) {
            zzo += zzfhc.zzc(16, this.zzfq.longValue());
        }
        if (this.zzfr != null) {
            zzo += zzfhc.zzc(17, this.zzfr.longValue());
        }
        if (this.zzfs != null) {
            zzo += zzfhc.zzc(18, this.zzfs.longValue());
        }
        if (this.zzft != null) {
            zzo += zzfhc.zzc(19, this.zzft.longValue());
        }
        if (this.zzfu != null) {
            zzo += zzfhc.zzc(20, this.zzfu.longValue());
        }
        return this.zzfv != null ? zzo + zzfhc.zzc(21, this.zzfv.longValue()) : zzo;
    }
}
