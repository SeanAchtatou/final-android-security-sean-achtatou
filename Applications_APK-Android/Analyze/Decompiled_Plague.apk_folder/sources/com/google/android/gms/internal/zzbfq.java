package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbq;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class zzbfq extends zzbej {
    public static final Parcelable.Creator<zzbfq> CREATOR = new zzbft();
    private int zzdzm;
    private final HashMap<String, Map<String, zzbfl<?, ?>>> zzfzu;
    private final ArrayList<zzbfr> zzfzv = null;
    private final String zzfzw;

    zzbfq(int i, ArrayList<zzbfr> arrayList, String str) {
        this.zzdzm = i;
        HashMap<String, Map<String, zzbfl<?, ?>>> hashMap = new HashMap<>();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            zzbfr zzbfr = arrayList.get(i2);
            hashMap.put(zzbfr.className, zzbfr.zzalo());
        }
        this.zzfzu = hashMap;
        this.zzfzw = (String) zzbq.checkNotNull(str);
        zzalm();
    }

    private final void zzalm() {
        for (String str : this.zzfzu.keySet()) {
            Map map = this.zzfzu.get(str);
            for (String str2 : map.keySet()) {
                ((zzbfl) map.get(str2)).zza(this);
            }
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        for (String next : this.zzfzu.keySet()) {
            sb.append(next);
            sb.append(":\n");
            Map map = this.zzfzu.get(next);
            for (String str : map.keySet()) {
                sb.append("  ");
                sb.append(str);
                sb.append(": ");
                sb.append(map.get(str));
            }
        }
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 1, this.zzdzm);
        ArrayList arrayList = new ArrayList();
        for (String next : this.zzfzu.keySet()) {
            arrayList.add(new zzbfr(next, this.zzfzu.get(next)));
        }
        zzbem.zzc(parcel, 2, arrayList, false);
        zzbem.zza(parcel, 3, this.zzfzw, false);
        zzbem.zzai(parcel, zze);
    }

    public final String zzaln() {
        return this.zzfzw;
    }

    public final Map<String, zzbfl<?, ?>> zzgl(String str) {
        return this.zzfzu.get(str);
    }
}
