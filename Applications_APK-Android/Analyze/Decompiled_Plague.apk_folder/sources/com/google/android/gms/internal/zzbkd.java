package com.google.android.gms.internal;

import android.content.IntentSender;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;

@Deprecated
public final class zzbkd {
    private String zzejt;
    private DriveId zzghn;
    private MetadataChangeSet zzgjw;
    private Integer zzgjx;
    private final int zzgjy = 0;

    public zzbkd(int i) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final IntentSender build(GoogleApiClient googleApiClient) {
        zzbq.zza(googleApiClient.isConnected(), (Object) "Client must be connected");
        zzanm();
        zzbll zzbll = (zzbll) googleApiClient.zza(Drive.zzdyh);
        this.zzgjw.zzanz().setContext(zzbll.getContext());
        try {
            return ((zzbpf) zzbll.zzakb()).zza(new zzbke(this.zzgjw.zzanz(), this.zzgjx.intValue(), this.zzejt, this.zzghn, 0));
        } catch (RemoteException e) {
            throw new RuntimeException("Unable to connect Drive Play Service", e);
        }
    }

    public final int getRequestId() {
        return this.zzgjx.intValue();
    }

    public final void setActivityTitle(String str) {
        this.zzejt = (String) zzbq.checkNotNull(str);
    }

    public final void zza(DriveId driveId) {
        this.zzghn = (DriveId) zzbq.checkNotNull(driveId);
    }

    public final void zza(MetadataChangeSet metadataChangeSet) {
        this.zzgjw = (MetadataChangeSet) zzbq.checkNotNull(metadataChangeSet);
    }

    public final MetadataChangeSet zzani() {
        return this.zzgjw;
    }

    public final DriveId zzanj() {
        return this.zzghn;
    }

    public final String zzank() {
        return this.zzejt;
    }

    public final void zzanm() {
        zzbq.checkNotNull(this.zzgjw, "Must provide initial metadata via setInitialMetadata.");
        this.zzgjx = Integer.valueOf(this.zzgjx == null ? 0 : this.zzgjx.intValue());
    }

    public final void zzct(int i) {
        this.zzgjx = Integer.valueOf(i);
    }
}
