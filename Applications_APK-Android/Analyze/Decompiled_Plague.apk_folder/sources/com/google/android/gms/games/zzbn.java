package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbn extends zzac<Void> {
    final /* synthetic */ zzbm zzhkq;

    zzbn(zzbm zzbm) {
        this.zzhkq = zzbm;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException {
        this.zzhkq.zzhko.zza(new zzbo(this));
        taskCompletionSource.setResult(null);
    }
}
