package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

public final class zzdoy {
    public static zzdsk zza(zzdqy zzdqy) throws GeneralSecurityException {
        switch (zzdoz.zzlpo[zzdqy.ordinal()]) {
            case 1:
                return zzdsk.NIST_P256;
            case 2:
                return zzdsk.NIST_P384;
            case 3:
                return zzdsk.NIST_P521;
            default:
                String valueOf = String.valueOf(zzdqy);
                StringBuilder sb = new StringBuilder(20 + String.valueOf(valueOf).length());
                sb.append("unknown curve type: ");
                sb.append(valueOf);
                throw new GeneralSecurityException(sb.toString());
        }
    }

    public static zzdsl zza(zzdqo zzdqo) throws GeneralSecurityException {
        switch (zzdoz.zzlpp[zzdqo.ordinal()]) {
            case 1:
                return zzdsl.UNCOMPRESSED;
            case 2:
                return zzdsl.COMPRESSED;
            default:
                String valueOf = String.valueOf(zzdqo);
                StringBuilder sb = new StringBuilder(22 + String.valueOf(valueOf).length());
                sb.append("unknown point format: ");
                sb.append(valueOf);
                throw new GeneralSecurityException(sb.toString());
        }
    }

    public static String zza(zzdrc zzdrc) throws NoSuchAlgorithmException {
        switch (zzdoz.zzlpn[zzdrc.ordinal()]) {
            case 1:
                return "HmacSha1";
            case 2:
                return "HmacSha256";
            case 3:
                return "HmacSha512";
            default:
                String valueOf = String.valueOf(zzdrc);
                StringBuilder sb = new StringBuilder(27 + String.valueOf(valueOf).length());
                sb.append("hash unsupported for HMAC: ");
                sb.append(valueOf);
                throw new NoSuchAlgorithmException(sb.toString());
        }
    }
}
