package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfig extends zzfhe<zzfig> implements Cloneable {
    private String version = "";
    private int zzivi = 0;
    private String zzpkb = "";

    public zzfig() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzcxw */
    public zzfig clone() {
        try {
            return (zzfig) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfig)) {
            return false;
        }
        zzfig zzfig = (zzfig) obj;
        if (this.zzivi != zzfig.zzivi) {
            return false;
        }
        if (this.zzpkb == null) {
            if (zzfig.zzpkb != null) {
                return false;
            }
        } else if (!this.zzpkb.equals(zzfig.zzpkb)) {
            return false;
        }
        if (this.version == null) {
            if (zzfig.version != null) {
                return false;
            }
        } else if (!this.version.equals(zzfig.version)) {
            return false;
        }
        return (this.zzpgy == null || this.zzpgy.isEmpty()) ? zzfig.zzpgy == null || zzfig.zzpgy.isEmpty() : this.zzpgy.equals(zzfig.zzpgy);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((527 + getClass().getName().hashCode()) * 31) + this.zzivi) * 31) + (this.zzpkb == null ? 0 : this.zzpkb.hashCode())) * 31) + (this.version == null ? 0 : this.version.hashCode())) * 31;
        if (this.zzpgy != null && !this.zzpgy.isEmpty()) {
            i = this.zzpgy.hashCode();
        }
        return hashCode + i;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 8) {
                this.zzivi = zzfhb.zzctv();
            } else if (zzcts == 18) {
                this.zzpkb = zzfhb.readString();
            } else if (zzcts == 26) {
                this.version = zzfhb.readString();
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzivi != 0) {
            zzfhc.zzaa(1, this.zzivi);
        }
        if (this.zzpkb != null && !this.zzpkb.equals("")) {
            zzfhc.zzn(2, this.zzpkb);
        }
        if (this.version != null && !this.version.equals("")) {
            zzfhc.zzn(3, this.version);
        }
        super.zza(zzfhc);
    }

    public final /* synthetic */ zzfhe zzcxe() throws CloneNotSupportedException {
        return (zzfig) clone();
    }

    public final /* synthetic */ zzfhk zzcxf() throws CloneNotSupportedException {
        return (zzfig) clone();
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzivi != 0) {
            zzo += zzfhc.zzad(1, this.zzivi);
        }
        if (this.zzpkb != null && !this.zzpkb.equals("")) {
            zzo += zzfhc.zzo(2, this.zzpkb);
        }
        return (this.version == null || this.version.equals("")) ? zzo : zzo + zzfhc.zzo(3, this.version);
    }
}
