package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.events.ChangeListener;
import com.google.android.gms.drive.events.zzj;
import java.util.ArrayList;
import java.util.Set;

public class zzbog implements DriveResource {
    protected final DriveId zzgfy;

    public zzbog(DriveId driveId) {
        this.zzgfy = driveId;
    }

    public PendingResult<Status> addChangeListener(GoogleApiClient googleApiClient, ChangeListener changeListener) {
        return ((zzbll) googleApiClient.zza(Drive.zzdyh)).zza(googleApiClient, this.zzgfy, changeListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public PendingResult<Status> addChangeSubscription(GoogleApiClient googleApiClient) {
        zzbll zzbll = (zzbll) googleApiClient.zza(Drive.zzdyh);
        zzbjt zzbjt = new zzbjt(1, this.zzgfy);
        zzbq.checkArgument(zzj.zza(zzbjt.zzgas, zzbjt.zzgfy));
        zzbq.zza(zzbll.isConnected(), (Object) "Client must be connected");
        if (zzbll.zzgkn) {
            return googleApiClient.zze(new zzblo(zzbll, googleApiClient, zzbjt));
        }
        throw new IllegalStateException("Application must define an exported DriveEventService subclass in AndroidManifest.xml to add event subscriptions");
    }

    public PendingResult<Status> delete(GoogleApiClient googleApiClient) {
        return googleApiClient.zze(new zzbol(this, googleApiClient));
    }

    public DriveId getDriveId() {
        return this.zzgfy;
    }

    public PendingResult<DriveResource.MetadataResult> getMetadata(GoogleApiClient googleApiClient) {
        return googleApiClient.zzd(new zzboh(this, googleApiClient, false));
    }

    public PendingResult<DriveApi.MetadataBufferResult> listParents(GoogleApiClient googleApiClient) {
        return googleApiClient.zzd(new zzboi(this, googleApiClient));
    }

    public PendingResult<Status> removeChangeListener(GoogleApiClient googleApiClient, ChangeListener changeListener) {
        return ((zzbll) googleApiClient.zza(Drive.zzdyh)).zzb(googleApiClient, this.zzgfy, changeListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public PendingResult<Status> removeChangeSubscription(GoogleApiClient googleApiClient) {
        zzbll zzbll = (zzbll) googleApiClient.zza(Drive.zzdyh);
        DriveId driveId = this.zzgfy;
        zzbq.checkArgument(zzj.zza(1, driveId));
        zzbq.zza(zzbll.isConnected(), (Object) "Client must be connected");
        return googleApiClient.zze(new zzblp(zzbll, googleApiClient, driveId, 1));
    }

    public PendingResult<Status> setParents(GoogleApiClient googleApiClient, Set<DriveId> set) {
        if (set != null) {
            return googleApiClient.zze(new zzboj(this, googleApiClient, new ArrayList(set)));
        }
        throw new IllegalArgumentException("ParentIds must be provided.");
    }

    public PendingResult<Status> trash(GoogleApiClient googleApiClient) {
        return googleApiClient.zze(new zzbom(this, googleApiClient));
    }

    public PendingResult<Status> untrash(GoogleApiClient googleApiClient) {
        return googleApiClient.zze(new zzbon(this, googleApiClient));
    }

    public PendingResult<DriveResource.MetadataResult> updateMetadata(GoogleApiClient googleApiClient, MetadataChangeSet metadataChangeSet) {
        if (metadataChangeSet != null) {
            return googleApiClient.zze(new zzbok(this, googleApiClient, metadataChangeSet));
        }
        throw new IllegalArgumentException("ChangeSet must be provided.");
    }
}
