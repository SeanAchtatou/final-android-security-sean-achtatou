package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzboh extends zzbor {
    private /* synthetic */ boolean zzgnf = false;
    private /* synthetic */ zzbog zzgng;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzboh(zzbog zzbog, GoogleApiClient googleApiClient, boolean z) {
        super(zzbog, googleApiClient, null);
        this.zzgng = zzbog;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbpb(this.zzgng.zzgfy, this.zzgnf), new zzbop(this));
    }
}
