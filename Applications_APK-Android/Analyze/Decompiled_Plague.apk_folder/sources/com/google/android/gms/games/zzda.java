package com.google.android.gms.games;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.video.CaptureState;
import com.google.android.gms.games.video.Videos;

final class zzda implements zzbo<Videos.CaptureStateResult, CaptureState> {
    zzda() {
    }

    public final /* synthetic */ Object zzb(Result result) {
        Videos.CaptureStateResult captureStateResult = (Videos.CaptureStateResult) result;
        if (captureStateResult == null) {
            return null;
        }
        return captureStateResult.getCaptureState();
    }
}
