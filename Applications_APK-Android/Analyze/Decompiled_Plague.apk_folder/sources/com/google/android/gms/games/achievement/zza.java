package com.google.android.gms.games.achievement;

import android.os.Parcelable;

public final class zza implements Parcelable.Creator<AchievementEntity> {
    /* JADX WARN: Type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v5, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r26) {
        /*
            r25 = this;
            r0 = r26
            int r1 = com.google.android.gms.internal.zzbek.zzd(r26)
            r2 = 0
            r4 = 0
            r5 = 0
            r21 = r2
            r23 = r21
            r8 = r4
            r15 = r8
            r18 = r15
            r19 = r18
            r7 = r5
            r9 = r7
            r10 = r9
            r11 = r10
            r12 = r11
            r13 = r12
            r14 = r13
            r16 = r14
            r17 = r16
            r20 = r17
        L_0x0021:
            int r2 = r26.dataPosition()
            if (r2 >= r1) goto L_0x0096
            int r2 = r26.readInt()
            r3 = 65535(0xffff, float:9.1834E-41)
            r3 = r3 & r2
            switch(r3) {
                case 1: goto L_0x0091;
                case 2: goto L_0x008c;
                case 3: goto L_0x0087;
                case 4: goto L_0x0082;
                case 5: goto L_0x0078;
                case 6: goto L_0x0073;
                case 7: goto L_0x0069;
                case 8: goto L_0x0064;
                case 9: goto L_0x005f;
                case 10: goto L_0x005a;
                case 11: goto L_0x004f;
                case 12: goto L_0x004a;
                case 13: goto L_0x0045;
                case 14: goto L_0x0040;
                case 15: goto L_0x003b;
                case 16: goto L_0x0036;
                default: goto L_0x0032;
            }
        L_0x0032:
            com.google.android.gms.internal.zzbek.zzb(r0, r2)
            goto L_0x0021
        L_0x0036:
            long r23 = com.google.android.gms.internal.zzbek.zzi(r0, r2)
            goto L_0x0021
        L_0x003b:
            long r21 = com.google.android.gms.internal.zzbek.zzi(r0, r2)
            goto L_0x0021
        L_0x0040:
            java.lang.String r20 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x0021
        L_0x0045:
            int r19 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x0021
        L_0x004a:
            int r18 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x0021
        L_0x004f:
            android.os.Parcelable$Creator<com.google.android.gms.games.PlayerEntity> r3 = com.google.android.gms.games.PlayerEntity.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r17 = r2
            com.google.android.gms.games.PlayerEntity r17 = (com.google.android.gms.games.PlayerEntity) r17
            goto L_0x0021
        L_0x005a:
            java.lang.String r16 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x0021
        L_0x005f:
            int r15 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x0021
        L_0x0064:
            java.lang.String r14 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x0021
        L_0x0069:
            android.os.Parcelable$Creator r3 = android.net.Uri.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r13 = r2
            android.net.Uri r13 = (android.net.Uri) r13
            goto L_0x0021
        L_0x0073:
            java.lang.String r12 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x0021
        L_0x0078:
            android.os.Parcelable$Creator r3 = android.net.Uri.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r11 = r2
            android.net.Uri r11 = (android.net.Uri) r11
            goto L_0x0021
        L_0x0082:
            java.lang.String r10 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x0021
        L_0x0087:
            java.lang.String r9 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x0021
        L_0x008c:
            int r8 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x0021
        L_0x0091:
            java.lang.String r7 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x0021
        L_0x0096:
            com.google.android.gms.internal.zzbek.zzaf(r0, r1)
            com.google.android.gms.games.achievement.AchievementEntity r0 = new com.google.android.gms.games.achievement.AchievementEntity
            r6 = r0
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r23)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.games.achievement.zza.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new AchievementEntity[i];
    }
}
