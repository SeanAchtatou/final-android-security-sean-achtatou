package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.internal.zzid;

@zzzb
public final class zzze extends zzafh implements zzzr {
    private final Context mContext;
    private zztn zzccq;
    private zzzz zzcdj;
    private zzaad zzchw;
    /* access modifiers changed from: private */
    public Runnable zzchx;
    /* access modifiers changed from: private */
    public final Object zzchy = new Object();
    private final zzzd zzckx;
    /* access modifiers changed from: private */
    public final zzaaa zzcky;
    private final zzib zzckz;
    private final zzig zzcla;
    zzahi zzclb;

    public zzze(Context context, zzaaa zzaaa, zzzd zzzd, zzig zzig) {
        zzib zzib;
        zzic zzic;
        this.zzckx = zzzd;
        this.mContext = context;
        this.zzcky = zzaaa;
        this.zzcla = zzig;
        this.zzckz = new zzib(this.zzcla, ((Boolean) zzbs.zzep().zzd(zzmq.zzbpk)).booleanValue());
        this.zzckz.zza(new zzzf(this));
        zzim zzim = new zzim();
        zzim.zzbbq = Integer.valueOf(this.zzcky.zzatd.zzdbz);
        zzim.zzbbr = Integer.valueOf(this.zzcky.zzatd.zzdca);
        zzim.zzbbs = Integer.valueOf(this.zzcky.zzatd.zzdcb ? 0 : 2);
        this.zzckz.zza(new zzzg(zzim));
        if (this.zzcky.zzclp != null) {
            this.zzckz.zza(new zzzh(this));
        }
        zziw zziw = this.zzcky.zzath;
        if (zziw.zzbdb && "interstitial_mb".equals(zziw.zzbda)) {
            zzib = this.zzckz;
            zzic = zzzi.zzcle;
        } else if (zziw.zzbdb && "reward_mb".equals(zziw.zzbda)) {
            zzib = this.zzckz;
            zzic = zzzj.zzcle;
        } else if (zziw.zzbdd || zziw.zzbdb) {
            zzib = this.zzckz;
            zzic = zzzl.zzcle;
        } else {
            zzib = this.zzckz;
            zzic = zzzk.zzcle;
        }
        zzib.zza(zzic);
        this.zzckz.zza(zzid.zza.zzb.AD_REQUEST);
    }

    private final zziw zza(zzzz zzzz) throws zzzo {
        if (((this.zzcdj == null || this.zzcdj.zzatx == null || this.zzcdj.zzatx.size() <= 1) ? false : true) && this.zzccq != null && !this.zzccq.zzccl) {
            return null;
        }
        if (this.zzchw.zzbde) {
            for (zziw zziw : zzzz.zzath.zzbdc) {
                if (zziw.zzbde) {
                    return new zziw(zziw, zzzz.zzath.zzbdc);
                }
            }
        }
        if (this.zzchw.zzcnj == null) {
            throw new zzzo("The ad response must specify one of the supported ad sizes.", 0);
        }
        String[] split = this.zzchw.zzcnj.split("x");
        if (split.length != 2) {
            String valueOf = String.valueOf(this.zzchw.zzcnj);
            throw new zzzo(valueOf.length() != 0 ? "Invalid ad size format from the ad response: ".concat(valueOf) : new String("Invalid ad size format from the ad response: "), 0);
        }
        try {
            int parseInt = Integer.parseInt(split[0]);
            int parseInt2 = Integer.parseInt(split[1]);
            for (zziw zziw2 : zzzz.zzath.zzbdc) {
                float f = this.mContext.getResources().getDisplayMetrics().density;
                int i = zziw2.width == -1 ? (int) (((float) zziw2.widthPixels) / f) : zziw2.width;
                int i2 = zziw2.height == -2 ? (int) (((float) zziw2.heightPixels) / f) : zziw2.height;
                if (parseInt == i && parseInt2 == i2 && !zziw2.zzbde) {
                    return new zziw(zziw2, zzzz.zzath.zzbdc);
                }
            }
            String valueOf2 = String.valueOf(this.zzchw.zzcnj);
            throw new zzzo(valueOf2.length() != 0 ? "The ad size from the ad response was not one of the requested sizes: ".concat(valueOf2) : new String("The ad size from the ad response was not one of the requested sizes: "), 0);
        } catch (NumberFormatException unused) {
            String valueOf3 = String.valueOf(this.zzchw.zzcnj);
            throw new zzzo(valueOf3.length() != 0 ? "Invalid ad size number from the ad response: ".concat(valueOf3) : new String("Invalid ad size number from the ad response: "), 0);
        }
    }

    /* access modifiers changed from: private */
    public final void zzc(int i, String str) {
        int i2 = i;
        if (i2 == 3 || i2 == -1) {
            zzafj.zzcn(str);
        } else {
            zzafj.zzco(str);
        }
        this.zzchw = this.zzchw == null ? new zzaad(i2) : new zzaad(i2, this.zzchw.zzccb);
        this.zzckx.zza(new zzaev(this.zzcdj != null ? this.zzcdj : new zzzz(this.zzcky, -1, null, null, null), this.zzchw, this.zzccq, null, i2, -1, this.zzchw.zzcnk, null, this.zzckz, null));
    }

    public final void onStop() {
        synchronized (this.zzchy) {
            if (this.zzclb != null) {
                this.zzclb.cancel();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final zzahi zza(zzaiy zzaiy, zzaka<zzzz> zzaka) {
        Context context = this.mContext;
        if (new zzzq(context).zza(zzaiy)) {
            zzafj.zzbw("Fetching ad response from local ad request service.");
            zzzw zzzw = new zzzw(context, zzaka, this);
            zzzw.zzmx();
            return zzzw;
        }
        zzafj.zzbw("Fetching ad response from remote ad request service.");
        zzjk.zzhx();
        if (zzais.zzbd(context)) {
            return new zzzx(context, zzaiy, zzaka, this);
        }
        zzafj.zzco("Failed to connect to remote ad request service.");
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzagr.zzb(com.google.android.gms.internal.zzis, boolean):void
     arg types: [com.google.android.gms.internal.zzis, int]
     candidates:
      com.google.android.gms.internal.zzagr.zzb(android.app.Activity, android.view.ViewTreeObserver$OnScrollChangedListener):void
      com.google.android.gms.internal.zzagr.zzb(android.content.Context, android.content.Intent):void
      com.google.android.gms.internal.zzagr.zzb(com.google.android.gms.internal.zzis, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01bd  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x01d1  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01db  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x01ec  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(@android.support.annotation.NonNull com.google.android.gms.internal.zzaad r14) {
        /*
            r13 = this;
            java.lang.String r0 = "Received ad response."
            com.google.android.gms.internal.zzafj.zzbw(r0)
            r13.zzchw = r14
            com.google.android.gms.common.util.zzd r14 = com.google.android.gms.ads.internal.zzbs.zzei()
            long r6 = r14.elapsedRealtime()
            java.lang.Object r14 = r13.zzchy
            monitor-enter(r14)
            r0 = 0
            r13.zzclb = r0     // Catch:{ all -> 0x021a }
            monitor-exit(r14)     // Catch:{ all -> 0x021a }
            com.google.android.gms.internal.zzaez r14 = com.google.android.gms.ads.internal.zzbs.zzeg()
            android.content.Context r1 = r13.mContext
            com.google.android.gms.internal.zzaad r2 = r13.zzchw
            boolean r2 = r2.zzcmk
            r14.zzg(r1, r2)
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r14 = com.google.android.gms.internal.zzmq.zzbkg
            com.google.android.gms.internal.zzmo r1 = com.google.android.gms.ads.internal.zzbs.zzep()
            java.lang.Object r14 = r1.zzd(r14)
            java.lang.Boolean r14 = (java.lang.Boolean) r14
            boolean r14 = r14.booleanValue()
            r1 = 0
            if (r14 == 0) goto L_0x0098
            com.google.android.gms.internal.zzaad r14 = r13.zzchw
            boolean r14 = r14.zzcmw
            if (r14 == 0) goto L_0x0070
            com.google.android.gms.ads.internal.zzbs.zzeg()
            android.content.Context r14 = r13.mContext
            com.google.android.gms.internal.zzzz r2 = r13.zzcdj
            java.lang.String r2 = r2.zzatb
            java.lang.String r3 = "admob"
            android.content.SharedPreferences r14 = r14.getSharedPreferences(r3, r1)
            java.util.Set r3 = java.util.Collections.emptySet()
            java.lang.String r4 = "never_pool_slots"
            java.util.Set r3 = r14.getStringSet(r4, r3)
            boolean r4 = r3.contains(r2)
            if (r4 != 0) goto L_0x0098
            java.util.HashSet r4 = new java.util.HashSet
            r4.<init>(r3)
            r4.add(r2)
        L_0x0063:
            android.content.SharedPreferences$Editor r14 = r14.edit()
            java.lang.String r2 = "never_pool_slots"
            r14.putStringSet(r2, r4)
            r14.apply()
            goto L_0x0098
        L_0x0070:
            com.google.android.gms.ads.internal.zzbs.zzeg()
            android.content.Context r14 = r13.mContext
            com.google.android.gms.internal.zzzz r2 = r13.zzcdj
            java.lang.String r2 = r2.zzatb
            java.lang.String r3 = "admob"
            android.content.SharedPreferences r14 = r14.getSharedPreferences(r3, r1)
            java.util.Set r3 = java.util.Collections.emptySet()
            java.lang.String r4 = "never_pool_slots"
            java.util.Set r3 = r14.getStringSet(r4, r3)
            boolean r4 = r3.contains(r2)
            if (r4 == 0) goto L_0x0098
            java.util.HashSet r4 = new java.util.HashSet
            r4.<init>(r3)
            r4.remove(r2)
            goto L_0x0063
        L_0x0098:
            com.google.android.gms.internal.zzaad r14 = r13.zzchw     // Catch:{ zzzo -> 0x020d }
            int r14 = r14.errorCode     // Catch:{ zzzo -> 0x020d }
            r2 = -2
            r3 = -3
            if (r14 == r2) goto L_0x00c7
            com.google.android.gms.internal.zzaad r14 = r13.zzchw     // Catch:{ zzzo -> 0x020d }
            int r14 = r14.errorCode     // Catch:{ zzzo -> 0x020d }
            if (r14 == r3) goto L_0x00c7
            com.google.android.gms.internal.zzzo r14 = new com.google.android.gms.internal.zzzo     // Catch:{ zzzo -> 0x020d }
            com.google.android.gms.internal.zzaad r0 = r13.zzchw     // Catch:{ zzzo -> 0x020d }
            int r0 = r0.errorCode     // Catch:{ zzzo -> 0x020d }
            r1 = 66
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ zzzo -> 0x020d }
            r2.<init>(r1)     // Catch:{ zzzo -> 0x020d }
            java.lang.String r1 = "There was a problem getting an ad response. ErrorCode: "
            r2.append(r1)     // Catch:{ zzzo -> 0x020d }
            r2.append(r0)     // Catch:{ zzzo -> 0x020d }
            java.lang.String r0 = r2.toString()     // Catch:{ zzzo -> 0x020d }
            com.google.android.gms.internal.zzaad r1 = r13.zzchw     // Catch:{ zzzo -> 0x020d }
            int r1 = r1.errorCode     // Catch:{ zzzo -> 0x020d }
            r14.<init>(r0, r1)     // Catch:{ zzzo -> 0x020d }
            throw r14     // Catch:{ zzzo -> 0x020d }
        L_0x00c7:
            com.google.android.gms.internal.zzaad r14 = r13.zzchw     // Catch:{ zzzo -> 0x020d }
            int r14 = r14.errorCode     // Catch:{ zzzo -> 0x020d }
            if (r14 == r3) goto L_0x0172
            com.google.android.gms.internal.zzaad r14 = r13.zzchw     // Catch:{ zzzo -> 0x020d }
            java.lang.String r14 = r14.body     // Catch:{ zzzo -> 0x020d }
            boolean r14 = android.text.TextUtils.isEmpty(r14)     // Catch:{ zzzo -> 0x020d }
            if (r14 == 0) goto L_0x00e0
            com.google.android.gms.internal.zzzo r14 = new com.google.android.gms.internal.zzzo     // Catch:{ zzzo -> 0x020d }
            java.lang.String r0 = "No fill from ad server."
            r1 = 3
            r14.<init>(r0, r1)     // Catch:{ zzzo -> 0x020d }
            throw r14     // Catch:{ zzzo -> 0x020d }
        L_0x00e0:
            com.google.android.gms.internal.zzaez r14 = com.google.android.gms.ads.internal.zzbs.zzeg()     // Catch:{ zzzo -> 0x020d }
            android.content.Context r2 = r13.mContext     // Catch:{ zzzo -> 0x020d }
            com.google.android.gms.internal.zzaad r3 = r13.zzchw     // Catch:{ zzzo -> 0x020d }
            boolean r3 = r3.zzclw     // Catch:{ zzzo -> 0x020d }
            r14.zzf(r2, r3)     // Catch:{ zzzo -> 0x020d }
            com.google.android.gms.internal.zzaad r14 = r13.zzchw     // Catch:{ zzzo -> 0x020d }
            boolean r14 = r14.zzcng     // Catch:{ zzzo -> 0x020d }
            if (r14 == 0) goto L_0x0131
            com.google.android.gms.internal.zztn r14 = new com.google.android.gms.internal.zztn     // Catch:{ JSONException -> 0x010a }
            com.google.android.gms.internal.zzaad r2 = r13.zzchw     // Catch:{ JSONException -> 0x010a }
            java.lang.String r2 = r2.body     // Catch:{ JSONException -> 0x010a }
            r14.<init>(r2)     // Catch:{ JSONException -> 0x010a }
            r13.zzccq = r14     // Catch:{ JSONException -> 0x010a }
            com.google.android.gms.internal.zzaez r14 = com.google.android.gms.ads.internal.zzbs.zzeg()     // Catch:{ JSONException -> 0x010a }
            com.google.android.gms.internal.zztn r2 = r13.zzccq     // Catch:{ JSONException -> 0x010a }
            boolean r2 = r2.zzcbz     // Catch:{ JSONException -> 0x010a }
            r14.zzz(r2)     // Catch:{ JSONException -> 0x010a }
            goto L_0x013c
        L_0x010a:
            r14 = move-exception
            java.lang.String r0 = "Could not parse mediation config."
            com.google.android.gms.internal.zzafj.zzb(r0, r14)     // Catch:{ zzzo -> 0x020d }
            com.google.android.gms.internal.zzzo r14 = new com.google.android.gms.internal.zzzo     // Catch:{ zzzo -> 0x020d }
            java.lang.String r0 = "Could not parse mediation config: "
            com.google.android.gms.internal.zzaad r2 = r13.zzchw     // Catch:{ zzzo -> 0x020d }
            java.lang.String r2 = r2.body     // Catch:{ zzzo -> 0x020d }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ zzzo -> 0x020d }
            int r3 = r2.length()     // Catch:{ zzzo -> 0x020d }
            if (r3 == 0) goto L_0x0127
            java.lang.String r0 = r0.concat(r2)     // Catch:{ zzzo -> 0x020d }
            goto L_0x012d
        L_0x0127:
            java.lang.String r2 = new java.lang.String     // Catch:{ zzzo -> 0x020d }
            r2.<init>(r0)     // Catch:{ zzzo -> 0x020d }
            r0 = r2
        L_0x012d:
            r14.<init>(r0, r1)     // Catch:{ zzzo -> 0x020d }
            throw r14     // Catch:{ zzzo -> 0x020d }
        L_0x0131:
            com.google.android.gms.internal.zzaez r14 = com.google.android.gms.ads.internal.zzbs.zzeg()     // Catch:{ zzzo -> 0x020d }
            com.google.android.gms.internal.zzaad r2 = r13.zzchw     // Catch:{ zzzo -> 0x020d }
            boolean r2 = r2.zzcbz     // Catch:{ zzzo -> 0x020d }
            r14.zzz(r2)     // Catch:{ zzzo -> 0x020d }
        L_0x013c:
            com.google.android.gms.internal.zzaad r14 = r13.zzchw     // Catch:{ zzzo -> 0x020d }
            java.lang.String r14 = r14.zzcml     // Catch:{ zzzo -> 0x020d }
            boolean r14 = android.text.TextUtils.isEmpty(r14)     // Catch:{ zzzo -> 0x020d }
            if (r14 != 0) goto L_0x0172
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r14 = com.google.android.gms.internal.zzmq.zzboh     // Catch:{ zzzo -> 0x020d }
            com.google.android.gms.internal.zzmo r2 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ zzzo -> 0x020d }
            java.lang.Object r14 = r2.zzd(r14)     // Catch:{ zzzo -> 0x020d }
            java.lang.Boolean r14 = (java.lang.Boolean) r14     // Catch:{ zzzo -> 0x020d }
            boolean r14 = r14.booleanValue()     // Catch:{ zzzo -> 0x020d }
            if (r14 == 0) goto L_0x0172
            java.lang.String r14 = "Received cookie from server. Setting webview cookie in CookieManager."
            com.google.android.gms.internal.zzafj.zzbw(r14)     // Catch:{ zzzo -> 0x020d }
            com.google.android.gms.internal.zzagw r14 = com.google.android.gms.ads.internal.zzbs.zzee()     // Catch:{ zzzo -> 0x020d }
            android.content.Context r2 = r13.mContext     // Catch:{ zzzo -> 0x020d }
            android.webkit.CookieManager r14 = r14.zzax(r2)     // Catch:{ zzzo -> 0x020d }
            if (r14 == 0) goto L_0x0172
            java.lang.String r2 = "googleads.g.doubleclick.net"
            com.google.android.gms.internal.zzaad r3 = r13.zzchw     // Catch:{ zzzo -> 0x020d }
            java.lang.String r3 = r3.zzcml     // Catch:{ zzzo -> 0x020d }
            r14.setCookie(r2, r3)     // Catch:{ zzzo -> 0x020d }
        L_0x0172:
            com.google.android.gms.internal.zzzz r14 = r13.zzcdj     // Catch:{ zzzo -> 0x020d }
            com.google.android.gms.internal.zziw r14 = r14.zzath     // Catch:{ zzzo -> 0x020d }
            com.google.android.gms.internal.zziw[] r14 = r14.zzbdc     // Catch:{ zzzo -> 0x020d }
            if (r14 == 0) goto L_0x0182
            com.google.android.gms.internal.zzzz r14 = r13.zzcdj     // Catch:{ zzzo -> 0x020d }
            com.google.android.gms.internal.zziw r14 = r13.zza(r14)     // Catch:{ zzzo -> 0x020d }
            r4 = r14
            goto L_0x0183
        L_0x0182:
            r4 = r0
        L_0x0183:
            com.google.android.gms.internal.zzaez r14 = com.google.android.gms.ads.internal.zzbs.zzeg()
            com.google.android.gms.internal.zzaad r2 = r13.zzchw
            boolean r2 = r2.zzcnq
            r14.zzx(r2)
            com.google.android.gms.internal.zzaez r14 = com.google.android.gms.ads.internal.zzbs.zzeg()
            com.google.android.gms.internal.zzaad r2 = r13.zzchw
            boolean r2 = r2.zzcod
            r14.zzy(r2)
            com.google.android.gms.internal.zzaad r14 = r13.zzchw
            java.lang.String r14 = r14.zzcno
            boolean r14 = android.text.TextUtils.isEmpty(r14)
            if (r14 != 0) goto L_0x01b4
            org.json.JSONObject r14 = new org.json.JSONObject     // Catch:{ Exception -> 0x01ae }
            com.google.android.gms.internal.zzaad r2 = r13.zzchw     // Catch:{ Exception -> 0x01ae }
            java.lang.String r2 = r2.zzcno     // Catch:{ Exception -> 0x01ae }
            r14.<init>(r2)     // Catch:{ Exception -> 0x01ae }
            r10 = r14
            goto L_0x01b5
        L_0x01ae:
            r14 = move-exception
            java.lang.String r2 = "Error parsing the JSON for Active View."
            com.google.android.gms.internal.zzafj.zzb(r2, r14)
        L_0x01b4:
            r10 = r0
        L_0x01b5:
            com.google.android.gms.internal.zzaad r14 = r13.zzchw
            int r14 = r14.zzcof
            r2 = 2
            r3 = 1
            if (r14 != r2) goto L_0x01cb
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r3)
            com.google.android.gms.ads.internal.zzbs.zzec()
            com.google.android.gms.internal.zzzz r14 = r13.zzcdj
            com.google.android.gms.internal.zzis r14 = r14.zzclo
            com.google.android.gms.internal.zzagr.zzb(r14, r3)
        L_0x01cb:
            com.google.android.gms.internal.zzaad r14 = r13.zzchw
            int r14 = r14.zzcof
            if (r14 != r3) goto L_0x01d5
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r1)
        L_0x01d5:
            com.google.android.gms.internal.zzaad r14 = r13.zzchw
            int r14 = r14.zzcof
            if (r14 != 0) goto L_0x01ec
            com.google.android.gms.ads.internal.zzbs.zzec()
            com.google.android.gms.internal.zzzz r14 = r13.zzcdj
            com.google.android.gms.internal.zzis r14 = r14.zzclo
            boolean r14 = com.google.android.gms.internal.zzagr.zzp(r14)
            java.lang.Boolean r14 = java.lang.Boolean.valueOf(r14)
            r12 = r14
            goto L_0x01ed
        L_0x01ec:
            r12 = r0
        L_0x01ed:
            com.google.android.gms.internal.zzaev r14 = new com.google.android.gms.internal.zzaev
            com.google.android.gms.internal.zzzz r1 = r13.zzcdj
            com.google.android.gms.internal.zzaad r2 = r13.zzchw
            com.google.android.gms.internal.zztn r3 = r13.zzccq
            r5 = -2
            com.google.android.gms.internal.zzaad r0 = r13.zzchw
            long r8 = r0.zzcnk
            com.google.android.gms.internal.zzib r11 = r13.zzckz
            r0 = r14
            r0.<init>(r1, r2, r3, r4, r5, r6, r8, r10, r11, r12)
            com.google.android.gms.internal.zzzd r0 = r13.zzckx
            r0.zza(r14)
        L_0x0205:
            android.os.Handler r14 = com.google.android.gms.internal.zzagr.zzczc
            java.lang.Runnable r0 = r13.zzchx
            r14.removeCallbacks(r0)
            return
        L_0x020d:
            r14 = move-exception
            int r0 = r14.getErrorCode()
            java.lang.String r14 = r14.getMessage()
            r13.zzc(r0, r14)
            goto L_0x0205
        L_0x021a:
            r0 = move-exception
            monitor-exit(r14)     // Catch:{ all -> 0x021a }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzze.zza(com.google.android.gms.internal.zzaad):void");
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzb(zzil zzil) {
        zzil.zzbbo.zzbbl = this.zzcky.zzclp.packageName;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzc(zzil zzil) {
        zzil.zzbbn = this.zzcky.zzcmb;
    }

    public final void zzdg() {
        String string;
        zzafj.zzbw("AdLoaderBackgroundTask started.");
        this.zzchx = new zzzm(this);
        zzagr.zzczc.postDelayed(this.zzchx, ((Long) zzbs.zzep().zzd(zzmq.zzbls)).longValue());
        long elapsedRealtime = zzbs.zzei().elapsedRealtime();
        if (!((Boolean) zzbs.zzep().zzd(zzmq.zzblq)).booleanValue() || this.zzcky.zzclo.extras == null || (string = this.zzcky.zzclo.extras.getString("_ad")) == null) {
            zzake zzake = new zzake();
            zzagl.zza(new zzzn(this, zzake));
            String zzx = zzbs.zzfa().zzx(this.mContext);
            String zzy = zzbs.zzfa().zzy(this.mContext);
            String zzz = zzbs.zzfa().zzz(this.mContext);
            zzbs.zzfa().zzg(this.mContext, zzz);
            this.zzcdj = new zzzz(this.zzcky, elapsedRealtime, zzx, zzy, zzz);
            zzake.zzj(this.zzcdj);
            return;
        }
        this.zzcdj = new zzzz(this.zzcky, elapsedRealtime, null, null, null);
        zza(zzabm.zza(this.mContext, this.zzcdj, string));
    }
}
