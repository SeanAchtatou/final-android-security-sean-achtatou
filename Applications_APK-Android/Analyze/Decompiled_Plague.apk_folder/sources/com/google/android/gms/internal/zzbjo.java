package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.drive.events.zzk;
import java.util.Arrays;

public final class zzbjo {
    private final zzk zzgjf;
    private final long zzgjg;
    private final long zzgjh;

    /* JADX WARN: Type inference failed for: r0v0, types: [com.google.android.gms.internal.zzbjp, com.google.android.gms.drive.events.zzk] */
    public zzbjo(zzbjr zzbjr) {
        this.zzgjf = new zzbjp(zzbjr);
        this.zzgjg = zzbjr.zzgjg;
        this.zzgjh = zzbjr.zzgjh;
    }

    public final boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        zzbjo zzbjo = (zzbjo) obj;
        return zzbg.equal(this.zzgjf, zzbjo.zzgjf) && this.zzgjg == zzbjo.zzgjg && this.zzgjh == zzbjo.zzgjh;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.zzgjh), Long.valueOf(this.zzgjg), Long.valueOf(this.zzgjh)});
    }

    public final String toString() {
        return String.format("FileTransferProgress[FileTransferState: %s, BytesTransferred: %d, TotalBytes: %d]", this.zzgjf.toString(), Long.valueOf(this.zzgjg), Long.valueOf(this.zzgjh));
    }
}
