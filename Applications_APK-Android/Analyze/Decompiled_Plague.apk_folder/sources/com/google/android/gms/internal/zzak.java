package com.google.android.gms.internal;

import com.mopub.volley.toolbox.HttpClientStack;
import java.net.URI;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;

public final class zzak extends HttpEntityEnclosingRequestBase {
    public zzak() {
    }

    public zzak(String str) {
        setURI(URI.create(str));
    }

    public final String getMethod() {
        return HttpClientStack.HttpPatch.METHOD_NAME;
    }
}
