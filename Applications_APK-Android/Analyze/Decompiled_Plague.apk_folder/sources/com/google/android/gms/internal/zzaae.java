package com.google.android.gms.internal;

import android.os.Parcelable;

public final class zzaae implements Parcelable.Creator<zzaad> {
    /* JADX WARN: Type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v5, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v6, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r57) {
        /*
            r56 = this;
            r0 = r57
            int r1 = com.google.android.gms.internal.zzbek.zzd(r57)
            r2 = 0
            r4 = 0
            r5 = 0
            r13 = r2
            r16 = r13
            r19 = r16
            r23 = r19
            r7 = r4
            r11 = r7
            r15 = r11
            r21 = r15
            r26 = r21
            r29 = r26
            r30 = r29
            r31 = r30
            r32 = r31
            r33 = r32
            r37 = r33
            r38 = r37
            r42 = r38
            r44 = r42
            r47 = r44
            r51 = r47
            r52 = r51
            r54 = r52
            r55 = r54
            r8 = r5
            r9 = r8
            r10 = r9
            r12 = r10
            r18 = r12
            r22 = r18
            r25 = r22
            r27 = r25
            r28 = r27
            r34 = r28
            r35 = r34
            r36 = r35
            r39 = r36
            r40 = r39
            r41 = r40
            r43 = r41
            r45 = r43
            r46 = r45
            r48 = r46
            r49 = r48
            r50 = r49
            r53 = r50
        L_0x005c:
            int r2 = r57.dataPosition()
            if (r2 >= r1) goto L_0x0186
            int r2 = r57.readInt()
            r3 = 65535(0xffff, float:9.1834E-41)
            r3 = r3 & r2
            switch(r3) {
                case 1: goto L_0x0180;
                case 2: goto L_0x017a;
                case 3: goto L_0x0174;
                case 4: goto L_0x016e;
                case 5: goto L_0x0168;
                case 6: goto L_0x0162;
                case 7: goto L_0x015c;
                case 8: goto L_0x0156;
                case 9: goto L_0x0150;
                case 10: goto L_0x014a;
                case 11: goto L_0x0144;
                case 12: goto L_0x013e;
                case 13: goto L_0x0138;
                case 14: goto L_0x0132;
                case 15: goto L_0x012c;
                case 16: goto L_0x006d;
                case 17: goto L_0x006d;
                case 18: goto L_0x0126;
                case 19: goto L_0x0120;
                case 20: goto L_0x006d;
                case 21: goto L_0x011a;
                case 22: goto L_0x0114;
                case 23: goto L_0x010e;
                case 24: goto L_0x0108;
                case 25: goto L_0x0102;
                case 26: goto L_0x00fc;
                case 27: goto L_0x006d;
                case 28: goto L_0x00f0;
                case 29: goto L_0x00ea;
                case 30: goto L_0x00e4;
                case 31: goto L_0x00de;
                case 32: goto L_0x00d8;
                case 33: goto L_0x00cd;
                case 34: goto L_0x00c8;
                case 35: goto L_0x00c3;
                case 36: goto L_0x00be;
                case 37: goto L_0x00b3;
                case 38: goto L_0x00ae;
                case 39: goto L_0x00a9;
                case 40: goto L_0x00a4;
                case 41: goto L_0x006d;
                case 42: goto L_0x009f;
                case 43: goto L_0x009a;
                case 44: goto L_0x008f;
                case 45: goto L_0x008a;
                case 46: goto L_0x0085;
                case 47: goto L_0x0080;
                case 48: goto L_0x007b;
                case 49: goto L_0x0076;
                case 50: goto L_0x0071;
                default: goto L_0x006d;
            }
        L_0x006d:
            com.google.android.gms.internal.zzbek.zzb(r0, r2)
            goto L_0x005c
        L_0x0071:
            int r55 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x005c
        L_0x0076:
            boolean r54 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x007b:
            android.os.Bundle r53 = com.google.android.gms.internal.zzbek.zzs(r0, r2)
            goto L_0x005c
        L_0x0080:
            boolean r52 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x0085:
            boolean r51 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x008a:
            java.lang.String r50 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x005c
        L_0x008f:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzaee> r3 = com.google.android.gms.internal.zzaee.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r49 = r2
            com.google.android.gms.internal.zzaee r49 = (com.google.android.gms.internal.zzaee) r49
            goto L_0x005c
        L_0x009a:
            java.lang.String r48 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x005c
        L_0x009f:
            boolean r47 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x00a4:
            java.util.ArrayList r46 = com.google.android.gms.internal.zzbek.zzac(r0, r2)
            goto L_0x005c
        L_0x00a9:
            java.lang.String r45 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x005c
        L_0x00ae:
            boolean r44 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x00b3:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzaaf> r3 = com.google.android.gms.internal.zzaaf.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r43 = r2
            com.google.android.gms.internal.zzaaf r43 = (com.google.android.gms.internal.zzaaf) r43
            goto L_0x005c
        L_0x00be:
            boolean r42 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x00c3:
            java.util.ArrayList r41 = com.google.android.gms.internal.zzbek.zzac(r0, r2)
            goto L_0x005c
        L_0x00c8:
            java.util.ArrayList r40 = com.google.android.gms.internal.zzbek.zzac(r0, r2)
            goto L_0x005c
        L_0x00cd:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzadw> r3 = com.google.android.gms.internal.zzadw.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r39 = r2
            com.google.android.gms.internal.zzadw r39 = (com.google.android.gms.internal.zzadw) r39
            goto L_0x005c
        L_0x00d8:
            boolean r38 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x00de:
            boolean r37 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x00e4:
            java.lang.String r36 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x005c
        L_0x00ea:
            java.lang.String r35 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x005c
        L_0x00f0:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzaap> r3 = com.google.android.gms.internal.zzaap.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzbek.zza(r0, r2, r3)
            r34 = r2
            com.google.android.gms.internal.zzaap r34 = (com.google.android.gms.internal.zzaap) r34
            goto L_0x005c
        L_0x00fc:
            boolean r33 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x0102:
            boolean r32 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x0108:
            boolean r31 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x010e:
            boolean r30 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x0114:
            boolean r29 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x011a:
            java.lang.String r28 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x005c
        L_0x0120:
            java.lang.String r27 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x005c
        L_0x0126:
            boolean r26 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x012c:
            java.lang.String r25 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x005c
        L_0x0132:
            long r23 = com.google.android.gms.internal.zzbek.zzi(r0, r2)
            goto L_0x005c
        L_0x0138:
            java.lang.String r22 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x005c
        L_0x013e:
            int r21 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x005c
        L_0x0144:
            long r19 = com.google.android.gms.internal.zzbek.zzi(r0, r2)
            goto L_0x005c
        L_0x014a:
            java.util.ArrayList r18 = com.google.android.gms.internal.zzbek.zzac(r0, r2)
            goto L_0x005c
        L_0x0150:
            long r16 = com.google.android.gms.internal.zzbek.zzi(r0, r2)
            goto L_0x005c
        L_0x0156:
            boolean r15 = com.google.android.gms.internal.zzbek.zzc(r0, r2)
            goto L_0x005c
        L_0x015c:
            long r13 = com.google.android.gms.internal.zzbek.zzi(r0, r2)
            goto L_0x005c
        L_0x0162:
            java.util.ArrayList r12 = com.google.android.gms.internal.zzbek.zzac(r0, r2)
            goto L_0x005c
        L_0x0168:
            int r11 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x005c
        L_0x016e:
            java.util.ArrayList r10 = com.google.android.gms.internal.zzbek.zzac(r0, r2)
            goto L_0x005c
        L_0x0174:
            java.lang.String r9 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x005c
        L_0x017a:
            java.lang.String r8 = com.google.android.gms.internal.zzbek.zzq(r0, r2)
            goto L_0x005c
        L_0x0180:
            int r7 = com.google.android.gms.internal.zzbek.zzg(r0, r2)
            goto L_0x005c
        L_0x0186:
            com.google.android.gms.internal.zzbek.zzaf(r0, r1)
            com.google.android.gms.internal.zzaad r0 = new com.google.android.gms.internal.zzaad
            r6 = r0
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r15, r16, r18, r19, r21, r22, r23, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaae.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzaad[i];
    }
}
