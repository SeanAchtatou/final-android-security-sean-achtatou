package com.google.android.gms.common.api.internal;

import java.lang.ref.WeakReference;

final class zzbj extends zzcb {
    private WeakReference<zzbd> zzfpt;

    zzbj(zzbd zzbd) {
        this.zzfpt = new WeakReference<>(zzbd);
    }

    public final void zzagu() {
        zzbd zzbd = this.zzfpt.get();
        if (zzbd != null) {
            zzbd.resume();
        }
    }
}
