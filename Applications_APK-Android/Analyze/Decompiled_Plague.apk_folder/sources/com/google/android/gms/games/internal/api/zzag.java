package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.multiplayer.InvitationBuffer;
import com.google.android.gms.games.multiplayer.Invitations;

final class zzag implements Invitations.LoadInvitationsResult {
    private /* synthetic */ Status zzekv;

    zzag(zzaf zzaf, Status status) {
        this.zzekv = status;
    }

    public final InvitationBuffer getInvitations() {
        return new InvitationBuffer(DataHolder.zzca(14));
    }

    public final Status getStatus() {
        return this.zzekv;
    }

    public final void release() {
    }
}
