package com.google.android.gms.internal;

import android.util.Log;
import com.google.ads.AdRequest;
import com.google.android.gms.ads.internal.zzbs;

@zzzb
public final class zzafj extends zzaiw {
    public static void v(String str) {
        if (zzpt()) {
            Log.v(AdRequest.LOGTAG, str);
        }
    }

    public static boolean zzpt() {
        if (!zzae(2)) {
            return false;
        }
        return ((Boolean) zzbs.zzep().zzd(zzmq.zzbkx)).booleanValue();
    }
}
