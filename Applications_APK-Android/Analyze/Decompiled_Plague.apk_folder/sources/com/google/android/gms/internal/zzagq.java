package com.google.android.gms.internal;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class zzagq implements ThreadFactory {
    private final AtomicInteger zzcvo = new AtomicInteger(1);
    private /* synthetic */ String zzczb;

    zzagq(String str) {
        this.zzczb = str;
    }

    public final Thread newThread(Runnable runnable) {
        String str = this.zzczb;
        int andIncrement = this.zzcvo.getAndIncrement();
        StringBuilder sb = new StringBuilder(23 + String.valueOf(str).length());
        sb.append("AdWorker(");
        sb.append(str);
        sb.append(") #");
        sb.append(andIncrement);
        return new Thread(runnable, sb.toString());
    }
}
