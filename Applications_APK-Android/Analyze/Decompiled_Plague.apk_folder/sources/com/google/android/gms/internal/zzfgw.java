package com.google.android.gms.internal;

public enum zzfgw {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf(0.0d)),
    BOOLEAN(false),
    STRING(""),
    BYTE_STRING(zzfdh.zzpal),
    ENUM(null),
    MESSAGE(null);
    
    private final Object zzpgp;

    private zzfgw(Object obj) {
        this.zzpgp = obj;
    }
}
