package com.google.android.gms.common.api.internal;

import android.support.annotation.WorkerThread;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;

final class zzdj implements Runnable {
    private /* synthetic */ Result zzfsk;
    private /* synthetic */ zzdi zzfsl;

    zzdj(zzdi zzdi, Result result) {
        this.zzfsl = zzdi;
        this.zzfsk = result;
    }

    @WorkerThread
    public final void run() {
        try {
            zzs.zzfly.set(true);
            this.zzfsl.zzfsi.sendMessage(this.zzfsl.zzfsi.obtainMessage(0, this.zzfsl.zzfsd.onSuccess(this.zzfsk)));
            zzs.zzfly.set(false);
            zzdi.zzd(this.zzfsk);
            GoogleApiClient googleApiClient = (GoogleApiClient) this.zzfsl.zzfmb.get();
            if (googleApiClient != null) {
                googleApiClient.zzb(this.zzfsl);
            }
        } catch (RuntimeException e) {
            this.zzfsl.zzfsi.sendMessage(this.zzfsl.zzfsi.obtainMessage(1, e));
            zzs.zzfly.set(false);
            zzdi.zzd(this.zzfsk);
            GoogleApiClient googleApiClient2 = (GoogleApiClient) this.zzfsl.zzfmb.get();
            if (googleApiClient2 != null) {
                googleApiClient2.zzb(this.zzfsl);
            }
        } catch (Throwable th) {
            zzs.zzfly.set(false);
            zzdi.zzd(this.zzfsk);
            GoogleApiClient googleApiClient3 = (GoogleApiClient) this.zzfsl.zzfmb.get();
            if (googleApiClient3 != null) {
                googleApiClient3.zzb(this.zzfsl);
            }
            throw th;
        }
    }
}
