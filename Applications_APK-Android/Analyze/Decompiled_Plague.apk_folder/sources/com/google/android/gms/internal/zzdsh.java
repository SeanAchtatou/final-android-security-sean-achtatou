package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

final class zzdsh extends zzdsf {
    private zzdsh(zzdsa zzdsa) {
        super(zzdsa);
    }

    /* access modifiers changed from: package-private */
    public final byte[] zza(byte[] bArr, ByteBuffer byteBuffer) {
        int zzga = zzdsf.zzfz(bArr.length);
        int remaining = byteBuffer.remaining();
        int zzga2 = zzdsf.zzfz(remaining) + zzga;
        ByteBuffer order = ByteBuffer.allocate(zzga2 + 16).order(ByteOrder.LITTLE_ENDIAN);
        order.put(bArr);
        order.position(zzga);
        order.put(byteBuffer);
        order.position(zzga2);
        order.putLong((long) bArr.length);
        order.putLong((long) remaining);
        return order.array();
    }
}
