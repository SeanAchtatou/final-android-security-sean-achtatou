package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.DriveId;

public final class zzbkm implements Parcelable.Creator<zzbkl> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        DriveId driveId = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                zzbek.zzb(parcel, readInt);
            } else {
                driveId = (DriveId) zzbek.zza(parcel, readInt, DriveId.CREATOR);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbkl(driveId);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbkl[i];
    }
}
