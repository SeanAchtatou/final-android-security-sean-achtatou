package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.view.View;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzm;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.event.Events;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzad;
import com.google.android.gms.games.internal.api.zzah;
import com.google.android.gms.games.internal.api.zzax;
import com.google.android.gms.games.internal.api.zzay;
import com.google.android.gms.games.internal.api.zzaz;
import com.google.android.gms.games.internal.api.zzbj;
import com.google.android.gms.games.internal.api.zzbu;
import com.google.android.gms.games.internal.api.zzbv;
import com.google.android.gms.games.internal.api.zzcd;
import com.google.android.gms.games.internal.api.zzcr;
import com.google.android.gms.games.internal.api.zzcs;
import com.google.android.gms.games.internal.api.zzcw;
import com.google.android.gms.games.internal.api.zzdt;
import com.google.android.gms.games.internal.api.zzo;
import com.google.android.gms.games.internal.api.zzq;
import com.google.android.gms.games.internal.api.zzy;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.games.multiplayer.Invitations;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;
import com.google.android.gms.games.quest.Quests;
import com.google.android.gms.games.request.Requests;
import com.google.android.gms.games.snapshot.Snapshots;
import com.google.android.gms.games.stats.Stats;
import com.google.android.gms.games.video.Videos;
import com.google.android.gms.internal.zzcaa;
import com.google.android.gms.internal.zzcai;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@KeepForSdk
public final class Games {
    @Deprecated
    public static final Api<GamesOptions> API = new Api<>("Games.API", zzdyi, zzdyh);
    @Deprecated
    public static final Achievements Achievements = new com.google.android.gms.games.internal.api.zza();
    public static final String EXTRA_PLAYER_IDS = "players";
    public static final String EXTRA_STATUS = "status";
    @Deprecated
    public static final Events Events = new zzq();
    @Deprecated
    public static final GamesMetadata GamesMetadata = new zzy();
    @Deprecated
    public static final Invitations Invitations = new zzad();
    @Deprecated
    public static final Leaderboards Leaderboards = new zzah();
    @Deprecated
    public static final Notifications Notifications = new zzay();
    @Deprecated
    public static final Players Players = new zzaz();
    @Deprecated
    public static final Quests Quests = new zzbj();
    @Deprecated
    public static final RealTimeMultiplayer RealTimeMultiplayer = new zzbu();
    @Deprecated
    public static final Requests Requests = new zzbv();
    public static final Scope SCOPE_GAMES = new Scope(Scopes.GAMES);
    public static final Scope SCOPE_GAMES_LITE = new Scope("https://www.googleapis.com/auth/games_lite");
    @Deprecated
    public static final Snapshots Snapshots = new zzcd();
    @Deprecated
    public static final Stats Stats = new zzcs();
    @Deprecated
    public static final TurnBasedMultiplayer TurnBasedMultiplayer = new zzcw();
    @Deprecated
    public static final Videos Videos = new zzdt();
    static final Api.zzf<GamesClientImpl> zzdyh = new Api.zzf<>();
    private static final Api.zza<GamesClientImpl, GamesOptions> zzdyi = new zzi();
    private static final Api.zza<GamesClientImpl, GamesOptions> zzhhn = new zzj();
    public static final Scope zzhho = new Scope("https://www.googleapis.com/auth/games.firstparty");
    private static Api<GamesOptions> zzhhp = new Api<>("Games.API_1P", zzhhn, zzdyh);
    private static zzcaa zzhhq = new zzo();
    private static Multiplayer zzhhr = new zzax();
    private static zzcai zzhhs = new zzcr();

    @Deprecated
    public static final class GamesOptions implements GoogleSignInOptionsExtension, Api.ApiOptions.HasGoogleSignInAccountOptions, Api.ApiOptions.Optional {
        public final boolean zzhhu;
        public final boolean zzhhv;
        public final int zzhhw;
        public final boolean zzhhx;
        public final int zzhhy;
        public final String zzhhz;
        public final ArrayList<String> zzhia;
        public final boolean zzhib;
        public final boolean zzhic;
        public final boolean zzhid;
        public final GoogleSignInAccount zzhie;

        @Deprecated
        public static final class Builder {
            private boolean zzhhu;
            private boolean zzhhv;
            private int zzhhw;
            private boolean zzhhx;
            private int zzhhy;
            private String zzhhz;
            private ArrayList<String> zzhia;
            private boolean zzhib;
            private boolean zzhic;
            private boolean zzhid;
            GoogleSignInAccount zzhie;

            private Builder() {
                this.zzhhu = false;
                this.zzhhv = true;
                this.zzhhw = 17;
                this.zzhhx = false;
                this.zzhhy = 4368;
                this.zzhhz = null;
                this.zzhia = new ArrayList<>();
                this.zzhib = false;
                this.zzhic = false;
                this.zzhid = false;
                this.zzhie = null;
            }

            private Builder(GamesOptions gamesOptions) {
                this.zzhhu = false;
                this.zzhhv = true;
                this.zzhhw = 17;
                this.zzhhx = false;
                this.zzhhy = 4368;
                this.zzhhz = null;
                this.zzhia = new ArrayList<>();
                this.zzhib = false;
                this.zzhic = false;
                this.zzhid = false;
                this.zzhie = null;
                if (gamesOptions != null) {
                    this.zzhhu = gamesOptions.zzhhu;
                    this.zzhhv = gamesOptions.zzhhv;
                    this.zzhhw = gamesOptions.zzhhw;
                    this.zzhhx = gamesOptions.zzhhx;
                    this.zzhhy = gamesOptions.zzhhy;
                    this.zzhhz = gamesOptions.zzhhz;
                    this.zzhia = gamesOptions.zzhia;
                    this.zzhib = gamesOptions.zzhib;
                    this.zzhic = gamesOptions.zzhic;
                    this.zzhid = gamesOptions.zzhid;
                    this.zzhie = gamesOptions.zzhie;
                }
            }

            /* synthetic */ Builder(GamesOptions gamesOptions, zzi zzi) {
                this((GamesOptions) null);
            }

            /* synthetic */ Builder(zzi zzi) {
                this();
            }

            public final GamesOptions build() {
                return new GamesOptions(this.zzhhu, this.zzhhv, this.zzhhw, this.zzhhx, this.zzhhy, this.zzhhz, this.zzhia, this.zzhib, this.zzhic, this.zzhid, this.zzhie, null);
            }

            public final Builder setSdkVariant(int i) {
                this.zzhhy = i;
                return this;
            }

            public final Builder setShowConnectingPopup(boolean z) {
                this.zzhhv = z;
                this.zzhhw = 17;
                return this;
            }

            public final Builder setShowConnectingPopup(boolean z, int i) {
                this.zzhhv = z;
                this.zzhhw = i;
                return this;
            }
        }

        private GamesOptions(boolean z, boolean z2, int i, boolean z3, int i2, String str, ArrayList<String> arrayList, boolean z4, boolean z5, boolean z6, GoogleSignInAccount googleSignInAccount) {
            this.zzhhu = z;
            this.zzhhv = z2;
            this.zzhhw = i;
            this.zzhhx = z3;
            this.zzhhy = i2;
            this.zzhhz = str;
            this.zzhia = arrayList;
            this.zzhib = z4;
            this.zzhic = z5;
            this.zzhid = z6;
            this.zzhie = googleSignInAccount;
        }

        /* synthetic */ GamesOptions(boolean z, boolean z2, int i, boolean z3, int i2, String str, ArrayList arrayList, boolean z4, boolean z5, boolean z6, GoogleSignInAccount googleSignInAccount, zzi zzi) {
            this(z, z2, i, z3, i2, str, arrayList, z4, z5, z6, googleSignInAccount);
        }

        public static Builder builder() {
            return new Builder((zzi) null);
        }

        public static Builder zza(@NonNull GoogleSignInAccount googleSignInAccount, @Nullable GamesOptions gamesOptions) {
            Builder builder = new Builder(null, null);
            builder.zzhie = googleSignInAccount;
            return builder;
        }

        public final boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof GamesOptions)) {
                return false;
            }
            GamesOptions gamesOptions = (GamesOptions) obj;
            if (this.zzhhu == gamesOptions.zzhhu && this.zzhhv == gamesOptions.zzhhv && this.zzhhw == gamesOptions.zzhhw && this.zzhhx == gamesOptions.zzhhx && this.zzhhy == gamesOptions.zzhhy && (this.zzhhz != null ? this.zzhhz.equals(gamesOptions.zzhhz) : gamesOptions.zzhhz == null) && this.zzhia.equals(gamesOptions.zzhia) && this.zzhib == gamesOptions.zzhib && this.zzhic == gamesOptions.zzhic && this.zzhid == gamesOptions.zzhid) {
                if (this.zzhie == null) {
                    if (gamesOptions.zzhie == null) {
                        return true;
                    }
                } else if (this.zzhie.equals(gamesOptions.zzhie)) {
                    return true;
                }
            }
            return false;
        }

        public final int getExtensionType() {
            return 1;
        }

        public final GoogleSignInAccount getGoogleSignInAccount() {
            return this.zzhie;
        }

        public final List<Scope> getImpliedScopes() {
            return Collections.singletonList(this.zzhib ? Games.SCOPE_GAMES : Games.SCOPE_GAMES_LITE);
        }

        public final int hashCode() {
            int i = 0;
            int hashCode = (((((((((((((((((((true + (this.zzhhu ? 1 : 0)) * 31) + (this.zzhhv ? 1 : 0)) * 31) + this.zzhhw) * 31) + (this.zzhhx ? 1 : 0)) * 31) + this.zzhhy) * 31) + (this.zzhhz == null ? 0 : this.zzhhz.hashCode())) * 31) + this.zzhia.hashCode()) * 31) + (this.zzhib ? 1 : 0)) * 31) + (this.zzhic ? 1 : 0)) * 31) + (this.zzhid ? 1 : 0)) * 31;
            if (this.zzhie != null) {
                i = this.zzhie.hashCode();
            }
            return hashCode + i;
        }

        public final Bundle toBundle() {
            return zzaqw();
        }

        public final Bundle zzaqw() {
            Bundle bundle = new Bundle();
            bundle.putBoolean("com.google.android.gms.games.key.isHeadless", this.zzhhu);
            bundle.putBoolean("com.google.android.gms.games.key.showConnectingPopup", this.zzhhv);
            bundle.putInt("com.google.android.gms.games.key.connectingPopupGravity", this.zzhhw);
            bundle.putBoolean("com.google.android.gms.games.key.retryingSignIn", this.zzhhx);
            bundle.putInt("com.google.android.gms.games.key.sdkVariant", this.zzhhy);
            bundle.putString("com.google.android.gms.games.key.forceResolveAccountKey", this.zzhhz);
            bundle.putStringArrayList("com.google.android.gms.games.key.proxyApis", this.zzhia);
            bundle.putBoolean("com.google.android.gms.games.key.requireGooglePlus", this.zzhib);
            bundle.putBoolean("com.google.android.gms.games.key.unauthenticated", this.zzhic);
            bundle.putBoolean("com.google.android.gms.games.key.skipWelcomePopup", this.zzhid);
            return bundle;
        }
    }

    @KeepForSdk
    @Deprecated
    public interface GetServerAuthCodeResult extends Result {
        @KeepForSdk
        String getCode();
    }

    public static abstract class zza<R extends Result> extends zzm<R, GamesClientImpl> {
        public zza(GoogleApiClient googleApiClient) {
            super(Games.zzdyh, googleApiClient);
        }

        public final /* bridge */ /* synthetic */ void setResult(Object obj) {
            super.setResult((Result) obj);
        }
    }

    static abstract class zzb extends Api.zza<GamesClientImpl, GamesOptions> {
        private zzb() {
        }

        /* synthetic */ zzb(zzi zzi) {
            this();
        }

        public final int getPriority() {
            return 1;
        }

        public final /* synthetic */ Api.zze zza(Context context, Looper looper, zzr zzr, Object obj, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
            GamesOptions gamesOptions = (GamesOptions) obj;
            if (gamesOptions == null) {
                gamesOptions = new GamesOptions.Builder((zzi) null).build();
            }
            return new GamesClientImpl(context, looper, zzr, gamesOptions, connectionCallbacks, onConnectionFailedListener);
        }
    }

    static abstract class zzc extends zza<GetServerAuthCodeResult> {
        private zzc(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        /* synthetic */ zzc(GoogleApiClient googleApiClient, zzi zzi) {
            this(googleApiClient);
        }

        public final /* synthetic */ Result zzb(Status status) {
            return new zzm(this, status);
        }
    }

    static abstract class zzd extends zza<Status> {
        private zzd(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        /* synthetic */ zzd(GoogleApiClient googleApiClient, zzi zzi) {
            this(googleApiClient);
        }

        public final /* synthetic */ Result zzb(Status status) {
            return status;
        }
    }

    private Games() {
    }

    public static AchievementsClient getAchievementsClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new AchievementsClient(activity, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static AchievementsClient getAchievementsClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new AchievementsClient(context, GamesOptions.zza(googleSignInAccount, null).build());
    }

    @Deprecated
    public static String getAppId(GoogleApiClient googleApiClient) {
        return zza(googleApiClient, true).zzasc();
    }

    @RequiresPermission("android.permission.GET_ACCOUNTS")
    @Deprecated
    public static String getCurrentAccountName(GoogleApiClient googleApiClient) {
        return zza(googleApiClient, true).zzarh();
    }

    public static EventsClient getEventsClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new EventsClient(activity, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static EventsClient getEventsClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new EventsClient(context, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static GamesClient getGamesClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new GamesClient(activity, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static GamesClient getGamesClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new GamesClient(context, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static GamesMetadataClient getGamesMetadataClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new GamesMetadataClient(activity, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static GamesMetadataClient getGamesMetadataClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new GamesMetadataClient(context, GamesOptions.zza(googleSignInAccount, null).build());
    }

    @KeepForSdk
    @Deprecated
    public static PendingResult<GetServerAuthCodeResult> getGamesServerAuthCode(GoogleApiClient googleApiClient, String str) {
        zzbq.zzh(str, "Please provide a valid serverClientId");
        return googleApiClient.zze(new zzk(googleApiClient, str));
    }

    public static InvitationsClient getInvitationsClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new InvitationsClient(activity, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static InvitationsClient getInvitationsClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new InvitationsClient(context, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static LeaderboardsClient getLeaderboardsClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new LeaderboardsClient(activity, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static LeaderboardsClient getLeaderboardsClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new LeaderboardsClient(context, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static NotificationsClient getNotificationsClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new NotificationsClient(activity, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static NotificationsClient getNotificationsClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new NotificationsClient(context, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static PlayerStatsClient getPlayerStatsClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new PlayerStatsClient(activity, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static PlayerStatsClient getPlayerStatsClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new PlayerStatsClient(context, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static PlayersClient getPlayersClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new PlayersClient(activity, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static PlayersClient getPlayersClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new PlayersClient(context, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static RealTimeMultiplayerClient getRealTimeMultiplayerClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new RealTimeMultiplayerClient(activity, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static RealTimeMultiplayerClient getRealTimeMultiplayerClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new RealTimeMultiplayerClient(context, GamesOptions.zza(googleSignInAccount, null).build());
    }

    @Deprecated
    public static int getSdkVariant(GoogleApiClient googleApiClient) {
        return zza(googleApiClient, true).zzasb();
    }

    @Deprecated
    public static Intent getSettingsIntent(GoogleApiClient googleApiClient) {
        return zza(googleApiClient, true).zzarz();
    }

    public static SnapshotsClient getSnapshotsClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new SnapshotsClient(activity, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static SnapshotsClient getSnapshotsClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new SnapshotsClient(context, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static TurnBasedMultiplayerClient getTurnBasedMultiplayerClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new TurnBasedMultiplayerClient(activity, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static TurnBasedMultiplayerClient getTurnBasedMultiplayerClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new TurnBasedMultiplayerClient(context, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static VideosClient getVideosClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new VideosClient(activity, GamesOptions.zza(googleSignInAccount, null).build());
    }

    public static VideosClient getVideosClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount, "GoogleSignInAccount must not be null");
        return new VideosClient(context, GamesOptions.zza(googleSignInAccount, null).build());
    }

    @Deprecated
    public static void setGravityForPopups(GoogleApiClient googleApiClient, int i) {
        GamesClientImpl zza2 = zza(googleApiClient, false);
        if (zza2 != null) {
            zza2.zzdi(i);
        }
    }

    @Deprecated
    public static void setViewForPopups(GoogleApiClient googleApiClient, View view) {
        zzbq.checkNotNull(view);
        GamesClientImpl zza2 = zza(googleApiClient, false);
        if (zza2 != null) {
            zza2.zzu(view);
        }
    }

    @Deprecated
    public static PendingResult<Status> signOut(GoogleApiClient googleApiClient) {
        return googleApiClient.zze(new zzl(googleApiClient));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public static GamesClientImpl zza(GoogleApiClient googleApiClient, boolean z) {
        zzbq.checkArgument(googleApiClient != null, "GoogleApiClient parameter is required.");
        zzbq.zza(googleApiClient.isConnected(), (Object) "GoogleApiClient must be connected.");
        return zzb(googleApiClient, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public static GamesClientImpl zzb(GoogleApiClient googleApiClient, boolean z) {
        zzbq.zza(googleApiClient.zza(API), (Object) "GoogleApiClient is not configured to use the Games Api. Pass Games.API into GoogleApiClient.Builder#addApi() to use this feature.");
        boolean hasConnectedApi = googleApiClient.hasConnectedApi(API);
        if (z && !hasConnectedApi) {
            throw new IllegalStateException("GoogleApiClient has an optional Games.API and is not connected to Games. Use GoogleApiClient.hasConnectedApi(Games.API) to guard this call.");
        } else if (hasConnectedApi) {
            return (GamesClientImpl) googleApiClient.zza(zzdyh);
        } else {
            return null;
        }
    }

    public static GamesClientImpl zzg(GoogleApiClient googleApiClient) {
        return zza(googleApiClient, true);
    }
}
