package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbek;

public final class zzu implements Parcelable.Creator<zzt> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        while (parcel.dataPosition() < zzd) {
            zzbek.zzb(parcel, parcel.readInt());
        }
        zzbek.zzaf(parcel, zzd);
        return new zzt();
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzt[i];
    }
}
