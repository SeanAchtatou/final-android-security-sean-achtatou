package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbr;

public final class zzcwm extends zzbej {
    public static final Parcelable.Creator<zzcwm> CREATOR = new zzcwn();
    private int zzdzm;
    private zzbr zzjzi;

    zzcwm(int i, zzbr zzbr) {
        this.zzdzm = i;
        this.zzjzi = zzbr;
    }

    public zzcwm(zzbr zzbr) {
        this(1, zzbr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.internal.zzbr, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 1, this.zzdzm);
        zzbem.zza(parcel, 2, (Parcelable) this.zzjzi, i, false);
        zzbem.zzai(parcel, zze);
    }
}
