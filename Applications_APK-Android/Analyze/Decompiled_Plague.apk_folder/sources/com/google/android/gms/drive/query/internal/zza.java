package com.google.android.gms.drive.query.internal;

import com.google.android.gms.drive.query.Filter;
import com.google.android.gms.drive.query.zzd;
import com.google.android.gms.internal.zzbej;

public abstract class zza extends zzbej implements Filter {
    public String toString() {
        return String.format("Filter[%s]", zza(new zzd()));
    }
}
