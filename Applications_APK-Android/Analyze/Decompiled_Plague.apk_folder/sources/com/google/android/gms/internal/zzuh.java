package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public final class zzuh extends zzed implements zzuf {
    zzuh(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
    }

    public final void destroy() throws RemoteException {
        zzb(5, zzaz());
    }

    public final Bundle getInterstitialAdapterInfo() throws RemoteException {
        Parcel zza = zza(18, zzaz());
        Bundle bundle = (Bundle) zzef.zza(zza, Bundle.CREATOR);
        zza.recycle();
        return bundle;
    }

    public final zzku getVideoController() throws RemoteException {
        Parcel zza = zza(26, zzaz());
        zzku zzh = zzkv.zzh(zza.readStrongBinder());
        zza.recycle();
        return zzh;
    }

    public final IObjectWrapper getView() throws RemoteException {
        Parcel zza = zza(2, zzaz());
        IObjectWrapper zzap = IObjectWrapper.zza.zzap(zza.readStrongBinder());
        zza.recycle();
        return zzap;
    }

    public final boolean isInitialized() throws RemoteException {
        Parcel zza = zza(13, zzaz());
        boolean zza2 = zzef.zza(zza);
        zza.recycle();
        return zza2;
    }

    public final void pause() throws RemoteException {
        zzb(8, zzaz());
    }

    public final void resume() throws RemoteException {
        zzb(9, zzaz());
    }

    public final void setImmersiveMode(boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, z);
        zzb(25, zzaz);
    }

    public final void showInterstitial() throws RemoteException {
        zzb(4, zzaz());
    }

    public final void showVideo() throws RemoteException {
        zzb(12, zzaz());
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzads zzads, List<String> list) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        zzef.zza(zzaz, zzads);
        zzaz.writeStringList(list);
        zzb(23, zzaz);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzis zzis, String str, zzads zzads, String str2) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        zzef.zza(zzaz, zzis);
        zzaz.writeString(str);
        zzef.zza(zzaz, zzads);
        zzaz.writeString(str2);
        zzb(10, zzaz);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzis zzis, String str, zzui zzui) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        zzef.zza(zzaz, zzis);
        zzaz.writeString(str);
        zzef.zza(zzaz, zzui);
        zzb(3, zzaz);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzis zzis, String str, String str2, zzui zzui) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        zzef.zza(zzaz, zzis);
        zzaz.writeString(str);
        zzaz.writeString(str2);
        zzef.zza(zzaz, zzui);
        zzb(7, zzaz);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzis zzis, String str, String str2, zzui zzui, zzom zzom, List<String> list) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        zzef.zza(zzaz, zzis);
        zzaz.writeString(str);
        zzaz.writeString(str2);
        zzef.zza(zzaz, zzui);
        zzef.zza(zzaz, zzom);
        zzaz.writeStringList(list);
        zzb(14, zzaz);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zziw zziw, zzis zzis, String str, zzui zzui) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        zzef.zza(zzaz, zziw);
        zzef.zza(zzaz, zzis);
        zzaz.writeString(str);
        zzef.zza(zzaz, zzui);
        zzb(1, zzaz);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zziw zziw, zzis zzis, String str, String str2, zzui zzui) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        zzef.zza(zzaz, zziw);
        zzef.zza(zzaz, zzis);
        zzaz.writeString(str);
        zzaz.writeString(str2);
        zzef.zza(zzaz, zzui);
        zzb(6, zzaz);
    }

    public final void zza(zzis zzis, String str, String str2) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzis);
        zzaz.writeString(str);
        zzaz.writeString(str2);
        zzb(20, zzaz);
    }

    public final void zzc(zzis zzis, String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzis);
        zzaz.writeString(str);
        zzb(11, zzaz);
    }

    public final void zzg(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        zzb(21, zzaz);
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzuo zzly() throws android.os.RemoteException {
        /*
            r4 = this;
            android.os.Parcel r0 = r4.zzaz()
            r1 = 15
            android.os.Parcel r0 = r4.zza(r1, r0)
            android.os.IBinder r1 = r0.readStrongBinder()
            if (r1 != 0) goto L_0x0012
            r1 = 0
            goto L_0x0026
        L_0x0012:
            java.lang.String r2 = "com.google.android.gms.ads.internal.mediation.client.INativeAppInstallAdMapper"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r3 = r2 instanceof com.google.android.gms.internal.zzuo
            if (r3 == 0) goto L_0x0020
            r1 = r2
            com.google.android.gms.internal.zzuo r1 = (com.google.android.gms.internal.zzuo) r1
            goto L_0x0026
        L_0x0020:
            com.google.android.gms.internal.zzuq r2 = new com.google.android.gms.internal.zzuq
            r2.<init>(r1)
            r1 = r2
        L_0x0026:
            r0.recycle()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzuh.zzly():com.google.android.gms.internal.zzuo");
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.zzur zzlz() throws android.os.RemoteException {
        /*
            r4 = this;
            android.os.Parcel r0 = r4.zzaz()
            r1 = 16
            android.os.Parcel r0 = r4.zza(r1, r0)
            android.os.IBinder r1 = r0.readStrongBinder()
            if (r1 != 0) goto L_0x0012
            r1 = 0
            goto L_0x0026
        L_0x0012:
            java.lang.String r2 = "com.google.android.gms.ads.internal.mediation.client.INativeContentAdMapper"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r3 = r2 instanceof com.google.android.gms.internal.zzur
            if (r3 == 0) goto L_0x0020
            r1 = r2
            com.google.android.gms.internal.zzur r1 = (com.google.android.gms.internal.zzur) r1
            goto L_0x0026
        L_0x0020:
            com.google.android.gms.internal.zzut r2 = new com.google.android.gms.internal.zzut
            r2.<init>(r1)
            r1 = r2
        L_0x0026:
            r0.recycle()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzuh.zzlz():com.google.android.gms.internal.zzur");
    }

    public final Bundle zzma() throws RemoteException {
        Parcel zza = zza(17, zzaz());
        Bundle bundle = (Bundle) zzef.zza(zza, Bundle.CREATOR);
        zza.recycle();
        return bundle;
    }

    public final Bundle zzmb() throws RemoteException {
        Parcel zza = zza(19, zzaz());
        Bundle bundle = (Bundle) zzef.zza(zza, Bundle.CREATOR);
        zza.recycle();
        return bundle;
    }

    public final boolean zzmc() throws RemoteException {
        Parcel zza = zza(22, zzaz());
        boolean zza2 = zzef.zza(zza);
        zza.recycle();
        return zza2;
    }

    public final zzpu zzmd() throws RemoteException {
        Parcel zza = zza(24, zzaz());
        zzpu zzn = zzpv.zzn(zza.readStrongBinder());
        zza.recycle();
        return zzn;
    }
}
