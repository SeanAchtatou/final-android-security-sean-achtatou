package com.google.android.gms.internal;

import java.io.IOException;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class zzfgz extends zzfgx {
    zzfgz(String str, int i) {
        super(str, 1);
    }

    /* access modifiers changed from: package-private */
    public final Object zzb(zzfdq zzfdq) throws IOException {
        return zzfdq.zzctz();
    }
}
