package com.google.android.gms.games.internal.api;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.games.Games;

public class zzp extends GoogleApi<Games.GamesOptions> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$zza):void
     arg types: [android.app.Activity, com.google.android.gms.common.api.Api<com.google.android.gms.games.Games$GamesOptions>, com.google.android.gms.games.Games$GamesOptions, com.google.android.gms.common.api.GoogleApi$zza]
     candidates:
      com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.internal.zzdb):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.content.Context, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$zza):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.content.Context, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.internal.zzdb):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$zza):void */
    protected zzp(@NonNull Activity activity, @Nullable Games.GamesOptions gamesOptions) {
        super(activity, (Api) Games.API, (Api.ApiOptions) gamesOptions, GoogleApi.zza.zzfjp);
    }

    protected zzp(@NonNull Context context, @Nullable Games.GamesOptions gamesOptions) {
        super(context, Games.API, gamesOptions, GoogleApi.zza.zzfjp);
    }
}
