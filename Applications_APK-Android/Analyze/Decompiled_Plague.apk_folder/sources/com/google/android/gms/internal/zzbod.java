package com.google.android.gms.internal;

import com.google.android.gms.drive.events.OpenFileCallback;

final /* synthetic */ class zzbod implements zzbnx {
    private final zzbpw zzgnc;

    zzbod(zzbpw zzbpw) {
        this.zzgnc = zzbpw;
    }

    public final void accept(Object obj) {
        zzbpw zzbpw = this.zzgnc;
        ((OpenFileCallback) obj).onProgress(zzbpw.zzgnz, zzbpw.zzgoa);
    }
}
