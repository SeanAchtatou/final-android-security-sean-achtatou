package com.google.android.gms.internal;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

final class zzfgb extends AbstractSet<Map.Entry<K, V>> {
    private /* synthetic */ zzffu zzped;

    private zzfgb(zzffu zzffu) {
        this.zzped = zzffu;
    }

    /* synthetic */ zzfgb(zzffu zzffu, zzffv zzffv) {
        this(zzffu);
    }

    public final /* synthetic */ boolean add(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (contains(entry)) {
            return false;
        }
        this.zzped.put((Comparable) entry.getKey(), entry.getValue());
        return true;
    }

    public final void clear() {
        this.zzped.clear();
    }

    public final boolean contains(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.zzped.get(entry.getKey());
        Object value = entry.getValue();
        if (obj2 != value) {
            return obj2 != null && obj2.equals(value);
        }
        return true;
    }

    public final Iterator<Map.Entry<K, V>> iterator() {
        return new zzfga(this.zzped, null);
    }

    public final boolean remove(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (!contains(entry)) {
            return false;
        }
        this.zzped.remove(entry.getKey());
        return true;
    }

    public final int size() {
        return this.zzped.size();
    }
}
