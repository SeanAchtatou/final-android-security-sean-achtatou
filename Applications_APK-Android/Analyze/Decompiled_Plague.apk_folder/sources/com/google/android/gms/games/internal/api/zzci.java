package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.snapshot.SnapshotContents;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;

final class zzci extends zzcp {
    private /* synthetic */ SnapshotMetadataChange zzhqq;
    private /* synthetic */ String zzhqs;
    private /* synthetic */ String zzhqt;
    private /* synthetic */ SnapshotContents zzhqu;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzci(zzcd zzcd, GoogleApiClient googleApiClient, String str, String str2, SnapshotMetadataChange snapshotMetadataChange, SnapshotContents snapshotContents) {
        super(googleApiClient, null);
        this.zzhqs = str;
        this.zzhqt = str2;
        this.zzhqq = snapshotMetadataChange;
        this.zzhqu = snapshotContents;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhqs, this.zzhqt, this.zzhqq, this.zzhqu);
    }
}
