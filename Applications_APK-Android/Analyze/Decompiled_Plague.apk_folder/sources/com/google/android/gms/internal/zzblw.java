package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.DriveFile;

final class zzblw extends zzbky {
    private /* synthetic */ zzblv zzgli;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzblw(zzblv zzblv, GoogleApiClient googleApiClient) {
        super(googleApiClient);
        this.zzgli = zzblv;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbqu(this.zzgli.getDriveId(), DriveFile.MODE_WRITE_ONLY, this.zzgli.zzglf.getRequestId()), new zzbqw(this, null));
    }
}
