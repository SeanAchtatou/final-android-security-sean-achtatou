package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

public final class zzfih extends zzfhe<zzfih> implements Cloneable {
    private byte[] zzpkc = zzfhn.zzphr;
    private String zzpkd = "";
    private byte[][] zzpke = zzfhn.zzphq;
    private boolean zzpkf = false;

    public zzfih() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzcxx */
    public zzfih clone() {
        try {
            zzfih zzfih = (zzfih) super.clone();
            if (this.zzpke != null && this.zzpke.length > 0) {
                zzfih.zzpke = (byte[][]) this.zzpke.clone();
            }
            return zzfih;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfih)) {
            return false;
        }
        zzfih zzfih = (zzfih) obj;
        if (!Arrays.equals(this.zzpkc, zzfih.zzpkc)) {
            return false;
        }
        if (this.zzpkd == null) {
            if (zzfih.zzpkd != null) {
                return false;
            }
        } else if (!this.zzpkd.equals(zzfih.zzpkd)) {
            return false;
        }
        if (zzfhi.zza(this.zzpke, zzfih.zzpke) && this.zzpkf == zzfih.zzpkf) {
            return (this.zzpgy == null || this.zzpgy.isEmpty()) ? zzfih.zzpgy == null || zzfih.zzpgy.isEmpty() : this.zzpgy.equals(zzfih.zzpgy);
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((527 + getClass().getName().hashCode()) * 31) + Arrays.hashCode(this.zzpkc)) * 31) + (this.zzpkd == null ? 0 : this.zzpkd.hashCode())) * 31) + zzfhi.zzd(this.zzpke)) * 31) + (this.zzpkf ? 1231 : 1237)) * 31;
        if (this.zzpgy != null && !this.zzpgy.isEmpty()) {
            i = this.zzpgy.hashCode();
        }
        return hashCode + i;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 10) {
                this.zzpkc = zzfhb.readBytes();
            } else if (zzcts == 18) {
                int zzb = zzfhn.zzb(zzfhb, 18);
                int length = this.zzpke == null ? 0 : this.zzpke.length;
                byte[][] bArr = new byte[(zzb + length)][];
                if (length != 0) {
                    System.arraycopy(this.zzpke, 0, bArr, 0, length);
                }
                while (length < bArr.length - 1) {
                    bArr[length] = zzfhb.readBytes();
                    zzfhb.zzcts();
                    length++;
                }
                bArr[length] = zzfhb.readBytes();
                this.zzpke = bArr;
            } else if (zzcts == 24) {
                this.zzpkf = zzfhb.zzcty();
            } else if (zzcts == 34) {
                this.zzpkd = zzfhb.readString();
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (!Arrays.equals(this.zzpkc, zzfhn.zzphr)) {
            zzfhc.zzc(1, this.zzpkc);
        }
        if (this.zzpke != null && this.zzpke.length > 0) {
            for (byte[] bArr : this.zzpke) {
                if (bArr != null) {
                    zzfhc.zzc(2, bArr);
                }
            }
        }
        if (this.zzpkf) {
            zzfhc.zzl(3, this.zzpkf);
        }
        if (this.zzpkd != null && !this.zzpkd.equals("")) {
            zzfhc.zzn(4, this.zzpkd);
        }
        super.zza(zzfhc);
    }

    public final /* synthetic */ zzfhe zzcxe() throws CloneNotSupportedException {
        return (zzfih) clone();
    }

    public final /* synthetic */ zzfhk zzcxf() throws CloneNotSupportedException {
        return (zzfih) clone();
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (!Arrays.equals(this.zzpkc, zzfhn.zzphr)) {
            zzo += zzfhc.zzd(1, this.zzpkc);
        }
        if (this.zzpke != null && this.zzpke.length > 0) {
            int i = 0;
            int i2 = 0;
            for (byte[] bArr : this.zzpke) {
                if (bArr != null) {
                    i2++;
                    i += zzfhc.zzbf(bArr);
                }
            }
            zzo = zzo + i + (i2 * 1);
        }
        if (this.zzpkf) {
            zzo += zzfhc.zzkw(3) + 1;
        }
        return (this.zzpkd == null || this.zzpkd.equals("")) ? zzo : zzo + zzfhc.zzo(4, this.zzpkd);
    }
}
