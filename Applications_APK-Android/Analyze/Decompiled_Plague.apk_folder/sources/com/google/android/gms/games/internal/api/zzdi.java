package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzdi implements TurnBasedMultiplayer.CancelMatchResult {
    private /* synthetic */ Status zzekv;
    private /* synthetic */ zzdh zzhrb;

    zzdi(zzdh zzdh, Status status) {
        this.zzhrb = zzdh;
        this.zzekv = status;
    }

    public final String getMatchId() {
        return this.zzhrb.zzbte;
    }

    public final Status getStatus() {
        return this.zzekv;
    }
}
