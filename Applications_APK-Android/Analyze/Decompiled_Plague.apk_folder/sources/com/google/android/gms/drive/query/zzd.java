package com.google.android.gms.drive.query;

import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.zzb;
import com.google.android.gms.drive.query.internal.zzj;
import com.google.android.gms.drive.query.internal.zzx;
import java.util.Iterator;
import java.util.List;

public final class zzd implements zzj<String> {
    public final /* synthetic */ Object zza(zzb zzb, Object obj) {
        return String.format("contains(%s,%s)", zzb.getName(), obj);
    }

    public final /* synthetic */ Object zza(zzx zzx, MetadataField metadataField, Object obj) {
        return String.format("cmp(%s,%s,%s)", zzx.getTag(), metadataField.getName(), obj);
    }

    public final /* synthetic */ Object zza(zzx zzx, List list) {
        StringBuilder sb = new StringBuilder(String.valueOf(zzx.getTag()).concat("("));
        String str = "";
        Iterator it = list.iterator();
        while (it.hasNext()) {
            sb.append(str);
            sb.append((String) it.next());
            str = ",";
        }
        sb.append(")");
        return sb.toString();
    }

    public final /* synthetic */ Object zzapi() {
        return "ownedByMe()";
    }

    public final /* synthetic */ Object zzapj() {
        return "all()";
    }

    public final /* synthetic */ Object zzd(MetadataField metadataField, Object obj) {
        return String.format("has(%s,%s)", metadataField.getName(), obj);
    }

    public final /* synthetic */ Object zze(MetadataField metadataField) {
        return String.format("fieldOnly(%s)", metadataField.getName());
    }

    public final /* synthetic */ Object zzgv(String str) {
        return String.format("fullTextSearch(%s)", str);
    }

    public final /* synthetic */ Object zzx(Object obj) {
        return String.format("not(%s)", (String) obj);
    }
}
