package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.zzi;
import java.util.ArrayList;

public final class zzbpx implements Parcelable.Creator<zzbpw> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        long j = 0;
        long j2 = 0;
        int i = 0;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    j = zzbek.zzi(parcel, readInt);
                    break;
                case 3:
                    j2 = zzbek.zzi(parcel, readInt);
                    break;
                case 4:
                    i = zzbek.zzg(parcel, readInt);
                    break;
                case 5:
                    arrayList = zzbek.zzc(parcel, readInt, zzi.CREATOR);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbpw(j, j2, i, arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbpw[i];
    }
}
