package com.google.android.gms.internal;

import android.content.IntentSender;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzbpg extends zzed implements zzbpf {
    zzbpg(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.drive.internal.IDriveService");
    }

    public final IntentSender zza(zzbke zzbke) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbke);
        Parcel zza = zza(11, zzaz);
        IntentSender intentSender = (IntentSender) zzef.zza(zza, IntentSender.CREATOR);
        zza.recycle();
        return intentSender;
    }

    public final IntentSender zza(zzbqx zzbqx) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbqx);
        Parcel zza = zza(10, zzaz);
        IntentSender intentSender = (IntentSender) zzef.zza(zza, IntentSender.CREATOR);
        zza.recycle();
        return intentSender;
    }

    public final zzbot zza(zzbqu zzbqu, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbqu);
        zzef.zza(zzaz, zzbph);
        Parcel zza = zza(7, zzaz);
        zzbot zzbot = (zzbot) zzef.zza(zza, zzbot.CREATOR);
        zza.recycle();
        return zzbot;
    }

    public final void zza(zzbjt zzbjt, zzbpj zzbpj, String str, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbjt);
        zzef.zza(zzaz, zzbpj);
        zzaz.writeString(null);
        zzef.zza(zzaz, zzbph);
        zzb(14, zzaz);
    }

    public final void zza(zzbjw zzbjw, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbjw);
        zzef.zza(zzaz, zzbph);
        zzb(18, zzaz);
    }

    public final void zza(zzbjy zzbjy, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbjy);
        zzef.zza(zzaz, zzbph);
        zzb(8, zzaz);
    }

    public final void zza(zzbkb zzbkb, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbkb);
        zzef.zza(zzaz, zzbph);
        zzb(4, zzaz);
    }

    public final void zza(zzbkg zzbkg, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbkg);
        zzef.zza(zzaz, zzbph);
        zzb(5, zzaz);
    }

    public final void zza(zzbki zzbki, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbki);
        zzef.zza(zzaz, zzbph);
        zzb(6, zzaz);
    }

    public final void zza(zzbkl zzbkl, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbkl);
        zzef.zza(zzaz, zzbph);
        zzb(24, zzaz);
    }

    public final void zza(zzbkn zzbkn) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbkn);
        zzb(16, zzaz);
    }

    public final void zza(zzbpb zzbpb, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbpb);
        zzef.zza(zzaz, zzbph);
        zzb(1, zzaz);
    }

    public final void zza(zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbph);
        zzb(9, zzaz);
    }

    public final void zza(zzbpo zzbpo, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbpo);
        zzef.zza(zzaz, zzbph);
        zzb(13, zzaz);
    }

    public final void zza(zzbrb zzbrb, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbrb);
        zzef.zza(zzaz, zzbph);
        zzb(2, zzaz);
    }

    public final void zza(zzbrd zzbrd, zzbpj zzbpj, String str, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbrd);
        zzef.zza(zzaz, zzbpj);
        zzaz.writeString(null);
        zzef.zza(zzaz, zzbph);
        zzb(15, zzaz);
    }

    public final void zza(zzbrf zzbrf, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbrf);
        zzef.zza(zzaz, zzbph);
        zzb(36, zzaz);
    }

    public final void zza(zzbrh zzbrh, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbrh);
        zzef.zza(zzaz, zzbph);
        zzb(28, zzaz);
    }

    public final void zza(zzbrm zzbrm, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbrm);
        zzef.zza(zzaz, zzbph);
        zzb(17, zzaz);
    }

    public final void zza(zzbro zzbro, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbro);
        zzef.zza(zzaz, zzbph);
        zzb(38, zzaz);
    }

    public final void zza(zzbrq zzbrq, zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbrq);
        zzef.zza(zzaz, zzbph);
        zzb(3, zzaz);
    }

    public final void zzb(zzbph zzbph) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbph);
        zzb(35, zzaz);
    }
}
