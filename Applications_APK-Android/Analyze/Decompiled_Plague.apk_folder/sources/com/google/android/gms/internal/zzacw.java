package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;

public abstract class zzacw extends zzee implements zzacv {
    public zzacw() {
        attachInterface(this, "com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdListener");
    }

    public static zzacv zzz(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdListener");
        return queryLocalInterface instanceof zzacv ? (zzacv) queryLocalInterface : new zzacx(iBinder);
    }

    /* JADX WARN: Type inference failed for: r3v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTransact(int r2, android.os.Parcel r3, android.os.Parcel r4, int r5) throws android.os.RemoteException {
        /*
            r1 = this;
            boolean r5 = r1.zza(r2, r3, r4, r5)
            r0 = 1
            if (r5 == 0) goto L_0x0008
            return r0
        L_0x0008:
            switch(r2) {
                case 1: goto L_0x0045;
                case 2: goto L_0x0041;
                case 3: goto L_0x003d;
                case 4: goto L_0x0039;
                case 5: goto L_0x0019;
                case 6: goto L_0x0015;
                case 7: goto L_0x000d;
                default: goto L_0x000b;
            }
        L_0x000b:
            r2 = 0
            return r2
        L_0x000d:
            int r2 = r3.readInt()
            r1.onRewardedVideoAdFailedToLoad(r2)
            goto L_0x0048
        L_0x0015:
            r1.onRewardedVideoAdLeftApplication()
            goto L_0x0048
        L_0x0019:
            android.os.IBinder r2 = r3.readStrongBinder()
            if (r2 != 0) goto L_0x0021
            r2 = 0
            goto L_0x0035
        L_0x0021:
            java.lang.String r3 = "com.google.android.gms.ads.internal.reward.client.IRewardItem"
            android.os.IInterface r3 = r2.queryLocalInterface(r3)
            boolean r5 = r3 instanceof com.google.android.gms.internal.zzacn
            if (r5 == 0) goto L_0x002f
            r2 = r3
            com.google.android.gms.internal.zzacn r2 = (com.google.android.gms.internal.zzacn) r2
            goto L_0x0035
        L_0x002f:
            com.google.android.gms.internal.zzacp r3 = new com.google.android.gms.internal.zzacp
            r3.<init>(r2)
            r2 = r3
        L_0x0035:
            r1.zza(r2)
            goto L_0x0048
        L_0x0039:
            r1.onRewardedVideoAdClosed()
            goto L_0x0048
        L_0x003d:
            r1.onRewardedVideoStarted()
            goto L_0x0048
        L_0x0041:
            r1.onRewardedVideoAdOpened()
            goto L_0x0048
        L_0x0045:
            r1.onRewardedVideoAdLoaded()
        L_0x0048:
            r4.writeNoException()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzacw.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
