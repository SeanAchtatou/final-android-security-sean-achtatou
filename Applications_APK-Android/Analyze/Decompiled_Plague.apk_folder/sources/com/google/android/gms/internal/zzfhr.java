package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfhr extends zzfhe<zzfhr> {
    private byte[] body = null;
    private zzfhs zzpij = null;
    public zzfhq[] zzpik = zzfhq.zzcxm();
    private byte[] zzpil = null;
    private Integer zzpim = null;

    public zzfhr() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 10) {
                if (this.zzpij == null) {
                    this.zzpij = new zzfhs();
                }
                zzfhb.zza(this.zzpij);
            } else if (zzcts == 18) {
                int zzb = zzfhn.zzb(zzfhb, 18);
                int length = this.zzpik == null ? 0 : this.zzpik.length;
                zzfhq[] zzfhqArr = new zzfhq[(zzb + length)];
                if (length != 0) {
                    System.arraycopy(this.zzpik, 0, zzfhqArr, 0, length);
                }
                while (length < zzfhqArr.length - 1) {
                    zzfhqArr[length] = new zzfhq();
                    zzfhb.zza(zzfhqArr[length]);
                    zzfhb.zzcts();
                    length++;
                }
                zzfhqArr[length] = new zzfhq();
                zzfhb.zza(zzfhqArr[length]);
                this.zzpik = zzfhqArr;
            } else if (zzcts == 26) {
                this.body = zzfhb.readBytes();
            } else if (zzcts == 34) {
                this.zzpil = zzfhb.readBytes();
            } else if (zzcts == 40) {
                this.zzpim = Integer.valueOf(zzfhb.zzctv());
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzpij != null) {
            zzfhc.zza(1, this.zzpij);
        }
        if (this.zzpik != null && this.zzpik.length > 0) {
            for (zzfhq zzfhq : this.zzpik) {
                if (zzfhq != null) {
                    zzfhc.zza(2, zzfhq);
                }
            }
        }
        if (this.body != null) {
            zzfhc.zzc(3, this.body);
        }
        if (this.zzpil != null) {
            zzfhc.zzc(4, this.zzpil);
        }
        if (this.zzpim != null) {
            zzfhc.zzaa(5, this.zzpim.intValue());
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzpij != null) {
            zzo += zzfhc.zzb(1, this.zzpij);
        }
        if (this.zzpik != null && this.zzpik.length > 0) {
            for (zzfhq zzfhq : this.zzpik) {
                if (zzfhq != null) {
                    zzo += zzfhc.zzb(2, zzfhq);
                }
            }
        }
        if (this.body != null) {
            zzo += zzfhc.zzd(3, this.body);
        }
        if (this.zzpil != null) {
            zzo += zzfhc.zzd(4, this.zzpil);
        }
        return this.zzpim != null ? zzo + zzfhc.zzad(5, this.zzpim.intValue()) : zzo;
    }
}
