package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.zzc;

public final class zzbps extends zzbej {
    public static final Parcelable.Creator<zzbps> CREATOR = new zzbpt();
    final zzc zzglf;
    final boolean zzgnw;

    public zzbps(zzc zzc, boolean z) {
        this.zzglf = zzc;
        this.zzgnw = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.zzc, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, (Parcelable) this.zzglf, i, false);
        zzbem.zza(parcel, 3, this.zzgnw);
        zzbem.zzai(parcel, zze);
    }

    public final zzc zzaot() {
        return this.zzglf;
    }
}
