package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.query.Filter;
import com.google.android.gms.internal.zzbem;
import java.util.ArrayList;
import java.util.List;

public final class zzr extends zza {
    public static final Parcelable.Creator<zzr> CREATOR = new zzs();
    private List<Filter> zzgry;
    private zzx zzgse;
    private List<FilterHolder> zzgst;

    public zzr(zzx zzx, Filter filter, Filter... filterArr) {
        this.zzgse = zzx;
        this.zzgst = new ArrayList(filterArr.length + 1);
        this.zzgst.add(new FilterHolder(filter));
        this.zzgry = new ArrayList(filterArr.length + 1);
        this.zzgry.add(filter);
        for (Filter filter2 : filterArr) {
            this.zzgst.add(new FilterHolder(filter2));
            this.zzgry.add(filter2);
        }
    }

    public zzr(zzx zzx, Iterable<Filter> iterable) {
        this.zzgse = zzx;
        this.zzgry = new ArrayList();
        this.zzgst = new ArrayList();
        for (Filter next : iterable) {
            this.zzgry.add(next);
            this.zzgst.add(new FilterHolder(next));
        }
    }

    zzr(zzx zzx, List<FilterHolder> list) {
        this.zzgse = zzx;
        this.zzgst = list;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.zzx, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, (Parcelable) this.zzgse, i, false);
        zzbem.zzc(parcel, 2, this.zzgst, false);
        zzbem.zzai(parcel, zze);
    }

    public final <T> T zza(zzj<T> zzj) {
        ArrayList arrayList = new ArrayList();
        for (FilterHolder filter : this.zzgst) {
            arrayList.add(filter.getFilter().zza(zzj));
        }
        return zzj.zza(this.zzgse, arrayList);
    }
}
