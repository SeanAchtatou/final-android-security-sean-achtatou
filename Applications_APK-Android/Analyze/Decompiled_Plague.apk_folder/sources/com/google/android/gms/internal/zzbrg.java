package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbrg implements Parcelable.Creator<zzbrf> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        zzboz zzboz = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                zzbek.zzb(parcel, readInt);
            } else {
                zzboz = (zzboz) zzbek.zza(parcel, readInt, zzboz.CREATOR);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbrf(zzboz);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbrf[i];
    }
}
