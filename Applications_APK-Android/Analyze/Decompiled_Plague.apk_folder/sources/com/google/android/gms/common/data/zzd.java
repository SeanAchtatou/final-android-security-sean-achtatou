package com.google.android.gms.common.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.internal.zzben;
import com.tapjoy.TJAdUnitConstants;

public class zzd<T extends zzben> extends AbstractDataBuffer<T> {
    private static final String[] zzftf = {TJAdUnitConstants.String.DATA};
    private final Parcelable.Creator<T> zzftg;

    public zzd(DataHolder dataHolder, Parcelable.Creator<T> creator) {
        super(dataHolder);
        this.zzftg = creator;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends com.google.android.gms.internal.zzben> void zza(com.google.android.gms.common.data.DataHolder.zza r3, T r4) {
        /*
            android.os.Parcel r0 = android.os.Parcel.obtain()
            r1 = 0
            r4.writeToParcel(r0, r1)
            android.content.ContentValues r4 = new android.content.ContentValues
            r4.<init>()
            java.lang.String r1 = "data"
            byte[] r2 = r0.marshall()
            r4.put(r1, r2)
            r3.zza(r4)
            r0.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.data.zzd.zza(com.google.android.gms.common.data.DataHolder$zza, com.google.android.gms.internal.zzben):void");
    }

    public static DataHolder.zza zzajm() {
        return DataHolder.zzb(zzftf);
    }

    /* renamed from: zzby */
    public T get(int i) {
        byte[] zzg = this.zzfnz.zzg(TJAdUnitConstants.String.DATA, i, this.zzfnz.zzbz(i));
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(zzg, 0, zzg.length);
        obtain.setDataPosition(0);
        T t = (zzben) this.zzftg.createFromParcel(obtain);
        obtain.recycle();
        return t;
    }
}
