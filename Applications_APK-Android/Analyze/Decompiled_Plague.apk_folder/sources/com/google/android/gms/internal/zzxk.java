package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.widget.PopupWindow;

@zzzb
@TargetApi(19)
public final class zzxk extends zzxh {
    private Object zzcie = new Object();
    private PopupWindow zzcif;
    private boolean zzcig = false;

    zzxk(Context context, zzaev zzaev, zzama zzama, zzxg zzxg) {
        super(context, zzaev, zzama, zzxg);
    }

    private final void zzmz() {
        synchronized (this.zzcie) {
            this.zzcig = true;
            if ((this.mContext instanceof Activity) && ((Activity) this.mContext).isDestroyed()) {
                this.zzcif = null;
            }
            if (this.zzcif != null) {
                if (this.zzcif.isShowing()) {
                    this.zzcif.dismiss();
                }
                this.zzcif = null;
            }
        }
    }

    public final void cancel() {
        zzmz();
        super.cancel();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.widget.FrameLayout, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:21|22|23|24|25) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:24:0x006a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzmy() {
        /*
            r8 = this;
            android.content.Context r0 = r8.mContext
            boolean r0 = r0 instanceof android.app.Activity
            r1 = 0
            if (r0 == 0) goto L_0x0010
            android.content.Context r0 = r8.mContext
            android.app.Activity r0 = (android.app.Activity) r0
            android.view.Window r0 = r0.getWindow()
            goto L_0x0011
        L_0x0010:
            r0 = r1
        L_0x0011:
            if (r0 == 0) goto L_0x0071
            android.view.View r2 = r0.getDecorView()
            if (r2 != 0) goto L_0x001a
            return
        L_0x001a:
            android.content.Context r2 = r8.mContext
            android.app.Activity r2 = (android.app.Activity) r2
            boolean r2 = r2.isDestroyed()
            if (r2 == 0) goto L_0x0025
            return
        L_0x0025:
            android.widget.FrameLayout r2 = new android.widget.FrameLayout
            android.content.Context r3 = r8.mContext
            r2.<init>(r3)
            android.view.ViewGroup$LayoutParams r3 = new android.view.ViewGroup$LayoutParams
            r4 = -1
            r3.<init>(r4, r4)
            r2.setLayoutParams(r3)
            com.google.android.gms.internal.zzama r3 = r8.zzbwq
            if (r3 != 0) goto L_0x003a
            throw r1
        L_0x003a:
            android.view.View r3 = (android.view.View) r3
            r2.addView(r3, r4, r4)
            java.lang.Object r3 = r8.zzcie
            monitor-enter(r3)
            boolean r5 = r8.zzcig     // Catch:{ all -> 0x006e }
            if (r5 == 0) goto L_0x0048
            monitor-exit(r3)     // Catch:{ all -> 0x006e }
            return
        L_0x0048:
            android.widget.PopupWindow r5 = new android.widget.PopupWindow     // Catch:{ all -> 0x006e }
            r6 = 0
            r7 = 1
            r5.<init>(r2, r7, r7, r6)     // Catch:{ all -> 0x006e }
            r8.zzcif = r5     // Catch:{ all -> 0x006e }
            android.widget.PopupWindow r2 = r8.zzcif     // Catch:{ all -> 0x006e }
            r2.setOutsideTouchable(r7)     // Catch:{ all -> 0x006e }
            android.widget.PopupWindow r2 = r8.zzcif     // Catch:{ all -> 0x006e }
            r2.setClippingEnabled(r6)     // Catch:{ all -> 0x006e }
            java.lang.String r2 = "Displaying the 1x1 popup off the screen."
            com.google.android.gms.internal.zzafj.zzbw(r2)     // Catch:{ all -> 0x006e }
            android.widget.PopupWindow r2 = r8.zzcif     // Catch:{ Exception -> 0x006a }
            android.view.View r0 = r0.getDecorView()     // Catch:{ Exception -> 0x006a }
            r2.showAtLocation(r0, r6, r4, r4)     // Catch:{ Exception -> 0x006a }
            goto L_0x006c
        L_0x006a:
            r8.zzcif = r1     // Catch:{ all -> 0x006e }
        L_0x006c:
            monitor-exit(r3)     // Catch:{ all -> 0x006e }
            return
        L_0x006e:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x006e }
            throw r0
        L_0x0071:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzxk.zzmy():void");
    }

    /* access modifiers changed from: protected */
    public final void zzx(int i) {
        zzmz();
        super.zzx(i);
    }
}
