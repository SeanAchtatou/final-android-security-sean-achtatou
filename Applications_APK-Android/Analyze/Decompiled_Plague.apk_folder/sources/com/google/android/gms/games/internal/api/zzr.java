package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzr extends zzu {
    private /* synthetic */ boolean zzhpv;
    private /* synthetic */ String[] zzhpy;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzr(zzq zzq, GoogleApiClient googleApiClient, boolean z, String[] strArr) {
        super(googleApiClient, null);
        this.zzhpv = z;
        this.zzhpy = strArr;
    }

    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhpv, this.zzhpy);
    }
}
