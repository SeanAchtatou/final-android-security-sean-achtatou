package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrk;
import java.security.GeneralSecurityException;

public final class zzdpe implements zzdos<zzdoo> {
    zzdpe() {
    }

    /* access modifiers changed from: private */
    /* renamed from: zzd */
    public final zzdoo zza(zzfdh zzfdh) throws GeneralSecurityException {
        try {
            zzdqa zzn = zzdqa.zzn(zzfdh);
            if (!(zzn instanceof zzdqa)) {
                throw new GeneralSecurityException("expected AesEaxKey proto");
            }
            zzdqa zzdqa = zzn;
            zzdte.zzt(zzdqa.getVersion(), 0);
            zzdte.zzgc(zzdqa.zzblt().size());
            if (zzdqa.zzbmc().zzblz() == 12 || zzdqa.zzbmc().zzblz() == 16) {
                return new zzdry(zzdqa.zzblt().toByteArray(), zzdqa.zzbmc().zzblz());
            }
            throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
        } catch (zzfew e) {
            throw new GeneralSecurityException("expected serialized AesEaxKey proto", e);
        }
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.AesEaxKey";
    }

    public final /* synthetic */ Object zza(zzffi zzffi) throws GeneralSecurityException {
        if (!(zzffi instanceof zzdqa)) {
            throw new GeneralSecurityException("expected AesEaxKey proto");
        }
        zzdqa zzdqa = (zzdqa) zzffi;
        zzdte.zzt(zzdqa.getVersion(), 0);
        zzdte.zzgc(zzdqa.zzblt().size());
        if (zzdqa.zzbmc().zzblz() == 12 || zzdqa.zzbmc().zzblz() == 16) {
            return new zzdry(zzdqa.zzblt().toByteArray(), zzdqa.zzbmc().zzblz());
        }
        throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
    }

    public final zzffi zzb(zzfdh zzfdh) throws GeneralSecurityException {
        try {
            return zzb(zzdqc.zzp(zzfdh));
        } catch (zzfew e) {
            throw new GeneralSecurityException("expected serialized AesEaxKeyFormat proto", e);
        }
    }

    public final zzffi zzb(zzffi zzffi) throws GeneralSecurityException {
        if (!(zzffi instanceof zzdqc)) {
            throw new GeneralSecurityException("expected AesEaxKeyFormat proto");
        }
        zzdqc zzdqc = (zzdqc) zzffi;
        zzdte.zzgc(zzdqc.getKeySize());
        if (zzdqc.zzbmc().zzblz() == 12 || zzdqc.zzbmc().zzblz() == 16) {
            return zzdqa.zzbmd().zzo(zzfdh.zzay(zzdtd.zzgb(zzdqc.getKeySize()))).zzb(zzdqc.zzbmc()).zzfk(0).zzcvk();
        }
        throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
    }

    public final zzdrk zzc(zzfdh zzfdh) throws GeneralSecurityException {
        return (zzdrk) zzdrk.zzbnv().zzoa("type.googleapis.com/google.crypto.tink.AesEaxKey").zzaa(((zzdqa) zzb(zzfdh)).toByteString()).zzb(zzdrk.zzb.SYMMETRIC).zzcvk();
    }
}
