package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.zzc;

public final class zzbjz implements Parcelable.Creator<zzbjy> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        zzc zzc = null;
        int i = 0;
        Boolean bool = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    zzc = (zzc) zzbek.zza(parcel, readInt, zzc.CREATOR);
                    break;
                case 3:
                    bool = zzbek.zzd(parcel, readInt);
                    break;
                case 4:
                    i = zzbek.zzg(parcel, readInt);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbjy(zzc, bool, i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbjy[i];
    }
}
