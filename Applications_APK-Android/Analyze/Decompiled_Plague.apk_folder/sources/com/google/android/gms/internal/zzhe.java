package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;

@zzzb
public final class zzhe extends zzgz {
    private MessageDigest zzazp;

    public final byte[] zzx(String str) {
        byte[] bArr;
        String[] split = str.split(" ");
        int i = 4;
        if (split.length == 1) {
            int zzz = zzhd.zzz(split[0]);
            ByteBuffer allocate = ByteBuffer.allocate(4);
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            allocate.putInt(zzz);
            bArr = allocate.array();
        } else if (split.length < 5) {
            byte[] bArr2 = new byte[(split.length << 1)];
            for (int i2 = 0; i2 < split.length; i2++) {
                int zzz2 = zzhd.zzz(split[i2]);
                int i3 = (zzz2 >> 16) ^ (65535 & zzz2);
                byte[] bArr3 = {(byte) i3, (byte) (i3 >> 8)};
                int i4 = i2 << 1;
                bArr2[i4] = bArr3[0];
                bArr2[i4 + 1] = bArr3[1];
            }
            bArr = bArr2;
        } else {
            byte[] bArr4 = new byte[split.length];
            for (int i5 = 0; i5 < split.length; i5++) {
                int zzz3 = zzhd.zzz(split[i5]);
                bArr4[i5] = (byte) ((zzz3 >> 24) ^ (((zzz3 & 255) ^ ((zzz3 >> 8) & 255)) ^ ((zzz3 >> 16) & 255)));
            }
            bArr = bArr4;
        }
        this.zzazp = zzha();
        synchronized (this.mLock) {
            if (this.zzazp == null) {
                byte[] bArr5 = new byte[0];
                return bArr5;
            }
            this.zzazp.reset();
            this.zzazp.update(bArr);
            byte[] digest = this.zzazp.digest();
            if (digest.length <= 4) {
                i = digest.length;
            }
            byte[] bArr6 = new byte[i];
            System.arraycopy(digest, 0, bArr6, 0, bArr6.length);
            return bArr6;
        }
    }
}
