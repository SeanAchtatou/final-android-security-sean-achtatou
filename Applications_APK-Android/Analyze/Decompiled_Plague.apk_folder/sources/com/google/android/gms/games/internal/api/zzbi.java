package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.PlayerBuffer;
import com.google.android.gms.games.Players;

final class zzbi implements Players.LoadPlayersResult {
    private /* synthetic */ Status zzekv;

    zzbi(zzbh zzbh, Status status) {
        this.zzekv = status;
    }

    public final PlayerBuffer getPlayers() {
        return new PlayerBuffer(DataHolder.zzca(14));
    }

    public final Status getStatus() {
        return this.zzekv;
    }

    public final void release() {
    }
}
