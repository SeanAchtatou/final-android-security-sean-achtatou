package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;

final class zzb implements zzbo<Achievements.LoadAchievementsResult, AchievementBuffer> {
    zzb() {
    }

    public final /* synthetic */ Object zzb(@Nullable Result result) {
        Achievements.LoadAchievementsResult loadAchievementsResult = (Achievements.LoadAchievementsResult) result;
        if (loadAchievementsResult == null) {
            return null;
        }
        return loadAchievementsResult.getAchievements();
    }
}
