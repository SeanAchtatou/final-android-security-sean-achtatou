package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd;
import java.util.ArrayList;
import java.util.List;

@zzzb
public final class zzox extends NativeAd.AdChoicesInfo {
    private final List<NativeAd.Image> zzbrm = new ArrayList();
    private final zzou zzbug;
    private String zzbuh;

    public zzox(zzou zzou) {
        zzoy zzoy;
        IBinder iBinder;
        this.zzbug = zzou;
        try {
            this.zzbuh = this.zzbug.getText();
        } catch (RemoteException e) {
            zzaiw.zzb("Error while obtaining attribution text.", e);
            this.zzbuh = "";
        }
        try {
            for (zzoy next : zzou.zzjg()) {
                if (!(next instanceof IBinder) || (iBinder = (IBinder) next) == null) {
                    zzoy = null;
                } else {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
                    zzoy = queryLocalInterface instanceof zzoy ? (zzoy) queryLocalInterface : new zzpa(iBinder);
                }
                if (zzoy != null) {
                    this.zzbrm.add(new zzpb(zzoy));
                }
            }
        } catch (RemoteException e2) {
            zzaiw.zzb("Error while obtaining image.", e2);
        }
    }

    public final List<NativeAd.Image> getImages() {
        return this.zzbrm;
    }

    public final CharSequence getText() {
        return this.zzbuh;
    }
}
