package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.FileUploadPreferences;
import com.google.android.gms.drive.TransferPreferences;

@Deprecated
public final class zzboz extends zzbej implements FileUploadPreferences {
    public static final Parcelable.Creator<zzboz> CREATOR = new zzbpa();
    private int zzgia;
    private int zzgnn;
    private boolean zzgno;

    public zzboz(int i, int i2, boolean z) {
        this.zzgnn = i;
        this.zzgia = i2;
        this.zzgno = z;
    }

    public zzboz(TransferPreferences transferPreferences) {
        this(transferPreferences.getNetworkPreference(), transferPreferences.getBatteryUsagePreference(), transferPreferences.isRoamingAllowed());
    }

    private static boolean zzcx(int i) {
        switch (i) {
            case 1:
            case 2:
                return true;
            default:
                return false;
        }
    }

    private static boolean zzcy(int i) {
        switch (i) {
            case 256:
            case 257:
                return true;
            default:
                return false;
        }
    }

    public final int getBatteryUsagePreference() {
        if (!zzcy(this.zzgia)) {
            return 0;
        }
        return this.zzgia;
    }

    public final int getNetworkTypePreference() {
        if (!zzcx(this.zzgnn)) {
            return 0;
        }
        return this.zzgnn;
    }

    public final boolean isRoamingAllowed() {
        return this.zzgno;
    }

    public final void setBatteryUsagePreference(int i) {
        if (!zzcy(i)) {
            throw new IllegalArgumentException("Invalid battery usage preference value.");
        }
        this.zzgia = i;
    }

    public final void setNetworkTypePreference(int i) {
        if (!zzcx(i)) {
            throw new IllegalArgumentException("Invalid data connection preference value.");
        }
        this.zzgnn = i;
    }

    public final void setRoamingAllowed(boolean z) {
        this.zzgno = z;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 2, this.zzgnn);
        zzbem.zzc(parcel, 3, this.zzgia);
        zzbem.zza(parcel, 4, this.zzgno);
        zzbem.zzai(parcel, zze);
    }
}
