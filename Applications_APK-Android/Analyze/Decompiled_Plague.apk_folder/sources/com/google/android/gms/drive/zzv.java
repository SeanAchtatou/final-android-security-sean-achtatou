package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class zzv extends zzbej {
    public static final Parcelable.Creator<zzv> CREATOR = new zzw();
    private String zzghs;
    private int zzght;
    private String zzghu;
    private String zzghv;
    private int zzghw;
    private boolean zzghx;

    public zzv(String str, int i, String str2, String str3, int i2, boolean z) {
        this.zzghs = str;
        this.zzght = i;
        this.zzghu = str2;
        this.zzghv = str3;
        this.zzghw = i2;
        this.zzghx = z;
    }

    private static boolean zzcs(int i) {
        switch (i) {
            case 256:
            case 257:
            case 258:
                return true;
            default:
                return false;
        }
    }

    public final boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        zzv zzv = (zzv) obj;
        return zzbg.equal(this.zzghs, zzv.zzghs) && this.zzght == zzv.zzght && this.zzghw == zzv.zzghw && this.zzghx == zzv.zzghx;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.zzghs, Integer.valueOf(this.zzght), Integer.valueOf(this.zzghw), Boolean.valueOf(this.zzghx)});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        boolean z = false;
        zzbem.zza(parcel, 2, !zzcs(this.zzght) ? null : this.zzghs, false);
        int i2 = -1;
        zzbem.zzc(parcel, 3, !zzcs(this.zzght) ? -1 : this.zzght);
        zzbem.zza(parcel, 4, this.zzghu, false);
        zzbem.zza(parcel, 5, this.zzghv, false);
        switch (this.zzghw) {
            case 0:
            case 1:
            case 2:
            case 3:
                z = true;
                break;
        }
        if (z) {
            i2 = this.zzghw;
        }
        zzbem.zzc(parcel, 6, i2);
        zzbem.zza(parcel, 7, this.zzghx);
        zzbem.zzai(parcel, zze);
    }
}
