package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfhq extends zzfhe<zzfhq> {
    private static volatile zzfhq[] zzpih;
    public byte[] zzocl = null;
    public byte[] zzpii = null;

    public zzfhq() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    public static zzfhq[] zzcxm() {
        if (zzpih == null) {
            synchronized (zzfhi.zzphg) {
                if (zzpih == null) {
                    zzpih = new zzfhq[0];
                }
            }
        }
        return zzpih;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 10) {
                this.zzpii = zzfhb.readBytes();
            } else if (zzcts == 18) {
                this.zzocl = zzfhb.readBytes();
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        zzfhc.zzc(1, this.zzpii);
        if (this.zzocl != null) {
            zzfhc.zzc(2, this.zzocl);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo() + zzfhc.zzd(1, this.zzpii);
        return this.zzocl != null ? zzo + zzfhc.zzd(2, this.zzocl) : zzo;
    }
}
