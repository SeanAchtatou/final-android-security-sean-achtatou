package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfhp extends zzfhe<zzfhp> {
    public String zzcuw = null;

    public zzfhp() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 10) {
                this.zzcuw = zzfhb.readString();
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzcuw != null) {
            zzfhc.zzn(1, this.zzcuw);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        return this.zzcuw != null ? zzo + zzfhc.zzo(1, this.zzcuw) : zzo;
    }
}
