package com.google.android.gms.internal;

final class zzfdp implements zzfdl {
    private zzfdp() {
    }

    /* synthetic */ zzfdp(zzfdi zzfdi) {
        this();
    }

    public final byte[] zzf(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }
}
