package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.BinderThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzbt;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.internal.zzcvy;
import com.google.android.gms.internal.zzcwb;
import com.google.android.gms.internal.zzcwc;
import com.google.android.gms.internal.zzcwg;
import com.google.android.gms.internal.zzcwo;
import java.util.Set;

public final class zzcy extends zzcwg implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static Api.zza<? extends zzcwb, zzcwc> zzfrz = zzcvy.zzdyi;
    private final Context mContext;
    private final Handler mHandler;
    private Set<Scope> zzees;
    private final Api.zza<? extends zzcwb, zzcwc> zzfiy;
    private zzr zzfnd;
    private zzcwb zzfoj;
    private zzda zzfsa;

    @WorkerThread
    public zzcy(Context context, Handler handler, @NonNull zzr zzr) {
        this(context, handler, zzr, zzfrz);
    }

    @WorkerThread
    public zzcy(Context context, Handler handler, @NonNull zzr zzr, Api.zza<? extends zzcwb, zzcwc> zza) {
        this.mContext = context;
        this.mHandler = handler;
        this.zzfnd = (zzr) zzbq.checkNotNull(zzr, "ClientSettings must not be null");
        this.zzees = zzr.zzakj();
        this.zzfiy = zza;
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public final void zzc(zzcwo zzcwo) {
        ConnectionResult zzagt = zzcwo.zzagt();
        if (zzagt.isSuccess()) {
            zzbt zzbcw = zzcwo.zzbcw();
            zzagt = zzbcw.zzagt();
            if (!zzagt.isSuccess()) {
                String valueOf = String.valueOf(zzagt);
                StringBuilder sb = new StringBuilder(48 + String.valueOf(valueOf).length());
                sb.append("Sign-in succeeded with resolve account failure: ");
                sb.append(valueOf);
                Log.wtf("SignInCoordinator", sb.toString(), new Exception());
            } else {
                this.zzfsa.zzb(zzbcw.zzald(), this.zzees);
                this.zzfoj.disconnect();
            }
        }
        this.zzfsa.zzh(zzagt);
        this.zzfoj.disconnect();
    }

    @WorkerThread
    public final void onConnected(@Nullable Bundle bundle) {
        this.zzfoj.zza(this);
    }

    @WorkerThread
    public final void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        this.zzfsa.zzh(connectionResult);
    }

    @WorkerThread
    public final void onConnectionSuspended(int i) {
        this.zzfoj.disconnect();
    }

    @WorkerThread
    public final void zza(zzda zzda) {
        if (this.zzfoj != null) {
            this.zzfoj.disconnect();
        }
        this.zzfnd.zzc(Integer.valueOf(System.identityHashCode(this)));
        this.zzfoj = (zzcwb) this.zzfiy.zza(this.mContext, this.mHandler.getLooper(), this.zzfnd, this.zzfnd.zzakp(), this, this);
        this.zzfsa = zzda;
        this.zzfoj.connect();
    }

    public final zzcwb zzais() {
        return this.zzfoj;
    }

    public final void zzaje() {
        if (this.zzfoj != null) {
            this.zzfoj.disconnect();
        }
    }

    @BinderThread
    public final void zzb(zzcwo zzcwo) {
        this.mHandler.post(new zzcz(this, zzcwo));
    }
}
