package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class VideoCapabilities extends zzc {
    public static final Parcelable.Creator<VideoCapabilities> CREATOR = new zza();
    private final boolean zzhyg;
    private final boolean zzhyh;
    private final boolean zzhyi;
    private final boolean[] zzhyj;
    private final boolean[] zzhyk;

    public VideoCapabilities(boolean z, boolean z2, boolean z3, boolean[] zArr, boolean[] zArr2) {
        this.zzhyg = z;
        this.zzhyh = z2;
        this.zzhyi = z3;
        this.zzhyj = zArr;
        this.zzhyk = zArr2;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof VideoCapabilities)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        VideoCapabilities videoCapabilities = (VideoCapabilities) obj;
        return zzbg.equal(videoCapabilities.getSupportedCaptureModes(), getSupportedCaptureModes()) && zzbg.equal(videoCapabilities.getSupportedQualityLevels(), getSupportedQualityLevels()) && zzbg.equal(Boolean.valueOf(videoCapabilities.isCameraSupported()), Boolean.valueOf(isCameraSupported())) && zzbg.equal(Boolean.valueOf(videoCapabilities.isMicSupported()), Boolean.valueOf(isMicSupported())) && zzbg.equal(Boolean.valueOf(videoCapabilities.isWriteStorageSupported()), Boolean.valueOf(isWriteStorageSupported()));
    }

    public final boolean[] getSupportedCaptureModes() {
        return this.zzhyj;
    }

    public final boolean[] getSupportedQualityLevels() {
        return this.zzhyk;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{getSupportedCaptureModes(), getSupportedQualityLevels(), Boolean.valueOf(isCameraSupported()), Boolean.valueOf(isMicSupported()), Boolean.valueOf(isWriteStorageSupported())});
    }

    public final boolean isCameraSupported() {
        return this.zzhyg;
    }

    public final boolean isFullySupported(int i, int i2) {
        return this.zzhyg && this.zzhyh && this.zzhyi && supportsCaptureMode(i) && supportsQualityLevel(i2);
    }

    public final boolean isMicSupported() {
        return this.zzhyh;
    }

    public final boolean isWriteStorageSupported() {
        return this.zzhyi;
    }

    public final boolean supportsCaptureMode(int i) {
        zzbq.checkState(VideoConfiguration.isValidCaptureMode(i, false));
        return this.zzhyj[i];
    }

    public final boolean supportsQualityLevel(int i) {
        zzbq.checkState(VideoConfiguration.isValidQualityLevel(i, false));
        return this.zzhyk[i];
    }

    public final String toString() {
        return zzbg.zzw(this).zzg("SupportedCaptureModes", getSupportedCaptureModes()).zzg("SupportedQualityLevels", getSupportedQualityLevels()).zzg("CameraSupported", Boolean.valueOf(isCameraSupported())).zzg("MicSupported", Boolean.valueOf(isMicSupported())).zzg("StorageWriteSupported", Boolean.valueOf(isWriteStorageSupported())).toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
     arg types: [android.os.Parcel, int, boolean[], int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, isCameraSupported());
        zzbem.zza(parcel, 2, isMicSupported());
        zzbem.zza(parcel, 3, isWriteStorageSupported());
        zzbem.zza(parcel, 4, getSupportedCaptureModes(), false);
        zzbem.zza(parcel, 5, getSupportedQualityLevels(), false);
        zzbem.zzai(parcel, zze);
    }
}
