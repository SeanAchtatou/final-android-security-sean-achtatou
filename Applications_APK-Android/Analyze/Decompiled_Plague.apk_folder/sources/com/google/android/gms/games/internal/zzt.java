package com.google.android.gms.games.internal;

import com.google.android.gms.internal.zzee;

public abstract class zzt extends zzee implements zzs {
    public zzt() {
        attachInterface(this, "com.google.android.gms.games.internal.IGamesCallbacks");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0222  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0222 A[FALL_THROUGH] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTransact(int r8, android.os.Parcel r9, android.os.Parcel r10, int r11) throws android.os.RemoteException {
        /*
            r7 = this;
            boolean r11 = r7.zza(r8, r9, r10, r11)
            r0 = 1
            if (r11 == 0) goto L_0x0008
            return r0
        L_0x0008:
            switch(r8) {
                case 5001: goto L_0x039f;
                case 5002: goto L_0x0393;
                case 5003: goto L_0x0387;
                case 5004: goto L_0x037b;
                case 5005: goto L_0x0367;
                case 5006: goto L_0x035b;
                case 5007: goto L_0x034f;
                case 5008: goto L_0x0343;
                case 5009: goto L_0x0337;
                case 5010: goto L_0x0330;
                case 5011: goto L_0x0330;
                default: goto L_0x000b;
            }
        L_0x000b:
            switch(r8) {
                case 5016: goto L_0x032b;
                case 5017: goto L_0x031e;
                case 5018: goto L_0x0311;
                case 5019: goto L_0x0304;
                case 5020: goto L_0x02f7;
                case 5021: goto L_0x02ea;
                case 5022: goto L_0x02dd;
                case 5023: goto L_0x02d0;
                case 5024: goto L_0x02c3;
                case 5025: goto L_0x02b6;
                case 5026: goto L_0x02a5;
                case 5027: goto L_0x0294;
                case 5028: goto L_0x0283;
                case 5029: goto L_0x0272;
                case 5030: goto L_0x0261;
                case 5031: goto L_0x0250;
                case 5032: goto L_0x0243;
                case 5033: goto L_0x0232;
                case 5034: goto L_0x0227;
                case 5035: goto L_0x0330;
                case 5036: goto L_0x0222;
                case 5037: goto L_0x0215;
                case 5038: goto L_0x0330;
                case 5039: goto L_0x0330;
                case 5040: goto L_0x0222;
                default: goto L_0x000e;
            }
        L_0x000e:
            switch(r8) {
                case 6001: goto L_0x020c;
                case 6002: goto L_0x0203;
                default: goto L_0x0011;
            }
        L_0x0011:
            switch(r8) {
                case 8001: goto L_0x01f6;
                case 8002: goto L_0x01e5;
                case 8003: goto L_0x01d8;
                case 8004: goto L_0x01cb;
                case 8005: goto L_0x01be;
                case 8006: goto L_0x01b1;
                case 8007: goto L_0x01a4;
                case 8008: goto L_0x0197;
                case 8009: goto L_0x018e;
                case 8010: goto L_0x0185;
                default: goto L_0x0014;
            }
        L_0x0014:
            switch(r8) {
                case 10001: goto L_0x0178;
                case 10002: goto L_0x016f;
                case 10003: goto L_0x0162;
                case 10004: goto L_0x0330;
                case 10005: goto L_0x0151;
                case 10006: goto L_0x0330;
                default: goto L_0x0017;
            }
        L_0x0017:
            switch(r8) {
                case 12003: goto L_0x0052;
                case 12004: goto L_0x013c;
                case 12005: goto L_0x012f;
                case 12006: goto L_0x0122;
                case 12007: goto L_0x0115;
                case 12008: goto L_0x0108;
                default: goto L_0x001a;
            }
        L_0x001a:
            switch(r8) {
                case 12011: goto L_0x00fb;
                case 12012: goto L_0x00ee;
                case 12013: goto L_0x0330;
                case 12014: goto L_0x00e1;
                case 12015: goto L_0x0052;
                case 12016: goto L_0x0330;
                case 12017: goto L_0x00b3;
                default: goto L_0x001d;
            }
        L_0x001d:
            switch(r8) {
                case 13001: goto L_0x0330;
                case 13002: goto L_0x0222;
                default: goto L_0x0020;
            }
        L_0x0020:
            switch(r8) {
                case 17001: goto L_0x0330;
                case 17002: goto L_0x0222;
                default: goto L_0x0023;
            }
        L_0x0023:
            switch(r8) {
                case 19001: goto L_0x00a2;
                case 19002: goto L_0x0095;
                case 19003: goto L_0x008d;
                case 19004: goto L_0x0222;
                default: goto L_0x0026;
            }
        L_0x0026:
            switch(r8) {
                case 19006: goto L_0x0330;
                case 19007: goto L_0x0052;
                case 19008: goto L_0x0222;
                case 19009: goto L_0x0222;
                case 19010: goto L_0x0222;
                default: goto L_0x0029;
            }
        L_0x0029:
            switch(r8) {
                case 20001: goto L_0x0330;
                case 20002: goto L_0x0330;
                case 20003: goto L_0x0330;
                case 20004: goto L_0x0330;
                case 20005: goto L_0x0330;
                case 20006: goto L_0x0330;
                case 20007: goto L_0x0330;
                case 20008: goto L_0x0330;
                case 20009: goto L_0x0330;
                case 20010: goto L_0x0082;
                case 20011: goto L_0x007e;
                case 20012: goto L_0x007a;
                case 20013: goto L_0x007a;
                case 20014: goto L_0x007a;
                case 20015: goto L_0x007a;
                case 20016: goto L_0x0222;
                case 20017: goto L_0x0073;
                case 20018: goto L_0x0222;
                case 20019: goto L_0x006a;
                case 20020: goto L_0x0059;
                default: goto L_0x002c;
            }
        L_0x002c:
            switch(r8) {
                case 9001: goto L_0x0330;
                case 11001: goto L_0x0052;
                case 12001: goto L_0x0045;
                case 14001: goto L_0x003e;
                case 15001: goto L_0x0031;
                default: goto L_0x002f;
            }
        L_0x002f:
            r8 = 0
            return r8
        L_0x0031:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzaj(r8)
            goto L_0x03aa
        L_0x003e:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            r9.createTypedArray(r8)
            goto L_0x03aa
        L_0x0045:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzad(r8)
            goto L_0x03aa
        L_0x0052:
            r9.readInt()
            android.os.Parcelable$Creator r8 = android.os.Bundle.CREATOR
            goto L_0x0332
        L_0x0059:
            int r8 = r9.readInt()
            android.os.Parcelable$Creator r11 = android.os.Bundle.CREATOR
            android.os.Parcelable r9 = com.google.android.gms.internal.zzef.zza(r9, r11)
            android.os.Bundle r9 = (android.os.Bundle) r9
            r7.zzd(r8, r9)
            goto L_0x03aa
        L_0x006a:
            int r8 = r9.readInt()
            r7.onCaptureOverlayStateChanged(r8)
            goto L_0x03aa
        L_0x0073:
            r9.readInt()
            android.os.Parcelable$Creator r8 = android.net.Uri.CREATOR
            goto L_0x0332
        L_0x007a:
            android.os.Parcelable$Creator<com.google.android.gms.common.api.Status> r8 = com.google.android.gms.common.api.Status.CREATOR
            goto L_0x0332
        L_0x007e:
            r9.readInt()
            goto L_0x0088
        L_0x0082:
            r9.readInt()
            r9.readString()
        L_0x0088:
            r9.readString()
            goto L_0x03aa
        L_0x008d:
            r9.readInt()
            com.google.android.gms.internal.zzef.zza(r9)
            goto L_0x022d
        L_0x0095:
            int r8 = r9.readInt()
            boolean r9 = com.google.android.gms.internal.zzef.zza(r9)
            r7.zzi(r8, r9)
            goto L_0x03aa
        L_0x00a2:
            int r8 = r9.readInt()
            android.os.Parcelable$Creator<com.google.android.gms.games.video.VideoCapabilities> r11 = com.google.android.gms.games.video.VideoCapabilities.CREATOR
            android.os.Parcelable r9 = com.google.android.gms.internal.zzef.zza(r9, r11)
            com.google.android.gms.games.video.VideoCapabilities r9 = (com.google.android.gms.games.video.VideoCapabilities) r9
            r7.zza(r8, r9)
            goto L_0x03aa
        L_0x00b3:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            r2 = r8
            com.google.android.gms.common.data.DataHolder r2 = (com.google.android.gms.common.data.DataHolder) r2
            java.lang.String r3 = r9.readString()
            android.os.Parcelable$Creator<com.google.android.gms.drive.zzc> r8 = com.google.android.gms.drive.zzc.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            r4 = r8
            com.google.android.gms.drive.zzc r4 = (com.google.android.gms.drive.zzc) r4
            android.os.Parcelable$Creator<com.google.android.gms.drive.zzc> r8 = com.google.android.gms.drive.zzc.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            r5 = r8
            com.google.android.gms.drive.zzc r5 = (com.google.android.gms.drive.zzc) r5
            android.os.Parcelable$Creator<com.google.android.gms.drive.zzc> r8 = com.google.android.gms.drive.zzc.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            r6 = r8
            com.google.android.gms.drive.zzc r6 = (com.google.android.gms.drive.zzc) r6
            r1 = r7
            r1.zza(r2, r3, r4, r5, r6)
            goto L_0x03aa
        L_0x00e1:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzah(r8)
            goto L_0x03aa
        L_0x00ee:
            int r8 = r9.readInt()
            java.lang.String r9 = r9.readString()
            r7.zzj(r8, r9)
            goto L_0x03aa
        L_0x00fb:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzg(r8)
            goto L_0x03aa
        L_0x0108:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzai(r8)
            goto L_0x03aa
        L_0x0115:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzag(r8)
            goto L_0x03aa
        L_0x0122:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzaf(r8)
            goto L_0x03aa
        L_0x012f:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzae(r8)
            goto L_0x03aa
        L_0x013c:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            android.os.Parcelable$Creator<com.google.android.gms.drive.zzc> r11 = com.google.android.gms.drive.zzc.CREATOR
            android.os.Parcelable r9 = com.google.android.gms.internal.zzef.zza(r9, r11)
            com.google.android.gms.drive.zzc r9 = (com.google.android.gms.drive.zzc) r9
            r7.zza(r8, r9)
            goto L_0x03aa
        L_0x0151:
            int r8 = r9.readInt()
            android.os.Parcelable$Creator r11 = android.os.Bundle.CREATOR
            android.os.Parcelable r9 = com.google.android.gms.internal.zzef.zza(r9, r11)
            android.os.Bundle r9 = (android.os.Bundle) r9
            r7.zzc(r8, r9)
            goto L_0x03aa
        L_0x0162:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzac(r8)
            goto L_0x03aa
        L_0x016f:
            java.lang.String r8 = r9.readString()
            r7.onRequestRemoved(r8)
            goto L_0x03aa
        L_0x0178:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzo(r8)
            goto L_0x03aa
        L_0x0185:
            java.lang.String r8 = r9.readString()
            r7.onInvitationRemoved(r8)
            goto L_0x03aa
        L_0x018e:
            java.lang.String r8 = r9.readString()
            r7.onTurnBasedMatchRemoved(r8)
            goto L_0x03aa
        L_0x0197:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzt(r8)
            goto L_0x03aa
        L_0x01a4:
            int r8 = r9.readInt()
            java.lang.String r9 = r9.readString()
            r7.zzi(r8, r9)
            goto L_0x03aa
        L_0x01b1:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzs(r8)
            goto L_0x03aa
        L_0x01be:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzr(r8)
            goto L_0x03aa
        L_0x01cb:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzq(r8)
            goto L_0x03aa
        L_0x01d8:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzp(r8)
            goto L_0x03aa
        L_0x01e5:
            int r8 = r9.readInt()
            android.os.Parcelable$Creator r11 = android.os.Bundle.CREATOR
            android.os.Parcelable r9 = com.google.android.gms.internal.zzef.zza(r9, r11)
            android.os.Bundle r9 = (android.os.Bundle) r9
            r7.zzb(r8, r9)
            goto L_0x03aa
        L_0x01f6:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzab(r8)
            goto L_0x03aa
        L_0x0203:
            java.lang.String r8 = r9.readString()
            r7.onP2PDisconnected(r8)
            goto L_0x03aa
        L_0x020c:
            java.lang.String r8 = r9.readString()
            r7.onP2PConnected(r8)
            goto L_0x03aa
        L_0x0215:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzn(r8)
            goto L_0x03aa
        L_0x0222:
            r9.readInt()
            goto L_0x03aa
        L_0x0227:
            r9.readInt()
            r9.readString()
        L_0x022d:
            com.google.android.gms.internal.zzef.zza(r9)
            goto L_0x03aa
        L_0x0232:
            int r8 = r9.readInt()
            int r11 = r9.readInt()
            java.lang.String r9 = r9.readString()
            r7.zzb(r8, r11, r9)
            goto L_0x03aa
        L_0x0243:
            android.os.Parcelable$Creator<com.google.android.gms.games.multiplayer.realtime.RealTimeMessage> r8 = com.google.android.gms.games.multiplayer.realtime.RealTimeMessage.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.games.multiplayer.realtime.RealTimeMessage r8 = (com.google.android.gms.games.multiplayer.realtime.RealTimeMessage) r8
            r7.onRealTimeMessageReceived(r8)
            goto L_0x03aa
        L_0x0250:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            java.lang.String[] r9 = r9.createStringArray()
            r7.zzf(r8, r9)
            goto L_0x03aa
        L_0x0261:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            java.lang.String[] r9 = r9.createStringArray()
            r7.zze(r8, r9)
            goto L_0x03aa
        L_0x0272:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            java.lang.String[] r9 = r9.createStringArray()
            r7.zzd(r8, r9)
            goto L_0x03aa
        L_0x0283:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            java.lang.String[] r9 = r9.createStringArray()
            r7.zzc(r8, r9)
            goto L_0x03aa
        L_0x0294:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            java.lang.String[] r9 = r9.createStringArray()
            r7.zzb(r8, r9)
            goto L_0x03aa
        L_0x02a5:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            java.lang.String[] r9 = r9.createStringArray()
            r7.zza(r8, r9)
            goto L_0x03aa
        L_0x02b6:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzaa(r8)
            goto L_0x03aa
        L_0x02c3:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzz(r8)
            goto L_0x03aa
        L_0x02d0:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzy(r8)
            goto L_0x03aa
        L_0x02dd:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzx(r8)
            goto L_0x03aa
        L_0x02ea:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzw(r8)
            goto L_0x03aa
        L_0x02f7:
            int r8 = r9.readInt()
            java.lang.String r9 = r9.readString()
            r7.onLeftRoom(r8, r9)
            goto L_0x03aa
        L_0x0304:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzv(r8)
            goto L_0x03aa
        L_0x0311:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzu(r8)
            goto L_0x03aa
        L_0x031e:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzm(r8)
            goto L_0x03aa
        L_0x032b:
            r7.zzait()
            goto L_0x03aa
        L_0x0330:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
        L_0x0332:
            com.google.android.gms.internal.zzef.zza(r9, r8)
            goto L_0x03aa
        L_0x0337:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzl(r8)
            goto L_0x03aa
        L_0x0343:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzk(r8)
            goto L_0x03aa
        L_0x034f:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzj(r8)
            goto L_0x03aa
        L_0x035b:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzi(r8)
            goto L_0x03aa
        L_0x0367:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r11 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r9 = com.google.android.gms.internal.zzef.zza(r9, r11)
            com.google.android.gms.common.data.DataHolder r9 = (com.google.android.gms.common.data.DataHolder) r9
            r7.zza(r8, r9)
            goto L_0x03aa
        L_0x037b:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzh(r8)
            goto L_0x03aa
        L_0x0387:
            int r8 = r9.readInt()
            java.lang.String r9 = r9.readString()
            r7.zzh(r8, r9)
            goto L_0x03aa
        L_0x0393:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r8 = com.google.android.gms.common.data.DataHolder.CREATOR
            android.os.Parcelable r8 = com.google.android.gms.internal.zzef.zza(r9, r8)
            com.google.android.gms.common.data.DataHolder r8 = (com.google.android.gms.common.data.DataHolder) r8
            r7.zzf(r8)
            goto L_0x03aa
        L_0x039f:
            int r8 = r9.readInt()
            java.lang.String r9 = r9.readString()
            r7.zzg(r8, r9)
        L_0x03aa:
            r10.writeNoException()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.games.internal.zzt.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
