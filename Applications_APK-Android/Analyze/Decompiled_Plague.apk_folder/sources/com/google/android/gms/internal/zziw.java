package com.google.android.gms.internal;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.zzb;

@zzzb
public class zziw extends zzbej {
    public static final Parcelable.Creator<zziw> CREATOR = new zzix();
    public final int height;
    public final int heightPixels;
    public final int width;
    public final int widthPixels;
    public final String zzbda;
    public final boolean zzbdb;
    public final zziw[] zzbdc;
    public final boolean zzbdd;
    public final boolean zzbde;
    public boolean zzbdf;

    public zziw() {
        this("interstitial_mb", 0, 0, true, 0, 0, null, false, false, false);
    }

    public zziw(Context context, AdSize adSize) {
        this(context, new AdSize[]{adSize});
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x007d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zziw(android.content.Context r13, com.google.android.gms.ads.AdSize[] r14) {
        /*
            r12 = this;
            r12.<init>()
            r0 = 0
            r1 = r14[r0]
            r12.zzbdb = r0
            boolean r2 = r1.isFluid()
            r12.zzbde = r2
            boolean r2 = r12.zzbde
            if (r2 == 0) goto L_0x0023
            com.google.android.gms.ads.AdSize r2 = com.google.android.gms.ads.AdSize.BANNER
            int r2 = r2.getWidth()
            r12.width = r2
            com.google.android.gms.ads.AdSize r2 = com.google.android.gms.ads.AdSize.BANNER
            int r2 = r2.getHeight()
        L_0x0020:
            r12.height = r2
            goto L_0x002e
        L_0x0023:
            int r2 = r1.getWidth()
            r12.width = r2
            int r2 = r1.getHeight()
            goto L_0x0020
        L_0x002e:
            int r2 = r12.width
            r3 = -1
            r4 = 1
            if (r2 != r3) goto L_0x0036
            r2 = r4
            goto L_0x0037
        L_0x0036:
            r2 = r0
        L_0x0037:
            int r3 = r12.height
            r5 = -2
            if (r3 != r5) goto L_0x003e
            r3 = r4
            goto L_0x003f
        L_0x003e:
            r3 = r0
        L_0x003f:
            android.content.res.Resources r5 = r13.getResources()
            android.util.DisplayMetrics r5 = r5.getDisplayMetrics()
            if (r2 == 0) goto L_0x0080
            com.google.android.gms.internal.zzjk.zzhx()
            boolean r6 = com.google.android.gms.internal.zzais.zzbf(r13)
            if (r6 == 0) goto L_0x0066
            com.google.android.gms.internal.zzjk.zzhx()
            boolean r6 = com.google.android.gms.internal.zzais.zzbg(r13)
            if (r6 == 0) goto L_0x0066
            int r6 = r5.widthPixels
            com.google.android.gms.internal.zzjk.zzhx()
            int r7 = com.google.android.gms.internal.zzais.zzbh(r13)
            int r6 = r6 - r7
            goto L_0x0068
        L_0x0066:
            int r6 = r5.widthPixels
        L_0x0068:
            r12.widthPixels = r6
            int r6 = r12.widthPixels
            float r6 = (float) r6
            float r7 = r5.density
            float r6 = r6 / r7
            double r6 = (double) r6
            int r8 = (int) r6
            double r9 = (double) r8
            double r6 = r6 - r9
            r9 = 4576918229304087675(0x3f847ae147ae147b, double:0.01)
            int r11 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r11 < 0) goto L_0x008d
            int r8 = r8 + 1
            goto L_0x008d
        L_0x0080:
            int r8 = r12.width
            com.google.android.gms.internal.zzjk.zzhx()
            int r6 = r12.width
            int r6 = com.google.android.gms.internal.zzais.zza(r5, r6)
            r12.widthPixels = r6
        L_0x008d:
            if (r3 == 0) goto L_0x0094
            int r6 = zzd(r5)
            goto L_0x0096
        L_0x0094:
            int r6 = r12.height
        L_0x0096:
            com.google.android.gms.internal.zzjk.zzhx()
            int r5 = com.google.android.gms.internal.zzais.zza(r5, r6)
            r12.heightPixels = r5
            if (r2 != 0) goto L_0x00b0
            if (r3 == 0) goto L_0x00a4
            goto L_0x00b0
        L_0x00a4:
            boolean r2 = r12.zzbde
            if (r2 == 0) goto L_0x00ab
            java.lang.String r1 = "320x50_mb"
            goto L_0x00cb
        L_0x00ab:
            java.lang.String r1 = r1.toString()
            goto L_0x00cb
        L_0x00b0:
            r1 = 26
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r1)
            r2.append(r8)
            java.lang.String r1 = "x"
            r2.append(r1)
            r2.append(r6)
            java.lang.String r1 = "_as"
            r2.append(r1)
            java.lang.String r1 = r2.toString()
        L_0x00cb:
            r12.zzbda = r1
            int r1 = r14.length
            if (r1 <= r4) goto L_0x00e7
            int r1 = r14.length
            com.google.android.gms.internal.zziw[] r1 = new com.google.android.gms.internal.zziw[r1]
            r12.zzbdc = r1
            r1 = r0
        L_0x00d6:
            int r2 = r14.length
            if (r1 >= r2) goto L_0x00ea
            com.google.android.gms.internal.zziw[] r2 = r12.zzbdc
            com.google.android.gms.internal.zziw r3 = new com.google.android.gms.internal.zziw
            r4 = r14[r1]
            r3.<init>(r13, r4)
            r2[r1] = r3
            int r1 = r1 + 1
            goto L_0x00d6
        L_0x00e7:
            r13 = 0
            r12.zzbdc = r13
        L_0x00ea:
            r12.zzbdd = r0
            r12.zzbdf = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zziw.<init>(android.content.Context, com.google.android.gms.ads.AdSize[]):void");
    }

    public zziw(zziw zziw, zziw[] zziwArr) {
        this(zziw.zzbda, zziw.height, zziw.heightPixels, zziw.zzbdb, zziw.width, zziw.widthPixels, zziwArr, zziw.zzbdd, zziw.zzbde, zziw.zzbdf);
    }

    zziw(String str, int i, int i2, boolean z, int i3, int i4, zziw[] zziwArr, boolean z2, boolean z3, boolean z4) {
        this.zzbda = str;
        this.height = i;
        this.heightPixels = i2;
        this.zzbdb = z;
        this.width = i3;
        this.widthPixels = i4;
        this.zzbdc = zziwArr;
        this.zzbdd = z2;
        this.zzbde = z3;
        this.zzbdf = z4;
    }

    public static int zzb(DisplayMetrics displayMetrics) {
        return displayMetrics.widthPixels;
    }

    public static int zzc(DisplayMetrics displayMetrics) {
        return (int) (((float) zzd(displayMetrics)) * displayMetrics.density);
    }

    private static int zzd(DisplayMetrics displayMetrics) {
        int i = (int) (((float) displayMetrics.heightPixels) / displayMetrics.density);
        if (i <= 400) {
            return 32;
        }
        return i <= 720 ? 50 : 90;
    }

    public static zziw zzg(Context context) {
        return new zziw("320x50_mb", 0, 0, false, 0, 0, null, true, false, false);
    }

    public static zziw zzhp() {
        return new zziw("reward_mb", 0, 0, true, 0, 0, null, false, false, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.zziw[], int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, this.zzbda, false);
        zzbem.zzc(parcel, 3, this.height);
        zzbem.zzc(parcel, 4, this.heightPixels);
        zzbem.zza(parcel, 5, this.zzbdb);
        zzbem.zzc(parcel, 6, this.width);
        zzbem.zzc(parcel, 7, this.widthPixels);
        zzbem.zza(parcel, 8, (Parcelable[]) this.zzbdc, i, false);
        zzbem.zza(parcel, 9, this.zzbdd);
        zzbem.zza(parcel, 10, this.zzbde);
        zzbem.zza(parcel, 11, this.zzbdf);
        zzbem.zzai(parcel, zze);
    }

    public final AdSize zzhq() {
        return zzb.zza(this.width, this.height, this.zzbda);
    }
}
