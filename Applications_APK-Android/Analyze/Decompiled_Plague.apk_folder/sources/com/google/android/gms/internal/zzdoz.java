package com.google.android.gms.internal;

final /* synthetic */ class zzdoz {
    static final /* synthetic */ int[] zzlpn = new int[zzdrc.values().length];
    static final /* synthetic */ int[] zzlpo = new int[zzdqy.values().length];
    static final /* synthetic */ int[] zzlpp = new int[zzdqo.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(21:0|(2:1|2)|3|5|6|7|9|10|11|12|13|15|16|17|19|20|21|22|23|24|26) */
    /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|9|10|11|12|13|15|16|17|19|20|21|22|23|24|26) */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0032 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x005a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0064 */
    static {
        /*
            com.google.android.gms.internal.zzdqo[] r0 = com.google.android.gms.internal.zzdqo.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.google.android.gms.internal.zzdoz.zzlpp = r0
            r0 = 1
            int[] r1 = com.google.android.gms.internal.zzdoz.zzlpp     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.google.android.gms.internal.zzdqo r2 = com.google.android.gms.internal.zzdqo.UNCOMPRESSED     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            r1 = 2
            int[] r2 = com.google.android.gms.internal.zzdoz.zzlpp     // Catch:{ NoSuchFieldError -> 0x001f }
            com.google.android.gms.internal.zzdqo r3 = com.google.android.gms.internal.zzdqo.COMPRESSED     // Catch:{ NoSuchFieldError -> 0x001f }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            com.google.android.gms.internal.zzdqy[] r2 = com.google.android.gms.internal.zzdqy.values()
            int r2 = r2.length
            int[] r2 = new int[r2]
            com.google.android.gms.internal.zzdoz.zzlpo = r2
            int[] r2 = com.google.android.gms.internal.zzdoz.zzlpo     // Catch:{ NoSuchFieldError -> 0x0032 }
            com.google.android.gms.internal.zzdqy r3 = com.google.android.gms.internal.zzdqy.NIST_P256     // Catch:{ NoSuchFieldError -> 0x0032 }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0032 }
            r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x0032 }
        L_0x0032:
            int[] r2 = com.google.android.gms.internal.zzdoz.zzlpo     // Catch:{ NoSuchFieldError -> 0x003c }
            com.google.android.gms.internal.zzdqy r3 = com.google.android.gms.internal.zzdqy.NIST_P384     // Catch:{ NoSuchFieldError -> 0x003c }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x003c }
            r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x003c }
        L_0x003c:
            r2 = 3
            int[] r3 = com.google.android.gms.internal.zzdoz.zzlpo     // Catch:{ NoSuchFieldError -> 0x0047 }
            com.google.android.gms.internal.zzdqy r4 = com.google.android.gms.internal.zzdqy.NIST_P521     // Catch:{ NoSuchFieldError -> 0x0047 }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
            r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x0047 }
        L_0x0047:
            com.google.android.gms.internal.zzdrc[] r3 = com.google.android.gms.internal.zzdrc.values()
            int r3 = r3.length
            int[] r3 = new int[r3]
            com.google.android.gms.internal.zzdoz.zzlpn = r3
            int[] r3 = com.google.android.gms.internal.zzdoz.zzlpn     // Catch:{ NoSuchFieldError -> 0x005a }
            com.google.android.gms.internal.zzdrc r4 = com.google.android.gms.internal.zzdrc.SHA1     // Catch:{ NoSuchFieldError -> 0x005a }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x005a }
            r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x005a }
        L_0x005a:
            int[] r0 = com.google.android.gms.internal.zzdoz.zzlpn     // Catch:{ NoSuchFieldError -> 0x0064 }
            com.google.android.gms.internal.zzdrc r3 = com.google.android.gms.internal.zzdrc.SHA256     // Catch:{ NoSuchFieldError -> 0x0064 }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0064 }
            r0[r3] = r1     // Catch:{ NoSuchFieldError -> 0x0064 }
        L_0x0064:
            int[] r0 = com.google.android.gms.internal.zzdoz.zzlpn     // Catch:{ NoSuchFieldError -> 0x006e }
            com.google.android.gms.internal.zzdrc r1 = com.google.android.gms.internal.zzdrc.SHA512     // Catch:{ NoSuchFieldError -> 0x006e }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006e }
        L_0x006e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdoz.<clinit>():void");
    }
}
