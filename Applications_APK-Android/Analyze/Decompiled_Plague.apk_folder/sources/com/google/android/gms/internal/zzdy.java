package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzdy extends zzec {
    public zzdy(zzda zzda, String str, String str2, zzaw zzaw, int i, int i2) {
        super(zzda, str, str2, zzaw, i, 48);
    }

    /* access modifiers changed from: protected */
    public final void zzat() throws IllegalAccessException, InvocationTargetException {
        zzaw zzaw;
        int i;
        this.zzajo.zzee = 2;
        boolean booleanValue = ((Boolean) this.zzajx.invoke(null, this.zzagk.getApplicationContext())).booleanValue();
        synchronized (this.zzajo) {
            if (booleanValue) {
                try {
                    zzaw = this.zzajo;
                    i = 1;
                } catch (Throwable th) {
                    throw th;
                }
            } else {
                zzaw = this.zzajo;
                i = 0;
            }
            zzaw.zzee = i;
        }
    }
}
