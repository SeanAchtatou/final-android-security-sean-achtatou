package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;

public final class zzi extends zzbej {
    public static final Parcelable.Creator<zzi> CREATOR = new zzj();
    private long zzggn;
    private long zzggo;

    public zzi(long j, long j2) {
        this.zzggn = j;
        this.zzggo = j2;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, this.zzggn);
        zzbem.zza(parcel, 3, this.zzggo);
        zzbem.zzai(parcel, zze);
    }
}
