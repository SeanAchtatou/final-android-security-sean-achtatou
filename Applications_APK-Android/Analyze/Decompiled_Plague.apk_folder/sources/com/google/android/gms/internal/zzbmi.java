package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.drive.DriveFolder;

final class zzbmi extends zzbjv {
    private final zzn<DriveFolder.DriveFileResult> zzfzc;

    public zzbmi(zzn<DriveFolder.DriveFileResult> zzn) {
        this.zzfzc = zzn;
    }

    public final void onError(Status status) throws RemoteException {
        this.zzfzc.setResult(new zzbmk(status, null));
    }

    public final void zza(zzbpy zzbpy) throws RemoteException {
        this.zzfzc.setResult(new zzbmk(Status.zzfko, new zzbma(zzbpy.zzgjm)));
    }
}
