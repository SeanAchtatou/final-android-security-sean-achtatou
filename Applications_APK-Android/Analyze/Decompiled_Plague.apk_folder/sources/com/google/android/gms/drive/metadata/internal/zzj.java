package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbek;

public final class zzj implements Parcelable.Creator<MetadataBundle> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                zzbek.zzb(parcel, readInt);
            } else {
                bundle = zzbek.zzs(parcel, readInt);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new MetadataBundle(bundle);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new MetadataBundle[i];
    }
}
