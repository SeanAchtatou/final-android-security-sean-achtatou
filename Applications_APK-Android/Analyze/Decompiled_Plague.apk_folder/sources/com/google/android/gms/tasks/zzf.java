package com.google.android.gms.tasks;

final class zzf implements Runnable {
    private /* synthetic */ Task zzkrh;
    private /* synthetic */ zze zzkrl;

    zzf(zze zze, Task task) {
        this.zzkrl = zze;
        this.zzkrh = task;
    }

    public final void run() {
        synchronized (this.zzkrl.mLock) {
            if (this.zzkrl.zzkrk != null) {
                this.zzkrl.zzkrk.onComplete(this.zzkrh);
            }
        }
    }
}
