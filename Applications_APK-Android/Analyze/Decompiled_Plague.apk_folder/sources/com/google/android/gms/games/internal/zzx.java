package com.google.android.gms.games.internal;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.zzc;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.multiplayer.ParticipantResult;
import com.google.android.gms.games.multiplayer.realtime.RoomEntity;
import com.google.android.gms.games.snapshot.zze;
import com.google.android.gms.internal.zzed;
import com.google.android.gms.internal.zzef;

public final class zzx extends zzed implements zzw {
    zzx(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.games.internal.IGamesService");
    }

    public final String getAppId() throws RemoteException {
        Parcel zza = zza(5003, zzaz());
        String readString = zza.readString();
        zza.recycle();
        return readString;
    }

    public final int zza(zzs zzs, byte[] bArr, String str, String str2) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeByteArray(bArr);
        zzaz.writeString(str);
        zzaz.writeString(str2);
        Parcel zza = zza(5033, zzaz);
        int readInt = zza.readInt();
        zza.recycle();
        return readInt;
    }

    public final Intent zza(int i, byte[] bArr, int i2, String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeInt(i);
        zzaz.writeByteArray(bArr);
        zzaz.writeInt(i2);
        zzaz.writeString(str);
        Parcel zza = zza(10012, zzaz);
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final Intent zza(PlayerEntity playerEntity) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, playerEntity);
        Parcel zza = zza(15503, zzaz);
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final Intent zza(RoomEntity roomEntity, int i) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, roomEntity);
        zzaz.writeInt(i);
        Parcel zza = zza(9011, zzaz);
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final Intent zza(String str, boolean z, boolean z2, int i) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeString(str);
        zzef.zza(zzaz, z);
        zzef.zza(zzaz, z2);
        zzaz.writeInt(i);
        Parcel zza = zza(12001, zzaz);
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final void zza(IBinder iBinder, Bundle bundle) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeStrongBinder(iBinder);
        zzef.zza(zzaz, bundle);
        zzb(5005, zzaz);
    }

    public final void zza(zzc zzc) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzc);
        zzb(12019, zzaz);
    }

    public final void zza(zzs zzs) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzb(5002, zzaz);
    }

    public final void zza(zzs zzs, int i) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeInt(i);
        zzb(10016, zzaz);
    }

    public final void zza(zzs zzs, int i, int i2, int i3) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeInt(i);
        zzaz.writeInt(i2);
        zzaz.writeInt(i3);
        zzb(10009, zzaz);
    }

    public final void zza(zzs zzs, int i, int i2, String[] strArr, Bundle bundle) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeInt(i);
        zzaz.writeInt(i2);
        zzaz.writeStringArray(strArr);
        zzef.zza(zzaz, bundle);
        zzb(8004, zzaz);
    }

    public final void zza(zzs zzs, int i, boolean z, boolean z2) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeInt(i);
        zzef.zza(zzaz, z);
        zzef.zza(zzaz, z2);
        zzb(5015, zzaz);
    }

    public final void zza(zzs zzs, int i, int[] iArr) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeInt(i);
        zzaz.writeIntArray(iArr);
        zzb(10018, zzaz);
    }

    public final void zza(zzs zzs, long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeLong(j);
        zzb(5058, zzaz);
    }

    public final void zza(zzs zzs, Bundle bundle, int i, int i2) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzef.zza(zzaz, bundle);
        zzaz.writeInt(i);
        zzaz.writeInt(i2);
        zzb(5021, zzaz);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzef.zza(android.os.Parcel, boolean):void
     arg types: [android.os.Parcel, int]
     candidates:
      com.google.android.gms.internal.zzef.zza(android.os.Parcel, android.os.Parcelable$Creator):T
      com.google.android.gms.internal.zzef.zza(android.os.Parcel, android.os.IInterface):void
      com.google.android.gms.internal.zzef.zza(android.os.Parcel, android.os.Parcelable):void
      com.google.android.gms.internal.zzef.zza(android.os.Parcel, boolean):void */
    public final void zza(zzs zzs, IBinder iBinder, int i, String[] strArr, Bundle bundle, boolean z, long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeStrongBinder(iBinder);
        zzaz.writeInt(i);
        zzaz.writeStringArray(strArr);
        zzef.zza(zzaz, bundle);
        zzef.zza(zzaz, false);
        zzaz.writeLong(j);
        zzb(5030, zzaz);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzef.zza(android.os.Parcel, boolean):void
     arg types: [android.os.Parcel, int]
     candidates:
      com.google.android.gms.internal.zzef.zza(android.os.Parcel, android.os.Parcelable$Creator):T
      com.google.android.gms.internal.zzef.zza(android.os.Parcel, android.os.IInterface):void
      com.google.android.gms.internal.zzef.zza(android.os.Parcel, android.os.Parcelable):void
      com.google.android.gms.internal.zzef.zza(android.os.Parcel, boolean):void */
    public final void zza(zzs zzs, IBinder iBinder, String str, boolean z, long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeStrongBinder(iBinder);
        zzaz.writeString(str);
        zzef.zza(zzaz, false);
        zzaz.writeLong(j);
        zzb(5031, zzaz);
    }

    public final void zza(zzs zzs, String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzb(5032, zzaz);
    }

    public final void zza(zzs zzs, String str, int i, int i2, int i3, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzaz.writeInt(i);
        zzaz.writeInt(i2);
        zzaz.writeInt(i3);
        zzef.zza(zzaz, z);
        zzb(5019, zzaz);
    }

    public final void zza(zzs zzs, String str, int i, IBinder iBinder, Bundle bundle) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzaz.writeInt(i);
        zzaz.writeStrongBinder(iBinder);
        zzef.zza(zzaz, bundle);
        zzb(5025, zzaz);
    }

    public final void zza(zzs zzs, String str, int i, boolean z, boolean z2) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzaz.writeInt(i);
        zzef.zza(zzaz, z);
        zzef.zza(zzaz, z2);
        zzb(9020, zzaz);
    }

    public final void zza(zzs zzs, String str, long j, String str2) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzaz.writeLong(j);
        zzaz.writeString(str2);
        zzb((int) GamesStatusCodes.STATUS_INVALID_REAL_TIME_ROOM_ID, zzaz);
    }

    public final void zza(zzs zzs, String str, IBinder iBinder, Bundle bundle) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzaz.writeStrongBinder(iBinder);
        zzef.zza(zzaz, bundle);
        zzb(5023, zzaz);
    }

    public final void zza(zzs zzs, String str, zze zze, zzc zzc) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzef.zza(zzaz, zze);
        zzef.zza(zzaz, zzc);
        zzb(12007, zzaz);
    }

    public final void zza(zzs zzs, String str, String str2) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzaz.writeString(str2);
        zzb(8011, zzaz);
    }

    public final void zza(zzs zzs, String str, String str2, int i, int i2) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(null);
        zzaz.writeString(str2);
        zzaz.writeInt(i);
        zzaz.writeInt(i2);
        zzb((int) GamesStatusCodes.STATUS_MILESTONE_CLAIM_FAILED, zzaz);
    }

    public final void zza(zzs zzs, String str, String str2, zze zze, zzc zzc) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzaz.writeString(str2);
        zzef.zza(zzaz, zze);
        zzef.zza(zzaz, zzc);
        zzb(12033, zzaz);
    }

    public final void zza(zzs zzs, String str, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzef.zza(zzaz, z);
        zzb((int) GamesStatusCodes.STATUS_MATCH_ERROR_INVALID_MATCH_RESULTS, zzaz);
    }

    public final void zza(zzs zzs, String str, boolean z, int i) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzef.zza(zzaz, z);
        zzaz.writeInt(i);
        zzb(15001, zzaz);
    }

    public final void zza(zzs zzs, String str, byte[] bArr, String str2, ParticipantResult[] participantResultArr) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzaz.writeByteArray(bArr);
        zzaz.writeString(str2);
        zzaz.writeTypedArray(participantResultArr, 0);
        zzb(8007, zzaz);
    }

    public final void zza(zzs zzs, String str, byte[] bArr, ParticipantResult[] participantResultArr) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzaz.writeByteArray(bArr);
        zzaz.writeTypedArray(participantResultArr, 0);
        zzb(8008, zzaz);
    }

    public final void zza(zzs zzs, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzef.zza(zzaz, z);
        zzb((int) GamesStatusCodes.STATUS_MULTIPLAYER_ERROR_NOT_TRUSTED_TESTER, zzaz);
    }

    public final void zza(zzs zzs, boolean z, String[] strArr) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzef.zza(zzaz, z);
        zzaz.writeStringArray(strArr);
        zzb(12031, zzaz);
    }

    public final void zza(zzs zzs, int[] iArr, int i, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeIntArray(iArr);
        zzaz.writeInt(i);
        zzef.zza(zzaz, z);
        zzb(12010, zzaz);
    }

    public final void zza(zzs zzs, String[] strArr) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeStringArray(strArr);
        zzb((int) GamesActivityResultCodes.RESULT_NETWORK_FAILURE, zzaz);
    }

    public final void zza(zzs zzs, String[] strArr, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeStringArray(strArr);
        zzef.zza(zzaz, z);
        zzb(12029, zzaz);
    }

    public final void zza(zzu zzu, long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzu);
        zzaz.writeLong(j);
        zzb(15501, zzaz);
    }

    public final void zza(String str, IBinder iBinder, Bundle bundle) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeString(str);
        zzaz.writeStrongBinder(iBinder);
        zzef.zza(zzaz, bundle);
        zzb(13002, zzaz);
    }

    public final void zza(String str, zzs zzs) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeString(str);
        zzef.zza(zzaz, zzs);
        zzb(20001, zzaz);
    }

    public final void zzab(long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeLong(j);
        zzb(5001, zzaz);
    }

    public final void zzac(long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeLong(j);
        zzb(5059, zzaz);
    }

    public final void zzad(long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeLong(j);
        zzb(8013, zzaz);
    }

    public final void zzae(long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeLong(j);
        zzb((int) GamesActivityResultCodes.RESULT_SIGN_IN_FAILED, zzaz);
    }

    public final Bundle zzaew() throws RemoteException {
        Parcel zza = zza(5004, zzaz());
        Bundle bundle = (Bundle) zzef.zza(zza, Bundle.CREATOR);
        zza.recycle();
        return bundle;
    }

    public final void zzaf(long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeLong(j);
        zzb(12012, zzaz);
    }

    public final void zzag(long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeLong(j);
        zzb(22027, zzaz);
    }

    public final String zzarg() throws RemoteException {
        Parcel zza = zza(5007, zzaz());
        String readString = zza.readString();
        zza.recycle();
        return readString;
    }

    public final Intent zzarm() throws RemoteException {
        Parcel zza = zza((int) GamesStatusCodes.STATUS_VIDEO_STORAGE_ERROR, zzaz());
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final Intent zzaro() throws RemoteException {
        Parcel zza = zza(9005, zzaz());
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final Intent zzarp() throws RemoteException {
        Parcel zza = zza((int) GamesStatusCodes.STATUS_VIDEO_ALREADY_CAPTURING, zzaz());
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final Intent zzarq() throws RemoteException {
        Parcel zza = zza(9007, zzaz());
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final Intent zzary() throws RemoteException {
        Parcel zza = zza(9012, zzaz());
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final int zzasa() throws RemoteException {
        Parcel zza = zza(9019, zzaz());
        int readInt = zza.readInt();
        zza.recycle();
        return readInt;
    }

    public final int zzasd() throws RemoteException {
        Parcel zza = zza(8024, zzaz());
        int readInt = zza.readInt();
        zza.recycle();
        return readInt;
    }

    public final Intent zzasf() throws RemoteException {
        Parcel zza = zza(10015, zzaz());
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final int zzasg() throws RemoteException {
        Parcel zza = zza(10013, zzaz());
        int readInt = zza.readInt();
        zza.recycle();
        return readInt;
    }

    public final int zzash() throws RemoteException {
        Parcel zza = zza(10023, zzaz());
        int readInt = zza.readInt();
        zza.recycle();
        return readInt;
    }

    public final int zzasi() throws RemoteException {
        Parcel zza = zza(12035, zzaz());
        int readInt = zza.readInt();
        zza.recycle();
        return readInt;
    }

    public final int zzask() throws RemoteException {
        Parcel zza = zza(12036, zzaz());
        int readInt = zza.readInt();
        zza.recycle();
        return readInt;
    }

    public final boolean zzaso() throws RemoteException {
        Parcel zza = zza(22030, zzaz());
        boolean zza2 = zzef.zza(zza);
        zza.recycle();
        return zza2;
    }

    public final void zzass() throws RemoteException {
        zzb(5006, zzaz());
    }

    public final String zzasu() throws RemoteException {
        Parcel zza = zza(5012, zzaz());
        String readString = zza.readString();
        zza.recycle();
        return readString;
    }

    public final DataHolder zzasv() throws RemoteException {
        Parcel zza = zza(5013, zzaz());
        DataHolder dataHolder = (DataHolder) zzef.zza(zza, DataHolder.CREATOR);
        zza.recycle();
        return dataHolder;
    }

    public final DataHolder zzasw() throws RemoteException {
        Parcel zza = zza(5502, zzaz());
        DataHolder dataHolder = (DataHolder) zzef.zza(zza, DataHolder.CREATOR);
        zza.recycle();
        return dataHolder;
    }

    public final Intent zzasx() throws RemoteException {
        Parcel zza = zza(9010, zzaz());
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final Intent zzasy() throws RemoteException {
        Parcel zza = zza(19002, zzaz());
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final int zzb(byte[] bArr, String str, String[] strArr) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeByteArray(bArr);
        zzaz.writeString(str);
        zzaz.writeStringArray(strArr);
        Parcel zza = zza(5034, zzaz);
        int readInt = zza.readInt();
        zza.recycle();
        return readInt;
    }

    public final Intent zzb(int i, int i2, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeInt(i);
        zzaz.writeInt(i2);
        zzef.zza(zzaz, z);
        Parcel zza = zza(9008, zzaz);
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final void zzb(zzs zzs) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzb(5026, zzaz);
    }

    public final void zzb(zzs zzs, int i) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeInt(i);
        zzb(22016, zzaz);
    }

    public final void zzb(zzs zzs, long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeLong(j);
        zzb(8012, zzaz);
    }

    public final void zzb(zzs zzs, String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzb(8005, zzaz);
    }

    public final void zzb(zzs zzs, String str, int i, int i2, int i3, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzaz.writeInt(i);
        zzaz.writeInt(i2);
        zzaz.writeInt(i3);
        zzef.zza(zzaz, z);
        zzb(5020, zzaz);
    }

    public final void zzb(zzs zzs, String str, int i, IBinder iBinder, Bundle bundle) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzaz.writeInt(i);
        zzaz.writeStrongBinder(iBinder);
        zzef.zza(zzaz, bundle);
        zzb((int) GamesStatusCodes.STATUS_PARTICIPANT_NOT_CONNECTED, zzaz);
    }

    public final void zzb(zzs zzs, String str, IBinder iBinder, Bundle bundle) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzaz.writeStrongBinder(iBinder);
        zzef.zza(zzaz, bundle);
        zzb(5024, zzaz);
    }

    public final void zzb(zzs zzs, String str, String str2) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzaz.writeString(str2);
        zzb(12009, zzaz);
    }

    public final void zzb(zzs zzs, String str, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzef.zza(zzaz, z);
        zzb(13006, zzaz);
    }

    public final void zzb(zzs zzs, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzef.zza(zzaz, z);
        zzb((int) GamesStatusCodes.STATUS_MATCH_ERROR_OUT_OF_DATE_VERSION, zzaz);
    }

    public final void zzb(zzs zzs, String[] strArr) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeStringArray(strArr);
        zzb((int) GamesActivityResultCodes.RESULT_SEND_REQUEST_FAILED, zzaz);
    }

    public final void zzc(zzs zzs) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzb(21007, zzaz);
    }

    public final void zzc(zzs zzs, long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeLong(j);
        zzb((int) GamesActivityResultCodes.RESULT_RECONNECT_REQUIRED, zzaz);
    }

    public final void zzc(zzs zzs, String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzb(8006, zzaz);
    }

    public final void zzc(zzs zzs, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzef.zza(zzaz, z);
        zzb(8027, zzaz);
    }

    public final Intent zzd(int i, int i2, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeInt(i);
        zzaz.writeInt(i2);
        zzef.zza(zzaz, z);
        Parcel zza = zza((int) GamesStatusCodes.STATUS_VIDEO_OUT_OF_DISK_SPACE, zzaz);
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final Intent zzd(int[] iArr) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeIntArray(iArr);
        Parcel zza = zza(12030, zzaz);
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final void zzd(zzs zzs) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzb(22028, zzaz);
    }

    public final void zzd(zzs zzs, long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeLong(j);
        zzb(12011, zzaz);
    }

    public final void zzd(zzs zzs, String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzb(8009, zzaz);
    }

    public final void zzd(zzs zzs, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzef.zza(zzaz, z);
        zzb(12002, zzaz);
    }

    public final void zzdj(int i) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeInt(i);
        zzb(5036, zzaz);
    }

    public final void zze(zzs zzs, long j) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeLong(j);
        zzb(22026, zzaz);
    }

    public final void zze(zzs zzs, String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzb(8010, zzaz);
    }

    public final void zze(zzs zzs, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzef.zza(zzaz, z);
        zzb(12016, zzaz);
    }

    public final void zzf(zzs zzs, String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzb(8014, zzaz);
    }

    public final void zzf(zzs zzs, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzef.zza(zzaz, z);
        zzb(17001, zzaz);
    }

    public final void zzg(zzs zzs, String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzb(12020, zzaz);
    }

    public final void zzh(zzs zzs, String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzs);
        zzaz.writeString(str);
        zzb(12008, zzaz);
    }

    public final Intent zzhm(String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeString(str);
        Parcel zza = zza(12034, zzaz);
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final void zzho(String str) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeString(str);
        zzb((int) GamesStatusCodes.STATUS_QUEST_NO_LONGER_AVAILABLE, zzaz);
    }

    public final Intent zzk(String str, int i, int i2) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeString(str);
        zzaz.writeInt(i);
        zzaz.writeInt(i2);
        Parcel zza = zza(18001, zzaz);
        Intent intent = (Intent) zzef.zza(zza, Intent.CREATOR);
        zza.recycle();
        return intent;
    }

    public final void zzp(String str, int i) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeString(str);
        zzaz.writeInt(i);
        zzb(12017, zzaz);
    }

    public final void zzq(String str, int i) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeString(str);
        zzaz.writeInt(i);
        zzb(5029, zzaz);
    }

    public final void zzs(String str, int i) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeString(str);
        zzaz.writeInt(i);
        zzb(5028, zzaz);
    }
}
