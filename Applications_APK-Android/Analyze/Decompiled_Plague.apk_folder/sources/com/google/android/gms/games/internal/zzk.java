package com.google.android.gms.games.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.AnnotatedData;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.TimeUnit;

final class zzk implements PendingResult.zza {
    private /* synthetic */ PendingResult zzfym;
    private /* synthetic */ TaskCompletionSource zzfyn;
    private /* synthetic */ zzbo zzhpj;
    private /* synthetic */ zzo zzhpn;

    zzk(PendingResult pendingResult, TaskCompletionSource taskCompletionSource, zzbo zzbo, zzo zzo) {
        this.zzfym = pendingResult;
        this.zzfyn = taskCompletionSource;
        this.zzhpj = zzbo;
        this.zzhpn = zzo;
    }

    public final void zzr(@NonNull Status status) {
        boolean z = status.getStatusCode() == 3;
        Result await = this.zzfym.await(0, TimeUnit.MILLISECONDS);
        if (status.isSuccess() || z) {
            this.zzfyn.setResult(new AnnotatedData(this.zzhpj.zzb(await), z));
            return;
        }
        if (!(await == null || this.zzhpn == null)) {
            this.zzhpn.release(await);
        }
        this.zzfyn.setException(zzb.zzy(zzg.zzah(status)));
    }
}
