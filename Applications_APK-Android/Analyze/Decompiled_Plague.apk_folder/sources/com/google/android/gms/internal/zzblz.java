package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzblz extends zzblk {
    private /* synthetic */ zzblv zzgli;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzblz(zzblv zzblv, GoogleApiClient googleApiClient) {
        super(googleApiClient);
        this.zzgli = zzblv;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbjy(this.zzgli.zzglf.getRequestId(), false), new zzbrj(this));
    }
}
