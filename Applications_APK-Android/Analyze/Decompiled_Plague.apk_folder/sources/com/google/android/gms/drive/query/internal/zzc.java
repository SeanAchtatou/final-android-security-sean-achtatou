package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.internal.zzbek;

public final class zzc implements Parcelable.Creator<zzb> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        zzx zzx = null;
        MetadataBundle metadataBundle = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    zzx = (zzx) zzbek.zza(parcel, readInt, zzx.CREATOR);
                    break;
                case 2:
                    metadataBundle = (MetadataBundle) zzbek.zza(parcel, readInt, MetadataBundle.CREATOR);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzb(zzx, metadataBundle);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzb[i];
    }
}
