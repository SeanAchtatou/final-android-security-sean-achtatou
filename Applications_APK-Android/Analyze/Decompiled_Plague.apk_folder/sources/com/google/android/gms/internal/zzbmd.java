package com.google.android.gms.internal;

import com.google.android.gms.common.api.internal.zzco;
import com.google.android.gms.drive.DriveFile;

final class zzbmd implements zzco<DriveFile.DownloadProgressListener> {
    private /* synthetic */ long zzglo;
    private /* synthetic */ long zzglp;

    zzbmd(zzbmc zzbmc, long j, long j2) {
        this.zzglo = j;
        this.zzglp = j2;
    }

    public final void zzahn() {
    }

    public final /* synthetic */ void zzt(Object obj) {
        ((DriveFile.DownloadProgressListener) obj).onProgress(this.zzglo, this.zzglp);
    }
}
