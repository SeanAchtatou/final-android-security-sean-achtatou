package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzd extends zzb<Void> {
    private zzct<Api.zzb, ?> zzfkw;
    private zzdp<Api.zzb, ?> zzfkx;

    public zzd(zzcu zzcu, TaskCompletionSource<Void> taskCompletionSource) {
        super(3, taskCompletionSource);
        this.zzfkw = zzcu.zzfkw;
        this.zzfkx = zzcu.zzfkx;
    }

    public final /* bridge */ /* synthetic */ void zza(@NonNull zzah zzah, boolean z) {
    }

    public final void zzb(zzbr<?> zzbr) throws RemoteException {
        this.zzfkw.zzb(zzbr.zzahd(), this.zzeay);
        if (this.zzfkw.zzajc() != null) {
            zzbr.zzaim().put(this.zzfkw.zzajc(), new zzcu(this.zzfkw, this.zzfkx));
        }
    }

    public final /* bridge */ /* synthetic */ void zzs(@NonNull Status status) {
        super.zzs(status);
    }
}
