package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class zzdrx implements zzdtb {
    private final SecretKeySpec zzltz;
    private final int zzlua;
    private final int zzlub = zzdsr.zzlvk.zzoc("AES/CTR/NoPadding").getBlockSize();

    public zzdrx(byte[] bArr, int i) throws GeneralSecurityException {
        this.zzltz = new SecretKeySpec(bArr, "AES");
        if (i < 12 || i > this.zzlub) {
            throw new GeneralSecurityException("invalid IV size");
        }
        this.zzlua = i;
    }

    public final byte[] zzaf(byte[] bArr) throws GeneralSecurityException {
        if (bArr.length > Integer.MAX_VALUE - this.zzlua) {
            StringBuilder sb = new StringBuilder(43);
            sb.append("plaintext length can not exceed ");
            sb.append(Integer.MAX_VALUE - this.zzlua);
            throw new GeneralSecurityException(sb.toString());
        }
        byte[] bArr2 = new byte[(this.zzlua + bArr.length)];
        byte[] zzgb = zzdtd.zzgb(this.zzlua);
        System.arraycopy(zzgb, 0, bArr2, 0, this.zzlua);
        int length = bArr.length;
        int i = this.zzlua;
        Cipher zzoc = zzdsr.zzlvk.zzoc("AES/CTR/NoPadding");
        byte[] bArr3 = new byte[this.zzlub];
        System.arraycopy(zzgb, 0, bArr3, 0, this.zzlua);
        zzoc.init(1, this.zzltz, new IvParameterSpec(bArr3));
        if (zzoc.doFinal(bArr, 0, length, bArr2, i) == length) {
            return bArr2;
        }
        throw new GeneralSecurityException("stored output's length does not match input's length");
    }
}
