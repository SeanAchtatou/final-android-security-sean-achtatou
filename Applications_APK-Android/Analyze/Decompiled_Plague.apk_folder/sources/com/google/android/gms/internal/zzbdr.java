package com.google.android.gms.internal;

import android.content.Context;
import android.util.Log;
import com.tapjoy.TapjoyConstants;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public final class zzbdr implements zzbcy {
    private static final Charset UTF_8 = Charset.forName("UTF-8");
    private static final zzctn zzfho = new zzctn(zzctc.zzkm("com.google.android.gms.clearcut.public")).zzko("gms:playlog:service:sampling_").zzkp("LogSampling__");
    private static Map<String, zzctg<String>> zzfhp;
    private static Boolean zzfhq;
    private static Long zzfhr;
    private final Context zzaif;

    public zzbdr(Context context) {
        this.zzaif = context;
        if (zzfhp == null) {
            zzfhp = new HashMap();
        }
        if (this.zzaif != null) {
            zzctg.zzdw(this.zzaif);
        }
    }

    private static boolean zzbz(Context context) {
        if (zzfhq == null) {
            zzfhq = Boolean.valueOf(zzbgc.zzcy(context).checkCallingOrSelfPermission("com.google.android.providers.gsf.permission.READ_GSERVICES") == 0);
        }
        return zzfhq.booleanValue();
    }

    private static zzbds zzfr(String str) {
        if (str == null) {
            return null;
        }
        String str2 = "";
        int indexOf = str.indexOf(44);
        int i = 0;
        if (indexOf >= 0) {
            str2 = str.substring(0, indexOf);
            i = indexOf + 1;
        }
        String str3 = str2;
        int indexOf2 = str.indexOf(47, i);
        if (indexOf2 <= 0) {
            String valueOf = String.valueOf(str);
            Log.e("LogSamplerImpl", valueOf.length() != 0 ? "Failed to parse the rule: ".concat(valueOf) : new String("Failed to parse the rule: "));
            return null;
        }
        try {
            long parseLong = Long.parseLong(str.substring(i, indexOf2));
            long parseLong2 = Long.parseLong(str.substring(indexOf2 + 1));
            if (parseLong >= 0 && parseLong2 >= 0) {
                return new zzbds(str3, parseLong, parseLong2);
            }
            StringBuilder sb = new StringBuilder(72);
            sb.append("negative values not supported: ");
            sb.append(parseLong);
            sb.append("/");
            sb.append(parseLong2);
            Log.e("LogSamplerImpl", sb.toString());
            return null;
        } catch (NumberFormatException e) {
            String valueOf2 = String.valueOf(str);
            Log.e("LogSamplerImpl", valueOf2.length() != 0 ? "parseLong() failed while parsing: ".concat(valueOf2) : new String("parseLong() failed while parsing: "), e);
            return null;
        }
    }

    public final boolean zzg(String str, int i) {
        long j;
        byte[] bArr;
        long j2;
        long j3;
        String str2 = null;
        String valueOf = (str == null || str.isEmpty()) ? i >= 0 ? String.valueOf(i) : null : str;
        if (valueOf == null) {
            return true;
        }
        if (this.zzaif != null && zzbz(this.zzaif)) {
            zzctg<String> zzctg = zzfhp.get(valueOf);
            if (zzctg == null) {
                zzctg = zzfho.zzaw(valueOf, null);
                zzfhp.put(valueOf, zzctg);
            }
            str2 = zzctg.get();
        }
        zzbds zzfr = zzfr(str2);
        if (zzfr == null) {
            return true;
        }
        String str3 = zzfr.zzfhs;
        Context context = this.zzaif;
        if (zzfhr == null) {
            if (context != null) {
                zzfhr = zzbz(context) ? Long.valueOf(zzdld.getLong(context.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID, 0)) : 0L;
            } else {
                j = 0;
                if (str3 != null || str3.isEmpty()) {
                    bArr = ByteBuffer.allocate(8).putLong(j).array();
                } else {
                    byte[] bytes = str3.getBytes(UTF_8);
                    ByteBuffer allocate = ByteBuffer.allocate(bytes.length + 8);
                    allocate.put(bytes);
                    allocate.putLong(j);
                    bArr = allocate.array();
                }
                long zzi = zzbdm.zzi(bArr);
                j2 = zzfr.zzfht;
                j3 = zzfr.zzfhu;
                if (j2 >= 0 || j3 < 0) {
                    StringBuilder sb = new StringBuilder(72);
                    sb.append("negative values not supported: ");
                    sb.append(j2);
                    sb.append("/");
                    sb.append(j3);
                    throw new IllegalArgumentException(sb.toString());
                } else if (j3 <= 0) {
                    return false;
                } else {
                    if (zzi < 0) {
                        zzi = (Long.MAX_VALUE % j3) + 1 + ((zzi & Long.MAX_VALUE) % j3);
                    }
                    return zzi % j3 < j2;
                }
            }
        }
        j = zzfhr.longValue();
        if (str3 != null) {
        }
        bArr = ByteBuffer.allocate(8).putLong(j).array();
        long zzi2 = zzbdm.zzi(bArr);
        j2 = zzfr.zzfht;
        j3 = zzfr.zzfhu;
        if (j2 >= 0) {
        }
        StringBuilder sb2 = new StringBuilder(72);
        sb2.append("negative values not supported: ");
        sb2.append(j2);
        sb2.append("/");
        sb2.append(j3);
        throw new IllegalArgumentException(sb2.toString());
    }
}
