package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.zzc;
import java.util.Set;

public abstract class zzab<T extends IInterface> extends zzd<T> implements Api.zze, zzaf {
    private final Account zzdzb;
    private final Set<Scope> zzees;
    private final zzr zzfnd;

    protected zzab(Context context, Looper looper, int i, zzr zzr, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this(context, looper, zzag.zzcl(context), GoogleApiAvailability.getInstance(), i, zzr, (GoogleApiClient.ConnectionCallbacks) zzbq.checkNotNull(connectionCallbacks), (GoogleApiClient.OnConnectionFailedListener) zzbq.checkNotNull(onConnectionFailedListener));
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private zzab(android.content.Context r11, android.os.Looper r12, com.google.android.gms.common.internal.zzag r13, com.google.android.gms.common.GoogleApiAvailability r14, int r15, com.google.android.gms.common.internal.zzr r16, com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks r17, com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener r18) {
        /*
            r10 = this;
            r9 = r10
            r0 = r17
            r1 = r18
            r2 = 0
            if (r0 != 0) goto L_0x000a
            r6 = r2
            goto L_0x0010
        L_0x000a:
            com.google.android.gms.common.internal.zzac r3 = new com.google.android.gms.common.internal.zzac
            r3.<init>(r0)
            r6 = r3
        L_0x0010:
            if (r1 != 0) goto L_0x0014
            r7 = r2
            goto L_0x001a
        L_0x0014:
            com.google.android.gms.common.internal.zzad r0 = new com.google.android.gms.common.internal.zzad
            r0.<init>(r1)
            r7 = r0
        L_0x001a:
            java.lang.String r8 = r16.zzakn()
            r0 = r9
            r1 = r11
            r2 = r12
            r3 = r13
            r4 = r14
            r5 = r15
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            r0 = r16
            r9.zzfnd = r0
            android.accounts.Account r1 = r16.getAccount()
            r9.zzdzb = r1
            java.util.Set r0 = r16.zzakk()
            java.util.Set r1 = r9.zzb(r0)
            java.util.Iterator r2 = r1.iterator()
        L_0x003d:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0057
            java.lang.Object r3 = r2.next()
            com.google.android.gms.common.api.Scope r3 = (com.google.android.gms.common.api.Scope) r3
            boolean r3 = r0.contains(r3)
            if (r3 != 0) goto L_0x003d
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Expanding scopes is not permitted, use implied scopes instead"
            r0.<init>(r1)
            throw r0
        L_0x0057:
            r9.zzees = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.zzab.<init>(android.content.Context, android.os.Looper, com.google.android.gms.common.internal.zzag, com.google.android.gms.common.GoogleApiAvailability, int, com.google.android.gms.common.internal.zzr, com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks, com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener):void");
    }

    public final Account getAccount() {
        return this.zzdzb;
    }

    public zzc[] zzajz() {
        return new zzc[0];
    }

    /* access modifiers changed from: protected */
    public final Set<Scope> zzakd() {
        return this.zzees;
    }

    /* access modifiers changed from: protected */
    public final zzr zzakv() {
        return this.zzfnd;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public Set<Scope> zzb(@NonNull Set<Scope> set) {
        return set;
    }
}
