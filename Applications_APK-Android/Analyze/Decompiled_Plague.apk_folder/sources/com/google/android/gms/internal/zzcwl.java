package com.google.android.gms.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.internal.zzz;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzan;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzbr;
import com.google.android.gms.common.internal.zzm;
import com.google.android.gms.common.internal.zzr;

public final class zzcwl extends zzab<zzcwj> implements zzcwb {
    private final zzr zzfnd;
    private Integer zzfwp;
    private final boolean zzjzg;
    private final Bundle zzjzh;

    private zzcwl(Context context, Looper looper, boolean z, zzr zzr, Bundle bundle, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 44, zzr, connectionCallbacks, onConnectionFailedListener);
        this.zzjzg = true;
        this.zzfnd = zzr;
        this.zzjzh = bundle;
        this.zzfwp = zzr.zzakq();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzcwl.<init>(android.content.Context, android.os.Looper, boolean, com.google.android.gms.common.internal.zzr, android.os.Bundle, com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks, com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener):void
     arg types: [android.content.Context, android.os.Looper, int, com.google.android.gms.common.internal.zzr, android.os.Bundle, com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks, com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener]
     candidates:
      com.google.android.gms.internal.zzcwl.<init>(android.content.Context, android.os.Looper, boolean, com.google.android.gms.common.internal.zzr, com.google.android.gms.internal.zzcwc, com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks, com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener):void
      com.google.android.gms.internal.zzcwl.<init>(android.content.Context, android.os.Looper, boolean, com.google.android.gms.common.internal.zzr, android.os.Bundle, com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks, com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener):void */
    public zzcwl(Context context, Looper looper, boolean z, zzr zzr, zzcwc zzcwc, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this(context, looper, true, zzr, zza(zzr), connectionCallbacks, onConnectionFailedListener);
    }

    public static Bundle zza(zzr zzr) {
        zzcwc zzakp = zzr.zzakp();
        Integer zzakq = zzr.zzakq();
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", zzr.getAccount());
        if (zzakq != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", zzakq.intValue());
        }
        if (zzakp != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", zzakp.zzbcq());
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", zzakp.isIdTokenRequested());
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", zzakp.getServerClientId());
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", zzakp.zzbcr());
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", zzakp.zzbcs());
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", zzakp.zzbct());
            if (zzakp.zzbcu() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", zzakp.zzbcu().longValue());
            }
            if (zzakp.zzbcv() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", zzakp.zzbcv().longValue());
            }
        }
        return bundle;
    }

    public final void connect() {
        zza(new zzm(this));
    }

    public final void zza(zzan zzan, boolean z) {
        try {
            ((zzcwj) zzakb()).zza(zzan, this.zzfwp.intValue(), z);
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when saveDefaultAccount is called");
        }
    }

    public final void zza(zzcwh zzcwh) {
        zzbq.checkNotNull(zzcwh, "Expecting a valid ISignInCallbacks");
        try {
            Account zzakh = this.zzfnd.zzakh();
            GoogleSignInAccount googleSignInAccount = null;
            if ("<<default account>>".equals(zzakh.name)) {
                googleSignInAccount = zzz.zzbr(getContext()).zzabg();
            }
            ((zzcwj) zzakb()).zza(new zzcwm(new zzbr(zzakh, this.zzfwp.intValue(), googleSignInAccount)), zzcwh);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
            try {
                zzcwh.zzb(new zzcwo(8));
            } catch (RemoteException unused) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final Bundle zzaae() {
        if (!getContext().getPackageName().equals(this.zzfnd.zzakm())) {
            this.zzjzh.putString("com.google.android.gms.signin.internal.realClientPackageName", this.zzfnd.zzakm());
        }
        return this.zzjzh;
    }

    public final boolean zzaam() {
        return this.zzjzg;
    }

    public final void zzbcp() {
        try {
            ((zzcwj) zzakb()).zzeh(this.zzfwp.intValue());
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when clearAccountFromSessionStore is called");
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ IInterface zzd(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
        return queryLocalInterface instanceof zzcwj ? (zzcwj) queryLocalInterface : new zzcwk(iBinder);
    }

    /* access modifiers changed from: protected */
    public final String zzhf() {
        return "com.google.android.gms.signin.service.START";
    }

    /* access modifiers changed from: protected */
    public final String zzhg() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }
}
