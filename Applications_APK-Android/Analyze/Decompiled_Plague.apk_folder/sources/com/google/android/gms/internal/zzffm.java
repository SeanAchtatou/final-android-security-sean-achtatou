package com.google.android.gms.internal;

public interface zzffm<MessageType> {
    MessageType zzc(zzfdq zzfdq, zzfea zzfea) throws zzfew;

    MessageType zze(zzfdq zzfdq, zzfea zzfea) throws zzfew;
}
