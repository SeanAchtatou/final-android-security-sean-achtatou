package com.google.android.gms.internal;

import android.content.IntentSender;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzdf;
import com.google.android.gms.drive.OpenFileActivityOptions;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzblt extends zzdf<zzbll, IntentSender> {
    private /* synthetic */ OpenFileActivityOptions zzgld;

    zzblt(zzblq zzblq, OpenFileActivityOptions openFileActivityOptions) {
        this.zzgld = openFileActivityOptions;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        try {
            taskCompletionSource.setResult(((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbqx(this.zzgld.title, this.zzgld.zzgho, this.zzgld.zzghq, this.zzgld.zzghp)));
        } catch (RemoteException e) {
            taskCompletionSource.setException(e);
        }
    }
}
