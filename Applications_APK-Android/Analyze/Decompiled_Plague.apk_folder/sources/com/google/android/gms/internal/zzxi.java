package com.google.android.gms.internal;

import org.json.JSONObject;

@zzzb
public final class zzxi extends zzafh {
    /* access modifiers changed from: private */
    public final zzxg zzchu;
    private final zzaev zzchv;
    private final zzaad zzchw = this.zzchv.zzcwe;

    public zzxi(zzaev zzaev, zzxg zzxg) {
        this.zzchv = zzaev;
        this.zzchu = zzxg;
    }

    public final void onStop() {
    }

    public final void zzdg() {
        zzis zzis = this.zzchv.zzcpe.zzclo;
        int i = this.zzchw.orientation;
        long j = this.zzchw.zzccb;
        String str = this.zzchv.zzcpe.zzclr;
        long j2 = this.zzchw.zzcnh;
        zziw zziw = this.zzchv.zzath;
        long j3 = this.zzchw.zzcnf;
        long j4 = this.zzchv.zzcvw;
        long j5 = j2;
        long j6 = this.zzchw.zzcnk;
        String str2 = this.zzchw.zzcnl;
        JSONObject jSONObject = this.zzchv.zzcvq;
        boolean z = this.zzchv.zzcwe.zzcny;
        zzaaf zzaaf = this.zzchv.zzcwe.zzcnz;
        zzib zzib = this.zzchv.zzcwc;
        String str3 = str2;
        zziw zziw2 = zziw;
        long j7 = j5;
        zzaeu zzaeu = r1;
        long j8 = j3;
        long j9 = j4;
        long j10 = j6;
        zzaeu zzaeu2 = new zzaeu(zzis, null, null, 0, null, null, i, j, str, false, null, null, null, null, null, j7, zziw2, j8, j9, j10, str3, jSONObject, null, null, null, null, z, zzaaf, null, null, null, zzib, this.zzchv.zzcwe.zzapy, this.zzchv.zzcwd);
        zzagr.zzczc.post(new zzxj(this, zzaeu));
    }
}
