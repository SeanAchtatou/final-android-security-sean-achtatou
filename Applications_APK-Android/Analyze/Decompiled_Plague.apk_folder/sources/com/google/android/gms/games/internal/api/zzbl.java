package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzbl extends zzbq {
    private /* synthetic */ String zzhqg;
    private /* synthetic */ String zzhqh;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbl(zzbj zzbj, GoogleApiClient googleApiClient, String str, String str2) {
        super(googleApiClient, null);
        this.zzhqg = str;
        this.zzhqh = str2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zzb(this, this.zzhqg, this.zzhqh);
    }
}
