package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.TransformedResult;
import com.google.android.gms.common.internal.zzbq;
import java.lang.ref.WeakReference;

public final class zzdi<R extends Result> extends TransformedResult<R> implements ResultCallback<R> {
    /* access modifiers changed from: private */
    public final Object zzflz = new Object();
    /* access modifiers changed from: private */
    public final WeakReference<GoogleApiClient> zzfmb;
    /* access modifiers changed from: private */
    public ResultTransform<? super R, ? extends Result> zzfsd = null;
    /* access modifiers changed from: private */
    public zzdi<? extends Result> zzfse = null;
    private volatile ResultCallbacks<? super R> zzfsf = null;
    private PendingResult<R> zzfsg = null;
    private Status zzfsh = null;
    /* access modifiers changed from: private */
    public final zzdk zzfsi;
    private boolean zzfsj = false;

    public zzdi(WeakReference<GoogleApiClient> weakReference) {
        zzbq.checkNotNull(weakReference, "GoogleApiClient reference must not be null");
        this.zzfmb = weakReference;
        GoogleApiClient googleApiClient = this.zzfmb.get();
        this.zzfsi = new zzdk(this, googleApiClient != null ? googleApiClient.getLooper() : Looper.getMainLooper());
    }

    private final void zzajf() {
        if (this.zzfsd != null || this.zzfsf != null) {
            GoogleApiClient googleApiClient = this.zzfmb.get();
            if (!(this.zzfsj || this.zzfsd == null || googleApiClient == null)) {
                googleApiClient.zza(this);
                this.zzfsj = true;
            }
            if (this.zzfsh != null) {
                zzx(this.zzfsh);
            } else if (this.zzfsg != null) {
                this.zzfsg.setResultCallback(this);
            }
        }
    }

    private final boolean zzajh() {
        return (this.zzfsf == null || this.zzfmb.get() == null) ? false : true;
    }

    /* access modifiers changed from: private */
    public static void zzd(Result result) {
        if (result instanceof Releasable) {
            try {
                ((Releasable) result).release();
            } catch (RuntimeException e) {
                String valueOf = String.valueOf(result);
                StringBuilder sb = new StringBuilder(18 + String.valueOf(valueOf).length());
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("TransformedResultImpl", sb.toString(), e);
            }
        }
    }

    /* access modifiers changed from: private */
    public final void zzd(Status status) {
        synchronized (this.zzflz) {
            this.zzfsh = status;
            zzx(this.zzfsh);
        }
    }

    private final void zzx(Status status) {
        synchronized (this.zzflz) {
            if (this.zzfsd != null) {
                Status onFailure = this.zzfsd.onFailure(status);
                zzbq.checkNotNull(onFailure, "onFailure must not return null");
                this.zzfse.zzd(onFailure);
            } else if (zzajh()) {
                this.zzfsf.onFailure(status);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final void andFinally(@NonNull ResultCallbacks<? super R> resultCallbacks) {
        synchronized (this.zzflz) {
            boolean z = false;
            zzbq.zza(this.zzfsf == null, (Object) "Cannot call andFinally() twice.");
            if (this.zzfsd == null) {
                z = true;
            }
            zzbq.zza(z, (Object) "Cannot call then() and andFinally() on the same TransformedResult.");
            this.zzfsf = resultCallbacks;
            zzajf();
        }
    }

    public final void onResult(R r) {
        synchronized (this.zzflz) {
            if (!r.getStatus().isSuccess()) {
                zzd(r.getStatus());
                zzd((Result) r);
            } else if (this.zzfsd != null) {
                zzcv.zzaid().submit(new zzdj(this, r));
            } else if (zzajh()) {
                this.zzfsf.onSuccess(r);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    @NonNull
    public final <S extends Result> TransformedResult<S> then(@NonNull ResultTransform<? super R, ? extends S> resultTransform) {
        zzdi<? extends Result> zzdi;
        synchronized (this.zzflz) {
            boolean z = false;
            zzbq.zza(this.zzfsd == null, (Object) "Cannot call then() twice.");
            if (this.zzfsf == null) {
                z = true;
            }
            zzbq.zza(z, (Object) "Cannot call then() and andFinally() on the same TransformedResult.");
            this.zzfsd = resultTransform;
            zzdi = new zzdi<>(this.zzfmb);
            this.zzfse = zzdi;
            zzajf();
        }
        return zzdi;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.google.android.gms.common.api.PendingResult<?>, com.google.android.gms.common.api.PendingResult<R>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(com.google.android.gms.common.api.PendingResult<?> r2) {
        /*
            r1 = this;
            java.lang.Object r0 = r1.zzflz
            monitor-enter(r0)
            r1.zzfsg = r2     // Catch:{ all -> 0x000a }
            r1.zzajf()     // Catch:{ all -> 0x000a }
            monitor-exit(r0)     // Catch:{ all -> 0x000a }
            return
        L_0x000a:
            r2 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x000a }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzdi.zza(com.google.android.gms.common.api.PendingResult):void");
    }

    /* access modifiers changed from: package-private */
    public final void zzajg() {
        this.zzfsf = null;
    }
}
