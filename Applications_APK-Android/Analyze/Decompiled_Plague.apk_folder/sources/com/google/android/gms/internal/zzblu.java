package com.google.android.gms.internal;

import android.content.IntentSender;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzdf;
import com.google.android.gms.drive.CreateFileActivityOptions;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzblu extends zzdf<zzbll, IntentSender> {
    private /* synthetic */ CreateFileActivityOptions zzgle;

    zzblu(zzblq zzblq, CreateFileActivityOptions createFileActivityOptions) {
        this.zzgle = createFileActivityOptions;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        zzbll zzbll = (zzbll) zzb;
        try {
            this.zzgle.zzgjt.setContext(zzbll.getContext());
            taskCompletionSource.setResult(((zzbpf) zzbll.zzakb()).zza(new zzbke(this.zzgle.zzgjt, this.zzgle.zzgju.intValue(), this.zzgle.title, this.zzgle.zzghq, Integer.valueOf(this.zzgle.zzgjv))));
        } catch (RemoteException e) {
            taskCompletionSource.setException(e);
        }
    }
}
