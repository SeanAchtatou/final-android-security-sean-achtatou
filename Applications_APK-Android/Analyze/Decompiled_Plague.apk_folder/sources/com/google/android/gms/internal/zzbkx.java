package com.google.android.gms.internal;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;

final class zzbkx implements Releasable, DriveApi.DriveContentsResult {
    private final Status mStatus;
    private final DriveContents zzggb;

    public zzbkx(Status status, DriveContents driveContents) {
        this.mStatus = status;
        this.zzggb = driveContents;
    }

    public final DriveContents getDriveContents() {
        return this.zzggb;
    }

    public final Status getStatus() {
        return this.mStatus;
    }

    public final void release() {
        if (this.zzggb != null) {
            this.zzggb.zzanq();
        }
    }
}
