package com.google.android.gms.drive.query;

import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.SearchableCollectionMetadataField;
import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.SearchableOrderedMetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.drive.query.internal.zzb;
import com.google.android.gms.drive.query.internal.zzd;
import com.google.android.gms.drive.query.internal.zzn;
import com.google.android.gms.drive.query.internal.zzp;
import com.google.android.gms.drive.query.internal.zzr;
import com.google.android.gms.drive.query.internal.zzv;
import com.google.android.gms.drive.query.internal.zzx;
import com.google.android.gms.drive.query.internal.zzz;

public class Filters {
    public static Filter and(@NonNull Filter filter, @NonNull Filter... filterArr) {
        zzbq.checkNotNull(filter, "Filter may not be null.");
        zzbq.checkNotNull(filterArr, "Additional filters may not be null.");
        return new zzr(zzx.zzgta, filter, filterArr);
    }

    public static Filter and(@NonNull Iterable<Filter> iterable) {
        zzbq.checkNotNull(iterable, "Filters may not be null");
        return new zzr(zzx.zzgta, iterable);
    }

    public static Filter contains(@NonNull SearchableMetadataField<String> searchableMetadataField, @NonNull String str) {
        zzbq.checkNotNull(searchableMetadataField, "Field may not be null.");
        zzbq.checkNotNull(str, "Value may not be null.");
        return new zzb(zzx.zzgtd, searchableMetadataField, str);
    }

    public static Filter eq(@NonNull CustomPropertyKey customPropertyKey, @NonNull String str) {
        zzbq.checkNotNull(customPropertyKey, "Custom property key may not be null.");
        zzbq.checkNotNull(str, "Custom property value may not be null.");
        return new zzn(SearchableField.zzgsa, new AppVisibleCustomProperties.zza().zza(customPropertyKey, str).zzapa());
    }

    public static <T> Filter eq(@NonNull SearchableMetadataField<T> searchableMetadataField, @NonNull T t) {
        zzbq.checkNotNull(searchableMetadataField, "Field may not be null.");
        zzbq.checkNotNull(t, "Value may not be null.");
        return new zzb(zzx.zzgsv, searchableMetadataField, t);
    }

    public static <T extends Comparable<T>> Filter greaterThan(@NonNull SearchableOrderedMetadataField<T> searchableOrderedMetadataField, @NonNull T t) {
        zzbq.checkNotNull(searchableOrderedMetadataField, "Field may not be null.");
        zzbq.checkNotNull(t, "Value may not be null.");
        return new zzb(zzx.zzgsy, searchableOrderedMetadataField, t);
    }

    public static <T extends Comparable<T>> Filter greaterThanEquals(@NonNull SearchableOrderedMetadataField<T> searchableOrderedMetadataField, @NonNull T t) {
        zzbq.checkNotNull(searchableOrderedMetadataField, "Field may not be null.");
        zzbq.checkNotNull(t, "Value may not be null.");
        return new zzb(zzx.zzgsz, searchableOrderedMetadataField, t);
    }

    public static <T> Filter in(@NonNull SearchableCollectionMetadataField<T> searchableCollectionMetadataField, @NonNull T t) {
        zzbq.checkNotNull(searchableCollectionMetadataField, "Field may not be null.");
        zzbq.checkNotNull(t, "Value may not be null.");
        return new zzp(searchableCollectionMetadataField, t);
    }

    public static <T extends Comparable<T>> Filter lessThan(@NonNull SearchableOrderedMetadataField<T> searchableOrderedMetadataField, @NonNull T t) {
        zzbq.checkNotNull(searchableOrderedMetadataField, "Field may not be null.");
        zzbq.checkNotNull(t, "Value may not be null.");
        return new zzb(zzx.zzgsw, searchableOrderedMetadataField, t);
    }

    public static <T extends Comparable<T>> Filter lessThanEquals(@NonNull SearchableOrderedMetadataField<T> searchableOrderedMetadataField, @NonNull T t) {
        zzbq.checkNotNull(searchableOrderedMetadataField, "Field may not be null.");
        zzbq.checkNotNull(t, "Value may not be null.");
        return new zzb(zzx.zzgsx, searchableOrderedMetadataField, t);
    }

    public static Filter not(@NonNull Filter filter) {
        zzbq.checkNotNull(filter, "Filter may not be null");
        return new zzv(filter);
    }

    public static Filter openedByMe() {
        return new zzd(SearchableField.LAST_VIEWED_BY_ME);
    }

    public static Filter or(@NonNull Filter filter, @NonNull Filter... filterArr) {
        zzbq.checkNotNull(filter, "Filter may not be null.");
        zzbq.checkNotNull(filterArr, "Additional filters may not be null.");
        return new zzr(zzx.zzgtb, filter, filterArr);
    }

    public static Filter or(@NonNull Iterable<Filter> iterable) {
        zzbq.checkNotNull(iterable, "Filters may not be null");
        return new zzr(zzx.zzgtb, iterable);
    }

    public static Filter ownedByMe() {
        return new zzz();
    }

    public static Filter sharedWithMe() {
        return new zzd(SearchableField.zzgrz);
    }
}
