package com.google.android.gms.internal;

@zzzb
public final class zzxf {
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r7v0, types: [com.google.android.gms.internal.zzahi, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r7v5 */
    /* JADX WARN: Type inference failed for: r0v14, types: [com.google.android.gms.internal.zzxn] */
    /* JADX WARN: Type inference failed for: r7v6 */
    /* JADX WARN: Type inference failed for: r7v7 */
    /* JADX WARN: Type inference failed for: r0v15, types: [com.google.android.gms.internal.zzxl] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.internal.zzahi zza(android.content.Context r8, com.google.android.gms.ads.internal.zza r9, com.google.android.gms.internal.zzaev r10, com.google.android.gms.internal.zzcs r11, @android.support.annotation.Nullable com.google.android.gms.internal.zzama r12, com.google.android.gms.internal.zzuc r13, com.google.android.gms.internal.zzxg r14, com.google.android.gms.internal.zznd r15) {
        /*
            com.google.android.gms.internal.zzaad r2 = r10.zzcwe
            boolean r4 = r2.zzcng
            if (r4 == 0) goto L_0x0013
            com.google.android.gms.internal.zzxl r7 = new com.google.android.gms.internal.zzxl
            r0 = r7
            r1 = r8
            r2 = r10
            r3 = r13
            r4 = r14
            r5 = r15
            r6 = r12
            r0.<init>(r1, r2, r3, r4, r5, r6)
            goto L_0x006e
        L_0x0013:
            boolean r4 = r2.zzbdd
            if (r4 != 0) goto L_0x0052
            boolean r4 = r9 instanceof com.google.android.gms.ads.internal.zzba
            if (r4 == 0) goto L_0x001c
            goto L_0x0052
        L_0x001c:
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r0 = com.google.android.gms.internal.zzmq.zzbit
            com.google.android.gms.internal.zzmo r2 = com.google.android.gms.ads.internal.zzbs.zzep()
            java.lang.Object r0 = r2.zzd(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x004c
            boolean r0 = com.google.android.gms.common.util.zzq.zzalz()
            if (r0 == 0) goto L_0x004c
            boolean r0 = com.google.android.gms.common.util.zzq.zzamb()
            if (r0 != 0) goto L_0x004c
            if (r12 == 0) goto L_0x004c
            com.google.android.gms.internal.zzanp r0 = r12.zzso()
            boolean r0 = r0.zztx()
            if (r0 == 0) goto L_0x004c
            com.google.android.gms.internal.zzxk r7 = new com.google.android.gms.internal.zzxk
            r7.<init>(r8, r10, r12, r14)
            goto L_0x006e
        L_0x004c:
            com.google.android.gms.internal.zzxh r7 = new com.google.android.gms.internal.zzxh
            r7.<init>(r8, r10, r12, r14)
            goto L_0x006e
        L_0x0052:
            boolean r2 = r2.zzbdd
            if (r2 == 0) goto L_0x0069
            boolean r2 = r9 instanceof com.google.android.gms.ads.internal.zzba
            if (r2 == 0) goto L_0x0069
            com.google.android.gms.internal.zzxn r7 = new com.google.android.gms.internal.zzxn
            r2 = r9
            com.google.android.gms.ads.internal.zzba r2 = (com.google.android.gms.ads.internal.zzba) r2
            r0 = r7
            r1 = r8
            r3 = r10
            r4 = r11
            r5 = r14
            r6 = r15
            r0.<init>(r1, r2, r3, r4, r5, r6)
            goto L_0x006e
        L_0x0069:
            com.google.android.gms.internal.zzxi r7 = new com.google.android.gms.internal.zzxi
            r7.<init>(r10, r14)
        L_0x006e:
            java.lang.String r0 = "AdRenderer: "
            java.lang.Class r1 = r7.getClass()
            java.lang.String r1 = r1.getName()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r2 = r1.length()
            if (r2 == 0) goto L_0x0087
            java.lang.String r0 = r0.concat(r1)
            goto L_0x008d
        L_0x0087:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r0)
            r0 = r1
        L_0x008d:
            com.google.android.gms.internal.zzafj.zzbw(r0)
            r7.zzmx()
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzxf.zza(android.content.Context, com.google.android.gms.ads.internal.zza, com.google.android.gms.internal.zzaev, com.google.android.gms.internal.zzcs, com.google.android.gms.internal.zzama, com.google.android.gms.internal.zzuc, com.google.android.gms.internal.zzxg, com.google.android.gms.internal.zznd):com.google.android.gms.internal.zzahi");
    }
}
