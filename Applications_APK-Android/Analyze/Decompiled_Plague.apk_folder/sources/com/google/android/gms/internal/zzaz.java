package com.google.android.gms.internal;

import java.io.IOException;

public final class zzaz extends zzfhe<zzaz> {
    private Long zzfz = null;
    private Integer zzga = null;
    private Boolean zzgb = null;
    private int[] zzgc = zzfhn.zzphl;
    private Long zzgd = null;

    public zzaz() {
        this.zzpai = -1;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 8) {
                this.zzfz = Long.valueOf(zzfhb.zzcum());
            } else if (zzcts == 16) {
                this.zzga = Integer.valueOf(zzfhb.zzcuh());
            } else if (zzcts == 24) {
                this.zzgb = Boolean.valueOf(zzfhb.zzcty());
            } else if (zzcts == 32) {
                int zzb = zzfhn.zzb(zzfhb, 32);
                int length = this.zzgc == null ? 0 : this.zzgc.length;
                int[] iArr = new int[(zzb + length)];
                if (length != 0) {
                    System.arraycopy(this.zzgc, 0, iArr, 0, length);
                }
                while (length < iArr.length - 1) {
                    iArr[length] = zzfhb.zzcuh();
                    zzfhb.zzcts();
                    length++;
                }
                iArr[length] = zzfhb.zzcuh();
                this.zzgc = iArr;
            } else if (zzcts == 34) {
                int zzki = zzfhb.zzki(zzfhb.zzcuh());
                int position = zzfhb.getPosition();
                int i = 0;
                while (zzfhb.zzcuj() > 0) {
                    zzfhb.zzcuh();
                    i++;
                }
                zzfhb.zzlv(position);
                int length2 = this.zzgc == null ? 0 : this.zzgc.length;
                int[] iArr2 = new int[(i + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.zzgc, 0, iArr2, 0, length2);
                }
                while (length2 < iArr2.length) {
                    iArr2[length2] = zzfhb.zzcuh();
                    length2++;
                }
                this.zzgc = iArr2;
                zzfhb.zzkj(zzki);
            } else if (zzcts == 40) {
                this.zzgd = Long.valueOf(zzfhb.zzcum());
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzfz != null) {
            zzfhc.zzf(1, this.zzfz.longValue());
        }
        if (this.zzga != null) {
            zzfhc.zzaa(2, this.zzga.intValue());
        }
        if (this.zzgb != null) {
            zzfhc.zzl(3, this.zzgb.booleanValue());
        }
        if (this.zzgc != null && this.zzgc.length > 0) {
            for (int zzaa : this.zzgc) {
                zzfhc.zzaa(4, zzaa);
            }
        }
        if (this.zzgd != null) {
            zzfhc.zza(5, this.zzgd.longValue());
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzfz != null) {
            zzo += zzfhc.zzc(1, this.zzfz.longValue());
        }
        if (this.zzga != null) {
            zzo += zzfhc.zzad(2, this.zzga.intValue());
        }
        if (this.zzgb != null) {
            this.zzgb.booleanValue();
            zzo += zzfhc.zzkw(3) + 1;
        }
        if (this.zzgc != null && this.zzgc.length > 0) {
            int i = 0;
            for (int zzkx : this.zzgc) {
                i += zzfhc.zzkx(zzkx);
            }
            zzo = zzo + i + (1 * this.zzgc.length);
        }
        if (this.zzgd == null) {
            return zzo;
        }
        return zzo + zzfhc.zzkw(5) + zzfhc.zzdh(this.zzgd.longValue());
    }
}
