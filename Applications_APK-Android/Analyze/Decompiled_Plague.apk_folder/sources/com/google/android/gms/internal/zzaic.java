package com.google.android.gms.internal;

final class zzaic implements zzu {
    private /* synthetic */ String zzckw;
    private /* synthetic */ zzaif zzdau;

    zzaic(zzahy zzahy, String str, zzaif zzaif) {
        this.zzckw = str;
        this.zzdau = zzaif;
    }

    public final void zzd(zzaa zzaa) {
        String str = this.zzckw;
        String zzaa2 = zzaa.toString();
        StringBuilder sb = new StringBuilder(21 + String.valueOf(str).length() + String.valueOf(zzaa2).length());
        sb.append("Failed to load URL: ");
        sb.append(str);
        sb.append("\n");
        sb.append(zzaa2);
        zzafj.zzco(sb.toString());
        this.zzdau.zzb(null);
    }
}
