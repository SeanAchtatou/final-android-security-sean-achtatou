package com.google.android.gms.internal;

import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class zzbka {
    public final String title;
    public final DriveId zzghq;
    public final MetadataBundle zzgjt;
    public final Integer zzgju;
    public final int zzgjv;

    protected zzbka(MetadataBundle metadataBundle, Integer num, String str, DriveId driveId, int i) {
        this.zzgjt = metadataBundle;
        this.zzgju = num;
        this.title = str;
        this.zzghq = driveId;
        this.zzgjv = i;
    }
}
