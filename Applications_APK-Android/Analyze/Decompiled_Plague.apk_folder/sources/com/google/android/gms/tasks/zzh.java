package com.google.android.gms.tasks;

final class zzh implements Runnable {
    private /* synthetic */ Task zzkrh;
    private /* synthetic */ zzg zzkrn;

    zzh(zzg zzg, Task task) {
        this.zzkrn = zzg;
        this.zzkrh = task;
    }

    public final void run() {
        synchronized (this.zzkrn.mLock) {
            if (this.zzkrn.zzkrm != null) {
                this.zzkrn.zzkrm.onFailure(this.zzkrh.getException());
            }
        }
    }
}
