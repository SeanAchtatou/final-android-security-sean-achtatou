package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.drive.DriveSpace;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public final class zzt extends zzbej {
    public static final Parcelable.Creator<zzt> CREATOR = new zzu();
    private List<DriveSpace> zzgik;
    @NonNull
    private final Set<DriveSpace> zzgil;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    zzt(@NonNull List<DriveSpace> list) {
        this(list, list == null ? Collections.emptySet() : new HashSet(list));
    }

    private zzt(List<DriveSpace> list, @NonNull Set<DriveSpace> set) {
        this.zzgik = list;
        this.zzgil = set;
    }

    public final boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return zzbg.equal(this.zzgil, ((zzt) obj).zzgil);
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.zzgil});
    }

    public final String toString() {
        return String.format(Locale.US, "TransferStateOptions[Spaces=%s]", this.zzgik);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 2, this.zzgik, false);
        zzbem.zzai(parcel, zze);
    }
}
