package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.google.android.gms.ads.AdActivity;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;

@zzzb
public final class zzafd {
    private final Object mLock = new Object();
    private String mSessionId;
    int zzcxo = -1;
    private long zzcxz = -1;
    private long zzcya = -1;
    private int zzcyb = -1;
    private long zzcyc = 0;
    private int zzcyd = 0;
    private int zzcye = 0;

    public zzafd(String str) {
        this.mSessionId = str;
    }

    private static boolean zzae(Context context) {
        int identifier = context.getResources().getIdentifier("Theme.Translucent", TJAdUnitConstants.String.STYLE, TapjoyConstants.TJC_DEVICE_PLATFORM_TYPE);
        if (identifier != 0) {
            try {
                if (identifier == context.getPackageManager().getActivityInfo(new ComponentName(context.getPackageName(), AdActivity.CLASS_NAME), 0).theme) {
                    return true;
                }
                zzafj.zzcn("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
                return false;
            } catch (PackageManager.NameNotFoundException unused) {
                zzafj.zzco("Fail to fetch AdActivity theme");
            }
        }
        zzafj.zzcn("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0080, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzb(com.google.android.gms.internal.zzis r11, long r12) {
        /*
            r10 = this;
            java.lang.Object r0 = r10.mLock
            monitor-enter(r0)
            com.google.android.gms.internal.zzaez r1 = com.google.android.gms.ads.internal.zzbs.zzeg()     // Catch:{ all -> 0x0081 }
            long r1 = r1.zzpc()     // Catch:{ all -> 0x0081 }
            com.google.android.gms.common.util.zzd r3 = com.google.android.gms.ads.internal.zzbs.zzei()     // Catch:{ all -> 0x0081 }
            long r3 = r3.currentTimeMillis()     // Catch:{ all -> 0x0081 }
            long r5 = r10.zzcya     // Catch:{ all -> 0x0081 }
            r7 = -1
            int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r9 != 0) goto L_0x0043
            long r5 = r3 - r1
            com.google.android.gms.internal.zzmg<java.lang.Long> r1 = com.google.android.gms.internal.zzmq.zzbjs     // Catch:{ all -> 0x0081 }
            com.google.android.gms.internal.zzmo r2 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ all -> 0x0081 }
            java.lang.Object r1 = r2.zzd(r1)     // Catch:{ all -> 0x0081 }
            java.lang.Long r1 = (java.lang.Long) r1     // Catch:{ all -> 0x0081 }
            long r1 = r1.longValue()     // Catch:{ all -> 0x0081 }
            int r7 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r7 <= 0) goto L_0x0035
            r1 = -1
            r10.zzcxo = r1     // Catch:{ all -> 0x0081 }
            goto L_0x003f
        L_0x0035:
            com.google.android.gms.internal.zzaez r1 = com.google.android.gms.ads.internal.zzbs.zzeg()     // Catch:{ all -> 0x0081 }
            int r1 = r1.zzpf()     // Catch:{ all -> 0x0081 }
            r10.zzcxo = r1     // Catch:{ all -> 0x0081 }
        L_0x003f:
            r10.zzcya = r12     // Catch:{ all -> 0x0081 }
            long r12 = r10.zzcya     // Catch:{ all -> 0x0081 }
        L_0x0043:
            r10.zzcxz = r12     // Catch:{ all -> 0x0081 }
            r12 = 1
            if (r11 == 0) goto L_0x0059
            android.os.Bundle r13 = r11.extras     // Catch:{ all -> 0x0081 }
            if (r13 == 0) goto L_0x0059
            android.os.Bundle r11 = r11.extras     // Catch:{ all -> 0x0081 }
            java.lang.String r13 = "gw"
            r1 = 2
            int r11 = r11.getInt(r13, r1)     // Catch:{ all -> 0x0081 }
            if (r11 != r12) goto L_0x0059
            monitor-exit(r0)     // Catch:{ all -> 0x0081 }
            return
        L_0x0059:
            int r11 = r10.zzcyb     // Catch:{ all -> 0x0081 }
            int r11 = r11 + r12
            r10.zzcyb = r11     // Catch:{ all -> 0x0081 }
            int r11 = r10.zzcxo     // Catch:{ all -> 0x0081 }
            int r11 = r11 + r12
            r10.zzcxo = r11     // Catch:{ all -> 0x0081 }
            int r11 = r10.zzcxo     // Catch:{ all -> 0x0081 }
            if (r11 != 0) goto L_0x0073
            r11 = 0
            r10.zzcyc = r11     // Catch:{ all -> 0x0081 }
            com.google.android.gms.internal.zzaez r11 = com.google.android.gms.ads.internal.zzbs.zzeg()     // Catch:{ all -> 0x0081 }
            r11.zzj(r3)     // Catch:{ all -> 0x0081 }
            goto L_0x007f
        L_0x0073:
            com.google.android.gms.internal.zzaez r11 = com.google.android.gms.ads.internal.zzbs.zzeg()     // Catch:{ all -> 0x0081 }
            long r11 = r11.zzpd()     // Catch:{ all -> 0x0081 }
            long r1 = r3 - r11
            r10.zzcyc = r1     // Catch:{ all -> 0x0081 }
        L_0x007f:
            monitor-exit(r0)     // Catch:{ all -> 0x0081 }
            return
        L_0x0081:
            r11 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0081 }
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzafd.zzb(com.google.android.gms.internal.zzis, long):void");
    }

    public final Bundle zzl(Context context, String str) {
        Bundle bundle;
        synchronized (this.mLock) {
            bundle = new Bundle();
            bundle.putString(TapjoyConstants.TJC_SESSION_ID, this.mSessionId);
            bundle.putLong("basets", this.zzcya);
            bundle.putLong("currts", this.zzcxz);
            bundle.putString("seq_num", str);
            bundle.putInt("preqs", this.zzcyb);
            bundle.putInt("preqs_in_session", this.zzcxo);
            bundle.putLong("time_in_session", this.zzcyc);
            bundle.putInt("pclick", this.zzcyd);
            bundle.putInt("pimp", this.zzcye);
            bundle.putBoolean("support_transparent_background", zzae(context));
        }
        return bundle;
    }

    public final void zzoi() {
        synchronized (this.mLock) {
            this.zzcye++;
        }
    }

    public final void zzoj() {
        synchronized (this.mLock) {
            this.zzcyd++;
        }
    }
}
