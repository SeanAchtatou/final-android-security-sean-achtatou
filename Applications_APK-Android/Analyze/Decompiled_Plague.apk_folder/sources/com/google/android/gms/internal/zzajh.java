package com.google.android.gms.internal;

import java.util.concurrent.ExecutionException;

final /* synthetic */ class zzajh implements Runnable {
    private final zzajd zzdce;
    private final zzajp zzdcf;

    zzajh(zzajd zzajd, zzajp zzajp) {
        this.zzdce = zzajd;
        this.zzdcf = zzajp;
    }

    public final void run() {
        Throwable e;
        zzajd zzajd = this.zzdce;
        try {
            zzajd.onSuccess(this.zzdcf.get());
        } catch (ExecutionException e2) {
            e = e2.getCause();
            zzajd.zzb(e);
        } catch (InterruptedException e3) {
            e = e3;
            Thread.currentThread().interrupt();
            zzajd.zzb(e);
        } catch (Exception e4) {
            e = e4;
            zzajd.zzb(e);
        }
    }
}
