package com.google.android.gms.internal;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.OnChangeListener;
import com.google.android.gms.drive.events.zzj;

final class zzbnz {
    private OnChangeListener zzgmu;
    private DriveId zzgmv;
    /* access modifiers changed from: private */
    public zzbov zzgmw;

    zzbnz(zzbmu zzbmu, OnChangeListener onChangeListener, DriveId driveId) {
        zzbq.checkState(zzj.zza(1, driveId));
        this.zzgmu = onChangeListener;
        this.zzgmv = driveId;
        Looper looper = zzbmu.getLooper();
        Context applicationContext = zzbmu.getApplicationContext();
        onChangeListener.getClass();
        this.zzgmw = new zzbov(looper, applicationContext, 1, zzboa.zza(onChangeListener));
        this.zzgmw.zzcv(1);
    }
}
