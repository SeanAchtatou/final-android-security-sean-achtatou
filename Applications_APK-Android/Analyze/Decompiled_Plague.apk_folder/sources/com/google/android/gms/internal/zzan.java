package com.google.android.gms.internal;

import com.mopub.common.Constants;
import com.mopub.volley.toolbox.HttpClientStack;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.SSLSocketFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;

public final class zzan implements zzam {
    private final zzao zzcb;
    private final SSLSocketFactory zzcc;

    public zzan() {
        this(null);
    }

    private zzan(zzao zzao) {
        this(null, null);
    }

    private zzan(zzao zzao, SSLSocketFactory sSLSocketFactory) {
        this.zzcb = null;
        this.zzcc = null;
    }

    private static HttpEntity zza(HttpURLConnection httpURLConnection) {
        InputStream inputStream;
        BasicHttpEntity basicHttpEntity = new BasicHttpEntity();
        try {
            inputStream = httpURLConnection.getInputStream();
        } catch (IOException unused) {
            inputStream = httpURLConnection.getErrorStream();
        }
        basicHttpEntity.setContent(inputStream);
        basicHttpEntity.setContentLength((long) httpURLConnection.getContentLength());
        basicHttpEntity.setContentEncoding(httpURLConnection.getContentEncoding());
        basicHttpEntity.setContentType(httpURLConnection.getContentType());
        return basicHttpEntity;
    }

    private static void zza(HttpURLConnection httpURLConnection, zzp<?> zzp) throws IOException, zza {
        byte[] zzg = zzp.zzg();
        if (zzg != null) {
            httpURLConnection.setDoOutput(true);
            httpURLConnection.addRequestProperty("Content-Type", zzp.zzf());
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.write(zzg);
            dataOutputStream.close();
        }
    }

    public final HttpResponse zza(zzp<?> zzp, Map<String, String> map) throws IOException, zza {
        String str;
        String str2;
        String str3;
        String url = zzp.getUrl();
        HashMap hashMap = new HashMap();
        hashMap.putAll(zzp.getHeaders());
        hashMap.putAll(map);
        if (this.zzcb != null) {
            str = this.zzcb.zzg(url);
            if (str == null) {
                String valueOf = String.valueOf(url);
                throw new IOException(valueOf.length() != 0 ? "URL blocked by rewriter: ".concat(valueOf) : new String("URL blocked by rewriter: "));
            }
        } else {
            str = url;
        }
        URL url2 = new URL(str);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url2.openConnection();
        httpURLConnection.setInstanceFollowRedirects(HttpURLConnection.getFollowRedirects());
        int zzi = zzp.zzi();
        httpURLConnection.setConnectTimeout(zzi);
        httpURLConnection.setReadTimeout(zzi);
        httpURLConnection.setUseCaches(false);
        boolean z = true;
        httpURLConnection.setDoInput(true);
        Constants.HTTPS.equals(url2.getProtocol());
        for (String str4 : hashMap.keySet()) {
            httpURLConnection.addRequestProperty(str4, (String) hashMap.get(str4));
        }
        switch (zzp.getMethod()) {
            case -1:
                break;
            case 0:
                str2 = "GET";
                httpURLConnection.setRequestMethod(str2);
                break;
            case 1:
                str3 = "POST";
                httpURLConnection.setRequestMethod(str3);
                zza(httpURLConnection, zzp);
                break;
            case 2:
                str3 = "PUT";
                httpURLConnection.setRequestMethod(str3);
                zza(httpURLConnection, zzp);
                break;
            case 3:
                str2 = "DELETE";
                httpURLConnection.setRequestMethod(str2);
                break;
            case 4:
                str2 = "HEAD";
                httpURLConnection.setRequestMethod(str2);
                break;
            case 5:
                str2 = "OPTIONS";
                httpURLConnection.setRequestMethod(str2);
                break;
            case 6:
                str2 = "TRACE";
                httpURLConnection.setRequestMethod(str2);
                break;
            case 7:
                str3 = HttpClientStack.HttpPatch.METHOD_NAME;
                httpURLConnection.setRequestMethod(str3);
                zza(httpURLConnection, zzp);
                break;
            default:
                throw new IllegalStateException("Unknown method type.");
        }
        ProtocolVersion protocolVersion = new ProtocolVersion("HTTP", 1, 1);
        if (httpURLConnection.getResponseCode() == -1) {
            throw new IOException("Could not retrieve response code from HttpUrlConnection.");
        }
        BasicStatusLine basicStatusLine = new BasicStatusLine(protocolVersion, httpURLConnection.getResponseCode(), httpURLConnection.getResponseMessage());
        BasicHttpResponse basicHttpResponse = new BasicHttpResponse(basicStatusLine);
        int method = zzp.getMethod();
        int statusCode = basicStatusLine.getStatusCode();
        if (method == 4 || ((100 <= statusCode && statusCode < 200) || statusCode == 204 || statusCode == 304)) {
            z = false;
        }
        if (z) {
            basicHttpResponse.setEntity(zza(httpURLConnection));
        }
        for (Map.Entry next : httpURLConnection.getHeaderFields().entrySet()) {
            if (next.getKey() != null) {
                basicHttpResponse.addHeader(new BasicHeader((String) next.getKey(), (String) ((List) next.getValue()).get(0)));
            }
        }
        return basicHttpResponse;
    }
}
