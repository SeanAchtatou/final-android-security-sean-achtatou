package com.google.android.gms.internal;

public final class zzfbq {
    private final byte[] zzown = new byte[256];
    private int zzowo;
    private int zzowp;

    public zzfbq(byte[] bArr) {
        for (int i = 0; i < 256; i++) {
            this.zzown[i] = (byte) i;
        }
        byte b = 0;
        for (int i2 = 0; i2 < 256; i2++) {
            b = (b + this.zzown[i2] + bArr[i2 % bArr.length]) & 255;
            byte b2 = this.zzown[i2];
            this.zzown[i2] = this.zzown[b];
            this.zzown[b] = b2;
        }
        this.zzowo = 0;
        this.zzowp = 0;
    }

    public final void zzax(byte[] bArr) {
        int i = this.zzowo;
        int i2 = this.zzowp;
        for (int i3 = 0; i3 < bArr.length; i3++) {
            i = (i + 1) & 255;
            i2 = (i2 + this.zzown[i]) & 255;
            byte b = this.zzown[i];
            this.zzown[i] = this.zzown[i2];
            this.zzown[i2] = b;
            bArr[i3] = (byte) (bArr[i3] ^ this.zzown[(this.zzown[i] + this.zzown[i2]) & 255]);
        }
        this.zzowo = i;
        this.zzowp = i2;
    }
}
