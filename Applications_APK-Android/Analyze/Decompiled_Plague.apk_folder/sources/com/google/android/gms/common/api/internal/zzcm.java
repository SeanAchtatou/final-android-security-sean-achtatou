package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.google.android.gms.common.internal.zzbq;

final class zzcm extends Handler {
    private /* synthetic */ zzcl zzfrs;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzcm(zzcl zzcl, Looper looper) {
        super(looper);
        this.zzfrs = zzcl;
    }

    public final void handleMessage(Message message) {
        boolean z = true;
        if (message.what != 1) {
            z = false;
        }
        zzbq.checkArgument(z);
        this.zzfrs.zzb((zzco) message.obj);
    }
}
