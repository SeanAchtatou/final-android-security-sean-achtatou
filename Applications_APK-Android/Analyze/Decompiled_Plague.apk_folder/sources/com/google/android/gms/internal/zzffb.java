package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.Map;

final class zzffb<K> implements Iterator<Map.Entry<K, Object>> {
    private Iterator<Map.Entry<K, Object>> zzmhm;

    public zzffb(Iterator<Map.Entry<K, Object>> it) {
        this.zzmhm = it;
    }

    public final boolean hasNext() {
        return this.zzmhm.hasNext();
    }

    public final /* synthetic */ Object next() {
        Map.Entry next = this.zzmhm.next();
        return next.getValue() instanceof zzfey ? new zzffa(next) : next;
    }

    public final void remove() {
        this.zzmhm.remove();
    }
}
