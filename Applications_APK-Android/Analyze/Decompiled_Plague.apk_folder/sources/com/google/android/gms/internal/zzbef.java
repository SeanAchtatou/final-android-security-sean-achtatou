package com.google.android.gms.internal;

import android.graphics.drawable.Drawable;

final class zzbef extends Drawable.ConstantState {
    private zzbef() {
    }

    public final int getChangingConfigurations() {
        return 0;
    }

    public final Drawable newDrawable() {
        return zzbee.zzfvg;
    }
}
