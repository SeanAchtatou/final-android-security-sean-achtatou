package com.google.android.gms.ads.internal.gmsg;

import android.text.TextUtils;
import com.google.android.gms.internal.zzadw;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzzb;
import com.tapjoy.TapjoyConstants;
import com.tapjoy.mraid.view.MraidView;
import java.util.Map;

@zzzb
public final class zzaf implements zzt<Object> {
    private final zzag zzbwt;

    public zzaf(zzag zzag) {
        this.zzbwt = zzag;
    }

    public final void zza(Object obj, Map<String, String> map) {
        String str = map.get(MraidView.ACTION_KEY);
        if ("grant".equals(str)) {
            zzadw zzadw = null;
            try {
                int parseInt = Integer.parseInt(map.get(TapjoyConstants.TJC_AMOUNT));
                String str2 = map.get("type");
                if (!TextUtils.isEmpty(str2)) {
                    zzadw = new zzadw(str2, parseInt);
                }
            } catch (NumberFormatException e) {
                zzafj.zzc("Unable to parse reward amount.", e);
            }
            this.zzbwt.zzb(zzadw);
        } else if ("video_start".equals(str)) {
            this.zzbwt.zzdf();
        }
    }
}
