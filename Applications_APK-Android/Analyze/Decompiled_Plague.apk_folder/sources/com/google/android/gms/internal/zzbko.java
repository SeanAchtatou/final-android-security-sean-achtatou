package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbko implements Parcelable.Creator<zzbkn> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        while (parcel.dataPosition() < zzd) {
            zzbek.zzb(parcel, parcel.readInt());
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbkn();
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbkn[i];
    }
}
