package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.MetadataChangeSet;

final class zzbok extends zzbor {
    private /* synthetic */ MetadataChangeSet zzglq;
    private /* synthetic */ zzbog zzgng;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbok(zzbog zzbog, GoogleApiClient googleApiClient, MetadataChangeSet metadataChangeSet) {
        super(zzbog, googleApiClient, null);
        this.zzgng = zzbog;
        this.zzglq = metadataChangeSet;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        zzbll zzbll = (zzbll) zzb;
        this.zzglq.zzanz().setContext(zzbll.getContext());
        ((zzbpf) zzbll.zzakb()).zza(new zzbrq(this.zzgng.zzgfy, this.zzglq.zzanz()), new zzbop(this));
    }
}
