package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.zzv;
import java.util.ArrayList;

public final class zzbpe implements Parcelable.Creator<zzbpd> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        ArrayList arrayList = null;
        int i = 0;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    arrayList = zzbek.zzc(parcel, readInt, zzv.CREATOR);
                    break;
                case 3:
                    i = zzbek.zzg(parcel, readInt);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbpd(arrayList, i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbpd[i];
    }
}
