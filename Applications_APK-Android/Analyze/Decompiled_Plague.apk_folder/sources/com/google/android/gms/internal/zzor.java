package com.google.android.gms.internal;

import android.graphics.Point;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.internal.zzbs;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

@zzzb
public final class zzor extends zzpi implements View.OnClickListener, View.OnTouchListener, ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener {
    static final String[] zzbto = {NativeAppInstallAd.ASSET_MEDIA_VIDEO, NativeContentAd.ASSET_MEDIA_VIDEO};
    private final Object mLock = new Object();
    @Nullable
    private zzoa zzbsk;
    @Nullable
    private View zzbtt;
    private Point zzbtv = new Point();
    private Point zzbtw = new Point();
    @Nullable
    private WeakReference<zzfy> zzbtx = new WeakReference<>(null);
    private final WeakReference<View> zzbub;
    private final Map<String, WeakReference<View>> zzbuc = new HashMap();
    private final Map<String, WeakReference<View>> zzbud = new HashMap();
    private final Map<String, WeakReference<View>> zzbue = new HashMap();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzakg.zza(android.view.View, android.view.ViewTreeObserver$OnGlobalLayoutListener):void
     arg types: [android.view.View, com.google.android.gms.internal.zzor]
     candidates:
      com.google.android.gms.internal.zzakg.zza(android.view.View, android.view.ViewTreeObserver$OnScrollChangedListener):void
      com.google.android.gms.internal.zzakg.zza(android.view.View, android.view.ViewTreeObserver$OnGlobalLayoutListener):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzakg.zza(android.view.View, android.view.ViewTreeObserver$OnScrollChangedListener):void
     arg types: [android.view.View, com.google.android.gms.internal.zzor]
     candidates:
      com.google.android.gms.internal.zzakg.zza(android.view.View, android.view.ViewTreeObserver$OnGlobalLayoutListener):void
      com.google.android.gms.internal.zzakg.zza(android.view.View, android.view.ViewTreeObserver$OnScrollChangedListener):void */
    public zzor(View view, HashMap<String, View> hashMap, HashMap<String, View> hashMap2) {
        zzbs.zzez();
        zzakg.zza(view, (ViewTreeObserver.OnGlobalLayoutListener) this);
        zzbs.zzez();
        zzakg.zza(view, (ViewTreeObserver.OnScrollChangedListener) this);
        view.setOnTouchListener(this);
        view.setOnClickListener(this);
        this.zzbub = new WeakReference<>(view);
        zzh(hashMap);
        this.zzbue.putAll(this.zzbuc);
        zzi(hashMap2);
        this.zzbue.putAll(this.zzbud);
        zzmq.initialize(view.getContext());
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003a, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(com.google.android.gms.internal.zzoe r7) {
        /*
            r6 = this;
            java.lang.Object r0 = r6.mLock
            monitor-enter(r0)
            java.lang.String[] r1 = com.google.android.gms.internal.zzor.zzbto     // Catch:{ all -> 0x003b }
            int r2 = r1.length     // Catch:{ all -> 0x003b }
            r3 = 0
        L_0x0007:
            if (r3 >= r2) goto L_0x001f
            r4 = r1[r3]     // Catch:{ all -> 0x003b }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r5 = r6.zzbue     // Catch:{ all -> 0x003b }
            java.lang.Object r4 = r5.get(r4)     // Catch:{ all -> 0x003b }
            java.lang.ref.WeakReference r4 = (java.lang.ref.WeakReference) r4     // Catch:{ all -> 0x003b }
            if (r4 == 0) goto L_0x001c
            java.lang.Object r1 = r4.get()     // Catch:{ all -> 0x003b }
            android.view.View r1 = (android.view.View) r1     // Catch:{ all -> 0x003b }
            goto L_0x0020
        L_0x001c:
            int r3 = r3 + 1
            goto L_0x0007
        L_0x001f:
            r1 = 0
        L_0x0020:
            boolean r2 = r1 instanceof android.widget.FrameLayout     // Catch:{ all -> 0x003b }
            if (r2 != 0) goto L_0x0029
            r7.zzkc()     // Catch:{ all -> 0x003b }
            monitor-exit(r0)     // Catch:{ all -> 0x003b }
            return
        L_0x0029:
            com.google.android.gms.internal.zzot r2 = new com.google.android.gms.internal.zzot     // Catch:{ all -> 0x003b }
            r2.<init>(r6, r1)     // Catch:{ all -> 0x003b }
            boolean r3 = r7 instanceof com.google.android.gms.internal.zznz     // Catch:{ all -> 0x003b }
            if (r3 == 0) goto L_0x0036
            r7.zzb(r1, r2)     // Catch:{ all -> 0x003b }
            goto L_0x0039
        L_0x0036:
            r7.zza(r1, r2)     // Catch:{ all -> 0x003b }
        L_0x0039:
            monitor-exit(r0)     // Catch:{ all -> 0x003b }
            return
        L_0x003b:
            r7 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003b }
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzor.zza(com.google.android.gms.internal.zzoe):void");
    }

    /* access modifiers changed from: private */
    public final boolean zza(String[] strArr) {
        for (String str : strArr) {
            if (this.zzbuc.get(str) != null) {
                return true;
            }
        }
        for (String str2 : strArr) {
            if (this.zzbud.get(str2) != null) {
                return false;
            }
        }
        return false;
    }

    private final void zzg(@Nullable View view) {
        synchronized (this.mLock) {
            if (this.zzbsk != null) {
                zzoa zzjz = this.zzbsk instanceof zznz ? ((zznz) this.zzbsk).zzjz() : this.zzbsk;
                if (zzjz != null) {
                    zzjz.zzg(view);
                }
            }
        }
    }

    private final void zzh(Map<String, View> map) {
        for (Map.Entry next : map.entrySet()) {
            String str = (String) next.getKey();
            View view = (View) next.getValue();
            if (view != null) {
                this.zzbuc.put(str, new WeakReference(view));
                if (!NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW.equals(str)) {
                    view.setOnTouchListener(this);
                    view.setClickable(true);
                    view.setOnClickListener(this);
                }
            }
        }
    }

    private final void zzi(Map<String, View> map) {
        for (Map.Entry next : map.entrySet()) {
            View view = (View) next.getValue();
            if (view != null) {
                this.zzbud.put((String) next.getKey(), new WeakReference(view));
                view.setOnTouchListener(this);
            }
        }
    }

    private final int zzt(int i) {
        int zzd;
        synchronized (this.mLock) {
            zzjk.zzhx();
            zzd = zzais.zzd(this.zzbsk.getContext(), i);
        }
        return zzd;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x008f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r9) {
        /*
            r8 = this;
            java.lang.Object r0 = r8.mLock
            monitor-enter(r0)
            com.google.android.gms.internal.zzoa r1 = r8.zzbsk     // Catch:{ all -> 0x0090 }
            if (r1 != 0) goto L_0x0009
            monitor-exit(r0)     // Catch:{ all -> 0x0090 }
            return
        L_0x0009:
            java.lang.ref.WeakReference<android.view.View> r1 = r8.zzbub     // Catch:{ all -> 0x0090 }
            java.lang.Object r1 = r1.get()     // Catch:{ all -> 0x0090 }
            r7 = r1
            android.view.View r7 = (android.view.View) r7     // Catch:{ all -> 0x0090 }
            if (r7 != 0) goto L_0x0016
            monitor-exit(r0)     // Catch:{ all -> 0x0090 }
            return
        L_0x0016:
            android.os.Bundle r5 = new android.os.Bundle     // Catch:{ all -> 0x0090 }
            r5.<init>()     // Catch:{ all -> 0x0090 }
            java.lang.String r1 = "x"
            android.graphics.Point r2 = r8.zzbtv     // Catch:{ all -> 0x0090 }
            int r2 = r2.x     // Catch:{ all -> 0x0090 }
            int r2 = r8.zzt(r2)     // Catch:{ all -> 0x0090 }
            float r2 = (float) r2     // Catch:{ all -> 0x0090 }
            r5.putFloat(r1, r2)     // Catch:{ all -> 0x0090 }
            java.lang.String r1 = "y"
            android.graphics.Point r2 = r8.zzbtv     // Catch:{ all -> 0x0090 }
            int r2 = r2.y     // Catch:{ all -> 0x0090 }
            int r2 = r8.zzt(r2)     // Catch:{ all -> 0x0090 }
            float r2 = (float) r2     // Catch:{ all -> 0x0090 }
            r5.putFloat(r1, r2)     // Catch:{ all -> 0x0090 }
            java.lang.String r1 = "start_x"
            android.graphics.Point r2 = r8.zzbtw     // Catch:{ all -> 0x0090 }
            int r2 = r2.x     // Catch:{ all -> 0x0090 }
            int r2 = r8.zzt(r2)     // Catch:{ all -> 0x0090 }
            float r2 = (float) r2     // Catch:{ all -> 0x0090 }
            r5.putFloat(r1, r2)     // Catch:{ all -> 0x0090 }
            java.lang.String r1 = "start_y"
            android.graphics.Point r2 = r8.zzbtw     // Catch:{ all -> 0x0090 }
            int r2 = r2.y     // Catch:{ all -> 0x0090 }
            int r2 = r8.zzt(r2)     // Catch:{ all -> 0x0090 }
            float r2 = (float) r2     // Catch:{ all -> 0x0090 }
            r5.putFloat(r1, r2)     // Catch:{ all -> 0x0090 }
            android.view.View r1 = r8.zzbtt     // Catch:{ all -> 0x0090 }
            if (r1 == 0) goto L_0x0087
            android.view.View r1 = r8.zzbtt     // Catch:{ all -> 0x0090 }
            boolean r1 = r1.equals(r9)     // Catch:{ all -> 0x0090 }
            if (r1 == 0) goto L_0x0087
            com.google.android.gms.internal.zzoa r1 = r8.zzbsk     // Catch:{ all -> 0x0090 }
            boolean r1 = r1 instanceof com.google.android.gms.internal.zznz     // Catch:{ all -> 0x0090 }
            if (r1 == 0) goto L_0x0080
            com.google.android.gms.internal.zzoa r1 = r8.zzbsk     // Catch:{ all -> 0x0090 }
            com.google.android.gms.internal.zznz r1 = (com.google.android.gms.internal.zznz) r1     // Catch:{ all -> 0x0090 }
            com.google.android.gms.internal.zzoa r1 = r1.zzjz()     // Catch:{ all -> 0x0090 }
            if (r1 == 0) goto L_0x008e
            com.google.android.gms.internal.zzoa r1 = r8.zzbsk     // Catch:{ all -> 0x0090 }
            com.google.android.gms.internal.zznz r1 = (com.google.android.gms.internal.zznz) r1     // Catch:{ all -> 0x0090 }
            com.google.android.gms.internal.zzoa r2 = r1.zzjz()     // Catch:{ all -> 0x0090 }
            java.lang.String r4 = "1007"
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r6 = r8.zzbue     // Catch:{ all -> 0x0090 }
        L_0x007b:
            r3 = r9
            r2.zza(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0090 }
            goto L_0x008e
        L_0x0080:
            com.google.android.gms.internal.zzoa r2 = r8.zzbsk     // Catch:{ all -> 0x0090 }
            java.lang.String r4 = "1007"
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r6 = r8.zzbue     // Catch:{ all -> 0x0090 }
            goto L_0x007b
        L_0x0087:
            com.google.android.gms.internal.zzoa r1 = r8.zzbsk     // Catch:{ all -> 0x0090 }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r2 = r8.zzbue     // Catch:{ all -> 0x0090 }
            r1.zza(r9, r2, r5, r7)     // Catch:{ all -> 0x0090 }
        L_0x008e:
            monitor-exit(r0)     // Catch:{ all -> 0x0090 }
            return
        L_0x0090:
            r9 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0090 }
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzor.onClick(android.view.View):void");
    }

    public final void onGlobalLayout() {
        View view;
        synchronized (this.mLock) {
            if (!(this.zzbsk == null || (view = this.zzbub.get()) == null)) {
                this.zzbsk.zzc(view, this.zzbue);
            }
        }
    }

    public final void onScrollChanged() {
        View view;
        synchronized (this.mLock) {
            if (!(this.zzbsk == null || (view = this.zzbub.get()) == null)) {
                this.zzbsk.zzc(view, this.zzbue);
            }
        }
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        synchronized (this.mLock) {
            if (this.zzbsk == null) {
                return false;
            }
            View view2 = this.zzbub.get();
            if (view2 == null) {
                return false;
            }
            int[] iArr = new int[2];
            view2.getLocationOnScreen(iArr);
            Point point = new Point((int) (motionEvent.getRawX() - ((float) iArr[0])), (int) (motionEvent.getRawY() - ((float) iArr[1])));
            this.zzbtv = point;
            if (motionEvent.getAction() == 0) {
                this.zzbtw = point;
            }
            MotionEvent obtain = MotionEvent.obtain(motionEvent);
            obtain.setLocation((float) point.x, (float) point.y);
            this.zzbsk.zzd(obtain);
            obtain.recycle();
            return false;
        }
    }

    public final void unregisterNativeAd() {
        synchronized (this.mLock) {
            this.zzbtt = null;
            this.zzbsk = null;
            this.zzbtv = null;
            this.zzbtw = null;
        }
    }

    /* JADX WARN: Type inference failed for: r2v18, types: [android.view.View] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzoe.zza(android.view.View$OnClickListener, boolean):android.view.View
     arg types: [com.google.android.gms.internal.zzor, int]
     candidates:
      com.google.android.gms.internal.zzoe.zza(java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>>, android.view.View):org.json.JSONObject
      com.google.android.gms.internal.zzoe.zza(android.view.View, com.google.android.gms.internal.zzny):void
      com.google.android.gms.internal.zzoe.zza(android.view.View, java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>>):void
      com.google.android.gms.internal.zzoa.zza(android.view.View, com.google.android.gms.internal.zzny):void
      com.google.android.gms.internal.zzoa.zza(android.view.View, java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>>):void
      com.google.android.gms.internal.zzoe.zza(android.view.View$OnClickListener, boolean):android.view.View */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(com.google.android.gms.dynamic.IObjectWrapper r9) {
        /*
            r8 = this;
            java.lang.Object r0 = r8.mLock
            monitor-enter(r0)
            r1 = 0
            r8.zzg(r1)     // Catch:{ all -> 0x017b }
            java.lang.Object r9 = com.google.android.gms.dynamic.zzn.zzx(r9)     // Catch:{ all -> 0x017b }
            boolean r2 = r9 instanceof com.google.android.gms.internal.zzoe     // Catch:{ all -> 0x017b }
            if (r2 != 0) goto L_0x0016
            java.lang.String r9 = "Not an instance of native engine. This is most likely a transient error"
            com.google.android.gms.internal.zzafj.zzco(r9)     // Catch:{ all -> 0x017b }
            monitor-exit(r0)     // Catch:{ all -> 0x017b }
            return
        L_0x0016:
            com.google.android.gms.internal.zzoe r9 = (com.google.android.gms.internal.zzoe) r9     // Catch:{ all -> 0x017b }
            boolean r2 = r9.zzjx()     // Catch:{ all -> 0x017b }
            if (r2 != 0) goto L_0x0025
            java.lang.String r9 = "Your account must be enabled to use this feature. Talk to your account manager to request this feature for your account."
            com.google.android.gms.internal.zzafj.e(r9)     // Catch:{ all -> 0x017b }
            monitor-exit(r0)     // Catch:{ all -> 0x017b }
            return
        L_0x0025:
            java.lang.ref.WeakReference<android.view.View> r2 = r8.zzbub     // Catch:{ all -> 0x017b }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x017b }
            r7 = r2
            android.view.View r7 = (android.view.View) r7     // Catch:{ all -> 0x017b }
            com.google.android.gms.internal.zzoa r2 = r8.zzbsk     // Catch:{ all -> 0x017b }
            if (r2 == 0) goto L_0x004d
            if (r7 == 0) goto L_0x004d
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r2 = com.google.android.gms.internal.zzmq.zzbmx     // Catch:{ all -> 0x017b }
            com.google.android.gms.internal.zzmo r3 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ all -> 0x017b }
            java.lang.Object r2 = r3.zzd(r2)     // Catch:{ all -> 0x017b }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ all -> 0x017b }
            boolean r2 = r2.booleanValue()     // Catch:{ all -> 0x017b }
            if (r2 == 0) goto L_0x004d
            com.google.android.gms.internal.zzoa r2 = r8.zzbsk     // Catch:{ all -> 0x017b }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r3 = r8.zzbue     // Catch:{ all -> 0x017b }
            r2.zzb(r7, r3)     // Catch:{ all -> 0x017b }
        L_0x004d:
            java.lang.Object r2 = r8.mLock     // Catch:{ all -> 0x017b }
            monitor-enter(r2)     // Catch:{ all -> 0x017b }
            com.google.android.gms.internal.zzoa r3 = r8.zzbsk     // Catch:{ all -> 0x0178 }
            boolean r3 = r3 instanceof com.google.android.gms.internal.zzoe     // Catch:{ all -> 0x0178 }
            if (r3 != 0) goto L_0x0058
        L_0x0056:
            monitor-exit(r2)     // Catch:{ all -> 0x0178 }
            goto L_0x0096
        L_0x0058:
            com.google.android.gms.internal.zzoa r3 = r8.zzbsk     // Catch:{ all -> 0x0178 }
            com.google.android.gms.internal.zzoe r3 = (com.google.android.gms.internal.zzoe) r3     // Catch:{ all -> 0x0178 }
            java.lang.ref.WeakReference<android.view.View> r4 = r8.zzbub     // Catch:{ all -> 0x0178 }
            java.lang.Object r4 = r4.get()     // Catch:{ all -> 0x0178 }
            android.view.View r4 = (android.view.View) r4     // Catch:{ all -> 0x0178 }
            if (r3 == 0) goto L_0x0056
            android.content.Context r5 = r3.getContext()     // Catch:{ all -> 0x0178 }
            if (r5 == 0) goto L_0x0056
            if (r4 == 0) goto L_0x0056
            com.google.android.gms.internal.zzael r5 = com.google.android.gms.ads.internal.zzbs.zzfa()     // Catch:{ all -> 0x0178 }
            android.content.Context r4 = r4.getContext()     // Catch:{ all -> 0x0178 }
            boolean r4 = r5.zzt(r4)     // Catch:{ all -> 0x0178 }
            if (r4 == 0) goto L_0x0056
            com.google.android.gms.internal.zzaek r3 = r3.zzke()     // Catch:{ all -> 0x0178 }
            if (r3 == 0) goto L_0x0086
            r4 = 0
            r3.zzu(r4)     // Catch:{ all -> 0x0178 }
        L_0x0086:
            java.lang.ref.WeakReference<com.google.android.gms.internal.zzfy> r4 = r8.zzbtx     // Catch:{ all -> 0x0178 }
            java.lang.Object r4 = r4.get()     // Catch:{ all -> 0x0178 }
            com.google.android.gms.internal.zzfy r4 = (com.google.android.gms.internal.zzfy) r4     // Catch:{ all -> 0x0178 }
            if (r4 == 0) goto L_0x0056
            if (r3 == 0) goto L_0x0056
            r4.zzb(r3)     // Catch:{ all -> 0x0178 }
            goto L_0x0056
        L_0x0096:
            com.google.android.gms.internal.zzoa r2 = r8.zzbsk     // Catch:{ all -> 0x017b }
            boolean r2 = r2 instanceof com.google.android.gms.internal.zznz     // Catch:{ all -> 0x017b }
            if (r2 == 0) goto L_0x00ae
            com.google.android.gms.internal.zzoa r2 = r8.zzbsk     // Catch:{ all -> 0x017b }
            com.google.android.gms.internal.zznz r2 = (com.google.android.gms.internal.zznz) r2     // Catch:{ all -> 0x017b }
            boolean r2 = r2.zzjy()     // Catch:{ all -> 0x017b }
            if (r2 == 0) goto L_0x00ae
            com.google.android.gms.internal.zzoa r2 = r8.zzbsk     // Catch:{ all -> 0x017b }
            com.google.android.gms.internal.zznz r2 = (com.google.android.gms.internal.zznz) r2     // Catch:{ all -> 0x017b }
            r2.zzc(r9)     // Catch:{ all -> 0x017b }
            goto L_0x00ba
        L_0x00ae:
            r8.zzbsk = r9     // Catch:{ all -> 0x017b }
            boolean r2 = r9 instanceof com.google.android.gms.internal.zznz     // Catch:{ all -> 0x017b }
            if (r2 == 0) goto L_0x00ba
            r2 = r9
            com.google.android.gms.internal.zznz r2 = (com.google.android.gms.internal.zznz) r2     // Catch:{ all -> 0x017b }
            r2.zzc(r1)     // Catch:{ all -> 0x017b }
        L_0x00ba:
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r2 = r8.zzbue     // Catch:{ all -> 0x017b }
            java.lang.String r3 = "1098"
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x017b }
            java.lang.ref.WeakReference r2 = (java.lang.ref.WeakReference) r2     // Catch:{ all -> 0x017b }
            if (r2 != 0) goto L_0x00cc
            java.lang.String r1 = "Ad choices asset view is not provided."
            com.google.android.gms.internal.zzafj.zzco(r1)     // Catch:{ all -> 0x017b }
            goto L_0x010a
        L_0x00cc:
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x017b }
            android.view.View r2 = (android.view.View) r2     // Catch:{ all -> 0x017b }
            boolean r3 = r2 instanceof android.view.ViewGroup     // Catch:{ all -> 0x017b }
            if (r3 == 0) goto L_0x00d9
            r1 = r2
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1     // Catch:{ all -> 0x017b }
        L_0x00d9:
            if (r1 == 0) goto L_0x010a
            r2 = 1
            android.view.View r2 = r9.zza(r8, r2)     // Catch:{ all -> 0x017b }
            r8.zzbtt = r2     // Catch:{ all -> 0x017b }
            android.view.View r2 = r8.zzbtt     // Catch:{ all -> 0x017b }
            if (r2 == 0) goto L_0x010a
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r2 = r8.zzbue     // Catch:{ all -> 0x017b }
            java.lang.String r3 = "1007"
            java.lang.ref.WeakReference r4 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x017b }
            android.view.View r5 = r8.zzbtt     // Catch:{ all -> 0x017b }
            r4.<init>(r5)     // Catch:{ all -> 0x017b }
            r2.put(r3, r4)     // Catch:{ all -> 0x017b }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r2 = r8.zzbuc     // Catch:{ all -> 0x017b }
            java.lang.String r3 = "1007"
            java.lang.ref.WeakReference r4 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x017b }
            android.view.View r5 = r8.zzbtt     // Catch:{ all -> 0x017b }
            r4.<init>(r5)     // Catch:{ all -> 0x017b }
            r2.put(r3, r4)     // Catch:{ all -> 0x017b }
            r1.removeAllViews()     // Catch:{ all -> 0x017b }
            android.view.View r2 = r8.zzbtt     // Catch:{ all -> 0x017b }
            r1.addView(r2)     // Catch:{ all -> 0x017b }
        L_0x010a:
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r3 = r8.zzbuc     // Catch:{ all -> 0x017b }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r4 = r8.zzbud     // Catch:{ all -> 0x017b }
            r1 = r9
            r2 = r7
            r5 = r8
            r6 = r8
            r1.zza(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x017b }
            android.os.Handler r1 = com.google.android.gms.internal.zzagr.zzczc     // Catch:{ all -> 0x017b }
            com.google.android.gms.internal.zzos r2 = new com.google.android.gms.internal.zzos     // Catch:{ all -> 0x017b }
            r2.<init>(r8, r9)     // Catch:{ all -> 0x017b }
            r1.post(r2)     // Catch:{ all -> 0x017b }
            r8.zzg(r7)     // Catch:{ all -> 0x017b }
            java.lang.Object r9 = r8.mLock     // Catch:{ all -> 0x017b }
            monitor-enter(r9)     // Catch:{ all -> 0x017b }
            com.google.android.gms.internal.zzoa r1 = r8.zzbsk     // Catch:{ all -> 0x0175 }
            boolean r1 = r1 instanceof com.google.android.gms.internal.zzoe     // Catch:{ all -> 0x0175 }
            if (r1 != 0) goto L_0x012d
        L_0x012b:
            monitor-exit(r9)     // Catch:{ all -> 0x0175 }
            goto L_0x0173
        L_0x012d:
            com.google.android.gms.internal.zzoa r1 = r8.zzbsk     // Catch:{ all -> 0x0175 }
            com.google.android.gms.internal.zzoe r1 = (com.google.android.gms.internal.zzoe) r1     // Catch:{ all -> 0x0175 }
            java.lang.ref.WeakReference<android.view.View> r2 = r8.zzbub     // Catch:{ all -> 0x0175 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0175 }
            android.view.View r2 = (android.view.View) r2     // Catch:{ all -> 0x0175 }
            if (r1 == 0) goto L_0x012b
            android.content.Context r3 = r1.getContext()     // Catch:{ all -> 0x0175 }
            if (r3 == 0) goto L_0x012b
            if (r2 == 0) goto L_0x012b
            com.google.android.gms.internal.zzael r3 = com.google.android.gms.ads.internal.zzbs.zzfa()     // Catch:{ all -> 0x0175 }
            android.content.Context r4 = r2.getContext()     // Catch:{ all -> 0x0175 }
            boolean r3 = r3.zzt(r4)     // Catch:{ all -> 0x0175 }
            if (r3 == 0) goto L_0x012b
            java.lang.ref.WeakReference<com.google.android.gms.internal.zzfy> r3 = r8.zzbtx     // Catch:{ all -> 0x0175 }
            java.lang.Object r3 = r3.get()     // Catch:{ all -> 0x0175 }
            com.google.android.gms.internal.zzfy r3 = (com.google.android.gms.internal.zzfy) r3     // Catch:{ all -> 0x0175 }
            if (r3 != 0) goto L_0x016b
            com.google.android.gms.internal.zzfy r3 = new com.google.android.gms.internal.zzfy     // Catch:{ all -> 0x0175 }
            android.content.Context r4 = r2.getContext()     // Catch:{ all -> 0x0175 }
            r3.<init>(r4, r2)     // Catch:{ all -> 0x0175 }
            java.lang.ref.WeakReference r2 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x0175 }
            r2.<init>(r3)     // Catch:{ all -> 0x0175 }
            r8.zzbtx = r2     // Catch:{ all -> 0x0175 }
        L_0x016b:
            com.google.android.gms.internal.zzaek r1 = r1.zzke()     // Catch:{ all -> 0x0175 }
            r3.zza(r1)     // Catch:{ all -> 0x0175 }
            goto L_0x012b
        L_0x0173:
            monitor-exit(r0)     // Catch:{ all -> 0x017b }
            return
        L_0x0175:
            r1 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0175 }
            throw r1     // Catch:{ all -> 0x017b }
        L_0x0178:
            r9 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0178 }
            throw r9     // Catch:{ all -> 0x017b }
        L_0x017b:
            r9 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x017b }
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzor.zza(com.google.android.gms.dynamic.IObjectWrapper):void");
    }
}
