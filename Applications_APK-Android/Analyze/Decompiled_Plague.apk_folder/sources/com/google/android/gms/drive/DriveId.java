package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.zzal;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import com.google.android.gms.internal.zzbma;
import com.google.android.gms.internal.zzbmf;
import com.google.android.gms.internal.zzbog;
import com.google.android.gms.internal.zzbsc;
import com.google.android.gms.internal.zzbsd;
import com.google.android.gms.internal.zzfhj;
import com.google.android.gms.internal.zzfhk;

public class DriveId extends zzbej implements ReflectedParcelable {
    public static final Parcelable.Creator<DriveId> CREATOR = new zzl();
    public static final int RESOURCE_TYPE_FILE = 0;
    public static final int RESOURCE_TYPE_FOLDER = 1;
    public static final int RESOURCE_TYPE_UNKNOWN = -1;
    private static final zzal zzggp = new zzal("DriveId", "");
    private long zzgft;
    private volatile String zzgfv = null;
    private String zzggq;
    private long zzggr;
    private int zzggs;
    private volatile String zzggt = null;

    public DriveId(String str, long j, long j2, int i) {
        this.zzggq = str;
        boolean z = true;
        zzbq.checkArgument(!"".equals(str));
        if (str == null && j == -1) {
            z = false;
        }
        zzbq.checkArgument(z);
        this.zzggr = j;
        this.zzgft = j2;
        this.zzggs = i;
    }

    public static DriveId decodeFromString(String str) {
        boolean startsWith = str.startsWith("DriveId:");
        String valueOf = String.valueOf(str);
        zzbq.checkArgument(startsWith, valueOf.length() != 0 ? "Invalid DriveId: ".concat(valueOf) : new String("Invalid DriveId: "));
        return zzm(Base64.decode(str.substring(8), 10));
    }

    public static DriveId zzgq(String str) {
        zzbq.checkNotNull(str);
        return new DriveId(str, -1, -1, -1);
    }

    private static DriveId zzm(byte[] bArr) {
        try {
            zzbsc zzbsc = (zzbsc) zzfhk.zza(new zzbsc(), bArr);
            return new DriveId("".equals(zzbsc.zzgoz) ? null : zzbsc.zzgoz, zzbsc.zzgpa, zzbsc.zzgox, zzbsc.zzgpb);
        } catch (zzfhj unused) {
            throw new IllegalArgumentException();
        }
    }

    public DriveFile asDriveFile() {
        if (this.zzggs != 1) {
            return new zzbma(this);
        }
        throw new IllegalStateException("This DriveId corresponds to a folder. Call asDriveFolder instead.");
    }

    public DriveFolder asDriveFolder() {
        if (this.zzggs != 0) {
            return new zzbmf(this);
        }
        throw new IllegalStateException("This DriveId corresponds to a file. Call asDriveFile instead.");
    }

    public DriveResource asDriveResource() {
        return this.zzggs == 1 ? asDriveFolder() : this.zzggs == 0 ? asDriveFile() : new zzbog(this);
    }

    public final String encodeToString() {
        if (this.zzgfv == null) {
            zzbsc zzbsc = new zzbsc();
            zzbsc.versionCode = 1;
            zzbsc.zzgoz = this.zzggq == null ? "" : this.zzggq;
            zzbsc.zzgpa = this.zzggr;
            zzbsc.zzgox = this.zzgft;
            zzbsc.zzgpb = this.zzggs;
            String encodeToString = Base64.encodeToString(zzfhk.zzc(zzbsc), 10);
            String valueOf = String.valueOf("DriveId:");
            String valueOf2 = String.valueOf(encodeToString);
            this.zzgfv = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        }
        return this.zzgfv;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != DriveId.class) {
            return false;
        }
        DriveId driveId = (DriveId) obj;
        if (driveId.zzgft != this.zzgft) {
            return false;
        }
        if (driveId.zzggr == -1 && this.zzggr == -1) {
            return driveId.zzggq.equals(this.zzggq);
        }
        if (this.zzggq == null || driveId.zzggq == null) {
            return driveId.zzggr == this.zzggr;
        }
        if (driveId.zzggr == this.zzggr) {
            if (driveId.zzggq.equals(this.zzggq)) {
                return true;
            }
            zzggp.zzv("DriveId", "Unexpected unequal resourceId for same DriveId object.");
        }
        return false;
    }

    public String getResourceId() {
        return this.zzggq;
    }

    public int getResourceType() {
        return this.zzggs;
    }

    public int hashCode() {
        if (this.zzggr == -1) {
            return this.zzggq.hashCode();
        }
        String valueOf = String.valueOf(String.valueOf(this.zzgft));
        String valueOf2 = String.valueOf(String.valueOf(this.zzggr));
        return (valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf)).hashCode();
    }

    public final String toInvariantString() {
        if (this.zzggt == null) {
            zzbsd zzbsd = new zzbsd();
            zzbsd.zzgpa = this.zzggr;
            zzbsd.zzgox = this.zzgft;
            this.zzggt = Base64.encodeToString(zzfhk.zzc(zzbsd), 10);
        }
        return this.zzggt;
    }

    public String toString() {
        return encodeToString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, this.zzggq, false);
        zzbem.zza(parcel, 3, this.zzggr);
        zzbem.zza(parcel, 4, this.zzgft);
        zzbem.zzc(parcel, 5, this.zzggs);
        zzbem.zzai(parcel, zze);
    }
}
