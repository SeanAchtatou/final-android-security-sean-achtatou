package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.util.Arrays;

public final class zzdsq implements zzdoo {
    private final zzdtb zzlvg;
    private final zzdou zzlvh;
    private final int zzlvi;

    public zzdsq(zzdtb zzdtb, zzdou zzdou, int i) {
        this.zzlvg = zzdtb;
        this.zzlvh = zzdou;
        this.zzlvi = i;
    }

    public final byte[] zzd(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        byte[] zzaf = this.zzlvg.zzaf(bArr);
        byte[] copyOf = Arrays.copyOf(ByteBuffer.allocate(8).putLong(8 * ((long) bArr2.length)).array(), 8);
        return zzdte.zzc(zzaf, this.zzlvh.zzab(zzdte.zzc(bArr2, zzaf, copyOf)));
    }
}
