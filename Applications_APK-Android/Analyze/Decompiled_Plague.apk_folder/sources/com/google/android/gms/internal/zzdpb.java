package com.google.android.gms.internal;

import java.security.GeneralSecurityException;

public final class zzdpb {
    private static boolean zza(zzdos<zzdoo> zzdos) throws GeneralSecurityException {
        return zzdpa.zzlpq.zza(zzdos.getKeyType(), zzdos);
    }

    public static void zzbli() throws GeneralSecurityException {
        zza(new zzdpc());
        zza(new zzdpf());
        zza(new zzdpe());
        zza(new zzdpg());
    }
}
