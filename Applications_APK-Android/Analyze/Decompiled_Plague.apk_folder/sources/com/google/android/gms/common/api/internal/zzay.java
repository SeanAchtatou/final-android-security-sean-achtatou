package com.google.android.gms.common.api.internal;

import android.support.annotation.BinderThread;
import com.google.android.gms.internal.zzcwg;
import com.google.android.gms.internal.zzcwo;
import java.lang.ref.WeakReference;

final class zzay extends zzcwg {
    private final WeakReference<zzar> zzfos;

    zzay(zzar zzar) {
        this.zzfos = new WeakReference<>(zzar);
    }

    @BinderThread
    public final void zzb(zzcwo zzcwo) {
        zzar zzar = this.zzfos.get();
        if (zzar != null) {
            zzar.zzfob.zza(new zzaz(this, zzar, zzar, zzcwo));
        }
    }
}
