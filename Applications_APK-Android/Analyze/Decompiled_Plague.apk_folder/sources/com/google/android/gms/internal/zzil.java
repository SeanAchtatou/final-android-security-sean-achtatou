package com.google.android.gms.internal;

import java.io.IOException;

public final class zzil extends zzfhe<zzil> {
    public String zzbbn = null;
    public zzik zzbbo = null;
    public zzij zzbbp = null;

    public zzil() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        zzfhk zzfhk;
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts != 82) {
                if (zzcts == 122) {
                    if (this.zzbbo == null) {
                        this.zzbbo = new zzik();
                    }
                    zzfhk = this.zzbbo;
                } else if (zzcts == 146) {
                    if (this.zzbbp == null) {
                        this.zzbbp = new zzij();
                    }
                    zzfhk = this.zzbbp;
                } else if (!super.zza(zzfhb, zzcts)) {
                    return this;
                }
                zzfhb.zza(zzfhk);
            } else {
                this.zzbbn = zzfhb.readString();
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzbbn != null) {
            zzfhc.zzn(10, this.zzbbn);
        }
        if (this.zzbbo != null) {
            zzfhc.zza(15, this.zzbbo);
        }
        if (this.zzbbp != null) {
            zzfhc.zza(18, this.zzbbp);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzbbn != null) {
            zzo += zzfhc.zzo(10, this.zzbbn);
        }
        if (this.zzbbo != null) {
            zzo += zzfhc.zzb(15, this.zzbbo);
        }
        return this.zzbbp != null ? zzo + zzfhc.zzb(18, this.zzbbp) : zzo;
    }
}
