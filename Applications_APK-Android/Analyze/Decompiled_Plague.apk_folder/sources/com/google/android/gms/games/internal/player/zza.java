package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;

public interface zza extends Parcelable, Freezable<zza> {
    String zzatj();

    String zzatk();

    long zzatl();

    Uri zzatm();

    Uri zzatn();

    Uri zzato();
}
