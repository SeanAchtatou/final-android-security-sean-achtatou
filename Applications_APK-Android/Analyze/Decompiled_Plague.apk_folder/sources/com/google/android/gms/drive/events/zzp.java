package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;
import java.util.Locale;

public final class zzp extends zzbej {
    public static final Parcelable.Creator<zzp> CREATOR = new zzq();
    private int zzgjd;

    public zzp(int i) {
        this.zzgjd = i;
    }

    public final boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return zzbg.equal(Integer.valueOf(this.zzgjd), Integer.valueOf(((zzp) obj).zzgjd));
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.zzgjd)});
    }

    public final String toString() {
        return String.format(Locale.US, "TransferProgressOptions[type=%d]", Integer.valueOf(this.zzgjd));
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 2, this.zzgjd);
        zzbem.zzai(parcel, zze);
    }
}
