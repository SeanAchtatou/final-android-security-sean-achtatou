package com.google.android.gms.ads.internal.gmsg;

import android.content.Context;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.overlay.zzq;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzw;
import com.google.android.gms.internal.zzaiy;
import com.google.android.gms.internal.zzama;
import com.google.android.gms.internal.zzamv;
import com.google.android.gms.internal.zzamw;
import com.google.android.gms.internal.zzanj;
import com.google.android.gms.internal.zzanl;
import com.google.android.gms.internal.zzann;
import com.google.android.gms.internal.zzcs;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzvw;
import com.google.android.gms.internal.zzzb;
import com.tapjoy.TapjoyAuctionFlags;
import java.util.Map;

@zzzb
public final class zzab<T extends zzamv & zzamw & zzanj & zzanl & zzann> implements zzt<T> {
    private final Context mContext;
    private zzaiy zzaqi;
    private zzin zzbbt;
    private zzb zzbva;
    private zzw zzbwk;
    private zzvw zzbwl;
    private final zzcs zzbwn;
    private zzq zzbwo;
    private zzn zzbwp;
    private zzama zzbwq = null;

    public zzab(Context context, zzaiy zzaiy, zzcs zzcs, zzq zzq, zzin zzin, zzb zzb, zzn zzn, zzw zzw, zzvw zzvw) {
        this.mContext = context;
        this.zzaqi = zzaiy;
        this.zzbwn = zzcs;
        this.zzbwo = zzq;
        this.zzbbt = zzin;
        this.zzbva = zzb;
        this.zzbwk = zzw;
        this.zzbwl = zzvw;
        this.zzbwp = zzn;
    }

    private static boolean zzj(Map<String, String> map) {
        return TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE.equals(map.get("custom_close"));
    }

    private static int zzk(Map<String, String> map) {
        String str = map.get("o");
        if (str == null) {
            return -1;
        }
        if ("p".equalsIgnoreCase(str)) {
            return zzbs.zzee().zzqa();
        }
        if ("l".equalsIgnoreCase(str)) {
            return zzbs.zzee().zzpz();
        }
        if ("c".equalsIgnoreCase(str)) {
            return zzbs.zzee().zzqb();
        }
        return -1;
    }

    private final void zzk(boolean z) {
        if (this.zzbwl != null) {
            this.zzbwl.zzl(z);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:60:0x0139 A[SYNTHETIC, Splitter:B:60:0x0139] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0192  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x019d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ void zza(java.lang.Object r10, java.util.Map r11) {
        /*
            r9 = this;
            com.google.android.gms.internal.zzamv r10 = (com.google.android.gms.internal.zzamv) r10
            java.lang.String r0 = "u"
            java.lang.Object r0 = r11.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            android.content.Context r1 = r10.getContext()
            java.lang.String r0 = com.google.android.gms.internal.zzaeo.zzb(r0, r1)
            java.lang.String r1 = "a"
            java.lang.Object r1 = r11.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 != 0) goto L_0x0022
            java.lang.String r10 = "Action missing from an open GMSG."
            com.google.android.gms.internal.zzafj.zzco(r10)
            return
        L_0x0022:
            com.google.android.gms.ads.internal.zzw r2 = r9.zzbwk
            if (r2 == 0) goto L_0x0034
            com.google.android.gms.ads.internal.zzw r2 = r9.zzbwk
            boolean r2 = r2.zzcu()
            if (r2 != 0) goto L_0x0034
            com.google.android.gms.ads.internal.zzw r10 = r9.zzbwk
            r10.zzs(r0)
            return
        L_0x0034:
            java.lang.String r2 = "expand"
            boolean r2 = r2.equalsIgnoreCase(r1)
            r3 = 0
            if (r2 == 0) goto L_0x005d
            r0 = r10
            com.google.android.gms.internal.zzamw r0 = (com.google.android.gms.internal.zzamw) r0
            boolean r0 = r0.zzst()
            if (r0 == 0) goto L_0x004c
            java.lang.String r10 = "Cannot expand WebView that is already expanded."
            com.google.android.gms.internal.zzafj.zzco(r10)
            return
        L_0x004c:
            r9.zzk(r3)
            com.google.android.gms.internal.zzanj r10 = (com.google.android.gms.internal.zzanj) r10
            boolean r0 = zzj(r11)
            int r11 = zzk(r11)
            r10.zza(r0, r11)
            return
        L_0x005d:
            java.lang.String r2 = "webapp"
            boolean r2 = r2.equalsIgnoreCase(r1)
            if (r2 == 0) goto L_0x0096
            r9.zzk(r3)
            if (r0 == 0) goto L_0x0078
            com.google.android.gms.internal.zzanj r10 = (com.google.android.gms.internal.zzanj) r10
            boolean r1 = zzj(r11)
            int r11 = zzk(r11)
            r10.zza(r1, r11, r0)
            return
        L_0x0078:
            com.google.android.gms.internal.zzanj r10 = (com.google.android.gms.internal.zzanj) r10
            boolean r0 = zzj(r11)
            int r1 = zzk(r11)
            java.lang.String r2 = "html"
            java.lang.Object r2 = r11.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r3 = "baseurl"
            java.lang.Object r11 = r11.get(r3)
            java.lang.String r11 = (java.lang.String) r11
            r10.zza(r0, r1, r2, r11)
            return
        L_0x0096:
            java.lang.String r2 = "app"
            boolean r1 = r2.equalsIgnoreCase(r1)
            r2 = 1
            r4 = 0
            if (r1 == 0) goto L_0x00f2
            java.lang.String r1 = "true"
            java.lang.String r5 = "system_browser"
            java.lang.Object r5 = r11.get(r5)
            java.lang.String r5 = (java.lang.String) r5
            boolean r1 = r1.equalsIgnoreCase(r5)
            if (r1 == 0) goto L_0x00f2
            r9.zzk(r2)
            r10.getContext()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x00c2
            java.lang.String r10 = "Destination url cannot be empty."
            com.google.android.gms.internal.zzafj.zzco(r10)
            return
        L_0x00c2:
            com.google.android.gms.ads.internal.gmsg.zzac r0 = new com.google.android.gms.ads.internal.gmsg.zzac
            android.content.Context r1 = r10.getContext()
            r2 = r10
            com.google.android.gms.internal.zzanl r2 = (com.google.android.gms.internal.zzanl) r2
            com.google.android.gms.internal.zzcs r2 = r2.zzss()
            r3 = r10
            com.google.android.gms.internal.zzann r3 = (com.google.android.gms.internal.zzann) r3
            if (r3 != 0) goto L_0x00d5
            throw r4
        L_0x00d5:
            android.view.View r3 = (android.view.View) r3
            r0.<init>(r1, r2, r3)
            android.content.Intent r11 = r0.zzl(r11)
            com.google.android.gms.internal.zzanj r10 = (com.google.android.gms.internal.zzanj) r10     // Catch:{ ActivityNotFoundException -> 0x00e9 }
            com.google.android.gms.ads.internal.overlay.zzc r0 = new com.google.android.gms.ads.internal.overlay.zzc     // Catch:{ ActivityNotFoundException -> 0x00e9 }
            r0.<init>(r11)     // Catch:{ ActivityNotFoundException -> 0x00e9 }
            r10.zza(r0)     // Catch:{ ActivityNotFoundException -> 0x00e9 }
            return
        L_0x00e9:
            r10 = move-exception
            java.lang.String r10 = r10.getMessage()
            com.google.android.gms.internal.zzafj.zzco(r10)
            return
        L_0x00f2:
            r9.zzk(r2)
            java.lang.String r1 = "intent_url"
            java.lang.Object r1 = r11.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x0122
            android.content.Intent r2 = android.content.Intent.parseUri(r1, r3)     // Catch:{ URISyntaxException -> 0x0108 }
            goto L_0x0123
        L_0x0108:
            r2 = move-exception
            java.lang.String r3 = "Error parsing the url: "
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r5 = r1.length()
            if (r5 == 0) goto L_0x011a
            java.lang.String r1 = r3.concat(r1)
            goto L_0x011f
        L_0x011a:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r3)
        L_0x011f:
            com.google.android.gms.internal.zzafj.zzb(r1, r2)
        L_0x0122:
            r2 = r4
        L_0x0123:
            if (r2 == 0) goto L_0x0190
            android.net.Uri r1 = r2.getData()
            if (r1 == 0) goto L_0x0190
            android.net.Uri r1 = r2.getData()
            java.lang.String r3 = r1.toString()
            boolean r5 = android.text.TextUtils.isEmpty(r3)
            if (r5 != 0) goto L_0x018d
            com.google.android.gms.ads.internal.zzbs.zzec()     // Catch:{ Exception -> 0x0155 }
            android.content.Context r5 = r10.getContext()     // Catch:{ Exception -> 0x0155 }
            r6 = r10
            com.google.android.gms.internal.zzanl r6 = (com.google.android.gms.internal.zzanl) r6     // Catch:{ Exception -> 0x0155 }
            com.google.android.gms.internal.zzcs r6 = r6.zzss()     // Catch:{ Exception -> 0x0155 }
            r7 = r10
            com.google.android.gms.internal.zzann r7 = (com.google.android.gms.internal.zzann) r7     // Catch:{ Exception -> 0x0155 }
            if (r7 != 0) goto L_0x014d
            throw r4     // Catch:{ Exception -> 0x0155 }
        L_0x014d:
            android.view.View r7 = (android.view.View) r7     // Catch:{ Exception -> 0x0155 }
            java.lang.String r5 = com.google.android.gms.internal.zzagr.zza(r5, r6, r3, r7)     // Catch:{ Exception -> 0x0155 }
            r3 = r5
            goto L_0x0164
        L_0x0155:
            r5 = move-exception
            java.lang.String r6 = "Error occurred while adding signals."
            com.google.android.gms.internal.zzafj.zzb(r6, r5)
            com.google.android.gms.internal.zzaez r6 = com.google.android.gms.ads.internal.zzbs.zzeg()
            java.lang.String r7 = "OpenGmsgHandler.onGmsg"
            r6.zza(r5, r7)
        L_0x0164:
            android.net.Uri r5 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x016a }
            r1 = r5
            goto L_0x018d
        L_0x016a:
            r5 = move-exception
            java.lang.String r6 = "Error parsing the uri: "
            java.lang.String r3 = java.lang.String.valueOf(r3)
            int r7 = r3.length()
            if (r7 == 0) goto L_0x017c
            java.lang.String r3 = r6.concat(r3)
            goto L_0x0181
        L_0x017c:
            java.lang.String r3 = new java.lang.String
            r3.<init>(r6)
        L_0x0181:
            com.google.android.gms.internal.zzafj.zzb(r3, r5)
            com.google.android.gms.internal.zzaez r3 = com.google.android.gms.ads.internal.zzbs.zzeg()
            java.lang.String r6 = "OpenGmsgHandler.onGmsg"
            r3.zza(r5, r6)
        L_0x018d:
            r2.setData(r1)
        L_0x0190:
            if (r2 == 0) goto L_0x019d
            com.google.android.gms.internal.zzanj r10 = (com.google.android.gms.internal.zzanj) r10
            com.google.android.gms.ads.internal.overlay.zzc r11 = new com.google.android.gms.ads.internal.overlay.zzc
            r11.<init>(r2)
            r10.zza(r11)
            return
        L_0x019d:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x01bd
            com.google.android.gms.ads.internal.zzbs.zzec()
            android.content.Context r1 = r10.getContext()
            r2 = r10
            com.google.android.gms.internal.zzanl r2 = (com.google.android.gms.internal.zzanl) r2
            com.google.android.gms.internal.zzcs r2 = r2.zzss()
            r3 = r10
            com.google.android.gms.internal.zzann r3 = (com.google.android.gms.internal.zzann) r3
            if (r3 != 0) goto L_0x01b7
            throw r4
        L_0x01b7:
            android.view.View r3 = (android.view.View) r3
            java.lang.String r0 = com.google.android.gms.internal.zzagr.zza(r1, r2, r0, r3)
        L_0x01bd:
            r3 = r0
            com.google.android.gms.internal.zzanj r10 = (com.google.android.gms.internal.zzanj) r10
            com.google.android.gms.ads.internal.overlay.zzc r0 = new com.google.android.gms.ads.internal.overlay.zzc
            java.lang.String r1 = "i"
            java.lang.Object r1 = r11.get(r1)
            r2 = r1
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r1 = "m"
            java.lang.Object r1 = r11.get(r1)
            r4 = r1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.String r1 = "p"
            java.lang.Object r1 = r11.get(r1)
            r5 = r1
            java.lang.String r5 = (java.lang.String) r5
            java.lang.String r1 = "c"
            java.lang.Object r1 = r11.get(r1)
            r6 = r1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.String r1 = "f"
            java.lang.Object r1 = r11.get(r1)
            r7 = r1
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r1 = "e"
            java.lang.Object r11 = r11.get(r1)
            r8 = r11
            java.lang.String r8 = (java.lang.String) r8
            r1 = r0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            r10.zza(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.gmsg.zzab.zza(java.lang.Object, java.util.Map):void");
    }
}
