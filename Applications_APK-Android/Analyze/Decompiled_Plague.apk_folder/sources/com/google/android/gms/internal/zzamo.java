package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.gmsg.zzt;
import com.google.android.gms.ads.internal.overlay.zzc;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.zzbl;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.ads.internal.zzv;
import com.google.android.gms.common.util.zzq;
import com.miniclip.videoads.ProviderConfig;
import com.tapjoy.TapjoyAuctionFlags;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@zzzb
final class zzamo extends WebView implements ViewTreeObserver.OnGlobalLayoutListener, DownloadListener, zzama {
    /* access modifiers changed from: private */
    public final Object mLock = new Object();
    private String zzaky;
    private final zzv zzamw;
    private final zzaiy zzaov;
    private zzair zzaun;
    private final WindowManager zzavc;
    @Nullable
    private final zzcs zzbta;
    private int zzcfw = -1;
    private int zzcfx = -1;
    private int zzcfz = -1;
    private int zzcga = -1;
    private final zzib zzckz;
    private String zzcqb = "";
    private Boolean zzcxg;
    private zznb zzdgr;
    private final zzano zzdjd;
    private final zzbl zzdje;
    private zzamb zzdjf;
    private zzd zzdjg;
    private zzanp zzdjh;
    private boolean zzdji;
    private boolean zzdjj;
    private boolean zzdjk;
    private boolean zzdjl;
    private int zzdjm;
    private boolean zzdjn = true;
    private boolean zzdjo = false;
    private zzamr zzdjp;
    private boolean zzdjq;
    private boolean zzdjr;
    private zzny zzdjs;
    private int zzdjt;
    /* access modifiers changed from: private */
    public int zzdju;
    private zznb zzdjv;
    private zznb zzdjw;
    private zznc zzdjx;
    private WeakReference<View.OnClickListener> zzdjy;
    private zzd zzdjz;
    private boolean zzdka;
    private Map<String, zzalt> zzdkb;

    private zzamo(zzano zzano, zzanp zzanp, String str, boolean z, boolean z2, @Nullable zzcs zzcs, zzaiy zzaiy, zznd zznd, zzbl zzbl, zzv zzv, zzib zzib) {
        super(zzano);
        this.zzdjd = zzano;
        this.zzdjh = zzanp;
        this.zzaky = str;
        this.zzdjk = z;
        this.zzdjm = -1;
        this.zzbta = zzcs;
        this.zzaov = zzaiy;
        this.zzdje = zzbl;
        this.zzamw = zzv;
        this.zzavc = (WindowManager) getContext().getSystemService("window");
        this.zzckz = zzib;
        setBackgroundColor(0);
        WebSettings settings = getSettings();
        settings.setAllowFileAccess(false);
        try {
            settings.setJavaScriptEnabled(true);
        } catch (NullPointerException e) {
            zzafj.zzb("Unable to enable Javascript.", e);
        }
        settings.setSavePassword(false);
        settings.setSupportMultipleWindows(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(2);
        }
        settings.setUserAgentString(zzbs.zzec().zzp(zzano, zzaiy.zzcp));
        zzbs.zzee().zza(getContext(), settings);
        setDownloadListener(this);
        zzts();
        if (zzq.zzalx()) {
            addJavascriptInterface(new zzamu(this), "googleAdsJsInterface");
        }
        removeJavascriptInterface("accessibility");
        removeJavascriptInterface("accessibilityTraversal");
        this.zzaun = new zzair(this.zzdjd.zzrz(), this, this, null);
        zztv();
        this.zzdjx = new zznc(new zznd(true, "make_wv", this.zzaky));
        this.zzdjx.zziy().zzc(zznd);
        this.zzdgr = zzmw.zzb(this.zzdjx.zziy());
        this.zzdjx.zza("native:view_create", this.zzdgr);
        this.zzdjw = null;
        this.zzdjv = null;
        zzbs.zzee().zzaw(zzano);
    }

    private final void zza(Boolean bool) {
        synchronized (this.mLock) {
            this.zzcxg = bool;
        }
        zzbs.zzeg().zza(bool);
    }

    private final void zzag(boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("isVisible", z ? TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE : "0");
        zza("onAdVisibilityChanged", hashMap);
    }

    static zzamo zzb(Context context, zzanp zzanp, String str, boolean z, boolean z2, @Nullable zzcs zzcs, zzaiy zzaiy, zznd zznd, zzbl zzbl, zzv zzv, zzib zzib) {
        return new zzamo(new zzano(context), zzanp, str, z, z2, zzcs, zzaiy, zznd, zzbl, zzv, zzib);
    }

    private final void zzcv(String str) {
        synchronized (this.mLock) {
            if (!isDestroyed()) {
                loadUrl(str);
            } else {
                zzafj.zzco("The webview is destroyed. Ignoring action.");
            }
        }
    }

    private final void zzcw(String str) {
        if (zzq.zzalz()) {
            if (zzpa() == null) {
                synchronized (this.mLock) {
                    this.zzcxg = zzbs.zzeg().zzpa();
                    if (this.zzcxg == null) {
                        try {
                            evaluateJavascript("(function(){})()", null);
                            zza((Boolean) true);
                        } catch (IllegalStateException unused) {
                            zza((Boolean) false);
                        }
                    }
                }
            }
            if (zzpa().booleanValue()) {
                synchronized (this.mLock) {
                    if (!isDestroyed()) {
                        evaluateJavascript(str, null);
                    } else {
                        zzafj.zzco("The webview is destroyed. Ignoring action.");
                    }
                }
                return;
            }
            String valueOf = String.valueOf(str);
            zzcv(valueOf.length() != 0 ? "javascript:".concat(valueOf) : new String("javascript:"));
            return;
        }
        String valueOf2 = String.valueOf(str);
        zzcv(valueOf2.length() != 0 ? "javascript:".concat(valueOf2) : new String("javascript:"));
    }

    private final Boolean zzpa() {
        Boolean bool;
        synchronized (this.mLock) {
            bool = this.zzcxg;
        }
        return bool;
    }

    private final void zzpo() {
        synchronized (this.mLock) {
            if (!this.zzdka) {
                this.zzdka = true;
                zzbs.zzeg().zzpo();
            }
        }
    }

    private final boolean zztq() {
        int i;
        int i2;
        boolean z = false;
        if (!this.zzdjf.zzfr() && !this.zzdjf.zzte()) {
            return false;
        }
        zzbs.zzec();
        DisplayMetrics zza = zzagr.zza(this.zzavc);
        zzjk.zzhx();
        int zzb = zzais.zzb(zza, zza.widthPixels);
        zzjk.zzhx();
        int zzb2 = zzais.zzb(zza, zza.heightPixels);
        Activity zzrz = this.zzdjd.zzrz();
        if (zzrz == null || zzrz.getWindow() == null) {
            i2 = zzb;
            i = zzb2;
        } else {
            zzbs.zzec();
            int[] zzf = zzagr.zzf(zzrz);
            zzjk.zzhx();
            i2 = zzais.zzb(zza, zzf[0]);
            zzjk.zzhx();
            i = zzais.zzb(zza, zzf[1]);
        }
        if (this.zzcfw == zzb && this.zzcfx == zzb2 && this.zzcfz == i2 && this.zzcga == i) {
            return false;
        }
        if (!(this.zzcfw == zzb && this.zzcfx == zzb2)) {
            z = true;
        }
        this.zzcfw = zzb;
        this.zzcfx = zzb2;
        this.zzcfz = i2;
        this.zzcga = i;
        new zzwg(this).zza(zzb, zzb2, i2, i, zza.density, this.zzavc.getDefaultDisplay().getRotation());
        return z;
    }

    private final void zztr() {
        zzmw.zza(this.zzdjx.zziy(), this.zzdgr, "aeh2");
    }

    private final void zzts() {
        synchronized (this.mLock) {
            if (!this.zzdjk) {
                if (!this.zzdjh.zztx()) {
                    if (Build.VERSION.SDK_INT < 18) {
                        zzafj.zzbw("Disabling hardware acceleration on an AdView.");
                        synchronized (this.mLock) {
                            if (!this.zzdjl) {
                                zzbs.zzee().zzt(this);
                            }
                            this.zzdjl = true;
                        }
                    } else {
                        zzafj.zzbw("Enabling hardware acceleration on an AdView.");
                        zztt();
                    }
                }
            }
            zzafj.zzbw("Enabling hardware acceleration on an overlay.");
            zztt();
        }
    }

    private final void zztt() {
        synchronized (this.mLock) {
            if (this.zzdjl) {
                zzbs.zzee().zzs(this);
            }
            this.zzdjl = false;
        }
    }

    private final void zztu() {
        synchronized (this.mLock) {
            this.zzdkb = null;
        }
    }

    private final void zztv() {
        zznd zziy;
        if (this.zzdjx != null && (zziy = this.zzdjx.zziy()) != null && zzbs.zzeg().zzow() != null) {
            zzbs.zzeg().zzow().zza(zziy);
        }
    }

    public final void destroy() {
        synchronized (this.mLock) {
            zztv();
            this.zzaun.zzqp();
            if (this.zzdjg != null) {
                this.zzdjg.close();
                this.zzdjg.onDestroy();
                this.zzdjg = null;
            }
            this.zzdjf.reset();
            if (!this.zzdjj) {
                zzbs.zzey();
                zzals.zzb(this);
                zztu();
                this.zzdjj = true;
                zzafj.v("Initiating WebView self destruct sequence in 3...");
                this.zzdjf.zzti();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return;
     */
    @android.annotation.TargetApi(19)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void evaluateJavascript(java.lang.String r3, android.webkit.ValueCallback<java.lang.String> r4) {
        /*
            r2 = this;
            java.lang.Object r0 = r2.mLock
            monitor-enter(r0)
            boolean r1 = r2.isDestroyed()     // Catch:{ all -> 0x001b }
            if (r1 == 0) goto L_0x0016
            java.lang.String r3 = "The webview is destroyed. Ignoring action."
            com.google.android.gms.internal.zzafj.zzco(r3)     // Catch:{ all -> 0x001b }
            if (r4 == 0) goto L_0x0014
            r3 = 0
            r4.onReceiveValue(r3)     // Catch:{ all -> 0x001b }
        L_0x0014:
            monitor-exit(r0)     // Catch:{ all -> 0x001b }
            return
        L_0x0016:
            super.evaluateJavascript(r3, r4)     // Catch:{ all -> 0x001b }
            monitor-exit(r0)     // Catch:{ all -> 0x001b }
            return
        L_0x001b:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001b }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzamo.evaluateJavascript(java.lang.String, android.webkit.ValueCallback):void");
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        try {
            if (this.mLock != null) {
                synchronized (this.mLock) {
                    if (!this.zzdjj) {
                        this.zzdjf.reset();
                        zzbs.zzey();
                        zzals.zzb(this);
                        zztu();
                        zzpo();
                    }
                }
            }
            super.finalize();
        } catch (Throwable th) {
            super.finalize();
            throw th;
        }
    }

    public final View.OnClickListener getOnClickListener() {
        return this.zzdjy.get();
    }

    public final String getRequestId() {
        String str;
        synchronized (this.mLock) {
            str = this.zzcqb;
        }
        return str;
    }

    public final int getRequestedOrientation() {
        int i;
        synchronized (this.mLock) {
            i = this.zzdjm;
        }
        return i;
    }

    public final WebView getWebView() {
        return this;
    }

    public final boolean isDestroyed() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzdjj;
        }
        return z;
    }

    public final void loadData(String str, String str2, String str3) {
        synchronized (this.mLock) {
            if (!isDestroyed()) {
                super.loadData(str, str2, str3);
            } else {
                zzafj.zzco("The webview is destroyed. Ignoring action.");
            }
        }
    }

    public final void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        synchronized (this.mLock) {
            if (!isDestroyed()) {
                super.loadDataWithBaseURL(str, str2, str3, str4, str5);
            } else {
                zzafj.zzco("The webview is destroyed. Ignoring action.");
            }
        }
    }

    public final void loadUrl(String str) {
        synchronized (this.mLock) {
            if (!isDestroyed()) {
                try {
                    super.loadUrl(str);
                } catch (Exception | IncompatibleClassChangeError | NoClassDefFoundError e) {
                    zzbs.zzeg().zza(e, "AdWebViewImpl.loadUrl");
                    zzafj.zzc("Could not call loadUrl. ", e);
                }
            } else {
                zzafj.zzco("The webview is destroyed. Ignoring action.");
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        synchronized (this.mLock) {
            super.onAttachedToWindow();
            if (!isDestroyed()) {
                this.zzaun.onAttachedToWindow();
            }
            boolean z = this.zzdjq;
            if (this.zzdjf != null && this.zzdjf.zzte()) {
                if (!this.zzdjr) {
                    ViewTreeObserver.OnGlobalLayoutListener zztf = this.zzdjf.zztf();
                    if (zztf != null) {
                        zzbs.zzez();
                        if (this == null) {
                            throw null;
                        }
                        zzakg.zza(this, zztf);
                    }
                    ViewTreeObserver.OnScrollChangedListener zztg = this.zzdjf.zztg();
                    if (zztg != null) {
                        zzbs.zzez();
                        if (this == null) {
                            throw null;
                        }
                        zzakg.zza(this, zztg);
                    }
                    this.zzdjr = true;
                }
                zztq();
                z = true;
            }
            zzag(z);
        }
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        synchronized (this.mLock) {
            if (!isDestroyed()) {
                this.zzaun.onDetachedFromWindow();
            }
            super.onDetachedFromWindow();
            if (this.zzdjr && this.zzdjf != null && this.zzdjf.zzte() && getViewTreeObserver() != null && getViewTreeObserver().isAlive()) {
                ViewTreeObserver.OnGlobalLayoutListener zztf = this.zzdjf.zztf();
                if (zztf != null) {
                    zzbs.zzee().zza(getViewTreeObserver(), zztf);
                }
                ViewTreeObserver.OnScrollChangedListener zztg = this.zzdjf.zztg();
                if (zztg != null) {
                    getViewTreeObserver().removeOnScrollChangedListener(zztg);
                }
                this.zzdjr = false;
            }
        }
        zzag(false);
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse(str), str4);
            zzbs.zzec();
            zzagr.zza(getContext(), intent);
        } catch (ActivityNotFoundException unused) {
            StringBuilder sb = new StringBuilder(51 + String.valueOf(str).length() + String.valueOf(str4).length());
            sb.append("Couldn't find an Activity to view url/mimetype: ");
            sb.append(str);
            sb.append(" / ");
            sb.append(str4);
            zzafj.zzbw(sb.toString());
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    public final void onDraw(Canvas canvas) {
        if (!isDestroyed()) {
            if (Build.VERSION.SDK_INT != 21 || !canvas.isHardwareAccelerated() || isAttachedToWindow()) {
                super.onDraw(canvas);
                if (this.zzdjf != null && this.zzdjf.zztp() != null) {
                    this.zzdjf.zztp().zzcv();
                }
            }
        }
    }

    public final boolean onGenericMotionEvent(MotionEvent motionEvent) {
        if (((Boolean) zzbs.zzep().zzd(zzmq.zzbjm)).booleanValue()) {
            float axisValue = motionEvent.getAxisValue(9);
            float axisValue2 = motionEvent.getAxisValue(10);
            if (motionEvent.getActionMasked() == 8) {
                if (axisValue > 0.0f && !canScrollVertically(-1)) {
                    return false;
                }
                if (axisValue < 0.0f && !canScrollVertically(1)) {
                    return false;
                }
                if (axisValue2 > 0.0f && !canScrollHorizontally(-1)) {
                    return false;
                }
                if (axisValue2 < 0.0f && !canScrollHorizontally(1)) {
                    return false;
                }
            }
        }
        return super.onGenericMotionEvent(motionEvent);
    }

    public final void onGlobalLayout() {
        boolean zztq = zztq();
        zzd zzsm = zzsm();
        if (zzsm != null && zztq) {
            zzsm.zzms();
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0144, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x013c  */
    @android.annotation.SuppressLint({"DrawAllocation"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onMeasure(int r8, int r9) {
        /*
            r7 = this;
            java.lang.Object r0 = r7.mLock
            monitor-enter(r0)
            boolean r1 = r7.isDestroyed()     // Catch:{ all -> 0x014a }
            r2 = 0
            if (r1 == 0) goto L_0x000f
            r7.setMeasuredDimension(r2, r2)     // Catch:{ all -> 0x014a }
            monitor-exit(r0)     // Catch:{ all -> 0x014a }
            return
        L_0x000f:
            boolean r1 = r7.isInEditMode()     // Catch:{ all -> 0x014a }
            if (r1 != 0) goto L_0x0145
            boolean r1 = r7.zzdjk     // Catch:{ all -> 0x014a }
            if (r1 != 0) goto L_0x0145
            com.google.android.gms.internal.zzanp r1 = r7.zzdjh     // Catch:{ all -> 0x014a }
            boolean r1 = r1.zzty()     // Catch:{ all -> 0x014a }
            if (r1 == 0) goto L_0x0023
            goto L_0x0145
        L_0x0023:
            com.google.android.gms.internal.zzanp r1 = r7.zzdjh     // Catch:{ all -> 0x014a }
            boolean r1 = r1.isFluid()     // Catch:{ all -> 0x014a }
            if (r1 == 0) goto L_0x007c
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r1 = com.google.android.gms.internal.zzmq.zzbnf     // Catch:{ all -> 0x014a }
            com.google.android.gms.internal.zzmo r2 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ all -> 0x014a }
            java.lang.Object r1 = r2.zzd(r1)     // Catch:{ all -> 0x014a }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ all -> 0x014a }
            boolean r1 = r1.booleanValue()     // Catch:{ all -> 0x014a }
            if (r1 != 0) goto L_0x0077
            boolean r1 = com.google.android.gms.common.util.zzq.zzalx()     // Catch:{ all -> 0x014a }
            if (r1 != 0) goto L_0x0044
            goto L_0x0077
        L_0x0044:
            java.lang.String r1 = "/contentHeight"
            com.google.android.gms.internal.zzamp r2 = new com.google.android.gms.internal.zzamp     // Catch:{ all -> 0x014a }
            r2.<init>(r7)     // Catch:{ all -> 0x014a }
            r7.zza(r1, r2)     // Catch:{ all -> 0x014a }
            java.lang.String r1 = "(function() {  var height = -1;  if (document.body) {    height = document.body.offsetHeight;  } else if (document.documentElement) {    height = document.documentElement.offsetHeight;  }  var url = 'gmsg://mobileads.google.com/contentHeight?';  url += 'height=' + height;  try {    window.googleAdsJsInterface.notify(url);  } catch (e) {    var frame = document.getElementById('afma-notify-fluid');    if (!frame) {      frame = document.createElement('IFRAME');      frame.id = 'afma-notify-fluid';      frame.style.display = 'none';      var body = document.body || document.documentElement;      body.appendChild(frame);    }    frame.src = url;  }})();"
            r7.zzcw(r1)     // Catch:{ all -> 0x014a }
            com.google.android.gms.internal.zzano r1 = r7.zzdjd     // Catch:{ all -> 0x014a }
            android.content.res.Resources r1 = r1.getResources()     // Catch:{ all -> 0x014a }
            android.util.DisplayMetrics r1 = r1.getDisplayMetrics()     // Catch:{ all -> 0x014a }
            float r1 = r1.density     // Catch:{ all -> 0x014a }
            int r8 = android.view.View.MeasureSpec.getSize(r8)     // Catch:{ all -> 0x014a }
            int r2 = r7.zzdju     // Catch:{ all -> 0x014a }
            r3 = -1
            if (r2 == r3) goto L_0x006e
            int r9 = r7.zzdju     // Catch:{ all -> 0x014a }
            float r9 = (float) r9     // Catch:{ all -> 0x014a }
            float r9 = r9 * r1
            int r9 = (int) r9     // Catch:{ all -> 0x014a }
            goto L_0x0072
        L_0x006e:
            int r9 = android.view.View.MeasureSpec.getSize(r9)     // Catch:{ all -> 0x014a }
        L_0x0072:
            r7.setMeasuredDimension(r8, r9)     // Catch:{ all -> 0x014a }
            monitor-exit(r0)     // Catch:{ all -> 0x014a }
            return
        L_0x0077:
            super.onMeasure(r8, r9)     // Catch:{ all -> 0x014a }
            monitor-exit(r0)     // Catch:{ all -> 0x014a }
            return
        L_0x007c:
            com.google.android.gms.internal.zzanp r1 = r7.zzdjh     // Catch:{ all -> 0x014a }
            boolean r1 = r1.zztx()     // Catch:{ all -> 0x014a }
            if (r1 == 0) goto L_0x009b
            android.util.DisplayMetrics r8 = new android.util.DisplayMetrics     // Catch:{ all -> 0x014a }
            r8.<init>()     // Catch:{ all -> 0x014a }
            android.view.WindowManager r9 = r7.zzavc     // Catch:{ all -> 0x014a }
            android.view.Display r9 = r9.getDefaultDisplay()     // Catch:{ all -> 0x014a }
            r9.getMetrics(r8)     // Catch:{ all -> 0x014a }
            int r9 = r8.widthPixels     // Catch:{ all -> 0x014a }
            int r8 = r8.heightPixels     // Catch:{ all -> 0x014a }
            r7.setMeasuredDimension(r9, r8)     // Catch:{ all -> 0x014a }
            monitor-exit(r0)     // Catch:{ all -> 0x014a }
            return
        L_0x009b:
            int r1 = android.view.View.MeasureSpec.getMode(r8)     // Catch:{ all -> 0x014a }
            int r8 = android.view.View.MeasureSpec.getSize(r8)     // Catch:{ all -> 0x014a }
            int r3 = android.view.View.MeasureSpec.getMode(r9)     // Catch:{ all -> 0x014a }
            int r9 = android.view.View.MeasureSpec.getSize(r9)     // Catch:{ all -> 0x014a }
            r4 = 1073741824(0x40000000, float:2.0)
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = 2147483647(0x7fffffff, float:NaN)
            if (r1 == r5) goto L_0x00b9
            if (r1 != r4) goto L_0x00b7
            goto L_0x00b9
        L_0x00b7:
            r1 = r6
            goto L_0x00ba
        L_0x00b9:
            r1 = r8
        L_0x00ba:
            if (r3 == r5) goto L_0x00be
            if (r3 != r4) goto L_0x00bf
        L_0x00be:
            r6 = r9
        L_0x00bf:
            com.google.android.gms.internal.zzanp r3 = r7.zzdjh     // Catch:{ all -> 0x014a }
            int r3 = r3.widthPixels     // Catch:{ all -> 0x014a }
            r4 = 8
            if (r3 > r1) goto L_0x00e3
            com.google.android.gms.internal.zzanp r1 = r7.zzdjh     // Catch:{ all -> 0x014a }
            int r1 = r1.heightPixels     // Catch:{ all -> 0x014a }
            if (r1 <= r6) goto L_0x00ce
            goto L_0x00e3
        L_0x00ce:
            int r8 = r7.getVisibility()     // Catch:{ all -> 0x014a }
            if (r8 == r4) goto L_0x00d7
            r7.setVisibility(r2)     // Catch:{ all -> 0x014a }
        L_0x00d7:
            com.google.android.gms.internal.zzanp r8 = r7.zzdjh     // Catch:{ all -> 0x014a }
            int r8 = r8.widthPixels     // Catch:{ all -> 0x014a }
            com.google.android.gms.internal.zzanp r9 = r7.zzdjh     // Catch:{ all -> 0x014a }
            int r9 = r9.heightPixels     // Catch:{ all -> 0x014a }
            r7.setMeasuredDimension(r8, r9)     // Catch:{ all -> 0x014a }
            goto L_0x0143
        L_0x00e3:
            com.google.android.gms.internal.zzano r1 = r7.zzdjd     // Catch:{ all -> 0x014a }
            android.content.res.Resources r1 = r1.getResources()     // Catch:{ all -> 0x014a }
            android.util.DisplayMetrics r1 = r1.getDisplayMetrics()     // Catch:{ all -> 0x014a }
            float r1 = r1.density     // Catch:{ all -> 0x014a }
            com.google.android.gms.internal.zzanp r3 = r7.zzdjh     // Catch:{ all -> 0x014a }
            int r3 = r3.widthPixels     // Catch:{ all -> 0x014a }
            float r3 = (float) r3     // Catch:{ all -> 0x014a }
            float r3 = r3 / r1
            int r3 = (int) r3     // Catch:{ all -> 0x014a }
            com.google.android.gms.internal.zzanp r5 = r7.zzdjh     // Catch:{ all -> 0x014a }
            int r5 = r5.heightPixels     // Catch:{ all -> 0x014a }
            float r5 = (float) r5     // Catch:{ all -> 0x014a }
            float r5 = r5 / r1
            int r5 = (int) r5     // Catch:{ all -> 0x014a }
            float r8 = (float) r8     // Catch:{ all -> 0x014a }
            float r8 = r8 / r1
            int r8 = (int) r8     // Catch:{ all -> 0x014a }
            float r9 = (float) r9     // Catch:{ all -> 0x014a }
            float r9 = r9 / r1
            int r9 = (int) r9     // Catch:{ all -> 0x014a }
            r1 = 103(0x67, float:1.44E-43)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x014a }
            r6.<init>(r1)     // Catch:{ all -> 0x014a }
            java.lang.String r1 = "Not enough space to show ad. Needs "
            r6.append(r1)     // Catch:{ all -> 0x014a }
            r6.append(r3)     // Catch:{ all -> 0x014a }
            java.lang.String r1 = "x"
            r6.append(r1)     // Catch:{ all -> 0x014a }
            r6.append(r5)     // Catch:{ all -> 0x014a }
            java.lang.String r1 = " dp, but only has "
            r6.append(r1)     // Catch:{ all -> 0x014a }
            r6.append(r8)     // Catch:{ all -> 0x014a }
            java.lang.String r8 = "x"
            r6.append(r8)     // Catch:{ all -> 0x014a }
            r6.append(r9)     // Catch:{ all -> 0x014a }
            java.lang.String r8 = " dp."
            r6.append(r8)     // Catch:{ all -> 0x014a }
            java.lang.String r8 = r6.toString()     // Catch:{ all -> 0x014a }
            com.google.android.gms.internal.zzafj.zzco(r8)     // Catch:{ all -> 0x014a }
            int r8 = r7.getVisibility()     // Catch:{ all -> 0x014a }
            if (r8 == r4) goto L_0x0140
            r8 = 4
            r7.setVisibility(r8)     // Catch:{ all -> 0x014a }
        L_0x0140:
            r7.setMeasuredDimension(r2, r2)     // Catch:{ all -> 0x014a }
        L_0x0143:
            monitor-exit(r0)     // Catch:{ all -> 0x014a }
            return
        L_0x0145:
            super.onMeasure(r8, r9)     // Catch:{ all -> 0x014a }
            monitor-exit(r0)     // Catch:{ all -> 0x014a }
            return
        L_0x014a:
            r8 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x014a }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzamo.onMeasure(int, int):void");
    }

    public final void onPause() {
        if (!isDestroyed()) {
            try {
                super.onPause();
            } catch (Exception e) {
                zzafj.zzb("Could not pause webview.", e);
            }
        }
    }

    public final void onResume() {
        if (!isDestroyed()) {
            try {
                super.onResume();
            } catch (Exception e) {
                zzafj.zzb("Could not resume webview.", e);
            }
        }
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.zzdjf.zzte()) {
            synchronized (this.mLock) {
                if (this.zzdjs != null) {
                    this.zzdjs.zzc(motionEvent);
                }
            }
        } else if (this.zzbta != null) {
            this.zzbta.zza(motionEvent);
        }
        if (isDestroyed()) {
            return false;
        }
        return super.onTouchEvent(motionEvent);
    }

    public final void setContext(Context context) {
        this.zzdjd.setBaseContext(context);
        this.zzaun.zzi(this.zzdjd.zzrz());
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
        this.zzdjy = new WeakReference<>(onClickListener);
        super.setOnClickListener(onClickListener);
    }

    public final void setRequestedOrientation(int i) {
        synchronized (this.mLock) {
            this.zzdjm = i;
            if (this.zzdjg != null) {
                this.zzdjg.setRequestedOrientation(this.zzdjm);
            }
        }
    }

    public final void setWebViewClient(WebViewClient webViewClient) {
        super.setWebViewClient(webViewClient);
        if (webViewClient instanceof zzamb) {
            this.zzdjf = (zzamb) webViewClient;
        }
    }

    public final void stopLoading() {
        if (!isDestroyed()) {
            try {
                super.stopLoading();
            } catch (Exception e) {
                zzafj.zzb("Could not stop loading webview.", e);
            }
        }
    }

    public final void zza(zzc zzc) {
        this.zzdjf.zza(zzc);
    }

    public final void zza(zzamr zzamr) {
        synchronized (this.mLock) {
            if (this.zzdjp != null) {
                zzafj.e("Attempt to create multiple AdWebViewVideoControllers.");
            } else {
                this.zzdjp = zzamr;
            }
        }
    }

    public final void zza(zzanp zzanp) {
        synchronized (this.mLock) {
            this.zzdjh = zzanp;
            requestLayout();
        }
    }

    public final void zza(zzgb zzgb) {
        synchronized (this.mLock) {
            this.zzdjq = zzgb.zzaxe;
        }
        zzag(zzgb.zzaxe);
    }

    public final void zza(String str, zzt<? super zzama> zzt) {
        if (this.zzdjf != null) {
            this.zzdjf.zza(str, zzt);
        }
    }

    public final void zza(String str, Map<String, ?> map) {
        try {
            zza(str, zzbs.zzec().zzp(map));
        } catch (JSONException unused) {
            zzafj.zzco("Could not convert parameters to JSON.");
        }
    }

    public final void zza(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        String jSONObject2 = jSONObject.toString();
        StringBuilder sb = new StringBuilder();
        sb.append("(window.AFMA_ReceiveMessage || function() {})('");
        sb.append(str);
        sb.append("'");
        sb.append(",");
        sb.append(jSONObject2);
        sb.append(");");
        String valueOf = String.valueOf(sb.toString());
        zzafj.zzbw(valueOf.length() != 0 ? "Dispatching AFMA event: ".concat(valueOf) : new String("Dispatching AFMA event: "));
        zzcw(sb.toString());
    }

    public final void zza(boolean z, int i) {
        this.zzdjf.zza(z, i);
    }

    public final void zza(boolean z, int i, String str) {
        this.zzdjf.zza(z, i, str);
    }

    public final void zza(boolean z, int i, String str, String str2) {
        this.zzdjf.zza(z, i, str, str2);
    }

    public final void zzab(boolean z) {
        this.zzdjf.zzab(z);
    }

    public final void zzac(boolean z) {
        synchronized (this.mLock) {
            boolean z2 = z != this.zzdjk;
            this.zzdjk = z;
            zzts();
            if (z2) {
                new zzwg(this).zzbn(z ? "expanded" : "default");
            }
        }
    }

    public final void zzad(boolean z) {
        synchronized (this.mLock) {
            if (this.zzdjg != null) {
                this.zzdjg.zza(this.zzdjf.zzfr(), z);
            } else {
                this.zzdji = z;
            }
        }
    }

    public final void zzae(boolean z) {
        synchronized (this.mLock) {
            this.zzdjn = z;
        }
    }

    public final void zzaf(boolean z) {
        synchronized (this.mLock) {
            this.zzdjt += z ? 1 : -1;
            if (this.zzdjt <= 0 && this.zzdjg != null) {
                this.zzdjg.zzmv();
            }
        }
    }

    public final void zzag(int i) {
        if (i == 0) {
            zzmw.zza(this.zzdjx.zziy(), this.zzdgr, "aebb2");
        }
        zztr();
        if (this.zzdjx.zziy() != null) {
            this.zzdjx.zziy().zzf("close_type", String.valueOf(i));
        }
        HashMap hashMap = new HashMap(2);
        hashMap.put("closetype", String.valueOf(i));
        hashMap.put(ProviderConfig.MCVideoAdsVersionKey, this.zzaov.zzcp);
        zza("onhide", hashMap);
    }

    public final void zzb(zzd zzd) {
        synchronized (this.mLock) {
            this.zzdjg = zzd;
        }
    }

    public final void zzb(zzny zzny) {
        synchronized (this.mLock) {
            this.zzdjs = zzny;
        }
    }

    public final void zzb(String str, zzt<? super zzama> zzt) {
        if (this.zzdjf != null) {
            this.zzdjf.zzb(str, zzt);
        }
    }

    public final void zzb(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        String jSONObject2 = jSONObject.toString();
        StringBuilder sb = new StringBuilder(3 + String.valueOf(str).length() + String.valueOf(jSONObject2).length());
        sb.append(str);
        sb.append("(");
        sb.append(jSONObject2);
        sb.append(");");
        zzcw(sb.toString());
    }

    public final zzv zzbk() {
        return this.zzamw;
    }

    public final void zzc(zzd zzd) {
        synchronized (this.mLock) {
            this.zzdjz = zzd;
        }
    }

    public final void zzck() {
        synchronized (this.mLock) {
            this.zzdjo = true;
            if (this.zzdje != null) {
                this.zzdje.zzck();
            }
        }
    }

    public final void zzcl() {
        synchronized (this.mLock) {
            this.zzdjo = false;
            if (this.zzdje != null) {
                this.zzdje.zzcl();
            }
        }
    }

    public final void zzct(String str) {
        synchronized (this.mLock) {
            try {
                super.loadUrl(str);
            } catch (Exception | IncompatibleClassChangeError | NoClassDefFoundError | UnsatisfiedLinkError e) {
                zzbs.zzeg().zza(e, "AdWebViewImpl.loadUrlUnsafe");
                zzafj.zzc("Could not call loadUrl. ", e);
            }
        }
    }

    public final void zzcu(String str) {
        synchronized (this.mLock) {
            if (str == null) {
                str = "";
            }
            this.zzcqb = str;
        }
    }

    public final void zzmt() {
        if (this.zzdjv == null) {
            zzmw.zza(this.zzdjx.zziy(), this.zzdgr, "aes2");
            this.zzdjv = zzmw.zzb(this.zzdjx.zziy());
            this.zzdjx.zza("native:view_show", this.zzdjv);
        }
        HashMap hashMap = new HashMap(1);
        hashMap.put(ProviderConfig.MCVideoAdsVersionKey, this.zzaov.zzcp);
        zza("onshow", hashMap);
    }

    public final void zzmu() {
        zzd zzsm = zzsm();
        if (zzsm != null) {
            zzsm.zzmu();
        }
    }

    public final zzakz zzrw() {
        return null;
    }

    public final zzamr zzrx() {
        zzamr zzamr;
        synchronized (this.mLock) {
            zzamr = this.zzdjp;
        }
        return zzamr;
    }

    public final zznb zzry() {
        return this.zzdgr;
    }

    public final Activity zzrz() {
        return this.zzdjd.zzrz();
    }

    public final zznc zzsa() {
        return this.zzdjx;
    }

    public final zzaiy zzsb() {
        return this.zzaov;
    }

    public final int zzsc() {
        return getMeasuredHeight();
    }

    public final int zzsd() {
        return getMeasuredWidth();
    }

    public final void zzsj() {
        zztr();
        HashMap hashMap = new HashMap(1);
        hashMap.put(ProviderConfig.MCVideoAdsVersionKey, this.zzaov.zzcp);
        zza("onhide", hashMap);
    }

    public final void zzsk() {
        HashMap hashMap = new HashMap(3);
        zzbs.zzec();
        hashMap.put("app_muted", String.valueOf(zzagr.zzdi()));
        zzbs.zzec();
        hashMap.put("app_volume", String.valueOf(zzagr.zzdh()));
        zzbs.zzec();
        hashMap.put("device_volume", String.valueOf(zzagr.zzap(getContext())));
        zza("volume", hashMap);
    }

    public final Context zzsl() {
        return this.zzdjd.zzsl();
    }

    public final zzd zzsm() {
        zzd zzd;
        synchronized (this.mLock) {
            zzd = this.zzdjg;
        }
        return zzd;
    }

    public final zzd zzsn() {
        zzd zzd;
        synchronized (this.mLock) {
            zzd = this.zzdjz;
        }
        return zzd;
    }

    public final zzanp zzso() {
        zzanp zzanp;
        synchronized (this.mLock) {
            zzanp = this.zzdjh;
        }
        return zzanp;
    }

    public final String zzsp() {
        String str;
        synchronized (this.mLock) {
            str = this.zzaky;
        }
        return str;
    }

    public final zzamb zzsq() {
        return this.zzdjf;
    }

    public final boolean zzsr() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzdji;
        }
        return z;
    }

    public final zzcs zzss() {
        return this.zzbta;
    }

    public final boolean zzst() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzdjk;
        }
        return z;
    }

    public final void zzsu() {
        synchronized (this.mLock) {
            zzafj.v("Destroying WebView!");
            zzpo();
            zzagr.zzczc.post(new zzamq(this));
        }
    }

    public final boolean zzsv() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzdjn;
        }
        return z;
    }

    public final boolean zzsw() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzdjo;
        }
        return z;
    }

    public final boolean zzsx() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzdjt > 0;
        }
        return z;
    }

    public final void zzsy() {
        this.zzaun.zzqo();
    }

    public final void zzsz() {
        if (this.zzdjw == null) {
            this.zzdjw = zzmw.zzb(this.zzdjx.zziy());
            this.zzdjx.zza("native:view_load", this.zzdjw);
        }
    }

    public final zzny zzta() {
        zzny zzny;
        synchronized (this.mLock) {
            zzny = this.zzdjs;
        }
        return zzny;
    }

    public final void zztb() {
        setBackgroundColor(0);
    }

    public final void zztc() {
        zzafj.v("Cannot add text view to inner AdWebView");
    }
}
