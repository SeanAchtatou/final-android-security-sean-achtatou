package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;

final class zzx implements Result {
    private /* synthetic */ Status zzekv;

    zzx(zzw zzw, Status status) {
        this.zzekv = status;
    }

    public final Status getStatus() {
        return this.zzekv;
    }
}
