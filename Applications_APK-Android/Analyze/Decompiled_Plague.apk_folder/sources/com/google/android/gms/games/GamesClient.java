package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresPermission;
import android.view.View;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.internal.api.zzp;
import com.google.android.gms.tasks.Task;

public class GamesClient extends zzp {
    GamesClient(@NonNull Activity activity, @NonNull Games.GamesOptions gamesOptions) {
        super(activity, gamesOptions);
    }

    GamesClient(@NonNull Context context, @NonNull Games.GamesOptions gamesOptions) {
        super(context, gamesOptions);
    }

    public Task<Bundle> getActivationHint() {
        return zza(new zzs(this));
    }

    public Task<String> getAppId() {
        return zza(new zzq(this));
    }

    @RequiresPermission("android.permission.GET_ACCOUNTS")
    public Task<String> getCurrentAccountName() {
        return zza(new zzp(this));
    }

    @KeepForSdk
    public Task<Integer> getSdkVariant() {
        return zza(new zzt(this));
    }

    public Task<Intent> getSettingsIntent() {
        return zza(new zzr(this));
    }

    public Task<Void> setGravityForPopups(int i) {
        return zzb(new zzn(this, i));
    }

    public Task<Void> setViewForPopups(@NonNull View view) {
        return zzb(new zzo(this, view));
    }
}
