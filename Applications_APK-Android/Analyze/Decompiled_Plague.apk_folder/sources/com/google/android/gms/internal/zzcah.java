package com.google.android.gms.internal;

import java.util.HashMap;

public final class zzcah {
    private int zzfcq = 0;
    private HashMap<String, Integer> zzhti = new HashMap<>();

    public final zzcaf zzatq() {
        return new zzcaf(this.zzfcq, this.zzhti);
    }

    public final zzcah zzdn(int i) {
        this.zzfcq = i;
        return this;
    }

    public final zzcah zzw(String str, int i) {
        boolean z;
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
                z = true;
                break;
            default:
                z = false;
                break;
        }
        if (z) {
            this.zzhti.put(str, Integer.valueOf(i));
        }
        return this;
    }
}
