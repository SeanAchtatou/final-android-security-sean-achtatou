package com.google.android.gms.internal;

import android.content.Context;
import android.os.Binder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.zzbs;

@zzzb
public final class zzhm {
    @Nullable
    private Context mContext;
    /* access modifiers changed from: private */
    public final Object mLock = new Object();
    private final Runnable zzbab = new zzhn(this);
    /* access modifiers changed from: private */
    @Nullable
    public zzht zzbac;
    /* access modifiers changed from: private */
    @Nullable
    public zzhx zzbad;

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void connect() {
        /*
            r6 = this;
            java.lang.Object r0 = r6.mLock
            monitor-enter(r0)
            android.content.Context r1 = r6.mContext     // Catch:{ all -> 0x0030 }
            if (r1 == 0) goto L_0x002e
            com.google.android.gms.internal.zzht r1 = r6.zzbac     // Catch:{ all -> 0x0030 }
            if (r1 == 0) goto L_0x000c
            goto L_0x002e
        L_0x000c:
            com.google.android.gms.internal.zzhp r1 = new com.google.android.gms.internal.zzhp     // Catch:{ all -> 0x0030 }
            r1.<init>(r6)     // Catch:{ all -> 0x0030 }
            com.google.android.gms.internal.zzhq r2 = new com.google.android.gms.internal.zzhq     // Catch:{ all -> 0x0030 }
            r2.<init>(r6)     // Catch:{ all -> 0x0030 }
            com.google.android.gms.internal.zzht r3 = new com.google.android.gms.internal.zzht     // Catch:{ all -> 0x0030 }
            android.content.Context r4 = r6.mContext     // Catch:{ all -> 0x0030 }
            com.google.android.gms.internal.zzaii r5 = com.google.android.gms.ads.internal.zzbs.zzet()     // Catch:{ all -> 0x0030 }
            android.os.Looper r5 = r5.zzqm()     // Catch:{ all -> 0x0030 }
            r3.<init>(r4, r5, r1, r2)     // Catch:{ all -> 0x0030 }
            r6.zzbac = r3     // Catch:{ all -> 0x0030 }
            com.google.android.gms.internal.zzht r1 = r6.zzbac     // Catch:{ all -> 0x0030 }
            r1.zzajx()     // Catch:{ all -> 0x0030 }
            monitor-exit(r0)     // Catch:{ all -> 0x0030 }
            return
        L_0x002e:
            monitor-exit(r0)     // Catch:{ all -> 0x0030 }
            return
        L_0x0030:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0030 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzhm.connect():void");
    }

    /* access modifiers changed from: private */
    public final void disconnect() {
        synchronized (this.mLock) {
            if (this.zzbac != null) {
                if (this.zzbac.isConnected() || this.zzbac.isConnecting()) {
                    this.zzbac.disconnect();
                }
                this.zzbac = null;
                this.zzbad = null;
                Binder.flushPendingCommands();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0047, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void initialize(android.content.Context r3) {
        /*
            r2 = this;
            if (r3 != 0) goto L_0x0003
            return
        L_0x0003:
            java.lang.Object r0 = r2.mLock
            monitor-enter(r0)
            android.content.Context r1 = r2.mContext     // Catch:{ all -> 0x0048 }
            if (r1 == 0) goto L_0x000c
            monitor-exit(r0)     // Catch:{ all -> 0x0048 }
            return
        L_0x000c:
            android.content.Context r3 = r3.getApplicationContext()     // Catch:{ all -> 0x0048 }
            r2.mContext = r3     // Catch:{ all -> 0x0048 }
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r3 = com.google.android.gms.internal.zzmq.zzbom     // Catch:{ all -> 0x0048 }
            com.google.android.gms.internal.zzmo r1 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ all -> 0x0048 }
            java.lang.Object r3 = r1.zzd(r3)     // Catch:{ all -> 0x0048 }
            java.lang.Boolean r3 = (java.lang.Boolean) r3     // Catch:{ all -> 0x0048 }
            boolean r3 = r3.booleanValue()     // Catch:{ all -> 0x0048 }
            if (r3 == 0) goto L_0x0028
            r2.connect()     // Catch:{ all -> 0x0048 }
            goto L_0x0046
        L_0x0028:
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r3 = com.google.android.gms.internal.zzmq.zzbol     // Catch:{ all -> 0x0048 }
            com.google.android.gms.internal.zzmo r1 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ all -> 0x0048 }
            java.lang.Object r3 = r1.zzd(r3)     // Catch:{ all -> 0x0048 }
            java.lang.Boolean r3 = (java.lang.Boolean) r3     // Catch:{ all -> 0x0048 }
            boolean r3 = r3.booleanValue()     // Catch:{ all -> 0x0048 }
            if (r3 == 0) goto L_0x0046
            com.google.android.gms.internal.zzho r3 = new com.google.android.gms.internal.zzho     // Catch:{ all -> 0x0048 }
            r3.<init>(r2)     // Catch:{ all -> 0x0048 }
            com.google.android.gms.internal.zzgp r1 = com.google.android.gms.ads.internal.zzbs.zzef()     // Catch:{ all -> 0x0048 }
            r1.zza(r3)     // Catch:{ all -> 0x0048 }
        L_0x0046:
            monitor-exit(r0)     // Catch:{ all -> 0x0048 }
            return
        L_0x0048:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0048 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzhm.initialize(android.content.Context):void");
    }

    public final zzhr zza(zzhu zzhu) {
        synchronized (this.mLock) {
            if (this.zzbad == null) {
                zzhr zzhr = new zzhr();
                return zzhr;
            }
            try {
                zzhr zza = this.zzbad.zza(zzhu);
                return zza;
            } catch (RemoteException e) {
                zzafj.zzb("Unable to call into cache service.", e);
                return new zzhr();
            }
        }
    }

    public final void zzhb() {
        if (((Boolean) zzbs.zzep().zzd(zzmq.zzbon)).booleanValue()) {
            synchronized (this.mLock) {
                connect();
                zzbs.zzec();
                zzagr.zzczc.removeCallbacks(this.zzbab);
                zzbs.zzec();
                zzagr.zzczc.postDelayed(this.zzbab, ((Long) zzbs.zzep().zzd(zzmq.zzboo)).longValue());
            }
        }
    }
}
