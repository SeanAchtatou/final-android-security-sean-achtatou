package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.View;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzaaf;
import com.google.android.gms.internal.zzadw;
import com.google.android.gms.internal.zzaeu;
import com.google.android.gms.internal.zzaev;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzagr;
import com.google.android.gms.internal.zzaiy;
import com.google.android.gms.internal.zzama;
import com.google.android.gms.internal.zzib;
import com.google.android.gms.internal.zzis;
import com.google.android.gms.internal.zziw;
import com.google.android.gms.internal.zzku;
import com.google.android.gms.internal.zznj;
import com.google.android.gms.internal.zzns;
import com.google.android.gms.internal.zznu;
import com.google.android.gms.internal.zzny;
import com.google.android.gms.internal.zznz;
import com.google.android.gms.internal.zzoa;
import com.google.android.gms.internal.zzob;
import com.google.android.gms.internal.zzoy;
import com.google.android.gms.internal.zzpu;
import com.google.android.gms.internal.zzqe;
import com.google.android.gms.internal.zztn;
import com.google.android.gms.internal.zzuc;
import com.google.android.gms.internal.zzuo;
import com.google.android.gms.internal.zzur;
import com.google.android.gms.internal.zzzb;
import java.util.List;
import org.json.JSONObject;

@zzzb
public final class zzq extends zzd implements zzob {
    private boolean zzals;
    /* access modifiers changed from: private */
    public zzaeu zzank;
    private boolean zzanl = false;

    public zzq(Context context, zzv zzv, zziw zziw, String str, zzuc zzuc, zzaiy zzaiy) {
        super(context, zziw, str, zzuc, zzaiy, zzv);
    }

    private static zzaeu zza(zzaev zzaev, int i) {
        zzaev zzaev2 = zzaev;
        zzis zzis = zzaev2.zzcpe.zzclo;
        List<String> list = zzaev2.zzcwe.zzcbv;
        List<String> list2 = zzaev2.zzcwe.zzcbw;
        List<String> list3 = zzaev2.zzcwe.zzcni;
        int i2 = zzaev2.zzcwe.orientation;
        long j = zzaev2.zzcwe.zzccb;
        String str = zzaev2.zzcpe.zzclr;
        boolean z = zzaev2.zzcwe.zzcng;
        zztn zztn = zzaev2.zzcvs;
        long j2 = zzaev2.zzcwe.zzcnh;
        zziw zziw = zzaev2.zzath;
        long j3 = j2;
        zztn zztn2 = zztn;
        long j4 = zzaev2.zzcwe.zzcnf;
        long j5 = zzaev2.zzcvw;
        long j6 = zzaev2.zzcvx;
        String str2 = zzaev2.zzcwe.zzcnl;
        JSONObject jSONObject = zzaev2.zzcvq;
        zzadw zzadw = zzaev2.zzcwe.zzcnv;
        List<String> list4 = zzaev2.zzcwe.zzcnw;
        List<String> list5 = zzaev2.zzcwe.zzcnw;
        boolean z2 = zzaev2.zzcwe.zzcny;
        zzaaf zzaaf = zzaev2.zzcwe.zzcnz;
        List<String> list6 = zzaev2.zzcwe.zzcby;
        String str3 = zzaev2.zzcwe.zzcoc;
        long j7 = j6;
        zzib zzib = zzaev2.zzcwc;
        String str4 = str3;
        zzadw zzadw2 = zzadw;
        List<String> list7 = list4;
        List<String> list8 = list5;
        boolean z3 = z2;
        zzaaf zzaaf2 = zzaaf;
        List<String> list9 = list6;
        JSONObject jSONObject2 = jSONObject;
        int i3 = i;
        String str5 = str2;
        boolean z4 = zzaev2.zzcwe.zzapy;
        return new zzaeu(zzis, null, list, i3, list2, list3, i2, j, str, z, null, null, null, zztn2, null, j3, zziw, j4, j5, j7, str5, jSONObject2, null, zzadw2, list7, list8, z3, zzaaf2, null, list9, str4, zzib, z4, zzaev2.zzcwd);
    }

    private final boolean zzb(zzaeu zzaeu, zzaeu zzaeu2) {
        Handler handler;
        Runnable zzt;
        zzaeu zzaeu3 = zzaeu2;
        View view = null;
        zzc((List<String>) null);
        if (!this.zzamt.zzfg()) {
            zzafj.zzco("Native ad does not have custom rendering mode.");
        } else {
            try {
                zzuo zzly = zzaeu3.zzcde != null ? zzaeu3.zzcde.zzly() : null;
                zzur zzlz = zzaeu3.zzcde != null ? zzaeu3.zzcde.zzlz() : null;
                zzpu zzmd = zzaeu3.zzcde != null ? zzaeu3.zzcde.zzmd() : null;
                String zzc = zzc(zzaeu2);
                if (zzly != null && this.zzamt.zzatp != null) {
                    String headline = zzly.getHeadline();
                    List images = zzly.getImages();
                    String body = zzly.getBody();
                    zzoy zzjm = zzly.zzjm() != null ? zzly.zzjm() : null;
                    String callToAction = zzly.getCallToAction();
                    double starRating = zzly.getStarRating();
                    String store = zzly.getStore();
                    String price = zzly.getPrice();
                    Bundle extras = zzly.getExtras();
                    zzku videoController = zzly.getVideoController();
                    if (zzly.zzmf() != null) {
                        view = (View) zzn.zzx(zzly.zzmf());
                    }
                    zzns zzns = new zzns(headline, images, body, zzjm, callToAction, starRating, store, price, null, extras, videoController, view, zzly.zzjr(), zzc);
                    zzns zzns2 = zzns;
                    zzns2.zzb(new zznz(this.zzamt.zzaif, this, this.zzamt.zzatc, zzly, zzns));
                    handler = zzagr.zzczc;
                    zzt = new zzs(this, zzns2);
                } else if (zzlz != null && this.zzamt.zzatq != null) {
                    String headline2 = zzlz.getHeadline();
                    List images2 = zzlz.getImages();
                    String body2 = zzlz.getBody();
                    zzoy zzjt = zzlz.zzjt() != null ? zzlz.zzjt() : null;
                    String callToAction2 = zzlz.getCallToAction();
                    String advertiser = zzlz.getAdvertiser();
                    Bundle extras2 = zzlz.getExtras();
                    zzku videoController2 = zzlz.getVideoController();
                    if (zzlz.zzmf() != null) {
                        view = (View) zzn.zzx(zzlz.zzmf());
                    }
                    zznu zznu = new zznu(headline2, images2, body2, zzjt, callToAction2, advertiser, null, extras2, videoController2, view, zzlz.zzjr(), zzc);
                    zznu.zzb(new zznz(this.zzamt.zzaif, this, this.zzamt.zzatc, zzlz, zznu));
                    handler = zzagr.zzczc;
                    zzt = new zzt(this, zznu);
                } else if (zzmd == null || this.zzamt.zzats == null || this.zzamt.zzats.get(zzmd.getCustomTemplateId()) == null) {
                    zzafj.zzco("No matching mapper/listener for retrieved native ad template.");
                    zzi(0);
                    return false;
                } else {
                    zzagr.zzczc.post(new zzu(this, zzmd));
                    return super.zza(zzaeu, zzaeu2);
                }
                handler.post(zzt);
                return super.zza(zzaeu, zzaeu2);
            } catch (RemoteException e) {
                zzafj.zzc("Failed to get native ad mapper", e);
            }
        }
        zzi(0);
        return false;
    }

    private final boolean zzc(zzaeu zzaeu, zzaeu zzaeu2) {
        View zze = zzaq.zze(zzaeu2);
        if (zze == null) {
            return false;
        }
        View nextView = this.zzamt.zzate.getNextView();
        if (nextView != null) {
            if (nextView instanceof zzama) {
                ((zzama) nextView).destroy();
            }
            this.zzamt.zzate.removeView(nextView);
        }
        if (!zzaq.zzf(zzaeu2)) {
            try {
                zzb(zze);
            } catch (Throwable th) {
                zzbs.zzeg().zza(th, "AdLoaderManager.swapBannerViews");
                zzafj.zzc("Could not add mediation view to view hierarchy.", th);
                return false;
            }
        }
        if (this.zzamt.zzate.getChildCount() > 1) {
            this.zzamt.zzate.showNext();
        }
        if (zzaeu != null) {
            View nextView2 = this.zzamt.zzate.getNextView();
            if (nextView2 != null) {
                this.zzamt.zzate.removeView(nextView2);
            }
            this.zzamt.zzff();
        }
        this.zzamt.zzate.setMinimumWidth(zzbm().widthPixels);
        this.zzamt.zzate.setMinimumHeight(zzbm().heightPixels);
        this.zzamt.zzate.requestLayout();
        this.zzamt.zzate.setVisibility(0);
        return true;
    }

    @Nullable
    private final zztn zzcs() {
        if (this.zzamt.zzati == null || !this.zzamt.zzati.zzcng) {
            return null;
        }
        return this.zzamt.zzati.zzcvs;
    }

    @Nullable
    public final zzku getVideoController() {
        return null;
    }

    public final void pause() {
        if (!this.zzanl) {
            throw new IllegalStateException("Native Ad does not support pause().");
        }
        super.pause();
    }

    public final void resume() {
        if (!this.zzanl) {
            throw new IllegalStateException("Native Ad does not support resume().");
        }
        super.resume();
    }

    public final void setManualImpressionsEnabled(boolean z) {
        zzbq.zzfz("setManualImpressionsEnabled must be called from the main thread.");
        this.zzals = z;
    }

    public final void showInterstitial() {
        throw new IllegalStateException("Interstitial is not supported by AdLoaderManager.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(com.google.android.gms.internal.zzaev r11, com.google.android.gms.internal.zznd r12) {
        /*
            r10 = this;
            r0 = 0
            r10.zzank = r0
            int r0 = r11.errorCode
            r1 = 0
            r2 = -2
            if (r0 == r2) goto L_0x0012
            int r0 = r11.errorCode
            com.google.android.gms.internal.zzaeu r0 = zza(r11, r0)
        L_0x000f:
            r10.zzank = r0
            goto L_0x0022
        L_0x0012:
            com.google.android.gms.internal.zzaad r0 = r11.zzcwe
            boolean r0 = r0.zzcng
            if (r0 != 0) goto L_0x0022
            java.lang.String r0 = "partialAdState is not mediation"
            com.google.android.gms.internal.zzafj.zzco(r0)
            com.google.android.gms.internal.zzaeu r0 = zza(r11, r1)
            goto L_0x000f
        L_0x0022:
            com.google.android.gms.internal.zzaeu r0 = r10.zzank
            if (r0 == 0) goto L_0x0031
            android.os.Handler r11 = com.google.android.gms.internal.zzagr.zzczc
            com.google.android.gms.ads.internal.zzr r12 = new com.google.android.gms.ads.internal.zzr
            r12.<init>(r10)
            r11.post(r12)
            return
        L_0x0031:
            com.google.android.gms.internal.zziw r0 = r11.zzath
            if (r0 == 0) goto L_0x003b
            com.google.android.gms.ads.internal.zzbt r0 = r10.zzamt
            com.google.android.gms.internal.zziw r2 = r11.zzath
            r0.zzath = r2
        L_0x003b:
            com.google.android.gms.ads.internal.zzbt r0 = r10.zzamt
            r0.zzaue = r1
            com.google.android.gms.ads.internal.zzbt r0 = r10.zzamt
            com.google.android.gms.ads.internal.zzbs.zzeb()
            com.google.android.gms.ads.internal.zzbt r1 = r10.zzamt
            android.content.Context r2 = r1.zzaif
            com.google.android.gms.ads.internal.zzbt r1 = r10.zzamt
            com.google.android.gms.internal.zzcs r5 = r1.zzatc
            r6 = 0
            com.google.android.gms.internal.zzuc r7 = r10.zzanb
            r3 = r10
            r4 = r11
            r8 = r10
            r9 = r12
            com.google.android.gms.internal.zzahi r11 = com.google.android.gms.internal.zzxf.zza(r2, r3, r4, r5, r6, r7, r8, r9)
            r0.zzatg = r11
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.zzq.zza(com.google.android.gms.internal.zzaev, com.google.android.gms.internal.zznd):void");
    }

    public final void zza(zznj zznj) {
        throw new IllegalStateException("CustomRendering is not supported by AdLoaderManager.");
    }

    public final void zza(zzny zzny) {
        zzafj.zzco("Unexpected call to AdLoaderManager method");
    }

    public final void zza(zzoa zzoa) {
        zzafj.zzco("Unexpected call to AdLoaderManager method");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, boolean):void
     arg types: [com.google.android.gms.internal.zzaeu, int]
     candidates:
      com.google.android.gms.ads.internal.zzq.zza(com.google.android.gms.internal.zzaev, int):com.google.android.gms.internal.zzaeu
      com.google.android.gms.ads.internal.zzq.zza(com.google.android.gms.internal.zzaev, com.google.android.gms.internal.zznd):void
      com.google.android.gms.ads.internal.zzq.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzpu, java.lang.String):void
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaaa, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzis, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzaev, com.google.android.gms.internal.zznd):void
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzww, java.lang.String):void
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean
      com.google.android.gms.ads.internal.zza.zza(com.google.android.gms.internal.zzis, com.google.android.gms.internal.zznd):boolean
      com.google.android.gms.internal.zzkb.zza(com.google.android.gms.internal.zzww, java.lang.String):void
      com.google.android.gms.internal.zzto.zza(com.google.android.gms.internal.zzpu, java.lang.String):void
      com.google.android.gms.ads.internal.zzd.zza(com.google.android.gms.internal.zzaeu, boolean):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006e A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x006f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(@android.support.annotation.Nullable com.google.android.gms.internal.zzaeu r5, com.google.android.gms.internal.zzaeu r6) {
        /*
            r4 = this;
            com.google.android.gms.ads.internal.zzbt r0 = r4.zzamt
            boolean r0 = r0.zzfg()
            if (r0 != 0) goto L_0x0010
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "AdLoader API does not support custom rendering."
            r5.<init>(r6)
            throw r5
        L_0x0010:
            boolean r0 = r6.zzcng
            r1 = 0
            if (r0 != 0) goto L_0x001e
            r4.zzi(r1)
            java.lang.String r5 = "newState is not mediation."
        L_0x001a:
            com.google.android.gms.internal.zzafj.zzco(r5)
            return r1
        L_0x001e:
            com.google.android.gms.internal.zztm r0 = r6.zzcdd
            r2 = 1
            if (r0 == 0) goto L_0x0072
            com.google.android.gms.internal.zztm r0 = r6.zzcdd
            boolean r0 = r0.zzlp()
            if (r0 == 0) goto L_0x0072
            com.google.android.gms.ads.internal.zzbt r0 = r4.zzamt
            boolean r0 = r0.zzfg()
            if (r0 == 0) goto L_0x0046
            com.google.android.gms.ads.internal.zzbt r0 = r4.zzamt
            com.google.android.gms.ads.internal.zzbu r0 = r0.zzate
            if (r0 == 0) goto L_0x0046
            com.google.android.gms.ads.internal.zzbt r0 = r4.zzamt
            com.google.android.gms.ads.internal.zzbu r0 = r0.zzate
            com.google.android.gms.internal.zzahj r0 = r0.zzfj()
            java.lang.String r3 = r6.zzcnl
            r0.zzcj(r3)
        L_0x0046:
            boolean r0 = super.zza(r5, r6)
            if (r0 != 0) goto L_0x004e
        L_0x004c:
            r5 = r1
            goto L_0x006c
        L_0x004e:
            com.google.android.gms.ads.internal.zzbt r0 = r4.zzamt
            boolean r0 = r0.zzfg()
            if (r0 == 0) goto L_0x0060
            boolean r5 = r4.zzc(r5, r6)
            if (r5 != 0) goto L_0x0060
            r4.zzi(r1)
            goto L_0x004c
        L_0x0060:
            com.google.android.gms.ads.internal.zzbt r5 = r4.zzamt
            boolean r5 = r5.zzfh()
            if (r5 != 0) goto L_0x006b
            super.zza(r6, r1)
        L_0x006b:
            r5 = r2
        L_0x006c:
            if (r5 != 0) goto L_0x006f
            return r1
        L_0x006f:
            r4.zzanl = r2
            goto L_0x0085
        L_0x0072:
            com.google.android.gms.internal.zztm r0 = r6.zzcdd
            if (r0 == 0) goto L_0x009b
            com.google.android.gms.internal.zztm r0 = r6.zzcdd
            boolean r0 = r0.zzlq()
            if (r0 == 0) goto L_0x009b
            boolean r5 = r4.zzb(r5, r6)
            if (r5 != 0) goto L_0x0085
            return r1
        L_0x0085:
            java.util.ArrayList r5 = new java.util.ArrayList
            java.lang.Integer[] r6 = new java.lang.Integer[r2]
            r0 = 2
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r6[r1] = r0
            java.util.List r6 = java.util.Arrays.asList(r6)
            r5.<init>(r6)
            r4.zzd(r5)
            return r2
        L_0x009b:
            r4.zzi(r1)
            java.lang.String r5 = "Response is neither banner nor native."
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.zzq.zza(com.google.android.gms.internal.zzaeu, com.google.android.gms.internal.zzaeu):boolean");
    }

    /* access modifiers changed from: protected */
    public final boolean zza(zzis zzis, zzaeu zzaeu, boolean z) {
        return false;
    }

    public final boolean zzb(zzis zzis) {
        zzq zzq = this;
        zzis zzis2 = zzis;
        if (zzq.zzamt.zzatx != null && zzq.zzamt.zzatx.size() == 1 && zzq.zzamt.zzatx.get(0).intValue() == 2) {
            zzafj.e("Requesting only banner Ad from AdLoader or calling loadAd on returned banner is not yet supported");
            zzq.zzi(0);
            return false;
        } else if (zzq.zzamt.zzatw == null) {
            return super.zzb(zzis);
        } else {
            if (zzis2.zzbca != zzq.zzals) {
                int i = zzis2.versionCode;
                long j = zzis2.zzbbv;
                Bundle bundle = zzis2.extras;
                int i2 = zzis2.zzbbw;
                List<String> list = zzis2.zzbbx;
                boolean z = zzis2.zzbby;
                int i3 = zzis2.zzbbz;
                boolean z2 = zzis2.zzbca || zzq.zzals;
                zzis2 = new zzis(i, j, bundle, i2, list, z, i3, z2, zzis2.zzbcb, zzis2.zzbcc, zzis2.zzbcd, zzis2.zzbce, zzis2.zzbcf, zzis2.zzbcg, zzis2.zzbch, zzis2.zzbci, zzis2.zzbcj, zzis2.zzbck);
                zzq = this;
            }
            return super.zzb(zzis2);
        }
    }

    /* access modifiers changed from: protected */
    public final void zzbs() {
        super.zzbs();
        zzaeu zzaeu = this.zzamt.zzati;
        if (zzaeu != null && zzaeu.zzcdd != null && zzaeu.zzcdd.zzlp() && this.zzamt.zzatw != null) {
            try {
                this.zzamt.zzatw.zza(this, zzn.zzy(this.zzamt.zzaif));
            } catch (RemoteException e) {
                zzafj.zzc("Could not call PublisherAdViewLoadedListener.onPublisherAdViewLoaded().", e);
            }
        }
    }

    public final void zzc(@Nullable List<String> list) {
        zzbq.zzfz("setNativeTemplates must be called on the main UI thread.");
        this.zzamt.zzaub = list;
    }

    public final void zzcd() {
        if (this.zzamt.zzati == null || !"com.google.ads.mediation.admob.AdMobAdapter".equals(this.zzamt.zzati.zzcdf) || this.zzamt.zzati.zzcdd == null || !this.zzamt.zzati.zzcdd.zzlq()) {
            super.zzcd();
        } else {
            zzbu();
        }
    }

    public final void zzci() {
        if (this.zzamt.zzati == null || !"com.google.ads.mediation.admob.AdMobAdapter".equals(this.zzamt.zzati.zzcdf) || this.zzamt.zzati.zzcdd == null || !this.zzamt.zzati.zzcdd.zzlq()) {
            super.zzci();
        } else {
            zzbt();
        }
    }

    public final void zzcp() {
        zzafj.zzco("Unexpected call to AdLoaderManager method");
    }

    public final boolean zzcq() {
        if (zzcs() != null) {
            return zzcs().zzcch;
        }
        return false;
    }

    public final boolean zzcr() {
        if (zzcs() != null) {
            return zzcs().zzcci;
        }
        return false;
    }

    public final void zzd(List<Integer> list) {
        zzbq.zzfz("setAllowedAdTypes must be called on the main UI thread.");
        this.zzamt.zzatx = list;
    }

    @Nullable
    public final zzqe zzr(String str) {
        zzbq.zzfz("getOnCustomClickListener must be called on the main UI thread.");
        return this.zzamt.zzatr.get(str);
    }
}
