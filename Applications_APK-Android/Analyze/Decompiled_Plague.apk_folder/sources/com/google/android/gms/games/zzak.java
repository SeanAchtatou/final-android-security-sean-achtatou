package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzak extends zzac<Void> {
    private /* synthetic */ String zzhis;
    private /* synthetic */ long zzhiv;
    private /* synthetic */ String zzhiw;

    zzak(LeaderboardsClient leaderboardsClient, String str, long j, String str2) {
        this.zzhis = str;
        this.zzhiv = j;
        this.zzhiw = str2;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException {
        gamesClientImpl.zza((zzn<Leaderboards.SubmitScoreResult>) null, this.zzhis, this.zzhiv, this.zzhiw);
    }
}
