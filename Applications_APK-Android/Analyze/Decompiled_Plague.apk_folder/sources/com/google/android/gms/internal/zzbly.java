package com.google.android.gms.internal;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

final class zzbly implements ResultCallback<Status> {
    zzbly(zzblv zzblv) {
    }

    public final /* synthetic */ void onResult(Result result) {
        if (!((Status) result).isSuccess()) {
            zzblv.zzggp.zzw("DriveContentsImpl", "Error discarding contents");
        } else {
            zzblv.zzggp.zzu("DriveContentsImpl", "Contents discarded");
        }
    }
}
