package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.quest.Milestone;
import com.google.android.gms.games.quest.Quest;
import com.google.android.gms.games.quest.Quests;

final class zzbr implements Quests.ClaimMilestoneResult {
    private /* synthetic */ Status zzekv;

    zzbr(zzbq zzbq, Status status) {
        this.zzekv = status;
    }

    public final Milestone getMilestone() {
        return null;
    }

    public final Quest getQuest() {
        return null;
    }

    public final Status getStatus() {
        return this.zzekv;
    }
}
