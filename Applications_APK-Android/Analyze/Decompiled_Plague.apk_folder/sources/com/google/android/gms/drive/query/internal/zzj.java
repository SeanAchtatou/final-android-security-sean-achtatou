package com.google.android.gms.drive.query.internal;

import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.zzb;
import java.util.List;

public interface zzj<F> {
    <T> F zza(zzb zzb, Object obj);

    <T> F zza(zzx zzx, MetadataField<T> metadataField, T t);

    F zza(zzx zzx, List list);

    F zzapi();

    F zzapj();

    <T> F zzd(MetadataField<T> metadataField, T t);

    F zze(MetadataField<?> metadataField);

    F zzgv(String str);

    F zzx(F f);
}
