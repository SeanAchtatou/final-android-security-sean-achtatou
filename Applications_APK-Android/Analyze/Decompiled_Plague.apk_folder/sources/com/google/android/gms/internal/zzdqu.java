package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdqs;
import java.io.IOException;

public final class zzdqu extends zzfee<zzdqu, zza> implements zzffk {
    private static volatile zzffm<zzdqu> zzbas;
    /* access modifiers changed from: private */
    public static final zzdqu zzlrm;
    private int zzlqb;
    private zzdqs zzlrj;
    private zzfdh zzlrk = zzfdh.zzpal;
    private zzfdh zzlrl = zzfdh.zzpal;

    public static final class zza extends zzfef<zzdqu, zza> implements zzffk {
        private zza() {
            super(zzdqu.zzlrm);
        }

        /* synthetic */ zza(zzdqv zzdqv) {
            this();
        }
    }

    static {
        zzdqu zzdqu = new zzdqu();
        zzlrm = zzdqu;
        zzdqu.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdqu.zzpbs.zzbim();
    }

    private zzdqu() {
    }

    public static zzdqu zzv(zzfdh zzfdh) throws zzfew {
        return (zzdqu) zzfee.zza(zzlrm, zzfdh);
    }

    public final int getVersion() {
        return this.zzlqb;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdqs.zza zza2;
        boolean z = true;
        boolean z2 = false;
        switch (zzdqv.zzbaq[i - 1]) {
            case 1:
                return new zzdqu();
            case 2:
                return zzlrm;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdqu zzdqu = (zzdqu) obj2;
                this.zzlqb = zzfen.zza(this.zzlqb != 0, this.zzlqb, zzdqu.zzlqb != 0, zzdqu.zzlqb);
                this.zzlrj = (zzdqs) zzfen.zza(this.zzlrj, zzdqu.zzlrj);
                this.zzlrk = zzfen.zza(this.zzlrk != zzfdh.zzpal, this.zzlrk, zzdqu.zzlrk != zzfdh.zzpal, zzdqu.zzlrk);
                boolean z3 = this.zzlrl != zzfdh.zzpal;
                zzfdh zzfdh = this.zzlrl;
                if (zzdqu.zzlrl == zzfdh.zzpal) {
                    z = false;
                }
                this.zzlrl = zzfen.zza(z3, zzfdh, z, zzdqu.zzlrl);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzlqb = zzfdq.zzcub();
                                } else if (zzcts == 18) {
                                    if (this.zzlrj != null) {
                                        zzdqs zzdqs = this.zzlrj;
                                        zzfef zzfef = (zzfef) zzdqs.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdqs);
                                        zza2 = (zzdqs.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlrj = (zzdqs) zzfdq.zza(zzdqs.zzbmw(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlrj);
                                        this.zzlrj = (zzdqs) zza2.zzcvj();
                                    }
                                } else if (zzcts == 26) {
                                    this.zzlrk = zzfdq.zzcua();
                                } else if (zzcts == 34) {
                                    this.zzlrl = zzfdq.zzcua();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdqu.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlrm);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlrm;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlqb != 0) {
            zzfdv.zzab(1, this.zzlqb);
        }
        if (this.zzlrj != null) {
            zzfdv.zza(2, this.zzlrj == null ? zzdqs.zzbmw() : this.zzlrj);
        }
        if (!this.zzlrk.isEmpty()) {
            zzfdv.zza(3, this.zzlrk);
        }
        if (!this.zzlrl.isEmpty()) {
            zzfdv.zza(4, this.zzlrl);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzdqs zzbmy() {
        return this.zzlrj == null ? zzdqs.zzbmw() : this.zzlrj;
    }

    public final zzfdh zzbmz() {
        return this.zzlrk;
    }

    public final zzfdh zzbna() {
        return this.zzlrl;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlqb != 0) {
            i2 = 0 + zzfdv.zzae(1, this.zzlqb);
        }
        if (this.zzlrj != null) {
            i2 += zzfdv.zzb(2, this.zzlrj == null ? zzdqs.zzbmw() : this.zzlrj);
        }
        if (!this.zzlrk.isEmpty()) {
            i2 += zzfdv.zzb(3, this.zzlrk);
        }
        if (!this.zzlrl.isEmpty()) {
            i2 += zzfdv.zzb(4, this.zzlrl);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
