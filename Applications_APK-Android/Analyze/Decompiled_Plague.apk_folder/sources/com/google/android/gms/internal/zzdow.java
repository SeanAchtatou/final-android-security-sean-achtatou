package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrr;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class zzdow<P> {
    private static final Charset UTF_8 = Charset.forName("UTF-8");
    private ConcurrentMap<String, List<zzdox<P>>> zzlph = new ConcurrentHashMap();
    private zzdox<P> zzlpi;

    /* access modifiers changed from: protected */
    public final zzdox<P> zza(P p, zzdrr.zzb zzb) throws GeneralSecurityException {
        byte[] bArr;
        byte b;
        ByteBuffer byteBuffer;
        switch (zzdoq.zzlpe[zzb.zzboi().ordinal()]) {
            case 1:
            case 2:
                byteBuffer = ByteBuffer.allocate(5);
                b = 0;
                bArr = byteBuffer.put(b).putInt(zzb.zzboh()).array();
                break;
            case 3:
                byteBuffer = ByteBuffer.allocate(5);
                b = 1;
                bArr = byteBuffer.put(b).putInt(zzb.zzboh()).array();
                break;
            case 4:
                bArr = zzdop.zzlpd;
                break;
            default:
                throw new GeneralSecurityException("unknown output prefix type");
        }
        zzdox<P> zzdox = new zzdox<>(p, bArr, zzb.zzbog(), zzb.zzboi());
        ArrayList arrayList = new ArrayList();
        arrayList.add(zzdox);
        String str = new String(zzdox.zzblh(), UTF_8);
        List put = this.zzlph.put(str, Collections.unmodifiableList(arrayList));
        if (put != null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.addAll(put);
            arrayList2.add(zzdox);
            this.zzlph.put(str, Collections.unmodifiableList(arrayList2));
        }
        return zzdox;
    }

    /* access modifiers changed from: protected */
    public final void zza(zzdox<P> zzdox) {
        this.zzlpi = zzdox;
    }

    public final zzdox<P> zzblf() {
        return this.zzlpi;
    }
}
