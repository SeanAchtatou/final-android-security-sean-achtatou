package com.google.android.gms.games;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.games.TurnBasedMultiplayerClient;
import com.google.android.gms.games.internal.zzn;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;

final class zzcu implements zzn<TurnBasedMatch> {
    zzcu() {
    }

    public final /* synthetic */ ApiException zza(@NonNull Status status, @NonNull Object obj) {
        return status.getStatusCode() == 26593 ? new TurnBasedMultiplayerClient.MatchOutOfDateApiException(status, (TurnBasedMatch) obj) : zzb.zzy(status);
    }
}
