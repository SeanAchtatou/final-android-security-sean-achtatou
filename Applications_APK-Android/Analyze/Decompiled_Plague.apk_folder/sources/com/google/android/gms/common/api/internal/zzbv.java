package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.zzp;

final class zzbv implements zzp {
    final /* synthetic */ zzbr zzfqx;

    zzbv(zzbr zzbr) {
        this.zzfqx = zzbr;
    }

    public final void zzait() {
        this.zzfqx.zzfqo.mHandler.post(new zzbw(this));
    }
}
