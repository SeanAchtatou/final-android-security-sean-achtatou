package com.google.android.gms.internal;

final class zzfgd implements zzfge {
    private /* synthetic */ zzfdh zzpeg;

    zzfgd(zzfdh zzfdh) {
        this.zzpeg = zzfdh;
    }

    public final int size() {
        return this.zzpeg.size();
    }

    public final byte zzkd(int i) {
        return this.zzpeg.zzkd(i);
    }
}
