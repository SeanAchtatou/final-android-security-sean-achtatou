package com.google.android.gms.tasks;

import android.app.Activity;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import com.google.android.gms.common.api.internal.zzci;
import com.google.android.gms.common.internal.zzbq;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

final class zzn<TResult> extends Task<TResult> {
    private final Object mLock = new Object();
    private final zzl<TResult> zzkru = new zzl<>();
    private boolean zzkrv;
    private TResult zzkrw;
    private Exception zzkrx;

    static class zza extends LifecycleCallback {
        private final List<WeakReference<zzk<?>>> zzewl = new ArrayList();

        private zza(zzci zzci) {
            super(zzci);
            this.zzfrj.zza("TaskOnStopCallback", this);
        }

        public static zza zzr(Activity activity) {
            zzci zzn = zzn(activity);
            zza zza = (zza) zzn.zza("TaskOnStopCallback", zza.class);
            return zza == null ? new zza(zzn) : zza;
        }

        @MainThread
        public final void onStop() {
            synchronized (this.zzewl) {
                for (WeakReference<zzk<?>> weakReference : this.zzewl) {
                    zzk zzk = (zzk) weakReference.get();
                    if (zzk != null) {
                        zzk.cancel();
                    }
                }
                this.zzewl.clear();
            }
        }

        public final <T> void zzb(zzk<T> zzk) {
            synchronized (this.zzewl) {
                this.zzewl.add(new WeakReference(zzk));
            }
        }
    }

    zzn() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    private final void zzbiy() {
        zzbq.zza(this.zzkrv, (Object) "Task is not yet complete");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    private final void zzbiz() {
        zzbq.zza(!this.zzkrv, (Object) "Task is already complete");
    }

    private final void zzbja() {
        synchronized (this.mLock) {
            if (this.zzkrv) {
                this.zzkru.zzb(this);
            }
        }
    }

    @NonNull
    public final Task<TResult> addOnCompleteListener(@NonNull Activity activity, @NonNull OnCompleteListener<TResult> onCompleteListener) {
        zze zze = new zze(TaskExecutors.MAIN_THREAD, onCompleteListener);
        this.zzkru.zza(zze);
        zza.zzr(activity).zzb(zze);
        zzbja();
        return this;
    }

    @NonNull
    public final Task<TResult> addOnCompleteListener(@NonNull OnCompleteListener<TResult> onCompleteListener) {
        return addOnCompleteListener(TaskExecutors.MAIN_THREAD, onCompleteListener);
    }

    @NonNull
    public final Task<TResult> addOnCompleteListener(@NonNull Executor executor, @NonNull OnCompleteListener<TResult> onCompleteListener) {
        this.zzkru.zza(new zze(executor, onCompleteListener));
        zzbja();
        return this;
    }

    @NonNull
    public final Task<TResult> addOnFailureListener(@NonNull Activity activity, @NonNull OnFailureListener onFailureListener) {
        zzg zzg = new zzg(TaskExecutors.MAIN_THREAD, onFailureListener);
        this.zzkru.zza(zzg);
        zza.zzr(activity).zzb(zzg);
        zzbja();
        return this;
    }

    @NonNull
    public final Task<TResult> addOnFailureListener(@NonNull OnFailureListener onFailureListener) {
        return addOnFailureListener(TaskExecutors.MAIN_THREAD, onFailureListener);
    }

    @NonNull
    public final Task<TResult> addOnFailureListener(@NonNull Executor executor, @NonNull OnFailureListener onFailureListener) {
        this.zzkru.zza(new zzg(executor, onFailureListener));
        zzbja();
        return this;
    }

    @NonNull
    public final Task<TResult> addOnSuccessListener(@NonNull Activity activity, @NonNull OnSuccessListener<? super TResult> onSuccessListener) {
        zzi zzi = new zzi(TaskExecutors.MAIN_THREAD, onSuccessListener);
        this.zzkru.zza(zzi);
        zza.zzr(activity).zzb(zzi);
        zzbja();
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.tasks.Task.addOnSuccessListener(java.util.concurrent.Executor, com.google.android.gms.tasks.OnSuccessListener<? super ?>):com.google.android.gms.tasks.Task<TResult>
     arg types: [java.util.concurrent.Executor, com.google.android.gms.tasks.OnSuccessListener<? super TResult>]
     candidates:
      com.google.android.gms.tasks.zzn.addOnSuccessListener(android.app.Activity, com.google.android.gms.tasks.OnSuccessListener):com.google.android.gms.tasks.Task<TResult>
      com.google.android.gms.tasks.Task.addOnSuccessListener(android.app.Activity, com.google.android.gms.tasks.OnSuccessListener<? super ?>):com.google.android.gms.tasks.Task<TResult>
      com.google.android.gms.tasks.Task.addOnSuccessListener(java.util.concurrent.Executor, com.google.android.gms.tasks.OnSuccessListener<? super ?>):com.google.android.gms.tasks.Task<TResult> */
    @NonNull
    public final Task<TResult> addOnSuccessListener(@NonNull OnSuccessListener<? super TResult> onSuccessListener) {
        return addOnSuccessListener(TaskExecutors.MAIN_THREAD, (OnSuccessListener<? super Object>) onSuccessListener);
    }

    @NonNull
    public final Task<TResult> addOnSuccessListener(@NonNull Executor executor, @NonNull OnSuccessListener<? super TResult> onSuccessListener) {
        this.zzkru.zza(new zzi(executor, onSuccessListener));
        zzbja();
        return this;
    }

    @NonNull
    public final <TContinuationResult> Task<TContinuationResult> continueWith(@NonNull Continuation<TResult, TContinuationResult> continuation) {
        return continueWith(TaskExecutors.MAIN_THREAD, continuation);
    }

    @NonNull
    public final <TContinuationResult> Task<TContinuationResult> continueWith(@NonNull Executor executor, @NonNull Continuation<TResult, TContinuationResult> continuation) {
        zzn zzn = new zzn();
        this.zzkru.zza(new zza(executor, continuation, zzn));
        zzbja();
        return zzn;
    }

    @NonNull
    public final <TContinuationResult> Task<TContinuationResult> continueWithTask(@NonNull Continuation<TResult, Task<TContinuationResult>> continuation) {
        return continueWithTask(TaskExecutors.MAIN_THREAD, continuation);
    }

    @NonNull
    public final <TContinuationResult> Task<TContinuationResult> continueWithTask(@NonNull Executor executor, @NonNull Continuation<TResult, Task<TContinuationResult>> continuation) {
        zzn zzn = new zzn();
        this.zzkru.zza(new zzc(executor, continuation, zzn));
        zzbja();
        return zzn;
    }

    @Nullable
    public final Exception getException() {
        Exception exc;
        synchronized (this.mLock) {
            exc = this.zzkrx;
        }
        return exc;
    }

    public final TResult getResult() {
        TResult tresult;
        synchronized (this.mLock) {
            zzbiy();
            if (this.zzkrx != null) {
                throw new RuntimeExecutionException(this.zzkrx);
            }
            tresult = this.zzkrw;
        }
        return tresult;
    }

    public final <X extends Throwable> TResult getResult(@NonNull Class<X> cls) throws Throwable {
        TResult tresult;
        synchronized (this.mLock) {
            zzbiy();
            if (cls.isInstance(this.zzkrx)) {
                throw ((Throwable) cls.cast(this.zzkrx));
            } else if (this.zzkrx != null) {
                throw new RuntimeExecutionException(this.zzkrx);
            } else {
                tresult = this.zzkrw;
            }
        }
        return tresult;
    }

    public final boolean isComplete() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzkrv;
        }
        return z;
    }

    public final boolean isSuccessful() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzkrv && this.zzkrx == null;
        }
        return z;
    }

    public final void setException(@NonNull Exception exc) {
        zzbq.checkNotNull(exc, "Exception must not be null");
        synchronized (this.mLock) {
            zzbiz();
            this.zzkrv = true;
            this.zzkrx = exc;
        }
        this.zzkru.zzb(this);
    }

    public final void setResult(TResult tresult) {
        synchronized (this.mLock) {
            zzbiz();
            this.zzkrv = true;
            this.zzkrw = tresult;
        }
        this.zzkru.zzb(this);
    }

    public final boolean trySetException(@NonNull Exception exc) {
        zzbq.checkNotNull(exc, "Exception must not be null");
        synchronized (this.mLock) {
            if (this.zzkrv) {
                return false;
            }
            this.zzkrv = true;
            this.zzkrx = exc;
            this.zzkru.zzb(this);
            return true;
        }
    }

    public final boolean trySetResult(TResult tresult) {
        synchronized (this.mLock) {
            if (this.zzkrv) {
                return false;
            }
            this.zzkrv = true;
            this.zzkrw = tresult;
            this.zzkru.zzb(this);
            return true;
        }
    }
}
