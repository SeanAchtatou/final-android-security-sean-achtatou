package com.google.android.gms.common.api.internal;

final class zzbq implements zzl {
    private /* synthetic */ zzbp zzfqo;

    zzbq(zzbp zzbp) {
        this.zzfqo = zzbp;
    }

    public final void zzbe(boolean z) {
        this.zzfqo.mHandler.sendMessage(this.zzfqo.mHandler.obtainMessage(1, Boolean.valueOf(z)));
    }
}
