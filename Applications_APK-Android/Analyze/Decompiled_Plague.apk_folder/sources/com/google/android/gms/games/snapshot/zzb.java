package com.google.android.gms.games.snapshot;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.zzc;
import com.google.android.gms.internal.zzbek;

public final class zzb implements Parcelable.Creator<zza> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        zzc zzc = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 1) {
                zzbek.zzb(parcel, readInt);
            } else {
                zzc = (zzc) zzbek.zza(parcel, readInt, zzc.CREATOR);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zza(zzc);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zza[i];
    }
}
