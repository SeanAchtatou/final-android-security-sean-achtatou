package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;

public final class zzbqf implements Parcelable.Creator<zzbqe> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        DataHolder dataHolder = null;
        boolean z = false;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    dataHolder = (DataHolder) zzbek.zza(parcel, readInt, DataHolder.CREATOR);
                    break;
                case 3:
                    z = zzbek.zzc(parcel, readInt);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbqe(dataHolder, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbqe[i];
    }
}
