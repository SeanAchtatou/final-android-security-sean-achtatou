package com.google.android.gms.common.api;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.internal.zzak;
import com.google.android.gms.common.api.internal.zzbp;
import com.google.android.gms.common.api.internal.zzbr;
import com.google.android.gms.common.api.internal.zzbz;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.common.api.internal.zzcn;
import com.google.android.gms.common.api.internal.zzcp;
import com.google.android.gms.common.api.internal.zzct;
import com.google.android.gms.common.api.internal.zzcy;
import com.google.android.gms.common.api.internal.zzdb;
import com.google.android.gms.common.api.internal.zzdf;
import com.google.android.gms.common.api.internal.zzdp;
import com.google.android.gms.common.api.internal.zzg;
import com.google.android.gms.common.api.internal.zzh;
import com.google.android.gms.common.api.internal.zzm;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.internal.zzs;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Collections;

public class GoogleApi<O extends Api.ApiOptions> {
    private final Context mContext;
    private final int mId;
    private final Looper zzakm;
    private final Api<O> zzffv;
    private final O zzfjk;
    private final zzh<O> zzfjl;
    private final GoogleApiClient zzfjm;
    private final zzdb zzfjn;
    protected final zzbp zzfjo;

    public static class zza {
        public static final zza zzfjp = new zzd().zzagd();
        public final zzdb zzfjq;
        public final Looper zzfjr;

        private zza(zzdb zzdb, Account account, Looper looper) {
            this.zzfjq = zzdb;
            this.zzfjr = looper;
        }
    }

    @MainThread
    public GoogleApi(@NonNull Activity activity, Api<O> api, O o, zza zza2) {
        zzbq.checkNotNull(activity, "Null activity is not permitted.");
        zzbq.checkNotNull(api, "Api must not be null.");
        zzbq.checkNotNull(zza2, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.mContext = activity.getApplicationContext();
        this.zzffv = api;
        this.zzfjk = o;
        this.zzakm = zza2.zzfjr;
        this.zzfjl = zzh.zza(this.zzffv, this.zzfjk);
        this.zzfjm = new zzbz(this);
        this.zzfjo = zzbp.zzch(this.mContext);
        this.mId = this.zzfjo.zzaig();
        this.zzfjn = zza2.zzfjq;
        zzak.zza(activity, this.zzfjo, this.zzfjl);
        this.zzfjo.zza(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$zza):void
     arg types: [android.app.Activity, com.google.android.gms.common.api.Api<O>, O, com.google.android.gms.common.api.GoogleApi$zza]
     candidates:
      com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.internal.zzdb):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.content.Context, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$zza):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.content.Context, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.internal.zzdb):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$zza):void */
    @Deprecated
    public GoogleApi(@NonNull Activity activity, Api<O> api, O o, zzdb zzdb) {
        this(activity, (Api) api, (Api.ApiOptions) o, new zzd().zza(zzdb).zza(activity.getMainLooper()).zzagd());
    }

    protected GoogleApi(@NonNull Context context, Api<O> api, Looper looper) {
        zzbq.checkNotNull(context, "Null context is not permitted.");
        zzbq.checkNotNull(api, "Api must not be null.");
        zzbq.checkNotNull(looper, "Looper must not be null.");
        this.mContext = context.getApplicationContext();
        this.zzffv = api;
        this.zzfjk = null;
        this.zzakm = looper;
        this.zzfjl = zzh.zzb(api);
        this.zzfjm = new zzbz(this);
        this.zzfjo = zzbp.zzch(this.mContext);
        this.mId = this.zzfjo.zzaig();
        this.zzfjn = new zzg();
    }

    @Deprecated
    public GoogleApi(@NonNull Context context, Api<O> api, O o, Looper looper, zzdb zzdb) {
        this(context, api, (Api.ApiOptions) null, new zzd().zza(looper).zza(zzdb).zzagd());
    }

    public GoogleApi(@NonNull Context context, Api<O> api, O o, zza zza2) {
        zzbq.checkNotNull(context, "Null context is not permitted.");
        zzbq.checkNotNull(api, "Api must not be null.");
        zzbq.checkNotNull(zza2, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.mContext = context.getApplicationContext();
        this.zzffv = api;
        this.zzfjk = o;
        this.zzakm = zza2.zzfjr;
        this.zzfjl = zzh.zza(this.zzffv, this.zzfjk);
        this.zzfjm = new zzbz(this);
        this.zzfjo = zzbp.zzch(this.mContext);
        this.mId = this.zzfjo.zzaig();
        this.zzfjn = zza2.zzfjq;
        this.zzfjo.zza(this);
    }

    @Deprecated
    public GoogleApi(@NonNull Context context, Api<O> api, O o, zzdb zzdb) {
        this(context, api, o, new zzd().zza(zzdb).zzagd());
    }

    private final <A extends Api.zzb, T extends zzm<? extends Result, A>> T zza(int i, @NonNull T t) {
        t.zzagw();
        this.zzfjo.zza(this, i, t);
        return t;
    }

    private final <TResult, A extends Api.zzb> Task<TResult> zza(int i, @NonNull zzdf<A, TResult> zzdf) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.zzfjo.zza(this, i, zzdf, taskCompletionSource, this.zzfjn);
        return taskCompletionSource.getTask();
    }

    private final zzs zzagc() {
        GoogleSignInAccount googleSignInAccount;
        return new zzs().zze(this.zzfjk instanceof Api.ApiOptions.HasGoogleSignInAccountOptions ? ((Api.ApiOptions.HasGoogleSignInAccountOptions) this.zzfjk).getGoogleSignInAccount().getAccount() : this.zzfjk instanceof Api.ApiOptions.HasAccountOptions ? ((Api.ApiOptions.HasAccountOptions) this.zzfjk).getAccount() : null).zze((!(this.zzfjk instanceof Api.ApiOptions.HasGoogleSignInAccountOptions) || (googleSignInAccount = ((Api.ApiOptions.HasGoogleSignInAccountOptions) this.zzfjk).getGoogleSignInAccount()) == null) ? Collections.emptySet() : googleSignInAccount.getGrantedScopes());
    }

    public final Context getApplicationContext() {
        return this.mContext;
    }

    public final int getInstanceId() {
        return this.mId;
    }

    public final Looper getLooper() {
        return this.zzakm;
    }

    @WorkerThread
    public Api.zze zza(Looper looper, zzbr<O> zzbr) {
        return this.zzffv.zzafs().zza(this.mContext, looper, zzagc().zzga(this.mContext.getPackageName()).zzgb(this.mContext.getClass().getName()).zzakr(), this.zzfjk, zzbr, zzbr);
    }

    public final <L> zzcl<L> zza(@NonNull L l, String str) {
        return zzcp.zzb(l, this.zzakm, str);
    }

    public zzcy zza(Context context, Handler handler) {
        return new zzcy(context, handler, zzagc().zzakr());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.GoogleApi.zza(int, com.google.android.gms.common.api.internal.zzm):T
     arg types: [int, T]
     candidates:
      com.google.android.gms.common.api.GoogleApi.zza(int, com.google.android.gms.common.api.internal.zzdf):com.google.android.gms.tasks.Task<TResult>
      com.google.android.gms.common.api.GoogleApi.zza(android.os.Looper, com.google.android.gms.common.api.internal.zzbr):com.google.android.gms.common.api.Api$zze
      com.google.android.gms.common.api.GoogleApi.zza(java.lang.Object, java.lang.String):com.google.android.gms.common.api.internal.zzcl<L>
      com.google.android.gms.common.api.GoogleApi.zza(android.content.Context, android.os.Handler):com.google.android.gms.common.api.internal.zzcy
      com.google.android.gms.common.api.GoogleApi.zza(com.google.android.gms.common.api.internal.zzct, com.google.android.gms.common.api.internal.zzdp):com.google.android.gms.tasks.Task<java.lang.Void>
      com.google.android.gms.common.api.GoogleApi.zza(int, com.google.android.gms.common.api.internal.zzm):T */
    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T zza(@NonNull T t) {
        return zza(0, (zzm) t);
    }

    public final Task<Boolean> zza(@NonNull zzcn<?> zzcn) {
        zzbq.checkNotNull(zzcn, "Listener key cannot be null.");
        return this.zzfjo.zza(this, zzcn);
    }

    public final <A extends Api.zzb, T extends zzct<A, ?>, U extends zzdp<A, ?>> Task<Void> zza(@NonNull T t, U u) {
        zzbq.checkNotNull(t);
        zzbq.checkNotNull(u);
        zzbq.checkNotNull(t.zzajc(), "Listener has already been released.");
        zzbq.checkNotNull(u.zzajc(), "Listener has already been released.");
        zzbq.checkArgument(t.zzajc().equals(u.zzajc()), "Listener registration and unregistration methods must be constructed with the same ListenerHolder.");
        return this.zzfjo.zza(this, t, u);
    }

    public final <TResult, A extends Api.zzb> Task<TResult> zza(zzdf<A, TResult> zzdf) {
        return zza(0, zzdf);
    }

    public final Api<O> zzafy() {
        return this.zzffv;
    }

    public final O zzafz() {
        return this.zzfjk;
    }

    public final zzh<O> zzaga() {
        return this.zzfjl;
    }

    public final GoogleApiClient zzagb() {
        return this.zzfjm;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.GoogleApi.zza(int, com.google.android.gms.common.api.internal.zzm):T
     arg types: [int, T]
     candidates:
      com.google.android.gms.common.api.GoogleApi.zza(int, com.google.android.gms.common.api.internal.zzdf):com.google.android.gms.tasks.Task<TResult>
      com.google.android.gms.common.api.GoogleApi.zza(android.os.Looper, com.google.android.gms.common.api.internal.zzbr):com.google.android.gms.common.api.Api$zze
      com.google.android.gms.common.api.GoogleApi.zza(java.lang.Object, java.lang.String):com.google.android.gms.common.api.internal.zzcl<L>
      com.google.android.gms.common.api.GoogleApi.zza(android.content.Context, android.os.Handler):com.google.android.gms.common.api.internal.zzcy
      com.google.android.gms.common.api.GoogleApi.zza(com.google.android.gms.common.api.internal.zzct, com.google.android.gms.common.api.internal.zzdp):com.google.android.gms.tasks.Task<java.lang.Void>
      com.google.android.gms.common.api.GoogleApi.zza(int, com.google.android.gms.common.api.internal.zzm):T */
    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T zzb(@NonNull T t) {
        return zza(1, (zzm) t);
    }

    public final <TResult, A extends Api.zzb> Task<TResult> zzb(zzdf<A, TResult> zzdf) {
        return zza(1, zzdf);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.GoogleApi.zza(int, com.google.android.gms.common.api.internal.zzm):T
     arg types: [int, T]
     candidates:
      com.google.android.gms.common.api.GoogleApi.zza(int, com.google.android.gms.common.api.internal.zzdf):com.google.android.gms.tasks.Task<TResult>
      com.google.android.gms.common.api.GoogleApi.zza(android.os.Looper, com.google.android.gms.common.api.internal.zzbr):com.google.android.gms.common.api.Api$zze
      com.google.android.gms.common.api.GoogleApi.zza(java.lang.Object, java.lang.String):com.google.android.gms.common.api.internal.zzcl<L>
      com.google.android.gms.common.api.GoogleApi.zza(android.content.Context, android.os.Handler):com.google.android.gms.common.api.internal.zzcy
      com.google.android.gms.common.api.GoogleApi.zza(com.google.android.gms.common.api.internal.zzct, com.google.android.gms.common.api.internal.zzdp):com.google.android.gms.tasks.Task<java.lang.Void>
      com.google.android.gms.common.api.GoogleApi.zza(int, com.google.android.gms.common.api.internal.zzm):T */
    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T zzc(@NonNull T t) {
        return zza(2, (zzm) t);
    }
}
