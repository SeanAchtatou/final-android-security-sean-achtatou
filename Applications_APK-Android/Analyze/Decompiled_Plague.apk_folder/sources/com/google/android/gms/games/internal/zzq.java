package com.google.android.gms.games.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.common.api.internal.zzct;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class zzq<L> extends zzct<GamesClientImpl, L> {
    protected zzq(zzcl<L> zzcl) {
        super(zzcl);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zzb(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        try {
            zzb((GamesClientImpl) zzb, (TaskCompletionSource<Void>) taskCompletionSource);
        } catch (SecurityException e) {
            taskCompletionSource.trySetException(e);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void zzb(GamesClientImpl gamesClientImpl, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException;
}
