package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzan;
import com.google.android.gms.common.internal.zzbt;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.common.internal.zzt;
import com.google.android.gms.common.zze;
import com.google.android.gms.internal.zzcwb;
import com.google.android.gms.internal.zzcwc;
import com.google.android.gms.internal.zzcwo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

public final class zzar implements zzbk {
    /* access modifiers changed from: private */
    public final Context mContext;
    private final Api.zza<? extends zzcwb, zzcwc> zzfkf;
    /* access modifiers changed from: private */
    public final Lock zzfmy;
    private final zzr zzfnd;
    private final Map<Api<?>, Boolean> zzfng;
    /* access modifiers changed from: private */
    public final zze zzfni;
    private ConnectionResult zzfnr;
    /* access modifiers changed from: private */
    public final zzbl zzfob;
    private int zzfoe;
    private int zzfof = 0;
    private int zzfog;
    private final Bundle zzfoh = new Bundle();
    private final Set<Api.zzc> zzfoi = new HashSet();
    /* access modifiers changed from: private */
    public zzcwb zzfoj;
    private boolean zzfok;
    /* access modifiers changed from: private */
    public boolean zzfol;
    private boolean zzfom;
    /* access modifiers changed from: private */
    public zzan zzfon;
    private boolean zzfoo;
    private boolean zzfop;
    private ArrayList<Future<?>> zzfoq = new ArrayList<>();

    public zzar(zzbl zzbl, zzr zzr, Map<Api<?>, Boolean> map, zze zze, Api.zza<? extends zzcwb, zzcwc> zza, Lock lock, Context context) {
        this.zzfob = zzbl;
        this.zzfnd = zzr;
        this.zzfng = map;
        this.zzfni = zze;
        this.zzfkf = zza;
        this.zzfmy = lock;
        this.mContext = context;
    }

    /* access modifiers changed from: private */
    public final void zza(zzcwo zzcwo) {
        if (zzbt(0)) {
            ConnectionResult zzagt = zzcwo.zzagt();
            if (zzagt.isSuccess()) {
                zzbt zzbcw = zzcwo.zzbcw();
                ConnectionResult zzagt2 = zzbcw.zzagt();
                if (!zzagt2.isSuccess()) {
                    String valueOf = String.valueOf(zzagt2);
                    StringBuilder sb = new StringBuilder(48 + String.valueOf(valueOf).length());
                    sb.append("Sign-in succeeded with resolve account failure: ");
                    sb.append(valueOf);
                    Log.wtf("GoogleApiClientConnecting", sb.toString(), new Exception());
                    zze(zzagt2);
                    return;
                }
                this.zzfom = true;
                this.zzfon = zzbcw.zzald();
                this.zzfoo = zzbcw.zzale();
                this.zzfop = zzbcw.zzalf();
                zzahr();
            } else if (zzd(zzagt)) {
                zzaht();
                zzahr();
            } else {
                zze(zzagt);
            }
        }
    }

    /* access modifiers changed from: private */
    public final boolean zzahq() {
        ConnectionResult connectionResult;
        this.zzfog--;
        if (this.zzfog > 0) {
            return false;
        }
        if (this.zzfog < 0) {
            Log.w("GoogleApiClientConnecting", this.zzfob.zzfmo.zzaia());
            Log.wtf("GoogleApiClientConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            connectionResult = new ConnectionResult(8, null);
        } else if (this.zzfnr == null) {
            return true;
        } else {
            this.zzfob.zzfpz = this.zzfoe;
            connectionResult = this.zzfnr;
        }
        zze(connectionResult);
        return false;
    }

    /* access modifiers changed from: private */
    public final void zzahr() {
        if (this.zzfog == 0) {
            if (!this.zzfol || this.zzfom) {
                ArrayList arrayList = new ArrayList();
                this.zzfof = 1;
                this.zzfog = this.zzfob.zzfph.size();
                for (Api.zzc next : this.zzfob.zzfph.keySet()) {
                    if (!this.zzfob.zzfpw.containsKey(next)) {
                        arrayList.add(this.zzfob.zzfph.get(next));
                    } else if (zzahq()) {
                        zzahs();
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.zzfoq.add(zzbo.zzaid().submit(new zzax(this, arrayList)));
                }
            }
        }
    }

    private final void zzahs() {
        this.zzfob.zzaic();
        zzbo.zzaid().execute(new zzas(this));
        if (this.zzfoj != null) {
            if (this.zzfoo) {
                this.zzfoj.zza(this.zzfon, this.zzfop);
            }
            zzbf(false);
        }
        for (Api.zzc<?> zzc : this.zzfob.zzfpw.keySet()) {
            this.zzfob.zzfph.get(zzc).disconnect();
        }
        this.zzfob.zzfqa.zzj(this.zzfoh.isEmpty() ? null : this.zzfoh);
    }

    /* access modifiers changed from: private */
    public final void zzaht() {
        this.zzfol = false;
        this.zzfob.zzfmo.zzfpi = Collections.emptySet();
        for (Api.zzc next : this.zzfoi) {
            if (!this.zzfob.zzfpw.containsKey(next)) {
                this.zzfob.zzfpw.put(next, new ConnectionResult(17, null));
            }
        }
    }

    private final void zzahu() {
        ArrayList arrayList = this.zzfoq;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList.get(i);
            i++;
            ((Future) obj).cancel(true);
        }
        this.zzfoq.clear();
    }

    /* access modifiers changed from: private */
    public final Set<Scope> zzahv() {
        if (this.zzfnd == null) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet(this.zzfnd.zzakj());
        Map<Api<?>, zzt> zzakl = this.zzfnd.zzakl();
        for (Api next : zzakl.keySet()) {
            if (!this.zzfob.zzfpw.containsKey(next.zzaft())) {
                hashSet.addAll(zzakl.get(next).zzees);
            }
        }
        return hashSet;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        if ((r5.hasResolution() || r4.zzfni.zzbp(r5.getErrorCode()) != null) != false) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzb(com.google.android.gms.common.ConnectionResult r5, com.google.android.gms.common.api.Api<?> r6, boolean r7) {
        /*
            r4 = this;
            com.google.android.gms.common.api.Api$zzd r0 = r6.zzafr()
            int r0 = r0.getPriority()
            r1 = 0
            r2 = 1
            if (r7 == 0) goto L_0x0024
            boolean r7 = r5.hasResolution()
            if (r7 == 0) goto L_0x0014
        L_0x0012:
            r7 = r2
            goto L_0x0022
        L_0x0014:
            com.google.android.gms.common.zze r7 = r4.zzfni
            int r3 = r5.getErrorCode()
            android.content.Intent r7 = r7.zzbp(r3)
            if (r7 == 0) goto L_0x0021
            goto L_0x0012
        L_0x0021:
            r7 = r1
        L_0x0022:
            if (r7 == 0) goto L_0x002d
        L_0x0024:
            com.google.android.gms.common.ConnectionResult r7 = r4.zzfnr
            if (r7 == 0) goto L_0x002c
            int r7 = r4.zzfoe
            if (r0 >= r7) goto L_0x002d
        L_0x002c:
            r1 = r2
        L_0x002d:
            if (r1 == 0) goto L_0x0033
            r4.zzfnr = r5
            r4.zzfoe = r0
        L_0x0033:
            com.google.android.gms.common.api.internal.zzbl r7 = r4.zzfob
            java.util.Map<com.google.android.gms.common.api.Api$zzc<?>, com.google.android.gms.common.ConnectionResult> r7 = r7.zzfpw
            com.google.android.gms.common.api.Api$zzc r6 = r6.zzaft()
            r7.put(r6, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzar.zzb(com.google.android.gms.common.ConnectionResult, com.google.android.gms.common.api.Api, boolean):void");
    }

    private final void zzbf(boolean z) {
        if (this.zzfoj != null) {
            if (this.zzfoj.isConnected() && z) {
                this.zzfoj.zzbcp();
            }
            this.zzfoj.disconnect();
            this.zzfon = null;
        }
    }

    /* access modifiers changed from: private */
    public final boolean zzbt(int i) {
        if (this.zzfof == i) {
            return true;
        }
        Log.w("GoogleApiClientConnecting", this.zzfob.zzfmo.zzaia());
        String valueOf = String.valueOf(this);
        StringBuilder sb = new StringBuilder(23 + String.valueOf(valueOf).length());
        sb.append("Unexpected callback in ");
        sb.append(valueOf);
        Log.w("GoogleApiClientConnecting", sb.toString());
        int i2 = this.zzfog;
        StringBuilder sb2 = new StringBuilder(33);
        sb2.append("mRemainingConnections=");
        sb2.append(i2);
        Log.w("GoogleApiClientConnecting", sb2.toString());
        String zzbu = zzbu(this.zzfof);
        String zzbu2 = zzbu(i);
        StringBuilder sb3 = new StringBuilder(70 + String.valueOf(zzbu).length() + String.valueOf(zzbu2).length());
        sb3.append("GoogleApiClient connecting is in step ");
        sb3.append(zzbu);
        sb3.append(" but received callback for step ");
        sb3.append(zzbu2);
        Log.wtf("GoogleApiClientConnecting", sb3.toString(), new Exception());
        zze(new ConnectionResult(8, null));
        return false;
    }

    private static String zzbu(int i) {
        switch (i) {
            case 0:
                return "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
            case 1:
                return "STEP_GETTING_REMOTE_SERVICE";
            default:
                return "UNKNOWN";
        }
    }

    /* access modifiers changed from: private */
    public final boolean zzd(ConnectionResult connectionResult) {
        return this.zzfok && !connectionResult.hasResolution();
    }

    /* access modifiers changed from: private */
    public final void zze(ConnectionResult connectionResult) {
        zzahu();
        zzbf(!connectionResult.hasResolution());
        this.zzfob.zzg(connectionResult);
        this.zzfob.zzfqa.zzc(connectionResult);
    }

    public final void begin() {
        this.zzfob.zzfpw.clear();
        this.zzfol = false;
        this.zzfnr = null;
        this.zzfof = 0;
        this.zzfok = true;
        this.zzfom = false;
        this.zzfoo = false;
        HashMap hashMap = new HashMap();
        boolean z = false;
        for (Api next : this.zzfng.keySet()) {
            Api.zze zze = this.zzfob.zzfph.get(next.zzaft());
            z |= next.zzafr().getPriority() == 1;
            boolean booleanValue = this.zzfng.get(next).booleanValue();
            if (zze.zzaam()) {
                this.zzfol = true;
                if (booleanValue) {
                    this.zzfoi.add(next.zzaft());
                } else {
                    this.zzfok = false;
                }
            }
            hashMap.put(zze, new zzat(this, next, booleanValue));
        }
        if (z) {
            this.zzfol = false;
        }
        if (this.zzfol) {
            this.zzfnd.zzc(Integer.valueOf(System.identityHashCode(this.zzfob.zzfmo)));
            zzba zzba = new zzba(this, null);
            this.zzfoj = (zzcwb) this.zzfkf.zza(this.mContext, this.zzfob.zzfmo.getLooper(), this.zzfnd, this.zzfnd.zzakp(), zzba, zzba);
        }
        this.zzfog = this.zzfob.zzfph.size();
        this.zzfoq.add(zzbo.zzaid().submit(new zzau(this, hashMap)));
    }

    public final void connect() {
    }

    public final boolean disconnect() {
        zzahu();
        zzbf(true);
        this.zzfob.zzg(null);
        return true;
    }

    public final void onConnected(Bundle bundle) {
        if (zzbt(1)) {
            if (bundle != null) {
                this.zzfoh.putAll(bundle);
            }
            if (zzahq()) {
                zzahs();
            }
        }
    }

    public final void onConnectionSuspended(int i) {
        zze(new ConnectionResult(8, null));
    }

    public final void zza(ConnectionResult connectionResult, Api<?> api, boolean z) {
        if (zzbt(1)) {
            zzb(connectionResult, api, z);
            if (zzahq()) {
                zzahs();
            }
        }
    }

    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T zzd(T t) {
        this.zzfob.zzfmo.zzfnm.add(t);
        return t;
    }

    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T zze(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }
}
