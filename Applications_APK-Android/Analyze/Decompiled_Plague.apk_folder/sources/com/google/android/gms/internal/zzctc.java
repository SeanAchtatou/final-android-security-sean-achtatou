package com.google.android.gms.internal;

import android.net.Uri;
import com.google.android.gms.common.api.Api;

public final class zzctc {
    @Deprecated
    private static Api<Api.ApiOptions.NoOptions> API = new Api<>("Phenotype.API", zzdyi, zzdyh);
    private static final Api.zzf<zzctr> zzdyh = new Api.zzf<>();
    private static final Api.zza<zzctr, Api.ApiOptions.NoOptions> zzdyi = new zzctd();
    @Deprecated
    private static zzcte zzjug = new zzctq();

    public static Uri zzkm(String str) {
        String valueOf = String.valueOf(Uri.encode(str));
        return Uri.parse(valueOf.length() != 0 ? "content://com.google.android.gms.phenotype/".concat(valueOf) : new String("content://com.google.android.gms.phenotype/"));
    }
}
