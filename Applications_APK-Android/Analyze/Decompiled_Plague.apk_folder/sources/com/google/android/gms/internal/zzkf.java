package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.zzo;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzkf extends zzed implements zzke {
    zzkf(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IAdManagerCreator");
    }

    public final IBinder zza(IObjectWrapper iObjectWrapper, zziw zziw, String str, zzuc zzuc, int i, int i2) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, iObjectWrapper);
        zzef.zza(zzaz, zziw);
        zzaz.writeString(str);
        zzef.zza(zzaz, zzuc);
        zzaz.writeInt(zzo.GOOGLE_PLAY_SERVICES_VERSION_CODE);
        zzaz.writeInt(i2);
        Parcel zza = zza(2, zzaz);
        IBinder readStrongBinder = zza.readStrongBinder();
        zza.recycle();
        return readStrongBinder;
    }
}
