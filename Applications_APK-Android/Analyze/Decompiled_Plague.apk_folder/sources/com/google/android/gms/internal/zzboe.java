package com.google.android.gms.internal;

import com.google.android.gms.drive.events.OpenFileCallback;

final /* synthetic */ class zzboe implements zzbnx {
    private final zzbob zzgna;
    private final zzbps zzgnd;

    zzboe(zzbob zzbob, zzbps zzbps) {
        this.zzgna = zzbob;
        this.zzgnd = zzbps;
    }

    public final void accept(Object obj) {
        this.zzgna.zza(this.zzgnd, (OpenFileCallback) obj);
    }
}
