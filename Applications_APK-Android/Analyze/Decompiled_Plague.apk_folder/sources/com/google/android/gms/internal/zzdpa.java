package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrk;
import com.google.android.gms.internal.zzdrr;
import java.security.GeneralSecurityException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class zzdpa {
    public static final zzdpa zzlpq = new zzdpa();
    private final ConcurrentMap<String, zzdos> zzlpr = new ConcurrentHashMap();

    protected zzdpa() {
    }

    private final <P> zzdos<P> zzny(String str) throws GeneralSecurityException {
        zzdos<P> zzdos = this.zzlpr.get(str);
        if (zzdos != null) {
            return zzdos;
        }
        String valueOf = String.valueOf(str);
        throw new GeneralSecurityException(valueOf.length() != 0 ? "unsupported key type: ".concat(valueOf) : new String("unsupported key type: "));
    }

    public final <P> zzdow<P> zza(zzdot zzdot, zzdos zzdos) throws GeneralSecurityException {
        zzdrr zzble = zzdot.zzble();
        if (zzble.zzboc() == 0) {
            throw new GeneralSecurityException("empty keyset");
        }
        int zzboa = zzble.zzboa();
        boolean z = true;
        boolean z2 = false;
        for (zzdrr.zzb next : zzble.zzbob()) {
            if (!next.zzboe()) {
                throw new GeneralSecurityException(String.format("key %d has no key data", Integer.valueOf(next.zzboh())));
            } else if (next.zzboi() == zzdrv.UNKNOWN_PREFIX) {
                throw new GeneralSecurityException(String.format("key %d has unknown prefix", Integer.valueOf(next.zzboh())));
            } else if (next.zzbog() == zzdrn.UNKNOWN_STATUS) {
                throw new GeneralSecurityException(String.format("key %d has unknown status", Integer.valueOf(next.zzboh())));
            } else {
                if (next.zzbog() == zzdrn.ENABLED && next.zzboh() == zzboa) {
                    if (z2) {
                        throw new GeneralSecurityException("keyset contains multiple primary keys");
                    }
                    z2 = true;
                }
                if (next.zzbof().zzbnu() != zzdrk.zzb.ASYMMETRIC_PUBLIC) {
                    z = false;
                }
            }
        }
        if (z2 || z) {
            zzdow<P> zzdow = new zzdow<>();
            for (zzdrr.zzb next2 : zzdot.zzble().zzbob()) {
                if (next2.zzbog() == zzdrn.ENABLED) {
                    zzdox<P> zza = zzdow.zza(zzny(next2.zzbof().zzbns()).zza(next2.zzbof().zzbnt()), next2);
                    if (next2.zzboh() == zzdot.zzble().zzboa()) {
                        zzdow.zza(zza);
                    }
                }
            }
            return zzdow;
        }
        throw new GeneralSecurityException("keyset doesn't contain a valid primary key");
    }

    public final <P> zzdrk zza(zzdrp zzdrp) throws GeneralSecurityException {
        return zzny(zzdrp.zzbns()).zzc(zzdrp.zzbnt());
    }

    public final <P> zzffi zza(String str, zzffi zzffi) throws GeneralSecurityException {
        return zzny(str).zzb(zzffi);
    }

    public final <P> boolean zza(String str, zzdos zzdos) throws GeneralSecurityException {
        if (zzdos != null) {
            return this.zzlpr.putIfAbsent(str, zzdos) == null;
        }
        throw new NullPointerException("key manager must be non-null.");
    }

    public final <P> zzffi zzb(zzdrp zzdrp) throws GeneralSecurityException {
        return zzny(zzdrp.zzbns()).zzb(zzdrp.zzbnt());
    }

    public final <P> P zzb(String str, zzffi zzffi) throws GeneralSecurityException {
        return zzny(str).zza(zzffi);
    }
}
