package com.google.android.gms.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzbq;
import java.util.HashMap;
import java.util.Set;

public final class zzcaf {
    private static final String[] zzhth = {"requestId", "outcome"};
    private final int zzfcq;
    private final HashMap<String, Integer> zzhti;

    private zzcaf(int i, HashMap<String, Integer> hashMap) {
        this.zzfcq = i;
        this.zzhti = hashMap;
    }

    public static zzcaf zzan(DataHolder dataHolder) {
        zzcah zzcah = new zzcah();
        zzcah.zzdn(dataHolder.getStatusCode());
        int count = dataHolder.getCount();
        for (int i = 0; i < count; i++) {
            int zzbz = dataHolder.zzbz(i);
            zzcah.zzw(dataHolder.zzd("requestId", i, zzbz), dataHolder.zzc("outcome", i, zzbz));
        }
        return zzcah.zzatq();
    }

    public final Set<String> getRequestIds() {
        return this.zzhti.keySet();
    }

    public final int getRequestOutcome(String str) {
        boolean containsKey = this.zzhti.containsKey(str);
        StringBuilder sb = new StringBuilder(46 + String.valueOf(str).length());
        sb.append("Request ");
        sb.append(str);
        sb.append(" was not part of the update operation!");
        zzbq.checkArgument(containsKey, sb.toString());
        return this.zzhti.get(str).intValue();
    }
}
