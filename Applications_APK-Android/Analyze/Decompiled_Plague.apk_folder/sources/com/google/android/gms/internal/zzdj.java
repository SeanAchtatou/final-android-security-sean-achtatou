package com.google.android.gms.internal;

import java.util.concurrent.ExecutionException;

public final class zzdj extends zzec {
    private static final Object zzajl = new Object();
    private static volatile zzbs zzajm;
    private zzat zzajn = null;

    public zzdj(zzda zzda, String str, String str2, zzaw zzaw, int i, int i2, zzat zzat) {
        super(zzda, str, str2, zzaw, i, 27);
        this.zzajn = zzat;
    }

    private final String zzau() {
        try {
            if (this.zzagk.zzan() != null) {
                this.zzagk.zzan().get();
            }
            zzaw zzam = this.zzagk.zzam();
            if (zzam == null || zzam.zzcq == null) {
                return null;
            }
            return zzam.zzcq;
        } catch (InterruptedException | ExecutionException unused) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzat() throws java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException {
        /*
            r9 = this;
            com.google.android.gms.internal.zzbs r0 = com.google.android.gms.internal.zzdj.zzajm
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x002b
            com.google.android.gms.internal.zzbs r0 = com.google.android.gms.internal.zzdj.zzajm
            java.lang.String r0 = r0.zzcq
            boolean r0 = com.google.android.gms.internal.zzdf.zzn(r0)
            if (r0 != 0) goto L_0x002b
            com.google.android.gms.internal.zzbs r0 = com.google.android.gms.internal.zzdj.zzajm
            java.lang.String r0 = r0.zzcq
            java.lang.String r3 = "E"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x002b
            com.google.android.gms.internal.zzbs r0 = com.google.android.gms.internal.zzdj.zzajm
            java.lang.String r0 = r0.zzcq
            java.lang.String r3 = "0000000000000000000000000000000000000000000000000000000000000000"
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x0029
            goto L_0x002b
        L_0x0029:
            r0 = r2
            goto L_0x002c
        L_0x002b:
            r0 = r1
        L_0x002c:
            if (r0 == 0) goto L_0x00d4
            java.lang.Object r0 = com.google.android.gms.internal.zzdj.zzajl
            monitor-enter(r0)
            com.google.android.gms.internal.zzat r3 = r9.zzajn     // Catch:{ all -> 0x00d1 }
            r3 = 0
            boolean r4 = com.google.android.gms.internal.zzdf.zzn(r3)     // Catch:{ all -> 0x00d1 }
            r5 = 2
            if (r4 != 0) goto L_0x003d
            r4 = 4
            goto L_0x0080
        L_0x003d:
            com.google.android.gms.internal.zzat r4 = r9.zzajn     // Catch:{ all -> 0x00d1 }
            com.google.android.gms.internal.zzdf.zzn(r3)     // Catch:{ all -> 0x00d1 }
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x00d1 }
            boolean r4 = r4.booleanValue()     // Catch:{ all -> 0x00d1 }
            if (r4 == 0) goto L_0x007f
            com.google.android.gms.internal.zzda r4 = r9.zzagk     // Catch:{ all -> 0x00d1 }
            boolean r4 = r4.zzak()     // Catch:{ all -> 0x00d1 }
            if (r4 == 0) goto L_0x007a
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r4 = com.google.android.gms.internal.zzmq.zzbmf     // Catch:{ all -> 0x00d1 }
            com.google.android.gms.internal.zzmo r6 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ all -> 0x00d1 }
            java.lang.Object r4 = r6.zzd(r4)     // Catch:{ all -> 0x00d1 }
            java.lang.Boolean r4 = (java.lang.Boolean) r4     // Catch:{ all -> 0x00d1 }
            boolean r4 = r4.booleanValue()     // Catch:{ all -> 0x00d1 }
            if (r4 == 0) goto L_0x007a
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r4 = com.google.android.gms.internal.zzmq.zzbmg     // Catch:{ all -> 0x00d1 }
            com.google.android.gms.internal.zzmo r6 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ all -> 0x00d1 }
            java.lang.Object r4 = r6.zzd(r4)     // Catch:{ all -> 0x00d1 }
            java.lang.Boolean r4 = (java.lang.Boolean) r4     // Catch:{ all -> 0x00d1 }
            boolean r4 = r4.booleanValue()     // Catch:{ all -> 0x00d1 }
            if (r4 == 0) goto L_0x007a
            r4 = r1
            goto L_0x007b
        L_0x007a:
            r4 = r2
        L_0x007b:
            if (r4 == 0) goto L_0x007f
            r4 = 3
            goto L_0x0080
        L_0x007f:
            r4 = r5
        L_0x0080:
            java.lang.reflect.Method r6 = r9.zzajx     // Catch:{ all -> 0x00d1 }
            java.lang.Object[] r7 = new java.lang.Object[r5]     // Catch:{ all -> 0x00d1 }
            com.google.android.gms.internal.zzda r8 = r9.zzagk     // Catch:{ all -> 0x00d1 }
            android.content.Context r8 = r8.getContext()     // Catch:{ all -> 0x00d1 }
            r7[r2] = r8     // Catch:{ all -> 0x00d1 }
            if (r4 != r5) goto L_0x008f
            r2 = r1
        L_0x008f:
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x00d1 }
            r7[r1] = r2     // Catch:{ all -> 0x00d1 }
            java.lang.Object r1 = r6.invoke(r3, r7)     // Catch:{ all -> 0x00d1 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x00d1 }
            com.google.android.gms.internal.zzbs r2 = new com.google.android.gms.internal.zzbs     // Catch:{ all -> 0x00d1 }
            r2.<init>(r1)     // Catch:{ all -> 0x00d1 }
            com.google.android.gms.internal.zzdj.zzajm = r2     // Catch:{ all -> 0x00d1 }
            java.lang.String r1 = r2.zzcq     // Catch:{ all -> 0x00d1 }
            boolean r1 = com.google.android.gms.internal.zzdf.zzn(r1)     // Catch:{ all -> 0x00d1 }
            if (r1 != 0) goto L_0x00b6
            com.google.android.gms.internal.zzbs r1 = com.google.android.gms.internal.zzdj.zzajm     // Catch:{ all -> 0x00d1 }
            java.lang.String r1 = r1.zzcq     // Catch:{ all -> 0x00d1 }
            java.lang.String r2 = "E"
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x00d1 }
            if (r1 == 0) goto L_0x00cf
        L_0x00b6:
            switch(r4) {
                case 3: goto L_0x00c1;
                case 4: goto L_0x00ba;
                default: goto L_0x00b9;
            }     // Catch:{ all -> 0x00d1 }
        L_0x00b9:
            goto L_0x00cf
        L_0x00ba:
            com.google.android.gms.internal.zzbs r1 = com.google.android.gms.internal.zzdj.zzajm     // Catch:{ all -> 0x00d1 }
            java.lang.String r2 = r3.zzcq     // Catch:{ all -> 0x00d1 }
            r1.zzcq = r2     // Catch:{ all -> 0x00d1 }
            goto L_0x00cf
        L_0x00c1:
            java.lang.String r1 = r9.zzau()     // Catch:{ all -> 0x00d1 }
            boolean r2 = com.google.android.gms.internal.zzdf.zzn(r1)     // Catch:{ all -> 0x00d1 }
            if (r2 != 0) goto L_0x00cf
            com.google.android.gms.internal.zzbs r2 = com.google.android.gms.internal.zzdj.zzajm     // Catch:{ all -> 0x00d1 }
            r2.zzcq = r1     // Catch:{ all -> 0x00d1 }
        L_0x00cf:
            monitor-exit(r0)     // Catch:{ all -> 0x00d1 }
            goto L_0x00d4
        L_0x00d1:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00d1 }
            throw r1
        L_0x00d4:
            com.google.android.gms.internal.zzaw r0 = r9.zzajo
            monitor-enter(r0)
            com.google.android.gms.internal.zzbs r1 = com.google.android.gms.internal.zzdj.zzajm     // Catch:{ all -> 0x0109 }
            if (r1 == 0) goto L_0x0107
            com.google.android.gms.internal.zzaw r1 = r9.zzajo     // Catch:{ all -> 0x0109 }
            com.google.android.gms.internal.zzbs r2 = com.google.android.gms.internal.zzdj.zzajm     // Catch:{ all -> 0x0109 }
            java.lang.String r2 = r2.zzcq     // Catch:{ all -> 0x0109 }
            r1.zzcq = r2     // Catch:{ all -> 0x0109 }
            com.google.android.gms.internal.zzaw r1 = r9.zzajo     // Catch:{ all -> 0x0109 }
            com.google.android.gms.internal.zzbs r2 = com.google.android.gms.internal.zzdj.zzajm     // Catch:{ all -> 0x0109 }
            long r2 = r2.zzyo     // Catch:{ all -> 0x0109 }
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x0109 }
            r1.zzdu = r2     // Catch:{ all -> 0x0109 }
            com.google.android.gms.internal.zzaw r1 = r9.zzajo     // Catch:{ all -> 0x0109 }
            com.google.android.gms.internal.zzbs r2 = com.google.android.gms.internal.zzdj.zzajm     // Catch:{ all -> 0x0109 }
            java.lang.String r2 = r2.zzcs     // Catch:{ all -> 0x0109 }
            r1.zzcs = r2     // Catch:{ all -> 0x0109 }
            com.google.android.gms.internal.zzaw r1 = r9.zzajo     // Catch:{ all -> 0x0109 }
            com.google.android.gms.internal.zzbs r2 = com.google.android.gms.internal.zzdj.zzajm     // Catch:{ all -> 0x0109 }
            java.lang.String r2 = r2.zzct     // Catch:{ all -> 0x0109 }
            r1.zzct = r2     // Catch:{ all -> 0x0109 }
            com.google.android.gms.internal.zzaw r1 = r9.zzajo     // Catch:{ all -> 0x0109 }
            com.google.android.gms.internal.zzbs r2 = com.google.android.gms.internal.zzdj.zzajm     // Catch:{ all -> 0x0109 }
            java.lang.String r2 = r2.zzcu     // Catch:{ all -> 0x0109 }
            r1.zzcu = r2     // Catch:{ all -> 0x0109 }
        L_0x0107:
            monitor-exit(r0)     // Catch:{ all -> 0x0109 }
            return
        L_0x0109:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0109 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdj.zzat():void");
    }
}
