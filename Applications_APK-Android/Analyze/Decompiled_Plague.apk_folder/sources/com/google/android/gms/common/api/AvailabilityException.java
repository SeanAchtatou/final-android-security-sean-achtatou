package com.google.android.gms.common.api;

import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzh;
import com.google.android.gms.common.internal.zzbq;
import java.util.ArrayList;

public class AvailabilityException extends Exception {
    private final ArrayMap<zzh<?>, ConnectionResult> zzfjc;

    public AvailabilityException(ArrayMap<zzh<?>, ConnectionResult> arrayMap) {
        this.zzfjc = arrayMap;
    }

    public ConnectionResult getConnectionResult(GoogleApi<? extends Api.ApiOptions> googleApi) {
        zzh<? extends Api.ApiOptions> zzaga = googleApi.zzaga();
        zzbq.checkArgument(this.zzfjc.get(zzaga) != null, "The given API was not part of the availability request.");
        return this.zzfjc.get(zzaga);
    }

    public String getMessage() {
        ArrayList arrayList = new ArrayList();
        boolean z = true;
        for (zzh next : this.zzfjc.keySet()) {
            ConnectionResult connectionResult = this.zzfjc.get(next);
            if (connectionResult.isSuccess()) {
                z = false;
            }
            String zzagl = next.zzagl();
            String valueOf = String.valueOf(connectionResult);
            StringBuilder sb = new StringBuilder(2 + String.valueOf(zzagl).length() + String.valueOf(valueOf).length());
            sb.append(zzagl);
            sb.append(": ");
            sb.append(valueOf);
            arrayList.add(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(z ? "None of the queried APIs are available. " : "Some of the queried APIs are unavailable. ");
        sb2.append(TextUtils.join("; ", arrayList));
        return sb2.toString();
    }

    public final ArrayMap<zzh<?>, ConnectionResult> zzafw() {
        return this.zzfjc;
    }
}
