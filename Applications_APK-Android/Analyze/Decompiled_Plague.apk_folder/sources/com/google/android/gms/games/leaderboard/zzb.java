package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzbi;
import com.google.android.gms.internal.zzcab;
import java.util.Arrays;

public final class zzb implements LeaderboardVariant {
    private final int zzhuc;
    private final int zzhud;
    private final boolean zzhue;
    private final long zzhuf;
    private final String zzhug;
    private final long zzhuh;
    private final String zzhui;
    private final String zzhuj;
    private final long zzhuk;
    private final String zzhul;
    private final String zzhum;
    private final String zzhun;

    public zzb(LeaderboardVariant leaderboardVariant) {
        this.zzhuc = leaderboardVariant.getTimeSpan();
        this.zzhud = leaderboardVariant.getCollection();
        this.zzhue = leaderboardVariant.hasPlayerInfo();
        this.zzhuf = leaderboardVariant.getRawPlayerScore();
        this.zzhug = leaderboardVariant.getDisplayPlayerScore();
        this.zzhuh = leaderboardVariant.getPlayerRank();
        this.zzhui = leaderboardVariant.getDisplayPlayerRank();
        this.zzhuj = leaderboardVariant.getPlayerScoreTag();
        this.zzhuk = leaderboardVariant.getNumScores();
        this.zzhul = leaderboardVariant.zzats();
        this.zzhum = leaderboardVariant.zzatt();
        this.zzhun = leaderboardVariant.zzatu();
    }

    static int zza(LeaderboardVariant leaderboardVariant) {
        return Arrays.hashCode(new Object[]{Integer.valueOf(leaderboardVariant.getTimeSpan()), Integer.valueOf(leaderboardVariant.getCollection()), Boolean.valueOf(leaderboardVariant.hasPlayerInfo()), Long.valueOf(leaderboardVariant.getRawPlayerScore()), leaderboardVariant.getDisplayPlayerScore(), Long.valueOf(leaderboardVariant.getPlayerRank()), leaderboardVariant.getDisplayPlayerRank(), Long.valueOf(leaderboardVariant.getNumScores()), leaderboardVariant.zzats(), leaderboardVariant.zzatu(), leaderboardVariant.zzatt()});
    }

    static boolean zza(LeaderboardVariant leaderboardVariant, Object obj) {
        if (!(obj instanceof LeaderboardVariant)) {
            return false;
        }
        if (leaderboardVariant == obj) {
            return true;
        }
        LeaderboardVariant leaderboardVariant2 = (LeaderboardVariant) obj;
        return zzbg.equal(Integer.valueOf(leaderboardVariant2.getTimeSpan()), Integer.valueOf(leaderboardVariant.getTimeSpan())) && zzbg.equal(Integer.valueOf(leaderboardVariant2.getCollection()), Integer.valueOf(leaderboardVariant.getCollection())) && zzbg.equal(Boolean.valueOf(leaderboardVariant2.hasPlayerInfo()), Boolean.valueOf(leaderboardVariant.hasPlayerInfo())) && zzbg.equal(Long.valueOf(leaderboardVariant2.getRawPlayerScore()), Long.valueOf(leaderboardVariant.getRawPlayerScore())) && zzbg.equal(leaderboardVariant2.getDisplayPlayerScore(), leaderboardVariant.getDisplayPlayerScore()) && zzbg.equal(Long.valueOf(leaderboardVariant2.getPlayerRank()), Long.valueOf(leaderboardVariant.getPlayerRank())) && zzbg.equal(leaderboardVariant2.getDisplayPlayerRank(), leaderboardVariant.getDisplayPlayerRank()) && zzbg.equal(Long.valueOf(leaderboardVariant2.getNumScores()), Long.valueOf(leaderboardVariant.getNumScores())) && zzbg.equal(leaderboardVariant2.zzats(), leaderboardVariant.zzats()) && zzbg.equal(leaderboardVariant2.zzatu(), leaderboardVariant.zzatu()) && zzbg.equal(leaderboardVariant2.zzatt(), leaderboardVariant.zzatt());
    }

    static String zzb(LeaderboardVariant leaderboardVariant) {
        String str;
        zzbi zzg = zzbg.zzw(leaderboardVariant).zzg("TimeSpan", zzcab.zzdm(leaderboardVariant.getTimeSpan()));
        int collection = leaderboardVariant.getCollection();
        switch (collection) {
            case -1:
                str = "UNKNOWN";
                break;
            case 0:
                str = "PUBLIC";
                break;
            case 1:
                str = "SOCIAL";
                break;
            case 2:
                str = "SOCIAL_1P";
                break;
            default:
                StringBuilder sb = new StringBuilder(43);
                sb.append("Unknown leaderboard collection: ");
                sb.append(collection);
                throw new IllegalArgumentException(sb.toString());
        }
        return zzg.zzg("Collection", str).zzg("RawPlayerScore", leaderboardVariant.hasPlayerInfo() ? Long.valueOf(leaderboardVariant.getRawPlayerScore()) : "none").zzg("DisplayPlayerScore", leaderboardVariant.hasPlayerInfo() ? leaderboardVariant.getDisplayPlayerScore() : "none").zzg("PlayerRank", leaderboardVariant.hasPlayerInfo() ? Long.valueOf(leaderboardVariant.getPlayerRank()) : "none").zzg("DisplayPlayerRank", leaderboardVariant.hasPlayerInfo() ? leaderboardVariant.getDisplayPlayerRank() : "none").zzg("NumScores", Long.valueOf(leaderboardVariant.getNumScores())).zzg("TopPageNextToken", leaderboardVariant.zzats()).zzg("WindowPageNextToken", leaderboardVariant.zzatu()).zzg("WindowPagePrevToken", leaderboardVariant.zzatt()).toString();
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    public final /* bridge */ /* synthetic */ Object freeze() {
        if (this != null) {
            return this;
        }
        throw null;
    }

    public final int getCollection() {
        return this.zzhud;
    }

    public final String getDisplayPlayerRank() {
        return this.zzhui;
    }

    public final String getDisplayPlayerScore() {
        return this.zzhug;
    }

    public final long getNumScores() {
        return this.zzhuk;
    }

    public final long getPlayerRank() {
        return this.zzhuh;
    }

    public final String getPlayerScoreTag() {
        return this.zzhuj;
    }

    public final long getRawPlayerScore() {
        return this.zzhuf;
    }

    public final int getTimeSpan() {
        return this.zzhuc;
    }

    public final boolean hasPlayerInfo() {
        return this.zzhue;
    }

    public final int hashCode() {
        return zza(this);
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String toString() {
        return zzb(this);
    }

    public final String zzats() {
        return this.zzhul;
    }

    public final String zzatt() {
        return this.zzhum;
    }

    public final String zzatu() {
        return this.zzhun;
    }
}
