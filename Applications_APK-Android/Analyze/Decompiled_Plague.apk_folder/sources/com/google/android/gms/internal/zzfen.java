package com.google.android.gms.internal;

public interface zzfen {
    double zza(boolean z, double d, boolean z2, double d2);

    int zza(boolean z, int i, boolean z2, int i2);

    long zza(boolean z, long j, boolean z2, long j2);

    zzfdh zza(boolean z, zzfdh zzfdh, boolean z2, zzfdh zzfdh2);

    zzfeu zza(zzfeu zzfeu, zzfeu zzfeu2);

    <T> zzfev<T> zza(zzfev zzfev, zzfev zzfev2);

    <K, V> zzffh<K, V> zza(zzffh zzffh, zzffh zzffh2);

    <T extends zzffi> T zza(zzffi zzffi, zzffi zzffi2);

    zzfgi zza(zzfgi zzfgi, zzfgi zzfgi2);

    Object zza(boolean z, Object obj, Object obj2);

    String zza(boolean z, String str, boolean z2, String str2);

    boolean zza(boolean z, boolean z2, boolean z3, boolean z4);

    Object zzb(boolean z, Object obj, Object obj2);

    Object zzc(boolean z, Object obj, Object obj2);

    Object zzd(boolean z, Object obj, Object obj2);

    void zzdb(boolean z);

    Object zze(boolean z, Object obj, Object obj2);

    Object zzf(boolean z, Object obj, Object obj2);

    Object zzg(boolean z, Object obj, Object obj2);
}
