package com.google.android.gms.internal;

public final class zzbel extends RuntimeException {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzbel(java.lang.String r4, android.os.Parcel r5) {
        /*
            r3 = this;
            int r0 = r5.dataPosition()
            int r5 = r5.dataSize()
            java.lang.String r1 = java.lang.String.valueOf(r4)
            int r1 = r1.length()
            r2 = 41
            int r2 = r2 + r1
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r2)
            r1.append(r4)
            java.lang.String r4 = " Parcel: pos="
            r1.append(r4)
            r1.append(r0)
            java.lang.String r4 = " size="
            r1.append(r4)
            r1.append(r5)
            java.lang.String r4 = r1.toString()
            r3.<init>(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzbel.<init>(java.lang.String, android.os.Parcel):void");
    }
}
