package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public final class zzdl {
    public static final Status zzfsm = new Status(8, "The connection to Google Play services was lost");
    private static final zzs<?>[] zzfsn = new zzs[0];
    private final Map<Api.zzc<?>, Api.zze> zzfph;
    final Set<zzs<?>> zzfso = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
    private final zzdo zzfsp = new zzdm(this);

    public zzdl(Map<Api.zzc<?>, Api.zze> map) {
        this.zzfph = map;
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [com.google.android.gms.common.api.ResultCallback, com.google.android.gms.common.api.internal.zzdo, com.google.android.gms.common.api.zze, com.google.android.gms.common.api.internal.zzdm] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final void release() {
        /*
            r8 = this;
            java.util.Set<com.google.android.gms.common.api.internal.zzs<?>> r0 = r8.zzfso
            com.google.android.gms.common.api.internal.zzs<?>[] r1 = com.google.android.gms.common.api.internal.zzdl.zzfsn
            java.lang.Object[] r0 = r0.toArray(r1)
            com.google.android.gms.common.api.internal.zzs[] r0 = (com.google.android.gms.common.api.internal.zzs[]) r0
            r1 = 0
            int r2 = r0.length
            r3 = r1
        L_0x000d:
            if (r3 >= r2) goto L_0x0075
            r4 = r0[r3]
            r5 = 0
            r4.zza(r5)
            java.lang.Integer r6 = r4.zzagi()
            if (r6 != 0) goto L_0x0027
            boolean r5 = r4.zzagv()
            if (r5 == 0) goto L_0x0072
        L_0x0021:
            java.util.Set<com.google.android.gms.common.api.internal.zzs<?>> r5 = r8.zzfso
            r5.remove(r4)
            goto L_0x0072
        L_0x0027:
            r4.setResultCallback(r5)
            java.util.Map<com.google.android.gms.common.api.Api$zzc<?>, com.google.android.gms.common.api.Api$zze> r6 = r8.zzfph
            r7 = r4
            com.google.android.gms.common.api.internal.zzm r7 = (com.google.android.gms.common.api.internal.zzm) r7
            com.google.android.gms.common.api.Api$zzc r7 = r7.zzaft()
            java.lang.Object r6 = r6.get(r7)
            com.google.android.gms.common.api.Api$zze r6 = (com.google.android.gms.common.api.Api.zze) r6
            android.os.IBinder r6 = r6.zzafv()
            boolean r7 = r4.isReady()
            if (r7 == 0) goto L_0x004c
            com.google.android.gms.common.api.internal.zzdn r7 = new com.google.android.gms.common.api.internal.zzdn
            r7.<init>(r4, r5, r6, r5)
            r4.zza(r7)
            goto L_0x0021
        L_0x004c:
            if (r6 == 0) goto L_0x0060
            boolean r7 = r6.isBinderAlive()
            if (r7 == 0) goto L_0x0060
            com.google.android.gms.common.api.internal.zzdn r7 = new com.google.android.gms.common.api.internal.zzdn
            r7.<init>(r4, r5, r6, r5)
            r4.zza(r7)
            r6.linkToDeath(r7, r1)     // Catch:{ RemoteException -> 0x0063 }
            goto L_0x0021
        L_0x0060:
            r4.zza(r5)
        L_0x0063:
            r4.cancel()
            java.lang.Integer r6 = r4.zzagi()
            int r6 = r6.intValue()
            r5.remove(r6)
            goto L_0x0021
        L_0x0072:
            int r3 = r3 + 1
            goto L_0x000d
        L_0x0075:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzdl.release():void");
    }

    public final void zzaji() {
        for (zzs zzv : (zzs[]) this.zzfso.toArray(zzfsn)) {
            zzv.zzv(zzfsm);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzb(zzs<? extends Result> zzs) {
        this.zzfso.add(zzs);
        zzs.zza(this.zzfsp);
    }
}
