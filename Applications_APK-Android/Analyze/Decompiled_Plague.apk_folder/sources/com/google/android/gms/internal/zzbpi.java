package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.zza;

public abstract class zzbpi extends zzee implements zzbph {
    public zzbpi() {
        attachInterface(this, "com.google.android.gms.drive.internal.IDriveServiceCallbacks");
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        Parcelable.Creator creator;
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                zza((zzbpw) zzef.zza(parcel, zzbpw.CREATOR));
                break;
            case 2:
                zza((zzbqe) zzef.zza(parcel, zzbqe.CREATOR));
                break;
            case 3:
                zza((zzbpy) zzef.zza(parcel, zzbpy.CREATOR));
                break;
            case 4:
                zza((zzbqj) zzef.zza(parcel, zzbqj.CREATOR));
                break;
            case 5:
                zza((zzbps) zzef.zza(parcel, zzbps.CREATOR));
                break;
            case 6:
                onError((Status) zzef.zza(parcel, Status.CREATOR));
                break;
            case 7:
                onSuccess();
                break;
            case 8:
                zza((zzbqg) zzef.zza(parcel, zzbqg.CREATOR));
                break;
            case 9:
                creator = zzbqs.CREATOR;
                zzef.zza(parcel, creator);
                break;
            case 10:
            case 19:
            default:
                return false;
            case 11:
                zzef.zza(parcel, zzbqi.CREATOR);
                zzbtb.zzao(parcel.readStrongBinder());
                break;
            case 12:
                creator = zzbqo.CREATOR;
                zzef.zza(parcel, creator);
                break;
            case 13:
                zza((zzbql) zzef.zza(parcel, zzbql.CREATOR));
                break;
            case 14:
                zza((zzbpu) zzef.zza(parcel, zzbpu.CREATOR));
                break;
            case 15:
                zzef.zza(parcel);
                break;
            case 16:
                creator = zzbqc.CREATOR;
                zzef.zza(parcel, creator);
                break;
            case 17:
                creator = zza.CREATOR;
                zzef.zza(parcel, creator);
                break;
            case 18:
                creator = zzbpq.CREATOR;
                zzef.zza(parcel, creator);
                break;
            case 20:
                creator = zzbpd.CREATOR;
                zzef.zza(parcel, creator);
                break;
            case 21:
                creator = zzbrk.CREATOR;
                zzef.zza(parcel, creator);
                break;
            case 22:
                creator = zzbqq.CREATOR;
                zzef.zza(parcel, creator);
                break;
        }
        parcel2.writeNoException();
        return true;
    }
}
