package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Api;

public final class zzcwc implements Api.ApiOptions.Optional {
    public static final zzcwc zzjyz = new zzcwc(false, false, null, false, null, false, null, null);
    private final boolean zzecm = false;
    private final String zzecn = null;
    private final boolean zzeen = false;
    private final String zzeeo = null;
    private final boolean zzjza = false;
    private final boolean zzjzb = false;
    private final Long zzjzc = null;
    private final Long zzjzd = null;

    static {
        new zzcwd();
    }

    private zzcwc(boolean z, boolean z2, String str, boolean z3, String str2, boolean z4, Long l, Long l2) {
    }

    public final String getServerClientId() {
        return this.zzecn;
    }

    public final boolean isIdTokenRequested() {
        return this.zzecm;
    }

    public final boolean zzbcq() {
        return this.zzjza;
    }

    public final boolean zzbcr() {
        return this.zzeen;
    }

    @Nullable
    public final String zzbcs() {
        return this.zzeeo;
    }

    public final boolean zzbct() {
        return this.zzjzb;
    }

    @Nullable
    public final Long zzbcu() {
        return this.zzjzc;
    }

    @Nullable
    public final Long zzbcv() {
        return this.zzjzd;
    }
}
