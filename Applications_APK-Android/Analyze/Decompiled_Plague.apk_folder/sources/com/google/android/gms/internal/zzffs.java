package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Stack;

final class zzffs implements Iterator<zzfdn> {
    private final Stack<zzffp> zzpdn;
    private zzfdn zzpdo;

    private zzffs(zzfdh zzfdh) {
        this.zzpdn = new Stack<>();
        this.zzpdo = zzap(zzfdh);
    }

    private final zzfdn zzap(zzfdh zzfdh) {
        while (zzfdh instanceof zzffp) {
            zzffp zzffp = (zzffp) zzfdh;
            this.zzpdn.push(zzffp);
            zzfdh = zzffp.zzpdi;
        }
        return (zzfdn) zzfdh;
    }

    private final zzfdn zzcwh() {
        while (!this.zzpdn.isEmpty()) {
            zzfdn zzap = zzap(this.zzpdn.pop().zzpdj);
            if (!zzap.isEmpty()) {
                return zzap;
            }
        }
        return null;
    }

    public final boolean hasNext() {
        return this.zzpdo != null;
    }

    public final /* synthetic */ Object next() {
        if (this.zzpdo == null) {
            throw new NoSuchElementException();
        }
        zzfdn zzfdn = this.zzpdo;
        this.zzpdo = zzcwh();
        return zzfdn;
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
