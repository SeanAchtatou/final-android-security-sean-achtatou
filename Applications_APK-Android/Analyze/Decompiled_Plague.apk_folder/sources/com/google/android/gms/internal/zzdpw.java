package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdpy;
import java.io.IOException;

public final class zzdpw extends zzfee<zzdpw, zza> implements zzffk {
    private static volatile zzffm<zzdpw> zzbas;
    /* access modifiers changed from: private */
    public static final zzdpw zzlqm;
    private zzdpy zzlqi;
    private int zzlql;

    public static final class zza extends zzfef<zzdpw, zza> implements zzffk {
        private zza() {
            super(zzdpw.zzlqm);
        }

        /* synthetic */ zza(zzdpx zzdpx) {
            this();
        }
    }

    static {
        zzdpw zzdpw = new zzdpw();
        zzlqm = zzdpw;
        zzdpw.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdpw.zzpbs.zzbim();
    }

    private zzdpw() {
    }

    public static zzdpw zzblx() {
        return zzlqm;
    }

    public static zzdpw zzm(zzfdh zzfdh) throws zzfew {
        return (zzdpw) zzfee.zza(zzlqm, zzfdh);
    }

    public final int getKeySize() {
        return this.zzlql;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdpy.zza zza2;
        boolean z = false;
        switch (zzdpx.zzbaq[i - 1]) {
            case 1:
                return new zzdpw();
            case 2:
                return zzlqm;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdpw zzdpw = (zzdpw) obj2;
                this.zzlqi = (zzdpy) zzfen.zza(this.zzlqi, zzdpw.zzlqi);
                boolean z2 = this.zzlql != 0;
                int i2 = this.zzlql;
                if (zzdpw.zzlql != 0) {
                    z = true;
                }
                this.zzlql = zzfen.zza(z2, i2, z, zzdpw.zzlql);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 10) {
                                    if (this.zzlqi != null) {
                                        zzdpy zzdpy = this.zzlqi;
                                        zzfef zzfef = (zzfef) zzdpy.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdpy);
                                        zza2 = (zzdpy.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlqi = (zzdpy) zzfdq.zza(zzdpy.zzbma(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlqi);
                                        this.zzlqi = (zzdpy) zza2.zzcvj();
                                    }
                                } else if (zzcts == 16) {
                                    this.zzlql = zzfdq.zzcub();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdpw.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlqm);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlqm;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlqi != null) {
            zzfdv.zza(1, this.zzlqi == null ? zzdpy.zzbma() : this.zzlqi);
        }
        if (this.zzlql != 0) {
            zzfdv.zzab(2, this.zzlql);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzdpy zzbls() {
        return this.zzlqi == null ? zzdpy.zzbma() : this.zzlqi;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlqi != null) {
            i2 = 0 + zzfdv.zzb(1, this.zzlqi == null ? zzdpy.zzbma() : this.zzlqi);
        }
        if (this.zzlql != 0) {
            i2 += zzfdv.zzae(2, this.zzlql);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
