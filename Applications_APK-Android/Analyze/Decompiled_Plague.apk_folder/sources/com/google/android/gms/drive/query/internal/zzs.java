package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbek;
import java.util.ArrayList;
import java.util.List;

public final class zzs implements Parcelable.Creator<zzr> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        zzx zzx = null;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    zzx = (zzx) zzbek.zza(parcel, readInt, zzx.CREATOR);
                    break;
                case 2:
                    arrayList = zzbek.zzc(parcel, readInt, FilterHolder.CREATOR);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzr(zzx, (List<FilterHolder>) arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzr[i];
    }
}
