package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.events.OpenFileCallback;

final /* synthetic */ class zzboc implements zzbnx {
    private final zzbob zzgna;
    private final Status zzgnb;

    zzboc(zzbob zzbob, Status status) {
        this.zzgna = zzbob;
        this.zzgnb = status;
    }

    public final void accept(Object obj) {
        this.zzgna.zza(this.zzgnb, (OpenFileCallback) obj);
    }
}
