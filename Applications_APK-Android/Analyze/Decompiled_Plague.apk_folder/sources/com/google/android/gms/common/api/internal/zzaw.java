package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzj;

final class zzaw extends zzbm {
    private /* synthetic */ zzj zzfow;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzaw(zzau zzau, zzbk zzbk, zzj zzj) {
        super(zzbk);
        this.zzfow = zzj;
    }

    public final void zzahp() {
        this.zzfow.zzf(new ConnectionResult(16, null));
    }
}
