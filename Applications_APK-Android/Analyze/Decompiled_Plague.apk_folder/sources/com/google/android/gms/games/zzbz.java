package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.Snapshots;

final class zzbz implements zzbo<Snapshots.CommitSnapshotResult, SnapshotMetadata> {
    zzbz() {
    }

    public final /* synthetic */ Object zzb(@Nullable Result result) {
        SnapshotMetadata snapshotMetadata;
        Snapshots.CommitSnapshotResult commitSnapshotResult = (Snapshots.CommitSnapshotResult) result;
        if (commitSnapshotResult == null || (snapshotMetadata = commitSnapshotResult.getSnapshotMetadata()) == null) {
            return null;
        }
        return (SnapshotMetadata) snapshotMetadata.freeze();
    }
}
