package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzf extends zzb<Boolean> {
    private zzcn<?> zzfla;

    public zzf(zzcn<?> zzcn, TaskCompletionSource<Boolean> taskCompletionSource) {
        super(4, taskCompletionSource);
        this.zzfla = zzcn;
    }

    public final /* bridge */ /* synthetic */ void zza(@NonNull zzah zzah, boolean z) {
    }

    public final void zzb(zzbr<?> zzbr) throws RemoteException {
        zzcu remove = zzbr.zzaim().remove(this.zzfla);
        if (remove != null) {
            remove.zzfkx.zzc(zzbr.zzahd(), this.zzeay);
            remove.zzfkw.zzajd();
            return;
        }
        this.zzeay.trySetResult(false);
    }

    public final /* bridge */ /* synthetic */ void zzs(@NonNull Status status) {
        super.zzs(status);
    }
}
