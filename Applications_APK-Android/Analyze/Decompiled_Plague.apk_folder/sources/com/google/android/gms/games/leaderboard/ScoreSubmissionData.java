package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzbi;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzcab;
import java.util.HashMap;

public final class ScoreSubmissionData {
    private static final String[] zzhth = {"leaderboardId", "playerId", "timeSpan", "hasResult", "rawScore", "formattedScore", "newBest", "scoreTag"};
    private String zzfcj;
    private int zzfcq;
    private String zzhtj;
    private HashMap<Integer, Result> zzhuo = new HashMap<>();

    public static final class Result {
        public final String formattedScore;
        public final boolean newBest;
        public final long rawScore;
        public final String scoreTag;

        public Result(long j, String str, String str2, boolean z) {
            this.rawScore = j;
            this.formattedScore = str;
            this.scoreTag = str2;
            this.newBest = z;
        }

        public final String toString() {
            return zzbg.zzw(this).zzg("RawScore", Long.valueOf(this.rawScore)).zzg("FormattedScore", this.formattedScore).zzg("ScoreTag", this.scoreTag).zzg("NewBest", Boolean.valueOf(this.newBest)).toString();
        }
    }

    public ScoreSubmissionData(DataHolder dataHolder) {
        this.zzfcq = dataHolder.getStatusCode();
        int count = dataHolder.getCount();
        zzbq.checkArgument(count == 3);
        for (int i = 0; i < count; i++) {
            int zzbz = dataHolder.zzbz(i);
            if (i == 0) {
                this.zzhtj = dataHolder.zzd("leaderboardId", i, zzbz);
                this.zzfcj = dataHolder.zzd("playerId", i, zzbz);
            }
            if (dataHolder.zze("hasResult", i, zzbz)) {
                this.zzhuo.put(Integer.valueOf(dataHolder.zzc("timeSpan", i, zzbz)), new Result(dataHolder.zzb("rawScore", i, zzbz), dataHolder.zzd("formattedScore", i, zzbz), dataHolder.zzd("scoreTag", i, zzbz), dataHolder.zze("newBest", i, zzbz)));
            }
        }
    }

    public final String getLeaderboardId() {
        return this.zzhtj;
    }

    public final String getPlayerId() {
        return this.zzfcj;
    }

    public final Result getScoreResult(int i) {
        return this.zzhuo.get(Integer.valueOf(i));
    }

    public final String toString() {
        zzbi zzg = zzbg.zzw(this).zzg("PlayerId", this.zzfcj).zzg("StatusCode", Integer.valueOf(this.zzfcq));
        for (int i = 0; i < 3; i++) {
            Result result = this.zzhuo.get(Integer.valueOf(i));
            zzg.zzg("TimesSpan", zzcab.zzdm(i));
            zzg.zzg("Result", result == null ? "null" : result.toString());
        }
        return zzg.toString();
    }
}
