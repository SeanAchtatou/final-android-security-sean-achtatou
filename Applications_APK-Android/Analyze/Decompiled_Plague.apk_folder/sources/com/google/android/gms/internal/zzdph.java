package com.google.android.gms.internal;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;

public final class zzdph implements zzdos<zzdor> {
    zzdph() {
    }

    /* access modifiers changed from: private */
    /* renamed from: zzf */
    public final zzdor zza(zzfdh zzfdh) throws GeneralSecurityException {
        try {
            zzdqu zzv = zzdqu.zzv(zzfdh);
            if (!(zzv instanceof zzdqu)) {
                throw new GeneralSecurityException("expected EciesAeadHkdfPublicKey proto");
            }
            zzdqu zzdqu = zzv;
            zzdte.zzt(zzdqu.getVersion(), 0);
            zzdqs zzbmy = zzdqu.zzbmy();
            zzdsi.zza(zzdoy.zza(zzbmy.zzbmt().zzbnc()));
            zzdoy.zza(zzbmy.zzbmt().zzbnd());
            if (zzbmy.zzbmv() == zzdqo.UNKNOWN_FORMAT) {
                throw new GeneralSecurityException("unknown EC point format");
            }
            zzdpa.zzlpq.zza(zzbmy.zzbmu().zzbmq());
            zzdqs zzbmy2 = zzdqu.zzbmy();
            zzdqw zzbmt = zzbmy2.zzbmt();
            zzdsk zza = zzdoy.zza(zzbmt.zzbnc());
            byte[] byteArray = zzdqu.zzbmz().toByteArray();
            byte[] byteArray2 = zzdqu.zzbna().toByteArray();
            ECParameterSpec zza2 = zzdsi.zza(zza);
            ECPoint eCPoint = new ECPoint(new BigInteger(1, byteArray), new BigInteger(1, byteArray2));
            zzdsi.zza(eCPoint, zza2.getCurve());
            ECPublicKeySpec eCPublicKeySpec = new ECPublicKeySpec(eCPoint, zza2);
            return new zzdsn((ECPublicKey) KeyFactory.getInstance("EC").generatePublic(eCPublicKeySpec), zzbmt.zzbne().toByteArray(), zzdoy.zza(zzbmt.zzbnd()), zzdoy.zza(zzbmy2.zzbmv()), new zzdpl(zzbmy2.zzbmu().zzbmq()));
        } catch (zzfew e) {
            throw new GeneralSecurityException("expected serialized EciesAeadHkdfPublicKey proto", e);
        }
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.EciesAeadHkdfPublicKey";
    }

    public final /* synthetic */ Object zza(zzffi zzffi) throws GeneralSecurityException {
        if (!(zzffi instanceof zzdqu)) {
            throw new GeneralSecurityException("expected EciesAeadHkdfPublicKey proto");
        }
        zzdqu zzdqu = (zzdqu) zzffi;
        zzdte.zzt(zzdqu.getVersion(), 0);
        zzdqs zzbmy = zzdqu.zzbmy();
        zzdsi.zza(zzdoy.zza(zzbmy.zzbmt().zzbnc()));
        zzdoy.zza(zzbmy.zzbmt().zzbnd());
        if (zzbmy.zzbmv() == zzdqo.UNKNOWN_FORMAT) {
            throw new GeneralSecurityException("unknown EC point format");
        }
        zzdpa.zzlpq.zza(zzbmy.zzbmu().zzbmq());
        zzdqs zzbmy2 = zzdqu.zzbmy();
        zzdqw zzbmt = zzbmy2.zzbmt();
        zzdsk zza = zzdoy.zza(zzbmt.zzbnc());
        byte[] byteArray = zzdqu.zzbmz().toByteArray();
        byte[] byteArray2 = zzdqu.zzbna().toByteArray();
        ECParameterSpec zza2 = zzdsi.zza(zza);
        ECPoint eCPoint = new ECPoint(new BigInteger(1, byteArray), new BigInteger(1, byteArray2));
        zzdsi.zza(eCPoint, zza2.getCurve());
        ECPublicKeySpec eCPublicKeySpec = new ECPublicKeySpec(eCPoint, zza2);
        return new zzdsn((ECPublicKey) KeyFactory.getInstance("EC").generatePublic(eCPublicKeySpec), zzbmt.zzbne().toByteArray(), zzdoy.zza(zzbmt.zzbnd()), zzdoy.zza(zzbmy2.zzbmv()), new zzdpl(zzbmy2.zzbmu().zzbmq()));
    }

    public final zzffi zzb(zzfdh zzfdh) throws GeneralSecurityException {
        throw new GeneralSecurityException("Not implemented.");
    }

    public final zzffi zzb(zzffi zzffi) throws GeneralSecurityException {
        throw new GeneralSecurityException("Not implemented.");
    }

    public final zzdrk zzc(zzfdh zzfdh) throws GeneralSecurityException {
        throw new GeneralSecurityException("Not implemented.");
    }
}
