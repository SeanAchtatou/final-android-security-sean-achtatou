package com.google.android.gms.common.images;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.internal.zzbei;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class ImageManager {
    /* access modifiers changed from: private */
    public static final Object zzftx = new Object();
    /* access modifiers changed from: private */
    public static HashSet<Uri> zzfty = new HashSet<>();
    private static ImageManager zzftz;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final ExecutorService zzfua = Executors.newFixedThreadPool(4);
    /* access modifiers changed from: private */
    public final zza zzfub = null;
    /* access modifiers changed from: private */
    public final zzbei zzfuc = new zzbei();
    /* access modifiers changed from: private */
    public final Map<zza, ImageReceiver> zzfud = new HashMap();
    /* access modifiers changed from: private */
    public final Map<Uri, ImageReceiver> zzfue = new HashMap();
    /* access modifiers changed from: private */
    public final Map<Uri, Long> zzfuf = new HashMap();

    @KeepName
    final class ImageReceiver extends ResultReceiver {
        private final Uri mUri;
        /* access modifiers changed from: private */
        public final ArrayList<zza> zzfug = new ArrayList<>();

        ImageReceiver(Uri uri) {
            super(new Handler(Looper.getMainLooper()));
            this.mUri = uri;
        }

        public final void onReceiveResult(int i, Bundle bundle) {
            ImageManager.this.zzfua.execute(new zzb(this.mUri, (ParcelFileDescriptor) bundle.getParcelable("com.google.android.gms.extra.fileDescriptor")));
        }

        public final void zzajr() {
            Intent intent = new Intent("com.google.android.gms.common.images.LOAD_IMAGE");
            intent.putExtra("com.google.android.gms.extras.uri", this.mUri);
            intent.putExtra("com.google.android.gms.extras.resultReceiver", this);
            intent.putExtra("com.google.android.gms.extras.priority", 3);
            ImageManager.this.mContext.sendBroadcast(intent);
        }

        public final void zzb(zza zza) {
            com.google.android.gms.common.internal.zzc.zzfz("ImageReceiver.addImageRequest() must be called in the main thread");
            this.zzfug.add(zza);
        }

        public final void zzc(zza zza) {
            com.google.android.gms.common.internal.zzc.zzfz("ImageReceiver.removeImageRequest() must be called in the main thread");
            this.zzfug.remove(zza);
        }
    }

    public interface OnImageLoadedListener {
        void onImageLoaded(Uri uri, Drawable drawable, boolean z);
    }

    static final class zza extends LruCache<zzb, Bitmap> {
        /* access modifiers changed from: protected */
        public final /* synthetic */ void entryRemoved(boolean z, Object obj, Object obj2, Object obj3) {
            super.entryRemoved(z, (zzb) obj, (Bitmap) obj2, (Bitmap) obj3);
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ int sizeOf(Object obj, Object obj2) {
            Bitmap bitmap = (Bitmap) obj2;
            return bitmap.getHeight() * bitmap.getRowBytes();
        }
    }

    final class zzb implements Runnable {
        private final Uri mUri;
        private final ParcelFileDescriptor zzfui;

        public zzb(Uri uri, ParcelFileDescriptor parcelFileDescriptor) {
            this.mUri = uri;
            this.zzfui = parcelFileDescriptor;
        }

        public final void run() {
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                String valueOf = String.valueOf(Thread.currentThread());
                String valueOf2 = String.valueOf(Looper.getMainLooper().getThread());
                StringBuilder sb = new StringBuilder(56 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
                sb.append("checkNotMainThread: current thread ");
                sb.append(valueOf);
                sb.append(" IS the main thread ");
                sb.append(valueOf2);
                sb.append("!");
                Log.e("Asserts", sb.toString());
                throw new IllegalStateException("LoadBitmapFromDiskRunnable can't be executed in the main thread");
            }
            boolean z = false;
            Bitmap bitmap = null;
            if (this.zzfui != null) {
                try {
                    bitmap = BitmapFactory.decodeFileDescriptor(this.zzfui.getFileDescriptor());
                } catch (OutOfMemoryError e) {
                    String valueOf3 = String.valueOf(this.mUri);
                    StringBuilder sb2 = new StringBuilder(34 + String.valueOf(valueOf3).length());
                    sb2.append("OOM while loading bitmap for uri: ");
                    sb2.append(valueOf3);
                    Log.e("ImageManager", sb2.toString(), e);
                    z = true;
                }
                try {
                    this.zzfui.close();
                } catch (IOException e2) {
                    Log.e("ImageManager", "closed failed", e2);
                }
            }
            boolean z2 = z;
            CountDownLatch countDownLatch = new CountDownLatch(1);
            ImageManager.this.mHandler.post(new zzd(this.mUri, bitmap, z2, countDownLatch));
            try {
                countDownLatch.await();
            } catch (InterruptedException unused) {
                String valueOf4 = String.valueOf(this.mUri);
                StringBuilder sb3 = new StringBuilder(32 + String.valueOf(valueOf4).length());
                sb3.append("Latch interrupted while posting ");
                sb3.append(valueOf4);
                Log.w("ImageManager", sb3.toString());
            }
        }
    }

    final class zzc implements Runnable {
        private final zza zzfuj;

        public zzc(zza zza) {
            this.zzfuj = zza;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.images.zza.zza(android.content.Context, com.google.android.gms.internal.zzbei, boolean):void
         arg types: [android.content.Context, com.google.android.gms.internal.zzbei, int]
         candidates:
          com.google.android.gms.common.images.zza.zza(android.content.Context, android.graphics.Bitmap, boolean):void
          com.google.android.gms.common.images.zza.zza(android.content.Context, com.google.android.gms.internal.zzbei, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.images.zza.zza(android.content.Context, android.graphics.Bitmap, boolean):void
         arg types: [android.content.Context, android.graphics.Bitmap, int]
         candidates:
          com.google.android.gms.common.images.zza.zza(android.content.Context, com.google.android.gms.internal.zzbei, boolean):void
          com.google.android.gms.common.images.zza.zza(android.content.Context, android.graphics.Bitmap, boolean):void */
        public final void run() {
            com.google.android.gms.common.internal.zzc.zzfz("LoadImageRunnable must be executed on the main thread");
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.zzfud.get(this.zzfuj);
            if (imageReceiver != null) {
                ImageManager.this.zzfud.remove(this.zzfuj);
                imageReceiver.zzc(this.zzfuj);
            }
            zzb zzb = this.zzfuj.zzful;
            if (zzb.uri == null) {
                this.zzfuj.zza(ImageManager.this.mContext, ImageManager.this.zzfuc, true);
                return;
            }
            Bitmap zza = ImageManager.this.zza(zzb);
            if (zza != null) {
                this.zzfuj.zza(ImageManager.this.mContext, zza, true);
                return;
            }
            Long l = (Long) ImageManager.this.zzfuf.get(zzb.uri);
            if (l != null) {
                if (SystemClock.elapsedRealtime() - l.longValue() < 3600000) {
                    this.zzfuj.zza(ImageManager.this.mContext, ImageManager.this.zzfuc, true);
                    return;
                }
                ImageManager.this.zzfuf.remove(zzb.uri);
            }
            this.zzfuj.zza(ImageManager.this.mContext, ImageManager.this.zzfuc);
            ImageReceiver imageReceiver2 = (ImageReceiver) ImageManager.this.zzfue.get(zzb.uri);
            if (imageReceiver2 == null) {
                imageReceiver2 = new ImageReceiver(zzb.uri);
                ImageManager.this.zzfue.put(zzb.uri, imageReceiver2);
            }
            imageReceiver2.zzb(this.zzfuj);
            if (!(this.zzfuj instanceof zzd)) {
                ImageManager.this.zzfud.put(this.zzfuj, imageReceiver2);
            }
            synchronized (ImageManager.zzftx) {
                if (!ImageManager.zzfty.contains(zzb.uri)) {
                    ImageManager.zzfty.add(zzb.uri);
                    imageReceiver2.zzajr();
                }
            }
        }
    }

    final class zzd implements Runnable {
        private final Bitmap mBitmap;
        private final Uri mUri;
        private final CountDownLatch zzaoi;
        private boolean zzfuk;

        public zzd(Uri uri, Bitmap bitmap, boolean z, CountDownLatch countDownLatch) {
            this.mUri = uri;
            this.mBitmap = bitmap;
            this.zzfuk = z;
            this.zzaoi = countDownLatch;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.images.zza.zza(android.content.Context, android.graphics.Bitmap, boolean):void
         arg types: [android.content.Context, android.graphics.Bitmap, int]
         candidates:
          com.google.android.gms.common.images.zza.zza(android.content.Context, com.google.android.gms.internal.zzbei, boolean):void
          com.google.android.gms.common.images.zza.zza(android.content.Context, android.graphics.Bitmap, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.images.zza.zza(android.content.Context, com.google.android.gms.internal.zzbei, boolean):void
         arg types: [android.content.Context, com.google.android.gms.internal.zzbei, int]
         candidates:
          com.google.android.gms.common.images.zza.zza(android.content.Context, android.graphics.Bitmap, boolean):void
          com.google.android.gms.common.images.zza.zza(android.content.Context, com.google.android.gms.internal.zzbei, boolean):void */
        public final void run() {
            com.google.android.gms.common.internal.zzc.zzfz("OnBitmapLoadedRunnable must be executed in the main thread");
            boolean z = this.mBitmap != null;
            if (ImageManager.this.zzfub != null) {
                if (this.zzfuk) {
                    ImageManager.this.zzfub.evictAll();
                    System.gc();
                    this.zzfuk = false;
                    ImageManager.this.mHandler.post(this);
                    return;
                } else if (z) {
                    ImageManager.this.zzfub.put(new zzb(this.mUri), this.mBitmap);
                }
            }
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.zzfue.remove(this.mUri);
            if (imageReceiver != null) {
                ArrayList zza = imageReceiver.zzfug;
                int size = zza.size();
                for (int i = 0; i < size; i++) {
                    zza zza2 = (zza) zza.get(i);
                    if (z) {
                        zza2.zza(ImageManager.this.mContext, this.mBitmap, false);
                    } else {
                        ImageManager.this.zzfuf.put(this.mUri, Long.valueOf(SystemClock.elapsedRealtime()));
                        zza2.zza(ImageManager.this.mContext, ImageManager.this.zzfuc, false);
                    }
                    if (!(zza2 instanceof zzd)) {
                        ImageManager.this.zzfud.remove(zza2);
                    }
                }
            }
            this.zzaoi.countDown();
            synchronized (ImageManager.zzftx) {
                ImageManager.zzfty.remove(this.mUri);
            }
        }
    }

    private ImageManager(Context context, boolean z) {
        this.mContext = context.getApplicationContext();
    }

    public static ImageManager create(Context context) {
        if (zzftz == null) {
            zzftz = new ImageManager(context, false);
        }
        return zzftz;
    }

    /* access modifiers changed from: private */
    public final Bitmap zza(zzb zzb2) {
        if (this.zzfub == null) {
            return null;
        }
        return (Bitmap) this.zzfub.get(zzb2);
    }

    private final void zza(zza zza2) {
        com.google.android.gms.common.internal.zzc.zzfz("ImageManager.loadImage() must be called in the main thread");
        new zzc(zza2).run();
    }

    public final void loadImage(ImageView imageView, int i) {
        zza(new zzc(imageView, i));
    }

    public final void loadImage(ImageView imageView, Uri uri) {
        zza(new zzc(imageView, uri));
    }

    public final void loadImage(ImageView imageView, Uri uri, int i) {
        zzc zzc2 = new zzc(imageView, uri);
        zzc2.zzfun = i;
        zza(zzc2);
    }

    public final void loadImage(OnImageLoadedListener onImageLoadedListener, Uri uri) {
        zza(new zzd(onImageLoadedListener, uri));
    }

    public final void loadImage(OnImageLoadedListener onImageLoadedListener, Uri uri, int i) {
        zzd zzd2 = new zzd(onImageLoadedListener, uri);
        zzd2.zzfun = i;
        zza(zzd2);
    }
}
