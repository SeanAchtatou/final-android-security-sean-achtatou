package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.drive.DriveFolder;

final class zzbmj extends zzbjv {
    private final zzn<DriveFolder.DriveFolderResult> zzfzc;

    public zzbmj(zzn<DriveFolder.DriveFolderResult> zzn) {
        this.zzfzc = zzn;
    }

    public final void onError(Status status) throws RemoteException {
        this.zzfzc.setResult(new zzbmm(status, null));
    }

    public final void zza(zzbpy zzbpy) throws RemoteException {
        this.zzfzc.setResult(new zzbmm(Status.zzfko, new zzbmf(zzbpy.zzgjm)));
    }
}
