package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzzb;
import com.tapjoy.TapjoyAuctionFlags;
import java.util.Map;

@zzzb
public final class zzw implements zzt<Object> {
    private final zzx zzbwi;

    public zzw(zzx zzx) {
        this.zzbwi = zzx;
    }

    public final void zza(Object obj, Map<String, String> map) {
        boolean equals = TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE.equals(map.get("transparentBackground"));
        boolean equals2 = TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE.equals(map.get("blur"));
        float f = 0.0f;
        try {
            if (map.get("blurRadius") != null) {
                f = Float.parseFloat(map.get("blurRadius"));
            }
        } catch (NumberFormatException e) {
            zzafj.zzb("Fail to parse float", e);
        }
        this.zzbwi.zzd(equals);
        this.zzbwi.zza(equals2, f);
    }
}
