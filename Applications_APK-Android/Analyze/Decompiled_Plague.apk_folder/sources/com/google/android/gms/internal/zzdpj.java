package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class zzdpj {
    private static final Logger logger = Logger.getLogger(zzdpj.class.getName());

    static {
        try {
            zzdpb.zzbli();
            zzdpp.zzbli();
        } catch (GeneralSecurityException e) {
            Logger logger2 = logger;
            Level level = Level.SEVERE;
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(30 + String.valueOf(valueOf).length());
            sb.append("cannot register key managers: ");
            sb.append(valueOf);
            logger2.logp(level, "com.google.crypto.tink.hybrid.HybridEncryptFactory", "<clinit>", sb.toString());
        }
    }

    public static zzdor zza(zzdot zzdot) throws GeneralSecurityException {
        return new zzdpk(zzdpa.zzlpq.zza(zzdot, (zzdos) null));
    }
}
