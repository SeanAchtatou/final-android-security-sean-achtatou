package com.google.android.gms.common.data;

import java.util.ArrayList;

public abstract class zzg<T> extends AbstractDataBuffer<T> {
    private boolean zzftu = false;
    private ArrayList<Integer> zzftv;

    protected zzg(DataHolder dataHolder) {
        super(dataHolder);
    }

    private final void zzajp() {
        synchronized (this) {
            if (!this.zzftu) {
                int i = this.zzfnz.zzftm;
                this.zzftv = new ArrayList<>();
                if (i > 0) {
                    this.zzftv.add(0);
                    String zzajo = zzajo();
                    String zzd = this.zzfnz.zzd(zzajo, 0, this.zzfnz.zzbz(0));
                    for (int i2 = 1; i2 < i; i2++) {
                        int zzbz = this.zzfnz.zzbz(i2);
                        String zzd2 = this.zzfnz.zzd(zzajo, i2, zzbz);
                        if (zzd2 == null) {
                            StringBuilder sb = new StringBuilder(78 + String.valueOf(zzajo).length());
                            sb.append("Missing value for markerColumn: ");
                            sb.append(zzajo);
                            sb.append(", at row: ");
                            sb.append(i2);
                            sb.append(", for window: ");
                            sb.append(zzbz);
                            throw new NullPointerException(sb.toString());
                        }
                        if (!zzd2.equals(zzd)) {
                            this.zzftv.add(Integer.valueOf(i2));
                            zzd = zzd2;
                        }
                    }
                }
                this.zzftu = true;
            }
        }
    }

    private final int zzcc(int i) {
        if (i >= 0 && i < this.zzftv.size()) {
            return this.zzftv.get(i).intValue();
        }
        StringBuilder sb = new StringBuilder(53);
        sb.append("Position ");
        sb.append(i);
        sb.append(" is out of bounds for this buffer");
        throw new IllegalArgumentException(sb.toString());
    }

    public final T get(int i) {
        int i2;
        zzajp();
        int zzcc = zzcc(i);
        if (i < 0 || i == this.zzftv.size()) {
            i2 = 0;
        } else {
            i2 = (i == this.zzftv.size() - 1 ? this.zzfnz.zzftm : this.zzftv.get(i + 1).intValue()) - this.zzftv.get(i).intValue();
            if (i2 == 1) {
                this.zzfnz.zzbz(zzcc(i));
            }
        }
        return zzl(zzcc, i2);
    }

    public int getCount() {
        zzajp();
        return this.zzftv.size();
    }

    /* access modifiers changed from: protected */
    public abstract String zzajo();

    /* access modifiers changed from: protected */
    public abstract T zzl(int i, int i2);
}
