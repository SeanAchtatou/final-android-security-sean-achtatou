package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.multiplayer.turnbased.LoadMatchesResponse;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzcd implements zzbo<TurnBasedMultiplayer.LoadMatchesResult, LoadMatchesResponse> {
    zzcd() {
    }

    public final /* synthetic */ Object zzb(@Nullable Result result) {
        TurnBasedMultiplayer.LoadMatchesResult loadMatchesResult = (TurnBasedMultiplayer.LoadMatchesResult) result;
        if (loadMatchesResult == null) {
            return null;
        }
        return loadMatchesResult.getMatches();
    }
}
