package com.google.android.gms.internal;

import android.content.ComponentName;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsServiceConnection;
import java.lang.ref.WeakReference;

public final class zzfks extends CustomTabsServiceConnection {
    private WeakReference<zzfkt> zzqqa;

    public zzfks(zzfkt zzfkt) {
        this.zzqqa = new WeakReference<>(zzfkt);
    }

    public final void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
        zzfkt zzfkt = this.zzqqa.get();
        if (zzfkt != null) {
            zzfkt.zza(customTabsClient);
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        zzfkt zzfkt = this.zzqqa.get();
        if (zzfkt != null) {
            zzfkt.zzje();
        }
    }
}
