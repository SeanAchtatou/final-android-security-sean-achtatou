package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.zzy;
import com.google.android.gms.internal.zzbem;

public final class zzl extends zzy implements DriveEvent {
    public static final Parcelable.Creator<zzl> CREATOR = new zzm();
    private DataHolder zzfnz;
    private boolean zzgja;
    private int zzgjb;

    public zzl(DataHolder dataHolder, boolean z, int i) {
        this.zzfnz = dataHolder;
        this.zzgja = z;
        this.zzgjb = i;
    }

    public final int getType() {
        return 3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.data.DataHolder, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void zzaj(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, (Parcelable) this.zzfnz, i, false);
        zzbem.zza(parcel, 3, this.zzgja);
        zzbem.zzc(parcel, 4, this.zzgjb);
        zzbem.zzai(parcel, zze);
    }

    public final DataHolder zzaoi() {
        return this.zzfnz;
    }

    public final boolean zzaoj() {
        return this.zzgja;
    }

    public final int zzaok() {
        return this.zzgjb;
    }
}
