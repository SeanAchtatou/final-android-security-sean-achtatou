package com.google.android.gms.internal;

import java.io.IOException;

public final class zzdrp extends zzfee<zzdrp, zza> implements zzffk {
    private static volatile zzffm<zzdrp> zzbas;
    /* access modifiers changed from: private */
    public static final zzdrp zzltf;
    private String zzlso = "";
    private zzfdh zzlsp = zzfdh.zzpal;

    public static final class zza extends zzfef<zzdrp, zza> implements zzffk {
        private zza() {
            super(zzdrp.zzltf);
        }

        /* synthetic */ zza(zzdrq zzdrq) {
            this();
        }
    }

    static {
        zzdrp zzdrp = new zzdrp();
        zzltf = zzdrp;
        zzdrp.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdrp.zzpbs.zzbim();
    }

    private zzdrp() {
    }

    public static zzdrp zzbny() {
        return zzltf;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        boolean z = true;
        boolean z2 = false;
        switch (zzdrq.zzbaq[i - 1]) {
            case 1:
                return new zzdrp();
            case 2:
                return zzltf;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdrp zzdrp = (zzdrp) obj2;
                this.zzlso = zzfen.zza(!this.zzlso.isEmpty(), this.zzlso, !zzdrp.zzlso.isEmpty(), zzdrp.zzlso);
                boolean z3 = this.zzlsp != zzfdh.zzpal;
                zzfdh zzfdh = this.zzlsp;
                if (zzdrp.zzlsp == zzfdh.zzpal) {
                    z = false;
                }
                this.zzlsp = zzfen.zza(z3, zzfdh, z, zzdrp.zzlsp);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                if (((zzfea) obj2) != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 10) {
                                    this.zzlso = zzfdq.zzctz();
                                } else if (zzcts == 18) {
                                    this.zzlsp = zzfdq.zzcua();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdrp.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzltf);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzltf;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (!this.zzlso.isEmpty()) {
            zzfdv.zzn(1, this.zzlso);
        }
        if (!this.zzlsp.isEmpty()) {
            zzfdv.zza(2, this.zzlsp);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final String zzbns() {
        return this.zzlso;
    }

    public final zzfdh zzbnt() {
        return this.zzlsp;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (!this.zzlso.isEmpty()) {
            i2 = 0 + zzfdv.zzo(1, this.zzlso);
        }
        if (!this.zzlsp.isEmpty()) {
            i2 += zzfdv.zzb(2, this.zzlsp);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
