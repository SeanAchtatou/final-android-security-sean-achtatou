package com.google.android.gms.games;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;

final class zzbp implements Continuation<String, Task<Boolean>> {
    private /* synthetic */ zzcl zzhko;
    private /* synthetic */ RealTimeMultiplayerClient zzhkp;

    zzbp(RealTimeMultiplayerClient realTimeMultiplayerClient, zzcl zzcl) {
        this.zzhkp = realTimeMultiplayerClient;
        this.zzhko = zzcl;
    }

    public final /* synthetic */ Object then(@NonNull Task task) throws Exception {
        return this.zzhkp.zza(this.zzhko.zzajc());
    }
}
