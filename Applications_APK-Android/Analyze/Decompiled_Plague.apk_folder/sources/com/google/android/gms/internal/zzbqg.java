package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.zzy;

public final class zzbqg extends zzy {
    public static final Parcelable.Creator<zzbqg> CREATOR = new zzbqh();
    final DataHolder zzgok;

    public zzbqg(DataHolder dataHolder) {
        this.zzgok = dataHolder;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.data.DataHolder, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* access modifiers changed from: protected */
    public final void zzaj(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, (Parcelable) this.zzgok, i, false);
        zzbem.zzai(parcel, zze);
    }

    public final DataHolder zzaov() {
        return this.zzgok;
    }
}
