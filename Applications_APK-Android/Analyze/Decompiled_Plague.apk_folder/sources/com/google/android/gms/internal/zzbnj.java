package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzcn;
import com.google.android.gms.common.api.internal.zzdp;
import com.google.android.gms.drive.events.OpenFileCallback;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbnj extends zzdp<zzbll, OpenFileCallback> {
    private /* synthetic */ zzbjq zzgmg;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbnj(zzbmu zzbmu, zzcn zzcn, zzbjq zzbjq) {
        super(zzcn);
        this.zzgmg = zzbjq;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zzc(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        taskCompletionSource.setResult(Boolean.valueOf(this.zzgmg.cancel()));
    }
}
