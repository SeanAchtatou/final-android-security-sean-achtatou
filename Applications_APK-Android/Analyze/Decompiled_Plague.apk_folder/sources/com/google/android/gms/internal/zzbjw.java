package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.zzc;
import com.google.android.gms.drive.zzr;

public final class zzbjw extends zzbej {
    public static final Parcelable.Creator<zzbjw> CREATOR = new zzbjx();
    private String zzgha;
    private boolean zzghb;
    private boolean zzghf;
    private DriveId zzgjm;
    private MetadataBundle zzgjn;
    private zzc zzgjo;
    private int zzgjp;
    private int zzgjq;
    private boolean zzgjr;

    public zzbjw(DriveId driveId, MetadataBundle metadataBundle, int i, boolean z, zzr zzr) {
        this(driveId, metadataBundle, null, zzr.zzant(), zzr.zzans(), zzr.zzanu(), i, z, zzr.zzany());
    }

    zzbjw(DriveId driveId, MetadataBundle metadataBundle, zzc zzc, boolean z, String str, int i, int i2, boolean z2, boolean z3) {
        this.zzgjm = driveId;
        this.zzgjn = metadataBundle;
        this.zzgjo = zzc;
        this.zzghb = z;
        this.zzgha = str;
        this.zzgjp = i;
        this.zzgjq = i2;
        this.zzgjr = z2;
        this.zzghf = z3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.DriveId, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.metadata.internal.MetadataBundle, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.zzc, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, (Parcelable) this.zzgjm, i, false);
        zzbem.zza(parcel, 3, (Parcelable) this.zzgjn, i, false);
        zzbem.zza(parcel, 4, (Parcelable) this.zzgjo, i, false);
        zzbem.zza(parcel, 5, this.zzghb);
        zzbem.zza(parcel, 6, this.zzgha, false);
        zzbem.zzc(parcel, 7, this.zzgjp);
        zzbem.zzc(parcel, 8, this.zzgjq);
        zzbem.zza(parcel, 9, this.zzgjr);
        zzbem.zza(parcel, 10, this.zzghf);
        zzbem.zzai(parcel, zze);
    }
}
