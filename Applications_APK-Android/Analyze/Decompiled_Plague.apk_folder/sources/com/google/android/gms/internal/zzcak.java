package com.google.android.gms.internal;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.HandlerThread;
import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzg;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

final class zzcak implements zzf, zzg {
    private final String packageName;
    private zzcal zzhyr;
    private final String zzhys;
    private final LinkedBlockingQueue<zzaw> zzhyt;
    private final HandlerThread zzhyu = new HandlerThread("GassClient");

    public zzcak(Context context, String str, String str2) {
        this.packageName = str;
        this.zzhys = str2;
        this.zzhyu.start();
        this.zzhyr = new zzcal(context, this.zzhyu.getLooper(), this, this);
        this.zzhyt = new LinkedBlockingQueue<>();
        this.zzhyr.zzajx();
    }

    private final zzcaq zzauc() {
        try {
            return this.zzhyr.zzaue();
        } catch (DeadObjectException | IllegalStateException unused) {
            return null;
        }
    }

    private static zzaw zzaud() {
        zzaw zzaw = new zzaw();
        zzaw.zzdn = Long.valueOf((long) PlaybackStateCompat.ACTION_PREPARE_FROM_MEDIA_ID);
        return zzaw;
    }

    private final void zzne() {
        if (this.zzhyr == null) {
            return;
        }
        if (this.zzhyr.isConnected() || this.zzhyr.isConnecting()) {
            this.zzhyr.disconnect();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0031, code lost:
        zzne();
        r3.zzhyu.quit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0039, code lost:
        throw r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0025, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0027 */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0025 A[ExcHandler: all (r4v3 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:2:0x0006] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onConnected(android.os.Bundle r4) {
        /*
            r3 = this;
            com.google.android.gms.internal.zzcaq r4 = r3.zzauc()
            if (r4 == 0) goto L_0x003a
            com.google.android.gms.internal.zzcam r0 = new com.google.android.gms.internal.zzcam     // Catch:{ Throwable -> 0x0027, all -> 0x0025 }
            java.lang.String r1 = r3.packageName     // Catch:{ Throwable -> 0x0027, all -> 0x0025 }
            java.lang.String r2 = r3.zzhys     // Catch:{ Throwable -> 0x0027, all -> 0x0025 }
            r0.<init>(r1, r2)     // Catch:{ Throwable -> 0x0027, all -> 0x0025 }
            com.google.android.gms.internal.zzcao r4 = r4.zza(r0)     // Catch:{ Throwable -> 0x0027, all -> 0x0025 }
            com.google.android.gms.internal.zzaw r4 = r4.zzauf()     // Catch:{ Throwable -> 0x0027, all -> 0x0025 }
            java.util.concurrent.LinkedBlockingQueue<com.google.android.gms.internal.zzaw> r0 = r3.zzhyt     // Catch:{ Throwable -> 0x0027, all -> 0x0025 }
            r0.put(r4)     // Catch:{ Throwable -> 0x0027, all -> 0x0025 }
        L_0x001c:
            r3.zzne()
            android.os.HandlerThread r4 = r3.zzhyu
            r4.quit()
            return
        L_0x0025:
            r4 = move-exception
            goto L_0x0031
        L_0x0027:
            java.util.concurrent.LinkedBlockingQueue<com.google.android.gms.internal.zzaw> r4 = r3.zzhyt     // Catch:{ InterruptedException -> 0x001c, all -> 0x0025 }
            com.google.android.gms.internal.zzaw r0 = zzaud()     // Catch:{ InterruptedException -> 0x001c, all -> 0x0025 }
            r4.put(r0)     // Catch:{ InterruptedException -> 0x001c, all -> 0x0025 }
            goto L_0x001c
        L_0x0031:
            r3.zzne()
            android.os.HandlerThread r0 = r3.zzhyu
            r0.quit()
            throw r4
        L_0x003a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzcak.onConnected(android.os.Bundle):void");
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        try {
            this.zzhyt.put(zzaud());
        } catch (InterruptedException unused) {
        }
    }

    public final void onConnectionSuspended(int i) {
        try {
            this.zzhyt.put(zzaud());
        } catch (InterruptedException unused) {
        }
    }

    public final zzaw zzdo(int i) {
        zzaw zzaw;
        try {
            zzaw = this.zzhyt.poll(5000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException unused) {
            zzaw = null;
        }
        return zzaw == null ? zzaud() : zzaw;
    }
}
