package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;

public final class zzdi extends zzec {
    private static volatile String zzajk;
    private static final Object zzajl = new Object();

    public zzdi(zzda zzda, String str, String str2, zzaw zzaw, int i, int i2) {
        super(zzda, str, str2, zzaw, i, 29);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbr.zza(byte[], boolean):java.lang.String
     arg types: [byte[], int]
     candidates:
      com.google.android.gms.internal.zzbr.zza(java.lang.String, boolean):byte[]
      com.google.android.gms.internal.zzbr.zza(byte[], boolean):java.lang.String */
    /* access modifiers changed from: protected */
    public final void zzat() throws IllegalAccessException, InvocationTargetException {
        this.zzajo.zzdq = "E";
        if (zzajk == null) {
            synchronized (zzajl) {
                if (zzajk == null) {
                    zzajk = (String) this.zzajx.invoke(null, this.zzagk.getContext());
                }
            }
        }
        synchronized (this.zzajo) {
            this.zzajo.zzdq = zzbr.zza(zzajk.getBytes(), true);
        }
    }
}
