package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfed;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class zzfeb<FieldDescriptorType extends zzfed<FieldDescriptorType>> {
    private static final zzfeb zzpbp = new zzfeb(true);
    private boolean zzkqq;
    private final zzffu<FieldDescriptorType, Object> zzpbn = zzffu.zzlp(16);
    private boolean zzpbo = false;

    private zzfeb() {
    }

    private zzfeb(boolean z) {
        if (!this.zzkqq) {
            this.zzpbn.zzbim();
            this.zzkqq = true;
        }
    }

    static int zza(zzfgr zzfgr, int i, Object obj) {
        int i2;
        int zzkw = zzfdv.zzkw(i);
        if (zzfgr == zzfgr.GROUP) {
            zzfer.zzg((zzffi) obj);
            zzkw <<= 1;
        }
        switch (zzfec.zzpbr[zzfgr.ordinal()]) {
            case 1:
                i2 = zzfdv.zzn(((Double) obj).doubleValue());
                break;
            case 2:
                i2 = zzfdv.zzf(((Float) obj).floatValue());
                break;
            case 3:
                i2 = zzfdv.zzcv(((Long) obj).longValue());
                break;
            case 4:
                i2 = zzfdv.zzcw(((Long) obj).longValue());
                break;
            case 5:
                i2 = zzfdv.zzkx(((Integer) obj).intValue());
                break;
            case 6:
                i2 = zzfdv.zzcy(((Long) obj).longValue());
                break;
            case 7:
                i2 = zzfdv.zzla(((Integer) obj).intValue());
                break;
            case 8:
                i2 = zzfdv.zzda(((Boolean) obj).booleanValue());
                break;
            case 9:
                i2 = zzfdv.zzf((zzffi) obj);
                break;
            case 10:
                if (!(obj instanceof zzfey)) {
                    i2 = zzfdv.zze((zzffi) obj);
                    break;
                } else {
                    i2 = zzfdv.zza((zzfey) obj);
                    break;
                }
            case 11:
                if (!(obj instanceof zzfdh)) {
                    i2 = zzfdv.zztd((String) obj);
                    break;
                }
                i2 = zzfdv.zzan((zzfdh) obj);
                break;
            case 12:
                if (!(obj instanceof zzfdh)) {
                    i2 = zzfdv.zzbc((byte[]) obj);
                    break;
                }
                i2 = zzfdv.zzan((zzfdh) obj);
                break;
            case 13:
                i2 = zzfdv.zzky(((Integer) obj).intValue());
                break;
            case 14:
                i2 = zzfdv.zzlb(((Integer) obj).intValue());
                break;
            case 15:
                i2 = zzfdv.zzcz(((Long) obj).longValue());
                break;
            case 16:
                i2 = zzfdv.zzkz(((Integer) obj).intValue());
                break;
            case 17:
                i2 = zzfdv.zzcx(((Long) obj).longValue());
                break;
            case 18:
                i2 = zzfdv.zzlc(obj instanceof zzfes ? ((zzfes) obj).zzhn() : ((Integer) obj).intValue());
                break;
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
        return zzkw + i2;
    }

    public static Object zza(zzfdq zzfdq, zzfgr zzfgr, boolean z) throws IOException {
        zzfgx zzfgx = zzfgx.STRICT;
        switch (zzfgq.zzpbr[zzfgr.ordinal()]) {
            case 1:
                return Double.valueOf(zzfdq.readDouble());
            case 2:
                return Float.valueOf(zzfdq.readFloat());
            case 3:
                return Long.valueOf(zzfdq.zzctu());
            case 4:
                return Long.valueOf(zzfdq.zzctt());
            case 5:
                return Integer.valueOf(zzfdq.zzctv());
            case 6:
                return Long.valueOf(zzfdq.zzctw());
            case 7:
                return Integer.valueOf(zzfdq.zzctx());
            case 8:
                return Boolean.valueOf(zzfdq.zzcty());
            case 9:
                return zzfdq.zzcua();
            case 10:
                return Integer.valueOf(zzfdq.zzcub());
            case 11:
                return Integer.valueOf(zzfdq.zzcud());
            case 12:
                return Long.valueOf(zzfdq.zzcue());
            case 13:
                return Integer.valueOf(zzfdq.zzcuf());
            case 14:
                return Long.valueOf(zzfdq.zzcug());
            case 15:
                return zzfgx.zzb(zzfdq);
            case 16:
                throw new IllegalArgumentException("readPrimitiveField() cannot handle nested groups.");
            case 17:
                throw new IllegalArgumentException("readPrimitiveField() cannot handle embedded messages.");
            case 18:
                throw new IllegalArgumentException("readPrimitiveField() cannot handle enums.");
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    static void zza(zzfdv zzfdv, zzfgr zzfgr, int i, Object obj) throws IOException {
        if (zzfgr == zzfgr.GROUP) {
            zzffi zzffi = (zzffi) obj;
            zzfer.zzg(zzffi);
            zzfdv.zzz(i, 3);
            zzffi.zza(zzfdv);
            zzfdv.zzz(i, 4);
            return;
        }
        zzfdv.zzz(i, zzfgr.zzcxd());
        switch (zzfec.zzpbr[zzfgr.ordinal()]) {
            case 1:
                zzfdv.zzcu(Double.doubleToRawLongBits(((Double) obj).doubleValue()));
                return;
            case 2:
                zzfdv.zzkv(Float.floatToRawIntBits(((Float) obj).floatValue()));
                return;
            case 3:
                zzfdv.zzcs(((Long) obj).longValue());
                return;
            case 4:
                zzfdv.zzcs(((Long) obj).longValue());
                return;
            case 5:
                zzfdv.zzks(((Integer) obj).intValue());
                return;
            case 6:
                zzfdv.zzcu(((Long) obj).longValue());
                return;
            case 7:
                zzfdv.zzkv(((Integer) obj).intValue());
                return;
            case 8:
                zzfdv.zzb(((Boolean) obj).booleanValue() ? (byte) 1 : 0);
                return;
            case 9:
                ((zzffi) obj).zza(zzfdv);
                return;
            case 10:
                zzfdv.zzd((zzffi) obj);
                return;
            case 11:
                if (obj instanceof zzfdh) {
                    zzfdv.zzam((zzfdh) obj);
                    return;
                } else {
                    zzfdv.zztc((String) obj);
                    return;
                }
            case 12:
                if (obj instanceof zzfdh) {
                    zzfdv.zzam((zzfdh) obj);
                    return;
                }
                byte[] bArr = (byte[]) obj;
                zzfdv.zzi(bArr, 0, bArr.length);
                return;
            case 13:
                zzfdv.zzkt(((Integer) obj).intValue());
                return;
            case 14:
                zzfdv.zzkv(((Integer) obj).intValue());
                return;
            case 15:
                zzfdv.zzcu(((Long) obj).longValue());
                return;
            case 16:
                zzfdv.zzku(((Integer) obj).intValue());
                return;
            case 17:
                zzfdv.zzct(((Long) obj).longValue());
                return;
            case 18:
                if (obj instanceof zzfes) {
                    zzfdv.zzks(((zzfes) obj).zzhn());
                    return;
                } else {
                    zzfdv.zzks(((Integer) obj).intValue());
                    return;
                }
            default:
                return;
        }
    }

    private void zza(FieldDescriptorType fielddescriptortype, Object obj) {
        if (!fielddescriptortype.zzcvc()) {
            zza(fielddescriptortype.zzcvb(), obj);
        } else if (!(obj instanceof List)) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        } else {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList2.get(i);
                i++;
                zza(fielddescriptortype.zzcvb(), obj2);
            }
            obj = arrayList;
        }
        if (obj instanceof zzfey) {
            this.zzpbo = true;
        }
        this.zzpbn.put(fielddescriptortype, obj);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        if ((r3 instanceof byte[]) == false) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001b, code lost:
        if ((r3 instanceof com.google.android.gms.internal.zzfey) == false) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if ((r3 instanceof com.google.android.gms.internal.zzfes) == false) goto L_0x0043;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void zza(com.google.android.gms.internal.zzfgr r2, java.lang.Object r3) {
        /*
            com.google.android.gms.internal.zzfer.checkNotNull(r3)
            int[] r0 = com.google.android.gms.internal.zzfec.zzpbq
            com.google.android.gms.internal.zzfgw r2 = r2.zzcxc()
            int r2 = r2.ordinal()
            r2 = r0[r2]
            r0 = 1
            r1 = 0
            switch(r2) {
                case 1: goto L_0x0040;
                case 2: goto L_0x003d;
                case 3: goto L_0x003a;
                case 4: goto L_0x0037;
                case 5: goto L_0x0034;
                case 6: goto L_0x0031;
                case 7: goto L_0x0028;
                case 8: goto L_0x001e;
                case 9: goto L_0x0015;
                default: goto L_0x0014;
            }
        L_0x0014:
            goto L_0x0043
        L_0x0015:
            boolean r2 = r3 instanceof com.google.android.gms.internal.zzffi
            if (r2 != 0) goto L_0x0026
            boolean r2 = r3 instanceof com.google.android.gms.internal.zzfey
            if (r2 == 0) goto L_0x0043
            goto L_0x0026
        L_0x001e:
            boolean r2 = r3 instanceof java.lang.Integer
            if (r2 != 0) goto L_0x0026
            boolean r2 = r3 instanceof com.google.android.gms.internal.zzfes
            if (r2 == 0) goto L_0x0043
        L_0x0026:
            r1 = r0
            goto L_0x0043
        L_0x0028:
            boolean r2 = r3 instanceof com.google.android.gms.internal.zzfdh
            if (r2 != 0) goto L_0x0026
            boolean r2 = r3 instanceof byte[]
            if (r2 == 0) goto L_0x0043
            goto L_0x0026
        L_0x0031:
            boolean r0 = r3 instanceof java.lang.String
            goto L_0x0026
        L_0x0034:
            boolean r0 = r3 instanceof java.lang.Boolean
            goto L_0x0026
        L_0x0037:
            boolean r0 = r3 instanceof java.lang.Double
            goto L_0x0026
        L_0x003a:
            boolean r0 = r3 instanceof java.lang.Float
            goto L_0x0026
        L_0x003d:
            boolean r0 = r3 instanceof java.lang.Long
            goto L_0x0026
        L_0x0040:
            boolean r0 = r3 instanceof java.lang.Integer
            goto L_0x0026
        L_0x0043:
            if (r1 != 0) goto L_0x004d
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Wrong object type used with protocol message reflection."
            r2.<init>(r3)
            throw r2
        L_0x004d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfeb.zza(com.google.android.gms.internal.zzfgr, java.lang.Object):void");
    }

    public static <T extends zzfed<T>> zzfeb<T> zzcva() {
        return new zzfeb<>();
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        zzfeb zzfeb = new zzfeb();
        for (int i = 0; i < this.zzpbn.zzcwj(); i++) {
            Map.Entry<FieldDescriptorType, Object> zzlq = this.zzpbn.zzlq(i);
            zzfeb.zza((zzfed) zzlq.getKey(), zzlq.getValue());
        }
        for (Map.Entry next : this.zzpbn.zzcwk()) {
            zzfeb.zza((zzfed) next.getKey(), next.getValue());
        }
        zzfeb.zzpbo = this.zzpbo;
        return zzfeb;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzfeb)) {
            return false;
        }
        return this.zzpbn.equals(((zzfeb) obj).zzpbn);
    }

    public final int hashCode() {
        return this.zzpbn.hashCode();
    }

    public final Iterator<Map.Entry<FieldDescriptorType, Object>> iterator() {
        return this.zzpbo ? new zzffb(this.zzpbn.entrySet().iterator()) : this.zzpbn.entrySet().iterator();
    }
}
