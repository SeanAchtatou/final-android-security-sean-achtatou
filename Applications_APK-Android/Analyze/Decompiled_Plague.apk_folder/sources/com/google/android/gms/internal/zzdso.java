package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECParameterSpec;
import javax.crypto.KeyAgreement;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public final class zzdso {
    private ECPublicKey zzlvd;

    public zzdso(ECPublicKey eCPublicKey) {
        this.zzlvd = eCPublicKey;
    }

    public final zzdsp zza(String str, byte[] bArr, byte[] bArr2, int i, zzdsl zzdsl) throws GeneralSecurityException {
        ECParameterSpec params = this.zzlvd.getParams();
        KeyPairGenerator zzoc = zzdsr.zzlvp.zzoc("EC");
        zzoc.initialize(params);
        KeyPair generateKeyPair = zzoc.generateKeyPair();
        ECPublicKey eCPublicKey = (ECPublicKey) generateKeyPair.getPublic();
        zzdsi.zza(this.zzlvd.getW(), this.zzlvd.getParams().getCurve());
        KeyAgreement zzoc2 = zzdsr.zzlvo.zzoc("ECDH");
        zzoc2.init((ECPrivateKey) generateKeyPair.getPrivate());
        int i2 = 1;
        zzoc2.doPhase(this.zzlvd, true);
        byte[] generateSecret = zzoc2.generateSecret();
        byte[] zza = zzdsi.zza(eCPublicKey.getParams().getCurve(), zzdsl, eCPublicKey.getW());
        byte[] zzc = zzdte.zzc(zza, generateSecret);
        Mac zzoc3 = zzdsr.zzlvl.zzoc(str);
        if (i > 255 * zzoc3.getMacLength()) {
            throw new GeneralSecurityException("size too large");
        }
        if (bArr == null || bArr.length == 0) {
            zzoc3.init(new SecretKeySpec(new byte[zzoc3.getMacLength()], str));
        } else {
            zzoc3.init(new SecretKeySpec(bArr, str));
        }
        byte[] doFinal = zzoc3.doFinal(zzc);
        byte[] bArr3 = new byte[i];
        zzoc3.init(new SecretKeySpec(doFinal, str));
        byte[] bArr4 = new byte[0];
        int i3 = 0;
        while (true) {
            zzoc3.update(bArr4);
            zzoc3.update(bArr2);
            zzoc3.update((byte) i2);
            bArr4 = zzoc3.doFinal();
            if (bArr4.length + i3 < i) {
                System.arraycopy(bArr4, 0, bArr3, i3, bArr4.length);
                i3 += bArr4.length;
                i2++;
            } else {
                System.arraycopy(bArr4, 0, bArr3, i3, i - i3);
                return new zzdsp(zza, bArr3);
            }
        }
    }
}
