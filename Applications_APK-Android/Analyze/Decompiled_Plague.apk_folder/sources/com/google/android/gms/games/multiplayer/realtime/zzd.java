package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.support.annotation.Nullable;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;

public final class zzd extends RoomConfig {
    private final String zzdyn;
    private final int zzhus;
    @Deprecated
    private final RoomUpdateListener zzhve;
    @Deprecated
    private final RoomStatusUpdateListener zzhvf;
    @Deprecated
    private final RealTimeMessageReceivedListener zzhvg;
    private final RoomUpdateCallback zzhvh;
    private final RoomStatusUpdateCallback zzhvi;
    private final OnRealTimeMessageReceivedListener zzhvj;
    private final Bundle zzhvm;
    private final zzg zzhvn;
    private final String[] zzhvo;

    zzd(RoomConfig.Builder builder) {
        this.zzhve = builder.zzhve;
        this.zzhvf = builder.zzhvf;
        this.zzhvg = builder.zzhvg;
        this.zzhvh = builder.zzhvh;
        this.zzhvi = builder.zzhvi;
        this.zzhvj = builder.zzhvj;
        this.zzhvn = this.zzhvi != null ? new zzg(this.zzhvh, this.zzhvi, this.zzhvj) : null;
        this.zzdyn = builder.zzhvk;
        this.zzhus = builder.zzhus;
        this.zzhvm = builder.zzhvm;
        this.zzhvo = (String[]) builder.zzhvl.toArray(new String[builder.zzhvl.size()]);
        if (this.zzhvj == null && this.zzhvg == null) {
            throw new NullPointerException(String.valueOf("Must specify a message listener"));
        }
    }

    public final Bundle getAutoMatchCriteria() {
        return this.zzhvm;
    }

    public final String getInvitationId() {
        return this.zzdyn;
    }

    public final String[] getInvitedPlayerIds() {
        return this.zzhvo;
    }

    @Nullable
    @Deprecated
    public final RealTimeMessageReceivedListener getMessageReceivedListener() {
        return this.zzhvg;
    }

    @Nullable
    public final OnRealTimeMessageReceivedListener getOnMessageReceivedListener() {
        return this.zzhvj;
    }

    @Nullable
    public final RoomStatusUpdateCallback getRoomStatusUpdateCallback() {
        return this.zzhvi;
    }

    @Nullable
    @Deprecated
    public final RoomStatusUpdateListener getRoomStatusUpdateListener() {
        return this.zzhvf;
    }

    @Nullable
    public final RoomUpdateCallback getRoomUpdateCallback() {
        return this.zzhvh;
    }

    @Nullable
    @Deprecated
    public final RoomUpdateListener getRoomUpdateListener() {
        return this.zzhve;
    }

    public final int getVariant() {
        return this.zzhus;
    }

    public final zzh zzatw() {
        return this.zzhvn;
    }
}
