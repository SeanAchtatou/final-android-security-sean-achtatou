package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzbs;
import org.json.JSONException;
import org.json.JSONObject;

@zzzb
public final class zzalh {
    public final String zzdgk;
    private boolean zzdgl;
    private int zzdgm;
    private int zzdgn;
    private int zzdgo;
    private int zzdgp;
    private boolean zzdgq;

    public zzalh(String str) {
        JSONObject jSONObject = null;
        if (str != null) {
            try {
                jSONObject = new JSONObject(str);
            } catch (JSONException unused) {
            }
        }
        this.zzdgl = zza(jSONObject, "aggressive_media_codec_release", zzmq.zzbhe);
        this.zzdgk = zzc(jSONObject, "exo_player_version", zzmq.zzbgl);
        this.zzdgm = zzb(jSONObject, "exo_cache_buffer_size", zzmq.zzbgs);
        this.zzdgn = zzb(jSONObject, "exo_connect_timeout_millis", zzmq.zzbgm);
        this.zzdgo = zzb(jSONObject, "exo_read_timeout_millis", zzmq.zzbgn);
        this.zzdgp = zzb(jSONObject, "load_check_interval_bytes", zzmq.zzbgo);
        this.zzdgq = zza(jSONObject, "use_cache_data_source", zzmq.zzbop);
    }

    private static boolean zza(JSONObject jSONObject, String str, zzmg<Boolean> zzmg) {
        if (jSONObject != null) {
            try {
                return jSONObject.getBoolean(str);
            } catch (JSONException unused) {
            }
        }
        return ((Boolean) zzbs.zzep().zzd(zzmg)).booleanValue();
    }

    private static int zzb(JSONObject jSONObject, String str, zzmg<Integer> zzmg) {
        if (jSONObject != null) {
            try {
                return jSONObject.getInt(str);
            } catch (JSONException unused) {
            }
        }
        return ((Integer) zzbs.zzep().zzd(zzmg)).intValue();
    }

    private static String zzc(JSONObject jSONObject, String str, zzmg<String> zzmg) {
        if (jSONObject != null) {
            try {
                return jSONObject.getString(str);
            } catch (JSONException unused) {
            }
        }
        return (String) zzbs.zzep().zzd(zzmg);
    }
}
