package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbek;

public final class zza implements Parcelable.Creator<VideoCapabilities> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        boolean[] zArr = null;
        boolean[] zArr2 = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    z = zzbek.zzc(parcel, readInt);
                    break;
                case 2:
                    z2 = zzbek.zzc(parcel, readInt);
                    break;
                case 3:
                    z3 = zzbek.zzc(parcel, readInt);
                    break;
                case 4:
                    zArr = zzbek.zzv(parcel, readInt);
                    break;
                case 5:
                    zArr2 = zzbek.zzv(parcel, readInt);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new VideoCapabilities(z, z2, z3, zArr, zArr2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new VideoCapabilities[i];
    }
}
