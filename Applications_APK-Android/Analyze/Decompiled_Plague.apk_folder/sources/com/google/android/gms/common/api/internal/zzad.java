package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.common.internal.zzt;
import com.google.android.gms.common.zze;
import com.google.android.gms.internal.zzbfx;
import com.google.android.gms.internal.zzcwb;
import com.google.android.gms.internal.zzcwc;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public final class zzad implements zzcf {
    private final Looper zzakm;
    private final zzbp zzfjo;
    /* access modifiers changed from: private */
    public final Lock zzfmy;
    private final zzr zzfnd;
    /* access modifiers changed from: private */
    public final Map<Api.zzc<?>, zzac<?>> zzfne = new HashMap();
    /* access modifiers changed from: private */
    public final Map<Api.zzc<?>, zzac<?>> zzfnf = new HashMap();
    private final Map<Api<?>, Boolean> zzfng;
    /* access modifiers changed from: private */
    public final zzbd zzfnh;
    private final zze zzfni;
    /* access modifiers changed from: private */
    public final Condition zzfnj;
    private final boolean zzfnk;
    /* access modifiers changed from: private */
    public final boolean zzfnl;
    private final Queue<zzm<?, ?>> zzfnm = new LinkedList();
    /* access modifiers changed from: private */
    public boolean zzfnn;
    /* access modifiers changed from: private */
    public Map<zzh<?>, ConnectionResult> zzfno;
    /* access modifiers changed from: private */
    public Map<zzh<?>, ConnectionResult> zzfnp;
    private zzag zzfnq;
    /* access modifiers changed from: private */
    public ConnectionResult zzfnr;

    public zzad(Context context, Lock lock, Looper looper, zze zze, Map<Api.zzc<?>, Api.zze> map, zzr zzr, Map<Api<?>, Boolean> map2, Api.zza<? extends zzcwb, zzcwc> zza, ArrayList<zzw> arrayList, zzbd zzbd, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        this.zzfmy = lock;
        Looper looper2 = looper;
        this.zzakm = looper2;
        this.zzfnj = lock.newCondition();
        this.zzfni = zze;
        this.zzfnh = zzbd;
        this.zzfng = map2;
        zzr zzr2 = zzr;
        this.zzfnd = zzr2;
        this.zzfnk = z;
        HashMap hashMap = new HashMap();
        for (Api next : map2.keySet()) {
            hashMap.put(next.zzaft(), next);
        }
        HashMap hashMap2 = new HashMap();
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            zzw zzw = (zzw) obj;
            hashMap2.put(zzw.zzffv, zzw);
        }
        boolean z5 = false;
        boolean z6 = true;
        boolean z7 = false;
        for (Map.Entry next2 : map.entrySet()) {
            Api api = (Api) hashMap.get(next2.getKey());
            Api.zze zze2 = (Api.zze) next2.getValue();
            if (zze2.zzafu()) {
                if (!this.zzfng.get(api).booleanValue()) {
                    z3 = z6;
                    z4 = true;
                } else {
                    z3 = z6;
                    z4 = z7;
                }
                z2 = true;
            } else {
                z2 = z5;
                z4 = z7;
                z3 = false;
            }
            zzac zzac = r1;
            Api.zze zze3 = zze2;
            Map.Entry entry = next2;
            zzac zzac2 = new zzac(context, api, looper2, zze2, (zzw) hashMap2.get(api), zzr2, zza);
            this.zzfne.put((Api.zzc) entry.getKey(), zzac);
            if (zze3.zzaam()) {
                this.zzfnf.put((Api.zzc) entry.getKey(), zzac);
            }
            z7 = z4;
            z6 = z3;
            z5 = z2;
            looper2 = looper;
        }
        this.zzfnl = z5 && !z6 && !z7;
        this.zzfjo = zzbp.zzaie();
    }

    /* access modifiers changed from: private */
    public final boolean zza(zzac<?> zzac, ConnectionResult connectionResult) {
        return !connectionResult.isSuccess() && !connectionResult.hasResolution() && this.zzfng.get(zzac.zzafy()).booleanValue() && zzac.zzahd().zzafu() && this.zzfni.isUserResolvableError(connectionResult.getErrorCode());
    }

    private final boolean zzahe() {
        this.zzfmy.lock();
        try {
            if (this.zzfnn) {
                if (this.zzfnk) {
                    for (Api.zzc<?> zzb : this.zzfnf.keySet()) {
                        ConnectionResult zzb2 = zzb(zzb);
                        if (zzb2 != null) {
                            if (!zzb2.isSuccess()) {
                            }
                        }
                    }
                    this.zzfmy.unlock();
                    return true;
                }
            }
            return false;
        } finally {
            this.zzfmy.unlock();
        }
    }

    /* access modifiers changed from: private */
    public final void zzahf() {
        if (this.zzfnd == null) {
            this.zzfnh.zzfpi = Collections.emptySet();
            return;
        }
        HashSet hashSet = new HashSet(this.zzfnd.zzakj());
        Map<Api<?>, zzt> zzakl = this.zzfnd.zzakl();
        for (Api next : zzakl.keySet()) {
            ConnectionResult connectionResult = getConnectionResult(next);
            if (connectionResult != null && connectionResult.isSuccess()) {
                hashSet.addAll(zzakl.get(next).zzees);
            }
        }
        this.zzfnh.zzfpi = hashSet;
    }

    /* access modifiers changed from: private */
    public final void zzahg() {
        while (!this.zzfnm.isEmpty()) {
            zze(this.zzfnm.remove());
        }
        this.zzfnh.zzj(null);
    }

    /* access modifiers changed from: private */
    @Nullable
    public final ConnectionResult zzahh() {
        ConnectionResult connectionResult = null;
        int i = 0;
        int i2 = 0;
        ConnectionResult connectionResult2 = null;
        for (zzac next : this.zzfne.values()) {
            Api zzafy = next.zzafy();
            ConnectionResult connectionResult3 = this.zzfno.get(next.zzaga());
            if (!connectionResult3.isSuccess() && (!this.zzfng.get(zzafy).booleanValue() || connectionResult3.hasResolution() || this.zzfni.isUserResolvableError(connectionResult3.getErrorCode()))) {
                if (connectionResult3.getErrorCode() != 4 || !this.zzfnk) {
                    int priority = zzafy.zzafr().getPriority();
                    if (connectionResult == null || i > priority) {
                        connectionResult = connectionResult3;
                        i = priority;
                    }
                } else {
                    int priority2 = zzafy.zzafr().getPriority();
                    if (connectionResult2 == null || i2 > priority2) {
                        connectionResult2 = connectionResult3;
                        i2 = priority2;
                    }
                }
            }
        }
        return (connectionResult == null || connectionResult2 == null || i <= i2) ? connectionResult : connectionResult2;
    }

    @Nullable
    private final ConnectionResult zzb(@NonNull Api.zzc<?> zzc) {
        this.zzfmy.lock();
        try {
            zzac zzac = this.zzfne.get(zzc);
            if (this.zzfno != null && zzac != null) {
                return this.zzfno.get(zzac.zzaga());
            }
            this.zzfmy.unlock();
            return null;
        } finally {
            this.zzfmy.unlock();
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private final <T extends com.google.android.gms.common.api.internal.zzm<? extends com.google.android.gms.common.api.Result, ? extends com.google.android.gms.common.api.Api.zzb>> boolean zzg(@android.support.annotation.NonNull T r7) {
        /*
            r6 = this;
            com.google.android.gms.common.api.Api$zzc r0 = r7.zzaft()
            com.google.android.gms.common.ConnectionResult r1 = r6.zzb(r0)
            if (r1 == 0) goto L_0x0034
            int r1 = r1.getErrorCode()
            r2 = 4
            if (r1 != r2) goto L_0x0034
            com.google.android.gms.common.api.Status r1 = new com.google.android.gms.common.api.Status
            r3 = 0
            com.google.android.gms.common.api.internal.zzbp r4 = r6.zzfjo
            java.util.Map<com.google.android.gms.common.api.Api$zzc<?>, com.google.android.gms.common.api.internal.zzac<?>> r5 = r6.zzfne
            java.lang.Object r0 = r5.get(r0)
            com.google.android.gms.common.api.internal.zzac r0 = (com.google.android.gms.common.api.internal.zzac) r0
            com.google.android.gms.common.api.internal.zzh r0 = r0.zzaga()
            com.google.android.gms.common.api.internal.zzbd r5 = r6.zzfnh
            int r5 = java.lang.System.identityHashCode(r5)
            android.app.PendingIntent r0 = r4.zza(r0, r5)
            r1.<init>(r2, r3, r0)
            r7.zzu(r1)
            r7 = 1
            return r7
        L_0x0034:
            r7 = 0
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zzad.zzg(com.google.android.gms.common.api.internal.zzm):boolean");
    }

    public final ConnectionResult blockingConnect() {
        connect();
        while (isConnecting()) {
            try {
                this.zzfnj.await();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                return new ConnectionResult(15, null);
            }
        }
        return isConnected() ? ConnectionResult.zzfhy : this.zzfnr != null ? this.zzfnr : new ConnectionResult(13, null);
    }

    public final ConnectionResult blockingConnect(long j, TimeUnit timeUnit) {
        connect();
        long nanos = timeUnit.toNanos(j);
        while (isConnecting()) {
            if (nanos <= 0) {
                try {
                    disconnect();
                    return new ConnectionResult(14, null);
                } catch (InterruptedException unused) {
                    Thread.currentThread().interrupt();
                    return new ConnectionResult(15, null);
                }
            } else {
                nanos = this.zzfnj.awaitNanos(nanos);
            }
        }
        return isConnected() ? ConnectionResult.zzfhy : this.zzfnr != null ? this.zzfnr : new ConnectionResult(13, null);
    }

    public final void connect() {
        this.zzfmy.lock();
        try {
            if (!this.zzfnn) {
                this.zzfnn = true;
                this.zzfno = null;
                this.zzfnp = null;
                this.zzfnq = null;
                this.zzfnr = null;
                this.zzfjo.zzagm();
                this.zzfjo.zza(this.zzfne.values()).addOnCompleteListener(new zzbfx(this.zzakm), new zzaf(this));
            }
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final void disconnect() {
        this.zzfmy.lock();
        try {
            this.zzfnn = false;
            this.zzfno = null;
            this.zzfnp = null;
            if (this.zzfnq != null) {
                this.zzfnq.cancel();
                this.zzfnq = null;
            }
            this.zzfnr = null;
            while (!this.zzfnm.isEmpty()) {
                zzm remove = this.zzfnm.remove();
                remove.zza((zzdo) null);
                remove.cancel();
            }
            this.zzfnj.signalAll();
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    @Nullable
    public final ConnectionResult getConnectionResult(@NonNull Api<?> api) {
        return zzb(api.zzaft());
    }

    public final boolean isConnected() {
        this.zzfmy.lock();
        try {
            return this.zzfno != null && this.zzfnr == null;
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final boolean isConnecting() {
        this.zzfmy.lock();
        try {
            return this.zzfno == null && this.zzfnn;
        } finally {
            this.zzfmy.unlock();
        }
    }

    /* JADX INFO: finally extract failed */
    public final boolean zza(zzcx zzcx) {
        this.zzfmy.lock();
        try {
            if (!this.zzfnn || zzahe()) {
                this.zzfmy.unlock();
                return false;
            }
            this.zzfjo.zzagm();
            this.zzfnq = new zzag(this, zzcx);
            this.zzfjo.zza(this.zzfnf.values()).addOnCompleteListener(new zzbfx(this.zzakm), this.zzfnq);
            this.zzfmy.unlock();
            return true;
        } catch (Throwable th) {
            this.zzfmy.unlock();
            throw th;
        }
    }

    public final void zzagf() {
        this.zzfmy.lock();
        try {
            this.zzfjo.zzagf();
            if (this.zzfnq != null) {
                this.zzfnq.cancel();
                this.zzfnq = null;
            }
            if (this.zzfnp == null) {
                this.zzfnp = new ArrayMap(this.zzfnf.size());
            }
            ConnectionResult connectionResult = new ConnectionResult(4);
            for (zzac<?> zzaga : this.zzfnf.values()) {
                this.zzfnp.put(zzaga.zzaga(), connectionResult);
            }
            if (this.zzfno != null) {
                this.zzfno.putAll(this.zzfnp);
            }
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final void zzagy() {
    }

    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T zzd(@NonNull T t) {
        if (this.zzfnk && zzg((zzm) t)) {
            return t;
        }
        if (!isConnected()) {
            this.zzfnm.add(t);
            return t;
        }
        this.zzfnh.zzfpn.zzb(t);
        return this.zzfne.get(t.zzaft()).zza((zzm) t);
    }

    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T zze(@NonNull T t) {
        Api.zzc zzaft = t.zzaft();
        if (this.zzfnk && zzg((zzm) t)) {
            return t;
        }
        this.zzfnh.zzfpn.zzb(t);
        return this.zzfne.get(zzaft).zzb((zzm) t);
    }
}
