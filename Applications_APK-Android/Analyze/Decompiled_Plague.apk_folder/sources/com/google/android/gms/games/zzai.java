package com.google.android.gms.games;

import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzai extends zzac<Intent> {
    private /* synthetic */ String zzhis;
    private /* synthetic */ int zzhit;
    private /* synthetic */ int zzhiu;

    zzai(LeaderboardsClient leaderboardsClient, String str, int i, int i2) {
        this.zzhis = str;
        this.zzhit = i;
        this.zzhiu = i2;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<Intent> taskCompletionSource) throws RemoteException {
        taskCompletionSource.setResult(gamesClientImpl.zzj(this.zzhis, this.zzhit, this.zzhiu));
    }
}
