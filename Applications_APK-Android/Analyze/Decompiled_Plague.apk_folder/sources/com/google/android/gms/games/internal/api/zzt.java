package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzt extends zzw {
    private /* synthetic */ String zzhgq;
    private /* synthetic */ int zzhgr;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzt(zzq zzq, GoogleApiClient googleApiClient, String str, int i) {
        super(googleApiClient, null);
        this.zzhgq = str;
        this.zzhgr = i;
    }

    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zzp(this.zzhgq, this.zzhgr);
    }
}
