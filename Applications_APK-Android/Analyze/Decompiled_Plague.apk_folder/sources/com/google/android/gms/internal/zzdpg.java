package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrk;
import java.security.GeneralSecurityException;

public final class zzdpg implements zzdos<zzdoo> {
    zzdpg() {
    }

    private static zzdqm zzblj() throws GeneralSecurityException {
        return (zzdqm) zzdqm.zzbmo().zzfm(0).zzu(zzfdh.zzay(zzdtd.zzgb(32))).zzcvk();
    }

    /* access modifiers changed from: private */
    /* renamed from: zzd */
    public final zzdoo zza(zzfdh zzfdh) throws GeneralSecurityException {
        try {
            zzdqm zzt = zzdqm.zzt(zzfdh);
            if (!(zzt instanceof zzdqm)) {
                throw new GeneralSecurityException("expected ChaCha20Poly1305Key proto");
            }
            zzdqm zzdqm = zzt;
            zzdte.zzt(zzdqm.getVersion(), 0);
            if (zzdqm.zzblt().size() == 32) {
                return zzdsf.zzaj(zzdqm.zzblt().toByteArray());
            }
            throw new GeneralSecurityException("invalid ChaCha20Poly1305Key: incorrect key length");
        } catch (zzfew e) {
            throw new GeneralSecurityException("invalid ChaCha20Poly1305 key", e);
        }
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.ChaCha20Poly1305Key";
    }

    public final /* synthetic */ Object zza(zzffi zzffi) throws GeneralSecurityException {
        if (!(zzffi instanceof zzdqm)) {
            throw new GeneralSecurityException("expected ChaCha20Poly1305Key proto");
        }
        zzdqm zzdqm = (zzdqm) zzffi;
        zzdte.zzt(zzdqm.getVersion(), 0);
        if (zzdqm.zzblt().size() == 32) {
            return zzdsf.zzaj(zzdqm.zzblt().toByteArray());
        }
        throw new GeneralSecurityException("invalid ChaCha20Poly1305Key: incorrect key length");
    }

    public final zzffi zzb(zzfdh zzfdh) throws GeneralSecurityException {
        return zzblj();
    }

    public final zzffi zzb(zzffi zzffi) throws GeneralSecurityException {
        return zzblj();
    }

    public final zzdrk zzc(zzfdh zzfdh) throws GeneralSecurityException {
        return (zzdrk) zzdrk.zzbnv().zzoa("type.googleapis.com/google.crypto.tink.ChaCha20Poly1305Key").zzaa(zzblj().toByteString()).zzb(zzdrk.zzb.SYMMETRIC).zzcvk();
    }
}
