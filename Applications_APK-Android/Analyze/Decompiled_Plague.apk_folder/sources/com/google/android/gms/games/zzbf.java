package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.games.internal.zzw;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbf extends zzac<String> {
    private /* synthetic */ String zzhkg;

    zzbf(RealTimeMultiplayerClient realTimeMultiplayerClient, String str) {
        this.zzhkg = str;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<String> taskCompletionSource) throws RemoteException {
        ((zzw) gamesClientImpl.zzakb()).zza(new zzbg(this, taskCompletionSource), this.zzhkg);
    }
}
