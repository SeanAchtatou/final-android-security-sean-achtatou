package com.google.android.gms.ads.internal.js;

final class zzv implements Runnable {
    final /* synthetic */ zzc zzbzr;
    private /* synthetic */ zzp zzbzs;

    zzv(zzp zzp, zzc zzc) {
        this.zzbzs = zzp;
        this.zzbzr = zzc;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003a, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r3 = this;
            com.google.android.gms.ads.internal.js.zzp r0 = r3.zzbzs
            com.google.android.gms.ads.internal.js.zzo r0 = r0.zzbzq
            java.lang.Object r0 = r0.mLock
            monitor-enter(r0)
            com.google.android.gms.ads.internal.js.zzp r1 = r3.zzbzs     // Catch:{ all -> 0x003b }
            com.google.android.gms.ads.internal.js.zzaf r1 = r1.zzbzp     // Catch:{ all -> 0x003b }
            int r1 = r1.getStatus()     // Catch:{ all -> 0x003b }
            r2 = -1
            if (r1 == r2) goto L_0x0039
            com.google.android.gms.ads.internal.js.zzp r1 = r3.zzbzs     // Catch:{ all -> 0x003b }
            com.google.android.gms.ads.internal.js.zzaf r1 = r1.zzbzp     // Catch:{ all -> 0x003b }
            int r1 = r1.getStatus()     // Catch:{ all -> 0x003b }
            r2 = 1
            if (r1 != r2) goto L_0x0020
            goto L_0x0039
        L_0x0020:
            com.google.android.gms.ads.internal.js.zzp r1 = r3.zzbzs     // Catch:{ all -> 0x003b }
            com.google.android.gms.ads.internal.js.zzaf r1 = r1.zzbzp     // Catch:{ all -> 0x003b }
            r1.reject()     // Catch:{ all -> 0x003b }
            com.google.android.gms.ads.internal.zzbs.zzec()     // Catch:{ all -> 0x003b }
            com.google.android.gms.ads.internal.js.zzw r1 = new com.google.android.gms.ads.internal.js.zzw     // Catch:{ all -> 0x003b }
            r1.<init>(r3)     // Catch:{ all -> 0x003b }
            com.google.android.gms.internal.zzagr.runOnUiThread(r1)     // Catch:{ all -> 0x003b }
            java.lang.String r1 = "Could not receive loaded message in a timely manner. Rejecting."
            com.google.android.gms.internal.zzafj.v(r1)     // Catch:{ all -> 0x003b }
            monitor-exit(r0)     // Catch:{ all -> 0x003b }
            return
        L_0x0039:
            monitor-exit(r0)     // Catch:{ all -> 0x003b }
            return
        L_0x003b:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003b }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.js.zzv.run():void");
    }
}
