package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class GameEntity extends GamesDowngradeableSafeParcel implements Game {
    public static final Parcelable.Creator<GameEntity> CREATOR = new zza();
    private final boolean zzdhg;
    private final String zzdou;
    private final String zzedu;
    private final String zzekh;
    private final String zzhgs;
    private final String zzhgt;
    private final String zzhgu;
    private final Uri zzhgv;
    private final Uri zzhgw;
    private final Uri zzhgx;
    private final boolean zzhgy;
    private final boolean zzhgz;
    private final String zzhha;
    private final int zzhhb;
    private final int zzhhc;
    private final int zzhhd;
    private final boolean zzhhe;
    private final boolean zzhhf;
    private final String zzhhg;
    private final String zzhhh;
    private final String zzhhi;
    private final boolean zzhhj;
    private final boolean zzhhk;
    private final String zzhhl;
    private final boolean zzhhm;

    static final class zza extends zzh {
        zza() {
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return createFromParcel(parcel);
        }

        public final GameEntity zzi(Parcel parcel) {
            if (GameEntity.zze(GameEntity.zzaku()) || GameEntity.zzgc(GameEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            String readString5 = parcel.readString();
            String readString6 = parcel.readString();
            String readString7 = parcel.readString();
            Uri parse = readString7 == null ? null : Uri.parse(readString7);
            String readString8 = parcel.readString();
            Uri parse2 = readString8 == null ? null : Uri.parse(readString8);
            String readString9 = parcel.readString();
            return new GameEntity(readString, readString2, readString3, readString4, readString5, readString6, parse, parse2, readString9 == null ? null : Uri.parse(readString9), parcel.readInt() > 0, parcel.readInt() > 0, parcel.readString(), parcel.readInt(), parcel.readInt(), parcel.readInt(), false, false, null, null, null, false, false, false, null, false);
        }
    }

    public GameEntity(Game game) {
        this.zzekh = game.getApplicationId();
        this.zzhgs = game.getPrimaryCategory();
        this.zzhgt = game.getSecondaryCategory();
        this.zzdou = game.getDescription();
        this.zzhgu = game.getDeveloperName();
        this.zzedu = game.getDisplayName();
        this.zzhgv = game.getIconImageUri();
        this.zzhhg = game.getIconImageUrl();
        this.zzhgw = game.getHiResImageUri();
        this.zzhhh = game.getHiResImageUrl();
        this.zzhgx = game.getFeaturedImageUri();
        this.zzhhi = game.getFeaturedImageUrl();
        this.zzhgy = game.zzaqr();
        this.zzhgz = game.zzaqt();
        this.zzhha = game.zzaqu();
        this.zzhhb = 1;
        this.zzhhc = game.getAchievementTotalCount();
        this.zzhhd = game.getLeaderboardCount();
        this.zzhhe = game.isRealTimeMultiplayerEnabled();
        this.zzhhf = game.isTurnBasedMultiplayerEnabled();
        this.zzdhg = game.isMuted();
        this.zzhhj = game.zzaqs();
        this.zzhhk = game.areSnapshotsEnabled();
        this.zzhhl = game.getThemeColor();
        this.zzhhm = game.hasGamepadSupport();
    }

    GameEntity(String str, String str2, String str3, String str4, String str5, String str6, Uri uri, Uri uri2, Uri uri3, boolean z, boolean z2, String str7, int i, int i2, int i3, boolean z3, boolean z4, String str8, String str9, String str10, boolean z5, boolean z6, boolean z7, String str11, boolean z8) {
        this.zzekh = str;
        this.zzedu = str2;
        this.zzhgs = str3;
        this.zzhgt = str4;
        this.zzdou = str5;
        this.zzhgu = str6;
        this.zzhgv = uri;
        this.zzhhg = str8;
        this.zzhgw = uri2;
        this.zzhhh = str9;
        this.zzhgx = uri3;
        this.zzhhi = str10;
        this.zzhgy = z;
        this.zzhgz = z2;
        this.zzhha = str7;
        this.zzhhb = i;
        this.zzhhc = i2;
        this.zzhhd = i3;
        this.zzhhe = z3;
        this.zzhhf = z4;
        this.zzdhg = z5;
        this.zzhhj = z6;
        this.zzhhk = z7;
        this.zzhhl = str11;
        this.zzhhm = z8;
    }

    static int zza(Game game) {
        return Arrays.hashCode(new Object[]{game.getApplicationId(), game.getDisplayName(), game.getPrimaryCategory(), game.getSecondaryCategory(), game.getDescription(), game.getDeveloperName(), game.getIconImageUri(), game.getHiResImageUri(), game.getFeaturedImageUri(), Boolean.valueOf(game.zzaqr()), Boolean.valueOf(game.zzaqt()), game.zzaqu(), Integer.valueOf(game.getAchievementTotalCount()), Integer.valueOf(game.getLeaderboardCount()), Boolean.valueOf(game.isRealTimeMultiplayerEnabled()), Boolean.valueOf(game.isTurnBasedMultiplayerEnabled()), Boolean.valueOf(game.isMuted()), Boolean.valueOf(game.zzaqs()), Boolean.valueOf(game.areSnapshotsEnabled()), game.getThemeColor(), Boolean.valueOf(game.hasGamepadSupport())});
    }

    static boolean zza(Game game, Object obj) {
        if (!(obj instanceof Game)) {
            return false;
        }
        if (game == obj) {
            return true;
        }
        Game game2 = (Game) obj;
        if (zzbg.equal(game2.getApplicationId(), game.getApplicationId()) && zzbg.equal(game2.getDisplayName(), game.getDisplayName()) && zzbg.equal(game2.getPrimaryCategory(), game.getPrimaryCategory()) && zzbg.equal(game2.getSecondaryCategory(), game.getSecondaryCategory()) && zzbg.equal(game2.getDescription(), game.getDescription()) && zzbg.equal(game2.getDeveloperName(), game.getDeveloperName()) && zzbg.equal(game2.getIconImageUri(), game.getIconImageUri()) && zzbg.equal(game2.getHiResImageUri(), game.getHiResImageUri()) && zzbg.equal(game2.getFeaturedImageUri(), game.getFeaturedImageUri()) && zzbg.equal(Boolean.valueOf(game2.zzaqr()), Boolean.valueOf(game.zzaqr())) && zzbg.equal(Boolean.valueOf(game2.zzaqt()), Boolean.valueOf(game.zzaqt())) && zzbg.equal(game2.zzaqu(), game.zzaqu()) && zzbg.equal(Integer.valueOf(game2.getAchievementTotalCount()), Integer.valueOf(game.getAchievementTotalCount())) && zzbg.equal(Integer.valueOf(game2.getLeaderboardCount()), Integer.valueOf(game.getLeaderboardCount())) && zzbg.equal(Boolean.valueOf(game2.isRealTimeMultiplayerEnabled()), Boolean.valueOf(game.isRealTimeMultiplayerEnabled()))) {
            return zzbg.equal(Boolean.valueOf(game2.isTurnBasedMultiplayerEnabled()), Boolean.valueOf(game.isTurnBasedMultiplayerEnabled() && zzbg.equal(Boolean.valueOf(game2.isMuted()), Boolean.valueOf(game.isMuted())) && zzbg.equal(Boolean.valueOf(game2.zzaqs()), Boolean.valueOf(game.zzaqs())))) && zzbg.equal(Boolean.valueOf(game2.areSnapshotsEnabled()), Boolean.valueOf(game.areSnapshotsEnabled())) && zzbg.equal(game2.getThemeColor(), game.getThemeColor()) && zzbg.equal(Boolean.valueOf(game2.hasGamepadSupport()), Boolean.valueOf(game.hasGamepadSupport()));
        }
    }

    static String zzb(Game game) {
        return zzbg.zzw(game).zzg("ApplicationId", game.getApplicationId()).zzg("DisplayName", game.getDisplayName()).zzg("PrimaryCategory", game.getPrimaryCategory()).zzg("SecondaryCategory", game.getSecondaryCategory()).zzg("Description", game.getDescription()).zzg("DeveloperName", game.getDeveloperName()).zzg("IconImageUri", game.getIconImageUri()).zzg("IconImageUrl", game.getIconImageUrl()).zzg("HiResImageUri", game.getHiResImageUri()).zzg("HiResImageUrl", game.getHiResImageUrl()).zzg("FeaturedImageUri", game.getFeaturedImageUri()).zzg("FeaturedImageUrl", game.getFeaturedImageUrl()).zzg("PlayEnabledGame", Boolean.valueOf(game.zzaqr())).zzg("InstanceInstalled", Boolean.valueOf(game.zzaqt())).zzg("InstancePackageName", game.zzaqu()).zzg("AchievementTotalCount", Integer.valueOf(game.getAchievementTotalCount())).zzg("LeaderboardCount", Integer.valueOf(game.getLeaderboardCount())).zzg("RealTimeMultiplayerEnabled", Boolean.valueOf(game.isRealTimeMultiplayerEnabled())).zzg("TurnBasedMultiplayerEnabled", Boolean.valueOf(game.isTurnBasedMultiplayerEnabled())).zzg("AreSnapshotsEnabled", Boolean.valueOf(game.areSnapshotsEnabled())).zzg("ThemeColor", game.getThemeColor()).zzg("HasGamepadSupport", Boolean.valueOf(game.hasGamepadSupport())).toString();
    }

    public final boolean areSnapshotsEnabled() {
        return this.zzhhk;
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    public final Game freeze() {
        return this;
    }

    public final int getAchievementTotalCount() {
        return this.zzhhc;
    }

    public final String getApplicationId() {
        return this.zzekh;
    }

    public final String getDescription() {
        return this.zzdou;
    }

    public final void getDescription(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.zzdou, charArrayBuffer);
    }

    public final String getDeveloperName() {
        return this.zzhgu;
    }

    public final void getDeveloperName(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.zzhgu, charArrayBuffer);
    }

    public final String getDisplayName() {
        return this.zzedu;
    }

    public final void getDisplayName(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.zzedu, charArrayBuffer);
    }

    public final Uri getFeaturedImageUri() {
        return this.zzhgx;
    }

    public final String getFeaturedImageUrl() {
        return this.zzhhi;
    }

    public final Uri getHiResImageUri() {
        return this.zzhgw;
    }

    public final String getHiResImageUrl() {
        return this.zzhhh;
    }

    public final Uri getIconImageUri() {
        return this.zzhgv;
    }

    public final String getIconImageUrl() {
        return this.zzhhg;
    }

    public final int getLeaderboardCount() {
        return this.zzhhd;
    }

    public final String getPrimaryCategory() {
        return this.zzhgs;
    }

    public final String getSecondaryCategory() {
        return this.zzhgt;
    }

    public final String getThemeColor() {
        return this.zzhhl;
    }

    public final boolean hasGamepadSupport() {
        return this.zzhhm;
    }

    public final int hashCode() {
        return zza(this);
    }

    public final boolean isDataValid() {
        return true;
    }

    public final boolean isMuted() {
        return this.zzdhg;
    }

    public final boolean isRealTimeMultiplayerEnabled() {
        return this.zzhhe;
    }

    public final boolean isTurnBasedMultiplayerEnabled() {
        return this.zzhhf;
    }

    public final String toString() {
        return zzb(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, getApplicationId(), false);
        zzbem.zza(parcel, 2, getDisplayName(), false);
        zzbem.zza(parcel, 3, getPrimaryCategory(), false);
        zzbem.zza(parcel, 4, getSecondaryCategory(), false);
        zzbem.zza(parcel, 5, getDescription(), false);
        zzbem.zza(parcel, 6, getDeveloperName(), false);
        zzbem.zza(parcel, 7, (Parcelable) getIconImageUri(), i, false);
        zzbem.zza(parcel, 8, (Parcelable) getHiResImageUri(), i, false);
        zzbem.zza(parcel, 9, (Parcelable) getFeaturedImageUri(), i, false);
        zzbem.zza(parcel, 10, this.zzhgy);
        zzbem.zza(parcel, 11, this.zzhgz);
        zzbem.zza(parcel, 12, this.zzhha, false);
        zzbem.zzc(parcel, 13, this.zzhhb);
        zzbem.zzc(parcel, 14, getAchievementTotalCount());
        zzbem.zzc(parcel, 15, getLeaderboardCount());
        zzbem.zza(parcel, 16, isRealTimeMultiplayerEnabled());
        zzbem.zza(parcel, 17, isTurnBasedMultiplayerEnabled());
        zzbem.zza(parcel, 18, getIconImageUrl(), false);
        zzbem.zza(parcel, 19, getHiResImageUrl(), false);
        zzbem.zza(parcel, 20, getFeaturedImageUrl(), false);
        zzbem.zza(parcel, 21, this.zzdhg);
        zzbem.zza(parcel, 22, this.zzhhj);
        zzbem.zza(parcel, 23, areSnapshotsEnabled());
        zzbem.zza(parcel, 24, getThemeColor(), false);
        zzbem.zza(parcel, 25, hasGamepadSupport());
        zzbem.zzai(parcel, zze);
    }

    public final boolean zzaqr() {
        return this.zzhgy;
    }

    public final boolean zzaqs() {
        return this.zzhhj;
    }

    public final boolean zzaqt() {
        return this.zzhgz;
    }

    public final String zzaqu() {
        return this.zzhha;
    }
}
