package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.multiplayer.ParticipantResult;

final class zzdb extends zzdr {
    private /* synthetic */ String zzhlu;
    private /* synthetic */ byte[] zzhqw;
    private /* synthetic */ String zzhqx;
    private /* synthetic */ ParticipantResult[] zzhqy;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdb(zzcw zzcw, GoogleApiClient googleApiClient, String str, byte[] bArr, String str2, ParticipantResult[] participantResultArr) {
        super(googleApiClient, null);
        this.zzhlu = str;
        this.zzhqw = bArr;
        this.zzhqx = str2;
        this.zzhqy = participantResultArr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhlu, this.zzhqw, this.zzhqx, this.zzhqy);
    }
}
