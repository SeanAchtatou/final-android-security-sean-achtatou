package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.zzv;
import java.util.List;

public final class zzbpd extends zzbej {
    public static final Parcelable.Creator<zzbpd> CREATOR = new zzbpe();
    private int zzbwg;
    private List<zzv> zzgnq;

    public zzbpd(List<zzv> list, int i) {
        this.zzgnq = list;
        this.zzbwg = i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 2, this.zzgnq, false);
        zzbem.zzc(parcel, 3, this.zzbwg);
        zzbem.zzai(parcel, zze);
    }
}
