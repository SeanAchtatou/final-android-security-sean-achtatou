package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzae extends zzaf {
    private /* synthetic */ int zzhpz;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzae(zzad zzad, GoogleApiClient googleApiClient, int i) {
        super(googleApiClient, null);
        this.zzhpz = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhpz);
    }
}
