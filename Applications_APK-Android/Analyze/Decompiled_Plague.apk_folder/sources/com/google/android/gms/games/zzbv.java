package com.google.android.gms.games;

import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbv extends zzac<Intent> {
    private /* synthetic */ String zzhkz;
    private /* synthetic */ boolean zzhla;
    private /* synthetic */ boolean zzhlb;
    private /* synthetic */ int zzhlc;

    zzbv(SnapshotsClient snapshotsClient, String str, boolean z, boolean z2, int i) {
        this.zzhkz = str;
        this.zzhla = z;
        this.zzhlb = z2;
        this.zzhlc = i;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<Intent> taskCompletionSource) throws RemoteException {
        taskCompletionSource.setResult(gamesClientImpl.zza(this.zzhkz, this.zzhla, this.zzhlb, this.zzhlc));
    }
}
