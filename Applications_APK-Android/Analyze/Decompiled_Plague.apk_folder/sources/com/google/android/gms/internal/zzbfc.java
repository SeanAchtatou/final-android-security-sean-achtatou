package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzbfc extends zzed implements zzbfb {
    zzbfc(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.service.ICommonService");
    }

    public final void zza(zzbez zzbez) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbez);
        zzc(1, zzaz);
    }
}
