package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.internal.zzn;
import com.google.android.gms.auth.api.signin.internal.zzp;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleSignInOptions extends zzbej implements Api.ApiOptions.Optional, ReflectedParcelable {
    public static final Parcelable.Creator<GoogleSignInOptions> CREATOR = new zze();
    public static final GoogleSignInOptions DEFAULT_GAMES_SIGN_IN = new Builder().requestScopes(SCOPE_GAMES, new Scope[0]).build();
    public static final GoogleSignInOptions DEFAULT_SIGN_IN = new Builder().requestId().requestProfile().build();
    private static Scope SCOPE_GAMES = new Scope(Scopes.GAMES);
    public static final Scope zzeei = new Scope(Scopes.PROFILE);
    public static final Scope zzeej = new Scope("email");
    public static final Scope zzeek = new Scope("openid");
    private static Comparator<Scope> zzeer = new zzd();
    private int versionCode;
    /* access modifiers changed from: private */
    public Account zzdzb;
    /* access modifiers changed from: private */
    public boolean zzecm;
    /* access modifiers changed from: private */
    public String zzecn;
    /* access modifiers changed from: private */
    public final ArrayList<Scope> zzeel;
    /* access modifiers changed from: private */
    public final boolean zzeem;
    /* access modifiers changed from: private */
    public final boolean zzeen;
    /* access modifiers changed from: private */
    public String zzeeo;
    /* access modifiers changed from: private */
    public ArrayList<zzn> zzeep;
    private Map<Integer, zzn> zzeeq;

    public static final class Builder {
        private Account zzdzb;
        private boolean zzecm;
        private String zzecn;
        private boolean zzeem;
        private boolean zzeen;
        private String zzeeo;
        private Set<Scope> zzees = new HashSet();
        private Map<Integer, zzn> zzeet = new HashMap();

        public Builder() {
        }

        public Builder(@NonNull GoogleSignInOptions googleSignInOptions) {
            zzbq.checkNotNull(googleSignInOptions);
            this.zzees = new HashSet(googleSignInOptions.zzeel);
            this.zzeem = googleSignInOptions.zzeem;
            this.zzeen = googleSignInOptions.zzeen;
            this.zzecm = googleSignInOptions.zzecm;
            this.zzecn = googleSignInOptions.zzecn;
            this.zzdzb = googleSignInOptions.zzdzb;
            this.zzeeo = googleSignInOptions.zzeeo;
            this.zzeet = GoogleSignInOptions.zzw(googleSignInOptions.zzeep);
        }

        private final String zzep(String str) {
            zzbq.zzgh(str);
            zzbq.checkArgument(this.zzecn == null || this.zzecn.equals(str), "two different server client ids provided");
            return str;
        }

        public final Builder addExtension(GoogleSignInOptionsExtension googleSignInOptionsExtension) {
            if (this.zzeet.containsKey(Integer.valueOf(googleSignInOptionsExtension.getExtensionType()))) {
                throw new IllegalStateException("Only one extension per type may be added");
            }
            if (googleSignInOptionsExtension.getImpliedScopes() != null) {
                this.zzees.addAll(googleSignInOptionsExtension.getImpliedScopes());
            }
            this.zzeet.put(Integer.valueOf(googleSignInOptionsExtension.getExtensionType()), new zzn(googleSignInOptionsExtension));
            return this;
        }

        public final GoogleSignInOptions build() {
            if (this.zzecm && (this.zzdzb == null || !this.zzees.isEmpty())) {
                requestId();
            }
            return new GoogleSignInOptions(3, new ArrayList(this.zzees), this.zzdzb, this.zzecm, this.zzeem, this.zzeen, this.zzecn, this.zzeeo, this.zzeet, null);
        }

        public final Builder requestEmail() {
            this.zzees.add(GoogleSignInOptions.zzeej);
            return this;
        }

        public final Builder requestId() {
            this.zzees.add(GoogleSignInOptions.zzeek);
            return this;
        }

        public final Builder requestIdToken(String str) {
            this.zzecm = true;
            this.zzecn = zzep(str);
            return this;
        }

        public final Builder requestProfile() {
            this.zzees.add(GoogleSignInOptions.zzeei);
            return this;
        }

        public final Builder requestScopes(Scope scope, Scope... scopeArr) {
            this.zzees.add(scope);
            this.zzees.addAll(Arrays.asList(scopeArr));
            return this;
        }

        public final Builder requestServerAuthCode(String str) {
            return requestServerAuthCode(str, false);
        }

        public final Builder requestServerAuthCode(String str, boolean z) {
            this.zzeem = true;
            this.zzecn = zzep(str);
            this.zzeen = z;
            return this;
        }

        public final Builder setAccountName(String str) {
            this.zzdzb = new Account(zzbq.zzgh(str), "com.google");
            return this;
        }

        public final Builder setHostedDomain(String str) {
            this.zzeeo = zzbq.zzgh(str);
            return this;
        }
    }

    GoogleSignInOptions(int i, ArrayList<Scope> arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, ArrayList<zzn> arrayList2) {
        this(i, arrayList, account, z, z2, z3, str, str2, zzw(arrayList2));
    }

    private GoogleSignInOptions(int i, ArrayList<Scope> arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, Map<Integer, zzn> map) {
        this.versionCode = i;
        this.zzeel = arrayList;
        this.zzdzb = account;
        this.zzecm = z;
        this.zzeem = z2;
        this.zzeen = z3;
        this.zzecn = str;
        this.zzeeo = str2;
        this.zzeep = new ArrayList<>(map.values());
        this.zzeeq = map;
    }

    /* synthetic */ GoogleSignInOptions(int i, ArrayList arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, Map map, zzd zzd) {
        this(3, arrayList, account, z, z2, z3, str, str2, map);
    }

    private final JSONObject toJsonObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONArray jSONArray = new JSONArray();
            Collections.sort(this.zzeel, zzeer);
            ArrayList arrayList = this.zzeel;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj = arrayList.get(i);
                i++;
                jSONArray.put(((Scope) obj).zzagj());
            }
            jSONObject.put("scopes", jSONArray);
            if (this.zzdzb != null) {
                jSONObject.put("accountName", this.zzdzb.name);
            }
            jSONObject.put("idTokenRequested", this.zzecm);
            jSONObject.put("forceCodeForRefreshToken", this.zzeen);
            jSONObject.put("serverAuthRequested", this.zzeem);
            if (!TextUtils.isEmpty(this.zzecn)) {
                jSONObject.put("serverClientId", this.zzecn);
            }
            if (!TextUtils.isEmpty(this.zzeeo)) {
                jSONObject.put("hostedDomain", this.zzeeo);
            }
            return jSONObject;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Nullable
    public static GoogleSignInOptions zzeo(@Nullable String str) throws JSONException {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject(str);
        HashSet hashSet = new HashSet();
        JSONArray jSONArray = jSONObject.getJSONArray("scopes");
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            hashSet.add(new Scope(jSONArray.getString(i)));
        }
        String optString = jSONObject.optString("accountName", null);
        return new GoogleSignInOptions(3, new ArrayList(hashSet), !TextUtils.isEmpty(optString) ? new Account(optString, "com.google") : null, jSONObject.getBoolean("idTokenRequested"), jSONObject.getBoolean("serverAuthRequested"), jSONObject.getBoolean("forceCodeForRefreshToken"), jSONObject.optString("serverClientId", null), jSONObject.optString("hostedDomain", null), new HashMap());
    }

    /* access modifiers changed from: private */
    public static Map<Integer, zzn> zzw(@Nullable List<zzn> list) {
        HashMap hashMap = new HashMap();
        if (list == null) {
            return hashMap;
        }
        for (zzn next : list) {
            hashMap.put(Integer.valueOf(next.getType()), next);
        }
        return hashMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0045, code lost:
        if (r3.zzdzb.equals(r4.zzdzb) != false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0060, code lost:
        if (r3.zzecn.equals(r4.zzecn) != false) goto L_0x0062;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r4) {
        /*
            r3 = this;
            r0 = 0
            if (r4 != 0) goto L_0x0004
            return r0
        L_0x0004:
            com.google.android.gms.auth.api.signin.GoogleSignInOptions r4 = (com.google.android.gms.auth.api.signin.GoogleSignInOptions) r4     // Catch:{ ClassCastException -> 0x0076 }
            java.util.ArrayList<com.google.android.gms.auth.api.signin.internal.zzn> r1 = r3.zzeep     // Catch:{ ClassCastException -> 0x0076 }
            int r1 = r1.size()     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 > 0) goto L_0x0076
            java.util.ArrayList<com.google.android.gms.auth.api.signin.internal.zzn> r1 = r4.zzeep     // Catch:{ ClassCastException -> 0x0076 }
            int r1 = r1.size()     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 <= 0) goto L_0x0017
            return r0
        L_0x0017:
            java.util.ArrayList<com.google.android.gms.common.api.Scope> r1 = r3.zzeel     // Catch:{ ClassCastException -> 0x0076 }
            int r1 = r1.size()     // Catch:{ ClassCastException -> 0x0076 }
            java.util.ArrayList r2 = r4.zzaar()     // Catch:{ ClassCastException -> 0x0076 }
            int r2 = r2.size()     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != r2) goto L_0x0076
            java.util.ArrayList<com.google.android.gms.common.api.Scope> r1 = r3.zzeel     // Catch:{ ClassCastException -> 0x0076 }
            java.util.ArrayList r2 = r4.zzaar()     // Catch:{ ClassCastException -> 0x0076 }
            boolean r1 = r1.containsAll(r2)     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != 0) goto L_0x0034
            return r0
        L_0x0034:
            android.accounts.Account r1 = r3.zzdzb     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != 0) goto L_0x003d
            android.accounts.Account r1 = r4.zzdzb     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != 0) goto L_0x0076
            goto L_0x0047
        L_0x003d:
            android.accounts.Account r1 = r3.zzdzb     // Catch:{ ClassCastException -> 0x0076 }
            android.accounts.Account r2 = r4.zzdzb     // Catch:{ ClassCastException -> 0x0076 }
            boolean r1 = r1.equals(r2)     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 == 0) goto L_0x0076
        L_0x0047:
            java.lang.String r1 = r3.zzecn     // Catch:{ ClassCastException -> 0x0076 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 == 0) goto L_0x0058
            java.lang.String r1 = r4.zzecn     // Catch:{ ClassCastException -> 0x0076 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 == 0) goto L_0x0076
            goto L_0x0062
        L_0x0058:
            java.lang.String r1 = r3.zzecn     // Catch:{ ClassCastException -> 0x0076 }
            java.lang.String r2 = r4.zzecn     // Catch:{ ClassCastException -> 0x0076 }
            boolean r1 = r1.equals(r2)     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 == 0) goto L_0x0076
        L_0x0062:
            boolean r1 = r3.zzeen     // Catch:{ ClassCastException -> 0x0076 }
            boolean r2 = r4.zzeen     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != r2) goto L_0x0076
            boolean r1 = r3.zzecm     // Catch:{ ClassCastException -> 0x0076 }
            boolean r2 = r4.zzecm     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != r2) goto L_0x0076
            boolean r1 = r3.zzeem     // Catch:{ ClassCastException -> 0x0076 }
            boolean r4 = r4.zzeem     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != r4) goto L_0x0076
            r4 = 1
            return r4
        L_0x0076:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.auth.api.signin.GoogleSignInOptions.equals(java.lang.Object):boolean");
    }

    public final Account getAccount() {
        return this.zzdzb;
    }

    public Scope[] getScopeArray() {
        return (Scope[]) this.zzeel.toArray(new Scope[this.zzeel.size()]);
    }

    public final String getServerClientId() {
        return this.zzecn;
    }

    public int hashCode() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = this.zzeel;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            arrayList.add(((Scope) obj).zzagj());
        }
        Collections.sort(arrayList);
        return new zzp().zzr(arrayList).zzr(this.zzdzb).zzr(this.zzecn).zzaq(this.zzeen).zzaq(this.zzecm).zzaq(this.zzeem).zzaba();
    }

    public final boolean isIdTokenRequested() {
        return this.zzecm;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.accounts.Account, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzc(parcel, 1, this.versionCode);
        zzbem.zzc(parcel, 2, zzaar(), false);
        zzbem.zza(parcel, 3, (Parcelable) this.zzdzb, i, false);
        zzbem.zza(parcel, 4, this.zzecm);
        zzbem.zza(parcel, 5, this.zzeem);
        zzbem.zza(parcel, 6, this.zzeen);
        zzbem.zza(parcel, 7, this.zzecn, false);
        zzbem.zza(parcel, 8, this.zzeeo, false);
        zzbem.zzc(parcel, 9, this.zzeep, false);
        zzbem.zzai(parcel, zze);
    }

    public final ArrayList<Scope> zzaar() {
        return new ArrayList<>(this.zzeel);
    }

    public final boolean zzaas() {
        return this.zzeem;
    }

    public final String zzaat() {
        return toJsonObject().toString();
    }
}
