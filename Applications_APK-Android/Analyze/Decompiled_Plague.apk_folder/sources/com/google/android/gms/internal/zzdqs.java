package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdqq;
import com.google.android.gms.internal.zzdqw;
import java.io.IOException;

public final class zzdqs extends zzfee<zzdqs, zza> implements zzffk {
    private static volatile zzffm<zzdqs> zzbas;
    /* access modifiers changed from: private */
    public static final zzdqs zzlri;
    private zzdqw zzlrf;
    private zzdqq zzlrg;
    private int zzlrh;

    public static final class zza extends zzfef<zzdqs, zza> implements zzffk {
        private zza() {
            super(zzdqs.zzlri);
        }

        /* synthetic */ zza(zzdqt zzdqt) {
            this();
        }
    }

    static {
        zzdqs zzdqs = new zzdqs();
        zzlri = zzdqs;
        zzdqs.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdqs.zzpbs.zzbim();
    }

    private zzdqs() {
    }

    public static zzdqs zzbmw() {
        return zzlri;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdqw.zza zza2;
        zzdqq.zza zza3;
        boolean z = false;
        switch (zzdqt.zzbaq[i - 1]) {
            case 1:
                return new zzdqs();
            case 2:
                return zzlri;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdqs zzdqs = (zzdqs) obj2;
                this.zzlrf = (zzdqw) zzfen.zza(this.zzlrf, zzdqs.zzlrf);
                this.zzlrg = (zzdqq) zzfen.zza(this.zzlrg, zzdqs.zzlrg);
                boolean z2 = this.zzlrh != 0;
                int i2 = this.zzlrh;
                if (zzdqs.zzlrh != 0) {
                    z = true;
                }
                this.zzlrh = zzfen.zza(z2, i2, z, zzdqs.zzlrh);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 10) {
                                    if (this.zzlrf != null) {
                                        zzdqw zzdqw = this.zzlrf;
                                        zzfef zzfef = (zzfef) zzdqw.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdqw);
                                        zza2 = (zzdqw.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlrf = (zzdqw) zzfdq.zza(zzdqw.zzbnf(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlrf);
                                        this.zzlrf = (zzdqw) zza2.zzcvj();
                                    }
                                } else if (zzcts == 18) {
                                    if (this.zzlrg != null) {
                                        zzdqq zzdqq = this.zzlrg;
                                        zzfef zzfef2 = (zzfef) zzdqq.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef2.zza((zzfee) zzdqq);
                                        zza3 = (zzdqq.zza) zzfef2;
                                    } else {
                                        zza3 = null;
                                    }
                                    this.zzlrg = (zzdqq) zzfdq.zza(zzdqq.zzbmr(), zzfea);
                                    if (zza3 != null) {
                                        zza3.zza((zzfee) this.zzlrg);
                                        this.zzlrg = (zzdqq) zza3.zzcvj();
                                    }
                                } else if (zzcts == 24) {
                                    this.zzlrh = zzfdq.zzcuc();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdqs.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlri);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlri;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlrf != null) {
            zzfdv.zza(1, this.zzlrf == null ? zzdqw.zzbnf() : this.zzlrf);
        }
        if (this.zzlrg != null) {
            zzfdv.zza(2, this.zzlrg == null ? zzdqq.zzbmr() : this.zzlrg);
        }
        if (this.zzlrh != zzdqo.UNKNOWN_FORMAT.zzhn()) {
            zzfdv.zzaa(3, this.zzlrh);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzdqw zzbmt() {
        return this.zzlrf == null ? zzdqw.zzbnf() : this.zzlrf;
    }

    public final zzdqq zzbmu() {
        return this.zzlrg == null ? zzdqq.zzbmr() : this.zzlrg;
    }

    public final zzdqo zzbmv() {
        zzdqo zzfn = zzdqo.zzfn(this.zzlrh);
        return zzfn == null ? zzdqo.UNRECOGNIZED : zzfn;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlrf != null) {
            i2 = 0 + zzfdv.zzb(1, this.zzlrf == null ? zzdqw.zzbnf() : this.zzlrf);
        }
        if (this.zzlrg != null) {
            i2 += zzfdv.zzb(2, this.zzlrg == null ? zzdqq.zzbmr() : this.zzlrg);
        }
        if (this.zzlrh != zzdqo.UNKNOWN_FORMAT.zzhn()) {
            i2 += zzfdv.zzag(3, this.zzlrh);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
