package com.google.android.gms.internal;

import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;

final /* synthetic */ class zzbmw implements Continuation {
    private final zzcl zzgma;

    zzbmw(zzcl zzcl) {
        this.zzgma = zzcl;
    }

    public final Object then(Task task) {
        return zzbmu.zza(this.zzgma, task);
    }
}
