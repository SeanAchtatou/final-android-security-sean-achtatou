package com.google.android.gms.common.api.internal;

final class zzap extends zzbm {
    private /* synthetic */ zzao zzfod;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzap(zzao zzao, zzbk zzbk) {
        super(zzbk);
        this.zzfod = zzao;
    }

    public final void zzahp() {
        this.zzfod.onConnectionSuspended(1);
    }
}
