package com.google.android.gms.internal;

import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

@zzzb
public final class zzhj {
    private final int zzazk;
    private final zzgz zzazm;
    private String zzazu;
    private String zzazv;
    private final boolean zzazw = false;
    private final int zzazx;
    private final int zzazy;

    public zzhj(int i, int i2, int i3) {
        this.zzazk = i;
        if (i2 > 64 || i2 < 0) {
            this.zzazx = 64;
        } else {
            this.zzazx = i2;
        }
        if (i3 <= 0) {
            this.zzazy = 1;
        } else {
            this.zzazy = i3;
        }
        this.zzazm = new zzhi(this.zzazx);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.CharSequence):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.Object):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, float):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.String):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, long):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char[]):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, int):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, boolean):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, double):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder} */
    private final boolean zza(String str, HashSet<String> hashSet) {
        boolean z;
        String[] split = str.split("\n");
        if (split.length == 0) {
            return true;
        }
        for (String str2 : split) {
            if (str2.indexOf("'") != -1) {
                StringBuilder sb = new StringBuilder(str2);
                int i = 1;
                boolean z2 = false;
                while (true) {
                    int i2 = i + 2;
                    if (i2 > sb.length()) {
                        break;
                    }
                    if (sb.charAt(i) == '\'') {
                        if (sb.charAt(i - 1) != ' ') {
                            int i3 = i + 1;
                            if ((sb.charAt(i3) == 's' || sb.charAt(i3) == 'S') && (i2 == sb.length() || sb.charAt(i2) == ' ')) {
                                sb.insert(i, ' ');
                                i = i2;
                                z2 = true;
                            }
                        }
                        sb.setCharAt(i, ' ');
                        z2 = true;
                    }
                    i++;
                }
                String sb2 = z2 ? sb.toString() : null;
                if (sb2 != null) {
                    this.zzazv = sb2;
                    str2 = sb2;
                }
            }
            String[] zzb = zzhd.zzb(str2, true);
            if (zzb.length >= this.zzazy) {
                for (int i4 = 0; i4 < zzb.length; i4++) {
                    String str3 = "";
                    int i5 = 0;
                    while (true) {
                        if (i5 >= this.zzazy) {
                            z = true;
                            break;
                        }
                        int i6 = i4 + i5;
                        if (i6 >= zzb.length) {
                            z = false;
                            break;
                        }
                        if (i5 > 0) {
                            str3 = String.valueOf(str3).concat(" ");
                        }
                        String valueOf = String.valueOf(str3);
                        String valueOf2 = String.valueOf(zzb[i6]);
                        str3 = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                        i5++;
                    }
                    if (!z) {
                        break;
                    }
                    hashSet.add(str3);
                    if (hashSet.size() >= this.zzazk) {
                        return false;
                    }
                }
                if (hashSet.size() >= this.zzazk) {
                    return false;
                }
            }
        }
        return true;
    }

    public final String zza(ArrayList<String> arrayList, ArrayList<zzgy> arrayList2) {
        Collections.sort(arrayList2, new zzhk(this));
        HashSet hashSet = new HashSet();
        int i = 0;
        while (i < arrayList2.size() && zza(Normalizer.normalize(arrayList.get(arrayList2.get(i).zzgz()), Normalizer.Form.NFKC).toLowerCase(Locale.US), hashSet)) {
            i++;
        }
        zzhc zzhc = new zzhc();
        this.zzazu = "";
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            try {
                zzhc.write(this.zzazm.zzx((String) it.next()));
            } catch (IOException e) {
                zzafj.zzb("Error while writing hash to byteStream", e);
            }
        }
        return zzhc.toString();
    }
}
