package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzq;
import com.google.android.gms.common.zze;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzb
public final class zzady implements zzaeh {
    private final Context mContext;
    /* access modifiers changed from: private */
    public final Object mLock = new Object();
    private final zzaee zzcrj;
    /* access modifiers changed from: private */
    public final zzfho zzcul;
    private final LinkedHashMap<String, zzfhw> zzcum;
    private final zzaej zzcun;
    @VisibleForTesting
    boolean zzcuo;
    private HashSet<String> zzcup = new HashSet<>();
    private boolean zzcuq = false;
    private boolean zzcur = false;
    private boolean zzcus = false;

    public zzady(Context context, zzaiy zzaiy, zzaee zzaee, String str, zzaej zzaej) {
        zzbq.checkNotNull(zzaee, "SafeBrowsing config is not present.");
        this.mContext = context.getApplicationContext() != null ? context.getApplicationContext() : context;
        this.zzcum = new LinkedHashMap<>();
        this.zzcun = zzaej;
        this.zzcrj = zzaee;
        for (String lowerCase : this.zzcrj.zzcva) {
            this.zzcup.add(lowerCase.toLowerCase(Locale.ENGLISH));
        }
        this.zzcup.remove("cookie".toLowerCase(Locale.ENGLISH));
        zzfho zzfho = new zzfho();
        zzfho.zzphs = 8;
        zzfho.url = str;
        zzfho.zzphu = str;
        zzfho.zzphw = new zzfhp();
        zzfho.zzphw.zzcuw = this.zzcrj.zzcuw;
        zzfhx zzfhx = new zzfhx();
        zzfhx.zzpjd = zzaiy.zzcp;
        zzfhx.zzpjf = Boolean.valueOf(zzbgc.zzcy(this.mContext).zzami());
        zze.zzafm();
        long zzcd = (long) zze.zzcd(this.mContext);
        if (zzcd > 0) {
            zzfhx.zzpje = Long.valueOf(zzcd);
        }
        zzfho.zzpig = zzfhx;
        this.zzcul = zzfho;
    }

    @Nullable
    private final zzfhw zzbv(String str) {
        zzfhw zzfhw;
        synchronized (this.mLock) {
            zzfhw = this.zzcum.get(str);
        }
        return zzfhw;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public final void send() {
        if ((this.zzcuo && this.zzcrj.zzcvc) || (this.zzcus && this.zzcrj.zzcvb) || (!this.zzcuo && this.zzcrj.zzcuz)) {
            synchronized (this.mLock) {
                this.zzcul.zzphx = new zzfhw[this.zzcum.size()];
                this.zzcum.values().toArray(this.zzcul.zzphx);
                if (zzaeg.isEnabled()) {
                    String str = this.zzcul.url;
                    String str2 = this.zzcul.zzphy;
                    StringBuilder sb = new StringBuilder(53 + String.valueOf(str).length() + String.valueOf(str2).length());
                    sb.append("Sending SB report\n  url: ");
                    sb.append(str);
                    sb.append("\n  clickUrl: ");
                    sb.append(str2);
                    sb.append("\n  resources: \n");
                    StringBuilder sb2 = new StringBuilder(sb.toString());
                    for (zzfhw zzfhw : this.zzcul.zzphx) {
                        sb2.append("    [");
                        sb2.append(zzfhw.zzpjc.length);
                        sb2.append("] ");
                        sb2.append(zzfhw.url);
                    }
                    zzaeg.zzbw(sb2.toString());
                }
                zzajp<String> zza = new zzahy(this.mContext).zza(1, this.zzcrj.zzcux, null, zzfhk.zzc(this.zzcul));
                if (zzaeg.isEnabled()) {
                    zza.zza(new zzaeb(this), zzagl.zzcyx);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0026, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(java.lang.String r7, java.util.Map<java.lang.String, java.lang.String> r8, int r9) {
        /*
            r6 = this;
            java.lang.Object r0 = r6.mLock
            monitor-enter(r0)
            r1 = 3
            if (r9 != r1) goto L_0x000d
            r2 = 1
            r6.zzcus = r2     // Catch:{ all -> 0x000a }
            goto L_0x000d
        L_0x000a:
            r7 = move-exception
            goto L_0x00ca
        L_0x000d:
            java.util.LinkedHashMap<java.lang.String, com.google.android.gms.internal.zzfhw> r2 = r6.zzcum     // Catch:{ all -> 0x000a }
            boolean r2 = r2.containsKey(r7)     // Catch:{ all -> 0x000a }
            if (r2 == 0) goto L_0x0027
            if (r9 != r1) goto L_0x0025
            java.util.LinkedHashMap<java.lang.String, com.google.android.gms.internal.zzfhw> r8 = r6.zzcum     // Catch:{ all -> 0x000a }
            java.lang.Object r7 = r8.get(r7)     // Catch:{ all -> 0x000a }
            com.google.android.gms.internal.zzfhw r7 = (com.google.android.gms.internal.zzfhw) r7     // Catch:{ all -> 0x000a }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x000a }
            r7.zzpjb = r8     // Catch:{ all -> 0x000a }
        L_0x0025:
            monitor-exit(r0)     // Catch:{ all -> 0x000a }
            return
        L_0x0027:
            com.google.android.gms.internal.zzfhw r1 = new com.google.android.gms.internal.zzfhw     // Catch:{ all -> 0x000a }
            r1.<init>()     // Catch:{ all -> 0x000a }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x000a }
            r1.zzpjb = r9     // Catch:{ all -> 0x000a }
            java.util.LinkedHashMap<java.lang.String, com.google.android.gms.internal.zzfhw> r9 = r6.zzcum     // Catch:{ all -> 0x000a }
            int r9 = r9.size()     // Catch:{ all -> 0x000a }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x000a }
            r1.zzjhb = r9     // Catch:{ all -> 0x000a }
            r1.url = r7     // Catch:{ all -> 0x000a }
            com.google.android.gms.internal.zzfhr r9 = new com.google.android.gms.internal.zzfhr     // Catch:{ all -> 0x000a }
            r9.<init>()     // Catch:{ all -> 0x000a }
            r1.zzpiw = r9     // Catch:{ all -> 0x000a }
            java.util.HashSet<java.lang.String> r9 = r6.zzcup     // Catch:{ all -> 0x000a }
            int r9 = r9.size()     // Catch:{ all -> 0x000a }
            if (r9 <= 0) goto L_0x00c3
            if (r8 == 0) goto L_0x00c3
            java.util.LinkedList r9 = new java.util.LinkedList     // Catch:{ all -> 0x000a }
            r9.<init>()     // Catch:{ all -> 0x000a }
            java.util.Set r8 = r8.entrySet()     // Catch:{ all -> 0x000a }
            java.util.Iterator r8 = r8.iterator()     // Catch:{ all -> 0x000a }
        L_0x005e:
            boolean r2 = r8.hasNext()     // Catch:{ all -> 0x000a }
            if (r2 == 0) goto L_0x00b6
            java.lang.Object r2 = r8.next()     // Catch:{ all -> 0x000a }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ all -> 0x000a }
            java.lang.Object r3 = r2.getKey()     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            if (r3 == 0) goto L_0x0077
            java.lang.Object r3 = r2.getKey()     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            goto L_0x0079
        L_0x0077:
            java.lang.String r3 = ""
        L_0x0079:
            java.lang.Object r4 = r2.getValue()     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            if (r4 == 0) goto L_0x0086
            java.lang.Object r2 = r2.getValue()     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            goto L_0x0088
        L_0x0086:
            java.lang.String r2 = ""
        L_0x0088:
            java.util.Locale r4 = java.util.Locale.ENGLISH     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            java.lang.String r4 = r3.toLowerCase(r4)     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            java.util.HashSet<java.lang.String> r5 = r6.zzcup     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            boolean r4 = r5.contains(r4)     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            if (r4 != 0) goto L_0x0097
            goto L_0x005e
        L_0x0097:
            com.google.android.gms.internal.zzfhq r4 = new com.google.android.gms.internal.zzfhq     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            r4.<init>()     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            java.lang.String r5 = "UTF-8"
            byte[] r3 = r3.getBytes(r5)     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            r4.zzpii = r3     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            java.lang.String r3 = "UTF-8"
            byte[] r2 = r2.getBytes(r3)     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            r4.zzocl = r2     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            r9.add(r4)     // Catch:{ UnsupportedEncodingException -> 0x00b0 }
            goto L_0x005e
        L_0x00b0:
            java.lang.String r2 = "Cannot convert string to bytes, skip header."
            com.google.android.gms.internal.zzaeg.zzbw(r2)     // Catch:{ all -> 0x000a }
            goto L_0x005e
        L_0x00b6:
            int r8 = r9.size()     // Catch:{ all -> 0x000a }
            com.google.android.gms.internal.zzfhq[] r8 = new com.google.android.gms.internal.zzfhq[r8]     // Catch:{ all -> 0x000a }
            r9.toArray(r8)     // Catch:{ all -> 0x000a }
            com.google.android.gms.internal.zzfhr r9 = r1.zzpiw     // Catch:{ all -> 0x000a }
            r9.zzpik = r8     // Catch:{ all -> 0x000a }
        L_0x00c3:
            java.util.LinkedHashMap<java.lang.String, com.google.android.gms.internal.zzfhw> r8 = r6.zzcum     // Catch:{ all -> 0x000a }
            r8.put(r7, r1)     // Catch:{ all -> 0x000a }
            monitor-exit(r0)     // Catch:{ all -> 0x000a }
            return
        L_0x00ca:
            monitor-exit(r0)     // Catch:{ all -> 0x000a }
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzady.zza(java.lang.String, java.util.Map, int):void");
    }

    public final void zzbu(String str) {
        synchronized (this.mLock) {
            this.zzcul.zzphy = str;
        }
    }

    public final void zzl(View view) {
        if (this.zzcrj.zzcuy && !this.zzcur) {
            zzbs.zzec();
            Bitmap zzn = zzagr.zzn(view);
            if (zzn == null) {
                zzaeg.zzbw("Failed to capture the webview bitmap.");
                return;
            }
            this.zzcur = true;
            zzagr.zzb(new zzadz(this, zzn));
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public final void zzo(@Nullable Map<String, String> map) throws JSONException {
        if (map != null) {
            for (String next : map.keySet()) {
                JSONArray optJSONArray = new JSONObject(map.get(next)).optJSONArray("matches");
                if (optJSONArray != null) {
                    synchronized (this.mLock) {
                        int length = optJSONArray.length();
                        zzfhw zzbv = zzbv(next);
                        if (zzbv == null) {
                            String valueOf = String.valueOf(next);
                            zzaeg.zzbw(valueOf.length() != 0 ? "Cannot find the corresponding resource object for ".concat(valueOf) : new String("Cannot find the corresponding resource object for "));
                        } else {
                            zzbv.zzpjc = new String[length];
                            boolean z = false;
                            for (int i = 0; i < length; i++) {
                                zzbv.zzpjc[i] = optJSONArray.getJSONObject(i).getString("threat_type");
                            }
                            boolean z2 = this.zzcuo;
                            if (length > 0) {
                                z = true;
                            }
                            this.zzcuo = z | z2;
                        }
                    }
                }
            }
        }
    }

    public final zzaee zzoe() {
        return this.zzcrj;
    }

    public final boolean zzof() {
        return zzq.zzalz() && this.zzcrj.zzcuy && !this.zzcur;
    }

    public final void zzog() {
        this.zzcuq = true;
    }

    public final void zzoh() {
        synchronized (this.mLock) {
            zzajp<Map<String, String>> zza = this.zzcun.zza(this.mContext, this.zzcum.keySet());
            zza.zza(new zzaea(this, zza), zzagl.zzcyx);
        }
    }
}
