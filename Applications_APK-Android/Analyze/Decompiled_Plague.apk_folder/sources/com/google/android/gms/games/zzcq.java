package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzcq extends zzac<Void> {
    private /* synthetic */ String zzhkj;

    zzcq(TurnBasedMultiplayerClient turnBasedMultiplayerClient, String str) {
        this.zzhkj = str;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException {
        gamesClientImpl.zzs(this.zzhkj, 1);
        taskCompletionSource.setResult(null);
    }
}
