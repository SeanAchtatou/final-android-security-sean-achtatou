package com.google.android.gms.internal;

import java.io.IOException;

public final class zzid {

    public static final class zza extends zzfee<zza, C0001zza> implements zzffk {
        /* access modifiers changed from: private */
        public static final zza zzbar;
        private static volatile zzffm<zza> zzbas;

        /* renamed from: com.google.android.gms.internal.zzid$zza$zza  reason: collision with other inner class name */
        public static final class C0001zza extends zzfef<zza, C0001zza> implements zzffk {
            private C0001zza() {
                super(zza.zzbar);
            }

            /* synthetic */ C0001zza(zzie zzie) {
                this();
            }
        }

        public enum zzb implements zzfes {
            UNKNOWN_EVENT_TYPE(0),
            AD_REQUEST(1),
            AD_LOADED(2),
            AD_FAILED_TO_LOAD(3),
            AD_FAILED_TO_LOAD_NO_FILL(4),
            AD_IMPRESSION(5),
            AD_FIRST_CLICK(6),
            AD_SUBSEQUENT_CLICK(7);
            
            private static final zzfet<zzb> zzbbb = new zzif();
            private final int value;

            private zzb(int i) {
                this.value = i;
            }

            public final int zzhn() {
                return this.value;
            }
        }

        static {
            zza zza = new zza();
            zzbar = zza;
            zza.zza(zzfem.zzpcf, (Object) null, (Object) null);
            zza.zzpbs.zzbim();
        }

        private zza() {
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzie.zzbaq[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return zzbar;
                case 3:
                    return null;
                case 4:
                    return new C0001zza(null);
                case 5:
                    return this;
                case 6:
                    zzfdq zzfdq = (zzfdq) obj;
                    if (((zzfea) obj2) != null) {
                        boolean z = false;
                        while (!z) {
                            try {
                                int zzcts = zzfdq.zzcts();
                                if (zzcts == 0 || !zza(zzcts, zzfdq)) {
                                    z = true;
                                }
                            } catch (zzfew e) {
                                throw new RuntimeException(e.zzh(this));
                            } catch (IOException e2) {
                                throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                            }
                        }
                        break;
                    } else {
                        throw new NullPointerException();
                    }
                case 7:
                    break;
                case 8:
                    if (zzbas == null) {
                        synchronized (zza.class) {
                            if (zzbas == null) {
                                zzbas = new zzfeg(zzbar);
                            }
                        }
                    }
                    return zzbas;
                default:
                    throw new UnsupportedOperationException();
            }
            return zzbar;
        }

        public final void zza(zzfdv zzfdv) throws IOException {
            this.zzpbs.zza(zzfdv);
        }

        public final int zzhl() {
            int i = this.zzpbt;
            if (i != -1) {
                return i;
            }
            int zzhl = 0 + this.zzpbs.zzhl();
            this.zzpbt = zzhl;
            return zzhl;
        }
    }
}
