package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;

public final class zzd extends zzc implements zza {
    private final zze zzhjw;

    public zzd(DataHolder dataHolder, int i, zze zze) {
        super(dataHolder, i);
        this.zzhjw = zze;
    }

    public final int describeContents() {
        return 0;
    }

    public final boolean equals(Object obj) {
        return zzb.zza(this, obj);
    }

    public final /* synthetic */ Object freeze() {
        return new zzb(this);
    }

    public final int hashCode() {
        return zzb.zza(this);
    }

    public final String toString() {
        return zzb.zzb(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((zzb) ((zza) freeze())).writeToParcel(parcel, i);
    }

    public final String zzatj() {
        return getString(this.zzhjw.zzhsq);
    }

    public final String zzatk() {
        return getString(this.zzhjw.zzhsr);
    }

    public final long zzatl() {
        return getLong(this.zzhjw.zzhss);
    }

    public final Uri zzatm() {
        return zzfw(this.zzhjw.zzhst);
    }

    public final Uri zzatn() {
        return zzfw(this.zzhjw.zzhsu);
    }

    public final Uri zzato() {
        return zzfw(this.zzhjw.zzhsv);
    }
}
