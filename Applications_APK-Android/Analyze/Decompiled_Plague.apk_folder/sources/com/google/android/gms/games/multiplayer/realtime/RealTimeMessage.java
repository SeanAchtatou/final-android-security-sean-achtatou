package com.google.android.gms.games.multiplayer.realtime;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbq;

public final class RealTimeMessage implements Parcelable {
    public static final Parcelable.Creator<RealTimeMessage> CREATOR = new zza();
    public static final int RELIABLE = 1;
    public static final int UNRELIABLE = 0;
    private final String zzhvb;
    private final byte[] zzhvc;
    private final int zzhvd;

    private RealTimeMessage(Parcel parcel) {
        this(parcel.readString(), parcel.createByteArray(), parcel.readInt());
    }

    /* synthetic */ RealTimeMessage(Parcel parcel, zza zza) {
        this(parcel);
    }

    private RealTimeMessage(String str, byte[] bArr, int i) {
        this.zzhvb = (String) zzbq.checkNotNull(str);
        this.zzhvc = (byte[]) ((byte[]) zzbq.checkNotNull(bArr)).clone();
        this.zzhvd = i;
    }

    public final int describeContents() {
        return 0;
    }

    public final byte[] getMessageData() {
        return this.zzhvc;
    }

    public final String getSenderParticipantId() {
        return this.zzhvb;
    }

    public final boolean isReliable() {
        return this.zzhvd == 1;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.zzhvb);
        parcel.writeByteArray(this.zzhvc);
        parcel.writeInt(this.zzhvd);
    }
}
