package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdss;
import com.google.android.gms.security.ProviderInstaller;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.Provider;
import java.security.Security;
import java.security.Signature;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.Mac;

public class zzdsr<T_WRAPPER extends zzdss<T_ENGINE>, T_ENGINE> {
    private static final Logger logger = Logger.getLogger(zzdsr.class.getName());
    private static final List<Provider> zzlvj;
    public static final zzdsr<zzdst, Cipher> zzlvk = new zzdsr<>(new zzdst());
    public static final zzdsr<zzdsx, Mac> zzlvl = new zzdsr<>(new zzdsx());
    private static zzdsr<zzdsz, Signature> zzlvm = new zzdsr<>(new zzdsz());
    private static zzdsr<zzdsy, MessageDigest> zzlvn = new zzdsr<>(new zzdsy());
    public static final zzdsr<zzdsu, KeyAgreement> zzlvo = new zzdsr<>(new zzdsu());
    public static final zzdsr<zzdsw, KeyPairGenerator> zzlvp = new zzdsr<>(new zzdsw());
    private static zzdsr<zzdsv, KeyFactory> zzlvq = new zzdsr<>(new zzdsv());
    private T_WRAPPER zzlvr;
    private List<Provider> zzlvs = zzlvj;
    private boolean zzlvt = true;

    static {
        if (zzdte.zzaiy()) {
            String[] strArr = {ProviderInstaller.PROVIDER_NAME, "AndroidOpenSSL"};
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < 2; i++) {
                String str = strArr[i];
                Provider provider = Security.getProvider(str);
                if (provider != null) {
                    arrayList.add(provider);
                } else {
                    logger.logp(Level.INFO, "com.google.crypto.tink.subtle.EngineFactory", "toProviderList", String.format("Provider %s not available", str));
                }
            }
            zzlvj = arrayList;
        } else {
            zzlvj = new ArrayList();
        }
    }

    private zzdsr(T_WRAPPER t_wrapper) {
        this.zzlvr = t_wrapper;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T_WRAPPER
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private final boolean zza(java.lang.String r2, java.security.Provider r3) {
        /*
            r1 = this;
            T_WRAPPER r0 = r1.zzlvr     // Catch:{ Exception -> 0x0007 }
            r0.zzb(r2, r3)     // Catch:{ Exception -> 0x0007 }
            r2 = 1
            return r2
        L_0x0007:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdsr.zza(java.lang.String, java.security.Provider):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T_WRAPPER
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final T_ENGINE zzoc(java.lang.String r4) throws java.security.GeneralSecurityException {
        /*
            r3 = this;
            java.util.List<java.security.Provider> r0 = r3.zzlvs
            java.util.Iterator r0 = r0.iterator()
        L_0x0006:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x001f
            java.lang.Object r1 = r0.next()
            java.security.Provider r1 = (java.security.Provider) r1
            boolean r2 = r3.zza(r4, r1)
            if (r2 == 0) goto L_0x0006
            T_WRAPPER r0 = r3.zzlvr
        L_0x001a:
            java.lang.Object r4 = r0.zzb(r4, r1)
            return r4
        L_0x001f:
            boolean r0 = r3.zzlvt
            if (r0 == 0) goto L_0x0027
            T_WRAPPER r0 = r3.zzlvr
            r1 = 0
            goto L_0x001a
        L_0x0027:
            java.security.GeneralSecurityException r4 = new java.security.GeneralSecurityException
            java.lang.String r0 = "No good Provider found."
            r4.<init>(r0)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdsr.zzoc(java.lang.String):java.lang.Object");
    }
}
