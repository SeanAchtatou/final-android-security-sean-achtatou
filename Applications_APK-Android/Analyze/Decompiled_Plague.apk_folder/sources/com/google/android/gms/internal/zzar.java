package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.http.AndroidHttpClient;
import android.os.Build;
import java.io.File;

public final class zzar {
    public static zzs zza(Context context, zzam zzam) {
        String str;
        File file = new File(context.getCacheDir(), "volley");
        try {
            String packageName = context.getPackageName();
            int i = context.getPackageManager().getPackageInfo(packageName, 0).versionCode;
            StringBuilder sb = new StringBuilder(12 + String.valueOf(packageName).length());
            sb.append(packageName);
            sb.append("/");
            sb.append(i);
            str = sb.toString();
        } catch (PackageManager.NameNotFoundException unused) {
            str = "volley/0";
        }
        zzs zzs = new zzs(new zzag(file), new zzad(Build.VERSION.SDK_INT >= 9 ? new zzan() : new zzaj(AndroidHttpClient.newInstance(str))));
        zzs.start();
        return zzs;
    }
}
