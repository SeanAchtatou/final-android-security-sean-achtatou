package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzac implements zzf {
    private /* synthetic */ GoogleApiClient.ConnectionCallbacks zzfxf;

    zzac(GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        this.zzfxf = connectionCallbacks;
    }

    public final void onConnected(@Nullable Bundle bundle) {
        this.zzfxf.onConnected(bundle);
    }

    public final void onConnectionSuspended(int i) {
        this.zzfxf.onConnectionSuspended(i);
    }
}
