package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzbq;

public final class zzcl<L> {
    private final zzcm zzfrp;
    private volatile L zzfrq;
    private final zzcn<L> zzfrr;

    zzcl(@NonNull Looper looper, @NonNull L l, @NonNull String str) {
        this.zzfrp = new zzcm(this, looper);
        this.zzfrq = zzbq.checkNotNull(l, "Listener must not be null");
        this.zzfrr = new zzcn<>(l, zzbq.zzgh(str));
    }

    public final void clear() {
        this.zzfrq = null;
    }

    public final void zza(zzco<? super L> zzco) {
        zzbq.checkNotNull(zzco, "Notifier must not be null");
        this.zzfrp.sendMessage(this.zzfrp.obtainMessage(1, zzco));
    }

    public final boolean zzadx() {
        return this.zzfrq != null;
    }

    @NonNull
    public final zzcn<L> zzajc() {
        return this.zzfrr;
    }

    /* access modifiers changed from: package-private */
    public final void zzb(zzco<? super L> zzco) {
        L l = this.zzfrq;
        if (l == null) {
            zzco.zzahn();
            return;
        }
        try {
            zzco.zzt(l);
        } catch (RuntimeException e) {
            zzco.zzahn();
            throw e;
        }
    }
}
