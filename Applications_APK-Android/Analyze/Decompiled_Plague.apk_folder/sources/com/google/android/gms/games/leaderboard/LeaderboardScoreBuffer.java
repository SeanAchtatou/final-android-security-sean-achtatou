package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.data.AbstractDataBuffer;
import com.google.android.gms.common.data.DataHolder;

public final class LeaderboardScoreBuffer extends AbstractDataBuffer<LeaderboardScore> {
    private final zza zzhto;

    public LeaderboardScoreBuffer(DataHolder dataHolder) {
        super(dataHolder);
        this.zzhto = new zza(dataHolder.zzafx());
    }

    public final LeaderboardScore get(int i) {
        return new LeaderboardScoreRef(this.zzfnz, i);
    }

    public final zza zzatr() {
        return this.zzhto;
    }
}
