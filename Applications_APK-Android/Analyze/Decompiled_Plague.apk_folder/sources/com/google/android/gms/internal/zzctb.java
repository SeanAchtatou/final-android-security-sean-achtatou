package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzctb implements Parcelable.Creator<zzcsv> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        String str = null;
        byte[] bArr = null;
        byte[][] bArr2 = null;
        byte[][] bArr3 = null;
        byte[][] bArr4 = null;
        byte[][] bArr5 = null;
        int[] iArr = null;
        byte[][] bArr6 = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = zzbek.zzq(parcel, readInt);
                    break;
                case 3:
                    bArr = zzbek.zzt(parcel, readInt);
                    break;
                case 4:
                    bArr2 = zzbek.zzu(parcel, readInt);
                    break;
                case 5:
                    bArr3 = zzbek.zzu(parcel, readInt);
                    break;
                case 6:
                    bArr4 = zzbek.zzu(parcel, readInt);
                    break;
                case 7:
                    bArr5 = zzbek.zzu(parcel, readInt);
                    break;
                case 8:
                    iArr = zzbek.zzw(parcel, readInt);
                    break;
                case 9:
                    bArr6 = zzbek.zzu(parcel, readInt);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzcsv(str, bArr, bArr2, bArr3, bArr4, bArr5, iArr, bArr6);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzcsv[i];
    }
}
