package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfeo extends zzfee<zzfeo, zza> implements zzffk {
    private static volatile zzffm<zzfeo> zzbas;
    /* access modifiers changed from: private */
    public static final zzfeo zzpcp;
    private int zzpco;

    public static final class zza extends zzfef<zzfeo, zza> implements zzffk {
        private zza() {
            super(zzfeo.zzpcp);
        }

        /* synthetic */ zza(zzfep zzfep) {
            this();
        }

        public final zza zzli(int i) {
            zzcvi();
            ((zzfeo) this.zzpbv).setValue(i);
            return this;
        }
    }

    static {
        zzfeo zzfeo = new zzfeo();
        zzpcp = zzfeo;
        zzfeo.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzfeo.zzpbs.zzbim();
    }

    private zzfeo() {
    }

    /* access modifiers changed from: private */
    public final void setValue(int i) {
        this.zzpco = i;
    }

    public static zza zzcvn() {
        zzfeo zzfeo = zzpcp;
        zzfef zzfef = (zzfef) zzfeo.zza(zzfem.zzpch, (Object) null, (Object) null);
        zzfef.zza((zzfee) zzfeo);
        return (zza) zzfef;
    }

    public static zzfeo zzcvo() {
        return zzpcp;
    }

    public final int getValue() {
        return this.zzpco;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        boolean z;
        boolean z2 = true;
        switch (zzfep.zzbaq[i - 1]) {
            case 1:
                return new zzfeo();
            case 2:
                return zzpcp;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzfeo zzfeo = (zzfeo) obj2;
                boolean z3 = this.zzpco != 0;
                int i2 = this.zzpco;
                if (zzfeo.zzpco == 0) {
                    z2 = false;
                }
                this.zzpco = zzfen.zza(z3, i2, z2, zzfeo.zzpco);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                if (((zzfea) obj2) != null) {
                    boolean z4 = false;
                    while (!z4) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts != 8) {
                                    if ((zzcts & 7) == 4) {
                                        z = false;
                                    } else {
                                        if (this.zzpbs == zzfgi.zzcwu()) {
                                            this.zzpbs = zzfgi.zzcwv();
                                        }
                                        z = this.zzpbs.zzb(zzcts, zzfdq);
                                    }
                                    if (!z) {
                                    }
                                } else {
                                    this.zzpco = zzfdq.zzctv();
                                }
                            }
                            z4 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzfeo.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzpcp);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzpcp;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzpco != 0) {
            zzfdv.zzaa(1, this.zzpco);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzpco != 0) {
            i2 = 0 + zzfdv.zzad(1, this.zzpco);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
