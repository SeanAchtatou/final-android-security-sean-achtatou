package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzao extends zzac<Void> {
    private /* synthetic */ int zzhiz;

    zzao(NotificationsClient notificationsClient, int i) {
        this.zzhiz = i;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException {
        gamesClientImpl.zzdj(this.zzhiz);
        taskCompletionSource.setResult(null);
    }
}
