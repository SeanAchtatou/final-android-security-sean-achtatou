package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzbf extends zzbh {
    private /* synthetic */ int zzhqf;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbf(zzaz zzaz, GoogleApiClient googleApiClient, int i) {
        super(googleApiClient);
        this.zzhqf = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.Players$LoadPlayersResult>, java.lang.String, int, boolean, boolean):void
     arg types: [com.google.android.gms.games.internal.api.zzbf, java.lang.String, int, int, int]
     candidates:
      com.google.android.gms.games.internal.GamesClientImpl.zza(int, byte[], int, android.graphics.Bitmap, java.lang.String):android.content.Intent
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.leaderboard.Leaderboards$LoadPlayerScoreResult>, java.lang.String, java.lang.String, int, int):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.snapshot.Snapshots$OpenSnapshotResult>, java.lang.String, java.lang.String, com.google.android.gms.games.snapshot.SnapshotMetadataChange, com.google.android.gms.games.snapshot.SnapshotContents):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer$UpdateMatchResult>, java.lang.String, byte[], java.lang.String, com.google.android.gms.games.multiplayer.ParticipantResult[]):void
      com.google.android.gms.games.internal.GamesClientImpl.zza(com.google.android.gms.common.api.internal.zzn<com.google.android.gms.games.Players$LoadPlayersResult>, java.lang.String, int, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza((zzn<Players.LoadPlayersResult>) this, "played_with", this.zzhqf, true, false);
    }
}
