package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.drive.DriveApi;

final class zzbkz extends zzbjv {
    private final zzn<DriveApi.DriveIdResult> zzfzc;

    public zzbkz(zzn<DriveApi.DriveIdResult> zzn) {
        this.zzfzc = zzn;
    }

    public final void onError(Status status) throws RemoteException {
        this.zzfzc.setResult(new zzbla(status, null));
    }

    public final void zza(zzbpy zzbpy) throws RemoteException {
        this.zzfzc.setResult(new zzbla(Status.zzfko, zzbpy.zzgjm));
    }

    public final void zza(zzbqj zzbqj) throws RemoteException {
        this.zzfzc.setResult(new zzbla(Status.zzfko, new zzbkk(zzbqj.zzgjz).getDriveId()));
    }
}
