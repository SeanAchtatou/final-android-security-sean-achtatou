package com.google.android.gms.dynamic;

final class zzg implements zzi {
    private /* synthetic */ zza zzgtn;

    zzg(zza zza) {
        this.zzgtn = zza;
    }

    public final int getState() {
        return 4;
    }

    public final void zzb(LifecycleDelegate lifecycleDelegate) {
        this.zzgtn.zzgtj.onStart();
    }
}
