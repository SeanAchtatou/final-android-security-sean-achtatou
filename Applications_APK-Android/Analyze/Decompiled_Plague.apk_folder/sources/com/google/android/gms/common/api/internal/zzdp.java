package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class zzdp<A extends Api.zzb, L> {
    private final zzcn<L> zzfrr;

    protected zzdp(zzcn<L> zzcn) {
        this.zzfrr = zzcn;
    }

    public final zzcn<L> zzajc() {
        return this.zzfrr;
    }

    /* access modifiers changed from: protected */
    public abstract void zzc(A a, TaskCompletionSource<Boolean> taskCompletionSource) throws RemoteException;
}
