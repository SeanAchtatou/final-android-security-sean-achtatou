package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.zzp;

public final class zzbrd extends zzbej {
    public static final Parcelable.Creator<zzbrd> CREATOR = new zzbre();
    private int zzgas;
    private DriveId zzgfy;
    private zzp zzgjl;

    public zzbrd(DriveId driveId, int i) {
        this(driveId, i, null);
    }

    zzbrd(DriveId driveId, int i, zzp zzp) {
        this.zzgfy = driveId;
        this.zzgas = i;
        this.zzgjl = zzp;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.DriveId, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.events.zzp, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, (Parcelable) this.zzgfy, i, false);
        zzbem.zzc(parcel, 3, this.zzgas);
        zzbem.zza(parcel, 4, (Parcelable) this.zzgjl, i, false);
        zzbem.zzai(parcel, zze);
    }
}
