package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

final class zzc implements DynamiteModule.zzd {
    zzc() {
    }

    public final zzj zza(Context context, String str, zzi zzi) throws DynamiteModule.zzc {
        zzj zzj = new zzj();
        zzj.zzgum = zzi.zzab(context, str);
        if (zzj.zzgum != 0) {
            zzj.zzguo = -1;
            return zzj;
        }
        zzj.zzgun = zzi.zzc(context, str, true);
        if (zzj.zzgun != 0) {
            zzj.zzguo = 1;
        }
        return zzj;
    }
}
