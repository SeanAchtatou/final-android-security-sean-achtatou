package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;

public final class zzl implements ServiceConnection {
    private /* synthetic */ zzd zzfwg;
    private final int zzfwj;

    public zzl(zzd zzd, int i) {
        this.zzfwg = zzd;
        this.zzfwj = i;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        zzay zzay;
        if (iBinder == null) {
            this.zzfwg.zzcf(16);
            return;
        }
        synchronized (this.zzfwg.zzfvq) {
            zzd zzd = this.zzfwg;
            if (iBinder == null) {
                zzay = null;
            } else {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                zzay = (queryLocalInterface == null || !(queryLocalInterface instanceof zzay)) ? new zzaz(iBinder) : (zzay) queryLocalInterface;
            }
            zzay unused = zzd.zzfvr = zzay;
        }
        this.zzfwg.zza(0, (Bundle) null, this.zzfwj);
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.zzfwg.zzfvq) {
            zzay unused = this.zzfwg.zzfvr = (zzay) null;
        }
        this.zzfwg.mHandler.sendMessage(this.zzfwg.mHandler.obtainMessage(6, this.zzfwj, 1));
    }
}
