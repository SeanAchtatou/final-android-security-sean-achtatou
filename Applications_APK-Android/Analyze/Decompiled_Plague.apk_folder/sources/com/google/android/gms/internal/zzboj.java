package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import java.util.List;

final class zzboj extends zzblk {
    private /* synthetic */ List zzgmn;
    private /* synthetic */ zzbog zzgng;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzboj(zzbog zzbog, GoogleApiClient googleApiClient, List list) {
        super(googleApiClient);
        this.zzgng = zzbog;
        this.zzgmn = list;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(new zzbrh(this.zzgng.zzgfy, this.zzgmn), new zzbrj(this));
    }
}
