package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbqm implements Parcelable.Creator<zzbql> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        zzbqz zzbqz = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                zzbek.zzb(parcel, readInt);
            } else {
                zzbqz = (zzbqz) zzbek.zza(parcel, readInt, zzbqz.CREATOR);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbql(zzbqz);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbql[i];
    }
}
