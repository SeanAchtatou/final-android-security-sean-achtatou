package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzcf extends zzcp {
    private /* synthetic */ String zzhqn;
    private /* synthetic */ boolean zzhqo;
    private /* synthetic */ int zzhqp;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcf(zzcd zzcd, GoogleApiClient googleApiClient, String str, boolean z, int i) {
        super(googleApiClient, null);
        this.zzhqn = str;
        this.zzhqo = z;
        this.zzhqp = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhqn, this.zzhqo, this.zzhqp);
    }
}
