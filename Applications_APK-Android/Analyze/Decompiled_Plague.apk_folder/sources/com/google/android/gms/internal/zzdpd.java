package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrk;
import java.security.GeneralSecurityException;

final class zzdpd implements zzdos<zzdtb> {
    zzdpd() {
    }

    private static void zza(zzdpy zzdpy) throws GeneralSecurityException {
        if (zzdpy.zzblz() < 12 || zzdpy.zzblz() > 16) {
            throw new GeneralSecurityException("invalid IV size");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: zze */
    public final zzdrx zza(zzfdh zzfdh) throws GeneralSecurityException {
        try {
            zzdpu zzk = zzdpu.zzk(zzfdh);
            if (!(zzk instanceof zzdpu)) {
                throw new GeneralSecurityException("expected AesCtrKey proto");
            }
            zzdpu zzdpu = zzk;
            zzdte.zzt(zzdpu.getVersion(), 0);
            zzdte.zzgc(zzdpu.zzblt().size());
            zza(zzdpu.zzbls());
            return new zzdrx(zzdpu.zzblt().toByteArray(), zzdpu.zzbls().zzblz());
        } catch (zzfew e) {
            throw new GeneralSecurityException("expected serialized AesCtrKey proto", e);
        }
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.AesCtrKey";
    }

    public final /* synthetic */ Object zza(zzffi zzffi) throws GeneralSecurityException {
        if (!(zzffi instanceof zzdpu)) {
            throw new GeneralSecurityException("expected AesCtrKey proto");
        }
        zzdpu zzdpu = (zzdpu) zzffi;
        zzdte.zzt(zzdpu.getVersion(), 0);
        zzdte.zzgc(zzdpu.zzblt().size());
        zza(zzdpu.zzbls());
        return new zzdrx(zzdpu.zzblt().toByteArray(), zzdpu.zzbls().zzblz());
    }

    public final zzffi zzb(zzfdh zzfdh) throws GeneralSecurityException {
        try {
            return zzb(zzdpw.zzm(zzfdh));
        } catch (zzfew e) {
            throw new GeneralSecurityException("expected serialized AesCtrKeyFormat proto", e);
        }
    }

    public final zzffi zzb(zzffi zzffi) throws GeneralSecurityException {
        if (!(zzffi instanceof zzdpw)) {
            throw new GeneralSecurityException("expected AesCtrKeyFormat proto");
        }
        zzdpw zzdpw = (zzdpw) zzffi;
        zzdte.zzgc(zzdpw.getKeySize());
        zza(zzdpw.zzbls());
        return zzdpu.zzblu().zzc(zzdpw.zzbls()).zzl(zzfdh.zzay(zzdtd.zzgb(zzdpw.getKeySize()))).zzfj(0).zzcvk();
    }

    public final zzdrk zzc(zzfdh zzfdh) throws GeneralSecurityException {
        return (zzdrk) zzdrk.zzbnv().zzoa("type.googleapis.com/google.crypto.tink.AesCtrKey").zzaa(((zzdpu) zzb(zzfdh)).toByteString()).zzb(zzdrk.zzb.SYMMETRIC).zzcvk();
    }
}
