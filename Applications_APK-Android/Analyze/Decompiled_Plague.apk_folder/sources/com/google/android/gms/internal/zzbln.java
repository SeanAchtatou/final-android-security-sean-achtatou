package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzbln extends zzblk {
    private /* synthetic */ zzbov zzgkx;
    private /* synthetic */ zzbrd zzgky;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbln(zzbll zzbll, GoogleApiClient googleApiClient, zzbrd zzbrd, zzbov zzbov) {
        super(googleApiClient);
        this.zzgky = zzbrd;
        this.zzgkx = zzbov;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(this.zzgky, this.zzgkx, (String) null, new zzbrj(this));
    }
}
