package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

public final class zzbqo extends zzbej {
    public static final Parcelable.Creator<zzbqo> CREATOR = new zzbqp();
    private final List<String> zzgom;

    zzbqo(List<String> list) {
        this.zzgom = list;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zzb(parcel, 2, this.zzgom, false);
        zzbem.zzai(parcel, zze);
    }
}
