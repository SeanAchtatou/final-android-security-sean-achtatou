package com.google.android.gms.games;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.Leaderboards;

final class zzac implements zzbo<Leaderboards.LoadPlayerScoreResult, LeaderboardScore> {
    zzac() {
    }

    public final /* synthetic */ Object zzb(Result result) {
        LeaderboardScore score;
        Leaderboards.LoadPlayerScoreResult loadPlayerScoreResult = (Leaderboards.LoadPlayerScoreResult) result;
        if (loadPlayerScoreResult == null || (score = loadPlayerScoreResult.getScore()) == null) {
            return null;
        }
        return (LeaderboardScore) score.freeze();
    }
}
