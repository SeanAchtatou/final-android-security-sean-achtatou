package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.support.v4.util.ArraySet;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzbq;

public class zzak extends zzo {
    private zzbp zzfjo;
    private final ArraySet<zzh<?>> zzfny = new ArraySet<>();

    private zzak(zzci zzci) {
        super(zzci);
        this.zzfrj.zza("ConnectionlessLifecycleHelper", this);
    }

    public static void zza(Activity activity, zzbp zzbp, zzh<?> zzh) {
        zzn(activity);
        zzci zzn = zzn(activity);
        zzak zzak = (zzak) zzn.zza("ConnectionlessLifecycleHelper", zzak.class);
        if (zzak == null) {
            zzak = new zzak(zzn);
        }
        zzak.zzfjo = zzbp;
        zzbq.checkNotNull(zzh, "ApiKey cannot be null");
        zzak.zzfny.add(zzh);
        zzbp.zza(zzak);
    }

    private final void zzahm() {
        if (!this.zzfny.isEmpty()) {
            this.zzfjo.zza(this);
        }
    }

    public final void onResume() {
        super.onResume();
        zzahm();
    }

    public final void onStart() {
        super.onStart();
        zzahm();
    }

    public final void onStop() {
        super.onStop();
        this.zzfjo.zzb(this);
    }

    /* access modifiers changed from: protected */
    public final void zza(ConnectionResult connectionResult, int i) {
        this.zzfjo.zza(connectionResult, i);
    }

    /* access modifiers changed from: protected */
    public final void zzagm() {
        this.zzfjo.zzagm();
    }

    /* access modifiers changed from: package-private */
    public final ArraySet<zzh<?>> zzahl() {
        return this.zzfny;
    }
}
