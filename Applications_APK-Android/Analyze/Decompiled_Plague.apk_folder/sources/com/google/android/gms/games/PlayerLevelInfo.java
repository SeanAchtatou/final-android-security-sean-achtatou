package com.google.android.gms.games;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class PlayerLevelInfo extends zzc {
    public static final Parcelable.Creator<PlayerLevelInfo> CREATOR = new zzar();
    private final long zzhjs;
    private final long zzhjt;
    private final PlayerLevel zzhju;
    private final PlayerLevel zzhjv;

    public PlayerLevelInfo(long j, long j2, PlayerLevel playerLevel, PlayerLevel playerLevel2) {
        zzbq.checkState(j != -1);
        zzbq.checkNotNull(playerLevel);
        zzbq.checkNotNull(playerLevel2);
        this.zzhjs = j;
        this.zzhjt = j2;
        this.zzhju = playerLevel;
        this.zzhjv = playerLevel2;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof PlayerLevelInfo)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        PlayerLevelInfo playerLevelInfo = (PlayerLevelInfo) obj;
        return zzbg.equal(Long.valueOf(this.zzhjs), Long.valueOf(playerLevelInfo.zzhjs)) && zzbg.equal(Long.valueOf(this.zzhjt), Long.valueOf(playerLevelInfo.zzhjt)) && zzbg.equal(this.zzhju, playerLevelInfo.zzhju) && zzbg.equal(this.zzhjv, playerLevelInfo.zzhjv);
    }

    public final PlayerLevel getCurrentLevel() {
        return this.zzhju;
    }

    public final long getCurrentXpTotal() {
        return this.zzhjs;
    }

    public final long getLastLevelUpTimestamp() {
        return this.zzhjt;
    }

    public final PlayerLevel getNextLevel() {
        return this.zzhjv;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.zzhjs), Long.valueOf(this.zzhjt), this.zzhju, this.zzhjv});
    }

    public final boolean isMaxLevel() {
        return this.zzhju.equals(this.zzhjv);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.PlayerLevel, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, getCurrentXpTotal());
        zzbem.zza(parcel, 2, getLastLevelUpTimestamp());
        zzbem.zza(parcel, 3, (Parcelable) getCurrentLevel(), i, false);
        zzbem.zza(parcel, 4, (Parcelable) getNextLevel(), i, false);
        zzbem.zzai(parcel, zze);
    }
}
