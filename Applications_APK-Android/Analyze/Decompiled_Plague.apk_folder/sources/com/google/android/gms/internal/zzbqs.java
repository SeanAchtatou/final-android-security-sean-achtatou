package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbqs extends zzbej {
    public static final Parcelable.Creator<zzbqs> CREATOR = new zzbqt();
    private boolean zzgkl;

    public zzbqs(boolean z) {
        this.zzgkl = z;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, this.zzgkl);
        zzbem.zzai(parcel, zze);
    }
}
