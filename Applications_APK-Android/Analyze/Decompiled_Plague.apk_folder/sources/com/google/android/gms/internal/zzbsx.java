package com.google.android.gms.internal;

import com.google.android.gms.drive.metadata.SearchableOrderedMetadataField;
import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.drive.metadata.internal.zze;
import java.util.Date;

public final class zzbsx extends zze implements SearchableOrderedMetadataField<Date>, SortableMetadataField<Date> {
    public zzbsx(String str, int i) {
        super(str, 4100000);
    }
}
