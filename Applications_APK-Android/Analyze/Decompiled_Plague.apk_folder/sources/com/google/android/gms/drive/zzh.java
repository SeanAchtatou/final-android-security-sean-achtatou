package com.google.android.gms.drive;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.internal.zzbll;

final class zzh extends Api.zza<zzbll, Drive.zza> {
    zzh() {
    }

    public final /* synthetic */ Api.zze zza(Context context, Looper looper, zzr zzr, Object obj, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        return new zzbll(context, looper, zzr, connectionCallbacks, onConnectionFailedListener, ((Drive.zza) obj).zzann());
    }
}
