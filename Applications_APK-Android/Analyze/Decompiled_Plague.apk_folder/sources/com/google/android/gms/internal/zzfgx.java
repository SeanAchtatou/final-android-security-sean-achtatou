package com.google.android.gms.internal;

import java.io.IOException;

enum zzfgx {
    LOOSE,
    STRICT,
    LAZY;

    /* access modifiers changed from: package-private */
    public abstract Object zzb(zzfdq zzfdq) throws IOException;
}
