package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzdf;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbnt extends zzdf<zzbll, Metadata> {
    private /* synthetic */ MetadataChangeSet zzglq;
    private /* synthetic */ DriveResource zzgmc;

    zzbnt(zzbmu zzbmu, MetadataChangeSet metadataChangeSet, DriveResource driveResource) {
        this.zzglq = metadataChangeSet;
        this.zzgmc = driveResource;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        zzbll zzbll = (zzbll) zzb;
        this.zzglq.zzanz().setContext(zzbll.getContext());
        ((zzbpf) zzbll.zzakb()).zza(new zzbrq(this.zzgmc.getDriveId(), this.zzglq.zzanz()), new zzbry(taskCompletionSource));
    }
}
