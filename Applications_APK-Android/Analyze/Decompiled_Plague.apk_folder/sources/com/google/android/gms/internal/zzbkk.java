package com.google.android.gms.internal;

import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class zzbkk extends Metadata {
    private final MetadataBundle zzgkf;

    public zzbkk(MetadataBundle metadataBundle) {
        this.zzgkf = metadataBundle;
    }

    public final /* synthetic */ Object freeze() {
        return new zzbkk(this.zzgkf.zzape());
    }

    public final boolean isDataValid() {
        return this.zzgkf != null;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.zzgkf);
        StringBuilder sb = new StringBuilder(17 + String.valueOf(valueOf).length());
        sb.append("Metadata [mImpl=");
        sb.append(valueOf);
        sb.append("]");
        return sb.toString();
    }

    public final <T> T zza(MetadataField<T> metadataField) {
        return this.zzgkf.zza(metadataField);
    }
}
