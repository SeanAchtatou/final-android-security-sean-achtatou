package com.google.android.gms.common.data;

import java.util.NoSuchElementException;

public final class zzh<T> extends zzb<T> {
    private T zzftw;

    public zzh(DataBuffer<T> dataBuffer) {
        super(dataBuffer);
    }

    public final T next() {
        if (!hasNext()) {
            int i = this.zzftb;
            StringBuilder sb = new StringBuilder(46);
            sb.append("Cannot advance the iterator beyond ");
            sb.append(i);
            throw new NoSuchElementException(sb.toString());
        }
        this.zzftb++;
        if (this.zzftb == 0) {
            this.zzftw = this.zzfta.get(0);
            if (!(this.zzftw instanceof zzc)) {
                String valueOf = String.valueOf(this.zzftw.getClass());
                StringBuilder sb2 = new StringBuilder(44 + String.valueOf(valueOf).length());
                sb2.append("DataBuffer reference of type ");
                sb2.append(valueOf);
                sb2.append(" is not movable");
                throw new IllegalStateException(sb2.toString());
            }
        } else {
            ((zzc) this.zzftw).zzbx(this.zzftb);
        }
        return this.zzftw;
    }
}
