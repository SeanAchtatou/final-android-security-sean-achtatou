package com.google.android.gms.games.multiplayer.realtime;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzg;

public final class zzb extends zzg<Room> {
    public zzb(DataHolder dataHolder) {
        super(dataHolder);
    }

    /* access modifiers changed from: protected */
    public final String zzajo() {
        return "external_match_id";
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzl(int i, int i2) {
        return new zzf(this.zzfnz, i, i2);
    }
}
