package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfee;
import com.google.android.gms.internal.zzfef;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract class zzfee<MessageType extends zzfee<MessageType, BuilderType>, BuilderType extends zzfef<MessageType, BuilderType>> extends zzfcz<MessageType, BuilderType> {
    protected zzfgi zzpbs = zzfgi.zzcwu();
    protected int zzpbt = -1;

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected static <T extends com.google.android.gms.internal.zzfee<T, ?>> T zza(com.google.android.gms.internal.zzfee r4, com.google.android.gms.internal.zzfdh r5) throws com.google.android.gms.internal.zzfew {
        /*
            com.google.android.gms.internal.zzfea r0 = com.google.android.gms.internal.zzfea.zzcuz()
            com.google.android.gms.internal.zzfee r4 = zza(r4, r5, r0)
            r5 = 0
            r0 = 1
            r1 = 0
            if (r4 == 0) goto L_0x002a
            int r2 = com.google.android.gms.internal.zzfem.zzpcc
            java.lang.Boolean r3 = java.lang.Boolean.TRUE
            java.lang.Object r2 = r4.zza(r2, r3, r1)
            if (r2 == 0) goto L_0x0019
            r2 = r0
            goto L_0x001a
        L_0x0019:
            r2 = r5
        L_0x001a:
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.zzfgh r5 = new com.google.android.gms.internal.zzfgh
            r5.<init>(r4)
            com.google.android.gms.internal.zzfew r5 = r5.zzcwt()
            com.google.android.gms.internal.zzfew r4 = r5.zzh(r4)
            throw r4
        L_0x002a:
            if (r4 == 0) goto L_0x0047
            int r2 = com.google.android.gms.internal.zzfem.zzpcc
            java.lang.Boolean r3 = java.lang.Boolean.TRUE
            java.lang.Object r1 = r4.zza(r2, r3, r1)
            if (r1 == 0) goto L_0x0037
            r5 = r0
        L_0x0037:
            if (r5 != 0) goto L_0x0047
            com.google.android.gms.internal.zzfgh r5 = new com.google.android.gms.internal.zzfgh
            r5.<init>(r4)
            com.google.android.gms.internal.zzfew r5 = r5.zzcwt()
            com.google.android.gms.internal.zzfew r4 = r5.zzh(r4)
            throw r4
        L_0x0047:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfee.zza(com.google.android.gms.internal.zzfee, com.google.android.gms.internal.zzfdh):com.google.android.gms.internal.zzfee");
    }

    private static <T extends zzfee<T, ?>> T zza(zzfee zzfee, zzfdh zzfdh, zzfea zzfea) throws zzfew {
        T zza;
        try {
            zzfdq zzctl = zzfdh.zzctl();
            zza = zza(zzfee, zzctl, zzfea);
            zzctl.zzkf(0);
            return zza;
        } catch (zzfew e) {
            throw e.zzh(zza);
        } catch (zzfew e2) {
            throw e2;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    static <T extends com.google.android.gms.internal.zzfee<T, ?>> T zza(com.google.android.gms.internal.zzfee r2, com.google.android.gms.internal.zzfdq r3, com.google.android.gms.internal.zzfea r4) throws com.google.android.gms.internal.zzfew {
        /*
            int r0 = com.google.android.gms.internal.zzfem.zzpcg
            r1 = 0
            java.lang.Object r2 = r2.zza(r0, r1, r1)
            com.google.android.gms.internal.zzfee r2 = (com.google.android.gms.internal.zzfee) r2
            int r0 = com.google.android.gms.internal.zzfem.zzpce     // Catch:{ RuntimeException -> 0x0019 }
            r2.zza(r0, r3, r4)     // Catch:{ RuntimeException -> 0x0019 }
            int r3 = com.google.android.gms.internal.zzfem.zzpcf     // Catch:{ RuntimeException -> 0x0019 }
            r2.zza(r3, r1, r1)     // Catch:{ RuntimeException -> 0x0019 }
            com.google.android.gms.internal.zzfgi r3 = r2.zzpbs     // Catch:{ RuntimeException -> 0x0019 }
            r3.zzbim()     // Catch:{ RuntimeException -> 0x0019 }
            return r2
        L_0x0019:
            r2 = move-exception
            java.lang.Throwable r3 = r2.getCause()
            boolean r3 = r3 instanceof com.google.android.gms.internal.zzfew
            if (r3 == 0) goto L_0x0029
            java.lang.Throwable r2 = r2.getCause()
            com.google.android.gms.internal.zzfew r2 = (com.google.android.gms.internal.zzfew) r2
            throw r2
        L_0x0029:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfee.zza(com.google.android.gms.internal.zzfee, com.google.android.gms.internal.zzfdq, com.google.android.gms.internal.zzfea):com.google.android.gms.internal.zzfee");
    }

    protected static <T extends zzfee<T, ?>> T zza(zzfee zzfee, byte[] bArr) throws zzfew {
        T zza = zza(zzfee, bArr, zzfea.zzcuz());
        if (zza != null) {
            if (!(zza.zza(zzfem.zzpcc, Boolean.TRUE, null) != null)) {
                throw new zzfgh(zza).zzcwt().zzh(zza);
            }
        }
        return zza;
    }

    private static <T extends zzfee<T, ?>> T zza(zzfee zzfee, byte[] bArr, zzfea zzfea) throws zzfew {
        T zza;
        try {
            zzfdq zzba = zzfdq.zzba(bArr);
            zza = zza(zzfee, zzba, zzfea);
            zzba.zzkf(0);
            return zza;
        } catch (zzfew e) {
            throw e.zzh(zza);
        } catch (zzfew e2) {
            throw e2;
        }
    }

    static Object zza(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    protected static zzfeu zzcve() {
        return zzfeq.zzcvq();
    }

    protected static <E> zzfev<E> zzcvf() {
        return zzffo.zzcwf();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((zzfee) zza(zzfem.zzpci, (Object) null, (Object) null)).getClass().isInstance(obj)) {
            return false;
        }
        try {
            zzfeh zzfeh = zzfeh.zzpbx;
            zzfee zzfee = (zzfee) obj;
            zza(zzfem.zzpcd, zzfeh, zzfee);
            this.zzpbs = zzfeh.zza(this.zzpbs, zzfee.zzpbs);
            return true;
        } catch (zzfei unused) {
            return false;
        }
    }

    public int hashCode() {
        if (this.zzpaf != 0) {
            return this.zzpaf;
        }
        zzfek zzfek = new zzfek();
        zza(zzfem.zzpcd, zzfek, this);
        this.zzpbs = zzfek.zza(this.zzpbs, this.zzpbs);
        this.zzpaf = zzfek.zzpca;
        return this.zzpaf;
    }

    public final boolean isInitialized() {
        return zza(zzfem.zzpcc, Boolean.TRUE, null) != null;
    }

    public String toString() {
        return zzffl.zza(this, super.toString());
    }

    /* access modifiers changed from: protected */
    public abstract Object zza(int i, Object obj, Object obj2);

    /* access modifiers changed from: protected */
    public final boolean zza(int i, zzfdq zzfdq) throws IOException {
        if ((i & 7) == 4) {
            return false;
        }
        if (this.zzpbs == zzfgi.zzcwu()) {
            this.zzpbs = zzfgi.zzcwv();
        }
        return this.zzpbs.zzb(i, zzfdq);
    }

    public final zzffm<MessageType> zzcvd() {
        return (zzffm) zza(zzfem.zzpcj, (Object) null, (Object) null);
    }

    public final /* synthetic */ zzffj zzcvg() {
        zzfef zzfef = (zzfef) zza(zzfem.zzpch, (Object) null, (Object) null);
        zzfef.zza(this);
        return zzfef;
    }

    public final /* synthetic */ zzffi zzcvh() {
        return (zzfee) zza(zzfem.zzpci, (Object) null, (Object) null);
    }
}
