package com.google.android.gms.internal;

import android.os.Parcelable;

public final class zzbkh implements Parcelable.Creator<zzbkg> {
    /* JADX WARN: Type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r1v5, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r14) {
        /*
            r13 = this;
            int r0 = com.google.android.gms.internal.zzbek.zzd(r14)
            r1 = 0
            r2 = 0
            r8 = r1
            r10 = r8
            r11 = r10
            r4 = r2
            r5 = r4
            r6 = r5
            r7 = r6
            r9 = r7
            r12 = r9
        L_0x000f:
            int r1 = r14.dataPosition()
            if (r1 >= r0) goto L_0x0060
            int r1 = r14.readInt()
            r2 = 65535(0xffff, float:9.1834E-41)
            r2 = r2 & r1
            switch(r2) {
                case 2: goto L_0x0056;
                case 3: goto L_0x004c;
                case 4: goto L_0x0042;
                case 5: goto L_0x003d;
                case 6: goto L_0x0038;
                case 7: goto L_0x0033;
                case 8: goto L_0x002e;
                case 9: goto L_0x0029;
                case 10: goto L_0x0024;
                default: goto L_0x0020;
            }
        L_0x0020:
            com.google.android.gms.internal.zzbek.zzb(r14, r1)
            goto L_0x000f
        L_0x0024:
            java.lang.String r12 = com.google.android.gms.internal.zzbek.zzq(r14, r1)
            goto L_0x000f
        L_0x0029:
            int r11 = com.google.android.gms.internal.zzbek.zzg(r14, r1)
            goto L_0x000f
        L_0x002e:
            int r10 = com.google.android.gms.internal.zzbek.zzg(r14, r1)
            goto L_0x000f
        L_0x0033:
            java.lang.String r9 = com.google.android.gms.internal.zzbek.zzq(r14, r1)
            goto L_0x000f
        L_0x0038:
            boolean r8 = com.google.android.gms.internal.zzbek.zzc(r14, r1)
            goto L_0x000f
        L_0x003d:
            java.lang.Integer r7 = com.google.android.gms.internal.zzbek.zzh(r14, r1)
            goto L_0x000f
        L_0x0042:
            android.os.Parcelable$Creator<com.google.android.gms.drive.zzc> r2 = com.google.android.gms.drive.zzc.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r14, r1, r2)
            r6 = r1
            com.google.android.gms.drive.zzc r6 = (com.google.android.gms.drive.zzc) r6
            goto L_0x000f
        L_0x004c:
            android.os.Parcelable$Creator<com.google.android.gms.drive.metadata.internal.MetadataBundle> r2 = com.google.android.gms.drive.metadata.internal.MetadataBundle.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r14, r1, r2)
            r5 = r1
            com.google.android.gms.drive.metadata.internal.MetadataBundle r5 = (com.google.android.gms.drive.metadata.internal.MetadataBundle) r5
            goto L_0x000f
        L_0x0056:
            android.os.Parcelable$Creator<com.google.android.gms.drive.DriveId> r2 = com.google.android.gms.drive.DriveId.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzbek.zza(r14, r1, r2)
            r4 = r1
            com.google.android.gms.drive.DriveId r4 = (com.google.android.gms.drive.DriveId) r4
            goto L_0x000f
        L_0x0060:
            com.google.android.gms.internal.zzbek.zzaf(r14, r0)
            com.google.android.gms.internal.zzbkg r14 = new com.google.android.gms.internal.zzbkg
            r3 = r14
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzbkh.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbkg[i];
    }
}
