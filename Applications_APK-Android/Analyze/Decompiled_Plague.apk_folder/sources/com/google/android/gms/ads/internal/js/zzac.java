package com.google.android.gms.ads.internal.js;

import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzakd;

final class zzac implements zzakd<zzak> {
    zzac(zzab zzab) {
    }

    public final /* synthetic */ void zzf(Object obj) {
        zzafj.v("Ending javascript session.");
        ((zzal) ((zzak) obj)).zzln();
    }
}
