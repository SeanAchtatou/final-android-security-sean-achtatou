package com.google.android.gms.drive.query;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.drive.DriveSpace;
import com.google.android.gms.drive.query.internal.zzr;
import com.google.android.gms.drive.query.internal.zzt;
import com.google.android.gms.drive.query.internal.zzx;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class Query extends zzbej {
    public static final Parcelable.Creator<Query> CREATOR = new zzb();
    private List<DriveSpace> zzgik;
    private final Set<DriveSpace> zzgil;
    private zzr zzgrs;
    private String zzgrt;
    private SortOrder zzgru;
    final List<String> zzgrv;
    final boolean zzgrw;
    final boolean zzgrx;

    public static class Builder {
        private Set<DriveSpace> zzgil = Collections.emptySet();
        private String zzgrt;
        private SortOrder zzgru;
        private List<String> zzgrv;
        private boolean zzgrw;
        private boolean zzgrx;
        private final List<Filter> zzgry = new ArrayList();

        public Builder() {
        }

        public Builder(Query query) {
            this.zzgry.add(query.getFilter());
            this.zzgrt = query.getPageToken();
            this.zzgru = query.getSortOrder();
            this.zzgrv = query.zzgrv != null ? query.zzgrv : Collections.emptyList();
            this.zzgrw = query.zzgrw;
            this.zzgil = query.zzaph() != null ? query.zzaph() : Collections.emptySet();
            this.zzgrx = query.zzgrx;
        }

        public Builder addFilter(@NonNull Filter filter) {
            zzbq.checkNotNull(filter, "Filter may not be null.");
            if (!(filter instanceof zzt)) {
                this.zzgry.add(filter);
            }
            return this;
        }

        public Query build() {
            return new Query(new zzr(zzx.zzgta, this.zzgry), this.zzgrt, this.zzgru, this.zzgrv, this.zzgrw, this.zzgil, this.zzgrx);
        }

        @Deprecated
        public Builder setPageToken(String str) {
            this.zzgrt = str;
            return this;
        }

        public Builder setSortOrder(SortOrder sortOrder) {
            this.zzgru = sortOrder;
            return this;
        }
    }

    private Query(zzr zzr, String str, SortOrder sortOrder, @NonNull List<String> list, boolean z, @NonNull List<DriveSpace> list2, Set<DriveSpace> set, boolean z2) {
        this.zzgrs = zzr;
        this.zzgrt = str;
        this.zzgru = sortOrder;
        this.zzgrv = list;
        this.zzgrw = z;
        this.zzgik = list2;
        this.zzgil = set;
        this.zzgrx = z2;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    Query(com.google.android.gms.drive.query.internal.zzr r10, java.lang.String r11, com.google.android.gms.drive.query.SortOrder r12, @android.support.annotation.NonNull java.util.List<java.lang.String> r13, boolean r14, @android.support.annotation.NonNull java.util.List<com.google.android.gms.drive.DriveSpace> r15, boolean r16) {
        /*
            r9 = this;
            r6 = r15
            if (r6 != 0) goto L_0x0009
            java.util.Set r0 = java.util.Collections.emptySet()
        L_0x0007:
            r7 = r0
            goto L_0x000f
        L_0x0009:
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>(r6)
            goto L_0x0007
        L_0x000f:
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            r4 = r13
            r5 = r14
            r8 = r16
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.drive.query.Query.<init>(com.google.android.gms.drive.query.internal.zzr, java.lang.String, com.google.android.gms.drive.query.SortOrder, java.util.List, boolean, java.util.List, boolean):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private Query(com.google.android.gms.drive.query.internal.zzr r10, java.lang.String r11, com.google.android.gms.drive.query.SortOrder r12, java.util.List<java.lang.String> r13, boolean r14, java.util.Set<com.google.android.gms.drive.DriveSpace> r15, boolean r16) {
        /*
            r9 = this;
            r7 = r15
            if (r7 != 0) goto L_0x0009
            java.util.List r0 = java.util.Collections.emptyList()
        L_0x0007:
            r6 = r0
            goto L_0x000f
        L_0x0009:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>(r7)
            goto L_0x0007
        L_0x000f:
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            r4 = r13
            r5 = r14
            r8 = r16
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.drive.query.Query.<init>(com.google.android.gms.drive.query.internal.zzr, java.lang.String, com.google.android.gms.drive.query.SortOrder, java.util.List, boolean, java.util.Set, boolean):void");
    }

    public Filter getFilter() {
        return this.zzgrs;
    }

    @Deprecated
    public String getPageToken() {
        return this.zzgrt;
    }

    public SortOrder getSortOrder() {
        return this.zzgru;
    }

    public String toString() {
        return String.format(Locale.US, "Query[%s,%s,PageToken=%s,Spaces=%s]", this.zzgrs, this.zzgru, this.zzgrt, this.zzgik);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.zzr, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.SortOrder, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, (Parcelable) this.zzgrs, i, false);
        zzbem.zza(parcel, 3, this.zzgrt, false);
        zzbem.zza(parcel, 4, (Parcelable) this.zzgru, i, false);
        zzbem.zzb(parcel, 5, this.zzgrv, false);
        zzbem.zza(parcel, 6, this.zzgrw);
        zzbem.zzc(parcel, 7, this.zzgik, false);
        zzbem.zza(parcel, 8, this.zzgrx);
        zzbem.zzai(parcel, zze);
    }

    public final Set<DriveSpace> zzaph() {
        return this.zzgil;
    }
}
