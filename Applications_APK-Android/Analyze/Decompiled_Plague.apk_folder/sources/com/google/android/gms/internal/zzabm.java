package com.google.android.gms.internal;

import android.location.Location;
import android.support.annotation.Nullable;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;

@zzzb
public final class zzabm {
    private static final SimpleDateFormat zzcqa = new SimpleDateFormat("yyyyMMdd", Locale.US);

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00dc A[Catch:{ JSONException -> 0x0241 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e2 A[Catch:{ JSONException -> 0x0241 }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0145 A[Catch:{ JSONException -> 0x0241 }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x014e A[Catch:{ JSONException -> 0x0241 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.internal.zzaad zza(android.content.Context r52, com.google.android.gms.internal.zzzz r53, java.lang.String r54) {
        /*
            r9 = r53
            r15 = 0
            org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0241 }
            r1 = r54
            r10.<init>(r1)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "ad_base_url"
            r11 = 0
            java.lang.String r1 = r10.optString(r1, r11)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r2 = "ad_url"
            java.lang.String r4 = r10.optString(r2, r11)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r2 = "ad_size"
            java.lang.String r13 = r10.optString(r2, r11)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r2 = "ad_slot_size"
            java.lang.String r40 = r10.optString(r2, r13)     // Catch:{ JSONException -> 0x0241 }
            r12 = 1
            if (r9 == 0) goto L_0x002d
            int r2 = r9.zzclu     // Catch:{ JSONException -> 0x0241 }
            if (r2 == 0) goto L_0x002d
            r24 = r12
            goto L_0x002f
        L_0x002d:
            r24 = r15
        L_0x002f:
            java.lang.String r2 = "ad_json"
            java.lang.String r2 = r10.optString(r2, r11)     // Catch:{ JSONException -> 0x0241 }
            if (r2 != 0) goto L_0x003d
            java.lang.String r2 = "ad_html"
            java.lang.String r2 = r10.optString(r2, r11)     // Catch:{ JSONException -> 0x0241 }
        L_0x003d:
            if (r2 != 0) goto L_0x0045
            java.lang.String r2 = "body"
            java.lang.String r2 = r10.optString(r2, r11)     // Catch:{ JSONException -> 0x0241 }
        L_0x0045:
            if (r2 != 0) goto L_0x0053
            java.lang.String r3 = "ads"
            boolean r3 = r10.has(r3)     // Catch:{ JSONException -> 0x0241 }
            if (r3 == 0) goto L_0x0053
            java.lang.String r2 = r10.toString()     // Catch:{ JSONException -> 0x0241 }
        L_0x0053:
            java.lang.String r3 = "debug_dialog"
            java.lang.String r19 = r10.optString(r3, r11)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r3 = "debug_signals"
            java.lang.String r42 = r10.optString(r3, r11)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r3 = "interstitial_timeout"
            boolean r3 = r10.has(r3)     // Catch:{ JSONException -> 0x0241 }
            r7 = -1
            if (r3 == 0) goto L_0x007a
            java.lang.String r3 = "interstitial_timeout"
            double r5 = r10.getDouble(r3)     // Catch:{ JSONException -> 0x0241 }
            r16 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r5 = r5 * r16
            long r5 = (long) r5     // Catch:{ JSONException -> 0x0241 }
            r16 = r5
            goto L_0x007c
        L_0x007a:
            r16 = r7
        L_0x007c:
            java.lang.String r3 = "orientation"
            java.lang.String r3 = r10.optString(r3, r11)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r5 = "portrait"
            boolean r5 = r5.equals(r3)     // Catch:{ JSONException -> 0x0241 }
            r14 = -1
            if (r5 == 0) goto L_0x0096
            com.google.android.gms.internal.zzagw r3 = com.google.android.gms.ads.internal.zzbs.zzee()     // Catch:{ JSONException -> 0x0241 }
            int r3 = r3.zzqa()     // Catch:{ JSONException -> 0x0241 }
        L_0x0093:
            r18 = r3
            goto L_0x00a9
        L_0x0096:
            java.lang.String r5 = "landscape"
            boolean r3 = r5.equals(r3)     // Catch:{ JSONException -> 0x0241 }
            if (r3 == 0) goto L_0x00a7
            com.google.android.gms.internal.zzagw r3 = com.google.android.gms.ads.internal.zzbs.zzee()     // Catch:{ JSONException -> 0x0241 }
            int r3 = r3.zzpz()     // Catch:{ JSONException -> 0x0241 }
            goto L_0x0093
        L_0x00a7:
            r18 = r14
        L_0x00a9:
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ JSONException -> 0x0241 }
            if (r3 == 0) goto L_0x00d5
            boolean r3 = android.text.TextUtils.isEmpty(r4)     // Catch:{ JSONException -> 0x0241 }
            if (r3 != 0) goto L_0x00d5
            com.google.android.gms.internal.zzaiy r1 = r9.zzatd     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r3 = r1.zzcp     // Catch:{ JSONException -> 0x0241 }
            r5 = 0
            r6 = 0
            r20 = 0
            r21 = 0
            r1 = r9
            r2 = r52
            r7 = r20
            r8 = r21
            com.google.android.gms.internal.zzaad r1 = com.google.android.gms.internal.zzabh.zza(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r2 = r1.zzchl     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r3 = r1.body     // Catch:{ JSONException -> 0x0241 }
            long r4 = r1.zzcnk     // Catch:{ JSONException -> 0x0241 }
            r20 = r4
            r4 = r3
            r3 = r2
            goto L_0x00da
        L_0x00d5:
            r3 = r1
            r4 = r2
            r1 = r11
            r20 = -1
        L_0x00da:
            if (r4 != 0) goto L_0x00e2
            com.google.android.gms.internal.zzaad r1 = new com.google.android.gms.internal.zzaad     // Catch:{ JSONException -> 0x0241 }
            r1.<init>(r15)     // Catch:{ JSONException -> 0x0241 }
            return r1
        L_0x00e2:
            java.lang.String r2 = "click_urls"
            org.json.JSONArray r2 = r10.optJSONArray(r2)     // Catch:{ JSONException -> 0x0241 }
            if (r1 != 0) goto L_0x00ec
            r5 = r11
            goto L_0x00ee
        L_0x00ec:
            java.util.List<java.lang.String> r5 = r1.zzcbv     // Catch:{ JSONException -> 0x0241 }
        L_0x00ee:
            if (r2 == 0) goto L_0x00f5
            java.util.List r2 = zza(r2, r5)     // Catch:{ JSONException -> 0x0241 }
            r5 = r2
        L_0x00f5:
            java.lang.String r2 = "impression_urls"
            org.json.JSONArray r2 = r10.optJSONArray(r2)     // Catch:{ JSONException -> 0x0241 }
            if (r1 != 0) goto L_0x00ff
            r6 = r11
            goto L_0x0101
        L_0x00ff:
            java.util.List<java.lang.String> r6 = r1.zzcbw     // Catch:{ JSONException -> 0x0241 }
        L_0x0101:
            if (r2 == 0) goto L_0x0108
            java.util.List r2 = zza(r2, r6)     // Catch:{ JSONException -> 0x0241 }
            r6 = r2
        L_0x0108:
            java.lang.String r2 = "manual_impression_urls"
            org.json.JSONArray r2 = r10.optJSONArray(r2)     // Catch:{ JSONException -> 0x0241 }
            if (r1 != 0) goto L_0x0112
            r7 = r11
            goto L_0x0114
        L_0x0112:
            java.util.List<java.lang.String> r7 = r1.zzcni     // Catch:{ JSONException -> 0x0241 }
        L_0x0114:
            if (r2 == 0) goto L_0x011d
            java.util.List r2 = zza(r2, r7)     // Catch:{ JSONException -> 0x0241 }
            r22 = r2
            goto L_0x011f
        L_0x011d:
            r22 = r7
        L_0x011f:
            if (r1 == 0) goto L_0x0135
            int r2 = r1.orientation     // Catch:{ JSONException -> 0x0241 }
            if (r2 == r14) goto L_0x0129
            int r2 = r1.orientation     // Catch:{ JSONException -> 0x0241 }
            r18 = r2
        L_0x0129:
            long r7 = r1.zzcnf     // Catch:{ JSONException -> 0x0241 }
            r25 = 0
            int r2 = (r7 > r25 ? 1 : (r7 == r25 ? 0 : -1))
            if (r2 <= 0) goto L_0x0135
            long r1 = r1.zzcnf     // Catch:{ JSONException -> 0x0241 }
            r7 = r1
            goto L_0x0137
        L_0x0135:
            r7 = r16
        L_0x0137:
            java.lang.String r1 = "active_view"
            java.lang.String r23 = r10.optString(r1)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "ad_is_javascript"
            boolean r25 = r10.optBoolean(r1, r15)     // Catch:{ JSONException -> 0x0241 }
            if (r25 == 0) goto L_0x014e
            java.lang.String r1 = "ad_passback_url"
            java.lang.String r1 = r10.optString(r1, r11)     // Catch:{ JSONException -> 0x0241 }
            r26 = r1
            goto L_0x0150
        L_0x014e:
            r26 = r11
        L_0x0150:
            java.lang.String r1 = "mediation"
            boolean r14 = r10.optBoolean(r1, r15)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "custom_render_allowed"
            boolean r27 = r10.optBoolean(r1, r15)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "content_url_opted_out"
            boolean r28 = r10.optBoolean(r1, r12)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "content_vertical_opted_out"
            boolean r43 = r10.optBoolean(r1, r12)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "prefetch"
            boolean r29 = r10.optBoolean(r1, r15)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "refresh_interval_milliseconds"
            r11 = -1
            long r16 = r10.optLong(r1, r11)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "mediation_config_cache_time_milliseconds"
            long r11 = r10.optLong(r1, r11)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "gws_query_id"
            java.lang.String r2 = ""
            java.lang.String r30 = r10.optString(r1, r2)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "height"
            java.lang.String r2 = "fluid"
            java.lang.String r15 = ""
            java.lang.String r2 = r10.optString(r2, r15)     // Catch:{ JSONException -> 0x0241 }
            boolean r31 = r1.equals(r2)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "native_express"
            r2 = 0
            boolean r32 = r10.optBoolean(r1, r2)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "video_start_urls"
            org.json.JSONArray r1 = r10.optJSONArray(r1)     // Catch:{ JSONException -> 0x0241 }
            r2 = 0
            java.util.List r33 = zza(r1, r2)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "video_complete_urls"
            org.json.JSONArray r1 = r10.optJSONArray(r1)     // Catch:{ JSONException -> 0x0241 }
            java.util.List r34 = zza(r1, r2)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "rewards"
            org.json.JSONArray r1 = r10.optJSONArray(r1)     // Catch:{ JSONException -> 0x0241 }
            com.google.android.gms.internal.zzadw r35 = com.google.android.gms.internal.zzadw.zza(r1)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "use_displayed_impression"
            r15 = 0
            boolean r36 = r10.optBoolean(r1, r15)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "auto_protection_configuration"
            org.json.JSONObject r1 = r10.optJSONObject(r1)     // Catch:{ JSONException -> 0x0241 }
            com.google.android.gms.internal.zzaaf r37 = com.google.android.gms.internal.zzaaf.zzl(r1)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "set_cookie"
            java.lang.String r2 = ""
            java.lang.String r38 = r10.optString(r1, r2)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "remote_ping_urls"
            org.json.JSONArray r1 = r10.optJSONArray(r1)     // Catch:{ JSONException -> 0x0241 }
            r2 = 0
            java.util.List r39 = zza(r1, r2)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "safe_browsing"
            org.json.JSONObject r1 = r10.optJSONObject(r1)     // Catch:{ JSONException -> 0x0241 }
            com.google.android.gms.internal.zzaee r41 = com.google.android.gms.internal.zzaee.zzo(r1)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "render_in_browser"
            boolean r2 = r9.zzcbz     // Catch:{ JSONException -> 0x0241 }
            boolean r44 = r10.optBoolean(r1, r2)     // Catch:{ JSONException -> 0x0241 }
            java.lang.String r1 = "custom_close_blocked"
            boolean r45 = r10.optBoolean(r1)     // Catch:{ JSONException -> 0x0241 }
            com.google.android.gms.internal.zzaad r47 = new com.google.android.gms.internal.zzaad     // Catch:{ JSONException -> 0x0241 }
            boolean r10 = r9.zzclw     // Catch:{ JSONException -> 0x0241 }
            boolean r2 = r9.zzcmk     // Catch:{ JSONException -> 0x0241 }
            boolean r1 = r9.zzcmw     // Catch:{ JSONException -> 0x0241 }
            r46 = 0
            r48 = r1
            r1 = r47
            r49 = r2
            r2 = r9
            r9 = r14
            r50 = r10
            r10 = r11
            r12 = r22
            r22 = r13
            r13 = r16
            r15 = r18
            r16 = r22
            r17 = r20
            r20 = r25
            r21 = r26
            r22 = r23
            r23 = r27
            r25 = r50
            r26 = r28
            r27 = r29
            r28 = r30
            r29 = r31
            r30 = r32
            r31 = r35
            r32 = r33
            r33 = r34
            r34 = r36
            r35 = r37
            r36 = r49
            r37 = r38
            r38 = r39
            r39 = r44
            r44 = r48
            r1.<init>(r2, r3, r4, r5, r6, r7, r9, r10, r12, r13, r15, r16, r17, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41, r42, r43, r44, r45, r46)     // Catch:{ JSONException -> 0x0241 }
            return r47
        L_0x0241:
            r0 = move-exception
            r1 = r0
            java.lang.String r2 = "Could not parse the inline ad response: "
            java.lang.String r1 = r1.getMessage()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r3 = r1.length()
            if (r3 == 0) goto L_0x0258
            java.lang.String r1 = r2.concat(r1)
            goto L_0x025d
        L_0x0258:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r2)
        L_0x025d:
            com.google.android.gms.internal.zzafj.zzco(r1)
            com.google.android.gms.internal.zzaad r1 = new com.google.android.gms.internal.zzaad
            r2 = 0
            r1.<init>(r2)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzabm.zza(android.content.Context, com.google.android.gms.internal.zzzz, java.lang.String):com.google.android.gms.internal.zzaad");
    }

    @Nullable
    private static List<String> zza(@Nullable JSONArray jSONArray, @Nullable List<String> list) throws JSONException {
        if (jSONArray == null) {
            return null;
        }
        if (list == null) {
            list = new LinkedList<>();
        }
        for (int i = 0; i < jSONArray.length(); i++) {
            list.add(jSONArray.getString(i));
        }
        return list;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v27, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v28, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v15, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v16, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v30, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v32, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v34, resolved type: boolean} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x024e A[Catch:{ JSONException -> 0x08d6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x0259 A[Catch:{ JSONException -> 0x08d6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x026c  */
    /* JADX WARNING: Removed duplicated region for block: B:301:0x07f3 A[Catch:{ JSONException -> 0x08d6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:302:0x07f6 A[Catch:{ JSONException -> 0x08d6 }] */
    @android.support.annotation.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.json.JSONObject zza(android.content.Context r25, com.google.android.gms.internal.zzabf r26) {
        /*
            r1 = r26
            com.google.android.gms.internal.zzzz r2 = r1.zzcpe
            android.location.Location r3 = r1.zzbcd
            com.google.android.gms.internal.zzabu r4 = r1.zzcpf
            android.os.Bundle r5 = r1.zzclv
            org.json.JSONObject r6 = r1.zzcpg
            java.util.HashMap r8 = new java.util.HashMap     // Catch:{ JSONException -> 0x08d6 }
            r8.<init>()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r9 = "extra_caps"
            com.google.android.gms.internal.zzmg<java.lang.String> r10 = com.google.android.gms.internal.zzmq.zzbmo     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zzmo r11 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Object r10 = r11.zzd(r10)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r9, r10)     // Catch:{ JSONException -> 0x08d6 }
            java.util.List<java.lang.String> r9 = r1.zzcmc     // Catch:{ JSONException -> 0x08d6 }
            int r9 = r9.size()     // Catch:{ JSONException -> 0x08d6 }
            if (r9 <= 0) goto L_0x0035
            java.lang.String r9 = "eid"
            java.lang.String r10 = ","
            java.util.List<java.lang.String> r11 = r1.zzcmc     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r10 = android.text.TextUtils.join(r10, r11)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r9, r10)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0035:
            android.os.Bundle r9 = r2.zzcln     // Catch:{ JSONException -> 0x08d6 }
            if (r9 == 0) goto L_0x0040
            java.lang.String r9 = "ad_pos"
            android.os.Bundle r10 = r2.zzcln     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r9, r10)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0040:
            com.google.android.gms.internal.zzis r9 = r2.zzclo     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r10 = com.google.android.gms.internal.zzafg.zzpr()     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == 0) goto L_0x004d
            java.lang.String r11 = "abf"
            r8.put(r11, r10)     // Catch:{ JSONException -> 0x08d6 }
        L_0x004d:
            long r10 = r9.zzbbv     // Catch:{ JSONException -> 0x08d6 }
            r12 = -1
            int r14 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r14 == 0) goto L_0x0067
            java.lang.String r10 = "cust_age"
            java.text.SimpleDateFormat r11 = com.google.android.gms.internal.zzabm.zzcqa     // Catch:{ JSONException -> 0x08d6 }
            java.util.Date r14 = new java.util.Date     // Catch:{ JSONException -> 0x08d6 }
            long r12 = r9.zzbbv     // Catch:{ JSONException -> 0x08d6 }
            r14.<init>(r12)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r11 = r11.format(r14)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r10, r11)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0067:
            android.os.Bundle r10 = r9.extras     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == 0) goto L_0x0072
            java.lang.String r10 = "extras"
            android.os.Bundle r11 = r9.extras     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r10, r11)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0072:
            int r10 = r9.zzbbw     // Catch:{ JSONException -> 0x08d6 }
            r11 = -1
            if (r10 == r11) goto L_0x0082
            java.lang.String r10 = "cust_gender"
            int r12 = r9.zzbbw     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r10, r12)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0082:
            java.util.List<java.lang.String> r10 = r9.zzbbx     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == 0) goto L_0x008d
            java.lang.String r10 = "kw"
            java.util.List<java.lang.String> r12 = r9.zzbbx     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r10, r12)     // Catch:{ JSONException -> 0x08d6 }
        L_0x008d:
            int r10 = r9.zzbbz     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == r11) goto L_0x009c
            java.lang.String r10 = "tag_for_child_directed_treatment"
            int r12 = r9.zzbbz     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r10, r12)     // Catch:{ JSONException -> 0x08d6 }
        L_0x009c:
            boolean r10 = r9.zzbby     // Catch:{ JSONException -> 0x08d6 }
            r12 = 1
            if (r10 == 0) goto L_0x00c2
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r10 = com.google.android.gms.internal.zzmq.zzbqa     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zzmo r13 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Object r10 = r13.zzd(r10)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Boolean r10 = (java.lang.Boolean) r10     // Catch:{ JSONException -> 0x08d6 }
            boolean r10 = r10.booleanValue()     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == 0) goto L_0x00bd
            java.lang.String r10 = "test_request"
            java.lang.Boolean r13 = java.lang.Boolean.valueOf(r12)     // Catch:{ JSONException -> 0x08d6 }
        L_0x00b9:
            r8.put(r10, r13)     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x00c2
        L_0x00bd:
            java.lang.String r10 = "adtest"
            java.lang.String r13 = "on"
            goto L_0x00b9
        L_0x00c2:
            int r10 = r9.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r13 = 2
            if (r10 < r13) goto L_0x00e3
            boolean r10 = r9.zzbca     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == 0) goto L_0x00d4
            java.lang.String r10 = "d_imp_hdr"
            java.lang.Integer r14 = java.lang.Integer.valueOf(r12)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r10, r14)     // Catch:{ JSONException -> 0x08d6 }
        L_0x00d4:
            java.lang.String r10 = r9.zzbcb     // Catch:{ JSONException -> 0x08d6 }
            boolean r10 = android.text.TextUtils.isEmpty(r10)     // Catch:{ JSONException -> 0x08d6 }
            if (r10 != 0) goto L_0x00e3
            java.lang.String r10 = "ppid"
            java.lang.String r14 = r9.zzbcb     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r10, r14)     // Catch:{ JSONException -> 0x08d6 }
        L_0x00e3:
            int r10 = r9.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r14 = 3
            if (r10 < r14) goto L_0x00f3
            java.lang.String r10 = r9.zzbce     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == 0) goto L_0x00f3
            java.lang.String r10 = "url"
            java.lang.String r14 = r9.zzbce     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r10, r14)     // Catch:{ JSONException -> 0x08d6 }
        L_0x00f3:
            int r10 = r9.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r14 = 5
            if (r10 < r14) goto L_0x0119
            android.os.Bundle r10 = r9.zzbcg     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == 0) goto L_0x0103
            java.lang.String r10 = "custom_targeting"
            android.os.Bundle r15 = r9.zzbcg     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r10, r15)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0103:
            java.util.List<java.lang.String> r10 = r9.zzbch     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == 0) goto L_0x010e
            java.lang.String r10 = "category_exclusions"
            java.util.List<java.lang.String> r15 = r9.zzbch     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r10, r15)     // Catch:{ JSONException -> 0x08d6 }
        L_0x010e:
            java.lang.String r10 = r9.zzbci     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == 0) goto L_0x0119
            java.lang.String r10 = "request_agent"
            java.lang.String r15 = r9.zzbci     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r10, r15)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0119:
            int r10 = r9.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r15 = 6
            if (r10 < r15) goto L_0x0129
            java.lang.String r10 = r9.zzbcj     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == 0) goto L_0x0129
            java.lang.String r10 = "request_pkg"
            java.lang.String r7 = r9.zzbcj     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r10, r7)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0129:
            int r7 = r9.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r10 = 7
            if (r7 < r10) goto L_0x0139
            java.lang.String r7 = "is_designed_for_families"
            boolean r9 = r9.zzbck     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r9)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0139:
            com.google.android.gms.internal.zziw r7 = r2.zzath     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zziw[] r7 = r7.zzbdc     // Catch:{ JSONException -> 0x08d6 }
            if (r7 != 0) goto L_0x0156
            java.lang.String r7 = "format"
            com.google.android.gms.internal.zziw r10 = r2.zzath     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r10 = r10.zzbda     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zziw r7 = r2.zzath     // Catch:{ JSONException -> 0x08d6 }
            boolean r7 = r7.zzbde     // Catch:{ JSONException -> 0x08d6 }
            if (r7 == 0) goto L_0x018c
            java.lang.String r7 = "fluid"
            java.lang.String r10 = "height"
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x018c
        L_0x0156:
            com.google.android.gms.internal.zziw r7 = r2.zzath     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zziw[] r7 = r7.zzbdc     // Catch:{ JSONException -> 0x08d6 }
            int r10 = r7.length     // Catch:{ JSONException -> 0x08d6 }
            r15 = 0
            r16 = 0
            r17 = 0
        L_0x0160:
            if (r15 >= r10) goto L_0x018c
            r14 = r7[r15]     // Catch:{ JSONException -> 0x08d6 }
            boolean r12 = r14.zzbde     // Catch:{ JSONException -> 0x08d6 }
            if (r12 != 0) goto L_0x0173
            if (r16 != 0) goto L_0x0173
            java.lang.String r12 = "format"
            java.lang.String r13 = r14.zzbda     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r13)     // Catch:{ JSONException -> 0x08d6 }
            r16 = 1
        L_0x0173:
            boolean r12 = r14.zzbde     // Catch:{ JSONException -> 0x08d6 }
            if (r12 == 0) goto L_0x0182
            if (r17 != 0) goto L_0x0182
            java.lang.String r12 = "fluid"
            java.lang.String r13 = "height"
            r8.put(r12, r13)     // Catch:{ JSONException -> 0x08d6 }
            r17 = 1
        L_0x0182:
            if (r16 == 0) goto L_0x0186
            if (r17 != 0) goto L_0x018c
        L_0x0186:
            int r15 = r15 + 1
            r12 = 1
            r13 = 2
            r14 = 5
            goto L_0x0160
        L_0x018c:
            com.google.android.gms.internal.zziw r7 = r2.zzath     // Catch:{ JSONException -> 0x08d6 }
            int r7 = r7.width     // Catch:{ JSONException -> 0x08d6 }
            if (r7 != r11) goto L_0x0199
            java.lang.String r7 = "smart_w"
            java.lang.String r10 = "full"
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0199:
            com.google.android.gms.internal.zziw r7 = r2.zzath     // Catch:{ JSONException -> 0x08d6 }
            int r7 = r7.height     // Catch:{ JSONException -> 0x08d6 }
            r10 = -2
            if (r7 != r10) goto L_0x01a7
            java.lang.String r7 = "smart_h"
            java.lang.String r12 = "auto"
            r8.put(r7, r12)     // Catch:{ JSONException -> 0x08d6 }
        L_0x01a7:
            com.google.android.gms.internal.zziw r7 = r2.zzath     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zziw[] r7 = r7.zzbdc     // Catch:{ JSONException -> 0x08d6 }
            if (r7 == 0) goto L_0x0215
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x08d6 }
            r7.<init>()     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zziw r12 = r2.zzath     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zziw[] r12 = r12.zzbdc     // Catch:{ JSONException -> 0x08d6 }
            int r13 = r12.length     // Catch:{ JSONException -> 0x08d6 }
            r14 = 0
            r15 = 0
        L_0x01b9:
            if (r14 >= r13) goto L_0x01fb
            r9 = r12[r14]     // Catch:{ JSONException -> 0x08d6 }
            boolean r10 = r9.zzbde     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == 0) goto L_0x01c3
            r15 = 1
            goto L_0x01f6
        L_0x01c3:
            int r10 = r7.length()     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == 0) goto L_0x01ce
            java.lang.String r10 = "|"
            r7.append(r10)     // Catch:{ JSONException -> 0x08d6 }
        L_0x01ce:
            int r10 = r9.width     // Catch:{ JSONException -> 0x08d6 }
            if (r10 != r11) goto L_0x01da
            int r10 = r9.widthPixels     // Catch:{ JSONException -> 0x08d6 }
            float r10 = (float) r10     // Catch:{ JSONException -> 0x08d6 }
            float r11 = r4.zzaxd     // Catch:{ JSONException -> 0x08d6 }
            float r10 = r10 / r11
            int r10 = (int) r10     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x01dc
        L_0x01da:
            int r10 = r9.width     // Catch:{ JSONException -> 0x08d6 }
        L_0x01dc:
            r7.append(r10)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r10 = "x"
            r7.append(r10)     // Catch:{ JSONException -> 0x08d6 }
            int r10 = r9.height     // Catch:{ JSONException -> 0x08d6 }
            r11 = -2
            if (r10 != r11) goto L_0x01f1
            int r9 = r9.heightPixels     // Catch:{ JSONException -> 0x08d6 }
            float r9 = (float) r9     // Catch:{ JSONException -> 0x08d6 }
            float r10 = r4.zzaxd     // Catch:{ JSONException -> 0x08d6 }
            float r9 = r9 / r10
            int r9 = (int) r9     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x01f3
        L_0x01f1:
            int r9 = r9.height     // Catch:{ JSONException -> 0x08d6 }
        L_0x01f3:
            r7.append(r9)     // Catch:{ JSONException -> 0x08d6 }
        L_0x01f6:
            int r14 = r14 + 1
            r10 = -2
            r11 = -1
            goto L_0x01b9
        L_0x01fb:
            if (r15 == 0) goto L_0x0210
            int r9 = r7.length()     // Catch:{ JSONException -> 0x08d6 }
            if (r9 == 0) goto L_0x020a
            java.lang.String r9 = "|"
            r10 = 0
            r7.insert(r10, r9)     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x020b
        L_0x020a:
            r10 = 0
        L_0x020b:
            java.lang.String r9 = "320x50"
            r7.insert(r10, r9)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0210:
            java.lang.String r9 = "sz"
            r8.put(r9, r7)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0215:
            int r7 = r2.zzclu     // Catch:{ JSONException -> 0x08d6 }
            r9 = 24
            if (r7 == 0) goto L_0x0280
            java.lang.String r7 = "native_version"
            int r10 = r2.zzclu     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = "native_templates"
            java.util.List<java.lang.String> r10 = r2.zzaub     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = "native_image_orientation"
            com.google.android.gms.internal.zzom r10 = r2.zzatt     // Catch:{ JSONException -> 0x08d6 }
            if (r10 != 0) goto L_0x0236
        L_0x0233:
            java.lang.String r10 = "any"
            goto L_0x0243
        L_0x0236:
            int r10 = r10.zzbtk     // Catch:{ JSONException -> 0x08d6 }
            switch(r10) {
                case 0: goto L_0x0233;
                case 1: goto L_0x0241;
                case 2: goto L_0x023e;
                default: goto L_0x023b;
            }     // Catch:{ JSONException -> 0x08d6 }
        L_0x023b:
            java.lang.String r10 = "not_set"
            goto L_0x0243
        L_0x023e:
            java.lang.String r10 = "landscape"
            goto L_0x0243
        L_0x0241:
            java.lang.String r10 = "portrait"
        L_0x0243:
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
            java.util.List<java.lang.String> r7 = r2.zzcmd     // Catch:{ JSONException -> 0x08d6 }
            boolean r7 = r7.isEmpty()     // Catch:{ JSONException -> 0x08d6 }
            if (r7 != 0) goto L_0x0255
            java.lang.String r7 = "native_custom_templates"
            java.util.List<java.lang.String> r10 = r2.zzcmd     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0255:
            int r7 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            if (r7 < r9) goto L_0x0264
            java.lang.String r7 = "max_num_ads"
            int r10 = r2.zzcmz     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0264:
            java.lang.String r7 = r2.zzcmx     // Catch:{ JSONException -> 0x08d6 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ JSONException -> 0x08d6 }
            if (r7 != 0) goto L_0x0280
            java.lang.String r7 = "native_advanced_settings"
            org.json.JSONArray r10 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0279 }
            java.lang.String r11 = r2.zzcmx     // Catch:{ JSONException -> 0x0279 }
            r10.<init>(r11)     // Catch:{ JSONException -> 0x0279 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x0279 }
            goto L_0x0280
        L_0x0279:
            r0 = move-exception
            r7 = r0
            java.lang.String r10 = "Problem creating json from native advanced settings"
            com.google.android.gms.internal.zzafj.zzc(r10, r7)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0280:
            java.util.List<java.lang.Integer> r7 = r2.zzatx     // Catch:{ JSONException -> 0x08d6 }
            if (r7 == 0) goto L_0x02be
            java.util.List<java.lang.Integer> r7 = r2.zzatx     // Catch:{ JSONException -> 0x08d6 }
            int r7 = r7.size()     // Catch:{ JSONException -> 0x08d6 }
            if (r7 <= 0) goto L_0x02be
            java.util.List<java.lang.Integer> r7 = r2.zzatx     // Catch:{ JSONException -> 0x08d6 }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ JSONException -> 0x08d6 }
        L_0x0292:
            boolean r10 = r7.hasNext()     // Catch:{ JSONException -> 0x08d6 }
            if (r10 == 0) goto L_0x02be
            java.lang.Object r10 = r7.next()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r10 = (java.lang.Integer) r10     // Catch:{ JSONException -> 0x08d6 }
            int r11 = r10.intValue()     // Catch:{ JSONException -> 0x08d6 }
            r12 = 2
            if (r11 != r12) goto L_0x02b0
            java.lang.String r10 = "iba"
            r11 = 1
            java.lang.Boolean r12 = java.lang.Boolean.valueOf(r11)     // Catch:{ JSONException -> 0x08d6 }
        L_0x02ac:
            r8.put(r10, r12)     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x0292
        L_0x02b0:
            int r10 = r10.intValue()     // Catch:{ JSONException -> 0x08d6 }
            r11 = 1
            if (r10 != r11) goto L_0x0292
            java.lang.String r10 = "ina"
            java.lang.Boolean r12 = java.lang.Boolean.valueOf(r11)     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x02ac
        L_0x02be:
            com.google.android.gms.internal.zziw r7 = r2.zzath     // Catch:{ JSONException -> 0x08d6 }
            boolean r7 = r7.zzbdf     // Catch:{ JSONException -> 0x08d6 }
            if (r7 == 0) goto L_0x02ce
            java.lang.String r7 = "ene"
            r10 = 1
            java.lang.Boolean r11 = java.lang.Boolean.valueOf(r10)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r11)     // Catch:{ JSONException -> 0x08d6 }
        L_0x02ce:
            com.google.android.gms.internal.zzla r7 = r2.zzatv     // Catch:{ JSONException -> 0x08d6 }
            if (r7 == 0) goto L_0x02e9
            java.lang.String r7 = "is_icon_ad"
            r10 = 1
            java.lang.Boolean r11 = java.lang.Boolean.valueOf(r10)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r11)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = "icon_ad_expansion_behavior"
            com.google.android.gms.internal.zzla r10 = r2.zzatv     // Catch:{ JSONException -> 0x08d6 }
            int r10 = r10.zzbee     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
        L_0x02e9:
            java.lang.String r7 = "slotname"
            java.lang.String r10 = r2.zzatb     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = "pn"
            android.content.pm.ApplicationInfo r10 = r2.applicationInfo     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r10 = r10.packageName     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
            android.content.pm.PackageInfo r7 = r2.zzclp     // Catch:{ JSONException -> 0x08d6 }
            if (r7 == 0) goto L_0x030a
            java.lang.String r7 = "vc"
            android.content.pm.PackageInfo r10 = r2.zzclp     // Catch:{ JSONException -> 0x08d6 }
            int r10 = r10.versionCode     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
        L_0x030a:
            java.lang.String r7 = "ms"
            java.lang.String r10 = r1.zzclq     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = "seq_num"
            java.lang.String r10 = r2.zzclr     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = "session_id"
            java.lang.String r10 = r2.zzcls     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = "js"
            com.google.android.gms.internal.zzaiy r10 = r2.zzatd     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r10 = r10.zzcp     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zzace r7 = r1.zzcpb     // Catch:{ JSONException -> 0x08d6 }
            android.os.Bundle r10 = r2.zzcmp     // Catch:{ JSONException -> 0x08d6 }
            android.os.Bundle r11 = r1.zzcpa     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r12 = "am"
            int r13 = r4.zzcrt     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r13)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r12 = "cog"
            boolean r13 = r4.zzcru     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r13 = zzt(r13)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r13)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r12 = "coh"
            boolean r13 = r4.zzcrv     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r13 = zzt(r13)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r13)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r12 = r4.zzcrw     // Catch:{ JSONException -> 0x08d6 }
            boolean r12 = android.text.TextUtils.isEmpty(r12)     // Catch:{ JSONException -> 0x08d6 }
            if (r12 != 0) goto L_0x035e
            java.lang.String r12 = "carrier"
            java.lang.String r13 = r4.zzcrw     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r13)     // Catch:{ JSONException -> 0x08d6 }
        L_0x035e:
            java.lang.String r12 = "gl"
            java.lang.String r13 = r4.zzcrx     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r13)     // Catch:{ JSONException -> 0x08d6 }
            boolean r12 = r4.zzcry     // Catch:{ JSONException -> 0x08d6 }
            if (r12 == 0) goto L_0x0373
            java.lang.String r12 = "simulator"
            r13 = 1
            java.lang.Integer r14 = java.lang.Integer.valueOf(r13)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r14)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0373:
            boolean r12 = r4.zzcrz     // Catch:{ JSONException -> 0x08d6 }
            if (r12 == 0) goto L_0x0382
            java.lang.String r12 = "is_sidewinder"
            r13 = 1
            java.lang.Integer r14 = java.lang.Integer.valueOf(r13)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r14)     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x0383
        L_0x0382:
            r13 = 1
        L_0x0383:
            java.lang.String r12 = "ma"
            boolean r14 = r4.zzcsa     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r14 = zzt(r14)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r14)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r12 = "sp"
            boolean r14 = r4.zzcsb     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r14 = zzt(r14)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r14)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r12 = "hl"
            java.lang.String r14 = r4.zzcsc     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r14)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r12 = r4.zzcsd     // Catch:{ JSONException -> 0x08d6 }
            boolean r12 = android.text.TextUtils.isEmpty(r12)     // Catch:{ JSONException -> 0x08d6 }
            if (r12 != 0) goto L_0x03af
            java.lang.String r12 = "mv"
            java.lang.String r14 = r4.zzcsd     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r14)     // Catch:{ JSONException -> 0x08d6 }
        L_0x03af:
            java.lang.String r12 = "muv"
            int r14 = r4.zzcsf     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r14)     // Catch:{ JSONException -> 0x08d6 }
            int r12 = r4.zzcsg     // Catch:{ JSONException -> 0x08d6 }
            r14 = -2
            if (r12 == r14) goto L_0x03ca
            java.lang.String r12 = "cnt"
            int r14 = r4.zzcsg     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r14)     // Catch:{ JSONException -> 0x08d6 }
        L_0x03ca:
            java.lang.String r12 = "gnt"
            int r14 = r4.zzcsh     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r14)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r12 = "pt"
            int r14 = r4.zzcsi     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r14)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r12 = "rm"
            int r14 = r4.zzcsj     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r14)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r12 = "riv"
            int r14 = r4.zzcsk     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r12, r14)     // Catch:{ JSONException -> 0x08d6 }
            android.os.Bundle r12 = new android.os.Bundle     // Catch:{ JSONException -> 0x08d6 }
            r12.<init>()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r14 = "build_build"
            java.lang.String r15 = r4.zzcsp     // Catch:{ JSONException -> 0x08d6 }
            r12.putString(r14, r15)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r14 = "build_device"
            java.lang.String r15 = r4.zzcsq     // Catch:{ JSONException -> 0x08d6 }
            r12.putString(r14, r15)     // Catch:{ JSONException -> 0x08d6 }
            android.os.Bundle r14 = new android.os.Bundle     // Catch:{ JSONException -> 0x08d6 }
            r14.<init>()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r15 = "is_charging"
            boolean r13 = r4.zzcsm     // Catch:{ JSONException -> 0x08d6 }
            r14.putBoolean(r15, r13)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r13 = "battery_level"
            r19 = r10
            double r9 = r4.zzcsl     // Catch:{ JSONException -> 0x08d6 }
            r14.putDouble(r13, r9)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r9 = "battery"
            r12.putBundle(r9, r14)     // Catch:{ JSONException -> 0x08d6 }
            android.os.Bundle r9 = new android.os.Bundle     // Catch:{ JSONException -> 0x08d6 }
            r9.<init>()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r10 = "active_network_state"
            int r13 = r4.zzcso     // Catch:{ JSONException -> 0x08d6 }
            r9.putInt(r10, r13)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r10 = "active_network_metered"
            boolean r13 = r4.zzcsn     // Catch:{ JSONException -> 0x08d6 }
            r9.putBoolean(r10, r13)     // Catch:{ JSONException -> 0x08d6 }
            if (r7 == 0) goto L_0x0457
            android.os.Bundle r10 = new android.os.Bundle     // Catch:{ JSONException -> 0x08d6 }
            r10.<init>()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r13 = "predicted_latency_micros"
            int r14 = r7.zzcsz     // Catch:{ JSONException -> 0x08d6 }
            r10.putInt(r13, r14)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r13 = "predicted_down_throughput_bps"
            long r14 = r7.zzcta     // Catch:{ JSONException -> 0x08d6 }
            r10.putLong(r13, r14)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r13 = "predicted_up_throughput_bps"
            long r14 = r7.zzctb     // Catch:{ JSONException -> 0x08d6 }
            r10.putLong(r13, r14)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = "predictions"
            r9.putBundle(r7, r10)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0457:
            java.lang.String r7 = "network"
            r12.putBundle(r7, r9)     // Catch:{ JSONException -> 0x08d6 }
            android.os.Bundle r7 = new android.os.Bundle     // Catch:{ JSONException -> 0x08d6 }
            r7.<init>()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r9 = "is_browser_custom_tabs_capable"
            boolean r10 = r4.zzcsr     // Catch:{ JSONException -> 0x08d6 }
            r7.putBoolean(r9, r10)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r9 = "browser"
            r12.putBundle(r9, r7)     // Catch:{ JSONException -> 0x08d6 }
            if (r19 == 0) goto L_0x0532
            java.lang.String r7 = "android_mem_info"
            android.os.Bundle r9 = new android.os.Bundle     // Catch:{ JSONException -> 0x08d6 }
            r9.<init>()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r10 = "runtime_free"
            java.lang.String r13 = "runtime_free_memory"
            r22 = r2
            r23 = r3
            r20 = r5
            r21 = r6
            r14 = r19
            r5 = -1
            long r2 = r14.getLong(r13, r5)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r2 = java.lang.Long.toString(r2)     // Catch:{ JSONException -> 0x08d6 }
            r9.putString(r10, r2)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r2 = "runtime_max"
            java.lang.String r3 = "runtime_max_memory"
            r24 = r4
            long r3 = r14.getLong(r3, r5)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = java.lang.Long.toString(r3)     // Catch:{ JSONException -> 0x08d6 }
            r9.putString(r2, r3)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r2 = "runtime_total"
            java.lang.String r3 = "runtime_total_memory"
            long r3 = r14.getLong(r3, r5)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = java.lang.Long.toString(r3)     // Catch:{ JSONException -> 0x08d6 }
            r9.putString(r2, r3)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r2 = "web_view_count"
            java.lang.String r3 = "web_view_count"
            r10 = 0
            int r3 = r14.getInt(r3, r10)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ JSONException -> 0x08d6 }
            r9.putString(r2, r3)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r2 = "debug_memory_info"
            android.os.Parcelable r2 = r14.getParcelable(r2)     // Catch:{ JSONException -> 0x08d6 }
            android.os.Debug$MemoryInfo r2 = (android.os.Debug.MemoryInfo) r2     // Catch:{ JSONException -> 0x08d6 }
            if (r2 == 0) goto L_0x052e
            java.lang.String r3 = "debug_info_dalvik_private_dirty"
            int r4 = r2.dalvikPrivateDirty     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ JSONException -> 0x08d6 }
            r9.putString(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "debug_info_dalvik_pss"
            int r4 = r2.dalvikPss     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ JSONException -> 0x08d6 }
            r9.putString(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "debug_info_dalvik_shared_dirty"
            int r4 = r2.dalvikSharedDirty     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ JSONException -> 0x08d6 }
            r9.putString(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "debug_info_native_private_dirty"
            int r4 = r2.nativePrivateDirty     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ JSONException -> 0x08d6 }
            r9.putString(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "debug_info_native_pss"
            int r4 = r2.nativePss     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ JSONException -> 0x08d6 }
            r9.putString(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "debug_info_native_shared_dirty"
            int r4 = r2.nativeSharedDirty     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ JSONException -> 0x08d6 }
            r9.putString(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "debug_info_other_private_dirty"
            int r4 = r2.otherPrivateDirty     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ JSONException -> 0x08d6 }
            r9.putString(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "debug_info_other_pss"
            int r4 = r2.otherPss     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ JSONException -> 0x08d6 }
            r9.putString(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "debug_info_other_shared_dirty"
            int r2 = r2.otherSharedDirty     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ JSONException -> 0x08d6 }
            r9.putString(r3, r2)     // Catch:{ JSONException -> 0x08d6 }
        L_0x052e:
            r12.putBundle(r7, r9)     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x053d
        L_0x0532:
            r22 = r2
            r23 = r3
            r24 = r4
            r20 = r5
            r21 = r6
            r10 = 0
        L_0x053d:
            android.os.Bundle r2 = new android.os.Bundle     // Catch:{ JSONException -> 0x08d6 }
            r2.<init>()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "parental_controls"
            r2.putBundle(r3, r11)     // Catch:{ JSONException -> 0x08d6 }
            r3 = r24
            java.lang.String r4 = r3.zzcse     // Catch:{ JSONException -> 0x08d6 }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ JSONException -> 0x08d6 }
            if (r4 != 0) goto L_0x0558
            java.lang.String r4 = "package_version"
            java.lang.String r5 = r3.zzcse     // Catch:{ JSONException -> 0x08d6 }
            r2.putString(r4, r5)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0558:
            java.lang.String r4 = "play_store"
            r12.putBundle(r4, r2)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r2 = "device"
            r8.put(r2, r12)     // Catch:{ JSONException -> 0x08d6 }
            android.os.Bundle r2 = new android.os.Bundle     // Catch:{ JSONException -> 0x08d6 }
            r2.<init>()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = "doritos"
            java.lang.String r5 = r1.zzcpc     // Catch:{ JSONException -> 0x08d6 }
            r2.putString(r4, r5)     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r4 = com.google.android.gms.internal.zzmq.zzbjt     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zzmo r5 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Object r4 = r5.zzd(r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Boolean r4 = (java.lang.Boolean) r4     // Catch:{ JSONException -> 0x08d6 }
            boolean r4 = r4.booleanValue()     // Catch:{ JSONException -> 0x08d6 }
            if (r4 == 0) goto L_0x05bc
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r4 = r1.zzcpd     // Catch:{ JSONException -> 0x08d6 }
            if (r4 == 0) goto L_0x0591
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r4 = r1.zzcpd     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = r4.getId()     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r4 = r1.zzcpd     // Catch:{ JSONException -> 0x08d6 }
            boolean r9 = r4.isLimitAdTrackingEnabled()     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x0593
        L_0x0591:
            r9 = r10
            r7 = 0
        L_0x0593:
            boolean r4 = android.text.TextUtils.isEmpty(r7)     // Catch:{ JSONException -> 0x08d6 }
            if (r4 != 0) goto L_0x05ab
            java.lang.String r4 = "rdid"
            r2.putString(r4, r7)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = "is_lat"
            r2.putBoolean(r4, r9)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = "idtype"
            java.lang.String r5 = "adid"
        L_0x05a7:
            r2.putString(r4, r5)     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x05bc
        L_0x05ab:
            com.google.android.gms.internal.zzjk.zzhx()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = com.google.android.gms.internal.zzais.zzbc(r25)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r5 = "pdid"
            r2.putString(r5, r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = "pdidtype"
            java.lang.String r5 = "ssaid"
            goto L_0x05a7
        L_0x05bc:
            java.lang.String r4 = "pii"
            r8.put(r4, r2)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r2 = "platform"
            java.lang.String r4 = android.os.Build.MANUFACTURER     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r2, r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r2 = "submodel"
            java.lang.String r4 = android.os.Build.MODEL     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r2, r4)     // Catch:{ JSONException -> 0x08d6 }
            if (r23 == 0) goto L_0x05d9
            r2 = r23
            zza(r8, r2)     // Catch:{ JSONException -> 0x08d6 }
            r2 = r22
            goto L_0x05ef
        L_0x05d9:
            r2 = r22
            com.google.android.gms.internal.zzis r4 = r2.zzclo     // Catch:{ JSONException -> 0x08d6 }
            int r4 = r4.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r5 = 2
            if (r4 < r5) goto L_0x05ef
            com.google.android.gms.internal.zzis r4 = r2.zzclo     // Catch:{ JSONException -> 0x08d6 }
            android.location.Location r4 = r4.zzbcd     // Catch:{ JSONException -> 0x08d6 }
            if (r4 == 0) goto L_0x05ef
            com.google.android.gms.internal.zzis r4 = r2.zzclo     // Catch:{ JSONException -> 0x08d6 }
            android.location.Location r4 = r4.zzbcd     // Catch:{ JSONException -> 0x08d6 }
            zza(r8, r4)     // Catch:{ JSONException -> 0x08d6 }
        L_0x05ef:
            int r4 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r5 = 2
            if (r4 < r5) goto L_0x05fb
            java.lang.String r4 = "quality_signals"
            android.os.Bundle r5 = r2.zzclt     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r4, r5)     // Catch:{ JSONException -> 0x08d6 }
        L_0x05fb:
            int r4 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r5 = 4
            if (r4 < r5) goto L_0x060f
            boolean r4 = r2.zzclw     // Catch:{ JSONException -> 0x08d6 }
            if (r4 == 0) goto L_0x060f
            java.lang.String r4 = "forceHttps"
            boolean r5 = r2.zzclw     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r4, r5)     // Catch:{ JSONException -> 0x08d6 }
        L_0x060f:
            if (r20 == 0) goto L_0x0618
            java.lang.String r4 = "content_info"
            r5 = r20
            r8.put(r4, r5)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0618:
            int r4 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r5 = 5
            if (r4 < r5) goto L_0x063f
            java.lang.String r3 = "u_sd"
            float r4 = r2.zzaxd     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Float r4 = java.lang.Float.valueOf(r4)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "sh"
            int r4 = r2.zzcly     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "sw"
            int r4 = r2.zzclx     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x0660
        L_0x063f:
            java.lang.String r4 = "u_sd"
            float r5 = r3.zzaxd     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Float r5 = java.lang.Float.valueOf(r5)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r4, r5)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = "sh"
            int r5 = r3.zzcly     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r4, r5)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = "sw"
            int r3 = r3.zzclx     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r4, r3)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0660:
            int r3 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r4 = 6
            if (r3 < r4) goto L_0x068c
            java.lang.String r3 = r2.zzclz     // Catch:{ JSONException -> 0x08d6 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ JSONException -> 0x08d6 }
            if (r3 != 0) goto L_0x0681
            java.lang.String r3 = "view_hierarchy"
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ JSONException -> 0x067a }
            java.lang.String r5 = r2.zzclz     // Catch:{ JSONException -> 0x067a }
            r4.<init>(r5)     // Catch:{ JSONException -> 0x067a }
            r8.put(r3, r4)     // Catch:{ JSONException -> 0x067a }
            goto L_0x0681
        L_0x067a:
            r0 = move-exception
            r3 = r0
            java.lang.String r4 = "Problem serializing view hierarchy to JSON"
            com.google.android.gms.internal.zzafj.zzc(r4, r3)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0681:
            java.lang.String r3 = "correlation_id"
            long r4 = r2.zzcma     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
        L_0x068c:
            int r3 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r4 = 7
            if (r3 < r4) goto L_0x0698
            java.lang.String r3 = "request_id"
            java.lang.String r4 = r2.zzcmb     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0698:
            int r3 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r4 = 12
            if (r3 < r4) goto L_0x06ad
            java.lang.String r3 = r2.zzcmf     // Catch:{ JSONException -> 0x08d6 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ JSONException -> 0x08d6 }
            if (r3 != 0) goto L_0x06ad
            java.lang.String r3 = "anchor"
            java.lang.String r4 = r2.zzcmf     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
        L_0x06ad:
            int r3 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r4 = 13
            if (r3 < r4) goto L_0x06be
            java.lang.String r3 = "android_app_volume"
            float r4 = r2.zzcmg     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Float r4 = java.lang.Float.valueOf(r4)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
        L_0x06be:
            int r3 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r4 = 18
            if (r3 < r4) goto L_0x06cf
            java.lang.String r3 = "android_app_muted"
            boolean r5 = r2.zzcmm     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r3, r5)     // Catch:{ JSONException -> 0x08d6 }
        L_0x06cf:
            int r3 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r5 = 14
            if (r3 < r5) goto L_0x06e4
            int r3 = r2.zzcmh     // Catch:{ JSONException -> 0x08d6 }
            if (r3 <= 0) goto L_0x06e4
            java.lang.String r3 = "target_api"
            int r5 = r2.zzcmh     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r3, r5)     // Catch:{ JSONException -> 0x08d6 }
        L_0x06e4:
            int r3 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r5 = 15
            if (r3 < r5) goto L_0x06fc
            java.lang.String r3 = "scroll_index"
            int r5 = r2.zzcmi     // Catch:{ JSONException -> 0x08d6 }
            r6 = -1
            if (r5 != r6) goto L_0x06f2
            goto L_0x06f5
        L_0x06f2:
            int r11 = r2.zzcmi     // Catch:{ JSONException -> 0x08d6 }
            r6 = r11
        L_0x06f5:
            java.lang.Integer r5 = java.lang.Integer.valueOf(r6)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r3, r5)     // Catch:{ JSONException -> 0x08d6 }
        L_0x06fc:
            int r3 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r5 = 16
            if (r3 < r5) goto L_0x070d
            java.lang.String r3 = "_activity_context"
            boolean r5 = r2.zzcmj     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r3, r5)     // Catch:{ JSONException -> 0x08d6 }
        L_0x070d:
            int r3 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            if (r3 < r4) goto L_0x0738
            java.lang.String r3 = r2.zzcmn     // Catch:{ JSONException -> 0x08d6 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ JSONException -> 0x08d6 }
            if (r3 != 0) goto L_0x072d
            java.lang.String r3 = "app_settings"
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0726 }
            java.lang.String r6 = r2.zzcmn     // Catch:{ JSONException -> 0x0726 }
            r5.<init>(r6)     // Catch:{ JSONException -> 0x0726 }
            r8.put(r3, r5)     // Catch:{ JSONException -> 0x0726 }
            goto L_0x072d
        L_0x0726:
            r0 = move-exception
            r3 = r0
            java.lang.String r5 = "Problem creating json from app settings"
            com.google.android.gms.internal.zzafj.zzc(r5, r3)     // Catch:{ JSONException -> 0x08d6 }
        L_0x072d:
            java.lang.String r3 = "render_in_browser"
            boolean r5 = r2.zzcbz     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r3, r5)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0738:
            int r3 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            if (r3 < r4) goto L_0x0747
            java.lang.String r3 = "android_num_video_cache_tasks"
            int r4 = r2.zzcmo     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r3, r4)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0747:
            com.google.android.gms.internal.zzaiy r3 = r2.zzatd     // Catch:{ JSONException -> 0x08d6 }
            boolean r4 = r2.zzcna     // Catch:{ JSONException -> 0x08d6 }
            boolean r1 = r1.zzcph     // Catch:{ JSONException -> 0x08d6 }
            android.os.Bundle r5 = new android.os.Bundle     // Catch:{ JSONException -> 0x08d6 }
            r5.<init>()     // Catch:{ JSONException -> 0x08d6 }
            android.os.Bundle r6 = new android.os.Bundle     // Catch:{ JSONException -> 0x08d6 }
            r6.<init>()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = "cl"
            java.lang.String r9 = "175987007"
            r6.putString(r7, r9)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = "rapid_rc"
            java.lang.String r9 = "dev"
            r6.putString(r7, r9)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = "rapid_rollup"
            java.lang.String r9 = "HEAD"
            r6.putString(r7, r9)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = "build_meta"
            r5.putBundle(r7, r6)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r6 = "mf"
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r7 = com.google.android.gms.internal.zzmq.zzbmq     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zzmo r9 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Object r7 = r9.zzd(r7)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Boolean r7 = (java.lang.Boolean) r7     // Catch:{ JSONException -> 0x08d6 }
            boolean r7 = r7.booleanValue()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r7 = java.lang.Boolean.toString(r7)     // Catch:{ JSONException -> 0x08d6 }
            r5.putString(r6, r7)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r6 = "instant_app"
            r5.putBoolean(r6, r4)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = "lite"
            boolean r3 = r3.zzdcc     // Catch:{ JSONException -> 0x08d6 }
            r5.putBoolean(r4, r3)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "local_service"
            r5.putBoolean(r3, r1)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r1 = "sdk_env"
            r8.put(r1, r5)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r1 = "cache_state"
            r3 = r21
            r8.put(r1, r3)     // Catch:{ JSONException -> 0x08d6 }
            int r1 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r3 = 19
            if (r1 < r3) goto L_0x07b4
            java.lang.String r1 = "gct"
            java.lang.String r3 = r2.zzcmq     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r1, r3)     // Catch:{ JSONException -> 0x08d6 }
        L_0x07b4:
            int r1 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r3 = 21
            if (r1 < r3) goto L_0x07c5
            boolean r1 = r2.zzcmr     // Catch:{ JSONException -> 0x08d6 }
            if (r1 == 0) goto L_0x07c5
            java.lang.String r1 = "de"
            java.lang.String r3 = "1"
            r8.put(r1, r3)     // Catch:{ JSONException -> 0x08d6 }
        L_0x07c5:
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r1 = com.google.android.gms.internal.zzmq.zzbkg     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zzmo r3 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Object r1 = r3.zzd(r1)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ JSONException -> 0x08d6 }
            boolean r1 = r1.booleanValue()     // Catch:{ JSONException -> 0x08d6 }
            if (r1 == 0) goto L_0x080b
            com.google.android.gms.internal.zziw r1 = r2.zzath     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r1 = r1.zzbda     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "interstitial_mb"
            boolean r3 = r1.equals(r3)     // Catch:{ JSONException -> 0x08d6 }
            if (r3 != 0) goto L_0x07ee
            java.lang.String r3 = "reward_mb"
            boolean r1 = r1.equals(r3)     // Catch:{ JSONException -> 0x08d6 }
            if (r1 == 0) goto L_0x07ec
            goto L_0x07ee
        L_0x07ec:
            r1 = r10
            goto L_0x07ef
        L_0x07ee:
            r1 = 1
        L_0x07ef:
            android.os.Bundle r3 = r2.zzcms     // Catch:{ JSONException -> 0x08d6 }
            if (r3 == 0) goto L_0x07f6
            r18 = 1
            goto L_0x07f8
        L_0x07f6:
            r18 = r10
        L_0x07f8:
            if (r1 == 0) goto L_0x080b
            if (r18 == 0) goto L_0x080b
            android.os.Bundle r1 = new android.os.Bundle     // Catch:{ JSONException -> 0x08d6 }
            r1.<init>()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r4 = "interstitial_pool"
            r1.putBundle(r4, r3)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = "counters"
            r8.put(r3, r1)     // Catch:{ JSONException -> 0x08d6 }
        L_0x080b:
            java.lang.String r1 = r2.zzcmt     // Catch:{ JSONException -> 0x08d6 }
            if (r1 == 0) goto L_0x0816
            java.lang.String r1 = "gmp_app_id"
            java.lang.String r3 = r2.zzcmt     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r1, r3)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0816:
            java.lang.String r1 = r2.zzcmu     // Catch:{ JSONException -> 0x08d6 }
            if (r1 == 0) goto L_0x0839
            java.lang.String r1 = "TIME_OUT"
            java.lang.String r3 = r2.zzcmu     // Catch:{ JSONException -> 0x08d6 }
            boolean r1 = r1.equals(r3)     // Catch:{ JSONException -> 0x08d6 }
            if (r1 == 0) goto L_0x0834
            java.lang.String r1 = "sai_timeout"
            com.google.android.gms.internal.zzmg<java.lang.Long> r3 = com.google.android.gms.internal.zzmq.zzbjj     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zzmo r4 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Object r3 = r4.zzd(r3)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0830:
            r8.put(r1, r3)     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x083e
        L_0x0834:
            java.lang.String r1 = "fbs_aiid"
            java.lang.String r3 = r2.zzcmu     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x0830
        L_0x0839:
            java.lang.String r1 = "fbs_aiid"
            java.lang.String r3 = ""
            goto L_0x0830
        L_0x083e:
            java.lang.String r1 = r2.zzcmv     // Catch:{ JSONException -> 0x08d6 }
            if (r1 == 0) goto L_0x0849
            java.lang.String r1 = "fbs_aeid"
            java.lang.String r3 = r2.zzcmv     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r1, r3)     // Catch:{ JSONException -> 0x08d6 }
        L_0x0849:
            int r1 = r2.versionCode     // Catch:{ JSONException -> 0x08d6 }
            r3 = 24
            if (r1 < r3) goto L_0x085a
            java.lang.String r1 = "disable_ml"
            boolean r2 = r2.zzcnb     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ JSONException -> 0x08d6 }
            r8.put(r1, r2)     // Catch:{ JSONException -> 0x08d6 }
        L_0x085a:
            com.google.android.gms.internal.zzmg<java.lang.String> r1 = com.google.android.gms.internal.zzmq.zzbhh     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zzmo r2 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Object r1 = r2.zzd(r1)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ JSONException -> 0x08d6 }
            if (r1 == 0) goto L_0x08a1
            boolean r2 = r1.isEmpty()     // Catch:{ JSONException -> 0x08d6 }
            if (r2 != 0) goto L_0x08a1
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zzmg<java.lang.Integer> r3 = com.google.android.gms.internal.zzmq.zzbhi     // Catch:{ JSONException -> 0x08d6 }
            com.google.android.gms.internal.zzmo r4 = com.google.android.gms.ads.internal.zzbs.zzep()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Object r3 = r4.zzd(r3)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ JSONException -> 0x08d6 }
            int r3 = r3.intValue()     // Catch:{ JSONException -> 0x08d6 }
            if (r2 < r3) goto L_0x08a1
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ JSONException -> 0x08d6 }
            r2.<init>()     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r3 = ","
            java.lang.String[] r1 = r1.split(r3)     // Catch:{ JSONException -> 0x08d6 }
            int r3 = r1.length     // Catch:{ JSONException -> 0x08d6 }
        L_0x088e:
            if (r10 >= r3) goto L_0x089c
            r4 = r1[r10]     // Catch:{ JSONException -> 0x08d6 }
            java.util.List r5 = com.google.android.gms.internal.zzaiq.zzck(r4)     // Catch:{ JSONException -> 0x08d6 }
            r2.put(r4, r5)     // Catch:{ JSONException -> 0x08d6 }
            int r10 = r10 + 1
            goto L_0x088e
        L_0x089c:
            java.lang.String r1 = "video_decoders"
            r8.put(r1, r2)     // Catch:{ JSONException -> 0x08d6 }
        L_0x08a1:
            r1 = 2
            boolean r2 = com.google.android.gms.internal.zzafj.zzae(r1)     // Catch:{ JSONException -> 0x08d6 }
            if (r2 == 0) goto L_0x08cd
            com.google.android.gms.internal.zzagr r2 = com.google.android.gms.ads.internal.zzbs.zzec()     // Catch:{ JSONException -> 0x08d6 }
            org.json.JSONObject r2 = r2.zzp(r8)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r1 = r2.toString(r1)     // Catch:{ JSONException -> 0x08d6 }
            java.lang.String r2 = "Ad Request JSON: "
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ JSONException -> 0x08d6 }
            int r3 = r1.length()     // Catch:{ JSONException -> 0x08d6 }
            if (r3 == 0) goto L_0x08c5
            java.lang.String r1 = r2.concat(r1)     // Catch:{ JSONException -> 0x08d6 }
            goto L_0x08ca
        L_0x08c5:
            java.lang.String r1 = new java.lang.String     // Catch:{ JSONException -> 0x08d6 }
            r1.<init>(r2)     // Catch:{ JSONException -> 0x08d6 }
        L_0x08ca:
            com.google.android.gms.internal.zzafj.v(r1)     // Catch:{ JSONException -> 0x08d6 }
        L_0x08cd:
            com.google.android.gms.internal.zzagr r1 = com.google.android.gms.ads.internal.zzbs.zzec()     // Catch:{ JSONException -> 0x08d6 }
            org.json.JSONObject r1 = r1.zzp(r8)     // Catch:{ JSONException -> 0x08d6 }
            return r1
        L_0x08d6:
            r0 = move-exception
            r1 = r0
            java.lang.String r2 = "Problem serializing ad request to JSON: "
            java.lang.String r1 = r1.getMessage()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r3 = r1.length()
            if (r3 == 0) goto L_0x08ed
            java.lang.String r1 = r2.concat(r1)
            goto L_0x08f2
        L_0x08ed:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r2)
        L_0x08f2:
            com.google.android.gms.internal.zzafj.zzco(r1)
            r1 = 0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzabm.zza(android.content.Context, com.google.android.gms.internal.zzabf):org.json.JSONObject");
    }

    private static void zza(HashMap<String, Object> hashMap, Location location) {
        HashMap hashMap2 = new HashMap();
        Float valueOf = Float.valueOf(location.getAccuracy() * 1000.0f);
        Long valueOf2 = Long.valueOf(location.getTime() * 1000);
        Long valueOf3 = Long.valueOf((long) (location.getLatitude() * 1.0E7d));
        Long valueOf4 = Long.valueOf((long) (location.getLongitude() * 1.0E7d));
        hashMap2.put("radius", valueOf);
        hashMap2.put("lat", valueOf3);
        hashMap2.put("long", valueOf4);
        hashMap2.put("time", valueOf2);
        hashMap.put("uule", hashMap2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0121  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0131  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0140  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x014f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.json.JSONObject zzb(com.google.android.gms.internal.zzaad r9) throws org.json.JSONException {
        /*
            org.json.JSONObject r0 = new org.json.JSONObject
            r0.<init>()
            java.lang.String r1 = r9.zzchl
            if (r1 == 0) goto L_0x0010
            java.lang.String r1 = "ad_base_url"
            java.lang.String r2 = r9.zzchl
            r0.put(r1, r2)
        L_0x0010:
            java.lang.String r1 = r9.zzcnj
            if (r1 == 0) goto L_0x001b
            java.lang.String r1 = "ad_size"
            java.lang.String r2 = r9.zzcnj
            r0.put(r1, r2)
        L_0x001b:
            java.lang.String r1 = "native"
            boolean r2 = r9.zzbdd
            r0.put(r1, r2)
            boolean r1 = r9.zzbdd
            if (r1 == 0) goto L_0x002e
            java.lang.String r1 = "ad_json"
        L_0x0028:
            java.lang.String r2 = r9.body
            r0.put(r1, r2)
            goto L_0x0031
        L_0x002e:
            java.lang.String r1 = "ad_html"
            goto L_0x0028
        L_0x0031:
            java.lang.String r1 = r9.zzcnl
            if (r1 == 0) goto L_0x003c
            java.lang.String r1 = "debug_dialog"
            java.lang.String r2 = r9.zzcnl
            r0.put(r1, r2)
        L_0x003c:
            java.lang.String r1 = r9.zzcoc
            if (r1 == 0) goto L_0x0047
            java.lang.String r1 = "debug_signals"
            java.lang.String r2 = r9.zzcoc
            r0.put(r1, r2)
        L_0x0047:
            long r1 = r9.zzcnf
            r3 = -1
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x005d
            java.lang.String r1 = "interstitial_timeout"
            long r5 = r9.zzcnf
            double r5 = (double) r5
            r7 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r5 = r5 / r7
            r0.put(r1, r5)
        L_0x005d:
            int r1 = r9.orientation
            com.google.android.gms.internal.zzagw r2 = com.google.android.gms.ads.internal.zzbs.zzee()
            int r2 = r2.zzqa()
            if (r1 != r2) goto L_0x0071
            java.lang.String r1 = "orientation"
            java.lang.String r2 = "portrait"
        L_0x006d:
            r0.put(r1, r2)
            goto L_0x0082
        L_0x0071:
            int r1 = r9.orientation
            com.google.android.gms.internal.zzagw r2 = com.google.android.gms.ads.internal.zzbs.zzee()
            int r2 = r2.zzpz()
            if (r1 != r2) goto L_0x0082
            java.lang.String r1 = "orientation"
            java.lang.String r2 = "landscape"
            goto L_0x006d
        L_0x0082:
            java.util.List<java.lang.String> r1 = r9.zzcbv
            if (r1 == 0) goto L_0x0091
            java.lang.String r1 = "click_urls"
            java.util.List<java.lang.String> r2 = r9.zzcbv
            org.json.JSONArray r2 = zzo(r2)
            r0.put(r1, r2)
        L_0x0091:
            java.util.List<java.lang.String> r1 = r9.zzcbw
            if (r1 == 0) goto L_0x00a0
            java.lang.String r1 = "impression_urls"
            java.util.List<java.lang.String> r2 = r9.zzcbw
            org.json.JSONArray r2 = zzo(r2)
            r0.put(r1, r2)
        L_0x00a0:
            java.util.List<java.lang.String> r1 = r9.zzcni
            if (r1 == 0) goto L_0x00af
            java.lang.String r1 = "manual_impression_urls"
            java.util.List<java.lang.String> r2 = r9.zzcni
            org.json.JSONArray r2 = zzo(r2)
            r0.put(r1, r2)
        L_0x00af:
            java.lang.String r1 = r9.zzcno
            if (r1 == 0) goto L_0x00ba
            java.lang.String r1 = "active_view"
            java.lang.String r2 = r9.zzcno
            r0.put(r1, r2)
        L_0x00ba:
            java.lang.String r1 = "ad_is_javascript"
            boolean r2 = r9.zzcnm
            r0.put(r1, r2)
            java.lang.String r1 = r9.zzcnn
            if (r1 == 0) goto L_0x00cc
            java.lang.String r1 = "ad_passback_url"
            java.lang.String r2 = r9.zzcnn
            r0.put(r1, r2)
        L_0x00cc:
            java.lang.String r1 = "mediation"
            boolean r2 = r9.zzcng
            r0.put(r1, r2)
            java.lang.String r1 = "custom_render_allowed"
            boolean r2 = r9.zzcnp
            r0.put(r1, r2)
            java.lang.String r1 = "content_url_opted_out"
            boolean r2 = r9.zzcnq
            r0.put(r1, r2)
            java.lang.String r1 = "content_vertical_opted_out"
            boolean r2 = r9.zzcod
            r0.put(r1, r2)
            java.lang.String r1 = "prefetch"
            boolean r2 = r9.zzcnr
            r0.put(r1, r2)
            long r1 = r9.zzccb
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x00fc
            java.lang.String r1 = "refresh_interval_milliseconds"
            long r5 = r9.zzccb
            r0.put(r1, r5)
        L_0x00fc:
            long r1 = r9.zzcnh
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x0109
            java.lang.String r1 = "mediation_config_cache_time_milliseconds"
            long r2 = r9.zzcnh
            r0.put(r1, r2)
        L_0x0109:
            java.lang.String r1 = r9.zzcnu
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x0118
            java.lang.String r1 = "gws_query_id"
            java.lang.String r2 = r9.zzcnu
            r0.put(r1, r2)
        L_0x0118:
            java.lang.String r1 = "fluid"
            boolean r2 = r9.zzbde
            if (r2 == 0) goto L_0x0121
            java.lang.String r2 = "height"
            goto L_0x0123
        L_0x0121:
            java.lang.String r2 = ""
        L_0x0123:
            r0.put(r1, r2)
            java.lang.String r1 = "native_express"
            boolean r2 = r9.zzbdf
            r0.put(r1, r2)
            java.util.List<java.lang.String> r1 = r9.zzcnw
            if (r1 == 0) goto L_0x013c
            java.lang.String r1 = "video_start_urls"
            java.util.List<java.lang.String> r2 = r9.zzcnw
            org.json.JSONArray r2 = zzo(r2)
            r0.put(r1, r2)
        L_0x013c:
            java.util.List<java.lang.String> r1 = r9.zzcnx
            if (r1 == 0) goto L_0x014b
            java.lang.String r1 = "video_complete_urls"
            java.util.List<java.lang.String> r2 = r9.zzcnx
            org.json.JSONArray r2 = zzo(r2)
            r0.put(r1, r2)
        L_0x014b:
            com.google.android.gms.internal.zzadw r1 = r9.zzcnv
            if (r1 == 0) goto L_0x0171
            java.lang.String r1 = "rewards"
            com.google.android.gms.internal.zzadw r2 = r9.zzcnv
            org.json.JSONObject r3 = new org.json.JSONObject
            r3.<init>()
            java.lang.String r4 = "rb_type"
            java.lang.String r5 = r2.type
            r3.put(r4, r5)
            java.lang.String r4 = "rb_amount"
            int r2 = r2.zzcuk
            r3.put(r4, r2)
            org.json.JSONArray r2 = new org.json.JSONArray
            r2.<init>()
            r2.put(r3)
            r0.put(r1, r2)
        L_0x0171:
            java.lang.String r1 = "use_displayed_impression"
            boolean r2 = r9.zzcny
            r0.put(r1, r2)
            java.lang.String r1 = "auto_protection_configuration"
            com.google.android.gms.internal.zzaaf r2 = r9.zzcnz
            r0.put(r1, r2)
            java.lang.String r1 = "render_in_browser"
            boolean r9 = r9.zzcbz
            r0.put(r1, r9)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzabm.zzb(com.google.android.gms.internal.zzaad):org.json.JSONObject");
    }

    @Nullable
    private static JSONArray zzo(List<String> list) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (String put : list) {
            jSONArray.put(put);
        }
        return jSONArray;
    }

    private static Integer zzt(boolean z) {
        return Integer.valueOf(z ? 1 : 0);
    }
}
