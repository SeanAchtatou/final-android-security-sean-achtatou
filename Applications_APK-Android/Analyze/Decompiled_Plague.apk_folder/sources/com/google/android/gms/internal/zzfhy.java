package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfhy extends zzfee<zzfhy, zza> implements zzffk {
    private static volatile zzffm<zzfhy> zzbas;
    /* access modifiers changed from: private */
    public static final zzfhy zzpjj;
    private int zzltg;
    private int zzpjg;
    private String zzpjh = "";
    private zzfev<zzfde> zzpji = zzcvf();

    public static final class zza extends zzfef<zzfhy, zza> implements zzffk {
        private zza() {
            super(zzfhy.zzpjj);
        }

        /* synthetic */ zza(zzfhz zzfhz) {
            this();
        }
    }

    static {
        zzfhy zzfhy = new zzfhy();
        zzpjj = zzfhy;
        zzfhy.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzfhy.zzpbs.zzbim();
    }

    private zzfhy() {
    }

    public static zzfhy zzcxo() {
        return zzpjj;
    }

    public final int getCode() {
        return this.zzpjg;
    }

    public final String getMessage() {
        return this.zzpjh;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        boolean z = false;
        switch (zzfhz.zzbaq[i - 1]) {
            case 1:
                return new zzfhy();
            case 2:
                return zzpjj;
            case 3:
                this.zzpji.zzbim();
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzfhy zzfhy = (zzfhy) obj2;
                boolean z2 = this.zzpjg != 0;
                int i2 = this.zzpjg;
                if (zzfhy.zzpjg != 0) {
                    z = true;
                }
                this.zzpjg = zzfen.zza(z2, i2, z, zzfhy.zzpjg);
                this.zzpjh = zzfen.zza(!this.zzpjh.isEmpty(), this.zzpjh, true ^ zzfhy.zzpjh.isEmpty(), zzfhy.zzpjh);
                this.zzpji = zzfen.zza(this.zzpji, zzfhy.zzpji);
                if (zzfen == zzfel.zzpcb) {
                    this.zzltg |= zzfhy.zzltg;
                }
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzpjg = zzfdq.zzctv();
                                } else if (zzcts == 18) {
                                    this.zzpjh = zzfdq.zzctz();
                                } else if (zzcts == 26) {
                                    if (!this.zzpji.zzcth()) {
                                        zzfev<zzfde> zzfev = this.zzpji;
                                        int size = zzfev.size();
                                        this.zzpji = zzfev.zzln(size == 0 ? 10 : size << 1);
                                    }
                                    this.zzpji.add((zzfde) zzfdq.zza(zzfde.zzctj(), zzfea));
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzfhy.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzpjj);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzpjj;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzpjg != 0) {
            zzfdv.zzaa(1, this.zzpjg);
        }
        if (!this.zzpjh.isEmpty()) {
            zzfdv.zzn(2, this.zzpjh);
        }
        for (int i = 0; i < this.zzpji.size(); i++) {
            zzfdv.zza(3, this.zzpji.get(i));
        }
        this.zzpbs.zza(zzfdv);
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int zzad = this.zzpjg != 0 ? zzfdv.zzad(1, this.zzpjg) + 0 : 0;
        if (!this.zzpjh.isEmpty()) {
            zzad += zzfdv.zzo(2, this.zzpjh);
        }
        for (int i2 = 0; i2 < this.zzpji.size(); i2++) {
            zzad += zzfdv.zzb(3, this.zzpji.get(i2));
        }
        int zzhl = zzad + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
