package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;

final class zzav extends zzbm {
    private /* synthetic */ ConnectionResult zzfou;
    private /* synthetic */ zzau zzfov;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzav(zzau zzau, zzbk zzbk, ConnectionResult connectionResult) {
        super(zzbk);
        this.zzfov = zzau;
        this.zzfou = connectionResult;
    }

    public final void zzahp() {
        this.zzfov.zzfor.zze(this.zzfou);
    }
}
