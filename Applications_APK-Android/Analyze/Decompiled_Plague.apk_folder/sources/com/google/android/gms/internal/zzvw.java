package com.google.android.gms.internal;

import android.app.Activity;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.util.zze;
import java.util.Map;
import java.util.Set;

@zzzb
public final class zzvw extends zzwg {
    private static Set<String> zzcev = zze.zzb("top-left", "top-right", "top-center", "center", "bottom-left", "bottom-right", "bottom-center");
    private final Object mLock = new Object();
    private int zzakw = -1;
    private int zzakx = -1;
    private final zzama zzbwq;
    private final Activity zzcem;
    private String zzcew = "top-right";
    private boolean zzcex = true;
    private int zzcey = 0;
    private int zzcez = 0;
    private int zzcfa = 0;
    private int zzcfb = 0;
    private zzanp zzcfc;
    private ImageView zzcfd;
    private LinearLayout zzcfe;
    private zzwh zzcff;
    private PopupWindow zzcfg;
    private RelativeLayout zzcfh;
    private ViewGroup zzcfi;

    public zzvw(zzama zzama, zzwh zzwh) {
        super(zzama, "resize");
        this.zzbwq = zzama;
        this.zzcem = zzama.zzrz();
        this.zzcff = zzwh;
    }

    private final void zza(int i, int i2) {
        zzb(i, i2 - zzbs.zzec().zzh(this.zzcem)[0], this.zzakw, this.zzakx);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00f5, code lost:
        if ((r5 + 50) <= r1[1]) goto L_0x00f8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0105 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0107  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final int[] zzmk() {
        /*
            r9 = this;
            com.google.android.gms.internal.zzagr r0 = com.google.android.gms.ads.internal.zzbs.zzec()
            android.app.Activity r1 = r9.zzcem
            int[] r0 = r0.zzg(r1)
            com.google.android.gms.internal.zzagr r1 = com.google.android.gms.ads.internal.zzbs.zzec()
            android.app.Activity r2 = r9.zzcem
            int[] r1 = r1.zzh(r2)
            r2 = 0
            r3 = r0[r2]
            r4 = 1
            r0 = r0[r4]
            int r5 = r9.zzakw
            r6 = 2
            r7 = 50
            if (r5 < r7) goto L_0x00fd
            int r5 = r9.zzakw
            if (r5 <= r3) goto L_0x0027
            goto L_0x00fd
        L_0x0027:
            int r5 = r9.zzakx
            if (r5 < r7) goto L_0x00fa
            int r5 = r9.zzakx
            if (r5 <= r0) goto L_0x0031
            goto L_0x00fa
        L_0x0031:
            int r5 = r9.zzakx
            if (r5 != r0) goto L_0x003d
            int r0 = r9.zzakw
            if (r0 != r3) goto L_0x003d
            java.lang.String r0 = "Cannot resize to a full-screen ad."
            goto L_0x00ff
        L_0x003d:
            boolean r0 = r9.zzcex
            if (r0 == 0) goto L_0x00f8
            java.lang.String r0 = r9.zzcew
            r5 = -1
            int r8 = r0.hashCode()
            switch(r8) {
                case -1364013995: goto L_0x007e;
                case -1012429441: goto L_0x0074;
                case -655373719: goto L_0x006a;
                case 1163912186: goto L_0x0060;
                case 1288627767: goto L_0x0056;
                case 1755462605: goto L_0x004c;
                default: goto L_0x004b;
            }
        L_0x004b:
            goto L_0x0088
        L_0x004c:
            java.lang.String r8 = "top-center"
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0088
            r0 = r4
            goto L_0x0089
        L_0x0056:
            java.lang.String r8 = "bottom-center"
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0088
            r0 = 4
            goto L_0x0089
        L_0x0060:
            java.lang.String r8 = "bottom-right"
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0088
            r0 = 5
            goto L_0x0089
        L_0x006a:
            java.lang.String r8 = "bottom-left"
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0088
            r0 = 3
            goto L_0x0089
        L_0x0074:
            java.lang.String r8 = "top-left"
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0088
            r0 = r2
            goto L_0x0089
        L_0x007e:
            java.lang.String r8 = "center"
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0088
            r0 = r6
            goto L_0x0089
        L_0x0088:
            r0 = r5
        L_0x0089:
            switch(r0) {
                case 0: goto L_0x00e3;
                case 1: goto L_0x00d7;
                case 2: goto L_0x00c0;
                case 3: goto L_0x00b1;
                case 4: goto L_0x00a5;
                case 5: goto L_0x009b;
                default: goto L_0x008c;
            }
        L_0x008c:
            int r0 = r9.zzcey
            int r5 = r9.zzcfa
            int r0 = r0 + r5
            int r5 = r9.zzakw
            int r0 = r0 + r5
            int r0 = r0 - r7
        L_0x0095:
            int r5 = r9.zzcez
            int r8 = r9.zzcfb
            int r5 = r5 + r8
            goto L_0x00e9
        L_0x009b:
            int r0 = r9.zzcey
            int r5 = r9.zzcfa
            int r0 = r0 + r5
            int r5 = r9.zzakw
            int r0 = r0 + r5
            int r0 = r0 - r7
            goto L_0x00b6
        L_0x00a5:
            int r0 = r9.zzcey
            int r5 = r9.zzcfa
            int r0 = r0 + r5
            int r5 = r9.zzakw
            int r5 = r5 / r6
            int r0 = r0 + r5
            int r0 = r0 + -25
            goto L_0x00b6
        L_0x00b1:
            int r0 = r9.zzcey
            int r5 = r9.zzcfa
            int r0 = r0 + r5
        L_0x00b6:
            int r5 = r9.zzcez
            int r8 = r9.zzcfb
            int r5 = r5 + r8
            int r8 = r9.zzakx
            int r5 = r5 + r8
            int r5 = r5 - r7
            goto L_0x00e9
        L_0x00c0:
            int r0 = r9.zzcey
            int r5 = r9.zzcfa
            int r0 = r0 + r5
            int r5 = r9.zzakw
            int r5 = r5 / r6
            int r0 = r0 + r5
            int r0 = r0 + -25
            int r5 = r9.zzcez
            int r8 = r9.zzcfb
            int r5 = r5 + r8
            int r8 = r9.zzakx
            int r8 = r8 / r6
            int r5 = r5 + r8
            int r5 = r5 + -25
            goto L_0x00e9
        L_0x00d7:
            int r0 = r9.zzcey
            int r5 = r9.zzcfa
            int r0 = r0 + r5
            int r5 = r9.zzakw
            int r5 = r5 / r6
            int r0 = r0 + r5
            int r0 = r0 + -25
            goto L_0x0095
        L_0x00e3:
            int r0 = r9.zzcey
            int r5 = r9.zzcfa
            int r0 = r0 + r5
            goto L_0x0095
        L_0x00e9:
            if (r0 < 0) goto L_0x0102
            int r0 = r0 + r7
            if (r0 > r3) goto L_0x0102
            r0 = r1[r2]
            if (r5 < r0) goto L_0x0102
            int r5 = r5 + r7
            r0 = r1[r4]
            if (r5 <= r0) goto L_0x00f8
            goto L_0x0102
        L_0x00f8:
            r0 = r4
            goto L_0x0103
        L_0x00fa:
            java.lang.String r0 = "Height is too small or too large."
            goto L_0x00ff
        L_0x00fd:
            java.lang.String r0 = "Width is too small or too large."
        L_0x00ff:
            com.google.android.gms.internal.zzafj.zzco(r0)
        L_0x0102:
            r0 = r2
        L_0x0103:
            if (r0 != 0) goto L_0x0107
            r0 = 0
            return r0
        L_0x0107:
            boolean r0 = r9.zzcex
            if (r0 == 0) goto L_0x011c
            int[] r0 = new int[r6]
            int r1 = r9.zzcey
            int r3 = r9.zzcfa
            int r1 = r1 + r3
            r0[r2] = r1
            int r1 = r9.zzcez
            int r2 = r9.zzcfb
            int r1 = r1 + r2
            r0[r4] = r1
            return r0
        L_0x011c:
            com.google.android.gms.internal.zzagr r0 = com.google.android.gms.ads.internal.zzbs.zzec()
            android.app.Activity r1 = r9.zzcem
            int[] r0 = r0.zzg(r1)
            com.google.android.gms.internal.zzagr r1 = com.google.android.gms.ads.internal.zzbs.zzec()
            android.app.Activity r3 = r9.zzcem
            int[] r1 = r1.zzh(r3)
            r0 = r0[r2]
            int r3 = r9.zzcey
            int r5 = r9.zzcfa
            int r3 = r3 + r5
            int r5 = r9.zzcez
            int r7 = r9.zzcfb
            int r5 = r5 + r7
            if (r3 >= 0) goto L_0x0140
            r0 = r2
            goto L_0x014a
        L_0x0140:
            int r7 = r9.zzakw
            int r7 = r7 + r3
            if (r7 <= r0) goto L_0x0149
            int r3 = r9.zzakw
            int r0 = r0 - r3
            goto L_0x014a
        L_0x0149:
            r0 = r3
        L_0x014a:
            r3 = r1[r2]
            if (r5 >= r3) goto L_0x0151
            r5 = r1[r2]
            goto L_0x015e
        L_0x0151:
            int r3 = r9.zzakx
            int r3 = r3 + r5
            r7 = r1[r4]
            if (r3 <= r7) goto L_0x015e
            r1 = r1[r4]
            int r3 = r9.zzakx
            int r5 = r1 - r3
        L_0x015e:
            int[] r1 = new int[r6]
            r1[r2] = r0
            r1[r4] = r5
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzvw.zzmk():int[]");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzagr.zza(android.view.View, int, int, boolean):android.widget.PopupWindow
     arg types: [android.widget.RelativeLayout, int, int, int]
     candidates:
      com.google.android.gms.internal.zzagr.zza(android.content.Context, com.google.android.gms.internal.zzcs, java.lang.String, android.view.View):java.lang.String
      com.google.android.gms.internal.zzagr.zza(android.content.Context, java.lang.String, boolean, java.net.HttpURLConnection):void
      com.google.android.gms.internal.zzagr.zza(android.view.View, int, int, boolean):android.widget.PopupWindow */
    public final void execute(Map<String, String> map) {
        char c;
        synchronized (this.mLock) {
            if (this.zzcem == null) {
                zzbl("Not an activity context. Cannot resize.");
            } else if (this.zzbwq.zzso() == null) {
                zzbl("Webview is not yet available, size is not set.");
            } else if (this.zzbwq.zzso().zztx()) {
                zzbl("Is interstitial. Cannot resize an interstitial.");
            } else if (this.zzbwq.zzst()) {
                zzbl("Cannot resize an expanded banner.");
            } else {
                if (!TextUtils.isEmpty(map.get("width"))) {
                    zzbs.zzec();
                    this.zzakw = zzagr.zzcd(map.get("width"));
                }
                if (!TextUtils.isEmpty(map.get("height"))) {
                    zzbs.zzec();
                    this.zzakx = zzagr.zzcd(map.get("height"));
                }
                if (!TextUtils.isEmpty(map.get("offsetX"))) {
                    zzbs.zzec();
                    this.zzcfa = zzagr.zzcd(map.get("offsetX"));
                }
                if (!TextUtils.isEmpty(map.get("offsetY"))) {
                    zzbs.zzec();
                    this.zzcfb = zzagr.zzcd(map.get("offsetY"));
                }
                if (!TextUtils.isEmpty(map.get("allowOffscreen"))) {
                    this.zzcex = Boolean.parseBoolean(map.get("allowOffscreen"));
                }
                String str = map.get("customClosePosition");
                if (!TextUtils.isEmpty(str)) {
                    this.zzcew = str;
                }
                if (!(this.zzakw >= 0 && this.zzakx >= 0)) {
                    zzbl("Invalid width and height options. Cannot resize.");
                    return;
                }
                Window window = this.zzcem.getWindow();
                if (window != null) {
                    if (window.getDecorView() != null) {
                        int[] zzmk = zzmk();
                        if (zzmk == null) {
                            zzbl("Resize location out of screen or close button is not visible.");
                            return;
                        }
                        zzjk.zzhx();
                        int zzc = zzais.zzc(this.zzcem, this.zzakw);
                        zzjk.zzhx();
                        int zzc2 = zzais.zzc(this.zzcem, this.zzakx);
                        zzama zzama = this.zzbwq;
                        if (zzama == null) {
                            throw null;
                        }
                        ViewParent parent = ((View) zzama).getParent();
                        if (parent == null || !(parent instanceof ViewGroup)) {
                            zzbl("Webview is detached, probably in the middle of a resize or expand.");
                            return;
                        }
                        ViewGroup viewGroup = (ViewGroup) parent;
                        zzama zzama2 = this.zzbwq;
                        if (zzama2 == null) {
                            throw null;
                        }
                        viewGroup.removeView((View) zzama2);
                        if (this.zzcfg == null) {
                            this.zzcfi = (ViewGroup) parent;
                            zzbs.zzec();
                            zzama zzama3 = this.zzbwq;
                            if (zzama3 == null) {
                                throw null;
                            }
                            Bitmap zzm = zzagr.zzm((View) zzama3);
                            this.zzcfd = new ImageView(this.zzcem);
                            this.zzcfd.setImageBitmap(zzm);
                            this.zzcfc = this.zzbwq.zzso();
                            this.zzcfi.addView(this.zzcfd);
                        } else {
                            this.zzcfg.dismiss();
                        }
                        this.zzcfh = new RelativeLayout(this.zzcem);
                        this.zzcfh.setBackgroundColor(0);
                        this.zzcfh.setLayoutParams(new ViewGroup.LayoutParams(zzc, zzc2));
                        zzbs.zzec();
                        this.zzcfg = zzagr.zza((View) this.zzcfh, zzc, zzc2, false);
                        this.zzcfg.setOutsideTouchable(true);
                        this.zzcfg.setTouchable(true);
                        this.zzcfg.setClippingEnabled(!this.zzcex);
                        RelativeLayout relativeLayout = this.zzcfh;
                        zzama zzama4 = this.zzbwq;
                        if (zzama4 == null) {
                            throw null;
                        }
                        relativeLayout.addView((View) zzama4, -1, -1);
                        this.zzcfe = new LinearLayout(this.zzcem);
                        zzjk.zzhx();
                        int zzc3 = zzais.zzc(this.zzcem, 50);
                        zzjk.zzhx();
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(zzc3, zzais.zzc(this.zzcem, 50));
                        String str2 = this.zzcew;
                        switch (str2.hashCode()) {
                            case -1364013995:
                                if (str2.equals("center")) {
                                    c = 2;
                                    break;
                                }
                                c = 65535;
                                break;
                            case -1012429441:
                                if (str2.equals("top-left")) {
                                    c = 0;
                                    break;
                                }
                                c = 65535;
                                break;
                            case -655373719:
                                if (str2.equals("bottom-left")) {
                                    c = 3;
                                    break;
                                }
                                c = 65535;
                                break;
                            case 1163912186:
                                if (str2.equals("bottom-right")) {
                                    c = 5;
                                    break;
                                }
                                c = 65535;
                                break;
                            case 1288627767:
                                if (str2.equals("bottom-center")) {
                                    c = 4;
                                    break;
                                }
                                c = 65535;
                                break;
                            case 1755462605:
                                if (str2.equals("top-center")) {
                                    c = 1;
                                    break;
                                }
                                c = 65535;
                                break;
                            default:
                                c = 65535;
                                break;
                        }
                        switch (c) {
                            case 0:
                                layoutParams.addRule(10);
                                layoutParams.addRule(9);
                                break;
                            case 1:
                                layoutParams.addRule(10);
                                layoutParams.addRule(14);
                                break;
                            case 2:
                                layoutParams.addRule(13);
                                break;
                            case 3:
                                layoutParams.addRule(12);
                                layoutParams.addRule(9);
                                break;
                            case 4:
                                layoutParams.addRule(12);
                                layoutParams.addRule(14);
                                break;
                            case 5:
                                layoutParams.addRule(12);
                                layoutParams.addRule(11);
                                break;
                            default:
                                layoutParams.addRule(10);
                                layoutParams.addRule(11);
                                break;
                        }
                        this.zzcfe.setOnClickListener(new zzvx(this));
                        this.zzcfe.setContentDescription("Close button");
                        this.zzcfh.addView(this.zzcfe, layoutParams);
                        try {
                            PopupWindow popupWindow = this.zzcfg;
                            View decorView = window.getDecorView();
                            zzjk.zzhx();
                            int zzc4 = zzais.zzc(this.zzcem, zzmk[0]);
                            zzjk.zzhx();
                            popupWindow.showAtLocation(decorView, 0, zzc4, zzais.zzc(this.zzcem, zzmk[1]));
                            int i = zzmk[0];
                            int i2 = zzmk[1];
                            if (this.zzcff != null) {
                                this.zzcff.zza(i, i2, this.zzakw, this.zzakx);
                            }
                            this.zzbwq.zza(zzanp.zzi(zzc, zzc2));
                            zza(zzmk[0], zzmk[1]);
                            zzbn("resized");
                            return;
                        } catch (RuntimeException e) {
                            String valueOf = String.valueOf(e.getMessage());
                            zzbl(valueOf.length() != 0 ? "Cannot show popup window: ".concat(valueOf) : new String("Cannot show popup window: "));
                            RelativeLayout relativeLayout2 = this.zzcfh;
                            zzama zzama5 = this.zzbwq;
                            if (zzama5 == null) {
                                throw null;
                            }
                            relativeLayout2.removeView((View) zzama5);
                            if (this.zzcfi != null) {
                                this.zzcfi.removeView(this.zzcfd);
                                ViewGroup viewGroup2 = this.zzcfi;
                                zzama zzama6 = this.zzbwq;
                                if (zzama6 == null) {
                                    throw null;
                                }
                                viewGroup2.addView((View) zzama6);
                                this.zzbwq.zza(this.zzcfc);
                            }
                            return;
                        }
                    }
                }
                zzbl("Activity context is not ready, cannot get window or decor view.");
            }
        }
    }

    public final void zza(int i, int i2, boolean z) {
        synchronized (this.mLock) {
            this.zzcey = i;
            this.zzcez = i2;
            if (this.zzcfg != null && z) {
                int[] zzmk = zzmk();
                if (zzmk != null) {
                    PopupWindow popupWindow = this.zzcfg;
                    zzjk.zzhx();
                    int zzc = zzais.zzc(this.zzcem, zzmk[0]);
                    zzjk.zzhx();
                    popupWindow.update(zzc, zzais.zzc(this.zzcem, zzmk[1]), this.zzcfg.getWidth(), this.zzcfg.getHeight());
                    zza(zzmk[0], zzmk[1]);
                } else {
                    zzl(true);
                }
            }
        }
    }

    public final void zzb(int i, int i2) {
        this.zzcey = i;
        this.zzcez = i2;
    }

    public final void zzl(boolean z) {
        synchronized (this.mLock) {
            if (this.zzcfg != null) {
                this.zzcfg.dismiss();
                RelativeLayout relativeLayout = this.zzcfh;
                zzama zzama = this.zzbwq;
                if (zzama == null) {
                    throw null;
                }
                relativeLayout.removeView((View) zzama);
                if (this.zzcfi != null) {
                    this.zzcfi.removeView(this.zzcfd);
                    ViewGroup viewGroup = this.zzcfi;
                    zzama zzama2 = this.zzbwq;
                    if (zzama2 == null) {
                        throw null;
                    }
                    viewGroup.addView((View) zzama2);
                    this.zzbwq.zza(this.zzcfc);
                }
                if (z) {
                    zzbn("default");
                    if (this.zzcff != null) {
                        this.zzcff.zzco();
                    }
                }
                this.zzcfg = null;
                this.zzcfh = null;
                this.zzcfi = null;
                this.zzcfe = null;
            }
        }
    }

    public final boolean zzml() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzcfg != null;
        }
        return z;
    }
}
