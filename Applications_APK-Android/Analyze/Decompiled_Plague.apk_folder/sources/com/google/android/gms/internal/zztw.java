package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzbs;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@zzzb
public final class zztw implements zztl {
    private final Context mContext;
    /* access modifiers changed from: private */
    public final Object mLock = new Object();
    /* access modifiers changed from: private */
    public final long mStartTime;
    private final zzuc zzanb;
    private final boolean zzauu;
    private final zztn zzccq;
    private final boolean zzccu;
    private final boolean zzccv;
    private final zzzz zzcdj;
    /* access modifiers changed from: private */
    public final long zzcdk;
    private final int zzcdl;
    /* access modifiers changed from: private */
    public boolean zzcdm = false;
    /* access modifiers changed from: private */
    public final Map<zzajp<zztt>, zztq> zzcdn = new HashMap();
    private final String zzcdo;
    private List<zztt> zzcdp = new ArrayList();

    public zztw(Context context, zzzz zzzz, zzuc zzuc, zztn zztn, boolean z, boolean z2, String str, long j, long j2, int i, boolean z3) {
        this.mContext = context;
        this.zzcdj = zzzz;
        this.zzanb = zzuc;
        this.zzccq = zztn;
        this.zzauu = z;
        this.zzccu = z2;
        this.zzcdo = str;
        this.mStartTime = j;
        this.zzcdk = j2;
        this.zzcdl = 2;
        this.zzccv = z3;
    }

    private final void zza(zzajp<zztt> zzajp) {
        zzagr.zzczc.post(new zzty(this, zzajp));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        if (r4.hasNext() == false) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        r0 = r4.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r1 = (com.google.android.gms.internal.zztt) r0.get();
        r3.zzcdp.add(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        if (r1 == null) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002f, code lost:
        if (r1.zzcdc != 0) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0031, code lost:
        zza(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0034, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0035, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0036, code lost:
        com.google.android.gms.internal.zzafj.zzc("Exception while processing an adapter; continuing with other adapters", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003c, code lost:
        zza((com.google.android.gms.internal.zzajp<com.google.android.gms.internal.zztt>) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0046, code lost:
        return new com.google.android.gms.internal.zztt(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        r4 = r4.iterator();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.internal.zztt zzh(java.util.List<com.google.android.gms.internal.zzajp<com.google.android.gms.internal.zztt>> r4) {
        /*
            r3 = this;
            java.lang.Object r0 = r3.mLock
            monitor-enter(r0)
            boolean r1 = r3.zzcdm     // Catch:{ all -> 0x0047 }
            if (r1 == 0) goto L_0x000f
            com.google.android.gms.internal.zztt r4 = new com.google.android.gms.internal.zztt     // Catch:{ all -> 0x0047 }
            r1 = -1
            r4.<init>(r1)     // Catch:{ all -> 0x0047 }
            monitor-exit(r0)     // Catch:{ all -> 0x0047 }
            return r4
        L_0x000f:
            monitor-exit(r0)     // Catch:{ all -> 0x0047 }
            java.util.Iterator r4 = r4.iterator()
        L_0x0014:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x003c
            java.lang.Object r0 = r4.next()
            com.google.android.gms.internal.zzajp r0 = (com.google.android.gms.internal.zzajp) r0
            java.lang.Object r1 = r0.get()     // Catch:{ InterruptedException | ExecutionException -> 0x0035 }
            com.google.android.gms.internal.zztt r1 = (com.google.android.gms.internal.zztt) r1     // Catch:{ InterruptedException | ExecutionException -> 0x0035 }
            java.util.List<com.google.android.gms.internal.zztt> r2 = r3.zzcdp     // Catch:{ InterruptedException | ExecutionException -> 0x0035 }
            r2.add(r1)     // Catch:{ InterruptedException | ExecutionException -> 0x0035 }
            if (r1 == 0) goto L_0x0014
            int r2 = r1.zzcdc     // Catch:{ InterruptedException | ExecutionException -> 0x0035 }
            if (r2 != 0) goto L_0x0014
            r3.zza(r0)     // Catch:{ InterruptedException | ExecutionException -> 0x0035 }
            return r1
        L_0x0035:
            r0 = move-exception
            java.lang.String r1 = "Exception while processing an adapter; continuing with other adapters"
            com.google.android.gms.internal.zzafj.zzc(r1, r0)
            goto L_0x0014
        L_0x003c:
            r4 = 0
            r3.zza(r4)
            com.google.android.gms.internal.zztt r4 = new com.google.android.gms.internal.zztt
            r0 = 1
            r4.<init>(r0)
            return r4
        L_0x0047:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0047 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zztw.zzh(java.util.List):com.google.android.gms.internal.zztt");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        if (r14.zzccq.zzccf == -1) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001a, code lost:
        r0 = r14.zzccq.zzccf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        r0 = com.tapjoy.TapjoyConstants.TIMER_INCREMENT;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0021, code lost:
        r15 = r15.iterator();
        r3 = null;
        r4 = -1;
        r1 = r0;
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
        if (r15.hasNext() == false) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        r5 = r15.next();
        r6 = com.google.android.gms.ads.internal.zzbs.zzei().currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0041, code lost:
        if (r1 != 0) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0047, code lost:
        if (r5.isDone() == false) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0049, code lost:
        r10 = r5.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004d, code lost:
        r10 = (com.google.android.gms.internal.zztt) r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0050, code lost:
        r15 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0054, code lost:
        r10 = r5.get(r1, java.util.concurrent.TimeUnit.MILLISECONDS);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005b, code lost:
        r14.zzcdp.add(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0060, code lost:
        if (r10 == null) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0064, code lost:
        if (r10.zzcdc != 0) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0066, code lost:
        r11 = r10.zzcdh;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0068, code lost:
        if (r11 == null) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x006e, code lost:
        if (r11.zzlw() <= r4) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0074, code lost:
        r3 = r5;
        r0 = r10;
        r4 = r11.zzlw();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        com.google.android.gms.internal.zzafj.zzc("Exception while processing an adapter; continuing with other adapters", r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x008e, code lost:
        java.lang.Math.max(r1 - (com.google.android.gms.ads.internal.zzbs.zzei().currentTimeMillis() - r6), 0L);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x009d, code lost:
        throw r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x009e, code lost:
        zza(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a1, code lost:
        if (r0 != null) goto L_0x00aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00a9, code lost:
        return new com.google.android.gms.internal.zztt(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00aa, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.internal.zztt zzi(java.util.List<com.google.android.gms.internal.zzajp<com.google.android.gms.internal.zztt>> r15) {
        /*
            r14 = this;
            java.lang.Object r0 = r14.mLock
            monitor-enter(r0)
            boolean r1 = r14.zzcdm     // Catch:{ all -> 0x00ab }
            r2 = -1
            if (r1 == 0) goto L_0x000f
            com.google.android.gms.internal.zztt r15 = new com.google.android.gms.internal.zztt     // Catch:{ all -> 0x00ab }
            r15.<init>(r2)     // Catch:{ all -> 0x00ab }
            monitor-exit(r0)     // Catch:{ all -> 0x00ab }
            return r15
        L_0x000f:
            monitor-exit(r0)     // Catch:{ all -> 0x00ab }
            com.google.android.gms.internal.zztn r0 = r14.zzccq
            long r0 = r0.zzccf
            r3 = -1
            int r5 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x001f
            com.google.android.gms.internal.zztn r0 = r14.zzccq
            long r0 = r0.zzccf
            goto L_0x0021
        L_0x001f:
            r0 = 10000(0x2710, double:4.9407E-320)
        L_0x0021:
            java.util.Iterator r15 = r15.iterator()
            r3 = 0
            r4 = r2
            r1 = r0
            r0 = r3
        L_0x0029:
            boolean r5 = r15.hasNext()
            if (r5 == 0) goto L_0x009e
            java.lang.Object r5 = r15.next()
            com.google.android.gms.internal.zzajp r5 = (com.google.android.gms.internal.zzajp) r5
            com.google.android.gms.common.util.zzd r6 = com.google.android.gms.ads.internal.zzbs.zzei()
            long r6 = r6.currentTimeMillis()
            r8 = 0
            int r10 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
            if (r10 != 0) goto L_0x0054
            boolean r10 = r5.isDone()     // Catch:{ RemoteException | InterruptedException | ExecutionException | TimeoutException -> 0x0052 }
            if (r10 == 0) goto L_0x0054
            java.lang.Object r10 = r5.get()     // Catch:{ RemoteException | InterruptedException | ExecutionException | TimeoutException -> 0x0052 }
        L_0x004d:
            com.google.android.gms.internal.zztt r10 = (com.google.android.gms.internal.zztt) r10     // Catch:{ RemoteException | InterruptedException | ExecutionException | TimeoutException -> 0x0052 }
            goto L_0x005b
        L_0x0050:
            r15 = move-exception
            goto L_0x008e
        L_0x0052:
            r5 = move-exception
            goto L_0x0078
        L_0x0054:
            java.util.concurrent.TimeUnit r10 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ RemoteException | InterruptedException | ExecutionException | TimeoutException -> 0x0052 }
            java.lang.Object r10 = r5.get(r1, r10)     // Catch:{ RemoteException | InterruptedException | ExecutionException | TimeoutException -> 0x0052 }
            goto L_0x004d
        L_0x005b:
            java.util.List<com.google.android.gms.internal.zztt> r11 = r14.zzcdp     // Catch:{ RemoteException | InterruptedException | ExecutionException | TimeoutException -> 0x0052 }
            r11.add(r10)     // Catch:{ RemoteException | InterruptedException | ExecutionException | TimeoutException -> 0x0052 }
            if (r10 == 0) goto L_0x007d
            int r11 = r10.zzcdc     // Catch:{ RemoteException | InterruptedException | ExecutionException | TimeoutException -> 0x0052 }
            if (r11 != 0) goto L_0x007d
            com.google.android.gms.internal.zzul r11 = r10.zzcdh     // Catch:{ RemoteException | InterruptedException | ExecutionException | TimeoutException -> 0x0052 }
            if (r11 == 0) goto L_0x007d
            int r12 = r11.zzlw()     // Catch:{ RemoteException | InterruptedException | ExecutionException | TimeoutException -> 0x0052 }
            if (r12 <= r4) goto L_0x007d
            int r11 = r11.zzlw()     // Catch:{ RemoteException | InterruptedException | ExecutionException | TimeoutException -> 0x0052 }
            r3 = r5
            r0 = r10
            r4 = r11
            goto L_0x007d
        L_0x0078:
            java.lang.String r10 = "Exception while processing an adapter; continuing with other adapters"
            com.google.android.gms.internal.zzafj.zzc(r10, r5)     // Catch:{ all -> 0x0050 }
        L_0x007d:
            com.google.android.gms.common.util.zzd r5 = com.google.android.gms.ads.internal.zzbs.zzei()
            long r10 = r5.currentTimeMillis()
            long r12 = r10 - r6
            long r5 = r1 - r12
            long r1 = java.lang.Math.max(r5, r8)
            goto L_0x0029
        L_0x008e:
            com.google.android.gms.common.util.zzd r0 = com.google.android.gms.ads.internal.zzbs.zzei()
            long r3 = r0.currentTimeMillis()
            long r10 = r3 - r6
            long r3 = r1 - r10
            java.lang.Math.max(r3, r8)
            throw r15
        L_0x009e:
            r14.zza(r3)
            if (r0 != 0) goto L_0x00aa
            com.google.android.gms.internal.zztt r15 = new com.google.android.gms.internal.zztt
            r0 = 1
            r15.<init>(r0)
            return r15
        L_0x00aa:
            return r0
        L_0x00ab:
            r15 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00ab }
            throw r15
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zztw.zzi(java.util.List):com.google.android.gms.internal.zztt");
    }

    public final void cancel() {
        synchronized (this.mLock) {
            this.zzcdm = true;
            for (zztq cancel : this.zzcdn.values()) {
                cancel.cancel();
            }
        }
    }

    public final zztt zzg(List<zztm> list) {
        zzafj.zzbw("Starting mediation.");
        ExecutorService newCachedThreadPool = Executors.newCachedThreadPool();
        ArrayList arrayList = new ArrayList();
        zziw zziw = this.zzcdj.zzath;
        int[] iArr = new int[2];
        if (zziw.zzbdc != null) {
            zzbs.zzew();
            if (zztv.zza(this.zzcdo, iArr)) {
                int i = 0;
                int i2 = iArr[0];
                int i3 = iArr[1];
                zziw[] zziwArr = zziw.zzbdc;
                int length = zziwArr.length;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    zziw zziw2 = zziwArr[i];
                    if (i2 == zziw2.width && i3 == zziw2.height) {
                        zziw = zziw2;
                        break;
                    }
                    i++;
                }
            }
        }
        Iterator<zztm> it = list.iterator();
        while (it.hasNext()) {
            zztm next = it.next();
            String valueOf = String.valueOf(next.zzcbc);
            zzafj.zzcn(valueOf.length() != 0 ? "Trying mediation network: ".concat(valueOf) : new String("Trying mediation network: "));
            Iterator<String> it2 = next.zzcbd.iterator();
            while (it2.hasNext()) {
                Context context = this.mContext;
                zzuc zzuc = this.zzanb;
                zztn zztn = this.zzccq;
                zzis zzis = this.zzcdj.zzclo;
                zzaiy zzaiy = this.zzcdj.zzatd;
                boolean z = this.zzauu;
                boolean z2 = this.zzccu;
                Iterator<zztm> it3 = it;
                zzom zzom = this.zzcdj.zzatt;
                Iterator<String> it4 = it2;
                ArrayList arrayList2 = arrayList;
                zzaiy zzaiy2 = zzaiy;
                zztq zztq = new zztq(context, it2.next(), zzuc, zztn, next, zzis, zziw, zzaiy2, z, z2, zzom, this.zzcdj.zzaub, this.zzcdj.zzcmd, this.zzcdj.zzcmy, this.zzccv);
                zzajp zza = zzagl.zza(newCachedThreadPool, new zztx(this, zztq));
                this.zzcdn.put(zza, zztq);
                ArrayList arrayList3 = arrayList2;
                arrayList3.add(zza);
                arrayList = arrayList3;
                it = it3;
                it2 = it4;
            }
        }
        ArrayList arrayList4 = arrayList;
        return this.zzcdl != 2 ? zzh(arrayList4) : zzi(arrayList4);
    }

    public final List<zztt> zzlo() {
        return this.zzcdp;
    }
}
