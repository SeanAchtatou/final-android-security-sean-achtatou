package com.google.android.gms.games;

import android.support.annotation.NonNull;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.internal.zzo;

final class zzax implements zzo<Players.LoadPlayersResult> {
    zzax() {
    }

    public final /* synthetic */ void release(@NonNull Object obj) {
        Players.LoadPlayersResult loadPlayersResult = (Players.LoadPlayersResult) obj;
        if (loadPlayersResult.getPlayers() != null) {
            loadPlayersResult.getPlayers().release();
        }
    }
}
