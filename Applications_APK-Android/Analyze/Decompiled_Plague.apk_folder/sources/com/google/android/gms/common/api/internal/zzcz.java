package com.google.android.gms.common.api.internal;

import com.google.android.gms.internal.zzcwo;

final class zzcz implements Runnable {
    private /* synthetic */ zzcwo zzfoz;
    private /* synthetic */ zzcy zzfsb;

    zzcz(zzcy zzcy, zzcwo zzcwo) {
        this.zzfsb = zzcy;
        this.zzfoz = zzcwo;
    }

    public final void run() {
        this.zzfsb.zzc(this.zzfoz);
    }
}
