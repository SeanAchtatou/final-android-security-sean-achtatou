package com.google.android.gms.games.snapshot;

import android.graphics.Bitmap;
import android.net.Uri;
import com.google.android.gms.common.data.BitmapTeleporter;

public interface SnapshotMetadataChange {
    public static final SnapshotMetadataChange EMPTY_CHANGE = new zze();

    public static final class Builder {
        private String zzdou;
        private Long zzhxc;
        private Long zzhxd;
        private BitmapTeleporter zzhxe;
        private Uri zzhxf;

        public final SnapshotMetadataChange build() {
            return new zze(this.zzdou, this.zzhxc, this.zzhxe, this.zzhxf, this.zzhxd);
        }

        public final Builder fromMetadata(SnapshotMetadata snapshotMetadata) {
            this.zzdou = snapshotMetadata.getDescription();
            this.zzhxc = Long.valueOf(snapshotMetadata.getPlayedTime());
            this.zzhxd = Long.valueOf(snapshotMetadata.getProgressValue());
            if (this.zzhxc.longValue() == -1) {
                this.zzhxc = null;
            }
            this.zzhxf = snapshotMetadata.getCoverImageUri();
            if (this.zzhxf != null) {
                this.zzhxe = null;
            }
            return this;
        }

        public final Builder setCoverImage(Bitmap bitmap) {
            this.zzhxe = new BitmapTeleporter(bitmap);
            this.zzhxf = null;
            return this;
        }

        public final Builder setDescription(String str) {
            this.zzdou = str;
            return this;
        }

        public final Builder setPlayedTimeMillis(long j) {
            this.zzhxc = Long.valueOf(j);
            return this;
        }

        public final Builder setProgressValue(long j) {
            this.zzhxd = Long.valueOf(j);
            return this;
        }
    }

    Bitmap getCoverImage();

    String getDescription();

    Long getPlayedTimeMillis();

    Long getProgressValue();

    BitmapTeleporter zzaua();
}
