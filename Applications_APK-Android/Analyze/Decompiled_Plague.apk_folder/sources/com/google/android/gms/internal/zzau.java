package com.google.android.gms.internal;

import java.io.IOException;

public final class zzau extends zzfhe<zzau> {
    public String zzcq;
    private String zzcr;
    private String zzcs;
    private String zzct;
    private String zzcu;

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 10) {
                this.zzcq = zzfhb.readString();
            } else if (zzcts == 18) {
                this.zzcr = zzfhb.readString();
            } else if (zzcts == 26) {
                this.zzcs = zzfhb.readString();
            } else if (zzcts == 34) {
                this.zzct = zzfhb.readString();
            } else if (zzcts == 42) {
                this.zzcu = zzfhb.readString();
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzcq != null) {
            zzfhc.zzn(1, this.zzcq);
        }
        if (this.zzcr != null) {
            zzfhc.zzn(2, this.zzcr);
        }
        if (this.zzcs != null) {
            zzfhc.zzn(3, this.zzcs);
        }
        if (this.zzct != null) {
            zzfhc.zzn(4, this.zzct);
        }
        if (this.zzcu != null) {
            zzfhc.zzn(5, this.zzcu);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzcq != null) {
            zzo += zzfhc.zzo(1, this.zzcq);
        }
        if (this.zzcr != null) {
            zzo += zzfhc.zzo(2, this.zzcr);
        }
        if (this.zzcs != null) {
            zzo += zzfhc.zzo(3, this.zzcs);
        }
        if (this.zzct != null) {
            zzo += zzfhc.zzo(4, this.zzct);
        }
        return this.zzcu != null ? zzo + zzfhc.zzo(5, this.zzcu) : zzo;
    }
}
