package com.google.android.gms.games.snapshot;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbek;

public final class zzc implements Parcelable.Creator<SnapshotEntity> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        SnapshotMetadataEntity snapshotMetadataEntity = null;
        zza zza = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            int i = 65535 & readInt;
            if (i == 1) {
                snapshotMetadataEntity = (SnapshotMetadataEntity) zzbek.zza(parcel, readInt, SnapshotMetadataEntity.CREATOR);
            } else if (i != 3) {
                zzbek.zzb(parcel, readInt);
            } else {
                zza = (zza) zzbek.zza(parcel, readInt, zza.CREATOR);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new SnapshotEntity(snapshotMetadataEntity, zza);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new SnapshotEntity[i];
    }
}
