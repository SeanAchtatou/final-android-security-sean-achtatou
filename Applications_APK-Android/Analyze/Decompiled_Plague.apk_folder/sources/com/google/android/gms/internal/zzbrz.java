package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zzbrz extends zzbrs<DriveContents> {
    public zzbrz(TaskCompletionSource<DriveContents> taskCompletionSource) {
        super(taskCompletionSource);
    }

    public final void zza(zzbps zzbps) throws RemoteException {
        zzaox().setResult(new zzblv(zzbps.zzaot()));
    }
}
