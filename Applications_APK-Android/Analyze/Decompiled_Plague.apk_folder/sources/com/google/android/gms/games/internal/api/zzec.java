package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.video.CaptureState;
import com.google.android.gms.games.video.Videos;

final class zzec implements Videos.CaptureStateResult {
    private /* synthetic */ Status zzekv;

    zzec(zzeb zzeb, Status status) {
        this.zzekv = status;
    }

    public final CaptureState getCaptureState() {
        return null;
    }

    public final Status getStatus() {
        return this.zzekv;
    }
}
