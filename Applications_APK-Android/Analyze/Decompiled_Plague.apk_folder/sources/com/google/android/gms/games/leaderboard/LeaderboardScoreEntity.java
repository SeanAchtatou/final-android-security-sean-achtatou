package com.google.android.gms.games.leaderboard;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import java.util.Arrays;

public final class LeaderboardScoreEntity implements LeaderboardScore {
    private final long zzhtp;
    private final String zzhtq;
    private final String zzhtr;
    private final long zzhts;
    private final long zzhtt;
    private final String zzhtu;
    private final Uri zzhtv;
    private final Uri zzhtw;
    private final PlayerEntity zzhtx;
    private final String zzhty;
    private final String zzhtz;
    private final String zzhua;

    public LeaderboardScoreEntity(LeaderboardScore leaderboardScore) {
        this.zzhtp = leaderboardScore.getRank();
        this.zzhtq = (String) zzbq.checkNotNull(leaderboardScore.getDisplayRank());
        this.zzhtr = (String) zzbq.checkNotNull(leaderboardScore.getDisplayScore());
        this.zzhts = leaderboardScore.getRawScore();
        this.zzhtt = leaderboardScore.getTimestampMillis();
        this.zzhtu = leaderboardScore.getScoreHolderDisplayName();
        this.zzhtv = leaderboardScore.getScoreHolderIconImageUri();
        this.zzhtw = leaderboardScore.getScoreHolderHiResImageUri();
        Player scoreHolder = leaderboardScore.getScoreHolder();
        this.zzhtx = scoreHolder == null ? null : (PlayerEntity) scoreHolder.freeze();
        this.zzhty = leaderboardScore.getScoreTag();
        this.zzhtz = leaderboardScore.getScoreHolderIconImageUrl();
        this.zzhua = leaderboardScore.getScoreHolderHiResImageUrl();
    }

    static int zza(LeaderboardScore leaderboardScore) {
        return Arrays.hashCode(new Object[]{Long.valueOf(leaderboardScore.getRank()), leaderboardScore.getDisplayRank(), Long.valueOf(leaderboardScore.getRawScore()), leaderboardScore.getDisplayScore(), Long.valueOf(leaderboardScore.getTimestampMillis()), leaderboardScore.getScoreHolderDisplayName(), leaderboardScore.getScoreHolderIconImageUri(), leaderboardScore.getScoreHolderHiResImageUri(), leaderboardScore.getScoreHolder()});
    }

    static boolean zza(LeaderboardScore leaderboardScore, Object obj) {
        if (!(obj instanceof LeaderboardScore)) {
            return false;
        }
        if (leaderboardScore == obj) {
            return true;
        }
        LeaderboardScore leaderboardScore2 = (LeaderboardScore) obj;
        return zzbg.equal(Long.valueOf(leaderboardScore2.getRank()), Long.valueOf(leaderboardScore.getRank())) && zzbg.equal(leaderboardScore2.getDisplayRank(), leaderboardScore.getDisplayRank()) && zzbg.equal(Long.valueOf(leaderboardScore2.getRawScore()), Long.valueOf(leaderboardScore.getRawScore())) && zzbg.equal(leaderboardScore2.getDisplayScore(), leaderboardScore.getDisplayScore()) && zzbg.equal(Long.valueOf(leaderboardScore2.getTimestampMillis()), Long.valueOf(leaderboardScore.getTimestampMillis())) && zzbg.equal(leaderboardScore2.getScoreHolderDisplayName(), leaderboardScore.getScoreHolderDisplayName()) && zzbg.equal(leaderboardScore2.getScoreHolderIconImageUri(), leaderboardScore.getScoreHolderIconImageUri()) && zzbg.equal(leaderboardScore2.getScoreHolderHiResImageUri(), leaderboardScore.getScoreHolderHiResImageUri()) && zzbg.equal(leaderboardScore2.getScoreHolder(), leaderboardScore.getScoreHolder()) && zzbg.equal(leaderboardScore2.getScoreTag(), leaderboardScore.getScoreTag());
    }

    static String zzb(LeaderboardScore leaderboardScore) {
        return zzbg.zzw(leaderboardScore).zzg("Rank", Long.valueOf(leaderboardScore.getRank())).zzg("DisplayRank", leaderboardScore.getDisplayRank()).zzg("Score", Long.valueOf(leaderboardScore.getRawScore())).zzg("DisplayScore", leaderboardScore.getDisplayScore()).zzg("Timestamp", Long.valueOf(leaderboardScore.getTimestampMillis())).zzg("DisplayName", leaderboardScore.getScoreHolderDisplayName()).zzg("IconImageUri", leaderboardScore.getScoreHolderIconImageUri()).zzg("IconImageUrl", leaderboardScore.getScoreHolderIconImageUrl()).zzg("HiResImageUri", leaderboardScore.getScoreHolderHiResImageUri()).zzg("HiResImageUrl", leaderboardScore.getScoreHolderHiResImageUrl()).zzg("Player", leaderboardScore.getScoreHolder() == null ? null : leaderboardScore.getScoreHolder()).zzg("ScoreTag", leaderboardScore.getScoreTag()).toString();
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    public final /* bridge */ /* synthetic */ Object freeze() {
        if (this != null) {
            return this;
        }
        throw null;
    }

    public final String getDisplayRank() {
        return this.zzhtq;
    }

    public final void getDisplayRank(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.zzhtq, charArrayBuffer);
    }

    public final String getDisplayScore() {
        return this.zzhtr;
    }

    public final void getDisplayScore(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.zzhtr, charArrayBuffer);
    }

    public final long getRank() {
        return this.zzhtp;
    }

    public final long getRawScore() {
        return this.zzhts;
    }

    public final Player getScoreHolder() {
        return this.zzhtx;
    }

    public final String getScoreHolderDisplayName() {
        return this.zzhtx == null ? this.zzhtu : this.zzhtx.getDisplayName();
    }

    public final void getScoreHolderDisplayName(CharArrayBuffer charArrayBuffer) {
        if (this.zzhtx == null) {
            zzg.zzb(this.zzhtu, charArrayBuffer);
        } else {
            this.zzhtx.getDisplayName(charArrayBuffer);
        }
    }

    public final Uri getScoreHolderHiResImageUri() {
        return this.zzhtx == null ? this.zzhtw : this.zzhtx.getHiResImageUri();
    }

    public final String getScoreHolderHiResImageUrl() {
        return this.zzhtx == null ? this.zzhua : this.zzhtx.getHiResImageUrl();
    }

    public final Uri getScoreHolderIconImageUri() {
        return this.zzhtx == null ? this.zzhtv : this.zzhtx.getIconImageUri();
    }

    public final String getScoreHolderIconImageUrl() {
        return this.zzhtx == null ? this.zzhtz : this.zzhtx.getIconImageUrl();
    }

    public final String getScoreTag() {
        return this.zzhty;
    }

    public final long getTimestampMillis() {
        return this.zzhtt;
    }

    public final int hashCode() {
        return zza(this);
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String toString() {
        return zzb(this);
    }
}
