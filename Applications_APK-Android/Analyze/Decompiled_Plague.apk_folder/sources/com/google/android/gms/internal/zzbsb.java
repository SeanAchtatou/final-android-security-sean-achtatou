package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbsb extends zzfhe<zzbsb> {
    public long sequenceNumber = -1;
    public int versionCode = 1;
    public long zzgox = -1;
    public long zzgoy = -1;

    public zzbsb() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzbsb)) {
            return false;
        }
        zzbsb zzbsb = (zzbsb) obj;
        if (this.versionCode == zzbsb.versionCode && this.sequenceNumber == zzbsb.sequenceNumber && this.zzgox == zzbsb.zzgox && this.zzgoy == zzbsb.zzgoy) {
            return (this.zzpgy == null || this.zzpgy.isEmpty()) ? zzbsb.zzpgy == null || zzbsb.zzpgy.isEmpty() : this.zzpgy.equals(zzbsb.zzpgy);
        }
        return false;
    }

    public final int hashCode() {
        return ((((((((((527 + getClass().getName().hashCode()) * 31) + this.versionCode) * 31) + ((int) (this.sequenceNumber ^ (this.sequenceNumber >>> 32)))) * 31) + ((int) (this.zzgox ^ (this.zzgox >>> 32)))) * 31) + ((int) (this.zzgoy ^ (this.zzgoy >>> 32)))) * 31) + ((this.zzpgy == null || this.zzpgy.isEmpty()) ? 0 : this.zzpgy.hashCode());
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 8) {
                this.versionCode = zzfhb.zzcuh();
            } else if (zzcts == 16) {
                long zzcum = zzfhb.zzcum();
                this.sequenceNumber = (zzcum >>> 1) ^ (-(zzcum & 1));
            } else if (zzcts == 24) {
                long zzcum2 = zzfhb.zzcum();
                this.zzgox = (zzcum2 >>> 1) ^ (-(zzcum2 & 1));
            } else if (zzcts == 32) {
                long zzcum3 = zzfhb.zzcum();
                this.zzgoy = (zzcum3 >>> 1) ^ (-(zzcum3 & 1));
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        zzfhc.zzaa(1, this.versionCode);
        zzfhc.zzg(2, this.sequenceNumber);
        zzfhc.zzg(3, this.zzgox);
        zzfhc.zzg(4, this.zzgoy);
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        return super.zzo() + zzfhc.zzad(1, this.versionCode) + zzfhc.zzh(2, this.sequenceNumber) + zzfhc.zzh(3, this.zzgox) + zzfhc.zzh(4, this.zzgoy);
    }
}
