package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzb extends zzk {
    private /* synthetic */ boolean zzhpv;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzb(zza zza, GoogleApiClient googleApiClient, boolean z) {
        super(googleApiClient, null);
        this.zzhpv = z;
    }

    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zzc(this, this.zzhpv);
    }
}
