package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.games.internal.zzw;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.List;

final class zzbb extends zzac<Void> {
    private /* synthetic */ byte[] zzhkf;
    private /* synthetic */ String zzhkg;
    private /* synthetic */ List zzhki;

    zzbb(RealTimeMultiplayerClient realTimeMultiplayerClient, List list, byte[] bArr, String str) {
        this.zzhki = list;
        this.zzhkf = bArr;
        this.zzhkg = str;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException {
        zzbq.checkNotNull(this.zzhki, "Participant IDs must not be null");
        if (((zzw) gamesClientImpl.zzakb()).zzb(this.zzhkf, this.zzhkg, (String[]) this.zzhki.toArray(new String[this.zzhki.size()])) == 0) {
            taskCompletionSource.setResult(null);
        } else {
            taskCompletionSource.trySetException(zzb.zzy(GamesClientStatusCodes.zzdg(GamesClientStatusCodes.REAL_TIME_MESSAGE_SEND_FAILED)));
        }
    }
}
