package com.google.android.gms.internal;

import android.support.v4.view.MotionEventCompat;
import com.millennialmedia.InterstitialAd;
import com.millennialmedia.internal.AdPlacementReporter;
import com.miniclip.input.MCInput;
import com.mopub.mobileads.resource.DrawableConstants;
import java.io.IOException;

public final class zzaw extends zzfhe<zzaw> {
    public String zzcp = null;
    public String zzcq = null;
    public String zzcs = null;
    public String zzct = null;
    public String zzcu = null;
    public String zzcv = null;
    public Long zzcw = null;
    private Long zzcx = null;
    public Long zzcy = null;
    public Long zzcz = null;
    private Long zzda = null;
    private Long zzdb = null;
    private Long zzdc = null;
    private Long zzdd = null;
    private Long zzde = null;
    public Long zzdf = null;
    private String zzdg = null;
    public Long zzdh = null;
    public Long zzdi = null;
    public Long zzdj = null;
    public Long zzdk = null;
    private Long zzdl = null;
    private Long zzdm = null;
    public Long zzdn = null;
    public Long zzdo = null;
    public Long zzdp = null;
    public String zzdq = null;
    public Long zzdr = null;
    public Long zzds = null;
    public Long zzdt = null;
    public Long zzdu = null;
    public Long zzdv = null;
    public Long zzdw = null;
    private zzaz zzdx = null;
    public Long zzdy = null;
    public Long zzdz = null;
    public Long zzea = null;
    public Long zzeb = null;
    public Long zzec = null;
    public Long zzed = null;
    public Integer zzee;
    public Integer zzef;
    public Long zzeg = null;
    public Long zzeh = null;
    private Long zzei = null;
    private Long zzej = null;
    private Long zzek = null;
    public Integer zzel;
    public zzax zzem = null;
    public zzax[] zzen = zzax.zzp();
    public zzay zzeo = null;
    private Long zzep = null;
    public Long zzeq = null;
    public Long zzer = null;
    public Long zzes = null;
    private Long zzet = null;
    private Long zzeu = null;
    private Long zzev = null;
    public String zzew = null;
    public Integer zzex;
    public Boolean zzey = null;
    private String zzez = null;
    public Long zzfa = null;
    public zzbb zzfb = null;

    public zzaw() {
        this.zzpai = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzb */
    public final zzaw zza(zzfhb zzfhb) throws IOException {
        int i;
        zzfhk zzfhk;
        while (true) {
            int zzcts = zzfhb.zzcts();
            switch (zzcts) {
                case 0:
                    return this;
                case 10:
                    this.zzcv = zzfhb.readString();
                    break;
                case 18:
                    this.zzcp = zzfhb.readString();
                    break;
                case 24:
                    this.zzcw = Long.valueOf(zzfhb.zzcum());
                    break;
                case 32:
                    this.zzcx = Long.valueOf(zzfhb.zzcum());
                    break;
                case MotionEventCompat.AXIS_GENERIC_9:
                    this.zzcy = Long.valueOf(zzfhb.zzcum());
                    break;
                case 48:
                    this.zzcz = Long.valueOf(zzfhb.zzcum());
                    break;
                case DrawableConstants.CloseButton.WIDGET_HEIGHT_DIPS /*56*/:
                    this.zzda = Long.valueOf(zzfhb.zzcum());
                    break;
                case 64:
                    this.zzdb = Long.valueOf(zzfhb.zzcum());
                    break;
                case DrawableConstants.GradientStrip.GRADIENT_STRIP_HEIGHT_DIPS /*72*/:
                    this.zzdc = Long.valueOf(zzfhb.zzcum());
                    break;
                case 80:
                    this.zzdd = Long.valueOf(zzfhb.zzcum());
                    break;
                case 88:
                    this.zzde = Long.valueOf(zzfhb.zzcum());
                    break;
                case MCInput.KEYCODE_BUTTON_A /*96*/:
                    this.zzdf = Long.valueOf(zzfhb.zzcum());
                    break;
                case MCInput.KEYCODE_BUTTON_THUMBL /*106*/:
                    this.zzdg = zzfhb.readString();
                    break;
                case AdPlacementReporter.PLAYLIST_LOADED_FROM_CACHE_DEMAND_ITEM_FAILED /*112*/:
                    this.zzdh = Long.valueOf(zzfhb.zzcum());
                    break;
                case 120:
                    this.zzdi = Long.valueOf(zzfhb.zzcum());
                    break;
                case 128:
                    this.zzdj = Long.valueOf(zzfhb.zzcum());
                    break;
                case 136:
                    this.zzdk = Long.valueOf(zzfhb.zzcum());
                    break;
                case 144:
                    this.zzdl = Long.valueOf(zzfhb.zzcum());
                    break;
                case 152:
                    this.zzdm = Long.valueOf(zzfhb.zzcum());
                    break;
                case 160:
                    this.zzdn = Long.valueOf(zzfhb.zzcum());
                    break;
                case 168:
                    this.zzev = Long.valueOf(zzfhb.zzcum());
                    break;
                case 176:
                    this.zzdo = Long.valueOf(zzfhb.zzcum());
                    break;
                case 184:
                    this.zzdp = Long.valueOf(zzfhb.zzcum());
                    break;
                case 194:
                    this.zzew = zzfhb.readString();
                    break;
                case 200:
                    this.zzfa = Long.valueOf(zzfhb.zzcum());
                    break;
                case 208:
                    i = zzfhb.getPosition();
                    int zzcuh = zzfhb.zzcuh();
                    switch (zzcuh) {
                        default:
                            StringBuilder sb = new StringBuilder(44);
                            sb.append(zzcuh);
                            sb.append(" is not a valid enum DeviceIdType");
                            throw new IllegalArgumentException(sb.toString());
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                            this.zzex = Integer.valueOf(zzcuh);
                            continue;
                    }
                case 218:
                    this.zzcq = zzfhb.readString();
                    break;
                case 224:
                    this.zzey = Boolean.valueOf(zzfhb.zzcty());
                    break;
                case 234:
                    this.zzdq = zzfhb.readString();
                    break;
                case 242:
                    this.zzez = zzfhb.readString();
                    break;
                case 248:
                    this.zzdr = Long.valueOf(zzfhb.zzcum());
                    break;
                case 256:
                    this.zzds = Long.valueOf(zzfhb.zzcum());
                    break;
                case 264:
                    this.zzdt = Long.valueOf(zzfhb.zzcum());
                    break;
                case 274:
                    this.zzcs = zzfhb.readString();
                    break;
                case 280:
                    this.zzdu = Long.valueOf(zzfhb.zzcum());
                    break;
                case 288:
                    this.zzdv = Long.valueOf(zzfhb.zzcum());
                    break;
                case 296:
                    this.zzdw = Long.valueOf(zzfhb.zzcum());
                    break;
                case 306:
                    if (this.zzdx == null) {
                        this.zzdx = new zzaz();
                    }
                    zzfhk = this.zzdx;
                    zzfhb.zza(zzfhk);
                    break;
                case 312:
                    this.zzdy = Long.valueOf(zzfhb.zzcum());
                    break;
                case 320:
                    this.zzdz = Long.valueOf(zzfhb.zzcum());
                    break;
                case 328:
                    this.zzea = Long.valueOf(zzfhb.zzcum());
                    break;
                case 336:
                    this.zzeb = Long.valueOf(zzfhb.zzcum());
                    break;
                case 346:
                    int zzb = zzfhn.zzb(zzfhb, 346);
                    int length = this.zzen == null ? 0 : this.zzen.length;
                    zzax[] zzaxArr = new zzax[(zzb + length)];
                    if (length != 0) {
                        System.arraycopy(this.zzen, 0, zzaxArr, 0, length);
                    }
                    while (length < zzaxArr.length - 1) {
                        zzaxArr[length] = new zzax();
                        zzfhb.zza(zzaxArr[length]);
                        zzfhb.zzcts();
                        length++;
                    }
                    zzaxArr[length] = new zzax();
                    zzfhb.zza(zzaxArr[length]);
                    this.zzen = zzaxArr;
                    break;
                case 352:
                    this.zzec = Long.valueOf(zzfhb.zzcum());
                    break;
                case 360:
                    this.zzed = Long.valueOf(zzfhb.zzcum());
                    break;
                case 370:
                    this.zzct = zzfhb.readString();
                    break;
                case 378:
                    this.zzcu = zzfhb.readString();
                    break;
                case 384:
                    i = zzfhb.getPosition();
                    this.zzee = Integer.valueOf(zzav.zzd(zzfhb.zzcuh()));
                    break;
                case 392:
                    i = zzfhb.getPosition();
                    this.zzef = Integer.valueOf(zzav.zzd(zzfhb.zzcuh()));
                    break;
                case 402:
                    if (this.zzem == null) {
                        this.zzem = new zzax();
                    }
                    zzfhk = this.zzem;
                    zzfhb.zza(zzfhk);
                    break;
                case 408:
                    this.zzeg = Long.valueOf(zzfhb.zzcum());
                    break;
                case 416:
                    this.zzeh = Long.valueOf(zzfhb.zzcum());
                    break;
                case 424:
                    this.zzei = Long.valueOf(zzfhb.zzcum());
                    break;
                case 432:
                    this.zzej = Long.valueOf(zzfhb.zzcum());
                    break;
                case 440:
                    this.zzek = Long.valueOf(zzfhb.zzcum());
                    break;
                case 448:
                    i = zzfhb.getPosition();
                    try {
                        this.zzel = Integer.valueOf(zzav.zzd(zzfhb.zzcuh()));
                        break;
                    } catch (IllegalArgumentException unused) {
                        zzfhb.zzlv(i);
                        zza(zzfhb, zzcts);
                        break;
                    }
                case 458:
                    if (this.zzeo == null) {
                        this.zzeo = new zzay();
                    }
                    zzfhk = this.zzeo;
                    zzfhb.zza(zzfhk);
                    break;
                case 464:
                    this.zzep = Long.valueOf(zzfhb.zzcum());
                    break;
                case 472:
                    this.zzeq = Long.valueOf(zzfhb.zzcum());
                    break;
                case 480:
                    this.zzer = Long.valueOf(zzfhb.zzcum());
                    break;
                case 488:
                    this.zzes = Long.valueOf(zzfhb.zzcum());
                    break;
                case 496:
                    this.zzet = Long.valueOf(zzfhb.zzcum());
                    break;
                case 504:
                    this.zzeu = Long.valueOf(zzfhb.zzcum());
                    break;
                case 1610:
                    if (this.zzfb == null) {
                        this.zzfb = new zzbb();
                    }
                    zzfhk = this.zzfb;
                    zzfhb.zza(zzfhk);
                    break;
                default:
                    if (super.zza(zzfhb, zzcts)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzcv != null) {
            zzfhc.zzn(1, this.zzcv);
        }
        if (this.zzcp != null) {
            zzfhc.zzn(2, this.zzcp);
        }
        if (this.zzcw != null) {
            zzfhc.zzf(3, this.zzcw.longValue());
        }
        if (this.zzcx != null) {
            zzfhc.zzf(4, this.zzcx.longValue());
        }
        if (this.zzcy != null) {
            zzfhc.zzf(5, this.zzcy.longValue());
        }
        if (this.zzcz != null) {
            zzfhc.zzf(6, this.zzcz.longValue());
        }
        if (this.zzda != null) {
            zzfhc.zzf(7, this.zzda.longValue());
        }
        if (this.zzdb != null) {
            zzfhc.zzf(8, this.zzdb.longValue());
        }
        if (this.zzdc != null) {
            zzfhc.zzf(9, this.zzdc.longValue());
        }
        if (this.zzdd != null) {
            zzfhc.zzf(10, this.zzdd.longValue());
        }
        if (this.zzde != null) {
            zzfhc.zzf(11, this.zzde.longValue());
        }
        if (this.zzdf != null) {
            zzfhc.zzf(12, this.zzdf.longValue());
        }
        if (this.zzdg != null) {
            zzfhc.zzn(13, this.zzdg);
        }
        if (this.zzdh != null) {
            zzfhc.zzf(14, this.zzdh.longValue());
        }
        if (this.zzdi != null) {
            zzfhc.zzf(15, this.zzdi.longValue());
        }
        if (this.zzdj != null) {
            zzfhc.zzf(16, this.zzdj.longValue());
        }
        if (this.zzdk != null) {
            zzfhc.zzf(17, this.zzdk.longValue());
        }
        if (this.zzdl != null) {
            zzfhc.zzf(18, this.zzdl.longValue());
        }
        if (this.zzdm != null) {
            zzfhc.zzf(19, this.zzdm.longValue());
        }
        if (this.zzdn != null) {
            zzfhc.zzf(20, this.zzdn.longValue());
        }
        if (this.zzev != null) {
            zzfhc.zzf(21, this.zzev.longValue());
        }
        if (this.zzdo != null) {
            zzfhc.zzf(22, this.zzdo.longValue());
        }
        if (this.zzdp != null) {
            zzfhc.zzf(23, this.zzdp.longValue());
        }
        if (this.zzew != null) {
            zzfhc.zzn(24, this.zzew);
        }
        if (this.zzfa != null) {
            zzfhc.zzf(25, this.zzfa.longValue());
        }
        if (this.zzex != null) {
            zzfhc.zzaa(26, this.zzex.intValue());
        }
        if (this.zzcq != null) {
            zzfhc.zzn(27, this.zzcq);
        }
        if (this.zzey != null) {
            zzfhc.zzl(28, this.zzey.booleanValue());
        }
        if (this.zzdq != null) {
            zzfhc.zzn(29, this.zzdq);
        }
        if (this.zzez != null) {
            zzfhc.zzn(30, this.zzez);
        }
        if (this.zzdr != null) {
            zzfhc.zzf(31, this.zzdr.longValue());
        }
        if (this.zzds != null) {
            zzfhc.zzf(32, this.zzds.longValue());
        }
        if (this.zzdt != null) {
            zzfhc.zzf(33, this.zzdt.longValue());
        }
        if (this.zzcs != null) {
            zzfhc.zzn(34, this.zzcs);
        }
        if (this.zzdu != null) {
            zzfhc.zzf(35, this.zzdu.longValue());
        }
        if (this.zzdv != null) {
            zzfhc.zzf(36, this.zzdv.longValue());
        }
        if (this.zzdw != null) {
            zzfhc.zzf(37, this.zzdw.longValue());
        }
        if (this.zzdx != null) {
            zzfhc.zza(38, this.zzdx);
        }
        if (this.zzdy != null) {
            zzfhc.zzf(39, this.zzdy.longValue());
        }
        if (this.zzdz != null) {
            zzfhc.zzf(40, this.zzdz.longValue());
        }
        if (this.zzea != null) {
            zzfhc.zzf(41, this.zzea.longValue());
        }
        if (this.zzeb != null) {
            zzfhc.zzf(42, this.zzeb.longValue());
        }
        if (this.zzen != null && this.zzen.length > 0) {
            for (zzax zzax : this.zzen) {
                if (zzax != null) {
                    zzfhc.zza(43, zzax);
                }
            }
        }
        if (this.zzec != null) {
            zzfhc.zzf(44, this.zzec.longValue());
        }
        if (this.zzed != null) {
            zzfhc.zzf(45, this.zzed.longValue());
        }
        if (this.zzct != null) {
            zzfhc.zzn(46, this.zzct);
        }
        if (this.zzcu != null) {
            zzfhc.zzn(47, this.zzcu);
        }
        if (this.zzee != null) {
            zzfhc.zzaa(48, this.zzee.intValue());
        }
        if (this.zzef != null) {
            zzfhc.zzaa(49, this.zzef.intValue());
        }
        if (this.zzem != null) {
            zzfhc.zza(50, this.zzem);
        }
        if (this.zzeg != null) {
            zzfhc.zzf(51, this.zzeg.longValue());
        }
        if (this.zzeh != null) {
            zzfhc.zzf(52, this.zzeh.longValue());
        }
        if (this.zzei != null) {
            zzfhc.zzf(53, this.zzei.longValue());
        }
        if (this.zzej != null) {
            zzfhc.zzf(54, this.zzej.longValue());
        }
        if (this.zzek != null) {
            zzfhc.zzf(55, this.zzek.longValue());
        }
        if (this.zzel != null) {
            zzfhc.zzaa(56, this.zzel.intValue());
        }
        if (this.zzeo != null) {
            zzfhc.zza(57, this.zzeo);
        }
        if (this.zzep != null) {
            zzfhc.zzf(58, this.zzep.longValue());
        }
        if (this.zzeq != null) {
            zzfhc.zzf(59, this.zzeq.longValue());
        }
        if (this.zzer != null) {
            zzfhc.zzf(60, this.zzer.longValue());
        }
        if (this.zzes != null) {
            zzfhc.zzf(61, this.zzes.longValue());
        }
        if (this.zzet != null) {
            zzfhc.zzf(62, this.zzet.longValue());
        }
        if (this.zzeu != null) {
            zzfhc.zzf(63, this.zzeu.longValue());
        }
        if (this.zzfb != null) {
            zzfhc.zza((int) InterstitialAd.InterstitialErrorStatus.EXPIRED, this.zzfb);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzcv != null) {
            zzo += zzfhc.zzo(1, this.zzcv);
        }
        if (this.zzcp != null) {
            zzo += zzfhc.zzo(2, this.zzcp);
        }
        if (this.zzcw != null) {
            zzo += zzfhc.zzc(3, this.zzcw.longValue());
        }
        if (this.zzcx != null) {
            zzo += zzfhc.zzc(4, this.zzcx.longValue());
        }
        if (this.zzcy != null) {
            zzo += zzfhc.zzc(5, this.zzcy.longValue());
        }
        if (this.zzcz != null) {
            zzo += zzfhc.zzc(6, this.zzcz.longValue());
        }
        if (this.zzda != null) {
            zzo += zzfhc.zzc(7, this.zzda.longValue());
        }
        if (this.zzdb != null) {
            zzo += zzfhc.zzc(8, this.zzdb.longValue());
        }
        if (this.zzdc != null) {
            zzo += zzfhc.zzc(9, this.zzdc.longValue());
        }
        if (this.zzdd != null) {
            zzo += zzfhc.zzc(10, this.zzdd.longValue());
        }
        if (this.zzde != null) {
            zzo += zzfhc.zzc(11, this.zzde.longValue());
        }
        if (this.zzdf != null) {
            zzo += zzfhc.zzc(12, this.zzdf.longValue());
        }
        if (this.zzdg != null) {
            zzo += zzfhc.zzo(13, this.zzdg);
        }
        if (this.zzdh != null) {
            zzo += zzfhc.zzc(14, this.zzdh.longValue());
        }
        if (this.zzdi != null) {
            zzo += zzfhc.zzc(15, this.zzdi.longValue());
        }
        if (this.zzdj != null) {
            zzo += zzfhc.zzc(16, this.zzdj.longValue());
        }
        if (this.zzdk != null) {
            zzo += zzfhc.zzc(17, this.zzdk.longValue());
        }
        if (this.zzdl != null) {
            zzo += zzfhc.zzc(18, this.zzdl.longValue());
        }
        if (this.zzdm != null) {
            zzo += zzfhc.zzc(19, this.zzdm.longValue());
        }
        if (this.zzdn != null) {
            zzo += zzfhc.zzc(20, this.zzdn.longValue());
        }
        if (this.zzev != null) {
            zzo += zzfhc.zzc(21, this.zzev.longValue());
        }
        if (this.zzdo != null) {
            zzo += zzfhc.zzc(22, this.zzdo.longValue());
        }
        if (this.zzdp != null) {
            zzo += zzfhc.zzc(23, this.zzdp.longValue());
        }
        if (this.zzew != null) {
            zzo += zzfhc.zzo(24, this.zzew);
        }
        if (this.zzfa != null) {
            zzo += zzfhc.zzc(25, this.zzfa.longValue());
        }
        if (this.zzex != null) {
            zzo += zzfhc.zzad(26, this.zzex.intValue());
        }
        if (this.zzcq != null) {
            zzo += zzfhc.zzo(27, this.zzcq);
        }
        if (this.zzey != null) {
            this.zzey.booleanValue();
            zzo += zzfhc.zzkw(28) + 1;
        }
        if (this.zzdq != null) {
            zzo += zzfhc.zzo(29, this.zzdq);
        }
        if (this.zzez != null) {
            zzo += zzfhc.zzo(30, this.zzez);
        }
        if (this.zzdr != null) {
            zzo += zzfhc.zzc(31, this.zzdr.longValue());
        }
        if (this.zzds != null) {
            zzo += zzfhc.zzc(32, this.zzds.longValue());
        }
        if (this.zzdt != null) {
            zzo += zzfhc.zzc(33, this.zzdt.longValue());
        }
        if (this.zzcs != null) {
            zzo += zzfhc.zzo(34, this.zzcs);
        }
        if (this.zzdu != null) {
            zzo += zzfhc.zzc(35, this.zzdu.longValue());
        }
        if (this.zzdv != null) {
            zzo += zzfhc.zzc(36, this.zzdv.longValue());
        }
        if (this.zzdw != null) {
            zzo += zzfhc.zzc(37, this.zzdw.longValue());
        }
        if (this.zzdx != null) {
            zzo += zzfhc.zzb(38, this.zzdx);
        }
        if (this.zzdy != null) {
            zzo += zzfhc.zzc(39, this.zzdy.longValue());
        }
        if (this.zzdz != null) {
            zzo += zzfhc.zzc(40, this.zzdz.longValue());
        }
        if (this.zzea != null) {
            zzo += zzfhc.zzc(41, this.zzea.longValue());
        }
        if (this.zzeb != null) {
            zzo += zzfhc.zzc(42, this.zzeb.longValue());
        }
        if (this.zzen != null && this.zzen.length > 0) {
            for (zzax zzax : this.zzen) {
                if (zzax != null) {
                    zzo += zzfhc.zzb(43, zzax);
                }
            }
        }
        if (this.zzec != null) {
            zzo += zzfhc.zzc(44, this.zzec.longValue());
        }
        if (this.zzed != null) {
            zzo += zzfhc.zzc(45, this.zzed.longValue());
        }
        if (this.zzct != null) {
            zzo += zzfhc.zzo(46, this.zzct);
        }
        if (this.zzcu != null) {
            zzo += zzfhc.zzo(47, this.zzcu);
        }
        if (this.zzee != null) {
            zzo += zzfhc.zzad(48, this.zzee.intValue());
        }
        if (this.zzef != null) {
            zzo += zzfhc.zzad(49, this.zzef.intValue());
        }
        if (this.zzem != null) {
            zzo += zzfhc.zzb(50, this.zzem);
        }
        if (this.zzeg != null) {
            zzo += zzfhc.zzc(51, this.zzeg.longValue());
        }
        if (this.zzeh != null) {
            zzo += zzfhc.zzc(52, this.zzeh.longValue());
        }
        if (this.zzei != null) {
            zzo += zzfhc.zzc(53, this.zzei.longValue());
        }
        if (this.zzej != null) {
            zzo += zzfhc.zzc(54, this.zzej.longValue());
        }
        if (this.zzek != null) {
            zzo += zzfhc.zzc(55, this.zzek.longValue());
        }
        if (this.zzel != null) {
            zzo += zzfhc.zzad(56, this.zzel.intValue());
        }
        if (this.zzeo != null) {
            zzo += zzfhc.zzb(57, this.zzeo);
        }
        if (this.zzep != null) {
            zzo += zzfhc.zzc(58, this.zzep.longValue());
        }
        if (this.zzeq != null) {
            zzo += zzfhc.zzc(59, this.zzeq.longValue());
        }
        if (this.zzer != null) {
            zzo += zzfhc.zzc(60, this.zzer.longValue());
        }
        if (this.zzes != null) {
            zzo += zzfhc.zzc(61, this.zzes.longValue());
        }
        if (this.zzet != null) {
            zzo += zzfhc.zzc(62, this.zzet.longValue());
        }
        if (this.zzeu != null) {
            zzo += zzfhc.zzc(63, this.zzeu.longValue());
        }
        return this.zzfb != null ? zzo + zzfhc.zzb((int) InterstitialAd.InterstitialErrorStatus.EXPIRED, this.zzfb) : zzo;
    }
}
