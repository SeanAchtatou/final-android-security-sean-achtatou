package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.zze;

final class zzas implements Runnable {
    private /* synthetic */ zzar zzfor;

    zzas(zzar zzar) {
        this.zzfor = zzar;
    }

    public final void run() {
        zze.zzcc(this.zzfor.mContext);
    }
}
