package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.internal.api.zzac;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzct extends zzac<Void> {
    private /* synthetic */ String zzhlu;

    zzct(TurnBasedMultiplayerClient turnBasedMultiplayerClient, String str) {
        this.zzhlu = str;
    }

    /* access modifiers changed from: protected */
    public final void zza(GamesClientImpl gamesClientImpl, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException {
        gamesClientImpl.zzhk(this.zzhlu);
        taskCompletionSource.setResult(null);
    }
}
