package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.zzbek;

public final class zzb implements Parcelable.Creator<VideoConfiguration> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        int i = 0;
        int i2 = 0;
        boolean z = false;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbek.zzg(parcel, readInt);
                    break;
                case 2:
                    i2 = zzbek.zzg(parcel, readInt);
                    break;
                case 3:
                    str = zzbek.zzq(parcel, readInt);
                    break;
                case 4:
                    str2 = zzbek.zzq(parcel, readInt);
                    break;
                case 5:
                    str3 = zzbek.zzq(parcel, readInt);
                    break;
                case 6:
                    str4 = zzbek.zzq(parcel, readInt);
                    break;
                case 7:
                    z = zzbek.zzc(parcel, readInt);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new VideoConfiguration(i, i2, str, str2, str3, str4, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new VideoConfiguration[i];
    }
}
