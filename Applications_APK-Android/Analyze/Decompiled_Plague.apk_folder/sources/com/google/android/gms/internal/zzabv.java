package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.internal.zzbs;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.common.util.zzq;
import java.util.Locale;

public final class zzabv {
    private float zzaxd;
    private int zzclx;
    private int zzcly;
    private int zzcrt;
    private boolean zzcru;
    private boolean zzcrv;
    private String zzcrw;
    private String zzcrx;
    private boolean zzcry;
    private boolean zzcrz;
    private boolean zzcsa;
    private boolean zzcsb;
    private String zzcsc;
    private String zzcsd;
    private String zzcse;
    private int zzcsf;
    private int zzcsg;
    private int zzcsh;
    private int zzcsi;
    private int zzcsj;
    private int zzcsk;
    private double zzcsl;
    private boolean zzcsm;
    private boolean zzcsn;
    private int zzcso;
    private String zzcsp;
    private String zzcsq;
    private boolean zzcsr;

    public zzabv(Context context) {
        DisplayMetrics displayMetrics;
        PackageManager packageManager = context.getPackageManager();
        zzm(context);
        zzn(context);
        zzo(context);
        Locale locale = Locale.getDefault();
        boolean z = false;
        this.zzcru = zza(packageManager, "geo:0,0?q=donuts") != null;
        this.zzcrv = zza(packageManager, "http://www.google.com") != null ? true : z;
        this.zzcrx = locale.getCountry();
        zzjk.zzhx();
        this.zzcry = zzais.zzqs();
        this.zzcrz = zzi.zzcr(context);
        this.zzcsc = locale.getLanguage();
        this.zzcsd = zzb(context, packageManager);
        this.zzcse = zza(context, packageManager);
        Resources resources = context.getResources();
        if (resources != null && (displayMetrics = resources.getDisplayMetrics()) != null) {
            this.zzaxd = displayMetrics.density;
            this.zzclx = displayMetrics.widthPixels;
            this.zzcly = displayMetrics.heightPixels;
        }
    }

    public zzabv(Context context, zzabu zzabu) {
        context.getPackageManager();
        zzm(context);
        zzn(context);
        zzo(context);
        this.zzcsp = Build.FINGERPRINT;
        this.zzcsq = Build.DEVICE;
        this.zzcsr = zzq.zzalv() && zznn.zzi(context);
        this.zzcru = zzabu.zzcru;
        this.zzcrv = zzabu.zzcrv;
        this.zzcrx = zzabu.zzcrx;
        this.zzcry = zzabu.zzcry;
        this.zzcrz = zzabu.zzcrz;
        this.zzcsc = zzabu.zzcsc;
        this.zzcsd = zzabu.zzcsd;
        this.zzcse = zzabu.zzcse;
        this.zzaxd = zzabu.zzaxd;
        this.zzclx = zzabu.zzclx;
        this.zzcly = zzabu.zzcly;
    }

    private static ResolveInfo zza(PackageManager packageManager, String str) {
        try {
            return packageManager.resolveActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)), 65536);
        } catch (Throwable th) {
            zzbs.zzeg().zza(th, "DeviceInfo.getResolveInfo");
            return null;
        }
    }

    private static String zza(Context context, PackageManager packageManager) {
        try {
            PackageInfo packageInfo = zzbgc.zzcy(context).getPackageInfo("com.android.vending", 128);
            if (packageInfo != null) {
                int i = packageInfo.versionCode;
                String str = packageInfo.packageName;
                StringBuilder sb = new StringBuilder(12 + String.valueOf(str).length());
                sb.append(i);
                sb.append(".");
                sb.append(str);
                return sb.toString();
            }
        } catch (Exception unused) {
        }
        return null;
    }

    private static String zzb(Context context, PackageManager packageManager) {
        ActivityInfo activityInfo;
        ResolveInfo zza = zza(packageManager, "market://details?id=com.google.android.gms.ads");
        if (zza == null || (activityInfo = zza.activityInfo) == null) {
            return null;
        }
        try {
            PackageInfo packageInfo = zzbgc.zzcy(context).getPackageInfo(activityInfo.packageName, 0);
            if (packageInfo != null) {
                int i = packageInfo.versionCode;
                String str = activityInfo.packageName;
                StringBuilder sb = new StringBuilder(12 + String.valueOf(str).length());
                sb.append(i);
                sb.append(".");
                sb.append(str);
                return sb.toString();
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return null;
    }

    private final void zzm(Context context) {
        zzbs.zzec();
        AudioManager zzao = zzagr.zzao(context);
        if (zzao != null) {
            try {
                this.zzcrt = zzao.getMode();
                this.zzcsa = zzao.isMusicActive();
                this.zzcsb = zzao.isSpeakerphoneOn();
                this.zzcsf = zzao.getStreamVolume(3);
                this.zzcsj = zzao.getRingerMode();
                this.zzcsk = zzao.getStreamVolume(2);
                return;
            } catch (Throwable th) {
                zzbs.zzeg().zza(th, "DeviceInfo.gatherAudioInfo");
            }
        }
        this.zzcrt = -2;
        this.zzcsa = false;
        this.zzcsb = false;
        this.zzcsf = 0;
        this.zzcsj = 0;
        this.zzcsk = 0;
    }

    @TargetApi(16)
    private final void zzn(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        this.zzcrw = telephonyManager.getNetworkOperator();
        this.zzcsh = telephonyManager.getNetworkType();
        this.zzcsi = telephonyManager.getPhoneType();
        this.zzcsg = -2;
        this.zzcsn = false;
        this.zzcso = -1;
        zzbs.zzec();
        if (zzagr.zzd(context, context.getPackageName(), "android.permission.ACCESS_NETWORK_STATE")) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                this.zzcsg = activeNetworkInfo.getType();
                this.zzcso = activeNetworkInfo.getDetailedState().ordinal();
            } else {
                this.zzcsg = -1;
            }
            if (Build.VERSION.SDK_INT >= 16) {
                this.zzcsn = connectivityManager.isActiveNetworkMetered();
            }
        }
    }

    private final void zzo(Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        boolean z = false;
        if (registerReceiver != null) {
            int intExtra = registerReceiver.getIntExtra("status", -1);
            this.zzcsl = (double) (((float) registerReceiver.getIntExtra("level", -1)) / ((float) registerReceiver.getIntExtra("scale", -1)));
            if (intExtra == 2 || intExtra == 5) {
                z = true;
            }
            this.zzcsm = z;
            return;
        }
        this.zzcsl = -1.0d;
        this.zzcsm = false;
    }

    public final zzabu zzns() {
        int i = this.zzcrt;
        boolean z = this.zzcru;
        boolean z2 = this.zzcrv;
        String str = this.zzcrw;
        String str2 = this.zzcrx;
        boolean z3 = this.zzcry;
        boolean z4 = this.zzcrz;
        boolean z5 = this.zzcsa;
        boolean z6 = this.zzcsb;
        String str3 = this.zzcsc;
        String str4 = this.zzcsd;
        String str5 = this.zzcse;
        int i2 = this.zzcsf;
        int i3 = this.zzcsg;
        int i4 = this.zzcsh;
        int i5 = i3;
        int i6 = this.zzcsi;
        int i7 = this.zzcsj;
        int i8 = this.zzcsk;
        float f = this.zzaxd;
        int i9 = this.zzclx;
        int i10 = i2;
        int i11 = this.zzcly;
        double d = this.zzcsl;
        boolean z7 = this.zzcsm;
        boolean z8 = this.zzcsn;
        boolean z9 = z7;
        boolean z10 = z8;
        return new zzabu(i, z, z2, str, str2, z3, z4, z5, z6, str3, str4, str5, i10, i5, i4, i6, i7, i8, f, i9, i11, d, z9, z10, this.zzcso, this.zzcsp, this.zzcsr, this.zzcsq);
    }
}
