package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdrr;
import com.google.android.gms.internal.zzdrt;
import java.security.GeneralSecurityException;

public final class zzdot {
    private final zzdrr zzlpf;
    private final zzdra zzlpg = null;

    zzdot(zzdrr zzdrr) {
        this.zzlpf = zzdrr;
    }

    public static void zza(zzdrr zzdrr) throws GeneralSecurityException {
        if (zzdrr == null || zzdrr.zzboc() <= 0) {
            throw new GeneralSecurityException("empty keyset");
        }
    }

    public final String toString() {
        zzdrr zzdrr = this.zzlpf;
        zzdrt.zza zzfu = zzdrt.zzbol().zzfu(zzdrr.zzboa());
        for (zzdrr.zzb next : zzdrr.zzbob()) {
            zzfu.zzb((zzdrt.zzb) zzdrt.zzb.zzboo().zzob(next.zzbof().zzbns()).zzb(next.zzbog()).zzb(next.zzboi()).zzfw(next.zzboh()).zzcvk());
        }
        return ((zzdrt) zzfu.zzcvk()).toString();
    }

    public final zzdrr zzble() {
        return this.zzlpf;
    }
}
