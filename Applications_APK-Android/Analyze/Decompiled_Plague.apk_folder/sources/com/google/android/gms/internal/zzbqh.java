package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;

public final class zzbqh implements Parcelable.Creator<zzbqg> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        DataHolder dataHolder = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                zzbek.zzb(parcel, readInt);
            } else {
                dataHolder = (DataHolder) zzbek.zza(parcel, readInt, DataHolder.CREATOR);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbqg(dataHolder);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbqg[i];
    }
}
