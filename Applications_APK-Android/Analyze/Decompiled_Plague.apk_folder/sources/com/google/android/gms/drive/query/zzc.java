package com.google.android.gms.drive.query;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.query.internal.zzf;
import com.google.android.gms.internal.zzbek;
import java.util.ArrayList;

public final class zzc implements Parcelable.Creator<SortOrder> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        ArrayList arrayList = null;
        boolean z = false;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    arrayList = zzbek.zzc(parcel, readInt, zzf.CREATOR);
                    break;
                case 2:
                    z = zzbek.zzc(parcel, readInt);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new SortOrder(arrayList, z);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new SortOrder[i];
    }
}
