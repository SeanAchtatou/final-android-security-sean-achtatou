package com.google.android.gms.dynamite;

import android.content.Context;
import android.database.Cursor;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.zzn;
import java.lang.reflect.Field;

public final class DynamiteModule {
    private static Boolean zzgtw;
    private static zzk zzgtx;
    private static zzm zzgty;
    private static String zzgtz;
    private static final ThreadLocal<zza> zzgua = new ThreadLocal<>();
    private static final zzi zzgub = new zza();
    public static final zzd zzguc = new zzb();
    private static zzd zzgud = new zzc();
    public static final zzd zzgue = new zzd();
    public static final zzd zzguf = new zze();
    public static final zzd zzgug = new zzf();
    public static final zzd zzguh = new zzg();
    private final Context zzgui;

    @DynamiteApi
    public static class DynamiteLoaderClassLoader {
        public static ClassLoader sClassLoader;
    }

    static class zza {
        public Cursor zzguj;

        private zza() {
        }

        /* synthetic */ zza(zza zza) {
            this();
        }
    }

    static class zzb implements zzi {
        private final int zzguk;
        private final int zzgul = 0;

        public zzb(int i, int i2) {
            this.zzguk = i;
        }

        public final int zzab(Context context, String str) {
            return this.zzguk;
        }

        public final int zzc(Context context, String str, boolean z) {
            return 0;
        }
    }

    public static class zzc extends Exception {
        private zzc(String str) {
            super(str);
        }

        /* synthetic */ zzc(String str, zza zza) {
            this(str);
        }

        private zzc(String str, Throwable th) {
            super(str, th);
        }

        /* synthetic */ zzc(String str, Throwable th, zza zza) {
            this(str, th);
        }
    }

    public interface zzd {
        zzj zza(Context context, String str, zzi zzi) throws zzc;
    }

    private DynamiteModule(Context context) {
        this.zzgui = (Context) zzbq.checkNotNull(context);
    }

    private static Context zza(Context context, String str, int i, Cursor cursor, zzm zzm) {
        try {
            return (Context) zzn.zzx(zzm.zza(zzn.zzy(context), str, i, zzn.zzy(cursor)));
        } catch (Exception e) {
            String valueOf = String.valueOf(e.toString());
            Log.e("DynamiteModule", valueOf.length() != 0 ? "Failed to load DynamiteLoader: ".concat(valueOf) : new String("Failed to load DynamiteLoader: "));
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0081, code lost:
        if (r1.zzguj != null) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00e2, code lost:
        if (r1.zzguj != null) goto L_0x0083;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.dynamite.DynamiteModule zza(android.content.Context r10, com.google.android.gms.dynamite.DynamiteModule.zzd r11, java.lang.String r12) throws com.google.android.gms.dynamite.DynamiteModule.zzc {
        /*
            java.lang.ThreadLocal<com.google.android.gms.dynamite.DynamiteModule$zza> r0 = com.google.android.gms.dynamite.DynamiteModule.zzgua
            java.lang.Object r0 = r0.get()
            com.google.android.gms.dynamite.DynamiteModule$zza r0 = (com.google.android.gms.dynamite.DynamiteModule.zza) r0
            com.google.android.gms.dynamite.DynamiteModule$zza r1 = new com.google.android.gms.dynamite.DynamiteModule$zza
            r2 = 0
            r1.<init>(r2)
            java.lang.ThreadLocal<com.google.android.gms.dynamite.DynamiteModule$zza> r3 = com.google.android.gms.dynamite.DynamiteModule.zzgua
            r3.set(r1)
            com.google.android.gms.dynamite.zzi r3 = com.google.android.gms.dynamite.DynamiteModule.zzgub     // Catch:{ all -> 0x0132 }
            com.google.android.gms.dynamite.zzj r3 = r11.zza(r10, r12, r3)     // Catch:{ all -> 0x0132 }
            java.lang.String r4 = "DynamiteModule"
            int r5 = r3.zzgum     // Catch:{ all -> 0x0132 }
            int r6 = r3.zzgun     // Catch:{ all -> 0x0132 }
            r7 = 68
            java.lang.String r8 = java.lang.String.valueOf(r12)     // Catch:{ all -> 0x0132 }
            int r8 = r8.length()     // Catch:{ all -> 0x0132 }
            int r7 = r7 + r8
            java.lang.String r8 = java.lang.String.valueOf(r12)     // Catch:{ all -> 0x0132 }
            int r8 = r8.length()     // Catch:{ all -> 0x0132 }
            int r7 = r7 + r8
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0132 }
            r8.<init>(r7)     // Catch:{ all -> 0x0132 }
            java.lang.String r7 = "Considering local module "
            r8.append(r7)     // Catch:{ all -> 0x0132 }
            r8.append(r12)     // Catch:{ all -> 0x0132 }
            java.lang.String r7 = ":"
            r8.append(r7)     // Catch:{ all -> 0x0132 }
            r8.append(r5)     // Catch:{ all -> 0x0132 }
            java.lang.String r5 = " and remote module "
            r8.append(r5)     // Catch:{ all -> 0x0132 }
            r8.append(r12)     // Catch:{ all -> 0x0132 }
            java.lang.String r5 = ":"
            r8.append(r5)     // Catch:{ all -> 0x0132 }
            r8.append(r6)     // Catch:{ all -> 0x0132 }
            java.lang.String r5 = r8.toString()     // Catch:{ all -> 0x0132 }
            android.util.Log.i(r4, r5)     // Catch:{ all -> 0x0132 }
            int r4 = r3.zzguo     // Catch:{ all -> 0x0132 }
            if (r4 == 0) goto L_0x0108
            int r4 = r3.zzguo     // Catch:{ all -> 0x0132 }
            r5 = -1
            if (r4 != r5) goto L_0x006c
            int r4 = r3.zzgum     // Catch:{ all -> 0x0132 }
            if (r4 == 0) goto L_0x0108
        L_0x006c:
            int r4 = r3.zzguo     // Catch:{ all -> 0x0132 }
            r6 = 1
            if (r4 != r6) goto L_0x0077
            int r4 = r3.zzgun     // Catch:{ all -> 0x0132 }
            if (r4 != 0) goto L_0x0077
            goto L_0x0108
        L_0x0077:
            int r4 = r3.zzguo     // Catch:{ all -> 0x0132 }
            if (r4 != r5) goto L_0x008e
            com.google.android.gms.dynamite.DynamiteModule r10 = zzad(r10, r12)     // Catch:{ all -> 0x0132 }
            android.database.Cursor r11 = r1.zzguj
            if (r11 == 0) goto L_0x0088
        L_0x0083:
            android.database.Cursor r11 = r1.zzguj
            r11.close()
        L_0x0088:
            java.lang.ThreadLocal<com.google.android.gms.dynamite.DynamiteModule$zza> r11 = com.google.android.gms.dynamite.DynamiteModule.zzgua
            r11.set(r0)
            return r10
        L_0x008e:
            int r4 = r3.zzguo     // Catch:{ all -> 0x0132 }
            if (r4 != r6) goto L_0x00ed
            int r4 = r3.zzgun     // Catch:{ zzc -> 0x00a7 }
            com.google.android.gms.dynamite.DynamiteModule r4 = zza(r10, r12, r4)     // Catch:{ zzc -> 0x00a7 }
            android.database.Cursor r10 = r1.zzguj
            if (r10 == 0) goto L_0x00a1
            android.database.Cursor r10 = r1.zzguj
            r10.close()
        L_0x00a1:
            java.lang.ThreadLocal<com.google.android.gms.dynamite.DynamiteModule$zza> r10 = com.google.android.gms.dynamite.DynamiteModule.zzgua
            r10.set(r0)
            return r4
        L_0x00a7:
            r4 = move-exception
            java.lang.String r6 = "DynamiteModule"
            java.lang.String r7 = "Failed to load remote module: "
            java.lang.String r8 = r4.getMessage()     // Catch:{ all -> 0x0132 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ all -> 0x0132 }
            int r9 = r8.length()     // Catch:{ all -> 0x0132 }
            if (r9 == 0) goto L_0x00bf
            java.lang.String r7 = r7.concat(r8)     // Catch:{ all -> 0x0132 }
            goto L_0x00c5
        L_0x00bf:
            java.lang.String r8 = new java.lang.String     // Catch:{ all -> 0x0132 }
            r8.<init>(r7)     // Catch:{ all -> 0x0132 }
            r7 = r8
        L_0x00c5:
            android.util.Log.w(r6, r7)     // Catch:{ all -> 0x0132 }
            int r6 = r3.zzgum     // Catch:{ all -> 0x0132 }
            if (r6 == 0) goto L_0x00e5
            com.google.android.gms.dynamite.DynamiteModule$zzb r6 = new com.google.android.gms.dynamite.DynamiteModule$zzb     // Catch:{ all -> 0x0132 }
            int r3 = r3.zzgum     // Catch:{ all -> 0x0132 }
            r7 = 0
            r6.<init>(r3, r7)     // Catch:{ all -> 0x0132 }
            com.google.android.gms.dynamite.zzj r11 = r11.zza(r10, r12, r6)     // Catch:{ all -> 0x0132 }
            int r11 = r11.zzguo     // Catch:{ all -> 0x0132 }
            if (r11 != r5) goto L_0x00e5
            com.google.android.gms.dynamite.DynamiteModule r10 = zzad(r10, r12)     // Catch:{ all -> 0x0132 }
            android.database.Cursor r11 = r1.zzguj
            if (r11 == 0) goto L_0x0088
            goto L_0x0083
        L_0x00e5:
            com.google.android.gms.dynamite.DynamiteModule$zzc r10 = new com.google.android.gms.dynamite.DynamiteModule$zzc     // Catch:{ all -> 0x0132 }
            java.lang.String r11 = "Remote load failed. No local fallback found."
            r10.<init>(r11, r4, r2)     // Catch:{ all -> 0x0132 }
            throw r10     // Catch:{ all -> 0x0132 }
        L_0x00ed:
            com.google.android.gms.dynamite.DynamiteModule$zzc r10 = new com.google.android.gms.dynamite.DynamiteModule$zzc     // Catch:{ all -> 0x0132 }
            int r11 = r3.zzguo     // Catch:{ all -> 0x0132 }
            r12 = 47
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0132 }
            r3.<init>(r12)     // Catch:{ all -> 0x0132 }
            java.lang.String r12 = "VersionPolicy returned invalid code:"
            r3.append(r12)     // Catch:{ all -> 0x0132 }
            r3.append(r11)     // Catch:{ all -> 0x0132 }
            java.lang.String r11 = r3.toString()     // Catch:{ all -> 0x0132 }
            r10.<init>(r11, r2)     // Catch:{ all -> 0x0132 }
            throw r10     // Catch:{ all -> 0x0132 }
        L_0x0108:
            com.google.android.gms.dynamite.DynamiteModule$zzc r10 = new com.google.android.gms.dynamite.DynamiteModule$zzc     // Catch:{ all -> 0x0132 }
            int r11 = r3.zzgum     // Catch:{ all -> 0x0132 }
            int r12 = r3.zzgun     // Catch:{ all -> 0x0132 }
            r3 = 91
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0132 }
            r4.<init>(r3)     // Catch:{ all -> 0x0132 }
            java.lang.String r3 = "No acceptable module found. Local version is "
            r4.append(r3)     // Catch:{ all -> 0x0132 }
            r4.append(r11)     // Catch:{ all -> 0x0132 }
            java.lang.String r11 = " and remote version is "
            r4.append(r11)     // Catch:{ all -> 0x0132 }
            r4.append(r12)     // Catch:{ all -> 0x0132 }
            java.lang.String r11 = "."
            r4.append(r11)     // Catch:{ all -> 0x0132 }
            java.lang.String r11 = r4.toString()     // Catch:{ all -> 0x0132 }
            r10.<init>(r11, r2)     // Catch:{ all -> 0x0132 }
            throw r10     // Catch:{ all -> 0x0132 }
        L_0x0132:
            r10 = move-exception
            android.database.Cursor r11 = r1.zzguj
            if (r11 == 0) goto L_0x013c
            android.database.Cursor r11 = r1.zzguj
            r11.close()
        L_0x013c:
            java.lang.ThreadLocal<com.google.android.gms.dynamite.DynamiteModule$zza> r11 = com.google.android.gms.dynamite.DynamiteModule.zzgua
            r11.set(r0)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.zza(android.content.Context, com.google.android.gms.dynamite.DynamiteModule$zzd, java.lang.String):com.google.android.gms.dynamite.DynamiteModule");
    }

    private static DynamiteModule zza(Context context, String str, int i) throws zzc {
        Boolean bool;
        synchronized (DynamiteModule.class) {
            bool = zzgtw;
        }
        if (bool != null) {
            return bool.booleanValue() ? zzc(context, str, i) : zzb(context, str, i);
        }
        throw new zzc("Failed to determine which loading route to use.", (zza) null);
    }

    /* JADX WARN: Type inference failed for: r1v5, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void zza(java.lang.ClassLoader r3) throws com.google.android.gms.dynamite.DynamiteModule.zzc {
        /*
            r0 = 0
            java.lang.String r1 = "com.google.android.gms.dynamiteloader.DynamiteLoaderV2"
            java.lang.Class r3 = r3.loadClass(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException -> 0x0031 }
            r1 = 0
            java.lang.Class[] r2 = new java.lang.Class[r1]     // Catch:{ ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException -> 0x0031 }
            java.lang.reflect.Constructor r3 = r3.getConstructor(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException -> 0x0031 }
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException -> 0x0031 }
            java.lang.Object r3 = r3.newInstance(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException -> 0x0031 }
            android.os.IBinder r3 = (android.os.IBinder) r3     // Catch:{ ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException -> 0x0031 }
            if (r3 != 0) goto L_0x001a
            r3 = r0
            goto L_0x002e
        L_0x001a:
            java.lang.String r1 = "com.google.android.gms.dynamite.IDynamiteLoaderV2"
            android.os.IInterface r1 = r3.queryLocalInterface(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException -> 0x0031 }
            boolean r2 = r1 instanceof com.google.android.gms.dynamite.zzm     // Catch:{ ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException -> 0x0031 }
            if (r2 == 0) goto L_0x0028
            r3 = r1
            com.google.android.gms.dynamite.zzm r3 = (com.google.android.gms.dynamite.zzm) r3     // Catch:{ ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException -> 0x0031 }
            goto L_0x002e
        L_0x0028:
            com.google.android.gms.dynamite.zzn r1 = new com.google.android.gms.dynamite.zzn     // Catch:{ ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException -> 0x0031 }
            r1.<init>(r3)     // Catch:{ ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException -> 0x0031 }
            r3 = r1
        L_0x002e:
            com.google.android.gms.dynamite.DynamiteModule.zzgty = r3     // Catch:{ ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException -> 0x0031 }
            return
        L_0x0031:
            r3 = move-exception
            com.google.android.gms.dynamite.DynamiteModule$zzc r1 = new com.google.android.gms.dynamite.DynamiteModule$zzc
            java.lang.String r2 = "Failed to instantiate dynamite loader"
            r1.<init>(r2, r3, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.zza(java.lang.ClassLoader):void");
    }

    public static int zzab(Context context, String str) {
        try {
            ClassLoader classLoader = context.getApplicationContext().getClassLoader();
            StringBuilder sb = new StringBuilder(1 + String.valueOf("com.google.android.gms.dynamite.descriptors.").length() + String.valueOf(str).length() + String.valueOf("ModuleDescriptor").length());
            sb.append("com.google.android.gms.dynamite.descriptors.");
            sb.append(str);
            sb.append(".");
            sb.append("ModuleDescriptor");
            Class<?> loadClass = classLoader.loadClass(sb.toString());
            Field declaredField = loadClass.getDeclaredField("MODULE_ID");
            Field declaredField2 = loadClass.getDeclaredField("MODULE_VERSION");
            if (declaredField.get(null).equals(str)) {
                return declaredField2.getInt(null);
            }
            String valueOf = String.valueOf(declaredField.get(null));
            StringBuilder sb2 = new StringBuilder(51 + String.valueOf(valueOf).length() + String.valueOf(str).length());
            sb2.append("Module descriptor id '");
            sb2.append(valueOf);
            sb2.append("' didn't match expected id '");
            sb2.append(str);
            sb2.append("'");
            Log.e("DynamiteModule", sb2.toString());
            return 0;
        } catch (ClassNotFoundException unused) {
            StringBuilder sb3 = new StringBuilder(45 + String.valueOf(str).length());
            sb3.append("Local module descriptor class for ");
            sb3.append(str);
            sb3.append(" not found.");
            Log.w("DynamiteModule", sb3.toString());
            return 0;
        } catch (Exception e) {
            String valueOf2 = String.valueOf(e.getMessage());
            Log.e("DynamiteModule", valueOf2.length() != 0 ? "Failed to load module descriptor class: ".concat(valueOf2) : new String("Failed to load module descriptor class: "));
            return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.dynamite.DynamiteModule.zzc(android.content.Context, java.lang.String, boolean):int
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.google.android.gms.dynamite.DynamiteModule.zzc(android.content.Context, java.lang.String, int):com.google.android.gms.dynamite.DynamiteModule
      com.google.android.gms.dynamite.DynamiteModule.zzc(android.content.Context, java.lang.String, boolean):int */
    public static int zzac(Context context, String str) {
        return zzc(context, str, false);
    }

    private static DynamiteModule zzad(Context context, String str) {
        String valueOf = String.valueOf(str);
        Log.i("DynamiteModule", valueOf.length() != 0 ? "Selected local version of ".concat(valueOf) : new String("Selected local version of "));
        return new DynamiteModule(context.getApplicationContext());
    }

    private static DynamiteModule zzb(Context context, String str, int i) throws zzc {
        StringBuilder sb = new StringBuilder(51 + String.valueOf(str).length());
        sb.append("Selected remote version of ");
        sb.append(str);
        sb.append(", version >= ");
        sb.append(i);
        Log.i("DynamiteModule", sb.toString());
        zzk zzdc = zzdc(context);
        if (zzdc == null) {
            throw new zzc("Failed to create IDynamiteLoader.", (zza) null);
        }
        try {
            IObjectWrapper zza2 = zzdc.zza(zzn.zzy(context), str, i);
            if (zzn.zzx(zza2) != null) {
                return new DynamiteModule((Context) zzn.zzx(zza2));
            }
            throw new zzc("Failed to load remote module.", (zza) null);
        } catch (RemoteException e) {
            throw new zzc("Failed to load remote module.", e, null);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:39|40) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:15|16|17|18) */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r2.set(null, java.lang.ClassLoader.getSystemClassLoader());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0085, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0035 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x007c */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00bb A[SYNTHETIC, Splitter:B:55:0x00bb] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00e2  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:22:0x0050=Splitter:B:22:0x0050, B:17:0x0035=Splitter:B:17:0x0035, B:34:0x0079=Splitter:B:34:0x0079} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int zzc(android.content.Context r8, java.lang.String r9, boolean r10) {
        /*
            java.lang.Class<com.google.android.gms.dynamite.DynamiteModule> r0 = com.google.android.gms.dynamite.DynamiteModule.class
            monitor-enter(r0)
            java.lang.Boolean r1 = com.google.android.gms.dynamite.DynamiteModule.zzgtw     // Catch:{ all -> 0x00e7 }
            if (r1 != 0) goto L_0x00b4
            android.content.Context r1 = r8.getApplicationContext()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x008a }
            java.lang.ClassLoader r1 = r1.getClassLoader()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x008a }
            java.lang.Class<com.google.android.gms.dynamite.DynamiteModule$DynamiteLoaderClassLoader> r2 = com.google.android.gms.dynamite.DynamiteModule.DynamiteLoaderClassLoader.class
            java.lang.String r2 = r2.getName()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x008a }
            java.lang.Class r1 = r1.loadClass(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x008a }
            java.lang.String r2 = "sClassLoader"
            java.lang.reflect.Field r2 = r1.getDeclaredField(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x008a }
            monitor-enter(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x008a }
            r3 = 0
            java.lang.Object r4 = r2.get(r3)     // Catch:{ all -> 0x0087 }
            java.lang.ClassLoader r4 = (java.lang.ClassLoader) r4     // Catch:{ all -> 0x0087 }
            if (r4 == 0) goto L_0x0038
            java.lang.ClassLoader r2 = java.lang.ClassLoader.getSystemClassLoader()     // Catch:{ all -> 0x0087 }
            if (r4 != r2) goto L_0x0032
        L_0x002f:
            java.lang.Boolean r2 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x0087 }
            goto L_0x0084
        L_0x0032:
            zza(r4)     // Catch:{ zzc -> 0x0035 }
        L_0x0035:
            java.lang.Boolean r2 = java.lang.Boolean.TRUE     // Catch:{ all -> 0x0087 }
            goto L_0x0084
        L_0x0038:
            java.lang.String r4 = "com.google.android.gms"
            android.content.Context r5 = r8.getApplicationContext()     // Catch:{ all -> 0x0087 }
            java.lang.String r5 = r5.getPackageName()     // Catch:{ all -> 0x0087 }
            boolean r4 = r4.equals(r5)     // Catch:{ all -> 0x0087 }
            if (r4 == 0) goto L_0x0050
            java.lang.ClassLoader r4 = java.lang.ClassLoader.getSystemClassLoader()     // Catch:{ all -> 0x0087 }
            r2.set(r3, r4)     // Catch:{ all -> 0x0087 }
            goto L_0x002f
        L_0x0050:
            int r4 = zze(r8, r9, r10)     // Catch:{ zzc -> 0x007c }
            java.lang.String r5 = com.google.android.gms.dynamite.DynamiteModule.zzgtz     // Catch:{ zzc -> 0x007c }
            if (r5 == 0) goto L_0x0079
            java.lang.String r5 = com.google.android.gms.dynamite.DynamiteModule.zzgtz     // Catch:{ zzc -> 0x007c }
            boolean r5 = r5.isEmpty()     // Catch:{ zzc -> 0x007c }
            if (r5 == 0) goto L_0x0061
            goto L_0x0079
        L_0x0061:
            com.google.android.gms.dynamite.zzh r5 = new com.google.android.gms.dynamite.zzh     // Catch:{ zzc -> 0x007c }
            java.lang.String r6 = com.google.android.gms.dynamite.DynamiteModule.zzgtz     // Catch:{ zzc -> 0x007c }
            java.lang.ClassLoader r7 = java.lang.ClassLoader.getSystemClassLoader()     // Catch:{ zzc -> 0x007c }
            r5.<init>(r6, r7)     // Catch:{ zzc -> 0x007c }
            zza(r5)     // Catch:{ zzc -> 0x007c }
            r2.set(r3, r5)     // Catch:{ zzc -> 0x007c }
            java.lang.Boolean r5 = java.lang.Boolean.TRUE     // Catch:{ zzc -> 0x007c }
            com.google.android.gms.dynamite.DynamiteModule.zzgtw = r5     // Catch:{ zzc -> 0x007c }
            monitor-exit(r1)     // Catch:{ all -> 0x0087 }
            monitor-exit(r0)     // Catch:{ all -> 0x00e7 }
            return r4
        L_0x0079:
            monitor-exit(r1)     // Catch:{ all -> 0x0087 }
            monitor-exit(r0)     // Catch:{ all -> 0x00e7 }
            return r4
        L_0x007c:
            java.lang.ClassLoader r4 = java.lang.ClassLoader.getSystemClassLoader()     // Catch:{ all -> 0x0087 }
            r2.set(r3, r4)     // Catch:{ all -> 0x0087 }
            goto L_0x002f
        L_0x0084:
            monitor-exit(r1)     // Catch:{ all -> 0x0087 }
            r1 = r2
            goto L_0x00b2
        L_0x0087:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0087 }
            throw r2     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x008a }
        L_0x008a:
            r1 = move-exception
            java.lang.String r2 = "DynamiteModule"
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ all -> 0x00e7 }
            r3 = 30
            java.lang.String r4 = java.lang.String.valueOf(r1)     // Catch:{ all -> 0x00e7 }
            int r4 = r4.length()     // Catch:{ all -> 0x00e7 }
            int r3 = r3 + r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e7 }
            r4.<init>(r3)     // Catch:{ all -> 0x00e7 }
            java.lang.String r3 = "Failed to load module via V2: "
            r4.append(r3)     // Catch:{ all -> 0x00e7 }
            r4.append(r1)     // Catch:{ all -> 0x00e7 }
            java.lang.String r1 = r4.toString()     // Catch:{ all -> 0x00e7 }
            android.util.Log.w(r2, r1)     // Catch:{ all -> 0x00e7 }
            java.lang.Boolean r1 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x00e7 }
        L_0x00b2:
            com.google.android.gms.dynamite.DynamiteModule.zzgtw = r1     // Catch:{ all -> 0x00e7 }
        L_0x00b4:
            monitor-exit(r0)     // Catch:{ all -> 0x00e7 }
            boolean r0 = r1.booleanValue()
            if (r0 == 0) goto L_0x00e2
            int r8 = zze(r8, r9, r10)     // Catch:{ zzc -> 0x00c0 }
            return r8
        L_0x00c0:
            r8 = move-exception
            java.lang.String r9 = "DynamiteModule"
            java.lang.String r10 = "Failed to retrieve remote module version: "
            java.lang.String r8 = r8.getMessage()
            java.lang.String r8 = java.lang.String.valueOf(r8)
            int r0 = r8.length()
            if (r0 == 0) goto L_0x00d8
            java.lang.String r8 = r10.concat(r8)
            goto L_0x00dd
        L_0x00d8:
            java.lang.String r8 = new java.lang.String
            r8.<init>(r10)
        L_0x00dd:
            android.util.Log.w(r9, r8)
            r8 = 0
            return r8
        L_0x00e2:
            int r8 = zzd(r8, r9, r10)
            return r8
        L_0x00e7:
            r8 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00e7 }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.zzc(android.content.Context, java.lang.String, boolean):int");
    }

    private static DynamiteModule zzc(Context context, String str, int i) throws zzc {
        zzm zzm;
        StringBuilder sb = new StringBuilder(51 + String.valueOf(str).length());
        sb.append("Selected remote version of ");
        sb.append(str);
        sb.append(", version >= ");
        sb.append(i);
        Log.i("DynamiteModule", sb.toString());
        synchronized (DynamiteModule.class) {
            zzm = zzgty;
        }
        if (zzm == null) {
            throw new zzc("DynamiteLoaderV2 was not cached.", (zza) null);
        }
        zza zza2 = zzgua.get();
        if (zza2 == null || zza2.zzguj == null) {
            throw new zzc("No result cursor", (zza) null);
        }
        Context zza3 = zza(context.getApplicationContext(), str, i, zza2.zzguj, zzm);
        if (zza3 != null) {
            return new DynamiteModule(zza3);
        }
        throw new zzc("Failed to get module context", (zza) null);
    }

    private static int zzd(Context context, String str, boolean z) {
        zzk zzdc = zzdc(context);
        if (zzdc == null) {
            return 0;
        }
        try {
            return zzdc.zza(zzn.zzy(context), str, z);
        } catch (RemoteException e) {
            String valueOf = String.valueOf(e.getMessage());
            Log.w("DynamiteModule", valueOf.length() != 0 ? "Failed to retrieve remote module version: ".concat(valueOf) : new String("Failed to retrieve remote module version: "));
            return 0;
        }
    }

    /* JADX WARN: Type inference failed for: r1v7, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.google.android.gms.dynamite.zzk zzdc(android.content.Context r5) {
        /*
            java.lang.Class<com.google.android.gms.dynamite.DynamiteModule> r0 = com.google.android.gms.dynamite.DynamiteModule.class
            monitor-enter(r0)
            com.google.android.gms.dynamite.zzk r1 = com.google.android.gms.dynamite.DynamiteModule.zzgtx     // Catch:{ all -> 0x006f }
            if (r1 == 0) goto L_0x000b
            com.google.android.gms.dynamite.zzk r5 = com.google.android.gms.dynamite.DynamiteModule.zzgtx     // Catch:{ all -> 0x006f }
            monitor-exit(r0)     // Catch:{ all -> 0x006f }
            return r5
        L_0x000b:
            com.google.android.gms.common.zze r1 = com.google.android.gms.common.zze.zzafm()     // Catch:{ all -> 0x006f }
            int r1 = r1.isGooglePlayServicesAvailable(r5)     // Catch:{ all -> 0x006f }
            r2 = 0
            if (r1 == 0) goto L_0x0018
            monitor-exit(r0)     // Catch:{ all -> 0x006f }
            return r2
        L_0x0018:
            java.lang.String r1 = "com.google.android.gms"
            r3 = 3
            android.content.Context r5 = r5.createPackageContext(r1, r3)     // Catch:{ Exception -> 0x004d }
            java.lang.ClassLoader r5 = r5.getClassLoader()     // Catch:{ Exception -> 0x004d }
            java.lang.String r1 = "com.google.android.gms.chimera.container.DynamiteLoaderImpl"
            java.lang.Class r5 = r5.loadClass(r1)     // Catch:{ Exception -> 0x004d }
            java.lang.Object r5 = r5.newInstance()     // Catch:{ Exception -> 0x004d }
            android.os.IBinder r5 = (android.os.IBinder) r5     // Catch:{ Exception -> 0x004d }
            if (r5 != 0) goto L_0x0033
            r5 = r2
            goto L_0x0047
        L_0x0033:
            java.lang.String r1 = "com.google.android.gms.dynamite.IDynamiteLoader"
            android.os.IInterface r1 = r5.queryLocalInterface(r1)     // Catch:{ Exception -> 0x004d }
            boolean r3 = r1 instanceof com.google.android.gms.dynamite.zzk     // Catch:{ Exception -> 0x004d }
            if (r3 == 0) goto L_0x0041
            r5 = r1
            com.google.android.gms.dynamite.zzk r5 = (com.google.android.gms.dynamite.zzk) r5     // Catch:{ Exception -> 0x004d }
            goto L_0x0047
        L_0x0041:
            com.google.android.gms.dynamite.zzl r1 = new com.google.android.gms.dynamite.zzl     // Catch:{ Exception -> 0x004d }
            r1.<init>(r5)     // Catch:{ Exception -> 0x004d }
            r5 = r1
        L_0x0047:
            if (r5 == 0) goto L_0x006d
            com.google.android.gms.dynamite.DynamiteModule.zzgtx = r5     // Catch:{ Exception -> 0x004d }
            monitor-exit(r0)     // Catch:{ all -> 0x006f }
            return r5
        L_0x004d:
            r5 = move-exception
            java.lang.String r1 = "DynamiteModule"
            java.lang.String r3 = "Failed to load IDynamiteLoader from GmsCore: "
            java.lang.String r5 = r5.getMessage()     // Catch:{ all -> 0x006f }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ all -> 0x006f }
            int r4 = r5.length()     // Catch:{ all -> 0x006f }
            if (r4 == 0) goto L_0x0065
            java.lang.String r5 = r3.concat(r5)     // Catch:{ all -> 0x006f }
            goto L_0x006a
        L_0x0065:
            java.lang.String r5 = new java.lang.String     // Catch:{ all -> 0x006f }
            r5.<init>(r3)     // Catch:{ all -> 0x006f }
        L_0x006a:
            android.util.Log.e(r1, r5)     // Catch:{ all -> 0x006f }
        L_0x006d:
            monitor-exit(r0)     // Catch:{ all -> 0x006f }
            return r2
        L_0x006f:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x006f }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.zzdc(android.content.Context):com.google.android.gms.dynamite.zzk");
    }

    /* JADX WARNING: Removed duplicated region for block: B:55:0x00b3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int zze(android.content.Context r8, java.lang.String r9, boolean r10) throws com.google.android.gms.dynamite.DynamiteModule.zzc {
        /*
            r0 = 0
            if (r10 == 0) goto L_0x000d
            java.lang.String r10 = "api_force_staging"
            goto L_0x000f
        L_0x0006:
            r8 = move-exception
            goto L_0x00b1
        L_0x0009:
            r8 = move-exception
            r9 = r0
            goto L_0x00a2
        L_0x000d:
            java.lang.String r10 = "api"
        L_0x000f:
            java.lang.String r1 = "content://com.google.android.gms.chimera/"
            r2 = 1
            java.lang.String r3 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            int r3 = r3.length()     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            int r2 = r2 + r3
            java.lang.String r3 = java.lang.String.valueOf(r10)     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            int r3 = r3.length()     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            int r2 = r2 + r3
            java.lang.String r3 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            int r3 = r3.length()     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            int r2 = r2 + r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            r3.append(r1)     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            r3.append(r10)     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            java.lang.String r10 = "/"
            r3.append(r10)     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            r3.append(r9)     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            java.lang.String r9 = r3.toString()     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            android.net.Uri r2 = android.net.Uri.parse(r9)     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            android.content.ContentResolver r1 = r8.getContentResolver()     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            android.database.Cursor r8 = r1.query(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0009, all -> 0x0006 }
            if (r8 == 0) goto L_0x0093
            boolean r9 = r8.moveToFirst()     // Catch:{ Exception -> 0x008e, all -> 0x008a }
            if (r9 != 0) goto L_0x005d
            goto L_0x0093
        L_0x005d:
            r9 = 0
            int r9 = r8.getInt(r9)     // Catch:{ Exception -> 0x008e, all -> 0x008a }
            if (r9 <= 0) goto L_0x0084
            java.lang.Class<com.google.android.gms.dynamite.DynamiteModule> r10 = com.google.android.gms.dynamite.DynamiteModule.class
            monitor-enter(r10)     // Catch:{ Exception -> 0x008e, all -> 0x008a }
            r1 = 2
            java.lang.String r1 = r8.getString(r1)     // Catch:{ all -> 0x0081 }
            com.google.android.gms.dynamite.DynamiteModule.zzgtz = r1     // Catch:{ all -> 0x0081 }
            monitor-exit(r10)     // Catch:{ all -> 0x0081 }
            java.lang.ThreadLocal<com.google.android.gms.dynamite.DynamiteModule$zza> r10 = com.google.android.gms.dynamite.DynamiteModule.zzgua     // Catch:{ Exception -> 0x008e, all -> 0x008a }
            java.lang.Object r10 = r10.get()     // Catch:{ Exception -> 0x008e, all -> 0x008a }
            com.google.android.gms.dynamite.DynamiteModule$zza r10 = (com.google.android.gms.dynamite.DynamiteModule.zza) r10     // Catch:{ Exception -> 0x008e, all -> 0x008a }
            if (r10 == 0) goto L_0x0084
            android.database.Cursor r1 = r10.zzguj     // Catch:{ Exception -> 0x008e, all -> 0x008a }
            if (r1 != 0) goto L_0x0084
            r10.zzguj = r8     // Catch:{ Exception -> 0x008e, all -> 0x008a }
            r8 = r0
            goto L_0x0084
        L_0x0081:
            r9 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x0081 }
            throw r9     // Catch:{ Exception -> 0x008e, all -> 0x008a }
        L_0x0084:
            if (r8 == 0) goto L_0x0089
            r8.close()
        L_0x0089:
            return r9
        L_0x008a:
            r9 = move-exception
            r0 = r8
            r8 = r9
            goto L_0x00b1
        L_0x008e:
            r9 = move-exception
            r7 = r9
            r9 = r8
            r8 = r7
            goto L_0x00a2
        L_0x0093:
            java.lang.String r9 = "DynamiteModule"
            java.lang.String r10 = "Failed to retrieve remote module version."
            android.util.Log.w(r9, r10)     // Catch:{ Exception -> 0x008e, all -> 0x008a }
            com.google.android.gms.dynamite.DynamiteModule$zzc r9 = new com.google.android.gms.dynamite.DynamiteModule$zzc     // Catch:{ Exception -> 0x008e, all -> 0x008a }
            java.lang.String r10 = "Failed to connect to dynamite module ContentResolver."
            r9.<init>(r10, r0)     // Catch:{ Exception -> 0x008e, all -> 0x008a }
            throw r9     // Catch:{ Exception -> 0x008e, all -> 0x008a }
        L_0x00a2:
            boolean r10 = r8 instanceof com.google.android.gms.dynamite.DynamiteModule.zzc     // Catch:{ all -> 0x00af }
            if (r10 == 0) goto L_0x00a7
            throw r8     // Catch:{ all -> 0x00af }
        L_0x00a7:
            com.google.android.gms.dynamite.DynamiteModule$zzc r10 = new com.google.android.gms.dynamite.DynamiteModule$zzc     // Catch:{ all -> 0x00af }
            java.lang.String r1 = "V2 version check failed"
            r10.<init>(r1, r8, r0)     // Catch:{ all -> 0x00af }
            throw r10     // Catch:{ all -> 0x00af }
        L_0x00af:
            r8 = move-exception
            r0 = r9
        L_0x00b1:
            if (r0 == 0) goto L_0x00b6
            r0.close()
        L_0x00b6:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.zze(android.content.Context, java.lang.String, boolean):int");
    }

    public final Context zzapp() {
        return this.zzgui;
    }

    public final IBinder zzgw(String str) throws zzc {
        try {
            return (IBinder) this.zzgui.getClassLoader().loadClass(str).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            String valueOf = String.valueOf(str);
            throw new zzc(valueOf.length() != 0 ? "Failed to instantiate module class: ".concat(valueOf) : new String("Failed to instantiate module class: "), e, null);
        }
    }
}
