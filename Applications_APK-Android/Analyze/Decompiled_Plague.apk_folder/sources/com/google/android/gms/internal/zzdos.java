package com.google.android.gms.internal;

import java.security.GeneralSecurityException;

public interface zzdos<P> {
    String getKeyType();

    P zza(zzfdh zzfdh) throws GeneralSecurityException;

    P zza(zzffi zzffi) throws GeneralSecurityException;

    zzffi zzb(zzfdh zzfdh) throws GeneralSecurityException;

    zzffi zzb(zzffi zzffi) throws GeneralSecurityException;

    zzdrk zzc(zzfdh zzfdh) throws GeneralSecurityException;
}
