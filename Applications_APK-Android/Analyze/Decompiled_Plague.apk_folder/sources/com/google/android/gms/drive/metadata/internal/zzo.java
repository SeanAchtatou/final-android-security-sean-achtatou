package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.SearchableCollectionMetadataField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

public final class zzo extends zzl<DriveId> implements SearchableCollectionMetadataField<DriveId> {
    public static final zzg zzgpq = new zzp();

    public zzo(int i) {
        super("parents", Collections.emptySet(), Arrays.asList("parentsExtra", "dbInstanceId", "parentsExtraHolder"), 4100000);
    }

    /* access modifiers changed from: private */
    public static void zzd(DataHolder dataHolder) {
        Bundle zzafx = dataHolder.zzafx();
        if (zzafx != null) {
            synchronized (dataHolder) {
                DataHolder dataHolder2 = (DataHolder) zzafx.getParcelable("parentsExtraHolder");
                if (dataHolder2 != null) {
                    dataHolder2.close();
                    zzafx.remove("parentsExtraHolder");
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzc(DataHolder dataHolder, int i, int i2) {
        return zzc(dataHolder, i, i2);
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public final Collection<DriveId> zzd(DataHolder dataHolder, int i, int i2) {
        DataHolder dataHolder2;
        DataHolder dataHolder3 = dataHolder;
        Bundle zzafx = dataHolder.zzafx();
        ArrayList parcelableArrayList = zzafx.getParcelableArrayList("parentsExtra");
        if (parcelableArrayList == null) {
            if (zzafx.getParcelable("parentsExtraHolder") != null) {
                synchronized (dataHolder) {
                    try {
                        dataHolder2 = (DataHolder) dataHolder.zzafx().getParcelable("parentsExtraHolder");
                        if (dataHolder2 != null) {
                            int count = dataHolder.getCount();
                            ArrayList arrayList = new ArrayList(count);
                            HashMap hashMap = new HashMap(count);
                            int i3 = 0;
                            for (int i4 = 0; i4 < count; i4++) {
                                int zzbz = dataHolder3.zzbz(i4);
                                ParentDriveIdSet parentDriveIdSet = new ParentDriveIdSet();
                                arrayList.add(parentDriveIdSet);
                                hashMap.put(Long.valueOf(dataHolder3.zzb("sqlId", i4, zzbz)), parentDriveIdSet);
                            }
                            Bundle zzafx2 = dataHolder2.zzafx();
                            String string = zzafx2.getString("childSqlIdColumn");
                            String string2 = zzafx2.getString("parentSqlIdColumn");
                            String string3 = zzafx2.getString("parentResIdColumn");
                            int count2 = dataHolder2.getCount();
                            while (i3 < count2) {
                                int zzbz2 = dataHolder2.zzbz(i3);
                                ((ParentDriveIdSet) hashMap.get(Long.valueOf(dataHolder2.zzb(string, i3, zzbz2)))).zzgpp.add(new zzq(dataHolder2.zzd(string3, i3, zzbz2), dataHolder2.zzb(string2, i3, zzbz2), 1));
                                i3++;
                                count2 = count2;
                                string3 = string3;
                            }
                            dataHolder.zzafx().putParcelableArrayList("parentsExtra", arrayList);
                            dataHolder2.close();
                            dataHolder.zzafx().remove("parentsExtraHolder");
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                parcelableArrayList = zzafx.getParcelableArrayList("parentsExtra");
            }
            if (parcelableArrayList == null) {
                return null;
            }
        }
        return ((ParentDriveIdSet) parcelableArrayList.get(i)).zzaa(zzafx.getLong("dbInstanceId"));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzn(Bundle bundle) {
        return zzn(bundle);
    }

    /* access modifiers changed from: protected */
    public final Collection<DriveId> zzo(Bundle bundle) {
        Collection zzo = super.zzn(bundle);
        if (zzo == null) {
            return null;
        }
        return new HashSet(zzo);
    }
}
