package com.google.android.gms.internal;

import java.io.IOException;

public final class zzdqe extends zzfee<zzdqe, zza> implements zzffk {
    private static volatile zzffm<zzdqe> zzbas;
    /* access modifiers changed from: private */
    public static final zzdqe zzlqs;
    private int zzlqn;

    public static final class zza extends zzfef<zzdqe, zza> implements zzffk {
        private zza() {
            super(zzdqe.zzlqs);
        }

        /* synthetic */ zza(zzdqf zzdqf) {
            this();
        }
    }

    static {
        zzdqe zzdqe = new zzdqe();
        zzlqs = zzdqe;
        zzdqe.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdqe.zzpbs.zzbim();
    }

    private zzdqe() {
    }

    public static zzdqe zzbmg() {
        return zzlqs;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        boolean z = true;
        boolean z2 = false;
        switch (zzdqf.zzbaq[i - 1]) {
            case 1:
                return new zzdqe();
            case 2:
                return zzlqs;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdqe zzdqe = (zzdqe) obj2;
                boolean z3 = this.zzlqn != 0;
                int i2 = this.zzlqn;
                if (zzdqe.zzlqn == 0) {
                    z = false;
                }
                this.zzlqn = zzfen.zza(z3, i2, z, zzdqe.zzlqn);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                if (((zzfea) obj2) != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzlqn = zzfdq.zzcub();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdqe.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlqs);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlqs;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlqn != 0) {
            zzfdv.zzab(1, this.zzlqn);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final int zzblz() {
        return this.zzlqn;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlqn != 0) {
            i2 = 0 + zzfdv.zzae(1, this.zzlqn);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
