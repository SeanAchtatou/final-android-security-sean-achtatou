package com.google.android.gms.internal;

public final class zzdov {
    /* JADX WARNING: Removed duplicated region for block: B:4:0x0015 A[Catch:{ zzfew -> 0x0050 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final com.google.android.gms.internal.zzdot zzac(byte[] r4) throws java.security.GeneralSecurityException {
        /*
            com.google.android.gms.internal.zzdrr r4 = com.google.android.gms.internal.zzdrr.zzae(r4)     // Catch:{ zzfew -> 0x0050 }
            com.google.android.gms.internal.zzdot.zza(r4)     // Catch:{ zzfew -> 0x0050 }
            java.util.List r0 = r4.zzbob()     // Catch:{ zzfew -> 0x0050 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ zzfew -> 0x0050 }
        L_0x000f:
            boolean r1 = r0.hasNext()     // Catch:{ zzfew -> 0x0050 }
            if (r1 == 0) goto L_0x0047
            java.lang.Object r1 = r0.next()     // Catch:{ zzfew -> 0x0050 }
            com.google.android.gms.internal.zzdrr$zzb r1 = (com.google.android.gms.internal.zzdrr.zzb) r1     // Catch:{ zzfew -> 0x0050 }
            com.google.android.gms.internal.zzdrk r2 = r1.zzbof()     // Catch:{ zzfew -> 0x0050 }
            com.google.android.gms.internal.zzdrk$zzb r2 = r2.zzbnu()     // Catch:{ zzfew -> 0x0050 }
            com.google.android.gms.internal.zzdrk$zzb r3 = com.google.android.gms.internal.zzdrk.zzb.UNKNOWN_KEYMATERIAL     // Catch:{ zzfew -> 0x0050 }
            if (r2 == r3) goto L_0x003f
            com.google.android.gms.internal.zzdrk r2 = r1.zzbof()     // Catch:{ zzfew -> 0x0050 }
            com.google.android.gms.internal.zzdrk$zzb r2 = r2.zzbnu()     // Catch:{ zzfew -> 0x0050 }
            com.google.android.gms.internal.zzdrk$zzb r3 = com.google.android.gms.internal.zzdrk.zzb.SYMMETRIC     // Catch:{ zzfew -> 0x0050 }
            if (r2 == r3) goto L_0x003f
            com.google.android.gms.internal.zzdrk r1 = r1.zzbof()     // Catch:{ zzfew -> 0x0050 }
            com.google.android.gms.internal.zzdrk$zzb r1 = r1.zzbnu()     // Catch:{ zzfew -> 0x0050 }
            com.google.android.gms.internal.zzdrk$zzb r2 = com.google.android.gms.internal.zzdrk.zzb.ASYMMETRIC_PRIVATE     // Catch:{ zzfew -> 0x0050 }
            if (r1 != r2) goto L_0x000f
        L_0x003f:
            java.security.GeneralSecurityException r4 = new java.security.GeneralSecurityException     // Catch:{ zzfew -> 0x0050 }
            java.lang.String r0 = "keyset contains secret key material"
            r4.<init>(r0)     // Catch:{ zzfew -> 0x0050 }
            throw r4     // Catch:{ zzfew -> 0x0050 }
        L_0x0047:
            com.google.android.gms.internal.zzdot.zza(r4)     // Catch:{ zzfew -> 0x0050 }
            com.google.android.gms.internal.zzdot r0 = new com.google.android.gms.internal.zzdot     // Catch:{ zzfew -> 0x0050 }
            r0.<init>(r4)     // Catch:{ zzfew -> 0x0050 }
            return r0
        L_0x0050:
            java.security.GeneralSecurityException r4 = new java.security.GeneralSecurityException
            java.lang.String r0 = "invalid keyset"
            r4.<init>(r0)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdov.zzac(byte[]):com.google.android.gms.internal.zzdot");
    }
}
