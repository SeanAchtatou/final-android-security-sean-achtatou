package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.internal.zzcl;
import com.google.android.gms.common.api.internal.zzcp;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.internal.api.zzp;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.zzh;
import com.google.android.gms.tasks.Task;
import java.util.List;

public class RealTimeMultiplayerClient extends zzp {

    public interface ReliableMessageSentCallback extends RealTimeMultiplayer.ReliableMessageSentCallback {
        void onRealTimeMessageSent(int i, int i2, String str);
    }

    RealTimeMultiplayerClient(@NonNull Activity activity, @NonNull Games.GamesOptions gamesOptions) {
        super(activity, gamesOptions);
    }

    RealTimeMultiplayerClient(@NonNull Context context, @NonNull Games.GamesOptions gamesOptions) {
        super(context, gamesOptions);
    }

    public Task<Void> create(@NonNull RoomConfig roomConfig) {
        zzcl zza = zza(roomConfig.zzatw(), zzh.class.getSimpleName());
        return zza(new zzbi(this, zza, zza, roomConfig), new zzbj(this, zza.zzajc()));
    }

    public Task<Void> declineInvitation(@NonNull String str) {
        return zzb(new zzbd(this, str));
    }

    public Task<Void> dismissInvitation(@NonNull String str) {
        return zzb(new zzbe(this, str));
    }

    public Task<Intent> getSelectOpponentsIntent(@IntRange(from = 1) int i, @IntRange(from = 1) int i2) {
        return getSelectOpponentsIntent(i, i2, true);
    }

    public Task<Intent> getSelectOpponentsIntent(@IntRange(from = 1) int i, @IntRange(from = 1) int i2, boolean z) {
        return zza(new zzbh(this, i, i2, z));
    }

    public Task<Intent> getWaitingRoomIntent(@NonNull Room room, @IntRange(from = 0) int i) {
        return zza(new zzaz(this, room, i));
    }

    public Task<Void> join(@NonNull RoomConfig roomConfig) {
        zzcl zza = zza(roomConfig.zzatw(), zzh.class.getSimpleName());
        return zza(new zzbk(this, zza, zza, roomConfig), new zzbl(this, zza.zzajc()));
    }

    public Task<Void> leave(@NonNull RoomConfig roomConfig, @NonNull String str) {
        zzcl zza = zza(roomConfig.zzatw(), zzh.class.getSimpleName());
        return zza(new zzbf(this, str)).continueWithTask(new zzbp(this, zza)).continueWithTask(new zzbm(this, zza, str, roomConfig));
    }

    public Task<Integer> sendReliableMessage(@NonNull byte[] bArr, @NonNull String str, @NonNull String str2, @Nullable ReliableMessageSentCallback reliableMessageSentCallback) {
        return zzb(new zzbq(this, reliableMessageSentCallback != null ? zzcp.zzb(reliableMessageSentCallback, Looper.getMainLooper(), ReliableMessageSentCallback.class.getSimpleName()) : null, bArr, str, str2));
    }

    public Task<Void> sendUnreliableMessage(@NonNull byte[] bArr, @NonNull String str, @NonNull String str2) {
        return zzb(new zzba(this, bArr, str, str2));
    }

    public Task<Void> sendUnreliableMessage(@NonNull byte[] bArr, @NonNull String str, @NonNull List<String> list) {
        return zzb(new zzbb(this, list, bArr, str));
    }

    public Task<Void> sendUnreliableMessageToOthers(@NonNull byte[] bArr, @NonNull String str) {
        return zzb(new zzbc(this, bArr, str));
    }
}
