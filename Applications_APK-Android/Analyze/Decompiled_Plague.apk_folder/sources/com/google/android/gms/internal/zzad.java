package com.google.android.gms.internal;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.TreeMap;
import org.apache.http.Header;
import org.apache.http.HttpEntity;

public class zzad implements zzk {
    private static boolean DEBUG = zzab.DEBUG;
    private zzam zzbm;
    private zzae zzbn;

    public zzad(zzam zzam) {
        this(zzam, new zzae(4096));
    }

    private zzad(zzam zzam, zzae zzae) {
        this.zzbm = zzam;
        this.zzbn = zzae;
    }

    private static Map<String, String> zza(Header[] headerArr) {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (int i = 0; i < headerArr.length; i++) {
            treeMap.put(headerArr[i].getName(), headerArr[i].getValue());
        }
        return treeMap;
    }

    private static void zza(String str, zzp<?> zzp, zzaa zzaa) throws zzaa {
        zzx zzj = zzp.zzj();
        int zzi = zzp.zzi();
        try {
            zzj.zza(zzaa);
            zzp.zzb(String.format("%s-retry [timeout=%s]", str, Integer.valueOf(zzi)));
        } catch (zzaa e) {
            zzp.zzb(String.format("%s-timeout-giveup [timeout=%s]", str, Integer.valueOf(zzi)));
            throw e;
        }
    }

    private final byte[] zza(HttpEntity httpEntity) throws IOException, zzy {
        zzap zzap = new zzap(this.zzbn, (int) httpEntity.getContentLength());
        byte[] bArr = null;
        try {
            InputStream content = httpEntity.getContent();
            if (content == null) {
                throw new zzy();
            }
            byte[] zzb = this.zzbn.zzb(1024);
            while (true) {
                try {
                    int read = content.read(zzb);
                    if (read == -1) {
                        break;
                    }
                    zzap.write(zzb, 0, read);
                } catch (Throwable th) {
                    th = th;
                    bArr = zzb;
                    try {
                        httpEntity.consumeContent();
                    } catch (IOException unused) {
                        zzab.zza("Error occurred when calling consumingContent", new Object[0]);
                    }
                    this.zzbn.zza(bArr);
                    zzap.close();
                    throw th;
                }
            }
            byte[] byteArray = zzap.toByteArray();
            try {
                httpEntity.consumeContent();
            } catch (IOException unused2) {
                zzab.zza("Error occurred when calling consumingContent", new Object[0]);
            }
            this.zzbn.zza(zzb);
            zzap.close();
            return byteArray;
        } catch (Throwable th2) {
            th = th2;
            httpEntity.consumeContent();
            this.zzbn.zza(bArr);
            zzap.close();
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01c0, code lost:
        throw new java.lang.RuntimeException(r2, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01c1, code lost:
        r5 = "connection";
        r6 = new com.google.android.gms.internal.zzz();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01c9, code lost:
        r5 = "socket";
        r6 = new com.google.android.gms.internal.zzz();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x01d0, code lost:
        zza(r5, r2, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00cc, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00cd, code lost:
        r13 = r5;
        r14 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0115, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0117, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0118, code lost:
        r7 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0119, code lost:
        r13 = r5;
        r14 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x011c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x011d, code lost:
        r5 = r0;
        r14 = r12;
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0122, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0123, code lost:
        r14 = r5;
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0126, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0127, code lost:
        r14 = r5;
        r10 = null;
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x012a, code lost:
        r5 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x012d, code lost:
        r5 = r10.getStatusLine().getStatusCode();
        com.google.android.gms.internal.zzab.zzc("Unexpected response code %d for %s", java.lang.Integer.valueOf(r5), r24.getUrl());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x014a, code lost:
        if (r13 != null) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x014c, code lost:
        r11 = new com.google.android.gms.internal.zzn(r5, r13, r14, false, android.os.SystemClock.elapsedRealtime() - r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x015c, code lost:
        if (r5 == 401) goto L_0x0185;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0165, code lost:
        if (r5 < 400) goto L_0x0171;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0170, code lost:
        throw new com.google.android.gms.internal.zzf(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0173, code lost:
        if (r5 < 500) goto L_0x017f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x017e, code lost:
        throw new com.google.android.gms.internal.zzy(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0184, code lost:
        throw new com.google.android.gms.internal.zzy(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0185, code lost:
        zza("auth", r2, new com.google.android.gms.internal.zza(r11));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0191, code lost:
        r5 = "network";
        r6 = new com.google.android.gms.internal.zzm();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x019e, code lost:
        throw new com.google.android.gms.internal.zzo(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x019f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01a0, code lost:
        r3 = r0;
        r2 = java.lang.String.valueOf(r24.getUrl());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01b1, code lost:
        if (r2.length() != 0) goto L_0x01b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01b3, code lost:
        r2 = "Bad URL ".concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01b8, code lost:
        r2 = new java.lang.String("Bad URL ");
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:103:? A[ExcHandler: ConnectTimeoutException (unused org.apache.http.conn.ConnectTimeoutException), SYNTHETIC, Splitter:B:2:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:105:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), SYNTHETIC, Splitter:B:2:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0199 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x019f A[ExcHandler: MalformedURLException (r0v0 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:2:0x000e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.gms.internal.zzn zza(com.google.android.gms.internal.zzp<?> r24) throws com.google.android.gms.internal.zzaa {
        /*
            r23 = this;
            r1 = r23
            r2 = r24
            long r3 = android.os.SystemClock.elapsedRealtime()
        L_0x0008:
            java.util.Map r5 = java.util.Collections.emptyMap()
            r8 = 0
            r9 = 0
            java.util.HashMap r10 = new java.util.HashMap     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
            r10.<init>()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
            com.google.android.gms.internal.zzc r11 = r24.zze()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
            if (r11 == 0) goto L_0x003c
            java.lang.String r12 = r11.zza     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
            if (r12 == 0) goto L_0x0024
            java.lang.String r12 = "If-None-Match"
            java.lang.String r13 = r11.zza     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
            r10.put(r12, r13)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
        L_0x0024:
            long r12 = r11.zzc     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
            r14 = 0
            int r16 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r16 <= 0) goto L_0x003c
            java.util.Date r12 = new java.util.Date     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
            long r13 = r11.zzc     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
            r12.<init>(r13)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
            java.lang.String r11 = "If-Modified-Since"
            java.lang.String r12 = org.apache.http.impl.cookie.DateUtils.formatDate(r12)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
            r10.put(r11, r12)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
        L_0x003c:
            com.google.android.gms.internal.zzam r11 = r1.zzbm     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
            org.apache.http.HttpResponse r10 = r11.zza(r2, r10)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0126 }
            org.apache.http.StatusLine r11 = r10.getStatusLine()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0122 }
            int r13 = r11.getStatusCode()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0122 }
            org.apache.http.Header[] r12 = r10.getAllHeaders()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0122 }
            java.util.Map r12 = zza(r12)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0122 }
            r5 = 304(0x130, float:4.26E-43)
            if (r13 != r5) goto L_0x0095
            com.google.android.gms.internal.zzc r5 = r24.zze()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x008f }
            if (r5 != 0) goto L_0x0071
            com.google.android.gms.internal.zzn r5 = new com.google.android.gms.internal.zzn     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x008f }
            r15 = 304(0x130, float:4.26E-43)
            r16 = 0
            r18 = 1
            long r13 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x008f }
            long r19 = r13 - r3
            r14 = r5
            r17 = r12
            r14.<init>(r15, r16, r17, r18, r19)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x008f }
            return r5
        L_0x0071:
            java.util.Map<java.lang.String, java.lang.String> r11 = r5.zzf     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x008f }
            r11.putAll(r12)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x008f }
            com.google.android.gms.internal.zzn r11 = new com.google.android.gms.internal.zzn     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x008f }
            r14 = 304(0x130, float:4.26E-43)
            byte[] r15 = r5.data     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x008f }
            java.util.Map<java.lang.String, java.lang.String> r5 = r5.zzf     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x008f }
            r17 = 1
            long r18 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x008f }
            long r20 = r18 - r3
            r13 = r11
            r16 = r5
            r18 = r20
            r13.<init>(r14, r15, r16, r17, r18)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x008f }
            return r11
        L_0x008f:
            r0 = move-exception
            r5 = r0
            r13 = r8
            r14 = r12
            goto L_0x012b
        L_0x0095:
            org.apache.http.HttpEntity r5 = r10.getEntity()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x011c }
            if (r5 == 0) goto L_0x00a4
            org.apache.http.HttpEntity r5 = r10.getEntity()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x008f }
            byte[] r5 = r1.zza(r5)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x008f }
            goto L_0x00a6
        L_0x00a4:
            byte[] r5 = new byte[r9]     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x011c }
        L_0x00a6:
            long r14 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            long r6 = r14 - r3
            boolean r8 = com.google.android.gms.internal.zzad.DEBUG     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            if (r8 != 0) goto L_0x00b6
            r14 = 3000(0xbb8, double:1.482E-320)
            int r8 = (r6 > r14 ? 1 : (r6 == r14 ? 0 : -1))
            if (r8 <= 0) goto L_0x00f3
        L_0x00b6:
            java.lang.String r8 = "HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]"
            r14 = 5
            java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            r14[r9] = r2     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            r7 = 1
            r14[r7] = r6     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            if (r5 == 0) goto L_0x00d1
            int r6 = r5.length     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x00cc }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x00cc }
            goto L_0x00d3
        L_0x00cc:
            r0 = move-exception
            r13 = r5
            r14 = r12
            goto L_0x012a
        L_0x00d1:
            java.lang.String r6 = "null"
        L_0x00d3:
            r7 = 2
            r14[r7] = r6     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            r6 = 3
            int r7 = r11.getStatusCode()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            r14[r6] = r7     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            r6 = 4
            com.google.android.gms.internal.zzx r7 = r24.zzj()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            int r7 = r7.zzb()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            r14[r6] = r7     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            com.google.android.gms.internal.zzab.zzb(r8, r14)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
        L_0x00f3:
            r6 = 200(0xc8, float:2.8E-43)
            if (r13 < r6) goto L_0x010e
            r6 = 299(0x12b, float:4.19E-43)
            if (r13 <= r6) goto L_0x00fc
            goto L_0x010e
        L_0x00fc:
            com.google.android.gms.internal.zzn r6 = new com.google.android.gms.internal.zzn     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            r16 = 0
            long r7 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0117 }
            long r17 = r7 - r3
            r7 = r12
            r12 = r6
            r14 = r5
            r15 = r7
            r12.<init>(r13, r14, r15, r16, r17)     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0115 }
            return r6
        L_0x010e:
            r7 = r12
            java.io.IOException r6 = new java.io.IOException     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0115 }
            r6.<init>()     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0115 }
            throw r6     // Catch:{ SocketTimeoutException -> 0x01c9, ConnectTimeoutException -> 0x01c1, MalformedURLException -> 0x019f, IOException -> 0x0115 }
        L_0x0115:
            r0 = move-exception
            goto L_0x0119
        L_0x0117:
            r0 = move-exception
            r7 = r12
        L_0x0119:
            r13 = r5
            r14 = r7
            goto L_0x012a
        L_0x011c:
            r0 = move-exception
            r7 = r12
            r5 = r0
            r14 = r7
            r13 = r8
            goto L_0x012b
        L_0x0122:
            r0 = move-exception
            r14 = r5
            r13 = r8
            goto L_0x012a
        L_0x0126:
            r0 = move-exception
            r14 = r5
            r10 = r8
            r13 = r10
        L_0x012a:
            r5 = r0
        L_0x012b:
            if (r10 == 0) goto L_0x0199
            org.apache.http.StatusLine r5 = r10.getStatusLine()
            int r5 = r5.getStatusCode()
            java.lang.String r6 = "Unexpected response code %d for %s"
            r7 = 2
            java.lang.Object[] r7 = new java.lang.Object[r7]
            java.lang.Integer r8 = java.lang.Integer.valueOf(r5)
            r7[r9] = r8
            java.lang.String r8 = r24.getUrl()
            r9 = 1
            r7[r9] = r8
            com.google.android.gms.internal.zzab.zzc(r6, r7)
            if (r13 == 0) goto L_0x0191
            com.google.android.gms.internal.zzn r6 = new com.google.android.gms.internal.zzn
            r15 = 0
            long r7 = android.os.SystemClock.elapsedRealtime()
            long r16 = r7 - r3
            r11 = r6
            r12 = r5
            r11.<init>(r12, r13, r14, r15, r16)
            r7 = 401(0x191, float:5.62E-43)
            if (r5 == r7) goto L_0x0185
            r7 = 403(0x193, float:5.65E-43)
            if (r5 != r7) goto L_0x0163
            goto L_0x0185
        L_0x0163:
            r2 = 400(0x190, float:5.6E-43)
            if (r5 < r2) goto L_0x0171
            r2 = 499(0x1f3, float:6.99E-43)
            if (r5 > r2) goto L_0x0171
            com.google.android.gms.internal.zzf r2 = new com.google.android.gms.internal.zzf
            r2.<init>(r6)
            throw r2
        L_0x0171:
            r2 = 500(0x1f4, float:7.0E-43)
            if (r5 < r2) goto L_0x017f
            r2 = 599(0x257, float:8.4E-43)
            if (r5 > r2) goto L_0x017f
            com.google.android.gms.internal.zzy r2 = new com.google.android.gms.internal.zzy
            r2.<init>(r6)
            throw r2
        L_0x017f:
            com.google.android.gms.internal.zzy r2 = new com.google.android.gms.internal.zzy
            r2.<init>(r6)
            throw r2
        L_0x0185:
            java.lang.String r5 = "auth"
            com.google.android.gms.internal.zza r7 = new com.google.android.gms.internal.zza
            r7.<init>(r6)
            zza(r5, r2, r7)
            goto L_0x0008
        L_0x0191:
            java.lang.String r5 = "network"
            com.google.android.gms.internal.zzm r6 = new com.google.android.gms.internal.zzm
            r6.<init>()
            goto L_0x01d0
        L_0x0199:
            com.google.android.gms.internal.zzo r2 = new com.google.android.gms.internal.zzo
            r2.<init>(r5)
            throw r2
        L_0x019f:
            r0 = move-exception
            r3 = r0
            java.lang.RuntimeException r4 = new java.lang.RuntimeException
            java.lang.String r5 = "Bad URL "
            java.lang.String r2 = r24.getUrl()
            java.lang.String r2 = java.lang.String.valueOf(r2)
            int r6 = r2.length()
            if (r6 == 0) goto L_0x01b8
            java.lang.String r2 = r5.concat(r2)
            goto L_0x01bd
        L_0x01b8:
            java.lang.String r2 = new java.lang.String
            r2.<init>(r5)
        L_0x01bd:
            r4.<init>(r2, r3)
            throw r4
        L_0x01c1:
            java.lang.String r5 = "connection"
            com.google.android.gms.internal.zzz r6 = new com.google.android.gms.internal.zzz
            r6.<init>()
            goto L_0x01d0
        L_0x01c9:
            java.lang.String r5 = "socket"
            com.google.android.gms.internal.zzz r6 = new com.google.android.gms.internal.zzz
            r6.<init>()
        L_0x01d0:
            zza(r5, r2, r6)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzad.zza(com.google.android.gms.internal.zzp):com.google.android.gms.internal.zzn");
    }
}
