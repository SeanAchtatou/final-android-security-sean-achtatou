package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.MetadataBuffer;

final class zzble extends zzbjv {
    private final zzn<DriveApi.MetadataBufferResult> zzfzc;

    zzble(zzn<DriveApi.MetadataBufferResult> zzn) {
        this.zzfzc = zzn;
    }

    public final void onError(Status status) throws RemoteException {
        this.zzfzc.setResult(new zzblc(status, null, false));
    }

    public final void zza(zzbqe zzbqe) throws RemoteException {
        this.zzfzc.setResult(new zzblc(Status.zzfko, new MetadataBuffer(zzbqe.zzgoj), zzbqe.zzgkl));
    }
}
