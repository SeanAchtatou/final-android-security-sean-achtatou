package com.google.android.gms.internal;

import android.os.IBinder;

public final class zzbtc extends zzed implements zzbta {
    zzbtc(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.drive.realtime.internal.IRealtimeService");
    }
}
