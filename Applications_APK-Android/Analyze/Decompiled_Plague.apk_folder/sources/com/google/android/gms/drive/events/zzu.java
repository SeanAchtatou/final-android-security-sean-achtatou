package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.DriveSpace;
import com.google.android.gms.internal.zzbek;
import java.util.ArrayList;

public final class zzu implements Parcelable.Creator<zzt> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        ArrayList arrayList = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                zzbek.zzb(parcel, readInt);
            } else {
                arrayList = zzbek.zzc(parcel, readInt, DriveSpace.CREATOR);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzt(arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzt[i];
    }
}
