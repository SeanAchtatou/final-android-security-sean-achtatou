package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import java.util.concurrent.atomic.AtomicReference;

public abstract class zzo extends LifecycleCallback implements DialogInterface.OnCancelListener {
    protected volatile boolean mStarted;
    protected final GoogleApiAvailability zzfke;
    protected final AtomicReference<zzp> zzflq;
    private final Handler zzflr;

    protected zzo(zzci zzci) {
        this(zzci, GoogleApiAvailability.getInstance());
    }

    private zzo(zzci zzci, GoogleApiAvailability googleApiAvailability) {
        super(zzci);
        this.zzflq = new AtomicReference<>(null);
        this.zzflr = new Handler(Looper.getMainLooper());
        this.zzfke = googleApiAvailability;
    }

    private static int zza(@Nullable zzp zzp) {
        if (zzp == null) {
            return -1;
        }
        return zzp.zzags();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void onActivityResult(int i, int i2, Intent intent) {
        zzp zzp = this.zzflq.get();
        boolean z = true;
        switch (i) {
            case 1:
                if (i2 != -1) {
                    if (i2 == 0) {
                        int i3 = 13;
                        if (intent != null) {
                            i3 = intent.getIntExtra("<<ResolutionFailureErrorDetail>>", 13);
                        }
                        zzp zzp2 = new zzp(new ConnectionResult(i3, null), zza(zzp));
                        this.zzflq.set(zzp2);
                        zzp = zzp2;
                    }
                    z = false;
                    break;
                }
                break;
            case 2:
                int isGooglePlayServicesAvailable = this.zzfke.isGooglePlayServicesAvailable(getActivity());
                if (isGooglePlayServicesAvailable != 0) {
                    z = false;
                }
                if (zzp != null) {
                    if (zzp.zzagt().getErrorCode() == 18 && isGooglePlayServicesAvailable == 18) {
                        return;
                    }
                } else {
                    return;
                }
            default:
                z = false;
                break;
        }
        if (z) {
            zzagr();
        } else if (zzp != null) {
            zza(zzp.zzagt(), zzp.zzags());
        }
    }

    public void onCancel(DialogInterface dialogInterface) {
        zza(new ConnectionResult(13, null), zza(this.zzflq.get()));
        zzagr();
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.zzflq.set(bundle.getBoolean("resolving_error", false) ? new zzp(new ConnectionResult(bundle.getInt("failed_status"), (PendingIntent) bundle.getParcelable("failed_resolution")), bundle.getInt("failed_client_id", -1)) : null);
        }
    }

    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        zzp zzp = this.zzflq.get();
        if (zzp != null) {
            bundle.putBoolean("resolving_error", true);
            bundle.putInt("failed_client_id", zzp.zzags());
            bundle.putInt("failed_status", zzp.zzagt().getErrorCode());
            bundle.putParcelable("failed_resolution", zzp.zzagt().getResolution());
        }
    }

    public void onStart() {
        super.onStart();
        this.mStarted = true;
    }

    public void onStop() {
        super.onStop();
        this.mStarted = false;
    }

    /* access modifiers changed from: protected */
    public abstract void zza(ConnectionResult connectionResult, int i);

    /* access modifiers changed from: protected */
    public abstract void zzagm();

    /* access modifiers changed from: protected */
    public final void zzagr() {
        this.zzflq.set(null);
        zzagm();
    }

    public final void zzb(ConnectionResult connectionResult, int i) {
        zzp zzp = new zzp(connectionResult, i);
        if (this.zzflq.compareAndSet(null, zzp)) {
            this.zzflr.post(new zzq(this, zzp));
        }
    }
}
