package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public final class zzbou implements Parcelable.Creator<zzbot> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        IBinder iBinder = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                zzbek.zzb(parcel, readInt);
            } else {
                iBinder = zzbek.zzr(parcel, readInt);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbot(iBinder);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbot[i];
    }
}
