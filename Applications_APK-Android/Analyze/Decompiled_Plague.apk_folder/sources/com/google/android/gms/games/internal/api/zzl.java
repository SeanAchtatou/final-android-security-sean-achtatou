package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;

final class zzl implements Achievements.LoadAchievementsResult {
    private /* synthetic */ Status zzekv;

    zzl(zzk zzk, Status status) {
        this.zzekv = status;
    }

    public final AchievementBuffer getAchievements() {
        return new AchievementBuffer(DataHolder.zzca(14));
    }

    public final Status getStatus() {
        return this.zzekv;
    }

    public final void release() {
    }
}
