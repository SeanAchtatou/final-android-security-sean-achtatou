package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.snapshot.SnapshotMetadataBuffer;
import com.google.android.gms.games.snapshot.Snapshots;

final class zzco implements Snapshots.LoadSnapshotsResult {
    private /* synthetic */ Status zzekv;

    zzco(zzcn zzcn, Status status) {
        this.zzekv = status;
    }

    public final SnapshotMetadataBuffer getSnapshots() {
        return new SnapshotMetadataBuffer(DataHolder.zzca(14));
    }

    public final Status getStatus() {
        return this.zzekv;
    }

    public final void release() {
    }
}
