package com.google.android.gms.internal;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;

final /* synthetic */ class zzbmx implements Continuation {
    private final zzbjq zzgmb;

    zzbmx(zzbjq zzbjq) {
        this.zzgmb = zzbjq;
    }

    public final Object then(Task task) {
        return zzbmu.zza(this.zzgmb, task);
    }
}
