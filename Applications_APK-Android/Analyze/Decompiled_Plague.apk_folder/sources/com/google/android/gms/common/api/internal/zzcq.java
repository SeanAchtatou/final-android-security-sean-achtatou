package com.google.android.gms.common.api.internal;

import android.app.Activity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.CancellationException;

public class zzcq extends zzo {
    private TaskCompletionSource<Void> zzeay = new TaskCompletionSource<>();

    private zzcq(zzci zzci) {
        super(zzci);
        this.zzfrj.zza("GmsAvailabilityHelper", this);
    }

    public static zzcq zzp(Activity activity) {
        zzci zzn = zzn(activity);
        zzcq zzcq = (zzcq) zzn.zza("GmsAvailabilityHelper", zzcq.class);
        if (zzcq == null) {
            return new zzcq(zzn);
        }
        if (zzcq.zzeay.getTask().isComplete()) {
            zzcq.zzeay = new TaskCompletionSource<>();
        }
        return zzcq;
    }

    public final Task<Void> getTask() {
        return this.zzeay.getTask();
    }

    public final void onDestroy() {
        super.onDestroy();
        this.zzeay.trySetException(new CancellationException("Host activity was destroyed before Google Play services could be made available."));
    }

    /* access modifiers changed from: protected */
    public final void zza(ConnectionResult connectionResult, int i) {
        this.zzeay.setException(zzb.zzy(new Status(connectionResult.getErrorCode(), connectionResult.getErrorMessage(), connectionResult.getResolution())));
    }

    /* access modifiers changed from: protected */
    public final void zzagm() {
        int isGooglePlayServicesAvailable = this.zzfke.isGooglePlayServicesAvailable(this.zzfrj.zzajb());
        if (isGooglePlayServicesAvailable == 0) {
            this.zzeay.setResult(null);
        } else if (!this.zzeay.getTask().isComplete()) {
            zzb(new ConnectionResult(isGooglePlayServicesAvailable, null), 0);
        }
    }
}
