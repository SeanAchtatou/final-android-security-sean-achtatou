package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzbq;
import java.util.Arrays;

public final class zzbdt extends zzbej {
    public static final Parcelable.Creator<zzbdt> CREATOR = new zzbdu();
    private String packageName;
    private int zzfgi;
    public final String zzfgj;
    public final int zzfgk;
    private String zzfgl;
    private String zzfgm;
    private boolean zzfgn;
    private int zzfgo;
    private boolean zzfhv;

    public zzbdt(String str, int i, int i2, String str2, String str3, String str4, boolean z, int i3) {
        this.packageName = (String) zzbq.checkNotNull(str);
        this.zzfgi = i;
        this.zzfgk = i2;
        this.zzfgj = str2;
        this.zzfgl = str3;
        this.zzfgm = str4;
        this.zzfhv = !z;
        this.zzfgn = z;
        this.zzfgo = i3;
    }

    public zzbdt(String str, int i, int i2, String str2, String str3, boolean z, String str4, boolean z2, int i3) {
        this.packageName = str;
        this.zzfgi = i;
        this.zzfgk = i2;
        this.zzfgl = str2;
        this.zzfgm = str3;
        this.zzfhv = z;
        this.zzfgj = str4;
        this.zzfgn = z2;
        this.zzfgo = i3;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof zzbdt) {
            zzbdt zzbdt = (zzbdt) obj;
            return zzbg.equal(this.packageName, zzbdt.packageName) && this.zzfgi == zzbdt.zzfgi && this.zzfgk == zzbdt.zzfgk && zzbg.equal(this.zzfgj, zzbdt.zzfgj) && zzbg.equal(this.zzfgl, zzbdt.zzfgl) && zzbg.equal(this.zzfgm, zzbdt.zzfgm) && this.zzfhv == zzbdt.zzfhv && this.zzfgn == zzbdt.zzfgn && this.zzfgo == zzbdt.zzfgo;
        }
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.packageName, Integer.valueOf(this.zzfgi), Integer.valueOf(this.zzfgk), this.zzfgj, this.zzfgl, this.zzfgm, Boolean.valueOf(this.zzfhv), Boolean.valueOf(this.zzfgn), Integer.valueOf(this.zzfgo)});
    }

    public final String toString() {
        return "PlayLoggerContext[" + "package=" + this.packageName + ',' + "packageVersionCode=" + this.zzfgi + ',' + "logSource=" + this.zzfgk + ',' + "logSourceName=" + this.zzfgj + ',' + "uploadAccount=" + this.zzfgl + ',' + "loggingId=" + this.zzfgm + ',' + "logAndroidId=" + this.zzfhv + ',' + "isAnonymous=" + this.zzfgn + ',' + "qosTier=" + this.zzfgo + "]";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, this.packageName, false);
        zzbem.zzc(parcel, 3, this.zzfgi);
        zzbem.zzc(parcel, 4, this.zzfgk);
        zzbem.zza(parcel, 5, this.zzfgl, false);
        zzbem.zza(parcel, 6, this.zzfgm, false);
        zzbem.zza(parcel, 7, this.zzfhv);
        zzbem.zza(parcel, 8, this.zzfgj, false);
        zzbem.zza(parcel, 9, this.zzfgn);
        zzbem.zzc(parcel, 10, this.zzfgo);
        zzbem.zzai(parcel, zze);
    }
}
