package com.google.android.gms.drive;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.internal.zzbkp;
import com.google.android.gms.internal.zzbll;
import com.google.android.gms.internal.zzblq;
import com.google.android.gms.internal.zzbme;
import com.google.android.gms.internal.zzbmo;
import com.google.android.gms.internal.zzbmu;
import com.google.android.gms.internal.zzbos;
import java.util.Arrays;
import java.util.Set;

public final class Drive {
    @Deprecated
    public static final Api<Api.ApiOptions.NoOptions> API = new Api<>("Drive.API", zzdyi, zzdyh);
    @Deprecated
    public static final DriveApi DriveApi = new zzbkp();
    @Deprecated
    public static final DrivePreferencesApi DrivePreferencesApi = new zzbmo();
    public static final Scope SCOPE_APPFOLDER = new Scope(Scopes.DRIVE_APPFOLDER);
    public static final Scope SCOPE_FILE = new Scope(Scopes.DRIVE_FILE);
    public static final Api.zzf<zzbll> zzdyh = new Api.zzf<>();
    private static final Api.zza<zzbll, Api.ApiOptions.NoOptions> zzdyi = new zzf();
    private static final Api.zza<zzbll, zzb> zzggd = new zzg();
    private static final Api.zza<zzbll, zza> zzgge = new zzh();
    private static Scope zzggf = new Scope("https://www.googleapis.com/auth/drive");
    private static Scope zzggg = new Scope("https://www.googleapis.com/auth/drive.apps");
    private static Api<zzb> zzggh = new Api<>("Drive.INTERNAL_API", zzggd, zzdyh);
    public static final Api<zza> zzggi = new Api<>("Drive.API_CONNECTIONLESS", zzgge, zzdyh);
    private static zzk zzggj = new zzbme();
    private static zzm zzggk = new zzbos();

    public static class zza implements Api.ApiOptions.HasGoogleSignInAccountOptions {
        private final Bundle zzggl = new Bundle();
        private final GoogleSignInAccount zzggm;

        public zza(@NonNull GoogleSignInAccount googleSignInAccount) {
            this.zzggm = googleSignInAccount;
        }

        public final boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj == null || obj.getClass() != getClass()) {
                return false;
            }
            zza zza = (zza) obj;
            if (!this.zzggm.equals(zza.getGoogleSignInAccount())) {
                return false;
            }
            String string = this.zzggl.getString("method_trace_filename");
            String string2 = zza.zzggl.getString("method_trace_filename");
            return ((string == null && string2 == null) || !(string == null || string2 == null || !string.equals(string2))) && this.zzggl.getBoolean("bypass_initial_sync") == zza.zzggl.getBoolean("bypass_initial_sync") && this.zzggl.getInt("proxy_type") == zza.zzggl.getInt("proxy_type");
        }

        public final GoogleSignInAccount getGoogleSignInAccount() {
            return this.zzggm;
        }

        public final int hashCode() {
            return Arrays.hashCode(new Object[]{this.zzggm, this.zzggl.getString("method_trace_filename", ""), Integer.valueOf(this.zzggl.getInt("proxy_type")), Boolean.valueOf(this.zzggl.getBoolean("bypass_initial_sync"))});
        }

        public final Bundle zzann() {
            return this.zzggl;
        }
    }

    public static class zzb implements Api.ApiOptions.Optional {
    }

    private Drive() {
    }

    public static DriveClient getDriveClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zza(googleSignInAccount);
        return new zzblq(activity, new zza(googleSignInAccount));
    }

    public static DriveClient getDriveClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zza(googleSignInAccount);
        return new zzblq(context, new zza(googleSignInAccount));
    }

    public static DriveResourceClient getDriveResourceClient(@NonNull Activity activity, @NonNull GoogleSignInAccount googleSignInAccount) {
        zza(googleSignInAccount);
        return new zzbmu(activity, new zza(googleSignInAccount));
    }

    public static DriveResourceClient getDriveResourceClient(@NonNull Context context, @NonNull GoogleSignInAccount googleSignInAccount) {
        zza(googleSignInAccount);
        return new zzbmu(context, new zza(googleSignInAccount));
    }

    private static void zza(GoogleSignInAccount googleSignInAccount) {
        zzbq.checkNotNull(googleSignInAccount);
        Set<Scope> grantedScopes = googleSignInAccount.getGrantedScopes();
        zzbq.checkArgument(grantedScopes.contains(SCOPE_FILE) || grantedScopes.contains(SCOPE_APPFOLDER) || grantedScopes.contains(zzggf) || grantedScopes.contains(zzggg), "You must request a Drive scope in order to interact with the Drive API.");
    }
}
