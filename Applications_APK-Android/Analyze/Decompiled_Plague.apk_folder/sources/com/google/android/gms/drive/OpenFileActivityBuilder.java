package com.google.android.gms.drive;

import android.content.IntentSender;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.drive.query.Filter;
import com.google.android.gms.drive.query.internal.FilterHolder;
import com.google.android.gms.drive.query.internal.zzk;
import com.google.android.gms.internal.zzbll;
import com.google.android.gms.internal.zzbpf;
import com.google.android.gms.internal.zzbqx;

@Deprecated
public class OpenFileActivityBuilder {
    public static final String EXTRA_RESPONSE_DRIVE_ID = "response_drive_id";
    private String zzejt;
    private String[] zzghl;
    private Filter zzghm;
    private DriveId zzghn;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public IntentSender build(GoogleApiClient googleApiClient) {
        zzbq.zza(googleApiClient.isConnected(), (Object) "Client must be connected");
        zzanm();
        try {
            return ((zzbpf) ((zzbll) googleApiClient.zza(Drive.zzdyh)).zzakb()).zza(new zzbqx(this.zzejt, this.zzghl, this.zzghn, this.zzghm == null ? null : new FilterHolder(this.zzghm)));
        } catch (RemoteException e) {
            throw new RuntimeException("Unable to connect Drive Play Service", e);
        }
    }

    /* access modifiers changed from: package-private */
    public final String getTitle() {
        return this.zzejt;
    }

    public OpenFileActivityBuilder setActivityStartFolder(DriveId driveId) {
        this.zzghn = (DriveId) zzbq.checkNotNull(driveId);
        return this;
    }

    public OpenFileActivityBuilder setActivityTitle(String str) {
        this.zzejt = (String) zzbq.checkNotNull(str);
        return this;
    }

    public OpenFileActivityBuilder setMimeType(String[] strArr) {
        zzbq.checkArgument(strArr != null, "mimeTypes may not be null");
        this.zzghl = strArr;
        return this;
    }

    public OpenFileActivityBuilder setSelectionFilter(Filter filter) {
        zzbq.checkArgument(filter != null, "filter may not be null");
        zzbq.checkArgument(true ^ zzk.zza(filter), "FullTextSearchFilter cannot be used as a selection filter");
        this.zzghm = filter;
        return this;
    }

    /* access modifiers changed from: package-private */
    public final void zzanm() {
        if (this.zzghl == null) {
            this.zzghl = new String[0];
        }
        if (this.zzghl.length > 0 && this.zzghm != null) {
            throw new IllegalStateException("Cannot use a selection filter and set mimetypes simultaneously");
        }
    }

    /* access modifiers changed from: package-private */
    public final String[] zzaob() {
        return this.zzghl;
    }

    /* access modifiers changed from: package-private */
    public final Filter zzaoc() {
        return this.zzghm;
    }

    /* access modifiers changed from: package-private */
    public final DriveId zzaod() {
        return this.zzghn;
    }
}
