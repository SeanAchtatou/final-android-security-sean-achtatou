package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class zzdrz implements zzdoo {
    private final SecretKeySpec zzltz;

    public zzdrz(byte[] bArr) {
        this.zzltz = new SecretKeySpec(bArr, "AES");
    }

    public final byte[] zzd(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        if (bArr.length > 2147483619) {
            throw new GeneralSecurityException("plaintext too long");
        }
        byte[] bArr3 = new byte[(bArr.length + 12 + 16)];
        byte[] zzgb = zzdtd.zzgb(12);
        System.arraycopy(zzgb, 0, bArr3, 0, 12);
        Cipher zzoc = zzdsr.zzlvk.zzoc("AES/GCM/NoPadding");
        zzoc.init(1, this.zzltz, new GCMParameterSpec(128, zzgb));
        zzoc.updateAAD(bArr2);
        zzoc.doFinal(bArr, 0, bArr.length, bArr3, 12);
        return bArr3;
    }
}
