package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzci implements zzbo<TurnBasedMultiplayer.LeaveMatchResult, Void> {
    zzci() {
    }

    public final /* bridge */ /* synthetic */ Object zzb(@Nullable Result result) {
        return null;
    }
}
