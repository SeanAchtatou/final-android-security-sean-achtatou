package com.google.android.gms.common.data;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.internal.zzbq;
import java.util.Arrays;

public class zzc {
    protected final DataHolder zzfnz;
    protected int zzftd;
    private int zzfte;

    public zzc(DataHolder dataHolder, int i) {
        this.zzfnz = (DataHolder) zzbq.checkNotNull(dataHolder);
        zzbx(i);
    }

    public boolean equals(Object obj) {
        if (obj instanceof zzc) {
            zzc zzc = (zzc) obj;
            return zzbg.equal(Integer.valueOf(zzc.zzftd), Integer.valueOf(this.zzftd)) && zzbg.equal(Integer.valueOf(zzc.zzfte), Integer.valueOf(this.zzfte)) && zzc.zzfnz == this.zzfnz;
        }
    }

    /* access modifiers changed from: protected */
    public final boolean getBoolean(String str) {
        return this.zzfnz.zze(str, this.zzftd, this.zzfte);
    }

    /* access modifiers changed from: protected */
    public final byte[] getByteArray(String str) {
        return this.zzfnz.zzg(str, this.zzftd, this.zzfte);
    }

    /* access modifiers changed from: protected */
    public final float getFloat(String str) {
        return this.zzfnz.zzf(str, this.zzftd, this.zzfte);
    }

    /* access modifiers changed from: protected */
    public final int getInteger(String str) {
        return this.zzfnz.zzc(str, this.zzftd, this.zzfte);
    }

    /* access modifiers changed from: protected */
    public final long getLong(String str) {
        return this.zzfnz.zzb(str, this.zzftd, this.zzfte);
    }

    /* access modifiers changed from: protected */
    public final String getString(String str) {
        return this.zzfnz.zzd(str, this.zzftd, this.zzfte);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.zzftd), Integer.valueOf(this.zzfte), this.zzfnz});
    }

    public boolean isDataValid() {
        return !this.zzfnz.isClosed();
    }

    /* access modifiers changed from: protected */
    public final void zza(String str, CharArrayBuffer charArrayBuffer) {
        this.zzfnz.zza(str, this.zzftd, this.zzfte, charArrayBuffer);
    }

    /* access modifiers changed from: protected */
    public final void zzbx(int i) {
        zzbq.checkState(i >= 0 && i < this.zzfnz.zzftm);
        this.zzftd = i;
        this.zzfte = this.zzfnz.zzbz(this.zzftd);
    }

    public final boolean zzfv(String str) {
        return this.zzfnz.zzfv(str);
    }

    /* access modifiers changed from: protected */
    public final Uri zzfw(String str) {
        String zzd = this.zzfnz.zzd(str, this.zzftd, this.zzfte);
        if (zzd == null) {
            return null;
        }
        return Uri.parse(zzd);
    }

    /* access modifiers changed from: protected */
    public final boolean zzfx(String str) {
        return this.zzfnz.zzh(str, this.zzftd, this.zzfte);
    }
}
