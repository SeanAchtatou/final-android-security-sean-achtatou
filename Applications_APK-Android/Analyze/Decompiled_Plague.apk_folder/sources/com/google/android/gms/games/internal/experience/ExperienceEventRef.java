package com.google.android.gms.games.internal.experience;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.Game;

public final class ExperienceEventRef extends zzc implements ExperienceEvent {
    public final int describeContents() {
        throw new NoSuchMethodError();
    }

    public final boolean equals(Object obj) {
        throw new NoSuchMethodError();
    }

    public final /* synthetic */ Object freeze() {
        throw new NoSuchMethodError();
    }

    public final Game getGame() {
        throw new NoSuchMethodError();
    }

    public final Uri getIconImageUri() {
        throw new NoSuchMethodError();
    }

    public final String getIconImageUrl() {
        return getString("icon_url");
    }

    public final int getType() {
        throw new NoSuchMethodError();
    }

    public final int hashCode() {
        throw new NoSuchMethodError();
    }

    public final String toString() {
        throw new NoSuchMethodError();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        throw new NoSuchMethodError();
    }

    public final String zzatc() {
        throw new NoSuchMethodError();
    }

    public final String zzatd() {
        throw new NoSuchMethodError();
    }

    public final String zzate() {
        throw new NoSuchMethodError();
    }

    public final long zzatf() {
        throw new NoSuchMethodError();
    }

    public final long zzatg() {
        throw new NoSuchMethodError();
    }

    public final long zzath() {
        throw new NoSuchMethodError();
    }

    public final int zzati() {
        throw new NoSuchMethodError();
    }
}
