package com.google.android.gms.internal;

import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;

final class zzadz implements Runnable {
    private /* synthetic */ Bitmap val$bitmap;
    private /* synthetic */ zzady zzcut;

    zzadz(zzady zzady, Bitmap bitmap) {
        this.zzcut = zzady;
        this.val$bitmap = bitmap;
    }

    public final void run() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.val$bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        synchronized (this.zzcut.mLock) {
            this.zzcut.zzcul.zzphz = new zzfhv();
            this.zzcut.zzcul.zzphz.zzpiu = byteArrayOutputStream.toByteArray();
            this.zzcut.zzcul.zzphz.mimeType = "image/png";
            this.zzcut.zzcul.zzphz.zzphs = 1;
        }
    }
}
