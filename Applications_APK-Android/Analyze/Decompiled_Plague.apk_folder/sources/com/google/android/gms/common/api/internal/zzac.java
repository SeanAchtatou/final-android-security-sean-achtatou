package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.internal.zzcwb;
import com.google.android.gms.internal.zzcwc;

public final class zzac<O extends Api.ApiOptions> extends GoogleApi<O> {
    private final Api.zza<? extends zzcwb, zzcwc> zzfkf;
    private final Api.zze zzfnb;
    private final zzw zzfnc;
    private final zzr zzfnd;

    public zzac(@NonNull Context context, Api<O> api, Looper looper, @NonNull Api.zze zze, @NonNull zzw zzw, zzr zzr, Api.zza<? extends zzcwb, zzcwc> zza) {
        super(context, api, looper);
        this.zzfnb = zze;
        this.zzfnc = zzw;
        this.zzfnd = zzr;
        this.zzfkf = zza;
        this.zzfjo.zza(this);
    }

    public final Api.zze zza(Looper looper, zzbr<O> zzbr) {
        this.zzfnc.zza(zzbr);
        return this.zzfnb;
    }

    public final zzcy zza(Context context, Handler handler) {
        return new zzcy(context, handler, this.zzfnd, this.zzfkf);
    }

    public final Api.zze zzahd() {
        return this.zzfnb;
    }
}
