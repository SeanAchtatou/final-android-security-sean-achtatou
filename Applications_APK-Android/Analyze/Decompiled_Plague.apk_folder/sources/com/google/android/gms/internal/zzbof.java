package com.google.android.gms.internal;

import com.google.android.gms.common.api.internal.zzco;
import com.google.android.gms.drive.events.OpenFileCallback;

final class zzbof implements zzco<OpenFileCallback> {
    private /* synthetic */ zzbnx zzgne;

    zzbof(zzbob zzbob, zzbnx zzbnx) {
        this.zzgne = zzbnx;
    }

    public final void zzahn() {
    }

    public final /* synthetic */ void zzt(Object obj) {
        this.zzgne.accept((OpenFileCallback) obj);
    }
}
