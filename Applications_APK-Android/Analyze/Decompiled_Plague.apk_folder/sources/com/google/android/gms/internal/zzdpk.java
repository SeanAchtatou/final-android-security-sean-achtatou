package com.google.android.gms.internal;

import java.security.GeneralSecurityException;

final class zzdpk implements zzdor {
    private /* synthetic */ zzdow zzlps;

    zzdpk(zzdow zzdow) {
        this.zzlps = zzdow;
    }

    public final byte[] zzd(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        return zzdte.zzc(this.zzlps.zzblf().zzblh(), ((zzdor) this.zzlps.zzblf().zzblg()).zzd(bArr, bArr2));
    }
}
