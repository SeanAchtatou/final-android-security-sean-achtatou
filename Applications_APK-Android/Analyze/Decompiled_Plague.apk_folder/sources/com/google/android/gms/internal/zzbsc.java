package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbsc extends zzfhe<zzbsc> {
    public int versionCode = 1;
    public long zzgox = -1;
    public String zzgoz = "";
    public long zzgpa = -1;
    public int zzgpb = -1;

    public zzbsc() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzbsc)) {
            return false;
        }
        zzbsc zzbsc = (zzbsc) obj;
        if (this.versionCode != zzbsc.versionCode) {
            return false;
        }
        if (this.zzgoz == null) {
            if (zzbsc.zzgoz != null) {
                return false;
            }
        } else if (!this.zzgoz.equals(zzbsc.zzgoz)) {
            return false;
        }
        if (this.zzgpa == zzbsc.zzgpa && this.zzgox == zzbsc.zzgox && this.zzgpb == zzbsc.zzgpb) {
            return (this.zzpgy == null || this.zzpgy.isEmpty()) ? zzbsc.zzpgy == null || zzbsc.zzpgy.isEmpty() : this.zzpgy.equals(zzbsc.zzpgy);
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((((527 + getClass().getName().hashCode()) * 31) + this.versionCode) * 31) + (this.zzgoz == null ? 0 : this.zzgoz.hashCode())) * 31) + ((int) (this.zzgpa ^ (this.zzgpa >>> 32)))) * 31) + ((int) (this.zzgox ^ (this.zzgox >>> 32)))) * 31) + this.zzgpb) * 31;
        if (this.zzpgy != null && !this.zzpgy.isEmpty()) {
            i = this.zzpgy.hashCode();
        }
        return hashCode + i;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 8) {
                this.versionCode = zzfhb.zzcuh();
            } else if (zzcts == 18) {
                this.zzgoz = zzfhb.readString();
            } else if (zzcts == 24) {
                long zzcum = zzfhb.zzcum();
                this.zzgpa = (zzcum >>> 1) ^ (-(zzcum & 1));
            } else if (zzcts == 32) {
                long zzcum2 = zzfhb.zzcum();
                this.zzgox = (zzcum2 >>> 1) ^ (-(zzcum2 & 1));
            } else if (zzcts == 40) {
                this.zzgpb = zzfhb.zzcuh();
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        zzfhc.zzaa(1, this.versionCode);
        zzfhc.zzn(2, this.zzgoz);
        zzfhc.zzg(3, this.zzgpa);
        zzfhc.zzg(4, this.zzgox);
        if (this.zzgpb != -1) {
            zzfhc.zzaa(5, this.zzgpb);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo() + zzfhc.zzad(1, this.versionCode) + zzfhc.zzo(2, this.zzgoz) + zzfhc.zzh(3, this.zzgpa) + zzfhc.zzh(4, this.zzgox);
        return this.zzgpb != -1 ? zzo + zzfhc.zzad(5, this.zzgpb) : zzo;
    }
}
