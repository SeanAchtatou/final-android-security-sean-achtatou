package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.internal.zzf;
import com.google.android.gms.games.multiplayer.InvitationBuffer;

public final class LoadMatchesResponse {
    private final InvitationBuffer zzhvt;
    private final TurnBasedMatchBuffer zzhvu;
    private final TurnBasedMatchBuffer zzhvv;
    private final TurnBasedMatchBuffer zzhvw;

    public LoadMatchesResponse(Bundle bundle) {
        DataHolder zzc = zzc(bundle, 0);
        if (zzc != null) {
            this.zzhvt = new InvitationBuffer(zzc);
        } else {
            this.zzhvt = null;
        }
        DataHolder zzc2 = zzc(bundle, 1);
        if (zzc2 != null) {
            this.zzhvu = new TurnBasedMatchBuffer(zzc2);
        } else {
            this.zzhvu = null;
        }
        DataHolder zzc3 = zzc(bundle, 2);
        if (zzc3 != null) {
            this.zzhvv = new TurnBasedMatchBuffer(zzc3);
        } else {
            this.zzhvv = null;
        }
        DataHolder zzc4 = zzc(bundle, 3);
        if (zzc4 != null) {
            this.zzhvw = new TurnBasedMatchBuffer(zzc4);
        } else {
            this.zzhvw = null;
        }
    }

    private static DataHolder zzc(Bundle bundle, int i) {
        String str;
        switch (i) {
            case 0:
                str = "TURN_STATUS_INVITED";
                break;
            case 1:
                str = "TURN_STATUS_MY_TURN";
                break;
            case 2:
                str = "TURN_STATUS_THEIR_TURN";
                break;
            case 3:
                str = "TURN_STATUS_COMPLETE";
                break;
            default:
                StringBuilder sb = new StringBuilder(38);
                sb.append("Unknown match turn status: ");
                sb.append(i);
                zzf.zzw("MatchTurnStatus", sb.toString());
                str = "TURN_STATUS_UNKNOWN";
                break;
        }
        if (!bundle.containsKey(str)) {
            return null;
        }
        return (DataHolder) bundle.getParcelable(str);
    }

    @Deprecated
    public final void close() {
        release();
    }

    public final TurnBasedMatchBuffer getCompletedMatches() {
        return this.zzhvw;
    }

    public final InvitationBuffer getInvitations() {
        return this.zzhvt;
    }

    public final TurnBasedMatchBuffer getMyTurnMatches() {
        return this.zzhvu;
    }

    public final TurnBasedMatchBuffer getTheirTurnMatches() {
        return this.zzhvv;
    }

    public final boolean hasData() {
        if (this.zzhvt != null && this.zzhvt.getCount() > 0) {
            return true;
        }
        if (this.zzhvu != null && this.zzhvu.getCount() > 0) {
            return true;
        }
        if (this.zzhvv == null || this.zzhvv.getCount() <= 0) {
            return this.zzhvw != null && this.zzhvw.getCount() > 0;
        }
        return true;
    }

    public final void release() {
        if (this.zzhvt != null) {
            this.zzhvt.release();
        }
        if (this.zzhvu != null) {
            this.zzhvu.release();
        }
        if (this.zzhvv != null) {
            this.zzhvv.release();
        }
        if (this.zzhvw != null) {
            this.zzhvw.release();
        }
    }
}
