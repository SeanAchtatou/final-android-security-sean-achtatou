package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.util.Arrays;

public abstract class zzdsf implements zzdoo {
    private final zzdsa zzlui;

    private zzdsf(zzdsa zzdsa) {
        this.zzlui = zzdsa;
    }

    private static void zza(byte[] bArr, long j, int i) {
        int i2 = 0;
        while (i2 < 4) {
            bArr[i + i2] = (byte) ((int) (j & 255));
            i2++;
            j >>= 8;
        }
    }

    private final byte[] zza(ByteBuffer byteBuffer, byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = bArr2;
        byte[] zza = zza(bArr, byteBuffer);
        if (bArr3.length < 32) {
            throw new IllegalArgumentException("The key length in bytes must be 32.");
        }
        long zzc = zzc(bArr3, 0, 0) & 67108863;
        int i = 2;
        int i2 = 3;
        long zzc2 = zzc(bArr3, 3, 2) & 67108611;
        long zzc3 = zzc(bArr3, 6, 4) & 67092735;
        long zzc4 = zzc(bArr3, 9, 6) & 66076671;
        long zzc5 = zzc(bArr3, 12, 8) & 1048575;
        long j = zzc2 * 5;
        long j2 = zzc3 * 5;
        long j3 = zzc4 * 5;
        long j4 = zzc5 * 5;
        byte[] bArr4 = new byte[17];
        long j5 = 0;
        int i3 = 0;
        long j6 = 0;
        long j7 = 0;
        long j8 = 0;
        long j9 = 0;
        while (i3 < zza.length) {
            int min = Math.min(16, zza.length - i3);
            System.arraycopy(zza, i3, bArr4, 0, min);
            bArr4[min] = 1;
            if (min != 16) {
                Arrays.fill(bArr4, min + 1, 17, (byte) 0);
            }
            long zzc6 = j9 + zzc(bArr4, 0, 0);
            long zzc7 = j5 + zzc(bArr4, i2, i);
            long zzc8 = j6 + zzc(bArr4, 6, 4);
            long zzc9 = j7 + zzc(bArr4, 9, 6);
            long zzc10 = j8 + (zzc(bArr4, 12, 8) | ((long) (bArr4[16] << 24)));
            long j10 = (zzc6 * zzc) + (zzc7 * j4) + (zzc8 * j3) + (zzc9 * j2) + (zzc10 * j);
            long j11 = (zzc6 * zzc2) + (zzc7 * zzc) + (zzc8 * j4) + (zzc9 * j3) + (zzc10 * j2);
            long j12 = (zzc6 * zzc3) + (zzc7 * zzc2) + (zzc8 * zzc) + (zzc9 * j4) + (zzc10 * j3);
            long j13 = (zzc6 * zzc4) + (zzc7 * zzc3) + (zzc8 * zzc2) + (zzc9 * zzc) + (zzc10 * j4);
            long j14 = (zzc6 * zzc5) + (zzc7 * zzc4) + (zzc8 * zzc3) + (zzc9 * zzc2) + (zzc10 * zzc);
            long j15 = j10 & 67108863;
            long j16 = j11 + (j10 >> 26);
            long j17 = j16 & 67108863;
            long j18 = j12 + (j16 >> 26);
            long j19 = j18 & 67108863;
            long j20 = j13 + (j18 >> 26);
            long j21 = j20 & 67108863;
            long j22 = j14 + (j20 >> 26);
            long j23 = j22 & 67108863;
            long j24 = j15 + ((j22 >> 26) * 5);
            long j25 = j24 & 67108863;
            long j26 = j17 + (j24 >> 26);
            i3 += 16;
            j8 = j23;
            j6 = j19;
            i = 2;
            i2 = 3;
            j9 = j25;
            j5 = j26;
            j7 = j21;
        }
        long j27 = j6 + (j5 >> 26);
        long j28 = j27 & 67108863;
        long j29 = j7 + (j27 >> 26);
        long j30 = j29 & 67108863;
        long j31 = j8 + (j29 >> 26);
        long j32 = j31 & 67108863;
        long j33 = j9 + ((j31 >> 26) * 5);
        long j34 = j33 & 67108863;
        long j35 = (j5 & 67108863) + (j33 >> 26);
        long j36 = j34 + 5;
        long j37 = j36 & 67108863;
        long j38 = j35 + (j36 >> 26);
        long j39 = j38 & 67108863;
        long j40 = j28 + (j38 >> 26);
        long j41 = j40 & 67108863;
        long j42 = j30 + (j40 >> 26);
        long j43 = j42 & 67108863;
        long j44 = (j32 + (j42 >> 26)) - 67108864;
        long j45 = j44 >> 63;
        long j46 = j34 & j45;
        long j47 = j35 & j45;
        long j48 = j28 & j45;
        long j49 = j30 & j45;
        long j50 = j32 & j45;
        long j51 = j45 ^ -1;
        long j52 = j46 | (j37 & j51);
        long j53 = j47 | (j39 & j51);
        long j54 = j48 | (j41 & j51);
        long j55 = j49 | (j43 & j51);
        long zze = ((j52 | (j53 << 26)) & 4294967295L) + zze(bArr3, 16);
        long j56 = zze & 4294967295L;
        long zze2 = (((j53 >> 6) | (j54 << 20)) & 4294967295L) + zze(bArr3, 20) + (zze >> 32);
        long j57 = zze2 & 4294967295L;
        long zze3 = (((j54 >> 12) | (j55 << 14)) & 4294967295L) + zze(bArr3, 24) + (zze2 >> 32);
        byte[] bArr5 = new byte[16];
        zza(bArr5, j56, 0);
        zza(bArr5, j57, 4);
        zza(bArr5, zze3 & 4294967295L, 8);
        zza(bArr5, ((((j55 >> 18) | ((j50 | (j44 & j51)) << 8)) & 4294967295L) + zze(bArr3, 28) + (zze3 >> 32)) & 4294967295L, 12);
        return bArr5;
    }

    public static zzdsf zzaj(byte[] bArr) {
        return new zzdsh(zzdsa.zzah(bArr));
    }

    private static long zzc(byte[] bArr, int i, int i2) {
        return (zze(bArr, i) >> i2) & 67108863;
    }

    private static long zze(byte[] bArr, int i) {
        return ((long) (((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16))) & 4294967295L;
    }

    /* access modifiers changed from: private */
    public static int zzfz(int i) {
        return (((i + 16) - 1) / 16) << 4;
    }

    /* access modifiers changed from: package-private */
    public abstract byte[] zza(byte[] bArr, ByteBuffer byteBuffer);

    public final byte[] zzd(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        ByteBuffer allocate = ByteBuffer.allocate(this.zzlui.zzbor() + bArr.length + 16);
        if (allocate.remaining() < bArr.length + this.zzlui.zzbor() + 16) {
            throw new IllegalArgumentException("Given ByteBuffer output is too small");
        }
        int position = allocate.position();
        this.zzlui.zza(allocate, bArr);
        allocate.position(position);
        byte[] bArr3 = new byte[this.zzlui.zzbor()];
        allocate.get(bArr3);
        byte[] zzfy = new zzdse(this.zzlui, bArr3, 0).zzfy(32);
        allocate.limit(allocate.limit() - 16);
        byte[] zza = zza(allocate, bArr2, zzfy);
        allocate.limit(allocate.limit() + 16);
        allocate.put(zza);
        return allocate.array();
    }
}
