package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzdm implements TurnBasedMultiplayer.LeaveMatchResult {
    private /* synthetic */ Status zzekv;

    zzdm(zzdl zzdl, Status status) {
        this.zzekv = status;
    }

    public final TurnBasedMatch getMatch() {
        return null;
    }

    public final Status getStatus() {
        return this.zzekv;
    }
}
