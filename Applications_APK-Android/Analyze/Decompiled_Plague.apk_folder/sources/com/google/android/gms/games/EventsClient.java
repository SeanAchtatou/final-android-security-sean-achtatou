package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.event.EventBuffer;
import com.google.android.gms.games.event.Events;
import com.google.android.gms.games.internal.api.zzp;
import com.google.android.gms.games.internal.zzg;
import com.google.android.gms.tasks.Task;

public class EventsClient extends zzp {
    private static final zzbo<Events.LoadEventsResult, EventBuffer> zzhgp = new zzg();

    EventsClient(@NonNull Activity activity, @NonNull Games.GamesOptions gamesOptions) {
        super(activity, gamesOptions);
    }

    EventsClient(@NonNull Context context, @NonNull Games.GamesOptions gamesOptions) {
        super(context, gamesOptions);
    }

    public void increment(@NonNull String str, @IntRange(from = 0) int i) {
        zzb(new zzf(this, str, i));
    }

    public Task<AnnotatedData<EventBuffer>> load(boolean z) {
        return zzg.zzc(Games.Events.load(zzagb(), z), zzhgp);
    }

    public Task<AnnotatedData<EventBuffer>> loadByIds(boolean z, @NonNull String... strArr) {
        return zzg.zzc(Games.Events.loadByIds(zzagb(), z, strArr), zzhgp);
    }
}
