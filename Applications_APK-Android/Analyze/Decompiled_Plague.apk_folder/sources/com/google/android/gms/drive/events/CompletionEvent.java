package com.google.android.gms.drive.events;

import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.zzal;
import com.google.android.gms.common.util.zzn;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.internal.zzbej;
import com.google.android.gms.internal.zzbem;
import com.google.android.gms.internal.zzbpm;
import com.google.android.gms.internal.zzbse;
import com.tapjoy.TapjoyConstants;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class CompletionEvent extends zzbej implements ResourceEvent {
    public static final Parcelable.Creator<CompletionEvent> CREATOR = new zzg();
    public static final int STATUS_CANCELED = 3;
    public static final int STATUS_CONFLICT = 2;
    public static final int STATUS_FAILURE = 1;
    public static final int STATUS_SUCCESS = 0;
    private static final zzal zzggp = new zzal("CompletionEvent", "");
    private int zzbzn;
    private String zzdyx;
    private DriveId zzgfy;
    private ParcelFileDescriptor zzgim;
    private ParcelFileDescriptor zzgin;
    private MetadataBundle zzgio;
    private List<String> zzgip;
    private IBinder zzgiq;
    private boolean zzgir = false;
    private boolean zzgis = false;
    private boolean zzgit = false;

    CompletionEvent(DriveId driveId, String str, ParcelFileDescriptor parcelFileDescriptor, ParcelFileDescriptor parcelFileDescriptor2, MetadataBundle metadataBundle, List<String> list, int i, IBinder iBinder) {
        this.zzgfy = driveId;
        this.zzdyx = str;
        this.zzgim = parcelFileDescriptor;
        this.zzgin = parcelFileDescriptor2;
        this.zzgio = metadataBundle;
        this.zzgip = list;
        this.zzbzn = i;
        this.zzgiq = iBinder;
    }

    private final void zzaa(boolean z) {
        zzaoe();
        this.zzgit = true;
        zzn.zza(this.zzgim);
        zzn.zza(this.zzgin);
        if (this.zzgio != null && this.zzgio.zzd(zzbse.zzgqw)) {
            ((BitmapTeleporter) this.zzgio.zza(zzbse.zzgqw)).release();
        }
        if (this.zzgiq == null) {
            zzggp.zzd("CompletionEvent", "No callback on %s", z ? "snooze" : TapjoyConstants.TJC_FULLSCREEN_AD_DISMISS_URL);
            return;
        }
        try {
            zzbpm.zzan(this.zzgiq).zzaa(z);
        } catch (RemoteException e) {
            zzggp.zzd("CompletionEvent", String.format("RemoteException on %s", z ? "snooze" : TapjoyConstants.TJC_FULLSCREEN_AD_DISMISS_URL), e);
        }
    }

    private final void zzaoe() {
        if (this.zzgit) {
            throw new IllegalStateException("Event has already been dismissed or snoozed.");
        }
    }

    public final void dismiss() {
        zzaa(false);
    }

    public final String getAccountName() {
        zzaoe();
        return this.zzdyx;
    }

    public final InputStream getBaseContentsInputStream() {
        zzaoe();
        if (this.zzgim == null) {
            return null;
        }
        if (this.zzgir) {
            throw new IllegalStateException("getBaseInputStream() can only be called once per CompletionEvent instance.");
        }
        this.zzgir = true;
        return new FileInputStream(this.zzgim.getFileDescriptor());
    }

    public final DriveId getDriveId() {
        zzaoe();
        return this.zzgfy;
    }

    public final InputStream getModifiedContentsInputStream() {
        zzaoe();
        if (this.zzgin == null) {
            return null;
        }
        if (this.zzgis) {
            throw new IllegalStateException("getModifiedInputStream() can only be called once per CompletionEvent instance.");
        }
        this.zzgis = true;
        return new FileInputStream(this.zzgin.getFileDescriptor());
    }

    public final MetadataChangeSet getModifiedMetadataChangeSet() {
        zzaoe();
        if (this.zzgio != null) {
            return new MetadataChangeSet(this.zzgio);
        }
        return null;
    }

    public final int getStatus() {
        zzaoe();
        return this.zzbzn;
    }

    public final List<String> getTrackingTags() {
        zzaoe();
        return new ArrayList(this.zzgip);
    }

    public final int getType() {
        return 2;
    }

    public final void snooze() {
        zzaa(true);
    }

    public final String toString() {
        String str;
        if (this.zzgip == null) {
            str = "<null>";
        } else {
            String join = TextUtils.join("','", this.zzgip);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("'");
            sb.append(join);
            sb.append("'");
            str = sb.toString();
        }
        return String.format(Locale.US, "CompletionEvent [id=%s, status=%s, trackingTag=%s]", this.zzgfy, Integer.valueOf(this.zzbzn), str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.DriveId, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.os.ParcelFileDescriptor, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.metadata.internal.MetadataBundle, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
     arg types: [android.os.Parcel, int, android.os.IBinder, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int i2 = i | 1;
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, (Parcelable) this.zzgfy, i2, false);
        zzbem.zza(parcel, 3, this.zzdyx, false);
        zzbem.zza(parcel, 4, (Parcelable) this.zzgim, i2, false);
        zzbem.zza(parcel, 5, (Parcelable) this.zzgin, i2, false);
        zzbem.zza(parcel, 6, (Parcelable) this.zzgio, i2, false);
        zzbem.zzb(parcel, 7, this.zzgip, false);
        zzbem.zzc(parcel, 8, this.zzbzn);
        zzbem.zza(parcel, 9, this.zzgiq, false);
        zzbem.zzai(parcel, zze);
    }
}
