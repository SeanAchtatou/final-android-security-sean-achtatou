package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class zzkk extends zzee implements zzkj {
    public zzkk() {
        attachInterface(this, "com.google.android.gms.ads.internal.client.IClientApi");
    }

    public static zzkj asInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IClientApi");
        return queryLocalInterface instanceof zzkj ? (zzkj) queryLocalInterface : new zzkl(iBinder);
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        IInterface iInterface;
        if (zza(i, parcel, parcel2, i2)) {
            return true;
        }
        switch (i) {
            case 1:
                iInterface = createBannerAdManager(IObjectWrapper.zza.zzap(parcel.readStrongBinder()), (zziw) zzef.zza(parcel, zziw.CREATOR), parcel.readString(), zzud.zzt(parcel.readStrongBinder()), parcel.readInt());
                break;
            case 2:
                iInterface = createInterstitialAdManager(IObjectWrapper.zza.zzap(parcel.readStrongBinder()), (zziw) zzef.zza(parcel, zziw.CREATOR), parcel.readString(), zzud.zzt(parcel.readStrongBinder()), parcel.readInt());
                break;
            case 3:
                iInterface = createAdLoaderBuilder(IObjectWrapper.zza.zzap(parcel.readStrongBinder()), parcel.readString(), zzud.zzt(parcel.readStrongBinder()), parcel.readInt());
                break;
            case 4:
                iInterface = getMobileAdsSettingsManager(IObjectWrapper.zza.zzap(parcel.readStrongBinder()));
                break;
            case 5:
                iInterface = createNativeAdViewDelegate(IObjectWrapper.zza.zzap(parcel.readStrongBinder()), IObjectWrapper.zza.zzap(parcel.readStrongBinder()));
                break;
            case 6:
                iInterface = createRewardedVideoAd(IObjectWrapper.zza.zzap(parcel.readStrongBinder()), zzud.zzt(parcel.readStrongBinder()), parcel.readInt());
                break;
            case 7:
                iInterface = createInAppPurchaseManager(IObjectWrapper.zza.zzap(parcel.readStrongBinder()));
                break;
            case 8:
                iInterface = createAdOverlay(IObjectWrapper.zza.zzap(parcel.readStrongBinder()));
                break;
            case 9:
                iInterface = getMobileAdsSettingsManagerWithClientJarVersion(IObjectWrapper.zza.zzap(parcel.readStrongBinder()), parcel.readInt());
                break;
            case 10:
                iInterface = createSearchAdManager(IObjectWrapper.zza.zzap(parcel.readStrongBinder()), (zziw) zzef.zza(parcel, zziw.CREATOR), parcel.readString(), parcel.readInt());
                break;
            case 11:
                iInterface = createNativeAdViewHolderDelegate(IObjectWrapper.zza.zzap(parcel.readStrongBinder()), IObjectWrapper.zza.zzap(parcel.readStrongBinder()), IObjectWrapper.zza.zzap(parcel.readStrongBinder()));
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        zzef.zza(parcel2, iInterface);
        return true;
    }
}
