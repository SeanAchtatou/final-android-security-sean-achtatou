package com.google.android.gms.games;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.achievement.Achievements;

final class zzc implements zzbo<Achievements.UpdateAchievementResult, Void> {
    zzc() {
    }

    public final /* bridge */ /* synthetic */ Object zzb(Result result) {
        return null;
    }
}
