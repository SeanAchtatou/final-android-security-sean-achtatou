package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import android.support.v4.widget.ExploreByTouchHelper;
import com.google.android.gms.ads.internal.zzbs;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@zzzb
public final class zzgo {
    private final Object mLock = new Object();
    private int zzaxx;
    private List<zzgn> zzaxy = new LinkedList();

    public final boolean zza(zzgn zzgn) {
        synchronized (this.mLock) {
            return this.zzaxy.contains(zzgn);
        }
    }

    public final boolean zzb(zzgn zzgn) {
        synchronized (this.mLock) {
            Iterator<zzgn> it = this.zzaxy.iterator();
            while (it.hasNext()) {
                zzgn next = it.next();
                if (!((Boolean) zzbs.zzep().zzd(zzmq.zzbig)).booleanValue() || zzbs.zzeg().zzos()) {
                    if (((Boolean) zzbs.zzep().zzd(zzmq.zzbii)).booleanValue() && !zzbs.zzeg().zzot() && zzgn != next && next.zzgj().equals(zzgn.zzgj())) {
                        it.remove();
                        return true;
                    }
                } else if (zzgn != next && next.zzgh().equals(zzgn.zzgh())) {
                    it.remove();
                    return true;
                }
            }
            return false;
        }
    }

    public final void zzc(zzgn zzgn) {
        synchronized (this.mLock) {
            if (this.zzaxy.size() >= 10) {
                int size = this.zzaxy.size();
                StringBuilder sb = new StringBuilder(41);
                sb.append("Queue is full, current size = ");
                sb.append(size);
                zzafj.zzbw(sb.toString());
                this.zzaxy.remove(0);
            }
            int i = this.zzaxx;
            this.zzaxx = i + 1;
            zzgn.zzo(i);
            this.zzaxy.add(zzgn);
        }
    }

    @Nullable
    public final zzgn zzgp() {
        synchronized (this.mLock) {
            zzgn zzgn = null;
            if (this.zzaxy.size() == 0) {
                zzafj.zzbw("Queue empty");
                return null;
            }
            int i = 0;
            if (this.zzaxy.size() >= 2) {
                int i2 = ExploreByTouchHelper.INVALID_ID;
                int i3 = 0;
                for (zzgn next : this.zzaxy) {
                    int score = next.getScore();
                    if (score > i2) {
                        i = i3;
                        zzgn = next;
                        i2 = score;
                    }
                    i3++;
                }
                this.zzaxy.remove(i);
                return zzgn;
            }
            zzgn zzgn2 = this.zzaxy.get(0);
            zzgn2.zzgk();
            return zzgn2;
        }
    }
}
