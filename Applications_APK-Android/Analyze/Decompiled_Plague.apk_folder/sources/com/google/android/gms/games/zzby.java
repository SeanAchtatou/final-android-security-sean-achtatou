package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.snapshot.Snapshots;

final class zzby implements zzbo<Snapshots.DeleteSnapshotResult, String> {
    zzby() {
    }

    public final /* synthetic */ Object zzb(@Nullable Result result) {
        Snapshots.DeleteSnapshotResult deleteSnapshotResult = (Snapshots.DeleteSnapshotResult) result;
        if (deleteSnapshotResult == null) {
            return null;
        }
        return deleteSnapshotResult.getSnapshotId();
    }
}
