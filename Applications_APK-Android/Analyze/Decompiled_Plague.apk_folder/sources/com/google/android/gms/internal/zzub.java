package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.ads.mediation.AdUrlAdapter;
import com.google.ads.mediation.MediationAdapter;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import com.google.android.gms.ads.mediation.customevent.CustomEventAdapter;
import com.google.android.gms.ads.mediation.customevent.CustomEventExtras;
import java.util.Map;

@zzzb
public final class zzub extends zzud {
    private Map<Class<? extends NetworkExtras>, NetworkExtras> zzcdv;

    private final <NETWORK_EXTRAS extends com.google.ads.mediation.NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> zzuf zzbh(String str) throws RemoteException {
        try {
            Class<?> cls = Class.forName(str, false, zzub.class.getClassLoader());
            if (MediationAdapter.class.isAssignableFrom(cls)) {
                MediationAdapter mediationAdapter = (MediationAdapter) cls.newInstance();
                return new zzvb(mediationAdapter, (com.google.ads.mediation.NetworkExtras) this.zzcdv.get(mediationAdapter.getAdditionalParametersType()));
            } else if (com.google.android.gms.ads.mediation.MediationAdapter.class.isAssignableFrom(cls)) {
                return new zzuw((com.google.android.gms.ads.mediation.MediationAdapter) cls.newInstance());
            } else {
                StringBuilder sb = new StringBuilder(64 + String.valueOf(str).length());
                sb.append("Could not instantiate mediation adapter: ");
                sb.append(str);
                sb.append(" (not a valid adapter).");
                zzaiw.zzco(sb.toString());
                throw new RemoteException();
            }
        } catch (Throwable unused) {
            return zzbi(str);
        }
    }

    private final zzuf zzbi(String str) throws RemoteException {
        try {
            zzaiw.zzbw("Reflection failed, retrying using direct instantiation");
            if ("com.google.ads.mediation.admob.AdMobAdapter".equals(str)) {
                return new zzuw(new AdMobAdapter());
            }
            if ("com.google.ads.mediation.AdUrlAdapter".equals(str)) {
                return new zzuw(new AdUrlAdapter());
            }
            if ("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(str)) {
                return new zzuw(new CustomEventAdapter());
            }
            if ("com.google.ads.mediation.customevent.CustomEventAdapter".equals(str)) {
                com.google.ads.mediation.customevent.CustomEventAdapter customEventAdapter = new com.google.ads.mediation.customevent.CustomEventAdapter();
                return new zzvb(customEventAdapter, (CustomEventExtras) this.zzcdv.get(customEventAdapter.getAdditionalParametersType()));
            }
            throw new RemoteException();
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder(43 + String.valueOf(str).length());
            sb.append("Could not instantiate mediation adapter: ");
            sb.append(str);
            sb.append(". ");
            zzaiw.zzc(sb.toString(), th);
        }
    }

    public final zzuf zzbf(String str) throws RemoteException {
        return zzbh(str);
    }

    public final boolean zzbg(String str) throws RemoteException {
        try {
            return CustomEvent.class.isAssignableFrom(Class.forName(str, false, zzub.class.getClassLoader()));
        } catch (Throwable unused) {
            StringBuilder sb = new StringBuilder(80 + String.valueOf(str).length());
            sb.append("Could not load custom event implementation class: ");
            sb.append(str);
            sb.append(", assuming old implementation.");
            zzaiw.zzco(sb.toString());
            return false;
        }
    }

    public final void zzm(Map<Class<? extends NetworkExtras>, NetworkExtras> map) {
        this.zzcdv = map;
    }
}
