package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.IInterface;

public abstract class zzacr extends zzee implements zzacq {
    public zzacr() {
        attachInterface(this, "com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
    }

    public static zzacq zzy(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
        return queryLocalInterface instanceof zzacq ? (zzacq) queryLocalInterface : new zzacs(iBinder);
    }

    /* JADX WARN: Type inference failed for: r3v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTransact(int r2, android.os.Parcel r3, android.os.Parcel r4, int r5) throws android.os.RemoteException {
        /*
            r1 = this;
            boolean r5 = r1.zza(r2, r3, r4, r5)
            r0 = 1
            if (r5 == 0) goto L_0x0008
            return r0
        L_0x0008:
            r5 = 34
            if (r2 == r5) goto L_0x0096
            switch(r2) {
                case 1: goto L_0x0087;
                case 2: goto L_0x0083;
                case 3: goto L_0x0063;
                default: goto L_0x000f;
            }
        L_0x000f:
            switch(r2) {
                case 5: goto L_0x0058;
                case 6: goto L_0x0054;
                case 7: goto L_0x0050;
                case 8: goto L_0x004c;
                case 9: goto L_0x0040;
                case 10: goto L_0x0034;
                case 11: goto L_0x0028;
                case 12: goto L_0x001d;
                case 13: goto L_0x0014;
                default: goto L_0x0012;
            }
        L_0x0012:
            r2 = 0
            return r2
        L_0x0014:
            java.lang.String r2 = r3.readString()
            r1.setUserId(r2)
            goto L_0x0092
        L_0x001d:
            java.lang.String r2 = r1.getMediationAdapterClassName()
            r4.writeNoException()
            r4.writeString(r2)
            return r0
        L_0x0028:
            android.os.IBinder r2 = r3.readStrongBinder()
            com.google.android.gms.dynamic.IObjectWrapper r2 = com.google.android.gms.dynamic.IObjectWrapper.zza.zzap(r2)
            r1.zzd(r2)
            goto L_0x0092
        L_0x0034:
            android.os.IBinder r2 = r3.readStrongBinder()
            com.google.android.gms.dynamic.IObjectWrapper r2 = com.google.android.gms.dynamic.IObjectWrapper.zza.zzap(r2)
            r1.zzc(r2)
            goto L_0x0092
        L_0x0040:
            android.os.IBinder r2 = r3.readStrongBinder()
            com.google.android.gms.dynamic.IObjectWrapper r2 = com.google.android.gms.dynamic.IObjectWrapper.zza.zzap(r2)
            r1.zzb(r2)
            goto L_0x0092
        L_0x004c:
            r1.destroy()
            goto L_0x0092
        L_0x0050:
            r1.resume()
            goto L_0x0092
        L_0x0054:
            r1.pause()
            goto L_0x0092
        L_0x0058:
            boolean r2 = r1.isLoaded()
            r4.writeNoException()
            com.google.android.gms.internal.zzef.zza(r4, r2)
            return r0
        L_0x0063:
            android.os.IBinder r2 = r3.readStrongBinder()
            if (r2 != 0) goto L_0x006b
            r2 = 0
            goto L_0x007f
        L_0x006b:
            java.lang.String r3 = "com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdListener"
            android.os.IInterface r3 = r2.queryLocalInterface(r3)
            boolean r5 = r3 instanceof com.google.android.gms.internal.zzacv
            if (r5 == 0) goto L_0x0079
            r2 = r3
            com.google.android.gms.internal.zzacv r2 = (com.google.android.gms.internal.zzacv) r2
            goto L_0x007f
        L_0x0079:
            com.google.android.gms.internal.zzacx r3 = new com.google.android.gms.internal.zzacx
            r3.<init>(r2)
            r2 = r3
        L_0x007f:
            r1.zza(r2)
            goto L_0x0092
        L_0x0083:
            r1.show()
            goto L_0x0092
        L_0x0087:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzadb> r2 = com.google.android.gms.internal.zzadb.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.zzef.zza(r3, r2)
            com.google.android.gms.internal.zzadb r2 = (com.google.android.gms.internal.zzadb) r2
            r1.zza(r2)
        L_0x0092:
            r4.writeNoException()
            return r0
        L_0x0096:
            boolean r2 = com.google.android.gms.internal.zzef.zza(r3)
            r1.setImmersiveMode(r2)
            goto L_0x0092
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzacr.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
