package com.google.android.gms.internal;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.zzan;

public interface zzcwb extends Api.zze {
    void connect();

    void zza(zzan zzan, boolean z);

    void zza(zzcwh zzcwh);

    void zzbcp();
}
