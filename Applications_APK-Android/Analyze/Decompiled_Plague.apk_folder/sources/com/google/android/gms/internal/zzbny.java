package com.google.android.gms.internal;

import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzdf;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.metadata.internal.zzk;
import com.google.android.gms.drive.zzo;
import com.google.android.gms.drive.zzq;
import com.google.android.gms.tasks.TaskCompletionSource;

abstract class zzbny<T> extends zzdf<zzbll, T> {
    int zzgjv;
    private final MetadataChangeSet zzgmo;
    private final DriveContents zzgmp;
    zzo zzgmq = zzaos();
    private zzk zzgmr = zzk.zzgt(this.zzgmo.getMimeType());
    MetadataChangeSet zzgms;
    int zzgmt;

    zzbny(@NonNull MetadataChangeSet metadataChangeSet, @Nullable DriveContents driveContents) {
        this.zzgmo = metadataChangeSet;
        this.zzgmp = driveContents;
        zzbmf.zzb(this.zzgmo);
        if (this.zzgmr != null && this.zzgmr.isFolder()) {
            throw new IllegalArgumentException("May not create folders using this method. Use DriveFolderManagerClient#createFolder() instead of mime type application/vnd.google-apps.folder");
        } else if (this.zzgmp == null) {
        } else {
            if (!(this.zzgmp instanceof zzblv)) {
                throw new IllegalArgumentException("Only DriveContents obtained from the Drive API are accepted.");
            } else if (this.zzgmp.getDriveId() != null) {
                throw new IllegalArgumentException("Only DriveContents obtained through DriveApi.newDriveContents are accepted for file creation.");
            } else if (this.zzgmp.zzanr()) {
                throw new IllegalArgumentException("DriveContents are already closed.");
            }
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        zzbll zzbll = (zzbll) zzb;
        this.zzgmq.zza(zzbll);
        String zzanx = this.zzgmq.zzanx();
        this.zzgms = zzanx == null ? this.zzgmo : zzbmf.zza(this.zzgmo, zzanx);
        this.zzgms.zzanz().setContext(zzbll.getContext());
        this.zzgmt = zzbmf.zza(this.zzgmp, this.zzgmr);
        this.zzgjv = (this.zzgmr == null || !this.zzgmr.zzapg()) ? 0 : 1;
        zza(zzbll, taskCompletionSource);
    }

    /* access modifiers changed from: protected */
    public abstract void zza(zzbll zzbll, TaskCompletionSource<T> taskCompletionSource) throws RemoteException;

    /* access modifiers changed from: package-private */
    public zzo zzaos() {
        return (zzo) new zzq().build();
    }
}
