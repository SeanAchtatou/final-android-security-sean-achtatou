package com.google.android.gms.tasks;

final class zzj implements Runnable {
    private /* synthetic */ Task zzkrh;
    private /* synthetic */ zzi zzkrp;

    zzj(zzi zzi, Task task) {
        this.zzkrp = zzi;
        this.zzkrh = task;
    }

    public final void run() {
        synchronized (this.zzkrp.mLock) {
            if (this.zzkrp.zzkro != null) {
                this.zzkrp.zzkro.onSuccess(this.zzkrh.getResult());
            }
        }
    }
}
