package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.InputDeviceCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.ads.internal.zzbs;
import com.mopub.mobileads.VastIconXmlManager;
import com.tapjoy.TJAdUnitConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@zzzb
public final class zzakw extends FrameLayout implements zzakt {
    private final zzali zzdef;
    private final FrameLayout zzdeg;
    private final zznd zzdeh;
    private final zzalk zzdei;
    private final long zzdej;
    @Nullable
    private zzaku zzdek;
    private boolean zzdel;
    private boolean zzdem;
    private boolean zzden;
    private boolean zzdeo;
    private long zzdep;
    private long zzdeq;
    private String zzder;
    private Bitmap zzdes;
    private ImageView zzdet;
    private boolean zzdeu;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzakw(android.content.Context r11, com.google.android.gms.internal.zzali r12, int r13, boolean r14, com.google.android.gms.internal.zznd r15, com.google.android.gms.internal.zzalh r16) {
        /*
            r10 = this;
            r0 = r10
            r8 = r11
            r0.<init>(r8)
            r3 = r12
            r0.zzdef = r3
            r6 = r15
            r0.zzdeh = r6
            android.widget.FrameLayout r1 = new android.widget.FrameLayout
            r1.<init>(r8)
            r0.zzdeg = r1
            android.widget.FrameLayout r1 = r0.zzdeg
            android.widget.FrameLayout$LayoutParams r2 = new android.widget.FrameLayout$LayoutParams
            r9 = -1
            r2.<init>(r9, r9)
            r0.addView(r1, r2)
            com.google.android.gms.ads.internal.zzv r1 = r3.zzbk()
            com.google.android.gms.common.internal.zzc.zzu(r1)
            com.google.android.gms.ads.internal.zzv r1 = r3.zzbk()
            com.google.android.gms.internal.zzakv r1 = r1.zzanr
            r2 = r8
            r4 = r13
            r5 = r14
            r7 = r16
            com.google.android.gms.internal.zzaku r1 = r1.zza(r2, r3, r4, r5, r6, r7)
            r0.zzdek = r1
            com.google.android.gms.internal.zzaku r1 = r0.zzdek
            if (r1 == 0) goto L_0x005c
            android.widget.FrameLayout r1 = r0.zzdeg
            com.google.android.gms.internal.zzaku r2 = r0.zzdek
            android.widget.FrameLayout$LayoutParams r3 = new android.widget.FrameLayout$LayoutParams
            r4 = 17
            r3.<init>(r9, r9, r4)
            r1.addView(r2, r3)
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r1 = com.google.android.gms.internal.zzmq.zzbgz
            com.google.android.gms.internal.zzmo r2 = com.google.android.gms.ads.internal.zzbs.zzep()
            java.lang.Object r1 = r2.zzd(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x005c
            r0.zzrn()
        L_0x005c:
            android.widget.ImageView r1 = new android.widget.ImageView
            r1.<init>(r8)
            r0.zzdet = r1
            com.google.android.gms.internal.zzmg<java.lang.Long> r1 = com.google.android.gms.internal.zzmq.zzbhd
            com.google.android.gms.internal.zzmo r2 = com.google.android.gms.ads.internal.zzbs.zzep()
            java.lang.Object r1 = r2.zzd(r1)
            java.lang.Long r1 = (java.lang.Long) r1
            long r1 = r1.longValue()
            r0.zzdej = r1
            com.google.android.gms.internal.zzmg<java.lang.Boolean> r1 = com.google.android.gms.internal.zzmq.zzbhb
            com.google.android.gms.internal.zzmo r2 = com.google.android.gms.ads.internal.zzbs.zzep()
            java.lang.Object r1 = r2.zzd(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            r0.zzdeo = r1
            com.google.android.gms.internal.zznd r1 = r0.zzdeh
            if (r1 == 0) goto L_0x009b
            com.google.android.gms.internal.zznd r1 = r0.zzdeh
            java.lang.String r2 = "spinner_used"
            boolean r3 = r0.zzdeo
            if (r3 == 0) goto L_0x0096
            java.lang.String r3 = "1"
            goto L_0x0098
        L_0x0096:
            java.lang.String r3 = "0"
        L_0x0098:
            r1.zzf(r2, r3)
        L_0x009b:
            com.google.android.gms.internal.zzalk r1 = new com.google.android.gms.internal.zzalk
            r1.<init>(r0)
            r0.zzdei = r1
            com.google.android.gms.internal.zzaku r1 = r0.zzdek
            if (r1 == 0) goto L_0x00ab
            com.google.android.gms.internal.zzaku r1 = r0.zzdek
            r1.zza(r0)
        L_0x00ab:
            com.google.android.gms.internal.zzaku r1 = r0.zzdek
            if (r1 != 0) goto L_0x00b6
            java.lang.String r1 = "AdVideoUnderlay Error"
            java.lang.String r2 = "Allocating player failed."
            r0.zzg(r1, r2)
        L_0x00b6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzakw.<init>(android.content.Context, com.google.android.gms.internal.zzali, int, boolean, com.google.android.gms.internal.zznd, com.google.android.gms.internal.zzalh):void");
    }

    public static void zza(zzali zzali) {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "no_video_view");
        zzali.zza("onVideoEvent", hashMap);
    }

    public static void zza(zzali zzali, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "decoderProps");
        hashMap.put("error", str);
        zzali.zza("onVideoEvent", hashMap);
    }

    public static void zza(zzali zzali, Map<String, List<Map<String, Object>>> map) {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "decoderProps");
        hashMap.put("mimeTypes", map);
        zzali.zza("onVideoEvent", hashMap);
    }

    /* access modifiers changed from: private */
    public final void zza(String str, String... strArr) {
        HashMap hashMap = new HashMap();
        hashMap.put("event", str);
        String str2 = null;
        for (String str3 : strArr) {
            if (str2 == null) {
                str2 = str3;
            } else {
                hashMap.put(str2, str3);
                str2 = null;
            }
        }
        this.zzdef.zza("onVideoEvent", hashMap);
    }

    private final boolean zzrp() {
        return this.zzdet.getParent() != null;
    }

    private final void zzrq() {
        if (this.zzdef.zzrz() != null && this.zzdem && !this.zzden) {
            this.zzdef.zzrz().getWindow().clearFlags(128);
            this.zzdem = false;
        }
    }

    public final void destroy() {
        this.zzdei.pause();
        if (this.zzdek != null) {
            this.zzdek.stop();
        }
        zzrq();
    }

    public final void onPaused() {
        zza("pause", new String[0]);
        zzrq();
        this.zzdel = false;
    }

    public final void pause() {
        if (this.zzdek != null) {
            this.zzdek.pause();
        }
    }

    public final void play() {
        if (this.zzdek != null) {
            this.zzdek.play();
        }
    }

    public final void seekTo(int i) {
        if (this.zzdek != null) {
            this.zzdek.seekTo(i);
        }
    }

    public final void zza(float f, float f2) {
        if (this.zzdek != null) {
            this.zzdek.zza(f, f2);
        }
    }

    public final void zzb(float f) {
        if (this.zzdek != null) {
            zzaku zzaku = this.zzdek;
            zzaku.zzdee.zzb(f);
            zzaku.zzrd();
        }
    }

    public final void zzcp(String str) {
        this.zzder = str;
    }

    public final void zzd(int i, int i2, int i3, int i4) {
        if (i3 != 0 && i4 != 0) {
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(i3, i4);
            layoutParams.setMargins(i, i2, 0, 0);
            this.zzdeg.setLayoutParams(layoutParams);
            requestLayout();
        }
    }

    public final void zzf(int i, int i2) {
        if (this.zzdeo) {
            int max = Math.max(i / ((Integer) zzbs.zzep().zzd(zzmq.zzbhc)).intValue(), 1);
            int max2 = Math.max(i2 / ((Integer) zzbs.zzep().zzd(zzmq.zzbhc)).intValue(), 1);
            if (this.zzdes == null || this.zzdes.getWidth() != max || this.zzdes.getHeight() != max2) {
                this.zzdes = Bitmap.createBitmap(max, max2, Bitmap.Config.ARGB_8888);
                this.zzdeu = false;
            }
        }
    }

    @TargetApi(14)
    public final void zzf(MotionEvent motionEvent) {
        if (this.zzdek != null) {
            this.zzdek.dispatchTouchEvent(motionEvent);
        }
    }

    public final void zzg(String str, @Nullable String str2) {
        zza("error", "what", str, "extra", str2);
    }

    public final void zzre() {
        this.zzdei.resume();
        zzagr.zzczc.post(new zzakx(this));
    }

    public final void zzrf() {
        if (this.zzdek != null && this.zzdeq == 0) {
            zza("canplaythrough", VastIconXmlManager.DURATION, String.valueOf(((float) this.zzdek.getDuration()) / 1000.0f), TJAdUnitConstants.String.VIDEO_WIDTH, String.valueOf(this.zzdek.getVideoWidth()), TJAdUnitConstants.String.VIDEO_HEIGHT, String.valueOf(this.zzdek.getVideoHeight()));
        }
    }

    public final void zzrg() {
        if (this.zzdef.zzrz() != null && !this.zzdem) {
            this.zzden = (this.zzdef.zzrz().getWindow().getAttributes().flags & 128) != 0;
            if (!this.zzden) {
                this.zzdef.zzrz().getWindow().addFlags(128);
                this.zzdem = true;
            }
        }
        this.zzdel = true;
    }

    public final void zzrh() {
        zza("ended", new String[0]);
        zzrq();
    }

    public final void zzri() {
        if (this.zzdeu && this.zzdes != null && !zzrp()) {
            this.zzdet.setImageBitmap(this.zzdes);
            this.zzdet.invalidate();
            this.zzdeg.addView(this.zzdet, new FrameLayout.LayoutParams(-1, -1));
            this.zzdeg.bringChildToFront(this.zzdet);
        }
        this.zzdei.pause();
        this.zzdeq = this.zzdep;
        zzagr.zzczc.post(new zzaky(this));
    }

    public final void zzrj() {
        if (this.zzdel && zzrp()) {
            this.zzdeg.removeView(this.zzdet);
        }
        if (this.zzdes != null) {
            long elapsedRealtime = zzbs.zzei().elapsedRealtime();
            if (this.zzdek.getBitmap(this.zzdes) != null) {
                this.zzdeu = true;
            }
            long elapsedRealtime2 = zzbs.zzei().elapsedRealtime() - elapsedRealtime;
            if (zzafj.zzpt()) {
                StringBuilder sb = new StringBuilder(46);
                sb.append("Spinner frame grab took ");
                sb.append(elapsedRealtime2);
                sb.append("ms");
                zzafj.v(sb.toString());
            }
            if (elapsedRealtime2 > this.zzdej) {
                zzafj.zzco("Spinner frame grab crossed jank threshold! Suspending spinner.");
                this.zzdeo = false;
                this.zzdes = null;
                if (this.zzdeh != null) {
                    this.zzdeh.zzf("spinner_jank", Long.toString(elapsedRealtime2));
                }
            }
        }
    }

    public final void zzrk() {
        if (this.zzdek != null) {
            if (!TextUtils.isEmpty(this.zzder)) {
                this.zzdek.setVideoPath(this.zzder);
            } else {
                zza("no_src", new String[0]);
            }
        }
    }

    public final void zzrl() {
        if (this.zzdek != null) {
            zzaku zzaku = this.zzdek;
            zzaku.zzdee.setMuted(true);
            zzaku.zzrd();
        }
    }

    public final void zzrm() {
        if (this.zzdek != null) {
            zzaku zzaku = this.zzdek;
            zzaku.zzdee.setMuted(false);
            zzaku.zzrd();
        }
    }

    @TargetApi(14)
    public final void zzrn() {
        if (this.zzdek != null) {
            TextView textView = new TextView(this.zzdek.getContext());
            String valueOf = String.valueOf(this.zzdek.zzqz());
            textView.setText(valueOf.length() != 0 ? "AdMob - ".concat(valueOf) : new String("AdMob - "));
            textView.setTextColor((int) SupportMenu.CATEGORY_MASK);
            textView.setBackgroundColor(InputDeviceCompat.SOURCE_ANY);
            this.zzdeg.addView(textView, new FrameLayout.LayoutParams(-2, -2, 17));
            this.zzdeg.bringChildToFront(textView);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzro() {
        if (this.zzdek != null) {
            long currentPosition = (long) this.zzdek.getCurrentPosition();
            if (this.zzdep != currentPosition && currentPosition > 0) {
                zza("timeupdate", "time", String.valueOf(((float) currentPosition) / 1000.0f));
                this.zzdep = currentPosition;
            }
        }
    }
}
