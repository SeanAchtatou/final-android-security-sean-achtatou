package com.google.android.gms.games.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.TimeUnit;

final class zzm implements PendingResult.zza {
    private /* synthetic */ PendingResult zzfym;
    private /* synthetic */ TaskCompletionSource zzfyn;
    private /* synthetic */ zzp zzhpi;
    private /* synthetic */ zzbo zzhpj;

    zzm(zzp zzp, PendingResult pendingResult, TaskCompletionSource taskCompletionSource, zzbo zzbo) {
        this.zzhpi = zzp;
        this.zzfym = pendingResult;
        this.zzfyn = taskCompletionSource;
        this.zzhpj = zzbo;
    }

    public final void zzr(@NonNull Status status) {
        if (this.zzhpi.zzag(status)) {
            this.zzfyn.setResult(this.zzhpj.zzb(this.zzfym.await(0, TimeUnit.MILLISECONDS)));
            return;
        }
        this.zzfyn.setException(zzb.zzy(zzg.zzah(status)));
    }
}
