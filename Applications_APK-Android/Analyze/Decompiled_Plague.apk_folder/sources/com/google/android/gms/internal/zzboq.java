package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.Metadata;

final class zzboq implements DriveResource.MetadataResult {
    private final Status mStatus;
    private final Metadata zzgnh;

    public zzboq(Status status, Metadata metadata) {
        this.mStatus = status;
        this.zzgnh = metadata;
    }

    public final Metadata getMetadata() {
        return this.zzgnh;
    }

    public final Status getStatus() {
        return this.mStatus;
    }
}
