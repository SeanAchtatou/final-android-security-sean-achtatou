package com.google.android.gms.internal;

import java.io.IOException;

public final class zzffe<K, V> {
    private final V value;
    private final K zzmhr;
    private final zzffg<K, V> zzpcz;

    private zzffe(zzfgr zzfgr, K k, zzfgr zzfgr2, V v) {
        this.zzpcz = new zzffg<>(zzfgr, k, zzfgr2, v);
        this.zzmhr = k;
        this.value = v;
    }

    private static <K, V> int zza(zzffg<K, V> zzffg, K k, V v) {
        return zzfeb.zza(zzffg.zzpda, 1, k) + zzfeb.zza(zzffg.zzpdc, 2, v);
    }

    public static <K, V> zzffe<K, V> zza(zzfgr zzfgr, K k, zzfgr zzfgr2, V v) {
        return new zzffe<>(zzfgr, k, zzfgr2, v);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzfeb.zza(com.google.android.gms.internal.zzfdq, com.google.android.gms.internal.zzfgr, boolean):java.lang.Object
     arg types: [com.google.android.gms.internal.zzfdq, com.google.android.gms.internal.zzfgr, int]
     candidates:
      com.google.android.gms.internal.zzfeb.zza(com.google.android.gms.internal.zzfgr, int, java.lang.Object):int
      com.google.android.gms.internal.zzfeb.zza(com.google.android.gms.internal.zzfdq, com.google.android.gms.internal.zzfgr, boolean):java.lang.Object */
    private static <T> T zza(zzfdq zzfdq, zzfea zzfea, zzfgr zzfgr, T t) throws IOException {
        switch (zzfff.zzpbr[zzfgr.ordinal()]) {
            case 1:
                zzffj zzcvg = ((zzffi) t).zzcvg();
                zzfdq.zza(zzcvg, zzfea);
                return zzcvg.zzcvl();
            case 2:
                return Integer.valueOf(zzfdq.zzcuc());
            case 3:
                throw new RuntimeException("Groups are not allowed in maps.");
            default:
                return zzfeb.zza(zzfdq, zzfgr, true);
        }
    }

    public final void zza(zzfdv zzfdv, int i, K k, V v) throws IOException {
        zzfdv.zzz(i, 2);
        zzfdv.zzkt(zza(this.zzpcz, k, v));
        zzffg<K, V> zzffg = this.zzpcz;
        zzfeb.zza(zzfdv, zzffg.zzpda, 1, k);
        zzfeb.zza(zzfdv, zzffg.zzpdc, 2, v);
    }

    public final void zza(zzffh<K, V> zzffh, zzfdq zzfdq, zzfea zzfea) throws IOException {
        int zzki = zzfdq.zzki(zzfdq.zzcuh());
        K k = this.zzpcz.zzpdb;
        V v = this.zzpcz.zzjul;
        while (true) {
            int zzcts = zzfdq.zzcts();
            if (zzcts == 0) {
                break;
            } else if (zzcts == (this.zzpcz.zzpda.zzcxd() | 8)) {
                k = zza(zzfdq, zzfea, this.zzpcz.zzpda, k);
            } else if (zzcts == (this.zzpcz.zzpdc.zzcxd() | 16)) {
                v = zza(zzfdq, zzfea, this.zzpcz.zzpdc, v);
            } else if (!zzfdq.zzkg(zzcts)) {
                break;
            }
        }
        zzfdq.zzkf(0);
        zzfdq.zzkj(zzki);
        zzffh.put(k, v);
    }

    public final int zzb(int i, K k, V v) {
        return zzfdv.zzkw(i) + zzfdv.zzld(zza(this.zzpcz, k, v));
    }
}
