package com.google.android.gms.internal;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.zzbs;
import com.tapjoy.TapjoyAuctionFlags;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzzb
public final class zzfc implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener {
    private final Context mApplicationContext;
    private Object mLock = new Object();
    private boolean zzarc = false;
    private zzail zzaui;
    private final WeakReference<zzaeu> zzauy;
    private WeakReference<ViewTreeObserver> zzauz;
    private final zzgm zzava;
    protected final zzfa zzavb;
    private final WindowManager zzavc;
    private final PowerManager zzavd;
    private final KeyguardManager zzave;
    private final DisplayMetrics zzavf;
    @Nullable
    private zzfj zzavg;
    private boolean zzavh;
    private boolean zzavi = false;
    private boolean zzavj;
    private boolean zzavk;
    private boolean zzavl;
    @Nullable
    private BroadcastReceiver zzavm;
    private final HashSet<Object> zzavn = new HashSet<>();
    private final HashSet<zzfx> zzavo = new HashSet<>();
    private final Rect zzavp = new Rect();
    private final zzff zzavq;
    private float zzavr;

    public zzfc(Context context, zziw zziw, zzaeu zzaeu, zzaiy zzaiy, zzgm zzgm) {
        this.zzauy = new WeakReference<>(zzaeu);
        this.zzava = zzgm;
        this.zzauz = new WeakReference<>(null);
        this.zzavj = true;
        this.zzavl = false;
        this.zzaui = new zzail(200);
        this.zzavb = new zzfa(UUID.randomUUID().toString(), zzaiy, zziw.zzbda, zzaeu.zzcvq, zzaeu.zzfr(), zziw.zzbdd);
        this.zzavc = (WindowManager) context.getSystemService("window");
        this.zzavd = (PowerManager) context.getApplicationContext().getSystemService("power");
        this.zzave = (KeyguardManager) context.getSystemService("keyguard");
        this.mApplicationContext = context;
        this.zzavq = new zzff(this, new Handler());
        this.mApplicationContext.getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, this.zzavq);
        this.zzavf = context.getResources().getDisplayMetrics();
        Display defaultDisplay = this.zzavc.getDefaultDisplay();
        this.zzavp.right = defaultDisplay.getWidth();
        this.zzavp.bottom = defaultDisplay.getHeight();
        zzft();
    }

    private final boolean isScreenOn() {
        return Build.VERSION.SDK_INT >= 20 ? this.zzavd.isInteractive() : this.zzavd.isScreenOn();
    }

    private static int zza(int i, DisplayMetrics displayMetrics) {
        return (int) (((float) i) / displayMetrics.density);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private final JSONObject zza(@Nullable View view, @Nullable Boolean bool) throws JSONException {
        if (view == null) {
            return zzfy().put("isAttachedToWindow", false).put("isScreenOn", isScreenOn()).put("isVisible", false);
        }
        boolean isAttachedToWindow = zzbs.zzee().isAttachedToWindow(view);
        int[] iArr = new int[2];
        int[] iArr2 = new int[2];
        try {
            view.getLocationOnScreen(iArr);
            view.getLocationInWindow(iArr2);
        } catch (Exception e) {
            zzafj.zzb("Failure getting view location.", e);
        }
        Rect rect = new Rect();
        rect.left = iArr[0];
        rect.top = iArr[1];
        rect.right = rect.left + view.getWidth();
        rect.bottom = rect.top + view.getHeight();
        Rect rect2 = new Rect();
        boolean globalVisibleRect = view.getGlobalVisibleRect(rect2, null);
        Rect rect3 = new Rect();
        boolean localVisibleRect = view.getLocalVisibleRect(rect3);
        Rect rect4 = new Rect();
        view.getHitRect(rect4);
        JSONObject zzfy = zzfy();
        zzfy.put("windowVisibility", view.getWindowVisibility()).put("isAttachedToWindow", isAttachedToWindow).put("viewBox", new JSONObject().put("top", zza(this.zzavp.top, this.zzavf)).put("bottom", zza(this.zzavp.bottom, this.zzavf)).put("left", zza(this.zzavp.left, this.zzavf)).put("right", zza(this.zzavp.right, this.zzavf))).put("adBox", new JSONObject().put("top", zza(rect.top, this.zzavf)).put("bottom", zza(rect.bottom, this.zzavf)).put("left", zza(rect.left, this.zzavf)).put("right", zza(rect.right, this.zzavf))).put("globalVisibleBox", new JSONObject().put("top", zza(rect2.top, this.zzavf)).put("bottom", zza(rect2.bottom, this.zzavf)).put("left", zza(rect2.left, this.zzavf)).put("right", zza(rect2.right, this.zzavf))).put("globalVisibleBoxVisible", globalVisibleRect).put("localVisibleBox", new JSONObject().put("top", zza(rect3.top, this.zzavf)).put("bottom", zza(rect3.bottom, this.zzavf)).put("left", zza(rect3.left, this.zzavf)).put("right", zza(rect3.right, this.zzavf))).put("localVisibleBoxVisible", localVisibleRect).put("hitBox", new JSONObject().put("top", zza(rect4.top, this.zzavf)).put("bottom", zza(rect4.bottom, this.zzavf)).put("left", zza(rect4.left, this.zzavf)).put("right", zza(rect4.right, this.zzavf))).put("screenDensity", (double) this.zzavf.density);
        if (bool == null) {
            bool = Boolean.valueOf(zzbs.zzec().zza(view, this.zzavd, this.zzave));
        }
        zzfy.put("isVisible", bool.booleanValue());
        return zzfy;
    }

    private final void zza(JSONObject jSONObject, boolean z) {
        try {
            JSONObject zzb = zzb(jSONObject);
            ArrayList arrayList = new ArrayList(this.zzavo);
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj = arrayList.get(i);
                i++;
                ((zzfx) obj).zzb(zzb, z);
            }
        } catch (Throwable th) {
            zzafj.zzb("Skipping active view message.", th);
        }
    }

    private static JSONObject zzb(JSONObject jSONObject) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject2 = new JSONObject();
        jSONArray.put(jSONObject);
        jSONObject2.put("units", jSONArray);
        return jSONObject2;
    }

    private final void zzfv() {
        if (this.zzavg != null) {
            this.zzavg.zza(this);
        }
    }

    private final void zzfx() {
        ViewTreeObserver viewTreeObserver = this.zzauz.get();
        if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
            viewTreeObserver.removeOnScrollChangedListener(this);
            viewTreeObserver.removeGlobalOnLayoutListener(this);
        }
    }

    private final JSONObject zzfy() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONObject put = jSONObject.put("afmaVersion", this.zzavb.zzfo()).put("activeViewJSON", this.zzavb.zzfp()).put("timestamp", zzbs.zzei().elapsedRealtime()).put("adFormat", this.zzavb.zzfn()).put("hashCode", this.zzavb.zzfq()).put("isMraid", this.zzavb.zzfr()).put("isStopped", this.zzavi).put("isPaused", this.zzarc).put("isNative", this.zzavb.zzfs()).put("isScreenOn", isScreenOn());
        zzbs.zzec();
        JSONObject put2 = put.put("appMuted", zzagr.zzdi());
        zzbs.zzec();
        put2.put("appVolume", (double) zzagr.zzdh()).put("deviceVolume", (double) this.zzavr);
        return jSONObject;
    }

    public final void onGlobalLayout() {
        zzl(2);
    }

    public final void onScrollChanged() {
        zzl(1);
    }

    public final void pause() {
        synchronized (this.mLock) {
            this.zzarc = true;
            zzl(3);
        }
    }

    public final void resume() {
        synchronized (this.mLock) {
            this.zzarc = false;
            zzl(3);
        }
    }

    public final void stop() {
        synchronized (this.mLock) {
            this.zzavi = true;
            zzl(3);
        }
    }

    public final void zza(zzfj zzfj) {
        synchronized (this.mLock) {
            this.zzavg = zzfj;
        }
    }

    public final void zza(zzfx zzfx) {
        if (this.zzavo.isEmpty()) {
            synchronized (this.mLock) {
                if (this.zzavm == null) {
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction("android.intent.action.SCREEN_ON");
                    intentFilter.addAction("android.intent.action.SCREEN_OFF");
                    this.zzavm = new zzfd(this);
                    this.mApplicationContext.registerReceiver(this.zzavm, intentFilter);
                }
            }
            zzl(3);
        }
        this.zzavo.add(zzfx);
        try {
            zzfx.zzb(zzb(zza(this.zzava.zzfz(), (Boolean) null)), false);
        } catch (JSONException e) {
            zzafj.zzb("Skipping measurement update for new client.", e);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzfx zzfx, Map<String, String> map) {
        String valueOf = String.valueOf(this.zzavb.zzfq());
        zzafj.zzbw(valueOf.length() != 0 ? "Received request to untrack: ".concat(valueOf) : new String("Received request to untrack: "));
        zzb(zzfx);
    }

    public final void zzb(zzfx zzfx) {
        this.zzavo.remove(zzfx);
        zzfx.zzgd();
        if (this.zzavo.isEmpty()) {
            synchronized (this.mLock) {
                zzfx();
                synchronized (this.mLock) {
                    if (this.zzavm != null) {
                        try {
                            this.mApplicationContext.unregisterReceiver(this.zzavm);
                        } catch (IllegalStateException e) {
                            zzafj.zzb("Failed trying to unregister the receiver", e);
                        } catch (Exception e2) {
                            zzbs.zzeg().zza(e2, "ActiveViewUnit.stopScreenStatusMonitoring");
                        }
                        this.zzavm = null;
                    }
                }
                this.mApplicationContext.getContentResolver().unregisterContentObserver(this.zzavq);
                int i = 0;
                this.zzavj = false;
                zzfv();
                ArrayList arrayList = new ArrayList(this.zzavo);
                int size = arrayList.size();
                while (i < size) {
                    Object obj = arrayList.get(i);
                    i++;
                    zzb((zzfx) obj);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean zzd(@Nullable Map<String, String> map) {
        if (map == null) {
            return false;
        }
        String str = map.get("hashCode");
        return !TextUtils.isEmpty(str) && str.equals(this.zzavb.zzfq());
    }

    /* access modifiers changed from: package-private */
    public final void zze(Map<String, String> map) {
        zzl(3);
    }

    /* access modifiers changed from: package-private */
    public final void zzf(Map<String, String> map) {
        if (map.containsKey("isVisible")) {
            if (!TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE.equals(map.get("isVisible"))) {
                "true".equals(map.get("isVisible"));
            }
            Iterator<Object> it = this.zzavn.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }

    public final void zzft() {
        zzbs.zzec();
        this.zzavr = zzagr.zzap(this.mApplicationContext);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzfc.zza(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.google.android.gms.internal.zzfc.zza(int, android.util.DisplayMetrics):int
      com.google.android.gms.internal.zzfc.zza(android.view.View, java.lang.Boolean):org.json.JSONObject
      com.google.android.gms.internal.zzfc.zza(com.google.android.gms.internal.zzfx, java.util.Map<java.lang.String, java.lang.String>):void
      com.google.android.gms.internal.zzfc.zza(org.json.JSONObject, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0036 A[Catch:{ JSONException -> 0x0020, RuntimeException -> 0x0019 }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003b A[Catch:{ JSONException -> 0x0020, RuntimeException -> 0x0019 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzfu() {
        /*
            r5 = this;
            java.lang.Object r0 = r5.mLock
            monitor-enter(r0)
            boolean r1 = r5.zzavj     // Catch:{ all -> 0x0046 }
            if (r1 == 0) goto L_0x0044
            r1 = 1
            r5.zzavk = r1     // Catch:{ all -> 0x0046 }
            org.json.JSONObject r2 = r5.zzfy()     // Catch:{ JSONException -> 0x0020, RuntimeException -> 0x0019 }
            java.lang.String r3 = "doneReasonCode"
            java.lang.String r4 = "u"
            r2.put(r3, r4)     // Catch:{ JSONException -> 0x0020, RuntimeException -> 0x0019 }
            r5.zza(r2, r1)     // Catch:{ JSONException -> 0x0020, RuntimeException -> 0x0019 }
            goto L_0x0024
        L_0x0019:
            r1 = move-exception
            java.lang.String r2 = "Failure while processing active view data."
        L_0x001c:
            com.google.android.gms.internal.zzafj.zzb(r2, r1)     // Catch:{ all -> 0x0046 }
            goto L_0x0024
        L_0x0020:
            r1 = move-exception
            java.lang.String r2 = "JSON failure while processing active view data."
            goto L_0x001c
        L_0x0024:
            java.lang.String r1 = "Untracking ad unit: "
            com.google.android.gms.internal.zzfa r2 = r5.zzavb     // Catch:{ all -> 0x0046 }
            java.lang.String r2 = r2.zzfq()     // Catch:{ all -> 0x0046 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x0046 }
            int r3 = r2.length()     // Catch:{ all -> 0x0046 }
            if (r3 == 0) goto L_0x003b
            java.lang.String r1 = r1.concat(r2)     // Catch:{ all -> 0x0046 }
            goto L_0x0041
        L_0x003b:
            java.lang.String r2 = new java.lang.String     // Catch:{ all -> 0x0046 }
            r2.<init>(r1)     // Catch:{ all -> 0x0046 }
            r1 = r2
        L_0x0041:
            com.google.android.gms.internal.zzafj.zzbw(r1)     // Catch:{ all -> 0x0046 }
        L_0x0044:
            monitor-exit(r0)     // Catch:{ all -> 0x0046 }
            return
        L_0x0046:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0046 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfc.zzfu():void");
    }

    public final boolean zzfw() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzavj;
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzfc.zza(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.google.android.gms.internal.zzfc.zza(int, android.util.DisplayMetrics):int
      com.google.android.gms.internal.zzfc.zza(android.view.View, java.lang.Boolean):org.json.JSONObject
      com.google.android.gms.internal.zzfc.zza(com.google.android.gms.internal.zzfx, java.util.Map<java.lang.String, java.lang.String>):void
      com.google.android.gms.internal.zzfc.zza(org.json.JSONObject, boolean):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00cd, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzl(int r8) {
        /*
            r7 = this;
            java.lang.Object r0 = r7.mLock
            monitor-enter(r0)
            java.util.HashSet<com.google.android.gms.internal.zzfx> r1 = r7.zzavo     // Catch:{ all -> 0x00ce }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x00ce }
        L_0x0009:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x00ce }
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x001f
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x00ce }
            com.google.android.gms.internal.zzfx r2 = (com.google.android.gms.internal.zzfx) r2     // Catch:{ all -> 0x00ce }
            boolean r2 = r2.zzgc()     // Catch:{ all -> 0x00ce }
            if (r2 == 0) goto L_0x0009
            r1 = r4
            goto L_0x0020
        L_0x001f:
            r1 = r3
        L_0x0020:
            if (r1 == 0) goto L_0x00cc
            boolean r1 = r7.zzavj     // Catch:{ all -> 0x00ce }
            if (r1 != 0) goto L_0x0028
            goto L_0x00cc
        L_0x0028:
            com.google.android.gms.internal.zzgm r1 = r7.zzava     // Catch:{ all -> 0x00ce }
            android.view.View r1 = r1.zzfz()     // Catch:{ all -> 0x00ce }
            if (r1 == 0) goto L_0x0040
            com.google.android.gms.internal.zzagr r2 = com.google.android.gms.ads.internal.zzbs.zzec()     // Catch:{ all -> 0x00ce }
            android.os.PowerManager r5 = r7.zzavd     // Catch:{ all -> 0x00ce }
            android.app.KeyguardManager r6 = r7.zzave     // Catch:{ all -> 0x00ce }
            boolean r2 = r2.zza(r1, r5, r6)     // Catch:{ all -> 0x00ce }
            if (r2 == 0) goto L_0x0040
            r2 = r4
            goto L_0x0041
        L_0x0040:
            r2 = r3
        L_0x0041:
            if (r1 == 0) goto L_0x0053
            if (r2 == 0) goto L_0x0053
            android.graphics.Rect r5 = new android.graphics.Rect     // Catch:{ all -> 0x00ce }
            r5.<init>()     // Catch:{ all -> 0x00ce }
            r6 = 0
            boolean r5 = r1.getGlobalVisibleRect(r5, r6)     // Catch:{ all -> 0x00ce }
            if (r5 == 0) goto L_0x0053
            r5 = r4
            goto L_0x0054
        L_0x0053:
            r5 = r3
        L_0x0054:
            com.google.android.gms.internal.zzgm r6 = r7.zzava     // Catch:{ all -> 0x00ce }
            boolean r6 = r6.zzga()     // Catch:{ all -> 0x00ce }
            if (r6 == 0) goto L_0x0061
            r7.zzfu()     // Catch:{ all -> 0x00ce }
            monitor-exit(r0)     // Catch:{ all -> 0x00ce }
            return
        L_0x0061:
            if (r8 != r4) goto L_0x0071
            com.google.android.gms.internal.zzail r6 = r7.zzaui     // Catch:{ all -> 0x00ce }
            boolean r6 = r6.tryAcquire()     // Catch:{ all -> 0x00ce }
            if (r6 != 0) goto L_0x0071
            boolean r6 = r7.zzavl     // Catch:{ all -> 0x00ce }
            if (r5 != r6) goto L_0x0071
            monitor-exit(r0)     // Catch:{ all -> 0x00ce }
            return
        L_0x0071:
            if (r5 != 0) goto L_0x007b
            boolean r6 = r7.zzavl     // Catch:{ all -> 0x00ce }
            if (r6 != 0) goto L_0x007b
            if (r8 != r4) goto L_0x007b
            monitor-exit(r0)     // Catch:{ all -> 0x00ce }
            return
        L_0x007b:
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r2)     // Catch:{ RuntimeException | JSONException -> 0x0089 }
            org.json.JSONObject r8 = r7.zza(r1, r8)     // Catch:{ RuntimeException | JSONException -> 0x0089 }
            r7.zza(r8, r3)     // Catch:{ RuntimeException | JSONException -> 0x0089 }
            r7.zzavl = r5     // Catch:{ RuntimeException | JSONException -> 0x0089 }
            goto L_0x008f
        L_0x0089:
            r8 = move-exception
            java.lang.String r1 = "Active view update failed."
            com.google.android.gms.internal.zzafj.zza(r1, r8)     // Catch:{ all -> 0x00ce }
        L_0x008f:
            com.google.android.gms.internal.zzgm r8 = r7.zzava     // Catch:{ all -> 0x00ce }
            com.google.android.gms.internal.zzgm r8 = r8.zzgb()     // Catch:{ all -> 0x00ce }
            android.view.View r8 = r8.zzfz()     // Catch:{ all -> 0x00ce }
            if (r8 == 0) goto L_0x00c7
            java.lang.ref.WeakReference<android.view.ViewTreeObserver> r1 = r7.zzauz     // Catch:{ all -> 0x00ce }
            java.lang.Object r1 = r1.get()     // Catch:{ all -> 0x00ce }
            android.view.ViewTreeObserver r1 = (android.view.ViewTreeObserver) r1     // Catch:{ all -> 0x00ce }
            android.view.ViewTreeObserver r8 = r8.getViewTreeObserver()     // Catch:{ all -> 0x00ce }
            if (r8 == r1) goto L_0x00c7
            r7.zzfx()     // Catch:{ all -> 0x00ce }
            boolean r2 = r7.zzavh     // Catch:{ all -> 0x00ce }
            if (r2 == 0) goto L_0x00b8
            if (r1 == 0) goto L_0x00c0
            boolean r1 = r1.isAlive()     // Catch:{ all -> 0x00ce }
            if (r1 == 0) goto L_0x00c0
        L_0x00b8:
            r7.zzavh = r4     // Catch:{ all -> 0x00ce }
            r8.addOnScrollChangedListener(r7)     // Catch:{ all -> 0x00ce }
            r8.addOnGlobalLayoutListener(r7)     // Catch:{ all -> 0x00ce }
        L_0x00c0:
            java.lang.ref.WeakReference r1 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x00ce }
            r1.<init>(r8)     // Catch:{ all -> 0x00ce }
            r7.zzauz = r1     // Catch:{ all -> 0x00ce }
        L_0x00c7:
            r7.zzfv()     // Catch:{ all -> 0x00ce }
            monitor-exit(r0)     // Catch:{ all -> 0x00ce }
            return
        L_0x00cc:
            monitor-exit(r0)     // Catch:{ all -> 0x00ce }
            return
        L_0x00ce:
            r8 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00ce }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfc.zzl(int):void");
    }
}
