package com.google.android.gms.games;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzcg implements zzbo<TurnBasedMultiplayer.CancelMatchResult, String> {
    zzcg() {
    }

    public final /* synthetic */ Object zzb(Result result) {
        TurnBasedMultiplayer.CancelMatchResult cancelMatchResult = (TurnBasedMultiplayer.CancelMatchResult) result;
        if (cancelMatchResult == null) {
            return null;
        }
        return cancelMatchResult.getMatchId();
    }
}
