package com.google.android.gms.internal;

import java.io.IOException;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class zzfgy extends zzfgx {
    zzfgy(String str, int i) {
        super(str, 0);
    }

    /* access modifiers changed from: package-private */
    public final Object zzb(zzfdq zzfdq) throws IOException {
        return zzfdq.readString();
    }
}
