package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zzn;

final class zzbev extends zzbep {
    private final zzn<Status> zzfzc;

    public zzbev(zzn<Status> zzn) {
        this.zzfzc = zzn;
    }

    public final void zzci(int i) throws RemoteException {
        this.zzfzc.setResult(new Status(i));
    }
}
