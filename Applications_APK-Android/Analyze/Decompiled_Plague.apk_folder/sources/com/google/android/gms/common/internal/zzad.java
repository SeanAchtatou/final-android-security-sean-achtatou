package com.google.android.gms.common.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzad implements zzg {
    private /* synthetic */ GoogleApiClient.OnConnectionFailedListener zzfxg;

    zzad(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this.zzfxg = onConnectionFailedListener;
    }

    public final void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        this.zzfxg.onConnectionFailed(connectionResult);
    }
}
