package com.google.android.gms.common.data;

import com.google.android.gms.common.internal.zzbq;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class zzb<T> implements Iterator<T> {
    protected final DataBuffer<T> zzfta;
    protected int zzftb = -1;

    public zzb(DataBuffer<T> dataBuffer) {
        this.zzfta = (DataBuffer) zzbq.checkNotNull(dataBuffer);
    }

    public boolean hasNext() {
        return this.zzftb < this.zzfta.getCount() - 1;
    }

    public T next() {
        if (!hasNext()) {
            int i = this.zzftb;
            StringBuilder sb = new StringBuilder(46);
            sb.append("Cannot advance the iterator beyond ");
            sb.append(i);
            throw new NoSuchElementException(sb.toString());
        }
        DataBuffer<T> dataBuffer = this.zzfta;
        int i2 = this.zzftb + 1;
        this.zzftb = i2;
        return dataBuffer.get(i2);
    }

    public void remove() {
        throw new UnsupportedOperationException("Cannot remove elements from a DataBufferIterator");
    }
}
