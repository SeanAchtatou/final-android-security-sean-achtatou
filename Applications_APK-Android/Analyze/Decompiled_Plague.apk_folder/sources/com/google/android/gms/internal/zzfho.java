package com.google.android.gms.internal;

import android.support.v4.view.MotionEventCompat;
import com.millennialmedia.internal.AdPlacementReporter;
import com.miniclip.input.MCInput;
import com.mopub.mobileads.resource.DrawableConstants;
import java.io.IOException;

public final class zzfho extends zzfhe<zzfho> {
    public String url = null;
    public Integer zzphs = null;
    private Integer zzpht = null;
    public String zzphu = null;
    private String zzphv = null;
    public zzfhp zzphw = null;
    public zzfhw[] zzphx = zzfhw.zzcxn();
    public String zzphy = null;
    public zzfhv zzphz = null;
    private Boolean zzpia = null;
    private String[] zzpib = zzfhn.EMPTY_STRING_ARRAY;
    private String zzpic = null;
    private Boolean zzpid = null;
    private Boolean zzpie = null;
    private byte[] zzpif = null;
    public zzfhx zzpig = null;

    public zzfho() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzao */
    public final zzfho zza(zzfhb zzfhb) throws IOException {
        int i;
        zzfhk zzfhk;
        while (true) {
            int zzcts = zzfhb.zzcts();
            switch (zzcts) {
                case 0:
                    return this;
                case 10:
                    this.url = zzfhb.readString();
                    break;
                case 18:
                    this.zzphu = zzfhb.readString();
                    break;
                case MotionEventCompat.AXIS_SCROLL:
                    this.zzphv = zzfhb.readString();
                    break;
                case MotionEventCompat.AXIS_GENERIC_3:
                    int zzb = zzfhn.zzb(zzfhb, 34);
                    int length = this.zzphx == null ? 0 : this.zzphx.length;
                    zzfhw[] zzfhwArr = new zzfhw[(zzb + length)];
                    if (length != 0) {
                        System.arraycopy(this.zzphx, 0, zzfhwArr, 0, length);
                    }
                    while (length < zzfhwArr.length - 1) {
                        zzfhwArr[length] = new zzfhw();
                        zzfhb.zza(zzfhwArr[length]);
                        zzfhb.zzcts();
                        length++;
                    }
                    zzfhwArr[length] = new zzfhw();
                    zzfhb.zza(zzfhwArr[length]);
                    this.zzphx = zzfhwArr;
                    break;
                case MotionEventCompat.AXIS_GENERIC_9:
                    this.zzpia = Boolean.valueOf(zzfhb.zzcty());
                    break;
                case 50:
                    int zzb2 = zzfhn.zzb(zzfhb, 50);
                    int length2 = this.zzpib == null ? 0 : this.zzpib.length;
                    String[] strArr = new String[(zzb2 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.zzpib, 0, strArr, 0, length2);
                    }
                    while (length2 < strArr.length - 1) {
                        strArr[length2] = zzfhb.readString();
                        zzfhb.zzcts();
                        length2++;
                    }
                    strArr[length2] = zzfhb.readString();
                    this.zzpib = strArr;
                    break;
                case 58:
                    this.zzpic = zzfhb.readString();
                    break;
                case 64:
                    this.zzpid = Boolean.valueOf(zzfhb.zzcty());
                    break;
                case DrawableConstants.GradientStrip.GRADIENT_STRIP_HEIGHT_DIPS /*72*/:
                    this.zzpie = Boolean.valueOf(zzfhb.zzcty());
                    break;
                case 80:
                    i = zzfhb.getPosition();
                    int zzctv = zzfhb.zzctv();
                    switch (zzctv) {
                        default:
                            StringBuilder sb = new StringBuilder(42);
                            sb.append(zzctv);
                            sb.append(" is not a valid enum ReportType");
                            throw new IllegalArgumentException(sb.toString());
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            this.zzphs = Integer.valueOf(zzctv);
                            continue;
                    }
                case 88:
                    i = zzfhb.getPosition();
                    try {
                        int zzctv2 = zzfhb.zzctv();
                        switch (zzctv2) {
                            default:
                                StringBuilder sb2 = new StringBuilder(39);
                                sb2.append(zzctv2);
                                sb2.append(" is not a valid enum Verdict");
                                throw new IllegalArgumentException(sb2.toString());
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                                this.zzpht = Integer.valueOf(zzctv2);
                                continue;
                        }
                    } catch (IllegalArgumentException unused) {
                        zzfhb.zzlv(i);
                        zza(zzfhb, zzcts);
                        break;
                    }
                case MCInput.KEYCODE_BUTTON_C /*98*/:
                    if (this.zzphw == null) {
                        this.zzphw = new zzfhp();
                    }
                    zzfhk = this.zzphw;
                    zzfhb.zza(zzfhk);
                    break;
                case MCInput.KEYCODE_BUTTON_THUMBL /*106*/:
                    this.zzphy = zzfhb.readString();
                    break;
                case AdPlacementReporter.PLAYLIST_REPLACED_IN_CACHE /*114*/:
                    if (this.zzphz == null) {
                        this.zzphz = new zzfhv();
                    }
                    zzfhk = this.zzphz;
                    zzfhb.zza(zzfhk);
                    break;
                case 122:
                    this.zzpif = zzfhb.readBytes();
                    break;
                case 138:
                    if (this.zzpig == null) {
                        this.zzpig = new zzfhx();
                    }
                    zzfhk = this.zzpig;
                    zzfhb.zza(zzfhk);
                    break;
                default:
                    if (super.zza(zzfhb, zzcts)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.url != null) {
            zzfhc.zzn(1, this.url);
        }
        if (this.zzphu != null) {
            zzfhc.zzn(2, this.zzphu);
        }
        if (this.zzphv != null) {
            zzfhc.zzn(3, this.zzphv);
        }
        if (this.zzphx != null && this.zzphx.length > 0) {
            for (zzfhw zzfhw : this.zzphx) {
                if (zzfhw != null) {
                    zzfhc.zza(4, zzfhw);
                }
            }
        }
        if (this.zzpia != null) {
            zzfhc.zzl(5, this.zzpia.booleanValue());
        }
        if (this.zzpib != null && this.zzpib.length > 0) {
            for (String str : this.zzpib) {
                if (str != null) {
                    zzfhc.zzn(6, str);
                }
            }
        }
        if (this.zzpic != null) {
            zzfhc.zzn(7, this.zzpic);
        }
        if (this.zzpid != null) {
            zzfhc.zzl(8, this.zzpid.booleanValue());
        }
        if (this.zzpie != null) {
            zzfhc.zzl(9, this.zzpie.booleanValue());
        }
        if (this.zzphs != null) {
            zzfhc.zzaa(10, this.zzphs.intValue());
        }
        if (this.zzpht != null) {
            zzfhc.zzaa(11, this.zzpht.intValue());
        }
        if (this.zzphw != null) {
            zzfhc.zza(12, this.zzphw);
        }
        if (this.zzphy != null) {
            zzfhc.zzn(13, this.zzphy);
        }
        if (this.zzphz != null) {
            zzfhc.zza(14, this.zzphz);
        }
        if (this.zzpif != null) {
            zzfhc.zzc(15, this.zzpif);
        }
        if (this.zzpig != null) {
            zzfhc.zza(17, this.zzpig);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.url != null) {
            zzo += zzfhc.zzo(1, this.url);
        }
        if (this.zzphu != null) {
            zzo += zzfhc.zzo(2, this.zzphu);
        }
        if (this.zzphv != null) {
            zzo += zzfhc.zzo(3, this.zzphv);
        }
        if (this.zzphx != null && this.zzphx.length > 0) {
            int i = zzo;
            for (zzfhw zzfhw : this.zzphx) {
                if (zzfhw != null) {
                    i += zzfhc.zzb(4, zzfhw);
                }
            }
            zzo = i;
        }
        if (this.zzpia != null) {
            this.zzpia.booleanValue();
            zzo += zzfhc.zzkw(5) + 1;
        }
        if (this.zzpib != null && this.zzpib.length > 0) {
            int i2 = 0;
            int i3 = 0;
            for (String str : this.zzpib) {
                if (str != null) {
                    i3++;
                    i2 += zzfhc.zztd(str);
                }
            }
            zzo = zzo + i2 + (i3 * 1);
        }
        if (this.zzpic != null) {
            zzo += zzfhc.zzo(7, this.zzpic);
        }
        if (this.zzpid != null) {
            this.zzpid.booleanValue();
            zzo += zzfhc.zzkw(8) + 1;
        }
        if (this.zzpie != null) {
            this.zzpie.booleanValue();
            zzo += zzfhc.zzkw(9) + 1;
        }
        if (this.zzphs != null) {
            zzo += zzfhc.zzad(10, this.zzphs.intValue());
        }
        if (this.zzpht != null) {
            zzo += zzfhc.zzad(11, this.zzpht.intValue());
        }
        if (this.zzphw != null) {
            zzo += zzfhc.zzb(12, this.zzphw);
        }
        if (this.zzphy != null) {
            zzo += zzfhc.zzo(13, this.zzphy);
        }
        if (this.zzphz != null) {
            zzo += zzfhc.zzb(14, this.zzphz);
        }
        if (this.zzpif != null) {
            zzo += zzfhc.zzd(15, this.zzpif);
        }
        return this.zzpig != null ? zzo + zzfhc.zzb(17, this.zzpig) : zzo;
    }
}
