package com.google.android.gms.games.snapshot;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class SnapshotMetadataEntity extends zzc implements SnapshotMetadata {
    public static final Parcelable.Creator<SnapshotMetadataEntity> CREATOR = new zzf();
    private final String zzdou;
    private final String zzejt;
    private final String zzhni;
    private final GameEntity zzhrl;
    private final Uri zzhxf;
    private final PlayerEntity zzhxi;
    private final String zzhxj;
    private final long zzhxk;
    private final long zzhxl;
    private final float zzhxm;
    private final String zzhxn;
    private final boolean zzhxo;
    private final long zzhxp;
    private final String zzhxq;

    SnapshotMetadataEntity(GameEntity gameEntity, PlayerEntity playerEntity, String str, Uri uri, String str2, String str3, String str4, long j, long j2, float f, String str5, boolean z, long j3, String str6) {
        this.zzhrl = gameEntity;
        this.zzhxi = playerEntity;
        this.zzhni = str;
        this.zzhxf = uri;
        this.zzhxj = str2;
        this.zzhxm = f;
        this.zzejt = str3;
        this.zzdou = str4;
        this.zzhxk = j;
        this.zzhxl = j2;
        this.zzhxn = str5;
        this.zzhxo = z;
        this.zzhxp = j3;
        this.zzhxq = str6;
    }

    public SnapshotMetadataEntity(SnapshotMetadata snapshotMetadata) {
        this.zzhrl = new GameEntity(snapshotMetadata.getGame());
        this.zzhxi = new PlayerEntity(snapshotMetadata.getOwner());
        this.zzhni = snapshotMetadata.getSnapshotId();
        this.zzhxf = snapshotMetadata.getCoverImageUri();
        this.zzhxj = snapshotMetadata.getCoverImageUrl();
        this.zzhxm = snapshotMetadata.getCoverImageAspectRatio();
        this.zzejt = snapshotMetadata.getTitle();
        this.zzdou = snapshotMetadata.getDescription();
        this.zzhxk = snapshotMetadata.getLastModifiedTimestamp();
        this.zzhxl = snapshotMetadata.getPlayedTime();
        this.zzhxn = snapshotMetadata.getUniqueName();
        this.zzhxo = snapshotMetadata.hasChangePending();
        this.zzhxp = snapshotMetadata.getProgressValue();
        this.zzhxq = snapshotMetadata.getDeviceName();
    }

    static int zza(SnapshotMetadata snapshotMetadata) {
        return Arrays.hashCode(new Object[]{snapshotMetadata.getGame(), snapshotMetadata.getOwner(), snapshotMetadata.getSnapshotId(), snapshotMetadata.getCoverImageUri(), Float.valueOf(snapshotMetadata.getCoverImageAspectRatio()), snapshotMetadata.getTitle(), snapshotMetadata.getDescription(), Long.valueOf(snapshotMetadata.getLastModifiedTimestamp()), Long.valueOf(snapshotMetadata.getPlayedTime()), snapshotMetadata.getUniqueName(), Boolean.valueOf(snapshotMetadata.hasChangePending()), Long.valueOf(snapshotMetadata.getProgressValue()), snapshotMetadata.getDeviceName()});
    }

    static boolean zza(SnapshotMetadata snapshotMetadata, Object obj) {
        if (!(obj instanceof SnapshotMetadata)) {
            return false;
        }
        if (snapshotMetadata == obj) {
            return true;
        }
        SnapshotMetadata snapshotMetadata2 = (SnapshotMetadata) obj;
        return zzbg.equal(snapshotMetadata2.getGame(), snapshotMetadata.getGame()) && zzbg.equal(snapshotMetadata2.getOwner(), snapshotMetadata.getOwner()) && zzbg.equal(snapshotMetadata2.getSnapshotId(), snapshotMetadata.getSnapshotId()) && zzbg.equal(snapshotMetadata2.getCoverImageUri(), snapshotMetadata.getCoverImageUri()) && zzbg.equal(Float.valueOf(snapshotMetadata2.getCoverImageAspectRatio()), Float.valueOf(snapshotMetadata.getCoverImageAspectRatio())) && zzbg.equal(snapshotMetadata2.getTitle(), snapshotMetadata.getTitle()) && zzbg.equal(snapshotMetadata2.getDescription(), snapshotMetadata.getDescription()) && zzbg.equal(Long.valueOf(snapshotMetadata2.getLastModifiedTimestamp()), Long.valueOf(snapshotMetadata.getLastModifiedTimestamp())) && zzbg.equal(Long.valueOf(snapshotMetadata2.getPlayedTime()), Long.valueOf(snapshotMetadata.getPlayedTime())) && zzbg.equal(snapshotMetadata2.getUniqueName(), snapshotMetadata.getUniqueName()) && zzbg.equal(Boolean.valueOf(snapshotMetadata2.hasChangePending()), Boolean.valueOf(snapshotMetadata.hasChangePending())) && zzbg.equal(Long.valueOf(snapshotMetadata2.getProgressValue()), Long.valueOf(snapshotMetadata.getProgressValue())) && zzbg.equal(snapshotMetadata2.getDeviceName(), snapshotMetadata.getDeviceName());
    }

    static String zzb(SnapshotMetadata snapshotMetadata) {
        return zzbg.zzw(snapshotMetadata).zzg("Game", snapshotMetadata.getGame()).zzg("Owner", snapshotMetadata.getOwner()).zzg("SnapshotId", snapshotMetadata.getSnapshotId()).zzg("CoverImageUri", snapshotMetadata.getCoverImageUri()).zzg("CoverImageUrl", snapshotMetadata.getCoverImageUrl()).zzg("CoverImageAspectRatio", Float.valueOf(snapshotMetadata.getCoverImageAspectRatio())).zzg("Description", snapshotMetadata.getDescription()).zzg("LastModifiedTimestamp", Long.valueOf(snapshotMetadata.getLastModifiedTimestamp())).zzg("PlayedTime", Long.valueOf(snapshotMetadata.getPlayedTime())).zzg("UniqueName", snapshotMetadata.getUniqueName()).zzg("ChangePending", Boolean.valueOf(snapshotMetadata.hasChangePending())).zzg("ProgressValue", Long.valueOf(snapshotMetadata.getProgressValue())).zzg("DeviceName", snapshotMetadata.getDeviceName()).toString();
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    public final SnapshotMetadata freeze() {
        return this;
    }

    public final float getCoverImageAspectRatio() {
        return this.zzhxm;
    }

    public final Uri getCoverImageUri() {
        return this.zzhxf;
    }

    public final String getCoverImageUrl() {
        return this.zzhxj;
    }

    public final String getDescription() {
        return this.zzdou;
    }

    public final void getDescription(CharArrayBuffer charArrayBuffer) {
        zzg.zzb(this.zzdou, charArrayBuffer);
    }

    public final String getDeviceName() {
        return this.zzhxq;
    }

    public final Game getGame() {
        return this.zzhrl;
    }

    public final long getLastModifiedTimestamp() {
        return this.zzhxk;
    }

    public final Player getOwner() {
        return this.zzhxi;
    }

    public final long getPlayedTime() {
        return this.zzhxl;
    }

    public final long getProgressValue() {
        return this.zzhxp;
    }

    public final String getSnapshotId() {
        return this.zzhni;
    }

    public final String getTitle() {
        return this.zzejt;
    }

    public final String getUniqueName() {
        return this.zzhxn;
    }

    public final boolean hasChangePending() {
        return this.zzhxo;
    }

    public final int hashCode() {
        return zza(this);
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String toString() {
        return zzb(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.Game, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.Player, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, (Parcelable) getGame(), i, false);
        zzbem.zza(parcel, 2, (Parcelable) getOwner(), i, false);
        zzbem.zza(parcel, 3, getSnapshotId(), false);
        zzbem.zza(parcel, 5, (Parcelable) getCoverImageUri(), i, false);
        zzbem.zza(parcel, 6, getCoverImageUrl(), false);
        zzbem.zza(parcel, 7, this.zzejt, false);
        zzbem.zza(parcel, 8, getDescription(), false);
        zzbem.zza(parcel, 9, getLastModifiedTimestamp());
        zzbem.zza(parcel, 10, getPlayedTime());
        zzbem.zza(parcel, 11, getCoverImageAspectRatio());
        zzbem.zza(parcel, 12, getUniqueName(), false);
        zzbem.zza(parcel, 13, hasChangePending());
        zzbem.zza(parcel, 14, getProgressValue());
        zzbem.zza(parcel, 15, getDeviceName(), false);
        zzbem.zzai(parcel, zze);
    }
}
