package com.google.android.gms.internal;

import android.content.SharedPreferences;
import android.util.Log;

final class zzctl extends zzctg<String> {
    zzctl(zzctn zzctn, String str, String str2) {
        super(zzctn, str, str2, null);
    }

    /* access modifiers changed from: private */
    /* renamed from: zzc */
    public final String zzb(SharedPreferences sharedPreferences) {
        try {
            return sharedPreferences.getString(this.zzjuj, null);
        } catch (ClassCastException e) {
            String valueOf = String.valueOf(this.zzjuj);
            Log.e("PhenotypeFlag", valueOf.length() != 0 ? "Invalid string value in SharedPreferences for ".concat(valueOf) : new String("Invalid string value in SharedPreferences for "), e);
            return null;
        }
    }

    public final /* synthetic */ Object zzkn(String str) {
        return str;
    }
}
