package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfif extends zzfhe<zzfif> implements Cloneable {
    private String[] zzpjw = zzfhn.EMPTY_STRING_ARRAY;
    private String[] zzpjx = zzfhn.EMPTY_STRING_ARRAY;
    private int[] zzpjy = zzfhn.zzphl;
    private long[] zzpjz = zzfhn.zzphm;
    private long[] zzpka = zzfhn.zzphm;

    public zzfif() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzcxv */
    public zzfif clone() {
        try {
            zzfif zzfif = (zzfif) super.clone();
            if (this.zzpjw != null && this.zzpjw.length > 0) {
                zzfif.zzpjw = (String[]) this.zzpjw.clone();
            }
            if (this.zzpjx != null && this.zzpjx.length > 0) {
                zzfif.zzpjx = (String[]) this.zzpjx.clone();
            }
            if (this.zzpjy != null && this.zzpjy.length > 0) {
                zzfif.zzpjy = (int[]) this.zzpjy.clone();
            }
            if (this.zzpjz != null && this.zzpjz.length > 0) {
                zzfif.zzpjz = (long[]) this.zzpjz.clone();
            }
            if (this.zzpka != null && this.zzpka.length > 0) {
                zzfif.zzpka = (long[]) this.zzpka.clone();
            }
            return zzfif;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfif)) {
            return false;
        }
        zzfif zzfif = (zzfif) obj;
        if (zzfhi.equals(this.zzpjw, zzfif.zzpjw) && zzfhi.equals(this.zzpjx, zzfif.zzpjx) && zzfhi.equals(this.zzpjy, zzfif.zzpjy) && zzfhi.equals(this.zzpjz, zzfif.zzpjz) && zzfhi.equals(this.zzpka, zzfif.zzpka)) {
            return (this.zzpgy == null || this.zzpgy.isEmpty()) ? zzfif.zzpgy == null || zzfif.zzpgy.isEmpty() : this.zzpgy.equals(zzfif.zzpgy);
        }
        return false;
    }

    public final int hashCode() {
        return ((((((((((((527 + getClass().getName().hashCode()) * 31) + zzfhi.hashCode(this.zzpjw)) * 31) + zzfhi.hashCode(this.zzpjx)) * 31) + zzfhi.hashCode(this.zzpjy)) * 31) + zzfhi.hashCode(this.zzpjz)) * 31) + zzfhi.hashCode(this.zzpka)) * 31) + ((this.zzpgy == null || this.zzpgy.isEmpty()) ? 0 : this.zzpgy.hashCode());
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        int i;
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 10) {
                int zzb = zzfhn.zzb(zzfhb, 10);
                int length = this.zzpjw == null ? 0 : this.zzpjw.length;
                String[] strArr = new String[(zzb + length)];
                if (length != 0) {
                    System.arraycopy(this.zzpjw, 0, strArr, 0, length);
                }
                while (length < strArr.length - 1) {
                    strArr[length] = zzfhb.readString();
                    zzfhb.zzcts();
                    length++;
                }
                strArr[length] = zzfhb.readString();
                this.zzpjw = strArr;
            } else if (zzcts == 18) {
                int zzb2 = zzfhn.zzb(zzfhb, 18);
                int length2 = this.zzpjx == null ? 0 : this.zzpjx.length;
                String[] strArr2 = new String[(zzb2 + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.zzpjx, 0, strArr2, 0, length2);
                }
                while (length2 < strArr2.length - 1) {
                    strArr2[length2] = zzfhb.readString();
                    zzfhb.zzcts();
                    length2++;
                }
                strArr2[length2] = zzfhb.readString();
                this.zzpjx = strArr2;
            } else if (zzcts != 24) {
                if (zzcts == 26) {
                    i = zzfhb.zzki(zzfhb.zzcuh());
                    int position = zzfhb.getPosition();
                    int i2 = 0;
                    while (zzfhb.zzcuj() > 0) {
                        zzfhb.zzctv();
                        i2++;
                    }
                    zzfhb.zzlv(position);
                    int length3 = this.zzpjy == null ? 0 : this.zzpjy.length;
                    int[] iArr = new int[(i2 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.zzpjy, 0, iArr, 0, length3);
                    }
                    while (length3 < iArr.length) {
                        iArr[length3] = zzfhb.zzctv();
                        length3++;
                    }
                    this.zzpjy = iArr;
                } else if (zzcts == 32) {
                    int zzb3 = zzfhn.zzb(zzfhb, 32);
                    int length4 = this.zzpjz == null ? 0 : this.zzpjz.length;
                    long[] jArr = new long[(zzb3 + length4)];
                    if (length4 != 0) {
                        System.arraycopy(this.zzpjz, 0, jArr, 0, length4);
                    }
                    while (length4 < jArr.length - 1) {
                        jArr[length4] = zzfhb.zzctu();
                        zzfhb.zzcts();
                        length4++;
                    }
                    jArr[length4] = zzfhb.zzctu();
                    this.zzpjz = jArr;
                } else if (zzcts == 34) {
                    i = zzfhb.zzki(zzfhb.zzcuh());
                    int position2 = zzfhb.getPosition();
                    int i3 = 0;
                    while (zzfhb.zzcuj() > 0) {
                        zzfhb.zzctu();
                        i3++;
                    }
                    zzfhb.zzlv(position2);
                    int length5 = this.zzpjz == null ? 0 : this.zzpjz.length;
                    long[] jArr2 = new long[(i3 + length5)];
                    if (length5 != 0) {
                        System.arraycopy(this.zzpjz, 0, jArr2, 0, length5);
                    }
                    while (length5 < jArr2.length) {
                        jArr2[length5] = zzfhb.zzctu();
                        length5++;
                    }
                    this.zzpjz = jArr2;
                } else if (zzcts == 40) {
                    int zzb4 = zzfhn.zzb(zzfhb, 40);
                    int length6 = this.zzpka == null ? 0 : this.zzpka.length;
                    long[] jArr3 = new long[(zzb4 + length6)];
                    if (length6 != 0) {
                        System.arraycopy(this.zzpka, 0, jArr3, 0, length6);
                    }
                    while (length6 < jArr3.length - 1) {
                        jArr3[length6] = zzfhb.zzctu();
                        zzfhb.zzcts();
                        length6++;
                    }
                    jArr3[length6] = zzfhb.zzctu();
                    this.zzpka = jArr3;
                } else if (zzcts == 42) {
                    i = zzfhb.zzki(zzfhb.zzcuh());
                    int position3 = zzfhb.getPosition();
                    int i4 = 0;
                    while (zzfhb.zzcuj() > 0) {
                        zzfhb.zzctu();
                        i4++;
                    }
                    zzfhb.zzlv(position3);
                    int length7 = this.zzpka == null ? 0 : this.zzpka.length;
                    long[] jArr4 = new long[(i4 + length7)];
                    if (length7 != 0) {
                        System.arraycopy(this.zzpka, 0, jArr4, 0, length7);
                    }
                    while (length7 < jArr4.length) {
                        jArr4[length7] = zzfhb.zzctu();
                        length7++;
                    }
                    this.zzpka = jArr4;
                } else if (!super.zza(zzfhb, zzcts)) {
                    return this;
                }
                zzfhb.zzkj(i);
            } else {
                int zzb5 = zzfhn.zzb(zzfhb, 24);
                int length8 = this.zzpjy == null ? 0 : this.zzpjy.length;
                int[] iArr2 = new int[(zzb5 + length8)];
                if (length8 != 0) {
                    System.arraycopy(this.zzpjy, 0, iArr2, 0, length8);
                }
                while (length8 < iArr2.length - 1) {
                    iArr2[length8] = zzfhb.zzctv();
                    zzfhb.zzcts();
                    length8++;
                }
                iArr2[length8] = zzfhb.zzctv();
                this.zzpjy = iArr2;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzpjw != null && this.zzpjw.length > 0) {
            for (String str : this.zzpjw) {
                if (str != null) {
                    zzfhc.zzn(1, str);
                }
            }
        }
        if (this.zzpjx != null && this.zzpjx.length > 0) {
            for (String str2 : this.zzpjx) {
                if (str2 != null) {
                    zzfhc.zzn(2, str2);
                }
            }
        }
        if (this.zzpjy != null && this.zzpjy.length > 0) {
            for (int zzaa : this.zzpjy) {
                zzfhc.zzaa(3, zzaa);
            }
        }
        if (this.zzpjz != null && this.zzpjz.length > 0) {
            for (long zzf : this.zzpjz) {
                zzfhc.zzf(4, zzf);
            }
        }
        if (this.zzpka != null && this.zzpka.length > 0) {
            for (long zzf2 : this.zzpka) {
                zzfhc.zzf(5, zzf2);
            }
        }
        super.zza(zzfhc);
    }

    public final /* synthetic */ zzfhe zzcxe() throws CloneNotSupportedException {
        return (zzfif) clone();
    }

    public final /* synthetic */ zzfhk zzcxf() throws CloneNotSupportedException {
        return (zzfif) clone();
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzpjw != null && this.zzpjw.length > 0) {
            int i = 0;
            int i2 = 0;
            for (String str : this.zzpjw) {
                if (str != null) {
                    i2++;
                    i += zzfhc.zztd(str);
                }
            }
            zzo = zzo + i + (i2 * 1);
        }
        if (this.zzpjx != null && this.zzpjx.length > 0) {
            int i3 = 0;
            int i4 = 0;
            for (String str2 : this.zzpjx) {
                if (str2 != null) {
                    i4++;
                    i3 += zzfhc.zztd(str2);
                }
            }
            zzo = zzo + i3 + (i4 * 1);
        }
        if (this.zzpjy != null && this.zzpjy.length > 0) {
            int i5 = 0;
            for (int zzkx : this.zzpjy) {
                i5 += zzfhc.zzkx(zzkx);
            }
            zzo = zzo + i5 + (this.zzpjy.length * 1);
        }
        if (this.zzpjz != null && this.zzpjz.length > 0) {
            int i6 = 0;
            for (long zzdh : this.zzpjz) {
                i6 += zzfhc.zzdh(zzdh);
            }
            zzo = zzo + i6 + (this.zzpjz.length * 1);
        }
        if (this.zzpka == null || this.zzpka.length <= 0) {
            return zzo;
        }
        int i7 = 0;
        for (long zzdh2 : this.zzpka) {
            i7 += zzfhc.zzdh(zzdh2);
        }
        return zzo + i7 + (1 * this.zzpka.length);
    }
}
