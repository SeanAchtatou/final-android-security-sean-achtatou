package com.google.android.gms.common;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.UserManager;
import android.util.Log;
import com.google.android.gms.common.util.zzq;
import com.google.android.gms.common.util.zzx;
import com.google.android.gms.internal.zzbgc;
import java.util.concurrent.atomic.AtomicBoolean;

public class zzo {
    @Deprecated
    public static final String GOOGLE_PLAY_SERVICES_PACKAGE = "com.google.android.gms";
    @Deprecated
    public static final int GOOGLE_PLAY_SERVICES_VERSION_CODE = 11720000;
    public static final String GOOGLE_PLAY_STORE_PACKAGE = "com.android.vending";
    private static boolean zzfip = false;
    private static boolean zzfiq = false;
    private static boolean zzfir = false;
    private static boolean zzfis = false;
    static final AtomicBoolean zzfit = new AtomicBoolean();
    private static final AtomicBoolean zzfiu = new AtomicBoolean();

    zzo() {
    }

    @Deprecated
    public static PendingIntent getErrorPendingIntent(int i, Context context, int i2) {
        return zze.zzafm().getErrorResolutionPendingIntent(context, i, i2);
    }

    @Deprecated
    public static String getErrorString(int i) {
        return ConnectionResult.getStatusString(i);
    }

    public static Context getRemoteContext(Context context) {
        try {
            return context.createPackageContext("com.google.android.gms", 3);
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    public static Resources getRemoteResource(Context context) {
        try {
            return context.getPackageManager().getResourcesForApplication("com.google.android.gms");
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00bf, code lost:
        if (com.google.android.gms.common.zzp.zza(r6, r8) == null) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00cc, code lost:
        if (com.google.android.gms.common.zzp.zza(r6, com.google.android.gms.common.zzj.zzfil) == null) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00d7, code lost:
        if ((r6.versionCode / 1000) >= (com.google.android.gms.common.zzo.GOOGLE_PLAY_SERVICES_VERSION_CODE / 1000)) goto L_0x00ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00d9, code lost:
        r0 = com.google.android.gms.common.zzo.GOOGLE_PLAY_SERVICES_VERSION_CODE;
        r1 = r6.versionCode;
        r3 = new java.lang.StringBuilder(77);
        r3.append("Google Play services out of date.  Requires ");
        r3.append(r0);
        r3.append(" but found ");
        r3.append(r1);
        android.util.Log.w("GooglePlayServicesUtil", r3.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00fe, code lost:
        return 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ff, code lost:
        r8 = r6.applicationInfo;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0101, code lost:
        if (r8 != null) goto L_0x0113;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        r8 = r0.getApplicationInfo("com.google.android.gms", 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x010a, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x010b, code lost:
        android.util.Log.wtf("GooglePlayServicesUtil", "Google Play services missing when getting application info.", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0112, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0115, code lost:
        if (r8.enabled != false) goto L_0x0119;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0117, code lost:
        return 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0119, code lost:
        return 0;
     */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int isGooglePlayServicesAvailable(android.content.Context r8) {
        /*
            android.content.pm.PackageManager r0 = r8.getPackageManager()
            android.content.res.Resources r1 = r8.getResources()     // Catch:{ Throwable -> 0x000e }
            int r2 = com.google.android.gms.R.string.common_google_play_services_unknown_issue     // Catch:{ Throwable -> 0x000e }
            r1.getString(r2)     // Catch:{ Throwable -> 0x000e }
            goto L_0x0015
        L_0x000e:
            java.lang.String r1 = "GooglePlayServicesUtil"
            java.lang.String r2 = "The Google Play services resources were not found. Check your project configuration to ensure that the resources are included."
            android.util.Log.e(r1, r2)
        L_0x0015:
            java.lang.String r1 = "com.google.android.gms"
            java.lang.String r2 = r8.getPackageName()
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x0076
            java.util.concurrent.atomic.AtomicBoolean r1 = com.google.android.gms.common.zzo.zzfiu
            boolean r1 = r1.get()
            if (r1 != 0) goto L_0x0076
            int r1 = com.google.android.gms.common.internal.zzbf.zzcn(r8)
            if (r1 != 0) goto L_0x0037
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "A required meta-data tag in your app's AndroidManifest.xml does not exist.  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />"
            r8.<init>(r0)
            throw r8
        L_0x0037:
            int r2 = com.google.android.gms.common.zzo.GOOGLE_PLAY_SERVICES_VERSION_CODE
            if (r1 == r2) goto L_0x0076
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            int r0 = com.google.android.gms.common.zzo.GOOGLE_PLAY_SERVICES_VERSION_CODE
            java.lang.String r2 = "com.google.android.gms.version"
            r3 = 290(0x122, float:4.06E-43)
            java.lang.String r4 = java.lang.String.valueOf(r2)
            int r4 = r4.length()
            int r3 = r3 + r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>(r3)
            java.lang.String r3 = "The meta-data tag in your app's AndroidManifest.xml does not have the right value.  Expected "
            r4.append(r3)
            r4.append(r0)
            java.lang.String r0 = " but found "
            r4.append(r0)
            r4.append(r1)
            java.lang.String r0 = ".  You must have the following declaration within the <application> element:     <meta-data android:name=\""
            r4.append(r0)
            r4.append(r2)
            java.lang.String r0 = "\" android:value=\"@integer/google_play_services_version\" />"
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r8.<init>(r0)
            throw r8
        L_0x0076:
            boolean r1 = com.google.android.gms.common.util.zzi.zzcq(r8)
            r2 = 0
            r3 = 1
            if (r1 != 0) goto L_0x0086
            boolean r1 = com.google.android.gms.common.util.zzi.zzcs(r8)
            if (r1 != 0) goto L_0x0086
            r1 = r3
            goto L_0x0087
        L_0x0086:
            r1 = r2
        L_0x0087:
            r4 = 0
            r5 = 9
            if (r1 == 0) goto L_0x009d
            java.lang.String r4 = "com.android.vending"
            r6 = 8256(0x2040, float:1.1569E-41)
            android.content.pm.PackageInfo r4 = r0.getPackageInfo(r4, r6)     // Catch:{ NameNotFoundException -> 0x0095 }
            goto L_0x009d
        L_0x0095:
            java.lang.String r8 = "GooglePlayServicesUtil"
            java.lang.String r0 = "Google Play Store is missing."
        L_0x0099:
            android.util.Log.w(r8, r0)
            return r5
        L_0x009d:
            java.lang.String r6 = "com.google.android.gms"
            r7 = 64
            android.content.pm.PackageInfo r6 = r0.getPackageInfo(r6, r7)     // Catch:{ NameNotFoundException -> 0x011a }
            com.google.android.gms.common.zzp.zzcg(r8)
            if (r1 == 0) goto L_0x00c6
            com.google.android.gms.common.zzg[] r8 = com.google.android.gms.common.zzj.zzfil
            com.google.android.gms.common.zzg r8 = com.google.android.gms.common.zzp.zza(r4, r8)
            if (r8 != 0) goto L_0x00b7
            java.lang.String r8 = "GooglePlayServicesUtil"
            java.lang.String r0 = "Google Play Store signature invalid."
            goto L_0x0099
        L_0x00b7:
            com.google.android.gms.common.zzg[] r1 = new com.google.android.gms.common.zzg[r3]
            r1[r2] = r8
            com.google.android.gms.common.zzg r8 = com.google.android.gms.common.zzp.zza(r6, r1)
            if (r8 != 0) goto L_0x00cf
        L_0x00c1:
            java.lang.String r8 = "GooglePlayServicesUtil"
            java.lang.String r0 = "Google Play services signature invalid."
            goto L_0x0099
        L_0x00c6:
            com.google.android.gms.common.zzg[] r8 = com.google.android.gms.common.zzj.zzfil
            com.google.android.gms.common.zzg r8 = com.google.android.gms.common.zzp.zza(r6, r8)
            if (r8 != 0) goto L_0x00cf
            goto L_0x00c1
        L_0x00cf:
            int r8 = com.google.android.gms.common.zzo.GOOGLE_PLAY_SERVICES_VERSION_CODE
            int r8 = r8 / 1000
            int r1 = r6.versionCode
            int r1 = r1 / 1000
            if (r1 >= r8) goto L_0x00ff
            java.lang.String r8 = "GooglePlayServicesUtil"
            int r0 = com.google.android.gms.common.zzo.GOOGLE_PLAY_SERVICES_VERSION_CODE
            int r1 = r6.versionCode
            r2 = 77
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r2 = "Google Play services out of date.  Requires "
            r3.append(r2)
            r3.append(r0)
            java.lang.String r0 = " but found "
            r3.append(r0)
            r3.append(r1)
            java.lang.String r0 = r3.toString()
            android.util.Log.w(r8, r0)
            r8 = 2
            return r8
        L_0x00ff:
            android.content.pm.ApplicationInfo r8 = r6.applicationInfo
            if (r8 != 0) goto L_0x0113
            java.lang.String r8 = "com.google.android.gms"
            android.content.pm.ApplicationInfo r8 = r0.getApplicationInfo(r8, r2)     // Catch:{ NameNotFoundException -> 0x010a }
            goto L_0x0113
        L_0x010a:
            r8 = move-exception
            java.lang.String r0 = "GooglePlayServicesUtil"
            java.lang.String r1 = "Google Play services missing when getting application info."
            android.util.Log.wtf(r0, r1, r8)
            return r3
        L_0x0113:
            boolean r8 = r8.enabled
            if (r8 != 0) goto L_0x0119
            r8 = 3
            return r8
        L_0x0119:
            return r2
        L_0x011a:
            java.lang.String r8 = "GooglePlayServicesUtil"
            java.lang.String r0 = "Google Play services is missing."
            android.util.Log.w(r8, r0)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.zzo.isGooglePlayServicesAvailable(android.content.Context):int");
    }

    @Deprecated
    public static boolean isUserRecoverableError(int i) {
        if (i == 9) {
            return true;
        }
        switch (i) {
            case 1:
            case 2:
            case 3:
                return true;
            default:
                return false;
        }
    }

    @TargetApi(19)
    @Deprecated
    public static boolean zzb(Context context, int i, String str) {
        return zzx.zzb(context, i, str);
    }

    @Deprecated
    public static void zzbn(Context context) throws GooglePlayServicesRepairableException, GooglePlayServicesNotAvailableException {
        int isGooglePlayServicesAvailable = zze.zzafm().isGooglePlayServicesAvailable(context);
        if (isGooglePlayServicesAvailable != 0) {
            zze.zzafm();
            Intent zza = zze.zza(context, isGooglePlayServicesAvailable, "e");
            StringBuilder sb = new StringBuilder(57);
            sb.append("GooglePlayServices not available due to error ");
            sb.append(isGooglePlayServicesAvailable);
            Log.e("GooglePlayServicesUtil", sb.toString());
            if (zza == null) {
                throw new GooglePlayServicesNotAvailableException(isGooglePlayServicesAvailable);
            }
            throw new GooglePlayServicesRepairableException(isGooglePlayServicesAvailable, "Google Play Services not available", zza);
        }
    }

    @Deprecated
    public static void zzcc(Context context) {
        if (!zzfit.getAndSet(true)) {
            try {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
                if (notificationManager != null) {
                    notificationManager.cancel(10436);
                }
            } catch (SecurityException unused) {
            }
        }
    }

    @Deprecated
    public static int zzcd(Context context) {
        try {
            return context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode;
        } catch (PackageManager.NameNotFoundException unused) {
            Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            return 0;
        }
    }

    public static boolean zzcf(Context context) {
        if (!zzfis) {
            try {
                PackageInfo packageInfo = zzbgc.zzcy(context).getPackageInfo("com.google.android.gms", 64);
                if (packageInfo != null) {
                    zzp.zzcg(context);
                    if (zzp.zza(packageInfo, zzj.zzfil[1]) != null) {
                        zzfir = true;
                        zzfis = true;
                    }
                }
                zzfir = false;
            } catch (PackageManager.NameNotFoundException e) {
                Log.w("GooglePlayServicesUtil", "Cannot find Google Play services package name.", e);
            } catch (Throwable th) {
                zzfis = true;
                throw th;
            }
            zzfis = true;
        }
        return zzfir || !"user".equals(Build.TYPE);
    }

    @Deprecated
    public static boolean zze(Context context, int i) {
        if (i == 18) {
            return true;
        }
        if (i == 1) {
            return zzv(context, "com.google.android.gms");
        }
        return false;
    }

    @Deprecated
    public static boolean zzf(Context context, int i) {
        return zzx.zzf(context, i);
    }

    @TargetApi(21)
    static boolean zzv(Context context, String str) {
        Bundle applicationRestrictions;
        boolean equals = str.equals("com.google.android.gms");
        if (zzq.zzamb()) {
            try {
                for (PackageInstaller.SessionInfo appPackageName : context.getPackageManager().getPackageInstaller().getAllSessions()) {
                    if (str.equals(appPackageName.getAppPackageName())) {
                        return true;
                    }
                }
            } catch (Exception unused) {
                return false;
            }
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(str, 8192);
            if (equals) {
                return applicationInfo.enabled;
            }
            if (applicationInfo.enabled) {
                if (!(zzq.zzaly() && (applicationRestrictions = ((UserManager) context.getSystemService("user")).getApplicationRestrictions(context.getPackageName())) != null && "true".equals(applicationRestrictions.getString("restricted_profile")))) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused2) {
        }
    }
}
