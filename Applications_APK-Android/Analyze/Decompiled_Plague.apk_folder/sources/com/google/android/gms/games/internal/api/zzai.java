package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.zzn;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.leaderboard.Leaderboards;

final class zzai extends zzap {
    private /* synthetic */ boolean zzhpv;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzai(zzah zzah, GoogleApiClient googleApiClient, boolean z) {
        super(googleApiClient, null);
        this.zzhpv = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zzb((zzn<Leaderboards.LeaderboardMetadataResult>) this, this.zzhpv);
    }
}
