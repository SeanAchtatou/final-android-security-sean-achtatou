package com.google.android.gms.games;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.games.video.Videos;

final class zzdb implements zzbo<Videos.CaptureCapabilitiesResult, VideoCapabilities> {
    zzdb() {
    }

    public final /* synthetic */ Object zzb(Result result) {
        Videos.CaptureCapabilitiesResult captureCapabilitiesResult = (Videos.CaptureCapabilitiesResult) result;
        if (captureCapabilitiesResult == null) {
            return null;
        }
        return captureCapabilitiesResult.getCapabilities();
    }
}
