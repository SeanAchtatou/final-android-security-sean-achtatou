package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.zzp;

public final class zzbre implements Parcelable.Creator<zzbrd> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        DriveId driveId = null;
        int i = 0;
        zzp zzp = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    driveId = (DriveId) zzbek.zza(parcel, readInt, DriveId.CREATOR);
                    break;
                case 3:
                    i = zzbek.zzg(parcel, readInt);
                    break;
                case 4:
                    zzp = (zzp) zzbek.zza(parcel, readInt, zzp.CREATOR);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbrd(driveId, i, zzp);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbrd[i];
    }
}
