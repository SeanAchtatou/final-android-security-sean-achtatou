package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@zzzb
public abstract class zzgz {
    @Nullable
    private static MessageDigest zzazi;
    protected Object mLock = new Object();

    /* access modifiers changed from: protected */
    @Nullable
    public final MessageDigest zzha() {
        synchronized (this.mLock) {
            if (zzazi != null) {
                MessageDigest messageDigest = zzazi;
                return messageDigest;
            }
            for (int i = 0; i < 2; i++) {
                try {
                    zzazi = MessageDigest.getInstance("MD5");
                } catch (NoSuchAlgorithmException unused) {
                }
            }
            MessageDigest messageDigest2 = zzazi;
            return messageDigest2;
        }
    }

    /* access modifiers changed from: package-private */
    public abstract byte[] zzx(String str);
}
