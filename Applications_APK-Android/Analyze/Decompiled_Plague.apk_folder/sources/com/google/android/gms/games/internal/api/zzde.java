package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzde extends zzdl {
    private /* synthetic */ String zzhlu;
    private /* synthetic */ String zzhqx;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzde(zzcw zzcw, GoogleApiClient googleApiClient, String str, String str2) {
        super(googleApiClient, null);
        this.zzhlu = str;
        this.zzhqx = str2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhlu, this.zzhqx);
    }
}
