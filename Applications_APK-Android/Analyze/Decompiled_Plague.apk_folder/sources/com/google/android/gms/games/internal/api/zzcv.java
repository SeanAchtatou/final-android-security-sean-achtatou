package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.stats.PlayerStats;
import com.google.android.gms.games.stats.Stats;

final class zzcv implements Stats.LoadPlayerStatsResult {
    private /* synthetic */ Status zzekv;

    zzcv(zzcu zzcu, Status status) {
        this.zzekv = status;
    }

    public final PlayerStats getPlayerStats() {
        return null;
    }

    public final Status getStatus() {
        return this.zzekv;
    }

    public final void release() {
    }
}
