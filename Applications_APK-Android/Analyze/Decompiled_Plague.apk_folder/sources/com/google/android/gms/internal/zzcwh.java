package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzcwh extends IInterface {
    void zzb(zzcwo zzcwo) throws RemoteException;
}
