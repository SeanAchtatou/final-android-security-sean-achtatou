package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.internal.api.zzp;
import com.google.android.gms.games.internal.zzg;
import com.google.android.gms.tasks.Task;

public class AchievementsClient extends zzp {
    private static final zzbo<Achievements.LoadAchievementsResult, AchievementBuffer> zzhgk = new zzb();
    private static final zzbo<Achievements.UpdateAchievementResult, Void> zzhgl = new zzc();
    private static final zzbo<Achievements.UpdateAchievementResult, Boolean> zzhgm = new zzd();
    private static final com.google.android.gms.games.internal.zzp zzhgn = new zze();

    AchievementsClient(@NonNull Activity activity, @NonNull Games.GamesOptions gamesOptions) {
        super(activity, gamesOptions);
    }

    AchievementsClient(@NonNull Context context, @NonNull Games.GamesOptions gamesOptions) {
        super(context, gamesOptions);
    }

    private static Task<Void> zzc(@NonNull PendingResult<Achievements.UpdateAchievementResult> pendingResult) {
        return zzg.zza(pendingResult, zzhgn, zzhgl);
    }

    private static Task<Boolean> zzd(@NonNull PendingResult<Achievements.UpdateAchievementResult> pendingResult) {
        return zzg.zza(pendingResult, zzhgn, zzhgm);
    }

    public Task<Intent> getAchievementsIntent() {
        return zza(new zza(this));
    }

    public void increment(@NonNull String str, @IntRange(from = 0) int i) {
        Games.Achievements.increment(zzagb(), str, i);
    }

    public Task<Boolean> incrementImmediate(@NonNull String str, @IntRange(from = 0) int i) {
        return zzd(Games.Achievements.incrementImmediate(zzagb(), str, i));
    }

    public Task<AnnotatedData<AchievementBuffer>> load(boolean z) {
        return zzg.zzc(Games.Achievements.load(zzagb(), z), zzhgk);
    }

    public void reveal(@NonNull String str) {
        Games.Achievements.reveal(zzagb(), str);
    }

    public Task<Void> revealImmediate(@NonNull String str) {
        return zzc(Games.Achievements.revealImmediate(zzagb(), str));
    }

    public void setSteps(@NonNull String str, @IntRange(from = 0) int i) {
        Games.Achievements.setSteps(zzagb(), str, i);
    }

    public Task<Boolean> setStepsImmediate(@NonNull String str, @IntRange(from = 0) int i) {
        return zzd(Games.Achievements.setStepsImmediate(zzagb(), str, i));
    }

    public void unlock(@NonNull String str) {
        Games.Achievements.unlock(zzagb(), str);
    }

    public Task<Void> unlockImmediate(@NonNull String str) {
        return zzc(Games.Achievements.unlockImmediate(zzagb(), str));
    }
}
