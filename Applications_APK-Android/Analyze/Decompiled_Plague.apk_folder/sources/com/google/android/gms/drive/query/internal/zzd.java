package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.internal.zzbem;

public final class zzd extends zza {
    public static final Parcelable.Creator<zzd> CREATOR = new zze();
    private MetadataBundle zzgsf;
    private final MetadataField<?> zzgsg;

    public zzd(SearchableMetadataField<?> searchableMetadataField) {
        this(MetadataBundle.zzb(searchableMetadataField, null));
    }

    zzd(MetadataBundle metadataBundle) {
        this.zzgsf = metadataBundle;
        this.zzgsg = zzi.zza(metadataBundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.metadata.internal.MetadataBundle, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, (Parcelable) this.zzgsf, i, false);
        zzbem.zzai(parcel, zze);
    }

    public final <T> T zza(zzj<T> zzj) {
        return zzj.zze(this.zzgsg);
    }
}
