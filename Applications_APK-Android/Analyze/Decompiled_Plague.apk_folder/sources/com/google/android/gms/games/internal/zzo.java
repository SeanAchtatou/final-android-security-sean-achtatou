package com.google.android.gms.games.internal;

import android.support.annotation.NonNull;

public interface zzo<R> {
    void release(@NonNull R r);
}
