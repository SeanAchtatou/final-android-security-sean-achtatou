package com.google.android.gms.ads.internal.gmsg;

import android.content.Context;
import android.support.annotation.Keep;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.internal.zzafj;
import com.google.android.gms.internal.zzagl;
import com.google.android.gms.internal.zzaiy;
import com.google.android.gms.internal.zzzb;
import com.millennialmedia.NativeAd;
import com.tapjoy.TJAdUnitConstants;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Keep
@zzzb
@KeepName
public class HttpClient implements zzt<com.google.android.gms.ads.internal.js.zza> {
    private final Context mContext;
    private final zzaiy zzaov;

    @zzzb
    static class zza {
        private final String mValue;
        private final String zzbfo;

        public zza(String str, String str2) {
            this.zzbfo = str;
            this.mValue = str2;
        }

        public final String getKey() {
            return this.zzbfo;
        }

        public final String getValue() {
            return this.mValue;
        }
    }

    @zzzb
    static class zzb {
        private final String zzbvz;
        private final URL zzbwa;
        private final ArrayList<zza> zzbwb;
        private final String zzbwc;

        zzb(String str, URL url, ArrayList<zza> arrayList, String str2) {
            this.zzbvz = str;
            this.zzbwa = url;
            this.zzbwb = arrayList;
            this.zzbwc = str2;
        }

        public final String zzkh() {
            return this.zzbvz;
        }

        public final URL zzki() {
            return this.zzbwa;
        }

        public final ArrayList<zza> zzkj() {
            return this.zzbwb;
        }

        public final String zzkk() {
            return this.zzbwc;
        }
    }

    @zzzb
    class zzc {
        private final zzd zzbwd;
        private final boolean zzbwe;
        private final String zzbwf;

        public zzc(HttpClient httpClient, boolean z, zzd zzd, String str) {
            this.zzbwe = z;
            this.zzbwd = zzd;
            this.zzbwf = str;
        }

        public final String getReason() {
            return this.zzbwf;
        }

        public final boolean isSuccess() {
            return this.zzbwe;
        }

        public final zzd zzkl() {
            return this.zzbwd;
        }
    }

    @zzzb
    static class zzd {
        private final String zzbrz;
        private final String zzbvz;
        private final int zzbwg;
        private final List<zza> zzbwh;

        zzd(String str, int i, List<zza> list, String str2) {
            this.zzbvz = str;
            this.zzbwg = i;
            this.zzbwh = list;
            this.zzbrz = str2;
        }

        public final String getBody() {
            return this.zzbrz;
        }

        public final int getResponseCode() {
            return this.zzbwg;
        }

        public final String zzkh() {
            return this.zzbvz;
        }

        public final Iterable<zza> zzkm() {
            return this.zzbwh;
        }
    }

    public HttpClient(Context context, zzaiy zzaiy) {
        this.mContext = context;
        this.zzaov = zzaiy;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzagr.zza(android.content.Context, java.lang.String, boolean, java.net.HttpURLConnection):void
     arg types: [android.content.Context, java.lang.String, int, java.net.HttpURLConnection]
     candidates:
      com.google.android.gms.internal.zzagr.zza(android.view.View, int, int, boolean):android.widget.PopupWindow
      com.google.android.gms.internal.zzagr.zza(android.content.Context, com.google.android.gms.internal.zzcs, java.lang.String, android.view.View):java.lang.String
      com.google.android.gms.internal.zzagr.zza(android.content.Context, java.lang.String, boolean, java.net.HttpURLConnection):void */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ec  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.ads.internal.gmsg.HttpClient.zzc zza(com.google.android.gms.ads.internal.gmsg.HttpClient.zzb r12) {
        /*
            r11 = this;
            r0 = 0
            r1 = 0
            java.net.URL r2 = r12.zzki()     // Catch:{ Exception -> 0x00d8, all -> 0x00d5 }
            java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Exception -> 0x00d8, all -> 0x00d5 }
            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ Exception -> 0x00d8, all -> 0x00d5 }
            com.google.android.gms.internal.zzagr r3 = com.google.android.gms.ads.internal.zzbs.zzec()     // Catch:{ Exception -> 0x00d3 }
            android.content.Context r4 = r11.mContext     // Catch:{ Exception -> 0x00d3 }
            com.google.android.gms.internal.zzaiy r5 = r11.zzaov     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r5 = r5.zzcp     // Catch:{ Exception -> 0x00d3 }
            r3.zza(r4, r5, r0, r2)     // Catch:{ Exception -> 0x00d3 }
            java.util.ArrayList r3 = r12.zzkj()     // Catch:{ Exception -> 0x00d3 }
            java.util.ArrayList r3 = (java.util.ArrayList) r3     // Catch:{ Exception -> 0x00d3 }
            int r4 = r3.size()     // Catch:{ Exception -> 0x00d3 }
            r5 = r0
        L_0x0024:
            if (r5 >= r4) goto L_0x003a
            java.lang.Object r6 = r3.get(r5)     // Catch:{ Exception -> 0x00d3 }
            int r5 = r5 + 1
            com.google.android.gms.ads.internal.gmsg.HttpClient$zza r6 = (com.google.android.gms.ads.internal.gmsg.HttpClient.zza) r6     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r7 = r6.getKey()     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r6 = r6.getValue()     // Catch:{ Exception -> 0x00d3 }
            r2.addRequestProperty(r7, r6)     // Catch:{ Exception -> 0x00d3 }
            goto L_0x0024
        L_0x003a:
            java.lang.String r3 = r12.zzkk()     // Catch:{ Exception -> 0x00d3 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x00d3 }
            r4 = 1
            if (r3 != 0) goto L_0x0063
            r2.setDoOutput(r4)     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r3 = r12.zzkk()     // Catch:{ Exception -> 0x00d3 }
            byte[] r3 = r3.getBytes()     // Catch:{ Exception -> 0x00d3 }
            int r5 = r3.length     // Catch:{ Exception -> 0x00d3 }
            r2.setFixedLengthStreamingMode(r5)     // Catch:{ Exception -> 0x00d3 }
            java.io.BufferedOutputStream r5 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x00d3 }
            java.io.OutputStream r6 = r2.getOutputStream()     // Catch:{ Exception -> 0x00d3 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x00d3 }
            r5.write(r3)     // Catch:{ Exception -> 0x00d3 }
            r5.close()     // Catch:{ Exception -> 0x00d3 }
        L_0x0063:
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x00d3 }
            r3.<init>()     // Catch:{ Exception -> 0x00d3 }
            java.util.Map r5 = r2.getHeaderFields()     // Catch:{ Exception -> 0x00d3 }
            if (r5 == 0) goto L_0x00ab
            java.util.Map r5 = r2.getHeaderFields()     // Catch:{ Exception -> 0x00d3 }
            java.util.Set r5 = r5.entrySet()     // Catch:{ Exception -> 0x00d3 }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ Exception -> 0x00d3 }
        L_0x007a:
            boolean r6 = r5.hasNext()     // Catch:{ Exception -> 0x00d3 }
            if (r6 == 0) goto L_0x00ab
            java.lang.Object r6 = r5.next()     // Catch:{ Exception -> 0x00d3 }
            java.util.Map$Entry r6 = (java.util.Map.Entry) r6     // Catch:{ Exception -> 0x00d3 }
            java.lang.Object r7 = r6.getValue()     // Catch:{ Exception -> 0x00d3 }
            java.util.List r7 = (java.util.List) r7     // Catch:{ Exception -> 0x00d3 }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ Exception -> 0x00d3 }
        L_0x0090:
            boolean r8 = r7.hasNext()     // Catch:{ Exception -> 0x00d3 }
            if (r8 == 0) goto L_0x007a
            java.lang.Object r8 = r7.next()     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x00d3 }
            com.google.android.gms.ads.internal.gmsg.HttpClient$zza r9 = new com.google.android.gms.ads.internal.gmsg.HttpClient$zza     // Catch:{ Exception -> 0x00d3 }
            java.lang.Object r10 = r6.getKey()     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ Exception -> 0x00d3 }
            r9.<init>(r10, r8)     // Catch:{ Exception -> 0x00d3 }
            r3.add(r9)     // Catch:{ Exception -> 0x00d3 }
            goto L_0x0090
        L_0x00ab:
            com.google.android.gms.ads.internal.gmsg.HttpClient$zzd r5 = new com.google.android.gms.ads.internal.gmsg.HttpClient$zzd     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r12 = r12.zzkh()     // Catch:{ Exception -> 0x00d3 }
            int r6 = r2.getResponseCode()     // Catch:{ Exception -> 0x00d3 }
            com.google.android.gms.ads.internal.zzbs.zzec()     // Catch:{ Exception -> 0x00d3 }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00d3 }
            java.io.InputStream r8 = r2.getInputStream()     // Catch:{ Exception -> 0x00d3 }
            r7.<init>(r8)     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r7 = com.google.android.gms.internal.zzagr.zza(r7)     // Catch:{ Exception -> 0x00d3 }
            r5.<init>(r12, r6, r3, r7)     // Catch:{ Exception -> 0x00d3 }
            com.google.android.gms.ads.internal.gmsg.HttpClient$zzc r12 = new com.google.android.gms.ads.internal.gmsg.HttpClient$zzc     // Catch:{ Exception -> 0x00d3 }
            r12.<init>(r11, r4, r5, r1)     // Catch:{ Exception -> 0x00d3 }
            if (r2 == 0) goto L_0x00d2
            r2.disconnect()
        L_0x00d2:
            return r12
        L_0x00d3:
            r12 = move-exception
            goto L_0x00da
        L_0x00d5:
            r12 = move-exception
            r2 = r1
            goto L_0x00ea
        L_0x00d8:
            r12 = move-exception
            r2 = r1
        L_0x00da:
            com.google.android.gms.ads.internal.gmsg.HttpClient$zzc r3 = new com.google.android.gms.ads.internal.gmsg.HttpClient$zzc     // Catch:{ all -> 0x00e9 }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x00e9 }
            r3.<init>(r11, r0, r1, r12)     // Catch:{ all -> 0x00e9 }
            if (r2 == 0) goto L_0x00e8
            r2.disconnect()
        L_0x00e8:
            return r3
        L_0x00e9:
            r12 = move-exception
        L_0x00ea:
            if (r2 == 0) goto L_0x00ef
            r2.disconnect()
        L_0x00ef:
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.gmsg.HttpClient.zza(com.google.android.gms.ads.internal.gmsg.HttpClient$zzb):com.google.android.gms.ads.internal.gmsg.HttpClient$zzc");
    }

    private static JSONObject zza(zzd zzd2) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("http_request_id", zzd2.zzkh());
            if (zzd2.getBody() != null) {
                jSONObject.put(NativeAd.COMPONENT_ID_BODY, zzd2.getBody());
            }
            JSONArray jSONArray = new JSONArray();
            for (zza next : zzd2.zzkm()) {
                jSONArray.put(new JSONObject().put("key", next.getKey()).put("value", next.getValue()));
            }
            jSONObject.put("headers", jSONArray);
            jSONObject.put("response_code", zzd2.getResponseCode());
            return jSONObject;
        } catch (JSONException e) {
            zzafj.zzb("Error constructing JSON for http response.", e);
            return jSONObject;
        }
    }

    private static zzb zzd(JSONObject jSONObject) {
        String optString = jSONObject.optString("http_request_id");
        String optString2 = jSONObject.optString(TJAdUnitConstants.String.URL);
        URL url = null;
        String optString3 = jSONObject.optString("post_body", null);
        try {
            url = new URL(optString2);
        } catch (MalformedURLException e) {
            zzafj.zzb("Error constructing http request.", e);
        }
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject.optJSONArray("headers");
        if (optJSONArray == null) {
            optJSONArray = new JSONArray();
        }
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                arrayList.add(new zza(optJSONObject.optString("key"), optJSONObject.optString("value")));
            }
        }
        return new zzb(optString, url, arrayList, optString3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    @Keep
    @KeepName
    public JSONObject send(JSONObject jSONObject) {
        String str;
        JSONObject jSONObject2 = new JSONObject();
        try {
            str = jSONObject.optString("http_request_id");
            try {
                zzc zza2 = zza(zzd(jSONObject));
                if (zza2.isSuccess()) {
                    jSONObject2.put("response", zza(zza2.zzkl()));
                    jSONObject2.put("success", true);
                    return jSONObject2;
                }
                jSONObject2.put("response", new JSONObject().put("http_request_id", str));
                jSONObject2.put("success", false);
                jSONObject2.put("reason", zza2.getReason());
                return jSONObject2;
            } catch (Exception e) {
                e = e;
                zzafj.zzb("Error executing http request.", e);
                try {
                    jSONObject2.put("response", new JSONObject().put("http_request_id", str));
                    jSONObject2.put("success", false);
                    jSONObject2.put("reason", e.toString());
                    return jSONObject2;
                } catch (JSONException e2) {
                    zzafj.zzb("Error executing http request.", e2);
                    return jSONObject2;
                }
            }
        } catch (Exception e3) {
            e = e3;
            str = "";
            zzafj.zzb("Error executing http request.", e);
            jSONObject2.put("response", new JSONObject().put("http_request_id", str));
            jSONObject2.put("success", false);
            jSONObject2.put("reason", e.toString());
            return jSONObject2;
        }
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzagl.zza(new zzu(this, map, (com.google.android.gms.ads.internal.js.zza) obj));
    }
}
