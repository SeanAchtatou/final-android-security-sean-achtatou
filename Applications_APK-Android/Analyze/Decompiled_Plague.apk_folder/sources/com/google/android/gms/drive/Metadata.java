package com.google.android.gms.drive;

import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.internal.zzbse;
import com.google.android.gms.internal.zzbsr;
import com.google.android.gms.internal.zzbsz;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

public abstract class Metadata implements Freezable<Metadata> {
    public static final int CONTENT_AVAILABLE_LOCALLY = 1;
    public static final int CONTENT_NOT_AVAILABLE_LOCALLY = 0;

    public String getAlternateLink() {
        return (String) zza(zzbse.zzgps);
    }

    public int getContentAvailability() {
        Integer num = (Integer) zza(zzbsz.zzgrq);
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    public Date getCreatedDate() {
        return (Date) zza(zzbsr.zzgrj);
    }

    public Map<CustomPropertyKey, String> getCustomProperties() {
        AppVisibleCustomProperties appVisibleCustomProperties = (AppVisibleCustomProperties) zza(zzbse.zzgpt);
        return appVisibleCustomProperties == null ? Collections.emptyMap() : appVisibleCustomProperties.zzaoz();
    }

    public String getDescription() {
        return (String) zza(zzbse.zzgpu);
    }

    public DriveId getDriveId() {
        return (DriveId) zza(zzbse.zzgpr);
    }

    public String getEmbedLink() {
        return (String) zza(zzbse.zzgpv);
    }

    public String getFileExtension() {
        return (String) zza(zzbse.zzgpw);
    }

    public long getFileSize() {
        return ((Long) zza(zzbse.zzgpx)).longValue();
    }

    public Date getLastViewedByMeDate() {
        return (Date) zza(zzbsr.zzgrk);
    }

    public String getMimeType() {
        return (String) zza(zzbse.zzgqo);
    }

    public Date getModifiedByMeDate() {
        return (Date) zza(zzbsr.zzgrm);
    }

    public Date getModifiedDate() {
        return (Date) zza(zzbsr.zzgrl);
    }

    public String getOriginalFilename() {
        return (String) zza(zzbse.zzgqp);
    }

    public long getQuotaBytesUsed() {
        return ((Long) zza(zzbse.zzgqu)).longValue();
    }

    public Date getSharedWithMeDate() {
        return (Date) zza(zzbsr.zzgrn);
    }

    public String getTitle() {
        return (String) zza(zzbse.zzgqx);
    }

    public String getWebContentLink() {
        return (String) zza(zzbse.zzgqz);
    }

    public String getWebViewLink() {
        return (String) zza(zzbse.zzgra);
    }

    public boolean isEditable() {
        Boolean bool = (Boolean) zza(zzbse.zzgqd);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public boolean isExplicitlyTrashed() {
        Boolean bool = (Boolean) zza(zzbse.zzgqe);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public boolean isFolder() {
        return DriveFolder.MIME_TYPE.equals(getMimeType());
    }

    public boolean isInAppFolder() {
        Boolean bool = (Boolean) zza(zzbse.zzgqb);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public boolean isPinnable() {
        Boolean bool = (Boolean) zza(zzbsz.zzgrr);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public boolean isPinned() {
        Boolean bool = (Boolean) zza(zzbse.zzgqg);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public boolean isRestricted() {
        Boolean bool = (Boolean) zza(zzbse.zzgqi);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public boolean isShared() {
        Boolean bool = (Boolean) zza(zzbse.zzgqj);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public boolean isStarred() {
        Boolean bool = (Boolean) zza(zzbse.zzgqv);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public boolean isTrashable() {
        Boolean bool = (Boolean) zza(zzbse.zzgqm);
        if (bool == null) {
            return true;
        }
        return bool.booleanValue();
    }

    public boolean isTrashed() {
        Boolean bool = (Boolean) zza(zzbse.zzgqy);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public boolean isViewed() {
        Boolean bool = (Boolean) zza(zzbse.zzgqn);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public abstract <T> T zza(MetadataField<T> metadataField);
}
