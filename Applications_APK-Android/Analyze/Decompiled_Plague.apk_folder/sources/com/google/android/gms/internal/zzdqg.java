package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdqk;
import java.io.IOException;

public final class zzdqg extends zzfee<zzdqg, zza> implements zzffk {
    private static volatile zzffm<zzdqg> zzbas;
    /* access modifiers changed from: private */
    public static final zzdqg zzlqu;
    private int zzlqb;
    private zzfdh zzlqj = zzfdh.zzpal;
    private zzdqk zzlqt;

    public static final class zza extends zzfef<zzdqg, zza> implements zzffk {
        private zza() {
            super(zzdqg.zzlqu);
        }

        /* synthetic */ zza(zzdqh zzdqh) {
            this();
        }

        public final zza zzb(zzdqk zzdqk) {
            zzcvi();
            ((zzdqg) this.zzpbv).zza(zzdqk);
            return this;
        }

        public final zza zzfl(int i) {
            zzcvi();
            ((zzdqg) this.zzpbv).setVersion(0);
            return this;
        }

        public final zza zzr(zzfdh zzfdh) {
            zzcvi();
            ((zzdqg) this.zzpbv).zzj(zzfdh);
            return this;
        }
    }

    static {
        zzdqg zzdqg = new zzdqg();
        zzlqu = zzdqg;
        zzdqg.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdqg.zzpbs.zzbim();
    }

    private zzdqg() {
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzlqb = i;
    }

    /* access modifiers changed from: private */
    public final void zza(zzdqk zzdqk) {
        if (zzdqk == null) {
            throw new NullPointerException();
        }
        this.zzlqt = zzdqk;
    }

    public static zza zzbmi() {
        zzdqg zzdqg = zzlqu;
        zzfef zzfef = (zzfef) zzdqg.zza(zzfem.zzpch, (Object) null, (Object) null);
        zzfef.zza((zzfee) zzdqg);
        return (zza) zzfef;
    }

    /* access modifiers changed from: private */
    public final void zzj(zzfdh zzfdh) {
        if (zzfdh == null) {
            throw new NullPointerException();
        }
        this.zzlqj = zzfdh;
    }

    public static zzdqg zzq(zzfdh zzfdh) throws zzfew {
        return (zzdqg) zzfee.zza(zzlqu, zzfdh);
    }

    public final int getVersion() {
        return this.zzlqb;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdqk.zza zza2;
        boolean z = true;
        boolean z2 = false;
        switch (zzdqh.zzbaq[i - 1]) {
            case 1:
                return new zzdqg();
            case 2:
                return zzlqu;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdqg zzdqg = (zzdqg) obj2;
                this.zzlqb = zzfen.zza(this.zzlqb != 0, this.zzlqb, zzdqg.zzlqb != 0, zzdqg.zzlqb);
                this.zzlqt = (zzdqk) zzfen.zza(this.zzlqt, zzdqg.zzlqt);
                boolean z3 = this.zzlqj != zzfdh.zzpal;
                zzfdh zzfdh = this.zzlqj;
                if (zzdqg.zzlqj == zzfdh.zzpal) {
                    z = false;
                }
                this.zzlqj = zzfen.zza(z3, zzfdh, z, zzdqg.zzlqj);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzlqb = zzfdq.zzcub();
                                } else if (zzcts == 18) {
                                    if (this.zzlqt != null) {
                                        zzdqk zzdqk = this.zzlqt;
                                        zzfef zzfef = (zzfef) zzdqk.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdqk);
                                        zza2 = (zzdqk.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlqt = (zzdqk) zzfdq.zza(zzdqk.zzbmm(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlqt);
                                        this.zzlqt = (zzdqk) zza2.zzcvj();
                                    }
                                } else if (zzcts == 26) {
                                    this.zzlqj = zzfdq.zzcua();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdqg.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlqu);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlqu;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlqb != 0) {
            zzfdv.zzab(1, this.zzlqb);
        }
        if (this.zzlqt != null) {
            zzfdv.zza(2, this.zzlqt == null ? zzdqk.zzbmm() : this.zzlqt);
        }
        if (!this.zzlqj.isEmpty()) {
            zzfdv.zza(3, this.zzlqj);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzfdh zzblt() {
        return this.zzlqj;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlqb != 0) {
            i2 = 0 + zzfdv.zzae(1, this.zzlqb);
        }
        if (this.zzlqt != null) {
            i2 += zzfdv.zzb(2, this.zzlqt == null ? zzdqk.zzbmm() : this.zzlqt);
        }
        if (!this.zzlqj.isEmpty()) {
            i2 += zzfdv.zzb(3, this.zzlqj);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
