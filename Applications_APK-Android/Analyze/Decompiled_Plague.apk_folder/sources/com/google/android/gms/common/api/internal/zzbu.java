package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;

final class zzbu implements Runnable {
    private /* synthetic */ zzbr zzfqx;
    private /* synthetic */ ConnectionResult zzfqy;

    zzbu(zzbr zzbr, ConnectionResult connectionResult) {
        this.zzfqx = zzbr;
        this.zzfqy = connectionResult;
    }

    public final void run() {
        this.zzfqx.onConnectionFailed(this.zzfqy);
    }
}
