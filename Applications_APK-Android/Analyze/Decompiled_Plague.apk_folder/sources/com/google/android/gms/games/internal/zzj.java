package com.google.android.gms.games.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.TimeUnit;

final class zzj implements PendingResult.zza {
    private /* synthetic */ PendingResult zzfym;
    private /* synthetic */ TaskCompletionSource zzfyn;
    private /* synthetic */ zzbo zzhpm;

    zzj(PendingResult pendingResult, TaskCompletionSource taskCompletionSource, zzbo zzbo) {
        this.zzfym = pendingResult;
        this.zzfyn = taskCompletionSource;
        this.zzhpm = zzbo;
    }

    public final void zzr(@NonNull Status status) {
        Result await = this.zzfym.await(0, TimeUnit.MILLISECONDS);
        if (status.isSuccess()) {
            this.zzfyn.setResult(this.zzhpm.zzb(await));
        } else {
            this.zzfyn.setException(zzb.zzy(zzg.zzah(status)));
        }
    }
}
