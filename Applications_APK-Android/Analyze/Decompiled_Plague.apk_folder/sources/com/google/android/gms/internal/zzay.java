package com.google.android.gms.internal;

import java.io.IOException;

public final class zzay extends zzfhe<zzay> {
    private Long zzej = null;
    private Long zzek = null;
    public Long zzfw = null;
    public Long zzfx = null;
    public Long zzfy = null;

    public zzay() {
        this.zzpai = -1;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 8) {
                this.zzej = Long.valueOf(zzfhb.zzcum());
            } else if (zzcts == 16) {
                this.zzek = Long.valueOf(zzfhb.zzcum());
            } else if (zzcts == 24) {
                this.zzfw = Long.valueOf(zzfhb.zzcum());
            } else if (zzcts == 32) {
                this.zzfx = Long.valueOf(zzfhb.zzcum());
            } else if (zzcts == 40) {
                this.zzfy = Long.valueOf(zzfhb.zzcum());
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzej != null) {
            zzfhc.zzf(1, this.zzej.longValue());
        }
        if (this.zzek != null) {
            zzfhc.zzf(2, this.zzek.longValue());
        }
        if (this.zzfw != null) {
            zzfhc.zzf(3, this.zzfw.longValue());
        }
        if (this.zzfx != null) {
            zzfhc.zzf(4, this.zzfx.longValue());
        }
        if (this.zzfy != null) {
            zzfhc.zzf(5, this.zzfy.longValue());
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzej != null) {
            zzo += zzfhc.zzc(1, this.zzej.longValue());
        }
        if (this.zzek != null) {
            zzo += zzfhc.zzc(2, this.zzek.longValue());
        }
        if (this.zzfw != null) {
            zzo += zzfhc.zzc(3, this.zzfw.longValue());
        }
        if (this.zzfx != null) {
            zzo += zzfhc.zzc(4, this.zzfx.longValue());
        }
        return this.zzfy != null ? zzo + zzfhc.zzc(5, this.zzfy.longValue()) : zzo;
    }
}
