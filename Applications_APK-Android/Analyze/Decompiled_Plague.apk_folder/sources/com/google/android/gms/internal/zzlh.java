package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.dynamic.zzn;
import com.google.android.gms.internal.zzja;

@zzzb
public final class zzlh {
    private static final Object sLock = new Object();
    private static zzlh zzbfa;
    private zzkp zzbfb;
    private RewardedVideoAd zzbfc;

    private zzlh() {
    }

    public static zzlh zzik() {
        zzlh zzlh;
        synchronized (sLock) {
            if (zzbfa == null) {
                zzbfa = new zzlh();
            }
            zzlh = zzbfa;
        }
        return zzlh;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzja.zza(android.content.Context, boolean, com.google.android.gms.internal.zzja$zza):T
     arg types: [android.content.Context, int, com.google.android.gms.internal.zzji]
     candidates:
      com.google.android.gms.internal.zzja.zza(com.google.android.gms.internal.zzja, android.content.Context, java.lang.String):void
      com.google.android.gms.internal.zzja.zza(android.content.Context, android.widget.FrameLayout, android.widget.FrameLayout):com.google.android.gms.internal.zzpc
      com.google.android.gms.internal.zzja.zza(android.view.View, java.util.HashMap<java.lang.String, android.view.View>, java.util.HashMap<java.lang.String, android.view.View>):com.google.android.gms.internal.zzph
      com.google.android.gms.internal.zzja.zza(android.content.Context, boolean, com.google.android.gms.internal.zzja$zza):T */
    public final RewardedVideoAd getRewardedVideoAdInstance(Context context) {
        synchronized (sLock) {
            if (this.zzbfc != null) {
                RewardedVideoAd rewardedVideoAd = this.zzbfc;
                return rewardedVideoAd;
            }
            this.zzbfc = new zzadd(context, (zzacq) zzja.zza(context, false, (zzja.zza) new zzji(zzjk.zzhy(), context, new zzub())));
            RewardedVideoAd rewardedVideoAd2 = this.zzbfc;
            return rewardedVideoAd2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final void openDebugMenu(Context context, String str) {
        zzbq.zza(this.zzbfb != null, (Object) "MobileAds.initialize() must be called prior to opening debug menu.");
        try {
            this.zzbfb.zzb(zzn.zzy(context), str);
        } catch (RemoteException e) {
            zzaiw.zzb("Unable to open debug menu.", e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final void setAppMuted(boolean z) {
        zzbq.zza(this.zzbfb != null, (Object) "MobileAds.initialize() must be called prior to setting app muted state.");
        try {
            this.zzbfb.setAppMuted(z);
        } catch (RemoteException e) {
            zzaiw.zzb("Unable to set app mute state.", e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzbq.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzbq.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzbq.zza(boolean, java.lang.Object):void */
    public final void setAppVolume(float f) {
        boolean z = false;
        zzbq.checkArgument(0.0f <= f && f <= 1.0f, "The app volume must be a value between 0 and 1 inclusive.");
        if (this.zzbfb != null) {
            z = true;
        }
        zzbq.zza(z, (Object) "MobileAds.initialize() must be called prior to setting the app volume.");
        try {
            this.zzbfb.setAppVolume(f);
        } catch (RemoteException e) {
            zzaiw.zzb("Unable to set app volume.", e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzja.zza(android.content.Context, boolean, com.google.android.gms.internal.zzja$zza):T
     arg types: [android.content.Context, int, com.google.android.gms.internal.zzjf]
     candidates:
      com.google.android.gms.internal.zzja.zza(com.google.android.gms.internal.zzja, android.content.Context, java.lang.String):void
      com.google.android.gms.internal.zzja.zza(android.content.Context, android.widget.FrameLayout, android.widget.FrameLayout):com.google.android.gms.internal.zzpc
      com.google.android.gms.internal.zzja.zza(android.view.View, java.util.HashMap<java.lang.String, android.view.View>, java.util.HashMap<java.lang.String, android.view.View>):com.google.android.gms.internal.zzph
      com.google.android.gms.internal.zzja.zza(android.content.Context, boolean, com.google.android.gms.internal.zzja$zza):T */
    public final void zza(Context context, String str, zzlj zzlj) {
        synchronized (sLock) {
            if (this.zzbfb == null) {
                if (context == null) {
                    throw new IllegalArgumentException("Context cannot be null.");
                }
                try {
                    this.zzbfb = (zzkp) zzja.zza(context, false, (zzja.zza) new zzjf(zzjk.zzhy(), context));
                    this.zzbfb.initialize();
                    if (str != null) {
                        this.zzbfb.zza(str, zzn.zzy(new zzli(this, context)));
                    }
                } catch (RemoteException e) {
                    zzaiw.zzc("MobileAdsSettingManager initialization failed", e);
                }
            }
        }
    }

    public final float zzdh() {
        if (this.zzbfb == null) {
            return 1.0f;
        }
        try {
            return this.zzbfb.zzdh();
        } catch (RemoteException e) {
            zzaiw.zzb("Unable to get app volume.", e);
            return 1.0f;
        }
    }

    public final boolean zzdi() {
        if (this.zzbfb == null) {
            return false;
        }
        try {
            return this.zzbfb.zzdi();
        } catch (RemoteException e) {
            zzaiw.zzb("Unable to get app mute state.", e);
            return false;
        }
    }
}
