package com.google.android.gms.internal;

public abstract class zzug extends zzee implements zzuf {
    public zzug() {
        attachInterface(this, "com.google.android.gms.ads.internal.mediation.client.IMediationAdapter");
    }

    /* JADX WARN: Type inference failed for: r11v5, types: [android.os.IInterface] */
    /* JADX WARN: Type inference failed for: r11v7, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTransact(int r10, android.os.Parcel r11, android.os.Parcel r12, int r13) throws android.os.RemoteException {
        /*
            r9 = this;
            boolean r13 = r9.zza(r10, r11, r12, r13)
            r0 = 1
            if (r13 == 0) goto L_0x0008
            return r0
        L_0x0008:
            r13 = 0
            switch(r10) {
                case 1: goto L_0x01e2;
                case 2: goto L_0x01d7;
                case 3: goto L_0x01a6;
                case 4: goto L_0x019f;
                case 5: goto L_0x019b;
                case 6: goto L_0x0158;
                case 7: goto L_0x011e;
                case 8: goto L_0x0119;
                case 9: goto L_0x0114;
                case 10: goto L_0x00ed;
                case 11: goto L_0x00dc;
                case 12: goto L_0x00d7;
                case 13: goto L_0x00cc;
                case 14: goto L_0x0085;
                case 15: goto L_0x007f;
                case 16: goto L_0x0079;
                case 17: goto L_0x006e;
                case 18: goto L_0x0069;
                case 19: goto L_0x0064;
                case 20: goto L_0x004f;
                case 21: goto L_0x0042;
                case 22: goto L_0x003c;
                case 23: goto L_0x0023;
                case 24: goto L_0x001d;
                case 25: goto L_0x0014;
                case 26: goto L_0x000e;
                default: goto L_0x000c;
            }
        L_0x000c:
            r10 = 0
            return r10
        L_0x000e:
            com.google.android.gms.internal.zzku r10 = r9.getVideoController()
            goto L_0x01db
        L_0x0014:
            boolean r10 = com.google.android.gms.internal.zzef.zza(r11)
            r9.setImmersiveMode(r10)
            goto L_0x01a2
        L_0x001d:
            com.google.android.gms.internal.zzpu r10 = r9.zzmd()
            goto L_0x01db
        L_0x0023:
            android.os.IBinder r10 = r11.readStrongBinder()
            com.google.android.gms.dynamic.IObjectWrapper r10 = com.google.android.gms.dynamic.IObjectWrapper.zza.zzap(r10)
            android.os.IBinder r13 = r11.readStrongBinder()
            com.google.android.gms.internal.zzads r13 = com.google.android.gms.internal.zzadt.zzaa(r13)
            java.util.ArrayList r11 = r11.createStringArrayList()
            r9.zza(r10, r13, r11)
            goto L_0x01a2
        L_0x003c:
            boolean r10 = r9.zzmc()
            goto L_0x00d0
        L_0x0042:
            android.os.IBinder r10 = r11.readStrongBinder()
            com.google.android.gms.dynamic.IObjectWrapper r10 = com.google.android.gms.dynamic.IObjectWrapper.zza.zzap(r10)
            r9.zzg(r10)
            goto L_0x01a2
        L_0x004f:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzis> r10 = com.google.android.gms.internal.zzis.CREATOR
            android.os.Parcelable r10 = com.google.android.gms.internal.zzef.zza(r11, r10)
            com.google.android.gms.internal.zzis r10 = (com.google.android.gms.internal.zzis) r10
            java.lang.String r13 = r11.readString()
            java.lang.String r11 = r11.readString()
            r9.zza(r10, r13, r11)
            goto L_0x01a2
        L_0x0064:
            android.os.Bundle r10 = r9.zzmb()
            goto L_0x0072
        L_0x0069:
            android.os.Bundle r10 = r9.getInterstitialAdapterInfo()
            goto L_0x0072
        L_0x006e:
            android.os.Bundle r10 = r9.zzma()
        L_0x0072:
            r12.writeNoException()
            com.google.android.gms.internal.zzef.zzb(r12, r10)
            return r0
        L_0x0079:
            com.google.android.gms.internal.zzur r10 = r9.zzlz()
            goto L_0x01db
        L_0x007f:
            com.google.android.gms.internal.zzuo r10 = r9.zzly()
            goto L_0x01db
        L_0x0085:
            android.os.IBinder r10 = r11.readStrongBinder()
            com.google.android.gms.dynamic.IObjectWrapper r2 = com.google.android.gms.dynamic.IObjectWrapper.zza.zzap(r10)
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzis> r10 = com.google.android.gms.internal.zzis.CREATOR
            android.os.Parcelable r10 = com.google.android.gms.internal.zzef.zza(r11, r10)
            r3 = r10
            com.google.android.gms.internal.zzis r3 = (com.google.android.gms.internal.zzis) r3
            java.lang.String r4 = r11.readString()
            java.lang.String r5 = r11.readString()
            android.os.IBinder r10 = r11.readStrongBinder()
            if (r10 != 0) goto L_0x00a6
        L_0x00a4:
            r6 = r13
            goto L_0x00b9
        L_0x00a6:
            java.lang.String r13 = "com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener"
            android.os.IInterface r13 = r10.queryLocalInterface(r13)
            boolean r1 = r13 instanceof com.google.android.gms.internal.zzui
            if (r1 == 0) goto L_0x00b3
            com.google.android.gms.internal.zzui r13 = (com.google.android.gms.internal.zzui) r13
            goto L_0x00a4
        L_0x00b3:
            com.google.android.gms.internal.zzuk r13 = new com.google.android.gms.internal.zzuk
            r13.<init>(r10)
            goto L_0x00a4
        L_0x00b9:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzom> r10 = com.google.android.gms.internal.zzom.CREATOR
            android.os.Parcelable r10 = com.google.android.gms.internal.zzef.zza(r11, r10)
            r7 = r10
            com.google.android.gms.internal.zzom r7 = (com.google.android.gms.internal.zzom) r7
            java.util.ArrayList r8 = r11.createStringArrayList()
            r1 = r9
            r1.zza(r2, r3, r4, r5, r6, r7, r8)
            goto L_0x01a2
        L_0x00cc:
            boolean r10 = r9.isInitialized()
        L_0x00d0:
            r12.writeNoException()
            com.google.android.gms.internal.zzef.zza(r12, r10)
            return r0
        L_0x00d7:
            r9.showVideo()
            goto L_0x01a2
        L_0x00dc:
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzis> r10 = com.google.android.gms.internal.zzis.CREATOR
            android.os.Parcelable r10 = com.google.android.gms.internal.zzef.zza(r11, r10)
            com.google.android.gms.internal.zzis r10 = (com.google.android.gms.internal.zzis) r10
            java.lang.String r11 = r11.readString()
            r9.zzc(r10, r11)
            goto L_0x01a2
        L_0x00ed:
            android.os.IBinder r10 = r11.readStrongBinder()
            com.google.android.gms.dynamic.IObjectWrapper r2 = com.google.android.gms.dynamic.IObjectWrapper.zza.zzap(r10)
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzis> r10 = com.google.android.gms.internal.zzis.CREATOR
            android.os.Parcelable r10 = com.google.android.gms.internal.zzef.zza(r11, r10)
            r3 = r10
            com.google.android.gms.internal.zzis r3 = (com.google.android.gms.internal.zzis) r3
            java.lang.String r4 = r11.readString()
            android.os.IBinder r10 = r11.readStrongBinder()
            com.google.android.gms.internal.zzads r5 = com.google.android.gms.internal.zzadt.zzaa(r10)
            java.lang.String r6 = r11.readString()
            r1 = r9
            r1.zza(r2, r3, r4, r5, r6)
            goto L_0x01a2
        L_0x0114:
            r9.resume()
            goto L_0x01a2
        L_0x0119:
            r9.pause()
            goto L_0x01a2
        L_0x011e:
            android.os.IBinder r10 = r11.readStrongBinder()
            com.google.android.gms.dynamic.IObjectWrapper r2 = com.google.android.gms.dynamic.IObjectWrapper.zza.zzap(r10)
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzis> r10 = com.google.android.gms.internal.zzis.CREATOR
            android.os.Parcelable r10 = com.google.android.gms.internal.zzef.zza(r11, r10)
            r3 = r10
            com.google.android.gms.internal.zzis r3 = (com.google.android.gms.internal.zzis) r3
            java.lang.String r4 = r11.readString()
            java.lang.String r5 = r11.readString()
            android.os.IBinder r10 = r11.readStrongBinder()
            if (r10 != 0) goto L_0x013f
        L_0x013d:
            r6 = r13
            goto L_0x0153
        L_0x013f:
            java.lang.String r11 = "com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener"
            android.os.IInterface r11 = r10.queryLocalInterface(r11)
            boolean r13 = r11 instanceof com.google.android.gms.internal.zzui
            if (r13 == 0) goto L_0x014d
            r13 = r11
            com.google.android.gms.internal.zzui r13 = (com.google.android.gms.internal.zzui) r13
            goto L_0x013d
        L_0x014d:
            com.google.android.gms.internal.zzuk r13 = new com.google.android.gms.internal.zzuk
            r13.<init>(r10)
            goto L_0x013d
        L_0x0153:
            r1 = r9
            r1.zza(r2, r3, r4, r5, r6)
            goto L_0x01a2
        L_0x0158:
            android.os.IBinder r10 = r11.readStrongBinder()
            com.google.android.gms.dynamic.IObjectWrapper r2 = com.google.android.gms.dynamic.IObjectWrapper.zza.zzap(r10)
            android.os.Parcelable$Creator<com.google.android.gms.internal.zziw> r10 = com.google.android.gms.internal.zziw.CREATOR
            android.os.Parcelable r10 = com.google.android.gms.internal.zzef.zza(r11, r10)
            r3 = r10
            com.google.android.gms.internal.zziw r3 = (com.google.android.gms.internal.zziw) r3
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzis> r10 = com.google.android.gms.internal.zzis.CREATOR
            android.os.Parcelable r10 = com.google.android.gms.internal.zzef.zza(r11, r10)
            r4 = r10
            com.google.android.gms.internal.zzis r4 = (com.google.android.gms.internal.zzis) r4
            java.lang.String r5 = r11.readString()
            java.lang.String r6 = r11.readString()
            android.os.IBinder r10 = r11.readStrongBinder()
            if (r10 != 0) goto L_0x0182
        L_0x0180:
            r7 = r13
            goto L_0x0196
        L_0x0182:
            java.lang.String r11 = "com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener"
            android.os.IInterface r11 = r10.queryLocalInterface(r11)
            boolean r13 = r11 instanceof com.google.android.gms.internal.zzui
            if (r13 == 0) goto L_0x0190
            r13 = r11
            com.google.android.gms.internal.zzui r13 = (com.google.android.gms.internal.zzui) r13
            goto L_0x0180
        L_0x0190:
            com.google.android.gms.internal.zzuk r13 = new com.google.android.gms.internal.zzuk
            r13.<init>(r10)
            goto L_0x0180
        L_0x0196:
            r1 = r9
            r1.zza(r2, r3, r4, r5, r6, r7)
            goto L_0x01a2
        L_0x019b:
            r9.destroy()
            goto L_0x01a2
        L_0x019f:
            r9.showInterstitial()
        L_0x01a2:
            r12.writeNoException()
            return r0
        L_0x01a6:
            android.os.IBinder r10 = r11.readStrongBinder()
            com.google.android.gms.dynamic.IObjectWrapper r10 = com.google.android.gms.dynamic.IObjectWrapper.zza.zzap(r10)
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzis> r1 = com.google.android.gms.internal.zzis.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.zzef.zza(r11, r1)
            com.google.android.gms.internal.zzis r1 = (com.google.android.gms.internal.zzis) r1
            java.lang.String r2 = r11.readString()
            android.os.IBinder r11 = r11.readStrongBinder()
            if (r11 != 0) goto L_0x01c1
            goto L_0x01d3
        L_0x01c1:
            java.lang.String r13 = "com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener"
            android.os.IInterface r13 = r11.queryLocalInterface(r13)
            boolean r3 = r13 instanceof com.google.android.gms.internal.zzui
            if (r3 == 0) goto L_0x01ce
            com.google.android.gms.internal.zzui r13 = (com.google.android.gms.internal.zzui) r13
            goto L_0x01d3
        L_0x01ce:
            com.google.android.gms.internal.zzuk r13 = new com.google.android.gms.internal.zzuk
            r13.<init>(r11)
        L_0x01d3:
            r9.zza(r10, r1, r2, r13)
            goto L_0x01a2
        L_0x01d7:
            com.google.android.gms.dynamic.IObjectWrapper r10 = r9.getView()
        L_0x01db:
            r12.writeNoException()
            com.google.android.gms.internal.zzef.zza(r12, r10)
            return r0
        L_0x01e2:
            android.os.IBinder r10 = r11.readStrongBinder()
            com.google.android.gms.dynamic.IObjectWrapper r2 = com.google.android.gms.dynamic.IObjectWrapper.zza.zzap(r10)
            android.os.Parcelable$Creator<com.google.android.gms.internal.zziw> r10 = com.google.android.gms.internal.zziw.CREATOR
            android.os.Parcelable r10 = com.google.android.gms.internal.zzef.zza(r11, r10)
            r3 = r10
            com.google.android.gms.internal.zziw r3 = (com.google.android.gms.internal.zziw) r3
            android.os.Parcelable$Creator<com.google.android.gms.internal.zzis> r10 = com.google.android.gms.internal.zzis.CREATOR
            android.os.Parcelable r10 = com.google.android.gms.internal.zzef.zza(r11, r10)
            r4 = r10
            com.google.android.gms.internal.zzis r4 = (com.google.android.gms.internal.zzis) r4
            java.lang.String r5 = r11.readString()
            android.os.IBinder r10 = r11.readStrongBinder()
            if (r10 != 0) goto L_0x0208
        L_0x0206:
            r6 = r13
            goto L_0x021c
        L_0x0208:
            java.lang.String r11 = "com.google.android.gms.ads.internal.mediation.client.IMediationAdapterListener"
            android.os.IInterface r11 = r10.queryLocalInterface(r11)
            boolean r13 = r11 instanceof com.google.android.gms.internal.zzui
            if (r13 == 0) goto L_0x0216
            r13 = r11
            com.google.android.gms.internal.zzui r13 = (com.google.android.gms.internal.zzui) r13
            goto L_0x0206
        L_0x0216:
            com.google.android.gms.internal.zzuk r13 = new com.google.android.gms.internal.zzuk
            r13.<init>(r10)
            goto L_0x0206
        L_0x021c:
            r1 = r9
            r1.zza(r2, r3, r4, r5, r6)
            goto L_0x01a2
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzug.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
