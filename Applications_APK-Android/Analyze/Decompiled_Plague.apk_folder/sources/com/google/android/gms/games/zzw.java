package com.google.android.gms.games;

import android.support.annotation.NonNull;
import com.google.android.gms.games.GamesMetadata;
import com.google.android.gms.games.internal.zzo;

final class zzw implements zzo<GamesMetadata.LoadGamesResult> {
    zzw() {
    }

    public final /* synthetic */ void release(@NonNull Object obj) {
        GamesMetadata.LoadGamesResult loadGamesResult = (GamesMetadata.LoadGamesResult) obj;
        if (loadGamesResult.getGames() != null) {
            loadGamesResult.getGames().release();
        }
    }
}
