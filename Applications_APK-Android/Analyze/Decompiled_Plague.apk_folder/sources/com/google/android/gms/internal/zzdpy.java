package com.google.android.gms.internal;

import java.io.IOException;

public final class zzdpy extends zzfee<zzdpy, zza> implements zzffk {
    private static volatile zzffm<zzdpy> zzbas;
    /* access modifiers changed from: private */
    public static final zzdpy zzlqo;
    private int zzlqn;

    public static final class zza extends zzfef<zzdpy, zza> implements zzffk {
        private zza() {
            super(zzdpy.zzlqo);
        }

        /* synthetic */ zza(zzdpz zzdpz) {
            this();
        }
    }

    static {
        zzdpy zzdpy = new zzdpy();
        zzlqo = zzdpy;
        zzdpy.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdpy.zzpbs.zzbim();
    }

    private zzdpy() {
    }

    public static zzdpy zzbma() {
        return zzlqo;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        boolean z = true;
        boolean z2 = false;
        switch (zzdpz.zzbaq[i - 1]) {
            case 1:
                return new zzdpy();
            case 2:
                return zzlqo;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdpy zzdpy = (zzdpy) obj2;
                boolean z3 = this.zzlqn != 0;
                int i2 = this.zzlqn;
                if (zzdpy.zzlqn == 0) {
                    z = false;
                }
                this.zzlqn = zzfen.zza(z3, i2, z, zzdpy.zzlqn);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                if (((zzfea) obj2) != null) {
                    while (!z2) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 8) {
                                    this.zzlqn = zzfdq.zzcub();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z2 = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdpy.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlqo);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlqo;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlqn != 0) {
            zzfdv.zzab(1, this.zzlqn);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final int zzblz() {
        return this.zzlqn;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlqn != 0) {
            i2 = 0 + zzfdv.zzae(1, this.zzlqn);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
