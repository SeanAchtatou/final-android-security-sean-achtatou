package com.google.android.gms.internal;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzbpl extends IInterface {
    void zzaa(boolean z) throws RemoteException;
}
