package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzbfp implements Parcelable.Creator<zzbfs> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        String str = null;
        int i = 0;
        zzbfl zzbfl = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = zzbek.zzg(parcel, readInt);
                    break;
                case 2:
                    str = zzbek.zzq(parcel, readInt);
                    break;
                case 3:
                    zzbfl = (zzbfl) zzbek.zza(parcel, readInt, zzbfl.CREATOR);
                    break;
                default:
                    zzbek.zzb(parcel, readInt);
                    break;
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzbfs(i, str, zzbfl);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbfs[i];
    }
}
