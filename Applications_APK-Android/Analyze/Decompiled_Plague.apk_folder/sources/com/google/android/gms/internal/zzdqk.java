package com.google.android.gms.internal;

import java.io.IOException;

public final class zzdqk extends zzfee<zzdqk, zza> implements zzffk {
    private static volatile zzffm<zzdqk> zzbas;
    /* access modifiers changed from: private */
    public static final zzdqk zzlqw;

    public static final class zza extends zzfef<zzdqk, zza> implements zzffk {
        private zza() {
            super(zzdqk.zzlqw);
        }

        /* synthetic */ zza(zzdql zzdql) {
            this();
        }
    }

    static {
        zzdqk zzdqk = new zzdqk();
        zzlqw = zzdqk;
        zzdqk.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdqk.zzpbs.zzbim();
    }

    private zzdqk() {
    }

    public static zzdqk zzbmm() {
        return zzlqw;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdql.zzbaq[i - 1]) {
            case 1:
                return new zzdqk();
            case 2:
                return zzlqw;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                if (((zzfea) obj2) != null) {
                    boolean z = false;
                    while (!z) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts == 0 || !zza(zzcts, zzfdq)) {
                                z = true;
                            }
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdqk.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlqw);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlqw;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        this.zzpbs.zza(zzfdv);
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int zzhl = 0 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
