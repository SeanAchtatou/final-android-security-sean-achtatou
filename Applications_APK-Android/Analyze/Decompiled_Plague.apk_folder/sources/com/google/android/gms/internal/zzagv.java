package com.google.android.gms.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class zzagv extends BroadcastReceiver {
    private /* synthetic */ zzagr zzczl;

    private zzagv(zzagr zzagr) {
        this.zzczl = zzagr;
    }

    /* synthetic */ zzagv(zzagr zzagr, zzags zzags) {
        this(zzagr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzagr.zza(com.google.android.gms.internal.zzagr, boolean):boolean
     arg types: [com.google.android.gms.internal.zzagr, int]
     candidates:
      com.google.android.gms.internal.zzagr.zza(com.google.android.gms.internal.zzagr, java.lang.String):java.lang.String
      com.google.android.gms.internal.zzagr.zza(android.app.Activity, android.view.ViewTreeObserver$OnGlobalLayoutListener):void
      com.google.android.gms.internal.zzagr.zza(android.app.Activity, android.view.ViewTreeObserver$OnScrollChangedListener):void
      com.google.android.gms.internal.zzagr.zza(android.content.Context, android.content.Intent):void
      com.google.android.gms.internal.zzagr.zza(android.content.Context, android.net.Uri):void
      com.google.android.gms.internal.zzagr.zza(org.json.JSONArray, java.lang.Object):void
      com.google.android.gms.internal.zzagr.zza(android.app.Activity, android.content.res.Configuration):boolean
      com.google.android.gms.internal.zzagr.zza(android.os.Bundle, org.json.JSONObject):org.json.JSONObject
      com.google.android.gms.internal.zzagr.zza(android.content.Context, java.util.List<java.lang.String>):void
      com.google.android.gms.internal.zzagr.zza(android.view.View, android.content.Context):boolean
      com.google.android.gms.internal.zzagr.zza(com.google.android.gms.internal.zzagr, boolean):boolean */
    public final void onReceive(Context context, Intent intent) {
        if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            boolean unused = this.zzczl.zzczf = true;
        } else if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
            boolean unused2 = this.zzczl.zzczf = false;
        }
    }
}
