package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzr;
import com.google.android.gms.common.zze;
import com.google.android.gms.internal.zzcwb;
import com.google.android.gms.internal.zzcwc;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public final class zzbl implements zzcf, zzx {
    private final Context mContext;
    private Api.zza<? extends zzcwb, zzcwc> zzfkf;
    final zzbd zzfmo;
    /* access modifiers changed from: private */
    public final Lock zzfmy;
    private zzr zzfnd;
    private Map<Api<?>, Boolean> zzfng;
    private final zze zzfni;
    final Map<Api.zzc<?>, Api.zze> zzfph;
    private final Condition zzfpu;
    private final zzbn zzfpv;
    final Map<Api.zzc<?>, ConnectionResult> zzfpw = new HashMap();
    /* access modifiers changed from: private */
    public volatile zzbk zzfpx;
    private ConnectionResult zzfpy = null;
    int zzfpz;
    final zzcg zzfqa;

    public zzbl(Context context, zzbd zzbd, Lock lock, Looper looper, zze zze, Map<Api.zzc<?>, Api.zze> map, zzr zzr, Map<Api<?>, Boolean> map2, Api.zza<? extends zzcwb, zzcwc> zza, ArrayList<zzw> arrayList, zzcg zzcg) {
        this.mContext = context;
        this.zzfmy = lock;
        this.zzfni = zze;
        this.zzfph = map;
        this.zzfnd = zzr;
        this.zzfng = map2;
        this.zzfkf = zza;
        this.zzfmo = zzbd;
        this.zzfqa = zzcg;
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            ((zzw) obj).zza(this);
        }
        this.zzfpv = new zzbn(this, looper);
        this.zzfpu = lock.newCondition();
        this.zzfpx = new zzbc(this);
    }

    public final ConnectionResult blockingConnect() {
        connect();
        while (isConnecting()) {
            try {
                this.zzfpu.await();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                return new ConnectionResult(15, null);
            }
        }
        return isConnected() ? ConnectionResult.zzfhy : this.zzfpy != null ? this.zzfpy : new ConnectionResult(13, null);
    }

    public final ConnectionResult blockingConnect(long j, TimeUnit timeUnit) {
        connect();
        long nanos = timeUnit.toNanos(j);
        while (isConnecting()) {
            if (nanos <= 0) {
                try {
                    disconnect();
                    return new ConnectionResult(14, null);
                } catch (InterruptedException unused) {
                    Thread.currentThread().interrupt();
                    return new ConnectionResult(15, null);
                }
            } else {
                nanos = this.zzfpu.awaitNanos(nanos);
            }
        }
        return isConnected() ? ConnectionResult.zzfhy : this.zzfpy != null ? this.zzfpy : new ConnectionResult(13, null);
    }

    public final void connect() {
        this.zzfpx.connect();
    }

    public final void disconnect() {
        if (this.zzfpx.disconnect()) {
            this.zzfpw.clear();
        }
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String concat = String.valueOf(str).concat("  ");
        printWriter.append((CharSequence) str).append((CharSequence) "mState=").println(this.zzfpx);
        for (Api next : this.zzfng.keySet()) {
            printWriter.append((CharSequence) str).append((CharSequence) next.getName()).println(":");
            this.zzfph.get(next.zzaft()).dump(concat, fileDescriptor, printWriter, strArr);
        }
    }

    @Nullable
    public final ConnectionResult getConnectionResult(@NonNull Api<?> api) {
        Api.zzc<?> zzaft = api.zzaft();
        if (!this.zzfph.containsKey(zzaft)) {
            return null;
        }
        if (this.zzfph.get(zzaft).isConnected()) {
            return ConnectionResult.zzfhy;
        }
        if (this.zzfpw.containsKey(zzaft)) {
            return this.zzfpw.get(zzaft);
        }
        return null;
    }

    public final boolean isConnected() {
        return this.zzfpx instanceof zzao;
    }

    public final boolean isConnecting() {
        return this.zzfpx instanceof zzar;
    }

    public final void onConnected(@Nullable Bundle bundle) {
        this.zzfmy.lock();
        try {
            this.zzfpx.onConnected(bundle);
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final void onConnectionSuspended(int i) {
        this.zzfmy.lock();
        try {
            this.zzfpx.onConnectionSuspended(i);
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final void zza(@NonNull ConnectionResult connectionResult, @NonNull Api<?> api, boolean z) {
        this.zzfmy.lock();
        try {
            this.zzfpx.zza(connectionResult, api, z);
        } finally {
            this.zzfmy.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzbm zzbm) {
        this.zzfpv.sendMessage(this.zzfpv.obtainMessage(1, zzbm));
    }

    /* access modifiers changed from: package-private */
    public final void zza(RuntimeException runtimeException) {
        this.zzfpv.sendMessage(this.zzfpv.obtainMessage(2, runtimeException));
    }

    public final boolean zza(zzcx zzcx) {
        return false;
    }

    public final void zzagf() {
    }

    public final void zzagy() {
        if (isConnected()) {
            ((zzao) this.zzfpx).zzaho();
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzaib() {
        this.zzfmy.lock();
        try {
            this.zzfpx = new zzar(this, this.zzfnd, this.zzfng, this.zzfni, this.zzfkf, this.zzfmy, this.mContext);
            this.zzfpx.begin();
            this.zzfpu.signalAll();
        } finally {
            this.zzfmy.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzaic() {
        this.zzfmy.lock();
        try {
            this.zzfmo.zzahy();
            this.zzfpx = new zzao(this);
            this.zzfpx.begin();
            this.zzfpu.signalAll();
        } finally {
            this.zzfmy.unlock();
        }
    }

    public final <A extends Api.zzb, R extends Result, T extends zzm<R, A>> T zzd(@NonNull T t) {
        t.zzagw();
        return this.zzfpx.zzd(t);
    }

    public final <A extends Api.zzb, T extends zzm<? extends Result, A>> T zze(@NonNull T t) {
        t.zzagw();
        return this.zzfpx.zze(t);
    }

    /* access modifiers changed from: package-private */
    public final void zzg(ConnectionResult connectionResult) {
        this.zzfmy.lock();
        try {
            this.zzfpy = connectionResult;
            this.zzfpx = new zzbc(this);
            this.zzfpx.begin();
            this.zzfpu.signalAll();
        } finally {
            this.zzfmy.unlock();
        }
    }
}
