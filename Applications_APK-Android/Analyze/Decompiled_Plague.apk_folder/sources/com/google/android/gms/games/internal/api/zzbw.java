package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzbw extends zzcb {
    private /* synthetic */ String[] zzhqk;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbw(zzbv zzbv, GoogleApiClient googleApiClient, String[] strArr) {
        super(googleApiClient, null);
        this.zzhqk = strArr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zza(this, this.zzhqk);
    }
}
