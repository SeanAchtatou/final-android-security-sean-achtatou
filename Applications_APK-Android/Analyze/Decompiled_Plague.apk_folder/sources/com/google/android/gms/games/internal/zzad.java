package com.google.android.gms.games.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import com.google.android.gms.common.util.zzq;
import java.lang.ref.WeakReference;

@TargetApi(12)
final class zzad extends zzaa implements View.OnAttachStateChangeListener, ViewTreeObserver.OnGlobalLayoutListener {
    private boolean zzhmr = false;
    private WeakReference<View> zzhpu;

    protected zzad(GamesClientImpl gamesClientImpl, int i) {
        super(gamesClientImpl, i);
    }

    @TargetApi(17)
    private final void zzw(View view) {
        Display display;
        int i = -1;
        if (zzq.zzalx() && (display = view.getDisplay()) != null) {
            i = display.getDisplayId();
        }
        IBinder windowToken = view.getWindowToken();
        int[] iArr = new int[2];
        view.getLocationInWindow(iArr);
        int width = view.getWidth();
        int height = view.getHeight();
        this.zzhpr.zzhpt = i;
        this.zzhpr.zzhps = windowToken;
        this.zzhpr.left = iArr[0];
        this.zzhpr.top = iArr[1];
        this.zzhpr.right = iArr[0] + width;
        this.zzhpr.bottom = iArr[1] + height;
        if (this.zzhmr) {
            zzasz();
        }
    }

    public final void onGlobalLayout() {
        View view;
        if (this.zzhpu != null && (view = this.zzhpu.get()) != null) {
            zzw(view);
        }
    }

    public final void onViewAttachedToWindow(View view) {
        zzw(view);
    }

    public final void onViewDetachedFromWindow(View view) {
        this.zzhpq.zzass();
        view.removeOnAttachStateChangeListener(this);
    }

    public final void zzasz() {
        boolean z;
        if (this.zzhpr.zzhps != null) {
            super.zzasz();
            z = false;
        } else {
            z = true;
        }
        this.zzhmr = z;
    }

    /* access modifiers changed from: protected */
    public final void zzdl(int i) {
        this.zzhpr = new zzac(i, null);
    }

    @TargetApi(16)
    public final void zzv(View view) {
        this.zzhpq.zzass();
        if (this.zzhpu != null) {
            View view2 = this.zzhpu.get();
            Context context = this.zzhpq.getContext();
            if (view2 == null && (context instanceof Activity)) {
                view2 = ((Activity) context).getWindow().getDecorView();
            }
            if (view2 != null) {
                view2.removeOnAttachStateChangeListener(this);
                ViewTreeObserver viewTreeObserver = view2.getViewTreeObserver();
                if (zzq.zzalw()) {
                    viewTreeObserver.removeOnGlobalLayoutListener(this);
                } else {
                    viewTreeObserver.removeGlobalOnLayoutListener(this);
                }
            }
        }
        this.zzhpu = null;
        Context context2 = this.zzhpq.getContext();
        if (view == null && (context2 instanceof Activity)) {
            Activity activity = (Activity) context2;
            view = activity.findViewById(16908290);
            if (view == null) {
                view = activity.getWindow().getDecorView();
            }
            zzf.zzv("PopupManager", "You have not specified a View to use as content view for popups. Falling back to the Activity content view. Note that this may not work as expected in multi-screen environments");
        }
        if (view != null) {
            zzw(view);
            this.zzhpu = new WeakReference<>(view);
            view.addOnAttachStateChangeListener(this);
            view.getViewTreeObserver().addOnGlobalLayoutListener(this);
            return;
        }
        zzf.zzw("PopupManager", "No content view usable to display popups. Popups will not be displayed in response to this client's calls. Use setViewForPopups() to set your content view.");
    }
}
