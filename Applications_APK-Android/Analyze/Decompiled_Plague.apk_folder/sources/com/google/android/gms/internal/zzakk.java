package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.view.Surface;
import android.view.TextureView;
import com.google.android.gms.ads.internal.zzbs;
import com.miniclip.googlebilling.IabHelper;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@zzzb
@TargetApi(14)
public final class zzakk extends zzaku implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnVideoSizeChangedListener, TextureView.SurfaceTextureListener {
    private static final Map<Integer, String> zzddi = new HashMap();
    private final zzalj zzddj;
    private final boolean zzddk;
    private int zzddl = 0;
    private int zzddm = 0;
    private MediaPlayer zzddn;
    private Uri zzddo;
    private int zzddp;
    private int zzddq;
    private int zzddr;
    private int zzdds;
    private int zzddt;
    private zzalg zzddu;
    private boolean zzddv;
    private int zzddw;
    /* access modifiers changed from: private */
    public zzakt zzddx;

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            zzddi.put(Integer.valueOf((int) IabHelper.IABHELPER_SEND_INTENT_FAILED), "MEDIA_ERROR_IO");
            zzddi.put(Integer.valueOf((int) IabHelper.IABHELPER_MISSING_TOKEN), "MEDIA_ERROR_MALFORMED");
            zzddi.put(Integer.valueOf((int) IabHelper.IABHELPER_INVALID_CONSUMPTION), "MEDIA_ERROR_UNSUPPORTED");
            zzddi.put(-110, "MEDIA_ERROR_TIMED_OUT");
            zzddi.put(3, "MEDIA_INFO_VIDEO_RENDERING_START");
        }
        zzddi.put(100, "MEDIA_ERROR_SERVER_DIED");
        zzddi.put(1, "MEDIA_ERROR_UNKNOWN");
        zzddi.put(1, "MEDIA_INFO_UNKNOWN");
        zzddi.put(700, "MEDIA_INFO_VIDEO_TRACK_LAGGING");
        zzddi.put(701, "MEDIA_INFO_BUFFERING_START");
        zzddi.put(702, "MEDIA_INFO_BUFFERING_END");
        zzddi.put(800, "MEDIA_INFO_BAD_INTERLEAVING");
        zzddi.put(801, "MEDIA_INFO_NOT_SEEKABLE");
        zzddi.put(802, "MEDIA_INFO_METADATA_UPDATE");
        if (Build.VERSION.SDK_INT >= 19) {
            zzddi.put(901, "MEDIA_INFO_UNSUPPORTED_SUBTITLE");
            zzddi.put(902, "MEDIA_INFO_SUBTITLE_TIMED_OUT");
        }
    }

    public zzakk(Context context, boolean z, boolean z2, zzalh zzalh, zzalj zzalj) {
        super(context);
        setSurfaceTextureListener(this);
        this.zzddj = zzalj;
        this.zzddv = z;
        this.zzddk = z2;
        this.zzddj.zza(this);
    }

    private final void zza(float f) {
        if (this.zzddn != null) {
            try {
                this.zzddn.setVolume(f, f);
            } catch (IllegalStateException unused) {
            }
        } else {
            zzafj.zzco("AdMediaPlayerView setMediaPlayerVolume() called before onPrepared().");
        }
    }

    private final void zzaa(boolean z) {
        zzafj.v("AdMediaPlayerView release");
        if (this.zzddu != null) {
            this.zzddu.zzrs();
            this.zzddu = null;
        }
        if (this.zzddn != null) {
            this.zzddn.reset();
            this.zzddn.release();
            this.zzddn = null;
            zzaf(0);
            if (z) {
                this.zzddm = 0;
                this.zzddm = 0;
            }
        }
    }

    private final void zzaf(int i) {
        if (i == 3) {
            this.zzddj.zzse();
            this.zzdee.zzse();
        } else if (this.zzddl == 3) {
            this.zzddj.zzsf();
            this.zzdee.zzsf();
        }
        this.zzddl = i;
    }

    private final void zzra() {
        zzafj.v("AdMediaPlayerView init MediaPlayer");
        SurfaceTexture surfaceTexture = getSurfaceTexture();
        if (this.zzddo != null && surfaceTexture != null) {
            zzaa(false);
            try {
                zzbs.zzeu();
                this.zzddn = new MediaPlayer();
                this.zzddn.setOnBufferingUpdateListener(this);
                this.zzddn.setOnCompletionListener(this);
                this.zzddn.setOnErrorListener(this);
                this.zzddn.setOnInfoListener(this);
                this.zzddn.setOnPreparedListener(this);
                this.zzddn.setOnVideoSizeChangedListener(this);
                this.zzddr = 0;
                if (this.zzddv) {
                    this.zzddu = new zzalg(getContext());
                    this.zzddu.zza(surfaceTexture, getWidth(), getHeight());
                    this.zzddu.start();
                    SurfaceTexture zzrt = this.zzddu.zzrt();
                    if (zzrt != null) {
                        surfaceTexture = zzrt;
                    } else {
                        this.zzddu.zzrs();
                        this.zzddu = null;
                    }
                }
                this.zzddn.setDataSource(getContext(), this.zzddo);
                zzbs.zzev();
                this.zzddn.setSurface(new Surface(surfaceTexture));
                this.zzddn.setAudioStreamType(3);
                this.zzddn.setScreenOnWhilePlaying(true);
                this.zzddn.prepareAsync();
                zzaf(1);
            } catch (IOException | IllegalArgumentException | IllegalStateException e) {
                String valueOf = String.valueOf(this.zzddo);
                StringBuilder sb = new StringBuilder(36 + String.valueOf(valueOf).length());
                sb.append("Failed to initialize MediaPlayer at ");
                sb.append(valueOf);
                zzafj.zzc(sb.toString(), e);
                onError(this.zzddn, 1, 0);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzrb() {
        /*
            r8 = this;
            boolean r0 = r8.zzddk
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            boolean r0 = r8.zzrc()
            if (r0 == 0) goto L_0x005a
            android.media.MediaPlayer r0 = r8.zzddn
            int r0 = r0.getCurrentPosition()
            if (r0 <= 0) goto L_0x005a
            int r0 = r8.zzddm
            r1 = 3
            if (r0 == r1) goto L_0x005a
            java.lang.String r0 = "AdMediaPlayerView nudging MediaPlayer"
            com.google.android.gms.internal.zzafj.v(r0)
            r0 = 0
            r8.zza(r0)
            android.media.MediaPlayer r0 = r8.zzddn
            r0.start()
            android.media.MediaPlayer r0 = r8.zzddn
            int r0 = r0.getCurrentPosition()
            com.google.android.gms.common.util.zzd r1 = com.google.android.gms.ads.internal.zzbs.zzei()
            long r1 = r1.currentTimeMillis()
        L_0x0034:
            boolean r3 = r8.zzrc()
            if (r3 == 0) goto L_0x0052
            android.media.MediaPlayer r3 = r8.zzddn
            int r3 = r3.getCurrentPosition()
            if (r3 != r0) goto L_0x0052
            com.google.android.gms.common.util.zzd r3 = com.google.android.gms.ads.internal.zzbs.zzei()
            long r3 = r3.currentTimeMillis()
            long r5 = r3 - r1
            r3 = 250(0xfa, double:1.235E-321)
            int r7 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r7 <= 0) goto L_0x0034
        L_0x0052:
            android.media.MediaPlayer r0 = r8.zzddn
            r0.pause()
            r8.zzrd()
        L_0x005a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzakk.zzrb():void");
    }

    private final boolean zzrc() {
        return (this.zzddn == null || this.zzddl == -1 || this.zzddl == 0 || this.zzddl == 1) ? false : true;
    }

    public final int getCurrentPosition() {
        if (zzrc()) {
            return this.zzddn.getCurrentPosition();
        }
        return 0;
    }

    public final int getDuration() {
        if (zzrc()) {
            return this.zzddn.getDuration();
        }
        return -1;
    }

    public final int getVideoHeight() {
        if (this.zzddn != null) {
            return this.zzddn.getVideoHeight();
        }
        return 0;
    }

    public final int getVideoWidth() {
        if (this.zzddn != null) {
            return this.zzddn.getVideoWidth();
        }
        return 0;
    }

    public final void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
        this.zzddr = i;
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        zzafj.v("AdMediaPlayerView completion");
        zzaf(5);
        this.zzddm = 5;
        zzagr.zzczc.post(new zzakm(this));
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        String str = zzddi.get(Integer.valueOf(i));
        String str2 = zzddi.get(Integer.valueOf(i2));
        StringBuilder sb = new StringBuilder(38 + String.valueOf(str).length() + String.valueOf(str2).length());
        sb.append("AdMediaPlayerView MediaPlayer error: ");
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        zzafj.zzco(sb.toString());
        zzaf(-1);
        this.zzddm = -1;
        zzagr.zzczc.post(new zzakn(this, str, str2));
        return true;
    }

    public final boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
        String str = zzddi.get(Integer.valueOf(i));
        String str2 = zzddi.get(Integer.valueOf(i2));
        StringBuilder sb = new StringBuilder(37 + String.valueOf(str).length() + String.valueOf(str2).length());
        sb.append("AdMediaPlayerView MediaPlayer info: ");
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        zzafj.v(sb.toString());
        return true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0045, code lost:
        if ((r5.zzddp * r7) > (r5.zzddq * r6)) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006a, code lost:
        if (r1 > r6) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0082, code lost:
        if (r1 > r6) goto L_0x0047;
     */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onMeasure(int r6, int r7) {
        /*
            r5 = this;
            int r0 = r5.zzddp
            int r0 = getDefaultSize(r0, r6)
            int r1 = r5.zzddq
            int r1 = getDefaultSize(r1, r7)
            int r2 = r5.zzddp
            if (r2 <= 0) goto L_0x0085
            int r2 = r5.zzddq
            if (r2 <= 0) goto L_0x0085
            com.google.android.gms.internal.zzalg r2 = r5.zzddu
            if (r2 != 0) goto L_0x0085
            int r0 = android.view.View.MeasureSpec.getMode(r6)
            int r6 = android.view.View.MeasureSpec.getSize(r6)
            int r1 = android.view.View.MeasureSpec.getMode(r7)
            int r7 = android.view.View.MeasureSpec.getSize(r7)
            r2 = 1073741824(0x40000000, float:2.0)
            if (r0 != r2) goto L_0x004f
            if (r1 != r2) goto L_0x004f
            int r0 = r5.zzddp
            int r0 = r0 * r7
            int r1 = r5.zzddq
            int r1 = r1 * r6
            if (r0 >= r1) goto L_0x003f
            int r6 = r5.zzddp
            int r6 = r6 * r7
            int r0 = r5.zzddq
            int r0 = r6 / r0
            r6 = r0
            goto L_0x0087
        L_0x003f:
            int r0 = r5.zzddp
            int r0 = r0 * r7
            int r1 = r5.zzddq
            int r1 = r1 * r6
            if (r0 <= r1) goto L_0x0087
        L_0x0047:
            int r7 = r5.zzddq
            int r7 = r7 * r6
            int r0 = r5.zzddp
            int r1 = r7 / r0
            goto L_0x0086
        L_0x004f:
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r0 != r2) goto L_0x0060
            int r0 = r5.zzddq
            int r0 = r0 * r6
            int r2 = r5.zzddp
            int r0 = r0 / r2
            if (r1 != r3) goto L_0x005e
            if (r0 <= r7) goto L_0x005e
            goto L_0x0087
        L_0x005e:
            r7 = r0
            goto L_0x0087
        L_0x0060:
            if (r1 != r2) goto L_0x006f
            int r1 = r5.zzddp
            int r1 = r1 * r7
            int r2 = r5.zzddq
            int r1 = r1 / r2
            if (r0 != r3) goto L_0x006d
            if (r1 <= r6) goto L_0x006d
            goto L_0x0087
        L_0x006d:
            r6 = r1
            goto L_0x0087
        L_0x006f:
            int r2 = r5.zzddp
            int r4 = r5.zzddq
            if (r1 != r3) goto L_0x007e
            if (r4 <= r7) goto L_0x007e
            int r1 = r5.zzddp
            int r1 = r1 * r7
            int r2 = r5.zzddq
            int r1 = r1 / r2
            goto L_0x0080
        L_0x007e:
            r1 = r2
            r7 = r4
        L_0x0080:
            if (r0 != r3) goto L_0x006d
            if (r1 <= r6) goto L_0x006d
            goto L_0x0047
        L_0x0085:
            r6 = r0
        L_0x0086:
            r7 = r1
        L_0x0087:
            r5.setMeasuredDimension(r6, r7)
            com.google.android.gms.internal.zzalg r0 = r5.zzddu
            if (r0 == 0) goto L_0x0093
            com.google.android.gms.internal.zzalg r0 = r5.zzddu
            r0.zzh(r6, r7)
        L_0x0093:
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 16
            if (r0 != r1) goto L_0x00b0
            int r0 = r5.zzdds
            if (r0 <= 0) goto L_0x00a1
            int r0 = r5.zzdds
            if (r0 != r6) goto L_0x00a9
        L_0x00a1:
            int r0 = r5.zzddt
            if (r0 <= 0) goto L_0x00ac
            int r0 = r5.zzddt
            if (r0 == r7) goto L_0x00ac
        L_0x00a9:
            r5.zzrb()
        L_0x00ac:
            r5.zzdds = r6
            r5.zzddt = r7
        L_0x00b0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzakk.onMeasure(int, int):void");
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        zzafj.v("AdMediaPlayerView prepared");
        zzaf(2);
        this.zzddj.zzrf();
        zzagr.zzczc.post(new zzakl(this));
        this.zzddp = mediaPlayer.getVideoWidth();
        this.zzddq = mediaPlayer.getVideoHeight();
        if (this.zzddw != 0) {
            seekTo(this.zzddw);
        }
        zzrb();
        int i = this.zzddp;
        int i2 = this.zzddq;
        StringBuilder sb = new StringBuilder(62);
        sb.append("AdMediaPlayerView stream dimensions: ");
        sb.append(i);
        sb.append(" x ");
        sb.append(i2);
        zzafj.zzcn(sb.toString());
        if (this.zzddm == 3) {
            play();
        }
        zzrd();
    }

    public final void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        zzafj.v("AdMediaPlayerView surface created");
        zzra();
        zzagr.zzczc.post(new zzako(this));
    }

    public final boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        zzafj.v("AdMediaPlayerView surface destroyed");
        if (this.zzddn != null && this.zzddw == 0) {
            this.zzddw = this.zzddn.getCurrentPosition();
        }
        if (this.zzddu != null) {
            this.zzddu.zzrs();
        }
        zzagr.zzczc.post(new zzakq(this));
        zzaa(true);
        return true;
    }

    public final void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        zzafj.v("AdMediaPlayerView surface changed");
        boolean z = false;
        boolean z2 = this.zzddm == 3;
        if (this.zzddp == i && this.zzddq == i2) {
            z = true;
        }
        if (this.zzddn != null && z2 && z) {
            if (this.zzddw != 0) {
                seekTo(this.zzddw);
            }
            play();
        }
        if (this.zzddu != null) {
            this.zzddu.zzh(i, i2);
        }
        zzagr.zzczc.post(new zzakp(this, i, i2));
    }

    public final void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        this.zzddj.zzb(this);
        this.zzded.zza(surfaceTexture, this.zzddx);
    }

    public final void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
        StringBuilder sb = new StringBuilder(57);
        sb.append("AdMediaPlayerView size changed: ");
        sb.append(i);
        sb.append(" x ");
        sb.append(i2);
        zzafj.v(sb.toString());
        this.zzddp = mediaPlayer.getVideoWidth();
        this.zzddq = mediaPlayer.getVideoHeight();
        if (this.zzddp != 0 && this.zzddq != 0) {
            requestLayout();
        }
    }

    public final void pause() {
        zzafj.v("AdMediaPlayerView pause");
        if (zzrc() && this.zzddn.isPlaying()) {
            this.zzddn.pause();
            zzaf(4);
            zzagr.zzczc.post(new zzaks(this));
        }
        this.zzddm = 4;
    }

    public final void play() {
        zzafj.v("AdMediaPlayerView play");
        if (zzrc()) {
            this.zzddn.start();
            zzaf(3);
            this.zzded.zzrg();
            zzagr.zzczc.post(new zzakr(this));
        }
        this.zzddm = 3;
    }

    public final void seekTo(int i) {
        StringBuilder sb = new StringBuilder(34);
        sb.append("AdMediaPlayerView seek ");
        sb.append(i);
        zzafj.v(sb.toString());
        if (zzrc()) {
            this.zzddn.seekTo(i);
            this.zzddw = 0;
            return;
        }
        this.zzddw = i;
    }

    public final void setVideoPath(String str) {
        Uri parse = Uri.parse(str);
        zzhu zzd = zzhu.zzd(parse);
        if (zzd != null) {
            parse = Uri.parse(zzd.url);
        }
        this.zzddo = parse;
        this.zzddw = 0;
        zzra();
        requestLayout();
        invalidate();
    }

    public final void stop() {
        zzafj.v("AdMediaPlayerView stop");
        if (this.zzddn != null) {
            this.zzddn.stop();
            this.zzddn.release();
            this.zzddn = null;
            zzaf(0);
            this.zzddm = 0;
        }
        this.zzddj.onStop();
    }

    public final String toString() {
        String name = getClass().getName();
        String hexString = Integer.toHexString(hashCode());
        StringBuilder sb = new StringBuilder(1 + String.valueOf(name).length() + String.valueOf(hexString).length());
        sb.append(name);
        sb.append("@");
        sb.append(hexString);
        return sb.toString();
    }

    public final void zza(float f, float f2) {
        if (this.zzddu != null) {
            this.zzddu.zzb(f, f2);
        }
    }

    public final void zza(zzakt zzakt) {
        this.zzddx = zzakt;
    }

    public final String zzqz() {
        String valueOf = String.valueOf(this.zzddv ? " spherical" : "");
        return valueOf.length() != 0 ? "MediaPlayer".concat(valueOf) : new String("MediaPlayer");
    }

    public final void zzrd() {
        zza(this.zzdee.getVolume());
    }
}
