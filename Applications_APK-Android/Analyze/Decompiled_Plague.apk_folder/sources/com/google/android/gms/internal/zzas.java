package com.google.android.gms.internal;

import android.support.v4.view.MotionEventCompat;
import com.miniclip.input.MCInput;
import com.mopub.mobileads.resource.DrawableConstants;
import java.io.IOException;

public final class zzas extends zzfhe<zzas> {
    private String stackTrace = null;
    public String zzch = null;
    public Long zzci = null;
    private String zzcj = null;
    private String zzck = null;
    private Long zzcl = null;
    private Long zzcm = null;
    private String zzcn = null;
    private Long zzco = null;
    private String zzcp = null;

    public zzas() {
        this.zzpai = -1;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            switch (zzcts) {
                case 0:
                    return this;
                case 10:
                    this.zzch = zzfhb.readString();
                    break;
                case 16:
                    this.zzci = Long.valueOf(zzfhb.zzcum());
                    break;
                case MotionEventCompat.AXIS_SCROLL:
                    this.stackTrace = zzfhb.readString();
                    break;
                case MotionEventCompat.AXIS_GENERIC_3:
                    this.zzcj = zzfhb.readString();
                    break;
                case MotionEventCompat.AXIS_GENERIC_11:
                    this.zzck = zzfhb.readString();
                    break;
                case 48:
                    this.zzcl = Long.valueOf(zzfhb.zzcum());
                    break;
                case DrawableConstants.CloseButton.WIDGET_HEIGHT_DIPS /*56*/:
                    this.zzcm = Long.valueOf(zzfhb.zzcum());
                    break;
                case 66:
                    this.zzcn = zzfhb.readString();
                    break;
                case DrawableConstants.GradientStrip.GRADIENT_STRIP_HEIGHT_DIPS /*72*/:
                    this.zzco = Long.valueOf(zzfhb.zzcum());
                    break;
                case MCInput.KEYCODE_MENU /*82*/:
                    this.zzcp = zzfhb.readString();
                    break;
                default:
                    if (super.zza(zzfhb, zzcts)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzch != null) {
            zzfhc.zzn(1, this.zzch);
        }
        if (this.zzci != null) {
            zzfhc.zzf(2, this.zzci.longValue());
        }
        if (this.stackTrace != null) {
            zzfhc.zzn(3, this.stackTrace);
        }
        if (this.zzcj != null) {
            zzfhc.zzn(4, this.zzcj);
        }
        if (this.zzck != null) {
            zzfhc.zzn(5, this.zzck);
        }
        if (this.zzcl != null) {
            zzfhc.zzf(6, this.zzcl.longValue());
        }
        if (this.zzcm != null) {
            zzfhc.zzf(7, this.zzcm.longValue());
        }
        if (this.zzcn != null) {
            zzfhc.zzn(8, this.zzcn);
        }
        if (this.zzco != null) {
            zzfhc.zzf(9, this.zzco.longValue());
        }
        if (this.zzcp != null) {
            zzfhc.zzn(10, this.zzcp);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzch != null) {
            zzo += zzfhc.zzo(1, this.zzch);
        }
        if (this.zzci != null) {
            zzo += zzfhc.zzc(2, this.zzci.longValue());
        }
        if (this.stackTrace != null) {
            zzo += zzfhc.zzo(3, this.stackTrace);
        }
        if (this.zzcj != null) {
            zzo += zzfhc.zzo(4, this.zzcj);
        }
        if (this.zzck != null) {
            zzo += zzfhc.zzo(5, this.zzck);
        }
        if (this.zzcl != null) {
            zzo += zzfhc.zzc(6, this.zzcl.longValue());
        }
        if (this.zzcm != null) {
            zzo += zzfhc.zzc(7, this.zzcm.longValue());
        }
        if (this.zzcn != null) {
            zzo += zzfhc.zzo(8, this.zzcn);
        }
        if (this.zzco != null) {
            zzo += zzfhc.zzc(9, this.zzco.longValue());
        }
        return this.zzcp != null ? zzo + zzfhc.zzo(10, this.zzcp) : zzo;
    }
}
