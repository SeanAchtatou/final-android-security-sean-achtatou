package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzez extends zzed implements zzex {
    zzez(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
    }

    public final String getId() throws RemoteException {
        Parcel zza = zza(1, zzaz());
        String readString = zza.readString();
        zza.recycle();
        return readString;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzef.zza(android.os.Parcel, boolean):void
     arg types: [android.os.Parcel, int]
     candidates:
      com.google.android.gms.internal.zzef.zza(android.os.Parcel, android.os.Parcelable$Creator):T
      com.google.android.gms.internal.zzef.zza(android.os.Parcel, android.os.IInterface):void
      com.google.android.gms.internal.zzef.zza(android.os.Parcel, android.os.Parcelable):void
      com.google.android.gms.internal.zzef.zza(android.os.Parcel, boolean):void */
    public final boolean zzb(boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, true);
        Parcel zza = zza(2, zzaz);
        boolean zza2 = zzef.zza(zza);
        zza.recycle();
        return zza2;
    }
}
