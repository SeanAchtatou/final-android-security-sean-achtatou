package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public final class zzah {
    /* access modifiers changed from: private */
    public final Map<zzs<?>, Boolean> zzfnu = Collections.synchronizedMap(new WeakHashMap());
    /* access modifiers changed from: private */
    public final Map<TaskCompletionSource<?>, Boolean> zzfnv = Collections.synchronizedMap(new WeakHashMap());

    private final void zza(boolean z, Status status) {
        HashMap hashMap;
        HashMap hashMap2;
        synchronized (this.zzfnu) {
            hashMap = new HashMap(this.zzfnu);
        }
        synchronized (this.zzfnv) {
            hashMap2 = new HashMap(this.zzfnv);
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            if (z || ((Boolean) entry.getValue()).booleanValue()) {
                ((zzs) entry.getKey()).zzv(status);
            }
        }
        for (Map.Entry entry2 : hashMap2.entrySet()) {
            if (z || ((Boolean) entry2.getValue()).booleanValue()) {
                ((TaskCompletionSource) entry2.getKey()).trySetException(new ApiException(status));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzs<? extends Result> zzs, boolean z) {
        this.zzfnu.put(zzs, Boolean.valueOf(z));
        zzs.zza(new zzai(this, zzs));
    }

    /* access modifiers changed from: package-private */
    public final <TResult> void zza(TaskCompletionSource<TResult> taskCompletionSource, boolean z) {
        this.zzfnv.put(taskCompletionSource, Boolean.valueOf(z));
        taskCompletionSource.getTask().addOnCompleteListener(new zzaj(this, taskCompletionSource));
    }

    /* access modifiers changed from: package-private */
    public final boolean zzahi() {
        return !this.zzfnu.isEmpty() || !this.zzfnv.isEmpty();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.zzah.zza(boolean, com.google.android.gms.common.api.Status):void
     arg types: [int, com.google.android.gms.common.api.Status]
     candidates:
      com.google.android.gms.common.api.internal.zzah.zza(com.google.android.gms.common.api.internal.zzs<? extends com.google.android.gms.common.api.Result>, boolean):void
      com.google.android.gms.common.api.internal.zzah.zza(com.google.android.gms.tasks.TaskCompletionSource, boolean):void
      com.google.android.gms.common.api.internal.zzah.zza(boolean, com.google.android.gms.common.api.Status):void */
    public final void zzahj() {
        zza(false, zzbp.zzfqe);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.zzah.zza(boolean, com.google.android.gms.common.api.Status):void
     arg types: [int, com.google.android.gms.common.api.Status]
     candidates:
      com.google.android.gms.common.api.internal.zzah.zza(com.google.android.gms.common.api.internal.zzs<? extends com.google.android.gms.common.api.Result>, boolean):void
      com.google.android.gms.common.api.internal.zzah.zza(com.google.android.gms.tasks.TaskCompletionSource, boolean):void
      com.google.android.gms.common.api.internal.zzah.zza(boolean, com.google.android.gms.common.api.Status):void */
    public final void zzahk() {
        zza(true, zzdl.zzfsm);
    }
}
