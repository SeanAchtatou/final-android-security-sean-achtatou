package com.google.android.gms.dynamic;

import android.os.Bundle;
import java.util.Iterator;

final class zzb implements zzo<T> {
    private /* synthetic */ zza zzgtn;

    zzb(zza zza) {
        this.zzgtn = zza;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.dynamic.zza.zza(com.google.android.gms.dynamic.zza, com.google.android.gms.dynamic.LifecycleDelegate):com.google.android.gms.dynamic.LifecycleDelegate
     arg types: [com.google.android.gms.dynamic.zza, T]
     candidates:
      com.google.android.gms.dynamic.zza.zza(com.google.android.gms.dynamic.zza, android.os.Bundle):android.os.Bundle
      com.google.android.gms.dynamic.zza.zza(android.os.Bundle, com.google.android.gms.dynamic.zzi):void
      com.google.android.gms.dynamic.zza.zza(com.google.android.gms.dynamic.zza, com.google.android.gms.dynamic.LifecycleDelegate):com.google.android.gms.dynamic.LifecycleDelegate */
    public final void zza(T t) {
        LifecycleDelegate unused = this.zzgtn.zzgtj = (LifecycleDelegate) t;
        Iterator it = this.zzgtn.zzgtl.iterator();
        while (it.hasNext()) {
            ((zzi) it.next()).zzb(this.zzgtn.zzgtj);
        }
        this.zzgtn.zzgtl.clear();
        Bundle unused2 = this.zzgtn.zzgtk = null;
    }
}
