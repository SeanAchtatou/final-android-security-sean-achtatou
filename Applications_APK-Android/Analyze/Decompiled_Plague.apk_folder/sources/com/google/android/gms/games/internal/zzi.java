package com.google.android.gms.games.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.common.internal.zzbo;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.TimeUnit;

final class zzi implements PendingResult.zza {
    private /* synthetic */ PendingResult zzfym;
    private /* synthetic */ TaskCompletionSource zzfyn;
    private /* synthetic */ zzp zzhpi;
    private /* synthetic */ zzbo zzhpj;
    private /* synthetic */ zzbo zzhpk;
    private /* synthetic */ zzn zzhpl;

    zzi(PendingResult pendingResult, zzp zzp, TaskCompletionSource taskCompletionSource, zzbo zzbo, zzbo zzbo2, zzn zzn) {
        this.zzfym = pendingResult;
        this.zzhpi = zzp;
        this.zzfyn = taskCompletionSource;
        this.zzhpj = zzbo;
        this.zzhpk = zzbo2;
        this.zzhpl = zzn;
    }

    public final void zzr(@NonNull Status status) {
        Result await = this.zzfym.await(0, TimeUnit.MILLISECONDS);
        if (this.zzhpi.zzag(status)) {
            this.zzfyn.setResult(this.zzhpj.zzb(await));
            return;
        }
        Object zzb = this.zzhpk.zzb(await);
        if (zzb != null) {
            this.zzfyn.setException(this.zzhpl.zza(zzg.zzah(status), zzb));
        } else {
            this.zzfyn.setException(zzb.zzy(zzg.zzah(status)));
        }
    }
}
