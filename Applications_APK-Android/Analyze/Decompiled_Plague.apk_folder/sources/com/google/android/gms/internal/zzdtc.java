package com.google.android.gms.internal;

import java.security.GeneralSecurityException;
import java.security.Key;
import javax.crypto.Mac;

public final class zzdtc implements zzdou {
    private Mac zzlvu;
    private final int zzlvv;
    private final String zzlvw;
    private final Key zzlvx;

    public zzdtc(String str, Key key, int i) throws GeneralSecurityException {
        this.zzlvw = str;
        this.zzlvv = i;
        this.zzlvx = key;
        this.zzlvu = zzdsr.zzlvl.zzoc(str);
        this.zzlvu.init(key);
    }

    public final byte[] zzab(byte[] bArr) throws GeneralSecurityException {
        Mac mac;
        try {
            mac = (Mac) this.zzlvu.clone();
        } catch (CloneNotSupportedException unused) {
            mac = zzdsr.zzlvl.zzoc(this.zzlvw);
            mac.init(this.zzlvx);
        }
        mac.update(bArr);
        byte[] bArr2 = new byte[this.zzlvv];
        System.arraycopy(mac.doFinal(), 0, bArr2, 0, this.zzlvv);
        return bArr2;
    }
}
