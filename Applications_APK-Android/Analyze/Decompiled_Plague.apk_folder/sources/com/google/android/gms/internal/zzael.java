package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.zzbs;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@zzzb
public final class zzael {
    private final AtomicReference<ThreadPoolExecutor> zzcve = new AtomicReference<>(null);
    private final Object zzcvf = new Object();
    @Nullable
    private String zzcvg = null;
    private AtomicBoolean zzcvh = new AtomicBoolean(false);
    private final AtomicInteger zzcvi = new AtomicInteger(-1);
    private final AtomicReference<Object> zzcvj = new AtomicReference<>(null);
    private final AtomicReference<Object> zzcvk = new AtomicReference<>(null);
    private ConcurrentMap<String, Method> zzcvl = new ConcurrentHashMap(9);

    private static Bundle zza(Context context, String str, boolean z) {
        Bundle bundle = new Bundle();
        try {
            bundle.putLong("_aeid", Long.parseLong(str));
        } catch (NullPointerException | NumberFormatException e) {
            String valueOf = String.valueOf(str);
            zzafj.zzb(valueOf.length() != 0 ? "Invalid event ID: ".concat(valueOf) : new String("Invalid event ID: "), e);
        }
        if (z) {
            bundle.putInt("_r", 1);
        }
        return bundle;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, boolean):android.os.Bundle
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final Object zza(String str, Context context) {
        if (!zza(context, "com.google.android.gms.measurement.AppMeasurement", this.zzcvj, true)) {
            return null;
        }
        try {
            return zzi(context, str).invoke(this.zzcvj.get(), new Object[0]);
        } catch (Exception e) {
            zza(e, str, true);
            return null;
        }
    }

    private final void zza(Exception exc, String str, boolean z) {
        if (!this.zzcvh.get()) {
            StringBuilder sb = new StringBuilder(30 + String.valueOf(str).length());
            sb.append("Invoke Firebase method ");
            sb.append(str);
            sb.append(" error.");
            zzafj.zzco(sb.toString());
            if (z) {
                zzafj.zzco("The Google Mobile Ads SDK will not integrate with Firebase. Admob/Firebase integration requires the latest Firebase SDK jar, but Firebase SDK is either missing or out of date");
                this.zzcvh.set(true);
            }
        }
    }

    private final boolean zza(Context context, String str, AtomicReference<Object> atomicReference, boolean z) {
        if (atomicReference.get() != null) {
            return true;
        }
        try {
            atomicReference.compareAndSet(null, context.getClassLoader().loadClass(str).getDeclaredMethod("getInstance", Context.class).invoke(null, context));
            return true;
        } catch (Exception e) {
            zza(e, "getInstance", z);
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, boolean):android.os.Bundle
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final Method zzaa(Context context) {
        Method method = this.zzcvl.get("logEventInternal");
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod("logEventInternal", String.class, String.class, Bundle.class);
            this.zzcvl.put("logEventInternal", declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, "logEventInternal", true);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, boolean):android.os.Bundle
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final void zzb(Context context, String str, String str2) {
        if (zza(context, "com.google.android.gms.measurement.AppMeasurement", this.zzcvj, true)) {
            Method zzh = zzh(context, str2);
            try {
                zzh.invoke(this.zzcvj.get(), str);
                StringBuilder sb = new StringBuilder(37 + String.valueOf(str2).length() + String.valueOf(str).length());
                sb.append("Invoke Firebase method ");
                sb.append(str2);
                sb.append(", Ad Unit Id: ");
                sb.append(str);
                zzafj.v(sb.toString());
            } catch (Exception e) {
                zza(e, str2, false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, boolean):android.os.Bundle
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final Method zzh(Context context, String str) {
        Method method = this.zzcvl.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod(str, String.class);
            this.zzcvl.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, str, false);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, boolean):android.os.Bundle
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final Method zzi(Context context, String str) {
        Method method = this.zzcvl.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod(str, new Class[0]);
            this.zzcvl.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, str, false);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, boolean):android.os.Bundle
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final Method zzj(Context context, String str) {
        Method method = this.zzcvl.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics").getDeclaredMethod(str, Activity.class, String.class, String.class);
            this.zzcvl.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, str, false);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, boolean):android.os.Bundle
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void */
    public final void zza(Context context, String str, String str2) {
        if (zzr(context) && zza(context, "com.google.android.gms.measurement.AppMeasurement", this.zzcvj, true)) {
            Method zzaa = zzaa(context);
            Bundle zza = zza(context, str2, "_ac".equals(str));
            try {
                zzaa.invoke(this.zzcvj.get(), "am", str, zza);
            } catch (Exception e) {
                zza(e, "logEventInternal", true);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ String zzab(Context context) throws Exception {
        return (String) zza("getAppInstanceId", context);
    }

    public final void zzb(Context context, String str) {
        if (zzr(context)) {
            zzb(context, str, "beginAdUnitExposure");
        }
    }

    public final void zzc(Context context, String str) {
        if (zzr(context)) {
            zzb(context, str, "endAdUnitExposure");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, boolean):android.os.Bundle
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void */
    public final void zzd(Context context, String str) {
        if (zzr(context) && (context instanceof Activity) && zza(context, "com.google.firebase.analytics.FirebaseAnalytics", this.zzcvk, false)) {
            Method zzj = zzj(context, "setCurrentScreen");
            try {
                zzj.invoke(this.zzcvk.get(), (Activity) context, str, context.getPackageName());
            } catch (Exception e) {
                zza(e, "setCurrentScreen", false);
            }
        }
    }

    public final void zze(Context context, String str) {
        zza(context, "_ac", str);
    }

    public final void zzf(Context context, String str) {
        zza(context, "_ai", str);
    }

    public final void zzg(Context context, String str) {
        zza(context, "_aq", str);
    }

    public final boolean zzr(Context context) {
        if (!((Boolean) zzbs.zzep().zzd(zzmq.zzbiz)).booleanValue() || this.zzcvh.get()) {
            return false;
        }
        if (this.zzcvi.get() == -1) {
            zzjk.zzhx();
            if (!zzais.zzbd(context)) {
                zzjk.zzhx();
                if (zzais.zzbe(context)) {
                    zzafj.zzco("Google Play Service is out of date, the Google Mobile Ads SDK will not integrate with Firebase. Admob/Firebase integration requires updated Google Play Service.");
                    this.zzcvi.set(0);
                }
            }
            this.zzcvi.set(1);
        }
        return this.zzcvi.get() == 1;
    }

    public final boolean zzs(Context context) {
        return ((Boolean) zzbs.zzep().zzd(zzmq.zzbja)).booleanValue() && zzr(context);
    }

    public final boolean zzt(Context context) {
        return ((Boolean) zzbs.zzep().zzd(zzmq.zzbjb)).booleanValue() && zzr(context);
    }

    public final boolean zzu(Context context) {
        return ((Boolean) zzbs.zzep().zzd(zzmq.zzbjc)).booleanValue() && zzr(context);
    }

    public final boolean zzv(Context context) {
        return ((Boolean) zzbs.zzep().zzd(zzmq.zzbjd)).booleanValue() && zzr(context);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v10, resolved type: java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, boolean):android.os.Bundle
      com.google.android.gms.internal.zzael.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.zzael.zza(java.lang.Exception, java.lang.String, boolean):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String zzw(android.content.Context r5) {
        /*
            r4 = this;
            boolean r0 = r4.zzr(r5)
            if (r0 != 0) goto L_0x0009
            java.lang.String r5 = ""
            return r5
        L_0x0009:
            java.lang.String r0 = "com.google.android.gms.measurement.AppMeasurement"
            java.util.concurrent.atomic.AtomicReference<java.lang.Object> r1 = r4.zzcvj
            r2 = 1
            boolean r0 = r4.zza(r5, r0, r1, r2)
            if (r0 != 0) goto L_0x0017
            java.lang.String r5 = ""
            return r5
        L_0x0017:
            r0 = 0
            java.lang.String r1 = "getCurrentScreenName"
            java.lang.reflect.Method r1 = r4.zzi(r5, r1)     // Catch:{ Exception -> 0x0049 }
            java.util.concurrent.atomic.AtomicReference<java.lang.Object> r2 = r4.zzcvj     // Catch:{ Exception -> 0x0049 }
            java.lang.Object r2 = r2.get()     // Catch:{ Exception -> 0x0049 }
            java.lang.Object[] r3 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0049 }
            java.lang.Object r1 = r1.invoke(r2, r3)     // Catch:{ Exception -> 0x0049 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0049 }
            if (r1 != 0) goto L_0x0043
            java.lang.String r1 = "getCurrentScreenClass"
            java.lang.reflect.Method r5 = r4.zzi(r5, r1)     // Catch:{ Exception -> 0x0049 }
            java.util.concurrent.atomic.AtomicReference<java.lang.Object> r1 = r4.zzcvj     // Catch:{ Exception -> 0x0049 }
            java.lang.Object r1 = r1.get()     // Catch:{ Exception -> 0x0049 }
            java.lang.Object[] r2 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0049 }
            java.lang.Object r5 = r5.invoke(r1, r2)     // Catch:{ Exception -> 0x0049 }
            r1 = r5
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0049 }
        L_0x0043:
            if (r1 == 0) goto L_0x0046
            return r1
        L_0x0046:
            java.lang.String r5 = ""
            return r5
        L_0x0049:
            r5 = move-exception
            java.lang.String r1 = "getCurrentScreenName"
            r4.zza(r5, r1, r0)
            java.lang.String r5 = ""
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzael.zzw(android.content.Context):java.lang.String");
    }

    @Nullable
    public final String zzx(Context context) {
        if (!zzr(context)) {
            return null;
        }
        synchronized (this.zzcvf) {
            if (this.zzcvg != null) {
                String str = this.zzcvg;
                return str;
            }
            this.zzcvg = (String) zza("getGmpAppId", context);
            String str2 = this.zzcvg;
            return str2;
        }
    }

    @Nullable
    public final String zzy(Context context) {
        if (!zzr(context)) {
            return null;
        }
        long longValue = ((Long) zzbs.zzep().zzd(zzmq.zzbjj)).longValue();
        if (longValue < 0) {
            return (String) zza("getAppInstanceId", context);
        }
        if (this.zzcve.get() == null) {
            this.zzcve.compareAndSet(null, new ThreadPoolExecutor(((Integer) zzbs.zzep().zzd(zzmq.zzbjk)).intValue(), ((Integer) zzbs.zzep().zzd(zzmq.zzbjk)).intValue(), 1, TimeUnit.MINUTES, new LinkedBlockingQueue(), new zzaen(this)));
        }
        Future submit = this.zzcve.get().submit(new zzaem(this, context));
        try {
            return (String) submit.get(longValue, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            submit.cancel(true);
            if (e instanceof TimeoutException) {
                return "TIME_OUT";
            }
            return null;
        }
    }

    @Nullable
    public final String zzz(Context context) {
        Object zza;
        if (zzr(context) && (zza = zza("generateEventId", context)) != null) {
            return zza.toString();
        }
        return null;
    }
}
