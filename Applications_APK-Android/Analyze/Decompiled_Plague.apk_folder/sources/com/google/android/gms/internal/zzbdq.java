package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzbdq extends zzed implements zzbdp {
    zzbdq(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.clearcut.internal.IClearcutLoggerService");
    }

    public final void zza(zzbdn zzbdn, zzbde zzbde) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzbdn);
        zzef.zza(zzaz, zzbde);
        zzc(1, zzaz);
    }
}
