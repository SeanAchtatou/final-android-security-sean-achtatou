package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.ArrayList;
import java.util.List;

@zzzb
public final class zzpp extends NativeAppInstallAd {
    private final VideoController zzbeq = new VideoController();
    private final zzpm zzbuj;
    private final List<NativeAd.Image> zzbuk = new ArrayList();
    private final zzpb zzbul;
    private final NativeAd.AdChoicesInfo zzbum;

    /* JADX WARN: Type inference failed for: r2v3, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007a A[Catch:{ RemoteException -> 0x0087 }] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzpp(com.google.android.gms.internal.zzpm r5) {
        /*
            r4 = this;
            r4.<init>()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r4.zzbuk = r0
            com.google.android.gms.ads.VideoController r0 = new com.google.android.gms.ads.VideoController
            r0.<init>()
            r4.zzbeq = r0
            r4.zzbuj = r5
            r5 = 0
            com.google.android.gms.internal.zzpm r0 = r4.zzbuj     // Catch:{ RemoteException -> 0x0055 }
            java.util.List r0 = r0.getImages()     // Catch:{ RemoteException -> 0x0055 }
            if (r0 == 0) goto L_0x005b
            java.util.Iterator r0 = r0.iterator()     // Catch:{ RemoteException -> 0x0055 }
        L_0x0020:
            boolean r1 = r0.hasNext()     // Catch:{ RemoteException -> 0x0055 }
            if (r1 == 0) goto L_0x005b
            java.lang.Object r1 = r0.next()     // Catch:{ RemoteException -> 0x0055 }
            boolean r2 = r1 instanceof android.os.IBinder     // Catch:{ RemoteException -> 0x0055 }
            if (r2 == 0) goto L_0x0047
            android.os.IBinder r1 = (android.os.IBinder) r1     // Catch:{ RemoteException -> 0x0055 }
            if (r1 == 0) goto L_0x0047
            java.lang.String r2 = "com.google.android.gms.ads.internal.formats.client.INativeAdImage"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)     // Catch:{ RemoteException -> 0x0055 }
            boolean r3 = r2 instanceof com.google.android.gms.internal.zzoy     // Catch:{ RemoteException -> 0x0055 }
            if (r3 == 0) goto L_0x0040
            r1 = r2
            com.google.android.gms.internal.zzoy r1 = (com.google.android.gms.internal.zzoy) r1     // Catch:{ RemoteException -> 0x0055 }
            goto L_0x0048
        L_0x0040:
            com.google.android.gms.internal.zzpa r2 = new com.google.android.gms.internal.zzpa     // Catch:{ RemoteException -> 0x0055 }
            r2.<init>(r1)     // Catch:{ RemoteException -> 0x0055 }
            r1 = r2
            goto L_0x0048
        L_0x0047:
            r1 = r5
        L_0x0048:
            if (r1 == 0) goto L_0x0020
            java.util.List<com.google.android.gms.ads.formats.NativeAd$Image> r2 = r4.zzbuk     // Catch:{ RemoteException -> 0x0055 }
            com.google.android.gms.internal.zzpb r3 = new com.google.android.gms.internal.zzpb     // Catch:{ RemoteException -> 0x0055 }
            r3.<init>(r1)     // Catch:{ RemoteException -> 0x0055 }
            r2.add(r3)     // Catch:{ RemoteException -> 0x0055 }
            goto L_0x0020
        L_0x0055:
            r0 = move-exception
            java.lang.String r1 = "Failed to get image."
            com.google.android.gms.internal.zzaiw.zzb(r1, r0)
        L_0x005b:
            com.google.android.gms.internal.zzpm r0 = r4.zzbuj     // Catch:{ RemoteException -> 0x0069 }
            com.google.android.gms.internal.zzoy r0 = r0.zzjm()     // Catch:{ RemoteException -> 0x0069 }
            if (r0 == 0) goto L_0x006f
            com.google.android.gms.internal.zzpb r1 = new com.google.android.gms.internal.zzpb     // Catch:{ RemoteException -> 0x0069 }
            r1.<init>(r0)     // Catch:{ RemoteException -> 0x0069 }
            goto L_0x0070
        L_0x0069:
            r0 = move-exception
            java.lang.String r1 = "Failed to get image."
            com.google.android.gms.internal.zzaiw.zzb(r1, r0)
        L_0x006f:
            r1 = r5
        L_0x0070:
            r4.zzbul = r1
            com.google.android.gms.internal.zzpm r0 = r4.zzbuj     // Catch:{ RemoteException -> 0x0087 }
            com.google.android.gms.internal.zzou r0 = r0.zzjs()     // Catch:{ RemoteException -> 0x0087 }
            if (r0 == 0) goto L_0x008d
            com.google.android.gms.internal.zzox r0 = new com.google.android.gms.internal.zzox     // Catch:{ RemoteException -> 0x0087 }
            com.google.android.gms.internal.zzpm r1 = r4.zzbuj     // Catch:{ RemoteException -> 0x0087 }
            com.google.android.gms.internal.zzou r1 = r1.zzjs()     // Catch:{ RemoteException -> 0x0087 }
            r0.<init>(r1)     // Catch:{ RemoteException -> 0x0087 }
            r5 = r0
            goto L_0x008d
        L_0x0087:
            r0 = move-exception
            java.lang.String r1 = "Failed to get attribution info."
            com.google.android.gms.internal.zzaiw.zzb(r1, r0)
        L_0x008d:
            r4.zzbum = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzpp.<init>(com.google.android.gms.internal.zzpm):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: zzjn */
    public final IObjectWrapper zzbg() {
        try {
            return this.zzbuj.zzjn();
        } catch (RemoteException e) {
            zzaiw.zzb("Failed to retrieve native ad engine.", e);
            return null;
        }
    }

    public final void destroy() {
        try {
            this.zzbuj.destroy();
        } catch (RemoteException e) {
            zzaiw.zzb("Failed to destroy", e);
        }
    }

    public final NativeAd.AdChoicesInfo getAdChoicesInfo() {
        return this.zzbum;
    }

    public final CharSequence getBody() {
        try {
            return this.zzbuj.getBody();
        } catch (RemoteException e) {
            zzaiw.zzb("Failed to get body.", e);
            return null;
        }
    }

    public final CharSequence getCallToAction() {
        try {
            return this.zzbuj.getCallToAction();
        } catch (RemoteException e) {
            zzaiw.zzb("Failed to get call to action.", e);
            return null;
        }
    }

    public final Bundle getExtras() {
        try {
            return this.zzbuj.getExtras();
        } catch (RemoteException e) {
            zzaiw.zzb("Failed to get extras", e);
            return null;
        }
    }

    public final CharSequence getHeadline() {
        try {
            return this.zzbuj.getHeadline();
        } catch (RemoteException e) {
            zzaiw.zzb("Failed to get headline.", e);
            return null;
        }
    }

    public final NativeAd.Image getIcon() {
        return this.zzbul;
    }

    public final List<NativeAd.Image> getImages() {
        return this.zzbuk;
    }

    public final CharSequence getMediationAdapterClassName() {
        try {
            return this.zzbuj.getMediationAdapterClassName();
        } catch (RemoteException e) {
            zzaiw.zzb("Failed to get mediation adapter class name.", e);
            return null;
        }
    }

    public final CharSequence getPrice() {
        try {
            return this.zzbuj.getPrice();
        } catch (RemoteException e) {
            zzaiw.zzb("Failed to get price.", e);
            return null;
        }
    }

    public final Double getStarRating() {
        try {
            double starRating = this.zzbuj.getStarRating();
            if (starRating == -1.0d) {
                return null;
            }
            return Double.valueOf(starRating);
        } catch (RemoteException e) {
            zzaiw.zzb("Failed to get star rating.", e);
            return null;
        }
    }

    public final CharSequence getStore() {
        try {
            return this.zzbuj.getStore();
        } catch (RemoteException e) {
            zzaiw.zzb("Failed to get store", e);
            return null;
        }
    }

    public final VideoController getVideoController() {
        try {
            if (this.zzbuj.getVideoController() != null) {
                this.zzbeq.zza(this.zzbuj.getVideoController());
            }
        } catch (RemoteException e) {
            zzaiw.zzb("Exception occurred while getting video controller", e);
        }
        return this.zzbeq;
    }

    public final void performClick(Bundle bundle) {
        try {
            this.zzbuj.performClick(bundle);
        } catch (RemoteException e) {
            zzaiw.zzb("Failed to perform click.", e);
        }
    }

    public final boolean recordImpression(Bundle bundle) {
        try {
            return this.zzbuj.recordImpression(bundle);
        } catch (RemoteException e) {
            zzaiw.zzb("Failed to record impression.", e);
            return false;
        }
    }

    public final void reportTouchEvent(Bundle bundle) {
        try {
            this.zzbuj.reportTouchEvent(bundle);
        } catch (RemoteException e) {
            zzaiw.zzb("Failed to report touch event.", e);
        }
    }
}
