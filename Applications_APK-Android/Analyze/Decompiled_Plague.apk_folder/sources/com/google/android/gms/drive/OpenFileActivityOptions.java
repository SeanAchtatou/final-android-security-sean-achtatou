package com.google.android.gms.drive;

import android.support.annotation.NonNull;
import com.google.android.gms.drive.query.Filter;
import com.google.android.gms.drive.query.internal.FilterHolder;
import java.util.List;

public final class OpenFileActivityOptions {
    public static final String EXTRA_RESPONSE_DRIVE_ID = "response_drive_id";
    public final String title;
    public final String[] zzgho;
    public final FilterHolder zzghp;
    public final DriveId zzghq;

    public static class Builder {
        private final OpenFileActivityBuilder zzghr = new OpenFileActivityBuilder();

        public OpenFileActivityOptions build() {
            this.zzghr.zzanm();
            return new OpenFileActivityOptions(this.zzghr.getTitle(), this.zzghr.zzaob(), this.zzghr.zzaoc(), this.zzghr.zzaod());
        }

        public Builder setActivityStartFolder(DriveId driveId) {
            this.zzghr.setActivityStartFolder(driveId);
            return this;
        }

        public Builder setActivityTitle(@NonNull String str) {
            this.zzghr.setActivityTitle(str);
            return this;
        }

        public Builder setMimeType(@NonNull List<String> list) {
            this.zzghr.setMimeType((String[]) list.toArray(new String[0]));
            return this;
        }

        public Builder setSelectionFilter(@NonNull Filter filter) {
            this.zzghr.setSelectionFilter(filter);
            return this;
        }
    }

    private OpenFileActivityOptions(String str, String[] strArr, Filter filter, DriveId driveId) {
        this.title = str;
        this.zzgho = strArr;
        this.zzghp = filter == null ? null : new FilterHolder(filter);
        this.zzghq = driveId;
    }
}
