package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzbg;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.internal.zzbem;
import java.util.Arrays;

public final class zza extends zzc implements PlayerStats {
    public static final Parcelable.Creator<zza> CREATOR = new zzb();
    private final float zzhxs;
    private final float zzhxt;
    private final int zzhxu;
    private final int zzhxv;
    private final int zzhxw;
    private final float zzhxx;
    private final float zzhxy;
    private final Bundle zzhxz;
    private final float zzhya;
    private final float zzhyb;
    private final float zzhyc;

    zza(float f, float f2, int i, int i2, int i3, float f3, float f4, Bundle bundle, float f5, float f6, float f7) {
        this.zzhxs = f;
        this.zzhxt = f2;
        this.zzhxu = i;
        this.zzhxv = i2;
        this.zzhxw = i3;
        this.zzhxx = f3;
        this.zzhxy = f4;
        this.zzhxz = bundle;
        this.zzhya = f5;
        this.zzhyb = f6;
        this.zzhyc = f7;
    }

    public zza(PlayerStats playerStats) {
        this.zzhxs = playerStats.getAverageSessionLength();
        this.zzhxt = playerStats.getChurnProbability();
        this.zzhxu = playerStats.getDaysSinceLastPlayed();
        this.zzhxv = playerStats.getNumberOfPurchases();
        this.zzhxw = playerStats.getNumberOfSessions();
        this.zzhxx = playerStats.getSessionPercentile();
        this.zzhxy = playerStats.getSpendPercentile();
        this.zzhya = playerStats.getSpendProbability();
        this.zzhyb = playerStats.getHighSpenderProbability();
        this.zzhyc = playerStats.getTotalSpendNext28Days();
        this.zzhxz = playerStats.zzaub();
    }

    static int zza(PlayerStats playerStats) {
        return Arrays.hashCode(new Object[]{Float.valueOf(playerStats.getAverageSessionLength()), Float.valueOf(playerStats.getChurnProbability()), Integer.valueOf(playerStats.getDaysSinceLastPlayed()), Integer.valueOf(playerStats.getNumberOfPurchases()), Integer.valueOf(playerStats.getNumberOfSessions()), Float.valueOf(playerStats.getSessionPercentile()), Float.valueOf(playerStats.getSpendPercentile()), Float.valueOf(playerStats.getSpendProbability()), Float.valueOf(playerStats.getHighSpenderProbability()), Float.valueOf(playerStats.getTotalSpendNext28Days())});
    }

    static boolean zza(PlayerStats playerStats, Object obj) {
        if (!(obj instanceof PlayerStats)) {
            return false;
        }
        if (playerStats == obj) {
            return true;
        }
        PlayerStats playerStats2 = (PlayerStats) obj;
        return zzbg.equal(Float.valueOf(playerStats2.getAverageSessionLength()), Float.valueOf(playerStats.getAverageSessionLength())) && zzbg.equal(Float.valueOf(playerStats2.getChurnProbability()), Float.valueOf(playerStats.getChurnProbability())) && zzbg.equal(Integer.valueOf(playerStats2.getDaysSinceLastPlayed()), Integer.valueOf(playerStats.getDaysSinceLastPlayed())) && zzbg.equal(Integer.valueOf(playerStats2.getNumberOfPurchases()), Integer.valueOf(playerStats.getNumberOfPurchases())) && zzbg.equal(Integer.valueOf(playerStats2.getNumberOfSessions()), Integer.valueOf(playerStats.getNumberOfSessions())) && zzbg.equal(Float.valueOf(playerStats2.getSessionPercentile()), Float.valueOf(playerStats.getSessionPercentile())) && zzbg.equal(Float.valueOf(playerStats2.getSpendPercentile()), Float.valueOf(playerStats.getSpendPercentile())) && zzbg.equal(Float.valueOf(playerStats2.getSpendProbability()), Float.valueOf(playerStats.getSpendProbability())) && zzbg.equal(Float.valueOf(playerStats2.getHighSpenderProbability()), Float.valueOf(playerStats.getHighSpenderProbability())) && zzbg.equal(Float.valueOf(playerStats2.getTotalSpendNext28Days()), Float.valueOf(playerStats.getTotalSpendNext28Days()));
    }

    static String zzb(PlayerStats playerStats) {
        return zzbg.zzw(playerStats).zzg("AverageSessionLength", Float.valueOf(playerStats.getAverageSessionLength())).zzg("ChurnProbability", Float.valueOf(playerStats.getChurnProbability())).zzg("DaysSinceLastPlayed", Integer.valueOf(playerStats.getDaysSinceLastPlayed())).zzg("NumberOfPurchases", Integer.valueOf(playerStats.getNumberOfPurchases())).zzg("NumberOfSessions", Integer.valueOf(playerStats.getNumberOfSessions())).zzg("SessionPercentile", Float.valueOf(playerStats.getSessionPercentile())).zzg("SpendPercentile", Float.valueOf(playerStats.getSpendPercentile())).zzg("SpendProbability", Float.valueOf(playerStats.getSpendProbability())).zzg("HighSpenderProbability", Float.valueOf(playerStats.getHighSpenderProbability())).zzg("TotalSpendNext28Days", Float.valueOf(playerStats.getTotalSpendNext28Days())).toString();
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    public final /* bridge */ /* synthetic */ Object freeze() {
        if (this != null) {
            return this;
        }
        throw null;
    }

    public final float getAverageSessionLength() {
        return this.zzhxs;
    }

    public final float getChurnProbability() {
        return this.zzhxt;
    }

    public final int getDaysSinceLastPlayed() {
        return this.zzhxu;
    }

    public final float getHighSpenderProbability() {
        return this.zzhyb;
    }

    public final int getNumberOfPurchases() {
        return this.zzhxv;
    }

    public final int getNumberOfSessions() {
        return this.zzhxw;
    }

    public final float getSessionPercentile() {
        return this.zzhxx;
    }

    public final float getSpendPercentile() {
        return this.zzhxy;
    }

    public final float getSpendProbability() {
        return this.zzhya;
    }

    public final float getTotalSpendNext28Days() {
        return this.zzhyc;
    }

    public final int hashCode() {
        return zza(this);
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String toString() {
        return zzb(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Bundle, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 1, getAverageSessionLength());
        zzbem.zza(parcel, 2, getChurnProbability());
        zzbem.zzc(parcel, 3, getDaysSinceLastPlayed());
        zzbem.zzc(parcel, 4, getNumberOfPurchases());
        zzbem.zzc(parcel, 5, getNumberOfSessions());
        zzbem.zza(parcel, 6, getSessionPercentile());
        zzbem.zza(parcel, 7, getSpendPercentile());
        zzbem.zza(parcel, 8, this.zzhxz, false);
        zzbem.zza(parcel, 9, getSpendProbability());
        zzbem.zza(parcel, 10, getHighSpenderProbability());
        zzbem.zza(parcel, 11, getTotalSpendNext28Days());
        zzbem.zzai(parcel, zze);
    }

    public final Bundle zzaub() {
        return this.zzhxz;
    }
}
