package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;

final class zzab implements zzcg {
    private /* synthetic */ zzy zzfna;

    private zzab(zzy zzy) {
        this.zzfna = zzy;
    }

    /* synthetic */ zzab(zzy zzy, zzz zzz) {
        this(zzy);
    }

    public final void zzc(@NonNull ConnectionResult connectionResult) {
        this.zzfna.zzfmy.lock();
        try {
            ConnectionResult unused = this.zzfna.zzfmw = connectionResult;
            this.zzfna.zzagz();
        } finally {
            this.zzfna.zzfmy.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.zzy.zza(com.google.android.gms.common.api.internal.zzy, boolean):boolean
     arg types: [com.google.android.gms.common.api.internal.zzy, int]
     candidates:
      com.google.android.gms.common.api.internal.zzy.zza(com.google.android.gms.common.api.internal.zzy, com.google.android.gms.common.ConnectionResult):com.google.android.gms.common.ConnectionResult
      com.google.android.gms.common.api.internal.zzy.zza(com.google.android.gms.common.api.internal.zzy, android.os.Bundle):void
      com.google.android.gms.common.api.internal.zzy.zza(com.google.android.gms.common.api.internal.zzy, boolean):boolean */
    public final void zzf(int i, boolean z) {
        this.zzfna.zzfmy.lock();
        try {
            if (this.zzfna.zzfmx) {
                boolean unused = this.zzfna.zzfmx = false;
                this.zzfna.zze(i, z);
            } else {
                boolean unused2 = this.zzfna.zzfmx = true;
                this.zzfna.zzfmp.onConnectionSuspended(i);
            }
        } finally {
            this.zzfna.zzfmy.unlock();
        }
    }

    public final void zzj(@Nullable Bundle bundle) {
        this.zzfna.zzfmy.lock();
        try {
            ConnectionResult unused = this.zzfna.zzfmw = ConnectionResult.zzfhy;
            this.zzfna.zzagz();
        } finally {
            this.zzfna.zzfmy.unlock();
        }
    }
}
