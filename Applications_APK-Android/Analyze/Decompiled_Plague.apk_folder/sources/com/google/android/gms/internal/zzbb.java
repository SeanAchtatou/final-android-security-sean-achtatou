package com.google.android.gms.internal;

import java.io.IOException;

public final class zzbb extends zzfhe<zzbb> {
    public Long zzfz = null;
    private String zzgh = null;
    private byte[] zzgi = null;

    public zzbb() {
        this.zzpai = -1;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 8) {
                this.zzfz = Long.valueOf(zzfhb.zzcum());
            } else if (zzcts == 26) {
                this.zzgh = zzfhb.readString();
            } else if (zzcts == 34) {
                this.zzgi = zzfhb.readBytes();
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.zzfz != null) {
            zzfhc.zzf(1, this.zzfz.longValue());
        }
        if (this.zzgh != null) {
            zzfhc.zzn(3, this.zzgh);
        }
        if (this.zzgi != null) {
            zzfhc.zzc(4, this.zzgi);
        }
        super.zza(zzfhc);
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.zzfz != null) {
            zzo += zzfhc.zzc(1, this.zzfz.longValue());
        }
        if (this.zzgh != null) {
            zzo += zzfhc.zzo(3, this.zzgh);
        }
        return this.zzgi != null ? zzo + zzfhc.zzd(4, this.zzgi) : zzo;
    }
}
