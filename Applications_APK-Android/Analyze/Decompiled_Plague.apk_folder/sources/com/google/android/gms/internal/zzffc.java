package com.google.android.gms.internal;

public class zzffc {
    private static final zzfea zzpaj = zzfea.zzcuz();
    private zzfdh zzpcw;
    private volatile zzffi zzpcx;
    private volatile zzfdh zzpcy;

    private zzfdh toByteString() {
        if (this.zzpcy != null) {
            return this.zzpcy;
        }
        synchronized (this) {
            if (this.zzpcy != null) {
                zzfdh zzfdh = this.zzpcy;
                return zzfdh;
            }
            this.zzpcy = this.zzpcx == null ? zzfdh.zzpal : this.zzpcx.toByteString();
            zzfdh zzfdh2 = this.zzpcy;
            return zzfdh2;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:7|8|9|10) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.android.gms.internal.zzffi zzi(com.google.android.gms.internal.zzffi r2) {
        /*
            r1 = this;
            com.google.android.gms.internal.zzffi r0 = r1.zzpcx
            if (r0 != 0) goto L_0x001c
            monitor-enter(r1)
            com.google.android.gms.internal.zzffi r0 = r1.zzpcx     // Catch:{ all -> 0x0019 }
            if (r0 == 0) goto L_0x000b
        L_0x0009:
            monitor-exit(r1)     // Catch:{ all -> 0x0019 }
            goto L_0x001c
        L_0x000b:
            r1.zzpcx = r2     // Catch:{ zzfew -> 0x0012 }
            com.google.android.gms.internal.zzfdh r0 = com.google.android.gms.internal.zzfdh.zzpal     // Catch:{ zzfew -> 0x0012 }
            r1.zzpcy = r0     // Catch:{ zzfew -> 0x0012 }
            goto L_0x0009
        L_0x0012:
            r1.zzpcx = r2     // Catch:{ all -> 0x0019 }
            com.google.android.gms.internal.zzfdh r2 = com.google.android.gms.internal.zzfdh.zzpal     // Catch:{ all -> 0x0019 }
            r1.zzpcy = r2     // Catch:{ all -> 0x0019 }
            goto L_0x0009
        L_0x0019:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0019 }
            throw r2
        L_0x001c:
            com.google.android.gms.internal.zzffi r2 = r1.zzpcx
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzffc.zzi(com.google.android.gms.internal.zzffi):com.google.android.gms.internal.zzffi");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzffc)) {
            return false;
        }
        zzffc zzffc = (zzffc) obj;
        zzffi zzffi = this.zzpcx;
        zzffi zzffi2 = zzffc.zzpcx;
        return (zzffi == null && zzffi2 == null) ? toByteString().equals(zzffc.toByteString()) : (zzffi == null || zzffi2 == null) ? zzffi != null ? zzffi.equals(zzffc.zzi(zzffi.zzcvh())) : zzi(zzffi2.zzcvh()).equals(zzffi2) : zzffi.equals(zzffi2);
    }

    public int hashCode() {
        return 1;
    }

    public final int zzhl() {
        if (this.zzpcy != null) {
            return this.zzpcy.size();
        }
        if (this.zzpcx != null) {
            return this.zzpcx.zzhl();
        }
        return 0;
    }

    public final zzffi zzj(zzffi zzffi) {
        zzffi zzffi2 = this.zzpcx;
        this.zzpcw = null;
        this.zzpcy = null;
        this.zzpcx = zzffi;
        return zzffi2;
    }
}
