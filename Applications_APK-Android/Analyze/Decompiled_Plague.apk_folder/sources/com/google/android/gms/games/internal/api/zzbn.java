package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.GamesClientImpl;

final class zzbn extends zzbs {
    private /* synthetic */ boolean zzhpv;
    private /* synthetic */ String[] zzhqj;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbn(zzbj zzbj, GoogleApiClient googleApiClient, boolean z, String[] strArr) {
        super(googleApiClient, null);
        this.zzhpv = z;
        this.zzhqj = strArr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((GamesClientImpl) zzb).zzb(this, this.zzhpv, this.zzhqj);
    }
}
