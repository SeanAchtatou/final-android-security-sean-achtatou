package com.google.android.gms.internal;

import com.google.android.gms.internal.zzffi;

public abstract class zzfdc<MessageType extends zzffi> implements zzffm<MessageType> {
    private static final zzfea zzpaj = zzfea.zzcuz();

    public final /* synthetic */ Object zzc(zzfdq zzfdq, zzfea zzfea) throws zzfew {
        zzffi zzffi = (zzffi) zze(zzfdq, zzfea);
        if (zzffi == null || zzffi.isInitialized()) {
            return zzffi;
        }
        throw (zzffi instanceof zzfcz ? new zzfgh((zzfcz) zzffi) : zzffi instanceof zzfdb ? new zzfgh((zzfdb) zzffi) : new zzfgh(zzffi)).zzcwt().zzh(zzffi);
    }
}
