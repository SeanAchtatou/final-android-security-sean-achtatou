package com.google.android.gms.common.api.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

final class zzbg implements GoogleApiClient.OnConnectionFailedListener {
    private /* synthetic */ zzdc zzfpr;

    zzbg(zzbd zzbd, zzdc zzdc) {
        this.zzfpr = zzdc;
    }

    public final void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        this.zzfpr.setResult(new Status(8));
    }
}
