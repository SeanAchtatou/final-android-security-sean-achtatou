package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.internal.zzan;

public final class zzcwk extends zzed implements zzcwj {
    zzcwk(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.signin.internal.ISignInService");
    }

    public final void zza(zzan zzan, int i, boolean z) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzan);
        zzaz.writeInt(i);
        zzef.zza(zzaz, z);
        zzb(9, zzaz);
    }

    public final void zza(zzcwm zzcwm, zzcwh zzcwh) throws RemoteException {
        Parcel zzaz = zzaz();
        zzef.zza(zzaz, zzcwm);
        zzef.zza(zzaz, zzcwh);
        zzb(12, zzaz);
    }

    public final void zzeh(int i) throws RemoteException {
        Parcel zzaz = zzaz();
        zzaz.writeInt(i);
        zzb(7, zzaz);
    }
}
