package com.google.android.gms.internal;

import java.io.IOException;

public final class zzfij extends zzfhe<zzfij> implements Cloneable {
    private static volatile zzfij[] zzpkz;
    private String key = "";
    private String value = "";

    public zzfij() {
        this.zzpgy = null;
        this.zzpai = -1;
    }

    public static zzfij[] zzcxz() {
        if (zzpkz == null) {
            synchronized (zzfhi.zzphg) {
                if (zzpkz == null) {
                    zzpkz = new zzfij[0];
                }
            }
        }
        return zzpkz;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzcya */
    public zzfij clone() {
        try {
            return (zzfij) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzfij)) {
            return false;
        }
        zzfij zzfij = (zzfij) obj;
        if (this.key == null) {
            if (zzfij.key != null) {
                return false;
            }
        } else if (!this.key.equals(zzfij.key)) {
            return false;
        }
        if (this.value == null) {
            if (zzfij.value != null) {
                return false;
            }
        } else if (!this.value.equals(zzfij.value)) {
            return false;
        }
        return (this.zzpgy == null || this.zzpgy.isEmpty()) ? zzfij.zzpgy == null || zzfij.zzpgy.isEmpty() : this.zzpgy.equals(zzfij.zzpgy);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((527 + getClass().getName().hashCode()) * 31) + (this.key == null ? 0 : this.key.hashCode())) * 31) + (this.value == null ? 0 : this.value.hashCode())) * 31;
        if (this.zzpgy != null && !this.zzpgy.isEmpty()) {
            i = this.zzpgy.hashCode();
        }
        return hashCode + i;
    }

    public final /* synthetic */ zzfhk zza(zzfhb zzfhb) throws IOException {
        while (true) {
            int zzcts = zzfhb.zzcts();
            if (zzcts == 0) {
                return this;
            }
            if (zzcts == 10) {
                this.key = zzfhb.readString();
            } else if (zzcts == 18) {
                this.value = zzfhb.readString();
            } else if (!super.zza(zzfhb, zzcts)) {
                return this;
            }
        }
    }

    public final void zza(zzfhc zzfhc) throws IOException {
        if (this.key != null && !this.key.equals("")) {
            zzfhc.zzn(1, this.key);
        }
        if (this.value != null && !this.value.equals("")) {
            zzfhc.zzn(2, this.value);
        }
        super.zza(zzfhc);
    }

    public final /* synthetic */ zzfhe zzcxe() throws CloneNotSupportedException {
        return (zzfij) clone();
    }

    public final /* synthetic */ zzfhk zzcxf() throws CloneNotSupportedException {
        return (zzfij) clone();
    }

    /* access modifiers changed from: protected */
    public final int zzo() {
        int zzo = super.zzo();
        if (this.key != null && !this.key.equals("")) {
            zzo += zzfhc.zzo(1, this.key);
        }
        return (this.value == null || this.value.equals("")) ? zzo : zzo + zzfhc.zzo(2, this.value);
    }
}
