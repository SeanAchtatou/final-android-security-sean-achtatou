package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.zzo;

final class zzbmg extends zzbml {
    private /* synthetic */ MetadataChangeSet zzglq;
    private /* synthetic */ int zzglr;
    private /* synthetic */ int zzgls;
    private /* synthetic */ zzo zzglt;
    private /* synthetic */ zzbmf zzglu;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbmg(zzbmf zzbmf, GoogleApiClient googleApiClient, MetadataChangeSet metadataChangeSet, int i, int i2, zzo zzo) {
        super(googleApiClient);
        this.zzglu = zzbmf;
        this.zzglq = metadataChangeSet;
        this.zzglr = i;
        this.zzgls = i2;
        this.zzglt = zzo;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        zzbll zzbll = (zzbll) zzb;
        this.zzglq.zzanz().setContext(zzbll.getContext());
        ((zzbpf) zzbll.zzakb()).zza(new zzbkg(this.zzglu.getDriveId(), this.zzglq.zzanz(), this.zzglr, this.zzgls, this.zzglt), new zzbmi(this));
    }
}
