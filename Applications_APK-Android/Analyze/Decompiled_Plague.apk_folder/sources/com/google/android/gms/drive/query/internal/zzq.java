package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.internal.zzbek;

public final class zzq implements Parcelable.Creator<zzp> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int zzd = zzbek.zzd(parcel);
        MetadataBundle metadataBundle = null;
        while (parcel.dataPosition() < zzd) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 1) {
                zzbek.zzb(parcel, readInt);
            } else {
                metadataBundle = (MetadataBundle) zzbek.zza(parcel, readInt, MetadataBundle.CREATOR);
            }
        }
        zzbek.zzaf(parcel, zzd);
        return new zzp(metadataBundle);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzp[i];
    }
}
