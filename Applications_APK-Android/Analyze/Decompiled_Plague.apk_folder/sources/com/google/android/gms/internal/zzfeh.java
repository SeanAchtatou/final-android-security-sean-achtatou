package com.google.android.gms.internal;

final class zzfeh implements zzfen {
    static final zzfeh zzpbx = new zzfeh();
    private static zzfei zzpby = new zzfei();

    private zzfeh() {
    }

    public final double zza(boolean z, double d, boolean z2, double d2) {
        if (z == z2 && d == d2) {
            return d;
        }
        throw zzpby;
    }

    public final int zza(boolean z, int i, boolean z2, int i2) {
        if (z == z2 && i == i2) {
            return i;
        }
        throw zzpby;
    }

    public final long zza(boolean z, long j, boolean z2, long j2) {
        if (z == z2 && j == j2) {
            return j;
        }
        throw zzpby;
    }

    public final zzfdh zza(boolean z, zzfdh zzfdh, boolean z2, zzfdh zzfdh2) {
        if (z == z2 && zzfdh.equals(zzfdh2)) {
            return zzfdh;
        }
        throw zzpby;
    }

    public final zzfeu zza(zzfeu zzfeu, zzfeu zzfeu2) {
        if (zzfeu.equals(zzfeu2)) {
            return zzfeu;
        }
        throw zzpby;
    }

    public final <T> zzfev<T> zza(zzfev zzfev, zzfev zzfev2) {
        if (zzfev.equals(zzfev2)) {
            return zzfev;
        }
        throw zzpby;
    }

    public final <K, V> zzffh<K, V> zza(zzffh zzffh, zzffh zzffh2) {
        if (zzffh.equals(zzffh2)) {
            return zzffh;
        }
        throw zzpby;
    }

    public final <T extends zzffi> T zza(zzffi zzffi, zzffi zzffi2) {
        if (zzffi == null && zzffi2 == null) {
            return null;
        }
        if (zzffi == null || zzffi2 == null) {
            throw zzpby;
        }
        zzfee zzfee = (zzfee) zzffi;
        if (zzfee != zzffi2 && ((zzfee) zzfee.zza(zzfem.zzpci, (Object) null, (Object) null)).getClass().isInstance(zzffi2)) {
            zzfee zzfee2 = (zzfee) zzffi2;
            zzfee.zza(zzfem.zzpcd, this, zzfee2);
            zzfee.zzpbs = zza(zzfee.zzpbs, zzfee2.zzpbs);
        }
        return zzffi;
    }

    public final zzfgi zza(zzfgi zzfgi, zzfgi zzfgi2) {
        if (zzfgi.equals(zzfgi2)) {
            return zzfgi;
        }
        throw zzpby;
    }

    public final Object zza(boolean z, Object obj, Object obj2) {
        if (z && obj.equals(obj2)) {
            return obj;
        }
        throw zzpby;
    }

    public final String zza(boolean z, String str, boolean z2, String str2) {
        if (z == z2 && str.equals(str2)) {
            return str;
        }
        throw zzpby;
    }

    public final boolean zza(boolean z, boolean z2, boolean z3, boolean z4) {
        if (z == z3 && z2 == z4) {
            return z2;
        }
        throw zzpby;
    }

    public final Object zzb(boolean z, Object obj, Object obj2) {
        if (z && obj.equals(obj2)) {
            return obj;
        }
        throw zzpby;
    }

    public final Object zzc(boolean z, Object obj, Object obj2) {
        if (z && obj.equals(obj2)) {
            return obj;
        }
        throw zzpby;
    }

    public final Object zzd(boolean z, Object obj, Object obj2) {
        if (z && obj.equals(obj2)) {
            return obj;
        }
        throw zzpby;
    }

    public final void zzdb(boolean z) {
        if (z) {
            throw zzpby;
        }
    }

    public final Object zze(boolean z, Object obj, Object obj2) {
        if (z && obj.equals(obj2)) {
            return obj;
        }
        throw zzpby;
    }

    public final Object zzf(boolean z, Object obj, Object obj2) {
        if (z && obj.equals(obj2)) {
            return obj;
        }
        throw zzpby;
    }

    public final Object zzg(boolean z, Object obj, Object obj2) {
        if (z) {
            zzfee zzfee = (zzfee) obj;
            zzffi zzffi = (zzffi) obj2;
            boolean z2 = true;
            if (zzfee != zzffi) {
                if (!((zzfee) zzfee.zza(zzfem.zzpci, (Object) null, (Object) null)).getClass().isInstance(zzffi)) {
                    z2 = false;
                } else {
                    zzfee zzfee2 = (zzfee) zzffi;
                    zzfee.zza(zzfem.zzpcd, this, zzfee2);
                    zzfee.zzpbs = zza(zzfee.zzpbs, zzfee2.zzpbs);
                }
            }
            if (z2) {
                return obj;
            }
        }
        throw zzpby;
    }
}
