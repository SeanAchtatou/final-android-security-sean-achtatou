package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzdf;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbnf extends zzdf<zzbll, Void> {
    private /* synthetic */ DriveResource zzgmc;

    zzbnf(zzbmu zzbmu, DriveResource driveResource) {
        this.zzgmc = driveResource;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        zzbll zzbll = (zzbll) zzb;
        if (!zzbll.zzgkn) {
            throw new IllegalStateException("Application must define an exported DriveEventService subclass in AndroidManifest.xml to add event subscriptions");
        }
        ((zzbpf) zzbll.zzakb()).zza(new zzbjt(1, this.zzgmc.getDriveId()), (zzbpj) null, (String) null, new zzbsa(taskCompletionSource));
    }
}
