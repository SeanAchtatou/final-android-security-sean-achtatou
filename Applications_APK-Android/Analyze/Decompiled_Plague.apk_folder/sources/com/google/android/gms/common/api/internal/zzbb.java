package com.google.android.gms.common.api.internal;

import android.support.annotation.WorkerThread;

abstract class zzbb implements Runnable {
    private /* synthetic */ zzar zzfor;

    private zzbb(zzar zzar) {
        this.zzfor = zzar;
    }

    /* synthetic */ zzbb(zzar zzar, zzas zzas) {
        this(zzar);
    }

    @WorkerThread
    public void run() {
        this.zzfor.zzfmy.lock();
        try {
            if (!Thread.interrupted()) {
                zzahp();
            }
        } catch (RuntimeException e) {
            this.zzfor.zzfob.zza(e);
        } catch (Throwable th) {
            this.zzfor.zzfmy.unlock();
            throw th;
        }
        this.zzfor.zzfmy.unlock();
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public abstract void zzahp();
}
