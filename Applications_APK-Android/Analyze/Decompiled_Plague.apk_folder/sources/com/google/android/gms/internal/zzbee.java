package com.google.android.gms.internal;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

final class zzbee extends Drawable {
    /* access modifiers changed from: private */
    public static final zzbee zzfvg = new zzbee();
    private static final zzbef zzfvh = new zzbef();

    private zzbee() {
    }

    public final void draw(Canvas canvas) {
    }

    public final Drawable.ConstantState getConstantState() {
        return zzfvh;
    }

    public final int getOpacity() {
        return -2;
    }

    public final void setAlpha(int i) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
