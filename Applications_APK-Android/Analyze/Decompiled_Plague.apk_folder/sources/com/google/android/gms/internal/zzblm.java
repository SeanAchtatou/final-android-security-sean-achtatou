package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzblm extends zzblk {
    private /* synthetic */ zzbjt zzgkw;
    private /* synthetic */ zzbov zzgkx;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzblm(zzbll zzbll, GoogleApiClient googleApiClient, zzbjt zzbjt, zzbov zzbov) {
        super(googleApiClient);
        this.zzgkw = zzbjt;
        this.zzgkx = zzbov;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb) throws RemoteException {
        ((zzbpf) ((zzbll) zzb).zzakb()).zza(this.zzgkw, this.zzgkx, (String) null, new zzbrj(this));
    }
}
