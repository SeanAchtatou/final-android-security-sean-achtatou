package com.google.android.gms.internal;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.interfaces.ECPublicKey;

public final class zzdsn implements zzdor {
    private static final byte[] zzlux = new byte[0];
    private final zzdso zzluy;
    private final String zzluz;
    private final byte[] zzlva;
    private final zzdsl zzlvb;
    private final zzdsm zzlvc;

    public zzdsn(ECPublicKey eCPublicKey, byte[] bArr, String str, zzdsl zzdsl, zzdsm zzdsm) throws GeneralSecurityException {
        zzdsi.zza(eCPublicKey.getW(), eCPublicKey.getParams().getCurve());
        this.zzluy = new zzdso(eCPublicKey);
        this.zzlva = bArr;
        this.zzluz = str;
        this.zzlvb = zzdsl;
        this.zzlvc = zzdsm;
    }

    public final byte[] zzd(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        zzdsp zza = this.zzluy.zza(this.zzluz, this.zzlva, bArr2, this.zzlvc.zzblk(), this.zzlvb);
        byte[] zzd = this.zzlvc.zzad(zza.zzbou()).zzd(bArr, zzlux);
        byte[] zzbot = zza.zzbot();
        return ByteBuffer.allocate(zzbot.length + zzd.length).put(zzbot).put(zzd).array();
    }
}
