package com.google.android.gms.internal;

import com.google.android.gms.internal.zzfee;
import com.google.android.gms.internal.zzfef;
import java.io.IOException;

public class zzfef<MessageType extends zzfee<MessageType, BuilderType>, BuilderType extends zzfef<MessageType, BuilderType>> extends zzfda<MessageType, BuilderType> {
    private final MessageType zzpbu;
    protected MessageType zzpbv;
    private boolean zzpbw = false;

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected zzfef(MessageType r3) {
        /*
            r2 = this;
            r2.<init>()
            r2.zzpbu = r3
            int r0 = com.google.android.gms.internal.zzfem.zzpcg
            r1 = 0
            java.lang.Object r3 = r3.zza(r0, r1, r1)
            com.google.android.gms.internal.zzfee r3 = (com.google.android.gms.internal.zzfee) r3
            r2.zzpbv = r3
            r3 = 0
            r2.zzpbw = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfef.<init>(com.google.android.gms.internal.zzfee):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private static void zza(MessageType r2, MessageType r3) {
        /*
            com.google.android.gms.internal.zzfel r0 = com.google.android.gms.internal.zzfel.zzpcb
            int r1 = com.google.android.gms.internal.zzfem.zzpcd
            r2.zza(r1, r0, r3)
            com.google.android.gms.internal.zzfgi r1 = r2.zzpbs
            com.google.android.gms.internal.zzfgi r3 = r3.zzpbs
            com.google.android.gms.internal.zzfgi r3 = r0.zza(r1, r3)
            r2.zzpbs = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfef.zza(com.google.android.gms.internal.zzfee, com.google.android.gms.internal.zzfee):void");
    }

    /* access modifiers changed from: private */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: zzd */
    public final BuilderType zzb(com.google.android.gms.internal.zzfdq r3, com.google.android.gms.internal.zzfea r4) throws java.io.IOException {
        /*
            r2 = this;
            r2.zzcvi()
            MessageType r0 = r2.zzpbv     // Catch:{ RuntimeException -> 0x000b }
            int r1 = com.google.android.gms.internal.zzfem.zzpce     // Catch:{ RuntimeException -> 0x000b }
            r0.zza(r1, r3, r4)     // Catch:{ RuntimeException -> 0x000b }
            return r2
        L_0x000b:
            r3 = move-exception
            java.lang.Throwable r4 = r3.getCause()
            boolean r4 = r4 instanceof java.io.IOException
            if (r4 == 0) goto L_0x001b
            java.lang.Throwable r3 = r3.getCause()
            java.io.IOException r3 = (java.io.IOException) r3
            throw r3
        L_0x001b:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfef.zzb(com.google.android.gms.internal.zzfdq, com.google.android.gms.internal.zzfea):com.google.android.gms.internal.zzfef");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public /* synthetic */ java.lang.Object clone() throws java.lang.CloneNotSupportedException {
        /*
            r4 = this;
            MessageType r0 = r4.zzpbu
            com.google.android.gms.internal.zzfee r0 = (com.google.android.gms.internal.zzfee) r0
            int r1 = com.google.android.gms.internal.zzfem.zzpch
            r2 = 0
            java.lang.Object r0 = r0.zza(r1, r2, r2)
            com.google.android.gms.internal.zzfef r0 = (com.google.android.gms.internal.zzfef) r0
            boolean r1 = r4.zzpbw
            if (r1 == 0) goto L_0x0014
        L_0x0011:
            MessageType r1 = r4.zzpbv
            goto L_0x0024
        L_0x0014:
            MessageType r1 = r4.zzpbv
            int r3 = com.google.android.gms.internal.zzfem.zzpcf
            r1.zza(r3, r2, r2)
            com.google.android.gms.internal.zzfgi r1 = r1.zzpbs
            r1.zzbim()
            r1 = 1
            r4.zzpbw = r1
            goto L_0x0011
        L_0x0024:
            com.google.android.gms.internal.zzfee r1 = (com.google.android.gms.internal.zzfee) r1
            r0.zza(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfef.clone():java.lang.Object");
    }

    public final boolean isInitialized() {
        return this.zzpbv.zza(zzfem.zzpcc, false, null) != null;
    }

    public final /* synthetic */ zzfda zza(zzfdq zzfdq, zzfea zzfea) throws IOException {
        return (zzfef) zzb(zzfdq, zzfea);
    }

    public final BuilderType zza(zzfee zzfee) {
        zzcvi();
        zza(this.zzpbv, zzfee);
        return this;
    }

    public final /* synthetic */ zzfda zzctg() {
        return (zzfef) clone();
    }

    public final /* synthetic */ zzffi zzcvh() {
        return this.zzpbu;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected final void zzcvi() {
        /*
            r3 = this;
            boolean r0 = r3.zzpbw
            if (r0 == 0) goto L_0x0019
            MessageType r0 = r3.zzpbv
            int r1 = com.google.android.gms.internal.zzfem.zzpcg
            r2 = 0
            java.lang.Object r0 = r0.zza(r1, r2, r2)
            com.google.android.gms.internal.zzfee r0 = (com.google.android.gms.internal.zzfee) r0
            MessageType r1 = r3.zzpbv
            zza(r0, r1)
            r3.zzpbv = r0
            r0 = 0
            r3.zzpbw = r0
        L_0x0019:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfef.zzcvi():void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final MessageType zzcvj() {
        /*
            r3 = this;
            boolean r0 = r3.zzpbw
            if (r0 == 0) goto L_0x0007
            MessageType r0 = r3.zzpbv
            return r0
        L_0x0007:
            MessageType r0 = r3.zzpbv
            int r1 = com.google.android.gms.internal.zzfem.zzpcf
            r2 = 0
            r0.zza(r1, r2, r2)
            com.google.android.gms.internal.zzfgi r0 = r0.zzpbs
            r0.zzbim()
            r0 = 1
            r3.zzpbw = r0
            MessageType r0 = r3.zzpbv
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfef.zzcvj():com.google.android.gms.internal.zzfee");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final MessageType zzcvk() {
        /*
            r5 = this;
            boolean r0 = r5.zzpbw
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0009
        L_0x0006:
            MessageType r0 = r5.zzpbv
            goto L_0x0018
        L_0x0009:
            MessageType r0 = r5.zzpbv
            int r3 = com.google.android.gms.internal.zzfem.zzpcf
            r0.zza(r3, r2, r2)
            com.google.android.gms.internal.zzfgi r0 = r0.zzpbs
            r0.zzbim()
            r5.zzpbw = r1
            goto L_0x0006
        L_0x0018:
            com.google.android.gms.internal.zzfee r0 = (com.google.android.gms.internal.zzfee) r0
            int r3 = com.google.android.gms.internal.zzfem.zzpcc
            java.lang.Boolean r4 = java.lang.Boolean.TRUE
            java.lang.Object r2 = r0.zza(r3, r4, r2)
            if (r2 == 0) goto L_0x0025
            goto L_0x0026
        L_0x0025:
            r1 = 0
        L_0x0026:
            if (r1 != 0) goto L_0x002e
            com.google.android.gms.internal.zzfgh r1 = new com.google.android.gms.internal.zzfgh
            r1.<init>(r0)
            throw r1
        L_0x002e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfef.zzcvk():com.google.android.gms.internal.zzfee");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final /* synthetic */ com.google.android.gms.internal.zzffi zzcvl() {
        /*
            r3 = this;
            boolean r0 = r3.zzpbw
            if (r0 == 0) goto L_0x0007
            MessageType r0 = r3.zzpbv
            return r0
        L_0x0007:
            MessageType r0 = r3.zzpbv
            int r1 = com.google.android.gms.internal.zzfem.zzpcf
            r2 = 0
            r0.zza(r1, r2, r2)
            com.google.android.gms.internal.zzfgi r0 = r0.zzpbs
            r0.zzbim()
            r0 = 1
            r3.zzpbw = r0
            MessageType r0 = r3.zzpbv
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfef.zzcvl():com.google.android.gms.internal.zzffi");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final /* synthetic */ com.google.android.gms.internal.zzffi zzcvm() {
        /*
            r5 = this;
            boolean r0 = r5.zzpbw
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0009
        L_0x0006:
            MessageType r0 = r5.zzpbv
            goto L_0x0018
        L_0x0009:
            MessageType r0 = r5.zzpbv
            int r3 = com.google.android.gms.internal.zzfem.zzpcf
            r0.zza(r3, r2, r2)
            com.google.android.gms.internal.zzfgi r0 = r0.zzpbs
            r0.zzbim()
            r5.zzpbw = r1
            goto L_0x0006
        L_0x0018:
            com.google.android.gms.internal.zzfee r0 = (com.google.android.gms.internal.zzfee) r0
            int r3 = com.google.android.gms.internal.zzfem.zzpcc
            java.lang.Boolean r4 = java.lang.Boolean.TRUE
            java.lang.Object r2 = r0.zza(r3, r4, r2)
            if (r2 == 0) goto L_0x0025
            goto L_0x0026
        L_0x0025:
            r1 = 0
        L_0x0026:
            if (r1 != 0) goto L_0x002e
            com.google.android.gms.internal.zzfgh r1 = new com.google.android.gms.internal.zzfgh
            r1.<init>(r0)
            throw r1
        L_0x002e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfef.zzcvm():com.google.android.gms.internal.zzffi");
    }
}
