package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.zzbq;
import com.google.android.gms.common.util.zzd;
import java.util.ArrayList;

public final class zzbcv {
    @Deprecated
    public static final Api<Api.ApiOptions.NoOptions> API = new Api<>("ClearcutLogger.API", zzdyi, zzdyh);
    private static Api.zzf<zzbdl> zzdyh = new Api.zzf<>();
    private static Api.zza<zzbdl, Api.ApiOptions.NoOptions> zzdyi = new zzbcw();
    private static final zzcsv[] zzfgf = new zzcsv[0];
    private static final String[] zzfgg = new String[0];
    private static final byte[][] zzfgh = new byte[0][];
    /* access modifiers changed from: private */
    public final String packageName;
    /* access modifiers changed from: private */
    public final int zzfgi;
    /* access modifiers changed from: private */
    public String zzfgj;
    /* access modifiers changed from: private */
    public int zzfgk = -1;
    private String zzfgl;
    private String zzfgm;
    /* access modifiers changed from: private */
    public final boolean zzfgn;
    private int zzfgo = 0;
    /* access modifiers changed from: private */
    public final zzbdb zzfgp;
    /* access modifiers changed from: private */
    public final zzd zzfgq;
    /* access modifiers changed from: private */
    public zzbda zzfgr;
    /* access modifiers changed from: private */
    public final zzbcy zzfgs;

    public zzbcv(Context context, int i, String str, String str2, String str3, boolean z, zzbdb zzbdb, zzd zzd, zzbda zzbda, zzbcy zzbcy) {
        this.packageName = context.getPackageName();
        this.zzfgi = zzbx(context);
        this.zzfgk = -1;
        this.zzfgj = str;
        this.zzfgl = null;
        this.zzfgm = null;
        this.zzfgn = true;
        this.zzfgp = zzbdb;
        this.zzfgq = zzd;
        this.zzfgr = new zzbda();
        this.zzfgo = 0;
        this.zzfgs = zzbcy;
        zzbq.checkArgument(true, "can't be anonymous with an upload account");
    }

    /* access modifiers changed from: private */
    public static int[] zzb(ArrayList<Integer> arrayList) {
        if (arrayList == null) {
            return null;
        }
        int[] iArr = new int[arrayList.size()];
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i = 0;
        int i2 = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            iArr[i2] = ((Integer) obj).intValue();
            i2++;
        }
        return iArr;
    }

    private static int zzbx(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException unused) {
            Log.wtf("ClearcutLogger", "This can't happen.");
            return 0;
        }
    }

    public final zzbcx zzh(byte[] bArr) {
        return new zzbcx(this, bArr, (zzbcw) null);
    }
}
