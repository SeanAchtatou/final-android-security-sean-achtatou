package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzdf;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbnr extends zzdf<zzbll, DriveFolder> {
    private /* synthetic */ MetadataChangeSet zzglq;
    private /* synthetic */ DriveFolder zzgml;

    zzbnr(zzbmu zzbmu, MetadataChangeSet metadataChangeSet, DriveFolder driveFolder) {
        this.zzglq = metadataChangeSet;
        this.zzgml = driveFolder;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        zzbll zzbll = (zzbll) zzb;
        this.zzglq.zzanz().setContext(zzbll.getContext());
        ((zzbpf) zzbll.zzakb()).zza(new zzbki(this.zzgml.getDriveId(), this.zzglq.zzanz()), new zzbrv(taskCompletionSource));
    }
}
