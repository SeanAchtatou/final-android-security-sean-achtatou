package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.quest.QuestBuffer;
import com.google.android.gms.games.quest.Quests;

final class zzbt implements Quests.LoadQuestsResult {
    private /* synthetic */ Status zzekv;

    zzbt(zzbs zzbs, Status status) {
        this.zzekv = status;
    }

    public final QuestBuffer getQuests() {
        return new QuestBuffer(DataHolder.zzca(this.zzekv.getStatusCode()));
    }

    public final Status getStatus() {
        return this.zzekv;
    }

    public final void release() {
    }
}
