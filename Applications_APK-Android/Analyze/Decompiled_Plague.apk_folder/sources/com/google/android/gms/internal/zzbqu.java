package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.DriveId;

public final class zzbqu extends zzbej {
    public static final Parcelable.Creator<zzbqu> CREATOR = new zzbqv();
    private int zzgfx;
    private DriveId zzgjm;
    private int zzgop;

    public zzbqu(DriveId driveId, int i, int i2) {
        this.zzgjm = driveId;
        this.zzgfx = i;
        this.zzgop = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.DriveId, int, int]
     candidates:
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.internal.zzbem.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int zze = zzbem.zze(parcel);
        zzbem.zza(parcel, 2, (Parcelable) this.zzgjm, i, false);
        zzbem.zzc(parcel, 3, this.zzgfx);
        zzbem.zzc(parcel, 4, this.zzgop);
        zzbem.zzai(parcel, zze);
    }
}
