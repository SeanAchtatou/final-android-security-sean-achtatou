package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.achievement.Achievements;

final class zzn implements Achievements.UpdateAchievementResult {
    private /* synthetic */ Status zzekv;
    private /* synthetic */ zzm zzhpx;

    zzn(zzm zzm, Status status) {
        this.zzhpx = zzm;
        this.zzekv = status;
    }

    public final String getAchievementId() {
        return this.zzhpx.zzbte;
    }

    public final Status getStatus() {
        return this.zzekv;
    }
}
