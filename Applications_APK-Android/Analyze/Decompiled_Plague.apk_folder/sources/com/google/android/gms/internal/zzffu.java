package com.google.android.gms.internal;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

class zzffu<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    private boolean zzkqq;
    private final int zzpdv;
    /* access modifiers changed from: private */
    public List<zzffz> zzpdw;
    /* access modifiers changed from: private */
    public Map<K, V> zzpdx;
    private volatile zzfgb zzpdy;
    private Map<K, V> zzpdz;

    private zzffu(int i) {
        this.zzpdv = i;
        this.zzpdw = Collections.emptyList();
        this.zzpdx = Collections.emptyMap();
        this.zzpdz = Collections.emptyMap();
    }

    /* synthetic */ zzffu(int i, zzffv zzffv) {
        this(i);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private final int zza(K r5) {
        /*
            r4 = this;
            java.util.List<com.google.android.gms.internal.zzffz> r0 = r4.zzpdw
            int r0 = r0.size()
            int r0 = r0 + -1
            if (r0 < 0) goto L_0x0025
            java.util.List<com.google.android.gms.internal.zzffz> r1 = r4.zzpdw
            java.lang.Object r1 = r1.get(r0)
            com.google.android.gms.internal.zzffz r1 = (com.google.android.gms.internal.zzffz) r1
            java.lang.Object r1 = r1.getKey()
            java.lang.Comparable r1 = (java.lang.Comparable) r1
            int r1 = r5.compareTo(r1)
            if (r1 <= 0) goto L_0x0022
            int r0 = r0 + 2
            int r5 = -r0
            return r5
        L_0x0022:
            if (r1 != 0) goto L_0x0025
            return r0
        L_0x0025:
            r1 = 0
        L_0x0026:
            if (r1 > r0) goto L_0x0049
            int r2 = r1 + r0
            int r2 = r2 / 2
            java.util.List<com.google.android.gms.internal.zzffz> r3 = r4.zzpdw
            java.lang.Object r3 = r3.get(r2)
            com.google.android.gms.internal.zzffz r3 = (com.google.android.gms.internal.zzffz) r3
            java.lang.Object r3 = r3.getKey()
            java.lang.Comparable r3 = (java.lang.Comparable) r3
            int r3 = r5.compareTo(r3)
            if (r3 >= 0) goto L_0x0043
            int r0 = r2 + -1
            goto L_0x0026
        L_0x0043:
            if (r3 <= 0) goto L_0x0048
            int r1 = r2 + 1
            goto L_0x0026
        L_0x0048:
            return r2
        L_0x0049:
            int r1 = r1 + 1
            int r5 = -r1
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzffu.zza(java.lang.Comparable):int");
    }

    /* access modifiers changed from: private */
    public final void zzcwl() {
        if (this.zzkqq) {
            throw new UnsupportedOperationException();
        }
    }

    private final SortedMap<K, V> zzcwm() {
        zzcwl();
        if (this.zzpdx.isEmpty() && !(this.zzpdx instanceof TreeMap)) {
            this.zzpdx = new TreeMap();
            this.zzpdz = ((TreeMap) this.zzpdx).descendingMap();
        }
        return (SortedMap) this.zzpdx;
    }

    static <FieldDescriptorType extends zzfed<FieldDescriptorType>> zzffu<FieldDescriptorType, Object> zzlp(int i) {
        return new zzffv(i);
    }

    /* access modifiers changed from: private */
    public final V zzlr(int i) {
        zzcwl();
        V value = this.zzpdw.remove(i).getValue();
        if (!this.zzpdx.isEmpty()) {
            Iterator it = zzcwm().entrySet().iterator();
            this.zzpdw.add(new zzffz(this, (Map.Entry) it.next()));
            it.remove();
        }
        return value;
    }

    public void clear() {
        zzcwl();
        if (!this.zzpdw.isEmpty()) {
            this.zzpdw.clear();
        }
        if (!this.zzpdx.isEmpty()) {
            this.zzpdx.clear();
        }
    }

    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return zza(comparable) >= 0 || this.zzpdx.containsKey(comparable);
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.zzpdy == null) {
            this.zzpdy = new zzfgb(this, null);
        }
        return this.zzpdy;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzffu)) {
            return super.equals(obj);
        }
        zzffu zzffu = (zzffu) obj;
        int size = size();
        if (size != zzffu.size()) {
            return false;
        }
        int zzcwj = zzcwj();
        if (zzcwj != zzffu.zzcwj()) {
            return entrySet().equals(zzffu.entrySet());
        }
        for (int i = 0; i < zzcwj; i++) {
            if (!zzlq(i).equals(zzffu.zzlq(i))) {
                return false;
            }
        }
        if (zzcwj != size) {
            return this.zzpdx.equals(zzffu.zzpdx);
        }
        return true;
    }

    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        return zza >= 0 ? this.zzpdw.get(zza).getValue() : this.zzpdx.get(comparable);
    }

    public int hashCode() {
        int zzcwj = zzcwj();
        int i = 0;
        for (int i2 = 0; i2 < zzcwj; i2++) {
            i += this.zzpdw.get(i2).hashCode();
        }
        return this.zzpdx.size() > 0 ? i + this.zzpdx.hashCode() : i;
    }

    public final boolean isImmutable() {
        return this.zzkqq;
    }

    public V remove(Object obj) {
        zzcwl();
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return zzlr(zza);
        }
        if (this.zzpdx.isEmpty()) {
            return null;
        }
        return this.zzpdx.remove(comparable);
    }

    public int size() {
        return this.zzpdw.size() + this.zzpdx.size();
    }

    /* renamed from: zza */
    public final V put(Comparable comparable, Object obj) {
        zzcwl();
        int zza = zza(comparable);
        if (zza >= 0) {
            return this.zzpdw.get(zza).setValue(obj);
        }
        zzcwl();
        if (this.zzpdw.isEmpty() && !(this.zzpdw instanceof ArrayList)) {
            this.zzpdw = new ArrayList(this.zzpdv);
        }
        int i = -(zza + 1);
        if (i >= this.zzpdv) {
            return zzcwm().put(comparable, obj);
        }
        if (this.zzpdw.size() == this.zzpdv) {
            zzffz remove = this.zzpdw.remove(this.zzpdv - 1);
            zzcwm().put((Comparable) remove.getKey(), remove.getValue());
        }
        this.zzpdw.add(i, new zzffz(this, comparable, obj));
        return null;
    }

    public void zzbim() {
        if (!this.zzkqq) {
            this.zzpdx = this.zzpdx.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(this.zzpdx);
            this.zzpdz = this.zzpdz.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(this.zzpdz);
            this.zzkqq = true;
        }
    }

    public final int zzcwj() {
        return this.zzpdw.size();
    }

    public final Iterable<Map.Entry<K, V>> zzcwk() {
        return this.zzpdx.isEmpty() ? zzffw.zzcwn() : this.zzpdx.entrySet();
    }

    public final Map.Entry<K, V> zzlq(int i) {
        return this.zzpdw.get(i);
    }
}
