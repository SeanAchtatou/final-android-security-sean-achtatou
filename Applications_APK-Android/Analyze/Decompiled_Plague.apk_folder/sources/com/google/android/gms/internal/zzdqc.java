package com.google.android.gms.internal;

import com.google.android.gms.internal.zzdqe;
import java.io.IOException;

public final class zzdqc extends zzfee<zzdqc, zza> implements zzffk {
    private static volatile zzffm<zzdqc> zzbas;
    /* access modifiers changed from: private */
    public static final zzdqc zzlqr;
    private int zzlql;
    private zzdqe zzlqp;

    public static final class zza extends zzfef<zzdqc, zza> implements zzffk {
        private zza() {
            super(zzdqc.zzlqr);
        }

        /* synthetic */ zza(zzdqd zzdqd) {
            this();
        }
    }

    static {
        zzdqc zzdqc = new zzdqc();
        zzlqr = zzdqc;
        zzdqc.zza(zzfem.zzpcf, (Object) null, (Object) null);
        zzdqc.zzpbs.zzbim();
    }

    private zzdqc() {
    }

    public static zzdqc zzp(zzfdh zzfdh) throws zzfew {
        return (zzdqc) zzfee.zza(zzlqr, zzfdh);
    }

    public final int getKeySize() {
        return this.zzlql;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        zzdqe.zza zza2;
        boolean z = false;
        switch (zzdqd.zzbaq[i - 1]) {
            case 1:
                return new zzdqc();
            case 2:
                return zzlqr;
            case 3:
                return null;
            case 4:
                return new zza(null);
            case 5:
                zzfen zzfen = (zzfen) obj;
                zzdqc zzdqc = (zzdqc) obj2;
                this.zzlqp = (zzdqe) zzfen.zza(this.zzlqp, zzdqc.zzlqp);
                boolean z2 = this.zzlql != 0;
                int i2 = this.zzlql;
                if (zzdqc.zzlql != 0) {
                    z = true;
                }
                this.zzlql = zzfen.zza(z2, i2, z, zzdqc.zzlql);
                return this;
            case 6:
                zzfdq zzfdq = (zzfdq) obj;
                zzfea zzfea = (zzfea) obj2;
                if (zzfea != null) {
                    while (!z) {
                        try {
                            int zzcts = zzfdq.zzcts();
                            if (zzcts != 0) {
                                if (zzcts == 10) {
                                    if (this.zzlqp != null) {
                                        zzdqe zzdqe = this.zzlqp;
                                        zzfef zzfef = (zzfef) zzdqe.zza(zzfem.zzpch, (Object) null, (Object) null);
                                        zzfef.zza((zzfee) zzdqe);
                                        zza2 = (zzdqe.zza) zzfef;
                                    } else {
                                        zza2 = null;
                                    }
                                    this.zzlqp = (zzdqe) zzfdq.zza(zzdqe.zzbmg(), zzfea);
                                    if (zza2 != null) {
                                        zza2.zza((zzfee) this.zzlqp);
                                        this.zzlqp = (zzdqe) zza2.zzcvj();
                                    }
                                } else if (zzcts == 16) {
                                    this.zzlql = zzfdq.zzcub();
                                } else if (!zza(zzcts, zzfdq)) {
                                }
                            }
                            z = true;
                        } catch (zzfew e) {
                            throw new RuntimeException(e.zzh(this));
                        } catch (IOException e2) {
                            throw new RuntimeException(new zzfew(e2.getMessage()).zzh(this));
                        }
                    }
                    break;
                } else {
                    throw new NullPointerException();
                }
            case 7:
                break;
            case 8:
                if (zzbas == null) {
                    synchronized (zzdqc.class) {
                        if (zzbas == null) {
                            zzbas = new zzfeg(zzlqr);
                        }
                    }
                }
                return zzbas;
            default:
                throw new UnsupportedOperationException();
        }
        return zzlqr;
    }

    public final void zza(zzfdv zzfdv) throws IOException {
        if (this.zzlqp != null) {
            zzfdv.zza(1, this.zzlqp == null ? zzdqe.zzbmg() : this.zzlqp);
        }
        if (this.zzlql != 0) {
            zzfdv.zzab(2, this.zzlql);
        }
        this.zzpbs.zza(zzfdv);
    }

    public final zzdqe zzbmc() {
        return this.zzlqp == null ? zzdqe.zzbmg() : this.zzlqp;
    }

    public final int zzhl() {
        int i = this.zzpbt;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.zzlqp != null) {
            i2 = 0 + zzfdv.zzb(1, this.zzlqp == null ? zzdqe.zzbmg() : this.zzlqp);
        }
        if (this.zzlql != 0) {
            i2 += zzfdv.zzae(2, this.zzlql);
        }
        int zzhl = i2 + this.zzpbs.zzhl();
        this.zzpbt = zzhl;
        return zzhl;
    }
}
