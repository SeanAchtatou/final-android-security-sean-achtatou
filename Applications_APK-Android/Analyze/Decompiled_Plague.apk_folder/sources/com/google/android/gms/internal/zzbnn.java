package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.zzdf;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.zzr;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzbnn extends zzdf<zzbll, Void> {
    private /* synthetic */ MetadataChangeSet zzglj;
    private /* synthetic */ DriveContents zzgmj;
    private /* synthetic */ zzr zzgmk;

    zzbnn(zzbmu zzbmu, zzr zzr, DriveContents driveContents, MetadataChangeSet metadataChangeSet) {
        this.zzgmk = zzr;
        this.zzgmj = driveContents;
        this.zzglj = metadataChangeSet;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Api.zzb zzb, TaskCompletionSource taskCompletionSource) throws RemoteException {
        zzbll zzbll = (zzbll) zzb;
        try {
            this.zzgmk.zza(zzbll);
        } catch (IllegalStateException e) {
            taskCompletionSource.setException(e);
        }
        this.zzgmj.zzanq();
        this.zzglj.zzanz().setContext(zzbll.getContext());
        ((zzbpf) zzbll.zzakb()).zza(new zzbjw(this.zzgmj.getDriveId(), this.zzglj.zzanz(), this.zzgmj.zzanp().getRequestId(), this.zzgmj.zzanp().zzanh(), this.zzgmk), new zzbsa(taskCompletionSource));
    }
}
