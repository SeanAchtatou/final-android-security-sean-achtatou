package com.facebook;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.facebook.AuthorizationClient;
import com.facebook.internal.SessionAuthorizationType;
import com.facebook.internal.Utility;
import com.facebook.internal.Validate;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Session implements Serializable {
    public static final String ACTION_ACTIVE_SESSION_CLOSED = "com.facebook.sdk.ACTIVE_SESSION_CLOSED";
    public static final String ACTION_ACTIVE_SESSION_OPENED = "com.facebook.sdk.ACTIVE_SESSION_OPENED";
    public static final String ACTION_ACTIVE_SESSION_SET = "com.facebook.sdk.ACTIVE_SESSION_SET";
    public static final String ACTION_ACTIVE_SESSION_UNSET = "com.facebook.sdk.ACTIVE_SESSION_UNSET";
    public static final String APPLICATION_ID_PROPERTY = "com.facebook.sdk.ApplicationId";
    private static final String AUTH_BUNDLE_SAVE_KEY = "com.facebook.sdk.Session.authBundleKey";
    public static final int DEFAULT_AUTHORIZE_ACTIVITY_CODE = 64206;
    private static final String MANAGE_PERMISSION_PREFIX = "manage";
    private static final Set<String> OTHER_PUBLISH_PERMISSIONS = new HashSet<String>() {
        {
            add("ads_management");
            add("create_event");
            add("rsvp_event");
        }
    };
    private static final String PUBLISH_PERMISSION_PREFIX = "publish";
    private static final String SESSION_BUNDLE_SAVE_KEY = "com.facebook.sdk.Session.saveSessionKey";
    private static final Object STATIC_LOCK = new Object();
    public static final String TAG = Session.class.getCanonicalName();
    private static final int TOKEN_EXTEND_RETRY_SECONDS = 3600;
    private static final int TOKEN_EXTEND_THRESHOLD_SECONDS = 86400;
    public static final String WEB_VIEW_ERROR_CODE_KEY = "com.facebook.sdk.WebViewErrorCode";
    public static final String WEB_VIEW_FAILING_URL_KEY = "com.facebook.sdk.FailingUrl";
    private static Session activeSession = null;
    private static final long serialVersionUID = 1;
    /* access modifiers changed from: private */
    public static volatile Context staticContext;
    private String applicationId;
    private volatile Bundle authorizationBundle;
    private AuthorizationClient authorizationClient;
    /* access modifiers changed from: private */
    public AutoPublishAsyncTask autoPublishAsyncTask;
    /* access modifiers changed from: private */
    public final List<StatusCallback> callbacks;
    /* access modifiers changed from: private */
    public volatile TokenRefreshRequest currentTokenRefreshRequest;
    /* access modifiers changed from: private */
    public Handler handler;
    private Date lastAttemptedTokenExtendDate;
    private final Object lock;
    private AuthorizationRequest pendingRequest;
    private SessionState state;
    private TokenCachingStrategy tokenCachingStrategy;
    private AccessToken tokenInfo;

    interface StartActivityDelegate {
        Activity getActivityContext();

        void startActivityForResult(Intent intent, int i);
    }

    public interface StatusCallback {
        void call(Session session, SessionState sessionState, Exception exc);
    }

    public int hashCode() {
        return 0;
    }

    private static class SerializationProxyV1 implements Serializable {
        private static final long serialVersionUID = 7663436173185080063L;
        private final String applicationId;
        private final Date lastAttemptedTokenExtendDate;
        private final AuthorizationRequest pendingRequest;
        private final boolean shouldAutoPublish;
        private final SessionState state;
        private final AccessToken tokenInfo;

        SerializationProxyV1(String str, SessionState sessionState, AccessToken accessToken, Date date, boolean z, AuthorizationRequest authorizationRequest) {
            this.applicationId = str;
            this.state = sessionState;
            this.tokenInfo = accessToken;
            this.lastAttemptedTokenExtendDate = date;
            this.shouldAutoPublish = z;
            this.pendingRequest = authorizationRequest;
        }

        private Object readResolve() {
            return new Session(this.applicationId, this.state, this.tokenInfo, this.lastAttemptedTokenExtendDate, this.shouldAutoPublish, this.pendingRequest);
        }
    }

    private Session(String str, SessionState sessionState, AccessToken accessToken, Date date, boolean z, AuthorizationRequest authorizationRequest) {
        this.lastAttemptedTokenExtendDate = new Date(0);
        this.lock = new Object();
        this.applicationId = str;
        this.state = sessionState;
        this.tokenInfo = accessToken;
        this.lastAttemptedTokenExtendDate = date;
        this.pendingRequest = authorizationRequest;
        this.handler = new Handler(Looper.getMainLooper());
        this.currentTokenRefreshRequest = null;
        this.tokenCachingStrategy = null;
        this.callbacks = new ArrayList();
    }

    public Session(Context context) {
        this(context, null, null, true);
    }

    Session(Context context, String str, TokenCachingStrategy tokenCachingStrategy2) {
        this(context, str, tokenCachingStrategy2, true);
    }

    Session(Context context, String str, TokenCachingStrategy tokenCachingStrategy2, boolean z) {
        this.lastAttemptedTokenExtendDate = new Date(0);
        this.lock = new Object();
        if (context != null && str == null) {
            str = Utility.getMetadataApplicationId(context);
        }
        Validate.notNull(str, "applicationId");
        initializeStaticContext(context);
        tokenCachingStrategy2 = tokenCachingStrategy2 == null ? new SharedPreferencesTokenCachingStrategy(staticContext) : tokenCachingStrategy2;
        this.applicationId = str;
        this.tokenCachingStrategy = tokenCachingStrategy2;
        this.state = SessionState.CREATED;
        Bundle bundle = null;
        this.pendingRequest = null;
        this.callbacks = new ArrayList();
        this.handler = new Handler(Looper.getMainLooper());
        bundle = z ? tokenCachingStrategy2.load() : bundle;
        if (TokenCachingStrategy.hasTokenInformation(bundle)) {
            Date date = TokenCachingStrategy.getDate(bundle, TokenCachingStrategy.EXPIRATION_DATE_KEY);
            Date date2 = new Date();
            if (date == null || date.before(date2)) {
                tokenCachingStrategy2.clear();
                this.tokenInfo = AccessToken.createEmptyToken(Collections.emptyList());
                return;
            }
            this.tokenInfo = AccessToken.createFromCache(bundle);
            this.state = SessionState.CREATED_TOKEN_LOADED;
            return;
        }
        this.tokenInfo = AccessToken.createEmptyToken(Collections.emptyList());
    }

    public final Bundle getAuthorizationBundle() {
        Bundle bundle;
        synchronized (this.lock) {
            bundle = this.authorizationBundle;
        }
        return bundle;
    }

    public final boolean isOpened() {
        boolean isOpened;
        synchronized (this.lock) {
            isOpened = this.state.isOpened();
        }
        return isOpened;
    }

    public final boolean isClosed() {
        boolean isClosed;
        synchronized (this.lock) {
            isClosed = this.state.isClosed();
        }
        return isClosed;
    }

    public final SessionState getState() {
        SessionState sessionState;
        synchronized (this.lock) {
            sessionState = this.state;
        }
        return sessionState;
    }

    public final String getApplicationId() {
        return this.applicationId;
    }

    public final String getAccessToken() {
        String token;
        synchronized (this.lock) {
            token = this.tokenInfo == null ? null : this.tokenInfo.getToken();
        }
        return token;
    }

    public final Date getExpirationDate() {
        Date expires;
        synchronized (this.lock) {
            expires = this.tokenInfo == null ? null : this.tokenInfo.getExpires();
        }
        return expires;
    }

    public final List<String> getPermissions() {
        List<String> permissions;
        synchronized (this.lock) {
            permissions = this.tokenInfo == null ? null : this.tokenInfo.getPermissions();
        }
        return permissions;
    }

    public final void openForRead(OpenRequest openRequest) {
        open(openRequest, SessionAuthorizationType.READ);
    }

    public final void openForPublish(OpenRequest openRequest) {
        open(openRequest, SessionAuthorizationType.PUBLISH);
    }

    public final void open(AccessToken accessToken, StatusCallback statusCallback) {
        synchronized (this.lock) {
            if (this.pendingRequest != null) {
                throw new UnsupportedOperationException("Session: an attempt was made to open a session that has a pending request.");
            } else if (this.state == SessionState.CREATED || this.state == SessionState.CREATED_TOKEN_LOADED) {
                if (statusCallback != null) {
                    addCallback(statusCallback);
                }
                this.tokenInfo = accessToken;
                if (this.tokenCachingStrategy != null) {
                    this.tokenCachingStrategy.save(accessToken.toCacheBundle());
                }
                SessionState sessionState = this.state;
                this.state = SessionState.OPENED;
                postStateChange(sessionState, this.state, null);
            } else {
                throw new UnsupportedOperationException("Session: an attempt was made to open an already opened session.");
            }
        }
        autoPublishAsync();
    }

    public final void requestNewReadPermissions(NewPermissionsRequest newPermissionsRequest) {
        requestNewPermissions(newPermissionsRequest, SessionAuthorizationType.READ);
    }

    public final void requestNewPublishPermissions(NewPermissionsRequest newPermissionsRequest) {
        requestNewPermissions(newPermissionsRequest, SessionAuthorizationType.PUBLISH);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        if (r6 == null) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        r1 = (com.facebook.AuthorizationClient.Result) r6.getSerializableExtra("com.facebook.LoginActivity:Result");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        if (r1 == null) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
        handleAuthorizationResult(r5, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002a, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002d, code lost:
        if (r2.authorizationClient == null) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002f, code lost:
        r2.authorizationClient.onActivityResult(r4, r5, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0034, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0035, code lost:
        if (r5 != 0) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0037, code lost:
        r4 = new com.facebook.FacebookOperationCanceledException("User canceled operation.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003f, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0040, code lost:
        finishAuthOrReauth(null, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0043, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean onActivityResult(android.app.Activity r3, int r4, int r5, android.content.Intent r6) {
        /*
            r2 = this;
            java.lang.String r0 = "currentActivity"
            com.facebook.internal.Validate.notNull(r3, r0)
            initializeStaticContext(r3)
            java.lang.Object r3 = r2.lock
            monitor-enter(r3)
            com.facebook.Session$AuthorizationRequest r0 = r2.pendingRequest     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x0044
            com.facebook.Session$AuthorizationRequest r0 = r2.pendingRequest     // Catch:{ all -> 0x0047 }
            int r0 = r0.getRequestCode()     // Catch:{ all -> 0x0047 }
            if (r4 == r0) goto L_0x0018
            goto L_0x0044
        L_0x0018:
            monitor-exit(r3)     // Catch:{ all -> 0x0047 }
            r3 = 0
            r0 = 1
            if (r6 == 0) goto L_0x0035
            java.lang.String r1 = "com.facebook.LoginActivity:Result"
            java.io.Serializable r1 = r6.getSerializableExtra(r1)
            com.facebook.AuthorizationClient$Result r1 = (com.facebook.AuthorizationClient.Result) r1
            if (r1 == 0) goto L_0x002b
            r2.handleAuthorizationResult(r5, r1)
            return r0
        L_0x002b:
            com.facebook.AuthorizationClient r1 = r2.authorizationClient
            if (r1 == 0) goto L_0x003f
            com.facebook.AuthorizationClient r3 = r2.authorizationClient
            r3.onActivityResult(r4, r5, r6)
            return r0
        L_0x0035:
            if (r5 != 0) goto L_0x003f
            com.facebook.FacebookOperationCanceledException r4 = new com.facebook.FacebookOperationCanceledException
            java.lang.String r5 = "User canceled operation."
            r4.<init>(r5)
            goto L_0x0040
        L_0x003f:
            r4 = r3
        L_0x0040:
            r2.finishAuthOrReauth(r3, r4)
            return r0
        L_0x0044:
            r4 = 0
            monitor-exit(r3)     // Catch:{ all -> 0x0047 }
            return r4
        L_0x0047:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0047 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.Session.onActivityResult(android.app.Activity, int, int, android.content.Intent):boolean");
    }

    public final void close() {
        synchronized (this.lock) {
            SessionState sessionState = this.state;
            switch (this.state) {
                case CREATED:
                case OPENING:
                    this.state = SessionState.CLOSED_LOGIN_FAILED;
                    postStateChange(sessionState, this.state, new FacebookException("Log in attempt aborted."));
                    break;
                case CREATED_TOKEN_LOADED:
                case OPENED:
                case OPENED_TOKEN_UPDATED:
                    this.state = SessionState.CLOSED;
                    postStateChange(sessionState, this.state, null);
                    break;
            }
        }
    }

    public final void closeAndClearTokenInformation() {
        if (this.tokenCachingStrategy != null) {
            this.tokenCachingStrategy.clear();
        }
        Utility.clearFacebookCookies(staticContext);
        close();
    }

    public final void addCallback(StatusCallback statusCallback) {
        synchronized (this.callbacks) {
            if (statusCallback != null) {
                try {
                    if (!this.callbacks.contains(statusCallback)) {
                        this.callbacks.add(statusCallback);
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
    }

    public final void removeCallback(StatusCallback statusCallback) {
        synchronized (this.callbacks) {
            this.callbacks.remove(statusCallback);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{Session");
        sb.append(" state:");
        sb.append(this.state);
        sb.append(", token:");
        sb.append(this.tokenInfo == null ? "null" : this.tokenInfo);
        sb.append(", appId:");
        sb.append(this.applicationId == null ? "null" : this.applicationId);
        sb.append("}");
        return sb.toString();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0037, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void extendTokenCompleted(android.os.Bundle r5) {
        /*
            r4 = this;
            java.lang.Object r0 = r4.lock
            monitor-enter(r0)
            com.facebook.SessionState r1 = r4.state     // Catch:{ all -> 0x0050 }
            int[] r2 = com.facebook.Session.AnonymousClass4.$SwitchMap$com$facebook$SessionState     // Catch:{ all -> 0x0050 }
            com.facebook.SessionState r3 = r4.state     // Catch:{ all -> 0x0050 }
            int r3 = r3.ordinal()     // Catch:{ all -> 0x0050 }
            r2 = r2[r3]     // Catch:{ all -> 0x0050 }
            switch(r2) {
                case 4: goto L_0x0015;
                case 5: goto L_0x001f;
                default: goto L_0x0012;
            }     // Catch:{ all -> 0x0050 }
        L_0x0012:
            java.lang.String r5 = com.facebook.Session.TAG     // Catch:{ all -> 0x0050 }
            goto L_0x0038
        L_0x0015:
            com.facebook.SessionState r2 = com.facebook.SessionState.OPENED_TOKEN_UPDATED     // Catch:{ all -> 0x0050 }
            r4.state = r2     // Catch:{ all -> 0x0050 }
            com.facebook.SessionState r2 = r4.state     // Catch:{ all -> 0x0050 }
            r3 = 0
            r4.postStateChange(r1, r2, r3)     // Catch:{ all -> 0x0050 }
        L_0x001f:
            com.facebook.AccessToken r1 = r4.tokenInfo     // Catch:{ all -> 0x0050 }
            com.facebook.AccessToken r5 = com.facebook.AccessToken.createFromRefresh(r1, r5)     // Catch:{ all -> 0x0050 }
            r4.tokenInfo = r5     // Catch:{ all -> 0x0050 }
            com.facebook.TokenCachingStrategy r5 = r4.tokenCachingStrategy     // Catch:{ all -> 0x0050 }
            if (r5 == 0) goto L_0x0036
            com.facebook.TokenCachingStrategy r5 = r4.tokenCachingStrategy     // Catch:{ all -> 0x0050 }
            com.facebook.AccessToken r1 = r4.tokenInfo     // Catch:{ all -> 0x0050 }
            android.os.Bundle r1 = r1.toCacheBundle()     // Catch:{ all -> 0x0050 }
            r5.save(r1)     // Catch:{ all -> 0x0050 }
        L_0x0036:
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
            return
        L_0x0038:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0050 }
            r1.<init>()     // Catch:{ all -> 0x0050 }
            java.lang.String r2 = "refreshToken ignored in state "
            r1.append(r2)     // Catch:{ all -> 0x0050 }
            com.facebook.SessionState r2 = r4.state     // Catch:{ all -> 0x0050 }
            r1.append(r2)     // Catch:{ all -> 0x0050 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0050 }
            android.util.Log.d(r5, r1)     // Catch:{ all -> 0x0050 }
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
            return
        L_0x0050:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.Session.extendTokenCompleted(android.os.Bundle):void");
    }

    private Object writeReplace() {
        return new SerializationProxyV1(this.applicationId, this.state, this.tokenInfo, this.lastAttemptedTokenExtendDate, false, this.pendingRequest);
    }

    private void readObject(ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Cannot readObject, serialization proxy required");
    }

    public static final void saveSession(Session session, Bundle bundle) {
        if (bundle != null && session != null && !bundle.containsKey(SESSION_BUNDLE_SAVE_KEY)) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                new ObjectOutputStream(byteArrayOutputStream).writeObject(session);
                bundle.putByteArray(SESSION_BUNDLE_SAVE_KEY, byteArrayOutputStream.toByteArray());
                bundle.putBundle(AUTH_BUNDLE_SAVE_KEY, session.authorizationBundle);
            } catch (IOException e) {
                throw new FacebookException("Unable to save session.", e);
            }
        }
    }

    public static final Session restoreSession(Context context, TokenCachingStrategy tokenCachingStrategy2, StatusCallback statusCallback, Bundle bundle) {
        byte[] byteArray;
        if (!(bundle == null || (byteArray = bundle.getByteArray(SESSION_BUNDLE_SAVE_KEY)) == null)) {
            try {
                Session session = (Session) new ObjectInputStream(new ByteArrayInputStream(byteArray)).readObject();
                initializeStaticContext(context);
                if (tokenCachingStrategy2 != null) {
                    session.tokenCachingStrategy = tokenCachingStrategy2;
                } else {
                    session.tokenCachingStrategy = new SharedPreferencesTokenCachingStrategy(context);
                }
                if (statusCallback != null) {
                    session.addCallback(statusCallback);
                }
                session.authorizationBundle = bundle.getBundle(AUTH_BUNDLE_SAVE_KEY);
                return session;
            } catch (ClassNotFoundException e) {
                Log.w(TAG, "Unable to restore session", e);
            } catch (IOException e2) {
                Log.w(TAG, "Unable to restore session.", e2);
            }
        }
        return null;
    }

    public static final Session getActiveSession() {
        Session session;
        synchronized (STATIC_LOCK) {
            session = activeSession;
        }
        return session;
    }

    public static final void setActiveSession(Session session) {
        synchronized (STATIC_LOCK) {
            if (session != activeSession) {
                Session session2 = activeSession;
                if (session2 != null) {
                    session2.close();
                }
                activeSession = session;
                if (session2 != null) {
                    postActiveSessionAction(ACTION_ACTIVE_SESSION_UNSET);
                }
                if (session != null) {
                    postActiveSessionAction(ACTION_ACTIVE_SESSION_SET);
                    if (session.isOpened()) {
                        postActiveSessionAction(ACTION_ACTIVE_SESSION_OPENED);
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.Session.openActiveSession(android.content.Context, boolean, com.facebook.Session$OpenRequest):com.facebook.Session
     arg types: [android.content.Context, int, ?[OBJECT, ARRAY]]
     candidates:
      com.facebook.Session.openActiveSession(android.app.Activity, boolean, com.facebook.Session$StatusCallback):com.facebook.Session
      com.facebook.Session.openActiveSession(android.content.Context, boolean, com.facebook.Session$OpenRequest):com.facebook.Session */
    public static Session openActiveSessionFromCache(Context context) {
        return openActiveSession(context, false, (OpenRequest) null);
    }

    public static Session openActiveSession(Activity activity, boolean z, StatusCallback statusCallback) {
        return openActiveSession(activity, z, new OpenRequest(activity).setCallback(statusCallback));
    }

    public static Session openActiveSession(Context context, Fragment fragment, boolean z, StatusCallback statusCallback) {
        return openActiveSession(context, z, new OpenRequest(fragment).setCallback(statusCallback));
    }

    public static Session openActiveSessionWithAccessToken(Context context, AccessToken accessToken, StatusCallback statusCallback) {
        Session session = new Session(context, null, null, false);
        setActiveSession(session);
        session.open(accessToken, statusCallback);
        return session;
    }

    private static Session openActiveSession(Context context, boolean z, OpenRequest openRequest) {
        Session build = new Builder(context).build();
        if (!SessionState.CREATED_TOKEN_LOADED.equals(build.getState()) && !z) {
            return null;
        }
        setActiveSession(build);
        build.openForRead(openRequest);
        return build;
    }

    static Context getStaticContext() {
        return staticContext;
    }

    static void initializeStaticContext(Context context) {
        if (context != null && staticContext == null) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            staticContext = context;
        }
    }

    /* access modifiers changed from: package-private */
    public void authorize(AuthorizationRequest authorizationRequest) {
        authorizationRequest.setApplicationId(this.applicationId);
        autoPublishAsync();
        boolean tryLoginActivity = tryLoginActivity(authorizationRequest);
        if (!tryLoginActivity && authorizationRequest.isLegacy) {
            tryLoginActivity = tryLegacyAuth(authorizationRequest);
        }
        if (!tryLoginActivity) {
            synchronized (this.lock) {
                SessionState sessionState = this.state;
                switch (this.state) {
                    case CLOSED:
                    case CLOSED_LOGIN_FAILED:
                        return;
                    default:
                        this.state = SessionState.CLOSED_LOGIN_FAILED;
                        postStateChange(sessionState, this.state, new FacebookException("Log in attempt failed."));
                        return;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0081, code lost:
        if (r1 != com.facebook.SessionState.OPENING) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0083, code lost:
        authorize(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void open(com.facebook.Session.OpenRequest r4, com.facebook.internal.SessionAuthorizationType r5) {
        /*
            r3 = this;
            r3.validatePermissions(r4, r5)
            r3.validateLoginBehavior(r4)
            java.lang.Object r5 = r3.lock
            monitor-enter(r5)
            com.facebook.Session$AuthorizationRequest r0 = r3.pendingRequest     // Catch:{ all -> 0x0087 }
            if (r0 == 0) goto L_0x001d
            com.facebook.SessionState r4 = r3.state     // Catch:{ all -> 0x0087 }
            com.facebook.SessionState r0 = r3.state     // Catch:{ all -> 0x0087 }
            java.lang.UnsupportedOperationException r1 = new java.lang.UnsupportedOperationException     // Catch:{ all -> 0x0087 }
            java.lang.String r2 = "Session: an attempt was made to open a session that has a pending request."
            r1.<init>(r2)     // Catch:{ all -> 0x0087 }
            r3.postStateChange(r4, r0, r1)     // Catch:{ all -> 0x0087 }
            monitor-exit(r5)     // Catch:{ all -> 0x0087 }
            return
        L_0x001d:
            com.facebook.SessionState r0 = r3.state     // Catch:{ all -> 0x0087 }
            int[] r1 = com.facebook.Session.AnonymousClass4.$SwitchMap$com$facebook$SessionState     // Catch:{ all -> 0x0087 }
            com.facebook.SessionState r2 = r3.state     // Catch:{ all -> 0x0087 }
            int r2 = r2.ordinal()     // Catch:{ all -> 0x0087 }
            r1 = r1[r2]     // Catch:{ all -> 0x0087 }
            r2 = 1
            if (r1 == r2) goto L_0x0061
            r2 = 3
            if (r1 == r2) goto L_0x0037
            java.lang.UnsupportedOperationException r4 = new java.lang.UnsupportedOperationException     // Catch:{ all -> 0x0087 }
            java.lang.String r0 = "Session: an attempt was made to open an already opened session."
            r4.<init>(r0)     // Catch:{ all -> 0x0087 }
            throw r4     // Catch:{ all -> 0x0087 }
        L_0x0037:
            if (r4 == 0) goto L_0x0053
            java.util.List r1 = r4.getPermissions()     // Catch:{ all -> 0x0087 }
            boolean r1 = com.facebook.internal.Utility.isNullOrEmpty(r1)     // Catch:{ all -> 0x0087 }
            if (r1 != 0) goto L_0x0053
            java.util.List r1 = r4.getPermissions()     // Catch:{ all -> 0x0087 }
            java.util.List r2 = r3.getPermissions()     // Catch:{ all -> 0x0087 }
            boolean r1 = com.facebook.internal.Utility.isSubset(r1, r2)     // Catch:{ all -> 0x0087 }
            if (r1 != 0) goto L_0x0053
            r3.pendingRequest = r4     // Catch:{ all -> 0x0087 }
        L_0x0053:
            com.facebook.Session$AuthorizationRequest r1 = r3.pendingRequest     // Catch:{ all -> 0x0087 }
            if (r1 != 0) goto L_0x005c
            com.facebook.SessionState r1 = com.facebook.SessionState.OPENED     // Catch:{ all -> 0x0087 }
            r3.state = r1     // Catch:{ all -> 0x0087 }
            goto L_0x0071
        L_0x005c:
            com.facebook.SessionState r1 = com.facebook.SessionState.OPENING     // Catch:{ all -> 0x0087 }
            r3.state = r1     // Catch:{ all -> 0x0087 }
            goto L_0x0071
        L_0x0061:
            com.facebook.SessionState r1 = com.facebook.SessionState.OPENING     // Catch:{ all -> 0x0087 }
            r3.state = r1     // Catch:{ all -> 0x0087 }
            if (r4 != 0) goto L_0x006f
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0087 }
            java.lang.String r0 = "openRequest cannot be null when opening a new Session"
            r4.<init>(r0)     // Catch:{ all -> 0x0087 }
            throw r4     // Catch:{ all -> 0x0087 }
        L_0x006f:
            r3.pendingRequest = r4     // Catch:{ all -> 0x0087 }
        L_0x0071:
            if (r4 == 0) goto L_0x007a
            com.facebook.Session$StatusCallback r2 = r4.getCallback()     // Catch:{ all -> 0x0087 }
            r3.addCallback(r2)     // Catch:{ all -> 0x0087 }
        L_0x007a:
            r2 = 0
            r3.postStateChange(r0, r1, r2)     // Catch:{ all -> 0x0087 }
            monitor-exit(r5)     // Catch:{ all -> 0x0087 }
            com.facebook.SessionState r5 = com.facebook.SessionState.OPENING
            if (r1 != r5) goto L_0x0086
            r3.authorize(r4)
        L_0x0086:
            return
        L_0x0087:
            r4 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0087 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.Session.open(com.facebook.Session$OpenRequest, com.facebook.internal.SessionAuthorizationType):void");
    }

    private void requestNewPermissions(NewPermissionsRequest newPermissionsRequest, SessionAuthorizationType sessionAuthorizationType) {
        validatePermissions(newPermissionsRequest, sessionAuthorizationType);
        validateLoginBehavior(newPermissionsRequest);
        if (newPermissionsRequest != null) {
            synchronized (this.lock) {
                if (this.pendingRequest != null) {
                    throw new UnsupportedOperationException("Session: an attempt was made to request new permissions for a session that has a pending request.");
                }
                switch (this.state) {
                    case OPENED:
                    case OPENED_TOKEN_UPDATED:
                        this.pendingRequest = newPermissionsRequest;
                        break;
                    default:
                        throw new UnsupportedOperationException("Session: an attempt was made to request new permissions for a session that is not currently open.");
                }
            }
            newPermissionsRequest.setValidateSameFbidAsToken(getAccessToken());
            authorize(newPermissionsRequest);
        }
    }

    private void validateLoginBehavior(AuthorizationRequest authorizationRequest) {
        if (authorizationRequest != null && !authorizationRequest.isLegacy) {
            Intent intent = new Intent();
            intent.setClass(getStaticContext(), LoginActivity.class);
            if (!resolveIntent(intent)) {
                throw new FacebookException(String.format("Cannot use SessionLoginBehavior %s when %s is not declared as an activity in AndroidManifest.xml", authorizationRequest.getLoginBehavior(), LoginActivity.class.getName()));
            }
        }
    }

    private void validatePermissions(AuthorizationRequest authorizationRequest, SessionAuthorizationType sessionAuthorizationType) {
        if (authorizationRequest != null && !Utility.isNullOrEmpty(authorizationRequest.getPermissions())) {
            for (String next : authorizationRequest.getPermissions()) {
                if (isPublishPermission(next)) {
                    if (SessionAuthorizationType.READ.equals(sessionAuthorizationType)) {
                        throw new FacebookException(String.format("Cannot pass a publish or manage permission (%s) to a request for read authorization", next));
                    }
                } else if (SessionAuthorizationType.PUBLISH.equals(sessionAuthorizationType)) {
                    Log.w(TAG, String.format("Should not pass a read permission (%s) to a request for publish or manage authorization", next));
                }
            }
        } else if (SessionAuthorizationType.PUBLISH.equals(sessionAuthorizationType)) {
            throw new FacebookException("Cannot request publish or manage authorization with no permissions.");
        }
    }

    static boolean isPublishPermission(String str) {
        return str != null && (str.startsWith(PUBLISH_PERMISSION_PREFIX) || str.startsWith(MANAGE_PERMISSION_PREFIX) || OTHER_PUBLISH_PERMISSIONS.contains(str));
    }

    /* access modifiers changed from: private */
    public void handleAuthorizationResult(int i, AuthorizationClient.Result result) {
        Exception exc;
        AccessToken accessToken;
        Exception facebookOperationCanceledException;
        if (i == -1) {
            if (result.code == AuthorizationClient.Result.Code.SUCCESS) {
                accessToken = result.token;
                exc = null;
                this.authorizationClient = null;
                finishAuthOrReauth(accessToken, exc);
            }
            facebookOperationCanceledException = new FacebookAuthorizationException(result.errorMessage);
        } else if (i == 0) {
            facebookOperationCanceledException = new FacebookOperationCanceledException(result.errorMessage);
        } else {
            accessToken = null;
            exc = null;
            this.authorizationClient = null;
            finishAuthOrReauth(accessToken, exc);
        }
        exc = facebookOperationCanceledException;
        accessToken = null;
        this.authorizationClient = null;
        finishAuthOrReauth(accessToken, exc);
    }

    private boolean tryLoginActivity(AuthorizationRequest authorizationRequest) {
        Intent loginActivityIntent = getLoginActivityIntent(authorizationRequest);
        if (!resolveIntent(loginActivityIntent)) {
            return false;
        }
        try {
            authorizationRequest.getStartActivityDelegate().startActivityForResult(loginActivityIntent, authorizationRequest.getRequestCode());
            return true;
        } catch (ActivityNotFoundException unused) {
            return false;
        }
    }

    private boolean resolveIntent(Intent intent) {
        return getStaticContext().getPackageManager().resolveActivity(intent, 0) != null;
    }

    private Intent getLoginActivityIntent(AuthorizationRequest authorizationRequest) {
        Intent intent = new Intent();
        intent.setClass(getStaticContext(), LoginActivity.class);
        intent.setAction(authorizationRequest.getLoginBehavior().toString());
        intent.putExtras(LoginActivity.populateIntentExtras(authorizationRequest.getAuthorizationClientRequest()));
        return intent;
    }

    private boolean tryLegacyAuth(AuthorizationRequest authorizationRequest) {
        this.authorizationClient = new AuthorizationClient();
        this.authorizationClient.setOnCompletedListener(new AuthorizationClient.OnCompletedListener() {
            public void onCompleted(AuthorizationClient.Result result) {
                Session.this.handleAuthorizationResult(-1, result);
            }
        });
        this.authorizationClient.setContext(getStaticContext());
        this.authorizationClient.startOrContinueAuth(authorizationRequest.getAuthorizationClientRequest());
        return true;
    }

    /* access modifiers changed from: package-private */
    public void finishAuthOrReauth(AccessToken accessToken, Exception exc) {
        if (accessToken != null && accessToken.isInvalid()) {
            accessToken = null;
            exc = new FacebookException("Invalid access token.");
        }
        synchronized (this.lock) {
            int i = AnonymousClass4.$SwitchMap$com$facebook$SessionState[this.state.ordinal()];
            if (i != 2) {
                switch (i) {
                    case 4:
                    case 5:
                        finishReauthorization(accessToken, exc);
                        break;
                }
            } else {
                finishAuthorization(accessToken, exc);
            }
        }
    }

    private void finishAuthorization(AccessToken accessToken, Exception exc) {
        SessionState sessionState = this.state;
        if (accessToken != null) {
            this.tokenInfo = accessToken;
            saveTokenToCache(accessToken);
            this.state = SessionState.OPENED;
        } else if (exc != null) {
            this.state = SessionState.CLOSED_LOGIN_FAILED;
        }
        this.pendingRequest = null;
        postStateChange(sessionState, this.state, exc);
    }

    private void finishReauthorization(AccessToken accessToken, Exception exc) {
        SessionState sessionState = this.state;
        if (accessToken != null) {
            this.tokenInfo = accessToken;
            saveTokenToCache(accessToken);
            this.state = SessionState.OPENED_TOKEN_UPDATED;
        }
        this.pendingRequest = null;
        postStateChange(sessionState, this.state, exc);
    }

    private void saveTokenToCache(AccessToken accessToken) {
        if (accessToken != null && this.tokenCachingStrategy != null) {
            this.tokenCachingStrategy.save(accessToken.toCacheBundle());
        }
    }

    /* access modifiers changed from: package-private */
    public void postStateChange(SessionState sessionState, final SessionState sessionState2, final Exception exc) {
        if (sessionState != sessionState2 || exc != null) {
            if (sessionState2.isClosed()) {
                this.tokenInfo = AccessToken.createEmptyToken(Collections.emptyList());
            }
            synchronized (this.callbacks) {
                runWithHandlerOrExecutor(this.handler, new Runnable() {
                    public void run() {
                        for (final StatusCallback statusCallback : Session.this.callbacks) {
                            Session.runWithHandlerOrExecutor(Session.this.handler, new Runnable() {
                                public void run() {
                                    statusCallback.call(Session.this, sessionState2, exc);
                                }
                            });
                        }
                    }
                });
            }
            if (this == activeSession && sessionState.isOpened() != sessionState2.isOpened()) {
                if (sessionState2.isOpened()) {
                    postActiveSessionAction(ACTION_ACTIVE_SESSION_OPENED);
                } else {
                    postActiveSessionAction(ACTION_ACTIVE_SESSION_CLOSED);
                }
            }
        }
    }

    static void postActiveSessionAction(String str) {
        LocalBroadcastManager.getInstance(getStaticContext()).sendBroadcast(new Intent(str));
    }

    /* access modifiers changed from: private */
    public static void runWithHandlerOrExecutor(Handler handler2, Runnable runnable) {
        if (handler2 != null) {
            handler2.post(runnable);
        } else {
            Settings.getExecutor().execute(runnable);
        }
    }

    /* access modifiers changed from: package-private */
    public void extendAccessTokenIfNeeded() {
        if (shouldExtendAccessToken()) {
            extendAccessToken();
        }
    }

    /* access modifiers changed from: package-private */
    public void extendAccessToken() {
        TokenRefreshRequest tokenRefreshRequest;
        synchronized (this.lock) {
            if (this.currentTokenRefreshRequest == null) {
                tokenRefreshRequest = new TokenRefreshRequest();
                this.currentTokenRefreshRequest = tokenRefreshRequest;
            } else {
                tokenRefreshRequest = null;
            }
        }
        if (tokenRefreshRequest != null) {
            tokenRefreshRequest.bind();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean shouldExtendAccessToken() {
        if (this.currentTokenRefreshRequest != null) {
            return false;
        }
        Date date = new Date();
        if (!this.state.isOpened() || !this.tokenInfo.getSource().canExtendToken() || date.getTime() - this.lastAttemptedTokenExtendDate.getTime() <= 3600000 || date.getTime() - this.tokenInfo.getLastRefresh().getTime() <= 86400000) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public AccessToken getTokenInfo() {
        return this.tokenInfo;
    }

    /* access modifiers changed from: package-private */
    public void setTokenInfo(AccessToken accessToken) {
        this.tokenInfo = accessToken;
    }

    /* access modifiers changed from: package-private */
    public Date getLastAttemptedTokenExtendDate() {
        return this.lastAttemptedTokenExtendDate;
    }

    /* access modifiers changed from: package-private */
    public void setLastAttemptedTokenExtendDate(Date date) {
        this.lastAttemptedTokenExtendDate = date;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentTokenRefreshRequest(TokenRefreshRequest tokenRefreshRequest) {
        this.currentTokenRefreshRequest = tokenRefreshRequest;
    }

    class TokenRefreshRequest implements ServiceConnection {
        final Messenger messageReceiver = new Messenger(new TokenRefreshRequestHandler(Session.this, this));
        Messenger messageSender = null;

        TokenRefreshRequest() {
        }

        public void bind() {
            Intent createTokenRefreshIntent = NativeProtocol.createTokenRefreshIntent(Session.getStaticContext());
            if (createTokenRefreshIntent == null || !Session.staticContext.bindService(createTokenRefreshIntent, new TokenRefreshRequest(), 1)) {
                cleanup();
            } else {
                Session.this.setLastAttemptedTokenExtendDate(new Date());
            }
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            this.messageSender = new Messenger(iBinder);
            refreshToken();
        }

        public void onServiceDisconnected(ComponentName componentName) {
            cleanup();
            Session.staticContext.unbindService(this);
        }

        /* access modifiers changed from: private */
        public void cleanup() {
            if (Session.this.currentTokenRefreshRequest == this) {
                TokenRefreshRequest unused = Session.this.currentTokenRefreshRequest = null;
            }
        }

        private void refreshToken() {
            Bundle bundle = new Bundle();
            bundle.putString("access_token", Session.this.getTokenInfo().getToken());
            Message obtain = Message.obtain();
            obtain.setData(bundle);
            obtain.replyTo = this.messageReceiver;
            try {
                this.messageSender.send(obtain);
            } catch (RemoteException unused) {
                cleanup();
            }
        }
    }

    static class TokenRefreshRequestHandler extends Handler {
        private WeakReference<TokenRefreshRequest> refreshRequestWeakReference;
        private WeakReference<Session> sessionWeakReference;

        TokenRefreshRequestHandler(Session session, TokenRefreshRequest tokenRefreshRequest) {
            super(Looper.getMainLooper());
            this.sessionWeakReference = new WeakReference<>(session);
            this.refreshRequestWeakReference = new WeakReference<>(tokenRefreshRequest);
        }

        public void handleMessage(Message message) {
            String string = message.getData().getString("access_token");
            Session session = this.sessionWeakReference.get();
            if (!(session == null || string == null)) {
                session.extendTokenCompleted(message.getData());
            }
            TokenRefreshRequest tokenRefreshRequest = this.refreshRequestWeakReference.get();
            if (tokenRefreshRequest != null) {
                Session.staticContext.unbindService(tokenRefreshRequest);
                tokenRefreshRequest.cleanup();
            }
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Session)) {
            return false;
        }
        Session session = (Session) obj;
        if (!areEqual(session.applicationId, this.applicationId) || !areEqual(session.authorizationBundle, this.authorizationBundle) || !areEqual(session.state, this.state) || !areEqual(session.getExpirationDate(), getExpirationDate())) {
            return false;
        }
        return true;
    }

    private static boolean areEqual(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    public static final class Builder {
        private String applicationId;
        private final Context context;
        private TokenCachingStrategy tokenCachingStrategy;

        public Builder(Context context2) {
            this.context = context2;
        }

        public Builder setApplicationId(String str) {
            this.applicationId = str;
            return this;
        }

        public Builder setTokenCachingStrategy(TokenCachingStrategy tokenCachingStrategy2) {
            this.tokenCachingStrategy = tokenCachingStrategy2;
            return this;
        }

        public Session build() {
            return new Session(this.context, this.applicationId, this.tokenCachingStrategy);
        }
    }

    private void autoPublishAsync() {
        AutoPublishAsyncTask autoPublishAsyncTask2;
        String str;
        synchronized (this) {
            if (this.autoPublishAsyncTask != null || !Settings.getShouldAutoPublishInstall() || (str = this.applicationId) == null) {
                autoPublishAsyncTask2 = null;
            } else {
                autoPublishAsyncTask2 = new AutoPublishAsyncTask(str, staticContext);
                this.autoPublishAsyncTask = autoPublishAsyncTask2;
            }
        }
        if (autoPublishAsyncTask2 != null) {
            autoPublishAsyncTask2.execute(new Void[0]);
        }
    }

    private class AutoPublishAsyncTask extends AsyncTask<Void, Void, Void> {
        private final Context mApplicationContext;
        private final String mApplicationId;

        public AutoPublishAsyncTask(String str, Context context) {
            this.mApplicationId = str;
            this.mApplicationContext = context.getApplicationContext();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voidArr) {
            try {
                Settings.publishInstallAndWait(this.mApplicationContext, this.mApplicationId);
                return null;
            } catch (Exception e) {
                Utility.logd("Facebook-publish", e.getMessage());
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void voidR) {
            synchronized (Session.this) {
                AutoPublishAsyncTask unused = Session.this.autoPublishAsyncTask = null;
            }
        }
    }

    public static class AuthorizationRequest implements Serializable {
        private static final long serialVersionUID = 1;
        private String applicationId;
        private SessionDefaultAudience defaultAudience;
        /* access modifiers changed from: private */
        public boolean isLegacy;
        private SessionLoginBehavior loginBehavior;
        private List<String> permissions;
        private int requestCode;
        /* access modifiers changed from: private */
        public final StartActivityDelegate startActivityDelegate;
        private StatusCallback statusCallback;
        private String validateSameFbidAsToken;

        AuthorizationRequest(final Activity activity) {
            this.loginBehavior = SessionLoginBehavior.SSO_WITH_FALLBACK;
            this.requestCode = Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE;
            this.isLegacy = false;
            this.permissions = Collections.emptyList();
            this.defaultAudience = SessionDefaultAudience.FRIENDS;
            this.startActivityDelegate = new StartActivityDelegate() {
                public void startActivityForResult(Intent intent, int i) {
                    activity.startActivityForResult(intent, i);
                }

                public Activity getActivityContext() {
                    return activity;
                }
            };
        }

        AuthorizationRequest(final Fragment fragment) {
            this.loginBehavior = SessionLoginBehavior.SSO_WITH_FALLBACK;
            this.requestCode = Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE;
            this.isLegacy = false;
            this.permissions = Collections.emptyList();
            this.defaultAudience = SessionDefaultAudience.FRIENDS;
            this.startActivityDelegate = new StartActivityDelegate() {
                public void startActivityForResult(Intent intent, int i) {
                    fragment.startActivityForResult(intent, i);
                }

                public Activity getActivityContext() {
                    return fragment.getActivity();
                }
            };
        }

        private AuthorizationRequest(SessionLoginBehavior sessionLoginBehavior, int i, List<String> list, String str, boolean z, String str2, String str3) {
            this.loginBehavior = SessionLoginBehavior.SSO_WITH_FALLBACK;
            this.requestCode = Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE;
            this.isLegacy = false;
            this.permissions = Collections.emptyList();
            this.defaultAudience = SessionDefaultAudience.FRIENDS;
            this.startActivityDelegate = new StartActivityDelegate() {
                public void startActivityForResult(Intent intent, int i) {
                    throw new UnsupportedOperationException("Cannot create an AuthorizationRequest without a valid Activity or Fragment");
                }

                public Activity getActivityContext() {
                    throw new UnsupportedOperationException("Cannot create an AuthorizationRequest without a valid Activity or Fragment");
                }
            };
            this.loginBehavior = sessionLoginBehavior;
            this.requestCode = i;
            this.permissions = list;
            this.defaultAudience = SessionDefaultAudience.valueOf(str);
            this.isLegacy = z;
            this.applicationId = str2;
            this.validateSameFbidAsToken = str3;
        }

        public void setIsLegacy(boolean z) {
            this.isLegacy = z;
        }

        /* access modifiers changed from: package-private */
        public boolean isLegacy() {
            return this.isLegacy;
        }

        /* access modifiers changed from: package-private */
        public AuthorizationRequest setCallback(StatusCallback statusCallback2) {
            this.statusCallback = statusCallback2;
            return this;
        }

        /* access modifiers changed from: package-private */
        public StatusCallback getCallback() {
            return this.statusCallback;
        }

        /* access modifiers changed from: package-private */
        public AuthorizationRequest setLoginBehavior(SessionLoginBehavior sessionLoginBehavior) {
            if (sessionLoginBehavior != null) {
                this.loginBehavior = sessionLoginBehavior;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public SessionLoginBehavior getLoginBehavior() {
            return this.loginBehavior;
        }

        /* access modifiers changed from: package-private */
        public AuthorizationRequest setRequestCode(int i) {
            if (i >= 0) {
                this.requestCode = i;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public int getRequestCode() {
            return this.requestCode;
        }

        /* access modifiers changed from: package-private */
        public AuthorizationRequest setPermissions(List<String> list) {
            if (list != null) {
                this.permissions = list;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public List<String> getPermissions() {
            return this.permissions;
        }

        /* access modifiers changed from: package-private */
        public AuthorizationRequest setDefaultAudience(SessionDefaultAudience sessionDefaultAudience) {
            if (sessionDefaultAudience != null) {
                this.defaultAudience = sessionDefaultAudience;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public SessionDefaultAudience getDefaultAudience() {
            return this.defaultAudience;
        }

        /* access modifiers changed from: package-private */
        public StartActivityDelegate getStartActivityDelegate() {
            return this.startActivityDelegate;
        }

        /* access modifiers changed from: package-private */
        public String getApplicationId() {
            return this.applicationId;
        }

        /* access modifiers changed from: package-private */
        public void setApplicationId(String str) {
            this.applicationId = str;
        }

        /* access modifiers changed from: package-private */
        public String getValidateSameFbidAsToken() {
            return this.validateSameFbidAsToken;
        }

        /* access modifiers changed from: package-private */
        public void setValidateSameFbidAsToken(String str) {
            this.validateSameFbidAsToken = str;
        }

        /* access modifiers changed from: package-private */
        public AuthorizationClient.AuthorizationRequest getAuthorizationClientRequest() {
            return new AuthorizationClient.AuthorizationRequest(this.loginBehavior, this.requestCode, this.isLegacy, this.permissions, this.defaultAudience, this.applicationId, this.validateSameFbidAsToken, new AuthorizationClient.StartActivityDelegate() {
                public void startActivityForResult(Intent intent, int i) {
                    AuthorizationRequest.this.startActivityDelegate.startActivityForResult(intent, i);
                }

                public Activity getActivityContext() {
                    return AuthorizationRequest.this.startActivityDelegate.getActivityContext();
                }
            });
        }

        /* access modifiers changed from: package-private */
        public Object writeReplace() {
            return new AuthRequestSerializationProxyV1(this.loginBehavior, this.requestCode, this.permissions, this.defaultAudience.name(), this.isLegacy, this.applicationId, this.validateSameFbidAsToken);
        }

        /* access modifiers changed from: package-private */
        public void readObject(ObjectInputStream objectInputStream) throws InvalidObjectException {
            throw new InvalidObjectException("Cannot readObject, serialization proxy required");
        }

        private static class AuthRequestSerializationProxyV1 implements Serializable {
            private static final long serialVersionUID = -8748347685113614927L;
            private final String applicationId;
            private final String defaultAudience;
            private boolean isLegacy;
            private final SessionLoginBehavior loginBehavior;
            private final List<String> permissions;
            private final int requestCode;
            private final String validateSameFbidAsToken;

            private AuthRequestSerializationProxyV1(SessionLoginBehavior sessionLoginBehavior, int i, List<String> list, String str, boolean z, String str2, String str3) {
                this.loginBehavior = sessionLoginBehavior;
                this.requestCode = i;
                this.permissions = list;
                this.defaultAudience = str;
                this.isLegacy = z;
                this.applicationId = str2;
                this.validateSameFbidAsToken = str3;
            }

            private Object readResolve() {
                return new AuthorizationRequest(this.loginBehavior, this.requestCode, this.permissions, this.defaultAudience, this.isLegacy, this.applicationId, this.validateSameFbidAsToken);
            }
        }
    }

    public static final class OpenRequest extends AuthorizationRequest {
        private static final long serialVersionUID = 1;

        public OpenRequest(Activity activity) {
            super(activity);
        }

        public OpenRequest(Fragment fragment) {
            super(fragment);
        }

        public final OpenRequest setCallback(StatusCallback statusCallback) {
            super.setCallback(statusCallback);
            return this;
        }

        public final OpenRequest setLoginBehavior(SessionLoginBehavior sessionLoginBehavior) {
            super.setLoginBehavior(sessionLoginBehavior);
            return this;
        }

        public final OpenRequest setRequestCode(int i) {
            super.setRequestCode(i);
            return this;
        }

        public final OpenRequest setPermissions(List<String> list) {
            super.setPermissions(list);
            return this;
        }

        public final OpenRequest setDefaultAudience(SessionDefaultAudience sessionDefaultAudience) {
            super.setDefaultAudience(sessionDefaultAudience);
            return this;
        }
    }

    public static final class NewPermissionsRequest extends AuthorizationRequest {
        private static final long serialVersionUID = 1;

        public NewPermissionsRequest(Activity activity, List<String> list) {
            super(activity);
            setPermissions(list);
        }

        public NewPermissionsRequest(Fragment fragment, List<String> list) {
            super(fragment);
            setPermissions(list);
        }

        public final NewPermissionsRequest setCallback(StatusCallback statusCallback) {
            super.setCallback(statusCallback);
            return this;
        }

        public final NewPermissionsRequest setLoginBehavior(SessionLoginBehavior sessionLoginBehavior) {
            super.setLoginBehavior(sessionLoginBehavior);
            return this;
        }

        public final NewPermissionsRequest setRequestCode(int i) {
            super.setRequestCode(i);
            return this;
        }

        public final NewPermissionsRequest setDefaultAudience(SessionDefaultAudience sessionDefaultAudience) {
            super.setDefaultAudience(sessionDefaultAudience);
            return this;
        }
    }
}
