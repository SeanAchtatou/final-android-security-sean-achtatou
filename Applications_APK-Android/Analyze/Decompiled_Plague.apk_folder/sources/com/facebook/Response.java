package com.facebook;

import android.content.Context;
import android.support.v4.os.EnvironmentCompat;
import com.facebook.internal.FileLruCache;
import com.facebook.internal.Logger;
import com.facebook.internal.Utility;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphObjectList;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Response {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final String BODY_KEY = "body";
    private static final String CODE_KEY = "code";
    private static final int INVALID_SESSION_FACEBOOK_ERROR_CODE = 190;
    public static final String NON_JSON_RESPONSE_PROPERTY = "FACEBOOK_NON_JSON_RESULT";
    private static final String RESPONSE_CACHE_TAG = "ResponseCache";
    private static final String RESPONSE_LOG_TAG = "Response";
    private static FileLruCache responseCache;
    private final HttpURLConnection connection;
    private final FacebookRequestError error;
    private final GraphObject graphObject;
    private final GraphObjectList<GraphObject> graphObjectList;
    private final boolean isFromCache;
    private final Request request;

    interface PagedResults extends GraphObject {
        GraphObjectList<GraphObject> getData();

        PagingInfo getPaging();
    }

    public enum PagingDirection {
        NEXT,
        PREVIOUS
    }

    interface PagingInfo extends GraphObject {
        String getNext();

        String getPrevious();
    }

    Response(Request request2, HttpURLConnection httpURLConnection, GraphObject graphObject2, boolean z) {
        this.request = request2;
        this.connection = httpURLConnection;
        this.graphObject = graphObject2;
        this.graphObjectList = null;
        this.isFromCache = z;
        this.error = null;
    }

    Response(Request request2, HttpURLConnection httpURLConnection, GraphObjectList<GraphObject> graphObjectList2, boolean z) {
        this.request = request2;
        this.connection = httpURLConnection;
        this.graphObject = null;
        this.graphObjectList = graphObjectList2;
        this.isFromCache = z;
        this.error = null;
    }

    Response(Request request2, HttpURLConnection httpURLConnection, FacebookRequestError facebookRequestError) {
        this.request = request2;
        this.connection = httpURLConnection;
        this.graphObject = null;
        this.graphObjectList = null;
        this.isFromCache = false;
        this.error = facebookRequestError;
    }

    public final FacebookRequestError getError() {
        return this.error;
    }

    public final GraphObject getGraphObject() {
        return this.graphObject;
    }

    public final <T extends GraphObject> T getGraphObjectAs(Class<T> cls) {
        if (this.graphObject == null) {
            return null;
        }
        if (cls != null) {
            return this.graphObject.cast(cls);
        }
        throw new NullPointerException("Must pass in a valid interface that extends GraphObject");
    }

    public final GraphObjectList<GraphObject> getGraphObjectList() {
        return this.graphObjectList;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Class, java.lang.Class<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T extends com.facebook.model.GraphObject> com.facebook.model.GraphObjectList<T> getGraphObjectListAs(java.lang.Class<T> r2) {
        /*
            r1 = this;
            com.facebook.model.GraphObjectList<com.facebook.model.GraphObject> r0 = r1.graphObjectList
            if (r0 != 0) goto L_0x0006
            r2 = 0
            return r2
        L_0x0006:
            com.facebook.model.GraphObjectList<com.facebook.model.GraphObject> r0 = r1.graphObjectList
            com.facebook.model.GraphObjectList r2 = r0.castToListOf(r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.Response.getGraphObjectListAs(java.lang.Class):com.facebook.model.GraphObjectList");
    }

    public final HttpURLConnection getConnection() {
        return this.connection;
    }

    public Request getRequest() {
        return this.request;
    }

    public Request getRequestForPagedResults(PagingDirection pagingDirection) {
        String str;
        PagingInfo paging;
        if (this.graphObject == null || (paging = ((PagedResults) this.graphObject.cast(PagedResults.class)).getPaging()) == null) {
            str = null;
        } else {
            str = pagingDirection == PagingDirection.NEXT ? paging.getNext() : paging.getPrevious();
        }
        if (Utility.isNullOrEmpty(str)) {
            return null;
        }
        if (str != null && str.equals(this.request.getUrlForSingleRequest())) {
            return null;
        }
        try {
            return new Request(this.request.getSession(), new URL(str));
        } catch (MalformedURLException unused) {
            return null;
        }
    }

    public String toString() {
        String str;
        try {
            Object[] objArr = new Object[1];
            objArr[0] = Integer.valueOf(this.connection != null ? this.connection.getResponseCode() : 200);
            str = String.format("%d", objArr);
        } catch (IOException unused) {
            str = EnvironmentCompat.MEDIA_UNKNOWN;
        }
        return "{Response: " + " responseCode: " + str + ", graphObject: " + this.graphObject + ", error: " + this.error + ", isFromCache:" + this.isFromCache + "}";
    }

    public final boolean getIsFromCache() {
        return this.isFromCache;
    }

    static FileLruCache getResponseCache() {
        Context staticContext;
        if (responseCache == null && (staticContext = Session.getStaticContext()) != null) {
            responseCache = new FileLruCache(staticContext, RESPONSE_CACHE_TAG, new FileLruCache.Limits());
        }
        return responseCache;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x007d, code lost:
        if (r1 != null) goto L_0x0093;
     */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0069 A[Catch:{ FacebookException -> 0x00d3, JSONException -> 0x00b8, IOException -> 0x009d }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x006f A[Catch:{ FacebookException -> 0x00d3, JSONException -> 0x00b8, IOException -> 0x009d }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:67:0x00d4=Splitter:B:67:0x00d4, B:62:0x00b9=Splitter:B:62:0x00b9, B:57:0x009e=Splitter:B:57:0x009e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.util.List<com.facebook.Response> fromHttpConnection(java.net.HttpURLConnection r10, com.facebook.RequestBatch r11) {
        /*
            boolean r0 = r11 instanceof com.facebook.internal.CacheableRequestBatch
            r1 = 0
            r2 = 0
            r3 = 1
            if (r0 == 0) goto L_0x005f
            r0 = r11
            com.facebook.internal.CacheableRequestBatch r0 = (com.facebook.internal.CacheableRequestBatch) r0
            com.facebook.internal.FileLruCache r4 = getResponseCache()
            java.lang.String r5 = r0.getCacheKeyOverride()
            boolean r6 = com.facebook.internal.Utility.isNullOrEmpty(r5)
            if (r6 == 0) goto L_0x0030
            int r6 = r11.size()
            if (r6 != r3) goto L_0x0027
            com.facebook.Request r5 = r11.get(r2)
            java.lang.String r5 = r5.getUrlForSingleRequest()
            goto L_0x0030
        L_0x0027:
            com.facebook.LoggingBehavior r6 = com.facebook.LoggingBehavior.REQUESTS
            java.lang.String r7 = "ResponseCache"
            java.lang.String r8 = "Not using cache for cacheable request because no key was specified"
            com.facebook.internal.Logger.log(r6, r7, r8)
        L_0x0030:
            boolean r0 = r0.getForceRoundTrip()
            if (r0 != 0) goto L_0x0061
            if (r4 == 0) goto L_0x0061
            boolean r0 = com.facebook.internal.Utility.isNullOrEmpty(r5)
            if (r0 != 0) goto L_0x0061
            java.io.InputStream r0 = r4.get(r5)     // Catch:{ FacebookException | IOException | JSONException -> 0x005b, all -> 0x0055 }
            if (r0 == 0) goto L_0x0050
            java.util.List r1 = createResponsesFromStream(r0, r1, r11, r3)     // Catch:{ FacebookException | IOException | JSONException -> 0x004e, all -> 0x004c }
            com.facebook.internal.Utility.closeQuietly(r0)
            return r1
        L_0x004c:
            r10 = move-exception
            goto L_0x0057
        L_0x004e:
            r1 = r0
            goto L_0x005b
        L_0x0050:
            com.facebook.internal.Utility.closeQuietly(r0)
            r1 = r0
            goto L_0x0061
        L_0x0055:
            r10 = move-exception
            r0 = r1
        L_0x0057:
            com.facebook.internal.Utility.closeQuietly(r0)
            throw r10
        L_0x005b:
            com.facebook.internal.Utility.closeQuietly(r1)
            goto L_0x0061
        L_0x005f:
            r4 = r1
            r5 = r4
        L_0x0061:
            int r0 = r10.getResponseCode()     // Catch:{ FacebookException -> 0x00d3, JSONException -> 0x00b8, IOException -> 0x009d }
            r6 = 400(0x190, float:5.6E-43)
            if (r0 < r6) goto L_0x006f
            java.io.InputStream r0 = r10.getErrorStream()     // Catch:{ FacebookException -> 0x00d3, JSONException -> 0x00b8, IOException -> 0x009d }
        L_0x006d:
            r1 = r0
            goto L_0x0093
        L_0x006f:
            java.io.InputStream r0 = r10.getInputStream()     // Catch:{ FacebookException -> 0x00d3, JSONException -> 0x00b8, IOException -> 0x009d }
            if (r4 == 0) goto L_0x006d
            if (r5 == 0) goto L_0x006d
            if (r0 == 0) goto L_0x006d
            java.io.InputStream r1 = r4.interceptAndPut(r5, r0)     // Catch:{ FacebookException -> 0x008e, JSONException -> 0x0089, IOException -> 0x0084, all -> 0x0080 }
            if (r1 == 0) goto L_0x006d
            goto L_0x0093
        L_0x0080:
            r10 = move-exception
            r1 = r0
            goto L_0x00e9
        L_0x0084:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x009e
        L_0x0089:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00b9
        L_0x008e:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00d4
        L_0x0093:
            java.util.List r0 = createResponsesFromStream(r1, r10, r11, r2)     // Catch:{ FacebookException -> 0x00d3, JSONException -> 0x00b8, IOException -> 0x009d }
            com.facebook.internal.Utility.closeQuietly(r1)
            return r0
        L_0x009b:
            r10 = move-exception
            goto L_0x00e9
        L_0x009d:
            r0 = move-exception
        L_0x009e:
            com.facebook.LoggingBehavior r4 = com.facebook.LoggingBehavior.REQUESTS     // Catch:{ all -> 0x009b }
            java.lang.String r5 = "Response"
            java.lang.String r6 = "Response <Error>: %s"
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x009b }
            r3[r2] = r0     // Catch:{ all -> 0x009b }
            com.facebook.internal.Logger.log(r4, r5, r6, r3)     // Catch:{ all -> 0x009b }
            com.facebook.FacebookException r2 = new com.facebook.FacebookException     // Catch:{ all -> 0x009b }
            r2.<init>(r0)     // Catch:{ all -> 0x009b }
            java.util.List r10 = constructErrorResponses(r11, r10, r2)     // Catch:{ all -> 0x009b }
            com.facebook.internal.Utility.closeQuietly(r1)
            return r10
        L_0x00b8:
            r0 = move-exception
        L_0x00b9:
            com.facebook.LoggingBehavior r4 = com.facebook.LoggingBehavior.REQUESTS     // Catch:{ all -> 0x009b }
            java.lang.String r5 = "Response"
            java.lang.String r6 = "Response <Error>: %s"
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x009b }
            r3[r2] = r0     // Catch:{ all -> 0x009b }
            com.facebook.internal.Logger.log(r4, r5, r6, r3)     // Catch:{ all -> 0x009b }
            com.facebook.FacebookException r2 = new com.facebook.FacebookException     // Catch:{ all -> 0x009b }
            r2.<init>(r0)     // Catch:{ all -> 0x009b }
            java.util.List r10 = constructErrorResponses(r11, r10, r2)     // Catch:{ all -> 0x009b }
            com.facebook.internal.Utility.closeQuietly(r1)
            return r10
        L_0x00d3:
            r0 = move-exception
        L_0x00d4:
            com.facebook.LoggingBehavior r4 = com.facebook.LoggingBehavior.REQUESTS     // Catch:{ all -> 0x009b }
            java.lang.String r5 = "Response"
            java.lang.String r6 = "Response <Error>: %s"
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x009b }
            r3[r2] = r0     // Catch:{ all -> 0x009b }
            com.facebook.internal.Logger.log(r4, r5, r6, r3)     // Catch:{ all -> 0x009b }
            java.util.List r10 = constructErrorResponses(r11, r10, r0)     // Catch:{ all -> 0x009b }
            com.facebook.internal.Utility.closeQuietly(r1)
            return r10
        L_0x00e9:
            com.facebook.internal.Utility.closeQuietly(r1)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.Response.fromHttpConnection(java.net.HttpURLConnection, com.facebook.RequestBatch):java.util.List");
    }

    static List<Response> createResponsesFromStream(InputStream inputStream, HttpURLConnection httpURLConnection, RequestBatch requestBatch, boolean z) throws FacebookException, JSONException, IOException {
        String readStreamToString = Utility.readStreamToString(inputStream);
        Logger.log(LoggingBehavior.INCLUDE_RAW_RESPONSES, RESPONSE_LOG_TAG, "Response (raw)\n  Size: %d\n  Response:\n%s\n", Integer.valueOf(readStreamToString.length()), readStreamToString);
        List<Response> createResponsesFromObject = createResponsesFromObject(httpURLConnection, requestBatch, new JSONTokener(readStreamToString).nextValue(), z);
        Logger.log(LoggingBehavior.REQUESTS, RESPONSE_LOG_TAG, "Response\n  Id: %s\n  Size: %d\n  Responses:\n%s\n", requestBatch.getId(), Integer.valueOf(readStreamToString.length()), createResponsesFromObject);
        return createResponsesFromObject;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0056  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.List<com.facebook.Response> createResponsesFromObject(java.net.HttpURLConnection r7, java.util.List<com.facebook.Request> r8, java.lang.Object r9, boolean r10) throws com.facebook.FacebookException, org.json.JSONException {
        /*
            int r0 = r8.size()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>(r0)
            r2 = 0
            r3 = 1
            if (r0 != r3) goto L_0x0051
            java.lang.Object r3 = r8.get(r2)
            com.facebook.Request r3 = (com.facebook.Request) r3
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0043, IOException -> 0x0034 }
            r4.<init>()     // Catch:{ JSONException -> 0x0043, IOException -> 0x0034 }
            java.lang.String r5 = "body"
            r4.put(r5, r9)     // Catch:{ JSONException -> 0x0043, IOException -> 0x0034 }
            if (r7 == 0) goto L_0x0024
            int r5 = r7.getResponseCode()     // Catch:{ JSONException -> 0x0043, IOException -> 0x0034 }
            goto L_0x0026
        L_0x0024:
            r5 = 200(0xc8, float:2.8E-43)
        L_0x0026:
            java.lang.String r6 = "code"
            r4.put(r6, r5)     // Catch:{ JSONException -> 0x0043, IOException -> 0x0034 }
            org.json.JSONArray r5 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0043, IOException -> 0x0034 }
            r5.<init>()     // Catch:{ JSONException -> 0x0043, IOException -> 0x0034 }
            r5.put(r4)     // Catch:{ JSONException -> 0x0043, IOException -> 0x0034 }
            goto L_0x0052
        L_0x0034:
            r4 = move-exception
            com.facebook.Response r5 = new com.facebook.Response
            com.facebook.FacebookRequestError r6 = new com.facebook.FacebookRequestError
            r6.<init>(r7, r4)
            r5.<init>(r3, r7, r6)
            r1.add(r5)
            goto L_0x0051
        L_0x0043:
            r4 = move-exception
            com.facebook.Response r5 = new com.facebook.Response
            com.facebook.FacebookRequestError r6 = new com.facebook.FacebookRequestError
            r6.<init>(r7, r4)
            r5.<init>(r3, r7, r6)
            r1.add(r5)
        L_0x0051:
            r5 = r9
        L_0x0052:
            boolean r3 = r5 instanceof org.json.JSONArray
            if (r3 == 0) goto L_0x0098
            org.json.JSONArray r5 = (org.json.JSONArray) r5
            int r3 = r5.length()
            if (r3 == r0) goto L_0x005f
            goto L_0x0098
        L_0x005f:
            int r0 = r5.length()
            if (r2 >= r0) goto L_0x0097
            java.lang.Object r0 = r8.get(r2)
            com.facebook.Request r0 = (com.facebook.Request) r0
            java.lang.Object r3 = r5.get(r2)     // Catch:{ JSONException -> 0x0086, FacebookException -> 0x0077 }
            com.facebook.Response r3 = createResponseFromObject(r0, r7, r3, r10, r9)     // Catch:{ JSONException -> 0x0086, FacebookException -> 0x0077 }
            r1.add(r3)     // Catch:{ JSONException -> 0x0086, FacebookException -> 0x0077 }
            goto L_0x0094
        L_0x0077:
            r3 = move-exception
            com.facebook.Response r4 = new com.facebook.Response
            com.facebook.FacebookRequestError r6 = new com.facebook.FacebookRequestError
            r6.<init>(r7, r3)
            r4.<init>(r0, r7, r6)
            r1.add(r4)
            goto L_0x0094
        L_0x0086:
            r3 = move-exception
            com.facebook.Response r4 = new com.facebook.Response
            com.facebook.FacebookRequestError r6 = new com.facebook.FacebookRequestError
            r6.<init>(r7, r3)
            r4.<init>(r0, r7, r6)
            r1.add(r4)
        L_0x0094:
            int r2 = r2 + 1
            goto L_0x005f
        L_0x0097:
            return r1
        L_0x0098:
            com.facebook.FacebookException r7 = new com.facebook.FacebookException
            java.lang.String r8 = "Unexpected number of results"
            r7.<init>(r8)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.Response.createResponsesFromObject(java.net.HttpURLConnection, java.util.List, java.lang.Object, boolean):java.util.List");
    }

    private static Response createResponseFromObject(Request request2, HttpURLConnection httpURLConnection, Object obj, boolean z, Object obj2) throws JSONException {
        Session session;
        if (obj instanceof JSONObject) {
            JSONObject jSONObject = (JSONObject) obj;
            FacebookRequestError checkResponseAndCreateError = FacebookRequestError.checkResponseAndCreateError(jSONObject, obj2, httpURLConnection);
            if (checkResponseAndCreateError != null) {
                if (checkResponseAndCreateError.getErrorCode() == INVALID_SESSION_FACEBOOK_ERROR_CODE && (session = request2.getSession()) != null) {
                    session.closeAndClearTokenInformation();
                }
                return new Response(request2, httpURLConnection, checkResponseAndCreateError);
            }
            Object stringPropertyAsJSON = Utility.getStringPropertyAsJSON(jSONObject, "body", NON_JSON_RESPONSE_PROPERTY);
            if (stringPropertyAsJSON instanceof JSONObject) {
                return new Response(request2, httpURLConnection, GraphObject.Factory.create((JSONObject) stringPropertyAsJSON), z);
            }
            if (stringPropertyAsJSON instanceof JSONArray) {
                return new Response(request2, httpURLConnection, GraphObject.Factory.createList((JSONArray) stringPropertyAsJSON, GraphObject.class), z);
            }
            obj = JSONObject.NULL;
        }
        if (obj == JSONObject.NULL) {
            return new Response(request2, httpURLConnection, (GraphObject) null, z);
        }
        throw new FacebookException("Got unexpected object type in response, class: " + obj.getClass().getSimpleName());
    }

    static List<Response> constructErrorResponses(List<Request> list, HttpURLConnection httpURLConnection, FacebookException facebookException) {
        int size = list.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            arrayList.add(new Response(list.get(i), httpURLConnection, new FacebookRequestError(httpURLConnection, facebookException)));
        }
        return arrayList;
    }
}
