package com.facebook;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import com.facebook.internal.Utility;
import com.facebook.internal.Validate;
import com.facebook.model.GraphObject;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONException;

public final class Settings {
    private static final String ANALYTICS_EVENT = "event";
    private static final String APPLICATION_FIELDS = "fields";
    private static final String ATTRIBUTION_ID_COLUMN_NAME = "aid";
    private static final Uri ATTRIBUTION_ID_CONTENT_URI = Uri.parse("content://com.facebook.katana.provider.AttributionIdProvider");
    private static final String ATTRIBUTION_KEY = "attribution";
    private static final String ATTRIBUTION_PREFERENCES = "com.facebook.sdk.attributionTracking";
    private static final int DEFAULT_CORE_POOL_SIZE = 5;
    private static final int DEFAULT_KEEP_ALIVE = 1;
    private static final int DEFAULT_MAXIMUM_POOL_SIZE = 128;
    private static final ThreadFactory DEFAULT_THREAD_FACTORY = new ThreadFactory() {
        private final AtomicInteger counter = new AtomicInteger(0);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "FacebookSdk #" + this.counter.incrementAndGet());
        }
    };
    private static final BlockingQueue<Runnable> DEFAULT_WORK_QUEUE = new LinkedBlockingQueue(10);
    private static final Object LOCK = new Object();
    private static final String MOBILE_INSTALL_EVENT = "MOBILE_APP_INSTALL";
    private static final String PUBLISH_ACTIVITY_PATH = "%s/activities";
    private static final String SUPPORTS_ATTRIBUTION = "supports_attribution";
    private static volatile Executor executor;
    private static final HashSet<LoggingBehavior> loggingBehaviors = new HashSet<>();
    private static volatile boolean shouldAutoPublishInstall;

    public static final Set<LoggingBehavior> getLoggingBehaviors() {
        Set<LoggingBehavior> unmodifiableSet;
        synchronized (loggingBehaviors) {
            unmodifiableSet = Collections.unmodifiableSet(new HashSet(loggingBehaviors));
        }
        return unmodifiableSet;
    }

    public static final void addLoggingBehavior(LoggingBehavior loggingBehavior) {
        synchronized (loggingBehaviors) {
            loggingBehaviors.add(loggingBehavior);
        }
    }

    public static final void removeLoggingBehavior(LoggingBehavior loggingBehavior) {
        synchronized (loggingBehaviors) {
            loggingBehaviors.remove(loggingBehavior);
        }
    }

    public static final void clearLoggingBehaviors() {
        synchronized (loggingBehaviors) {
            loggingBehaviors.clear();
        }
    }

    public static final boolean isLoggingBehaviorEnabled(LoggingBehavior loggingBehavior) {
        synchronized (loggingBehaviors) {
        }
        return false;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v3, types: [java.util.concurrent.Executor] */
    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.concurrent.Executor getExecutor() {
        /*
            java.lang.Object r0 = com.facebook.Settings.LOCK
            monitor-enter(r0)
            java.util.concurrent.Executor r1 = com.facebook.Settings.executor     // Catch:{ all -> 0x0024 }
            if (r1 != 0) goto L_0x0020
            java.util.concurrent.Executor r1 = getAsyncTaskExecutor()     // Catch:{ all -> 0x0024 }
            if (r1 != 0) goto L_0x001e
            java.util.concurrent.ThreadPoolExecutor r1 = new java.util.concurrent.ThreadPoolExecutor     // Catch:{ all -> 0x0024 }
            r3 = 5
            r4 = 128(0x80, float:1.794E-43)
            r5 = 1
            java.util.concurrent.TimeUnit r7 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x0024 }
            java.util.concurrent.BlockingQueue<java.lang.Runnable> r8 = com.facebook.Settings.DEFAULT_WORK_QUEUE     // Catch:{ all -> 0x0024 }
            java.util.concurrent.ThreadFactory r9 = com.facebook.Settings.DEFAULT_THREAD_FACTORY     // Catch:{ all -> 0x0024 }
            r2 = r1
            r2.<init>(r3, r4, r5, r7, r8, r9)     // Catch:{ all -> 0x0024 }
        L_0x001e:
            com.facebook.Settings.executor = r1     // Catch:{ all -> 0x0024 }
        L_0x0020:
            monitor-exit(r0)     // Catch:{ all -> 0x0024 }
            java.util.concurrent.Executor r0 = com.facebook.Settings.executor
            return r0
        L_0x0024:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0024 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.Settings.getExecutor():java.util.concurrent.Executor");
    }

    public static void setExecutor(Executor executor2) {
        Validate.notNull(executor2, "executor");
        synchronized (LOCK) {
            executor = executor2;
        }
    }

    private static Executor getAsyncTaskExecutor() {
        try {
            Field field = AsyncTask.class.getField("THREAD_POOL_EXECUTOR");
            if (field == null) {
                return null;
            }
            try {
                Object obj = field.get(null);
                if (obj != null && (obj instanceof Executor)) {
                    return (Executor) obj;
                }
                return null;
            } catch (IllegalAccessException unused) {
                return null;
            }
        } catch (NoSuchFieldException unused2) {
            return null;
        }
    }

    public static void publishInstallAsync(Context context, final String str) {
        final Context applicationContext = context.getApplicationContext();
        getExecutor().execute(new Runnable() {
            public void run() {
                Settings.publishInstallAndWait(applicationContext, str);
            }
        });
    }

    public static void setShouldAutoPublishInstall(boolean z) {
        shouldAutoPublishInstall = z;
    }

    public static boolean getShouldAutoPublishInstall() {
        return shouldAutoPublishInstall;
    }

    public static boolean publishInstallAndWait(Context context, String str) {
        if (str == null) {
            return false;
        }
        try {
            String attributionId = getAttributionId(context.getContentResolver());
            SharedPreferences sharedPreferences = context.getSharedPreferences(ATTRIBUTION_PREFERENCES, 0);
            String str2 = str + "ping";
            long j = sharedPreferences.getLong(str2, 0);
            if (j == 0 && attributionId != null) {
                Bundle bundle = new Bundle();
                bundle.putString(APPLICATION_FIELDS, SUPPORTS_ATTRIBUTION);
                Request newGraphPathRequest = Request.newGraphPathRequest(null, str, null);
                newGraphPathRequest.setParameters(bundle);
                Object property = newGraphPathRequest.executeAndWait().getGraphObject().getProperty(SUPPORTS_ATTRIBUTION);
                if (!(property instanceof Boolean)) {
                    throw new JSONException(String.format("%s contains %s instead of a Boolean", SUPPORTS_ATTRIBUTION, property));
                } else if (((Boolean) property).booleanValue()) {
                    GraphObject create = GraphObject.Factory.create();
                    create.setProperty("event", MOBILE_INSTALL_EVENT);
                    create.setProperty(ATTRIBUTION_KEY, attributionId);
                    Request.newPostRequest(null, String.format(PUBLISH_ACTIVITY_PATH, str), create, null).executeAndWait();
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    j = System.currentTimeMillis();
                    edit.putLong(str2, j);
                    edit.commit();
                }
            }
            if (j != 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            Utility.logd("Facebook-publish", e.getMessage());
            return false;
        }
    }

    public static String getAttributionId(ContentResolver contentResolver) {
        ContentResolver contentResolver2 = contentResolver;
        Cursor query = contentResolver2.query(ATTRIBUTION_ID_CONTENT_URI, new String[]{"aid"}, null, null, null);
        if (query == null || !query.moveToFirst()) {
            return null;
        }
        String string = query.getString(query.getColumnIndex("aid"));
        query.close();
        return string;
    }
}
