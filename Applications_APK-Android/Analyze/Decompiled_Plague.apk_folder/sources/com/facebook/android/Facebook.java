package com.facebook.android;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import com.facebook.AccessTokenSource;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.LegacyHelper;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.facebook.TokenCachingStrategy;
import com.facebook.internal.ServerProtocol;
import com.mopub.common.AdType;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Facebook {
    @Deprecated
    public static final String ATTRIBUTION_ID_COLUMN_NAME = "aid";
    @Deprecated
    public static final Uri ATTRIBUTION_ID_CONTENT_URI = Uri.parse("content://com.facebook.katana.provider.AttributionIdProvider");
    @Deprecated
    public static final String CANCEL_URI = "fbconnect://cancel";
    private static final int DEFAULT_AUTH_ACTIVITY_CODE = 32665;
    @Deprecated
    protected static String DIALOG_BASE_URL = "https://m.facebook.com/dialog/";
    @Deprecated
    public static final String EXPIRES = "expires_in";
    @Deprecated
    public static final String FB_APP_SIGNATURE = "30820268308201d102044a9c4610300d06092a864886f70d0101040500307a310b3009060355040613025553310b3009060355040813024341311230100603550407130950616c6f20416c746f31183016060355040a130f46616365626f6f6b204d6f62696c653111300f060355040b130846616365626f6f6b311d301b0603550403131446616365626f6f6b20436f72706f726174696f6e3020170d3039303833313231353231365a180f32303530303932353231353231365a307a310b3009060355040613025553310b3009060355040813024341311230100603550407130950616c6f20416c746f31183016060355040a130f46616365626f6f6b204d6f62696c653111300f060355040b130846616365626f6f6b311d301b0603550403131446616365626f6f6b20436f72706f726174696f6e30819f300d06092a864886f70d010101050003818d0030818902818100c207d51df8eb8c97d93ba0c8c1002c928fab00dc1b42fca5e66e99cc3023ed2d214d822bc59e8e35ddcf5f44c7ae8ade50d7e0c434f500e6c131f4a2834f987fc46406115de2018ebbb0d5a3c261bd97581ccfef76afc7135a6d59e8855ecd7eacc8f8737e794c60a761c536b72b11fac8e603f5da1a2d54aa103b8a13c0dbc10203010001300d06092a864886f70d0101040500038181005ee9be8bcbb250648d3b741290a82a1c9dc2e76a0af2f2228f1d9f9c4007529c446a70175c5a900d5141812866db46be6559e2141616483998211f4a673149fb2232a10d247663b26a9031e15f84bc1c74d141ff98a02d76f85b2c8ab2571b6469b232d8e768a7f7ca04f7abe4a775615916c07940656b58717457b42bd928a2";
    @Deprecated
    public static final int FORCE_DIALOG_AUTH = -1;
    @Deprecated
    protected static String GRAPH_BASE_URL = "https://graph.facebook.com/";
    private static final String LOGIN = "oauth";
    @Deprecated
    public static final String REDIRECT_URI = "fbconnect://success";
    @Deprecated
    protected static String RESTSERVER_URL = "https://api.facebook.com/restserver.php";
    @Deprecated
    public static final String SINGLE_SIGN_ON_DISABLED = "service_disabled";
    @Deprecated
    public static final String TOKEN = "access_token";
    private final long REFRESH_TOKEN_BARRIER = 86400000;
    /* access modifiers changed from: private */
    public long accessExpiresMillisecondsAfterEpoch = 0;
    /* access modifiers changed from: private */
    public String accessToken = null;
    /* access modifiers changed from: private */
    public long lastAccessUpdateMillisecondsAfterEpoch = 0;
    private final Object lock = new Object();
    private String mAppId;
    private Activity pendingAuthorizationActivity;
    /* access modifiers changed from: private */
    public String[] pendingAuthorizationPermissions;
    private Session pendingOpeningSession;
    /* access modifiers changed from: private */
    public volatile Session session;
    private boolean sessionInvalidated;
    private SetterTokenCachingStrategy tokenCache;
    private volatile Session userSetSession;

    public interface DialogListener {
        void onCancel();

        void onComplete(Bundle bundle);

        void onError(DialogError dialogError);

        void onFacebookError(FacebookError facebookError);
    }

    public interface ServiceListener {
        void onComplete(Bundle bundle);

        void onError(Error error);

        void onFacebookError(FacebookError facebookError);
    }

    @Deprecated
    public Facebook(String str) {
        if (str == null) {
            throw new IllegalArgumentException("You must specify your application ID when instantiating a Facebook object. See README for details.");
        }
        this.mAppId = str;
    }

    @Deprecated
    public void authorize(Activity activity, DialogListener dialogListener) {
        authorize(activity, new String[0], DEFAULT_AUTH_ACTIVITY_CODE, SessionLoginBehavior.SSO_WITH_FALLBACK, dialogListener);
    }

    @Deprecated
    public void authorize(Activity activity, String[] strArr, DialogListener dialogListener) {
        authorize(activity, strArr, DEFAULT_AUTH_ACTIVITY_CODE, SessionLoginBehavior.SSO_WITH_FALLBACK, dialogListener);
    }

    @Deprecated
    public void authorize(Activity activity, String[] strArr, int i, DialogListener dialogListener) {
        authorize(activity, strArr, i, i >= 0 ? SessionLoginBehavior.SSO_WITH_FALLBACK : SessionLoginBehavior.SUPPRESS_SSO, dialogListener);
    }

    private void authorize(Activity activity, String[] strArr, int i, SessionLoginBehavior sessionLoginBehavior, final DialogListener dialogListener) {
        String[] strArr2;
        checkUserSession("authorize");
        this.pendingOpeningSession = new Session.Builder(activity).setApplicationId(this.mAppId).setTokenCachingStrategy(getTokenCache()).build();
        this.pendingAuthorizationActivity = activity;
        boolean z = false;
        if (strArr != null) {
            strArr2 = strArr;
        } else {
            strArr2 = new String[0];
        }
        this.pendingAuthorizationPermissions = strArr2;
        Session.OpenRequest permissions = new Session.OpenRequest(activity).setCallback((Session.StatusCallback) new Session.StatusCallback() {
            public void call(Session session, SessionState sessionState, Exception exc) {
                Facebook.this.onSessionCallback(session, sessionState, exc, dialogListener);
            }
        }).setLoginBehavior(sessionLoginBehavior).setRequestCode(i).setPermissions((List<String>) Arrays.asList(strArr));
        Session session2 = this.pendingOpeningSession;
        if (this.pendingAuthorizationPermissions.length > 0) {
            z = true;
        }
        openSession(session2, permissions, z);
    }

    private void openSession(Session session2, Session.OpenRequest openRequest, boolean z) {
        openRequest.setIsLegacy(true);
        if (z) {
            session2.openForPublish(openRequest);
        } else {
            session2.openForRead(openRequest);
        }
    }

    /* access modifiers changed from: private */
    public void onSessionCallback(Session session2, SessionState sessionState, Exception exc, DialogListener dialogListener) {
        Bundle authorizationBundle = session2.getAuthorizationBundle();
        if (sessionState == SessionState.OPENED) {
            Session session3 = null;
            synchronized (this.lock) {
                if (session2 != this.session) {
                    session3 = this.session;
                    this.session = session2;
                    this.sessionInvalidated = false;
                }
            }
            if (session3 != null) {
                session3.close();
            }
            dialogListener.onComplete(authorizationBundle);
        } else if (exc == null) {
        } else {
            if (exc instanceof FacebookOperationCanceledException) {
                dialogListener.onCancel();
            } else if (!(exc instanceof FacebookAuthorizationException) || authorizationBundle == null || !authorizationBundle.containsKey(Session.WEB_VIEW_ERROR_CODE_KEY) || !authorizationBundle.containsKey(Session.WEB_VIEW_FAILING_URL_KEY)) {
                dialogListener.onFacebookError(new FacebookError(exc.getMessage()));
            } else {
                dialogListener.onError(new DialogError(exc.getMessage(), authorizationBundle.getInt(Session.WEB_VIEW_ERROR_CODE_KEY), authorizationBundle.getString(Session.WEB_VIEW_FAILING_URL_KEY)));
            }
        }
    }

    private boolean validateServiceIntent(Context context, Intent intent) {
        ResolveInfo resolveService = context.getPackageManager().resolveService(intent, 0);
        if (resolveService == null) {
            return false;
        }
        return validateAppSignatureForPackage(context, resolveService.serviceInfo.packageName);
    }

    private boolean validateAppSignatureForPackage(Context context, String str) {
        try {
            for (Signature charsString : context.getPackageManager().getPackageInfo(str, 64).signatures) {
                if (charsString.toCharsString().equals(FB_APP_SIGNATURE)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    @Deprecated
    public void authorizeCallback(int i, int i2, Intent intent) {
        checkUserSession("authorizeCallback");
        Session session2 = this.pendingOpeningSession;
        if (session2 != null && session2.onActivityResult(this.pendingAuthorizationActivity, i, i2, intent)) {
            this.pendingOpeningSession = null;
            this.pendingAuthorizationActivity = null;
            this.pendingAuthorizationPermissions = null;
        }
    }

    @Deprecated
    public boolean extendAccessToken(Context context, ServiceListener serviceListener) {
        checkUserSession("extendAccessToken");
        Intent intent = new Intent();
        intent.setClassName("com.facebook.katana", "com.facebook.katana.platform.TokenRefreshService");
        if (!validateServiceIntent(context, intent)) {
            return false;
        }
        return context.bindService(intent, new TokenRefreshServiceConnection(context, serviceListener), 1);
    }

    @Deprecated
    public boolean extendAccessTokenIfNeeded(Context context, ServiceListener serviceListener) {
        checkUserSession("extendAccessTokenIfNeeded");
        if (shouldExtendAccessToken()) {
            return extendAccessToken(context, serviceListener);
        }
        return true;
    }

    @Deprecated
    public boolean shouldExtendAccessToken() {
        checkUserSession("shouldExtendAccessToken");
        return isSessionValid() && System.currentTimeMillis() - this.lastAccessUpdateMillisecondsAfterEpoch >= 86400000;
    }

    private class TokenRefreshServiceConnection implements ServiceConnection {
        final Context applicationsContext;
        final Messenger messageReceiver = new Messenger(new TokenRefreshConnectionHandler(Facebook.this, this));
        Messenger messageSender = null;
        final ServiceListener serviceListener;

        public TokenRefreshServiceConnection(Context context, ServiceListener serviceListener2) {
            this.applicationsContext = context;
            this.serviceListener = serviceListener2;
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            this.messageSender = new Messenger(iBinder);
            refreshToken();
        }

        public void onServiceDisconnected(ComponentName componentName) {
            this.serviceListener.onError(new Error("Service disconnected"));
            this.applicationsContext.unbindService(this);
        }

        private void refreshToken() {
            Bundle bundle = new Bundle();
            bundle.putString("access_token", Facebook.this.accessToken);
            Message obtain = Message.obtain();
            obtain.setData(bundle);
            obtain.replyTo = this.messageReceiver;
            try {
                this.messageSender.send(obtain);
            } catch (RemoteException unused) {
                this.serviceListener.onError(new Error("Service connection error"));
            }
        }
    }

    private static class TokenRefreshConnectionHandler extends Handler {
        WeakReference<TokenRefreshServiceConnection> connectionWeakReference;
        WeakReference<Facebook> facebookWeakReference;

        TokenRefreshConnectionHandler(Facebook facebook, TokenRefreshServiceConnection tokenRefreshServiceConnection) {
            this.facebookWeakReference = new WeakReference<>(facebook);
            this.connectionWeakReference = new WeakReference<>(tokenRefreshServiceConnection);
        }

        public void handleMessage(Message message) {
            Facebook facebook = this.facebookWeakReference.get();
            TokenRefreshServiceConnection tokenRefreshServiceConnection = this.connectionWeakReference.get();
            if (facebook != null && tokenRefreshServiceConnection != null) {
                String string = message.getData().getString("access_token");
                long j = message.getData().getLong(Facebook.EXPIRES) * 1000;
                if (string != null) {
                    facebook.setAccessToken(string);
                    facebook.setAccessExpires(j);
                    Session access$200 = facebook.session;
                    if (access$200 != null) {
                        LegacyHelper.extendTokenCompleted(access$200, message.getData());
                    }
                    if (tokenRefreshServiceConnection.serviceListener != null) {
                        Bundle bundle = (Bundle) message.getData().clone();
                        bundle.putLong(Facebook.EXPIRES, j);
                        tokenRefreshServiceConnection.serviceListener.onComplete(bundle);
                    }
                } else if (tokenRefreshServiceConnection.serviceListener != null) {
                    String string2 = message.getData().getString("error");
                    if (message.getData().containsKey("error_code")) {
                        tokenRefreshServiceConnection.serviceListener.onFacebookError(new FacebookError(string2, null, message.getData().getInt("error_code")));
                    } else {
                        ServiceListener serviceListener = tokenRefreshServiceConnection.serviceListener;
                        if (string2 == null) {
                            string2 = "Unknown service error";
                        }
                        serviceListener.onError(new Error(string2));
                    }
                }
                if (tokenRefreshServiceConnection != null) {
                    tokenRefreshServiceConnection.applicationsContext.unbindService(tokenRefreshServiceConnection);
                }
            }
        }
    }

    @Deprecated
    public String logout(Context context) throws MalformedURLException, IOException {
        return logoutImpl(context);
    }

    /* access modifiers changed from: package-private */
    public String logoutImpl(Context context) throws MalformedURLException, IOException {
        Session session2;
        checkUserSession("logout");
        Bundle bundle = new Bundle();
        bundle.putString(TJAdUnitConstants.String.METHOD, "auth.expireSession");
        String request = request(bundle);
        long currentTimeMillis = System.currentTimeMillis();
        synchronized (this.lock) {
            session2 = this.session;
            this.session = null;
            this.accessToken = null;
            this.accessExpiresMillisecondsAfterEpoch = 0;
            this.lastAccessUpdateMillisecondsAfterEpoch = currentTimeMillis;
            this.sessionInvalidated = false;
        }
        if (session2 != null) {
            session2.closeAndClearTokenInformation();
        }
        return request;
    }

    @Deprecated
    public String request(Bundle bundle) throws MalformedURLException, IOException {
        if (bundle.containsKey(TJAdUnitConstants.String.METHOD)) {
            return requestImpl(null, bundle, "GET");
        }
        throw new IllegalArgumentException("API method must be specified. (parameters must contain key \"method\" and value). See http://developers.facebook.com/docs/reference/rest/");
    }

    @Deprecated
    public String request(String str) throws MalformedURLException, IOException {
        return requestImpl(str, new Bundle(), "GET");
    }

    @Deprecated
    public String request(String str, Bundle bundle) throws MalformedURLException, IOException {
        return requestImpl(str, bundle, "GET");
    }

    @Deprecated
    public String request(String str, Bundle bundle, String str2) throws FileNotFoundException, MalformedURLException, IOException {
        return requestImpl(str, bundle, str2);
    }

    /* access modifiers changed from: package-private */
    public String requestImpl(String str, Bundle bundle, String str2) throws FileNotFoundException, MalformedURLException, IOException {
        String str3;
        bundle.putString("format", AdType.STATIC_NATIVE);
        if (isSessionValid()) {
            bundle.putString("access_token", getAccessToken());
        }
        if (str != null) {
            str3 = GRAPH_BASE_URL + str;
        } else {
            str3 = RESTSERVER_URL;
        }
        return Util.openUrl(str3, str2, bundle);
    }

    @Deprecated
    public void dialog(Context context, String str, DialogListener dialogListener) {
        dialog(context, str, new Bundle(), dialogListener);
    }

    @Deprecated
    public void dialog(Context context, String str, Bundle bundle, DialogListener dialogListener) {
        bundle.putString("display", "touch");
        bundle.putString(ServerProtocol.DIALOG_PARAM_REDIRECT_URI, REDIRECT_URI);
        if (str.equals(LOGIN)) {
            bundle.putString("type", "user_agent");
            bundle.putString("client_id", this.mAppId);
        } else {
            bundle.putString(TapjoyConstants.TJC_APP_ID, this.mAppId);
            if (isSessionValid()) {
                bundle.putString("access_token", getAccessToken());
            }
        }
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            Util.showAlert(context, "Error", "Application requires permission to access the Internet");
        } else {
            new FbDialog(context, str, bundle, dialogListener).show();
        }
    }

    @Deprecated
    public boolean isSessionValid() {
        return getAccessToken() != null && (getAccessExpires() == 0 || System.currentTimeMillis() < getAccessExpires());
    }

    @Deprecated
    public void setSession(Session session2) {
        if (session2 == null) {
            throw new IllegalArgumentException("session cannot be null");
        }
        synchronized (this.lock) {
            this.userSetSession = session2;
        }
    }

    private void checkUserSession(String str) {
        if (this.userSetSession != null) {
            throw new UnsupportedOperationException(String.format("Cannot call %s after setSession has been called.", str));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001a, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x001b, code lost:
        if (r1 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x001d, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x001e, code lost:
        if (r2 == null) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0020, code lost:
        r1 = r2.getPermissions();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0027, code lost:
        if (r5.pendingAuthorizationPermissions == null) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0029, code lost:
        r1 = java.util.Arrays.asList(r5.pendingAuthorizationPermissions);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0030, code lost:
        r1 = java.util.Collections.emptyList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0034, code lost:
        r2 = new com.facebook.Session.Builder(r5.pendingAuthorizationActivity).setApplicationId(r5.mAppId).setTokenCachingStrategy(getTokenCache()).build();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0053, code lost:
        if (r2.getState() == com.facebook.SessionState.CREATED_TOKEN_LOADED) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0055, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0056, code lost:
        openSession(r2, new com.facebook.Session.OpenRequest(r5.pendingAuthorizationActivity).setPermissions(r1), !r1.isEmpty());
        r1 = r5.lock;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006c, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006f, code lost:
        if (r5.sessionInvalidated != false) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0073, code lost:
        if (r5.session != null) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0076, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0078, code lost:
        r0 = r5.session;
        r5.session = r2;
        r5.sessionInvalidated = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x007f, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0080, code lost:
        if (r0 == null) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0082, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0085, code lost:
        continue;
     */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.facebook.Session getSession() {
        /*
            r5 = this;
        L_0x0000:
            java.lang.Object r0 = r5.lock
            monitor-enter(r0)
            com.facebook.Session r1 = r5.userSetSession     // Catch:{ all -> 0x008f }
            if (r1 == 0) goto L_0x000b
            com.facebook.Session r1 = r5.userSetSession     // Catch:{ all -> 0x008f }
            monitor-exit(r0)     // Catch:{ all -> 0x008f }
            return r1
        L_0x000b:
            com.facebook.Session r1 = r5.session     // Catch:{ all -> 0x008f }
            if (r1 != 0) goto L_0x008b
            boolean r1 = r5.sessionInvalidated     // Catch:{ all -> 0x008f }
            if (r1 != 0) goto L_0x0015
            goto L_0x008b
        L_0x0015:
            java.lang.String r1 = r5.accessToken     // Catch:{ all -> 0x008f }
            com.facebook.Session r2 = r5.session     // Catch:{ all -> 0x008f }
            monitor-exit(r0)     // Catch:{ all -> 0x008f }
            r0 = 0
            if (r1 != 0) goto L_0x001e
            return r0
        L_0x001e:
            if (r2 == 0) goto L_0x0025
            java.util.List r1 = r2.getPermissions()
            goto L_0x0034
        L_0x0025:
            java.lang.String[] r1 = r5.pendingAuthorizationPermissions
            if (r1 == 0) goto L_0x0030
            java.lang.String[] r1 = r5.pendingAuthorizationPermissions
            java.util.List r1 = java.util.Arrays.asList(r1)
            goto L_0x0034
        L_0x0030:
            java.util.List r1 = java.util.Collections.emptyList()
        L_0x0034:
            com.facebook.Session$Builder r2 = new com.facebook.Session$Builder
            android.app.Activity r3 = r5.pendingAuthorizationActivity
            r2.<init>(r3)
            java.lang.String r3 = r5.mAppId
            com.facebook.Session$Builder r2 = r2.setApplicationId(r3)
            com.facebook.TokenCachingStrategy r3 = r5.getTokenCache()
            com.facebook.Session$Builder r2 = r2.setTokenCachingStrategy(r3)
            com.facebook.Session r2 = r2.build()
            com.facebook.SessionState r3 = r2.getState()
            com.facebook.SessionState r4 = com.facebook.SessionState.CREATED_TOKEN_LOADED
            if (r3 == r4) goto L_0x0056
            return r0
        L_0x0056:
            com.facebook.Session$OpenRequest r3 = new com.facebook.Session$OpenRequest
            android.app.Activity r4 = r5.pendingAuthorizationActivity
            r3.<init>(r4)
            com.facebook.Session$OpenRequest r3 = r3.setPermissions(r1)
            boolean r1 = r1.isEmpty()
            r1 = r1 ^ 1
            r5.openSession(r2, r3, r1)
            java.lang.Object r1 = r5.lock
            monitor-enter(r1)
            boolean r3 = r5.sessionInvalidated     // Catch:{ all -> 0x0088 }
            if (r3 != 0) goto L_0x0078
            com.facebook.Session r3 = r5.session     // Catch:{ all -> 0x0088 }
            if (r3 != 0) goto L_0x0076
            goto L_0x0078
        L_0x0076:
            r2 = r0
            goto L_0x007f
        L_0x0078:
            com.facebook.Session r0 = r5.session     // Catch:{ all -> 0x0088 }
            r5.session = r2     // Catch:{ all -> 0x0088 }
            r3 = 0
            r5.sessionInvalidated = r3     // Catch:{ all -> 0x0088 }
        L_0x007f:
            monitor-exit(r1)     // Catch:{ all -> 0x0088 }
            if (r0 == 0) goto L_0x0085
            r0.close()
        L_0x0085:
            if (r2 == 0) goto L_0x0000
            return r2
        L_0x0088:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0088 }
            throw r0
        L_0x008b:
            com.facebook.Session r1 = r5.session     // Catch:{ all -> 0x008f }
            monitor-exit(r0)     // Catch:{ all -> 0x008f }
            return r1
        L_0x008f:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x008f }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.android.Facebook.getSession():com.facebook.Session");
    }

    @Deprecated
    public String getAccessToken() {
        Session session2 = getSession();
        if (session2 != null) {
            return session2.getAccessToken();
        }
        return null;
    }

    @Deprecated
    public long getAccessExpires() {
        Session session2 = getSession();
        if (session2 != null) {
            return session2.getExpirationDate().getTime();
        }
        return this.accessExpiresMillisecondsAfterEpoch;
    }

    @Deprecated
    public long getLastAccessUpdate() {
        return this.lastAccessUpdateMillisecondsAfterEpoch;
    }

    @Deprecated
    public void setTokenFromCache(String str, long j, long j2) {
        checkUserSession("setTokenFromCache");
        synchronized (this.lock) {
            this.accessToken = str;
            this.accessExpiresMillisecondsAfterEpoch = j;
            this.lastAccessUpdateMillisecondsAfterEpoch = j2;
        }
    }

    @Deprecated
    public void setAccessToken(String str) {
        checkUserSession("setAccessToken");
        synchronized (this.lock) {
            this.accessToken = str;
            this.lastAccessUpdateMillisecondsAfterEpoch = System.currentTimeMillis();
            this.sessionInvalidated = true;
        }
    }

    @Deprecated
    public void setAccessExpires(long j) {
        checkUserSession("setAccessExpires");
        synchronized (this.lock) {
            this.accessExpiresMillisecondsAfterEpoch = j;
            this.lastAccessUpdateMillisecondsAfterEpoch = System.currentTimeMillis();
            this.sessionInvalidated = true;
        }
    }

    @Deprecated
    public void setAccessExpiresIn(String str) {
        long j;
        checkUserSession("setAccessExpiresIn");
        if (str != null) {
            if (str.equals("0")) {
                j = 0;
            } else {
                j = System.currentTimeMillis() + (Long.parseLong(str) * 1000);
            }
            setAccessExpires(j);
        }
    }

    @Deprecated
    public String getAppId() {
        return this.mAppId;
    }

    @Deprecated
    public void setAppId(String str) {
        checkUserSession("setAppId");
        synchronized (this.lock) {
            this.mAppId = str;
            this.sessionInvalidated = true;
        }
    }

    private TokenCachingStrategy getTokenCache() {
        if (this.tokenCache == null) {
            this.tokenCache = new SetterTokenCachingStrategy();
        }
        return this.tokenCache;
    }

    /* access modifiers changed from: private */
    public static String[] stringArray(List<String> list) {
        String[] strArr = new String[list.size()];
        if (list != null) {
            for (int i = 0; i < strArr.length; i++) {
                strArr[i] = list.get(i);
            }
        }
        return strArr;
    }

    /* access modifiers changed from: private */
    public static List<String> stringList(String[] strArr) {
        if (strArr != null) {
            return Arrays.asList(strArr);
        }
        return Collections.emptyList();
    }

    private class SetterTokenCachingStrategy extends TokenCachingStrategy {
        private SetterTokenCachingStrategy() {
        }

        public Bundle load() {
            Bundle bundle = new Bundle();
            if (Facebook.this.accessToken != null) {
                TokenCachingStrategy.putToken(bundle, Facebook.this.accessToken);
                TokenCachingStrategy.putExpirationMilliseconds(bundle, Facebook.this.accessExpiresMillisecondsAfterEpoch);
                TokenCachingStrategy.putPermissions(bundle, Facebook.stringList(Facebook.this.pendingAuthorizationPermissions));
                TokenCachingStrategy.putSource(bundle, AccessTokenSource.WEB_VIEW);
                TokenCachingStrategy.putLastRefreshMilliseconds(bundle, Facebook.this.lastAccessUpdateMillisecondsAfterEpoch);
            }
            return bundle;
        }

        public void save(Bundle bundle) {
            String unused = Facebook.this.accessToken = TokenCachingStrategy.getToken(bundle);
            long unused2 = Facebook.this.accessExpiresMillisecondsAfterEpoch = TokenCachingStrategy.getExpirationMilliseconds(bundle);
            String[] unused3 = Facebook.this.pendingAuthorizationPermissions = Facebook.stringArray(TokenCachingStrategy.getPermissions(bundle));
            long unused4 = Facebook.this.lastAccessUpdateMillisecondsAfterEpoch = TokenCachingStrategy.getLastRefreshMilliseconds(bundle);
        }

        public void clear() {
            String unused = Facebook.this.accessToken = null;
        }
    }

    @Deprecated
    public static String getAttributionId(ContentResolver contentResolver) {
        return Settings.getAttributionId(contentResolver);
    }

    @Deprecated
    public boolean getShouldAutoPublishInstall() {
        return Settings.getShouldAutoPublishInstall();
    }

    @Deprecated
    public void setShouldAutoPublishInstall(boolean z) {
        Settings.setShouldAutoPublishInstall(z);
    }

    @Deprecated
    public boolean publishInstall(Context context) {
        Settings.publishInstallAsync(context, this.mAppId);
        return false;
    }
}
