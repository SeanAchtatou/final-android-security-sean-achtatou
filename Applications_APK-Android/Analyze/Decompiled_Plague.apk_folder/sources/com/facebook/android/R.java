package com.facebook.android;

public final class R {

    public static final class attr {
        public static final int confirm_logout = 2130837510;
        public static final int done_button_background = 2130837511;
        public static final int done_button_text = 2130837512;
        public static final int extra_fields = 2130837513;
        public static final int fetch_user_info = 2130837514;
        public static final int is_cropped = 2130837526;
        public static final int login_text = 2130837527;
        public static final int logout_text = 2130837528;
        public static final int multi_select = 2130837529;
        public static final int preset_size = 2130837530;
        public static final int radius_in_meters = 2130837531;
        public static final int results_limit = 2130837532;
        public static final int search_text = 2130837534;
        public static final int show_pictures = 2130837535;
        public static final int show_search_box = 2130837536;
        public static final int show_title_bar = 2130837537;
        public static final int title_bar_background = 2130837538;
        public static final int title_text = 2130837539;
    }

    public static final class color {
        public static final int com_facebook_blue = 2130968576;
        public static final int com_facebook_loginview_text_color = 2130968577;
        public static final int com_facebook_usersettingsfragment_connected_shadow_color = 2130968578;
        public static final int com_facebook_usersettingsfragment_connected_text_color = 2130968579;
        public static final int com_facebook_usersettingsfragment_not_connected_text_color = 2130968580;
    }

    public static final class dimen {
        public static final int com_facebook_loginview_height = 2131034112;
        public static final int com_facebook_loginview_padding_bottom = 2131034113;
        public static final int com_facebook_loginview_padding_left = 2131034114;
        public static final int com_facebook_loginview_padding_right = 2131034115;
        public static final int com_facebook_loginview_padding_top = 2131034116;
        public static final int com_facebook_loginview_text_size = 2131034117;
        public static final int com_facebook_loginview_width = 2131034118;
        public static final int com_facebook_profilepictureview_preset_size_large = 2131034119;
        public static final int com_facebook_profilepictureview_preset_size_normal = 2131034120;
        public static final int com_facebook_profilepictureview_preset_size_small = 2131034121;
        public static final int com_facebook_usersettingsfragment_profile_picture_height = 2131034122;
        public static final int com_facebook_usersettingsfragment_profile_picture_width = 2131034123;
    }

    public static final class drawable {
        public static final int com_facebook_button_check = 2131099654;
        public static final int com_facebook_button_check_off = 2131099655;
        public static final int com_facebook_button_check_on = 2131099656;
        public static final int com_facebook_button_grey_focused = 2131099657;
        public static final int com_facebook_button_grey_normal = 2131099658;
        public static final int com_facebook_button_grey_pressed = 2131099659;
        public static final int com_facebook_close = 2131099660;
        public static final int com_facebook_icon = 2131099661;
        public static final int com_facebook_list_divider = 2131099662;
        public static final int com_facebook_list_section_header_background = 2131099663;
        public static final int com_facebook_loginbutton_blue = 2131099664;
        public static final int com_facebook_loginbutton_blue_focused = 2131099665;
        public static final int com_facebook_loginbutton_blue_normal = 2131099666;
        public static final int com_facebook_loginbutton_blue_pressed = 2131099667;
        public static final int com_facebook_loginbutton_silver = 2131099668;
        public static final int com_facebook_logo = 2131099669;
        public static final int com_facebook_picker_item_background = 2131099670;
        public static final int com_facebook_picker_list_focused = 2131099671;
        public static final int com_facebook_picker_list_longpressed = 2131099672;
        public static final int com_facebook_picker_list_pressed = 2131099673;
        public static final int com_facebook_picker_list_selector = 2131099674;
        public static final int com_facebook_picker_list_selector_background_transition = 2131099675;
        public static final int com_facebook_picker_list_selector_disabled = 2131099676;
        public static final int com_facebook_picker_top_button = 2131099677;
        public static final int com_facebook_place_default_icon = 2131099678;
        public static final int com_facebook_profile_default_icon = 2131099679;
        public static final int com_facebook_profile_picture_blank_portrait = 2131099680;
        public static final int com_facebook_profile_picture_blank_square = 2131099681;
        public static final int com_facebook_top_background = 2131099682;
        public static final int com_facebook_top_button = 2131099683;
        public static final int com_facebook_usersettingsfragment_background_gradient = 2131099684;
    }

    public static final class id {
        public static final int com_facebook_login_activity_progress_bar = 2131165201;
        public static final int com_facebook_picker_activity_circle = 2131165202;
        public static final int com_facebook_picker_checkbox = 2131165203;
        public static final int com_facebook_picker_checkbox_stub = 2131165204;
        public static final int com_facebook_picker_divider = 2131165205;
        public static final int com_facebook_picker_done_button = 2131165206;
        public static final int com_facebook_picker_image = 2131165207;
        public static final int com_facebook_picker_list_section_header = 2131165208;
        public static final int com_facebook_picker_list_view = 2131165209;
        public static final int com_facebook_picker_profile_pic_stub = 2131165210;
        public static final int com_facebook_picker_row_activity_circle = 2131165211;
        public static final int com_facebook_picker_title = 2131165212;
        public static final int com_facebook_picker_title_bar = 2131165213;
        public static final int com_facebook_picker_title_bar_stub = 2131165214;
        public static final int com_facebook_picker_top_bar = 2131165215;
        public static final int com_facebook_placepickerfragment_search_box_stub = 2131165216;
        public static final int com_facebook_usersettingsfragment_login_button = 2131165217;
        public static final int com_facebook_usersettingsfragment_logo_image = 2131165218;
        public static final int com_facebook_usersettingsfragment_profile_name = 2131165219;
        public static final int large = 2131165229;
        public static final int normal = 2131165275;
        public static final int picker_subtitle = 2131165279;
        public static final int search_box = 2131165287;
        public static final int small = 2131165288;
    }

    public static final class layout {
        public static final int com_facebook_friendpickerfragment = 2131296258;
        public static final int com_facebook_login_activity_layout = 2131296259;
        public static final int com_facebook_picker_activity_circle_row = 2131296260;
        public static final int com_facebook_picker_checkbox = 2131296261;
        public static final int com_facebook_picker_image = 2131296262;
        public static final int com_facebook_picker_list_row = 2131296263;
        public static final int com_facebook_picker_list_section_header = 2131296264;
        public static final int com_facebook_picker_search_box = 2131296265;
        public static final int com_facebook_picker_title_bar = 2131296266;
        public static final int com_facebook_picker_title_bar_stub = 2131296267;
        public static final int com_facebook_placepickerfragment = 2131296268;
        public static final int com_facebook_placepickerfragment_list_row = 2131296269;
        public static final int com_facebook_usersettingsfragment = 2131296270;
    }

    public static final class string {
        public static final int com_facebook_choose_friends = 2131427340;
        public static final int com_facebook_dialogloginactivity_ok_button = 2131427341;
        public static final int com_facebook_internet_permission_error_message = 2131427342;
        public static final int com_facebook_internet_permission_error_title = 2131427343;
        public static final int com_facebook_loading = 2131427344;
        public static final int com_facebook_loginview_cancel_action = 2131427345;
        public static final int com_facebook_loginview_log_in_button = 2131427346;
        public static final int com_facebook_loginview_log_out_action = 2131427347;
        public static final int com_facebook_loginview_log_out_button = 2131427348;
        public static final int com_facebook_loginview_logged_in_as = 2131427349;
        public static final int com_facebook_loginview_logged_in_using_facebook = 2131427350;
        public static final int com_facebook_logo_content_description = 2131427351;
        public static final int com_facebook_nearby = 2131427352;
        public static final int com_facebook_picker_done_button_text = 2131427353;
        public static final int com_facebook_placepicker_subtitle_catetory_only_format = 2131427354;
        public static final int com_facebook_placepicker_subtitle_format = 2131427355;
        public static final int com_facebook_placepicker_subtitle_were_here_only_format = 2131427356;
        public static final int com_facebook_requesterror_password_changed = 2131427357;
        public static final int com_facebook_requesterror_permissions = 2131427358;
        public static final int com_facebook_requesterror_reconnect = 2131427359;
        public static final int com_facebook_requesterror_relogin = 2131427360;
        public static final int com_facebook_requesterror_web_login = 2131427361;
        public static final int com_facebook_usersettingsfragment_log_in_button = 2131427362;
        public static final int com_facebook_usersettingsfragment_logged_in = 2131427363;
        public static final int com_facebook_usersettingsfragment_not_logged_in = 2131427364;
    }

    public static final class style {
        public static final int com_facebook_loginview_default_style = 2131492881;
        public static final int com_facebook_loginview_silver_style = 2131492882;
    }

    public static final class styleable {
        public static final int[] com_facebook_friend_picker_fragment = {com.miniclip.plagueinc.R.attr.multi_select};
        public static final int com_facebook_friend_picker_fragment_multi_select = 0;
        public static final int[] com_facebook_login_view = {com.miniclip.plagueinc.R.attr.confirm_logout, com.miniclip.plagueinc.R.attr.fetch_user_info, com.miniclip.plagueinc.R.attr.login_text, com.miniclip.plagueinc.R.attr.logout_text};
        public static final int com_facebook_login_view_confirm_logout = 0;
        public static final int com_facebook_login_view_fetch_user_info = 1;
        public static final int com_facebook_login_view_login_text = 2;
        public static final int com_facebook_login_view_logout_text = 3;
        public static final int[] com_facebook_picker_fragment = {com.miniclip.plagueinc.R.attr.done_button_background, com.miniclip.plagueinc.R.attr.done_button_text, com.miniclip.plagueinc.R.attr.extra_fields, com.miniclip.plagueinc.R.attr.show_pictures, com.miniclip.plagueinc.R.attr.show_title_bar, com.miniclip.plagueinc.R.attr.title_bar_background, com.miniclip.plagueinc.R.attr.title_text};
        public static final int com_facebook_picker_fragment_done_button_background = 0;
        public static final int com_facebook_picker_fragment_done_button_text = 1;
        public static final int com_facebook_picker_fragment_extra_fields = 2;
        public static final int com_facebook_picker_fragment_show_pictures = 3;
        public static final int com_facebook_picker_fragment_show_title_bar = 4;
        public static final int com_facebook_picker_fragment_title_bar_background = 5;
        public static final int com_facebook_picker_fragment_title_text = 6;
        public static final int[] com_facebook_place_picker_fragment = {com.miniclip.plagueinc.R.attr.radius_in_meters, com.miniclip.plagueinc.R.attr.results_limit, com.miniclip.plagueinc.R.attr.search_text, com.miniclip.plagueinc.R.attr.show_search_box};
        public static final int com_facebook_place_picker_fragment_radius_in_meters = 0;
        public static final int com_facebook_place_picker_fragment_results_limit = 1;
        public static final int com_facebook_place_picker_fragment_search_text = 2;
        public static final int com_facebook_place_picker_fragment_show_search_box = 3;
        public static final int[] com_facebook_profile_picture_view = {com.miniclip.plagueinc.R.attr.is_cropped, com.miniclip.plagueinc.R.attr.preset_size};
        public static final int com_facebook_profile_picture_view_is_cropped = 0;
        public static final int com_facebook_profile_picture_view_preset_size = 1;
    }
}
