package com.facebook;

public enum SessionDefaultAudience {
    NONE(null),
    ONLY_ME("SELF"),
    FRIENDS("ALL_FRIENDS"),
    EVERYONE("EVERYONE");
    
    private final String nativeProtocolAudience;

    private SessionDefaultAudience(String str) {
        this.nativeProtocolAudience = str;
    }

    /* access modifiers changed from: package-private */
    public String getNativeProtocolAudience() {
        return this.nativeProtocolAudience;
    }
}
