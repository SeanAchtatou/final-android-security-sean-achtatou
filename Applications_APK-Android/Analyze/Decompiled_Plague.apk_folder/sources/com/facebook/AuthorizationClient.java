package com.facebook;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;
import com.facebook.GetTokenClient;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.android.R;
import com.facebook.internal.ServerProtocol;
import com.facebook.internal.Utility;
import com.facebook.model.GraphMultiResult;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphObjectList;
import com.facebook.model.GraphUser;
import com.facebook.widget.WebDialog;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

class AuthorizationClient implements Serializable {
    private static final long serialVersionUID = 1;
    transient BackgroundProcessingListener backgroundProcessingListener;
    transient boolean checkedInternetPermission;
    transient Context context;
    AuthHandler currentHandler;
    List<AuthHandler> handlersToTry;
    transient OnCompletedListener onCompletedListener;
    AuthorizationRequest pendingRequest;
    transient StartActivityDelegate startActivityDelegate;

    interface BackgroundProcessingListener {
        void onBackgroundProcessingStarted();

        void onBackgroundProcessingStopped();
    }

    interface OnCompletedListener {
        void onCompleted(Result result);
    }

    interface StartActivityDelegate {
        Activity getActivityContext();

        void startActivityForResult(Intent intent, int i);
    }

    AuthorizationClient() {
    }

    /* access modifiers changed from: package-private */
    public void setContext(Context context2) {
        this.context = context2;
        this.startActivityDelegate = null;
    }

    /* access modifiers changed from: package-private */
    public void setContext(final Activity activity) {
        this.context = activity;
        this.startActivityDelegate = new StartActivityDelegate() {
            public void startActivityForResult(Intent intent, int i) {
                activity.startActivityForResult(intent, i);
            }

            public Activity getActivityContext() {
                return activity;
            }
        };
    }

    /* access modifiers changed from: package-private */
    public void startOrContinueAuth(AuthorizationRequest authorizationRequest) {
        if (getInProgress()) {
            continueAuth();
        } else {
            authorize(authorizationRequest);
        }
    }

    /* access modifiers changed from: package-private */
    public void authorize(AuthorizationRequest authorizationRequest) {
        if (authorizationRequest != null) {
            if (this.pendingRequest != null) {
                throw new FacebookException("Attempted to authorize while a request is pending.");
            } else if (!authorizationRequest.needsNewTokenValidation() || checkInternetPermission()) {
                this.pendingRequest = authorizationRequest;
                this.handlersToTry = getHandlerTypes(authorizationRequest);
                tryNextHandler();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void continueAuth() {
        if (this.pendingRequest == null || this.currentHandler == null) {
            throw new FacebookException("Attempted to continue authorization without a pending request.");
        } else if (this.currentHandler.needsRestart()) {
            this.currentHandler.cancel();
            tryCurrentHandler();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean getInProgress() {
        return (this.pendingRequest == null || this.currentHandler == null) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public void cancelCurrentHandler() {
        if (this.currentHandler != null) {
            this.currentHandler.cancel();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean onActivityResult(int i, int i2, Intent intent) {
        if (i == this.pendingRequest.getRequestCode()) {
            return this.currentHandler.onActivityResult(i, i2, intent);
        }
        return false;
    }

    private List<AuthHandler> getHandlerTypes(AuthorizationRequest authorizationRequest) {
        ArrayList arrayList = new ArrayList();
        SessionLoginBehavior loginBehavior = authorizationRequest.getLoginBehavior();
        if (loginBehavior.allowsKatanaAuth()) {
            if (!authorizationRequest.isLegacy()) {
                arrayList.add(new GetTokenAuthHandler());
                arrayList.add(new KatanaLoginDialogAuthHandler());
            }
            arrayList.add(new KatanaProxyAuthHandler());
        }
        if (loginBehavior.allowsWebViewAuth()) {
            arrayList.add(new WebViewAuthHandler());
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public boolean checkInternetPermission() {
        if (this.checkedInternetPermission) {
            return true;
        }
        if (checkPermission("android.permission.INTERNET") != 0) {
            complete(Result.createErrorResult(this.context.getString(R.string.com_facebook_internet_permission_error_title), this.context.getString(R.string.com_facebook_internet_permission_error_message)));
            return false;
        }
        this.checkedInternetPermission = true;
        return true;
    }

    /* access modifiers changed from: package-private */
    public void tryNextHandler() {
        while (this.handlersToTry != null && !this.handlersToTry.isEmpty()) {
            this.currentHandler = this.handlersToTry.remove(0);
            if (tryCurrentHandler()) {
                return;
            }
        }
        if (this.pendingRequest != null) {
            completeWithFailure();
        }
    }

    private void completeWithFailure() {
        complete(Result.createErrorResult("Login attempt failed.", null));
    }

    /* access modifiers changed from: package-private */
    public boolean tryCurrentHandler() {
        if (!this.currentHandler.needsInternetPermission() || checkInternetPermission()) {
            return this.currentHandler.tryAuthorize(this.pendingRequest);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void completeAndValidate(Result result) {
        if (result.token == null || !this.pendingRequest.needsNewTokenValidation()) {
            complete(result);
        } else {
            validateSameFbidAndFinish(result);
        }
    }

    /* access modifiers changed from: package-private */
    public void complete(Result result) {
        this.handlersToTry = null;
        this.currentHandler = null;
        this.pendingRequest = null;
        notifyOnCompleteListener(result);
    }

    /* access modifiers changed from: package-private */
    public OnCompletedListener getOnCompletedListener() {
        return this.onCompletedListener;
    }

    /* access modifiers changed from: package-private */
    public void setOnCompletedListener(OnCompletedListener onCompletedListener2) {
        this.onCompletedListener = onCompletedListener2;
    }

    /* access modifiers changed from: package-private */
    public BackgroundProcessingListener getBackgroundProcessingListener() {
        return this.backgroundProcessingListener;
    }

    /* access modifiers changed from: package-private */
    public void setBackgroundProcessingListener(BackgroundProcessingListener backgroundProcessingListener2) {
        this.backgroundProcessingListener = backgroundProcessingListener2;
    }

    /* access modifiers changed from: package-private */
    public StartActivityDelegate getStartActivityDelegate() {
        if (this.startActivityDelegate != null) {
            return this.startActivityDelegate;
        }
        if (this.pendingRequest != null) {
            return new StartActivityDelegate() {
                public void startActivityForResult(Intent intent, int i) {
                    AuthorizationClient.this.pendingRequest.getStartActivityDelegate().startActivityForResult(intent, i);
                }

                public Activity getActivityContext() {
                    return AuthorizationClient.this.pendingRequest.getStartActivityDelegate().getActivityContext();
                }
            };
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public int checkPermission(String str) {
        return this.context.checkCallingOrSelfPermission(str);
    }

    /* access modifiers changed from: package-private */
    public void validateSameFbidAndFinish(Result result) {
        if (result.token == null) {
            throw new FacebookException("Can't validate without a token");
        }
        RequestBatch createReauthValidationBatch = createReauthValidationBatch(result);
        notifyBackgroundProcessingStart();
        createReauthValidationBatch.executeAsync();
    }

    /* access modifiers changed from: package-private */
    public RequestBatch createReauthValidationBatch(final Result result) {
        final ArrayList arrayList = new ArrayList();
        final ArrayList arrayList2 = new ArrayList();
        String token = result.token.getToken();
        AnonymousClass3 r3 = new Request.Callback() {
            public void onCompleted(Response response) {
                try {
                    GraphUser graphUser = (GraphUser) response.getGraphObjectAs(GraphUser.class);
                    if (graphUser != null) {
                        arrayList.add(graphUser.getId());
                    }
                } catch (Exception unused) {
                }
            }
        };
        String previousAccessToken = this.pendingRequest.getPreviousAccessToken();
        Request createGetProfileIdRequest = createGetProfileIdRequest(previousAccessToken);
        createGetProfileIdRequest.setCallback(r3);
        Request createGetProfileIdRequest2 = createGetProfileIdRequest(token);
        createGetProfileIdRequest2.setCallback(r3);
        Request createGetPermissionsRequest = createGetPermissionsRequest(previousAccessToken);
        createGetPermissionsRequest.setCallback(new Request.Callback() {
            public void onCompleted(Response response) {
                GraphObjectList<GraphObject> data;
                try {
                    GraphMultiResult graphMultiResult = (GraphMultiResult) response.getGraphObjectAs(GraphMultiResult.class);
                    if (graphMultiResult != null && (data = graphMultiResult.getData()) != null && data.size() == 1) {
                        arrayList2.addAll(((GraphObject) data.get(0)).asMap().keySet());
                    }
                } catch (Exception unused) {
                }
            }
        });
        RequestBatch requestBatch = new RequestBatch(createGetProfileIdRequest, createGetProfileIdRequest2, createGetPermissionsRequest);
        requestBatch.setBatchApplicationId(this.pendingRequest.getApplicationId());
        requestBatch.addCallback(new RequestBatch.Callback() {
            public void onBatchCompleted(RequestBatch requestBatch) {
                Result result;
                try {
                    if (arrayList.size() != 2 || arrayList.get(0) == null || arrayList.get(1) == null || !((String) arrayList.get(0)).equals(arrayList.get(1))) {
                        result = Result.createErrorResult("User logged in as different Facebook user.", null);
                    } else {
                        result = Result.createTokenResult(AccessToken.createFromTokenWithRefreshedPermissions(result.token, arrayList2));
                    }
                    AuthorizationClient.this.complete(result);
                } catch (Exception e) {
                    AuthorizationClient.this.complete(Result.createErrorResult("Caught exception", e.getMessage()));
                } catch (Throwable th) {
                    AuthorizationClient.this.notifyBackgroundProcessingStop();
                    throw th;
                }
                AuthorizationClient.this.notifyBackgroundProcessingStop();
            }
        });
        return requestBatch;
    }

    /* access modifiers changed from: package-private */
    public Request createGetPermissionsRequest(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id");
        bundle.putString("access_token", str);
        return new Request(null, "me/permissions", bundle, HttpMethod.GET, null);
    }

    /* access modifiers changed from: package-private */
    public Request createGetProfileIdRequest(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id");
        bundle.putString("access_token", str);
        return new Request(null, "me", bundle, HttpMethod.GET, null);
    }

    private void notifyOnCompleteListener(Result result) {
        if (this.onCompletedListener != null) {
            this.onCompletedListener.onCompleted(result);
        }
    }

    /* access modifiers changed from: private */
    public void notifyBackgroundProcessingStart() {
        if (this.backgroundProcessingListener != null) {
            this.backgroundProcessingListener.onBackgroundProcessingStarted();
        }
    }

    /* access modifiers changed from: private */
    public void notifyBackgroundProcessingStop() {
        if (this.backgroundProcessingListener != null) {
            this.backgroundProcessingListener.onBackgroundProcessingStopped();
        }
    }

    abstract class AuthHandler implements Serializable {
        private static final long serialVersionUID = 1;

        /* access modifiers changed from: package-private */
        public void cancel() {
        }

        /* access modifiers changed from: package-private */
        public boolean needsInternetPermission() {
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean needsRestart() {
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean onActivityResult(int i, int i2, Intent intent) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public abstract boolean tryAuthorize(AuthorizationRequest authorizationRequest);

        AuthHandler() {
        }
    }

    class WebViewAuthHandler extends AuthHandler {
        private static final long serialVersionUID = 1;
        private transient WebDialog loginDialog;

        /* access modifiers changed from: package-private */
        public boolean needsInternetPermission() {
            return true;
        }

        /* access modifiers changed from: package-private */
        public boolean needsRestart() {
            return true;
        }

        WebViewAuthHandler() {
            super();
        }

        /* access modifiers changed from: package-private */
        public void cancel() {
            if (this.loginDialog != null) {
                this.loginDialog.dismiss();
                this.loginDialog = null;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean tryAuthorize(final AuthorizationRequest authorizationRequest) {
            String applicationId = authorizationRequest.getApplicationId();
            Bundle bundle = new Bundle();
            if (!Utility.isNullOrEmpty(authorizationRequest.getPermissions())) {
                bundle.putString("scope", TextUtils.join(",", authorizationRequest.getPermissions()));
            }
            Utility.clearFacebookCookies(AuthorizationClient.this.context);
            this.loginDialog = ((WebDialog.Builder) new AuthDialogBuilder(AuthorizationClient.this.getStartActivityDelegate().getActivityContext(), applicationId, bundle).setOnCompleteListener(new WebDialog.OnCompleteListener() {
                public void onComplete(Bundle bundle, FacebookException facebookException) {
                    WebViewAuthHandler.this.onWebDialogComplete(authorizationRequest, bundle, facebookException);
                }
            })).build();
            this.loginDialog.show();
            return true;
        }

        /* access modifiers changed from: package-private */
        public void onWebDialogComplete(AuthorizationRequest authorizationRequest, Bundle bundle, FacebookException facebookException) {
            Result result;
            if (bundle != null) {
                CookieSyncManager.createInstance(AuthorizationClient.this.context).sync();
                result = Result.createTokenResult(AccessToken.createFromWebBundle(authorizationRequest.getPermissions(), bundle, AccessTokenSource.WEB_VIEW));
            } else if (facebookException instanceof FacebookOperationCanceledException) {
                result = Result.createCancelResult("User canceled log in.");
            } else {
                result = Result.createErrorResult(facebookException.getMessage(), null);
            }
            AuthorizationClient.this.completeAndValidate(result);
        }
    }

    class GetTokenAuthHandler extends AuthHandler {
        private static final long serialVersionUID = 1;
        private transient GetTokenClient getTokenClient;

        GetTokenAuthHandler() {
            super();
        }

        /* access modifiers changed from: package-private */
        public void cancel() {
            if (this.getTokenClient != null) {
                this.getTokenClient.cancel();
                this.getTokenClient = null;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean tryAuthorize(final AuthorizationRequest authorizationRequest) {
            this.getTokenClient = new GetTokenClient(AuthorizationClient.this.context, authorizationRequest.getApplicationId());
            if (!this.getTokenClient.start()) {
                return false;
            }
            AuthorizationClient.this.notifyBackgroundProcessingStart();
            this.getTokenClient.setCompletedListener(new GetTokenClient.CompletedListener() {
                public void completed(Bundle bundle) {
                    GetTokenAuthHandler.this.getTokenCompleted(authorizationRequest, bundle);
                }
            });
            return true;
        }

        /* access modifiers changed from: package-private */
        public void getTokenCompleted(AuthorizationRequest authorizationRequest, Bundle bundle) {
            this.getTokenClient = null;
            AuthorizationClient.this.notifyBackgroundProcessingStop();
            if (bundle != null) {
                ArrayList<String> stringArrayList = bundle.getStringArrayList("com.facebook.platform.extra.PERMISSIONS");
                List<String> permissions = authorizationRequest.getPermissions();
                if (stringArrayList == null || (permissions != null && !stringArrayList.containsAll(permissions))) {
                    ArrayList arrayList = new ArrayList();
                    for (String next : permissions) {
                        if (!stringArrayList.contains(next)) {
                            arrayList.add(next);
                        }
                    }
                    authorizationRequest.setPermissions(arrayList);
                } else {
                    AuthorizationClient.this.completeAndValidate(Result.createTokenResult(AccessToken.createFromNativeLogin(bundle, AccessTokenSource.FACEBOOK_APPLICATION_SERVICE)));
                    return;
                }
            }
            AuthorizationClient.this.tryNextHandler();
        }
    }

    abstract class KatanaAuthHandler extends AuthHandler {
        private static final long serialVersionUID = 1;

        KatanaAuthHandler() {
            super();
        }

        /* access modifiers changed from: protected */
        public boolean tryIntent(Intent intent, int i) {
            if (intent == null) {
                return false;
            }
            try {
                AuthorizationClient.this.getStartActivityDelegate().startActivityForResult(intent, i);
                return true;
            } catch (ActivityNotFoundException unused) {
                return false;
            }
        }
    }

    class KatanaLoginDialogAuthHandler extends KatanaAuthHandler {
        private static final long serialVersionUID = 1;

        KatanaLoginDialogAuthHandler() {
            super();
        }

        /* access modifiers changed from: package-private */
        public boolean tryAuthorize(AuthorizationRequest authorizationRequest) {
            return tryIntent(NativeProtocol.createLoginDialog20121101Intent(AuthorizationClient.this.context, authorizationRequest.getApplicationId(), new ArrayList(authorizationRequest.getPermissions()), authorizationRequest.getDefaultAudience().getNativeProtocolAudience()), authorizationRequest.getRequestCode());
        }

        /* access modifiers changed from: package-private */
        public boolean onActivityResult(int i, int i2, Intent intent) {
            Result result;
            if (NativeProtocol.isServiceDisabledResult20121101(intent)) {
                AuthorizationClient.this.tryNextHandler();
                return true;
            }
            if (i2 == 0) {
                result = Result.createCancelResult(intent.getStringExtra("com.facebook.platform.status.ERROR_DESCRIPTION"));
            } else if (i2 != -1) {
                result = Result.createErrorResult("Unexpected resultCode from authorization.", null);
            } else {
                result = handleResultOk(intent);
            }
            if (result != null) {
                AuthorizationClient.this.completeAndValidate(result);
                return true;
            }
            AuthorizationClient.this.tryNextHandler();
            return true;
        }

        private Result handleResultOk(Intent intent) {
            Bundle extras = intent.getExtras();
            String string = extras.getString("com.facebook.platform.status.ERROR_TYPE");
            if (string == null) {
                return Result.createTokenResult(AccessToken.createFromNativeLogin(extras, AccessTokenSource.FACEBOOK_APPLICATION_NATIVE));
            }
            if ("ServiceDisabled".equals(string)) {
                return null;
            }
            if ("UserCanceled".equals(string)) {
                return Result.createCancelResult(null);
            }
            return Result.createErrorResult(string, extras.getString("error_description"));
        }
    }

    class KatanaProxyAuthHandler extends KatanaAuthHandler {
        private static final long serialVersionUID = 1;

        KatanaProxyAuthHandler() {
            super();
        }

        /* access modifiers changed from: package-private */
        public boolean tryAuthorize(AuthorizationRequest authorizationRequest) {
            return tryIntent(NativeProtocol.createProxyAuthIntent(AuthorizationClient.this.context, authorizationRequest.getApplicationId(), authorizationRequest.getPermissions()), authorizationRequest.getRequestCode());
        }

        /* access modifiers changed from: package-private */
        public boolean onActivityResult(int i, int i2, Intent intent) {
            Result result;
            String str = null;
            if (i2 == 0) {
                if (intent != null) {
                    str = intent.getStringExtra("error");
                }
                result = Result.createCancelResult(str);
            } else if (i2 != -1) {
                result = Result.createErrorResult("Unexpected resultCode from authorization.", null);
            } else {
                result = handleResultOk(intent);
            }
            if (result != null) {
                AuthorizationClient.this.completeAndValidate(result);
                return true;
            }
            AuthorizationClient.this.tryNextHandler();
            return true;
        }

        private Result handleResultOk(Intent intent) {
            Bundle extras = intent.getExtras();
            String string = extras.getString("error");
            if (string == null) {
                string = extras.getString("error_type");
            }
            if (string == null) {
                return Result.createTokenResult(AccessToken.createFromWebBundle(AuthorizationClient.this.pendingRequest.getPermissions(), extras, AccessTokenSource.FACEBOOK_APPLICATION_WEB));
            }
            if (ServerProtocol.errorsProxyAuthDisabled.contains(string)) {
                return null;
            }
            if (ServerProtocol.errorsUserCanceled.contains(string)) {
                return Result.createCancelResult(null);
            }
            return Result.createErrorResult(string, extras.getString("error_description"));
        }
    }

    static class AuthDialogBuilder extends WebDialog.Builder {
        private static final String OAUTH_DIALOG = "oauth";
        static final String REDIRECT_URI = "fbconnect://success";

        public AuthDialogBuilder(Context context, String str, Bundle bundle) {
            super(context, str, OAUTH_DIALOG, bundle);
        }

        public WebDialog build() {
            Bundle parameters = getParameters();
            parameters.putString(ServerProtocol.DIALOG_PARAM_REDIRECT_URI, "fbconnect://success");
            parameters.putString("client_id", getApplicationId());
            return new WebDialog(getContext(), OAUTH_DIALOG, parameters, getTheme(), getListener());
        }
    }

    static class AuthorizationRequest implements Serializable {
        private static final long serialVersionUID = 1;
        private String applicationId;
        private SessionDefaultAudience defaultAudience;
        private boolean isLegacy = false;
        private SessionLoginBehavior loginBehavior;
        private List<String> permissions;
        private String previousAccessToken;
        private int requestCode;
        private final transient StartActivityDelegate startActivityDelegate;

        AuthorizationRequest(SessionLoginBehavior sessionLoginBehavior, int i, boolean z, List<String> list, SessionDefaultAudience sessionDefaultAudience, String str, String str2, StartActivityDelegate startActivityDelegate2) {
            this.loginBehavior = sessionLoginBehavior;
            this.requestCode = i;
            this.isLegacy = z;
            this.permissions = list;
            this.defaultAudience = sessionDefaultAudience;
            this.applicationId = str;
            this.previousAccessToken = str2;
            this.startActivityDelegate = startActivityDelegate2;
        }

        /* access modifiers changed from: package-private */
        public StartActivityDelegate getStartActivityDelegate() {
            return this.startActivityDelegate;
        }

        /* access modifiers changed from: package-private */
        public List<String> getPermissions() {
            return this.permissions;
        }

        /* access modifiers changed from: package-private */
        public void setPermissions(List<String> list) {
            this.permissions = list;
        }

        /* access modifiers changed from: package-private */
        public SessionLoginBehavior getLoginBehavior() {
            return this.loginBehavior;
        }

        /* access modifiers changed from: package-private */
        public int getRequestCode() {
            return this.requestCode;
        }

        /* access modifiers changed from: package-private */
        public SessionDefaultAudience getDefaultAudience() {
            return this.defaultAudience;
        }

        /* access modifiers changed from: package-private */
        public String getApplicationId() {
            return this.applicationId;
        }

        /* access modifiers changed from: package-private */
        public boolean isLegacy() {
            return this.isLegacy;
        }

        /* access modifiers changed from: package-private */
        public void setIsLegacy(boolean z) {
            this.isLegacy = z;
        }

        /* access modifiers changed from: package-private */
        public String getPreviousAccessToken() {
            return this.previousAccessToken;
        }

        /* access modifiers changed from: package-private */
        public boolean needsNewTokenValidation() {
            return this.previousAccessToken != null && !this.isLegacy;
        }
    }

    static class Result implements Serializable {
        private static final long serialVersionUID = 1;
        final Code code;
        final String errorMessage;
        final AccessToken token;

        enum Code {
            SUCCESS,
            CANCEL,
            ERROR
        }

        private Result(Code code2, AccessToken accessToken, String str) {
            this.token = accessToken;
            this.errorMessage = str;
            this.code = code2;
        }

        static Result createTokenResult(AccessToken accessToken) {
            return new Result(Code.SUCCESS, accessToken, null);
        }

        static Result createCancelResult(String str) {
            return new Result(Code.CANCEL, null, str);
        }

        static Result createErrorResult(String str, String str2) {
            if (str2 != null) {
                str = str + ": " + str2;
            }
            return new Result(Code.ERROR, null, str);
        }
    }
}
