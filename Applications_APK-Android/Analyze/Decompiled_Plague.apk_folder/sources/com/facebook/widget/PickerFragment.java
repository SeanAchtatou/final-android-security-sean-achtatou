package com.facebook.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.AlphaAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.facebook.FacebookException;
import com.facebook.Request;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.android.R;
import com.facebook.internal.SessionTracker;
import com.facebook.model.GraphObject;
import com.facebook.widget.GraphObjectAdapter;
import com.facebook.widget.GraphObjectPagingLoader;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class PickerFragment<T extends GraphObject> extends Fragment {
    private static final String ACTIVITY_CIRCLE_SHOW_KEY = "com.facebook.android.PickerFragment.ActivityCircleShown";
    public static final String DONE_BUTTON_TEXT_BUNDLE_KEY = "com.facebook.widget.PickerFragment.DoneButtonText";
    public static final String EXTRA_FIELDS_BUNDLE_KEY = "com.facebook.widget.PickerFragment.ExtraFields";
    private static final int PROFILE_PICTURE_PREFETCH_BUFFER = 5;
    private static final String SELECTION_BUNDLE_KEY = "com.facebook.android.PickerFragment.Selection";
    public static final String SHOW_PICTURES_BUNDLE_KEY = "com.facebook.widget.PickerFragment.ShowPictures";
    public static final String SHOW_TITLE_BAR_BUNDLE_KEY = "com.facebook.widget.PickerFragment.ShowTitleBar";
    public static final String TITLE_TEXT_BUNDLE_KEY = "com.facebook.widget.PickerFragment.TitleText";
    private ProgressBar activityCircle;
    GraphObjectAdapter<T> adapter;
    private Button doneButton;
    private Drawable doneButtonBackground;
    private String doneButtonText;
    HashSet<String> extraFields = new HashSet<>();
    private GraphObjectFilter<T> filter;
    /* access modifiers changed from: private */
    public final Class<T> graphObjectClass;
    private final int layout;
    private ListView listView;
    private PickerFragment<T>.LoadingStrategy loadingStrategy;
    private OnDataChangedListener onDataChangedListener;
    /* access modifiers changed from: private */
    public OnDoneButtonClickedListener onDoneButtonClickedListener;
    /* access modifiers changed from: private */
    public OnErrorListener onErrorListener;
    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        public void onScrollStateChanged(AbsListView absListView, int i) {
        }

        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            PickerFragment.this.reprioritizeDownloads();
        }
    };
    private OnSelectionChangedListener onSelectionChangedListener;
    /* access modifiers changed from: private */
    public PickerFragment<T>.SelectionStrategy selectionStrategy;
    private SessionTracker sessionTracker;
    private boolean showPictures = true;
    private boolean showTitleBar = true;
    private Drawable titleBarBackground;
    private String titleText;
    private TextView titleTextView;

    public interface GraphObjectFilter<T> {
        boolean includeItem(T t);
    }

    public interface OnDataChangedListener {
        void onDataChanged(PickerFragment<?> pickerFragment);
    }

    public interface OnDoneButtonClickedListener {
        void onDoneButtonClicked(PickerFragment<?> pickerFragment);
    }

    public interface OnErrorListener {
        void onError(PickerFragment<?> pickerFragment, FacebookException facebookException);
    }

    public interface OnSelectionChangedListener {
        void onSelectionChanged(PickerFragment<?> pickerFragment);
    }

    /* access modifiers changed from: package-private */
    public abstract PickerFragment<T>.PickerFragmentAdapter<T> createAdapter();

    /* access modifiers changed from: package-private */
    public abstract PickerFragment<T>.LoadingStrategy createLoadingStrategy();

    /* access modifiers changed from: package-private */
    public abstract PickerFragment<T>.SelectionStrategy createSelectionStrategy();

    /* access modifiers changed from: package-private */
    public String getDefaultTitleText() {
        return null;
    }

    /* access modifiers changed from: package-private */
    public abstract Request getRequestForLoadData(Session session);

    /* access modifiers changed from: package-private */
    public void onLoadingData() {
    }

    PickerFragment(Class<T> cls, int i, Bundle bundle) {
        this.graphObjectClass = cls;
        this.layout = i;
        setPickerFragmentSettingsFromBundle(bundle);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.adapter = createAdapter();
        this.adapter.setFilter(new GraphObjectAdapter.Filter<T>() {
            public boolean includeItem(T t) {
                return PickerFragment.this.filterIncludesItem(t);
            }
        });
    }

    public void onInflate(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        super.onInflate(activity, attributeSet, bundle);
        TypedArray obtainStyledAttributes = activity.obtainStyledAttributes(attributeSet, R.styleable.com_facebook_picker_fragment);
        setShowPictures(obtainStyledAttributes.getBoolean(R.styleable.com_facebook_picker_fragment_show_pictures, this.showPictures));
        String string = obtainStyledAttributes.getString(R.styleable.com_facebook_picker_fragment_extra_fields);
        if (string != null) {
            setExtraFields(Arrays.asList(string.split(",")));
        }
        this.showTitleBar = obtainStyledAttributes.getBoolean(R.styleable.com_facebook_picker_fragment_show_title_bar, this.showTitleBar);
        this.titleText = obtainStyledAttributes.getString(R.styleable.com_facebook_picker_fragment_title_text);
        this.doneButtonText = obtainStyledAttributes.getString(R.styleable.com_facebook_picker_fragment_done_button_text);
        this.titleBarBackground = obtainStyledAttributes.getDrawable(R.styleable.com_facebook_picker_fragment_title_bar_background);
        this.doneButtonBackground = obtainStyledAttributes.getDrawable(R.styleable.com_facebook_picker_fragment_done_button_background);
        obtainStyledAttributes.recycle();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ViewGroup viewGroup2 = (ViewGroup) layoutInflater.inflate(this.layout, viewGroup, false);
        this.listView = (ListView) viewGroup2.findViewById(R.id.com_facebook_picker_list_view);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                PickerFragment.this.onListItemClick((ListView) adapterView, view, i);
            }
        });
        this.listView.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View view) {
                return false;
            }
        });
        this.listView.setOnScrollListener(this.onScrollListener);
        this.listView.setAdapter((ListAdapter) this.adapter);
        this.activityCircle = (ProgressBar) viewGroup2.findViewById(R.id.com_facebook_picker_activity_circle);
        return viewGroup2;
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        this.sessionTracker = new SessionTracker(getActivity(), new Session.StatusCallback() {
            public void call(Session session, SessionState sessionState, Exception exc) {
                if (!session.isOpened()) {
                    PickerFragment.this.clearResults();
                }
            }
        });
        setSettingsFromBundle(bundle);
        this.loadingStrategy = createLoadingStrategy();
        this.loadingStrategy.attach(this.adapter);
        this.selectionStrategy = createSelectionStrategy();
        this.selectionStrategy.readSelectionFromBundle(bundle, SELECTION_BUNDLE_KEY);
        if (this.showTitleBar) {
            inflateTitleBar((ViewGroup) getView());
        }
        if (this.activityCircle != null && bundle != null) {
            if (bundle.getBoolean(ACTIVITY_CIRCLE_SHOW_KEY, false)) {
                displayActivityCircle();
            } else {
                hideActivityCircle();
            }
        }
    }

    public void onDetach() {
        super.onDetach();
        this.listView.setOnScrollListener(null);
        this.listView.setAdapter((ListAdapter) null);
        this.loadingStrategy.detach();
        this.sessionTracker.stopTracking();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        saveSettingsToBundle(bundle);
        this.selectionStrategy.saveSelectionToBundle(bundle, SELECTION_BUNDLE_KEY);
        if (this.activityCircle != null) {
            bundle.putBoolean(ACTIVITY_CIRCLE_SHOW_KEY, this.activityCircle.getVisibility() == 0);
        }
    }

    public void setArguments(Bundle bundle) {
        super.setArguments(bundle);
        setSettingsFromBundle(bundle);
    }

    public OnDataChangedListener getOnDataChangedListener() {
        return this.onDataChangedListener;
    }

    public void setOnDataChangedListener(OnDataChangedListener onDataChangedListener2) {
        this.onDataChangedListener = onDataChangedListener2;
    }

    public OnSelectionChangedListener getOnSelectionChangedListener() {
        return this.onSelectionChangedListener;
    }

    public void setOnSelectionChangedListener(OnSelectionChangedListener onSelectionChangedListener2) {
        this.onSelectionChangedListener = onSelectionChangedListener2;
    }

    public OnDoneButtonClickedListener getOnDoneButtonClickedListener() {
        return this.onDoneButtonClickedListener;
    }

    public void setOnDoneButtonClickedListener(OnDoneButtonClickedListener onDoneButtonClickedListener2) {
        this.onDoneButtonClickedListener = onDoneButtonClickedListener2;
    }

    public OnErrorListener getOnErrorListener() {
        return this.onErrorListener;
    }

    public void setOnErrorListener(OnErrorListener onErrorListener2) {
        this.onErrorListener = onErrorListener2;
    }

    public GraphObjectFilter<T> getFilter() {
        return this.filter;
    }

    public void setFilter(GraphObjectFilter<T> graphObjectFilter) {
        this.filter = graphObjectFilter;
    }

    public Session getSession() {
        return this.sessionTracker.getSession();
    }

    public void setSession(Session session) {
        this.sessionTracker.setSession(session);
    }

    public boolean getShowPictures() {
        return this.showPictures;
    }

    public void setShowPictures(boolean z) {
        this.showPictures = z;
    }

    public Set<String> getExtraFields() {
        return new HashSet(this.extraFields);
    }

    public void setExtraFields(Collection<String> collection) {
        this.extraFields = new HashSet<>();
        if (collection != null) {
            this.extraFields.addAll(collection);
        }
    }

    public void setShowTitleBar(boolean z) {
        this.showTitleBar = z;
    }

    public boolean getShowTitleBar() {
        return this.showTitleBar;
    }

    public void setTitleText(String str) {
        this.titleText = str;
    }

    public String getTitleText() {
        if (this.titleText == null) {
            this.titleText = getDefaultTitleText();
        }
        return this.titleText;
    }

    public void setDoneButtonText(String str) {
        this.doneButtonText = str;
    }

    public String getDoneButtonText() {
        if (this.doneButtonText == null) {
            this.doneButtonText = getDefaultDoneButtonText();
        }
        return this.doneButtonText;
    }

    public void loadData(boolean z) {
        if (z || !this.loadingStrategy.isDataPresentOrLoading()) {
            loadDataSkippingRoundTripIfCached();
        }
    }

    public void setSettingsFromBundle(Bundle bundle) {
        setPickerFragmentSettingsFromBundle(bundle);
    }

    /* access modifiers changed from: package-private */
    public boolean filterIncludesItem(T t) {
        if (this.filter != null) {
            return this.filter.includeItem(t);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public List<T> getSelectedGraphObjects() {
        return this.adapter.getGraphObjectsById(this.selectionStrategy.getSelectedIds());
    }

    /* access modifiers changed from: package-private */
    public void saveSettingsToBundle(Bundle bundle) {
        bundle.putBoolean(SHOW_PICTURES_BUNDLE_KEY, this.showPictures);
        if (!this.extraFields.isEmpty()) {
            bundle.putString(EXTRA_FIELDS_BUNDLE_KEY, TextUtils.join(",", this.extraFields));
        }
        bundle.putBoolean(SHOW_TITLE_BAR_BUNDLE_KEY, this.showTitleBar);
        bundle.putString(TITLE_TEXT_BUNDLE_KEY, this.titleText);
        bundle.putString(DONE_BUTTON_TEXT_BUNDLE_KEY, this.doneButtonText);
    }

    /* access modifiers changed from: package-private */
    public String getDefaultDoneButtonText() {
        return getString(R.string.com_facebook_picker_done_button_text);
    }

    /* access modifiers changed from: package-private */
    public void displayActivityCircle() {
        if (this.activityCircle != null) {
            layoutActivityCircle();
            this.activityCircle.setVisibility(0);
        }
    }

    /* access modifiers changed from: package-private */
    public void layoutActivityCircle() {
        setAlpha(this.activityCircle, !this.adapter.isEmpty() ? 0.25f : 1.0f);
    }

    /* access modifiers changed from: package-private */
    public void hideActivityCircle() {
        if (this.activityCircle != null) {
            this.activityCircle.clearAnimation();
            this.activityCircle.setVisibility(4);
        }
    }

    /* access modifiers changed from: package-private */
    public void setSelectionStrategy(PickerFragment<T>.SelectionStrategy selectionStrategy2) {
        if (selectionStrategy2 != this.selectionStrategy) {
            this.selectionStrategy = selectionStrategy2;
            if (this.adapter != null) {
                this.adapter.notifyDataSetChanged();
            }
        }
    }

    private static void setAlpha(View view, float f) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f, f);
        alphaAnimation.setDuration(0);
        alphaAnimation.setFillAfter(true);
        view.startAnimation(alphaAnimation);
    }

    private void setPickerFragmentSettingsFromBundle(Bundle bundle) {
        if (bundle != null) {
            this.showPictures = bundle.getBoolean(SHOW_PICTURES_BUNDLE_KEY, this.showPictures);
            String string = bundle.getString(EXTRA_FIELDS_BUNDLE_KEY);
            if (string != null) {
                setExtraFields(Arrays.asList(string.split(",")));
            }
            this.showTitleBar = bundle.getBoolean(SHOW_TITLE_BAR_BUNDLE_KEY, this.showTitleBar);
            String string2 = bundle.getString(TITLE_TEXT_BUNDLE_KEY);
            if (string2 != null) {
                this.titleText = string2;
                if (this.titleTextView != null) {
                    this.titleTextView.setText(this.titleText);
                }
            }
            String string3 = bundle.getString(DONE_BUTTON_TEXT_BUNDLE_KEY);
            if (string3 != null) {
                this.doneButtonText = string3;
                if (this.doneButton != null) {
                    this.doneButton.setText(this.doneButtonText);
                }
            }
        }
    }

    private void inflateTitleBar(ViewGroup viewGroup) {
        ViewStub viewStub = (ViewStub) viewGroup.findViewById(R.id.com_facebook_picker_title_bar_stub);
        if (viewStub != null) {
            View inflate = viewStub.inflate();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            layoutParams.addRule(3, R.id.com_facebook_picker_title_bar);
            this.listView.setLayoutParams(layoutParams);
            if (this.titleBarBackground != null) {
                inflate.setBackgroundDrawable(this.titleBarBackground);
            }
            this.doneButton = (Button) viewGroup.findViewById(R.id.com_facebook_picker_done_button);
            if (this.doneButton != null) {
                this.doneButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (PickerFragment.this.onDoneButtonClickedListener != null) {
                            PickerFragment.this.onDoneButtonClickedListener.onDoneButtonClicked(PickerFragment.this);
                        }
                    }
                });
                if (getDoneButtonText() != null) {
                    this.doneButton.setText(getDoneButtonText());
                }
                if (this.doneButtonBackground != null) {
                    this.doneButton.setBackgroundDrawable(this.doneButtonBackground);
                }
            }
            this.titleTextView = (TextView) viewGroup.findViewById(R.id.com_facebook_picker_title);
            if (this.titleTextView != null && getTitleText() != null) {
                this.titleTextView.setText(getTitleText());
            }
        }
    }

    /* access modifiers changed from: private */
    public void onListItemClick(ListView listView2, View view, int i) {
        this.selectionStrategy.toggleSelection(this.adapter.getIdOfGraphObject((GraphObject) listView2.getItemAtPosition(i)));
        this.adapter.notifyDataSetChanged();
        if (this.onSelectionChangedListener != null) {
            this.onSelectionChangedListener.onSelectionChanged(this);
        }
    }

    private void loadDataSkippingRoundTripIfCached() {
        clearResults();
        Request requestForLoadData = getRequestForLoadData(getSession());
        if (requestForLoadData != null) {
            onLoadingData();
            this.loadingStrategy.startLoading(requestForLoadData);
        }
    }

    /* access modifiers changed from: private */
    public void clearResults() {
        if (this.adapter != null) {
            boolean z = !this.selectionStrategy.isEmpty();
            boolean z2 = !this.adapter.isEmpty();
            this.loadingStrategy.clearResults();
            this.selectionStrategy.clear();
            this.adapter.notifyDataSetChanged();
            if (z2 && this.onDataChangedListener != null) {
                this.onDataChangedListener.onDataChanged(this);
            }
            if (z && this.onSelectionChangedListener != null) {
                this.onSelectionChangedListener.onSelectionChanged(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void updateAdapter(SimpleGraphObjectCursor<T> simpleGraphObjectCursor) {
        int position;
        if (this.adapter != null) {
            View childAt = this.listView.getChildAt(1);
            int firstVisiblePosition = this.listView.getFirstVisiblePosition();
            if (firstVisiblePosition > 0) {
                firstVisiblePosition++;
            }
            GraphObjectAdapter.SectionAndItem<T> sectionAndItem = this.adapter.getSectionAndItem(firstVisiblePosition);
            int top = (childAt == null || sectionAndItem.getType() == GraphObjectAdapter.SectionAndItem.Type.ACTIVITY_CIRCLE) ? 0 : childAt.getTop();
            boolean changeCursor = this.adapter.changeCursor(simpleGraphObjectCursor);
            if (!(childAt == null || sectionAndItem == null || (position = this.adapter.getPosition(sectionAndItem.sectionKey, sectionAndItem.graphObject)) == -1)) {
                this.listView.setSelectionFromTop(position, top);
            }
            if (changeCursor && this.onDataChangedListener != null) {
                this.onDataChangedListener.onDataChanged(this);
            }
        }
    }

    /* access modifiers changed from: private */
    public void reprioritizeDownloads() {
        int lastVisiblePosition = this.listView.getLastVisiblePosition();
        if (lastVisiblePosition >= 0) {
            this.adapter.prioritizeViewRange(this.listView.getFirstVisiblePosition(), lastVisiblePosition, 5);
        }
    }

    abstract class LoadingStrategy {
        protected static final int CACHED_RESULT_REFRESH_DELAY = 2000;
        protected GraphObjectAdapter<T> adapter;
        protected GraphObjectPagingLoader<T> loader;

        LoadingStrategy() {
        }

        public void attach(GraphObjectAdapter<T> graphObjectAdapter) {
            this.loader = (GraphObjectPagingLoader) PickerFragment.this.getLoaderManager().initLoader(0, null, new LoaderManager.LoaderCallbacks<SimpleGraphObjectCursor<T>>() {
                public Loader<SimpleGraphObjectCursor<T>> onCreateLoader(int i, Bundle bundle) {
                    return LoadingStrategy.this.onCreateLoader();
                }

                public void onLoadFinished(Loader<SimpleGraphObjectCursor<T>> loader, SimpleGraphObjectCursor<T> simpleGraphObjectCursor) {
                    if (loader != LoadingStrategy.this.loader) {
                        throw new FacebookException("Received callback for unknown loader.");
                    }
                    LoadingStrategy.this.onLoadFinished((GraphObjectPagingLoader) loader, simpleGraphObjectCursor);
                }

                public void onLoaderReset(Loader<SimpleGraphObjectCursor<T>> loader) {
                    if (loader != LoadingStrategy.this.loader) {
                        throw new FacebookException("Received callback for unknown loader.");
                    }
                    LoadingStrategy.this.onLoadReset((GraphObjectPagingLoader) loader);
                }
            });
            this.loader.setOnErrorListener(new GraphObjectPagingLoader.OnErrorListener() {
                public void onError(FacebookException facebookException, GraphObjectPagingLoader<?> graphObjectPagingLoader) {
                    PickerFragment.this.hideActivityCircle();
                    if (PickerFragment.this.onErrorListener != null) {
                        PickerFragment.this.onErrorListener.onError(PickerFragment.this, facebookException);
                    }
                }
            });
            this.adapter = graphObjectAdapter;
            this.adapter.changeCursor(this.loader.getCursor());
        }

        public void detach() {
            this.adapter.setDataNeededListener(null);
            this.loader.setOnErrorListener(null);
            this.loader = null;
            this.adapter = null;
        }

        public void clearResults() {
            if (this.loader != null) {
                this.loader.clearResults();
            }
        }

        public void startLoading(Request request) {
            if (this.loader != null) {
                this.loader.startLoading(request, true);
                onStartLoading(this.loader, request);
            }
        }

        public boolean isDataPresentOrLoading() {
            return !this.adapter.isEmpty() || this.loader.isLoading();
        }

        /* access modifiers changed from: protected */
        public GraphObjectPagingLoader<T> onCreateLoader() {
            return new GraphObjectPagingLoader<>(PickerFragment.this.getActivity(), PickerFragment.this.graphObjectClass);
        }

        /* access modifiers changed from: protected */
        public void onStartLoading(GraphObjectPagingLoader<T> graphObjectPagingLoader, Request request) {
            PickerFragment.this.displayActivityCircle();
        }

        /* access modifiers changed from: protected */
        public void onLoadReset(GraphObjectPagingLoader<T> graphObjectPagingLoader) {
            this.adapter.changeCursor(null);
        }

        /* access modifiers changed from: protected */
        public void onLoadFinished(GraphObjectPagingLoader<T> graphObjectPagingLoader, SimpleGraphObjectCursor<T> simpleGraphObjectCursor) {
            PickerFragment.this.updateAdapter(simpleGraphObjectCursor);
        }
    }

    abstract class SelectionStrategy {
        /* access modifiers changed from: package-private */
        public abstract void clear();

        /* access modifiers changed from: package-private */
        public abstract Collection<String> getSelectedIds();

        /* access modifiers changed from: package-private */
        public abstract boolean isEmpty();

        /* access modifiers changed from: package-private */
        public abstract boolean isSelected(String str);

        /* access modifiers changed from: package-private */
        public abstract void readSelectionFromBundle(Bundle bundle, String str);

        /* access modifiers changed from: package-private */
        public abstract void saveSelectionToBundle(Bundle bundle, String str);

        /* access modifiers changed from: package-private */
        public abstract boolean shouldShowCheckBoxIfUnselected();

        /* access modifiers changed from: package-private */
        public abstract void toggleSelection(String str);

        SelectionStrategy() {
        }
    }

    class SingleSelectionStrategy extends PickerFragment<T>.SelectionStrategy {
        private String selectedId;

        /* access modifiers changed from: package-private */
        public boolean shouldShowCheckBoxIfUnselected() {
            return false;
        }

        SingleSelectionStrategy() {
            super();
        }

        public Collection<String> getSelectedIds() {
            return Arrays.asList(this.selectedId);
        }

        /* access modifiers changed from: package-private */
        public boolean isSelected(String str) {
            return (this.selectedId == null || str == null || !this.selectedId.equals(str)) ? false : true;
        }

        /* access modifiers changed from: package-private */
        public void toggleSelection(String str) {
            if (this.selectedId == null || !this.selectedId.equals(str)) {
                this.selectedId = str;
            } else {
                this.selectedId = null;
            }
        }

        /* access modifiers changed from: package-private */
        public void saveSelectionToBundle(Bundle bundle, String str) {
            if (!TextUtils.isEmpty(this.selectedId)) {
                bundle.putString(str, this.selectedId);
            }
        }

        /* access modifiers changed from: package-private */
        public void readSelectionFromBundle(Bundle bundle, String str) {
            if (bundle != null) {
                this.selectedId = bundle.getString(str);
            }
        }

        public void clear() {
            this.selectedId = null;
        }

        /* access modifiers changed from: package-private */
        public boolean isEmpty() {
            return this.selectedId == null;
        }
    }

    class MultiSelectionStrategy extends PickerFragment<T>.SelectionStrategy {
        private Set<String> selectedIds = new HashSet();

        /* access modifiers changed from: package-private */
        public boolean shouldShowCheckBoxIfUnselected() {
            return true;
        }

        MultiSelectionStrategy() {
            super();
        }

        public Collection<String> getSelectedIds() {
            return this.selectedIds;
        }

        /* access modifiers changed from: package-private */
        public boolean isSelected(String str) {
            return str != null && this.selectedIds.contains(str);
        }

        /* access modifiers changed from: package-private */
        public void toggleSelection(String str) {
            if (str == null) {
                return;
            }
            if (this.selectedIds.contains(str)) {
                this.selectedIds.remove(str);
            } else {
                this.selectedIds.add(str);
            }
        }

        /* access modifiers changed from: package-private */
        public void saveSelectionToBundle(Bundle bundle, String str) {
            if (!this.selectedIds.isEmpty()) {
                bundle.putString(str, TextUtils.join(",", this.selectedIds));
            }
        }

        /* access modifiers changed from: package-private */
        public void readSelectionFromBundle(Bundle bundle, String str) {
            String string;
            if (bundle != null && (string = bundle.getString(str)) != null) {
                String[] split = TextUtils.split(string, ",");
                this.selectedIds.clear();
                Collections.addAll(this.selectedIds, split);
            }
        }

        public void clear() {
            this.selectedIds.clear();
        }

        /* access modifiers changed from: package-private */
        public boolean isEmpty() {
            return this.selectedIds.isEmpty();
        }
    }

    abstract class PickerFragmentAdapter<U extends GraphObject> extends GraphObjectAdapter<T> {
        public PickerFragmentAdapter(Context context) {
            super(context);
        }

        /* access modifiers changed from: package-private */
        public boolean isGraphObjectSelected(String str) {
            return PickerFragment.this.selectionStrategy.isSelected(str);
        }

        /* access modifiers changed from: package-private */
        public void updateCheckboxState(CheckBox checkBox, boolean z) {
            checkBox.setChecked(z);
            checkBox.setVisibility((z || PickerFragment.this.selectionStrategy.shouldShowCheckBoxIfUnselected()) ? 0 : 8);
        }
    }
}
