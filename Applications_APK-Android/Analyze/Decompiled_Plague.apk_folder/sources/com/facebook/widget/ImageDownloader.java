package com.facebook.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import com.facebook.internal.Utility;
import com.facebook.widget.WorkQueue;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

class ImageDownloader {
    private static final int CACHE_READ_QUEUE_MAX_CONCURRENT = 2;
    private static final int DOWNLOAD_QUEUE_MAX_CONCURRENT = 8;
    private static WorkQueue cacheReadQueue = new WorkQueue(2);
    private static WorkQueue downloadQueue = new WorkQueue(8);
    private static final Handler handler = new Handler();
    private static final Map<RequestKey, DownloaderContext> pendingRequests = new HashMap();

    ImageDownloader() {
    }

    static void downloadAsync(ImageRequest imageRequest) {
        if (imageRequest != null) {
            RequestKey requestKey = new RequestKey(imageRequest.getImageUrl(), imageRequest.getCallerTag());
            synchronized (pendingRequests) {
                DownloaderContext downloaderContext = pendingRequests.get(requestKey);
                if (downloaderContext != null) {
                    downloaderContext.request = imageRequest;
                    downloaderContext.isCancelled = false;
                    downloaderContext.workItem.moveToFront();
                } else {
                    enqueueCacheRead(imageRequest, requestKey, imageRequest.isCachedRedirectAllowed());
                }
            }
        }
    }

    static boolean cancelRequest(ImageRequest imageRequest) {
        boolean z;
        RequestKey requestKey = new RequestKey(imageRequest.getImageUrl(), imageRequest.getCallerTag());
        synchronized (pendingRequests) {
            DownloaderContext downloaderContext = pendingRequests.get(requestKey);
            z = true;
            if (downloaderContext == null) {
                z = false;
            } else if (downloaderContext.workItem.cancel()) {
                pendingRequests.remove(requestKey);
            } else {
                downloaderContext.isCancelled = true;
            }
        }
        return z;
    }

    static void prioritizeRequest(ImageRequest imageRequest) {
        RequestKey requestKey = new RequestKey(imageRequest.getImageUrl(), imageRequest.getCallerTag());
        synchronized (pendingRequests) {
            DownloaderContext downloaderContext = pendingRequests.get(requestKey);
            if (downloaderContext != null) {
                downloaderContext.workItem.moveToFront();
            }
        }
    }

    private static void enqueueCacheRead(ImageRequest imageRequest, RequestKey requestKey, boolean z) {
        enqueueRequest(imageRequest, requestKey, cacheReadQueue, new CacheReadWorkItem(imageRequest.getContext(), requestKey, z));
    }

    private static void enqueueDownload(ImageRequest imageRequest, RequestKey requestKey) {
        enqueueRequest(imageRequest, requestKey, downloadQueue, new DownloadImageWorkItem(imageRequest.getContext(), requestKey));
    }

    private static void enqueueRequest(ImageRequest imageRequest, RequestKey requestKey, WorkQueue workQueue, Runnable runnable) {
        synchronized (pendingRequests) {
            DownloaderContext downloaderContext = new DownloaderContext();
            downloaderContext.request = imageRequest;
            pendingRequests.put(requestKey, downloaderContext);
            downloaderContext.workItem = workQueue.addActiveWorkItem(runnable);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000a, code lost:
        r2 = r7.request;
        r6 = r2.getCallback();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void issueResponse(com.facebook.widget.ImageDownloader.RequestKey r7, java.lang.Exception r8, android.graphics.Bitmap r9, boolean r10) {
        /*
            com.facebook.widget.ImageDownloader$DownloaderContext r7 = removePendingRequest(r7)
            if (r7 == 0) goto L_0x0020
            boolean r0 = r7.isCancelled
            if (r0 != 0) goto L_0x0020
            com.facebook.widget.ImageRequest r2 = r7.request
            com.facebook.widget.ImageRequest$Callback r6 = r2.getCallback()
            if (r6 == 0) goto L_0x0020
            android.os.Handler r7 = com.facebook.widget.ImageDownloader.handler
            com.facebook.widget.ImageDownloader$1 r0 = new com.facebook.widget.ImageDownloader$1
            r1 = r0
            r3 = r8
            r4 = r10
            r5 = r9
            r1.<init>(r2, r3, r4, r5, r6)
            r7.post(r0)
        L_0x0020:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.ImageDownloader.issueResponse(com.facebook.widget.ImageDownloader$RequestKey, java.lang.Exception, android.graphics.Bitmap, boolean):void");
    }

    /* access modifiers changed from: private */
    public static void readFromCache(RequestKey requestKey, Context context, boolean z) {
        InputStream inputStream;
        URL redirectedUrl;
        boolean z2 = false;
        if (!z || (redirectedUrl = UrlRedirectCache.getRedirectedUrl(context, requestKey.url)) == null) {
            inputStream = null;
        } else {
            inputStream = ImageResponseCache.getCachedImageStream(redirectedUrl, context);
            if (inputStream != null) {
                z2 = true;
            }
        }
        if (!z2) {
            inputStream = ImageResponseCache.getCachedImageStream(requestKey.url, context);
        }
        if (inputStream != null) {
            Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
            Utility.closeQuietly(inputStream);
            issueResponse(requestKey, null, decodeStream, z2);
            return;
        }
        DownloaderContext removePendingRequest = removePendingRequest(requestKey);
        if (removePendingRequest != null && !removePendingRequest.isCancelled) {
            enqueueDownload(removePendingRequest.request, requestKey);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v1, types: [java.lang.Exception] */
    /* JADX WARN: Type inference failed for: r4v14, types: [com.facebook.FacebookException] */
    /* JADX WARN: Type inference failed for: r0v7 */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007b, code lost:
        r4 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007c, code lost:
        r10 = null;
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x008e, code lost:
        r9 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x008e A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x000b] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:49:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void download(com.facebook.widget.ImageDownloader.RequestKey r9, android.content.Context r10) {
        /*
            r0 = 0
            r1 = 0
            r2 = 1
            java.net.URL r3 = r9.url     // Catch:{ IOException -> 0x009c, all -> 0x0093 }
            java.net.URLConnection r3 = r3.openConnection()     // Catch:{ IOException -> 0x009c, all -> 0x0093 }
            java.net.HttpURLConnection r3 = (java.net.HttpURLConnection) r3     // Catch:{ IOException -> 0x009c, all -> 0x0093 }
            r3.setInstanceFollowRedirects(r1)     // Catch:{ IOException -> 0x0090, all -> 0x008e }
            int r4 = r3.getResponseCode()     // Catch:{ IOException -> 0x0090, all -> 0x008e }
            r5 = 200(0xc8, float:2.8E-43)
            if (r4 == r5) goto L_0x007f
            switch(r4) {
                case 301: goto L_0x004b;
                case 302: goto L_0x004b;
                default: goto L_0x0019;
            }     // Catch:{ IOException -> 0x0090, all -> 0x008e }
        L_0x0019:
            java.io.InputStream r10 = r3.getErrorStream()     // Catch:{ IOException -> 0x0090, all -> 0x008e }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
            r4.<init>(r10)     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
            r5 = 128(0x80, float:1.794E-43)
            char[] r5 = new char[r5]     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
            r6.<init>()     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
        L_0x002b:
            int r7 = r5.length     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
            int r7 = r4.read(r5, r1, r7)     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
            if (r7 <= 0) goto L_0x0036
            r6.append(r5, r1, r7)     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
            goto L_0x002b
        L_0x0036:
            com.facebook.internal.Utility.closeQuietly(r4)     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
            com.facebook.FacebookException r4 = new com.facebook.FacebookException     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
            java.lang.String r5 = r6.toString()     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
            r8 = r4
            r4 = r0
            r0 = r8
            goto L_0x0087
        L_0x0046:
            r9 = move-exception
            r0 = r10
            goto L_0x0095
        L_0x0049:
            r4 = move-exception
            goto L_0x009f
        L_0x004b:
            java.lang.String r2 = "location"
            java.lang.String r2 = r3.getHeaderField(r2)     // Catch:{ IOException -> 0x007b, all -> 0x008e }
            boolean r4 = com.facebook.internal.Utility.isNullOrEmpty(r2)     // Catch:{ IOException -> 0x007b, all -> 0x008e }
            if (r4 != 0) goto L_0x0077
            java.net.URL r4 = new java.net.URL     // Catch:{ IOException -> 0x007b, all -> 0x008e }
            r4.<init>(r2)     // Catch:{ IOException -> 0x007b, all -> 0x008e }
            java.net.URL r2 = r9.url     // Catch:{ IOException -> 0x007b, all -> 0x008e }
            com.facebook.widget.UrlRedirectCache.cacheUrlRedirect(r10, r2, r4)     // Catch:{ IOException -> 0x007b, all -> 0x008e }
            com.facebook.widget.ImageDownloader$DownloaderContext r10 = removePendingRequest(r9)     // Catch:{ IOException -> 0x007b, all -> 0x008e }
            if (r10 == 0) goto L_0x0077
            boolean r2 = r10.isCancelled     // Catch:{ IOException -> 0x007b, all -> 0x008e }
            if (r2 != 0) goto L_0x0077
            com.facebook.widget.ImageRequest r10 = r10.request     // Catch:{ IOException -> 0x007b, all -> 0x008e }
            com.facebook.widget.ImageDownloader$RequestKey r2 = new com.facebook.widget.ImageDownloader$RequestKey     // Catch:{ IOException -> 0x007b, all -> 0x008e }
            java.lang.Object r5 = r9.tag     // Catch:{ IOException -> 0x007b, all -> 0x008e }
            r2.<init>(r4, r5)     // Catch:{ IOException -> 0x007b, all -> 0x008e }
            enqueueCacheRead(r10, r2, r1)     // Catch:{ IOException -> 0x007b, all -> 0x008e }
        L_0x0077:
            r10 = r0
            r4 = r10
            r2 = r1
            goto L_0x0087
        L_0x007b:
            r4 = move-exception
            r10 = r0
            r2 = r1
            goto L_0x009f
        L_0x007f:
            java.io.InputStream r10 = com.facebook.widget.ImageResponseCache.interceptAndCacheImageStream(r10, r3)     // Catch:{ IOException -> 0x0090, all -> 0x008e }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeStream(r10)     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
        L_0x0087:
            com.facebook.internal.Utility.closeQuietly(r10)
            com.facebook.internal.Utility.disconnectQuietly(r3)
            goto L_0x00a8
        L_0x008e:
            r9 = move-exception
            goto L_0x0095
        L_0x0090:
            r4 = move-exception
            r10 = r0
            goto L_0x009f
        L_0x0093:
            r9 = move-exception
            r3 = r0
        L_0x0095:
            com.facebook.internal.Utility.closeQuietly(r0)
            com.facebook.internal.Utility.disconnectQuietly(r3)
            throw r9
        L_0x009c:
            r4 = move-exception
            r10 = r0
            r3 = r10
        L_0x009f:
            com.facebook.internal.Utility.closeQuietly(r10)
            com.facebook.internal.Utility.disconnectQuietly(r3)
            r8 = r4
            r4 = r0
            r0 = r8
        L_0x00a8:
            if (r2 == 0) goto L_0x00ad
            issueResponse(r9, r0, r4, r1)
        L_0x00ad:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.ImageDownloader.download(com.facebook.widget.ImageDownloader$RequestKey, android.content.Context):void");
    }

    private static DownloaderContext removePendingRequest(RequestKey requestKey) {
        DownloaderContext remove;
        synchronized (pendingRequests) {
            remove = pendingRequests.remove(requestKey);
        }
        return remove;
    }

    private static class RequestKey {
        private static final int HASH_MULTIPLIER = 37;
        private static final int HASH_SEED = 29;
        Object tag;
        URL url;

        RequestKey(URL url2, Object obj) {
            this.url = url2;
            this.tag = obj;
        }

        public int hashCode() {
            return ((1073 + this.url.hashCode()) * 37) + this.tag.hashCode();
        }

        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof RequestKey)) {
                return false;
            }
            RequestKey requestKey = (RequestKey) obj;
            if (requestKey.url == this.url && requestKey.tag == this.tag) {
                return true;
            }
            return false;
        }
    }

    private static class DownloaderContext {
        boolean isCancelled;
        ImageRequest request;
        WorkQueue.WorkItem workItem;

        private DownloaderContext() {
        }
    }

    private static class CacheReadWorkItem implements Runnable {
        private boolean allowCachedRedirects;
        private Context context;
        private RequestKey key;

        CacheReadWorkItem(Context context2, RequestKey requestKey, boolean z) {
            this.context = context2;
            this.key = requestKey;
            this.allowCachedRedirects = z;
        }

        public void run() {
            ImageDownloader.readFromCache(this.key, this.context, this.allowCachedRedirects);
        }
    }

    private static class DownloadImageWorkItem implements Runnable {
        private Context context;
        private RequestKey key;

        DownloadImageWorkItem(Context context2, RequestKey requestKey) {
            this.context = context2;
            this.key = requestKey;
        }

        public void run() {
            ImageDownloader.download(this.key, this.context);
        }
    }
}
