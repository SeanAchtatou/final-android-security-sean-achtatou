package com.facebook.widget;

import android.content.Context;
import android.net.Uri;
import com.facebook.internal.Validate;
import java.net.MalformedURLException;
import java.net.URL;

class ImageRequest {
    private static final String HEIGHT_PARAM = "height";
    private static final String MIGRATION_PARAM = "migration_overrides";
    private static final String MIGRATION_VALUE = "{october_2012:true}";
    private static final String PROFILEPIC_URL_FORMAT = "https://graph.facebook.com/%s/picture";
    static final int UNSPECIFIED_DIMENSION = 0;
    private static final String WIDTH_PARAM = "width";
    private boolean allowCachedRedirects;
    private Callback callback;
    private Object callerTag;
    private Context context;
    private URL imageUrl;

    interface Callback {
        void onCompleted(ImageResponse imageResponse);
    }

    static URL getProfilePictureUrl(String str, int i, int i2) throws MalformedURLException {
        Validate.notNullOrEmpty(str, "userId");
        int max = Math.max(i, 0);
        int max2 = Math.max(i2, 0);
        if (max == 0 && max2 == 0) {
            throw new IllegalArgumentException("Either width or height must be greater than 0");
        }
        Uri.Builder encodedPath = new Uri.Builder().encodedPath(String.format(PROFILEPIC_URL_FORMAT, str));
        if (max2 != 0) {
            encodedPath.appendQueryParameter("height", String.valueOf(max2));
        }
        if (max != 0) {
            encodedPath.appendQueryParameter("width", String.valueOf(max));
        }
        encodedPath.appendQueryParameter(MIGRATION_PARAM, MIGRATION_VALUE);
        return new URL(encodedPath.toString());
    }

    private ImageRequest(Builder builder) {
        this.context = builder.context;
        this.imageUrl = builder.imageUrl;
        this.callback = builder.callback;
        this.allowCachedRedirects = builder.allowCachedRedirects;
        this.callerTag = builder.callerTag == null ? new Object() : builder.callerTag;
    }

    /* access modifiers changed from: package-private */
    public Context getContext() {
        return this.context;
    }

    /* access modifiers changed from: package-private */
    public URL getImageUrl() {
        return this.imageUrl;
    }

    /* access modifiers changed from: package-private */
    public Callback getCallback() {
        return this.callback;
    }

    /* access modifiers changed from: package-private */
    public boolean isCachedRedirectAllowed() {
        return this.allowCachedRedirects;
    }

    /* access modifiers changed from: package-private */
    public Object getCallerTag() {
        return this.callerTag;
    }

    static class Builder {
        /* access modifiers changed from: private */
        public boolean allowCachedRedirects;
        /* access modifiers changed from: private */
        public Callback callback;
        /* access modifiers changed from: private */
        public Object callerTag;
        /* access modifiers changed from: private */
        public Context context;
        /* access modifiers changed from: private */
        public URL imageUrl;

        Builder(Context context2, URL url) {
            Validate.notNull(url, "imageUrl");
            this.context = context2;
            this.imageUrl = url;
        }

        /* access modifiers changed from: package-private */
        public Builder setCallback(Callback callback2) {
            this.callback = callback2;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setCallerTag(Object obj) {
            this.callerTag = obj;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setAllowCachedRedirects(boolean z) {
            this.allowCachedRedirects = z;
            return this;
        }

        /* access modifiers changed from: package-private */
        public ImageRequest build() {
            return new ImageRequest(this);
        }
    }
}
