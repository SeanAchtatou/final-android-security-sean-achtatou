package com.facebook.widget;

import android.content.Context;
import com.facebook.internal.FileLruCache;
import com.facebook.internal.Utility;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

class UrlRedirectCache {
    private static final String REDIRECT_CONTENT_TAG = (TAG + "_Redirect");
    static final String TAG = "UrlRedirectCache";
    private static volatile FileLruCache urlRedirectCache;

    UrlRedirectCache() {
    }

    static synchronized FileLruCache getCache(Context context) throws IOException {
        FileLruCache fileLruCache;
        synchronized (UrlRedirectCache.class) {
            if (urlRedirectCache == null) {
                urlRedirectCache = new FileLruCache(context.getApplicationContext(), TAG, new FileLruCache.Limits());
            }
            fileLruCache = urlRedirectCache;
        }
        return fileLruCache;
    }

    static URL getRedirectedUrl(Context context, URL url) {
        InputStreamReader inputStreamReader;
        URL url2 = null;
        if (url == null) {
            return null;
        }
        String url3 = url.toString();
        try {
            FileLruCache cache = getCache(context);
            InputStreamReader inputStreamReader2 = null;
            boolean z = false;
            while (true) {
                try {
                    InputStream inputStream = cache.get(url3, REDIRECT_CONTENT_TAG);
                    if (inputStream == null) {
                        break;
                    }
                    z = true;
                    inputStreamReader = new InputStreamReader(inputStream);
                    try {
                        char[] cArr = new char[128];
                        StringBuilder sb = new StringBuilder();
                        while (true) {
                            int read = inputStreamReader.read(cArr, 0, cArr.length);
                            if (read <= 0) {
                                break;
                            }
                            sb.append(cArr, 0, read);
                        }
                        Utility.closeQuietly(inputStreamReader);
                        inputStreamReader2 = inputStreamReader;
                        url3 = sb.toString();
                    } catch (IOException | MalformedURLException unused) {
                        Utility.closeQuietly(inputStreamReader);
                        return url2;
                    } catch (Throwable th) {
                        th = th;
                        Utility.closeQuietly(inputStreamReader);
                        throw th;
                    }
                } catch (IOException | MalformedURLException unused2) {
                    inputStreamReader = inputStreamReader2;
                    Utility.closeQuietly(inputStreamReader);
                    return url2;
                } catch (Throwable th2) {
                    th = th2;
                    inputStreamReader = inputStreamReader2;
                    Utility.closeQuietly(inputStreamReader);
                    throw th;
                }
            }
            if (z) {
                url2 = new URL(url3);
            }
            Utility.closeQuietly(inputStreamReader2);
        } catch (IOException | MalformedURLException unused3) {
            inputStreamReader = null;
            Utility.closeQuietly(inputStreamReader);
            return url2;
        } catch (Throwable th3) {
            th = th3;
            inputStreamReader = null;
            Utility.closeQuietly(inputStreamReader);
            throw th;
        }
        return url2;
    }

    static void cacheUrlRedirect(Context context, URL url, URL url2) {
        if (url != null && url2 != null) {
            OutputStream outputStream = null;
            try {
                OutputStream openPutStream = getCache(context).openPutStream(url.toString(), REDIRECT_CONTENT_TAG);
                try {
                    openPutStream.write(url2.toString().getBytes());
                    Utility.closeQuietly(openPutStream);
                } catch (IOException unused) {
                    outputStream = openPutStream;
                    Utility.closeQuietly(outputStream);
                } catch (Throwable th) {
                    th = th;
                    outputStream = openPutStream;
                    Utility.closeQuietly(outputStream);
                    throw th;
                }
            } catch (IOException unused2) {
                Utility.closeQuietly(outputStream);
            } catch (Throwable th2) {
                th = th2;
                Utility.closeQuietly(outputStream);
                throw th;
            }
        }
    }
}
