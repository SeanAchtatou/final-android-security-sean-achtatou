package com.facebook.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.facebook.FacebookException;
import com.facebook.android.R;
import com.facebook.model.GraphObject;
import com.facebook.widget.ImageRequest;
import java.net.URL;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

class GraphObjectAdapter<T extends GraphObject> extends BaseAdapter implements SectionIndexer {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final int ACTIVITY_CIRCLE_VIEW_TYPE = 2;
    private static final int DISPLAY_SECTIONS_THRESHOLD = 1;
    private static final int GRAPH_OBJECT_VIEW_TYPE = 1;
    private static final int HEADER_VIEW_TYPE = 0;
    private static final String ID = "id";
    private static final int MAX_PREFETCHED_PICTURES = 20;
    private static final String NAME = "name";
    private static final String PICTURE = "picture";
    private Context context;
    private GraphObjectCursor<T> cursor;
    private DataNeededListener dataNeededListener;
    private boolean displaySections;
    private Filter<T> filter;
    private Map<String, T> graphObjectsById = new HashMap();
    private Map<String, ArrayList<T>> graphObjectsBySection = new HashMap();
    private String groupByField;
    private final LayoutInflater inflater;
    private final Map<String, ImageRequest> pendingRequests = new HashMap();
    private Map<String, ImageResponse> prefetchedPictureCache = new HashMap();
    private ArrayList<String> prefetchedProfilePictureIds = new ArrayList<>();
    private List<String> sectionKeys = new ArrayList();
    private boolean showCheckbox;
    private boolean showPicture;
    /* access modifiers changed from: private */
    public List<String> sortFields;

    public interface DataNeededListener {
        void onDataNeeded();
    }

    interface Filter<T> {
        boolean includeItem(T t);
    }

    private interface ItemPicture extends GraphObject {
        ItemPictureData getData();
    }

    private interface ItemPictureData extends GraphObject {
        String getUrl();
    }

    /* access modifiers changed from: protected */
    public CharSequence getSubTitleOfGraphObject(T t) {
        return null;
    }

    public int getViewTypeCount() {
        return 3;
    }

    public boolean hasStableIds() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean isGraphObjectSelected(String str) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void updateCheckboxState(CheckBox checkBox, boolean z) {
    }

    public static class SectionAndItem<T extends GraphObject> {
        public T graphObject;
        public String sectionKey;

        public enum Type {
            GRAPH_OBJECT,
            SECTION_HEADER,
            ACTIVITY_CIRCLE
        }

        public SectionAndItem(String str, T t) {
            this.sectionKey = str;
            this.graphObject = t;
        }

        public Type getType() {
            if (this.sectionKey == null) {
                return Type.ACTIVITY_CIRCLE;
            }
            if (this.graphObject == null) {
                return Type.SECTION_HEADER;
            }
            return Type.GRAPH_OBJECT;
        }
    }

    public GraphObjectAdapter(Context context2) {
        this.context = context2;
        this.inflater = LayoutInflater.from(context2);
    }

    public List<String> getSortFields() {
        return this.sortFields;
    }

    public void setSortFields(List<String> list) {
        this.sortFields = list;
    }

    public String getGroupByField() {
        return this.groupByField;
    }

    public void setGroupByField(String str) {
        this.groupByField = str;
    }

    public boolean getShowPicture() {
        return this.showPicture;
    }

    public void setShowPicture(boolean z) {
        this.showPicture = z;
    }

    public boolean getShowCheckbox() {
        return this.showCheckbox;
    }

    public void setShowCheckbox(boolean z) {
        this.showCheckbox = z;
    }

    public DataNeededListener getDataNeededListener() {
        return this.dataNeededListener;
    }

    public void setDataNeededListener(DataNeededListener dataNeededListener2) {
        this.dataNeededListener = dataNeededListener2;
    }

    public GraphObjectCursor<T> getCursor() {
        return this.cursor;
    }

    public boolean changeCursor(GraphObjectCursor<T> graphObjectCursor) {
        if (this.cursor == graphObjectCursor) {
            return false;
        }
        if (this.cursor != null) {
            this.cursor.close();
        }
        this.cursor = graphObjectCursor;
        rebuildAndNotify();
        return true;
    }

    public void rebuildAndNotify() {
        rebuildSections();
        notifyDataSetChanged();
    }

    public void prioritizeViewRange(int i, int i2, int i3) {
        ImageRequest imageRequest;
        if (i2 >= i) {
            for (int i4 = i2; i4 >= 0; i4--) {
                SectionAndItem sectionAndItem = getSectionAndItem(i4);
                if (!(sectionAndItem.graphObject == null || (imageRequest = this.pendingRequests.get(getIdOfGraphObject(sectionAndItem.graphObject))) == null)) {
                    ImageDownloader.prioritizeRequest(imageRequest);
                }
            }
            int min = Math.min(i3 + i2, getCount() - 1);
            ArrayList arrayList = new ArrayList();
            for (int max = Math.max(0, i - i3); max < i; max++) {
                SectionAndItem sectionAndItem2 = getSectionAndItem(max);
                if (sectionAndItem2.graphObject != null) {
                    arrayList.add(sectionAndItem2.graphObject);
                }
            }
            while (true) {
                i2++;
                if (i2 > min) {
                    break;
                }
                SectionAndItem sectionAndItem3 = getSectionAndItem(i2);
                if (sectionAndItem3.graphObject != null) {
                    arrayList.add(sectionAndItem3.graphObject);
                }
            }
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                GraphObject graphObject = (GraphObject) it.next();
                URL pictureUrlOfGraphObject = getPictureUrlOfGraphObject(graphObject);
                String idOfGraphObject = getIdOfGraphObject(graphObject);
                boolean remove = this.prefetchedProfilePictureIds.remove(idOfGraphObject);
                this.prefetchedProfilePictureIds.add(idOfGraphObject);
                if (!remove) {
                    downloadProfilePicture(idOfGraphObject, pictureUrlOfGraphObject, null);
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected java.lang.String getSectionKeyOfGraphObject(T r3) {
        /*
            r2 = this;
            java.lang.String r0 = r2.groupByField
            if (r0 == 0) goto L_0x001f
            java.lang.String r0 = r2.groupByField
            java.lang.Object r3 = r3.getProperty(r0)
            java.lang.String r3 = (java.lang.String) r3
            if (r3 == 0) goto L_0x0020
            int r0 = r3.length()
            if (r0 <= 0) goto L_0x0020
            r0 = 0
            r1 = 1
            java.lang.String r3 = r3.substring(r0, r1)
            java.lang.String r3 = r3.toUpperCase()
            goto L_0x0020
        L_0x001f:
            r3 = 0
        L_0x0020:
            if (r3 == 0) goto L_0x0023
            goto L_0x0025
        L_0x0023:
            java.lang.String r3 = ""
        L_0x0025:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.GraphObjectAdapter.getSectionKeyOfGraphObject(com.facebook.model.GraphObject):java.lang.String");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected java.lang.CharSequence getTitleOfGraphObject(T r2) {
        /*
            r1 = this;
            java.lang.String r0 = "name"
            java.lang.Object r2 = r2.getProperty(r0)
            java.lang.String r2 = (java.lang.String) r2
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.GraphObjectAdapter.getTitleOfGraphObject(com.facebook.model.GraphObject):java.lang.CharSequence");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected java.net.URL getPictureUrlOfGraphObject(T r3) {
        /*
            r2 = this;
            java.lang.String r0 = "picture"
            java.lang.Object r3 = r3.getProperty(r0)
            boolean r0 = r3 instanceof java.lang.String
            r1 = 0
            if (r0 == 0) goto L_0x000e
            java.lang.String r3 = (java.lang.String) r3
            goto L_0x002c
        L_0x000e:
            boolean r0 = r3 instanceof org.json.JSONObject
            if (r0 == 0) goto L_0x002b
            org.json.JSONObject r3 = (org.json.JSONObject) r3
            com.facebook.model.GraphObject r3 = com.facebook.model.GraphObject.Factory.create(r3)
            java.lang.Class<com.facebook.widget.GraphObjectAdapter$ItemPicture> r0 = com.facebook.widget.GraphObjectAdapter.ItemPicture.class
            com.facebook.model.GraphObject r3 = r3.cast(r0)
            com.facebook.widget.GraphObjectAdapter$ItemPicture r3 = (com.facebook.widget.GraphObjectAdapter.ItemPicture) r3
            com.facebook.widget.GraphObjectAdapter$ItemPictureData r3 = r3.getData()
            if (r3 == 0) goto L_0x002b
            java.lang.String r3 = r3.getUrl()
            goto L_0x002c
        L_0x002b:
            r3 = r1
        L_0x002c:
            if (r3 == 0) goto L_0x0034
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0034 }
            r0.<init>(r3)     // Catch:{ MalformedURLException -> 0x0034 }
            return r0
        L_0x0034:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.GraphObjectAdapter.getPictureUrlOfGraphObject(com.facebook.model.GraphObject):java.net.URL");
    }

    /* access modifiers changed from: protected */
    public View getSectionHeaderView(String str, View view, ViewGroup viewGroup) {
        TextView textView = (TextView) view;
        if (textView == null) {
            textView = (TextView) this.inflater.inflate(R.layout.com_facebook_picker_list_section_header, (ViewGroup) null);
        }
        textView.setText(str);
        return textView;
    }

    /* access modifiers changed from: protected */
    public View getGraphObjectView(T t, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = createGraphObjectView(t, view);
        }
        populateGraphObjectView(view, t);
        return view;
    }

    private View getActivityCircleView(View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.inflater.inflate(R.layout.com_facebook_picker_activity_circle_row, (ViewGroup) null);
        }
        ((ProgressBar) view.findViewById(R.id.com_facebook_picker_row_activity_circle)).setVisibility(0);
        return view;
    }

    /* access modifiers changed from: protected */
    public int getGraphObjectRowLayoutId(GraphObject graphObject) {
        return R.layout.com_facebook_picker_list_row;
    }

    /* access modifiers changed from: protected */
    public int getDefaultPicture() {
        return R.drawable.com_facebook_profile_default_icon;
    }

    /* access modifiers changed from: protected */
    public View createGraphObjectView(T t, View view) {
        View inflate = this.inflater.inflate(getGraphObjectRowLayoutId(t), (ViewGroup) null);
        ViewStub viewStub = (ViewStub) inflate.findViewById(R.id.com_facebook_picker_checkbox_stub);
        if (viewStub != null) {
            if (!getShowCheckbox()) {
                viewStub.setVisibility(8);
            } else {
                updateCheckboxState((CheckBox) viewStub.inflate(), false);
            }
        }
        ViewStub viewStub2 = (ViewStub) inflate.findViewById(R.id.com_facebook_picker_profile_pic_stub);
        if (!getShowPicture()) {
            viewStub2.setVisibility(8);
        } else {
            ((ImageView) viewStub2.inflate()).setVisibility(0);
        }
        return inflate;
    }

    /* access modifiers changed from: protected */
    public void populateGraphObjectView(View view, T t) {
        URL pictureUrlOfGraphObject;
        String idOfGraphObject = getIdOfGraphObject(t);
        view.setTag(idOfGraphObject);
        CharSequence titleOfGraphObject = getTitleOfGraphObject(t);
        TextView textView = (TextView) view.findViewById(R.id.com_facebook_picker_title);
        if (textView != null) {
            textView.setText(titleOfGraphObject, TextView.BufferType.SPANNABLE);
        }
        CharSequence subTitleOfGraphObject = getSubTitleOfGraphObject(t);
        TextView textView2 = (TextView) view.findViewById(R.id.picker_subtitle);
        if (textView2 != null) {
            if (subTitleOfGraphObject != null) {
                textView2.setText(subTitleOfGraphObject, TextView.BufferType.SPANNABLE);
                textView2.setVisibility(0);
            } else {
                textView2.setVisibility(8);
            }
        }
        if (getShowCheckbox()) {
            updateCheckboxState((CheckBox) view.findViewById(R.id.com_facebook_picker_checkbox), isGraphObjectSelected(idOfGraphObject));
        }
        if (getShowPicture() && (pictureUrlOfGraphObject = getPictureUrlOfGraphObject(t)) != null) {
            ImageView imageView = (ImageView) view.findViewById(R.id.com_facebook_picker_image);
            if (this.prefetchedPictureCache.containsKey(idOfGraphObject)) {
                ImageResponse imageResponse = this.prefetchedPictureCache.get(idOfGraphObject);
                imageView.setImageBitmap(imageResponse.getBitmap());
                imageView.setTag(imageResponse.getRequest().getImageUrl());
                return;
            }
            downloadProfilePicture(idOfGraphObject, pictureUrlOfGraphObject, imageView);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    java.lang.String getIdOfGraphObject(T r3) {
        /*
            r2 = this;
            java.util.Map r0 = r3.asMap()
            java.lang.String r1 = "id"
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x0019
            java.lang.String r0 = "id"
            java.lang.Object r3 = r3.getProperty(r0)
            boolean r0 = r3 instanceof java.lang.String
            if (r0 == 0) goto L_0x0019
            java.lang.String r3 = (java.lang.String) r3
            return r3
        L_0x0019:
            com.facebook.FacebookException r3 = new com.facebook.FacebookException
            java.lang.String r0 = "Received an object without an ID."
            r3.<init>(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.GraphObjectAdapter.getIdOfGraphObject(com.facebook.model.GraphObject):java.lang.String");
    }

    /* access modifiers changed from: package-private */
    public boolean filterIncludesItem(T t) {
        return this.filter == null || this.filter.includeItem(t);
    }

    /* access modifiers changed from: package-private */
    public Filter<T> getFilter() {
        return this.filter;
    }

    /* access modifiers changed from: package-private */
    public void setFilter(Filter<T> filter2) {
        this.filter = filter2;
    }

    /* access modifiers changed from: package-private */
    public String getPictureFieldSpecifier() {
        ImageView imageView = (ImageView) createGraphObjectView(null, null).findViewById(R.id.com_facebook_picker_image);
        if (imageView == null) {
            return null;
        }
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        return String.format("picture.height(%d).width(%d)", Integer.valueOf(layoutParams.height), Integer.valueOf(layoutParams.width));
    }

    private boolean shouldShowActivityCircleCell() {
        return this.cursor != null && this.cursor.areMoreObjectsAvailable() && this.dataNeededListener != null && !isEmpty();
    }

    private void rebuildSections() {
        this.sectionKeys = new ArrayList();
        this.graphObjectsBySection = new HashMap();
        this.graphObjectsById = new HashMap();
        boolean z = false;
        this.displaySections = false;
        if (this.cursor != null && this.cursor.getCount() != 0) {
            this.cursor.moveToFirst();
            int i = 0;
            do {
                T graphObject = this.cursor.getGraphObject();
                if (filterIncludesItem(graphObject)) {
                    i++;
                    String sectionKeyOfGraphObject = getSectionKeyOfGraphObject(graphObject);
                    if (!this.graphObjectsBySection.containsKey(sectionKeyOfGraphObject)) {
                        this.sectionKeys.add(sectionKeyOfGraphObject);
                        this.graphObjectsBySection.put(sectionKeyOfGraphObject, new ArrayList());
                    }
                    this.graphObjectsBySection.get(sectionKeyOfGraphObject).add(graphObject);
                    this.graphObjectsById.put(getIdOfGraphObject(graphObject), graphObject);
                }
            } while (this.cursor.moveToNext());
            if (this.sortFields != null) {
                final Collator instance = Collator.getInstance();
                for (ArrayList<T> sort : this.graphObjectsBySection.values()) {
                    Collections.sort(sort, new Comparator<GraphObject>() {
                        public int compare(GraphObject graphObject, GraphObject graphObject2) {
                            return GraphObjectAdapter.compareGraphObjects(graphObject, graphObject2, GraphObjectAdapter.this.sortFields, instance);
                        }
                    });
                }
            }
            Collections.sort(this.sectionKeys, Collator.getInstance());
            if (this.sectionKeys.size() > 1 && i > 1) {
                z = true;
            }
            this.displaySections = z;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v7, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: com.facebook.model.GraphObject} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: com.facebook.model.GraphObject} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.widget.GraphObjectAdapter.SectionAndItem<T> getSectionAndItem(int r6) {
        /*
            r5 = this;
            java.util.List<java.lang.String> r0 = r5.sectionKeys
            int r0 = r0.size()
            r1 = 0
            if (r0 != 0) goto L_0x000a
            return r1
        L_0x000a:
            boolean r0 = r5.displaySections
            if (r0 != 0) goto L_0x003e
            java.util.List<java.lang.String> r0 = r5.sectionKeys
            r2 = 0
            java.lang.Object r0 = r0.get(r2)
            java.lang.String r0 = (java.lang.String) r0
            java.util.Map<java.lang.String, java.util.ArrayList<T>> r2 = r5.graphObjectsBySection
            java.lang.Object r2 = r2.get(r0)
            java.util.List r2 = (java.util.List) r2
            if (r6 < 0) goto L_0x0038
            int r2 = r2.size()
            if (r6 >= r2) goto L_0x0038
            java.util.Map<java.lang.String, java.util.ArrayList<T>> r1 = r5.graphObjectsBySection
            java.lang.Object r1 = r1.get(r0)
            java.util.ArrayList r1 = (java.util.ArrayList) r1
            java.lang.Object r6 = r1.get(r6)
            r1 = r6
            com.facebook.model.GraphObject r1 = (com.facebook.model.GraphObject) r1
            r2 = r0
            goto L_0x0073
        L_0x0038:
            com.facebook.widget.GraphObjectAdapter$SectionAndItem r6 = new com.facebook.widget.GraphObjectAdapter$SectionAndItem
            r6.<init>(r1, r1)
            return r6
        L_0x003e:
            java.util.List<java.lang.String> r0 = r5.sectionKeys
            java.util.Iterator r0 = r0.iterator()
        L_0x0044:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x0072
            java.lang.Object r2 = r0.next()
            java.lang.String r2 = (java.lang.String) r2
            int r3 = r6 + -1
            if (r6 != 0) goto L_0x0055
            goto L_0x0073
        L_0x0055:
            java.util.Map<java.lang.String, java.util.ArrayList<T>> r6 = r5.graphObjectsBySection
            java.lang.Object r6 = r6.get(r2)
            java.util.List r6 = (java.util.List) r6
            int r4 = r6.size()
            if (r3 >= r4) goto L_0x006b
            java.lang.Object r6 = r6.get(r3)
            r1 = r6
            com.facebook.model.GraphObject r1 = (com.facebook.model.GraphObject) r1
            goto L_0x0073
        L_0x006b:
            int r6 = r6.size()
            int r6 = r3 - r6
            goto L_0x0044
        L_0x0072:
            r2 = r1
        L_0x0073:
            if (r2 == 0) goto L_0x007b
            com.facebook.widget.GraphObjectAdapter$SectionAndItem r6 = new com.facebook.widget.GraphObjectAdapter$SectionAndItem
            r6.<init>(r2, r1)
            return r6
        L_0x007b:
            java.lang.IndexOutOfBoundsException r6 = new java.lang.IndexOutOfBoundsException
            java.lang.String r0 = "position"
            r6.<init>(r0)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.GraphObjectAdapter.getSectionAndItem(int):com.facebook.widget.GraphObjectAdapter$SectionAndItem");
    }

    /* access modifiers changed from: package-private */
    public int getPosition(String str, T t) {
        Iterator<String> it = this.sectionKeys.iterator();
        boolean z = false;
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            String next = it.next();
            if (this.displaySections) {
                i++;
            }
            if (next.equals(str)) {
                z = true;
                break;
            }
            i += this.graphObjectsBySection.get(next).size();
        }
        if (!z) {
            return -1;
        }
        if (t == null) {
            return i - (this.displaySections ? 1 : 0);
        }
        Iterator it2 = this.graphObjectsBySection.get(str).iterator();
        while (it2.hasNext()) {
            if (GraphObject.Factory.hasSameId((GraphObject) it2.next(), t)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public boolean isEmpty() {
        return this.sectionKeys.size() == 0;
    }

    public int getCount() {
        int i = 0;
        if (this.sectionKeys.size() == 0) {
            return 0;
        }
        if (this.displaySections) {
            i = this.sectionKeys.size();
        }
        for (ArrayList<T> size : this.graphObjectsBySection.values()) {
            i += size.size();
        }
        return shouldShowActivityCircleCell() ? i + 1 : i;
    }

    public boolean areAllItemsEnabled() {
        return this.displaySections;
    }

    public boolean isEnabled(int i) {
        return getSectionAndItem(i).getType() == SectionAndItem.Type.GRAPH_OBJECT;
    }

    public Object getItem(int i) {
        SectionAndItem sectionAndItem = getSectionAndItem(i);
        if (sectionAndItem.getType() == SectionAndItem.Type.GRAPH_OBJECT) {
            return sectionAndItem.graphObject;
        }
        return null;
    }

    public long getItemId(int i) {
        String idOfGraphObject;
        SectionAndItem sectionAndItem = getSectionAndItem(i);
        if (sectionAndItem == null || sectionAndItem.graphObject == null || (idOfGraphObject = getIdOfGraphObject(sectionAndItem.graphObject)) == null) {
            return 0;
        }
        return Long.parseLong(idOfGraphObject);
    }

    public int getItemViewType(int i) {
        SectionAndItem sectionAndItem = getSectionAndItem(i);
        switch (sectionAndItem.getType()) {
            case SECTION_HEADER:
                return 0;
            case GRAPH_OBJECT:
                return 1;
            case ACTIVITY_CIRCLE:
                return 2;
            default:
                throw new FacebookException("Unexpected type of section and item.");
        }
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        SectionAndItem sectionAndItem = getSectionAndItem(i);
        switch (sectionAndItem.getType()) {
            case SECTION_HEADER:
                return getSectionHeaderView(sectionAndItem.sectionKey, view, viewGroup);
            case GRAPH_OBJECT:
                return getGraphObjectView(sectionAndItem.graphObject, view, viewGroup);
            case ACTIVITY_CIRCLE:
                this.dataNeededListener.onDataNeeded();
                return getActivityCircleView(view, viewGroup);
            default:
                throw new FacebookException("Unexpected type of section and item.");
        }
    }

    public Object[] getSections() {
        if (this.displaySections) {
            return this.sectionKeys.toArray();
        }
        return new Object[0];
    }

    public int getPositionForSection(int i) {
        int max;
        if (!this.displaySections || (max = Math.max(0, Math.min(i, this.sectionKeys.size() - 1))) >= this.sectionKeys.size()) {
            return 0;
        }
        return getPosition(this.sectionKeys.get(max), null);
    }

    public int getSectionForPosition(int i) {
        SectionAndItem sectionAndItem = getSectionAndItem(i);
        if (sectionAndItem == null || sectionAndItem.getType() == SectionAndItem.Type.ACTIVITY_CIRCLE) {
            return 0;
        }
        return Math.max(0, Math.min(this.sectionKeys.indexOf(sectionAndItem.sectionKey), this.sectionKeys.size() - 1));
    }

    public List<T> getGraphObjectsById(Collection<String> collection) {
        HashSet<String> hashSet = new HashSet<>();
        hashSet.addAll(collection);
        ArrayList arrayList = new ArrayList(hashSet.size());
        for (String str : hashSet) {
            GraphObject graphObject = (GraphObject) this.graphObjectsById.get(str);
            if (graphObject != null) {
                arrayList.add(graphObject);
            }
        }
        return arrayList;
    }

    private void downloadProfilePicture(final String str, URL url, final ImageView imageView) {
        if (url != null) {
            boolean z = imageView == null;
            if (z || !url.equals(imageView.getTag())) {
                if (!z) {
                    imageView.setTag(str);
                    imageView.setImageResource(getDefaultPicture());
                }
                ImageRequest build = new ImageRequest.Builder(this.context.getApplicationContext(), url).setCallerTag(this).setCallback(new ImageRequest.Callback() {
                    public void onCompleted(ImageResponse imageResponse) {
                        GraphObjectAdapter.this.processImageResponse(imageResponse, str, imageView);
                    }
                }).build();
                this.pendingRequests.put(str, build);
                ImageDownloader.downloadAsync(build);
            }
        }
    }

    /* access modifiers changed from: private */
    public void processImageResponse(ImageResponse imageResponse, String str, ImageView imageView) {
        this.pendingRequests.remove(str);
        if (imageView == null) {
            if (imageResponse.getBitmap() != null) {
                if (this.prefetchedPictureCache.size() >= 20) {
                    this.prefetchedPictureCache.remove(this.prefetchedProfilePictureIds.remove(0));
                }
                this.prefetchedPictureCache.put(str, imageResponse);
            }
        } else if (imageView != null && str.equals(imageView.getTag())) {
            Exception error = imageResponse.getError();
            Bitmap bitmap = imageResponse.getBitmap();
            if (error == null && bitmap != null) {
                imageView.setImageBitmap(bitmap);
                imageView.setTag(imageResponse.getRequest().getImageUrl());
            }
        }
    }

    /* access modifiers changed from: private */
    public static int compareGraphObjects(GraphObject graphObject, GraphObject graphObject2, Collection<String> collection, Collator collator) {
        for (String next : collection) {
            String str = (String) graphObject.getProperty(next);
            String str2 = (String) graphObject2.getProperty(next);
            if (str != null && str2 != null) {
                int compare = collator.compare(str, str2);
                if (compare != 0) {
                    return compare;
                }
            } else if (str != null || str2 != null) {
                return str == null ? -1 : 1;
            }
        }
        return 0;
    }
}
