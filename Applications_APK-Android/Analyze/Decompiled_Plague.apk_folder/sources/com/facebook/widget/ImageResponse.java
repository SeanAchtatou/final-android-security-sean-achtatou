package com.facebook.widget;

import android.graphics.Bitmap;

class ImageResponse {
    private Bitmap bitmap;
    private Exception error;
    private boolean isCachedRedirect;
    private ImageRequest request;

    ImageResponse(ImageRequest imageRequest, Exception exc, boolean z, Bitmap bitmap2) {
        this.request = imageRequest;
        this.error = exc;
        this.bitmap = bitmap2;
        this.isCachedRedirect = z;
    }

    /* access modifiers changed from: package-private */
    public ImageRequest getRequest() {
        return this.request;
    }

    /* access modifiers changed from: package-private */
    public Exception getError() {
        return this.error;
    }

    /* access modifiers changed from: package-private */
    public Bitmap getBitmap() {
        return this.bitmap;
    }

    /* access modifiers changed from: package-private */
    public boolean isCachedRedirect() {
        return this.isCachedRedirect;
    }
}
