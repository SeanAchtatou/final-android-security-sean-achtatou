package com.facebook;

import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.RequestBatch;
import com.facebook.internal.Logger;
import com.facebook.internal.ServerProtocol;
import com.facebook.internal.Utility;
import com.facebook.internal.Validate;
import com.facebook.model.GraphMultiResult;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphObjectList;
import com.facebook.model.GraphPlace;
import com.facebook.model.GraphUser;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.games.request.Requests;
import com.mopub.common.MoPubBrowser;
import com.tapjoy.TJAdUnitConstants;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Request {
    private static final String ACCESS_TOKEN_PARAM = "access_token";
    private static final String ATTACHED_FILES_PARAM = "attached_files";
    private static final String ATTACHMENT_FILENAME_PREFIX = "file";
    private static final String BATCH_APP_ID_PARAM = "batch_app_id";
    private static final String BATCH_BODY_PARAM = "body";
    private static final String BATCH_ENTRY_DEPENDS_ON_PARAM = "depends_on";
    private static final String BATCH_ENTRY_NAME_PARAM = "name";
    private static final String BATCH_ENTRY_OMIT_RESPONSE_ON_SUCCESS_PARAM = "omit_response_on_success";
    private static final String BATCH_METHOD_PARAM = "method";
    private static final String BATCH_PARAM = "batch";
    private static final String BATCH_RELATIVE_URL_PARAM = "relative_url";
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String FORMAT_JSON = "json";
    private static final String FORMAT_PARAM = "format";
    private static final String ISO_8601_FORMAT_STRING = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static final int MAXIMUM_BATCH_SIZE = 50;
    private static final String ME = "me";
    private static final String MIGRATION_BUNDLE_PARAM = "migration_bundle";
    private static final String MIME_BOUNDARY = "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f";
    private static final String MY_FEED = "me/feed";
    private static final String MY_FRIENDS = "me/friends";
    private static final String MY_PHOTOS = "me/photos";
    private static final String MY_VIDEOS = "me/videos";
    private static final String PICTURE_PARAM = "picture";
    private static final String SDK_ANDROID = "android";
    private static final String SDK_PARAM = "sdk";
    private static final String SEARCH = "search";
    private static final String USER_AGENT_BASE = "FBAndroidSDK";
    private static final String USER_AGENT_HEADER = "User-Agent";
    private static String defaultBatchApplicationId;
    private static volatile String userAgent;
    private String batchEntryDependsOn;
    private String batchEntryName;
    private boolean batchEntryOmitResultOnSuccess;
    private Callback callback;
    private GraphObject graphObject;
    private String graphPath;
    private HttpMethod httpMethod;
    private String overriddenURL;
    private Bundle parameters;
    private String restMethod;
    private Session session;

    public interface Callback {
        void onCompleted(Response response);
    }

    public interface GraphPlaceListCallback {
        void onCompleted(List<GraphPlace> list, Response response);
    }

    public interface GraphUserCallback {
        void onCompleted(GraphUser graphUser, Response response);
    }

    public interface GraphUserListCallback {
        void onCompleted(List<GraphUser> list, Response response);
    }

    private interface KeyValueSerializer {
        void writeString(String str, String str2) throws IOException;
    }

    public Request() {
        this(null, null, null, null, null);
    }

    public Request(Session session2, String str) {
        this(session2, str, null, null, null);
    }

    public Request(Session session2, String str, Bundle bundle, HttpMethod httpMethod2) {
        this(session2, str, bundle, httpMethod2, null);
    }

    public Request(Session session2, String str, Bundle bundle, HttpMethod httpMethod2, Callback callback2) {
        this.batchEntryOmitResultOnSuccess = true;
        this.session = session2;
        this.graphPath = str;
        this.callback = callback2;
        setHttpMethod(httpMethod2);
        if (bundle != null) {
            this.parameters = new Bundle(bundle);
        } else {
            this.parameters = new Bundle();
        }
        if (!this.parameters.containsKey(MIGRATION_BUNDLE_PARAM)) {
            this.parameters.putString(MIGRATION_BUNDLE_PARAM, FacebookSdkVersion.MIGRATION_BUNDLE);
        }
    }

    Request(Session session2, URL url) {
        this.batchEntryOmitResultOnSuccess = true;
        this.session = session2;
        this.overriddenURL = url.toString();
        setHttpMethod(HttpMethod.GET);
        this.parameters = new Bundle();
    }

    public static Request newPostRequest(Session session2, String str, GraphObject graphObject2, Callback callback2) {
        Request request = new Request(session2, str, null, HttpMethod.POST, callback2);
        request.setGraphObject(graphObject2);
        return request;
    }

    public static Request newRestRequest(Session session2, String str, Bundle bundle, HttpMethod httpMethod2) {
        Request request = new Request(session2, null, bundle, httpMethod2);
        request.setRestMethod(str);
        return request;
    }

    public static Request newMeRequest(Session session2, final GraphUserCallback graphUserCallback) {
        return new Request(session2, ME, null, null, new Callback() {
            public void onCompleted(Response response) {
                if (graphUserCallback != null) {
                    graphUserCallback.onCompleted((GraphUser) response.getGraphObjectAs(GraphUser.class), response);
                }
            }
        });
    }

    public static Request newMyFriendsRequest(Session session2, final GraphUserListCallback graphUserListCallback) {
        return new Request(session2, MY_FRIENDS, null, null, new Callback() {
            public void onCompleted(Response response) {
                if (graphUserListCallback != null) {
                    graphUserListCallback.onCompleted(Request.typedListFromResponse(response, GraphUser.class), response);
                }
            }
        });
    }

    public static Request newUploadPhotoRequest(Session session2, Bitmap bitmap, Callback callback2) {
        Bundle bundle = new Bundle(1);
        bundle.putParcelable(PICTURE_PARAM, bitmap);
        return new Request(session2, MY_PHOTOS, bundle, HttpMethod.POST, callback2);
    }

    public static Request newUploadPhotoRequest(Session session2, File file, Callback callback2) throws FileNotFoundException {
        ParcelFileDescriptor open = ParcelFileDescriptor.open(file, DriveFile.MODE_READ_ONLY);
        Bundle bundle = new Bundle(1);
        bundle.putParcelable(PICTURE_PARAM, open);
        return new Request(session2, MY_PHOTOS, bundle, HttpMethod.POST, callback2);
    }

    public static Request newUploadVideoRequest(Session session2, File file, Callback callback2) throws FileNotFoundException {
        ParcelFileDescriptor open = ParcelFileDescriptor.open(file, DriveFile.MODE_READ_ONLY);
        Bundle bundle = new Bundle(1);
        bundle.putParcelable(file.getName(), open);
        return new Request(session2, MY_VIDEOS, bundle, HttpMethod.POST, callback2);
    }

    public static Request newGraphPathRequest(Session session2, String str, Callback callback2) {
        return new Request(session2, str, null, null, callback2);
    }

    public static Request newPlacesSearchRequest(Session session2, Location location, int i, int i2, String str, final GraphPlaceListCallback graphPlaceListCallback) {
        if (location != null || !Utility.isNullOrEmpty(str)) {
            Bundle bundle = new Bundle(5);
            bundle.putString("type", "place");
            bundle.putInt("limit", i2);
            if (location != null) {
                bundle.putString("center", String.format(Locale.US, "%f,%f", Double.valueOf(location.getLatitude()), Double.valueOf(location.getLongitude())));
                bundle.putInt("distance", i);
            }
            if (!Utility.isNullOrEmpty(str)) {
                bundle.putString("q", str);
            }
            return new Request(session2, SEARCH, bundle, HttpMethod.GET, new Callback() {
                public void onCompleted(Response response) {
                    if (graphPlaceListCallback != null) {
                        graphPlaceListCallback.onCompleted(Request.typedListFromResponse(response, GraphPlace.class), response);
                    }
                }
            });
        }
        throw new FacebookException("Either location or searchText must be specified.");
    }

    public static Request newStatusUpdateRequest(Session session2, String str, Callback callback2) {
        Bundle bundle = new Bundle();
        bundle.putString("message", str);
        return new Request(session2, MY_FEED, bundle, HttpMethod.POST, callback2);
    }

    public final GraphObject getGraphObject() {
        return this.graphObject;
    }

    public final void setGraphObject(GraphObject graphObject2) {
        this.graphObject = graphObject2;
    }

    public final String getGraphPath() {
        return this.graphPath;
    }

    public final void setGraphPath(String str) {
        this.graphPath = str;
    }

    public final HttpMethod getHttpMethod() {
        return this.httpMethod;
    }

    public final void setHttpMethod(HttpMethod httpMethod2) {
        if (this.overriddenURL == null || httpMethod2 == HttpMethod.GET) {
            if (httpMethod2 == null) {
                httpMethod2 = HttpMethod.GET;
            }
            this.httpMethod = httpMethod2;
            return;
        }
        throw new FacebookException("Can't change HTTP method on request with overridden URL.");
    }

    public final Bundle getParameters() {
        return this.parameters;
    }

    public final void setParameters(Bundle bundle) {
        this.parameters = bundle;
    }

    public final String getRestMethod() {
        return this.restMethod;
    }

    public final void setRestMethod(String str) {
        this.restMethod = str;
    }

    public final Session getSession() {
        return this.session;
    }

    public final void setSession(Session session2) {
        this.session = session2;
    }

    public final String getBatchEntryName() {
        return this.batchEntryName;
    }

    public final void setBatchEntryName(String str) {
        this.batchEntryName = str;
    }

    public final String getBatchEntryDependsOn() {
        return this.batchEntryDependsOn;
    }

    public final void setBatchEntryDependsOn(String str) {
        this.batchEntryDependsOn = str;
    }

    public final boolean getBatchEntryOmitResultOnSuccess() {
        return this.batchEntryOmitResultOnSuccess;
    }

    public final void setBatchEntryOmitResultOnSuccess(boolean z) {
        this.batchEntryOmitResultOnSuccess = z;
    }

    public static final String getDefaultBatchApplicationId() {
        return defaultBatchApplicationId;
    }

    public static final void setDefaultBatchApplicationId(String str) {
        defaultBatchApplicationId = str;
    }

    public final Callback getCallback() {
        return this.callback;
    }

    public final void setCallback(Callback callback2) {
        this.callback = callback2;
    }

    public static RequestAsyncTask executePostRequestAsync(Session session2, String str, GraphObject graphObject2, Callback callback2) {
        return newPostRequest(session2, str, graphObject2, callback2).executeAsync();
    }

    public static RequestAsyncTask executeRestRequestAsync(Session session2, String str, Bundle bundle, HttpMethod httpMethod2) {
        return newRestRequest(session2, str, bundle, httpMethod2).executeAsync();
    }

    public static RequestAsyncTask executeMeRequestAsync(Session session2, GraphUserCallback graphUserCallback) {
        return newMeRequest(session2, graphUserCallback).executeAsync();
    }

    public static RequestAsyncTask executeMyFriendsRequestAsync(Session session2, GraphUserListCallback graphUserListCallback) {
        return newMyFriendsRequest(session2, graphUserListCallback).executeAsync();
    }

    public static RequestAsyncTask executeUploadPhotoRequestAsync(Session session2, Bitmap bitmap, Callback callback2) {
        return newUploadPhotoRequest(session2, bitmap, callback2).executeAsync();
    }

    public static RequestAsyncTask executeUploadPhotoRequestAsync(Session session2, File file, Callback callback2) throws FileNotFoundException {
        return newUploadPhotoRequest(session2, file, callback2).executeAsync();
    }

    public static RequestAsyncTask executeGraphPathRequestAsync(Session session2, String str, Callback callback2) {
        return newGraphPathRequest(session2, str, callback2).executeAsync();
    }

    public static RequestAsyncTask executePlacesSearchRequestAsync(Session session2, Location location, int i, int i2, String str, GraphPlaceListCallback graphPlaceListCallback) {
        return newPlacesSearchRequest(session2, location, i, i2, str, graphPlaceListCallback).executeAsync();
    }

    public static RequestAsyncTask executeStatusUpdateRequestAsync(Session session2, String str, Callback callback2) {
        return newStatusUpdateRequest(session2, str, callback2).executeAsync();
    }

    public final Response executeAndWait() {
        return executeAndWait(this);
    }

    public final RequestAsyncTask executeAsync() {
        return executeBatchAsync(this);
    }

    public static HttpURLConnection toHttpConnection(Request... requestArr) {
        return toHttpConnection(Arrays.asList(requestArr));
    }

    public static HttpURLConnection toHttpConnection(Collection<Request> collection) {
        Validate.notEmptyAndContainsNoNulls(collection, Requests.EXTRA_REQUESTS);
        return toHttpConnection(new RequestBatch(collection));
    }

    public static HttpURLConnection toHttpConnection(RequestBatch requestBatch) {
        URL url;
        Iterator it = requestBatch.iterator();
        while (it.hasNext()) {
            ((Request) it.next()).validate();
        }
        try {
            if (requestBatch.size() == 1) {
                url = new URL(requestBatch.get(0).getUrlForSingleRequest());
            } else {
                url = new URL(ServerProtocol.GRAPH_URL);
            }
            try {
                HttpURLConnection createConnection = createConnection(url);
                serializeToUrlConnection(requestBatch, createConnection);
                return createConnection;
            } catch (IOException e) {
                throw new FacebookException("could not construct request body", e);
            } catch (JSONException e2) {
                throw new FacebookException("could not construct request body", e2);
            }
        } catch (MalformedURLException e3) {
            throw new FacebookException("could not construct URL for request", e3);
        }
    }

    public static Response executeAndWait(Request request) {
        List<Response> executeBatchAndWait = executeBatchAndWait(request);
        if (executeBatchAndWait != null && executeBatchAndWait.size() == 1) {
            return executeBatchAndWait.get(0);
        }
        throw new FacebookException("invalid state: expected a single response");
    }

    public static List<Response> executeBatchAndWait(Request... requestArr) {
        Validate.notNull(requestArr, Requests.EXTRA_REQUESTS);
        return executeBatchAndWait(Arrays.asList(requestArr));
    }

    public static List<Response> executeBatchAndWait(Collection<Request> collection) {
        return executeBatchAndWait(new RequestBatch(collection));
    }

    public static List<Response> executeBatchAndWait(RequestBatch requestBatch) {
        Validate.notEmptyAndContainsNoNulls(requestBatch, Requests.EXTRA_REQUESTS);
        try {
            return executeConnectionAndWait(toHttpConnection(requestBatch), requestBatch);
        } catch (Exception e) {
            List<Response> constructErrorResponses = Response.constructErrorResponses(requestBatch.getRequests(), null, new FacebookException(e));
            runCallbacks(requestBatch, constructErrorResponses);
            return constructErrorResponses;
        }
    }

    public static RequestAsyncTask executeBatchAsync(Request... requestArr) {
        Validate.notNull(requestArr, Requests.EXTRA_REQUESTS);
        return executeBatchAsync(Arrays.asList(requestArr));
    }

    public static RequestAsyncTask executeBatchAsync(Collection<Request> collection) {
        return executeBatchAsync(new RequestBatch(collection));
    }

    public static RequestAsyncTask executeBatchAsync(RequestBatch requestBatch) {
        Validate.notEmptyAndContainsNoNulls(requestBatch, Requests.EXTRA_REQUESTS);
        RequestAsyncTask requestAsyncTask = new RequestAsyncTask(requestBatch);
        requestAsyncTask.executeOnSettingsExecutor();
        return requestAsyncTask;
    }

    public static List<Response> executeConnectionAndWait(HttpURLConnection httpURLConnection, Collection<Request> collection) {
        return executeConnectionAndWait(httpURLConnection, new RequestBatch(collection));
    }

    public static List<Response> executeConnectionAndWait(HttpURLConnection httpURLConnection, RequestBatch requestBatch) {
        List<Response> fromHttpConnection = Response.fromHttpConnection(httpURLConnection, requestBatch);
        Utility.disconnectQuietly(httpURLConnection);
        int size = requestBatch.size();
        if (size != fromHttpConnection.size()) {
            throw new FacebookException(String.format("Received %d responses while expecting %d", Integer.valueOf(fromHttpConnection.size()), Integer.valueOf(size)));
        }
        runCallbacks(requestBatch, fromHttpConnection);
        HashSet hashSet = new HashSet();
        Iterator it = requestBatch.iterator();
        while (it.hasNext()) {
            Request request = (Request) it.next();
            if (request.session != null) {
                hashSet.add(request.session);
            }
        }
        Iterator it2 = hashSet.iterator();
        while (it2.hasNext()) {
            ((Session) it2.next()).extendAccessTokenIfNeeded();
        }
        return fromHttpConnection;
    }

    public static RequestAsyncTask executeConnectionAsync(HttpURLConnection httpURLConnection, RequestBatch requestBatch) {
        return executeConnectionAsync(null, httpURLConnection, requestBatch);
    }

    public static RequestAsyncTask executeConnectionAsync(Handler handler, HttpURLConnection httpURLConnection, RequestBatch requestBatch) {
        Validate.notNull(httpURLConnection, "connection");
        RequestAsyncTask requestAsyncTask = new RequestAsyncTask(httpURLConnection, requestBatch);
        requestBatch.setCallbackHandler(handler);
        requestAsyncTask.executeOnSettingsExecutor();
        return requestAsyncTask;
    }

    public String toString() {
        return "{Request: " + " session: " + this.session + ", graphPath: " + this.graphPath + ", graphObject: " + this.graphObject + ", restMethod: " + this.restMethod + ", httpMethod: " + this.httpMethod + ", parameters: " + this.parameters + "}";
    }

    static void runCallbacks(final RequestBatch requestBatch, List<Response> list) {
        int size = requestBatch.size();
        final ArrayList arrayList = new ArrayList();
        for (int i = 0; i < size; i++) {
            Request request = requestBatch.get(i);
            if (request.callback != null) {
                arrayList.add(new Pair(request.callback, list.get(i)));
            }
        }
        if (arrayList.size() > 0) {
            AnonymousClass4 r7 = new Runnable() {
                public void run() {
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        Pair pair = (Pair) it.next();
                        ((Callback) pair.first).onCompleted((Response) pair.second);
                    }
                    for (RequestBatch.Callback onBatchCompleted : requestBatch.getCallbacks()) {
                        onBatchCompleted.onBatchCompleted(requestBatch);
                    }
                }
            };
            Handler callbackHandler = requestBatch.getCallbackHandler();
            if (callbackHandler == null) {
                r7.run();
            } else {
                callbackHandler.post(r7);
            }
        }
    }

    static HttpURLConnection createConnection(URL url) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestProperty(USER_AGENT_HEADER, getUserAgent());
        httpURLConnection.setRequestProperty(CONTENT_TYPE_HEADER, getMimeContentType());
        httpURLConnection.setChunkedStreamingMode(0);
        return httpURLConnection;
    }

    private void addCommonParameters() {
        if (this.session != null) {
            if (!this.session.isOpened()) {
                throw new FacebookException("Session provided to a Request in un-opened state.");
            } else if (!this.parameters.containsKey("access_token")) {
                String accessToken = this.session.getAccessToken();
                Logger.registerAccessToken(accessToken);
                this.parameters.putString("access_token", accessToken);
            }
        }
        this.parameters.putString("sdk", "android");
        this.parameters.putString(FORMAT_PARAM, "json");
    }

    private String appendParametersToBaseUrl(String str) {
        Uri.Builder encodedPath = new Uri.Builder().encodedPath(str);
        for (String next : this.parameters.keySet()) {
            Object obj = this.parameters.get(next);
            if (obj == null) {
                obj = "";
            }
            if (isSupportedParameterType(obj)) {
                encodedPath.appendQueryParameter(next, parameterToString(obj).toString());
            } else if (this.httpMethod == HttpMethod.GET) {
                throw new IllegalArgumentException(String.format("Unsupported parameter type for GET request: %s", obj.getClass().getSimpleName()));
            }
        }
        return encodedPath.toString();
    }

    /* access modifiers changed from: package-private */
    public final String getUrlForBatchedRequest() {
        String str;
        if (this.overriddenURL != null) {
            throw new FacebookException("Can't override URL for a batch request");
        }
        if (this.restMethod != null) {
            str = ServerProtocol.BATCHED_REST_METHOD_URL_BASE + this.restMethod;
        } else {
            str = this.graphPath;
        }
        addCommonParameters();
        return appendParametersToBaseUrl(str);
    }

    /* access modifiers changed from: package-private */
    public final String getUrlForSingleRequest() {
        String str;
        if (this.overriddenURL != null) {
            return this.overriddenURL.toString();
        }
        if (this.restMethod != null) {
            str = ServerProtocol.REST_URL_BASE + this.restMethod;
        } else {
            str = ServerProtocol.GRAPH_URL_BASE + this.graphPath;
        }
        addCommonParameters();
        return appendParametersToBaseUrl(str);
    }

    private void serializeToBatch(JSONArray jSONArray, Bundle bundle) throws JSONException, IOException {
        JSONObject jSONObject = new JSONObject();
        if (this.batchEntryName != null) {
            jSONObject.put("name", this.batchEntryName);
            jSONObject.put(BATCH_ENTRY_OMIT_RESPONSE_ON_SUCCESS_PARAM, this.batchEntryOmitResultOnSuccess);
        }
        if (this.batchEntryDependsOn != null) {
            jSONObject.put(BATCH_ENTRY_DEPENDS_ON_PARAM, this.batchEntryDependsOn);
        }
        String urlForBatchedRequest = getUrlForBatchedRequest();
        jSONObject.put(BATCH_RELATIVE_URL_PARAM, urlForBatchedRequest);
        jSONObject.put("method", this.httpMethod);
        if (this.session != null) {
            Logger.registerAccessToken(this.session.getAccessToken());
        }
        ArrayList arrayList = new ArrayList();
        for (String str : this.parameters.keySet()) {
            Object obj = this.parameters.get(str);
            if (isSupportedAttachmentType(obj)) {
                String format = String.format("%s%d", ATTACHMENT_FILENAME_PREFIX, Integer.valueOf(bundle.size()));
                arrayList.add(format);
                Utility.putObjectInBundle(bundle, format, obj);
            }
        }
        if (!arrayList.isEmpty()) {
            jSONObject.put(ATTACHED_FILES_PARAM, TextUtils.join(",", arrayList));
        }
        if (this.graphObject != null) {
            final ArrayList arrayList2 = new ArrayList();
            processGraphObject(this.graphObject, urlForBatchedRequest, new KeyValueSerializer() {
                public void writeString(String str, String str2) throws IOException {
                    arrayList2.add(String.format("%s=%s", str, URLEncoder.encode(str2, "UTF-8")));
                }
            });
            jSONObject.put("body", TextUtils.join("&", arrayList2));
        }
        jSONArray.put(jSONObject);
    }

    private void validate() {
        if (this.graphPath != null && this.restMethod != null) {
            throw new IllegalArgumentException("Only one of a graph path or REST method may be specified per request.");
        }
    }

    /* JADX INFO: finally extract failed */
    static final void serializeToUrlConnection(RequestBatch requestBatch, HttpURLConnection httpURLConnection) throws IOException, JSONException {
        Logger logger = new Logger(LoggingBehavior.REQUESTS, "Request");
        int size = requestBatch.size();
        HttpMethod httpMethod2 = size == 1 ? requestBatch.get(0).httpMethod : HttpMethod.POST;
        httpURLConnection.setRequestMethod(httpMethod2.name());
        URL url = httpURLConnection.getURL();
        logger.append("Request:\n");
        logger.appendKeyValue("Id", requestBatch.getId());
        logger.appendKeyValue(MoPubBrowser.DESTINATION_URL_KEY, url);
        logger.appendKeyValue("Method", httpURLConnection.getRequestMethod());
        logger.appendKeyValue(USER_AGENT_HEADER, httpURLConnection.getRequestProperty(USER_AGENT_HEADER));
        logger.appendKeyValue(CONTENT_TYPE_HEADER, httpURLConnection.getRequestProperty(CONTENT_TYPE_HEADER));
        httpURLConnection.setConnectTimeout(requestBatch.getTimeout());
        httpURLConnection.setReadTimeout(requestBatch.getTimeout());
        if (!(httpMethod2 == HttpMethod.POST)) {
            logger.log();
            return;
        }
        httpURLConnection.setDoOutput(true);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
        try {
            Serializer serializer = new Serializer(bufferedOutputStream, logger);
            if (size == 1) {
                Request request = requestBatch.get(0);
                logger.append("  Parameters:\n");
                serializeParameters(request.parameters, serializer);
                logger.append("  Attachments:\n");
                serializeAttachments(request.parameters, serializer);
                if (request.graphObject != null) {
                    processGraphObject(request.graphObject, url.getPath(), serializer);
                }
            } else {
                String batchAppId = getBatchAppId(requestBatch);
                if (Utility.isNullOrEmpty(batchAppId)) {
                    throw new FacebookException("At least one request in a batch must have an open Session, or a default app ID must be specified.");
                }
                serializer.writeString(BATCH_APP_ID_PARAM, batchAppId);
                Bundle bundle = new Bundle();
                serializeRequestsAsJSON(serializer, requestBatch, bundle);
                logger.append("  Attachments:\n");
                serializeAttachments(bundle, serializer);
            }
            bufferedOutputStream.close();
            logger.log();
        } catch (Throwable th) {
            bufferedOutputStream.close();
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void processGraphObject(com.facebook.model.GraphObject r5, java.lang.String r6, com.facebook.Request.KeyValueSerializer r7) throws java.io.IOException {
        /*
            java.lang.String r0 = "me/"
            boolean r0 = r6.startsWith(r0)
            r1 = 1
            r2 = 0
            if (r0 != 0) goto L_0x0015
            java.lang.String r0 = "/me/"
            boolean r0 = r6.startsWith(r0)
            if (r0 == 0) goto L_0x0013
            goto L_0x0015
        L_0x0013:
            r6 = r2
            goto L_0x002a
        L_0x0015:
            java.lang.String r0 = ":"
            int r0 = r6.indexOf(r0)
            java.lang.String r3 = "?"
            int r6 = r6.indexOf(r3)
            r3 = 3
            if (r0 <= r3) goto L_0x0013
            r3 = -1
            if (r6 == r3) goto L_0x0029
            if (r0 >= r6) goto L_0x0013
        L_0x0029:
            r6 = r1
        L_0x002a:
            java.util.Map r5 = r5.asMap()
            java.util.Set r5 = r5.entrySet()
            java.util.Iterator r5 = r5.iterator()
        L_0x0036:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0063
            java.lang.Object r0 = r5.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            if (r6 == 0) goto L_0x0054
            java.lang.Object r3 = r0.getKey()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String r4 = "image"
            boolean r3 = r3.equalsIgnoreCase(r4)
            if (r3 == 0) goto L_0x0054
            r3 = r1
            goto L_0x0055
        L_0x0054:
            r3 = r2
        L_0x0055:
            java.lang.Object r4 = r0.getKey()
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r0 = r0.getValue()
            processGraphObjectProperty(r4, r0, r7, r3)
            goto L_0x0036
        L_0x0063:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.Request.processGraphObject(com.facebook.model.GraphObject, java.lang.String, com.facebook.Request$KeyValueSerializer):void");
    }

    private static void processGraphObjectProperty(String str, Object obj, KeyValueSerializer keyValueSerializer, boolean z) throws IOException {
        Class<?> cls = obj.getClass();
        if (GraphObject.class.isAssignableFrom(cls)) {
            obj = ((GraphObject) obj).getInnerJSONObject();
            cls = obj.getClass();
        } else if (GraphObjectList.class.isAssignableFrom(cls)) {
            obj = ((GraphObjectList) obj).getInnerJSONArray();
            cls = obj.getClass();
        }
        if (JSONObject.class.isAssignableFrom(cls)) {
            JSONObject jSONObject = (JSONObject) obj;
            if (z) {
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    processGraphObjectProperty(String.format("%s[%s]", str, next), jSONObject.opt(next), keyValueSerializer, z);
                }
            } else if (jSONObject.has("id")) {
                processGraphObjectProperty(str, jSONObject.optString("id"), keyValueSerializer, z);
            } else if (jSONObject.has(TJAdUnitConstants.String.URL)) {
                processGraphObjectProperty(str, jSONObject.optString(TJAdUnitConstants.String.URL), keyValueSerializer, z);
            }
        } else if (JSONArray.class.isAssignableFrom(cls)) {
            JSONArray jSONArray = (JSONArray) obj;
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                processGraphObjectProperty(String.format("%s[%d]", str, Integer.valueOf(i)), jSONArray.opt(i), keyValueSerializer, z);
            }
        } else if (String.class.isAssignableFrom(cls) || Number.class.isAssignableFrom(cls) || Boolean.class.isAssignableFrom(cls)) {
            keyValueSerializer.writeString(str, obj.toString());
        } else if (Date.class.isAssignableFrom(cls)) {
            keyValueSerializer.writeString(str, new SimpleDateFormat(ISO_8601_FORMAT_STRING, Locale.US).format((Date) obj));
        }
    }

    private static void serializeParameters(Bundle bundle, Serializer serializer) throws IOException {
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if (isSupportedParameterType(obj)) {
                serializer.writeObject(next, obj);
            }
        }
    }

    private static void serializeAttachments(Bundle bundle, Serializer serializer) throws IOException {
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if (isSupportedAttachmentType(obj)) {
                serializer.writeObject(next, obj);
            }
        }
    }

    private static void serializeRequestsAsJSON(Serializer serializer, Collection<Request> collection, Bundle bundle) throws JSONException, IOException {
        JSONArray jSONArray = new JSONArray();
        for (Request serializeToBatch : collection) {
            serializeToBatch.serializeToBatch(jSONArray, bundle);
        }
        serializer.writeString(BATCH_PARAM, jSONArray.toString());
    }

    private static String getMimeContentType() {
        return String.format("multipart/form-data; boundary=%s", MIME_BOUNDARY);
    }

    private static String getUserAgent() {
        if (userAgent == null) {
            userAgent = String.format("%s.%s", USER_AGENT_BASE, FacebookSdkVersion.BUILD);
        }
        return userAgent;
    }

    private static String getBatchAppId(RequestBatch requestBatch) {
        if (!Utility.isNullOrEmpty(requestBatch.getBatchApplicationId())) {
            return requestBatch.getBatchApplicationId();
        }
        Iterator it = requestBatch.iterator();
        while (it.hasNext()) {
            Session session2 = ((Request) it.next()).session;
            if (session2 != null) {
                return session2.getApplicationId();
            }
        }
        return defaultBatchApplicationId;
    }

    /* access modifiers changed from: private */
    public static <T extends GraphObject> List<T> typedListFromResponse(Response response, Class<T> cls) {
        GraphObjectList<GraphObject> data;
        GraphMultiResult graphMultiResult = (GraphMultiResult) response.getGraphObjectAs(GraphMultiResult.class);
        if (graphMultiResult == null || (data = graphMultiResult.getData()) == null) {
            return null;
        }
        return data.castToListOf(cls);
    }

    private static boolean isSupportedAttachmentType(Object obj) {
        return (obj instanceof Bitmap) || (obj instanceof byte[]) || (obj instanceof ParcelFileDescriptor);
    }

    /* access modifiers changed from: private */
    public static boolean isSupportedParameterType(Object obj) {
        return (obj instanceof String) || (obj instanceof Boolean) || (obj instanceof Number) || (obj instanceof Date);
    }

    /* access modifiers changed from: private */
    public static String parameterToString(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if ((obj instanceof Boolean) || (obj instanceof Number)) {
            return obj.toString();
        }
        if (obj instanceof Date) {
            return new SimpleDateFormat(ISO_8601_FORMAT_STRING, Locale.US).format(obj);
        }
        throw new IllegalArgumentException("Unsupported parameter type.");
    }

    private static class Serializer implements KeyValueSerializer {
        private boolean firstWrite = true;
        private final Logger logger;
        private final BufferedOutputStream outputStream;

        public Serializer(BufferedOutputStream bufferedOutputStream, Logger logger2) {
            this.outputStream = bufferedOutputStream;
            this.logger = logger2;
        }

        public void writeObject(String str, Object obj) throws IOException {
            if (Request.isSupportedParameterType(obj)) {
                writeString(str, Request.parameterToString(obj));
            } else if (obj instanceof Bitmap) {
                writeBitmap(str, (Bitmap) obj);
            } else if (obj instanceof byte[]) {
                writeBytes(str, (byte[]) obj);
            } else if (obj instanceof ParcelFileDescriptor) {
                writeFile(str, (ParcelFileDescriptor) obj);
            } else {
                throw new IllegalArgumentException("value is not a supported type: String, Bitmap, byte[]");
            }
        }

        public void writeString(String str, String str2) throws IOException {
            writeContentDisposition(str, null, null);
            writeLine("%s", str2);
            writeRecordBoundary();
            if (this.logger != null) {
                Logger logger2 = this.logger;
                logger2.appendKeyValue("    " + str, str2);
            }
        }

        public void writeBitmap(String str, Bitmap bitmap) throws IOException {
            writeContentDisposition(str, str, "image/png");
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, this.outputStream);
            writeLine("", new Object[0]);
            writeRecordBoundary();
            Logger logger2 = this.logger;
            logger2.appendKeyValue("    " + str, "<Image>");
        }

        public void writeBytes(String str, byte[] bArr) throws IOException {
            writeContentDisposition(str, str, "content/unknown");
            this.outputStream.write(bArr);
            writeLine("", new Object[0]);
            writeRecordBoundary();
            Logger logger2 = this.logger;
            logger2.appendKeyValue("    " + str, String.format("<Data: %d>", Integer.valueOf(bArr.length)));
        }

        /* JADX WARNING: Removed duplicated region for block: B:24:0x0068  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x006d  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void writeFile(java.lang.String r7, android.os.ParcelFileDescriptor r8) throws java.io.IOException {
            /*
                r6 = this;
                java.lang.String r0 = "content/unknown"
                r6.writeContentDisposition(r7, r7, r0)
                r0 = 0
                android.os.ParcelFileDescriptor$AutoCloseInputStream r1 = new android.os.ParcelFileDescriptor$AutoCloseInputStream     // Catch:{ all -> 0x0063 }
                r1.<init>(r8)     // Catch:{ all -> 0x0063 }
                java.io.BufferedInputStream r8 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0060 }
                r8.<init>(r1)     // Catch:{ all -> 0x0060 }
                r0 = 8192(0x2000, float:1.14794E-41)
                byte[] r0 = new byte[r0]     // Catch:{ all -> 0x005e }
                r2 = 0
                r3 = r2
            L_0x0016:
                int r4 = r8.read(r0)     // Catch:{ all -> 0x005e }
                r5 = -1
                if (r4 == r5) goto L_0x0024
                java.io.BufferedOutputStream r5 = r6.outputStream     // Catch:{ all -> 0x005e }
                r5.write(r0, r2, r4)     // Catch:{ all -> 0x005e }
                int r3 = r3 + r4
                goto L_0x0016
            L_0x0024:
                if (r8 == 0) goto L_0x0029
                r8.close()
            L_0x0029:
                if (r1 == 0) goto L_0x002e
                r1.close()
            L_0x002e:
                java.lang.String r8 = ""
                java.lang.Object[] r0 = new java.lang.Object[r2]
                r6.writeLine(r8, r0)
                r6.writeRecordBoundary()
                com.facebook.internal.Logger r8 = r6.logger
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = "    "
                r0.append(r1)
                r0.append(r7)
                java.lang.String r7 = r0.toString()
                java.lang.String r0 = "<Data: %d>"
                r1 = 1
                java.lang.Object[] r1 = new java.lang.Object[r1]
                java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
                r1[r2] = r3
                java.lang.String r0 = java.lang.String.format(r0, r1)
                r8.appendKeyValue(r7, r0)
                return
            L_0x005e:
                r7 = move-exception
                goto L_0x0066
            L_0x0060:
                r7 = move-exception
                r8 = r0
                goto L_0x0066
            L_0x0063:
                r7 = move-exception
                r8 = r0
                r1 = r8
            L_0x0066:
                if (r8 == 0) goto L_0x006b
                r8.close()
            L_0x006b:
                if (r1 == 0) goto L_0x0070
                r1.close()
            L_0x0070:
                throw r7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.Request.Serializer.writeFile(java.lang.String, android.os.ParcelFileDescriptor):void");
        }

        public void writeRecordBoundary() throws IOException {
            writeLine("--%s", Request.MIME_BOUNDARY);
        }

        public void writeContentDisposition(String str, String str2, String str3) throws IOException {
            write("Content-Disposition: form-data; name=\"%s\"", str);
            if (str2 != null) {
                write("; filename=\"%s\"", str2);
            }
            writeLine("", new Object[0]);
            if (str3 != null) {
                writeLine("%s: %s", Request.CONTENT_TYPE_HEADER, str3);
            }
            writeLine("", new Object[0]);
        }

        public void write(String str, Object... objArr) throws IOException {
            if (this.firstWrite) {
                this.outputStream.write("--".getBytes());
                this.outputStream.write(Request.MIME_BOUNDARY.getBytes());
                this.outputStream.write("\r\n".getBytes());
                this.firstWrite = false;
            }
            this.outputStream.write(String.format(str, objArr).getBytes());
        }

        public void writeLine(String str, Object... objArr) throws IOException {
            write(str, objArr);
            write("\r\n", new Object[0]);
        }
    }
}
