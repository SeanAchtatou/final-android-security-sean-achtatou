package com.facebook.model;

import com.facebook.FacebookGraphObjectException;
import com.facebook.internal.Utility;
import com.facebook.internal.Validate;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public interface GraphObject {
    Map<String, Object> asMap();

    <T extends GraphObject> T cast(Class<T> cls);

    JSONObject getInnerJSONObject();

    Object getProperty(String str);

    void removeProperty(String str);

    void setProperty(String str, Object obj);

    public static final class Factory {
        private static final SimpleDateFormat[] dateFormats = {new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US), new SimpleDateFormat("yyyy-MM-dd", Locale.US)};
        private static final HashSet<Class<?>> verifiedGraphObjectClasses = new HashSet<>();

        private Factory() {
        }

        public static GraphObject create(JSONObject jSONObject) {
            return create(jSONObject, GraphObject.class);
        }

        public static <T extends GraphObject> T create(JSONObject jSONObject, Class<T> cls) {
            return createGraphObjectProxy(cls, jSONObject);
        }

        public static GraphObject create() {
            return create(GraphObject.class);
        }

        public static <T extends GraphObject> T create(Class cls) {
            return createGraphObjectProxy(cls, new JSONObject());
        }

        public static boolean hasSameId(GraphObject graphObject, GraphObject graphObject2) {
            if (graphObject == null || graphObject2 == null || !graphObject.asMap().containsKey("id") || !graphObject2.asMap().containsKey("id")) {
                return false;
            }
            if (graphObject.equals(graphObject2)) {
                return true;
            }
            Object property = graphObject.getProperty("id");
            Object property2 = graphObject2.getProperty("id");
            if (property == null || property2 == null || !(property instanceof String) || !(property2 instanceof String)) {
                return false;
            }
            return property.equals(property2);
        }

        public static <T> GraphObjectList<T> createList(JSONArray jSONArray, Class<T> cls) {
            return new GraphObjectListImpl(jSONArray, cls);
        }

        public static <T> GraphObjectList<T> createList(Class<T> cls) {
            return createList(new JSONArray(), cls);
        }

        /* access modifiers changed from: private */
        public static <T extends GraphObject> T createGraphObjectProxy(Class<T> cls, JSONObject jSONObject) {
            verifyCanProxyClass(cls);
            return (GraphObject) Proxy.newProxyInstance(GraphObject.class.getClassLoader(), new Class[]{cls}, new GraphObjectProxy(jSONObject, cls));
        }

        /* access modifiers changed from: private */
        public static Map<String, Object> createGraphObjectProxyForMap(JSONObject jSONObject) {
            return (Map) Proxy.newProxyInstance(GraphObject.class.getClassLoader(), new Class[]{Map.class}, new GraphObjectProxy(jSONObject, Map.class));
        }

        private static synchronized <T extends GraphObject> boolean hasClassBeenVerified(Class<T> cls) {
            boolean contains;
            synchronized (Factory.class) {
                contains = verifiedGraphObjectClasses.contains(cls);
            }
            return contains;
        }

        private static synchronized <T extends GraphObject> void recordClassHasBeenVerified(Class<T> cls) {
            synchronized (Factory.class) {
                verifiedGraphObjectClasses.add(cls);
            }
        }

        private static <T extends GraphObject> void verifyCanProxyClass(Class<T> cls) {
            if (!hasClassBeenVerified(cls)) {
                if (!cls.isInterface()) {
                    throw new FacebookGraphObjectException("Factory can only wrap interfaces, not class: " + cls.getName());
                }
                for (Method method : cls.getMethods()) {
                    String name = method.getName();
                    int length = method.getParameterTypes().length;
                    Class<?> returnType = method.getReturnType();
                    boolean isAnnotationPresent = method.isAnnotationPresent(PropertyName.class);
                    if (!method.getDeclaringClass().isAssignableFrom(GraphObject.class)) {
                        if (length == 1 && returnType == Void.TYPE) {
                            if (isAnnotationPresent) {
                                if (!Utility.isNullOrEmpty(((PropertyName) method.getAnnotation(PropertyName.class)).value())) {
                                }
                            } else if (name.startsWith("set") && name.length() > 3) {
                            }
                        } else if (length == 0 && returnType != Void.TYPE) {
                            if (isAnnotationPresent) {
                                if (!Utility.isNullOrEmpty(((PropertyName) method.getAnnotation(PropertyName.class)).value())) {
                                }
                            } else if (name.startsWith("get") && name.length() > 3) {
                            }
                        }
                        throw new FacebookGraphObjectException("Factory can't proxy method: " + method.toString());
                    }
                }
                recordClassHasBeenVerified(cls);
            }
        }

        static <U> U coerceValueToExpectedType(Object obj, Class<U> cls, ParameterizedType parameterizedType) {
            if (obj == null) {
                return null;
            }
            Class<?> cls2 = obj.getClass();
            if (cls.isAssignableFrom(cls2) || cls.isPrimitive()) {
                return obj;
            }
            if (!GraphObject.class.isAssignableFrom(cls)) {
                int i = 0;
                if (!Iterable.class.equals(cls) && !Collection.class.equals(cls) && !List.class.equals(cls) && !GraphObjectList.class.equals(cls)) {
                    if (String.class.equals(cls)) {
                        if (Double.class.isAssignableFrom(cls2) || Float.class.isAssignableFrom(cls2)) {
                            return String.format("%f", obj);
                        } else if (Number.class.isAssignableFrom(cls2)) {
                            return String.format("%d", obj);
                        }
                    } else if (Date.class.equals(cls) && String.class.isAssignableFrom(cls2)) {
                        SimpleDateFormat[] simpleDateFormatArr = dateFormats;
                        int length = simpleDateFormatArr.length;
                        while (i < length) {
                            try {
                                U parse = simpleDateFormatArr[i].parse((String) obj);
                                if (parse != null) {
                                    return parse;
                                }
                                i++;
                            } catch (ParseException unused) {
                            }
                        }
                    }
                    throw new FacebookGraphObjectException("Can't convert type" + cls2.getName() + " to " + cls.getName());
                } else if (parameterizedType == null) {
                    throw new FacebookGraphObjectException("can't infer generic type of: " + cls.toString());
                } else {
                    Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                    if (actualTypeArguments == null || actualTypeArguments.length != 1 || !(actualTypeArguments[0] instanceof Class)) {
                        throw new FacebookGraphObjectException("Expect collection properties to be of a type with exactly one generic parameter.");
                    }
                    Class cls3 = (Class) actualTypeArguments[0];
                    if (JSONArray.class.isAssignableFrom(cls2)) {
                        return createList((JSONArray) obj, cls3);
                    }
                    throw new FacebookGraphObjectException("Can't create Collection from " + cls2.getName());
                }
            } else if (JSONObject.class.isAssignableFrom(cls2)) {
                return createGraphObjectProxy(cls, (JSONObject) obj);
            } else {
                if (GraphObject.class.isAssignableFrom(cls2)) {
                    return ((GraphObject) obj).cast(cls);
                }
                throw new FacebookGraphObjectException("Can't create GraphObject from " + cls2.getName());
            }
        }

        static String convertCamelCaseToLowercaseWithUnderscores(String str) {
            return str.replaceAll("([a-z])([A-Z])", "$1_$2").toLowerCase(Locale.US);
        }

        /* access modifiers changed from: private */
        public static Object getUnderlyingJSONObject(Object obj) {
            Class<?> cls = obj.getClass();
            if (GraphObject.class.isAssignableFrom(cls)) {
                return ((GraphObject) obj).getInnerJSONObject();
            }
            return GraphObjectList.class.isAssignableFrom(cls) ? ((GraphObjectList) obj).getInnerJSONArray() : obj;
        }

        private static abstract class ProxyBase<STATE> implements InvocationHandler {
            private static final String EQUALS_METHOD = "equals";
            private static final String TOSTRING_METHOD = "toString";
            protected final STATE state;

            protected ProxyBase(STATE state2) {
                this.state = state2;
            }

            /* access modifiers changed from: protected */
            public final Object throwUnexpectedMethodSignature(Method method) {
                throw new FacebookGraphObjectException(getClass().getName() + " got an unexpected method signature: " + method.toString());
            }

            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: STATE
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            protected final java.lang.Object proxyObjectMethods(java.lang.Object r2, java.lang.reflect.Method r3, java.lang.Object[] r4) throws java.lang.Throwable {
                /*
                    r1 = this;
                    java.lang.String r2 = r3.getName()
                    java.lang.String r0 = "equals"
                    boolean r0 = r2.equals(r0)
                    if (r0 == 0) goto L_0x0032
                    r2 = 0
                    r3 = r4[r2]
                    if (r3 != 0) goto L_0x0016
                    java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
                    return r2
                L_0x0016:
                    java.lang.reflect.InvocationHandler r3 = java.lang.reflect.Proxy.getInvocationHandler(r3)
                    boolean r4 = r3 instanceof com.facebook.model.GraphObject.Factory.GraphObjectProxy
                    if (r4 != 0) goto L_0x0023
                    java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
                    return r2
                L_0x0023:
                    com.facebook.model.GraphObject$Factory$GraphObjectProxy r3 = (com.facebook.model.GraphObject.Factory.GraphObjectProxy) r3
                    STATE r2 = r1.state
                    java.lang.Object r3 = r3.state
                    boolean r2 = r2.equals(r3)
                    java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
                    return r2
                L_0x0032:
                    java.lang.String r0 = "toString"
                    boolean r2 = r2.equals(r0)
                    if (r2 == 0) goto L_0x003f
                    java.lang.String r2 = r1.toString()
                    return r2
                L_0x003f:
                    STATE r2 = r1.state
                    java.lang.Object r2 = r3.invoke(r2, r4)
                    return r2
                */
                throw new UnsupportedOperationException("Method not decompiled: com.facebook.model.GraphObject.Factory.ProxyBase.proxyObjectMethods(java.lang.Object, java.lang.reflect.Method, java.lang.Object[]):java.lang.Object");
            }
        }

        private static final class GraphObjectProxy extends ProxyBase<JSONObject> {
            private static final String CASTTOMAP_METHOD = "asMap";
            private static final String CAST_METHOD = "cast";
            private static final String CLEAR_METHOD = "clear";
            private static final String CONTAINSKEY_METHOD = "containsKey";
            private static final String CONTAINSVALUE_METHOD = "containsValue";
            private static final String ENTRYSET_METHOD = "entrySet";
            private static final String GETINNERJSONOBJECT_METHOD = "getInnerJSONObject";
            private static final String GETPROPERTY_METHOD = "getProperty";
            private static final String GET_METHOD = "get";
            private static final String ISEMPTY_METHOD = "isEmpty";
            private static final String KEYSET_METHOD = "keySet";
            private static final String PUTALL_METHOD = "putAll";
            private static final String PUT_METHOD = "put";
            private static final String REMOVEPROPERTY_METHOD = "removeProperty";
            private static final String REMOVE_METHOD = "remove";
            private static final String SETPROPERTY_METHOD = "setProperty";
            private static final String SIZE_METHOD = "size";
            private static final String VALUES_METHOD = "values";
            private final Class<?> graphObjectClass;

            public GraphObjectProxy(JSONObject jSONObject, Class<?> cls) {
                super(jSONObject);
                this.graphObjectClass = cls;
            }

            public String toString() {
                return String.format("GraphObject{graphObjectClass=%s, state=%s}", this.graphObjectClass.getSimpleName(), this.state);
            }

            public final Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
                Class<?> declaringClass = method.getDeclaringClass();
                if (declaringClass == Object.class) {
                    return proxyObjectMethods(obj, method, objArr);
                }
                if (declaringClass == Map.class) {
                    return proxyMapMethods(method, objArr);
                }
                if (declaringClass == GraphObject.class) {
                    return proxyGraphObjectMethods(obj, method, objArr);
                }
                if (GraphObject.class.isAssignableFrom(declaringClass)) {
                    return proxyGraphObjectGettersAndSetters(method, objArr);
                }
                return throwUnexpectedMethodSignature(method);
            }

            private final Object proxyMapMethods(Method method, Object[] objArr) {
                Map<String, Object> map;
                String name = method.getName();
                if (name.equals("clear")) {
                    JsonUtil.jsonObjectClear((JSONObject) this.state);
                    return null;
                }
                boolean z = false;
                if (name.equals(CONTAINSKEY_METHOD)) {
                    return Boolean.valueOf(((JSONObject) this.state).has((String) objArr[0]));
                }
                if (name.equals(CONTAINSVALUE_METHOD)) {
                    return Boolean.valueOf(JsonUtil.jsonObjectContainsValue((JSONObject) this.state, objArr[0]));
                }
                if (name.equals(ENTRYSET_METHOD)) {
                    return JsonUtil.jsonObjectEntrySet((JSONObject) this.state);
                }
                if (name.equals(GET_METHOD)) {
                    return ((JSONObject) this.state).opt((String) objArr[0]);
                }
                if (name.equals(ISEMPTY_METHOD)) {
                    if (((JSONObject) this.state).length() == 0) {
                        z = true;
                    }
                    return Boolean.valueOf(z);
                } else if (name.equals(KEYSET_METHOD)) {
                    return JsonUtil.jsonObjectKeySet((JSONObject) this.state);
                } else {
                    if (name.equals(PUT_METHOD)) {
                        return setJSONProperty(objArr);
                    }
                    if (name.equals(PUTALL_METHOD)) {
                        if (objArr[0] instanceof Map) {
                            map = (Map) objArr[0];
                        } else {
                            map = objArr[0] instanceof GraphObject ? ((GraphObject) objArr[0]).asMap() : null;
                        }
                        JsonUtil.jsonObjectPutAll((JSONObject) this.state, map);
                        return null;
                    } else if (name.equals(REMOVE_METHOD)) {
                        ((JSONObject) this.state).remove((String) objArr[0]);
                        return null;
                    } else if (name.equals(SIZE_METHOD)) {
                        return Integer.valueOf(((JSONObject) this.state).length());
                    } else {
                        if (name.equals("values")) {
                            return JsonUtil.jsonObjectValues((JSONObject) this.state);
                        }
                        return throwUnexpectedMethodSignature(method);
                    }
                }
            }

            private final Object proxyGraphObjectMethods(Object obj, Method method, Object[] objArr) {
                String name = method.getName();
                if (name.equals(CAST_METHOD)) {
                    Class cls = (Class) objArr[0];
                    if (cls == null || !cls.isAssignableFrom(this.graphObjectClass)) {
                        return Factory.createGraphObjectProxy(cls, (JSONObject) this.state);
                    }
                    return obj;
                } else if (name.equals(GETINNERJSONOBJECT_METHOD)) {
                    return ((GraphObjectProxy) Proxy.getInvocationHandler(obj)).state;
                } else {
                    if (name.equals(CASTTOMAP_METHOD)) {
                        return Factory.createGraphObjectProxyForMap((JSONObject) this.state);
                    }
                    if (name.equals(GETPROPERTY_METHOD)) {
                        return ((JSONObject) this.state).opt((String) objArr[0]);
                    }
                    if (name.equals(SETPROPERTY_METHOD)) {
                        return setJSONProperty(objArr);
                    }
                    if (!name.equals(REMOVEPROPERTY_METHOD)) {
                        return throwUnexpectedMethodSignature(method);
                    }
                    ((JSONObject) this.state).remove((String) objArr[0]);
                    return null;
                }
            }

            /* JADX WARN: Type inference failed for: r6v12, types: [java.lang.reflect.Type] */
            /* JADX WARNING: Multi-variable type inference failed */
            /* JADX WARNING: Unknown variable types count: 1 */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            private final java.lang.Object proxyGraphObjectGettersAndSetters(java.lang.reflect.Method r6, java.lang.Object[] r7) throws org.json.JSONException {
                /*
                    r5 = this;
                    java.lang.String r0 = r6.getName()
                    java.lang.Class[] r1 = r6.getParameterTypes()
                    int r1 = r1.length
                    java.lang.Class<com.facebook.model.PropertyName> r2 = com.facebook.model.PropertyName.class
                    java.lang.annotation.Annotation r2 = r6.getAnnotation(r2)
                    com.facebook.model.PropertyName r2 = (com.facebook.model.PropertyName) r2
                    if (r2 == 0) goto L_0x0018
                    java.lang.String r0 = r2.value()
                    goto L_0x0021
                L_0x0018:
                    r2 = 3
                    java.lang.String r0 = r0.substring(r2)
                    java.lang.String r0 = com.facebook.model.GraphObject.Factory.convertCamelCaseToLowercaseWithUnderscores(r0)
                L_0x0021:
                    r2 = 0
                    if (r1 != 0) goto L_0x0040
                    java.lang.Object r7 = r5.state
                    org.json.JSONObject r7 = (org.json.JSONObject) r7
                    java.lang.Object r7 = r7.opt(r0)
                    java.lang.Class r0 = r6.getReturnType()
                    java.lang.reflect.Type r6 = r6.getGenericReturnType()
                    boolean r1 = r6 instanceof java.lang.reflect.ParameterizedType
                    if (r1 == 0) goto L_0x003b
                    r2 = r6
                    java.lang.reflect.ParameterizedType r2 = (java.lang.reflect.ParameterizedType) r2
                L_0x003b:
                    java.lang.Object r6 = com.facebook.model.GraphObject.Factory.coerceValueToExpectedType(r7, r0, r2)
                    return r6
                L_0x0040:
                    r3 = 1
                    if (r1 != r3) goto L_0x00b0
                    r6 = 0
                    r6 = r7[r6]
                    java.lang.Class<com.facebook.model.GraphObject> r7 = com.facebook.model.GraphObject.class
                    java.lang.Class r1 = r6.getClass()
                    boolean r7 = r7.isAssignableFrom(r1)
                    if (r7 == 0) goto L_0x0059
                    com.facebook.model.GraphObject r6 = (com.facebook.model.GraphObject) r6
                    org.json.JSONObject r6 = r6.getInnerJSONObject()
                    goto L_0x00a8
                L_0x0059:
                    java.lang.Class<com.facebook.model.GraphObjectList> r7 = com.facebook.model.GraphObjectList.class
                    java.lang.Class r1 = r6.getClass()
                    boolean r7 = r7.isAssignableFrom(r1)
                    if (r7 == 0) goto L_0x006c
                    com.facebook.model.GraphObjectList r6 = (com.facebook.model.GraphObjectList) r6
                    org.json.JSONArray r6 = r6.getInnerJSONArray()
                    goto L_0x00a8
                L_0x006c:
                    java.lang.Class<java.lang.Iterable> r7 = java.lang.Iterable.class
                    java.lang.Class r1 = r6.getClass()
                    boolean r7 = r7.isAssignableFrom(r1)
                    if (r7 == 0) goto L_0x00a8
                    org.json.JSONArray r7 = new org.json.JSONArray
                    r7.<init>()
                    java.lang.Iterable r6 = (java.lang.Iterable) r6
                    java.util.Iterator r6 = r6.iterator()
                L_0x0083:
                    boolean r1 = r6.hasNext()
                    if (r1 == 0) goto L_0x00a7
                    java.lang.Object r1 = r6.next()
                    java.lang.Class<com.facebook.model.GraphObject> r3 = com.facebook.model.GraphObject.class
                    java.lang.Class r4 = r1.getClass()
                    boolean r3 = r3.isAssignableFrom(r4)
                    if (r3 == 0) goto L_0x00a3
                    com.facebook.model.GraphObject r1 = (com.facebook.model.GraphObject) r1
                    org.json.JSONObject r1 = r1.getInnerJSONObject()
                    r7.put(r1)
                    goto L_0x0083
                L_0x00a3:
                    r7.put(r1)
                    goto L_0x0083
                L_0x00a7:
                    r6 = r7
                L_0x00a8:
                    java.lang.Object r7 = r5.state
                    org.json.JSONObject r7 = (org.json.JSONObject) r7
                    r7.putOpt(r0, r6)
                    return r2
                L_0x00b0:
                    java.lang.Object r6 = r5.throwUnexpectedMethodSignature(r6)
                    return r6
                */
                throw new UnsupportedOperationException("Method not decompiled: com.facebook.model.GraphObject.Factory.GraphObjectProxy.proxyGraphObjectGettersAndSetters(java.lang.reflect.Method, java.lang.Object[]):java.lang.Object");
            }

            private Object setJSONProperty(Object[] objArr) {
                try {
                    ((JSONObject) this.state).putOpt((String) objArr[0], Factory.getUnderlyingJSONObject(objArr[1]));
                    return null;
                } catch (JSONException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        private static final class GraphObjectListImpl<T> extends AbstractList<T> implements GraphObjectList<T> {
            private final Class<?> itemType;
            private final JSONArray state;

            public GraphObjectListImpl(JSONArray jSONArray, Class<?> cls) {
                Validate.notNull(jSONArray, "state");
                Validate.notNull(cls, "itemType");
                this.state = jSONArray;
                this.itemType = cls;
            }

            public String toString() {
                return String.format("GraphObjectList{itemType=%s, state=%s}", this.itemType.getSimpleName(), this.state);
            }

            public void add(int i, T t) {
                if (i < 0) {
                    throw new IndexOutOfBoundsException();
                } else if (i < size()) {
                    throw new UnsupportedOperationException("Only adding items at the end of the list is supported.");
                } else {
                    put(i, t);
                }
            }

            public T set(int i, T t) {
                checkIndex(i);
                T t2 = get(i);
                put(i, t);
                return t2;
            }

            public int hashCode() {
                return this.state.hashCode();
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (getClass() != obj.getClass()) {
                    return false;
                }
                return this.state.equals(((GraphObjectListImpl) obj).state);
            }

            public T get(int i) {
                checkIndex(i);
                return Factory.coerceValueToExpectedType(this.state.opt(i), this.itemType, null);
            }

            public int size() {
                return this.state.length();
            }

            public final <U extends GraphObject> GraphObjectList<U> castToListOf(Class<U> cls) {
                if (!GraphObject.class.isAssignableFrom(this.itemType)) {
                    throw new FacebookGraphObjectException("Can't cast GraphObjectCollection of non-GraphObject type " + this.itemType);
                } else if (cls.isAssignableFrom(this.itemType)) {
                    return this;
                } else {
                    return Factory.createList(this.state, cls);
                }
            }

            public final JSONArray getInnerJSONArray() {
                return this.state;
            }

            public void clear() {
                throw new UnsupportedOperationException();
            }

            public boolean remove(Object obj) {
                throw new UnsupportedOperationException();
            }

            public boolean removeAll(Collection<?> collection) {
                throw new UnsupportedOperationException();
            }

            public boolean retainAll(Collection<?> collection) {
                throw new UnsupportedOperationException();
            }

            private void checkIndex(int i) {
                if (i < 0 || i >= this.state.length()) {
                    throw new IndexOutOfBoundsException();
                }
            }

            private void put(int i, T t) {
                try {
                    this.state.put(i, Factory.getUnderlyingJSONObject(t));
                } catch (JSONException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }
}
