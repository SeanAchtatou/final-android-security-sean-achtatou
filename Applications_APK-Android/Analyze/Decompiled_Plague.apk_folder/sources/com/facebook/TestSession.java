package com.facebook;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.Session;
import com.facebook.internal.Utility;
import com.facebook.internal.Validate;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphObjectList;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class TestSession extends Session {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final String LOG_TAG = "FacebookSDK.TestSession";
    private static Map<String, TestAccount> appTestAccounts = null;
    private static final long serialVersionUID = 1;
    private static String testApplicationId;
    private static String testApplicationSecret;
    private final Mode mode;
    private final List<String> requestedPermissions;
    private final String sessionUniqueUserTag;
    private String testAccountId;
    private boolean wasAskedToExtendAccessToken;

    private interface FqlResponse extends GraphObject {
        GraphObjectList<FqlResult> getData();
    }

    private interface FqlResult extends GraphObject {
        GraphObjectList<GraphObject> getFqlResultSet();
    }

    private enum Mode {
        PRIVATE,
        SHARED
    }

    private interface TestAccount extends GraphObject {
        String getAccessToken();

        String getId();

        String getName();

        void setName(String str);
    }

    private interface UserAccount extends GraphObject {
        String getName();

        String getUid();

        void setName(String str);
    }

    TestSession(Activity activity, List<String> list, TokenCachingStrategy tokenCachingStrategy, String str, Mode mode2) {
        super(activity, testApplicationId, tokenCachingStrategy);
        Validate.notNull(list, "permissions");
        Validate.notNullOrEmpty(testApplicationId, "testApplicationId");
        Validate.notNullOrEmpty(testApplicationSecret, "testApplicationSecret");
        this.sessionUniqueUserTag = str;
        this.mode = mode2;
        this.requestedPermissions = list;
    }

    public static TestSession createSessionWithPrivateUser(Activity activity, List<String> list) {
        return createTestSession(activity, list, Mode.PRIVATE, null);
    }

    public static TestSession createSessionWithSharedUser(Activity activity, List<String> list) {
        return createSessionWithSharedUser(activity, list, null);
    }

    public static TestSession createSessionWithSharedUser(Activity activity, List<String> list, String str) {
        return createTestSession(activity, list, Mode.SHARED, str);
    }

    public static synchronized String getTestApplicationId() {
        String str;
        synchronized (TestSession.class) {
            str = testApplicationId;
        }
        return str;
    }

    public static synchronized void setTestApplicationId(String str) {
        synchronized (TestSession.class) {
            if (testApplicationId == null || testApplicationId.equals(str)) {
                testApplicationId = str;
            } else {
                throw new FacebookException("Can't have more than one test application ID");
            }
        }
    }

    public static synchronized String getTestApplicationSecret() {
        String str;
        synchronized (TestSession.class) {
            str = testApplicationSecret;
        }
        return str;
    }

    public static synchronized void setTestApplicationSecret(String str) {
        synchronized (TestSession.class) {
            if (testApplicationSecret == null || testApplicationSecret.equals(str)) {
                testApplicationSecret = str;
            } else {
                throw new FacebookException("Can't have more than one test application secret");
            }
        }
    }

    public final String getTestUserId() {
        return this.testAccountId;
    }

    private static synchronized TestSession createTestSession(Activity activity, List<String> list, Mode mode2, String str) {
        TestSession testSession;
        synchronized (TestSession.class) {
            if (!Utility.isNullOrEmpty(testApplicationId)) {
                if (!Utility.isNullOrEmpty(testApplicationSecret)) {
                    if (Utility.isNullOrEmpty(list)) {
                        list = Arrays.asList("email", "publish_actions");
                    }
                    testSession = new TestSession(activity, list, new TestTokenCachingStrategy(), str, mode2);
                }
            }
            throw new FacebookException("Must provide app ID and secret");
        }
        return testSession;
    }

    private static synchronized void retrieveTestAccountsForAppIfNeeded() {
        synchronized (TestSession.class) {
            if (appTestAccounts == null) {
                appTestAccounts = new HashMap();
                String format = String.format("SELECT id,access_token FROM test_account WHERE app_id = %s", testApplicationId);
                Bundle bundle = new Bundle();
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("test_accounts", format);
                    jSONObject.put("users", "SELECT uid,name FROM user WHERE uid IN (SELECT id FROM #test_accounts)");
                    bundle.putString("q", jSONObject.toString());
                    bundle.putString("access_token", getAppAccessToken());
                    Response executeAndWait = new Request(null, "fql", bundle, null).executeAndWait();
                    if (executeAndWait.getError() != null) {
                        throw executeAndWait.getError().getException();
                    }
                    GraphObjectList<FqlResult> data = ((FqlResponse) executeAndWait.getGraphObjectAs(FqlResponse.class)).getData();
                    if (data != null) {
                        if (data.size() == 2) {
                            populateTestAccounts(((FqlResult) data.get(0)).getFqlResultSet().castToListOf(TestAccount.class), ((FqlResult) data.get(1)).getFqlResultSet().castToListOf(UserAccount.class));
                            return;
                        }
                    }
                    throw new FacebookException("Unexpected number of results from FQL query");
                } catch (JSONException e) {
                    throw new FacebookException(e);
                }
            }
        }
    }

    private static synchronized void populateTestAccounts(Collection<TestAccount> collection, Collection<UserAccount> collection2) {
        synchronized (TestSession.class) {
            for (TestAccount storeTestAccount : collection) {
                storeTestAccount(storeTestAccount);
            }
            for (UserAccount next : collection2) {
                TestAccount testAccount = appTestAccounts.get(next.getUid());
                if (testAccount != null) {
                    testAccount.setName(next.getName());
                }
            }
        }
    }

    private static synchronized void storeTestAccount(TestAccount testAccount) {
        synchronized (TestSession.class) {
            appTestAccounts.put(testAccount.getId(), testAccount);
        }
    }

    private static synchronized TestAccount findTestAccountMatchingIdentifier(String str) {
        synchronized (TestSession.class) {
            retrieveTestAccountsForAppIfNeeded();
            for (TestAccount next : appTestAccounts.values()) {
                if (next.getName().contains(str)) {
                    return next;
                }
            }
            return null;
        }
    }

    public final String toString() {
        String session = super.toString();
        return "{TestSession" + " testUserId:" + this.testAccountId + " " + session + "}";
    }

    /* access modifiers changed from: package-private */
    public void authorize(Session.AuthorizationRequest authorizationRequest) {
        if (this.mode == Mode.PRIVATE) {
            createTestAccountAndFinishAuth();
        } else {
            findOrCreateSharedTestAccount();
        }
    }

    /* access modifiers changed from: package-private */
    public void postStateChange(SessionState sessionState, SessionState sessionState2, Exception exc) {
        String str = this.testAccountId;
        super.postStateChange(sessionState, sessionState2, exc);
        if (sessionState2.isClosed() && str != null && this.mode == Mode.PRIVATE) {
            deleteTestAccount(str, getAppAccessToken());
        }
    }

    /* access modifiers changed from: package-private */
    public boolean getWasAskedToExtendAccessToken() {
        return this.wasAskedToExtendAccessToken;
    }

    /* access modifiers changed from: package-private */
    public void forceExtendAccessToken(boolean z) {
        AccessToken tokenInfo = getTokenInfo();
        setTokenInfo(new AccessToken(tokenInfo.getToken(), new Date(), tokenInfo.getPermissions(), AccessTokenSource.TEST_USER, new Date(0)));
        setLastAttemptedTokenExtendDate(new Date(0));
    }

    /* access modifiers changed from: package-private */
    public boolean shouldExtendAccessToken() {
        boolean shouldExtendAccessToken = super.shouldExtendAccessToken();
        this.wasAskedToExtendAccessToken = false;
        return shouldExtendAccessToken;
    }

    /* access modifiers changed from: package-private */
    public void extendAccessToken() {
        this.wasAskedToExtendAccessToken = true;
        super.extendAccessToken();
    }

    /* access modifiers changed from: package-private */
    public void fakeTokenRefreshAttempt() {
        setCurrentTokenRefreshRequest(new Session.TokenRefreshRequest());
    }

    static final String getAppAccessToken() {
        return testApplicationId + "|" + testApplicationSecret;
    }

    private void findOrCreateSharedTestAccount() {
        TestAccount findTestAccountMatchingIdentifier = findTestAccountMatchingIdentifier(getSharedTestAccountIdentifier());
        if (findTestAccountMatchingIdentifier != null) {
            finishAuthWithTestAccount(findTestAccountMatchingIdentifier);
        } else {
            createTestAccountAndFinishAuth();
        }
    }

    private void finishAuthWithTestAccount(TestAccount testAccount) {
        this.testAccountId = testAccount.getId();
        finishAuthOrReauth(AccessToken.createFromString(testAccount.getAccessToken(), this.requestedPermissions, AccessTokenSource.TEST_USER), null);
    }

    private TestAccount createTestAccountAndFinishAuth() {
        Bundle bundle = new Bundle();
        bundle.putString(TapjoyConstants.TJC_INSTALLED, "true");
        bundle.putString("permissions", getPermissionsString());
        bundle.putString("access_token", getAppAccessToken());
        if (this.mode == Mode.SHARED) {
            bundle.putString(TJAdUnitConstants.String.USAGE_TRACKER_NAME, String.format("Shared %s Testuser", getSharedTestAccountIdentifier()));
        }
        Response executeAndWait = new Request(null, String.format("%s/accounts/test-users", testApplicationId), bundle, HttpMethod.POST).executeAndWait();
        FacebookRequestError error = executeAndWait.getError();
        TestAccount testAccount = (TestAccount) executeAndWait.getGraphObjectAs(TestAccount.class);
        if (error != null) {
            finishAuthOrReauth(null, error.getException());
            return null;
        }
        if (this.mode == Mode.SHARED) {
            testAccount.setName(bundle.getString(TJAdUnitConstants.String.USAGE_TRACKER_NAME));
            storeTestAccount(testAccount);
        }
        finishAuthWithTestAccount(testAccount);
        return testAccount;
    }

    private void deleteTestAccount(String str, String str2) {
        Bundle bundle = new Bundle();
        bundle.putString("access_token", str2);
        Response executeAndWait = new Request(null, str, bundle, HttpMethod.DELETE).executeAndWait();
        FacebookRequestError error = executeAndWait.getError();
        GraphObject graphObject = executeAndWait.getGraphObject();
        if (error != null) {
            Log.w(LOG_TAG, String.format("Could not delete test account %s: %s", str, error.getException().toString()));
        } else if (graphObject.getProperty(Response.NON_JSON_RESPONSE_PROPERTY) == false) {
            Log.w(LOG_TAG, String.format("Could not delete test account %s: unknown reason", str));
        }
    }

    private String getPermissionsString() {
        return TextUtils.join(",", this.requestedPermissions);
    }

    private String getSharedTestAccountIdentifier() {
        return validNameStringFromInteger((((long) getPermissionsString().hashCode()) & 4294967295L) ^ (this.sessionUniqueUserTag != null ? ((long) this.sessionUniqueUserTag.hashCode()) & 4294967295L : 0));
    }

    private String validNameStringFromInteger(long j) {
        String l = Long.toString(j);
        StringBuilder sb = new StringBuilder("Perm");
        char[] charArray = l.toCharArray();
        int length = charArray.length;
        char c = 0;
        for (int i = 0; i < length; i++) {
            char c2 = charArray[i];
            c = c2 == c ? (char) (c2 + 10) : c2;
            sb.append((char) ((c + 'a') - 48));
        }
        return sb.toString();
    }

    private static final class TestTokenCachingStrategy extends TokenCachingStrategy {
        private Bundle bundle;

        private TestTokenCachingStrategy() {
        }

        public Bundle load() {
            return this.bundle;
        }

        public void save(Bundle bundle2) {
            this.bundle = bundle2;
        }

        public void clear() {
            this.bundle = null;
        }
    }
}
