package com.facebook;

import com.facebook.internal.Utility;
import com.tapjoy.TJAdUnitConstants;
import java.net.HttpURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

public final class FacebookRequestError {
    private static final String BODY_KEY = "body";
    private static final String CODE_KEY = "code";
    private static final int EC_APP_NOT_INSTALLED = 458;
    private static final int EC_APP_TOO_MANY_CALLS = 4;
    private static final int EC_INVALID_SESSION = 102;
    private static final int EC_INVALID_TOKEN = 190;
    private static final int EC_PASSWORD_CHANGED = 460;
    private static final int EC_PERMISSION_DENIED = 10;
    private static final Range EC_RANGE_PERMISSION = new Range(200, 299);
    private static final int EC_SERVICE_UNAVAILABLE = 2;
    private static final int EC_UNCONFIRMED_USER = 464;
    private static final int EC_UNKNOWN_ERROR = 1;
    private static final int EC_USER_CHECKPOINTED = 459;
    private static final int EC_USER_TOO_MANY_CALLS = 17;
    private static final String ERROR_CODE_FIELD_KEY = "code";
    private static final String ERROR_CODE_KEY = "error_code";
    private static final String ERROR_KEY = "error";
    private static final String ERROR_MESSAGE_FIELD_KEY = "message";
    private static final String ERROR_MSG_KEY = "error_msg";
    private static final String ERROR_REASON_KEY = "error_reason";
    private static final String ERROR_SUB_CODE_KEY = "error_subcode";
    private static final String ERROR_TYPE_FIELD_KEY = "type";
    private static final Range HTTP_RANGE_CLIENT_ERROR = new Range(400, 499);
    private static final Range HTTP_RANGE_SERVER_ERROR = new Range(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL, 599);
    private static final Range HTTP_RANGE_SUCCESS = new Range(200, 299);
    public static final int INVALID_ERROR_CODE = -1;
    public static final int INVALID_HTTP_STATUS_CODE = -1;
    private static final int INVALID_MESSAGE_ID = 0;
    private final Object batchRequestResult;
    private final Category category;
    private final HttpURLConnection connection;
    private final int errorCode;
    private final String errorMessage;
    private final String errorType;
    private final FacebookException exception;
    private final JSONObject requestResult;
    private final JSONObject requestResultBody;
    private final int requestStatusCode;
    private final boolean shouldNotifyUser;
    private final int subErrorCode;
    private final int userActionMessageId;

    public enum Category {
        AUTHENTICATION_RETRY,
        AUTHENTICATION_REOPEN_SESSION,
        PERMISSION,
        SERVER,
        THROTTLING,
        OTHER,
        BAD_REQUEST,
        CLIENT
    }

    private static class Range {
        private final int end;
        private final int start;

        private Range(int i, int i2) {
            this.start = i;
            this.end = i2;
        }

        /* access modifiers changed from: package-private */
        public boolean contains(int i) {
            return this.start <= i && i <= this.end;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0099  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private FacebookRequestError(int r1, int r2, int r3, java.lang.String r4, java.lang.String r5, org.json.JSONObject r6, org.json.JSONObject r7, java.lang.Object r8, java.net.HttpURLConnection r9, com.facebook.FacebookException r10) {
        /*
            r0 = this;
            r0.<init>()
            r0.requestStatusCode = r1
            r0.errorCode = r2
            r0.subErrorCode = r3
            r0.errorType = r4
            r0.errorMessage = r5
            r0.requestResultBody = r6
            r0.requestResult = r7
            r0.batchRequestResult = r8
            r0.connection = r9
            r4 = 1
            r6 = 0
            if (r10 == 0) goto L_0x001d
            r0.exception = r10
            r5 = r4
            goto L_0x0025
        L_0x001d:
            com.facebook.FacebookServiceException r7 = new com.facebook.FacebookServiceException
            r7.<init>(r0, r5)
            r0.exception = r7
            r5 = r6
        L_0x0025:
            r7 = 0
            if (r5 == 0) goto L_0x002d
            com.facebook.FacebookRequestError$Category r1 = com.facebook.FacebookRequestError.Category.CLIENT
            r4 = r6
            goto L_0x009a
        L_0x002d:
            if (r2 == r4) goto L_0x007b
            r5 = 2
            if (r2 != r5) goto L_0x0033
            goto L_0x007b
        L_0x0033:
            r5 = 4
            if (r2 == r5) goto L_0x0078
            r5 = 17
            if (r2 != r5) goto L_0x003b
            goto L_0x0078
        L_0x003b:
            r5 = 10
            if (r2 == r5) goto L_0x0071
            com.facebook.FacebookRequestError$Range r5 = com.facebook.FacebookRequestError.EC_RANGE_PERMISSION
            boolean r5 = r5.contains(r2)
            if (r5 == 0) goto L_0x0048
            goto L_0x0071
        L_0x0048:
            r5 = 102(0x66, float:1.43E-43)
            if (r2 == r5) goto L_0x0050
            r5 = 190(0xbe, float:2.66E-43)
            if (r2 != r5) goto L_0x007d
        L_0x0050:
            r2 = 459(0x1cb, float:6.43E-43)
            if (r3 == r2) goto L_0x006c
            r2 = 464(0x1d0, float:6.5E-43)
            if (r3 != r2) goto L_0x0059
            goto L_0x006c
        L_0x0059:
            com.facebook.FacebookRequestError$Category r7 = com.facebook.FacebookRequestError.Category.AUTHENTICATION_REOPEN_SESSION
            r2 = 458(0x1ca, float:6.42E-43)
            if (r3 != r2) goto L_0x0062
            int r2 = com.facebook.android.R.string.com_facebook_requesterror_relogin
            goto L_0x0075
        L_0x0062:
            r2 = 460(0x1cc, float:6.45E-43)
            if (r3 != r2) goto L_0x0069
            int r2 = com.facebook.android.R.string.com_facebook_requesterror_password_changed
            goto L_0x0075
        L_0x0069:
            int r2 = com.facebook.android.R.string.com_facebook_requesterror_reconnect
            goto L_0x0075
        L_0x006c:
            com.facebook.FacebookRequestError$Category r7 = com.facebook.FacebookRequestError.Category.AUTHENTICATION_RETRY
            int r6 = com.facebook.android.R.string.com_facebook_requesterror_web_login
            goto L_0x007e
        L_0x0071:
            com.facebook.FacebookRequestError$Category r7 = com.facebook.FacebookRequestError.Category.PERMISSION
            int r2 = com.facebook.android.R.string.com_facebook_requesterror_permissions
        L_0x0075:
            r4 = r6
            r6 = r2
            goto L_0x007e
        L_0x0078:
            com.facebook.FacebookRequestError$Category r7 = com.facebook.FacebookRequestError.Category.THROTTLING
            goto L_0x007d
        L_0x007b:
            com.facebook.FacebookRequestError$Category r7 = com.facebook.FacebookRequestError.Category.SERVER
        L_0x007d:
            r4 = r6
        L_0x007e:
            if (r7 != 0) goto L_0x0099
            com.facebook.FacebookRequestError$Range r2 = com.facebook.FacebookRequestError.HTTP_RANGE_CLIENT_ERROR
            boolean r2 = r2.contains(r1)
            if (r2 == 0) goto L_0x008b
            com.facebook.FacebookRequestError$Category r1 = com.facebook.FacebookRequestError.Category.BAD_REQUEST
            goto L_0x009a
        L_0x008b:
            com.facebook.FacebookRequestError$Range r2 = com.facebook.FacebookRequestError.HTTP_RANGE_SERVER_ERROR
            boolean r1 = r2.contains(r1)
            if (r1 == 0) goto L_0x0096
            com.facebook.FacebookRequestError$Category r1 = com.facebook.FacebookRequestError.Category.SERVER
            goto L_0x009a
        L_0x0096:
            com.facebook.FacebookRequestError$Category r1 = com.facebook.FacebookRequestError.Category.OTHER
            goto L_0x009a
        L_0x0099:
            r1 = r7
        L_0x009a:
            r0.category = r1
            r0.userActionMessageId = r6
            r0.shouldNotifyUser = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.FacebookRequestError.<init>(int, int, int, java.lang.String, java.lang.String, org.json.JSONObject, org.json.JSONObject, java.lang.Object, java.net.HttpURLConnection, com.facebook.FacebookException):void");
    }

    private FacebookRequestError(int i, int i2, int i3, String str, String str2, JSONObject jSONObject, JSONObject jSONObject2, Object obj, HttpURLConnection httpURLConnection) {
        this(i, i2, i3, str, str2, jSONObject, jSONObject2, obj, httpURLConnection, null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    FacebookRequestError(HttpURLConnection httpURLConnection, Exception exc) {
        this(-1, -1, -1, null, null, null, null, null, httpURLConnection, exc instanceof FacebookException ? (FacebookException) exc : new FacebookException(exc));
    }

    public FacebookRequestError(int i, String str, String str2) {
        this(-1, i, -1, str, str2, null, null, null, null, null);
    }

    public int getUserActionMessageId() {
        return this.userActionMessageId;
    }

    public boolean shouldNotifyUser() {
        return this.shouldNotifyUser;
    }

    public Category getCategory() {
        return this.category;
    }

    public int getRequestStatusCode() {
        return this.requestStatusCode;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public int getSubErrorCode() {
        return this.subErrorCode;
    }

    public String getErrorType() {
        return this.errorType;
    }

    public String getErrorMessage() {
        if (this.errorMessage != null) {
            return this.errorMessage;
        }
        return this.exception.getLocalizedMessage();
    }

    public JSONObject getRequestResultBody() {
        return this.requestResultBody;
    }

    public JSONObject getRequestResult() {
        return this.requestResult;
    }

    public Object getBatchRequestResult() {
        return this.batchRequestResult;
    }

    public HttpURLConnection getConnection() {
        return this.connection;
    }

    public FacebookException getException() {
        return this.exception;
    }

    public String toString() {
        return "{HttpStatus: " + this.requestStatusCode + ", errorCode: " + this.errorCode + ", errorType: " + this.errorType + ", errorMessage: " + this.errorMessage + "}";
    }

    static FacebookRequestError checkResponseAndCreateError(JSONObject jSONObject, Object obj, HttpURLConnection httpURLConnection) {
        String str;
        String str2;
        int i;
        try {
            if (jSONObject.has("code")) {
                int i2 = jSONObject.getInt("code");
                Object stringPropertyAsJSON = Utility.getStringPropertyAsJSON(jSONObject, "body", Response.NON_JSON_RESPONSE_PROPERTY);
                if (stringPropertyAsJSON != null && (stringPropertyAsJSON instanceof JSONObject)) {
                    JSONObject jSONObject2 = (JSONObject) stringPropertyAsJSON;
                    boolean z = true;
                    int i3 = -1;
                    if (jSONObject2.has("error")) {
                        JSONObject jSONObject3 = (JSONObject) Utility.getStringPropertyAsJSON(jSONObject2, "error", null);
                        String optString = jSONObject3.optString("type", null);
                        String optString2 = jSONObject3.optString("message", null);
                        int optInt = jSONObject3.optInt("code", -1);
                        i3 = jSONObject3.optInt(ERROR_SUB_CODE_KEY, -1);
                        String str3 = optString2;
                        str2 = optString;
                        i = optInt;
                        str = str3;
                    } else {
                        if (!jSONObject2.has(ERROR_CODE_KEY) && !jSONObject2.has(ERROR_MSG_KEY)) {
                            if (!jSONObject2.has(ERROR_REASON_KEY)) {
                                z = false;
                                i = -1;
                                str2 = null;
                                str = null;
                            }
                        }
                        String optString3 = jSONObject2.optString(ERROR_REASON_KEY, null);
                        String optString4 = jSONObject2.optString(ERROR_MSG_KEY, null);
                        int optInt2 = jSONObject2.optInt(ERROR_CODE_KEY, -1);
                        i3 = jSONObject2.optInt(ERROR_SUB_CODE_KEY, -1);
                        str = optString4;
                        i = optInt2;
                        str2 = optString3;
                    }
                    if (z) {
                        return new FacebookRequestError(i2, i, i3, str2, str, jSONObject2, jSONObject, obj, httpURLConnection);
                    }
                }
                if (!HTTP_RANGE_SUCCESS.contains(i2)) {
                    return new FacebookRequestError(i2, -1, -1, null, null, jSONObject.has("body") ? (JSONObject) Utility.getStringPropertyAsJSON(jSONObject, "body", Response.NON_JSON_RESPONSE_PROPERTY) : null, jSONObject, obj, httpURLConnection);
                }
            }
        } catch (JSONException unused) {
        }
        return null;
    }
}
