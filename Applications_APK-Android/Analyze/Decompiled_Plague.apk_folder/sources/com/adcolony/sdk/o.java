package com.adcolony.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import com.adcolony.sdk.aa;
import org.json.JSONObject;

class o {
    static AlertDialog a;
    /* access modifiers changed from: private */
    public af b;
    /* access modifiers changed from: private */
    public AlertDialog c;
    /* access modifiers changed from: private */
    public boolean d;

    o() {
        a.a("Alert.show", new ah() {
            public void a(af afVar) {
                if (!a.d()) {
                    new aa.a().a("Null Activity reference, can't build AlertDialog.").a(aa.g);
                } else if (y.d(afVar.c(), "on_resume")) {
                    af unused = o.this.b = afVar;
                } else {
                    o.this.a(afVar);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(final af afVar) {
        Activity c2 = a.c();
        if (c2 != null) {
            final AlertDialog.Builder builder = a.a().n().r() >= 21 ? new AlertDialog.Builder(c2, 16974374) : new AlertDialog.Builder(c2, 16974126);
            JSONObject c3 = afVar.c();
            String b2 = y.b(c3, "message");
            String b3 = y.b(c3, "title");
            String b4 = y.b(c3, "positive");
            String b5 = y.b(c3, "negative");
            builder.setMessage(b2);
            builder.setTitle(b3);
            builder.setPositiveButton(b4, new DialogInterface.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, boolean):boolean
                 arg types: [org.json.JSONObject, java.lang.String, int]
                 candidates:
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, int):int
                  com.adcolony.sdk.y.a(org.json.JSONArray, java.lang.String[], boolean):org.json.JSONArray
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, double):boolean
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, long):boolean
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, java.lang.String):boolean
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, org.json.JSONArray):boolean
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, org.json.JSONObject):boolean
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, boolean):boolean */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.o.a(com.adcolony.sdk.o, boolean):boolean
                 arg types: [com.adcolony.sdk.o, int]
                 candidates:
                  com.adcolony.sdk.o.a(com.adcolony.sdk.o, android.app.AlertDialog):android.app.AlertDialog
                  com.adcolony.sdk.o.a(com.adcolony.sdk.o, com.adcolony.sdk.af):com.adcolony.sdk.af
                  com.adcolony.sdk.o.a(com.adcolony.sdk.o, boolean):boolean */
                public void onClick(DialogInterface dialogInterface, int i) {
                    AlertDialog unused = o.this.c = (AlertDialog) null;
                    dialogInterface.dismiss();
                    JSONObject a2 = y.a();
                    y.a(a2, "positive", true);
                    boolean unused2 = o.this.d = false;
                    afVar.a(a2).b();
                }
            });
            if (!b5.equals("")) {
                builder.setNegativeButton(b5, new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, boolean):boolean
                     arg types: [org.json.JSONObject, java.lang.String, int]
                     candidates:
                      com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, int):int
                      com.adcolony.sdk.y.a(org.json.JSONArray, java.lang.String[], boolean):org.json.JSONArray
                      com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, double):boolean
                      com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, long):boolean
                      com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, java.lang.String):boolean
                      com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, org.json.JSONArray):boolean
                      com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, org.json.JSONObject):boolean
                      com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, boolean):boolean */
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.adcolony.sdk.o.a(com.adcolony.sdk.o, boolean):boolean
                     arg types: [com.adcolony.sdk.o, int]
                     candidates:
                      com.adcolony.sdk.o.a(com.adcolony.sdk.o, android.app.AlertDialog):android.app.AlertDialog
                      com.adcolony.sdk.o.a(com.adcolony.sdk.o, com.adcolony.sdk.af):com.adcolony.sdk.af
                      com.adcolony.sdk.o.a(com.adcolony.sdk.o, boolean):boolean */
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AlertDialog unused = o.this.c = (AlertDialog) null;
                        dialogInterface.dismiss();
                        JSONObject a2 = y.a();
                        y.a(a2, "positive", false);
                        boolean unused2 = o.this.d = false;
                        afVar.a(a2).b();
                    }
                });
            }
            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.o.a(com.adcolony.sdk.o, boolean):boolean
                 arg types: [com.adcolony.sdk.o, int]
                 candidates:
                  com.adcolony.sdk.o.a(com.adcolony.sdk.o, android.app.AlertDialog):android.app.AlertDialog
                  com.adcolony.sdk.o.a(com.adcolony.sdk.o, com.adcolony.sdk.af):com.adcolony.sdk.af
                  com.adcolony.sdk.o.a(com.adcolony.sdk.o, boolean):boolean */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, boolean):boolean
                 arg types: [org.json.JSONObject, java.lang.String, int]
                 candidates:
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, int):int
                  com.adcolony.sdk.y.a(org.json.JSONArray, java.lang.String[], boolean):org.json.JSONArray
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, double):boolean
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, long):boolean
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, java.lang.String):boolean
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, org.json.JSONArray):boolean
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, org.json.JSONObject):boolean
                  com.adcolony.sdk.y.a(org.json.JSONObject, java.lang.String, boolean):boolean */
                public void onCancel(DialogInterface dialogInterface) {
                    AlertDialog unused = o.this.c = (AlertDialog) null;
                    boolean unused2 = o.this.d = false;
                    JSONObject a2 = y.a();
                    y.a(a2, "positive", false);
                    afVar.a(a2).b();
                }
            });
            aw.a(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.o.a(com.adcolony.sdk.o, boolean):boolean
                 arg types: [com.adcolony.sdk.o, int]
                 candidates:
                  com.adcolony.sdk.o.a(com.adcolony.sdk.o, android.app.AlertDialog):android.app.AlertDialog
                  com.adcolony.sdk.o.a(com.adcolony.sdk.o, com.adcolony.sdk.af):com.adcolony.sdk.af
                  com.adcolony.sdk.o.a(com.adcolony.sdk.o, boolean):boolean */
                public void run() {
                    boolean unused = o.this.d = true;
                    AlertDialog unused2 = o.this.c = builder.show();
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.b != null) {
            a(this.b);
            this.b = null;
        }
    }

    /* access modifiers changed from: package-private */
    public AlertDialog b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void a(AlertDialog alertDialog) {
        this.c = alertDialog;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.d;
    }
}
