package com.adcolony.sdk;

import android.app.Activity;
import com.adcolony.sdk.aa;
import com.miniclip.videoads.ProviderConfig;
import java.lang.ref.WeakReference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class a {
    static boolean a;
    static boolean b;
    private static WeakReference<Activity> c;
    /* access modifiers changed from: private */
    public static l d;

    a() {
    }

    static void a(final Activity activity, AdColonyAppOptions adColonyAppOptions, boolean z) {
        a(activity);
        b = true;
        if (d == null) {
            d = new l();
            d.a(adColonyAppOptions, z);
        } else {
            d.a(adColonyAppOptions);
        }
        aw.b.execute(new Runnable() {
            public void run() {
                a.d.a(activity, (af) null);
            }
        });
        new aa.a().a("Configuring AdColony").a(aa.c);
        d.b(false);
        d.l().d(true);
        d.l().e(true);
        d.l().f(false);
        d.f = true;
        d.l().a(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.l.a(com.adcolony.sdk.AdColonyAppOptions, boolean):void
     arg types: [com.adcolony.sdk.AdColonyAppOptions, int]
     candidates:
      com.adcolony.sdk.l.a(com.adcolony.sdk.l, com.adcolony.sdk.af):void
      com.adcolony.sdk.l.a(com.adcolony.sdk.l, boolean):boolean
      com.adcolony.sdk.l.a(boolean, boolean):boolean
      com.adcolony.sdk.l.a(android.content.Context, com.adcolony.sdk.af):boolean
      com.adcolony.sdk.l.a(com.adcolony.sdk.AdColonyAppOptions, boolean):void */
    static l a() {
        if (!b()) {
            Activity c2 = c();
            if (c2 == null) {
                return new l();
            }
            d = new l();
            JSONObject c3 = y.c(c2.getFilesDir().getAbsolutePath() + "/adc3/AppInfo");
            JSONArray g = y.g(c3, "zoneIds");
            d.a(new AdColonyAppOptions().a(y.b(c3, ProviderConfig.MCVideoAdsAppIdKey)).a(y.a(g)), false);
        }
        return d;
    }

    static boolean b() {
        return d != null;
    }

    static void a(Activity activity) {
        if (activity == null) {
            c.clear();
        } else {
            c = new WeakReference<>(activity);
        }
    }

    static Activity c() {
        if (c == null) {
            return null;
        }
        return c.get();
    }

    static boolean d() {
        return (c == null || c.get() == null) ? false : true;
    }

    static boolean e() {
        return a;
    }

    static void a(String str, ah ahVar) {
        a().q().a(str, ahVar);
    }

    static ah a(String str, ah ahVar, boolean z) {
        a().q().a(str, ahVar);
        return ahVar;
    }

    static void b(String str, ah ahVar) {
        a().q().b(str, ahVar);
    }

    static void f() {
        a().q().b();
    }

    static void a(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = y.a();
        }
        y.a(jSONObject, "m_type", str);
        a().q().a(jSONObject);
    }

    static void a(String str) {
        try {
            af afVar = new af("CustomMessage.send", 0);
            afVar.c().put("message", str);
            afVar.b();
        } catch (JSONException e) {
            new aa.a().a("JSON error from ADC.java's send_custom_message(): ").a(e.toString()).a(aa.h);
        }
    }
}
