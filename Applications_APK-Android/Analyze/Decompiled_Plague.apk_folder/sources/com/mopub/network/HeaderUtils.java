package com.mopub.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.mopub.common.Preconditions;
import com.mopub.common.util.ResponseHeader;
import com.mopub.volley.Header;
import com.mopub.volley.toolbox.HttpResponse;
import com.tapjoy.TapjoyAuctionFlags;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

public class HeaderUtils {
    @Nullable
    public static String extractHeader(Map<String, String> map, ResponseHeader responseHeader) {
        return map.get(responseHeader.getKey().toLowerCase());
    }

    @NonNull
    public static String extractHeader(@Nullable JSONObject jSONObject, @NonNull ResponseHeader responseHeader) {
        Preconditions.checkNotNull(responseHeader);
        if (jSONObject == null) {
            return "";
        }
        return jSONObject.optString(getKeyIgnoreCase(jSONObject, responseHeader.getKey()));
    }

    @Nullable
    public static Integer extractIntegerHeader(JSONObject jSONObject, ResponseHeader responseHeader) {
        return formatIntHeader(extractHeader(jSONObject, responseHeader));
    }

    public static boolean extractBooleanHeader(Map<String, String> map, ResponseHeader responseHeader, boolean z) {
        return formatBooleanHeader(extractHeader(map, responseHeader), z);
    }

    public static boolean extractBooleanHeader(JSONObject jSONObject, ResponseHeader responseHeader, boolean z) {
        return formatBooleanHeader(extractHeader(jSONObject, responseHeader), z);
    }

    @Nullable
    public static Integer extractPercentHeader(JSONObject jSONObject, ResponseHeader responseHeader) {
        return formatPercentHeader(extractHeader(jSONObject, responseHeader));
    }

    @Nullable
    public static String extractPercentHeaderString(JSONObject jSONObject, ResponseHeader responseHeader) {
        Integer extractPercentHeader = extractPercentHeader(jSONObject, responseHeader);
        if (extractPercentHeader != null) {
            return extractPercentHeader.toString();
        }
        return null;
    }

    @Nullable
    public static String extractHeader(HttpResponse httpResponse, ResponseHeader responseHeader) {
        Header firstHeader = getFirstHeader(httpResponse.getHeaders(), responseHeader);
        if (firstHeader != null) {
            return firstHeader.getValue();
        }
        return null;
    }

    public static boolean extractBooleanHeader(HttpResponse httpResponse, ResponseHeader responseHeader, boolean z) {
        return formatBooleanHeader(extractHeader(httpResponse, responseHeader), z);
    }

    @Nullable
    public static Integer extractIntegerHeader(HttpResponse httpResponse, ResponseHeader responseHeader) {
        return formatIntHeader(extractHeader(httpResponse, responseHeader));
    }

    public static int extractIntHeader(HttpResponse httpResponse, ResponseHeader responseHeader, int i) {
        Integer extractIntegerHeader = extractIntegerHeader(httpResponse, responseHeader);
        if (extractIntegerHeader == null) {
            return i;
        }
        return extractIntegerHeader.intValue();
    }

    private static boolean formatBooleanHeader(@Nullable String str, boolean z) {
        return str == null ? z : str.equals(TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE);
    }

    @Nullable
    private static Integer formatIntHeader(String str) {
        try {
            return Integer.valueOf(Integer.parseInt(str));
        } catch (Exception unused) {
            NumberFormat instance = NumberFormat.getInstance(Locale.US);
            instance.setParseIntegerOnly(true);
            try {
                return Integer.valueOf(instance.parse(str.trim()).intValue());
            } catch (Exception unused2) {
                return null;
            }
        }
    }

    @Nullable
    private static Integer formatPercentHeader(@Nullable String str) {
        Integer formatIntHeader;
        if (str != null && (formatIntHeader = formatIntHeader(str.replace("%", ""))) != null && formatIntHeader.intValue() >= 0 && formatIntHeader.intValue() <= 100) {
            return formatIntHeader;
        }
        return null;
    }

    private static Header getFirstHeader(@Nullable List<Header> list, @NonNull ResponseHeader responseHeader) {
        Preconditions.checkNotNull(responseHeader);
        if (list == null) {
            return null;
        }
        for (Header next : list) {
            if (next.getName().equalsIgnoreCase(responseHeader.getKey())) {
                return next;
            }
        }
        return null;
    }

    @NonNull
    private static String getKeyIgnoreCase(@NonNull JSONObject jSONObject, @NonNull String str) {
        Preconditions.checkNotNull(jSONObject);
        Preconditions.checkNotNull(str);
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            if (str.equalsIgnoreCase(next)) {
                return next;
            }
        }
        return str;
    }
}
