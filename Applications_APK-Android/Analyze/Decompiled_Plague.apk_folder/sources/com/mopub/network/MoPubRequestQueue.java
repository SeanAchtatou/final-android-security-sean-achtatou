package com.mopub.network;

import android.os.Handler;
import android.support.annotation.NonNull;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.volley.Cache;
import com.mopub.volley.Network;
import com.mopub.volley.Request;
import com.mopub.volley.RequestQueue;
import com.mopub.volley.ResponseDelivery;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MoPubRequestQueue extends RequestQueue {
    private static final int CAPACITY = 10;
    /* access modifiers changed from: private */
    @NonNull
    public final Map<Request<?>, DelayedRequestHelper> mDelayedRequests = new HashMap(10);

    MoPubRequestQueue(Cache cache, Network network, int i, ResponseDelivery responseDelivery) {
        super(cache, network, i, responseDelivery);
    }

    MoPubRequestQueue(Cache cache, Network network, int i) {
        super(cache, network, i);
    }

    MoPubRequestQueue(Cache cache, Network network) {
        super(cache, network);
    }

    public void addDelayedRequest(@NonNull Request<?> request, int i) {
        Preconditions.checkNotNull(request);
        addDelayedRequest(request, new DelayedRequestHelper(this, request, i));
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void addDelayedRequest(@NonNull Request<?> request, @NonNull DelayedRequestHelper delayedRequestHelper) {
        Preconditions.checkNotNull(delayedRequestHelper);
        if (this.mDelayedRequests.containsKey(request)) {
            cancel(request);
        }
        delayedRequestHelper.start();
        this.mDelayedRequests.put(request, delayedRequestHelper);
    }

    public void cancelAll(@NonNull RequestQueue.RequestFilter requestFilter) {
        Preconditions.checkNotNull(requestFilter);
        super.cancelAll(requestFilter);
        Iterator<Map.Entry<Request<?>, DelayedRequestHelper>> it = this.mDelayedRequests.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            if (requestFilter.apply((Request) next.getKey())) {
                ((Request) next.getKey()).cancel();
                ((DelayedRequestHelper) next.getValue()).cancel();
                it.remove();
            }
        }
    }

    public void cancelAll(@NonNull final Object obj) {
        Preconditions.checkNotNull(obj);
        super.cancelAll(obj);
        cancelAll((RequestQueue.RequestFilter) new RequestQueue.RequestFilter() {
            public boolean apply(Request<?> request) {
                return request.getTag() == obj;
            }
        });
    }

    public void cancel(@NonNull final Request<?> request) {
        Preconditions.checkNotNull(request);
        cancelAll((RequestQueue.RequestFilter) new RequestQueue.RequestFilter() {
            public boolean apply(Request<?> request) {
                return request == request;
            }
        });
    }

    class DelayedRequestHelper {
        final int mDelayMs;
        @NonNull
        final Runnable mDelayedRunnable;
        @NonNull
        final Handler mHandler;

        DelayedRequestHelper(@NonNull MoPubRequestQueue moPubRequestQueue, Request<?> request, int i) {
            this(request, i, new Handler());
        }

        @VisibleForTesting
        DelayedRequestHelper(@NonNull final Request<?> request, int i, @NonNull Handler handler) {
            this.mDelayMs = i;
            this.mHandler = handler;
            this.mDelayedRunnable = new Runnable(MoPubRequestQueue.this) {
                public void run() {
                    MoPubRequestQueue.this.mDelayedRequests.remove(request);
                    MoPubRequestQueue.this.add(request);
                }
            };
        }

        /* access modifiers changed from: package-private */
        public void start() {
            this.mHandler.postDelayed(this.mDelayedRunnable, (long) this.mDelayMs);
        }

        /* access modifiers changed from: package-private */
        public void cancel() {
            this.mHandler.removeCallbacks(this.mDelayedRunnable);
        }
    }

    /* access modifiers changed from: package-private */
    @NonNull
    @Deprecated
    @VisibleForTesting
    public Map<Request<?>, DelayedRequestHelper> getDelayedRequests() {
        return this.mDelayedRequests;
    }
}
