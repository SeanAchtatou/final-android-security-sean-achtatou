package com.mopub.network;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.mopub.common.AdFormat;
import com.mopub.common.AdType;
import com.mopub.common.DataKeys;
import com.mopub.common.ExternalViewabilitySessionManager;
import com.mopub.common.FullAdType;
import com.mopub.common.MoPub;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.common.util.Json;
import com.mopub.common.util.ResponseHeader;
import com.mopub.mobileads.AdTypeTranslator;
import com.mopub.network.AdResponse;
import com.mopub.network.MoPubNetworkError;
import com.mopub.volley.DefaultRetryPolicy;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Response;
import com.mopub.volley.toolbox.HttpHeaderParser;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import org.json.JSONException;
import org.json.JSONObject;

public class AdRequest extends MoPubRequest<AdResponse> {
    private static final String ADM_KEY = "adm";
    @VisibleForTesting
    static final String AD_RESPONSES_KEY = "ad-responses";
    private static final String BODY_KEY = "body";
    private static final String HEADERS_KEY = "headers";
    @Nullable
    private static ServerOverrideListener sServerOverrideListener;
    @NonNull
    private final AdFormat mAdFormat;
    @Nullable
    private final String mAdUnitId;
    @NonNull
    private final Context mContext;
    @NonNull
    private final Listener mListener;

    public interface Listener extends Response.ErrorListener {
        void onSuccess(AdResponse adResponse);
    }

    public interface ServerOverrideListener {
        void onForceExplicitNo(@Nullable String str);

        void onForceGdprApplies();

        void onInvalidateConsent(@Nullable String str);

        void onReacquireConsent(@Nullable String str);
    }

    public AdRequest(@NonNull String str, @NonNull AdFormat adFormat, @Nullable String str2, @NonNull Context context, @NonNull Listener listener) {
        super(context, clearUrlIfSdkNotInitialized(str), listener);
        Preconditions.checkNotNull(adFormat);
        Preconditions.checkNotNull(listener);
        this.mAdUnitId = str2;
        this.mListener = listener;
        this.mAdFormat = adFormat;
        this.mContext = context.getApplicationContext();
        setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 1, 1.0f));
        setShouldCache(false);
        PersonalInfoManager personalInformationManager = MoPub.getPersonalInformationManager();
        if (personalInformationManager != null) {
            personalInformationManager.requestSync(false);
        }
    }

    @NonNull
    private static String clearUrlIfSdkNotInitialized(@NonNull String str) {
        if (MoPub.getPersonalInformationManager() != null && MoPub.isSdkInitialized()) {
            return str;
        }
        MoPubLog.e("Make sure to call MoPub#initializeSdk before loading an ad.");
        return "";
    }

    @NonNull
    public Listener getListener() {
        return this.mListener;
    }

    public static void setServerOverrideListener(@NonNull ServerOverrideListener serverOverrideListener) {
        sServerOverrideListener = serverOverrideListener;
    }

    public Map<String, String> getHeaders() {
        TreeMap treeMap = new TreeMap();
        String language = Locale.getDefault().getLanguage();
        Locale locale = this.mContext.getResources().getConfiguration().locale;
        if (locale != null && !locale.getLanguage().trim().isEmpty()) {
            language = locale.getLanguage().trim();
        }
        if (!language.isEmpty()) {
            treeMap.put(ResponseHeader.ACCEPT_LANGUAGE.getKey(), language);
        }
        return treeMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mopub.network.HeaderUtils.extractBooleanHeader(java.util.Map<java.lang.String, java.lang.String>, com.mopub.common.util.ResponseHeader, boolean):boolean
     arg types: [java.util.HashMap, com.mopub.common.util.ResponseHeader, int]
     candidates:
      com.mopub.network.HeaderUtils.extractBooleanHeader(com.mopub.volley.toolbox.HttpResponse, com.mopub.common.util.ResponseHeader, boolean):boolean
      com.mopub.network.HeaderUtils.extractBooleanHeader(org.json.JSONObject, com.mopub.common.util.ResponseHeader, boolean):boolean
      com.mopub.network.HeaderUtils.extractBooleanHeader(java.util.Map<java.lang.String, java.lang.String>, com.mopub.common.util.ResponseHeader, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mopub.network.HeaderUtils.extractBooleanHeader(org.json.JSONObject, com.mopub.common.util.ResponseHeader, boolean):boolean
     arg types: [org.json.JSONObject, com.mopub.common.util.ResponseHeader, int]
     candidates:
      com.mopub.network.HeaderUtils.extractBooleanHeader(com.mopub.volley.toolbox.HttpResponse, com.mopub.common.util.ResponseHeader, boolean):boolean
      com.mopub.network.HeaderUtils.extractBooleanHeader(java.util.Map<java.lang.String, java.lang.String>, com.mopub.common.util.ResponseHeader, boolean):boolean
      com.mopub.network.HeaderUtils.extractBooleanHeader(org.json.JSONObject, com.mopub.common.util.ResponseHeader, boolean):boolean */
    /* access modifiers changed from: protected */
    public Response<AdResponse> parseNetworkResponse(NetworkResponse networkResponse) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        Integer num;
        ExternalViewabilitySessionManager.ViewabilityVendor fromKey;
        HashMap hashMap = new HashMap();
        for (String next : networkResponse.headers.keySet()) {
            hashMap.put(next.toLowerCase(), networkResponse.headers.get(next));
        }
        if (HeaderUtils.extractBooleanHeader((Map<String, String>) hashMap, ResponseHeader.WARMUP, false)) {
            return Response.error(new MoPubNetworkError("Ad Unit is warming up.", MoPubNetworkError.Reason.WARMING_UP));
        }
        AdResponse.Builder builder = new AdResponse.Builder();
        builder.setAdUnitId(this.mAdUnitId);
        String parseStringBody = parseStringBody(networkResponse);
        builder.setResponseBody(parseStringBody);
        Integer num2 = null;
        if (AdType.MULTI.equalsIgnoreCase(HeaderUtils.extractHeader(hashMap, ResponseHeader.AD_RESPONSE_TYPE))) {
            try {
                jSONObject2 = new JSONObject(parseStringBody).getJSONArray(AD_RESPONSES_KEY).getJSONObject(0);
                jSONObject = jSONObject2.getJSONObject(HEADERS_KEY);
            } catch (JSONException e) {
                return Response.error(new MoPubNetworkError("Failed to decode header JSON", e, MoPubNetworkError.Reason.BAD_HEADER_DATA));
            }
        } else {
            jSONObject = new JSONObject(hashMap);
            jSONObject2 = null;
        }
        String extractHeader = HeaderUtils.extractHeader(jSONObject, ResponseHeader.AD_TYPE);
        String extractHeader2 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.FULL_AD_TYPE);
        builder.setAdType(extractHeader);
        builder.setFullAdType(extractHeader2);
        Integer extractIntegerHeader = HeaderUtils.extractIntegerHeader(jSONObject, ResponseHeader.REFRESH_TIME);
        if (extractIntegerHeader == null) {
            num = null;
        } else {
            num = Integer.valueOf(extractIntegerHeader.intValue() * 1000);
        }
        builder.setRefreshTimeMilliseconds(num);
        if (AdType.CLEAR.equals(extractHeader)) {
            builder.build();
            return Response.error(new MoPubNetworkError("No ads found for ad unit.", MoPubNetworkError.Reason.NO_FILL, num));
        }
        builder.setDspCreativeId(HeaderUtils.extractHeader(jSONObject, ResponseHeader.DSP_CREATIVE_ID));
        builder.setNetworkType(HeaderUtils.extractHeader(jSONObject, ResponseHeader.NETWORK_TYPE));
        String extractHeader3 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.REDIRECT_URL);
        builder.setRedirectUrl(extractHeader3);
        String extractHeader4 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.CLICK_TRACKING_URL);
        builder.setClickTrackingUrl(extractHeader4);
        builder.setImpressionTrackingUrl(HeaderUtils.extractHeader(jSONObject, ResponseHeader.IMPRESSION_URL));
        String extractHeader5 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.FAIL_URL);
        builder.setFailoverUrl(extractHeader5);
        builder.setRequestId(getRequestId(extractHeader5));
        boolean extractBooleanHeader = HeaderUtils.extractBooleanHeader(jSONObject, ResponseHeader.SCROLLABLE, false);
        builder.setScrollable(Boolean.valueOf(extractBooleanHeader));
        builder.setDimensions(HeaderUtils.extractIntegerHeader(jSONObject, ResponseHeader.WIDTH), HeaderUtils.extractIntegerHeader(jSONObject, ResponseHeader.HEIGHT));
        Integer extractIntegerHeader2 = HeaderUtils.extractIntegerHeader(jSONObject, ResponseHeader.AD_TIMEOUT);
        if (extractIntegerHeader2 != null) {
            num2 = Integer.valueOf(extractIntegerHeader2.intValue() * 1000);
        }
        builder.setAdTimeoutDelayMilliseconds(num2);
        if (AdType.STATIC_NATIVE.equals(extractHeader) || AdType.VIDEO_NATIVE.equals(extractHeader)) {
            try {
                builder.setJsonBody(new JSONObject(parseStringBody));
            } catch (JSONException e2) {
                return Response.error(new MoPubNetworkError("Failed to decode body JSON for native ad format", e2, MoPubNetworkError.Reason.BAD_BODY));
            }
        }
        builder.setCustomEventClassName(AdTypeTranslator.getCustomEventName(this.mAdFormat, extractHeader, extractHeader2, jSONObject));
        MoPub.BrowserAgent fromHeader = MoPub.BrowserAgent.fromHeader(HeaderUtils.extractIntegerHeader(jSONObject, ResponseHeader.BROWSER_AGENT));
        MoPub.setBrowserAgentFromAdServer(fromHeader);
        builder.setBrowserAgent(fromHeader);
        String extractHeader6 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.CUSTOM_EVENT_DATA);
        if (TextUtils.isEmpty(extractHeader6)) {
            extractHeader6 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.NATIVE_PARAMS);
        }
        try {
            Map<String, String> jsonStringToMap = Json.jsonStringToMap(extractHeader6);
            if (jSONObject2 != null) {
                try {
                    jsonStringToMap.put(DataKeys.ADM_KEY, jSONObject2.getString(ADM_KEY));
                } catch (JSONException e3) {
                    return Response.error(new MoPubNetworkError("Failed to parse ADM for advanced bidding", e3, MoPubNetworkError.Reason.BAD_BODY));
                }
            }
            if (!TextUtils.isEmpty(extractHeader3)) {
                jsonStringToMap.put(DataKeys.REDIRECT_URL_KEY, extractHeader3);
            }
            if (!TextUtils.isEmpty(extractHeader4)) {
                jsonStringToMap.put(DataKeys.CLICKTHROUGH_URL_KEY, extractHeader4);
            }
            if (eventDataIsInResponseBody(extractHeader, extractHeader2)) {
                jsonStringToMap.put(DataKeys.HTML_RESPONSE_BODY_KEY, parseStringBody);
                jsonStringToMap.put(DataKeys.SCROLLABLE_KEY, Boolean.toString(extractBooleanHeader));
                jsonStringToMap.put(DataKeys.CREATIVE_ORIENTATION_KEY, HeaderUtils.extractHeader(jSONObject, ResponseHeader.ORIENTATION));
            }
            if (AdType.STATIC_NATIVE.equals(extractHeader) || AdType.VIDEO_NATIVE.equals(extractHeader)) {
                String extractPercentHeaderString = HeaderUtils.extractPercentHeaderString(jSONObject, ResponseHeader.IMPRESSION_MIN_VISIBLE_PERCENT);
                String extractHeader7 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.IMPRESSION_VISIBLE_MS);
                String extractHeader8 = HeaderUtils.extractHeader(hashMap, ResponseHeader.IMPRESSION_MIN_VISIBLE_PX);
                if (!TextUtils.isEmpty(extractPercentHeaderString)) {
                    jsonStringToMap.put(DataKeys.IMPRESSION_MIN_VISIBLE_PERCENT, extractPercentHeaderString);
                }
                if (!TextUtils.isEmpty(extractHeader7)) {
                    jsonStringToMap.put(DataKeys.IMPRESSION_VISIBLE_MS, extractHeader7);
                }
                if (!TextUtils.isEmpty(extractHeader8)) {
                    jsonStringToMap.put(DataKeys.IMPRESSION_MIN_VISIBLE_PX, extractHeader8);
                }
            }
            if (AdType.VIDEO_NATIVE.equals(extractHeader)) {
                jsonStringToMap.put(DataKeys.PLAY_VISIBLE_PERCENT, HeaderUtils.extractPercentHeaderString(jSONObject, ResponseHeader.PLAY_VISIBLE_PERCENT));
                jsonStringToMap.put(DataKeys.PAUSE_VISIBLE_PERCENT, HeaderUtils.extractPercentHeaderString(jSONObject, ResponseHeader.PAUSE_VISIBLE_PERCENT));
                jsonStringToMap.put(DataKeys.MAX_BUFFER_MS, HeaderUtils.extractHeader(jSONObject, ResponseHeader.MAX_BUFFER_MS));
            }
            String extractHeader9 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.VIDEO_TRACKERS);
            if (!TextUtils.isEmpty(extractHeader9)) {
                jsonStringToMap.put(DataKeys.VIDEO_TRACKERS_KEY, extractHeader9);
            }
            if (AdType.REWARDED_VIDEO.equals(extractHeader) || (AdType.INTERSTITIAL.equals(extractHeader) && FullAdType.VAST.equals(extractHeader2))) {
                jsonStringToMap.put(DataKeys.EXTERNAL_VIDEO_VIEWABILITY_TRACKERS_KEY, HeaderUtils.extractHeader(jSONObject, ResponseHeader.VIDEO_VIEWABILITY_TRACKERS));
            }
            if (AdFormat.BANNER.equals(this.mAdFormat)) {
                jsonStringToMap.put(DataKeys.BANNER_IMPRESSION_MIN_VISIBLE_MS, HeaderUtils.extractHeader(hashMap, ResponseHeader.BANNER_IMPRESSION_MIN_VISIBLE_MS));
                jsonStringToMap.put(DataKeys.BANNER_IMPRESSION_MIN_VISIBLE_DIPS, HeaderUtils.extractHeader(hashMap, ResponseHeader.BANNER_IMPRESSION_MIN_VISIBLE_DIPS));
            }
            String extractHeader10 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.DISABLE_VIEWABILITY);
            if (!TextUtils.isEmpty(extractHeader10) && (fromKey = ExternalViewabilitySessionManager.ViewabilityVendor.fromKey(extractHeader10)) != null) {
                fromKey.disable();
            }
            builder.setServerExtras(jsonStringToMap);
            if (AdType.REWARDED_VIDEO.equals(extractHeader) || AdType.CUSTOM.equals(extractHeader) || AdType.REWARDED_PLAYABLE.equals(extractHeader)) {
                String extractHeader11 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.REWARDED_VIDEO_CURRENCY_NAME);
                String extractHeader12 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.REWARDED_VIDEO_CURRENCY_AMOUNT);
                String extractHeader13 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.REWARDED_CURRENCIES);
                String extractHeader14 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.REWARDED_VIDEO_COMPLETION_URL);
                Integer extractIntegerHeader3 = HeaderUtils.extractIntegerHeader(jSONObject, ResponseHeader.REWARDED_DURATION);
                boolean extractBooleanHeader2 = HeaderUtils.extractBooleanHeader(jSONObject, ResponseHeader.SHOULD_REWARD_ON_CLICK, false);
                builder.setRewardedVideoCurrencyName(extractHeader11);
                builder.setRewardedVideoCurrencyAmount(extractHeader12);
                builder.setRewardedCurrencies(extractHeader13);
                builder.setRewardedVideoCompletionUrl(extractHeader14);
                builder.setRewardedDuration(extractIntegerHeader3);
                builder.setShouldRewardOnClick(extractBooleanHeader2);
            }
            boolean extractBooleanHeader3 = HeaderUtils.extractBooleanHeader(jSONObject, ResponseHeader.INVALIDATE_CONSENT, false);
            boolean extractBooleanHeader4 = HeaderUtils.extractBooleanHeader(jSONObject, ResponseHeader.FORCE_EXPLICIT_NO, false);
            boolean extractBooleanHeader5 = HeaderUtils.extractBooleanHeader(jSONObject, ResponseHeader.REACQUIRE_CONSENT, false);
            String extractHeader15 = HeaderUtils.extractHeader(jSONObject, ResponseHeader.CONSENT_CHANGE_REASON);
            boolean extractBooleanHeader6 = HeaderUtils.extractBooleanHeader(jSONObject, ResponseHeader.FORCE_GDPR_APPLIES, false);
            if (sServerOverrideListener != null) {
                if (extractBooleanHeader6) {
                    sServerOverrideListener.onForceGdprApplies();
                }
                if (extractBooleanHeader4) {
                    sServerOverrideListener.onForceExplicitNo(extractHeader15);
                } else if (extractBooleanHeader3) {
                    sServerOverrideListener.onInvalidateConsent(extractHeader15);
                } else if (extractBooleanHeader5) {
                    sServerOverrideListener.onReacquireConsent(extractHeader15);
                }
            }
            builder.build();
            return Response.success(builder.build(), HttpHeaderParser.parseCacheHeaders(networkResponse));
        } catch (JSONException e4) {
            return Response.error(new MoPubNetworkError("Failed to decode server extras for custom event data.", e4, MoPubNetworkError.Reason.BAD_HEADER_DATA));
        }
    }

    private boolean eventDataIsInResponseBody(@Nullable String str, @Nullable String str2) {
        return AdType.MRAID.equals(str) || "html".equals(str) || (AdType.INTERSTITIAL.equals(str) && FullAdType.VAST.equals(str2)) || ((AdType.REWARDED_VIDEO.equals(str) && FullAdType.VAST.equals(str2)) || AdType.REWARDED_PLAYABLE.equals(str));
    }

    /* access modifiers changed from: protected */
    public void deliverResponse(AdResponse adResponse) {
        this.mListener.onSuccess(adResponse);
    }

    /* access modifiers changed from: package-private */
    @Nullable
    @VisibleForTesting
    public String getRequestId(@Nullable String str) {
        if (str == null) {
            return null;
        }
        try {
            return Uri.parse(str).getQueryParameter("request_id");
        } catch (UnsupportedOperationException unused) {
            MoPubLog.d("Unable to obtain request id from fail url.");
            return null;
        }
    }
}
