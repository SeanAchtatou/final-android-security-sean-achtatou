package com.mopub.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.VastErrorCode;
import com.mopub.mobileads.VastMacroHelper;
import com.mopub.mobileads.VastTracker;
import com.mopub.network.MoPubNetworkError;
import com.mopub.volley.DefaultRetryPolicy;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Response;
import com.mopub.volley.VolleyError;
import com.mopub.volley.toolbox.HttpHeaderParser;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TrackingRequest extends MoPubRequest<Void> {
    private static final int ZERO_RETRIES = 0;
    @Nullable
    private final Listener mListener;

    public interface Listener extends Response.ErrorListener {
        void onResponse(@NonNull String str);
    }

    private TrackingRequest(@NonNull Context context, @NonNull String str, @Nullable Listener listener) {
        super(context, str, listener);
        this.mListener = listener;
        setShouldCache(false);
        setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 1.0f));
    }

    /* access modifiers changed from: protected */
    public Response<Void> parseNetworkResponse(NetworkResponse networkResponse) {
        if (networkResponse.statusCode == 200) {
            return Response.success(null, HttpHeaderParser.parseCacheHeaders(networkResponse));
        }
        return Response.error(new MoPubNetworkError("Failed to log tracking request. Response code: " + networkResponse.statusCode + " for url: " + getUrl(), MoPubNetworkError.Reason.TRACKING_FAILURE));
    }

    public void deliverResponse(Void voidR) {
        if (this.mListener != null) {
            this.mListener.onResponse(getUrl());
        }
    }

    public static void makeVastTrackingHttpRequest(@NonNull List<VastTracker> list, @Nullable VastErrorCode vastErrorCode, @Nullable Integer num, @Nullable String str, @Nullable Context context) {
        Preconditions.checkNotNull(list);
        ArrayList arrayList = new ArrayList(list.size());
        for (VastTracker next : list) {
            if (next != null && (!next.isTracked() || next.isRepeatable())) {
                arrayList.add(next.getContent());
                next.setTracked();
            }
        }
        makeTrackingHttpRequest(new VastMacroHelper(arrayList).withErrorCode(vastErrorCode).withContentPlayHead(num).withAssetUri(str).getUris(), context);
    }

    public static void makeTrackingHttpRequest(@Nullable Iterable<String> iterable, @Nullable Context context, @Nullable final Listener listener) {
        if (iterable != null && context != null) {
            MoPubRequestQueue requestQueue = Networking.getRequestQueue(context);
            for (final String next : iterable) {
                if (!TextUtils.isEmpty(next)) {
                    requestQueue.add(new TrackingRequest(context, next, new Listener() {
                        public void onResponse(@NonNull String str) {
                            MoPubLog.d("Successfully hit tracking endpoint: " + str);
                            if (listener != null) {
                                listener.onResponse(str);
                            }
                        }

                        public void onErrorResponse(VolleyError volleyError) {
                            MoPubLog.d("Failed to hit tracking endpoint: " + next);
                            if (listener != null) {
                                listener.onErrorResponse(volleyError);
                            }
                        }
                    }));
                }
            }
        }
    }

    public static void makeTrackingHttpRequest(@Nullable String str, @Nullable Context context) {
        makeTrackingHttpRequest(str, context, (Listener) null);
    }

    public static void makeTrackingHttpRequest(@Nullable String str, @Nullable Context context, @Nullable Listener listener) {
        if (str != null) {
            makeTrackingHttpRequest(Arrays.asList(str), context, listener);
        }
    }

    public static void makeTrackingHttpRequest(@Nullable Iterable<String> iterable, @Nullable Context context) {
        makeTrackingHttpRequest(iterable, context, (Listener) null);
    }
}
