package com.mopub.nativeads;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.View;
import com.integralads.avid.library.mopub.BuildConfig;
import com.millennialmedia.AppInfo;
import com.millennialmedia.CreativeInfo;
import com.millennialmedia.MMException;
import com.millennialmedia.MMLog;
import com.millennialmedia.MMSDK;
import com.millennialmedia.NativeAd;
import com.millennialmedia.internal.ActivityListenerManager;
import com.mopub.common.MoPub;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.ConsentStatus;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.mobileads.MillennialUtils;
import com.mopub.nativeads.CustomEventNative;
import com.mopub.nativeads.NativeImageHelper;
import com.tapjoy.TapjoyAuctionFlags;
import java.util.ArrayList;
import java.util.Map;

public class MillennialNative extends CustomEventNative {
    private static final String APID_KEY = "adUnitID";
    private static final String DCN_KEY = "dcn";
    private static final String TAG = "MillennialNative";
    MillennialStaticNativeAd staticNativeAd;

    static {
        MoPubLog.d("Millennial Media Adapter Version: MoPubMM-1.3.0");
    }

    public CreativeInfo getCreativeInfo() {
        if (this.staticNativeAd == null) {
            return null;
        }
        return this.staticNativeAd.getCreativeInfo();
    }

    /* access modifiers changed from: protected */
    public void loadNativeAd(Context context, CustomEventNative.CustomEventNativeListener customEventNativeListener, Map<String, Object> map, Map<String, String> map2) {
        PersonalInfoManager personalInformationManager = MoPub.getPersonalInformationManager();
        if (personalInformationManager != null) {
            try {
                Boolean gdprApplies = personalInformationManager.gdprApplies();
                if (gdprApplies != null) {
                    MMSDK.setConsentRequired(gdprApplies.booleanValue());
                }
            } catch (NullPointerException e) {
                MoPubLog.d("GDPR applicability cannot be determined.", e);
            }
            if (personalInformationManager.getPersonalInfoConsentStatus() == ConsentStatus.EXPLICIT_YES) {
                MMSDK.setConsentData(BuildConfig.SDK_NAME, TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE);
            }
        }
        if (context instanceof Activity) {
            try {
                MMSDK.initialize((Activity) context, ActivityListenerManager.LifecycleState.RESUMED);
            } catch (IllegalStateException e2) {
                MoPubLog.d("Exception occurred initializing the MM SDK.", e2);
                customEventNativeListener.onNativeAdFailed(NativeErrorCode.NATIVE_ADAPTER_CONFIGURATION_ERROR);
                return;
            }
        } else if (context instanceof Application) {
            try {
                MMSDK.initialize((Application) context);
            } catch (MMException e3) {
                MoPubLog.d("Exception occurred initializing the MM SDK.", e3);
                customEventNativeListener.onNativeAdFailed(NativeErrorCode.NATIVE_ADAPTER_CONFIGURATION_ERROR);
                return;
            }
        } else {
            MoPubLog.d("MM SDK must be initialized with an Activity or Application context.");
            customEventNativeListener.onNativeAdFailed(NativeErrorCode.NATIVE_ADAPTER_CONFIGURATION_ERROR);
            return;
        }
        String str = map2.get("adUnitID");
        String str2 = map2.get(DCN_KEY);
        if (MillennialUtils.isEmpty(str)) {
            customEventNativeListener.onNativeAdFailed(NativeErrorCode.NATIVE_ADAPTER_CONFIGURATION_ERROR);
            return;
        }
        try {
            MMSDK.setAppInfo(new AppInfo().setMediator(MillennialUtils.MEDIATOR_ID).setSiteId(str2));
            this.staticNativeAd = new MillennialStaticNativeAd(context, NativeAd.createInstance(str, "inline"), new ImpressionTracker(context), new NativeClickHandler(context), customEventNativeListener);
            this.staticNativeAd.loadAd();
        } catch (MMException e4) {
            MoPubLog.d("An exception occurred loading a native ad from MM SDK", e4);
            customEventNativeListener.onNativeAdFailed(NativeErrorCode.NATIVE_ADAPTER_CONFIGURATION_ERROR);
        }
    }

    static class MillennialStaticNativeAd extends StaticNativeAd implements NativeAd.NativeListener {
        /* access modifiers changed from: private */
        public final Context context;
        private final ImpressionTracker impressionTracker;
        /* access modifiers changed from: private */
        public final CustomEventNative.CustomEventNativeListener listener;
        private NativeAd nativeAd;
        private final NativeClickHandler nativeClickHandler;

        private MillennialStaticNativeAd(Context context2, NativeAd nativeAd2, ImpressionTracker impressionTracker2, NativeClickHandler nativeClickHandler2, CustomEventNative.CustomEventNativeListener customEventNativeListener) {
            this.context = context2.getApplicationContext();
            this.nativeAd = nativeAd2;
            this.impressionTracker = impressionTracker2;
            this.nativeClickHandler = nativeClickHandler2;
            this.listener = customEventNativeListener;
            nativeAd2.setListener(this);
        }

        /* access modifiers changed from: package-private */
        public void loadAd() throws MMException {
            MoPubLog.d("Millennial native ad loading.");
            this.nativeAd.load(this.context, null);
        }

        /* access modifiers changed from: package-private */
        public CreativeInfo getCreativeInfo() {
            if (this.nativeAd == null) {
                return null;
            }
            return this.nativeAd.getCreativeInfo();
        }

        public void prepare(View view) {
            this.nativeAd.getIconImage();
            this.nativeAd.getDisclaimer();
            this.impressionTracker.addView(view, this);
            this.nativeClickHandler.setOnClickListener(view, this);
        }

        public void clear(View view) {
            this.impressionTracker.removeView(view);
            this.nativeClickHandler.clearOnClickListener(view);
        }

        public void destroy() {
            this.impressionTracker.destroy();
            this.nativeAd.destroy();
            this.nativeAd = null;
        }

        public void recordImpression(View view) {
            notifyAdImpressed();
            try {
                this.nativeAd.fireImpression();
                MoPubLog.d("Millennial native ad impression recorded.");
            } catch (MMException e) {
                MoPubLog.d("Error tracking Millennial native ad impression", e);
            }
        }

        public void handleClick(View view) {
            notifyAdClicked();
            this.nativeClickHandler.openClickDestinationUrl(getClickDestinationUrl(), view);
            this.nativeAd.fireCallToActionClicked();
            MoPubLog.d("Millennial native ad clicked.");
        }

        public void onLoaded(NativeAd nativeAd2) {
            CreativeInfo creativeInfo = getCreativeInfo();
            if (creativeInfo != null && MMLog.isDebugEnabled()) {
                MoPubLog.d("Native Creative Info: " + creativeInfo);
            }
            String imageUrl = nativeAd2.getImageUrl(NativeAd.ComponentName.ICON_IMAGE, 1);
            String imageUrl2 = nativeAd2.getImageUrl(NativeAd.ComponentName.MAIN_IMAGE, 1);
            setTitle(nativeAd2.getTitle().getText().toString());
            setText(nativeAd2.getBody().getText().toString());
            setCallToAction(nativeAd2.getCallToActionButton().getText().toString());
            String callToActionUrl = nativeAd2.getCallToActionUrl();
            if (callToActionUrl == null) {
                MillennialUtils.postOnUiThread(new Runnable() {
                    public void run() {
                        MoPubLog.d("Millennial native ad encountered null destination url.");
                        MillennialStaticNativeAd.this.listener.onNativeAdFailed(NativeErrorCode.NATIVE_ADAPTER_CONFIGURATION_ERROR);
                    }
                });
                return;
            }
            setClickDestinationUrl(callToActionUrl);
            setIconImageUrl(imageUrl);
            setMainImageUrl(imageUrl2);
            final ArrayList arrayList = new ArrayList();
            if (imageUrl != null) {
                arrayList.add(imageUrl);
            }
            if (imageUrl2 != null) {
                arrayList.add(imageUrl2);
            }
            addExtra(NativeAd.COMPONENT_ID_DISCLAIMER, nativeAd2.getDisclaimer().getText());
            addExtra(NativeAd.COMPONENT_ID_RATING, nativeAd2.getRating().getText());
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    NativeImageHelper.preCacheImages(MillennialStaticNativeAd.this.context, arrayList, new NativeImageHelper.ImageListener() {
                        public void onImagesCached() {
                            MillennialStaticNativeAd.this.listener.onNativeAdLoaded(MillennialStaticNativeAd.this);
                            MoPubLog.d("Millennial native ad loaded.");
                        }

                        public void onImagesFailedToCache(NativeErrorCode nativeErrorCode) {
                            MillennialStaticNativeAd.this.listener.onNativeAdFailed(nativeErrorCode);
                        }
                    });
                }
            });
        }

        public void onLoadFailed(NativeAd nativeAd2, NativeAd.NativeErrorStatus nativeErrorStatus) {
            final NativeErrorCode nativeErrorCode;
            int errorCode = nativeErrorStatus.getErrorCode();
            if (errorCode != 301) {
                switch (errorCode) {
                    case 1:
                        nativeErrorCode = NativeErrorCode.NATIVE_ADAPTER_CONFIGURATION_ERROR;
                        break;
                    case 2:
                        nativeErrorCode = NativeErrorCode.CONNECTION_ERROR;
                        break;
                    case 3:
                    case 5:
                        nativeErrorCode = NativeErrorCode.UNEXPECTED_RESPONSE_CODE;
                        break;
                    case 4:
                        break;
                    case 6:
                        nativeErrorCode = NativeErrorCode.NETWORK_TIMEOUT;
                        break;
                    case 7:
                        nativeErrorCode = NativeErrorCode.UNSPECIFIED;
                        break;
                    default:
                        nativeErrorCode = NativeErrorCode.NETWORK_NO_FILL;
                        break;
                }
                MillennialUtils.postOnUiThread(new Runnable() {
                    public void run() {
                        MillennialStaticNativeAd.this.listener.onNativeAdFailed(nativeErrorCode);
                    }
                });
                MoPubLog.d("Millennial native ad failed: " + nativeErrorStatus.getDescription());
            }
            nativeErrorCode = NativeErrorCode.UNSPECIFIED;
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MillennialStaticNativeAd.this.listener.onNativeAdFailed(nativeErrorCode);
                }
            });
            MoPubLog.d("Millennial native ad failed: " + nativeErrorStatus.getDescription());
        }

        public void onClicked(NativeAd nativeAd2, NativeAd.ComponentName componentName, int i) {
            MoPubLog.d("Millennial native ad click tracker fired.");
        }

        public void onAdLeftApplication(NativeAd nativeAd2) {
            MoPubLog.d("Millennial native ad has left the application.");
        }

        public void onExpired(NativeAd nativeAd2) {
            MoPubLog.d("Millennial native ad has expired!");
        }
    }
}
