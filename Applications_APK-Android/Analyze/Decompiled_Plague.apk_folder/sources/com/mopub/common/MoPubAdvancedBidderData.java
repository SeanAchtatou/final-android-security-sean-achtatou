package com.mopub.common;

import android.support.annotation.NonNull;
import com.mopub.common.logging.MoPubLog;
import org.json.JSONException;
import org.json.JSONObject;

public class MoPubAdvancedBidderData {
    private static final String TOKEN_KEY = "token";
    @NonNull
    final String mCreativeNetworkName;
    @NonNull
    final String mToken;

    public MoPubAdvancedBidderData(@NonNull String str, @NonNull String str2) {
        Preconditions.checkNotNull(str);
        Preconditions.checkNotNull(str2);
        this.mToken = str;
        this.mCreativeNetworkName = str2;
    }

    @NonNull
    public JSONObject toJson() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(TOKEN_KEY, this.mToken);
        } catch (JSONException unused) {
            MoPubLog.e("Invalid token format: " + this.mToken);
        }
        return jSONObject;
    }
}
