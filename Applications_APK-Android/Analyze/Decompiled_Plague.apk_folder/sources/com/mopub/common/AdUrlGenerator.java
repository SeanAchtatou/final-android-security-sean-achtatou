package com.mopub.common;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.mopub.common.ClientMetadata;
import com.mopub.common.privacy.ConsentData;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.common.util.DateAndTime;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyAuctionFlags;

public abstract class AdUrlGenerator extends BaseUrlGenerator {
    private static final String ADVANCED_BIDDING_TOKENS_KEY = "abt";
    private static final String CARRIER_NAME_KEY = "cn";
    private static final String CARRIER_TYPE_KEY = "ct";
    private static final String COUNTRY_CODE_KEY = "iso";
    private static final String IS_MRAID_KEY = "mr";
    private static final String KEYWORDS_KEY = "q";
    private static final String LAT_LONG_ACCURACY_KEY = "lla";
    private static final String LAT_LONG_FRESHNESS_KEY = "llf";
    private static final String LAT_LONG_FROM_SDK_KEY = "llsdk";
    private static final String LAT_LONG_KEY = "ll";
    private static final String MOBILE_COUNTRY_CODE_KEY = "mcc";
    private static final String MOBILE_NETWORK_CODE_KEY = "mnc";
    private static final String ORIENTATION_KEY = "o";
    private static final String SCREEN_SCALE_KEY = "sc";
    private static final String TIMEZONE_OFFSET_KEY = "z";
    private static final String USER_DATA_KEYWORDS_KEY = "user_data_q";
    private static final String VIEWABILITY_KEY = "vv";
    protected String mAdUnitId;
    @Nullable
    private final ConsentData mConsentData;
    protected Context mContext;
    protected Boolean mForceGdprApplies;
    protected String mKeywords;
    protected Location mLocation;
    @Nullable
    private final PersonalInfoManager mPersonalInfoManager = MoPub.getPersonalInformationManager();
    protected String mUserDataKeywords;

    @Deprecated
    public AdUrlGenerator withFacebookSupported(boolean z) {
        return this;
    }

    public AdUrlGenerator(Context context) {
        this.mContext = context;
        if (this.mPersonalInfoManager == null) {
            this.mConsentData = null;
        } else {
            this.mConsentData = this.mPersonalInfoManager.getConsentData();
        }
    }

    public AdUrlGenerator withAdUnitId(String str) {
        this.mAdUnitId = str;
        return this;
    }

    public AdUrlGenerator withKeywords(String str) {
        this.mKeywords = str;
        return this;
    }

    public AdUrlGenerator withUserDataKeywords(String str) {
        this.mUserDataKeywords = str;
        return this;
    }

    public AdUrlGenerator withLocation(Location location) {
        this.mLocation = location;
        return this;
    }

    /* access modifiers changed from: protected */
    public void setAdUnitId(String str) {
        addParam("id", str);
    }

    /* access modifiers changed from: protected */
    public void setSdkVersion(String str) {
        addParam("nv", str);
    }

    /* access modifiers changed from: protected */
    public void setKeywords(String str) {
        addParam(KEYWORDS_KEY, str);
    }

    /* access modifiers changed from: protected */
    public void setUserDataKeywords(String str) {
        if (MoPub.canCollectPersonalInformation()) {
            addParam(USER_DATA_KEYWORDS_KEY, str);
        }
    }

    /* access modifiers changed from: protected */
    public void setLocation(@Nullable Location location) {
        if (MoPub.canCollectPersonalInformation()) {
            Location lastKnownLocation = LocationService.getLastKnownLocation(this.mContext, MoPub.getLocationPrecision(), MoPub.getLocationAwareness());
            if (lastKnownLocation != null && (location == null || lastKnownLocation.getTime() >= location.getTime())) {
                location = lastKnownLocation;
            }
            if (location != null) {
                addParam(LAT_LONG_KEY, location.getLatitude() + "," + location.getLongitude());
                addParam(LAT_LONG_ACCURACY_KEY, String.valueOf((int) location.getAccuracy()));
                addParam(LAT_LONG_FRESHNESS_KEY, String.valueOf(calculateLocationStalenessInMilliseconds(location)));
                if (location == lastKnownLocation) {
                    addParam(LAT_LONG_FROM_SDK_KEY, TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setTimezone(String str) {
        addParam(TIMEZONE_OFFSET_KEY, str);
    }

    /* access modifiers changed from: protected */
    public void setOrientation(String str) {
        addParam(ORIENTATION_KEY, str);
    }

    /* access modifiers changed from: protected */
    public void setDensity(float f) {
        addParam(SCREEN_SCALE_KEY, "" + f);
    }

    /* access modifiers changed from: protected */
    public void setMraidFlag(boolean z) {
        if (z) {
            addParam(IS_MRAID_KEY, TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE);
        }
    }

    /* access modifiers changed from: protected */
    public void setMccCode(String str) {
        addParam(MOBILE_COUNTRY_CODE_KEY, str == null ? "" : str.substring(0, mncPortionLength(str)));
    }

    /* access modifiers changed from: protected */
    public void setMncCode(String str) {
        String str2;
        if (str == null) {
            str2 = "";
        } else {
            str2 = str.substring(mncPortionLength(str));
        }
        addParam(MOBILE_NETWORK_CODE_KEY, str2);
    }

    /* access modifiers changed from: protected */
    public void setIsoCountryCode(String str) {
        addParam(COUNTRY_CODE_KEY, str);
    }

    /* access modifiers changed from: protected */
    public void setCarrierName(String str) {
        addParam(CARRIER_NAME_KEY, str);
    }

    /* access modifiers changed from: protected */
    public void setNetworkType(ClientMetadata.MoPubNetworkType moPubNetworkType) {
        addParam(CARRIER_TYPE_KEY, moPubNetworkType);
    }

    /* access modifiers changed from: protected */
    public void setBundleId(String str) {
        if (!TextUtils.isEmpty(str)) {
            addParam(TJAdUnitConstants.String.BUNDLE, str);
        }
    }

    /* access modifiers changed from: protected */
    public void enableViewability(@NonNull String str) {
        Preconditions.checkNotNull(str);
        addParam(VIEWABILITY_KEY, str);
    }

    /* access modifiers changed from: protected */
    public void setAdvancedBiddingTokens() {
        addParam(ADVANCED_BIDDING_TOKENS_KEY, MoPub.getAdvancedBiddingTokensJson(this.mContext));
    }

    /* access modifiers changed from: protected */
    public void setGdprApplies() {
        if (this.mPersonalInfoManager != null) {
            addParam("gdpr_applies", this.mPersonalInfoManager.gdprApplies());
        }
    }

    /* access modifiers changed from: protected */
    public void setForceGdprApplies() {
        if (this.mConsentData != null) {
            addParam("force_gdpr_applies", Boolean.valueOf(this.mConsentData.isForceGdprApplies()));
        }
    }

    /* access modifiers changed from: protected */
    public void setCurrentConsentStatus() {
        if (this.mPersonalInfoManager != null) {
            addParam("current_consent_status", this.mPersonalInfoManager.getPersonalInfoConsentStatus().getValue());
        }
    }

    /* access modifiers changed from: protected */
    public void setConsentedPrivacyPolicyVersion() {
        if (this.mConsentData != null) {
            addParam("consented_privacy_policy_version", this.mConsentData.getConsentedPrivacyPolicyVersion());
        }
    }

    /* access modifiers changed from: protected */
    public void setConsentedVendorListVersion() {
        if (this.mConsentData != null) {
            addParam("consented_vendor_list_version", this.mConsentData.getConsentedVendorListVersion());
        }
    }

    /* access modifiers changed from: protected */
    public void addBaseParams(ClientMetadata clientMetadata) {
        setAdUnitId(this.mAdUnitId);
        setSdkVersion(clientMetadata.getSdkVersion());
        setDeviceInfo(clientMetadata.getDeviceManufacturer(), clientMetadata.getDeviceModel(), clientMetadata.getDeviceProduct());
        setBundleId(clientMetadata.getAppPackageName());
        setKeywords(this.mKeywords);
        if (MoPub.canCollectPersonalInformation()) {
            setUserDataKeywords(this.mUserDataKeywords);
            setLocation(this.mLocation);
        }
        setTimezone(DateAndTime.getTimeZoneOffsetString());
        setOrientation(clientMetadata.getOrientationString());
        setDeviceDimensions(clientMetadata.getDeviceDimensions());
        setDensity(clientMetadata.getDensity());
        String networkOperatorForUrl = clientMetadata.getNetworkOperatorForUrl();
        setMccCode(networkOperatorForUrl);
        setMncCode(networkOperatorForUrl);
        setIsoCountryCode(clientMetadata.getIsoCountryCode());
        setCarrierName(clientMetadata.getNetworkOperatorName());
        setNetworkType(clientMetadata.getActiveNetworkType());
        setAppVersion(clientMetadata.getAppVersion());
        setAdvancedBiddingTokens();
        appendAdvertisingInfoTemplates();
        setGdprApplies();
        setForceGdprApplies();
        setCurrentConsentStatus();
        setConsentedPrivacyPolicyVersion();
        setConsentedVendorListVersion();
    }

    private void addParam(String str, ClientMetadata.MoPubNetworkType moPubNetworkType) {
        addParam(str, moPubNetworkType.toString());
    }

    private int mncPortionLength(String str) {
        return Math.min(3, str.length());
    }

    private static int calculateLocationStalenessInMilliseconds(Location location) {
        Preconditions.checkNotNull(location);
        return (int) (System.currentTimeMillis() - location.getTime());
    }
}
