package com.mopub.common;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;
import com.mopub.common.ExternalViewabilitySession;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.VastVideoConfig;
import com.tapjoy.TapjoyAuctionFlags;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class ExternalViewabilitySessionManager {
    @NonNull
    private final Set<ExternalViewabilitySession> mViewabilitySessions = new HashSet();

    public enum ViewabilityVendor {
        AVID,
        MOAT,
        ALL;

        public void disable() {
            switch (this) {
                case AVID:
                    AvidViewabilitySession.disable();
                    break;
                case MOAT:
                    MoatViewabilitySession.disable();
                    break;
                case ALL:
                    AvidViewabilitySession.disable();
                    MoatViewabilitySession.disable();
                    break;
                default:
                    MoPubLog.d("Attempted to disable an invalid viewability vendor: " + this);
                    return;
            }
            MoPubLog.d("Disabled viewability for " + this);
        }

        @NonNull
        public static String getEnabledVendorKey() {
            boolean isEnabled = AvidViewabilitySession.isEnabled();
            boolean isEnabled2 = MoatViewabilitySession.isEnabled();
            if (isEnabled && isEnabled2) {
                return "3";
            }
            if (isEnabled) {
                return TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE;
            }
            return isEnabled2 ? "2" : "0";
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        @Nullable
        public static ViewabilityVendor fromKey(@NonNull String str) {
            char c;
            Preconditions.checkNotNull(str);
            switch (str.hashCode()) {
                case 49:
                    if (str.equals(TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE)) {
                        c = 0;
                        break;
                    }
                    c = 65535;
                    break;
                case 50:
                    if (str.equals("2")) {
                        c = 1;
                        break;
                    }
                    c = 65535;
                    break;
                case 51:
                    if (str.equals("3")) {
                        c = 2;
                        break;
                    }
                    c = 65535;
                    break;
                default:
                    c = 65535;
                    break;
            }
            switch (c) {
                case 0:
                    return AVID;
                case 1:
                    return MOAT;
                case 2:
                    return ALL;
                default:
                    return null;
            }
        }
    }

    public ExternalViewabilitySessionManager(@NonNull Context context) {
        Preconditions.checkNotNull(context);
        this.mViewabilitySessions.add(new AvidViewabilitySession());
        this.mViewabilitySessions.add(new MoatViewabilitySession());
        initialize(context);
    }

    private void initialize(@NonNull Context context) {
        Preconditions.checkNotNull(context);
        for (ExternalViewabilitySession next : this.mViewabilitySessions) {
            logEvent(next, "initialize", next.initialize(context), false);
        }
    }

    public void invalidate() {
        for (ExternalViewabilitySession next : this.mViewabilitySessions) {
            logEvent(next, "invalidate", next.invalidate(), false);
        }
    }

    public void createDisplaySession(@NonNull Context context, @NonNull WebView webView, boolean z) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(webView);
        for (ExternalViewabilitySession next : this.mViewabilitySessions) {
            logEvent(next, "start display session", next.createDisplaySession(context, webView, z), true);
        }
    }

    public void createDisplaySession(@NonNull Context context, @NonNull WebView webView) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(webView);
        createDisplaySession(context, webView, false);
    }

    public void startDeferredDisplaySession(@NonNull Activity activity) {
        for (ExternalViewabilitySession next : this.mViewabilitySessions) {
            logEvent(next, "record deferred session", next.startDeferredDisplaySession(activity), true);
        }
    }

    public void endDisplaySession() {
        for (ExternalViewabilitySession next : this.mViewabilitySessions) {
            logEvent(next, "end display session", next.endDisplaySession(), true);
        }
    }

    public void createVideoSession(@NonNull Activity activity, @NonNull View view, @NonNull VastVideoConfig vastVideoConfig) {
        Preconditions.checkNotNull(activity);
        Preconditions.checkNotNull(view);
        Preconditions.checkNotNull(vastVideoConfig);
        for (ExternalViewabilitySession next : this.mViewabilitySessions) {
            HashSet hashSet = new HashSet();
            if (next instanceof AvidViewabilitySession) {
                hashSet.addAll(vastVideoConfig.getAvidJavascriptResources());
            } else if (next instanceof MoatViewabilitySession) {
                hashSet.addAll(vastVideoConfig.getMoatImpressionPixels());
            }
            logEvent(next, "start video session", next.createVideoSession(activity, view, hashSet, vastVideoConfig.getExternalViewabilityTrackers()), true);
        }
    }

    public void registerVideoObstruction(@NonNull View view) {
        Preconditions.checkNotNull(view);
        for (ExternalViewabilitySession next : this.mViewabilitySessions) {
            logEvent(next, "register friendly obstruction", next.registerVideoObstruction(view), true);
        }
    }

    public void onVideoPrepared(@NonNull View view, int i) {
        Preconditions.checkNotNull(view);
        for (ExternalViewabilitySession next : this.mViewabilitySessions) {
            logEvent(next, "on video prepared", next.onVideoPrepared(view, i), true);
        }
    }

    public void recordVideoEvent(@NonNull ExternalViewabilitySession.VideoEvent videoEvent, int i) {
        Preconditions.checkNotNull(videoEvent);
        for (ExternalViewabilitySession next : this.mViewabilitySessions) {
            Boolean recordVideoEvent = next.recordVideoEvent(videoEvent, i);
            logEvent(next, "record video event (" + videoEvent.name() + ")", recordVideoEvent, true);
        }
    }

    public void endVideoSession() {
        for (ExternalViewabilitySession next : this.mViewabilitySessions) {
            logEvent(next, "end video session", next.endVideoSession(), true);
        }
    }

    private void logEvent(@NonNull ExternalViewabilitySession externalViewabilitySession, @NonNull String str, @Nullable Boolean bool, boolean z) {
        Preconditions.checkNotNull(externalViewabilitySession);
        Preconditions.checkNotNull(str);
        if (bool != null) {
            String format = String.format(Locale.US, "%s viewability event: %s%s.", externalViewabilitySession.getName(), bool.booleanValue() ? "" : "failed to ", str);
            if (z) {
                MoPubLog.v(format);
            } else {
                MoPubLog.d(format);
            }
        }
    }
}
