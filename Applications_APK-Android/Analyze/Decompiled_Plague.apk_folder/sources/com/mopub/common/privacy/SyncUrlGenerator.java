package com.mopub.common.privacy;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.mopub.common.BaseUrlGenerator;
import com.mopub.common.ClientMetadata;
import com.mopub.common.Constants;
import com.mopub.common.Preconditions;
import com.mopub.network.PlayServicesUrlRewriter;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;

public class SyncUrlGenerator extends BaseUrlGenerator {
    private static final String CACHED_VENDOR_LIST_IAB_HASH_KEY = "cached_vendor_list_iab_hash";
    private static final String CONSENT_CHANGE_REASON_KEY = "consent_change_reason";
    private static final String EXTRAS_KEY = "extras";
    private static final String FORCED_GDPR_APPLIES_CHANGED = "forced_gdpr_applies_changed";
    private static final String LAST_CHANGED_MS_KEY = "last_changed_ms";
    private static final String LAST_CONSENT_STATUS_KEY = "last_consent_status";
    @Nullable
    private String mAdUnitId;
    @Nullable
    private String mCachedVendorListIabHash;
    @Nullable
    private String mConsentChangeReason;
    @Nullable
    private String mConsentedPrivacyPolicyVersion;
    @Nullable
    private String mConsentedVendorListVersion;
    @NonNull
    private final Context mContext;
    @NonNull
    private final String mCurrentConsentStatus;
    @Nullable
    private String mExtras;
    private boolean mForceGdprApplies;
    @Nullable
    private Boolean mForceGdprAppliesChanged;
    @Nullable
    private Boolean mGdprApplies;
    @Nullable
    private String mLastChangedMs;
    @Nullable
    private String mLastConsentStatus;
    @Nullable
    private String mUdid;

    public SyncUrlGenerator(@NonNull Context context, @NonNull String str) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(str);
        this.mContext = context.getApplicationContext();
        this.mCurrentConsentStatus = str;
    }

    public SyncUrlGenerator withAdUnitId(@Nullable String str) {
        this.mAdUnitId = str;
        return this;
    }

    public SyncUrlGenerator withUdid(@Nullable String str) {
        this.mUdid = str;
        return this;
    }

    public SyncUrlGenerator withGdprApplies(@Nullable Boolean bool) {
        this.mGdprApplies = bool;
        return this;
    }

    public SyncUrlGenerator withForceGdprApplies(boolean z) {
        this.mForceGdprApplies = z;
        return this;
    }

    public SyncUrlGenerator withForceGdprAppliesChanged(@Nullable Boolean bool) {
        this.mForceGdprAppliesChanged = bool;
        return this;
    }

    public SyncUrlGenerator withLastChangedMs(@Nullable String str) {
        this.mLastChangedMs = str;
        return this;
    }

    public SyncUrlGenerator withLastConsentStatus(@Nullable ConsentStatus consentStatus) {
        this.mLastConsentStatus = consentStatus == null ? null : consentStatus.getValue();
        return this;
    }

    public SyncUrlGenerator withConsentChangeReason(@Nullable String str) {
        this.mConsentChangeReason = str;
        return this;
    }

    public SyncUrlGenerator withConsentedVendorListVersion(@Nullable String str) {
        this.mConsentedVendorListVersion = str;
        return this;
    }

    public SyncUrlGenerator withConsentedPrivacyPolicyVersion(@Nullable String str) {
        this.mConsentedPrivacyPolicyVersion = str;
        return this;
    }

    public SyncUrlGenerator withCachedVendorListIabHash(@Nullable String str) {
        this.mCachedVendorListIabHash = str;
        return this;
    }

    public SyncUrlGenerator withExtras(@Nullable String str) {
        this.mExtras = str;
        return this;
    }

    public String generateUrlString(@NonNull String str) {
        initUrlString(str, Constants.GDPR_SYNC_HANDLER);
        addParam("id", this.mAdUnitId);
        addParam("nv", "5.2.0");
        addParam(LAST_CHANGED_MS_KEY, this.mLastChangedMs);
        addParam(LAST_CONSENT_STATUS_KEY, this.mLastConsentStatus);
        addParam("current_consent_status", this.mCurrentConsentStatus);
        addParam(CONSENT_CHANGE_REASON_KEY, this.mConsentChangeReason);
        addParam("consented_vendor_list_version", this.mConsentedVendorListVersion);
        addParam("consented_privacy_policy_version", this.mConsentedPrivacyPolicyVersion);
        addParam(CACHED_VENDOR_LIST_IAB_HASH_KEY, this.mCachedVendorListIabHash);
        addParam(EXTRAS_KEY, this.mExtras);
        addParam(TapjoyConstants.TJC_DEVICE_ID_NAME, this.mUdid);
        addParam("gdpr_applies", this.mGdprApplies);
        addParam("force_gdpr_applies", Boolean.valueOf(this.mForceGdprApplies));
        addParam(FORCED_GDPR_APPLIES_CHANGED, this.mForceGdprAppliesChanged);
        addParam(TJAdUnitConstants.String.BUNDLE, ClientMetadata.getInstance(this.mContext).getAppPackageName());
        addParam("dnt", PlayServicesUrlRewriter.DO_NOT_TRACK_TEMPLATE);
        return getFinalUrlString();
    }
}
