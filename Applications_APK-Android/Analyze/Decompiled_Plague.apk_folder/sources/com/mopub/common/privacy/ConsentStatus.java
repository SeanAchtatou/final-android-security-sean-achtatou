package com.mopub.common.privacy;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.os.EnvironmentCompat;

public enum ConsentStatus {
    EXPLICIT_YES("explicit_yes"),
    EXPLICIT_NO("explicit_no"),
    UNKNOWN(EnvironmentCompat.MEDIA_UNKNOWN),
    POTENTIAL_WHITELIST("potential_whitelist"),
    DNT("dnt");
    
    @NonNull
    private final String mValue;

    private ConsentStatus(@NonNull String str) {
        this.mValue = str;
    }

    @NonNull
    public String getValue() {
        return this.mValue;
    }

    @NonNull
    public static ConsentStatus fromString(@Nullable String str) {
        if (str == null) {
            return UNKNOWN;
        }
        for (ConsentStatus consentStatus : values()) {
            if (str.equals(consentStatus.name())) {
                return consentStatus;
            }
        }
        return UNKNOWN;
    }
}
