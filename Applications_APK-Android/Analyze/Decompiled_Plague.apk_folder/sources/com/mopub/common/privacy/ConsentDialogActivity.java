package com.mopub.common.privacy;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.mopub.common.MoPub;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.ConsentDialogLayout;
import com.mopub.common.util.Intents;
import com.mopub.exceptions.IntentNotResolvableException;
import com.tapjoy.TapjoyConstants;

public class ConsentDialogActivity extends Activity {
    private static final int CLOSE_BUTTON_DELAY_MS = 10000;
    private static final String KEY_HTML_PAGE = "html-page-content";
    @Nullable
    Handler mCloseButtonHandler;
    @Nullable
    ConsentStatus mConsentStatus;
    @Nullable
    private Runnable mEnableCloseButtonRunnable;
    @Nullable
    ConsentDialogLayout mView;

    static void start(@NonNull Context context, @NonNull String str) {
        Preconditions.checkNotNull(context);
        if (TextUtils.isEmpty(str)) {
            MoPubLog.e("ConsentDialogActivity htmlData can't be empty string.");
            return;
        }
        try {
            Intents.startActivity(context, createIntent(context, str));
        } catch (ActivityNotFoundException | IntentNotResolvableException unused) {
            MoPubLog.e("ConsentDialogActivity not found - did you declare it in AndroidManifest.xml?");
        }
    }

    @NonNull
    static Intent createIntent(@NonNull Context context, @NonNull String str) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(str);
        Bundle bundle = new Bundle();
        bundle.putString(KEY_HTML_PAGE, str);
        return Intents.getStartActivityIntent(context, ConsentDialogActivity.class, bundle);
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        String stringExtra = getIntent().getStringExtra(KEY_HTML_PAGE);
        if (TextUtils.isEmpty(stringExtra)) {
            MoPubLog.e("Web page for ConsentDialogActivity is empty");
            finish();
            return;
        }
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        this.mView = new ConsentDialogLayout(this);
        this.mView.setConsentClickListener(new ConsentDialogLayout.ConsentListener() {
            public void onConsentClick(ConsentStatus consentStatus) {
                ConsentDialogActivity.this.saveConsentStatus(consentStatus);
                ConsentDialogActivity.this.setCloseButtonVisibility(false);
            }

            public void onCloseClick() {
                ConsentDialogActivity.this.finish();
            }
        });
        this.mEnableCloseButtonRunnable = new Runnable() {
            public void run() {
                ConsentDialogActivity.this.setCloseButtonVisibility(true);
            }
        };
        setContentView(this.mView);
        this.mView.startLoading(stringExtra, new ConsentDialogLayout.PageLoadListener() {
            public void onLoadProgress(int i) {
                int i2 = ConsentDialogLayout.FINISHED_LOADING;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.mCloseButtonHandler = new Handler();
        this.mCloseButtonHandler.postDelayed(this.mEnableCloseButtonRunnable, TapjoyConstants.TIMER_INCREMENT);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        setCloseButtonVisibility(true);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        PersonalInfoManager personalInformationManager = MoPub.getPersonalInformationManager();
        if (!(personalInformationManager == null || this.mConsentStatus == null)) {
            personalInformationManager.changeConsentStateFromDialog(this.mConsentStatus);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    public void setCloseButtonVisibility(boolean z) {
        if (this.mCloseButtonHandler != null) {
            this.mCloseButtonHandler.removeCallbacks(this.mEnableCloseButtonRunnable);
        }
        if (this.mView != null) {
            this.mView.setCloseVisible(z);
        }
    }

    /* access modifiers changed from: private */
    public void saveConsentStatus(@NonNull ConsentStatus consentStatus) {
        Preconditions.checkNotNull(consentStatus);
        this.mConsentStatus = consentStatus;
    }
}
