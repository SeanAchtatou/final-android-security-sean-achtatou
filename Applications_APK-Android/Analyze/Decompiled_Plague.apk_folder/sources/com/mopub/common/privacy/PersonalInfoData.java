package com.mopub.common.privacy;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.mopub.common.ClientMetadata;
import com.mopub.common.Preconditions;
import com.mopub.common.SharedPreferencesHelper;
import com.mopub.common.VisibleForTesting;
import java.util.Locale;

class PersonalInfoData implements ConsentData {
    private static final String AD_UNIT_ID_SP_KEY = "info/adunit";
    private static final String CONSENTED_PRIVACY_POLICY_VERSION_SP_KEY = "info/consented_privacy_policy_version";
    private static final String CONSENTED_VENDOR_LIST_IAB_FORMAT_SP_KEY = "info/consented_vendor_list_iab_format";
    private static final String CONSENTED_VENDOR_LIST_VERSION_SP_KEY = "info/consented_vendor_list_version";
    private static final String CONSENT_CHANGE_REASON_SP_KEY = "info/consent_change_reason";
    private static final String CONSENT_STATUS_BEFORE_DNT_SP_KEY = "info/consent_status_before_dnt";
    private static final String CONSENT_STATUS_SP_KEY = "info/consent_status";
    private static final String CURRENT_PRIVACY_POLICY_LINK_SP_KEY = "info/current_privacy_policy_link";
    private static final String CURRENT_PRIVACY_POLICY_VERSION_SP_KEY = "info/current_privacy_policy_version";
    private static final String CURRENT_VENDOR_LIST_IAB_FORMAT_SP_KEY = "info/current_vendor_list_iab_format";
    private static final String CURRENT_VENDOR_LIST_IAB_HASH_SP_KEY = "info/current_vendor_list_iab_hash";
    private static final String CURRENT_VENDOR_LIST_LINK_SP_KEY = "info/current_vendor_list_link";
    private static final String CURRENT_VENDOR_LIST_VERSION_SP_KEY = "info/current_vendor_list_version";
    private static final String EXTRAS_SP_KEY = "info/extras";
    private static final String FORCE_GDPR_APPLIES_SP_KEY = "info/force_gdpr_applies";
    private static final String GDPR_APPLIES_SP_KEY = "info/gdpr_applies";
    private static final String IS_WHITELISTED_SP_KEY = "info/is_whitelisted";
    private static final String LANGUAGE_MACRO_KEY = "%%LANGUAGE%%";
    private static final String LAST_CHANGED_MS_SP_KEY = "info/last_changed_ms";
    private static final String LAST_SUCCESSFULLY_SYNCED_CONSENT_STATUS_SP_KEY = "info/last_successfully_synced_consent_status";
    private static final String PERSONAL_INFO_DATA_SHARED_PREFS = "com.mopub.privacy";
    private static final String PERSONAL_INFO_PREFIX = "info/";
    private static final String REACQUIRE_CONSENT_SP_KEY = "info/reacquire_consent";
    private static final String UDID_SP_KEY = "info/udid";
    @NonNull
    private String mAdUnitId;
    @NonNull
    private final Context mAppContext;
    @Nullable
    private String mConsentChangeReason;
    @NonNull
    private ConsentStatus mConsentStatus = ConsentStatus.UNKNOWN;
    @Nullable
    private ConsentStatus mConsentStatusBeforeDnt;
    @Nullable
    private String mConsentedPrivacyPolicyVersion;
    @Nullable
    private String mConsentedVendorListIabFormat;
    @Nullable
    private String mConsentedVendorListVersion;
    @Nullable
    private String mCurrentPrivacyPolicyLink;
    @Nullable
    private String mCurrentPrivacyPolicyVersion;
    @Nullable
    private String mCurrentVendorListIabFormat;
    @Nullable
    private String mCurrentVendorListIabHash;
    @Nullable
    private String mCurrentVendorListLink;
    @Nullable
    private String mCurrentVendorListVersion;
    @Nullable
    private String mExtras;
    private boolean mForceGdprApplies;
    @Nullable
    private Boolean mGdprApplies;
    private boolean mIsWhitelisted;
    @Nullable
    private String mLastChangedMs;
    @Nullable
    private ConsentStatus mLastSuccessfullySyncedConsentStatus;
    private boolean mReacquireConsent;
    @Nullable
    private String mUdid;

    PersonalInfoData(@NonNull Context context, @NonNull String str) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(str);
        this.mAppContext = context.getApplicationContext();
        getStateFromDisk();
        this.mAdUnitId = str;
    }

    private void getStateFromDisk() {
        SharedPreferences sharedPreferences = SharedPreferencesHelper.getSharedPreferences(this.mAppContext, PERSONAL_INFO_DATA_SHARED_PREFS);
        this.mAdUnitId = sharedPreferences.getString(AD_UNIT_ID_SP_KEY, "");
        this.mConsentStatus = ConsentStatus.fromString(sharedPreferences.getString(CONSENT_STATUS_SP_KEY, ConsentStatus.UNKNOWN.name()));
        String string = sharedPreferences.getString(LAST_SUCCESSFULLY_SYNCED_CONSENT_STATUS_SP_KEY, null);
        if (TextUtils.isEmpty(string)) {
            this.mLastSuccessfullySyncedConsentStatus = null;
        } else {
            this.mLastSuccessfullySyncedConsentStatus = ConsentStatus.fromString(string);
        }
        this.mIsWhitelisted = sharedPreferences.getBoolean(IS_WHITELISTED_SP_KEY, false);
        this.mCurrentVendorListVersion = sharedPreferences.getString(CURRENT_VENDOR_LIST_VERSION_SP_KEY, null);
        this.mCurrentVendorListLink = sharedPreferences.getString(CURRENT_VENDOR_LIST_LINK_SP_KEY, null);
        this.mCurrentPrivacyPolicyVersion = sharedPreferences.getString(CURRENT_PRIVACY_POLICY_VERSION_SP_KEY, null);
        this.mCurrentPrivacyPolicyLink = sharedPreferences.getString(CURRENT_PRIVACY_POLICY_LINK_SP_KEY, null);
        this.mCurrentVendorListIabFormat = sharedPreferences.getString(CURRENT_VENDOR_LIST_IAB_FORMAT_SP_KEY, null);
        this.mCurrentVendorListIabHash = sharedPreferences.getString(CURRENT_VENDOR_LIST_IAB_HASH_SP_KEY, null);
        this.mConsentedVendorListVersion = sharedPreferences.getString(CONSENTED_VENDOR_LIST_VERSION_SP_KEY, null);
        this.mConsentedPrivacyPolicyVersion = sharedPreferences.getString(CONSENTED_PRIVACY_POLICY_VERSION_SP_KEY, null);
        this.mConsentedVendorListIabFormat = sharedPreferences.getString(CONSENTED_VENDOR_LIST_IAB_FORMAT_SP_KEY, null);
        this.mExtras = sharedPreferences.getString(EXTRAS_SP_KEY, null);
        this.mConsentChangeReason = sharedPreferences.getString(CONSENT_CHANGE_REASON_SP_KEY, null);
        this.mReacquireConsent = sharedPreferences.getBoolean(REACQUIRE_CONSENT_SP_KEY, false);
        String string2 = sharedPreferences.getString(GDPR_APPLIES_SP_KEY, null);
        if (TextUtils.isEmpty(string2)) {
            this.mGdprApplies = null;
        } else {
            this.mGdprApplies = Boolean.valueOf(Boolean.parseBoolean(string2));
        }
        this.mForceGdprApplies = sharedPreferences.getBoolean(FORCE_GDPR_APPLIES_SP_KEY, false);
        this.mUdid = sharedPreferences.getString(UDID_SP_KEY, null);
        this.mLastChangedMs = sharedPreferences.getString(LAST_CHANGED_MS_SP_KEY, null);
        String string3 = sharedPreferences.getString(CONSENT_STATUS_BEFORE_DNT_SP_KEY, null);
        if (TextUtils.isEmpty(string3)) {
            this.mConsentStatusBeforeDnt = null;
        } else {
            this.mConsentStatusBeforeDnt = ConsentStatus.fromString(string3);
        }
    }

    /* access modifiers changed from: package-private */
    public void writeToDisk() {
        String str;
        String str2;
        SharedPreferences.Editor edit = SharedPreferencesHelper.getSharedPreferences(this.mAppContext, PERSONAL_INFO_DATA_SHARED_PREFS).edit();
        edit.putString(AD_UNIT_ID_SP_KEY, this.mAdUnitId);
        edit.putString(CONSENT_STATUS_SP_KEY, this.mConsentStatus.name());
        String str3 = null;
        if (this.mLastSuccessfullySyncedConsentStatus == null) {
            str = null;
        } else {
            str = this.mLastSuccessfullySyncedConsentStatus.name();
        }
        edit.putString(LAST_SUCCESSFULLY_SYNCED_CONSENT_STATUS_SP_KEY, str);
        edit.putBoolean(IS_WHITELISTED_SP_KEY, this.mIsWhitelisted);
        edit.putString(CURRENT_VENDOR_LIST_VERSION_SP_KEY, this.mCurrentVendorListVersion);
        edit.putString(CURRENT_VENDOR_LIST_LINK_SP_KEY, this.mCurrentVendorListLink);
        edit.putString(CURRENT_PRIVACY_POLICY_VERSION_SP_KEY, this.mCurrentPrivacyPolicyVersion);
        edit.putString(CURRENT_PRIVACY_POLICY_LINK_SP_KEY, this.mCurrentPrivacyPolicyLink);
        edit.putString(CURRENT_VENDOR_LIST_IAB_FORMAT_SP_KEY, this.mCurrentVendorListIabFormat);
        edit.putString(CURRENT_VENDOR_LIST_IAB_HASH_SP_KEY, this.mCurrentVendorListIabHash);
        edit.putString(CONSENTED_VENDOR_LIST_VERSION_SP_KEY, this.mConsentedVendorListVersion);
        edit.putString(CONSENTED_PRIVACY_POLICY_VERSION_SP_KEY, this.mConsentedPrivacyPolicyVersion);
        edit.putString(CONSENTED_VENDOR_LIST_IAB_FORMAT_SP_KEY, this.mConsentedVendorListIabFormat);
        edit.putString(EXTRAS_SP_KEY, this.mExtras);
        edit.putString(CONSENT_CHANGE_REASON_SP_KEY, this.mConsentChangeReason);
        edit.putBoolean(REACQUIRE_CONSENT_SP_KEY, this.mReacquireConsent);
        if (this.mGdprApplies == null) {
            str2 = null;
        } else {
            str2 = this.mGdprApplies.toString();
        }
        edit.putString(GDPR_APPLIES_SP_KEY, str2);
        edit.putBoolean(FORCE_GDPR_APPLIES_SP_KEY, this.mForceGdprApplies);
        edit.putString(UDID_SP_KEY, this.mUdid);
        edit.putString(LAST_CHANGED_MS_SP_KEY, this.mLastChangedMs);
        if (this.mConsentStatusBeforeDnt != null) {
            str3 = this.mConsentStatusBeforeDnt.name();
        }
        edit.putString(CONSENT_STATUS_BEFORE_DNT_SP_KEY, str3);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public String getAdUnitId() {
        return this.mAdUnitId;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public ConsentStatus getConsentStatus() {
        return this.mConsentStatus;
    }

    /* access modifiers changed from: package-private */
    public void setConsentStatus(@NonNull ConsentStatus consentStatus) {
        this.mConsentStatus = consentStatus;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public ConsentStatus getLastSuccessfullySyncedConsentStatus() {
        return this.mLastSuccessfullySyncedConsentStatus;
    }

    /* access modifiers changed from: package-private */
    public void setLastSuccessfullySyncedConsentStatus(@Nullable ConsentStatus consentStatus) {
        this.mLastSuccessfullySyncedConsentStatus = consentStatus;
    }

    /* access modifiers changed from: package-private */
    public boolean isWhitelisted() {
        return this.mIsWhitelisted;
    }

    /* access modifiers changed from: package-private */
    public void setWhitelisted(boolean z) {
        this.mIsWhitelisted = z;
    }

    @Nullable
    public String getCurrentVendorListVersion() {
        return this.mCurrentVendorListVersion;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentVendorListVersion(@Nullable String str) {
        this.mCurrentVendorListVersion = str;
    }

    @NonNull
    public String getCurrentVendorListLink() {
        return getCurrentVendorListLink(null);
    }

    @NonNull
    public String getCurrentVendorListLink(@Nullable String str) {
        return replaceLanguageMacro(this.mCurrentVendorListLink, this.mAppContext, str);
    }

    /* access modifiers changed from: package-private */
    public void setCurrentVendorListLink(@Nullable String str) {
        this.mCurrentVendorListLink = str;
    }

    @Nullable
    public String getCurrentPrivacyPolicyVersion() {
        return this.mCurrentPrivacyPolicyVersion;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentPrivacyPolicyVersion(@Nullable String str) {
        this.mCurrentPrivacyPolicyVersion = str;
    }

    @NonNull
    public String getCurrentPrivacyPolicyLink() {
        return getCurrentPrivacyPolicyLink(null);
    }

    @NonNull
    public String getCurrentPrivacyPolicyLink(@Nullable String str) {
        return replaceLanguageMacro(this.mCurrentPrivacyPolicyLink, this.mAppContext, str);
    }

    /* access modifiers changed from: package-private */
    public void setCurrentPrivacyPolicyLink(@Nullable String str) {
        this.mCurrentPrivacyPolicyLink = str;
    }

    @Nullable
    public String getCurrentVendorListIabFormat() {
        return this.mCurrentVendorListIabFormat;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentVendorListIabFormat(@Nullable String str) {
        this.mCurrentVendorListIabFormat = str;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String getCurrentVendorListIabHash() {
        return this.mCurrentVendorListIabHash;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentVendorListIabHash(@Nullable String str) {
        this.mCurrentVendorListIabHash = str;
    }

    @Nullable
    public String getConsentedVendorListVersion() {
        return this.mConsentedVendorListVersion;
    }

    /* access modifiers changed from: package-private */
    public void setConsentedVendorListVersion(@Nullable String str) {
        this.mConsentedVendorListVersion = str;
    }

    @Nullable
    public String getConsentedPrivacyPolicyVersion() {
        return this.mConsentedPrivacyPolicyVersion;
    }

    /* access modifiers changed from: package-private */
    public void setConsentedPrivacyPolicyVersion(@Nullable String str) {
        this.mConsentedPrivacyPolicyVersion = str;
    }

    @Nullable
    public String getConsentedVendorListIabFormat() {
        return this.mConsentedVendorListIabFormat;
    }

    /* access modifiers changed from: package-private */
    public void setConsentedVendorListIabFormat(@Nullable String str) {
        this.mConsentedVendorListIabFormat = str;
    }

    @Nullable
    public String getExtras() {
        return this.mExtras;
    }

    public void setExtras(@Nullable String str) {
        this.mExtras = str;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String getConsentChangeReason() {
        return this.mConsentChangeReason;
    }

    /* access modifiers changed from: package-private */
    public void setConsentChangeReason(@Nullable String str) {
        this.mConsentChangeReason = str;
    }

    /* access modifiers changed from: package-private */
    public boolean shouldReacquireConsent() {
        return this.mReacquireConsent;
    }

    /* access modifiers changed from: package-private */
    public void setShouldReacquireConsent(boolean z) {
        this.mReacquireConsent = z;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public Boolean getGdprApplies() {
        return this.mGdprApplies;
    }

    /* access modifiers changed from: package-private */
    public void setGdprApplies(@Nullable Boolean bool) {
        this.mGdprApplies = bool;
    }

    public boolean isForceGdprApplies() {
        return this.mForceGdprApplies;
    }

    /* access modifiers changed from: package-private */
    public void setForceGdprApplies(boolean z) {
        this.mForceGdprApplies = z;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String getUdid() {
        return this.mUdid;
    }

    /* access modifiers changed from: package-private */
    public void setUdid(@Nullable String str) {
        this.mUdid = str;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String getLastChangedMs() {
        return this.mLastChangedMs;
    }

    /* access modifiers changed from: package-private */
    public void setLastChangedMs(@Nullable String str) {
        this.mLastChangedMs = str;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public ConsentStatus getConsentStatusBeforeDnt() {
        return this.mConsentStatusBeforeDnt;
    }

    /* access modifiers changed from: package-private */
    public void setConsentStatusBeforeDnt(@Nullable ConsentStatus consentStatus) {
        this.mConsentStatusBeforeDnt = consentStatus;
    }

    @NonNull
    @VisibleForTesting
    static String replaceLanguageMacro(@Nullable String str, @NonNull Context context, @Nullable String str2) {
        Preconditions.checkNotNull(context);
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        return str.replaceAll(LANGUAGE_MACRO_KEY, validateLanguage(context, str2));
    }

    @NonNull
    private static String validateLanguage(@NonNull Context context, @Nullable String str) {
        Preconditions.checkNotNull(context);
        for (String str2 : Locale.getISOLanguages()) {
            if (str2 != null && str2.equals(str)) {
                return str;
            }
        }
        return ClientMetadata.getCurrentLanguage(context);
    }
}
