package com.mopub.common.privacy;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import java.io.Serializable;
import java.util.Calendar;
import java.util.UUID;

public class AdvertisingId implements Serializable {
    private static final String PREFIX_IFA = "ifa:";
    private static final String PREFIX_MOPUB = "mopub:";
    static final long ROTATION_TIME_MS = 86400000;
    @NonNull
    final String mAdvertisingId;
    final boolean mDoNotTrack;
    @NonNull
    final Calendar mLastRotation = Calendar.getInstance();
    @NonNull
    final String mMopubId;

    AdvertisingId(@NonNull String str, @NonNull String str2, boolean z, long j) {
        Preconditions.checkNotNull(str);
        Preconditions.checkNotNull(str);
        this.mAdvertisingId = str;
        this.mMopubId = str2;
        this.mDoNotTrack = z;
        this.mLastRotation.setTimeInMillis(j);
    }

    public String getIdentifier(boolean z) {
        return (this.mDoNotTrack || !z) ? this.mMopubId : this.mAdvertisingId;
    }

    @NonNull
    public String getIdWithPrefix(boolean z) {
        if (this.mDoNotTrack || !z || this.mAdvertisingId.isEmpty()) {
            return PREFIX_MOPUB + this.mMopubId;
        }
        return PREFIX_IFA + this.mAdvertisingId;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public String getIfaWithPrefix() {
        if (TextUtils.isEmpty(this.mAdvertisingId)) {
            return "";
        }
        return PREFIX_IFA + this.mAdvertisingId;
    }

    public boolean isDoNotTrack() {
        return this.mDoNotTrack;
    }

    @NonNull
    static AdvertisingId generateExpiredAdvertisingId() {
        return new AdvertisingId("", generateIdString(), false, (Calendar.getInstance().getTimeInMillis() - ROTATION_TIME_MS) - 1);
    }

    @NonNull
    static AdvertisingId generateFreshAdvertisingId() {
        return new AdvertisingId("", generateIdString(), false, Calendar.getInstance().getTimeInMillis());
    }

    @NonNull
    static String generateIdString() {
        return UUID.randomUUID().toString();
    }

    /* access modifiers changed from: package-private */
    public boolean isRotationRequired() {
        return Calendar.getInstance().getTimeInMillis() - this.mLastRotation.getTimeInMillis() >= ROTATION_TIME_MS;
    }

    public String toString() {
        return "AdvertisingId{mLastRotation=" + this.mLastRotation + ", mAdvertisingId='" + this.mAdvertisingId + '\'' + ", mMopubId='" + this.mMopubId + '\'' + ", mDoNotTrack=" + this.mDoNotTrack + '}';
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AdvertisingId)) {
            return false;
        }
        AdvertisingId advertisingId = (AdvertisingId) obj;
        if (this.mDoNotTrack == advertisingId.mDoNotTrack && this.mAdvertisingId.equals(advertisingId.mAdvertisingId)) {
            return this.mMopubId.equals(advertisingId.mMopubId);
        }
        return false;
    }

    public int hashCode() {
        return (31 * ((this.mAdvertisingId.hashCode() * 31) + this.mMopubId.hashCode())) + (this.mDoNotTrack ? 1 : 0);
    }
}
