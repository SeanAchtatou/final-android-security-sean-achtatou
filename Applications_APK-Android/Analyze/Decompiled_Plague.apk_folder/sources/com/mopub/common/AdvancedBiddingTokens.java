package com.mopub.common;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Reflection;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class AdvancedBiddingTokens implements AdvancedBiddersInitializedListener {
    private static final String TOKEN_KEY = "token";
    @NonNull
    private List<MoPubAdvancedBidder> mAdvancedBidders = new ArrayList();
    @Nullable
    private final SdkInitializationListener mSdkInitializationListener;

    public AdvancedBiddingTokens(@Nullable SdkInitializationListener sdkInitializationListener) {
        this.mSdkInitializationListener = sdkInitializationListener;
    }

    public void addAdvancedBidders(@NonNull List<Class<? extends MoPubAdvancedBidder>> list) {
        Preconditions.checkNotNull(list);
        new AdvancedBiddersInitializationAsyncTask(list, this).execute(new Void[0]);
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String getTokensAsJsonString(@NonNull Context context) {
        Preconditions.checkNotNull(context);
        JSONObject tokensAsJsonObject = getTokensAsJsonObject(context);
        if (tokensAsJsonObject == null) {
            return null;
        }
        return tokensAsJsonObject.toString();
    }

    @Nullable
    private JSONObject getTokensAsJsonObject(@NonNull Context context) {
        Preconditions.checkNotNull(context);
        if (this.mAdvancedBidders.isEmpty()) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        for (MoPubAdvancedBidder next : this.mAdvancedBidders) {
            try {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put(TOKEN_KEY, next.getToken(context));
                jSONObject.put(next.getCreativeNetworkName(), jSONObject2);
            } catch (JSONException unused) {
                MoPubLog.d("JSON parsing failed for creative network name: " + next.getCreativeNetworkName());
            }
        }
        return jSONObject;
    }

    public void onAdvancedBiddersInitialized(@NonNull List<MoPubAdvancedBidder> list) {
        Preconditions.checkNotNull(list);
        this.mAdvancedBidders = list;
        if (this.mSdkInitializationListener != null) {
            this.mSdkInitializationListener.onInitializationFinished();
        }
    }

    private static class AdvancedBiddersInitializationAsyncTask extends AsyncTask<Void, Void, List<MoPubAdvancedBidder>> {
        @NonNull
        private final List<Class<? extends MoPubAdvancedBidder>> advancedBidderClasses;
        @NonNull
        private final AdvancedBiddersInitializedListener mAdvancedBiddersInitializedListener;

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((List<MoPubAdvancedBidder>) ((List) obj));
        }

        AdvancedBiddersInitializationAsyncTask(@NonNull List<Class<? extends MoPubAdvancedBidder>> list, @NonNull AdvancedBiddersInitializedListener advancedBiddersInitializedListener) {
            Preconditions.checkNotNull(list);
            Preconditions.checkNotNull(advancedBiddersInitializedListener);
            this.advancedBidderClasses = list;
            this.mAdvancedBiddersInitializedListener = advancedBiddersInitializedListener;
        }

        /* access modifiers changed from: protected */
        public List<MoPubAdvancedBidder> doInBackground(Void... voidArr) {
            ArrayList arrayList = new ArrayList();
            for (Class next : this.advancedBidderClasses) {
                try {
                    arrayList.add((MoPubAdvancedBidder) Reflection.instantiateClassWithEmptyConstructor(next.getName(), MoPubAdvancedBidder.class));
                } catch (Exception unused) {
                    MoPubLog.e("Unable to find class " + next.getName());
                }
            }
            return arrayList;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<MoPubAdvancedBidder> list) {
            this.mAdvancedBiddersInitializedListener.onAdvancedBiddersInitialized(list);
        }
    }
}
