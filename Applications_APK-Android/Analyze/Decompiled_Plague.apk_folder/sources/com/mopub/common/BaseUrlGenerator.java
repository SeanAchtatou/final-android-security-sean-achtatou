package com.mopub.common;

import android.graphics.Point;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.mopub.network.Networking;
import com.mopub.network.PlayServicesUrlRewriter;
import com.tapjoy.TapjoyAuctionFlags;

public abstract class BaseUrlGenerator {
    protected static final String AD_UNIT_ID_KEY = "id";
    protected static final String BUNDLE_ID_KEY = "bundle";
    protected static final String CONSENTED_PRIVACY_POLICY_VERSION_KEY = "consented_privacy_policy_version";
    protected static final String CONSENTED_VENDOR_LIST_VERSION_KEY = "consented_vendor_list_version";
    protected static final String CURRENT_CONSENT_STATUS_KEY = "current_consent_status";
    protected static final String DNT_KEY = "dnt";
    protected static final String FORCE_GDPR_APPLIES = "force_gdpr_applies";
    protected static final String GDPR_APPLIES = "gdpr_applies";
    private static final String HEIGHT_KEY = "h";
    protected static final String SDK_VERSION_KEY = "nv";
    protected static final String UDID_KEY = "udid";
    private static final String WIDTH_KEY = "w";
    private boolean mFirstParam;
    private StringBuilder mStringBuilder;

    public abstract String generateUrlString(String str);

    /* access modifiers changed from: protected */
    public void initUrlString(String str, String str2) {
        StringBuilder sb = new StringBuilder(Networking.getScheme());
        sb.append("://");
        sb.append(str);
        sb.append(str2);
        this.mStringBuilder = sb;
        this.mFirstParam = true;
    }

    /* access modifiers changed from: protected */
    public String getFinalUrlString() {
        return this.mStringBuilder.toString();
    }

    /* access modifiers changed from: protected */
    public void addParam(String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            this.mStringBuilder.append(getParamDelimiter());
            this.mStringBuilder.append(str);
            this.mStringBuilder.append("=");
            this.mStringBuilder.append(Uri.encode(str2));
        }
    }

    /* access modifiers changed from: protected */
    public void addParam(String str, Boolean bool) {
        if (bool != null) {
            this.mStringBuilder.append(getParamDelimiter());
            this.mStringBuilder.append(str);
            this.mStringBuilder.append("=");
            this.mStringBuilder.append(bool.booleanValue() ? TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE : "0");
        }
    }

    private String getParamDelimiter() {
        if (!this.mFirstParam) {
            return "&";
        }
        this.mFirstParam = false;
        return "?";
    }

    /* access modifiers changed from: protected */
    public void setApiVersion(String str) {
        addParam("v", str);
    }

    /* access modifiers changed from: protected */
    public void setAppVersion(String str) {
        addParam("av", str);
    }

    /* access modifiers changed from: protected */
    public void setExternalStoragePermission(boolean z) {
        addParam("android_perms_ext_storage", z ? TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE : "0");
    }

    /* access modifiers changed from: protected */
    public void setDeviceInfo(String... strArr) {
        StringBuilder sb = new StringBuilder();
        if (strArr != null && strArr.length >= 1) {
            for (int i = 0; i < strArr.length - 1; i++) {
                sb.append(strArr[i]);
                sb.append(",");
            }
            sb.append(strArr[strArr.length - 1]);
            addParam("dn", sb.toString());
        }
    }

    /* access modifiers changed from: protected */
    public void appendAdvertisingInfoTemplates() {
        addParam("udid", PlayServicesUrlRewriter.UDID_TEMPLATE);
        addParam(DNT_KEY, PlayServicesUrlRewriter.DO_NOT_TRACK_TEMPLATE);
    }

    /* access modifiers changed from: protected */
    public void setDeviceDimensions(@NonNull Point point) {
        addParam(WIDTH_KEY, "" + point.x);
        addParam(HEIGHT_KEY, "" + point.y);
    }
}
