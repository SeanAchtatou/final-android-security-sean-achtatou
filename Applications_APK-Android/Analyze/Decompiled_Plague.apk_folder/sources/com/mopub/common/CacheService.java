package com.mopub.common;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Utils;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

public class CacheService {
    private static final int APP_VERSION = 1;
    private static final int DISK_CACHE_INDEX = 0;
    static final String UNIQUE_CACHE_NAME = "mopub-cache";
    private static final int VALUE_COUNT = 1;
    private static DiskLruCache sDiskLruCache;

    public interface DiskLruCacheGetListener {
        void onComplete(String str, byte[] bArr);
    }

    public static boolean initializeDiskCache(Context context) {
        if (context == null) {
            return false;
        }
        if (sDiskLruCache == null) {
            File diskCacheDirectory = getDiskCacheDirectory(context);
            if (diskCacheDirectory == null) {
                return false;
            }
            try {
                sDiskLruCache = DiskLruCache.open(diskCacheDirectory, 1, 1, DeviceUtils.diskCacheSizeBytes(diskCacheDirectory));
            } catch (IOException e) {
                MoPubLog.d("Unable to create DiskLruCache", e);
                return false;
            }
        }
        return true;
    }

    public static void initialize(Context context) {
        initializeDiskCache(context);
    }

    public static String createValidDiskCacheKey(String str) {
        return Utils.sha1(str);
    }

    @Nullable
    public static File getDiskCacheDirectory(@NonNull Context context) {
        File cacheDir = context.getCacheDir();
        if (cacheDir == null) {
            return null;
        }
        String path = cacheDir.getPath();
        return new File(path + File.separator + UNIQUE_CACHE_NAME);
    }

    public static boolean containsKeyDiskCache(String str) {
        if (sDiskLruCache == null) {
            return false;
        }
        try {
            if (sDiskLruCache.get(createValidDiskCacheKey(str)) != null) {
                return true;
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    public static String getFilePathDiskCache(String str) {
        if (sDiskLruCache == null) {
            return null;
        }
        return sDiskLruCache.getDirectory() + File.separator + createValidDiskCacheKey(str) + "." + 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003a, code lost:
        if (r6 != null) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x003c, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0051, code lost:
        if (r6 == null) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0054, code lost:
        return r0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0058  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] getFromDiskCache(java.lang.String r6) {
        /*
            com.mopub.common.DiskLruCache r0 = com.mopub.common.CacheService.sDiskLruCache
            r1 = 0
            if (r0 != 0) goto L_0x0006
            return r1
        L_0x0006:
            com.mopub.common.DiskLruCache r0 = com.mopub.common.CacheService.sDiskLruCache     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
            java.lang.String r6 = createValidDiskCacheKey(r6)     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
            com.mopub.common.DiskLruCache$Snapshot r6 = r0.get(r6)     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
            if (r6 != 0) goto L_0x0018
            if (r6 == 0) goto L_0x0017
            r6.close()
        L_0x0017:
            return r1
        L_0x0018:
            r0 = 0
            java.io.InputStream r2 = r6.getInputStream(r0)     // Catch:{ Exception -> 0x0040 }
            if (r2 == 0) goto L_0x0039
            long r3 = r6.getLength(r0)     // Catch:{ Exception -> 0x0040 }
            int r0 = (int) r3     // Catch:{ Exception -> 0x0040 }
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0040 }
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0037 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0037 }
            com.mopub.common.util.Streams.readStream(r1, r0)     // Catch:{ all -> 0x0032 }
            com.mopub.common.util.Streams.closeStream(r1)     // Catch:{ Exception -> 0x0037 }
            goto L_0x003a
        L_0x0032:
            r2 = move-exception
            com.mopub.common.util.Streams.closeStream(r1)     // Catch:{ Exception -> 0x0037 }
            throw r2     // Catch:{ Exception -> 0x0037 }
        L_0x0037:
            r1 = move-exception
            goto L_0x004c
        L_0x0039:
            r0 = r1
        L_0x003a:
            if (r6 == 0) goto L_0x0054
        L_0x003c:
            r6.close()
            goto L_0x0054
        L_0x0040:
            r0 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x004c
        L_0x0045:
            r0 = move-exception
            r6 = r1
            goto L_0x0056
        L_0x0048:
            r6 = move-exception
            r0 = r1
            r1 = r6
            r6 = r0
        L_0x004c:
            java.lang.String r2 = "Unable to get from DiskLruCache"
            com.mopub.common.logging.MoPubLog.d(r2, r1)     // Catch:{ all -> 0x0055 }
            if (r6 == 0) goto L_0x0054
            goto L_0x003c
        L_0x0054:
            return r0
        L_0x0055:
            r0 = move-exception
        L_0x0056:
            if (r6 == 0) goto L_0x005b
            r6.close()
        L_0x005b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.common.CacheService.getFromDiskCache(java.lang.String):byte[]");
    }

    public static void getFromDiskCacheAsync(String str, DiskLruCacheGetListener diskLruCacheGetListener) {
        new DiskLruCacheGetTask(str, diskLruCacheGetListener).execute(new Void[0]);
    }

    public static boolean putToDiskCache(String str, byte[] bArr) {
        return putToDiskCache(str, new ByteArrayInputStream(bArr));
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x003b A[SYNTHETIC, Splitter:B:16:0x003b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean putToDiskCache(java.lang.String r3, java.io.InputStream r4) {
        /*
            com.mopub.common.DiskLruCache r0 = com.mopub.common.CacheService.sDiskLruCache
            r1 = 0
            if (r0 != 0) goto L_0x0006
            return r1
        L_0x0006:
            r0 = 0
            com.mopub.common.DiskLruCache r2 = com.mopub.common.CacheService.sDiskLruCache     // Catch:{ Exception -> 0x0032 }
            java.lang.String r3 = createValidDiskCacheKey(r3)     // Catch:{ Exception -> 0x0032 }
            com.mopub.common.DiskLruCache$Editor r3 = r2.edit(r3)     // Catch:{ Exception -> 0x0032 }
            if (r3 != 0) goto L_0x0014
            return r1
        L_0x0014:
            java.io.BufferedOutputStream r0 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x0030 }
            java.io.OutputStream r2 = r3.newOutputStream(r1)     // Catch:{ Exception -> 0x0030 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0030 }
            com.mopub.common.util.Streams.copyContent(r4, r0)     // Catch:{ Exception -> 0x0030 }
            r0.flush()     // Catch:{ Exception -> 0x0030 }
            r0.close()     // Catch:{ Exception -> 0x0030 }
            com.mopub.common.DiskLruCache r4 = com.mopub.common.CacheService.sDiskLruCache     // Catch:{ Exception -> 0x0030 }
            r4.flush()     // Catch:{ Exception -> 0x0030 }
            r3.commit()     // Catch:{ Exception -> 0x0030 }
            r3 = 1
            return r3
        L_0x0030:
            r4 = move-exception
            goto L_0x0034
        L_0x0032:
            r4 = move-exception
            r3 = r0
        L_0x0034:
            java.lang.String r0 = "Unable to put to DiskLruCache"
            com.mopub.common.logging.MoPubLog.d(r0, r4)
            if (r3 == 0) goto L_0x003e
            r3.abort()     // Catch:{ IOException -> 0x003e }
        L_0x003e:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.common.CacheService.putToDiskCache(java.lang.String, java.io.InputStream):boolean");
    }

    public static void putToDiskCacheAsync(String str, byte[] bArr) {
        new DiskLruCachePutTask(str, bArr).execute(new Void[0]);
    }

    private static class DiskLruCacheGetTask extends AsyncTask<Void, Void, byte[]> {
        private final DiskLruCacheGetListener mDiskLruCacheGetListener;
        private final String mKey;

        DiskLruCacheGetTask(String str, DiskLruCacheGetListener diskLruCacheGetListener) {
            this.mDiskLruCacheGetListener = diskLruCacheGetListener;
            this.mKey = str;
        }

        /* access modifiers changed from: protected */
        public byte[] doInBackground(Void... voidArr) {
            return CacheService.getFromDiskCache(this.mKey);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(byte[] bArr) {
            if (isCancelled()) {
                onCancelled();
            } else if (this.mDiskLruCacheGetListener != null) {
                this.mDiskLruCacheGetListener.onComplete(this.mKey, bArr);
            }
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            if (this.mDiskLruCacheGetListener != null) {
                this.mDiskLruCacheGetListener.onComplete(this.mKey, null);
            }
        }
    }

    private static class DiskLruCachePutTask extends AsyncTask<Void, Void, Void> {
        private final byte[] mContent;
        private final String mKey;

        DiskLruCachePutTask(String str, byte[] bArr) {
            this.mKey = str;
            this.mContent = bArr;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voidArr) {
            CacheService.putToDiskCache(this.mKey, this.mContent);
            return null;
        }
    }

    @Deprecated
    @VisibleForTesting
    public static void clearAndNullCaches() {
        if (sDiskLruCache != null) {
            try {
                sDiskLruCache.delete();
                sDiskLruCache = null;
            } catch (IOException unused) {
                sDiskLruCache = null;
            }
        }
    }

    @Deprecated
    @VisibleForTesting
    public static DiskLruCache getDiskLruCache() {
        return sDiskLruCache;
    }
}
