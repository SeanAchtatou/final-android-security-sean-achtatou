package com.mopub.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.mopub.common.UrlResolutionTask;
import com.mopub.common.logging.MoPubLog;
import java.util.EnumSet;

public class UrlHandler {
    /* access modifiers changed from: private */
    public static final ResultActions EMPTY_CLICK_LISTENER = new ResultActions() {
        public void urlHandlingFailed(@NonNull String str, @NonNull UrlAction urlAction) {
        }

        public void urlHandlingSucceeded(@NonNull String str, @NonNull UrlAction urlAction) {
        }
    };
    /* access modifiers changed from: private */
    public static final MoPubSchemeListener EMPTY_MOPUB_SCHEME_LISTENER = new MoPubSchemeListener() {
        public void onClose() {
        }

        public void onFailLoad() {
        }

        public void onFinishLoad() {
        }
    };
    private boolean mAlreadySucceeded;
    @Nullable
    private String mDspCreativeId;
    @NonNull
    private MoPubSchemeListener mMoPubSchemeListener;
    @NonNull
    private ResultActions mResultActions;
    private boolean mSkipShowMoPubBrowser;
    @NonNull
    private EnumSet<UrlAction> mSupportedUrlActions;
    /* access modifiers changed from: private */
    public boolean mTaskPending;

    public interface MoPubSchemeListener {
        void onClose();

        void onFailLoad();

        void onFinishLoad();
    }

    public interface ResultActions {
        void urlHandlingFailed(@NonNull String str, @NonNull UrlAction urlAction);

        void urlHandlingSucceeded(@NonNull String str, @NonNull UrlAction urlAction);
    }

    public static class Builder {
        @Nullable
        private String creativeId;
        @NonNull
        private MoPubSchemeListener moPubSchemeListener = UrlHandler.EMPTY_MOPUB_SCHEME_LISTENER;
        @NonNull
        private ResultActions resultActions = UrlHandler.EMPTY_CLICK_LISTENER;
        private boolean skipShowMoPubBrowser = false;
        @NonNull
        private EnumSet<UrlAction> supportedUrlActions = EnumSet.of(UrlAction.NOOP);

        public Builder withSupportedUrlActions(@NonNull UrlAction urlAction, @Nullable UrlAction... urlActionArr) {
            this.supportedUrlActions = EnumSet.of(urlAction, urlActionArr);
            return this;
        }

        public Builder withSupportedUrlActions(@NonNull EnumSet<UrlAction> enumSet) {
            this.supportedUrlActions = EnumSet.copyOf((EnumSet) enumSet);
            return this;
        }

        public Builder withResultActions(@NonNull ResultActions resultActions2) {
            this.resultActions = resultActions2;
            return this;
        }

        public Builder withMoPubSchemeListener(@NonNull MoPubSchemeListener moPubSchemeListener2) {
            this.moPubSchemeListener = moPubSchemeListener2;
            return this;
        }

        public Builder withoutMoPubBrowser() {
            this.skipShowMoPubBrowser = true;
            return this;
        }

        public Builder withDspCreativeId(@Nullable String str) {
            this.creativeId = str;
            return this;
        }

        public UrlHandler build() {
            return new UrlHandler(this.supportedUrlActions, this.resultActions, this.moPubSchemeListener, this.skipShowMoPubBrowser, this.creativeId);
        }
    }

    private UrlHandler(@NonNull EnumSet<UrlAction> enumSet, @NonNull ResultActions resultActions, @NonNull MoPubSchemeListener moPubSchemeListener, boolean z, @Nullable String str) {
        this.mSupportedUrlActions = EnumSet.copyOf((EnumSet) enumSet);
        this.mResultActions = resultActions;
        this.mMoPubSchemeListener = moPubSchemeListener;
        this.mSkipShowMoPubBrowser = z;
        this.mDspCreativeId = str;
        this.mAlreadySucceeded = false;
        this.mTaskPending = false;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public EnumSet<UrlAction> getSupportedUrlActions() {
        return EnumSet.copyOf((EnumSet) this.mSupportedUrlActions);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public ResultActions getResultActions() {
        return this.mResultActions;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public MoPubSchemeListener getMoPubSchemeListener() {
        return this.mMoPubSchemeListener;
    }

    /* access modifiers changed from: package-private */
    public boolean shouldSkipShowMoPubBrowser() {
        return this.mSkipShowMoPubBrowser;
    }

    public void handleUrl(@NonNull Context context, @NonNull String str) {
        Preconditions.checkNotNull(context);
        handleUrl(context, str, true);
    }

    public void handleUrl(@NonNull Context context, @NonNull String str, boolean z) {
        Preconditions.checkNotNull(context);
        handleUrl(context, str, z, null);
    }

    public void handleUrl(@NonNull Context context, @NonNull String str, boolean z, @Nullable Iterable<String> iterable) {
        Preconditions.checkNotNull(context);
        if (TextUtils.isEmpty(str)) {
            failUrlHandling(str, null, "Attempted to handle empty url.", null);
            return;
        }
        final Context context2 = context;
        final boolean z2 = z;
        final Iterable<String> iterable2 = iterable;
        final String str2 = str;
        UrlResolutionTask.getResolvedUrl(str, new UrlResolutionTask.UrlResolutionListener() {
            public void onSuccess(@NonNull String str) {
                boolean unused = UrlHandler.this.mTaskPending = false;
                UrlHandler.this.handleResolvedUrl(context2, str, z2, iterable2);
            }

            public void onFailure(@NonNull String str, @Nullable Throwable th) {
                boolean unused = UrlHandler.this.mTaskPending = false;
                UrlHandler.this.failUrlHandling(str2, null, str, th);
            }
        });
        this.mTaskPending = true;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v0, resolved type: com.mopub.common.UrlAction} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean handleResolvedUrl(@android.support.annotation.NonNull android.content.Context r15, @android.support.annotation.NonNull java.lang.String r16, boolean r17, @android.support.annotation.Nullable java.lang.Iterable<java.lang.String> r18) {
        /*
            r14 = this;
            r7 = r14
            r8 = r16
            boolean r1 = android.text.TextUtils.isEmpty(r16)
            r9 = 0
            r10 = 0
            if (r1 == 0) goto L_0x0011
            java.lang.String r1 = "Attempted to handle empty url."
            r7.failUrlHandling(r8, r10, r1, r10)
            return r9
        L_0x0011:
            com.mopub.common.UrlAction r1 = com.mopub.common.UrlAction.NOOP
            android.net.Uri r11 = android.net.Uri.parse(r16)
            java.util.EnumSet<com.mopub.common.UrlAction> r2 = r7.mSupportedUrlActions
            java.util.Iterator r12 = r2.iterator()
        L_0x001d:
            boolean r2 = r12.hasNext()
            if (r2 == 0) goto L_0x007b
            java.lang.Object r2 = r12.next()
            r13 = r2
            com.mopub.common.UrlAction r13 = (com.mopub.common.UrlAction) r13
            boolean r2 = r13.shouldTryHandlingUrl(r11)
            if (r2 == 0) goto L_0x0077
            java.lang.String r6 = r7.mDspCreativeId     // Catch:{ IntentNotResolvableException -> 0x0069 }
            r1 = r13
            r2 = r7
            r3 = r15
            r4 = r11
            r5 = r17
            r1.handleUrl(r2, r3, r4, r5, r6)     // Catch:{ IntentNotResolvableException -> 0x0069 }
            boolean r1 = r7.mAlreadySucceeded     // Catch:{ IntentNotResolvableException -> 0x0069 }
            r2 = 1
            if (r1 != 0) goto L_0x0068
            boolean r1 = r7.mTaskPending     // Catch:{ IntentNotResolvableException -> 0x0069 }
            if (r1 != 0) goto L_0x0068
            com.mopub.common.UrlAction r1 = com.mopub.common.UrlAction.IGNORE_ABOUT_SCHEME     // Catch:{ IntentNotResolvableException -> 0x0069 }
            boolean r1 = r1.equals(r13)     // Catch:{ IntentNotResolvableException -> 0x0069 }
            if (r1 != 0) goto L_0x0068
            com.mopub.common.UrlAction r1 = com.mopub.common.UrlAction.HANDLE_MOPUB_SCHEME     // Catch:{ IntentNotResolvableException -> 0x0069 }
            boolean r1 = r1.equals(r13)     // Catch:{ IntentNotResolvableException -> 0x0069 }
            if (r1 != 0) goto L_0x0068
            r3 = r15
            r4 = r18
            com.mopub.network.TrackingRequest.makeTrackingHttpRequest(r4, r3)     // Catch:{ IntentNotResolvableException -> 0x0066 }
            com.mopub.common.UrlHandler$ResultActions r1 = r7.mResultActions     // Catch:{ IntentNotResolvableException -> 0x0066 }
            java.lang.String r5 = r11.toString()     // Catch:{ IntentNotResolvableException -> 0x0066 }
            r1.urlHandlingSucceeded(r5, r13)     // Catch:{ IntentNotResolvableException -> 0x0066 }
            r7.mAlreadySucceeded = r2     // Catch:{ IntentNotResolvableException -> 0x0066 }
            goto L_0x0068
        L_0x0066:
            r0 = move-exception
            goto L_0x006d
        L_0x0068:
            return r2
        L_0x0069:
            r0 = move-exception
            r3 = r15
            r4 = r18
        L_0x006d:
            r1 = r0
            java.lang.String r2 = r1.getMessage()
            com.mopub.common.logging.MoPubLog.d(r2, r1)
            r1 = r13
            goto L_0x001d
        L_0x0077:
            r3 = r15
            r4 = r18
            goto L_0x001d
        L_0x007b:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Link ignored. Unable to handle url: "
            r2.append(r3)
            r2.append(r8)
            java.lang.String r2 = r2.toString()
            r7.failUrlHandling(r8, r1, r2, r10)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.common.UrlHandler.handleResolvedUrl(android.content.Context, java.lang.String, boolean, java.lang.Iterable):boolean");
    }

    /* access modifiers changed from: private */
    public void failUrlHandling(@Nullable String str, @Nullable UrlAction urlAction, @NonNull String str2, @Nullable Throwable th) {
        Preconditions.checkNotNull(str2);
        if (urlAction == null) {
            urlAction = UrlAction.NOOP;
        }
        MoPubLog.d(str2, th);
        this.mResultActions.urlHandlingFailed(str, urlAction);
    }
}
