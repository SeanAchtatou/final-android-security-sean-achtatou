package com.mopub.common;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import com.mopub.common.ExternalViewabilitySession;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Reflection;
import com.mopub.mobileads.VastExtensionXmlManager;
import com.tapjoy.TJAdUnitConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

class MoatViewabilitySession implements ExternalViewabilitySession {
    private static final String DEFAULT_PARTNER_CODE = "mopubinapphtmvideo468906546585";
    private static final String MOAT_AD_EVENT_PATH = "com.moat.analytics.mobile.mpub.MoatAdEvent";
    private static final String MOAT_AD_EVENT_TYPE_PATH = "com.moat.analytics.mobile.mpub.MoatAdEventType";
    private static final String MOAT_ANALYTICS_PATH = "com.moat.analytics.mobile.mpub.MoatAnalytics";
    private static final String MOAT_FACTORY_PATH = "com.moat.analytics.mobile.mpub.MoatFactory";
    private static final String MOAT_KEY = "moat";
    private static final String MOAT_OPTIONS_PATH = "com.moat.analytics.mobile.mpub.MoatOptions";
    private static final String MOAT_PLUGIN_PATH = "com.moat.analytics.mobile.mpub.MoatPlugin";
    private static final String MOAT_REACTIVE_VIDEO_TRACKER_PLUGIN_PATH = "com.moat.analytics.mobile.mpub.ReactiveVideoTrackerPlugin";
    private static final String MOAT_VAST_IDS_KEY = "zMoatVASTIDs";
    private static final String PARTNER_CODE_KEY = "partnerCode";
    private static final Map<String, String> QUERY_PARAM_MAPPING = new HashMap();
    private static boolean sIsVendorDisabled = false;
    private static Boolean sIsViewabilityEnabledViaReflection = null;
    private static boolean sWasInitialized = false;
    @NonNull
    private Map<String, String> mAdIds = new HashMap();
    @Nullable
    private Object mMoatVideoTracker;
    @Nullable
    private Object mMoatWebAdTracker;
    private boolean mWasVideoPrepared;

    @NonNull
    public String getName() {
        return VastExtensionXmlManager.MOAT;
    }

    MoatViewabilitySession() {
    }

    static {
        QUERY_PARAM_MAPPING.put("moatClientLevel1", "level1");
        QUERY_PARAM_MAPPING.put("moatClientLevel2", "level2");
        QUERY_PARAM_MAPPING.put("moatClientLevel3", "level3");
        QUERY_PARAM_MAPPING.put("moatClientLevel4", "level4");
        QUERY_PARAM_MAPPING.put("moatClientSlicer1", "slicer1");
        QUERY_PARAM_MAPPING.put("moatClientSlicer2", "slicer2");
    }

    static boolean isEnabled() {
        return !sIsVendorDisabled && isViewabilityEnabledViaReflection();
    }

    static void disable() {
        sIsVendorDisabled = true;
    }

    private static boolean isViewabilityEnabledViaReflection() {
        if (sIsViewabilityEnabledViaReflection == null) {
            sIsViewabilityEnabledViaReflection = Boolean.valueOf(Reflection.classFound(MOAT_FACTORY_PATH));
            StringBuilder sb = new StringBuilder();
            sb.append("Moat is ");
            sb.append(sIsViewabilityEnabledViaReflection.booleanValue() ? "" : "un");
            sb.append("available via reflection.");
            MoPubLog.d(sb.toString());
        }
        return sIsViewabilityEnabledViaReflection.booleanValue();
    }

    @Nullable
    public Boolean initialize(@NonNull Context context) {
        Application application;
        Preconditions.checkNotNull(context);
        if (!isEnabled()) {
            return null;
        }
        if (sWasInitialized) {
            return true;
        }
        if (context instanceof Activity) {
            application = ((Activity) context).getApplication();
        } else {
            try {
                application = (Application) context.getApplicationContext();
            } catch (ClassCastException unused) {
                MoPubLog.d("Unable to initialize Moat, error obtaining application context.");
                return false;
            }
        }
        try {
            Object instantiateClassWithEmptyConstructor = Reflection.instantiateClassWithEmptyConstructor(MOAT_OPTIONS_PATH, Object.class);
            instantiateClassWithEmptyConstructor.getClass().getField("disableAdIdCollection").setBoolean(instantiateClassWithEmptyConstructor, true);
            instantiateClassWithEmptyConstructor.getClass().getField("disableLocationServices").setBoolean(instantiateClassWithEmptyConstructor, true);
            new Reflection.MethodBuilder(new Reflection.MethodBuilder(null, "getInstance").setStatic(MOAT_ANALYTICS_PATH).execute(), TJAdUnitConstants.String.VIDEO_START).addParam(MOAT_OPTIONS_PATH, instantiateClassWithEmptyConstructor).addParam(Application.class, application).execute();
            sWasInitialized = true;
            return true;
        } catch (Exception e) {
            MoPubLog.d("Unable to initialize Moat: " + e.getMessage());
            return false;
        }
    }

    @Nullable
    public Boolean invalidate() {
        if (!isEnabled()) {
            return null;
        }
        this.mMoatWebAdTracker = null;
        this.mMoatVideoTracker = null;
        this.mAdIds.clear();
        return true;
    }

    @Nullable
    public Boolean createDisplaySession(@NonNull Context context, @NonNull WebView webView, boolean z) {
        Preconditions.checkNotNull(context);
        if (!isEnabled()) {
            return null;
        }
        try {
            this.mMoatWebAdTracker = new Reflection.MethodBuilder(new Reflection.MethodBuilder(null, "create").setStatic(MOAT_FACTORY_PATH).execute(), "createWebAdTracker").addParam(WebView.class, webView).execute();
            if (!z) {
                new Reflection.MethodBuilder(this.mMoatWebAdTracker, "startTracking").execute();
            }
            return true;
        } catch (Exception e) {
            MoPubLog.d("Unable to execute Moat start display session: " + e.getMessage());
            return false;
        }
    }

    @Nullable
    public Boolean startDeferredDisplaySession(@NonNull Activity activity) {
        if (!isEnabled()) {
            return null;
        }
        if (this.mMoatWebAdTracker == null) {
            MoPubLog.d("MoatWebAdTracker unexpectedly null.");
            return false;
        }
        try {
            new Reflection.MethodBuilder(this.mMoatWebAdTracker, "startTracking").execute();
            return true;
        } catch (Exception e) {
            MoPubLog.d("Unable to record deferred display session for Moat: " + e.getMessage());
            return false;
        }
    }

    @Nullable
    public Boolean endDisplaySession() {
        if (!isEnabled()) {
            return null;
        }
        if (this.mMoatWebAdTracker == null) {
            MoPubLog.d("Moat WebAdTracker unexpectedly null.");
            return false;
        }
        try {
            new Reflection.MethodBuilder(this.mMoatWebAdTracker, "stopTracking").execute();
            return true;
        } catch (Exception e) {
            MoPubLog.d("Unable to execute Moat end session: " + e.getMessage());
            return false;
        }
    }

    @Nullable
    public Boolean createVideoSession(@NonNull Activity activity, @NonNull View view, @NonNull Set<String> set, @NonNull Map<String, String> map) {
        Preconditions.checkNotNull(activity);
        Preconditions.checkNotNull(view);
        Preconditions.checkNotNull(set);
        Preconditions.checkNotNull(map);
        if (!isEnabled()) {
            return null;
        }
        updateAdIdsFromUrlStringAndBuyerResources(map.get(MOAT_KEY), set);
        String str = this.mAdIds.get("partnerCode");
        if (TextUtils.isEmpty(str)) {
            MoPubLog.d("partnerCode was empty when starting Moat video session");
            return false;
        }
        try {
            this.mMoatVideoTracker = new Reflection.MethodBuilder(new Reflection.MethodBuilder(null, "create").setStatic(MOAT_FACTORY_PATH).execute(), "createCustomTracker").addParam(MOAT_PLUGIN_PATH, Reflection.instantiateClassWithConstructor(MOAT_REACTIVE_VIDEO_TRACKER_PLUGIN_PATH, Object.class, new Class[]{String.class}, new Object[]{str})).execute();
            return true;
        } catch (Exception e) {
            MoPubLog.d("Unable to execute Moat start video session: " + e.getMessage());
            return false;
        }
    }

    @Nullable
    public Boolean registerVideoObstruction(@NonNull View view) {
        Preconditions.checkNotNull(view);
        if (!isEnabled()) {
            return null;
        }
        return true;
    }

    @Nullable
    public Boolean onVideoPrepared(@NonNull View view, int i) {
        Preconditions.checkNotNull(view);
        if (!isEnabled()) {
            return null;
        }
        if (this.mMoatVideoTracker == null) {
            MoPubLog.d("Moat VideoAdTracker unexpectedly null.");
            return false;
        } else if (this.mWasVideoPrepared) {
            return false;
        } else {
            try {
                new Reflection.MethodBuilder(this.mMoatVideoTracker, "trackVideoAd").addParam(Map.class, this.mAdIds).addParam(Integer.class, Integer.valueOf(i)).addParam(View.class, view).execute();
                this.mWasVideoPrepared = true;
                return true;
            } catch (Exception e) {
                MoPubLog.d("Unable to execute Moat onVideoPrepared: " + e.getMessage());
                return false;
            }
        }
    }

    @Nullable
    public Boolean recordVideoEvent(@NonNull ExternalViewabilitySession.VideoEvent videoEvent, int i) {
        Preconditions.checkNotNull(videoEvent);
        if (!isEnabled()) {
            return null;
        }
        if (this.mMoatVideoTracker == null) {
            MoPubLog.d("Moat VideoAdTracker unexpectedly null.");
            return false;
        }
        try {
            switch (videoEvent) {
                case AD_STARTED:
                case AD_STOPPED:
                case AD_PAUSED:
                case AD_PLAYING:
                case AD_SKIPPED:
                case AD_VIDEO_FIRST_QUARTILE:
                case AD_VIDEO_MIDPOINT:
                case AD_VIDEO_THIRD_QUARTILE:
                case AD_COMPLETE:
                    handleVideoEventReflection(videoEvent, i);
                    return true;
                case AD_LOADED:
                case AD_IMPRESSED:
                case AD_CLICK_THRU:
                case RECORD_AD_ERROR:
                    return null;
                default:
                    MoPubLog.d("Unexpected video event: " + videoEvent.getMoatEnumName());
                    return false;
            }
        } catch (Exception e) {
            MoPubLog.d("Video event " + videoEvent.getMoatEnumName() + " failed. " + e.getMessage());
            return false;
        }
    }

    @Nullable
    public Boolean endVideoSession() {
        if (!isEnabled()) {
            return null;
        }
        if (this.mMoatVideoTracker == null) {
            MoPubLog.d("Moat VideoAdTracker unexpectedly null.");
            return false;
        }
        try {
            new Reflection.MethodBuilder(this.mMoatVideoTracker, "stopTracking").execute();
            return true;
        } catch (Exception e) {
            MoPubLog.d("Unable to execute Moat end video session: " + e.getMessage());
            return false;
        }
    }

    private void updateAdIdsFromUrlStringAndBuyerResources(@Nullable String str, @Nullable Set<String> set) {
        this.mAdIds.clear();
        this.mAdIds.put("partnerCode", DEFAULT_PARTNER_CODE);
        this.mAdIds.put(MOAT_VAST_IDS_KEY, TextUtils.join(";", set));
        if (!TextUtils.isEmpty(str)) {
            Uri parse = Uri.parse(str);
            List<String> pathSegments = parse.getPathSegments();
            if (pathSegments.size() > 0 && !TextUtils.isEmpty(pathSegments.get(0))) {
                this.mAdIds.put("partnerCode", pathSegments.get(0));
            }
            String fragment = parse.getFragment();
            if (!TextUtils.isEmpty(fragment)) {
                for (String split : fragment.split("&")) {
                    String[] split2 = split.split("=");
                    if (split2.length >= 2) {
                        String str2 = split2[0];
                        String str3 = split2[1];
                        if (!TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str3) && QUERY_PARAM_MAPPING.containsKey(str2)) {
                            this.mAdIds.put(QUERY_PARAM_MAPPING.get(str2), str3);
                        }
                    }
                }
            }
        }
    }

    private boolean handleVideoEventReflection(@NonNull ExternalViewabilitySession.VideoEvent videoEvent, int i) throws Exception {
        if (videoEvent.getMoatEnumName() == null) {
            return false;
        }
        Class<?> cls = Class.forName(MOAT_AD_EVENT_TYPE_PATH);
        new Reflection.MethodBuilder(this.mMoatVideoTracker, "dispatchEvent").addParam(MOAT_AD_EVENT_PATH, Reflection.instantiateClassWithConstructor(MOAT_AD_EVENT_PATH, Object.class, new Class[]{cls, Integer.class}, new Object[]{Enum.valueOf(cls.asSubclass(Enum.class), videoEvent.getMoatEnumName()), Integer.valueOf(i)})).execute();
        return true;
    }
}
