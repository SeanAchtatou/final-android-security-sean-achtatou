package com.mopub.common;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.mopub.common.MoPub;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.mobileads.GooglePlayServicesInterstitial;
import java.math.BigDecimal;

public class LocationService {
    private static volatile LocationService sInstance;
    @Nullable
    @VisibleForTesting
    Location mLastKnownLocation;
    @VisibleForTesting
    long mLocationLastUpdatedMillis;

    public enum LocationAwareness {
        NORMAL,
        TRUNCATED,
        DISABLED;

        @Deprecated
        public MoPub.LocationAwareness getNewLocationAwareness() {
            if (this == TRUNCATED) {
                return MoPub.LocationAwareness.TRUNCATED;
            }
            if (this == DISABLED) {
                return MoPub.LocationAwareness.DISABLED;
            }
            return MoPub.LocationAwareness.NORMAL;
        }

        @Deprecated
        public static LocationAwareness fromMoPubLocationAwareness(MoPub.LocationAwareness locationAwareness) {
            if (locationAwareness == MoPub.LocationAwareness.DISABLED) {
                return DISABLED;
            }
            if (locationAwareness == MoPub.LocationAwareness.TRUNCATED) {
                return TRUNCATED;
            }
            return NORMAL;
        }
    }

    private LocationService() {
    }

    @NonNull
    @VisibleForTesting
    static LocationService getInstance() {
        LocationService locationService = sInstance;
        if (locationService == null) {
            synchronized (LocationService.class) {
                locationService = sInstance;
                if (locationService == null) {
                    locationService = new LocationService();
                    sInstance = locationService;
                }
            }
        }
        return locationService;
    }

    public enum ValidLocationProvider {
        NETWORK("network"),
        GPS("gps");
        
        @NonNull
        final String name;

        private ValidLocationProvider(@NonNull String str) {
            this.name = str;
        }

        public String toString() {
            return this.name;
        }

        private boolean hasRequiredPermissions(@NonNull Context context) {
            switch (this) {
                case NETWORK:
                    if (DeviceUtils.isPermissionGranted(context, "android.permission.ACCESS_FINE_LOCATION") || DeviceUtils.isPermissionGranted(context, "android.permission.ACCESS_COARSE_LOCATION")) {
                        return true;
                    }
                    return false;
                case GPS:
                    return DeviceUtils.isPermissionGranted(context, "android.permission.ACCESS_FINE_LOCATION");
                default:
                    return false;
            }
        }
    }

    @Nullable
    public static Location getLastKnownLocation(@NonNull Context context, int i, @NonNull MoPub.LocationAwareness locationAwareness) {
        if (!MoPub.canCollectPersonalInformation()) {
            return null;
        }
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(locationAwareness);
        if (locationAwareness == MoPub.LocationAwareness.DISABLED) {
            return null;
        }
        LocationService instance = getInstance();
        if (isLocationFreshEnough()) {
            return instance.mLastKnownLocation;
        }
        Location mostRecentValidLocation = getMostRecentValidLocation(getLocationFromProvider(context, ValidLocationProvider.GPS), getLocationFromProvider(context, ValidLocationProvider.NETWORK));
        if (locationAwareness == MoPub.LocationAwareness.TRUNCATED) {
            truncateLocationLatLon(mostRecentValidLocation, i);
        }
        instance.mLastKnownLocation = mostRecentValidLocation;
        instance.mLocationLastUpdatedMillis = SystemClock.elapsedRealtime();
        return mostRecentValidLocation;
    }

    @Nullable
    @VisibleForTesting
    static Location getLocationFromProvider(@NonNull Context context, @NonNull ValidLocationProvider validLocationProvider) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(validLocationProvider);
        if (!MoPub.canCollectPersonalInformation() || !ValidLocationProvider.access$000(validLocationProvider, context)) {
            return null;
        }
        try {
            return ((LocationManager) context.getSystemService(GooglePlayServicesInterstitial.LOCATION_KEY)).getLastKnownLocation(validLocationProvider.toString());
        } catch (SecurityException unused) {
            MoPubLog.d("Failed to retrieve location from " + validLocationProvider.toString() + " provider: access appears to be disabled.");
            return null;
        } catch (IllegalArgumentException unused2) {
            MoPubLog.d("Failed to retrieve location: device has no " + validLocationProvider.toString() + " location provider.");
            return null;
        } catch (NullPointerException unused3) {
            MoPubLog.d("Failed to retrieve location: device has no " + validLocationProvider.toString() + " location provider.");
            return null;
        }
    }

    @Nullable
    @VisibleForTesting
    static Location getMostRecentValidLocation(@Nullable Location location, @Nullable Location location2) {
        if (location == null) {
            return location2;
        }
        return (location2 != null && location.getTime() <= location2.getTime()) ? location2 : location;
    }

    @VisibleForTesting
    static void truncateLocationLatLon(@Nullable Location location, int i) {
        if (location != null && i >= 0) {
            location.setLatitude(BigDecimal.valueOf(location.getLatitude()).setScale(i, 5).doubleValue());
            location.setLongitude(BigDecimal.valueOf(location.getLongitude()).setScale(i, 5).doubleValue());
        }
    }

    private static boolean isLocationFreshEnough() {
        LocationService instance = getInstance();
        if (instance.mLastKnownLocation != null && SystemClock.elapsedRealtime() - instance.mLocationLastUpdatedMillis <= MoPub.getMinimumLocationRefreshTimeMillis()) {
            return true;
        }
        return false;
    }

    @Deprecated
    @VisibleForTesting
    public static void clearLastKnownLocation() {
        getInstance().mLastKnownLocation = null;
    }
}
