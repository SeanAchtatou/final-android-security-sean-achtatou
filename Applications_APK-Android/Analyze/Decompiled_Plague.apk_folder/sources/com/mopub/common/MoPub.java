package com.mopub.common;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.mopub.common.ExternalViewabilitySessionManager;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.common.util.Reflection;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MoPub {
    private static final int DEFAULT_LOCATION_PRECISION = 6;
    private static final long DEFAULT_LOCATION_REFRESH_TIME_MILLIS = 60000;
    private static final String MOPUB_REWARDED_VIDEOS = "com.mopub.mobileads.MoPubRewardedVideos";
    private static final String MOPUB_REWARDED_VIDEO_MANAGER = "com.mopub.mobileads.MoPubRewardedVideoManager";
    public static final String SDK_VERSION = "5.2.0";
    private static boolean sAdvancedBiddingEnabled = true;
    private static AdvancedBiddingTokens sAdvancedBiddingTokens = null;
    @NonNull
    private static volatile BrowserAgent sBrowserAgent = BrowserAgent.IN_APP;
    private static volatile boolean sIsBrowserAgentOverriddenByClient = false;
    @NonNull
    private static volatile LocationAwareness sLocationAwareness = LocationAwareness.NORMAL;
    private static volatile int sLocationPrecision = 6;
    private static volatile long sMinimumLocationRefreshTimeMillis = 60000;
    private static PersonalInfoManager sPersonalInfoManager = null;
    /* access modifiers changed from: private */
    public static boolean sSdkInitialized = false;
    /* access modifiers changed from: private */
    public static boolean sSdkInitializing = false;
    private static boolean sSearchedForUpdateActivityMethod = false;
    @Nullable
    private static Method sUpdateActivityMethod;

    public enum LocationAwareness {
        NORMAL,
        TRUNCATED,
        DISABLED
    }

    public enum BrowserAgent {
        IN_APP,
        NATIVE;

        @NonNull
        public static BrowserAgent fromHeader(@Nullable Integer num) {
            if (num == null) {
                return IN_APP;
            }
            return num.intValue() == 1 ? NATIVE : IN_APP;
        }
    }

    @NonNull
    public static LocationAwareness getLocationAwareness() {
        Preconditions.checkNotNull(sLocationAwareness);
        return sLocationAwareness;
    }

    public static void setLocationAwareness(@NonNull LocationAwareness locationAwareness) {
        Preconditions.checkNotNull(locationAwareness);
        sLocationAwareness = locationAwareness;
    }

    public static int getLocationPrecision() {
        return sLocationPrecision;
    }

    public static void setLocationPrecision(int i) {
        sLocationPrecision = Math.min(Math.max(0, i), 6);
    }

    public static void setMinimumLocationRefreshTimeMillis(long j) {
        sMinimumLocationRefreshTimeMillis = j;
    }

    public static long getMinimumLocationRefreshTimeMillis() {
        return sMinimumLocationRefreshTimeMillis;
    }

    public static void setBrowserAgent(@NonNull BrowserAgent browserAgent) {
        Preconditions.checkNotNull(browserAgent);
        sBrowserAgent = browserAgent;
        sIsBrowserAgentOverriddenByClient = true;
    }

    public static void setBrowserAgentFromAdServer(@NonNull BrowserAgent browserAgent) {
        Preconditions.checkNotNull(browserAgent);
        if (sIsBrowserAgentOverriddenByClient) {
            MoPubLog.w("Browser agent already overridden by client with value " + sBrowserAgent);
            return;
        }
        sBrowserAgent = browserAgent;
    }

    @NonNull
    public static BrowserAgent getBrowserAgent() {
        Preconditions.checkNotNull(sBrowserAgent);
        return sBrowserAgent;
    }

    public static void setAdvancedBiddingEnabled(boolean z) {
        sAdvancedBiddingEnabled = z;
    }

    public static boolean isAdvancedBiddingEnabled() {
        return sAdvancedBiddingEnabled;
    }

    public static void initializeSdk(@NonNull Context context, @NonNull SdkConfiguration sdkConfiguration, @Nullable final SdkInitializationListener sdkInitializationListener) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(sdkConfiguration);
        MoPubLog.d("Initializing MoPub with ad unit: " + sdkConfiguration.getAdUnitId());
        if ((context instanceof Activity) && Reflection.classFound(MOPUB_REWARDED_VIDEO_MANAGER)) {
            initializeRewardedVideo((Activity) context, sdkConfiguration);
        }
        if (sSdkInitialized) {
            MoPubLog.d("MoPub SDK is already initialized");
        } else if (sSdkInitializing) {
            MoPubLog.d("MoPub SDK is currently initializing.");
        } else if (Looper.getMainLooper() != Looper.myLooper()) {
            MoPubLog.e("MoPub can only be initialized on the main thread.");
        } else {
            sSdkInitializing = true;
            CompositeSdkInitializationListener compositeSdkInitializationListener = new CompositeSdkInitializationListener(new SdkInitializationListener() {
                public void onInitializationFinished() {
                    boolean unused = MoPub.sSdkInitializing = false;
                    boolean unused2 = MoPub.sSdkInitialized = true;
                    if (sdkInitializationListener != null) {
                        sdkInitializationListener.onInitializationFinished();
                    }
                }
            }, 2);
            sPersonalInfoManager = new PersonalInfoManager(context, sdkConfiguration.getAdUnitId(), compositeSdkInitializationListener);
            ClientMetadata.getInstance(context);
            sAdvancedBiddingTokens = new AdvancedBiddingTokens(compositeSdkInitializationListener);
            sAdvancedBiddingTokens.addAdvancedBidders(sdkConfiguration.getAdvancedBidders());
        }
    }

    public static boolean isSdkInitialized() {
        return sSdkInitialized;
    }

    public static boolean canCollectPersonalInformation() {
        return sPersonalInfoManager != null && sPersonalInfoManager.canCollectPersonalInformation();
    }

    @Nullable
    static String getAdvancedBiddingTokensJson(@NonNull Context context) {
        Preconditions.checkNotNull(context);
        if (!isAdvancedBiddingEnabled() || sAdvancedBiddingTokens == null) {
            return null;
        }
        return sAdvancedBiddingTokens.getTokensAsJsonString(context);
    }

    @Nullable
    public static PersonalInfoManager getPersonalInformationManager() {
        return sPersonalInfoManager;
    }

    @VisibleForTesting
    static boolean isBrowserAgentOverriddenByClient() {
        return sIsBrowserAgentOverriddenByClient;
    }

    @Deprecated
    @VisibleForTesting
    public static void resetBrowserAgent() {
        sBrowserAgent = BrowserAgent.IN_APP;
        sIsBrowserAgentOverriddenByClient = false;
    }

    public static void onCreate(@NonNull Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onCreate(activity);
        updateActivity(activity);
    }

    public static void onStart(@NonNull Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onStart(activity);
        updateActivity(activity);
    }

    public static void onPause(@NonNull Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onPause(activity);
    }

    public static void onResume(@NonNull Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onResume(activity);
        updateActivity(activity);
    }

    public static void onRestart(@NonNull Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onRestart(activity);
        updateActivity(activity);
    }

    public static void onStop(@NonNull Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onStop(activity);
    }

    public static void onDestroy(@NonNull Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onDestroy(activity);
    }

    public static void onBackPressed(@NonNull Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onBackPressed(activity);
    }

    public static void disableViewability(@NonNull ExternalViewabilitySessionManager.ViewabilityVendor viewabilityVendor) {
        Preconditions.checkNotNull(viewabilityVendor);
        viewabilityVendor.disable();
    }

    private static void initializeRewardedVideo(@NonNull Activity activity, @NonNull SdkConfiguration sdkConfiguration) {
        Preconditions.checkNotNull(activity);
        Preconditions.checkNotNull(sdkConfiguration);
        try {
            new Reflection.MethodBuilder(null, "initializeRewardedVideo").setStatic(Class.forName(MOPUB_REWARDED_VIDEOS)).setAccessible().addParam(Activity.class, activity).addParam(SdkConfiguration.class, sdkConfiguration).execute();
        } catch (ClassNotFoundException unused) {
            MoPubLog.w("initializeRewardedVideo was called without the rewarded video module");
        } catch (NoSuchMethodException unused2) {
            MoPubLog.w("initializeRewardedVideo was called without the rewarded video module");
        } catch (Exception e) {
            MoPubLog.e("Error while initializing rewarded video", e);
        }
    }

    @VisibleForTesting
    static void updateActivity(@NonNull Activity activity) {
        if (!sSearchedForUpdateActivityMethod) {
            sSearchedForUpdateActivityMethod = true;
            try {
                sUpdateActivityMethod = Reflection.getDeclaredMethodWithTraversal(Class.forName(MOPUB_REWARDED_VIDEO_MANAGER), "updateActivity", Activity.class);
            } catch (ClassNotFoundException | NoSuchMethodException unused) {
            }
        }
        if (sUpdateActivityMethod != null) {
            try {
                sUpdateActivityMethod.invoke(null, activity);
            } catch (IllegalAccessException e) {
                MoPubLog.e("Error while attempting to access the update activity method - this should not have happened", e);
            } catch (InvocationTargetException e2) {
                MoPubLog.e("Error while attempting to access the update activity method - this should not have happened", e2);
            }
        }
    }

    @Deprecated
    @VisibleForTesting
    static void clearAdvancedBidders() {
        sAdvancedBiddingTokens = null;
        sPersonalInfoManager = null;
        sSdkInitialized = false;
    }

    @Deprecated
    @VisibleForTesting
    static void setPersonalInfoManager(@Nullable PersonalInfoManager personalInfoManager) {
        sPersonalInfoManager = personalInfoManager;
    }
}
