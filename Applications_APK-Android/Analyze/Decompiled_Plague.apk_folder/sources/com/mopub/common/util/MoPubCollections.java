package com.mopub.common.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.mopub.common.Preconditions;
import java.util.Collection;
import java.util.Collections;

public class MoPubCollections {
    public static <T> void addAllNonNull(@NonNull Collection collection, @Nullable Object... objArr) {
        Collections.addAll(collection, objArr);
        collection.removeAll(Collections.singleton(null));
    }

    public static <T> void addAllNonNull(@NonNull Collection collection, @NonNull Collection collection2) {
        Preconditions.checkNotNull(collection);
        Preconditions.checkNotNull(collection2);
        collection.addAll(collection2);
        collection.removeAll(Collections.singleton(null));
    }
}
