package com.mopub.mraid;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.widget.FrameLayout;
import com.mopub.common.AdReport;
import com.mopub.common.CloseableLayout;
import com.mopub.common.ExternalViewabilitySessionManager;
import com.mopub.common.Preconditions;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.common.util.Views;
import com.mopub.mobileads.MraidVideoPlayerActivity;
import com.mopub.mobileads.WebViewCacheService;
import com.mopub.mobileads.util.WebViews;
import com.mopub.mraid.MraidBridge;
import java.lang.ref.WeakReference;
import java.net.URI;

public class MraidController {
    private final AdReport mAdReport;
    private boolean mAllowOrientationChange;
    @NonNull
    private final CloseableLayout mCloseableAdContainer;
    /* access modifiers changed from: private */
    @NonNull
    public final Context mContext;
    @Nullable
    private MraidWebViewDebugListener mDebugListener;
    /* access modifiers changed from: private */
    @NonNull
    public final FrameLayout mDefaultAdContainer;
    private MraidOrientation mForceOrientation;
    private boolean mIsPaused;
    /* access modifiers changed from: private */
    @NonNull
    public final MraidBridge mMraidBridge;
    private final MraidBridge.MraidBridgeListener mMraidBridgeListener;
    /* access modifiers changed from: private */
    @Nullable
    public MraidListener mMraidListener;
    /* access modifiers changed from: private */
    public final MraidNativeCommandHandler mMraidNativeCommandHandler;
    @Nullable
    private MraidBridge.MraidWebView mMraidWebView;
    @Nullable
    private UseCustomCloseListener mOnCloseButtonListener;
    @NonNull
    private OrientationBroadcastReceiver mOrientationBroadcastReceiver;
    @Nullable
    private Integer mOriginalActivityOrientation;
    /* access modifiers changed from: private */
    @NonNull
    public final PlacementType mPlacementType;
    @Nullable
    private ViewGroup mRootView;
    /* access modifiers changed from: private */
    @NonNull
    public final MraidScreenMetrics mScreenMetrics;
    @NonNull
    private final ScreenMetricsWaiter mScreenMetricsWaiter;
    /* access modifiers changed from: private */
    @NonNull
    public final MraidBridge mTwoPartBridge;
    private final MraidBridge.MraidBridgeListener mTwoPartBridgeListener;
    @Nullable
    private MraidBridge.MraidWebView mTwoPartWebView;
    /* access modifiers changed from: private */
    @NonNull
    public ViewState mViewState;
    @NonNull
    private final WeakReference<Activity> mWeakActivity;

    public interface MraidListener {
        void onClose();

        void onExpand();

        void onFailedToLoad();

        void onLoaded(View view);

        void onOpen();
    }

    public interface MraidWebViewCacheListener {
        void onReady(MraidBridge.MraidWebView mraidWebView, ExternalViewabilitySessionManager externalViewabilitySessionManager);
    }

    public interface UseCustomCloseListener {
        void useCustomCloseChanged(boolean z);
    }

    public MraidController(@NonNull Context context, @Nullable AdReport adReport, @NonNull PlacementType placementType) {
        this(context, adReport, placementType, new MraidBridge(adReport, placementType), new MraidBridge(adReport, PlacementType.INTERSTITIAL), new ScreenMetricsWaiter());
    }

    @VisibleForTesting
    MraidController(@NonNull Context context, @Nullable AdReport adReport, @NonNull PlacementType placementType, @NonNull MraidBridge mraidBridge, @NonNull MraidBridge mraidBridge2, @NonNull ScreenMetricsWaiter screenMetricsWaiter) {
        this.mViewState = ViewState.LOADING;
        this.mOrientationBroadcastReceiver = new OrientationBroadcastReceiver();
        this.mAllowOrientationChange = true;
        this.mForceOrientation = MraidOrientation.NONE;
        this.mMraidBridgeListener = new MraidBridge.MraidBridgeListener() {
            public void onPageLoaded() {
                MraidController.this.handlePageLoad();
            }

            public void onPageFailedToLoad() {
                if (MraidController.this.mMraidListener != null) {
                    MraidController.this.mMraidListener.onFailedToLoad();
                }
            }

            public void onVisibilityChanged(boolean z) {
                if (!MraidController.this.mTwoPartBridge.isAttached()) {
                    MraidController.this.mMraidBridge.notifyViewability(z);
                }
            }

            public boolean onJsAlert(@NonNull String str, @NonNull JsResult jsResult) {
                return MraidController.this.handleJsAlert(str, jsResult);
            }

            public boolean onConsoleMessage(@NonNull ConsoleMessage consoleMessage) {
                return MraidController.this.handleConsoleMessage(consoleMessage);
            }

            public void onClose() {
                MraidController.this.handleClose();
            }

            public void onResize(int i, int i2, int i3, int i4, @NonNull CloseableLayout.ClosePosition closePosition, boolean z) throws MraidCommandException {
                MraidController.this.handleResize(i, i2, i3, i4, closePosition, z);
            }

            public void onExpand(@Nullable URI uri, boolean z) throws MraidCommandException {
                MraidController.this.handleExpand(uri, z);
            }

            public void onUseCustomClose(boolean z) {
                MraidController.this.handleCustomClose(z);
            }

            public void onSetOrientationProperties(boolean z, MraidOrientation mraidOrientation) throws MraidCommandException {
                MraidController.this.handleSetOrientationProperties(z, mraidOrientation);
            }

            public void onOpen(@NonNull URI uri) {
                MraidController.this.handleOpen(uri.toString());
            }

            public void onPlayVideo(@NonNull URI uri) {
                MraidController.this.handleShowVideo(uri.toString());
            }
        };
        this.mTwoPartBridgeListener = new MraidBridge.MraidBridgeListener() {
            public void onExpand(@Nullable URI uri, boolean z) {
            }

            public void onPageFailedToLoad() {
            }

            public void onPageLoaded() {
                MraidController.this.handleTwoPartPageLoad();
            }

            public void onVisibilityChanged(boolean z) {
                MraidController.this.mMraidBridge.notifyViewability(z);
                MraidController.this.mTwoPartBridge.notifyViewability(z);
            }

            public boolean onJsAlert(@NonNull String str, @NonNull JsResult jsResult) {
                return MraidController.this.handleJsAlert(str, jsResult);
            }

            public boolean onConsoleMessage(@NonNull ConsoleMessage consoleMessage) {
                return MraidController.this.handleConsoleMessage(consoleMessage);
            }

            public void onResize(int i, int i2, int i3, int i4, @NonNull CloseableLayout.ClosePosition closePosition, boolean z) throws MraidCommandException {
                throw new MraidCommandException("Not allowed to resize from an expanded state");
            }

            public void onClose() {
                MraidController.this.handleClose();
            }

            public void onUseCustomClose(boolean z) {
                MraidController.this.handleCustomClose(z);
            }

            public void onSetOrientationProperties(boolean z, MraidOrientation mraidOrientation) throws MraidCommandException {
                MraidController.this.handleSetOrientationProperties(z, mraidOrientation);
            }

            public void onOpen(URI uri) {
                MraidController.this.handleOpen(uri.toString());
            }

            public void onPlayVideo(@NonNull URI uri) {
                MraidController.this.handleShowVideo(uri.toString());
            }
        };
        this.mContext = context.getApplicationContext();
        Preconditions.checkNotNull(this.mContext);
        this.mAdReport = adReport;
        if (context instanceof Activity) {
            this.mWeakActivity = new WeakReference<>((Activity) context);
        } else {
            this.mWeakActivity = new WeakReference<>(null);
        }
        this.mPlacementType = placementType;
        this.mMraidBridge = mraidBridge;
        this.mTwoPartBridge = mraidBridge2;
        this.mScreenMetricsWaiter = screenMetricsWaiter;
        this.mViewState = ViewState.LOADING;
        this.mScreenMetrics = new MraidScreenMetrics(this.mContext, this.mContext.getResources().getDisplayMetrics().density);
        this.mDefaultAdContainer = new FrameLayout(this.mContext);
        this.mCloseableAdContainer = new CloseableLayout(this.mContext);
        this.mCloseableAdContainer.setOnCloseListener(new CloseableLayout.OnCloseListener() {
            public void onClose() {
                MraidController.this.handleClose();
            }
        });
        View view = new View(this.mContext);
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.mCloseableAdContainer.addView(view, new FrameLayout.LayoutParams(-1, -1));
        this.mOrientationBroadcastReceiver.register(this.mContext);
        this.mMraidBridge.setMraidBridgeListener(this.mMraidBridgeListener);
        this.mTwoPartBridge.setMraidBridgeListener(this.mTwoPartBridgeListener);
        this.mMraidNativeCommandHandler = new MraidNativeCommandHandler();
    }

    public void setMraidListener(@Nullable MraidListener mraidListener) {
        this.mMraidListener = mraidListener;
    }

    public void setUseCustomCloseListener(@Nullable UseCustomCloseListener useCustomCloseListener) {
        this.mOnCloseButtonListener = useCustomCloseListener;
    }

    public void setDebugListener(@Nullable MraidWebViewDebugListener mraidWebViewDebugListener) {
        this.mDebugListener = mraidWebViewDebugListener;
    }

    public void fillContent(@Nullable Long l, @NonNull String str, @Nullable MraidWebViewCacheListener mraidWebViewCacheListener) {
        Preconditions.checkNotNull(str, "htmlData cannot be null");
        boolean hydrateMraidWebView = hydrateMraidWebView(l, mraidWebViewCacheListener);
        Preconditions.NoThrow.checkNotNull(this.mMraidWebView, "mMraidWebView cannot be null");
        this.mMraidBridge.attachView(this.mMraidWebView);
        this.mDefaultAdContainer.addView(this.mMraidWebView, new FrameLayout.LayoutParams(-1, -1));
        if (hydrateMraidWebView) {
            handlePageLoad();
        } else {
            this.mMraidBridge.setContentHtml(str);
        }
    }

    private boolean hydrateMraidWebView(@Nullable Long l, @Nullable MraidWebViewCacheListener mraidWebViewCacheListener) {
        WebViewCacheService.Config popWebViewConfig;
        if (l == null || (popWebViewConfig = WebViewCacheService.popWebViewConfig(l)) == null || !(popWebViewConfig.getWebView() instanceof MraidBridge.MraidWebView)) {
            MoPubLog.d("WebView cache miss. Creating a new MraidWebView.");
            this.mMraidWebView = new MraidBridge.MraidWebView(this.mContext);
            if (mraidWebViewCacheListener == null) {
                return false;
            }
            mraidWebViewCacheListener.onReady(this.mMraidWebView, null);
            return false;
        }
        this.mMraidWebView = (MraidBridge.MraidWebView) popWebViewConfig.getWebView();
        this.mMraidWebView.enablePlugins(true);
        if (mraidWebViewCacheListener != null) {
            mraidWebViewCacheListener.onReady(this.mMraidWebView, popWebViewConfig.getViewabilityManager());
        }
        return true;
    }

    /* access modifiers changed from: private */
    public int getDisplayRotation() {
        return ((WindowManager) this.mContext.getSystemService("window")).getDefaultDisplay().getRotation();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean handleConsoleMessage(@NonNull ConsoleMessage consoleMessage) {
        if (this.mDebugListener != null) {
            return this.mDebugListener.onConsoleMessage(consoleMessage);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean handleJsAlert(@NonNull String str, @NonNull JsResult jsResult) {
        if (this.mDebugListener != null) {
            return this.mDebugListener.onJsAlert(str, jsResult);
        }
        jsResult.confirm();
        return true;
    }

    @VisibleForTesting
    static class ScreenMetricsWaiter {
        @NonNull
        private final Handler mHandler = new Handler();
        @Nullable
        private WaitRequest mLastWaitRequest;

        ScreenMetricsWaiter() {
        }

        static class WaitRequest {
            @NonNull
            private final Handler mHandler;
            @Nullable
            private Runnable mSuccessRunnable;
            /* access modifiers changed from: private */
            @NonNull
            public final View[] mViews;
            int mWaitCount;
            private final Runnable mWaitingRunnable;

            private WaitRequest(@NonNull Handler handler, @NonNull View[] viewArr) {
                this.mWaitingRunnable = new Runnable() {
                    public void run() {
                        for (final View view : WaitRequest.this.mViews) {
                            if (view.getHeight() > 0 || view.getWidth() > 0) {
                                WaitRequest.this.countDown();
                            } else {
                                view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                                    public boolean onPreDraw() {
                                        view.getViewTreeObserver().removeOnPreDrawListener(this);
                                        WaitRequest.this.countDown();
                                        return true;
                                    }
                                });
                            }
                        }
                    }
                };
                this.mHandler = handler;
                this.mViews = viewArr;
            }

            /* access modifiers changed from: private */
            public void countDown() {
                this.mWaitCount--;
                if (this.mWaitCount == 0 && this.mSuccessRunnable != null) {
                    this.mSuccessRunnable.run();
                    this.mSuccessRunnable = null;
                }
            }

            /* access modifiers changed from: package-private */
            public void start(@NonNull Runnable runnable) {
                this.mSuccessRunnable = runnable;
                this.mWaitCount = this.mViews.length;
                this.mHandler.post(this.mWaitingRunnable);
            }

            /* access modifiers changed from: package-private */
            public void cancel() {
                this.mHandler.removeCallbacks(this.mWaitingRunnable);
                this.mSuccessRunnable = null;
            }
        }

        /* access modifiers changed from: package-private */
        public WaitRequest waitFor(@NonNull View... viewArr) {
            this.mLastWaitRequest = new WaitRequest(this.mHandler, viewArr);
            return this.mLastWaitRequest;
        }

        /* access modifiers changed from: package-private */
        public void cancelLastRequest() {
            if (this.mLastWaitRequest != null) {
                this.mLastWaitRequest.cancel();
                this.mLastWaitRequest = null;
            }
        }
    }

    @Nullable
    public MraidBridge.MraidWebView getCurrentWebView() {
        return this.mTwoPartBridge.isAttached() ? this.mTwoPartWebView : this.mMraidWebView;
    }

    /* access modifiers changed from: private */
    public boolean isInlineVideoAvailable() {
        Activity activity = this.mWeakActivity.get();
        if (activity == null || getCurrentWebView() == null) {
            return false;
        }
        return this.mMraidNativeCommandHandler.isInlineVideoAvailable(activity, getCurrentWebView());
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void handlePageLoad() {
        setViewState(ViewState.DEFAULT, new Runnable() {
            public void run() {
                MraidController.this.mMraidBridge.notifySupports(MraidController.this.mMraidNativeCommandHandler.isSmsAvailable(MraidController.this.mContext), MraidController.this.mMraidNativeCommandHandler.isTelAvailable(MraidController.this.mContext), MraidNativeCommandHandler.isCalendarAvailable(MraidController.this.mContext), MraidNativeCommandHandler.isStorePictureSupported(MraidController.this.mContext), MraidController.this.isInlineVideoAvailable());
                MraidController.this.mMraidBridge.notifyPlacementType(MraidController.this.mPlacementType);
                MraidController.this.mMraidBridge.notifyViewability(MraidController.this.mMraidBridge.isVisible());
                MraidController.this.mMraidBridge.notifyReady();
            }
        });
        if (this.mMraidListener != null) {
            this.mMraidListener.onLoaded(this.mDefaultAdContainer);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void handleTwoPartPageLoad() {
        updateScreenMetricsAsync(new Runnable() {
            public void run() {
                MraidBridge access$100 = MraidController.this.mTwoPartBridge;
                boolean isSmsAvailable = MraidController.this.mMraidNativeCommandHandler.isSmsAvailable(MraidController.this.mContext);
                boolean isTelAvailable = MraidController.this.mMraidNativeCommandHandler.isTelAvailable(MraidController.this.mContext);
                MraidNativeCommandHandler unused = MraidController.this.mMraidNativeCommandHandler;
                boolean isCalendarAvailable = MraidNativeCommandHandler.isCalendarAvailable(MraidController.this.mContext);
                MraidNativeCommandHandler unused2 = MraidController.this.mMraidNativeCommandHandler;
                access$100.notifySupports(isSmsAvailable, isTelAvailable, isCalendarAvailable, MraidNativeCommandHandler.isStorePictureSupported(MraidController.this.mContext), MraidController.this.isInlineVideoAvailable());
                MraidController.this.mTwoPartBridge.notifyViewState(MraidController.this.mViewState);
                MraidController.this.mTwoPartBridge.notifyPlacementType(MraidController.this.mPlacementType);
                MraidController.this.mTwoPartBridge.notifyViewability(MraidController.this.mTwoPartBridge.isVisible());
                MraidController.this.mTwoPartBridge.notifyReady();
            }
        });
    }

    private void updateScreenMetricsAsync(@Nullable final Runnable runnable) {
        this.mScreenMetricsWaiter.cancelLastRequest();
        final MraidBridge.MraidWebView currentWebView = getCurrentWebView();
        if (currentWebView != null) {
            this.mScreenMetricsWaiter.waitFor(this.mDefaultAdContainer, currentWebView).start(new Runnable() {
                public void run() {
                    DisplayMetrics displayMetrics = MraidController.this.mContext.getResources().getDisplayMetrics();
                    MraidController.this.mScreenMetrics.setScreenSize(displayMetrics.widthPixels, displayMetrics.heightPixels);
                    int[] iArr = new int[2];
                    ViewGroup access$1200 = MraidController.this.getRootView();
                    access$1200.getLocationOnScreen(iArr);
                    MraidController.this.mScreenMetrics.setRootViewPosition(iArr[0], iArr[1], access$1200.getWidth(), access$1200.getHeight());
                    MraidController.this.mDefaultAdContainer.getLocationOnScreen(iArr);
                    MraidController.this.mScreenMetrics.setDefaultAdPosition(iArr[0], iArr[1], MraidController.this.mDefaultAdContainer.getWidth(), MraidController.this.mDefaultAdContainer.getHeight());
                    currentWebView.getLocationOnScreen(iArr);
                    MraidController.this.mScreenMetrics.setCurrentAdPosition(iArr[0], iArr[1], currentWebView.getWidth(), currentWebView.getHeight());
                    MraidController.this.mMraidBridge.notifyScreenMetrics(MraidController.this.mScreenMetrics);
                    if (MraidController.this.mTwoPartBridge.isAttached()) {
                        MraidController.this.mTwoPartBridge.notifyScreenMetrics(MraidController.this.mScreenMetrics);
                    }
                    if (runnable != null) {
                        runnable.run();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public void handleOrientationChange(int i) {
        updateScreenMetricsAsync(null);
    }

    public void pause(boolean z) {
        this.mIsPaused = true;
        if (this.mMraidWebView != null) {
            WebViews.onPause(this.mMraidWebView, z);
        }
        if (this.mTwoPartWebView != null) {
            WebViews.onPause(this.mTwoPartWebView, z);
        }
    }

    public void resume() {
        this.mIsPaused = false;
        if (this.mMraidWebView != null) {
            this.mMraidWebView.onResume();
        }
        if (this.mTwoPartWebView != null) {
            this.mTwoPartWebView.onResume();
        }
    }

    public void destroy() {
        this.mScreenMetricsWaiter.cancelLastRequest();
        try {
            this.mOrientationBroadcastReceiver.unregister();
        } catch (IllegalArgumentException e) {
            if (!e.getMessage().contains("Receiver not registered")) {
                throw e;
            }
        }
        if (!this.mIsPaused) {
            pause(true);
        }
        Views.removeFromParent(this.mCloseableAdContainer);
        this.mMraidBridge.detach();
        if (this.mMraidWebView != null) {
            this.mMraidWebView.destroy();
            this.mMraidWebView = null;
        }
        this.mTwoPartBridge.detach();
        if (this.mTwoPartWebView != null) {
            this.mTwoPartWebView.destroy();
            this.mTwoPartWebView = null;
        }
    }

    private void setViewState(@NonNull ViewState viewState) {
        setViewState(viewState, null);
    }

    private void setViewState(@NonNull ViewState viewState, @Nullable Runnable runnable) {
        MoPubLog.d("MRAID state set to " + viewState);
        ViewState viewState2 = this.mViewState;
        this.mViewState = viewState;
        this.mMraidBridge.notifyViewState(viewState);
        if (this.mTwoPartBridge.isLoaded()) {
            this.mTwoPartBridge.notifyViewState(viewState);
        }
        if (this.mMraidListener != null) {
            if (viewState == ViewState.EXPANDED) {
                this.mMraidListener.onExpand();
            } else if (viewState2 == ViewState.EXPANDED && viewState == ViewState.DEFAULT) {
                this.mMraidListener.onClose();
            } else if (viewState == ViewState.HIDDEN) {
                this.mMraidListener.onClose();
            }
        }
        updateScreenMetricsAsync(runnable);
    }

    /* access modifiers changed from: package-private */
    public int clampInt(int i, int i2, int i3) {
        return Math.max(i, Math.min(i2, i3));
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void handleResize(int i, int i2, int i3, int i4, @NonNull CloseableLayout.ClosePosition closePosition, boolean z) throws MraidCommandException {
        if (this.mMraidWebView == null) {
            throw new MraidCommandException("Unable to resize after the WebView is destroyed");
        } else if (this.mViewState != ViewState.LOADING && this.mViewState != ViewState.HIDDEN) {
            if (this.mViewState == ViewState.EXPANDED) {
                throw new MraidCommandException("Not allowed to resize from an already expanded ad");
            } else if (this.mPlacementType == PlacementType.INTERSTITIAL) {
                throw new MraidCommandException("Not allowed to resize from an interstitial ad");
            } else {
                int dipsToIntPixels = Dips.dipsToIntPixels((float) i, this.mContext);
                int dipsToIntPixels2 = Dips.dipsToIntPixels((float) i2, this.mContext);
                int dipsToIntPixels3 = Dips.dipsToIntPixels((float) i3, this.mContext);
                int dipsToIntPixels4 = Dips.dipsToIntPixels((float) i4, this.mContext);
                int i5 = this.mScreenMetrics.getDefaultAdRect().left + dipsToIntPixels3;
                int i6 = this.mScreenMetrics.getDefaultAdRect().top + dipsToIntPixels4;
                Rect rect = new Rect(i5, i6, dipsToIntPixels + i5, i6 + dipsToIntPixels2);
                if (!z) {
                    Rect rootViewRect = this.mScreenMetrics.getRootViewRect();
                    if (rect.width() > rootViewRect.width() || rect.height() > rootViewRect.height()) {
                        throw new MraidCommandException("resizeProperties specified a size (" + i + ", " + i2 + ") and offset (" + i3 + ", " + i4 + ") that doesn't allow the ad to appear within the max allowed size (" + this.mScreenMetrics.getRootViewRectDips().width() + ", " + this.mScreenMetrics.getRootViewRectDips().height() + ")");
                    }
                    rect.offsetTo(clampInt(rootViewRect.left, rect.left, rootViewRect.right - rect.width()), clampInt(rootViewRect.top, rect.top, rootViewRect.bottom - rect.height()));
                }
                Rect rect2 = new Rect();
                this.mCloseableAdContainer.applyCloseRegionBounds(closePosition, rect, rect2);
                if (!this.mScreenMetrics.getRootViewRect().contains(rect2)) {
                    throw new MraidCommandException("resizeProperties specified a size (" + i + ", " + i2 + ") and offset (" + i3 + ", " + i4 + ") that doesn't allow the close region to appear within the max allowed size (" + this.mScreenMetrics.getRootViewRectDips().width() + ", " + this.mScreenMetrics.getRootViewRectDips().height() + ")");
                } else if (!rect.contains(rect2)) {
                    throw new MraidCommandException("resizeProperties specified a size (" + i + ", " + dipsToIntPixels2 + ") and offset (" + i3 + ", " + i4 + ") that don't allow the close region to appear within the resized ad.");
                } else {
                    this.mCloseableAdContainer.setCloseVisible(false);
                    this.mCloseableAdContainer.setClosePosition(closePosition);
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(rect.width(), rect.height());
                    layoutParams.leftMargin = rect.left - this.mScreenMetrics.getRootViewRect().left;
                    layoutParams.topMargin = rect.top - this.mScreenMetrics.getRootViewRect().top;
                    if (this.mViewState == ViewState.DEFAULT) {
                        this.mDefaultAdContainer.removeView(this.mMraidWebView);
                        this.mDefaultAdContainer.setVisibility(4);
                        this.mCloseableAdContainer.addView(this.mMraidWebView, new FrameLayout.LayoutParams(-1, -1));
                        getAndMemoizeRootView().addView(this.mCloseableAdContainer, layoutParams);
                    } else if (this.mViewState == ViewState.RESIZED) {
                        this.mCloseableAdContainer.setLayoutParams(layoutParams);
                    }
                    this.mCloseableAdContainer.setClosePosition(closePosition);
                    setViewState(ViewState.RESIZED);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void handleExpand(@Nullable URI uri, boolean z) throws MraidCommandException {
        if (this.mMraidWebView == null) {
            throw new MraidCommandException("Unable to expand after the WebView is destroyed");
        } else if (this.mPlacementType != PlacementType.INTERSTITIAL) {
            if (this.mViewState == ViewState.DEFAULT || this.mViewState == ViewState.RESIZED) {
                applyOrientation();
                boolean z2 = uri != null;
                if (z2) {
                    this.mTwoPartWebView = new MraidBridge.MraidWebView(this.mContext);
                    this.mTwoPartBridge.attachView(this.mTwoPartWebView);
                    this.mTwoPartBridge.setContentUrl(uri.toString());
                }
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
                if (this.mViewState == ViewState.DEFAULT) {
                    if (z2) {
                        this.mCloseableAdContainer.addView(this.mTwoPartWebView, layoutParams);
                    } else {
                        this.mDefaultAdContainer.removeView(this.mMraidWebView);
                        this.mDefaultAdContainer.setVisibility(4);
                        this.mCloseableAdContainer.addView(this.mMraidWebView, layoutParams);
                    }
                    getAndMemoizeRootView().addView(this.mCloseableAdContainer, new FrameLayout.LayoutParams(-1, -1));
                } else if (this.mViewState == ViewState.RESIZED && z2) {
                    this.mCloseableAdContainer.removeView(this.mMraidWebView);
                    this.mDefaultAdContainer.addView(this.mMraidWebView, layoutParams);
                    this.mDefaultAdContainer.setVisibility(4);
                    this.mCloseableAdContainer.addView(this.mTwoPartWebView, layoutParams);
                }
                this.mCloseableAdContainer.setLayoutParams(layoutParams);
                handleCustomClose(z);
                setViewState(ViewState.EXPANDED);
            }
        }
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public void handleClose() {
        if (this.mMraidWebView != null && this.mViewState != ViewState.LOADING && this.mViewState != ViewState.HIDDEN) {
            if (this.mViewState == ViewState.EXPANDED || this.mPlacementType == PlacementType.INTERSTITIAL) {
                unApplyOrientation();
            }
            if (this.mViewState == ViewState.RESIZED || this.mViewState == ViewState.EXPANDED) {
                if (!this.mTwoPartBridge.isAttached() || this.mTwoPartWebView == null) {
                    this.mCloseableAdContainer.removeView(this.mMraidWebView);
                    this.mDefaultAdContainer.addView(this.mMraidWebView, new FrameLayout.LayoutParams(-1, -1));
                    this.mDefaultAdContainer.setVisibility(0);
                } else {
                    this.mCloseableAdContainer.removeView(this.mTwoPartWebView);
                    this.mTwoPartBridge.detach();
                }
                Views.removeFromParent(this.mCloseableAdContainer);
                setViewState(ViewState.DEFAULT);
            } else if (this.mViewState == ViewState.DEFAULT) {
                this.mDefaultAdContainer.setVisibility(4);
                setViewState(ViewState.HIDDEN);
            }
        }
    }

    /* access modifiers changed from: private */
    @NonNull
    public ViewGroup getRootView() {
        if (this.mRootView != null) {
            return this.mRootView;
        }
        View topmostView = Views.getTopmostView(this.mWeakActivity.get(), this.mDefaultAdContainer);
        return topmostView instanceof ViewGroup ? (ViewGroup) topmostView : this.mDefaultAdContainer;
    }

    @NonNull
    private ViewGroup getAndMemoizeRootView() {
        if (this.mRootView == null) {
            this.mRootView = getRootView();
        }
        return this.mRootView;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void handleShowVideo(@NonNull String str) {
        MraidVideoPlayerActivity.startMraid(this.mContext, str);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void lockOrientation(int i) throws MraidCommandException {
        Activity activity = this.mWeakActivity.get();
        if (activity == null || !shouldAllowForceOrientation(this.mForceOrientation)) {
            throw new MraidCommandException("Attempted to lock orientation to unsupported value: " + this.mForceOrientation.name());
        }
        if (this.mOriginalActivityOrientation == null) {
            this.mOriginalActivityOrientation = Integer.valueOf(activity.getRequestedOrientation());
        }
        activity.setRequestedOrientation(i);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void applyOrientation() throws MraidCommandException {
        if (this.mForceOrientation != MraidOrientation.NONE) {
            lockOrientation(this.mForceOrientation.getActivityInfoOrientation());
        } else if (this.mAllowOrientationChange) {
            unApplyOrientation();
        } else {
            Activity activity = this.mWeakActivity.get();
            if (activity == null) {
                throw new MraidCommandException("Unable to set MRAID expand orientation to 'none'; expected passed in Activity Context.");
            }
            lockOrientation(DeviceUtils.getScreenOrientation(activity));
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void unApplyOrientation() {
        Activity activity = this.mWeakActivity.get();
        if (!(activity == null || this.mOriginalActivityOrientation == null)) {
            activity.setRequestedOrientation(this.mOriginalActivityOrientation.intValue());
        }
        this.mOriginalActivityOrientation = null;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public boolean shouldAllowForceOrientation(MraidOrientation mraidOrientation) {
        if (mraidOrientation == MraidOrientation.NONE) {
            return true;
        }
        Activity activity = this.mWeakActivity.get();
        if (activity == null) {
            return false;
        }
        try {
            ActivityInfo activityInfo = activity.getPackageManager().getActivityInfo(new ComponentName(activity, activity.getClass()), 0);
            int i = activityInfo.screenOrientation;
            if (i != -1) {
                if (i == mraidOrientation.getActivityInfoOrientation()) {
                    return true;
                }
                return false;
            } else if (!Utils.bitMaskContainsFlag(activityInfo.configChanges, 128) || !Utils.bitMaskContainsFlag(activityInfo.configChanges, 1024)) {
                return false;
            } else {
                return true;
            }
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public void handleCustomClose(boolean z) {
        if (z != (!this.mCloseableAdContainer.isCloseVisible())) {
            this.mCloseableAdContainer.setCloseVisible(!z);
            if (this.mOnCloseButtonListener != null) {
                this.mOnCloseButtonListener.useCustomCloseChanged(z);
            }
        }
    }

    @NonNull
    public FrameLayout getAdContainer() {
        return this.mDefaultAdContainer;
    }

    public void loadJavascript(@NonNull String str) {
        this.mMraidBridge.injectJavaScript(str);
    }

    @VisibleForTesting
    class OrientationBroadcastReceiver extends BroadcastReceiver {
        @Nullable
        private Context mContext;
        private int mLastRotation = -1;

        OrientationBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            int access$1400;
            if (this.mContext != null && "android.intent.action.CONFIGURATION_CHANGED".equals(intent.getAction()) && (access$1400 = MraidController.this.getDisplayRotation()) != this.mLastRotation) {
                this.mLastRotation = access$1400;
                MraidController.this.handleOrientationChange(this.mLastRotation);
            }
        }

        public void register(@NonNull Context context) {
            Preconditions.checkNotNull(context);
            this.mContext = context.getApplicationContext();
            if (this.mContext != null) {
                this.mContext.registerReceiver(this, new IntentFilter("android.intent.action.CONFIGURATION_CHANGED"));
            }
        }

        public void unregister() {
            if (this.mContext != null) {
                this.mContext.unregisterReceiver(this);
                this.mContext = null;
            }
        }
    }

    @NonNull
    public Context getContext() {
        return this.mContext;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public WeakReference<Activity> getWeakActivity() {
        return this.mWeakActivity;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void handleSetOrientationProperties(boolean z, MraidOrientation mraidOrientation) throws MraidCommandException {
        if (!shouldAllowForceOrientation(mraidOrientation)) {
            throw new MraidCommandException("Unable to force orientation to " + mraidOrientation);
        }
        this.mAllowOrientationChange = z;
        this.mForceOrientation = mraidOrientation;
        if (this.mViewState == ViewState.EXPANDED || this.mPlacementType == PlacementType.INTERSTITIAL) {
            applyOrientation();
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void handleOpen(@NonNull String str) {
        if (this.mMraidListener != null) {
            this.mMraidListener.onOpen();
        }
        UrlHandler.Builder builder = new UrlHandler.Builder();
        if (this.mAdReport != null) {
            builder.withDspCreativeId(this.mAdReport.getDspCreativeId());
        }
        builder.withSupportedUrlActions(UrlAction.IGNORE_ABOUT_SCHEME, UrlAction.OPEN_NATIVE_BROWSER, UrlAction.OPEN_IN_APP_BROWSER, UrlAction.HANDLE_SHARE_TWEET, UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK, UrlAction.FOLLOW_DEEP_LINK).build().handleUrl(this.mContext, str);
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @NonNull
    @VisibleForTesting
    public ViewState getViewState() {
        return this.mViewState;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public void setViewStateForTesting(@NonNull ViewState viewState) {
        this.mViewState = viewState;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @NonNull
    @VisibleForTesting
    public CloseableLayout getExpandedAdContainer() {
        return this.mCloseableAdContainer;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public void setRootView(FrameLayout frameLayout) {
        this.mRootView = frameLayout;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public void setRootViewSize(int i, int i2) {
        this.mScreenMetrics.setRootViewPosition(0, 0, i, i2);
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public Integer getOriginalActivityOrientation() {
        return this.mOriginalActivityOrientation;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public boolean getAllowOrientationChange() {
        return this.mAllowOrientationChange;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public MraidOrientation getForceOrientation() {
        return this.mForceOrientation;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public void setOrientationBroadcastReceiver(OrientationBroadcastReceiver orientationBroadcastReceiver) {
        this.mOrientationBroadcastReceiver = orientationBroadcastReceiver;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public MraidBridge.MraidWebView getMraidWebView() {
        return this.mMraidWebView;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public MraidBridge.MraidWebView getTwoPartWebView() {
        return this.mTwoPartWebView;
    }
}
