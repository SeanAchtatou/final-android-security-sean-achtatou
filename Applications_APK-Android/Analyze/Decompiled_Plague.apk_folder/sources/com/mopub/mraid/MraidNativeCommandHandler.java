package com.mopub.mraid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.Toast;
import com.google.android.gms.drive.DriveFile;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.AsyncTasks;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Intents;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.GooglePlayServicesInterstitial;
import com.tapjoy.TJAdUnitConstants;
import java.io.File;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MraidNativeCommandHandler {
    public static final String ANDROID_CALENDAR_CONTENT_TYPE = "vnd.android.cursor.item/event";
    private static final String[] DATE_FORMATS = {"yyyy-MM-dd'T'HH:mm:ssZZZZZ", "yyyy-MM-dd'T'HH:mmZZZZZ"};
    private static final int MAX_NUMBER_DAYS_IN_MONTH = 31;
    @VisibleForTesting
    static final String MIME_TYPE_HEADER = "Content-Type";

    interface MraidCommandFailureListener {
        void onFailure(MraidCommandException mraidCommandException);
    }

    /* access modifiers changed from: package-private */
    public void createCalendarEvent(Context context, Map<String, String> map) throws MraidCommandException {
        if (isCalendarAvailable(context)) {
            try {
                Map<String, Object> translateJSParamsToAndroidCalendarEventMapping = translateJSParamsToAndroidCalendarEventMapping(map);
                Intent type = new Intent("android.intent.action.INSERT").setType(ANDROID_CALENDAR_CONTENT_TYPE);
                for (String next : translateJSParamsToAndroidCalendarEventMapping.keySet()) {
                    Object obj = translateJSParamsToAndroidCalendarEventMapping.get(next);
                    if (obj instanceof Long) {
                        type.putExtra(next, ((Long) obj).longValue());
                    } else if (obj instanceof Integer) {
                        type.putExtra(next, ((Integer) obj).intValue());
                    } else {
                        type.putExtra(next, (String) obj);
                    }
                }
                type.setFlags(DriveFile.MODE_READ_ONLY);
                context.startActivity(type);
            } catch (ActivityNotFoundException unused) {
                MoPubLog.d("no calendar app installed");
                throw new MraidCommandException("Action is unsupported on this device - no calendar app installed");
            } catch (IllegalArgumentException e) {
                MoPubLog.d("create calendar: invalid parameters " + e.getMessage());
                throw new MraidCommandException(e);
            } catch (Exception e2) {
                MoPubLog.d("could not create calendar event");
                throw new MraidCommandException(e2);
            }
        } else {
            MoPubLog.d("unsupported action createCalendarEvent for devices pre-ICS");
            throw new MraidCommandException("Action is unsupported on this device (need Android version Ice Cream Sandwich or above)");
        }
    }

    /* access modifiers changed from: package-private */
    public void storePicture(@NonNull Context context, @NonNull String str, @NonNull MraidCommandFailureListener mraidCommandFailureListener) throws MraidCommandException {
        if (!isStorePictureSupported(context)) {
            MoPubLog.d("Error downloading file - the device does not have an SD card mounted, or the Android permission is not granted.");
            throw new MraidCommandException("Error downloading file  - the device does not have an SD card mounted, or the Android permission is not granted.");
        } else if (context instanceof Activity) {
            showUserDialog(context, str, mraidCommandFailureListener);
        } else {
            Toast.makeText(context, "Downloading image to Picture gallery...", 0).show();
            downloadImage(context, str, mraidCommandFailureListener);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isTelAvailable(Context context) {
        Intent intent = new Intent("android.intent.action.DIAL");
        intent.setData(Uri.parse("tel:"));
        return Intents.deviceCanHandleIntent(context, intent);
    }

    /* access modifiers changed from: package-private */
    public boolean isSmsAvailable(Context context) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("sms:"));
        return Intents.deviceCanHandleIntent(context, intent);
    }

    public static boolean isStorePictureSupported(Context context) {
        return "mounted".equals(Environment.getExternalStorageState()) && DeviceUtils.isPermissionGranted(context, "android.permission.WRITE_EXTERNAL_STORAGE");
    }

    static boolean isCalendarAvailable(Context context) {
        return Intents.deviceCanHandleIntent(context, new Intent("android.intent.action.INSERT").setType(ANDROID_CALENDAR_CONTENT_TYPE));
    }

    /* access modifiers changed from: package-private */
    public boolean isInlineVideoAvailable(@NonNull Activity activity, @NonNull View view) {
        while (view.isHardwareAccelerated() && !Utils.bitMaskContainsFlag(view.getLayerType(), 1)) {
            if (!(view.getParent() instanceof View)) {
                Window window = activity.getWindow();
                if (window == null || !Utils.bitMaskContainsFlag(window.getAttributes().flags, 16777216)) {
                    return false;
                }
                return true;
            }
            view = (View) view.getParent();
        }
        return false;
    }

    private Map<String, Object> translateJSParamsToAndroidCalendarEventMapping(Map<String, String> map) {
        HashMap hashMap = new HashMap();
        if (!map.containsKey("description") || !map.containsKey(TJAdUnitConstants.String.VIDEO_START)) {
            throw new IllegalArgumentException("Missing start and description fields");
        }
        hashMap.put("title", map.get("description"));
        if (!map.containsKey(TJAdUnitConstants.String.VIDEO_START) || map.get(TJAdUnitConstants.String.VIDEO_START) == null) {
            throw new IllegalArgumentException("Invalid calendar event: start is null.");
        }
        Date parseDate = parseDate(map.get(TJAdUnitConstants.String.VIDEO_START));
        if (parseDate != null) {
            hashMap.put("beginTime", Long.valueOf(parseDate.getTime()));
            if (map.containsKey("end") && map.get("end") != null) {
                Date parseDate2 = parseDate(map.get("end"));
                if (parseDate2 != null) {
                    hashMap.put("endTime", Long.valueOf(parseDate2.getTime()));
                } else {
                    throw new IllegalArgumentException("Invalid calendar event: end time is malformed. Date format expecting (yyyy-MM-DDTHH:MM:SS-xx:xx) or (yyyy-MM-DDTHH:MM-xx:xx) i.e. 2013-08-14T09:00:01-08:00");
                }
            }
            if (map.containsKey(GooglePlayServicesInterstitial.LOCATION_KEY)) {
                hashMap.put("eventLocation", map.get(GooglePlayServicesInterstitial.LOCATION_KEY));
            }
            if (map.containsKey("summary")) {
                hashMap.put("description", map.get("summary"));
            }
            if (map.containsKey("transparency")) {
                hashMap.put("availability", Integer.valueOf(map.get("transparency").equals(TJAdUnitConstants.String.TRANSPARENT) ? 1 : 0));
            }
            hashMap.put("rrule", parseRecurrenceRule(map));
            return hashMap;
        }
        throw new IllegalArgumentException("Invalid calendar event: start time is malformed. Date format expecting (yyyy-MM-DDTHH:MM:SS-xx:xx) or (yyyy-MM-DDTHH:MM-xx:xx) i.e. 2013-08-14T09:00:01-08:00");
    }

    private Date parseDate(String str) {
        String[] strArr = DATE_FORMATS;
        Date date = null;
        int i = 0;
        int length = strArr.length;
        while (i < length) {
            try {
                Date parse = new SimpleDateFormat(strArr[i], Locale.US).parse(str);
                if (parse != null) {
                    return parse;
                }
                date = parse;
                i++;
            } catch (ParseException unused) {
            }
        }
        return date;
    }

    private String parseRecurrenceRule(Map<String, String> map) throws IllegalArgumentException {
        StringBuilder sb = new StringBuilder();
        if (map.containsKey("frequency")) {
            String str = map.get("frequency");
            int parseInt = map.containsKey(TJAdUnitConstants.String.INTERVAL) ? Integer.parseInt(map.get(TJAdUnitConstants.String.INTERVAL)) : -1;
            if ("daily".equals(str)) {
                sb.append("FREQ=DAILY;");
                if (parseInt != -1) {
                    sb.append("INTERVAL=" + parseInt + ";");
                }
            } else if ("weekly".equals(str)) {
                sb.append("FREQ=WEEKLY;");
                if (parseInt != -1) {
                    sb.append("INTERVAL=" + parseInt + ";");
                }
                if (map.containsKey("daysInWeek")) {
                    String translateWeekIntegersToDays = translateWeekIntegersToDays(map.get("daysInWeek"));
                    if (translateWeekIntegersToDays == null) {
                        throw new IllegalArgumentException("invalid ");
                    }
                    sb.append("BYDAY=" + translateWeekIntegersToDays + ";");
                }
            } else if ("monthly".equals(str)) {
                sb.append("FREQ=MONTHLY;");
                if (parseInt != -1) {
                    sb.append("INTERVAL=" + parseInt + ";");
                }
                if (map.containsKey("daysInMonth")) {
                    String translateMonthIntegersToDays = translateMonthIntegersToDays(map.get("daysInMonth"));
                    if (translateMonthIntegersToDays == null) {
                        throw new IllegalArgumentException();
                    }
                    sb.append("BYMONTHDAY=" + translateMonthIntegersToDays + ";");
                }
            } else {
                throw new IllegalArgumentException("frequency is only supported for daily, weekly, and monthly.");
            }
        }
        return sb.toString();
    }

    private String translateWeekIntegersToDays(String str) throws IllegalArgumentException {
        StringBuilder sb = new StringBuilder();
        boolean[] zArr = new boolean[7];
        String[] split = str.split(",");
        for (String parseInt : split) {
            int parseInt2 = Integer.parseInt(parseInt);
            if (parseInt2 == 7) {
                parseInt2 = 0;
            }
            if (!zArr[parseInt2]) {
                sb.append(dayNumberToDayOfWeekString(parseInt2) + ",");
                zArr[parseInt2] = true;
            }
        }
        if (split.length == 0) {
            throw new IllegalArgumentException("must have at least 1 day of the week if specifying repeating weekly");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    private String translateMonthIntegersToDays(String str) throws IllegalArgumentException {
        StringBuilder sb = new StringBuilder();
        boolean[] zArr = new boolean[63];
        String[] split = str.split(",");
        for (String parseInt : split) {
            int parseInt2 = Integer.parseInt(parseInt);
            int i = parseInt2 + MAX_NUMBER_DAYS_IN_MONTH;
            if (!zArr[i]) {
                sb.append(dayNumberToDayOfMonthString(parseInt2) + ",");
                zArr[i] = true;
            }
        }
        if (split.length == 0) {
            throw new IllegalArgumentException("must have at least 1 day of the month if specifying repeating weekly");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    private String dayNumberToDayOfWeekString(int i) throws IllegalArgumentException {
        switch (i) {
            case 0:
                return "SU";
            case 1:
                return "MO";
            case 2:
                return "TU";
            case 3:
                return "WE";
            case 4:
                return "TH";
            case 5:
                return "FR";
            case 6:
                return "SA";
            default:
                throw new IllegalArgumentException("invalid day of week " + i);
        }
    }

    private String dayNumberToDayOfMonthString(int i) throws IllegalArgumentException {
        if (i == 0 || i < -31 || i > MAX_NUMBER_DAYS_IN_MONTH) {
            throw new IllegalArgumentException("invalid day of month " + i);
        }
        return "" + i;
    }

    /* access modifiers changed from: package-private */
    public void downloadImage(final Context context, String str, final MraidCommandFailureListener mraidCommandFailureListener) {
        AsyncTasks.safeExecuteOnExecutor(new DownloadImageAsyncTask(context, new DownloadImageAsyncTask.DownloadImageAsyncTaskListener() {
            public void onSuccess() {
                MoPubLog.d("Image successfully saved.");
            }

            public void onFailure() {
                Toast.makeText(context, "Image failed to download.", 0).show();
                MoPubLog.d("Error downloading and saving image file.");
                mraidCommandFailureListener.onFailure(new MraidCommandException("Error downloading and saving image file."));
            }
        }), str);
    }

    private void showUserDialog(final Context context, final String str, final MraidCommandFailureListener mraidCommandFailureListener) {
        new AlertDialog.Builder(context).setTitle("Save Image").setMessage("Download image to Picture gallery?").setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                MraidNativeCommandHandler.this.downloadImage(context, str, mraidCommandFailureListener);
            }
        }).setCancelable(true).show();
    }

    @VisibleForTesting
    static class DownloadImageAsyncTask extends AsyncTask<String, Void, Boolean> {
        private final Context mContext;
        private final DownloadImageAsyncTaskListener mListener;

        interface DownloadImageAsyncTaskListener {
            void onFailure();

            void onSuccess();
        }

        public DownloadImageAsyncTask(@NonNull Context context, @NonNull DownloadImageAsyncTaskListener downloadImageAsyncTaskListener) {
            this.mContext = context.getApplicationContext();
            this.mListener = downloadImageAsyncTaskListener;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Failed to insert an additional move for type inference into block B:23:? */
        /* JADX WARN: Type inference failed for: r8v4 */
        /* JADX WARN: Type inference failed for: r8v10 */
        /* JADX WARN: Type inference failed for: r8v11 */
        /* JADX WARN: Type inference failed for: r8v15, types: [java.io.OutputStream, java.io.Closeable, java.io.FileOutputStream] */
        /* JADX WARN: Type inference failed for: r8v16 */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Boolean doInBackground(@android.support.annotation.NonNull java.lang.String[] r8) {
            /*
                r7 = this;
                r0 = 0
                if (r8 == 0) goto L_0x0084
                int r1 = r8.length
                if (r1 == 0) goto L_0x0084
                r1 = r8[r0]
                if (r1 != 0) goto L_0x000c
                goto L_0x0084
            L_0x000c:
                java.io.File r1 = r7.getPictureStoragePath()
                r1.mkdirs()
                r8 = r8[r0]
                java.net.URI r2 = java.net.URI.create(r8)
                r3 = 0
                java.net.HttpURLConnection r8 = com.mopub.common.MoPubHttpUrlConnection.getHttpUrlConnection(r8)     // Catch:{ Exception -> 0x006d, all -> 0x006a }
                java.io.BufferedInputStream r4 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x006d, all -> 0x006a }
                java.io.InputStream r5 = r8.getInputStream()     // Catch:{ Exception -> 0x006d, all -> 0x006a }
                r4.<init>(r5)     // Catch:{ Exception -> 0x006d, all -> 0x006a }
                com.mopub.common.util.ResponseHeader r5 = com.mopub.common.util.ResponseHeader.LOCATION     // Catch:{ Exception -> 0x0067, all -> 0x0065 }
                java.lang.String r5 = r5.getKey()     // Catch:{ Exception -> 0x0067, all -> 0x0065 }
                java.lang.String r5 = r8.getHeaderField(r5)     // Catch:{ Exception -> 0x0067, all -> 0x0065 }
                boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Exception -> 0x0067, all -> 0x0065 }
                if (r6 != 0) goto L_0x003b
                java.net.URI r2 = java.net.URI.create(r5)     // Catch:{ Exception -> 0x0067, all -> 0x0065 }
            L_0x003b:
                java.util.Map r8 = r8.getHeaderFields()     // Catch:{ Exception -> 0x0067, all -> 0x0065 }
                java.lang.String r8 = r7.getFileNameForUriAndHeaders(r2, r8)     // Catch:{ Exception -> 0x0067, all -> 0x0065 }
                java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0067, all -> 0x0065 }
                r2.<init>(r1, r8)     // Catch:{ Exception -> 0x0067, all -> 0x0065 }
                java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0067, all -> 0x0065 }
                r8.<init>(r2)     // Catch:{ Exception -> 0x0067, all -> 0x0065 }
                com.mopub.common.util.Streams.copyContent(r4, r8)     // Catch:{ Exception -> 0x0068, all -> 0x0063 }
                java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x0068, all -> 0x0063 }
                r7.loadPictureIntoGalleryApp(r1)     // Catch:{ Exception -> 0x0068, all -> 0x0063 }
                r1 = 1
                java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0068, all -> 0x0063 }
                com.mopub.common.util.Streams.closeStream(r4)
                com.mopub.common.util.Streams.closeStream(r8)
                return r1
            L_0x0063:
                r0 = move-exception
                goto L_0x007b
            L_0x0065:
                r8 = move-exception
                goto L_0x007d
            L_0x0067:
                r8 = r3
            L_0x0068:
                r3 = r4
                goto L_0x006e
            L_0x006a:
                r8 = move-exception
                r4 = r3
                goto L_0x007d
            L_0x006d:
                r8 = r3
            L_0x006e:
                java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0079 }
                com.mopub.common.util.Streams.closeStream(r3)
                com.mopub.common.util.Streams.closeStream(r8)
                return r0
            L_0x0079:
                r0 = move-exception
                r4 = r3
            L_0x007b:
                r3 = r8
                r8 = r0
            L_0x007d:
                com.mopub.common.util.Streams.closeStream(r4)
                com.mopub.common.util.Streams.closeStream(r3)
                throw r8
            L_0x0084:
                java.lang.Boolean r8 = java.lang.Boolean.valueOf(r0)
                return r8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mopub.mraid.MraidNativeCommandHandler.DownloadImageAsyncTask.doInBackground(java.lang.String[]):java.lang.Boolean");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            if (bool == null || !bool.booleanValue()) {
                this.mListener.onFailure();
            } else {
                this.mListener.onSuccess();
            }
        }

        @Nullable
        private String getFileNameForUriAndHeaders(@NonNull URI uri, @Nullable Map<String, List<String>> map) {
            Preconditions.checkNotNull(uri);
            String path = uri.getPath();
            if (path == null || map == null) {
                return null;
            }
            String name = new File(path).getName();
            List list = map.get(MraidNativeCommandHandler.MIME_TYPE_HEADER);
            if (list != null && !list.isEmpty()) {
                if (list.get(0) != null) {
                    for (String str : ((String) list.get(0)).split(";")) {
                        if (str.contains("image/")) {
                            String str2 = "." + str.split("/")[1];
                            if (name.endsWith(str2)) {
                                return name;
                            }
                            return name + str2;
                        }
                    }
                    return name;
                }
            }
            return name;
        }

        private File getPictureStoragePath() {
            return new File(Environment.getExternalStorageDirectory(), "Pictures");
        }

        private void loadPictureIntoGalleryApp(String str) {
            MoPubMediaScannerConnectionClient moPubMediaScannerConnectionClient = new MoPubMediaScannerConnectionClient(str, null);
            MediaScannerConnection mediaScannerConnection = new MediaScannerConnection(this.mContext, moPubMediaScannerConnectionClient);
            moPubMediaScannerConnectionClient.setMediaScannerConnection(mediaScannerConnection);
            mediaScannerConnection.connect();
        }

        /* access modifiers changed from: package-private */
        @Deprecated
        @VisibleForTesting
        public DownloadImageAsyncTaskListener getListener() {
            return this.mListener;
        }
    }

    private static class MoPubMediaScannerConnectionClient implements MediaScannerConnection.MediaScannerConnectionClient {
        private final String mFilename;
        private MediaScannerConnection mMediaScannerConnection;
        private final String mMimeType;

        private MoPubMediaScannerConnectionClient(String str, String str2) {
            this.mFilename = str;
            this.mMimeType = str2;
        }

        /* access modifiers changed from: private */
        public void setMediaScannerConnection(MediaScannerConnection mediaScannerConnection) {
            this.mMediaScannerConnection = mediaScannerConnection;
        }

        public void onMediaScannerConnected() {
            if (this.mMediaScannerConnection != null) {
                this.mMediaScannerConnection.scanFile(this.mFilename, this.mMimeType);
            }
        }

        public void onScanCompleted(String str, Uri uri) {
            if (this.mMediaScannerConnection != null) {
                this.mMediaScannerConnection.disconnect();
            }
        }
    }
}
