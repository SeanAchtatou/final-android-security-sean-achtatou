package com.mopub.mobileads;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.adcolony.sdk.AdColonyAppOptions;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.mopub.common.MediationSettings;
import com.mopub.mobileads.CustomEventInterstitial;
import java.util.Map;

public class GooglePlayServicesInterstitial extends CustomEventInterstitial {
    public static final String AD_UNIT_ID_KEY = "adUnitID";
    public static final String LOCATION_KEY = "location";
    private InterstitialAd mGoogleInterstitialAd;
    /* access modifiers changed from: private */
    public CustomEventInterstitial.CustomEventInterstitialListener mInterstitialListener;

    /* access modifiers changed from: protected */
    public void loadInterstitial(Context context, CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, Map<String, Object> map, Map<String, String> map2) {
        this.mInterstitialListener = customEventInterstitialListener;
        if (extrasAreValid(map2)) {
            this.mGoogleInterstitialAd = new InterstitialAd(context);
            this.mGoogleInterstitialAd.setAdListener(new InterstitialAdListener());
            this.mGoogleInterstitialAd.setAdUnitId(map2.get("adUnitID"));
            AdRequest.Builder builder = new AdRequest.Builder();
            builder.setRequestAgent(AdColonyAppOptions.MOPUB);
            forwardNpaIfSet(builder);
            try {
                this.mGoogleInterstitialAd.loadAd(builder.build());
            } catch (NoClassDefFoundError unused) {
                this.mInterstitialListener.onInterstitialFailed(MoPubErrorCode.NETWORK_NO_FILL);
            }
        } else {
            this.mInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
        }
    }

    private void forwardNpaIfSet(AdRequest.Builder builder) {
        if (GooglePlayServicesMediationSettings.getNpaBundle() != null && !GooglePlayServicesMediationSettings.getNpaBundle().isEmpty()) {
            builder.addNetworkExtrasBundle(AdMobAdapter.class, GooglePlayServicesMediationSettings.getNpaBundle());
        }
    }

    /* access modifiers changed from: protected */
    public void showInterstitial() {
        if (this.mGoogleInterstitialAd.isLoaded()) {
            this.mGoogleInterstitialAd.show();
        } else {
            Log.d(AdColonyAppOptions.MOPUB, "Tried to show a Google Play Services interstitial ad before it finished loading. Please try again.");
        }
    }

    /* access modifiers changed from: protected */
    public void onInvalidate() {
        if (this.mGoogleInterstitialAd != null) {
            this.mGoogleInterstitialAd.setAdListener(null);
        }
    }

    private boolean extrasAreValid(Map<String, String> map) {
        return map.containsKey("adUnitID");
    }

    private class InterstitialAdListener extends AdListener {
        private InterstitialAdListener() {
        }

        public void onAdClosed() {
            Log.d(AdColonyAppOptions.MOPUB, "Google Play Services interstitial ad dismissed.");
            if (GooglePlayServicesInterstitial.this.mInterstitialListener != null) {
                GooglePlayServicesInterstitial.this.mInterstitialListener.onInterstitialDismissed();
            }
        }

        public void onAdFailedToLoad(int i) {
            Log.d(AdColonyAppOptions.MOPUB, "Google Play Services interstitial ad failed to load.");
            if (GooglePlayServicesInterstitial.this.mInterstitialListener != null) {
                GooglePlayServicesInterstitial.this.mInterstitialListener.onInterstitialFailed(getMoPubErrorCode(i));
            }
        }

        public void onAdLeftApplication() {
            Log.d(AdColonyAppOptions.MOPUB, "Google Play Services interstitial ad clicked.");
            if (GooglePlayServicesInterstitial.this.mInterstitialListener != null) {
                GooglePlayServicesInterstitial.this.mInterstitialListener.onInterstitialClicked();
            }
        }

        public void onAdLoaded() {
            Log.d(AdColonyAppOptions.MOPUB, "Google Play Services interstitial ad loaded successfully.");
            if (GooglePlayServicesInterstitial.this.mInterstitialListener != null) {
                GooglePlayServicesInterstitial.this.mInterstitialListener.onInterstitialLoaded();
            }
        }

        public void onAdOpened() {
            Log.d(AdColonyAppOptions.MOPUB, "Showing Google Play Services interstitial ad.");
            if (GooglePlayServicesInterstitial.this.mInterstitialListener != null) {
                GooglePlayServicesInterstitial.this.mInterstitialListener.onInterstitialShown();
            }
        }

        private MoPubErrorCode getMoPubErrorCode(int i) {
            switch (i) {
                case 0:
                    return MoPubErrorCode.INTERNAL_ERROR;
                case 1:
                    return MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR;
                case 2:
                    return MoPubErrorCode.NO_CONNECTION;
                case 3:
                    return MoPubErrorCode.NO_FILL;
                default:
                    return MoPubErrorCode.UNSPECIFIED;
            }
        }
    }

    public static final class GooglePlayServicesMediationSettings implements MediationSettings {
        private static Bundle npaBundle;

        public GooglePlayServicesMediationSettings() {
        }

        public GooglePlayServicesMediationSettings(Bundle bundle) {
            npaBundle = bundle;
        }

        public void setNpaBundle(Bundle bundle) {
            npaBundle = bundle;
        }

        /* access modifiers changed from: private */
        public static Bundle getNpaBundle() {
            return npaBundle;
        }
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    public InterstitialAd getGoogleInterstitialAd() {
        return this.mGoogleInterstitialAd;
    }
}
