package com.mopub.mobileads;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import com.integralads.avid.library.mopub.BuildConfig;
import com.millennialmedia.AppInfo;
import com.millennialmedia.CreativeInfo;
import com.millennialmedia.InterstitialAd;
import com.millennialmedia.MMException;
import com.millennialmedia.MMLog;
import com.millennialmedia.MMSDK;
import com.millennialmedia.internal.ActivityListenerManager;
import com.mopub.common.MoPub;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.ConsentStatus;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.mobileads.CustomEventInterstitial;
import com.tapjoy.TapjoyAuctionFlags;
import java.util.Map;

final class MillennialInterstitial extends CustomEventInterstitial {
    private static final String APID_KEY = "adUnitID";
    private static final String DCN_KEY = "dcn";
    private static final String TAG = "MillennialInterstitial";
    private Context context;
    /* access modifiers changed from: private */
    public CustomEventInterstitial.CustomEventInterstitialListener interstitialListener;
    private InterstitialAd millennialInterstitial;

    MillennialInterstitial() {
    }

    static {
        MoPubLog.d("Millennial Media Adapter Version: MoPubMM-1.3.0");
    }

    /* access modifiers changed from: private */
    public CreativeInfo getCreativeInfo() {
        if (this.millennialInterstitial == null) {
            return null;
        }
        return this.millennialInterstitial.getCreativeInfo();
    }

    /* access modifiers changed from: protected */
    public void loadInterstitial(Context context2, CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, Map<String, Object> map, Map<String, String> map2) {
        this.interstitialListener = customEventInterstitialListener;
        this.context = context2;
        PersonalInfoManager personalInformationManager = MoPub.getPersonalInformationManager();
        if (personalInformationManager != null) {
            try {
                Boolean gdprApplies = personalInformationManager.gdprApplies();
                if (gdprApplies != null) {
                    MMSDK.setConsentRequired(gdprApplies.booleanValue());
                }
            } catch (NullPointerException e) {
                MoPubLog.d("GDPR applicability cannot be determined.", e);
            }
            if (personalInformationManager.getPersonalInfoConsentStatus() == ConsentStatus.EXPLICIT_YES) {
                MMSDK.setConsentData(BuildConfig.SDK_NAME, TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE);
            }
        }
        if (context2 instanceof Activity) {
            try {
                MMSDK.initialize((Activity) context2, ActivityListenerManager.LifecycleState.RESUMED);
            } catch (IllegalStateException e2) {
                MoPubLog.d("Exception occurred initializing the MM SDK.", e2);
                this.interstitialListener.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
                return;
            }
        } else if (context2 instanceof Application) {
            try {
                MMSDK.initialize((Application) context2);
            } catch (MMException e3) {
                MoPubLog.d("Exception occurred initializing the MM SDK.", e3);
                this.interstitialListener.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
                return;
            }
        } else {
            MoPubLog.d("MM SDK must be initialized with an Activity or Application context.");
            this.interstitialListener.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
            return;
        }
        String str = map2.get("adUnitID");
        if (MillennialUtils.isEmpty(str)) {
            MoPubLog.d("Invalid extras-- Be sure you have an placement ID specified.");
            this.interstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
            return;
        }
        String str2 = map2.get(DCN_KEY);
        AppInfo mediator = new AppInfo().setMediator(MillennialUtils.MEDIATOR_ID);
        if (!MillennialUtils.isEmpty(str2)) {
            mediator.setSiteId(str2);
        }
        try {
            MMSDK.setAppInfo(mediator);
            MMSDK.setLocationEnabled(MoPub.getLocationAwareness() != MoPub.LocationAwareness.DISABLED);
            this.millennialInterstitial = InterstitialAd.createInstance(str);
            this.millennialInterstitial.setListener(new MillennialInterstitialListener());
            this.millennialInterstitial.load(context2, null);
        } catch (MMException e4) {
            MoPubLog.d("Exception occurred while obtaining an interstitial from MM SDK.", e4);
            this.interstitialListener.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
        }
    }

    /* access modifiers changed from: protected */
    public void showInterstitial() {
        if (this.millennialInterstitial.isReady()) {
            try {
                this.millennialInterstitial.show(this.context);
            } catch (MMException e) {
                MoPubLog.d("An exception occurred while attempting to show interstitial.", e);
                this.interstitialListener.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
            }
        } else {
            MoPubLog.d("showInterstitial called but interstitial is not ready.");
        }
    }

    /* access modifiers changed from: protected */
    public void onInvalidate() {
        if (this.millennialInterstitial != null) {
            this.millennialInterstitial.destroy();
            this.millennialInterstitial = null;
        }
    }

    class MillennialInterstitialListener implements InterstitialAd.InterstitialListener {
        MillennialInterstitialListener() {
        }

        public void onAdLeftApplication(InterstitialAd interstitialAd) {
            MoPubLog.d("Millennial Interstitial Ad - Leaving application");
        }

        public void onClicked(InterstitialAd interstitialAd) {
            MoPubLog.d("Millennial Interstitial Ad - Ad was clicked");
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MillennialInterstitial.this.interstitialListener.onInterstitialClicked();
                }
            });
        }

        public void onClosed(InterstitialAd interstitialAd) {
            MoPubLog.d("Millennial Interstitial Ad - Ad was closed");
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MillennialInterstitial.this.interstitialListener.onInterstitialDismissed();
                }
            });
        }

        public void onExpired(InterstitialAd interstitialAd) {
            MoPubLog.d("Millennial Interstitial Ad - Ad expired");
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MillennialInterstitial.this.interstitialListener.onInterstitialFailed(MoPubErrorCode.NO_FILL);
                }
            });
        }

        public void onLoadFailed(InterstitialAd interstitialAd, InterstitialAd.InterstitialErrorStatus interstitialErrorStatus) {
            final MoPubErrorCode moPubErrorCode;
            MoPubLog.d("Millennial Interstitial Ad - load failed (" + interstitialErrorStatus.getErrorCode() + "): " + interstitialErrorStatus.getDescription());
            int errorCode = interstitialErrorStatus.getErrorCode();
            if (errorCode != 7) {
                if (errorCode != 201) {
                    if (errorCode != 203) {
                        switch (errorCode) {
                            case 1:
                            case 3:
                            case 4:
                                break;
                            case 2:
                                moPubErrorCode = MoPubErrorCode.NO_CONNECTION;
                                break;
                            default:
                                moPubErrorCode = MoPubErrorCode.NETWORK_NO_FILL;
                                break;
                        }
                    } else {
                        MillennialInterstitial.this.interstitialListener.onInterstitialLoaded();
                        MoPubLog.d("Millennial Interstitial Ad - Attempted to load ads when ads are already loaded.");
                        return;
                    }
                }
                moPubErrorCode = MoPubErrorCode.INTERNAL_ERROR;
            } else {
                moPubErrorCode = MoPubErrorCode.UNSPECIFIED;
            }
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MillennialInterstitial.this.interstitialListener.onInterstitialFailed(moPubErrorCode);
                }
            });
        }

        public void onLoaded(InterstitialAd interstitialAd) {
            MoPubLog.d("Millennial Interstitial Ad - Ad loaded splendidly");
            CreativeInfo access$100 = MillennialInterstitial.this.getCreativeInfo();
            if (access$100 != null && MMLog.isDebugEnabled()) {
                MoPubLog.d("Interstitial Creative Info: " + access$100);
            }
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MillennialInterstitial.this.interstitialListener.onInterstitialLoaded();
                }
            });
        }

        public void onShowFailed(InterstitialAd interstitialAd, InterstitialAd.InterstitialErrorStatus interstitialErrorStatus) {
            MoPubLog.d("Millennial Interstitial Ad - Show failed (" + interstitialErrorStatus.getErrorCode() + "): " + interstitialErrorStatus.getDescription());
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MillennialInterstitial.this.interstitialListener.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
                }
            });
        }

        public void onShown(InterstitialAd interstitialAd) {
            MoPubLog.d("Millennial Interstitial Ad - Ad shown");
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MillennialInterstitial.this.interstitialListener.onInterstitialShown();
                }
            });
        }
    }
}
