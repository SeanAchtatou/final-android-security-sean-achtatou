package com.mopub.mobileads;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.ExploreByTouchHelper;
import android.text.TextUtils;
import android.view.View;
import com.mopub.common.AdReport;
import com.mopub.common.DataKeys;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.ReflectionTarget;
import com.mopub.mobileads.BannerVisibilityTracker;
import com.mopub.mobileads.CustomEventBanner;
import com.mopub.mobileads.factories.CustomEventBannerFactory;
import java.util.Map;
import java.util.TreeMap;

public class CustomEventBannerAdapter implements CustomEventBanner.CustomEventBannerListener {
    public static final int DEFAULT_BANNER_TIMEOUT_DELAY = 10000;
    private Context mContext;
    /* access modifiers changed from: private */
    public CustomEventBanner mCustomEventBanner;
    private final Handler mHandler;
    private int mImpressionMinVisibleDips = ExploreByTouchHelper.INVALID_ID;
    private int mImpressionMinVisibleMs = ExploreByTouchHelper.INVALID_ID;
    private boolean mInvalidated;
    private boolean mIsVisibilityImpressionTrackingEnabled = false;
    private Map<String, Object> mLocalExtras;
    /* access modifiers changed from: private */
    public MoPubView mMoPubView;
    private Map<String, String> mServerExtras;
    private final Runnable mTimeout;
    @Nullable
    private BannerVisibilityTracker mVisibilityTracker;

    public CustomEventBannerAdapter(@NonNull MoPubView moPubView, @NonNull String str, @NonNull Map<String, String> map, long j, @Nullable AdReport adReport) {
        Preconditions.checkNotNull(map);
        this.mHandler = new Handler();
        this.mMoPubView = moPubView;
        this.mContext = moPubView.getContext();
        this.mTimeout = new Runnable() {
            public void run() {
                MoPubLog.d("Third-party network timed out.");
                CustomEventBannerAdapter.this.onBannerFailed(MoPubErrorCode.NETWORK_TIMEOUT);
                CustomEventBannerAdapter.this.invalidate();
            }
        };
        MoPubLog.d("Attempting to invoke custom event: " + str);
        try {
            this.mCustomEventBanner = CustomEventBannerFactory.create(str);
            this.mServerExtras = new TreeMap(map);
            parseBannerImpressionTrackingHeaders();
            this.mLocalExtras = this.mMoPubView.getLocalExtras();
            if (this.mMoPubView.getLocation() != null) {
                this.mLocalExtras.put(GooglePlayServicesInterstitial.LOCATION_KEY, this.mMoPubView.getLocation());
            }
            this.mLocalExtras.put(DataKeys.BROADCAST_IDENTIFIER_KEY, Long.valueOf(j));
            this.mLocalExtras.put(DataKeys.AD_REPORT_KEY, adReport);
            this.mLocalExtras.put(DataKeys.AD_WIDTH, Integer.valueOf(this.mMoPubView.getAdWidth()));
            this.mLocalExtras.put(DataKeys.AD_HEIGHT, Integer.valueOf(this.mMoPubView.getAdHeight()));
            this.mLocalExtras.put(DataKeys.BANNER_IMPRESSION_PIXEL_COUNT_ENABLED, Boolean.valueOf(this.mIsVisibilityImpressionTrackingEnabled));
        } catch (Exception unused) {
            MoPubLog.d("Couldn't locate or instantiate custom event: " + str + ".");
            this.mMoPubView.loadFailUrl(MoPubErrorCode.ADAPTER_NOT_FOUND);
        }
    }

    /* access modifiers changed from: package-private */
    @ReflectionTarget
    public void loadAd() {
        if (!isInvalidated() && this.mCustomEventBanner != null) {
            this.mHandler.postDelayed(this.mTimeout, (long) getTimeoutDelayMilliseconds());
            try {
                this.mCustomEventBanner.loadBanner(this.mContext, this, this.mLocalExtras, this.mServerExtras);
            } catch (Exception e) {
                MoPubLog.d("Loading a custom event banner threw an exception.", e);
                onBannerFailed(MoPubErrorCode.INTERNAL_ERROR);
            }
        }
    }

    /* access modifiers changed from: package-private */
    @ReflectionTarget
    public void invalidate() {
        if (this.mCustomEventBanner != null) {
            try {
                this.mCustomEventBanner.onInvalidate();
            } catch (Exception e) {
                MoPubLog.d("Invalidating a custom event banner threw an exception", e);
            }
        }
        if (this.mVisibilityTracker != null) {
            try {
                this.mVisibilityTracker.destroy();
            } catch (Exception e2) {
                MoPubLog.d("Destroying a banner visibility tracker threw an exception", e2);
            }
        }
        this.mContext = null;
        this.mCustomEventBanner = null;
        this.mLocalExtras = null;
        this.mServerExtras = null;
        this.mInvalidated = true;
    }

    /* access modifiers changed from: package-private */
    public boolean isInvalidated() {
        return this.mInvalidated;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public int getImpressionMinVisibleDips() {
        return this.mImpressionMinVisibleDips;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public int getImpressionMinVisibleMs() {
        return this.mImpressionMinVisibleMs;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public boolean isVisibilityImpressionTrackingEnabled() {
        return this.mIsVisibilityImpressionTrackingEnabled;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    @Deprecated
    @VisibleForTesting
    public BannerVisibilityTracker getVisibilityTracker() {
        return this.mVisibilityTracker;
    }

    private void cancelTimeout() {
        this.mHandler.removeCallbacks(this.mTimeout);
    }

    private int getTimeoutDelayMilliseconds() {
        if (this.mMoPubView == null || this.mMoPubView.getAdTimeoutDelay() == null || this.mMoPubView.getAdTimeoutDelay().intValue() < 0) {
            return 10000;
        }
        return this.mMoPubView.getAdTimeoutDelay().intValue() * 1000;
    }

    private void parseBannerImpressionTrackingHeaders() {
        String str = this.mServerExtras.get(DataKeys.BANNER_IMPRESSION_MIN_VISIBLE_DIPS);
        String str2 = this.mServerExtras.get(DataKeys.BANNER_IMPRESSION_MIN_VISIBLE_MS);
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            try {
                this.mImpressionMinVisibleDips = Integer.parseInt(str);
            } catch (NumberFormatException unused) {
                MoPubLog.d("Cannot parse integer from header Banner-Impression-Min-Pixels");
            }
            try {
                this.mImpressionMinVisibleMs = Integer.parseInt(str2);
            } catch (NumberFormatException unused2) {
                MoPubLog.d("Cannot parse integer from header Banner-Impression-Min-Ms");
            }
            if (this.mImpressionMinVisibleDips > 0 && this.mImpressionMinVisibleMs >= 0) {
                this.mIsVisibilityImpressionTrackingEnabled = true;
            }
        }
    }

    public void onBannerLoaded(View view) {
        if (!isInvalidated()) {
            cancelTimeout();
            if (this.mMoPubView != null) {
                this.mMoPubView.nativeAdLoaded();
                if (this.mIsVisibilityImpressionTrackingEnabled) {
                    this.mMoPubView.pauseAutorefresh();
                    this.mVisibilityTracker = new BannerVisibilityTracker(this.mContext, this.mMoPubView, view, this.mImpressionMinVisibleDips, this.mImpressionMinVisibleMs);
                    this.mVisibilityTracker.setBannerVisibilityTrackerListener(new BannerVisibilityTracker.BannerVisibilityTrackerListener() {
                        public void onVisibilityChanged() {
                            CustomEventBannerAdapter.this.mMoPubView.trackNativeImpression();
                            if (CustomEventBannerAdapter.this.mCustomEventBanner != null) {
                                CustomEventBannerAdapter.this.mCustomEventBanner.trackMpxAndThirdPartyImpressions();
                            }
                            CustomEventBannerAdapter.this.mMoPubView.resumeAutorefresh();
                        }
                    });
                }
                this.mMoPubView.setAdContentView(view);
                if (!this.mIsVisibilityImpressionTrackingEnabled && !(view instanceof HtmlBannerWebView)) {
                    this.mMoPubView.trackNativeImpression();
                }
            }
        }
    }

    public void onBannerFailed(MoPubErrorCode moPubErrorCode) {
        if (!isInvalidated() && this.mMoPubView != null) {
            if (moPubErrorCode == null) {
                moPubErrorCode = MoPubErrorCode.UNSPECIFIED;
            }
            cancelTimeout();
            this.mMoPubView.loadFailUrl(moPubErrorCode);
        }
    }

    public void onBannerExpanded() {
        if (!isInvalidated()) {
            this.mMoPubView.expand();
            this.mMoPubView.adPresentedOverlay();
        }
    }

    public void onBannerCollapsed() {
        if (!isInvalidated()) {
            this.mMoPubView.collapse();
            this.mMoPubView.adClosed();
        }
    }

    public void onBannerClicked() {
        if (!isInvalidated() && this.mMoPubView != null) {
            this.mMoPubView.registerClick();
        }
    }

    public void onLeaveApplication() {
        onBannerClicked();
    }
}
