package com.mopub.mobileads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Strings;
import com.mopub.mobileads.util.XmlUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.w3c.dom.Node;

class VastLinearXmlManager {
    private static final String CLICK_THROUGH = "ClickThrough";
    private static final String CLICK_TRACKER = "ClickTracking";
    private static final String CLOSE = "close";
    private static final String CLOSE_LINEAR = "closeLinear";
    private static final String COMPLETE = "complete";
    private static final String CREATIVE_VIEW = "creativeView";
    private static final int CREATIVE_VIEW_TRACKER_THRESHOLD = 0;
    private static final String EVENT = "event";
    private static final float FIRST_QUARTER_MARKER = 0.25f;
    private static final String FIRST_QUARTILE = "firstQuartile";
    public static final String ICON = "Icon";
    public static final String ICONS = "Icons";
    private static final String MEDIA_FILE = "MediaFile";
    private static final String MEDIA_FILES = "MediaFiles";
    private static final String MIDPOINT = "midpoint";
    private static final float MID_POINT_MARKER = 0.5f;
    private static final String OFFSET = "offset";
    private static final String PAUSE = "pause";
    private static final String PROGRESS = "progress";
    private static final String RESUME = "resume";
    private static final String SKIP = "skip";
    private static final String SKIP_OFFSET = "skipoffset";
    private static final String START = "start";
    private static final int START_TRACKER_THRESHOLD = 0;
    private static final float THIRD_QUARTER_MARKER = 0.75f;
    private static final String THIRD_QUARTILE = "thirdQuartile";
    private static final String TRACKING_EVENTS = "TrackingEvents";
    private static final String VIDEO_CLICKS = "VideoClicks";
    private static final String VIDEO_TRACKER = "Tracking";
    @NonNull
    private final Node mLinearNode;

    VastLinearXmlManager(@NonNull Node node) {
        Preconditions.checkNotNull(node);
        this.mLinearNode = node;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public List<VastFractionalProgressTracker> getFractionalProgressTrackers() {
        ArrayList arrayList = new ArrayList();
        addQuartileTrackerWithFraction(arrayList, getVideoTrackersByAttribute("firstQuartile"), FIRST_QUARTER_MARKER);
        addQuartileTrackerWithFraction(arrayList, getVideoTrackersByAttribute("midpoint"), MID_POINT_MARKER);
        addQuartileTrackerWithFraction(arrayList, getVideoTrackersByAttribute("thirdQuartile"), THIRD_QUARTER_MARKER);
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.mLinearNode, TRACKING_EVENTS);
        if (firstMatchingChildNode != null) {
            for (Node next : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, VIDEO_TRACKER, "event", Collections.singletonList("progress"))) {
                String attributeValue = XmlUtils.getAttributeValue(next, "offset");
                if (attributeValue != null) {
                    String trim = attributeValue.trim();
                    if (Strings.isPercentageTracker(trim)) {
                        String nodeValue = XmlUtils.getNodeValue(next);
                        try {
                            float parseFloat = Float.parseFloat(trim.replace("%", "")) / 100.0f;
                            if (parseFloat >= 0.0f) {
                                arrayList.add(new VastFractionalProgressTracker(nodeValue, parseFloat));
                            }
                        } catch (NumberFormatException unused) {
                            MoPubLog.d(String.format("Failed to parse VAST progress tracker %s", trim));
                        }
                    }
                }
            }
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public List<VastAbsoluteProgressTracker> getAbsoluteProgressTrackers() {
        ArrayList arrayList = new ArrayList();
        for (String vastAbsoluteProgressTracker : getVideoTrackersByAttribute("start")) {
            arrayList.add(new VastAbsoluteProgressTracker(vastAbsoluteProgressTracker, 0));
        }
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.mLinearNode, TRACKING_EVENTS);
        if (firstMatchingChildNode != null) {
            for (Node next : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, VIDEO_TRACKER, "event", Collections.singletonList("progress"))) {
                String attributeValue = XmlUtils.getAttributeValue(next, "offset");
                if (attributeValue != null) {
                    String trim = attributeValue.trim();
                    if (Strings.isAbsoluteTracker(trim)) {
                        String nodeValue = XmlUtils.getNodeValue(next);
                        try {
                            Integer parseAbsoluteOffset = Strings.parseAbsoluteOffset(trim);
                            if (parseAbsoluteOffset != null && parseAbsoluteOffset.intValue() >= 0) {
                                arrayList.add(new VastAbsoluteProgressTracker(nodeValue, parseAbsoluteOffset.intValue()));
                            }
                        } catch (NumberFormatException unused) {
                            MoPubLog.d(String.format("Failed to parse VAST progress tracker %s", trim));
                        }
                    }
                }
            }
            for (Node nodeValue2 : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, VIDEO_TRACKER, "event", Collections.singletonList(CREATIVE_VIEW))) {
                String nodeValue3 = XmlUtils.getNodeValue(nodeValue2);
                if (nodeValue3 != null) {
                    arrayList.add(new VastAbsoluteProgressTracker(nodeValue3, 0));
                }
            }
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public List<VastTracker> getVideoCompleteTrackers() {
        return getVideoTrackersByAttributeAsVastTrackers("complete");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mopub.mobileads.VastTracker.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.mopub.mobileads.VastTracker.<init>(com.mopub.mobileads.VastTracker$MessageType, java.lang.String):void
      com.mopub.mobileads.VastTracker.<init>(java.lang.String, boolean):void */
    /* access modifiers changed from: package-private */
    @NonNull
    public List<VastTracker> getPauseTrackers() {
        List<String> videoTrackersByAttribute = getVideoTrackersByAttribute(PAUSE);
        ArrayList arrayList = new ArrayList();
        for (String vastTracker : videoTrackersByAttribute) {
            arrayList.add(new VastTracker(vastTracker, true));
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mopub.mobileads.VastTracker.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.mopub.mobileads.VastTracker.<init>(com.mopub.mobileads.VastTracker$MessageType, java.lang.String):void
      com.mopub.mobileads.VastTracker.<init>(java.lang.String, boolean):void */
    /* access modifiers changed from: package-private */
    @NonNull
    public List<VastTracker> getResumeTrackers() {
        List<String> videoTrackersByAttribute = getVideoTrackersByAttribute(RESUME);
        ArrayList arrayList = new ArrayList();
        for (String vastTracker : videoTrackersByAttribute) {
            arrayList.add(new VastTracker(vastTracker, true));
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public List<VastTracker> getVideoCloseTrackers() {
        List<VastTracker> videoTrackersByAttributeAsVastTrackers = getVideoTrackersByAttributeAsVastTrackers("close");
        videoTrackersByAttributeAsVastTrackers.addAll(getVideoTrackersByAttributeAsVastTrackers(CLOSE_LINEAR));
        return videoTrackersByAttributeAsVastTrackers;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public List<VastTracker> getVideoSkipTrackers() {
        return getVideoTrackersByAttributeAsVastTrackers(SKIP);
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String getClickThroughUrl() {
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.mLinearNode, VIDEO_CLICKS);
        if (firstMatchingChildNode == null) {
            return null;
        }
        return XmlUtils.getNodeValue(XmlUtils.getFirstMatchingChildNode(firstMatchingChildNode, CLICK_THROUGH));
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public List<VastTracker> getClickTrackers() {
        ArrayList arrayList = new ArrayList();
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.mLinearNode, VIDEO_CLICKS);
        if (firstMatchingChildNode == null) {
            return arrayList;
        }
        for (Node nodeValue : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, CLICK_TRACKER)) {
            String nodeValue2 = XmlUtils.getNodeValue(nodeValue);
            if (nodeValue2 != null) {
                arrayList.add(new VastTracker(nodeValue2));
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String getSkipOffset() {
        String attributeValue = XmlUtils.getAttributeValue(this.mLinearNode, SKIP_OFFSET);
        if (attributeValue != null && !attributeValue.trim().isEmpty()) {
            return attributeValue.trim();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public List<VastMediaXmlManager> getMediaXmlManagers() {
        ArrayList arrayList = new ArrayList();
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.mLinearNode, MEDIA_FILES);
        if (firstMatchingChildNode == null) {
            return arrayList;
        }
        for (Node vastMediaXmlManager : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, MEDIA_FILE)) {
            arrayList.add(new VastMediaXmlManager(vastMediaXmlManager));
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public List<VastIconXmlManager> getIconXmlManagers() {
        ArrayList arrayList = new ArrayList();
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.mLinearNode, ICONS);
        if (firstMatchingChildNode == null) {
            return arrayList;
        }
        for (Node vastIconXmlManager : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, ICON)) {
            arrayList.add(new VastIconXmlManager(vastIconXmlManager));
        }
        return arrayList;
    }

    @NonNull
    private List<VastTracker> getVideoTrackersByAttributeAsVastTrackers(@NonNull String str) {
        List<String> videoTrackersByAttribute = getVideoTrackersByAttribute(str);
        ArrayList arrayList = new ArrayList(videoTrackersByAttribute.size());
        for (String vastTracker : videoTrackersByAttribute) {
            arrayList.add(new VastTracker(vastTracker));
        }
        return arrayList;
    }

    @NonNull
    private List<String> getVideoTrackersByAttribute(@NonNull String str) {
        Preconditions.checkNotNull(str);
        ArrayList arrayList = new ArrayList();
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.mLinearNode, TRACKING_EVENTS);
        if (firstMatchingChildNode == null) {
            return arrayList;
        }
        for (Node nodeValue : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, VIDEO_TRACKER, "event", Collections.singletonList(str))) {
            String nodeValue2 = XmlUtils.getNodeValue(nodeValue);
            if (nodeValue2 != null) {
                arrayList.add(nodeValue2);
            }
        }
        return arrayList;
    }

    private void addQuartileTrackerWithFraction(@NonNull List<VastFractionalProgressTracker> list, @NonNull List<String> list2, float f) {
        Preconditions.checkNotNull(list, "trackers cannot be null");
        Preconditions.checkNotNull(list2, "urls cannot be null");
        for (String vastFractionalProgressTracker : list2) {
            list.add(new VastFractionalProgressTracker(vastFractionalProgressTracker, f));
        }
    }
}
