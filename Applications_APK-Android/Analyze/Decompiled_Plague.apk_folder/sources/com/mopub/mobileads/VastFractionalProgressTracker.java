package com.mopub.mobileads;

import android.support.annotation.NonNull;
import com.mopub.common.Preconditions;
import com.mopub.mobileads.VastTracker;
import java.io.Serializable;
import java.util.Locale;

public class VastFractionalProgressTracker extends VastTracker implements Comparable<VastFractionalProgressTracker>, Serializable {
    private static final long serialVersionUID = 0;
    private final float mFraction;

    public VastFractionalProgressTracker(@NonNull VastTracker.MessageType messageType, @NonNull String str, float f) {
        super(messageType, str);
        Preconditions.checkArgument(f >= 0.0f);
        this.mFraction = f;
    }

    public VastFractionalProgressTracker(@NonNull String str, float f) {
        this(VastTracker.MessageType.TRACKING_URL, str, f);
    }

    public float trackingFraction() {
        return this.mFraction;
    }

    public int compareTo(@NonNull VastFractionalProgressTracker vastFractionalProgressTracker) {
        return Double.compare((double) trackingFraction(), (double) vastFractionalProgressTracker.trackingFraction());
    }

    public String toString() {
        return String.format(Locale.US, "%2f: %s", Float.valueOf(this.mFraction), getContent());
    }
}
