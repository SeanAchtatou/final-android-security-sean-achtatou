package com.mopub.mobileads;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.integralads.avid.library.mopub.BuildConfig;
import com.millennialmedia.AppInfo;
import com.millennialmedia.CreativeInfo;
import com.millennialmedia.InterstitialAd;
import com.millennialmedia.MMException;
import com.millennialmedia.MMLog;
import com.millennialmedia.MMSDK;
import com.millennialmedia.XIncentivizedEventListener;
import com.millennialmedia.internal.ActivityListenerManager;
import com.mopub.common.BaseLifecycleListener;
import com.mopub.common.LifecycleListener;
import com.mopub.common.MoPub;
import com.mopub.common.MoPubReward;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.ConsentStatus;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.mobileads.CustomEventRewardedVideo;
import com.tapjoy.TapjoyAuctionFlags;
import java.util.Map;

final class MillennialRewardedVideo extends CustomEventRewardedVideo {
    private static final String APID_KEY = "adUnitID";
    private static final String DCN_KEY = "dcn";
    private static final String TAG = "MillennialRewardedVideo";
    private Activity activity;
    private String apid = null;
    /* access modifiers changed from: private */
    public InterstitialAd millennialInterstitial;
    private MillennialRewardedVideoListener millennialRewardedVideoListener = new MillennialRewardedVideoListener();

    MillennialRewardedVideo() {
    }

    static {
        MoPubLog.d("Millennial Media Adapter Version: MoPubMM-1.3.0");
    }

    /* access modifiers changed from: private */
    public CreativeInfo getCreativeInfo() {
        if (this.millennialInterstitial == null) {
            return null;
        }
        return this.millennialInterstitial.getCreativeInfo();
    }

    /* access modifiers changed from: protected */
    @Nullable
    public CustomEventRewardedVideo.CustomEventRewardedVideoListener getVideoListenerForSdk() {
        return this.millennialRewardedVideoListener;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public LifecycleListener getLifecycleListener() {
        return new BaseLifecycleListener();
    }

    /* access modifiers changed from: protected */
    @NonNull
    public String getAdNetworkId() {
        return this.apid == null ? "" : this.apid;
    }

    /* access modifiers changed from: protected */
    public void onInvalidate() {
        if (this.millennialInterstitial != null) {
            this.millennialInterstitial.destroy();
            this.millennialInterstitial = null;
            this.apid = null;
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkAndInitializeSdk(@NonNull Activity activity2, @NonNull Map<String, Object> map, @NonNull Map<String, String> map2) throws Exception {
        try {
            MMSDK.initialize(activity2, ActivityListenerManager.LifecycleState.RESUMED);
            PersonalInfoManager personalInformationManager = MoPub.getPersonalInformationManager();
            if (personalInformationManager == null) {
                return true;
            }
            try {
                Boolean gdprApplies = personalInformationManager.gdprApplies();
                if (gdprApplies != null) {
                    MMSDK.setConsentRequired(gdprApplies.booleanValue());
                }
            } catch (NullPointerException e) {
                MoPubLog.d("GDPR applicability cannot be determined.", e);
            }
            if (personalInformationManager.getPersonalInfoConsentStatus() != ConsentStatus.EXPLICIT_YES) {
                return true;
            }
            MMSDK.setConsentData(BuildConfig.SDK_NAME, TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE);
            return true;
        } catch (IllegalStateException e2) {
            MoPubLog.d("An exception occurred initializing the MM SDK", e2);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void loadWithSdkInitialized(@NonNull Activity activity2, @NonNull Map<String, Object> map, @NonNull Map<String, String> map2) throws Exception {
        this.activity = activity2;
        this.apid = map2.get("adUnitID");
        String str = map2.get(DCN_KEY);
        if (MillennialUtils.isEmpty(this.apid)) {
            MoPubLog.d("Invalid extras-- Be sure you have a placement ID specified.");
            MoPubRewardedVideoManager.onRewardedVideoLoadFailure(MillennialRewardedVideo.class, "", MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
            return;
        }
        try {
            MMSDK.setAppInfo(new AppInfo().setMediator(MillennialUtils.MEDIATOR_ID).setSiteId(str));
            MMSDK.setLocationEnabled(MoPub.getLocationAwareness() != MoPub.LocationAwareness.DISABLED);
            this.millennialInterstitial = InterstitialAd.createInstance(this.apid);
            this.millennialInterstitial.setListener(this.millennialRewardedVideoListener);
            this.millennialInterstitial.xSetIncentivizedListener(this.millennialRewardedVideoListener);
            this.millennialInterstitial.load(activity2, null);
        } catch (MMException e) {
            MoPubLog.d("An exception occurred loading an InterstitialAd", e);
            MoPubRewardedVideoManager.onRewardedVideoLoadFailure(MillennialRewardedVideo.class, this.apid, MoPubErrorCode.INTERNAL_ERROR);
        }
    }

    /* access modifiers changed from: protected */
    public boolean hasVideoAvailable() {
        return this.millennialInterstitial != null && this.millennialInterstitial.isReady();
    }

    /* access modifiers changed from: protected */
    public void showVideo() {
        if (this.millennialInterstitial == null || !this.millennialInterstitial.isReady()) {
            MoPubLog.d("showVideo called before MillennialInterstitial ad was loaded.");
            return;
        }
        try {
            this.millennialInterstitial.show(this.activity);
        } catch (MMException e) {
            MoPubLog.d("An exception occurred showing the MM SDK interstitial.", e);
            MoPubRewardedVideoManager.onRewardedVideoPlaybackError(MillennialRewardedVideo.class, this.millennialInterstitial.placementId, MoPubErrorCode.INTERNAL_ERROR);
        }
    }

    class MillennialRewardedVideoListener implements InterstitialAd.InterstitialListener, XIncentivizedEventListener, CustomEventRewardedVideo.CustomEventRewardedVideoListener {
        MillennialRewardedVideoListener() {
        }

        public void onAdLeftApplication(InterstitialAd interstitialAd) {
            MoPubLog.d("Millennial Rewarded Video Ad - Leaving application");
        }

        public void onClicked(final InterstitialAd interstitialAd) {
            MoPubLog.d("Millennial Rewarded Video Ad - Ad was clicked");
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MoPubRewardedVideoManager.onRewardedVideoClicked(MillennialRewardedVideo.class, interstitialAd.placementId);
                }
            });
        }

        public void onClosed(final InterstitialAd interstitialAd) {
            MoPubLog.d("Millennial Rewarded Video Ad - Ad was closed");
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MoPubRewardedVideoManager.onRewardedVideoClosed(MillennialRewardedVideo.class, interstitialAd.placementId);
                }
            });
        }

        public void onExpired(final InterstitialAd interstitialAd) {
            MoPubLog.d("Millennial Rewarded Video Ad - Ad expired");
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MoPubRewardedVideoManager.onRewardedVideoLoadFailure(MillennialRewardedVideo.class, interstitialAd.placementId, MoPubErrorCode.VIDEO_NOT_AVAILABLE);
                }
            });
        }

        public void onLoadFailed(final InterstitialAd interstitialAd, InterstitialAd.InterstitialErrorStatus interstitialErrorStatus) {
            final MoPubErrorCode moPubErrorCode;
            MoPubLog.d("Millennial Rewarded Video Ad - load failed (" + interstitialErrorStatus.getErrorCode() + "): " + interstitialErrorStatus.getDescription());
            int errorCode = interstitialErrorStatus.getErrorCode();
            if (errorCode != 7) {
                if (errorCode != 201) {
                    if (errorCode != 203) {
                        switch (errorCode) {
                            case 1:
                            case 3:
                            case 4:
                                break;
                            case 2:
                                moPubErrorCode = MoPubErrorCode.NO_CONNECTION;
                                break;
                            default:
                                moPubErrorCode = MoPubErrorCode.NETWORK_NO_FILL;
                                break;
                        }
                    } else {
                        MillennialUtils.postOnUiThread(new Runnable() {
                            public void run() {
                                MoPubRewardedVideoManager.onRewardedVideoLoadSuccess(MillennialRewardedVideo.class, interstitialAd.placementId);
                            }
                        });
                        MoPubLog.d("Millennial Rewarded Video Ad - Attempted to load ads when ads are already loaded.");
                        return;
                    }
                }
                moPubErrorCode = MoPubErrorCode.INTERNAL_ERROR;
            } else {
                moPubErrorCode = MoPubErrorCode.UNSPECIFIED;
            }
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MoPubRewardedVideoManager.onRewardedVideoLoadFailure(MillennialRewardedVideo.class, interstitialAd.placementId, moPubErrorCode);
                }
            });
        }

        public void onLoaded(final InterstitialAd interstitialAd) {
            MoPubLog.d("Millennial Rewarded Video Ad - Ad loaded splendidly");
            CreativeInfo access$000 = MillennialRewardedVideo.this.getCreativeInfo();
            if (access$000 != null && MMLog.isDebugEnabled()) {
                MoPubLog.d("Rewarded Video Creative Info: " + access$000);
            }
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MoPubRewardedVideoManager.onRewardedVideoLoadSuccess(MillennialRewardedVideo.class, interstitialAd.placementId);
                }
            });
        }

        public void onShowFailed(final InterstitialAd interstitialAd, InterstitialAd.InterstitialErrorStatus interstitialErrorStatus) {
            MoPubLog.d("Millennial Rewarded Video Ad - Show failed (" + interstitialErrorStatus.getErrorCode() + "): " + interstitialErrorStatus.getDescription());
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MoPubRewardedVideoManager.onRewardedVideoPlaybackError(MillennialRewardedVideo.class, interstitialAd.placementId, MoPubErrorCode.VIDEO_PLAYBACK_ERROR);
                }
            });
        }

        public void onShown(final InterstitialAd interstitialAd) {
            MoPubLog.d("Millennial Rewarded Video Ad - Ad shown");
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MoPubRewardedVideoManager.onRewardedVideoStarted(MillennialRewardedVideo.class, interstitialAd.placementId);
                }
            });
        }

        public boolean onVideoComplete() {
            MoPubLog.d("Millennial Rewarded Video Ad - Video completed");
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MoPubRewardedVideoManager.onRewardedVideoCompleted(MillennialRewardedVideo.class, MillennialRewardedVideo.this.millennialInterstitial.placementId, MoPubReward.success("", 0));
                }
            });
            return false;
        }

        public boolean onCustomEvent(XIncentivizedEventListener.XIncentiveEvent xIncentiveEvent) {
            MoPubLog.d("Millennial Rewarded Video Ad - Custom event received: " + xIncentiveEvent.eventId + ", " + xIncentiveEvent.args);
            return false;
        }
    }
}
