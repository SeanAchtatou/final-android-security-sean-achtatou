package com.mopub.mobileads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.util.XmlUtils;
import org.w3c.dom.Node;

public class VideoViewabilityTrackerXmlManager {
    public static final String PERCENT_VIEWABLE = "percentViewable";
    public static final String VIEWABLE_PLAYTIME = "viewablePlaytime";
    private final Node mVideoViewabilityNode;

    VideoViewabilityTrackerXmlManager(@NonNull Node node) {
        Preconditions.checkNotNull(node);
        this.mVideoViewabilityNode = node;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0044  */
    @android.support.annotation.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Integer getViewablePlaytimeMS() {
        /*
            r6 = this;
            org.w3c.dom.Node r0 = r6.mVideoViewabilityNode
            java.lang.String r1 = "viewablePlaytime"
            java.lang.String r0 = com.mopub.mobileads.util.XmlUtils.getAttributeValue(r0, r1)
            r1 = 0
            if (r0 != 0) goto L_0x000c
            return r1
        L_0x000c:
            boolean r2 = com.mopub.common.util.Strings.isAbsoluteTracker(r0)
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0027
            java.lang.Integer r2 = com.mopub.common.util.Strings.parseAbsoluteOffset(r0)     // Catch:{ NumberFormatException -> 0x0019 }
            goto L_0x0042
        L_0x0019:
            java.lang.String r2 = "Invalid VAST viewablePlaytime format for \"HH:MM:SS[.mmm]\": %s:"
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r4[r3] = r0
            java.lang.String r0 = java.lang.String.format(r2, r4)
            com.mopub.common.logging.MoPubLog.d(r0)
            goto L_0x0041
        L_0x0027:
            float r2 = java.lang.Float.parseFloat(r0)     // Catch:{ NumberFormatException -> 0x0034 }
            r5 = 1148846080(0x447a0000, float:1000.0)
            float r2 = r2 * r5
            int r2 = (int) r2     // Catch:{ NumberFormatException -> 0x0034 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ NumberFormatException -> 0x0034 }
            goto L_0x0042
        L_0x0034:
            java.lang.String r2 = "Invalid VAST viewablePlaytime format for \"SS[.mmm]\": %s:"
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r4[r3] = r0
            java.lang.String r0 = java.lang.String.format(r2, r4)
            com.mopub.common.logging.MoPubLog.d(r0)
        L_0x0041:
            r2 = r1
        L_0x0042:
            if (r2 == 0) goto L_0x004c
            int r0 = r2.intValue()
            if (r0 >= 0) goto L_0x004b
            goto L_0x004c
        L_0x004b:
            return r2
        L_0x004c:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.mobileads.VideoViewabilityTrackerXmlManager.getViewablePlaytimeMS():java.lang.Integer");
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public Integer getPercentViewable() {
        Integer num;
        String attributeValue = XmlUtils.getAttributeValue(this.mVideoViewabilityNode, PERCENT_VIEWABLE);
        if (attributeValue == null) {
            return null;
        }
        try {
            num = Integer.valueOf((int) Float.parseFloat(attributeValue.replace("%", "")));
        } catch (NumberFormatException unused) {
            MoPubLog.d(String.format("Invalid VAST percentViewable format for \"d{1,3}%%\": %s:", attributeValue));
            num = null;
        }
        if (num == null || num.intValue() < 0 || num.intValue() > 100) {
            return null;
        }
        return num;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String getVideoViewabilityTrackerUrl() {
        return XmlUtils.getNodeValue(this.mVideoViewabilityNode);
    }
}
