package com.mopub.mobileads;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import com.adcolony.sdk.AdColonyAppOptions;
import com.mopub.common.AdReport;
import com.mopub.common.AdUrlGenerator;
import com.mopub.common.ClientMetadata;
import com.mopub.common.Constants;
import com.mopub.common.MoPub;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.mraid.MraidNativeCommandHandler;
import com.mopub.network.AdRequest;
import com.mopub.network.AdResponse;
import com.mopub.network.MoPubNetworkError;
import com.mopub.network.Networking;
import com.mopub.network.TrackingRequest;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.VolleyError;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.WeakHashMap;

public class AdViewController {
    static final double BACKOFF_FACTOR = 1.5d;
    static final int DEFAULT_REFRESH_TIME_MILLISECONDS = 60000;
    static final int MAX_REFRESH_TIME_MILLISECONDS = 600000;
    private static final FrameLayout.LayoutParams WRAP_AND_CENTER_LAYOUT_PARAMS = new FrameLayout.LayoutParams(-2, -2, 17);
    private static final WeakHashMap<View, Boolean> sViewShouldHonorServerDimensions = new WeakHashMap<>();
    @Nullable
    private AdRequest mActiveRequest;
    @NonNull
    private final AdRequest.Listener mAdListener;
    @Nullable
    private AdResponse mAdResponse;
    @Nullable
    private String mAdUnitId;
    private boolean mAdWasLoaded;
    @VisibleForTesting
    int mBackoffPower = 1;
    private final long mBroadcastIdentifier;
    @Nullable
    private Context mContext;
    private boolean mCurrentAutoRefreshStatus = true;
    @Nullable
    private String mCustomEventClassName;
    private boolean mExpanded;
    private Handler mHandler;
    private boolean mIsDestroyed;
    private boolean mIsLoading;
    private boolean mIsTesting;
    private String mKeywords;
    private Map<String, Object> mLocalExtras = new HashMap();
    private Location mLocation;
    @Nullable
    private MoPubView mMoPubView;
    private final Runnable mRefreshRunnable;
    @Nullable
    private Integer mRefreshTimeMillis;
    private boolean mShouldAllowAutoRefresh = true;
    private int mTimeoutMilliseconds;
    private String mUrl;
    @Nullable
    private WebViewAdUrlGenerator mUrlGenerator;
    private String mUserDataKeywords;

    public static void setShouldHonorServerDimensions(View view) {
        sViewShouldHonorServerDimensions.put(view, true);
    }

    private static boolean getShouldHonorServerDimensions(View view) {
        return sViewShouldHonorServerDimensions.get(view) != null;
    }

    public AdViewController(@NonNull Context context, @NonNull MoPubView moPubView) {
        this.mContext = context;
        this.mMoPubView = moPubView;
        this.mTimeoutMilliseconds = -1;
        this.mBroadcastIdentifier = Utils.generateUniqueId();
        this.mUrlGenerator = new WebViewAdUrlGenerator(this.mContext.getApplicationContext(), MraidNativeCommandHandler.isStorePictureSupported(this.mContext));
        this.mAdListener = new AdRequest.Listener() {
            public void onSuccess(AdResponse adResponse) {
                AdViewController.this.onAdLoadSuccess(adResponse);
            }

            public void onErrorResponse(VolleyError volleyError) {
                AdViewController.this.onAdLoadError(volleyError);
            }
        };
        this.mRefreshRunnable = new Runnable() {
            public void run() {
                AdViewController.this.internalLoadAd();
            }
        };
        this.mRefreshTimeMillis = Integer.valueOf((int) DEFAULT_REFRESH_TIME_MILLISECONDS);
        this.mHandler = new Handler();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void onAdLoadSuccess(@NonNull AdResponse adResponse) {
        int i;
        this.mBackoffPower = 1;
        this.mAdResponse = adResponse;
        this.mCustomEventClassName = adResponse.getCustomEventClassName();
        if (this.mAdResponse.getAdTimeoutMillis() == null) {
            i = this.mTimeoutMilliseconds;
        } else {
            i = this.mAdResponse.getAdTimeoutMillis().intValue();
        }
        this.mTimeoutMilliseconds = i;
        this.mRefreshTimeMillis = this.mAdResponse.getRefreshTimeMillis();
        setNotLoading();
        loadCustomEvent(this.mMoPubView, adResponse.getCustomEventClassName(), adResponse.getServerExtras());
        scheduleRefreshTimerIfEnabled();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void onAdLoadError(VolleyError volleyError) {
        if (volleyError instanceof MoPubNetworkError) {
            MoPubNetworkError moPubNetworkError = (MoPubNetworkError) volleyError;
            if (moPubNetworkError.getRefreshTimeMillis() != null) {
                this.mRefreshTimeMillis = moPubNetworkError.getRefreshTimeMillis();
            }
        }
        MoPubErrorCode errorCodeFromVolleyError = getErrorCodeFromVolleyError(volleyError, this.mContext);
        if (errorCodeFromVolleyError == MoPubErrorCode.SERVER_ERROR) {
            this.mBackoffPower++;
        }
        setNotLoading();
        adDidFail(errorCodeFromVolleyError);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void loadCustomEvent(@Nullable MoPubView moPubView, @Nullable String str, @NonNull Map<String, String> map) {
        Preconditions.checkNotNull(map);
        if (moPubView == null) {
            MoPubLog.d("Can't load an ad in this ad view because it was destroyed.");
        } else {
            moPubView.loadCustomEvent(str, map);
        }
    }

    @NonNull
    @VisibleForTesting
    static MoPubErrorCode getErrorCodeFromVolleyError(@NonNull VolleyError volleyError, @Nullable Context context) {
        NetworkResponse networkResponse = volleyError.networkResponse;
        if (volleyError instanceof MoPubNetworkError) {
            switch (((MoPubNetworkError) volleyError).getReason()) {
                case WARMING_UP:
                    return MoPubErrorCode.WARMUP;
                case NO_FILL:
                    return MoPubErrorCode.NO_FILL;
                default:
                    return MoPubErrorCode.UNSPECIFIED;
            }
        } else if (networkResponse == null) {
            if (!DeviceUtils.isNetworkAvailable(context)) {
                return MoPubErrorCode.NO_CONNECTION;
            }
            return MoPubErrorCode.UNSPECIFIED;
        } else if (volleyError.networkResponse.statusCode >= 400) {
            return MoPubErrorCode.SERVER_ERROR;
        } else {
            return MoPubErrorCode.UNSPECIFIED;
        }
    }

    @Nullable
    public MoPubView getMoPubView() {
        return this.mMoPubView;
    }

    public void loadAd() {
        this.mBackoffPower = 1;
        internalLoadAd();
    }

    /* access modifiers changed from: private */
    public void internalLoadAd() {
        this.mAdWasLoaded = true;
        if (TextUtils.isEmpty(this.mAdUnitId)) {
            MoPubLog.d("Can't load an ad in this ad view because the ad unit ID is not set. Did you forget to call setAdUnitId()?");
        } else if (!isNetworkAvailable()) {
            MoPubLog.d("Can't load an ad because there is no network connectivity.");
            scheduleRefreshTimerIfEnabled();
        } else {
            loadNonJavascript(generateAdUrl());
        }
    }

    /* access modifiers changed from: package-private */
    public void loadNonJavascript(@Nullable String str) {
        if (str != null) {
            if (!str.startsWith("javascript:")) {
                MoPubLog.d("Loading url: " + str);
            }
            if (!this.mIsLoading) {
                this.mUrl = str;
                this.mIsLoading = true;
                fetchAd(this.mUrl);
            } else if (!TextUtils.isEmpty(this.mAdUnitId)) {
                MoPubLog.i("Already loading an ad for " + this.mAdUnitId + ", wait to finish.");
            }
        }
    }

    public void reload() {
        MoPubLog.d("Reload ad: " + this.mUrl);
        loadNonJavascript(this.mUrl);
    }

    /* access modifiers changed from: package-private */
    public boolean loadFailUrl(MoPubErrorCode moPubErrorCode) {
        this.mIsLoading = false;
        StringBuilder sb = new StringBuilder();
        sb.append("MoPubErrorCode: ");
        sb.append(moPubErrorCode == null ? "" : moPubErrorCode.toString());
        Log.v(AdColonyAppOptions.MOPUB, sb.toString());
        String failoverUrl = this.mAdResponse == null ? "" : this.mAdResponse.getFailoverUrl();
        if (!TextUtils.isEmpty(failoverUrl)) {
            MoPubLog.d("Loading failover url: " + failoverUrl);
            loadNonJavascript(failoverUrl);
            return true;
        }
        adDidFail(MoPubErrorCode.NO_FILL);
        return false;
    }

    /* access modifiers changed from: package-private */
    public void setNotLoading() {
        this.mIsLoading = false;
        if (this.mActiveRequest != null) {
            if (!this.mActiveRequest.isCanceled()) {
                this.mActiveRequest.cancel();
            }
            this.mActiveRequest = null;
        }
    }

    public String getKeywords() {
        return this.mKeywords;
    }

    public void setKeywords(String str) {
        this.mKeywords = str;
    }

    public String getUserDataKeywords() {
        if (!MoPub.canCollectPersonalInformation()) {
            return null;
        }
        return this.mUserDataKeywords;
    }

    public void setUserDataKeywords(String str) {
        if (!MoPub.canCollectPersonalInformation()) {
            this.mUserDataKeywords = null;
        } else {
            this.mUserDataKeywords = str;
        }
    }

    public Location getLocation() {
        if (!MoPub.canCollectPersonalInformation()) {
            return null;
        }
        return this.mLocation;
    }

    public void setLocation(Location location) {
        if (!MoPub.canCollectPersonalInformation()) {
            this.mLocation = null;
        } else {
            this.mLocation = location;
        }
    }

    public String getAdUnitId() {
        return this.mAdUnitId;
    }

    @Nullable
    public String getCustomEventClassName() {
        return this.mCustomEventClassName;
    }

    public void setAdUnitId(@NonNull String str) {
        this.mAdUnitId = str;
    }

    public long getBroadcastIdentifier() {
        return this.mBroadcastIdentifier;
    }

    public int getAdWidth() {
        if (this.mAdResponse == null || this.mAdResponse.getWidth() == null) {
            return 0;
        }
        return this.mAdResponse.getWidth().intValue();
    }

    public int getAdHeight() {
        if (this.mAdResponse == null || this.mAdResponse.getHeight() == null) {
            return 0;
        }
        return this.mAdResponse.getHeight().intValue();
    }

    @Deprecated
    public boolean getAutorefreshEnabled() {
        return getCurrentAutoRefreshStatus();
    }

    public boolean getCurrentAutoRefreshStatus() {
        return this.mCurrentAutoRefreshStatus;
    }

    /* access modifiers changed from: package-private */
    public void pauseRefresh() {
        setAutoRefreshStatus(false);
    }

    /* access modifiers changed from: package-private */
    public void resumeRefresh() {
        if (this.mShouldAllowAutoRefresh && !this.mExpanded) {
            setAutoRefreshStatus(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void setShouldAllowAutoRefresh(boolean z) {
        this.mShouldAllowAutoRefresh = z;
        setAutoRefreshStatus(z);
    }

    private void setAutoRefreshStatus(boolean z) {
        if (this.mAdWasLoaded && this.mCurrentAutoRefreshStatus != z) {
            String str = z ? "enabled" : "disabled";
            MoPubLog.d("Refresh " + str + " for ad unit (" + this.mAdUnitId + ").");
        }
        this.mCurrentAutoRefreshStatus = z;
        if (this.mAdWasLoaded && this.mCurrentAutoRefreshStatus) {
            scheduleRefreshTimerIfEnabled();
        } else if (!this.mCurrentAutoRefreshStatus) {
            cancelRefreshTimer();
        }
    }

    /* access modifiers changed from: package-private */
    public void expand() {
        this.mExpanded = true;
        pauseRefresh();
    }

    /* access modifiers changed from: package-private */
    public void collapse() {
        this.mExpanded = false;
        resumeRefresh();
    }

    @Nullable
    public AdReport getAdReport() {
        if (this.mAdUnitId == null || this.mAdResponse == null) {
            return null;
        }
        return new AdReport(this.mAdUnitId, ClientMetadata.getInstance(this.mContext), this.mAdResponse);
    }

    public boolean getTesting() {
        return this.mIsTesting;
    }

    public void setTesting(boolean z) {
        this.mIsTesting = z;
    }

    /* access modifiers changed from: package-private */
    public boolean isDestroyed() {
        return this.mIsDestroyed;
    }

    /* access modifiers changed from: package-private */
    public void cleanup() {
        if (!this.mIsDestroyed) {
            if (this.mActiveRequest != null) {
                this.mActiveRequest.cancel();
                this.mActiveRequest = null;
            }
            setAutoRefreshStatus(false);
            cancelRefreshTimer();
            this.mMoPubView = null;
            this.mContext = null;
            this.mUrlGenerator = null;
            this.mIsDestroyed = true;
        }
    }

    /* access modifiers changed from: package-private */
    public Integer getAdTimeoutDelay() {
        return Integer.valueOf(this.mTimeoutMilliseconds);
    }

    /* access modifiers changed from: package-private */
    public void trackImpression() {
        if (this.mAdResponse != null) {
            TrackingRequest.makeTrackingHttpRequest(this.mAdResponse.getImpressionTrackingUrl(), this.mContext);
        }
    }

    /* access modifiers changed from: package-private */
    public void registerClick() {
        if (this.mAdResponse != null) {
            TrackingRequest.makeTrackingHttpRequest(this.mAdResponse.getClickTrackingUrl(), this.mContext);
        }
    }

    /* access modifiers changed from: package-private */
    public void fetchAd(String str) {
        MoPubView moPubView = getMoPubView();
        if (moPubView == null || this.mContext == null) {
            MoPubLog.d("Can't load an ad in this ad view because it was destroyed.");
            setNotLoading();
            return;
        }
        AdRequest adRequest = new AdRequest(str, moPubView.getAdFormat(), this.mAdUnitId, this.mContext, this.mAdListener);
        Networking.getRequestQueue(this.mContext).add(adRequest);
        this.mActiveRequest = adRequest;
    }

    /* access modifiers changed from: package-private */
    public void forceRefresh() {
        setNotLoading();
        loadAd();
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String generateAdUrl() {
        Location location = null;
        if (this.mUrlGenerator == null) {
            return null;
        }
        boolean canCollectPersonalInformation = MoPub.canCollectPersonalInformation();
        AdUrlGenerator withUserDataKeywords = this.mUrlGenerator.withAdUnitId(this.mAdUnitId).withKeywords(this.mKeywords).withUserDataKeywords(canCollectPersonalInformation ? this.mUserDataKeywords : null);
        if (canCollectPersonalInformation) {
            location = this.mLocation;
        }
        withUserDataKeywords.withLocation(location);
        return this.mUrlGenerator.generateUrlString(Constants.HOST);
    }

    /* access modifiers changed from: package-private */
    public void adDidFail(MoPubErrorCode moPubErrorCode) {
        MoPubLog.i("Ad failed to load.");
        setNotLoading();
        MoPubView moPubView = getMoPubView();
        if (moPubView != null) {
            scheduleRefreshTimerIfEnabled();
            moPubView.adFailed(moPubErrorCode);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* access modifiers changed from: package-private */
    public void scheduleRefreshTimerIfEnabled() {
        cancelRefreshTimer();
        if (this.mCurrentAutoRefreshStatus && this.mRefreshTimeMillis != null && this.mRefreshTimeMillis.intValue() > 0) {
            this.mHandler.postDelayed(this.mRefreshRunnable, Math.min(600000L, ((long) this.mRefreshTimeMillis.intValue()) * ((long) Math.pow(BACKOFF_FACTOR, (double) this.mBackoffPower))));
        }
    }

    /* access modifiers changed from: package-private */
    public void setLocalExtras(Map<String, Object> map) {
        this.mLocalExtras = map != null ? new TreeMap(map) : new TreeMap();
    }

    /* access modifiers changed from: package-private */
    public Map<String, Object> getLocalExtras() {
        return this.mLocalExtras != null ? new TreeMap(this.mLocalExtras) : new TreeMap();
    }

    private void cancelRefreshTimer() {
        this.mHandler.removeCallbacks(this.mRefreshRunnable);
    }

    private boolean isNetworkAvailable() {
        if (this.mContext == null) {
            return false;
        }
        if (!DeviceUtils.isPermissionGranted(this.mContext, "android.permission.ACCESS_NETWORK_STATE")) {
            return true;
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void setAdContentView(final View view) {
        this.mHandler.post(new Runnable() {
            public void run() {
                MoPubView moPubView = AdViewController.this.getMoPubView();
                if (moPubView != null) {
                    moPubView.removeAllViews();
                    moPubView.addView(view, AdViewController.this.getAdLayoutParams(view));
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public FrameLayout.LayoutParams getAdLayoutParams(View view) {
        Integer num;
        Integer num2 = null;
        if (this.mAdResponse != null) {
            num2 = this.mAdResponse.getWidth();
            num = this.mAdResponse.getHeight();
        } else {
            num = null;
        }
        if (num2 == null || num == null || !getShouldHonorServerDimensions(view) || num2.intValue() <= 0 || num.intValue() <= 0) {
            return WRAP_AND_CENTER_LAYOUT_PARAMS;
        }
        return new FrameLayout.LayoutParams(Dips.asIntPixels((float) num2.intValue(), this.mContext), Dips.asIntPixels((float) num.intValue(), this.mContext), 17);
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public Integer getRefreshTimeMillis() {
        return this.mRefreshTimeMillis;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public void setRefreshTimeMillis(@Nullable Integer num) {
        this.mRefreshTimeMillis = num;
    }
}
