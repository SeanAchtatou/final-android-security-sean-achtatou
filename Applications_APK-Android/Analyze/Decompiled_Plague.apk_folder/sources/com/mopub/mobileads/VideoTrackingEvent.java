package com.mopub.mobileads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.tapjoy.TJAdUnitConstants;

enum VideoTrackingEvent {
    START(TJAdUnitConstants.String.VIDEO_START),
    FIRST_QUARTILE(TJAdUnitConstants.String.VIDEO_FIRST_QUARTILE),
    MIDPOINT(TJAdUnitConstants.String.VIDEO_MIDPOINT),
    THIRD_QUARTILE(TJAdUnitConstants.String.VIDEO_THIRD_QUARTILE),
    COMPLETE(TJAdUnitConstants.String.VIDEO_COMPLETE),
    COMPANION_AD_VIEW("companionAdView"),
    COMPANION_AD_CLICK("companionAdClick"),
    UNKNOWN("");
    
    private final String name;

    private VideoTrackingEvent(@NonNull String str) {
        this.name = str;
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    @NonNull
    public static VideoTrackingEvent fromString(@Nullable String str) {
        if (str == null) {
            return UNKNOWN;
        }
        for (VideoTrackingEvent videoTrackingEvent : values()) {
            if (str.equals(videoTrackingEvent.getName())) {
                return videoTrackingEvent;
            }
        }
        return UNKNOWN;
    }
}
