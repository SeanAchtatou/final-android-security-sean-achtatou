package com.mopub.mobileads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import java.util.Map;
import java.util.TreeMap;

public class AdRequestStatusMapping {
    @NonNull
    private final Map<String, AdRequestStatus> mAdUnitToAdRequestStatus = new TreeMap();

    private enum LoadingStatus {
        LOADING,
        LOADED,
        PLAYED
    }

    /* access modifiers changed from: package-private */
    public void markFail(@NonNull String str) {
        this.mAdUnitToAdRequestStatus.remove(str);
    }

    /* access modifiers changed from: package-private */
    public void markLoading(@NonNull String str) {
        this.mAdUnitToAdRequestStatus.put(str, new AdRequestStatus(LoadingStatus.LOADING));
    }

    /* access modifiers changed from: package-private */
    public void markLoaded(@NonNull String str, @Nullable String str2, @Nullable String str3, @Nullable String str4) {
        this.mAdUnitToAdRequestStatus.put(str, new AdRequestStatus(LoadingStatus.LOADED, str2, str3, str4));
    }

    /* access modifiers changed from: package-private */
    public void markPlayed(@NonNull String str) {
        if (this.mAdUnitToAdRequestStatus.containsKey(str)) {
            this.mAdUnitToAdRequestStatus.get(str).setStatus(LoadingStatus.PLAYED);
        } else {
            this.mAdUnitToAdRequestStatus.put(str, new AdRequestStatus(LoadingStatus.PLAYED));
        }
    }

    /* access modifiers changed from: package-private */
    public boolean canPlay(@NonNull String str) {
        AdRequestStatus adRequestStatus = this.mAdUnitToAdRequestStatus.get(str);
        return adRequestStatus != null && LoadingStatus.LOADED.equals(adRequestStatus.getStatus());
    }

    /* access modifiers changed from: package-private */
    public boolean isLoading(@NonNull String str) {
        if (this.mAdUnitToAdRequestStatus.containsKey(str) && this.mAdUnitToAdRequestStatus.get(str).getStatus() == LoadingStatus.LOADING) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String getFailoverUrl(@NonNull String str) {
        if (!this.mAdUnitToAdRequestStatus.containsKey(str)) {
            return null;
        }
        return this.mAdUnitToAdRequestStatus.get(str).getFailurl();
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String getImpressionTrackerUrlString(@NonNull String str) {
        if (!this.mAdUnitToAdRequestStatus.containsKey(str)) {
            return null;
        }
        return this.mAdUnitToAdRequestStatus.get(str).getImpressionUrl();
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String getClickTrackerUrlString(@NonNull String str) {
        if (!this.mAdUnitToAdRequestStatus.containsKey(str)) {
            return null;
        }
        return this.mAdUnitToAdRequestStatus.get(str).getClickUrl();
    }

    /* access modifiers changed from: package-private */
    public void clearImpressionUrl(@NonNull String str) {
        if (this.mAdUnitToAdRequestStatus.containsKey(str)) {
            this.mAdUnitToAdRequestStatus.get(str).setImpressionUrl(null);
        }
    }

    /* access modifiers changed from: package-private */
    public void clearClickUrl(@NonNull String str) {
        if (this.mAdUnitToAdRequestStatus.containsKey(str)) {
            this.mAdUnitToAdRequestStatus.get(str).setClickUrl(null);
        }
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public void clearMapping() {
        this.mAdUnitToAdRequestStatus.clear();
    }

    private static class AdRequestStatus {
        @Nullable
        private String mClickUrl;
        @Nullable
        private String mFailUrl;
        @Nullable
        private String mImpressionUrl;
        @NonNull
        private LoadingStatus mLoadingStatus;

        public AdRequestStatus(@NonNull LoadingStatus loadingStatus) {
            this(loadingStatus, null, null, null);
        }

        public AdRequestStatus(@NonNull LoadingStatus loadingStatus, @Nullable String str, @Nullable String str2, @Nullable String str3) {
            Preconditions.checkNotNull(loadingStatus);
            this.mLoadingStatus = loadingStatus;
            this.mFailUrl = str;
            this.mImpressionUrl = str2;
            this.mClickUrl = str3;
        }

        /* access modifiers changed from: private */
        @NonNull
        public LoadingStatus getStatus() {
            return this.mLoadingStatus;
        }

        /* access modifiers changed from: private */
        public void setStatus(@NonNull LoadingStatus loadingStatus) {
            this.mLoadingStatus = loadingStatus;
        }

        /* access modifiers changed from: private */
        @Nullable
        public String getFailurl() {
            return this.mFailUrl;
        }

        /* access modifiers changed from: private */
        @Nullable
        public String getImpressionUrl() {
            return this.mImpressionUrl;
        }

        /* access modifiers changed from: private */
        public void setImpressionUrl(@Nullable String str) {
            this.mImpressionUrl = str;
        }

        /* access modifiers changed from: private */
        @Nullable
        public String getClickUrl() {
            return this.mClickUrl;
        }

        /* access modifiers changed from: private */
        public void setClickUrl(@Nullable String str) {
            this.mClickUrl = str;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AdRequestStatus)) {
                return false;
            }
            AdRequestStatus adRequestStatus = (AdRequestStatus) obj;
            if (!this.mLoadingStatus.equals(adRequestStatus.mLoadingStatus) || !TextUtils.equals(this.mFailUrl, adRequestStatus.mFailUrl) || !TextUtils.equals(this.mImpressionUrl, adRequestStatus.mImpressionUrl) || !TextUtils.equals(this.mClickUrl, adRequestStatus.mClickUrl)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int i = 0;
            int ordinal = 31 * (((((899 + this.mLoadingStatus.ordinal()) * 31) + (this.mFailUrl != null ? this.mFailUrl.hashCode() : 0)) * 31) + (this.mImpressionUrl != null ? this.mImpressionUrl.hashCode() : 0));
            if (this.mClickUrl != null) {
                i = this.mClickUrl.hashCode();
            }
            return ordinal + i;
        }
    }
}
