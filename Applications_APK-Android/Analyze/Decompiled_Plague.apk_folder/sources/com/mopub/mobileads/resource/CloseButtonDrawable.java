package com.mopub.mobileads.resource;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.mopub.mobileads.resource.DrawableConstants;

public class CloseButtonDrawable extends BaseWidgetDrawable {
    private final Paint closeButtonPaint;
    private final float halfStrokeWidth;

    public CloseButtonDrawable() {
        this(8.0f);
    }

    public CloseButtonDrawable(float f) {
        this.halfStrokeWidth = f / 2.0f;
        this.closeButtonPaint = new Paint();
        this.closeButtonPaint.setColor(-1);
        this.closeButtonPaint.setStrokeWidth(f);
        this.closeButtonPaint.setStrokeCap(DrawableConstants.CloseButton.STROKE_CAP);
    }

    public void draw(Canvas canvas) {
        int width = getBounds().width();
        float height = (float) getBounds().height();
        float f = (float) width;
        Canvas canvas2 = canvas;
        canvas2.drawLine(0.0f + this.halfStrokeWidth, height - this.halfStrokeWidth, f - this.halfStrokeWidth, 0.0f + this.halfStrokeWidth, this.closeButtonPaint);
        canvas2.drawLine(0.0f + this.halfStrokeWidth, 0.0f + this.halfStrokeWidth, f - this.halfStrokeWidth, height - this.halfStrokeWidth, this.closeButtonPaint);
    }
}
