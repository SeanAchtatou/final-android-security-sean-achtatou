package com.mopub.mobileads;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.adcolony.sdk.AdColonyAppOptions;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.mopub.common.BaseLifecycleListener;
import com.mopub.common.LifecycleListener;
import com.mopub.common.MediationSettings;
import com.mopub.common.MoPubReward;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class GooglePlayServicesRewardedVideo extends CustomEventRewardedVideo implements RewardedVideoAdListener {
    private static final String ADAPTER_VERSION = "0.1.0";
    private static final String KEY_EXTRA_AD_UNIT_ID = "adunit";
    private static final String KEY_EXTRA_APPLICATION_ID = "appid";
    private static final String TAG = "MoPubToAdMobRewarded";
    private static AtomicBoolean sIsInitialized;
    private boolean isAdLoaded;
    /* access modifiers changed from: private */
    public String mAdUnitId;
    private LifecycleListener mLifecycleListener = new BaseLifecycleListener() {
        public void onPause(@NonNull Activity activity) {
            super.onPause(activity);
            if (GooglePlayServicesRewardedVideo.this.mRewardedVideoAd != null) {
                GooglePlayServicesRewardedVideo.this.mRewardedVideoAd.pause(activity);
            }
        }

        public void onResume(@NonNull Activity activity) {
            super.onResume(activity);
            if (GooglePlayServicesRewardedVideo.this.mRewardedVideoAd != null) {
                GooglePlayServicesRewardedVideo.this.mRewardedVideoAd.resume(activity);
            }
        }
    };
    /* access modifiers changed from: private */
    public RewardedVideoAd mRewardedVideoAd;

    public void onRewardedVideoAdOpened() {
    }

    public void onRewardedVideoCompleted() {
    }

    public GooglePlayServicesRewardedVideo() {
        sIsInitialized = new AtomicBoolean(false);
    }

    /* access modifiers changed from: protected */
    @Nullable
    public LifecycleListener getLifecycleListener() {
        return this.mLifecycleListener;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public String getAdNetworkId() {
        return this.mAdUnitId;
    }

    /* access modifiers changed from: protected */
    public void onInvalidate() {
        if (this.mRewardedVideoAd != null) {
            this.mRewardedVideoAd.setRewardedVideoAdListener(null);
            this.mRewardedVideoAd = null;
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkAndInitializeSdk(@NonNull Activity activity, @NonNull Map<String, Object> map, @NonNull Map<String, String> map2) throws Exception {
        if (sIsInitialized.getAndSet(true)) {
            return false;
        }
        Log.i(TAG, "Adapter version - 0.1.0");
        if (TextUtils.isEmpty(map2.get(KEY_EXTRA_APPLICATION_ID))) {
            MobileAds.initialize(activity);
        } else {
            MobileAds.initialize(activity, map2.get(KEY_EXTRA_APPLICATION_ID));
        }
        if (TextUtils.isEmpty(map2.get(KEY_EXTRA_AD_UNIT_ID))) {
            MoPubRewardedVideoManager.onRewardedVideoLoadFailure(GooglePlayServicesRewardedVideo.class, GooglePlayServicesRewardedVideo.class.getSimpleName(), MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
            return false;
        }
        this.mAdUnitId = map2.get(KEY_EXTRA_AD_UNIT_ID);
        this.mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(activity);
        this.mRewardedVideoAd.setRewardedVideoAdListener(this);
        return true;
    }

    /* access modifiers changed from: protected */
    public void loadWithSdkInitialized(@NonNull Activity activity, @NonNull Map<String, Object> map, @NonNull Map<String, String> map2) throws Exception {
        this.isAdLoaded = false;
        if (TextUtils.isEmpty(map2.get(KEY_EXTRA_AD_UNIT_ID))) {
            MoPubRewardedVideoManager.onRewardedVideoLoadFailure(GooglePlayServicesRewardedVideo.class, GooglePlayServicesRewardedVideo.class.getSimpleName(), MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
            return;
        }
        this.mAdUnitId = map2.get(KEY_EXTRA_AD_UNIT_ID);
        if (this.mRewardedVideoAd == null) {
            this.mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(activity);
            this.mRewardedVideoAd.setRewardedVideoAdListener(this);
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (GooglePlayServicesRewardedVideo.this.mRewardedVideoAd.isLoaded()) {
                    MoPubRewardedVideoManager.onRewardedVideoLoadSuccess(GooglePlayServicesRewardedVideo.class, GooglePlayServicesRewardedVideo.this.mAdUnitId);
                    return;
                }
                AdRequest.Builder builder = new AdRequest.Builder();
                builder.setRequestAgent(AdColonyAppOptions.MOPUB);
                GooglePlayServicesRewardedVideo.this.forwardNpaIfSet(builder);
                GooglePlayServicesRewardedVideo.this.mRewardedVideoAd.loadAd(GooglePlayServicesRewardedVideo.this.mAdUnitId, builder.build());
            }
        });
    }

    /* access modifiers changed from: private */
    public void forwardNpaIfSet(AdRequest.Builder builder) {
        if (GooglePlayServicesMediationSettings.getNpaBundle() != null && !GooglePlayServicesMediationSettings.getNpaBundle().isEmpty()) {
            builder.addNetworkExtrasBundle(AdMobAdapter.class, GooglePlayServicesMediationSettings.getNpaBundle());
        }
    }

    /* access modifiers changed from: protected */
    public boolean hasVideoAvailable() {
        return this.mRewardedVideoAd != null && this.isAdLoaded;
    }

    /* access modifiers changed from: protected */
    public void showVideo() {
        if (hasVideoAvailable()) {
            this.mRewardedVideoAd.show();
        } else {
            MoPubRewardedVideoManager.onRewardedVideoPlaybackError(GooglePlayServicesRewardedVideo.class, this.mAdUnitId, getMoPubErrorCode(0));
        }
    }

    public void onRewardedVideoAdLoaded() {
        MoPubRewardedVideoManager.onRewardedVideoLoadSuccess(GooglePlayServicesRewardedVideo.class, this.mAdUnitId);
        this.isAdLoaded = true;
    }

    public void onRewardedVideoStarted() {
        MoPubRewardedVideoManager.onRewardedVideoStarted(GooglePlayServicesRewardedVideo.class, this.mAdUnitId);
    }

    public void onRewardedVideoAdClosed() {
        MoPubRewardedVideoManager.onRewardedVideoClosed(GooglePlayServicesRewardedVideo.class, this.mAdUnitId);
    }

    public void onRewarded(RewardItem rewardItem) {
        MoPubRewardedVideoManager.onRewardedVideoCompleted(GooglePlayServicesRewardedVideo.class, this.mAdUnitId, MoPubReward.success(rewardItem.getType(), rewardItem.getAmount()));
    }

    public void onRewardedVideoAdLeftApplication() {
        MoPubRewardedVideoManager.onRewardedVideoClicked(GooglePlayServicesRewardedVideo.class, this.mAdUnitId);
    }

    public void onRewardedVideoAdFailedToLoad(int i) {
        MoPubRewardedVideoManager.onRewardedVideoLoadFailure(GooglePlayServicesRewardedVideo.class, this.mAdUnitId, getMoPubErrorCode(i));
    }

    private MoPubErrorCode getMoPubErrorCode(int i) {
        switch (i) {
            case 0:
                return MoPubErrorCode.INTERNAL_ERROR;
            case 1:
                return MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR;
            case 2:
                return MoPubErrorCode.NO_CONNECTION;
            case 3:
                return MoPubErrorCode.NO_FILL;
            default:
                return MoPubErrorCode.UNSPECIFIED;
        }
    }

    public static final class GooglePlayServicesMediationSettings implements MediationSettings {
        private static Bundle npaBundle;

        public GooglePlayServicesMediationSettings() {
        }

        public GooglePlayServicesMediationSettings(Bundle bundle) {
            npaBundle = bundle;
        }

        public void setNpaBundle(Bundle bundle) {
            npaBundle = bundle;
        }

        /* access modifiers changed from: private */
        public static Bundle getNpaBundle() {
            return npaBundle;
        }
    }
}
