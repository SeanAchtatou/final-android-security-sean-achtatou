package com.mopub.mobileads;

import android.support.annotation.NonNull;
import com.millennialmedia.mediation.CustomEventNative;
import com.mopub.common.Preconditions;

public enum VastErrorCode {
    XML_PARSING_ERROR(CustomEventNative.DEFAULT_TYPE),
    WRAPPER_TIMEOUT("301"),
    NO_ADS_VAST_RESPONSE("303"),
    GENERAL_LINEAR_AD_ERROR("400"),
    GENERAL_COMPANION_AD_ERROR("600"),
    UNDEFINED_ERROR("900");
    
    @NonNull
    private final String mErrorCode;

    private VastErrorCode(@NonNull String str) {
        Preconditions.checkNotNull(str, "errorCode cannot be null");
        this.mErrorCode = str;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public String getErrorCode() {
        return this.mErrorCode;
    }
}
