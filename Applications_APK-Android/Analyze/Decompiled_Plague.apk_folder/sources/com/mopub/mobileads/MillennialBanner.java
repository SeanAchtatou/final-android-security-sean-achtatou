package com.mopub.mobileads;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.widget.FrameLayout;
import com.integralads.avid.library.mopub.BuildConfig;
import com.millennialmedia.AppInfo;
import com.millennialmedia.CreativeInfo;
import com.millennialmedia.InlineAd;
import com.millennialmedia.MMException;
import com.millennialmedia.MMLog;
import com.millennialmedia.MMSDK;
import com.millennialmedia.internal.ActivityListenerManager;
import com.mopub.common.MoPub;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.ConsentStatus;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.mobileads.CustomEventBanner;
import com.tapjoy.TapjoyAuctionFlags;
import java.util.Map;

final class MillennialBanner extends CustomEventBanner {
    private static final String AD_HEIGHT_KEY = "adHeight";
    private static final String AD_WIDTH_KEY = "adWidth";
    private static final String APID_KEY = "adUnitID";
    private static final String DCN_KEY = "dcn";
    /* access modifiers changed from: private */
    public static final String TAG = "MillennialBanner";
    /* access modifiers changed from: private */
    public CustomEventBanner.CustomEventBannerListener bannerListener;
    private InlineAd inlineAd;
    /* access modifiers changed from: private */
    public FrameLayout internalView;

    MillennialBanner() {
    }

    static {
        MoPubLog.d("Millennial Media Adapter Version: MoPubMM-1.3.0");
    }

    /* access modifiers changed from: private */
    public CreativeInfo getCreativeInfo() {
        if (this.inlineAd == null) {
            return null;
        }
        return this.inlineAd.getCreativeInfo();
    }

    /* access modifiers changed from: protected */
    public void loadBanner(Context context, CustomEventBanner.CustomEventBannerListener customEventBannerListener, Map<String, Object> map, Map<String, String> map2) {
        PersonalInfoManager personalInformationManager = MoPub.getPersonalInformationManager();
        if (personalInformationManager != null) {
            try {
                Boolean gdprApplies = personalInformationManager.gdprApplies();
                if (gdprApplies != null) {
                    MMSDK.setConsentRequired(gdprApplies.booleanValue());
                }
            } catch (NullPointerException e) {
                MoPubLog.d("GDPR applicability cannot be determined.", e);
            }
            if (personalInformationManager.getPersonalInfoConsentStatus() == ConsentStatus.EXPLICIT_YES) {
                MMSDK.setConsentData(BuildConfig.SDK_NAME, TapjoyAuctionFlags.AUCTION_TYPE_FIRST_PRICE);
            }
        }
        if (context instanceof Activity) {
            try {
                MMSDK.initialize((Activity) context, ActivityListenerManager.LifecycleState.RESUMED);
            } catch (IllegalStateException e2) {
                MoPubLog.d("Exception occurred initializing the MM SDK.", e2);
                this.bannerListener.onBannerFailed(MoPubErrorCode.INTERNAL_ERROR);
                return;
            }
        } else if (context instanceof Application) {
            try {
                MMSDK.initialize((Application) context);
            } catch (MMException e3) {
                MoPubLog.d("Exception occurred initializing the MM SDK.", e3);
                this.bannerListener.onBannerFailed(MoPubErrorCode.INTERNAL_ERROR);
                return;
            }
        } else {
            MoPubLog.d("MM SDK must be initialized with an Activity or Application context.");
            this.bannerListener.onBannerFailed(MoPubErrorCode.INTERNAL_ERROR);
            return;
        }
        this.bannerListener = customEventBannerListener;
        String str = map2.get("adUnitID");
        int parseInt = Integer.parseInt(map2.get("adWidth"));
        int parseInt2 = Integer.parseInt(map2.get("adHeight"));
        if (MillennialUtils.isEmpty(str) || parseInt < 0 || parseInt2 < 0) {
            MoPubLog.d("We were given invalid extras! Make sure placement ID, width, and height are specified.");
            this.bannerListener.onBannerFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
            return;
        }
        String str2 = map2.get(DCN_KEY);
        AppInfo mediator = new AppInfo().setMediator(MillennialUtils.MEDIATOR_ID);
        if (!MillennialUtils.isEmpty(str2)) {
            mediator.setSiteId(str2);
        }
        try {
            MMSDK.setAppInfo(mediator);
            this.internalView = new FrameLayout(context);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2);
            boolean z = true;
            layoutParams.gravity = 1;
            this.internalView.setLayoutParams(layoutParams);
            this.inlineAd = InlineAd.createInstance(str, this.internalView);
            InlineAd.InlineAdMetadata adSize = new InlineAd.InlineAdMetadata().setAdSize(new InlineAd.AdSize(parseInt, parseInt2));
            this.inlineAd.setListener(new MillennialInlineListener());
            if (MoPub.getLocationAwareness() == MoPub.LocationAwareness.DISABLED) {
                z = false;
            }
            MMSDK.setLocationEnabled(z);
            AdViewController.setShouldHonorServerDimensions(this.internalView);
            this.inlineAd.request(adSize);
        } catch (MMException e4) {
            MoPubLog.d("MM SDK exception occurred obtaining an inline ad unit.", e4);
            this.bannerListener.onBannerFailed(MoPubErrorCode.INTERNAL_ERROR);
        }
    }

    /* access modifiers changed from: protected */
    public void onInvalidate() {
        if (this.inlineAd != null) {
            this.inlineAd.destroy();
            this.inlineAd = null;
        }
    }

    class MillennialInlineListener implements InlineAd.InlineListener {
        MillennialInlineListener() {
        }

        public void onAdLeftApplication(InlineAd inlineAd) {
            MoPubLog.d("Millennial Inline Ad - Leaving application");
        }

        public void onClicked(InlineAd inlineAd) {
            MoPubLog.d("Millennial Inline Ad - Ad clicked");
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MillennialBanner.this.bannerListener.onBannerClicked();
                }
            });
        }

        public void onCollapsed(InlineAd inlineAd) {
            MoPubLog.d("Millennial Inline Ad - Banner collapsed");
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MillennialBanner.this.bannerListener.onBannerCollapsed();
                }
            });
        }

        public void onExpanded(InlineAd inlineAd) {
            MoPubLog.d("Millennial Inline Ad - Banner expanded");
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MillennialBanner.this.bannerListener.onBannerExpanded();
                }
            });
        }

        public void onRequestFailed(InlineAd inlineAd, InlineAd.InlineErrorStatus inlineErrorStatus) {
            final MoPubErrorCode moPubErrorCode;
            MoPubLog.d("Millennial Inline Ad - Banner failed (" + inlineErrorStatus.getErrorCode() + "): " + inlineErrorStatus.getDescription());
            int errorCode = inlineErrorStatus.getErrorCode();
            if (errorCode != 7) {
                switch (errorCode) {
                    case 1:
                        moPubErrorCode = MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR;
                        break;
                    case 2:
                        moPubErrorCode = MoPubErrorCode.NO_CONNECTION;
                        break;
                    case 3:
                        moPubErrorCode = MoPubErrorCode.WARMUP;
                        break;
                    case 4:
                        moPubErrorCode = MoPubErrorCode.INTERNAL_ERROR;
                        break;
                    default:
                        moPubErrorCode = MoPubErrorCode.NETWORK_NO_FILL;
                        break;
                }
            } else {
                moPubErrorCode = MoPubErrorCode.UNSPECIFIED;
            }
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MillennialBanner.this.bannerListener.onBannerFailed(moPubErrorCode);
                }
            });
        }

        public void onRequestSucceeded(InlineAd inlineAd) {
            MoPubLog.d("Millennial Inline Ad - Banner request succeeded");
            CreativeInfo access$100 = MillennialBanner.this.getCreativeInfo();
            if (access$100 != null && MMLog.isDebugEnabled()) {
                String access$200 = MillennialBanner.TAG;
                MMLog.d(access$200, "Banner Creative Info: " + access$100);
            }
            MillennialUtils.postOnUiThread(new Runnable() {
                public void run() {
                    MillennialBanner.this.bannerListener.onBannerLoaded(MillennialBanner.this.internalView);
                }
            });
        }

        public void onResize(InlineAd inlineAd, int i, int i2) {
            MoPubLog.d("Millennial Inline Ad - Banner about to resize (width: " + i + ", height: " + i2 + ")");
        }

        public void onResized(InlineAd inlineAd, int i, int i2, boolean z) {
            StringBuilder sb = new StringBuilder();
            sb.append("Millennial Inline Ad - Banner resized (width: ");
            sb.append(i);
            sb.append(", height: ");
            sb.append(i2);
            sb.append("). ");
            sb.append(z ? "Returned to original placement." : "Got a fresh, new place.");
            MoPubLog.d(sb.toString());
        }
    }
}
