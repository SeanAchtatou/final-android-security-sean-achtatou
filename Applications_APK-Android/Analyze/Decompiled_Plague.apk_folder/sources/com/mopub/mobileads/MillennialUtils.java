package com.mopub.mobileads;

import android.os.Handler;
import android.os.Looper;

public final class MillennialUtils {
    public static final String MEDIATOR_ID = "MoPubMM-1.3.0";
    private static final String VERSION = "1.3.0";
    private static final Handler handler = new Handler(Looper.getMainLooper());

    public static void postOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }

    public static boolean isEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }
}
