package com.mopub.volley.toolbox;

import android.os.SystemClock;
import com.mopub.volley.Cache;
import com.mopub.volley.Header;
import com.mopub.volley.Network;
import com.mopub.volley.Request;
import com.mopub.volley.RetryPolicy;
import com.mopub.volley.VolleyError;
import com.mopub.volley.VolleyLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

public class BasicNetwork implements Network {
    protected static final boolean DEBUG = VolleyLog.DEBUG;
    private static final int DEFAULT_POOL_SIZE = 4096;
    private static final int SLOW_REQUEST_THRESHOLD_MS = 3000;
    private final BaseHttpStack mBaseHttpStack;
    @Deprecated
    protected final HttpStack mHttpStack;
    protected final ByteArrayPool mPool;

    @Deprecated
    public BasicNetwork(HttpStack httpStack) {
        this(httpStack, new ByteArrayPool(4096));
    }

    @Deprecated
    public BasicNetwork(HttpStack httpStack, ByteArrayPool byteArrayPool) {
        this.mHttpStack = httpStack;
        this.mBaseHttpStack = new AdaptedHttpStack(httpStack);
        this.mPool = byteArrayPool;
    }

    public BasicNetwork(BaseHttpStack baseHttpStack) {
        this(baseHttpStack, new ByteArrayPool(4096));
    }

    public BasicNetwork(BaseHttpStack baseHttpStack, ByteArrayPool byteArrayPool) {
        this.mBaseHttpStack = baseHttpStack;
        this.mHttpStack = baseHttpStack;
        this.mPool = byteArrayPool;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mopub.volley.NetworkResponse.<init>(int, byte[], boolean, long, java.util.List<com.mopub.volley.Header>):void
     arg types: [int, ?[OBJECT, ARRAY], int, long, java.util.List<com.mopub.volley.Header>]
     candidates:
      com.mopub.volley.NetworkResponse.<init>(int, byte[], java.util.Map<java.lang.String, java.lang.String>, boolean, long):void
      com.mopub.volley.NetworkResponse.<init>(int, byte[], boolean, long, java.util.List<com.mopub.volley.Header>):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mopub.volley.NetworkResponse.<init>(int, byte[], boolean, long, java.util.List<com.mopub.volley.Header>):void
     arg types: [int, byte[], int, long, java.util.List<com.mopub.volley.Header>]
     candidates:
      com.mopub.volley.NetworkResponse.<init>(int, byte[], java.util.Map<java.lang.String, java.lang.String>, boolean, long):void
      com.mopub.volley.NetworkResponse.<init>(int, byte[], boolean, long, java.util.List<com.mopub.volley.Header>):void */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005e, code lost:
        r1 = r0;
        r15 = null;
        r19 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a3, code lost:
        r3 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a9, code lost:
        throw new java.io.IOException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00ac, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ad, code lost:
        r3 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00b4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b5, code lost:
        r1 = r0;
        r15 = null;
        r19 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00bb, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00bc, code lost:
        r19 = r1;
        r15 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c0, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c1, code lost:
        r19 = r1;
        r12 = null;
        r15 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c5, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00c8, code lost:
        r1 = r12.getStatusCode();
        com.mopub.volley.VolleyLog.e("Unexpected response code %d for %s", java.lang.Integer.valueOf(r1), r29.getUrl());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00e1, code lost:
        if (r15 != null) goto L_0x00e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00e3, code lost:
        r13 = new com.mopub.volley.NetworkResponse(r1, r15, false, android.os.SystemClock.elapsedRealtime() - r9, r19);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00f4, code lost:
        if (r1 == 401) goto L_0x012f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00fd, code lost:
        if (r1 < 400) goto L_0x0109;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0108, code lost:
        throw new com.mopub.volley.ClientError(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x010b, code lost:
        if (r1 < 500) goto L_0x0129;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0115, code lost:
        if (r29.shouldRetryServerErrors() != false) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0117, code lost:
        attemptRetryOnException("server", r8, new com.mopub.volley.ServerError(r13));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0128, code lost:
        throw new com.mopub.volley.ServerError(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x012f, code lost:
        attemptRetryOnException("auth", r8, new com.mopub.volley.AuthFailureError(r13));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x013b, code lost:
        attemptRetryOnException("network", r8, new com.mopub.volley.NetworkError());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x014c, code lost:
        throw new com.mopub.volley.NoConnectionError(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x014d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0169, code lost:
        throw new java.lang.RuntimeException("Bad URL " + r29.getUrl(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x016a, code lost:
        attemptRetryOnException("socket", r8, new com.mopub.volley.TimeoutError());
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x014d A[ExcHandler: MalformedURLException (r0v0 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:2:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:81:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), SYNTHETIC, Splitter:B:2:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0147 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.mopub.volley.NetworkResponse performRequest(com.mopub.volley.Request<?> r29) throws com.mopub.volley.VolleyError {
        /*
            r28 = this;
            r7 = r28
            r8 = r29
            long r9 = android.os.SystemClock.elapsedRealtime()
        L_0x0008:
            java.util.List r1 = java.util.Collections.emptyList()
            r11 = 0
            r2 = 0
            com.mopub.volley.Cache$Entry r3 = r29.getCacheEntry()     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00c0 }
            java.util.Map r3 = r7.getCacheHeaders(r3)     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00c0 }
            com.mopub.volley.toolbox.BaseHttpStack r4 = r7.mBaseHttpStack     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00c0 }
            com.mopub.volley.toolbox.HttpResponse r12 = r4.executeRequest(r8, r3)     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00c0 }
            int r14 = r12.getStatusCode()     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00bb }
            java.util.List r13 = r12.getHeaders()     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00bb }
            r1 = 304(0x130, float:4.26E-43)
            if (r14 != r1) goto L_0x0064
            com.mopub.volley.Cache$Entry r1 = r29.getCacheEntry()     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x005d }
            if (r1 != 0) goto L_0x0043
            com.mopub.volley.NetworkResponse r1 = new com.mopub.volley.NetworkResponse     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x005d }
            r16 = 304(0x130, float:4.26E-43)
            r17 = 0
            r18 = 1
            long r3 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x005d }
            long r19 = r3 - r9
            r15 = r1
            r21 = r13
            r15.<init>(r16, r17, r18, r19, r21)     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x005d }
            return r1
        L_0x0043:
            java.util.List r27 = combineHeaders(r13, r1)     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x005d }
            com.mopub.volley.NetworkResponse r3 = new com.mopub.volley.NetworkResponse     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x005d }
            r22 = 304(0x130, float:4.26E-43)
            byte[] r1 = r1.data     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x005d }
            r24 = 1
            long r4 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x005d }
            long r25 = r4 - r9
            r21 = r3
            r23 = r1
            r21.<init>(r22, r23, r24, r25, r27)     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x005d }
            return r3
        L_0x005d:
            r0 = move-exception
            r1 = r0
            r15 = r2
            r19 = r13
            goto L_0x00c6
        L_0x0064:
            java.io.InputStream r1 = r12.getContent()     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00b4 }
            if (r1 == 0) goto L_0x0073
            int r3 = r12.getContentLength()     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x005d }
            byte[] r1 = r7.inputStreamToBytes(r1, r3)     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x005d }
            goto L_0x0075
        L_0x0073:
            byte[] r1 = new byte[r11]     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00b4 }
        L_0x0075:
            r20 = r1
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00ac }
            long r3 = r1 - r9
            r1 = r7
            r2 = r3
            r4 = r8
            r5 = r20
            r6 = r14
            r1.logSlowRequests(r2, r4, r5, r6)     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00ac }
            r1 = 200(0xc8, float:2.8E-43)
            if (r14 < r1) goto L_0x00a3
            r1 = 299(0x12b, float:4.19E-43)
            if (r14 <= r1) goto L_0x008f
            goto L_0x00a3
        L_0x008f:
            com.mopub.volley.NetworkResponse r1 = new com.mopub.volley.NetworkResponse     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00ac }
            r16 = 0
            long r2 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00ac }
            long r17 = r2 - r9
            r3 = r13
            r13 = r1
            r15 = r20
            r19 = r3
            r13.<init>(r14, r15, r16, r17, r19)     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00aa }
            return r1
        L_0x00a3:
            r3 = r13
            java.io.IOException r1 = new java.io.IOException     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00aa }
            r1.<init>()     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00aa }
            throw r1     // Catch:{ SocketTimeoutException -> 0x016a, MalformedURLException -> 0x014d, IOException -> 0x00aa }
        L_0x00aa:
            r0 = move-exception
            goto L_0x00ae
        L_0x00ac:
            r0 = move-exception
            r3 = r13
        L_0x00ae:
            r1 = r0
            r19 = r3
            r15 = r20
            goto L_0x00c6
        L_0x00b4:
            r0 = move-exception
            r3 = r13
            r1 = r0
            r15 = r2
            r19 = r3
            goto L_0x00c6
        L_0x00bb:
            r0 = move-exception
            r19 = r1
            r15 = r2
            goto L_0x00c5
        L_0x00c0:
            r0 = move-exception
            r19 = r1
            r12 = r2
            r15 = r12
        L_0x00c5:
            r1 = r0
        L_0x00c6:
            if (r12 == 0) goto L_0x0147
            int r1 = r12.getStatusCode()
            java.lang.String r2 = "Unexpected response code %d for %s"
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)
            r3[r11] = r4
            r4 = 1
            java.lang.String r5 = r29.getUrl()
            r3[r4] = r5
            com.mopub.volley.VolleyLog.e(r2, r3)
            if (r15 == 0) goto L_0x013b
            com.mopub.volley.NetworkResponse r2 = new com.mopub.volley.NetworkResponse
            r16 = 0
            long r3 = android.os.SystemClock.elapsedRealtime()
            long r17 = r3 - r9
            r13 = r2
            r14 = r1
            r13.<init>(r14, r15, r16, r17, r19)
            r3 = 401(0x191, float:5.62E-43)
            if (r1 == r3) goto L_0x012f
            r3 = 403(0x193, float:5.65E-43)
            if (r1 != r3) goto L_0x00fb
            goto L_0x012f
        L_0x00fb:
            r3 = 400(0x190, float:5.6E-43)
            if (r1 < r3) goto L_0x0109
            r3 = 499(0x1f3, float:6.99E-43)
            if (r1 > r3) goto L_0x0109
            com.mopub.volley.ClientError r1 = new com.mopub.volley.ClientError
            r1.<init>(r2)
            throw r1
        L_0x0109:
            r3 = 500(0x1f4, float:7.0E-43)
            if (r1 < r3) goto L_0x0129
            r3 = 599(0x257, float:8.4E-43)
            if (r1 > r3) goto L_0x0129
            boolean r1 = r29.shouldRetryServerErrors()
            if (r1 == 0) goto L_0x0123
            java.lang.String r1 = "server"
            com.mopub.volley.ServerError r3 = new com.mopub.volley.ServerError
            r3.<init>(r2)
            attemptRetryOnException(r1, r8, r3)
            goto L_0x0008
        L_0x0123:
            com.mopub.volley.ServerError r1 = new com.mopub.volley.ServerError
            r1.<init>(r2)
            throw r1
        L_0x0129:
            com.mopub.volley.ServerError r1 = new com.mopub.volley.ServerError
            r1.<init>(r2)
            throw r1
        L_0x012f:
            java.lang.String r1 = "auth"
            com.mopub.volley.AuthFailureError r3 = new com.mopub.volley.AuthFailureError
            r3.<init>(r2)
            attemptRetryOnException(r1, r8, r3)
            goto L_0x0008
        L_0x013b:
            java.lang.String r1 = "network"
            com.mopub.volley.NetworkError r2 = new com.mopub.volley.NetworkError
            r2.<init>()
            attemptRetryOnException(r1, r8, r2)
            goto L_0x0008
        L_0x0147:
            com.mopub.volley.NoConnectionError r2 = new com.mopub.volley.NoConnectionError
            r2.<init>(r1)
            throw r2
        L_0x014d:
            r0 = move-exception
            r1 = r0
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Bad URL "
            r3.append(r4)
            java.lang.String r4 = r29.getUrl()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3, r1)
            throw r2
        L_0x016a:
            java.lang.String r1 = "socket"
            com.mopub.volley.TimeoutError r2 = new com.mopub.volley.TimeoutError
            r2.<init>()
            attemptRetryOnException(r1, r8, r2)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.volley.toolbox.BasicNetwork.performRequest(com.mopub.volley.Request):com.mopub.volley.NetworkResponse");
    }

    private void logSlowRequests(long j, Request<?> request, byte[] bArr, int i) {
        if (DEBUG || j > 3000) {
            Object[] objArr = new Object[5];
            objArr[0] = request;
            objArr[1] = Long.valueOf(j);
            objArr[2] = bArr != null ? Integer.valueOf(bArr.length) : "null";
            objArr[3] = Integer.valueOf(i);
            objArr[4] = Integer.valueOf(request.getRetryPolicy().getCurrentRetryCount());
            VolleyLog.d("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", objArr);
        }
    }

    private static void attemptRetryOnException(String str, Request<?> request, VolleyError volleyError) throws VolleyError {
        RetryPolicy retryPolicy = request.getRetryPolicy();
        int timeoutMs = request.getTimeoutMs();
        try {
            retryPolicy.retry(volleyError);
            request.addMarker(String.format("%s-retry [timeout=%s]", str, Integer.valueOf(timeoutMs)));
        } catch (VolleyError e) {
            request.addMarker(String.format("%s-timeout-giveup [timeout=%s]", str, Integer.valueOf(timeoutMs)));
            throw e;
        }
    }

    private Map<String, String> getCacheHeaders(Cache.Entry entry) {
        if (entry == null) {
            return Collections.emptyMap();
        }
        HashMap hashMap = new HashMap();
        if (entry.etag != null) {
            hashMap.put("If-None-Match", entry.etag);
        }
        if (entry.lastModified > 0) {
            hashMap.put("If-Modified-Since", HttpHeaderParser.formatEpochAsRfc1123(entry.lastModified));
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public void logError(String str, String str2, long j) {
        VolleyLog.v("HTTP ERROR(%s) %d ms to fetch %s", str, Long.valueOf(SystemClock.elapsedRealtime() - j), str2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0046 A[SYNTHETIC, Splitter:B:23:0x0046] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] inputStreamToBytes(java.io.InputStream r6, int r7) throws java.io.IOException, com.mopub.volley.ServerError {
        /*
            r5 = this;
            com.mopub.volley.toolbox.PoolingByteArrayOutputStream r0 = new com.mopub.volley.toolbox.PoolingByteArrayOutputStream
            com.mopub.volley.toolbox.ByteArrayPool r1 = r5.mPool
            r0.<init>(r1, r7)
            r7 = 0
            r1 = 0
            if (r6 != 0) goto L_0x0013
            com.mopub.volley.ServerError r2 = new com.mopub.volley.ServerError     // Catch:{ all -> 0x0011 }
            r2.<init>()     // Catch:{ all -> 0x0011 }
            throw r2     // Catch:{ all -> 0x0011 }
        L_0x0011:
            r2 = move-exception
            goto L_0x0044
        L_0x0013:
            com.mopub.volley.toolbox.ByteArrayPool r2 = r5.mPool     // Catch:{ all -> 0x0011 }
            r3 = 1024(0x400, float:1.435E-42)
            byte[] r2 = r2.getBuf(r3)     // Catch:{ all -> 0x0011 }
        L_0x001b:
            int r1 = r6.read(r2)     // Catch:{ all -> 0x0040 }
            r3 = -1
            if (r1 == r3) goto L_0x0026
            r0.write(r2, r7, r1)     // Catch:{ all -> 0x0040 }
            goto L_0x001b
        L_0x0026:
            byte[] r1 = r0.toByteArray()     // Catch:{ all -> 0x0040 }
            if (r6 == 0) goto L_0x0037
            r6.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x0037
        L_0x0030:
            java.lang.String r6 = "Error occurred when closing InputStream"
            java.lang.Object[] r7 = new java.lang.Object[r7]
            com.mopub.volley.VolleyLog.v(r6, r7)
        L_0x0037:
            com.mopub.volley.toolbox.ByteArrayPool r6 = r5.mPool
            r6.returnBuf(r2)
            r0.close()
            return r1
        L_0x0040:
            r1 = move-exception
            r4 = r2
            r2 = r1
            r1 = r4
        L_0x0044:
            if (r6 == 0) goto L_0x0051
            r6.close()     // Catch:{ IOException -> 0x004a }
            goto L_0x0051
        L_0x004a:
            java.lang.String r6 = "Error occurred when closing InputStream"
            java.lang.Object[] r7 = new java.lang.Object[r7]
            com.mopub.volley.VolleyLog.v(r6, r7)
        L_0x0051:
            com.mopub.volley.toolbox.ByteArrayPool r6 = r5.mPool
            r6.returnBuf(r1)
            r0.close()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.volley.toolbox.BasicNetwork.inputStreamToBytes(java.io.InputStream, int):byte[]");
    }

    @Deprecated
    protected static Map<String, String> convertHeaders(Header[] headerArr) {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (int i = 0; i < headerArr.length; i++) {
            treeMap.put(headerArr[i].getName(), headerArr[i].getValue());
        }
        return treeMap;
    }

    private static List<Header> combineHeaders(List<Header> list, Cache.Entry entry) {
        TreeSet treeSet = new TreeSet(String.CASE_INSENSITIVE_ORDER);
        if (!list.isEmpty()) {
            for (Header name : list) {
                treeSet.add(name.getName());
            }
        }
        ArrayList arrayList = new ArrayList(list);
        if (entry.allResponseHeaders != null) {
            if (!entry.allResponseHeaders.isEmpty()) {
                for (Header next : entry.allResponseHeaders) {
                    if (!treeSet.contains(next.getName())) {
                        arrayList.add(next);
                    }
                }
            }
        } else if (!entry.responseHeaders.isEmpty()) {
            for (Map.Entry next2 : entry.responseHeaders.entrySet()) {
                if (!treeSet.contains(next2.getKey())) {
                    arrayList.add(new Header((String) next2.getKey(), (String) next2.getValue()));
                }
            }
        }
        return arrayList;
    }
}
