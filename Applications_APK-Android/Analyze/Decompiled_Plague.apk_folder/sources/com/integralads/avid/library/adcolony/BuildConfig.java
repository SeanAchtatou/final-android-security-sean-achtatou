package com.integralads.avid.library.adcolony;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.integralads.avid.library.adcolony";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final String RELEASE_DATE = "29-Jun-17";
    public static final String SDK_NAME = "adcolony";
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "3.6.7";
}
