package com.integralads.avid.library.adcolony.session.internal;

public enum SessionType {
    DISPLAY("display"),
    VIDEO("video"),
    MANAGED_DISPLAY("managedDisplay"),
    MANAGED_VIDEO("managedVideo");
    
    private final String a;

    private SessionType(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
