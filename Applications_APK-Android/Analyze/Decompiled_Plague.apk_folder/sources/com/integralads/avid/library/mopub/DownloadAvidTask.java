package com.integralads.avid.library.mopub;

import android.os.AsyncTask;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

public class DownloadAvidTask extends AsyncTask<String, Void, String> {
    private static final int BSIZE = 1024;
    private DownloadAvidTaskListener listener;

    public interface DownloadAvidTaskListener {
        void failedToLoadAvid();

        void onLoadAvid(String str);
    }

    public DownloadAvidTaskListener getListener() {
        return this.listener;
    }

    public void setListener(DownloadAvidTaskListener downloadAvidTaskListener) {
        this.listener = downloadAvidTaskListener;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Missing exception handler attribute for start block: B:32:0x0076 */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0071 A[SYNTHETIC, Splitter:B:27:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0098 A[Catch:{ IOException -> 0x0048 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:32:0x0076=Splitter:B:32:0x0076, B:24:0x0057=Splitter:B:24:0x0057} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String doInBackground(java.lang.String... r8) {
        /*
            r7 = this;
            r0 = 0
            r8 = r8[r0]
            boolean r1 = android.text.TextUtils.isEmpty(r8)
            r2 = 0
            if (r1 == 0) goto L_0x0010
            java.lang.String r8 = "AvidLoader: URL is empty"
            com.integralads.avid.library.mopub.utils.AvidLogs.e(r8)
            return r2
        L_0x0010:
            java.net.URL r1 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0075, IOException -> 0x0055, all -> 0x0052 }
            r1.<init>(r8)     // Catch:{ MalformedURLException -> 0x0075, IOException -> 0x0055, all -> 0x0052 }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ MalformedURLException -> 0x0075, IOException -> 0x0055, all -> 0x0052 }
            r1.connect()     // Catch:{ MalformedURLException -> 0x0075, IOException -> 0x0055, all -> 0x0052 }
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ MalformedURLException -> 0x0075, IOException -> 0x0055, all -> 0x0052 }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ MalformedURLException -> 0x0075, IOException -> 0x0055, all -> 0x0052 }
            r3.<init>(r1)     // Catch:{ MalformedURLException -> 0x0075, IOException -> 0x0055, all -> 0x0052 }
            java.io.StringWriter r1 = new java.io.StringWriter     // Catch:{ MalformedURLException -> 0x0076, IOException -> 0x0050 }
            r1.<init>()     // Catch:{ MalformedURLException -> 0x0076, IOException -> 0x0050 }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ MalformedURLException -> 0x0076, IOException -> 0x0050 }
        L_0x002e:
            int r5 = r3.read(r4)     // Catch:{ MalformedURLException -> 0x0076, IOException -> 0x0050 }
            r6 = -1
            if (r5 == r6) goto L_0x003e
            java.lang.String r6 = new java.lang.String     // Catch:{ MalformedURLException -> 0x0076, IOException -> 0x0050 }
            r6.<init>(r4, r0, r5)     // Catch:{ MalformedURLException -> 0x0076, IOException -> 0x0050 }
            r1.write(r6)     // Catch:{ MalformedURLException -> 0x0076, IOException -> 0x0050 }
            goto L_0x002e
        L_0x003e:
            java.lang.String r0 = r1.toString()     // Catch:{ MalformedURLException -> 0x0076, IOException -> 0x0050 }
            if (r3 == 0) goto L_0x004f
            r3.close()     // Catch:{ IOException -> 0x0048 }
            goto L_0x004f
        L_0x0048:
            r8 = move-exception
            java.lang.String r0 = "AvidLoader: can not close Stream"
            com.integralads.avid.library.mopub.utils.AvidLogs.e(r0, r8)
            return r2
        L_0x004f:
            return r0
        L_0x0050:
            r8 = move-exception
            goto L_0x0057
        L_0x0052:
            r8 = move-exception
            r3 = r2
            goto L_0x0096
        L_0x0055:
            r8 = move-exception
            r3 = r2
        L_0x0057:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0095 }
            r0.<init>()     // Catch:{ all -> 0x0095 }
            java.lang.String r1 = "AvidLoader: IO error "
            r0.append(r1)     // Catch:{ all -> 0x0095 }
            java.lang.String r8 = r8.getLocalizedMessage()     // Catch:{ all -> 0x0095 }
            r0.append(r8)     // Catch:{ all -> 0x0095 }
            java.lang.String r8 = r0.toString()     // Catch:{ all -> 0x0095 }
            com.integralads.avid.library.mopub.utils.AvidLogs.e(r8)     // Catch:{ all -> 0x0095 }
            if (r3 == 0) goto L_0x0074
            r3.close()     // Catch:{ IOException -> 0x0048 }
        L_0x0074:
            return r2
        L_0x0075:
            r3 = r2
        L_0x0076:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0095 }
            r0.<init>()     // Catch:{ all -> 0x0095 }
            java.lang.String r1 = "AvidLoader: something is wrong with the URL '"
            r0.append(r1)     // Catch:{ all -> 0x0095 }
            r0.append(r8)     // Catch:{ all -> 0x0095 }
            java.lang.String r8 = "'"
            r0.append(r8)     // Catch:{ all -> 0x0095 }
            java.lang.String r8 = r0.toString()     // Catch:{ all -> 0x0095 }
            com.integralads.avid.library.mopub.utils.AvidLogs.e(r8)     // Catch:{ all -> 0x0095 }
            if (r3 == 0) goto L_0x0094
            r3.close()     // Catch:{ IOException -> 0x0048 }
        L_0x0094:
            return r2
        L_0x0095:
            r8 = move-exception
        L_0x0096:
            if (r3 == 0) goto L_0x009b
            r3.close()     // Catch:{ IOException -> 0x0048 }
        L_0x009b:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.integralads.avid.library.mopub.DownloadAvidTask.doInBackground(java.lang.String[]):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        if (this.listener == null) {
            return;
        }
        if (!TextUtils.isEmpty(str)) {
            this.listener.onLoadAvid(str);
        } else {
            this.listener.failedToLoadAvid();
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        if (this.listener != null) {
            this.listener.failedToLoadAvid();
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void invokeOnPostExecute(String str) {
        onPostExecute(str);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void invokeOnCancelled() {
        onCancelled();
    }
}
