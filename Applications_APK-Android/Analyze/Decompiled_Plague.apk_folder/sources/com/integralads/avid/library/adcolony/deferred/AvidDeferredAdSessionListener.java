package com.integralads.avid.library.adcolony.deferred;

public interface AvidDeferredAdSessionListener {
    void recordReadyEvent();
}
