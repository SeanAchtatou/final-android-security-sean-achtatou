package com.integralads.avid.library.mopub.utils;

import android.text.TextUtils;
import android.util.Log;

public class AvidLogs {
    private static final boolean DEBUG = true;
    private static final String TAG = "AVID";

    public static void d(String str) {
        if (!TextUtils.isEmpty(str)) {
            Log.d("AVID", str);
        }
    }

    public static void w(String str) {
        if (!TextUtils.isEmpty(str)) {
            Log.w("AVID", str);
        }
    }

    public static void i(String str) {
        if (!TextUtils.isEmpty(str)) {
            Log.i("AVID", str);
        }
    }

    public static void e(String str) {
        if (!TextUtils.isEmpty(str)) {
            Log.e("AVID", str);
        }
    }

    public static void e(String str, Exception exc) {
        if (!TextUtils.isEmpty(str) || exc != null) {
            Log.e("AVID", str, exc);
        }
    }
}
