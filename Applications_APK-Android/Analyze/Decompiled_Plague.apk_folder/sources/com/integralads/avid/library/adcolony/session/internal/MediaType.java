package com.integralads.avid.library.adcolony.session.internal;

public enum MediaType {
    DISPLAY("display"),
    VIDEO("video");
    
    private final String a;

    private MediaType(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
