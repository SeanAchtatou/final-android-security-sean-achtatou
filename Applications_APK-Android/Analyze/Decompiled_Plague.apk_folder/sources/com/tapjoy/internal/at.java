package com.tapjoy.internal;

import javax.annotation.Nullable;

public abstract class at implements aq {
    /* access modifiers changed from: protected */
    @Nullable
    public abstract ar a(Object obj, boolean z);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tapjoy.internal.at.a(java.lang.Object, boolean):com.tapjoy.internal.ar
     arg types: [java.lang.Object, int]
     candidates:
      com.tapjoy.internal.at.a(java.lang.Object, java.lang.Object):void
      com.tapjoy.internal.aq.a(java.lang.Object, java.lang.Object):void
      com.tapjoy.internal.at.a(java.lang.Object, boolean):com.tapjoy.internal.ar */
    public final Object a(Object obj) {
        ar a = a(obj, false);
        if (a != null) {
            return a.a();
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tapjoy.internal.at.a(java.lang.Object, boolean):com.tapjoy.internal.ar
     arg types: [java.lang.Object, int]
     candidates:
      com.tapjoy.internal.at.a(java.lang.Object, java.lang.Object):void
      com.tapjoy.internal.aq.a(java.lang.Object, java.lang.Object):void
      com.tapjoy.internal.at.a(java.lang.Object, boolean):com.tapjoy.internal.ar */
    public void a(Object obj, Object obj2) {
        a(obj, true).a(obj2);
    }
}
