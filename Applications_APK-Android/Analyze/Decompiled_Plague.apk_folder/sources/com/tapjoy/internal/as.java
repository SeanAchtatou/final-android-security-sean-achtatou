package com.tapjoy.internal;

public final class as {

    public static final class a implements av {
        private final at a;

        public a(at atVar) {
            this.a = atVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tapjoy.internal.at.a(java.lang.Object, boolean):com.tapjoy.internal.ar
         arg types: [java.lang.Object, int]
         candidates:
          com.tapjoy.internal.at.a(java.lang.Object, java.lang.Object):void
          com.tapjoy.internal.aq.a(java.lang.Object, java.lang.Object):void
          com.tapjoy.internal.at.a(java.lang.Object, boolean):com.tapjoy.internal.ar */
        public final Object a(Object obj) {
            ar a2;
            Object a3;
            synchronized (this.a) {
                a2 = this.a.a(obj, false);
            }
            if (a2 == null) {
                return null;
            }
            synchronized (a2) {
                a3 = a2.a();
            }
            return a3;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tapjoy.internal.at.a(java.lang.Object, boolean):com.tapjoy.internal.ar
         arg types: [java.lang.Object, int]
         candidates:
          com.tapjoy.internal.at.a(java.lang.Object, java.lang.Object):void
          com.tapjoy.internal.aq.a(java.lang.Object, java.lang.Object):void
          com.tapjoy.internal.at.a(java.lang.Object, boolean):com.tapjoy.internal.ar */
        public final void a(Object obj, Object obj2) {
            ar a2;
            synchronized (this.a) {
                a2 = this.a.a(obj, true);
            }
            synchronized (a2) {
                a2.a(obj2);
            }
        }
    }
}
