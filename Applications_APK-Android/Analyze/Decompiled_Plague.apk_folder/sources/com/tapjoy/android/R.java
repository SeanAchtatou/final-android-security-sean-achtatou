package com.tapjoy.android;

public final class R {

    public static final class attr {
        public static final int adSize = 2130837504;
        public static final int adSizes = 2130837505;
        public static final int adUnitId = 2130837506;
        public static final int buttonSize = 2130837507;
        public static final int circleCrop = 2130837508;
        public static final int colorScheme = 2130837509;
        public static final int imageAspectRatio = 2130837524;
        public static final int imageAspectRatioAdjust = 2130837525;
        public static final int scopeUris = 2130837533;
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2130968581;
        public static final int common_google_signin_btn_text_dark_default = 2130968582;
        public static final int common_google_signin_btn_text_dark_disabled = 2130968583;
        public static final int common_google_signin_btn_text_dark_focused = 2130968584;
        public static final int common_google_signin_btn_text_dark_pressed = 2130968585;
        public static final int common_google_signin_btn_text_light = 2130968586;
        public static final int common_google_signin_btn_text_light_default = 2130968587;
        public static final int common_google_signin_btn_text_light_disabled = 2130968588;
        public static final int common_google_signin_btn_text_light_focused = 2130968589;
        public static final int common_google_signin_btn_text_light_pressed = 2130968590;
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131099685;
        public static final int common_google_signin_btn_icon_dark = 2131099686;
        public static final int common_google_signin_btn_icon_dark_focused = 2131099687;
        public static final int common_google_signin_btn_icon_dark_normal = 2131099688;
        public static final int common_google_signin_btn_icon_light = 2131099691;
        public static final int common_google_signin_btn_icon_light_focused = 2131099692;
        public static final int common_google_signin_btn_icon_light_normal = 2131099693;
        public static final int common_google_signin_btn_text_dark = 2131099695;
        public static final int common_google_signin_btn_text_dark_focused = 2131099696;
        public static final int common_google_signin_btn_text_dark_normal = 2131099697;
        public static final int common_google_signin_btn_text_light = 2131099700;
        public static final int common_google_signin_btn_text_light_focused = 2131099701;
        public static final int common_google_signin_btn_text_light_normal = 2131099702;
    }

    public static final class id {
        public static final int adjust_height = 2131165193;
        public static final int adjust_width = 2131165194;
        public static final int auto = 2131165196;
        public static final int dark = 2131165221;
        public static final int icon_only = 2131165226;
        public static final int light = 2131165230;
        public static final int none = 2131165274;
        public static final int normal = 2131165275;
        public static final int standard = 2131165289;
        public static final int wide = 2131165297;
    }

    public static final class integer {
        public static final int google_play_services_version = 2131230721;
    }

    public static final class string {
        public static final int cancel = 2131427337;
        public static final int common_google_play_services_enable_button = 2131427365;
        public static final int common_google_play_services_enable_text = 2131427366;
        public static final int common_google_play_services_enable_title = 2131427367;
        public static final int common_google_play_services_install_button = 2131427368;
        public static final int common_google_play_services_install_title = 2131427370;
        public static final int common_google_play_services_notification_ticker = 2131427371;
        public static final int common_google_play_services_unknown_issue = 2131427372;
        public static final int common_google_play_services_unsupported_text = 2131427373;
        public static final int common_google_play_services_update_button = 2131427374;
        public static final int common_google_play_services_update_text = 2131427375;
        public static final int common_google_play_services_update_title = 2131427376;
        public static final int common_google_play_services_updating_text = 2131427377;
        public static final int common_open_on_phone = 2131427379;
        public static final int common_signin_button_text = 2131427380;
        public static final int common_signin_button_text_long = 2131427381;
        public static final int date_format_month_day = 2131427382;
        public static final int date_format_year_month_day = 2131427383;
        public static final int empty = 2131427388;
        public static final int failed_to_get_more = 2131427389;
        public static final int failed_to_load = 2131427390;
        public static final int failed_to_refresh = 2131427391;
        public static final int getting_more = 2131427392;
        public static final int just_before = 2131427398;
        public static final int loading = 2131427400;
        public static final int no = 2131427402;
        public static final int ok = 2131427403;
        public static final int please_wait = 2131427404;
        public static final int pull_down_to_load = 2131427406;
        public static final int pull_down_to_refresh = 2131427407;
        public static final int pull_up_to_get_more = 2131427408;
        public static final int refresh = 2131427410;
        public static final int release_to_get_more = 2131427411;
        public static final int release_to_load = 2131427412;
        public static final int release_to_refresh = 2131427413;
        public static final int search_hint = 2131427422;
        public static final int settings = 2131427424;
        public static final int sign_in = 2131427425;
        public static final int sign_out = 2131427426;
        public static final int sign_up = 2131427427;
        public static final int today = 2131427431;
        public static final int updating = 2131427433;
        public static final int yes = 2131427435;
        public static final int yesterday = 2131427436;
    }

    public static final class style {
        public static final int Theme_IAPTheme = 2131492877;
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.miniclip.plagueinc.R.attr.adSize, com.miniclip.plagueinc.R.attr.adSizes, com.miniclip.plagueinc.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] LoadingImageView = {com.miniclip.plagueinc.R.attr.circleCrop, com.miniclip.plagueinc.R.attr.imageAspectRatio, com.miniclip.plagueinc.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.miniclip.plagueinc.R.attr.buttonSize, com.miniclip.plagueinc.R.attr.colorScheme, com.miniclip.plagueinc.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
