package com.amazon.device.iap.internal.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.amazon.device.iap.PurchasingListener;
import com.amazon.device.iap.PurchasingService;
import com.amazon.device.iap.internal.d;
import com.amazon.device.iap.internal.model.ProductBuilder;
import com.amazon.device.iap.internal.model.ProductDataResponseBuilder;
import com.amazon.device.iap.internal.model.PurchaseResponseBuilder;
import com.amazon.device.iap.internal.model.PurchaseUpdatesResponseBuilder;
import com.amazon.device.iap.internal.model.ReceiptBuilder;
import com.amazon.device.iap.internal.model.UserDataBuilder;
import com.amazon.device.iap.internal.model.UserDataResponseBuilder;
import com.amazon.device.iap.internal.util.b;
import com.amazon.device.iap.internal.util.e;
import com.amazon.device.iap.model.FulfillmentResult;
import com.amazon.device.iap.model.Product;
import com.amazon.device.iap.model.ProductDataResponse;
import com.amazon.device.iap.model.ProductType;
import com.amazon.device.iap.model.PurchaseResponse;
import com.amazon.device.iap.model.PurchaseUpdatesResponse;
import com.amazon.device.iap.model.Receipt;
import com.amazon.device.iap.model.RequestId;
import com.amazon.device.iap.model.UserData;
import com.amazon.device.iap.model.UserDataResponse;
import com.google.android.gms.drive.DriveFile;
import com.mopub.mobileads.VastIconXmlManager;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: SandboxRequestHandler */
public final class c implements com.amazon.device.iap.internal.c {
    /* access modifiers changed from: private */
    public static final String a = "c";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amazon.device.iap.internal.a.c.a(java.lang.String, boolean, boolean):void
     arg types: [java.lang.String, int, int]
     candidates:
      com.amazon.device.iap.internal.a.c.a(java.lang.String, java.lang.String, boolean):void
      com.amazon.device.iap.internal.a.c.a(com.amazon.device.iap.model.RequestId, java.lang.String, com.amazon.device.iap.model.FulfillmentResult):void
      com.amazon.device.iap.internal.c.a(com.amazon.device.iap.model.RequestId, java.lang.String, com.amazon.device.iap.model.FulfillmentResult):void
      com.amazon.device.iap.internal.a.c.a(java.lang.String, boolean, boolean):void */
    public void a(RequestId requestId) {
        e.a(a, "sendGetUserDataRequest");
        a(requestId.toString(), false, false);
    }

    private void a(String str, boolean z, boolean z2) {
        try {
            Context b = d.d().b();
            Bundle bundle = new Bundle();
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("requestId", str);
            jSONObject.put("packageName", b.getPackageName());
            jSONObject.put("sdkVersion", PurchasingService.SDK_VERSION);
            jSONObject.put("isPurchaseUpdates", z);
            jSONObject.put("reset", z2);
            bundle.putString("userInput", jSONObject.toString());
            Intent a2 = a("com.amazon.testclient.iap.appUserId");
            a2.addFlags(DriveFile.MODE_READ_ONLY);
            a2.putExtras(bundle);
            b.startService(a2);
        } catch (JSONException unused) {
            e.b(a, "Error in sendGetUserDataRequest.");
        }
    }

    public void a(RequestId requestId, String str) {
        e.a(a, "sendPurchaseRequest");
        try {
            Context b = d.d().b();
            Bundle bundle = new Bundle();
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("sku", str);
            jSONObject.put("requestId", requestId.toString());
            jSONObject.put("packageName", b.getPackageName());
            jSONObject.put("sdkVersion", PurchasingService.SDK_VERSION);
            bundle.putString("purchaseInput", jSONObject.toString());
            Intent a2 = a("com.amazon.testclient.iap.purchase");
            a2.addFlags(DriveFile.MODE_READ_ONLY);
            a2.putExtras(bundle);
            b.startService(a2);
        } catch (JSONException unused) {
            e.b(a, "Error in sendPurchaseRequest.");
        }
    }

    public void a(RequestId requestId, Set<String> set) {
        e.a(a, "sendItemDataRequest");
        try {
            Context b = d.d().b();
            Bundle bundle = new Bundle();
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray = new JSONArray((Collection) set);
            jSONObject.put("requestId", requestId.toString());
            jSONObject.put("packageName", b.getPackageName());
            jSONObject.put("skus", jSONArray);
            jSONObject.put("sdkVersion", PurchasingService.SDK_VERSION);
            bundle.putString("itemDataInput", jSONObject.toString());
            Intent a2 = a("com.amazon.testclient.iap.itemData");
            a2.addFlags(DriveFile.MODE_READ_ONLY);
            a2.putExtras(bundle);
            b.startService(a2);
        } catch (JSONException unused) {
            e.b(a, "Error in sendItemDataRequest.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amazon.device.iap.internal.a.c.a(java.lang.String, boolean, boolean):void
     arg types: [java.lang.String, int, boolean]
     candidates:
      com.amazon.device.iap.internal.a.c.a(java.lang.String, java.lang.String, boolean):void
      com.amazon.device.iap.internal.a.c.a(com.amazon.device.iap.model.RequestId, java.lang.String, com.amazon.device.iap.model.FulfillmentResult):void
      com.amazon.device.iap.internal.c.a(com.amazon.device.iap.model.RequestId, java.lang.String, com.amazon.device.iap.model.FulfillmentResult):void
      com.amazon.device.iap.internal.a.c.a(java.lang.String, boolean, boolean):void */
    public void a(RequestId requestId, boolean z) {
        if (requestId == null) {
            requestId = new RequestId();
        }
        String str = a;
        e.a(str, "sendPurchaseUpdatesRequest/sendGetUserData first:" + requestId);
        a(requestId.toString(), true, z);
    }

    public void a(RequestId requestId, String str, FulfillmentResult fulfillmentResult) {
        e.a(a, "sendNotifyPurchaseFulfilled");
        try {
            Context b = d.d().b();
            Bundle bundle = new Bundle();
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("requestId", requestId.toString());
            jSONObject.put("packageName", b.getPackageName());
            jSONObject.put("receiptId", str);
            jSONObject.put("fulfillmentResult", fulfillmentResult);
            jSONObject.put("sdkVersion", PurchasingService.SDK_VERSION);
            bundle.putString("purchaseFulfilledInput", jSONObject.toString());
            Intent a2 = a("com.amazon.testclient.iap.purchaseFulfilled");
            a2.addFlags(DriveFile.MODE_READ_ONLY);
            a2.putExtras(bundle);
            b.startService(a2);
        } catch (JSONException unused) {
            e.b(a, "Error in sendNotifyPurchaseFulfilled.");
        }
    }

    public void a(Context context, Intent intent) {
        e.a(a, "handleResponse");
        intent.setComponent(new ComponentName("com.amazon.sdktestclient", "com.amazon.sdktestclient.command.CommandBroker"));
        try {
            String string = intent.getExtras().getString("responseType");
            if (string.equalsIgnoreCase("com.amazon.testclient.iap.purchase")) {
                g(intent);
            } else if (string.equalsIgnoreCase("com.amazon.testclient.iap.appUserId")) {
                e(intent);
            } else if (string.equalsIgnoreCase("com.amazon.testclient.iap.itemData")) {
                c(intent);
            } else if (string.equalsIgnoreCase("com.amazon.testclient.iap.purchaseUpdates")) {
                a(intent);
            }
        } catch (Exception e) {
            Log.e(a, "Error handling response.", e);
        }
    }

    private Intent a(String str) {
        Intent intent = new Intent(str);
        intent.setComponent(new ComponentName("com.amazon.sdktestclient", "com.amazon.sdktestclient.command.CommandBroker"));
        return intent;
    }

    /* access modifiers changed from: protected */
    public void a(final Object obj) {
        com.amazon.device.iap.internal.util.d.a(obj, "response");
        Context b = d.d().b();
        final PurchasingListener a2 = d.d().a();
        if (b == null || a2 == null) {
            String str = a;
            e.a(str, "PurchasingListener is not set. Dropping response: " + obj);
            return;
        }
        new Handler(b.getMainLooper()).post(new Runnable() {
            public void run() {
                try {
                    if (obj instanceof ProductDataResponse) {
                        a2.onProductDataResponse((ProductDataResponse) obj);
                    } else if (obj instanceof UserDataResponse) {
                        a2.onUserDataResponse((UserDataResponse) obj);
                    } else if (obj instanceof PurchaseUpdatesResponse) {
                        a2.onPurchaseUpdatesResponse((PurchaseUpdatesResponse) obj);
                    } else if (obj instanceof PurchaseResponse) {
                        a2.onPurchaseResponse((PurchaseResponse) obj);
                    } else {
                        String a2 = c.a;
                        e.b(a2, "Unknown response type:" + obj.getClass().getName());
                    }
                } catch (Exception e) {
                    String a3 = c.a;
                    e.b(a3, "Error in sendResponse: " + e);
                }
            }
        });
    }

    private void a(Intent intent) throws JSONException {
        PurchaseUpdatesResponse b = b(intent);
        if (b.getRequestStatus() == PurchaseUpdatesResponse.RequestStatus.SUCCESSFUL) {
            String optString = new JSONObject(intent.getStringExtra("purchaseUpdatesOutput")).optString(VastIconXmlManager.OFFSET);
            String str = a;
            Log.i(str, "Offset for PurchaseUpdatesResponse:" + optString);
            b.a(b.getUserData().getUserId(), optString);
        }
        a(b);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:22|23|24|25|26|44|27|20) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0069 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.amazon.device.iap.model.PurchaseUpdatesResponse b(android.content.Intent r12) {
        /*
            r11 = this;
            com.amazon.device.iap.model.PurchaseUpdatesResponse$RequestStatus r0 = com.amazon.device.iap.model.PurchaseUpdatesResponse.RequestStatus.FAILED
            r1 = 0
            r2 = 0
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x0098 }
            java.lang.String r4 = "purchaseUpdatesOutput"
            java.lang.String r12 = r12.getStringExtra(r4)     // Catch:{ Exception -> 0x0098 }
            r3.<init>(r12)     // Catch:{ Exception -> 0x0098 }
            java.lang.String r12 = "requestId"
            java.lang.String r12 = r3.optString(r12)     // Catch:{ Exception -> 0x0098 }
            com.amazon.device.iap.model.RequestId r12 = com.amazon.device.iap.model.RequestId.fromString(r12)     // Catch:{ Exception -> 0x0098 }
            java.lang.String r4 = "status"
            java.lang.String r4 = r3.optString(r4)     // Catch:{ Exception -> 0x0092 }
            com.amazon.device.iap.model.PurchaseUpdatesResponse$RequestStatus r4 = com.amazon.device.iap.model.PurchaseUpdatesResponse.RequestStatus.valueOf(r4)     // Catch:{ Exception -> 0x0092 }
            java.lang.String r0 = "isMore"
            boolean r0 = r3.optBoolean(r0)     // Catch:{ Exception -> 0x008c }
            java.lang.String r5 = "userId"
            java.lang.String r5 = r3.optString(r5)     // Catch:{ Exception -> 0x0089 }
            java.lang.String r6 = "marketplace"
            java.lang.String r6 = r3.optString(r6)     // Catch:{ Exception -> 0x0089 }
            com.amazon.device.iap.internal.model.UserDataBuilder r7 = new com.amazon.device.iap.internal.model.UserDataBuilder     // Catch:{ Exception -> 0x0089 }
            r7.<init>()     // Catch:{ Exception -> 0x0089 }
            com.amazon.device.iap.internal.model.UserDataBuilder r5 = r7.setUserId(r5)     // Catch:{ Exception -> 0x0089 }
            com.amazon.device.iap.internal.model.UserDataBuilder r5 = r5.setMarketplace(r6)     // Catch:{ Exception -> 0x0089 }
            com.amazon.device.iap.model.UserData r5 = r5.build()     // Catch:{ Exception -> 0x0089 }
            com.amazon.device.iap.model.PurchaseUpdatesResponse$RequestStatus r6 = com.amazon.device.iap.model.PurchaseUpdatesResponse.RequestStatus.SUCCESSFUL     // Catch:{ Exception -> 0x0087 }
            if (r4 != r6) goto L_0x00a5
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ Exception -> 0x0087 }
            r6.<init>()     // Catch:{ Exception -> 0x0087 }
            java.lang.String r2 = "receipts"
            org.json.JSONArray r2 = r3.optJSONArray(r2)     // Catch:{ Exception -> 0x0084 }
            if (r2 == 0) goto L_0x0082
        L_0x0057:
            int r3 = r2.length()     // Catch:{ Exception -> 0x0084 }
            if (r1 >= r3) goto L_0x0082
            org.json.JSONObject r3 = r2.optJSONObject(r1)     // Catch:{ Exception -> 0x0084 }
            com.amazon.device.iap.model.Receipt r7 = r11.a(r3)     // Catch:{ Exception -> 0x0069 }
            r6.add(r7)     // Catch:{ Exception -> 0x0069 }
            goto L_0x007f
        L_0x0069:
            java.lang.String r7 = com.amazon.device.iap.internal.a.c.a     // Catch:{ Exception -> 0x0084 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0084 }
            r8.<init>()     // Catch:{ Exception -> 0x0084 }
            java.lang.String r9 = "Failed to parse receipt from json:"
            r8.append(r9)     // Catch:{ Exception -> 0x0084 }
            r8.append(r3)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r3 = r8.toString()     // Catch:{ Exception -> 0x0084 }
            android.util.Log.e(r7, r3)     // Catch:{ Exception -> 0x0084 }
        L_0x007f:
            int r1 = r1 + 1
            goto L_0x0057
        L_0x0082:
            r2 = r6
            goto L_0x00a5
        L_0x0084:
            r1 = move-exception
            r2 = r6
            goto L_0x009e
        L_0x0087:
            r1 = move-exception
            goto L_0x009e
        L_0x0089:
            r1 = move-exception
            r5 = r2
            goto L_0x009e
        L_0x008c:
            r0 = move-exception
            r5 = r2
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x009e
        L_0x0092:
            r3 = move-exception
            r4 = r0
            r0 = r1
            r5 = r2
            r1 = r3
            goto L_0x009e
        L_0x0098:
            r12 = move-exception
            r4 = r0
            r0 = r1
            r5 = r2
            r1 = r12
            r12 = r5
        L_0x009e:
            java.lang.String r3 = com.amazon.device.iap.internal.a.c.a
            java.lang.String r6 = "Error parsing purchase updates output"
            android.util.Log.e(r3, r6, r1)
        L_0x00a5:
            com.amazon.device.iap.internal.model.PurchaseUpdatesResponseBuilder r1 = new com.amazon.device.iap.internal.model.PurchaseUpdatesResponseBuilder
            r1.<init>()
            com.amazon.device.iap.internal.model.PurchaseUpdatesResponseBuilder r12 = r1.setRequestId(r12)
            com.amazon.device.iap.internal.model.PurchaseUpdatesResponseBuilder r12 = r12.setRequestStatus(r4)
            com.amazon.device.iap.internal.model.PurchaseUpdatesResponseBuilder r12 = r12.setUserData(r5)
            com.amazon.device.iap.internal.model.PurchaseUpdatesResponseBuilder r12 = r12.setReceipts(r2)
            com.amazon.device.iap.internal.model.PurchaseUpdatesResponseBuilder r12 = r12.setHasMore(r0)
            com.amazon.device.iap.model.PurchaseUpdatesResponse r12 = r12.build()
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amazon.device.iap.internal.a.c.b(android.content.Intent):com.amazon.device.iap.model.PurchaseUpdatesResponse");
    }

    private void c(Intent intent) {
        a(d(intent));
    }

    private ProductDataResponse d(Intent intent) {
        RequestId requestId;
        ProductDataResponse.RequestStatus requestStatus;
        LinkedHashSet linkedHashSet;
        HashMap hashMap;
        Exception e;
        ProductDataResponse.RequestStatus requestStatus2 = ProductDataResponse.RequestStatus.FAILED;
        HashMap hashMap2 = null;
        try {
            JSONObject jSONObject = new JSONObject(intent.getStringExtra("itemDataOutput"));
            requestId = RequestId.fromString(jSONObject.optString("requestId"));
            try {
                requestStatus = ProductDataResponse.RequestStatus.valueOf(jSONObject.optString("status"));
                try {
                    if (requestStatus != ProductDataResponse.RequestStatus.FAILED) {
                        linkedHashSet = new LinkedHashSet();
                        try {
                            hashMap = new HashMap();
                        } catch (Exception e2) {
                            e = e2;
                            hashMap = null;
                            e = e;
                            Log.e(a, "Error parsing item data output", e);
                            hashMap2 = hashMap;
                            return new ProductDataResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setProductData(hashMap2).setUnavailableSkus(linkedHashSet).build();
                        }
                        try {
                            JSONArray optJSONArray = jSONObject.optJSONArray("unavailableSkus");
                            if (optJSONArray != null) {
                                for (int i = 0; i < optJSONArray.length(); i++) {
                                    linkedHashSet.add(optJSONArray.getString(i));
                                }
                            }
                            JSONObject optJSONObject = jSONObject.optJSONObject("items");
                            if (optJSONObject != null) {
                                Iterator<String> keys = optJSONObject.keys();
                                while (keys.hasNext()) {
                                    String next = keys.next();
                                    hashMap.put(next, a(next, optJSONObject.optJSONObject(next)));
                                }
                            }
                        } catch (Exception e3) {
                            e = e3;
                            Log.e(a, "Error parsing item data output", e);
                            hashMap2 = hashMap;
                            return new ProductDataResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setProductData(hashMap2).setUnavailableSkus(linkedHashSet).build();
                        }
                        hashMap2 = hashMap;
                        return new ProductDataResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setProductData(hashMap2).setUnavailableSkus(linkedHashSet).build();
                    }
                    linkedHashSet = null;
                    return new ProductDataResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setProductData(hashMap2).setUnavailableSkus(linkedHashSet).build();
                } catch (Exception e4) {
                    hashMap = null;
                    e = e4;
                    linkedHashSet = null;
                    Log.e(a, "Error parsing item data output", e);
                    hashMap2 = hashMap;
                    return new ProductDataResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setProductData(hashMap2).setUnavailableSkus(linkedHashSet).build();
                }
            } catch (Exception e5) {
                e = e5;
                requestStatus = requestStatus2;
                linkedHashSet = null;
                hashMap = null;
                e = e;
                Log.e(a, "Error parsing item data output", e);
                hashMap2 = hashMap;
                return new ProductDataResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setProductData(hashMap2).setUnavailableSkus(linkedHashSet).build();
            }
        } catch (Exception e6) {
            requestStatus = requestStatus2;
            linkedHashSet = null;
            hashMap = null;
            e = e6;
            requestId = null;
            Log.e(a, "Error parsing item data output", e);
            hashMap2 = hashMap;
            return new ProductDataResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setProductData(hashMap2).setUnavailableSkus(linkedHashSet).build();
        }
    }

    private Product a(String str, JSONObject jSONObject) throws JSONException {
        ProductType valueOf = ProductType.valueOf(jSONObject.optString("itemType"));
        JSONObject jSONObject2 = jSONObject.getJSONObject("priceJson");
        Currency instance = Currency.getInstance(jSONObject2.optString("currency"));
        String str2 = instance.getSymbol() + new BigDecimal(jSONObject2.optString("value"));
        return new ProductBuilder().setSku(str).setProductType(valueOf).setDescription(jSONObject.optString("description")).setPrice(str2).setSmallIconUrl(jSONObject.optString("smallIconUrl")).setTitle(jSONObject.optString("title")).build();
    }

    private void e(Intent intent) {
        JSONObject jSONObject;
        UserDataResponse f = f(intent);
        RequestId requestId = f.getRequestId();
        String stringExtra = intent.getStringExtra("userInput");
        try {
            jSONObject = new JSONObject(stringExtra);
        } catch (JSONException e) {
            String str = a;
            Log.e(str, "Unable to parse request data: " + stringExtra, e);
            jSONObject = null;
        }
        if (requestId == null || jSONObject == null) {
            a(f);
        } else if (!jSONObject.optBoolean("isPurchaseUpdates", false)) {
            a(f);
        } else if (f.getUserData() == null || com.amazon.device.iap.internal.util.d.a(f.getUserData().getUserId())) {
            String str2 = a;
            Log.e(str2, "No Userid found in userDataResponse" + f);
            a(new PurchaseUpdatesResponseBuilder().setRequestId(requestId).setRequestStatus(PurchaseUpdatesResponse.RequestStatus.FAILED).setUserData(f.getUserData()).setReceipts(new ArrayList()).setHasMore(false).build());
        } else {
            String str3 = a;
            Log.i(str3, "sendGetPurchaseUpdates with user id" + f.getUserData().getUserId());
            a(requestId.toString(), f.getUserData().getUserId(), jSONObject.optBoolean("reset", true));
        }
    }

    private void a(String str, String str2, boolean z) {
        try {
            Context b = d.d().b();
            String a2 = b.a(str2);
            String str3 = a;
            Log.i(str3, "send PurchaseUpdates with user id:" + str2 + ";reset flag:" + z + ", local cursor:" + a2 + ", parsed from old requestId:" + str);
            Bundle bundle = new Bundle();
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("requestId", str.toString());
            if (z) {
                a2 = null;
            }
            jSONObject.put(VastIconXmlManager.OFFSET, a2);
            jSONObject.put("sdkVersion", PurchasingService.SDK_VERSION);
            jSONObject.put("packageName", b.getPackageName());
            bundle.putString("purchaseUpdatesInput", jSONObject.toString());
            Intent a3 = a("com.amazon.testclient.iap.purchaseUpdates");
            a3.addFlags(DriveFile.MODE_READ_ONLY);
            a3.putExtras(bundle);
            b.startService(a3);
        } catch (JSONException unused) {
            e.b(a, "Error in sendPurchaseUpdatesRequest.");
        }
    }

    private UserDataResponse f(Intent intent) {
        RequestId requestId;
        UserDataResponse.RequestStatus requestStatus;
        Exception e;
        UserDataResponse.RequestStatus requestStatus2 = UserDataResponse.RequestStatus.FAILED;
        UserData userData = null;
        try {
            JSONObject jSONObject = new JSONObject(intent.getStringExtra("userOutput"));
            requestId = RequestId.fromString(jSONObject.optString("requestId"));
            try {
                requestStatus = UserDataResponse.RequestStatus.valueOf(jSONObject.optString("status"));
                try {
                    if (requestStatus == UserDataResponse.RequestStatus.SUCCESSFUL) {
                        userData = new UserDataBuilder().setUserId(jSONObject.optString("userId")).setMarketplace(jSONObject.optString("marketplace")).build();
                    }
                } catch (Exception e2) {
                    e = e2;
                    Log.e(a, "Error parsing userid output", e);
                    return new UserDataResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setUserData(userData).build();
                }
            } catch (Exception e3) {
                requestStatus = requestStatus2;
                e = e3;
                Log.e(a, "Error parsing userid output", e);
                return new UserDataResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setUserData(userData).build();
            }
        } catch (Exception e4) {
            requestStatus = requestStatus2;
            e = e4;
            requestId = null;
            Log.e(a, "Error parsing userid output", e);
            return new UserDataResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setUserData(userData).build();
        }
        return new UserDataResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setUserData(userData).build();
    }

    private void g(Intent intent) {
        a(h(intent));
    }

    private PurchaseResponse h(Intent intent) {
        RequestId requestId;
        PurchaseResponse.RequestStatus requestStatus;
        UserData userData;
        Exception e;
        PurchaseResponse.RequestStatus requestStatus2 = PurchaseResponse.RequestStatus.FAILED;
        Receipt receipt = null;
        try {
            JSONObject jSONObject = new JSONObject(intent.getStringExtra("purchaseOutput"));
            requestId = RequestId.fromString(jSONObject.optString("requestId"));
            try {
                userData = new UserDataBuilder().setUserId(jSONObject.optString("userId")).setMarketplace(jSONObject.optString("marketplace")).build();
                try {
                    requestStatus = PurchaseResponse.RequestStatus.safeValueOf(jSONObject.optString("purchaseStatus"));
                } catch (Exception e2) {
                    e = e2;
                    requestStatus = requestStatus2;
                    Log.e(a, "Error parsing purchase output", e);
                    return new PurchaseResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setUserData(userData).setReceipt(receipt).build();
                }
            } catch (Exception e3) {
                e = e3;
                requestStatus = requestStatus2;
                userData = null;
                Log.e(a, "Error parsing purchase output", e);
                return new PurchaseResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setUserData(userData).setReceipt(receipt).build();
            }
            try {
                JSONObject optJSONObject = jSONObject.optJSONObject("receipt");
                if (optJSONObject != null) {
                    receipt = a(optJSONObject);
                }
            } catch (Exception e4) {
                e = e4;
                Log.e(a, "Error parsing purchase output", e);
                return new PurchaseResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setUserData(userData).setReceipt(receipt).build();
            }
        } catch (Exception e5) {
            e = e5;
            requestStatus = requestStatus2;
            requestId = null;
            userData = null;
            Log.e(a, "Error parsing purchase output", e);
            return new PurchaseResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setUserData(userData).setReceipt(receipt).build();
        }
        return new PurchaseResponseBuilder().setRequestId(requestId).setRequestStatus(requestStatus).setUserData(userData).setReceipt(receipt).build();
    }

    private Receipt a(JSONObject jSONObject) throws ParseException {
        String optString = jSONObject.optString("receiptId");
        String optString2 = jSONObject.optString("sku");
        ProductType valueOf = ProductType.valueOf(jSONObject.optString("itemType"));
        Date parse = b.a.parse(jSONObject.optString("purchaseDate"));
        String optString3 = jSONObject.optString("cancelDate");
        return new ReceiptBuilder().setReceiptId(optString).setSku(optString2).setProductType(valueOf).setPurchaseDate(parse).setCancelDate((optString3 == null || optString3.length() == 0) ? null : b.a.parse(optString3)).build();
    }
}
