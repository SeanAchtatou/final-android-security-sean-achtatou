package com.amazon.device.iap.internal.b.e;

import android.os.RemoteException;
import com.amazon.android.framework.exception.KiwiException;
import com.amazon.device.iap.internal.b.e;
import com.amazon.device.iap.internal.model.UserDataBuilder;
import com.amazon.device.iap.internal.model.UserDataResponseBuilder;
import com.amazon.device.iap.internal.util.d;
import com.amazon.device.iap.model.UserData;
import com.amazon.device.iap.model.UserDataResponse;
import com.amazon.venezia.command.SuccessResult;
import java.util.Map;

/* compiled from: GetUserIdCommandV2 */
public final class c extends b {
    private static final String b = "c";

    public c(e eVar) {
        super(eVar, "2.0");
    }

    /* access modifiers changed from: protected */
    public boolean a(SuccessResult successResult) throws RemoteException, KiwiException {
        String str = b;
        com.amazon.device.iap.internal.util.e.a(str, "onResult: result = " + successResult);
        Map data = successResult.getData();
        String str2 = b;
        com.amazon.device.iap.internal.util.e.a(str2, "data: " + data);
        String str3 = (String) data.get("userId");
        String str4 = (String) data.get("marketplace");
        e b2 = b();
        if (d.a(str3) || d.a(str4)) {
            b2.d().a(new UserDataResponseBuilder().setRequestId(b2.c()).setRequestStatus(UserDataResponse.RequestStatus.FAILED).build());
            return false;
        }
        UserData build = new UserDataBuilder().setUserId(str3).setMarketplace(str4).build();
        UserDataResponse build2 = new UserDataResponseBuilder().setRequestId(b2.c()).setRequestStatus(UserDataResponse.RequestStatus.SUCCESSFUL).setUserData(build).build();
        b2.d().a("userId", build.getUserId());
        b2.d().a(build2);
        return true;
    }
}
