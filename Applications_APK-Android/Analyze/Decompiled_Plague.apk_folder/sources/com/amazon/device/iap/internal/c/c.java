package com.amazon.device.iap.internal.c;

import android.content.Context;
import android.content.SharedPreferences;
import com.amazon.device.iap.internal.util.d;
import com.amazon.device.iap.internal.util.e;

/* compiled from: EntitlementTracker */
public class c {
    private static c a = new c();
    private static final String b = "c";
    private static final String c = (c.class.getName() + "_PREFS_");

    public static c a() {
        return a;
    }

    public void a(String str, String str2, String str3) {
        String str4 = b;
        e.a(str4, "enter saveEntitlementRecord for v1 Entitlement [" + str2 + "/" + str3 + "], user [" + str + "]");
        try {
            d.a(str, "userId");
            d.a(str2, "receiptId");
            d.a(str3, "sku");
            Context b2 = com.amazon.device.iap.internal.d.d().b();
            d.a(b2, "context");
            SharedPreferences.Editor edit = b2.getSharedPreferences(c + str, 0).edit();
            edit.putString(str3, str2);
            edit.commit();
        } catch (Throwable th) {
            String str5 = b;
            e.a(str5, "error in saving v1 Entitlement:" + str2 + "/" + str3 + ":" + th.getMessage());
        }
        String str6 = b;
        e.a(str6, "leaving saveEntitlementRecord for v1 Entitlement [" + str2 + "/" + str3 + "], user [" + str + "]");
    }

    public String a(String str, String str2) {
        String str3 = b;
        e.a(str3, "enter getReceiptIdFromSku for sku [" + str2 + "], user [" + str + "]");
        String str4 = null;
        try {
            d.a(str, "userId");
            d.a(str2, "sku");
            Context b2 = com.amazon.device.iap.internal.d.d().b();
            d.a(b2, "context");
            str4 = b2.getSharedPreferences(c + str, 0).getString(str2, null);
        } catch (Throwable th) {
            String str5 = b;
            e.a(str5, "error in saving v1 Entitlement:" + str2 + ":" + th.getMessage());
        }
        String str6 = b;
        e.a(str6, "leaving saveEntitlementRecord for sku [" + str2 + "], user [" + str + "]");
        return str4;
    }
}
