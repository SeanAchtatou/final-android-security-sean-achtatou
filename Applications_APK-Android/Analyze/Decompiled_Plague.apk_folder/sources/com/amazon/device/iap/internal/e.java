package com.amazon.device.iap.internal;

import android.util.Log;
import com.amazon.device.iap.internal.a.d;
import com.amazon.device.iap.internal.b.g;

/* compiled from: ImplementationFactory */
public final class e {
    private static final String a = "com.amazon.device.iap.internal.e";
    private static volatile boolean b;
    private static volatile boolean c;
    private static volatile c d;
    private static volatile a e;
    private static volatile b f;

    private static b d() {
        if (f == null) {
            synchronized (e.class) {
                if (f == null) {
                    if (a()) {
                        f = new d();
                    } else {
                        f = new g();
                    }
                }
            }
        }
        return f;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(9:12|13|14|15|16|17|18|19|20) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0022 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a() {
        /*
            boolean r0 = com.amazon.device.iap.internal.e.c
            if (r0 == 0) goto L_0x0007
            boolean r0 = com.amazon.device.iap.internal.e.b
            return r0
        L_0x0007:
            java.lang.Class<com.amazon.device.iap.internal.e> r0 = com.amazon.device.iap.internal.e.class
            monitor-enter(r0)
            boolean r1 = com.amazon.device.iap.internal.e.c     // Catch:{ all -> 0x002a }
            if (r1 == 0) goto L_0x0012
            boolean r1 = com.amazon.device.iap.internal.e.b     // Catch:{ all -> 0x002a }
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return r1
        L_0x0012:
            r1 = 1
            java.lang.Class<com.amazon.device.iap.internal.e> r2 = com.amazon.device.iap.internal.e.class
            java.lang.ClassLoader r2 = r2.getClassLoader()     // Catch:{ Throwable -> 0x0022 }
            java.lang.String r3 = "com.amazon.android.Kiwi"
            r2.loadClass(r3)     // Catch:{ Throwable -> 0x0022 }
            r2 = 0
            com.amazon.device.iap.internal.e.b = r2     // Catch:{ Throwable -> 0x0022 }
            goto L_0x0024
        L_0x0022:
            com.amazon.device.iap.internal.e.b = r1     // Catch:{ all -> 0x002a }
        L_0x0024:
            com.amazon.device.iap.internal.e.c = r1     // Catch:{ all -> 0x002a }
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            boolean r0 = com.amazon.device.iap.internal.e.b
            return r0
        L_0x002a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amazon.device.iap.internal.e.a():boolean");
    }

    public static c b() {
        if (d == null) {
            synchronized (e.class) {
                if (d == null) {
                    d = (c) a(c.class);
                }
            }
        }
        return d;
    }

    public static a c() {
        if (e == null) {
            synchronized (e.class) {
                if (e == null) {
                    e = (a) a(a.class);
                }
            }
        }
        return e;
    }

    private static <T> T a(Class<T> cls) {
        try {
            return d().a(cls).newInstance();
        } catch (Exception e2) {
            String str = a;
            Log.e(str, "error getting instance for " + cls, e2);
            return null;
        }
    }
}
