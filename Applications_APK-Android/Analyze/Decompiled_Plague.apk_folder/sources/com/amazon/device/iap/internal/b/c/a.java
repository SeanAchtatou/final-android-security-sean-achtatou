package com.amazon.device.iap.internal.b.c;

import android.os.RemoteException;
import com.amazon.android.framework.exception.KiwiException;
import com.amazon.device.iap.internal.b.e;
import com.amazon.device.iap.internal.model.ProductBuilder;
import com.amazon.device.iap.internal.model.ProductDataResponseBuilder;
import com.amazon.device.iap.internal.util.MetricsHelper;
import com.amazon.device.iap.internal.util.d;
import com.amazon.device.iap.model.Product;
import com.amazon.device.iap.model.ProductDataResponse;
import com.amazon.device.iap.model.ProductType;
import com.amazon.venezia.command.SuccessResult;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: GetItemDataCommandV2 */
public final class a extends c {
    private static final String b = "a";

    public a(e eVar, Set<String> set) {
        super(eVar, "2.0", set);
    }

    /* access modifiers changed from: protected */
    public boolean a(SuccessResult successResult) throws RemoteException, KiwiException {
        Map data = successResult.getData();
        String str = b;
        com.amazon.device.iap.internal.util.e.a(str, "data: " + data);
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        HashMap hashMap = new HashMap();
        for (String str2 : this.a) {
            if (!data.containsKey(str2)) {
                linkedHashSet.add(str2);
            } else {
                try {
                    hashMap.put(str2, a(str2, data));
                } catch (IllegalArgumentException e) {
                    linkedHashSet.add(str2);
                    String c = c();
                    MetricsHelper.submitJsonParsingExceptionMetrics(c, (String) data.get(str2), b + ".onResult()");
                    String str3 = b;
                    com.amazon.device.iap.internal.util.e.b(str3, "Error parsing JSON for SKU " + str2 + ": " + e.getMessage());
                }
            }
        }
        e b2 = b();
        b2.d().a(new ProductDataResponseBuilder().setRequestId(b2.c()).setRequestStatus(ProductDataResponse.RequestStatus.SUCCESSFUL).setUnavailableSkus(linkedHashSet).setProductData(hashMap).build());
        return true;
    }

    private Product a(String str, Map map) throws IllegalArgumentException {
        JSONObject optJSONObject;
        String str2 = (String) map.get(str);
        try {
            JSONObject jSONObject = new JSONObject(str2);
            ProductType valueOf = ProductType.valueOf(jSONObject.getString("itemType").toUpperCase());
            String string = jSONObject.getString("description");
            String optString = jSONObject.optString("price", null);
            if (d.a(optString) && (optJSONObject = jSONObject.optJSONObject("priceJson")) != null) {
                Currency instance = Currency.getInstance(optJSONObject.getString("currency"));
                optString = instance.getSymbol() + new BigDecimal(optJSONObject.getString("value"));
            }
            return new ProductBuilder().setSku(str).setProductType(valueOf).setDescription(string).setPrice(optString).setSmallIconUrl(jSONObject.getString("iconUrl")).setTitle(jSONObject.getString("title")).build();
        } catch (JSONException unused) {
            throw new IllegalArgumentException("error in parsing json string" + str2);
        }
    }
}
