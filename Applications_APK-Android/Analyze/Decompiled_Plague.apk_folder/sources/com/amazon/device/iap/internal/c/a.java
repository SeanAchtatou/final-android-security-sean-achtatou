package com.amazon.device.iap.internal.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import com.amazon.device.iap.internal.util.d;
import com.amazon.device.iap.internal.util.e;
import com.amazon.device.iap.model.Receipt;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: PendingReceiptsManager */
public class a {
    /* access modifiers changed from: private */
    public static final String a = "a";
    /* access modifiers changed from: private */
    public static final String b = (a.class.getName() + "_PREFS");
    private static final String c = (a.class.getName() + "_CLEANER_PREFS");
    /* access modifiers changed from: private */
    public static int d = 604800000;
    private static final a e = new a();

    public void a(String str, String str2, String str3, String str4) {
        String str5 = a;
        e.a(str5, "enter saveReceipt for receipt [" + str4 + "]");
        try {
            d.a(str2, "userId");
            d.a(str3, "receiptId");
            d.a(str4, "receiptString");
            Context b2 = com.amazon.device.iap.internal.d.d().b();
            d.a(b2, "context");
            d dVar = new d(str2, str4, str, System.currentTimeMillis());
            SharedPreferences.Editor edit = b2.getSharedPreferences(b, 0).edit();
            edit.putString(str3, dVar.d());
            edit.commit();
        } catch (Throwable th) {
            String str6 = a;
            e.a(str6, "error in saving pending receipt:" + str + "/" + str4 + ":" + th.getMessage());
        }
        String str7 = a;
        e.a(str7, "leaving saveReceipt for receipt id [" + str3 + "]");
    }

    private void e() {
        e.a(a, "enter old receipts cleanup! ");
        final Context b2 = com.amazon.device.iap.internal.d.d().b();
        d.a(b2, "context");
        a(System.currentTimeMillis());
        new Handler().post(new Runnable() {
            /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
                r3 = com.amazon.device.iap.internal.c.a.b();
                com.amazon.device.iap.internal.util.e.a(r3, "house keeping - try remove Receipt:" + r2 + " since it's invalid ");
                r10.b.a(r2);
             */
            /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
            /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x006b */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r10 = this;
                    java.lang.String r0 = com.amazon.device.iap.internal.c.a.a     // Catch:{ Throwable -> 0x008e }
                    java.lang.String r1 = "perform house keeping! "
                    com.amazon.device.iap.internal.util.e.a(r0, r1)     // Catch:{ Throwable -> 0x008e }
                    android.content.Context r0 = r0     // Catch:{ Throwable -> 0x008e }
                    java.lang.String r1 = com.amazon.device.iap.internal.c.a.b     // Catch:{ Throwable -> 0x008e }
                    r2 = 0
                    android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r2)     // Catch:{ Throwable -> 0x008e }
                    java.util.Map r1 = r0.getAll()     // Catch:{ Throwable -> 0x008e }
                    java.util.Set r1 = r1.keySet()     // Catch:{ Throwable -> 0x008e }
                    java.util.Iterator r1 = r1.iterator()     // Catch:{ Throwable -> 0x008e }
                L_0x0020:
                    boolean r2 = r1.hasNext()     // Catch:{ Throwable -> 0x008e }
                    if (r2 == 0) goto L_0x00a7
                    java.lang.Object r2 = r1.next()     // Catch:{ Throwable -> 0x008e }
                    java.lang.String r2 = (java.lang.String) r2     // Catch:{ Throwable -> 0x008e }
                    r3 = 0
                    java.lang.String r3 = r0.getString(r2, r3)     // Catch:{ e -> 0x006b }
                    com.amazon.device.iap.internal.c.d r3 = com.amazon.device.iap.internal.c.d.a(r3)     // Catch:{ e -> 0x006b }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ e -> 0x006b }
                    long r6 = r3.c()     // Catch:{ e -> 0x006b }
                    long r8 = r4 - r6
                    int r3 = com.amazon.device.iap.internal.c.a.d     // Catch:{ e -> 0x006b }
                    long r3 = (long) r3     // Catch:{ e -> 0x006b }
                    int r5 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
                    if (r5 <= 0) goto L_0x0020
                    java.lang.String r3 = com.amazon.device.iap.internal.c.a.a     // Catch:{ e -> 0x006b }
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ e -> 0x006b }
                    r4.<init>()     // Catch:{ e -> 0x006b }
                    java.lang.String r5 = "house keeping - try remove Receipt:"
                    r4.append(r5)     // Catch:{ e -> 0x006b }
                    r4.append(r2)     // Catch:{ e -> 0x006b }
                    java.lang.String r5 = " since it's too old"
                    r4.append(r5)     // Catch:{ e -> 0x006b }
                    java.lang.String r4 = r4.toString()     // Catch:{ e -> 0x006b }
                    com.amazon.device.iap.internal.util.e.a(r3, r4)     // Catch:{ e -> 0x006b }
                    com.amazon.device.iap.internal.c.a r3 = com.amazon.device.iap.internal.c.a.this     // Catch:{ e -> 0x006b }
                    r3.a(r2)     // Catch:{ e -> 0x006b }
                    goto L_0x0020
                L_0x006b:
                    java.lang.String r3 = com.amazon.device.iap.internal.c.a.a     // Catch:{ Throwable -> 0x008e }
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x008e }
                    r4.<init>()     // Catch:{ Throwable -> 0x008e }
                    java.lang.String r5 = "house keeping - try remove Receipt:"
                    r4.append(r5)     // Catch:{ Throwable -> 0x008e }
                    r4.append(r2)     // Catch:{ Throwable -> 0x008e }
                    java.lang.String r5 = " since it's invalid "
                    r4.append(r5)     // Catch:{ Throwable -> 0x008e }
                    java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x008e }
                    com.amazon.device.iap.internal.util.e.a(r3, r4)     // Catch:{ Throwable -> 0x008e }
                    com.amazon.device.iap.internal.c.a r3 = com.amazon.device.iap.internal.c.a.this     // Catch:{ Throwable -> 0x008e }
                    r3.a(r2)     // Catch:{ Throwable -> 0x008e }
                    goto L_0x0020
                L_0x008e:
                    r0 = move-exception
                    java.lang.String r1 = com.amazon.device.iap.internal.c.a.a
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "Error in running cleaning job:"
                    r2.append(r3)
                    r2.append(r0)
                    java.lang.String r0 = r2.toString()
                    com.amazon.device.iap.internal.util.e.a(r1, r0)
                L_0x00a7:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.amazon.device.iap.internal.c.a.AnonymousClass1.run():void");
            }
        });
    }

    public void a(String str) {
        String str2 = a;
        e.a(str2, "enter removeReceipt for receipt[" + str + "]");
        Context b2 = com.amazon.device.iap.internal.d.d().b();
        d.a(b2, "context");
        SharedPreferences.Editor edit = b2.getSharedPreferences(b, 0).edit();
        edit.remove(str);
        edit.commit();
        String str3 = a;
        e.a(str3, "leave removeReceipt for receipt[" + str + "]");
    }

    private long f() {
        Context b2 = com.amazon.device.iap.internal.d.d().b();
        d.a(b2, "context");
        long currentTimeMillis = System.currentTimeMillis();
        long j = b2.getSharedPreferences(c, 0).getLong("LAST_CLEANING_TIME", 0);
        if (j != 0) {
            return j;
        }
        a(currentTimeMillis);
        return currentTimeMillis;
    }

    private void a(long j) {
        Context b2 = com.amazon.device.iap.internal.d.d().b();
        d.a(b2, "context");
        SharedPreferences.Editor edit = b2.getSharedPreferences(c, 0).edit();
        edit.putLong("LAST_CLEANING_TIME", j);
        edit.commit();
    }

    public Set<Receipt> b(String str) {
        Context b2 = com.amazon.device.iap.internal.d.d().b();
        d.a(b2, "context");
        String str2 = a;
        e.a(str2, "enter getLocalReceipts for user[" + str + "]");
        HashSet hashSet = new HashSet();
        if (d.a(str)) {
            String str3 = a;
            e.b(str3, "empty UserId: " + str);
            throw new RuntimeException("Invalid UserId:" + str);
        }
        Map<String, ?> all = b2.getSharedPreferences(b, 0).getAll();
        for (String next : all.keySet()) {
            String str4 = (String) all.get(next);
            try {
                d a2 = d.a(str4);
                hashSet.add(com.amazon.device.iap.internal.util.a.a(new JSONObject(a2.b()), str, a2.a()));
            } catch (com.amazon.device.iap.internal.b.d unused) {
                a(next);
                String str5 = a;
                e.b(str5, "failed to verify signature:[" + str4 + "]");
            } catch (JSONException unused2) {
                a(next);
                String str6 = a;
                e.b(str6, "failed to convert string to JSON object:[" + str4 + "]");
            } catch (Throwable unused3) {
                String str7 = a;
                e.b(str7, "failed to load the receipt from SharedPreference:[" + str4 + "]");
            }
        }
        String str8 = a;
        e.a(str8, "leaving getLocalReceipts for user[" + str + "], " + hashSet.size() + " local receipts found.");
        if (System.currentTimeMillis() - f() > ((long) d)) {
            e();
        }
        return hashSet;
    }

    public static a a() {
        return e;
    }

    public String c(String str) {
        Context b2 = com.amazon.device.iap.internal.d.d().b();
        d.a(b2, "context");
        if (d.a(str)) {
            String str2 = a;
            e.b(str2, "empty receiptId: " + str);
            throw new RuntimeException("Invalid ReceiptId:" + str);
        }
        String string = b2.getSharedPreferences(b, 0).getString(str, null);
        if (string != null) {
            try {
                return d.a(string).a();
            } catch (e unused) {
            }
        }
        return null;
    }
}
