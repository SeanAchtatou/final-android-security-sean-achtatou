package com.amazon.device.iap.internal.util;

import android.content.Context;
import android.content.SharedPreferences;
import com.amazon.device.iap.internal.d;

/* compiled from: CursorUtil */
public class b {
    private static final String a = (b.class.getName() + "_PREFS");

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amazon.device.iap.internal.util.d.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.amazon.device.iap.internal.util.d.a(java.lang.String, java.lang.String):void
      com.amazon.device.iap.internal.util.d.a(java.util.Collection<? extends java.lang.Object>, java.lang.String):void
      com.amazon.device.iap.internal.util.d.a(java.lang.Object, java.lang.String):void */
    public static String a(String str) {
        d.a((Object) str, "userId");
        Context b = d.d().b();
        d.a(b, "context");
        return b.getSharedPreferences(a, 0).getString(str, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amazon.device.iap.internal.util.d.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.amazon.device.iap.internal.util.d.a(java.lang.String, java.lang.String):void
      com.amazon.device.iap.internal.util.d.a(java.util.Collection<? extends java.lang.Object>, java.lang.String):void
      com.amazon.device.iap.internal.util.d.a(java.lang.Object, java.lang.String):void */
    public static void a(String str, String str2) {
        d.a((Object) str, "userId");
        Context b = d.d().b();
        d.a(b, "context");
        SharedPreferences.Editor edit = b.getSharedPreferences(a, 0).edit();
        edit.putString(str, str2);
        edit.commit();
    }
}
