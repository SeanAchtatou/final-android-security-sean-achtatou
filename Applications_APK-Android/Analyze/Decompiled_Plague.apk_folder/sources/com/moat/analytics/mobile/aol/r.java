package com.moat.analytics.mobile.aol;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.FloatRange;
import android.telephony.TelephonyManager;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.lang.ref.WeakReference;

final class r {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static int f161 = 1;

    /* renamed from: ˊ  reason: contains not printable characters */
    private static e f162;

    /* renamed from: ˋ  reason: contains not printable characters */
    private static d f163;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static int f164;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public static String f165;

    /* renamed from: ॱ  reason: contains not printable characters */
    private static int[] f166 = {-39340411, 1646369784, -593413711, -1069164445, -50787683, -1327220997, 423245644, -742130253, 54775946, -495304555, 1880137505, 1742082653, 65717847, 1497802820, 828947133, -614454858, 941569790, -1897799303};

    r() {
    }

    @FloatRange(from = 0.0d, to = 1.0d)
    /* renamed from: ॱ  reason: contains not printable characters */
    static double m156() {
        try {
            return ((double) m147()) / ((double) ((AudioManager) c.m29().getSystemService(m153(new int[]{-1741845568, 995393484, -1443163044, -1832527325}, 5).intern())).getStreamMaxVolume(3));
        } catch (Exception e2) {
            o.m132(e2);
            return 0.0d;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static int m147() {
        try {
            return ((AudioManager) c.m29().getSystemService(m153(new int[]{-1741845568, 995393484, -1443163044, -1832527325}, 5).intern())).getStreamVolume(3);
        } catch (Exception e2) {
            o.m132(e2);
            return 0;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static void m154(final Application application) {
        try {
            AsyncTask.execute(new Runnable() {
                public final void run() {
                    try {
                        AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(application);
                        if (!advertisingIdInfo.isLimitAdTrackingEnabled()) {
                            String unused = r.f165 = advertisingIdInfo.getId();
                            a.m8(3, "Util", this, "Retrieved Advertising ID = " + r.f165);
                            return;
                        }
                        a.m8(3, "Util", this, "User has limited ad tracking");
                    } catch (Exception e) {
                        o.m132(e);
                    }
                }
            });
        } catch (Exception e2) {
            o.m132(e2);
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static String m152() {
        return f165;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static Context m155() {
        WeakReference<Context> weakReference = ((f) MoatAnalytics.getInstance()).f61;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static e m149() {
        if (f162 == null || !f162.f176) {
            f162 = new e((byte) 0);
        }
        return f162;
    }

    static class e {

        /* renamed from: ˊ  reason: contains not printable characters */
        private String f174;

        /* renamed from: ˋ  reason: contains not printable characters */
        private String f175;
        /* access modifiers changed from: private */

        /* renamed from: ˏ  reason: contains not printable characters */
        public boolean f176;

        /* renamed from: ॱ  reason: contains not printable characters */
        private String f177;

        /* synthetic */ e(byte b) {
            this();
        }

        private e() {
            this.f176 = false;
            this.f174 = "_unknown_";
            this.f175 = "_unknown_";
            this.f177 = "_unknown_";
            try {
                Context r0 = r.m155();
                if (r0 != null) {
                    this.f176 = true;
                    PackageManager packageManager = r0.getPackageManager();
                    this.f175 = r0.getPackageName();
                    this.f174 = packageManager.getApplicationLabel(r0.getApplicationInfo()).toString();
                    this.f177 = packageManager.getInstallerPackageName(this.f175);
                    return;
                }
                a.m8(3, "Util", this, "Can't get app name, appContext is null.");
            } catch (Exception e) {
                o.m132(e);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˎ  reason: contains not printable characters */
        public final String m160() {
            return this.f174;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public final String m159() {
            return this.f175;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ॱ  reason: contains not printable characters */
        public final String m161() {
            return this.f177 != null ? this.f177 : "_unknown_";
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static String m153(int[] iArr, int i) {
        char[] cArr = new char[4];
        char[] cArr2 = new char[(iArr.length << 1)];
        int[] iArr2 = (int[]) f166.clone();
        int i2 = 0;
        while (true) {
            if (i2 >= iArr.length) {
                return new String(cArr2, 0, i);
            }
            cArr[0] = iArr[i2] >>> 16;
            cArr[1] = (char) iArr[i2];
            int i3 = i2 + 1;
            cArr[2] = iArr[i3] >>> 16;
            cArr[3] = (char) iArr[i3];
            com.a.e.d.m0(cArr, iArr2, false);
            int i4 = i2 << 1;
            cArr2[i4] = cArr[0];
            cArr2[i4 + 1] = cArr[1];
            cArr2[i4 + 2] = cArr[2];
            cArr2[i4 + 3] = cArr[3];
            i2 += 2;
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static d m150() {
        if (f163 == null || !f163.f168) {
            f163 = new d((byte) 0);
        }
        return f163;
    }

    static class d {

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f168;

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f169;

        /* renamed from: ˋ  reason: contains not printable characters */
        boolean f170;

        /* renamed from: ˎ  reason: contains not printable characters */
        String f171;

        /* renamed from: ˏ  reason: contains not printable characters */
        String f172;

        /* renamed from: ॱ  reason: contains not printable characters */
        Integer f173;

        /* synthetic */ d(byte b) {
            this();
        }

        private d() {
            this.f171 = "_unknown_";
            this.f172 = "_unknown_";
            this.f173 = -1;
            this.f170 = false;
            this.f169 = false;
            this.f168 = false;
            try {
                Context r0 = r.m155();
                if (r0 != null) {
                    this.f168 = true;
                    TelephonyManager telephonyManager = (TelephonyManager) r0.getSystemService("phone");
                    this.f171 = telephonyManager.getSimOperatorName();
                    this.f172 = telephonyManager.getNetworkOperatorName();
                    this.f173 = Integer.valueOf(telephonyManager.getPhoneType());
                    this.f170 = r.m146();
                    this.f169 = r.m151(r0);
                }
            } catch (Exception e) {
                o.m132(e);
            }
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static boolean m151(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static /* synthetic */ boolean m146() {
        Context context;
        int i;
        WeakReference<Context> weakReference = ((f) MoatAnalytics.getInstance()).f61;
        if (weakReference != null) {
            context = weakReference.get();
        } else {
            context = null;
        }
        if (context != null) {
            int i2 = f161 + 27;
            f164 = i2 % 128;
            int i3 = i2 % 2;
            if ((Build.VERSION.SDK_INT < 17 ? (char) 22 : 19) != 22) {
                int i4 = f161 + 87;
                f164 = i4 % 128;
                int i5 = i4 % 2;
                i = Settings.Global.getInt(context.getContentResolver(), m153(new int[]{-474338915, -1244865125, 562481890, 44523707, -1306238932, 74746991}, 11).intern(), 0);
            } else {
                i = Settings.Secure.getInt(context.getContentResolver(), m153(new int[]{-474338915, -1244865125, 562481890, 44523707, -1306238932, 74746991}, 11).intern(), 0);
            }
        } else {
            i = 0;
        }
        if (!(i == 1)) {
            return false;
        }
        int i6 = f164 + 33;
        f161 = i6 % 128;
        int i7 = i6 % 2;
        return true;
    }
}
