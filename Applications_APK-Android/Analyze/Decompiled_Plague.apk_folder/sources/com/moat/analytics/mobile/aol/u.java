package com.moat.analytics.mobile.aol;

import android.graphics.Rect;
import android.location.Location;
import android.support.annotation.VisibleForTesting;
import android.support.v4.widget.ExploreByTouchHelper;
import android.util.DisplayMetrics;
import android.view.View;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

final class u {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static int f208 = 0;

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private static int f209 = 1;

    /* renamed from: ʻ  reason: contains not printable characters */
    private Location f210;

    /* renamed from: ʽ  reason: contains not printable characters */
    private JSONObject f211;

    /* renamed from: ˊ  reason: contains not printable characters */
    private Rect f212;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private JSONObject f213;

    /* renamed from: ˋ  reason: contains not printable characters */
    private JSONObject f214;

    /* renamed from: ˎ  reason: contains not printable characters */
    private Rect f215;

    /* renamed from: ˏ  reason: contains not printable characters */
    private c f216 = new c();

    /* renamed from: ॱ  reason: contains not printable characters */
    String f217 = "{}";

    /* renamed from: ᐝ  reason: contains not printable characters */
    private Map<String, Object> f218 = new HashMap();

    static class c {

        /* renamed from: ˊ  reason: contains not printable characters */
        double f224 = 0.0d;

        /* renamed from: ˋ  reason: contains not printable characters */
        Rect f225 = new Rect(0, 0, 0, 0);

        /* renamed from: ॱ  reason: contains not printable characters */
        double f226 = 0.0d;

        c() {
        }
    }

    static class a {

        /* renamed from: ˎ  reason: contains not printable characters */
        final Rect f219;

        /* renamed from: ˏ  reason: contains not printable characters */
        final View f220;

        a(View view, a aVar) {
            this.f220 = view;
            if (aVar != null) {
                int i = aVar.f219.left;
                int i2 = aVar.f219.top;
                int left = i + view.getLeft();
                int top = i2 + view.getTop();
                this.f219 = new Rect(left, top, view.getWidth() + left, view.getHeight() + top);
                return;
            }
            this.f219 = u.m196(view);
        }
    }

    u() {
    }

    /* JADX WARN: Type inference failed for: r2v70, types: [java.util.HashSet, java.util.Set<android.graphics.Rect>] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x012a, code lost:
        if ((!r23.isShown() ? true : false) != true) goto L_0x012c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x025f, code lost:
        if ((r13) != false) goto L_0x0219;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x0267, code lost:
        if ((r13) != false) goto L_0x0219;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:0x02cf, code lost:
        if ((r9 >= r10) != true) goto L_0x02d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:295:0x03ba, code lost:
        if (r12.equals(r1.f215) == false) goto L_0x03bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:306:0x03f3, code lost:
        if (r4.equals(r1.f212) == false) goto L_0x03f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x011e, code lost:
        if ((r23.isShown()) != true) goto L_0x012e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0135  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x013b A[SYNTHETIC, Splitter:B:110:0x013b] */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0140  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0164  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0167  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x016f  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0171  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x018d  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x0190  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0194  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x01ae A[Catch:{ Exception -> 0x050c }] */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x01b1 A[Catch:{ Exception -> 0x050c }] */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x01b4 A[Catch:{ Exception -> 0x050c }] */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x01b6 A[Catch:{ Exception -> 0x050c }] */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x01d2  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x01d5  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x01dd  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x01fa A[SYNTHETIC, Splitter:B:158:0x01fa] */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0210  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x0212  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x021f  */
    /* JADX WARNING: Removed duplicated region for block: B:234:0x02e0 A[Catch:{ Exception -> 0x050c }] */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x02e2 A[Catch:{ Exception -> 0x050c }] */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x02e8 A[Catch:{ Exception -> 0x050c }] */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x032e A[Catch:{ Exception -> 0x0507 }] */
    /* JADX WARNING: Removed duplicated region for block: B:259:0x0331 A[Catch:{ Exception -> 0x0507 }] */
    /* JADX WARNING: Removed duplicated region for block: B:262:0x0337 A[Catch:{ Exception -> 0x0507 }] */
    /* JADX WARNING: Removed duplicated region for block: B:281:0x0391  */
    /* JADX WARNING: Removed duplicated region for block: B:282:0x0393  */
    /* JADX WARNING: Removed duplicated region for block: B:284:0x0396  */
    /* JADX WARNING: Removed duplicated region for block: B:299:0x03d2  */
    /* JADX WARNING: Removed duplicated region for block: B:300:0x03d7  */
    /* JADX WARNING: Removed duplicated region for block: B:302:0x03dd  */
    /* JADX WARNING: Removed duplicated region for block: B:303:0x03e0  */
    /* JADX WARNING: Removed duplicated region for block: B:310:0x040b A[Catch:{ Exception -> 0x0507 }] */
    /* JADX WARNING: Removed duplicated region for block: B:311:0x0410 A[Catch:{ Exception -> 0x0507 }] */
    /* JADX WARNING: Removed duplicated region for block: B:313:0x0416 A[Catch:{ Exception -> 0x0507 }] */
    /* JADX WARNING: Removed duplicated region for block: B:323:0x043b A[Catch:{ Exception -> 0x0507 }] */
    /* JADX WARNING: Removed duplicated region for block: B:324:0x0440 A[Catch:{ Exception -> 0x0507 }] */
    /* JADX WARNING: Removed duplicated region for block: B:326:0x0446 A[Catch:{ Exception -> 0x0507 }] */
    /* JADX WARNING: Removed duplicated region for block: B:329:0x044b  */
    /* JADX WARNING: Removed duplicated region for block: B:330:0x044d  */
    /* JADX WARNING: Removed duplicated region for block: B:332:0x0452  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:368:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00e4 A[SYNTHETIC, Splitter:B:73:0x00e4] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0103  */
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m198(java.lang.String r22, android.view.View r23) {
        /*
            r21 = this;
            r1 = r21
            r2 = r23
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.lang.String r4 = "{}"
            r5 = 1
            r6 = 0
            if (r2 == 0) goto L_0x0011
            r7 = r6
            goto L_0x0012
        L_0x0011:
            r7 = r5
        L_0x0012:
            if (r7 == 0) goto L_0x0016
            goto L_0x0506
        L_0x0016:
            int r7 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x050c }
            r8 = 17
            if (r7 < r8) goto L_0x001e
            r7 = r6
            goto L_0x001f
        L_0x001e:
            r7 = r5
        L_0x001f:
            if (r7 == 0) goto L_0x0022
            goto L_0x004f
        L_0x0022:
            int r7 = com.moat.analytics.mobile.aol.u.f208
            int r7 = r7 + 33
            int r8 = r7 % 128
            com.moat.analytics.mobile.aol.u.f209 = r8
            int r7 = r7 % 2
            java.lang.ref.WeakReference<android.app.Activity> r7 = com.moat.analytics.mobile.aol.c.f43     // Catch:{ Exception -> 0x050c }
            if (r7 == 0) goto L_0x0032
            r7 = r6
            goto L_0x0033
        L_0x0032:
            r7 = r5
        L_0x0033:
            if (r7 == 0) goto L_0x0036
            goto L_0x004f
        L_0x0036:
            int r7 = com.moat.analytics.mobile.aol.u.f208
            int r7 = r7 + 13
            int r8 = r7 % 128
            com.moat.analytics.mobile.aol.u.f209 = r8
            int r7 = r7 % 2
            java.lang.ref.WeakReference<android.app.Activity> r7 = com.moat.analytics.mobile.aol.c.f43     // Catch:{ Exception -> 0x050c }
            java.lang.Object r7 = r7.get()     // Catch:{ Exception -> 0x050c }
            android.app.Activity r7 = (android.app.Activity) r7     // Catch:{ Exception -> 0x050c }
            if (r7 == 0) goto L_0x004c
            r8 = r5
            goto L_0x004d
        L_0x004c:
            r8 = r6
        L_0x004d:
            if (r8 == r5) goto L_0x005f
        L_0x004f:
            android.content.Context r7 = r23.getContext()     // Catch:{ Exception -> 0x005c }
            android.content.res.Resources r7 = r7.getResources()     // Catch:{ Exception -> 0x005c }
            android.util.DisplayMetrics r7 = r7.getDisplayMetrics()     // Catch:{ Exception -> 0x005c }
            goto L_0x0070
        L_0x005c:
            r0 = move-exception
            goto L_0x050f
        L_0x005f:
            android.util.DisplayMetrics r8 = new android.util.DisplayMetrics     // Catch:{ Exception -> 0x050c }
            r8.<init>()     // Catch:{ Exception -> 0x050c }
            android.view.WindowManager r7 = r7.getWindowManager()     // Catch:{ Exception -> 0x050c }
            android.view.Display r7 = r7.getDefaultDisplay()     // Catch:{ Exception -> 0x050c }
            r7.getRealMetrics(r8)     // Catch:{ Exception -> 0x050c }
            r7 = r8
        L_0x0070:
            int r8 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x050c }
            r9 = 19
            if (r8 < r9) goto L_0x0078
            r8 = r6
            goto L_0x0079
        L_0x0078:
            r8 = r5
        L_0x0079:
            r10 = 57
            r12 = 21
            if (r8 == r5) goto L_0x00b1
            int r8 = com.moat.analytics.mobile.aol.u.f208
            int r8 = r8 + 17
            int r13 = r8 % 128
            com.moat.analytics.mobile.aol.u.f209 = r13
            int r8 = r8 % 2
            if (r2 == 0) goto L_0x008e
            r8 = 76
            goto L_0x008f
        L_0x008e:
            r8 = r9
        L_0x008f:
            r13 = 76
            if (r8 == r13) goto L_0x0094
            goto L_0x00a0
        L_0x0094:
            boolean r8 = r23.isAttachedToWindow()     // Catch:{ Exception -> 0x005c }
            if (r8 == 0) goto L_0x009c
            r8 = r9
            goto L_0x009e
        L_0x009c:
            r8 = 91
        L_0x009e:
            if (r8 == r9) goto L_0x00a1
        L_0x00a0:
            goto L_0x00d8
        L_0x00a1:
            int r8 = com.moat.analytics.mobile.aol.u.f209
            int r8 = r8 + 117
            int r9 = r8 % 128
            com.moat.analytics.mobile.aol.u.f208 = r9
            int r8 = r8 % 2
            if (r8 == 0) goto L_0x00af
            r8 = r6
            goto L_0x00d6
        L_0x00af:
            r8 = r5
            goto L_0x00d6
        L_0x00b1:
            if (r2 == 0) goto L_0x00b5
            r8 = r5
            goto L_0x00b6
        L_0x00b5:
            r8 = r6
        L_0x00b6:
            if (r8 == 0) goto L_0x00d8
            int r8 = com.moat.analytics.mobile.aol.u.f208
            int r8 = r8 + r12
            int r9 = r8 % 128
            com.moat.analytics.mobile.aol.u.f209 = r9
            int r8 = r8 % 2
            android.os.IBinder r8 = r23.getWindowToken()     // Catch:{ Exception -> 0x005c }
            if (r8 == 0) goto L_0x00c9
            r8 = r6
            goto L_0x00ca
        L_0x00c9:
            r8 = r5
        L_0x00ca:
            if (r8 == 0) goto L_0x00cd
            goto L_0x00d8
        L_0x00cd:
            int r8 = com.moat.analytics.mobile.aol.u.f209
            int r8 = r8 + r10
            int r9 = r8 % 128
            com.moat.analytics.mobile.aol.u.f208 = r9
            int r8 = r8 % 2
        L_0x00d6:
            r8 = r5
            goto L_0x00d9
        L_0x00d8:
            r8 = r6
        L_0x00d9:
            if (r2 == 0) goto L_0x00de
            r9 = 11
            goto L_0x00e0
        L_0x00de:
            r9 = 60
        L_0x00e0:
            r13 = 60
            if (r9 == r13) goto L_0x00f2
            boolean r9 = r23.hasWindowFocus()     // Catch:{ Exception -> 0x005c }
            if (r9 == 0) goto L_0x00ec
            r9 = r5
            goto L_0x00ed
        L_0x00ec:
            r9 = r6
        L_0x00ed:
            if (r9 == r5) goto L_0x00f0
            goto L_0x00f2
        L_0x00f0:
            r9 = r5
            goto L_0x00f3
        L_0x00f2:
            r9 = r6
        L_0x00f3:
            if (r2 == 0) goto L_0x00f8
            r13 = 89
            goto L_0x00fa
        L_0x00f8:
            r13 = 58
        L_0x00fa:
            r14 = 89
            r15 = 65
            r10 = 69
            if (r13 == r14) goto L_0x0103
            goto L_0x012e
        L_0x0103:
            int r13 = com.moat.analytics.mobile.aol.u.f208
            int r13 = r13 + 39
            int r14 = r13 % 128
            com.moat.analytics.mobile.aol.u.f209 = r14
            int r13 = r13 % 2
            if (r13 != 0) goto L_0x0111
            r13 = r15
            goto L_0x0113
        L_0x0111:
            r13 = 18
        L_0x0113:
            if (r13 == r15) goto L_0x0121
            boolean r13 = r23.isShown()     // Catch:{ Exception -> 0x005c }
            if (r13 != 0) goto L_0x011d
            r13 = r6
            goto L_0x011e
        L_0x011d:
            r13 = r5
        L_0x011e:
            if (r13 == r5) goto L_0x012c
            goto L_0x012e
        L_0x0121:
            boolean r13 = r23.isShown()     // Catch:{ Exception -> 0x050c }
            if (r13 != 0) goto L_0x0129
            r13 = r10
            goto L_0x012a
        L_0x0129:
            r13 = r6
        L_0x012a:
            if (r13 == r10) goto L_0x012e
        L_0x012c:
            r13 = r6
            goto L_0x012f
        L_0x012e:
            r13 = r5
        L_0x012f:
            r14 = 93
            if (r2 != 0) goto L_0x0135
            r12 = r14
            goto L_0x0139
        L_0x0135:
            r16 = 32
            r12 = r16
        L_0x0139:
            if (r12 == r14) goto L_0x0140
            float r12 = m188(r23)     // Catch:{ Exception -> 0x005c }
            goto L_0x0141
        L_0x0140:
            r12 = 0
        L_0x0141:
            java.lang.String r14 = "dr"
            float r5 = r7.density     // Catch:{ Exception -> 0x050c }
            java.lang.Float r5 = java.lang.Float.valueOf(r5)     // Catch:{ Exception -> 0x050c }
            r3.put(r14, r5)     // Catch:{ Exception -> 0x050c }
            java.lang.String r5 = "dv"
            double r10 = com.moat.analytics.mobile.aol.r.m156()     // Catch:{ Exception -> 0x050c }
            java.lang.Double r10 = java.lang.Double.valueOf(r10)     // Catch:{ Exception -> 0x050c }
            r3.put(r5, r10)     // Catch:{ Exception -> 0x050c }
            java.lang.String r5 = "adKey"
            r10 = r22
            r3.put(r5, r10)     // Catch:{ Exception -> 0x050c }
            java.lang.String r5 = "isAttached"
            if (r8 == 0) goto L_0x0167
            r10 = 90
            goto L_0x0169
        L_0x0167:
            r10 = 45
        L_0x0169:
            r11 = 90
            r14 = 31
            if (r10 == r11) goto L_0x0171
            r10 = r6
            goto L_0x0182
        L_0x0171:
            int r10 = com.moat.analytics.mobile.aol.u.f209
            int r10 = r10 + 35
            int r11 = r10 % 128
            com.moat.analytics.mobile.aol.u.f208 = r11
            int r10 = r10 % 2
            if (r10 == 0) goto L_0x017f
            r10 = r14
            goto L_0x0181
        L_0x017f:
            r10 = 72
        L_0x0181:
            r10 = 1
        L_0x0182:
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x050c }
            r3.put(r5, r10)     // Catch:{ Exception -> 0x050c }
            java.lang.String r5 = "inFocus"
            if (r9 == 0) goto L_0x0190
            r10 = 48
            goto L_0x0191
        L_0x0190:
            r10 = 3
        L_0x0191:
            r11 = 3
            if (r10 == r11) goto L_0x01a0
            int r10 = com.moat.analytics.mobile.aol.u.f209
            int r10 = r10 + 15
            int r11 = r10 % 128
            com.moat.analytics.mobile.aol.u.f208 = r11
            int r10 = r10 % 2
            r10 = 1
            goto L_0x01a1
        L_0x01a0:
            r10 = r6
        L_0x01a1:
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x050c }
            r3.put(r5, r10)     // Catch:{ Exception -> 0x050c }
            java.lang.String r5 = "isHidden"
            r10 = 59
            if (r13 == 0) goto L_0x01b1
            r11 = 95
            goto L_0x01b2
        L_0x01b1:
            r11 = r10
        L_0x01b2:
            if (r11 == r10) goto L_0x01b6
            r11 = 1
            goto L_0x01b7
        L_0x01b6:
            r11 = r6
        L_0x01b7:
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ Exception -> 0x050c }
            r3.put(r5, r11)     // Catch:{ Exception -> 0x050c }
            java.lang.String r5 = "opacity"
            java.lang.Float r11 = java.lang.Float.valueOf(r12)     // Catch:{ Exception -> 0x050c }
            r3.put(r5, r11)     // Catch:{ Exception -> 0x050c }
            int r5 = r7.widthPixels     // Catch:{ Exception -> 0x050c }
            int r11 = r7.heightPixels     // Catch:{ Exception -> 0x050c }
            android.graphics.Rect r12 = new android.graphics.Rect     // Catch:{ Exception -> 0x050c }
            r12.<init>(r6, r6, r5, r11)     // Catch:{ Exception -> 0x050c }
            if (r2 == 0) goto L_0x01d5
            r5 = 90
            goto L_0x01d7
        L_0x01d5:
            r5 = 50
        L_0x01d7:
            r11 = 50
            r10 = 28
            if (r5 == r11) goto L_0x01fa
            int r5 = com.moat.analytics.mobile.aol.u.f208
            r11 = 91
            int r5 = r5 + r11
            int r11 = r5 % 128
            com.moat.analytics.mobile.aol.u.f209 = r11
            int r5 = r5 % 2
            if (r5 != 0) goto L_0x01ec
            r5 = r10
            goto L_0x01ee
        L_0x01ec:
            r5 = 98
        L_0x01ee:
            if (r5 == r10) goto L_0x01f5
            android.graphics.Rect r5 = m196(r23)     // Catch:{ Exception -> 0x005c }
            goto L_0x01ff
        L_0x01f5:
            android.graphics.Rect r5 = m196(r23)     // Catch:{ Exception -> 0x005c }
            goto L_0x01ff
        L_0x01fa:
            android.graphics.Rect r5 = new android.graphics.Rect     // Catch:{ Exception -> 0x050c }
            r5.<init>(r6, r6, r6, r6)     // Catch:{ Exception -> 0x050c }
        L_0x01ff:
            com.moat.analytics.mobile.aol.u$c r11 = new com.moat.analytics.mobile.aol.u$c     // Catch:{ Exception -> 0x050c }
            r11.<init>()     // Catch:{ Exception -> 0x050c }
            int r16 = r5.width()     // Catch:{ Exception -> 0x050c }
            int r17 = r5.height()     // Catch:{ Exception -> 0x050c }
            int r10 = r16 * r17
            if (r2 == 0) goto L_0x0212
            r6 = r14
            goto L_0x0216
        L_0x0212:
            r16 = 99
            r6 = r16
        L_0x0216:
            r15 = 0
            if (r6 == r14) goto L_0x021f
        L_0x0219:
            r19 = r4
            r20 = r5
            goto L_0x032a
        L_0x021f:
            int r6 = com.moat.analytics.mobile.aol.u.f209
            r16 = 65
            int r6 = r6 + 65
            int r14 = r6 % 128
            com.moat.analytics.mobile.aol.u.f208 = r14
            int r6 = r6 % 2
            if (r8 == 0) goto L_0x022f
            r6 = 1
            goto L_0x0230
        L_0x022f:
            r6 = 0
        L_0x0230:
            if (r6 == 0) goto L_0x0219
            int r6 = com.moat.analytics.mobile.aol.u.f209
            int r6 = r6 + 23
            int r8 = r6 % 128
            com.moat.analytics.mobile.aol.u.f208 = r8
            int r6 = r6 % 2
            if (r9 == 0) goto L_0x0243
            r6 = 69
        L_0x0240:
            r8 = 69
            goto L_0x0246
        L_0x0243:
            r6 = 31
            goto L_0x0240
        L_0x0246:
            if (r6 == r8) goto L_0x0249
            goto L_0x0219
        L_0x0249:
            int r6 = com.moat.analytics.mobile.aol.u.f209
            int r6 = r6 + 107
            int r8 = r6 % 128
            com.moat.analytics.mobile.aol.u.f208 = r8
            int r6 = r6 % 2
            if (r6 == 0) goto L_0x0257
            r6 = 1
            goto L_0x0258
        L_0x0257:
            r6 = 0
        L_0x0258:
            if (r6 == 0) goto L_0x0262
            if (r13 != 0) goto L_0x025e
            r6 = 0
            goto L_0x025f
        L_0x025e:
            r6 = 1
        L_0x025f:
            if (r6 == 0) goto L_0x026a
            goto L_0x0219
        L_0x0262:
            if (r13 != 0) goto L_0x0266
            r6 = 0
            goto L_0x0267
        L_0x0266:
            r6 = 1
        L_0x0267:
            if (r6 == 0) goto L_0x026a
            goto L_0x0219
        L_0x026a:
            if (r10 <= 0) goto L_0x026f
            r14 = 31
            goto L_0x0271
        L_0x026f:
            r14 = 77
        L_0x0271:
            r6 = 77
            if (r14 == r6) goto L_0x0219
            int r6 = com.moat.analytics.mobile.aol.u.f209
            int r6 = r6 + 49
            int r8 = r6 % 128
            com.moat.analytics.mobile.aol.u.f208 = r8
            int r6 = r6 % 2
            android.graphics.Rect r6 = new android.graphics.Rect     // Catch:{ Exception -> 0x050c }
            r8 = 0
            r6.<init>(r8, r8, r8, r8)     // Catch:{ Exception -> 0x050c }
            boolean r9 = m193(r2, r6)     // Catch:{ Exception -> 0x050c }
            if (r9 == 0) goto L_0x0290
            r9 = 59
        L_0x028d:
            r13 = 59
            goto L_0x0293
        L_0x0290:
            r9 = 86
            goto L_0x028d
        L_0x0293:
            if (r9 == r13) goto L_0x0296
            goto L_0x0219
        L_0x0296:
            int r9 = com.moat.analytics.mobile.aol.u.f208
            int r9 = r9 + 119
            int r13 = r9 % 128
            com.moat.analytics.mobile.aol.u.f209 = r13
            int r9 = r9 % 2
            if (r9 != 0) goto L_0x02a4
            r9 = r8
            goto L_0x02a5
        L_0x02a4:
            r9 = 1
        L_0x02a5:
            if (r9 == 0) goto L_0x02bf
            int r9 = r6.width()     // Catch:{ Exception -> 0x005c }
            int r13 = r6.height()     // Catch:{ Exception -> 0x005c }
            int r9 = r9 * r13
            if (r9 >= r10) goto L_0x02b5
            r13 = 86
            goto L_0x02b7
        L_0x02b5:
            r13 = 98
        L_0x02b7:
            r14 = 98
            if (r13 == r14) goto L_0x02bd
            r14 = 1
            goto L_0x02d1
        L_0x02bd:
            r14 = 1
            goto L_0x02d8
        L_0x02bf:
            int r9 = r6.width()     // Catch:{ Exception -> 0x050c }
            int r13 = r6.height()     // Catch:{ Exception -> 0x050c }
            int r9 = r9 * r13
            if (r9 >= r10) goto L_0x02cd
            r13 = r8
        L_0x02cb:
            r14 = 1
            goto L_0x02cf
        L_0x02cd:
            r13 = 1
            goto L_0x02cb
        L_0x02cf:
            if (r13 == r14) goto L_0x02d8
        L_0x02d1:
            java.lang.String r13 = "VisibilityInfo"
            java.lang.String r8 = "Ad is clipped"
            com.moat.analytics.mobile.aol.a.m9(r13, r15, r8)     // Catch:{ Exception -> 0x005c }
        L_0x02d8:
            android.view.View r8 = r23.getRootView()     // Catch:{ Exception -> 0x050c }
            boolean r8 = r8 instanceof android.view.ViewGroup     // Catch:{ Exception -> 0x050c }
            if (r8 == 0) goto L_0x02e2
            r8 = 0
            goto L_0x02e4
        L_0x02e2:
            r8 = 14
        L_0x02e4:
            r13 = 14
            if (r8 == r13) goto L_0x0219
            r11.f225 = r6     // Catch:{ Exception -> 0x050c }
            com.moat.analytics.mobile.aol.u$b r2 = m192(r6, r2)     // Catch:{ Exception -> 0x050c }
            boolean r8 = r2.f221     // Catch:{ Exception -> 0x050c }
            if (r8 == 0) goto L_0x02f5
            r8 = 26
            goto L_0x02f7
        L_0x02f5:
            r8 = 8
        L_0x02f7:
            r13 = 26
            if (r8 == r13) goto L_0x0322
            java.util.Set<android.graphics.Rect> r2 = r2.f222     // Catch:{ Exception -> 0x050c }
            int r2 = m189(r6, r2)     // Catch:{ Exception -> 0x050c }
            if (r2 <= 0) goto L_0x0308
            r6 = 29
        L_0x0305:
            r8 = 21
            goto L_0x030b
        L_0x0308:
            r6 = 21
            goto L_0x0305
        L_0x030b:
            if (r6 == r8) goto L_0x0317
            double r14 = (double) r2
            r19 = r4
            r20 = r5
            double r4 = (double) r9
            double r14 = r14 / r4
            r11.f226 = r14     // Catch:{ Exception -> 0x0507 }
            goto L_0x031b
        L_0x0317:
            r19 = r4
            r20 = r5
        L_0x031b:
            int r9 = r9 - r2
            double r4 = (double) r9     // Catch:{ Exception -> 0x0507 }
            double r8 = (double) r10     // Catch:{ Exception -> 0x0507 }
            double r4 = r4 / r8
            r11.f224 = r4     // Catch:{ Exception -> 0x0507 }
            goto L_0x032a
        L_0x0322:
            r19 = r4
            r20 = r5
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r11.f226 = r4     // Catch:{ Exception -> 0x0507 }
        L_0x032a:
            org.json.JSONObject r2 = r1.f214     // Catch:{ Exception -> 0x0507 }
            if (r2 == 0) goto L_0x0331
            r2 = 95
            goto L_0x0333
        L_0x0331:
            r2 = 46
        L_0x0333:
            r4 = 46
            if (r2 == r4) goto L_0x036c
            double r4 = r11.f224     // Catch:{ Exception -> 0x0507 }
            com.moat.analytics.mobile.aol.u$c r2 = r1.f216     // Catch:{ Exception -> 0x0507 }
            double r8 = r2.f224     // Catch:{ Exception -> 0x0507 }
            int r2 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r2 != 0) goto L_0x0344
            r2 = 53
            goto L_0x0346
        L_0x0344:
            r2 = 97
        L_0x0346:
            r4 = 97
            if (r2 == r4) goto L_0x036c
            android.graphics.Rect r2 = r11.f225     // Catch:{ Exception -> 0x0507 }
            com.moat.analytics.mobile.aol.u$c r4 = r1.f216     // Catch:{ Exception -> 0x0507 }
            android.graphics.Rect r4 = r4.f225     // Catch:{ Exception -> 0x0507 }
            boolean r2 = r2.equals(r4)     // Catch:{ Exception -> 0x0507 }
            if (r2 == 0) goto L_0x0359
            r2 = 64
            goto L_0x035b
        L_0x0359:
            r2 = 79
        L_0x035b:
            r4 = 79
            if (r2 == r4) goto L_0x036c
            double r4 = r11.f226     // Catch:{ Exception -> 0x0507 }
            com.moat.analytics.mobile.aol.u$c r2 = r1.f216     // Catch:{ Exception -> 0x0507 }
            double r8 = r2.f226     // Catch:{ Exception -> 0x0507 }
            int r2 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r2 == 0) goto L_0x036a
            goto L_0x036c
        L_0x036a:
            r5 = 0
            goto L_0x0382
        L_0x036c:
            r1.f216 = r11     // Catch:{ Exception -> 0x0507 }
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x0507 }
            com.moat.analytics.mobile.aol.u$c r4 = r1.f216     // Catch:{ Exception -> 0x0507 }
            android.graphics.Rect r4 = r4.f225     // Catch:{ Exception -> 0x0507 }
            android.graphics.Rect r4 = m190(r4, r7)     // Catch:{ Exception -> 0x0507 }
            java.util.HashMap r4 = m195(r4)     // Catch:{ Exception -> 0x0507 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0507 }
            r1.f214 = r2     // Catch:{ Exception -> 0x0507 }
            r5 = 1
        L_0x0382:
            java.lang.String r2 = "coveredPercent"
            double r8 = r11.f226     // Catch:{ Exception -> 0x0507 }
            java.lang.Double r4 = java.lang.Double.valueOf(r8)     // Catch:{ Exception -> 0x0507 }
            r3.put(r2, r4)     // Catch:{ Exception -> 0x0507 }
            org.json.JSONObject r2 = r1.f213     // Catch:{ Exception -> 0x0507 }
            if (r2 == 0) goto L_0x0393
            r2 = 1
            goto L_0x0394
        L_0x0393:
            r2 = 0
        L_0x0394:
            if (r2 == 0) goto L_0x03bc
            int r2 = com.moat.analytics.mobile.aol.u.f209
            int r2 = r2 + 35
            int r4 = r2 % 128
            com.moat.analytics.mobile.aol.u.f208 = r4
            int r2 = r2 % 2
            if (r2 == 0) goto L_0x03a5
            r2 = 38
            goto L_0x03a7
        L_0x03a5:
            r2 = 52
        L_0x03a7:
            r4 = 52
            if (r2 == r4) goto L_0x03b4
            android.graphics.Rect r2 = r1.f215     // Catch:{ Exception -> 0x0507 }
            boolean r2 = r12.equals(r2)     // Catch:{ Exception -> 0x0507 }
            if (r2 != 0) goto L_0x03ce
            goto L_0x03bc
        L_0x03b4:
            android.graphics.Rect r2 = r1.f215     // Catch:{ Exception -> 0x0507 }
            boolean r2 = r12.equals(r2)     // Catch:{ Exception -> 0x0507 }
            if (r2 != 0) goto L_0x03ce
        L_0x03bc:
            r1.f215 = r12     // Catch:{ Exception -> 0x0507 }
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x0507 }
            android.graphics.Rect r4 = m190(r12, r7)     // Catch:{ Exception -> 0x0507 }
            java.util.HashMap r4 = m195(r4)     // Catch:{ Exception -> 0x0507 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0507 }
            r1.f213 = r2     // Catch:{ Exception -> 0x0507 }
            r5 = 1
        L_0x03ce:
            org.json.JSONObject r2 = r1.f211     // Catch:{ Exception -> 0x0507 }
            if (r2 == 0) goto L_0x03d7
            r2 = 57
            r10 = 57
            goto L_0x03db
        L_0x03d7:
            r10 = 61
            r2 = 57
        L_0x03db:
            if (r10 == r2) goto L_0x03e0
            r4 = r20
            goto L_0x03f5
        L_0x03e0:
            int r2 = com.moat.analytics.mobile.aol.u.f208
            r4 = 21
            int r2 = r2 + r4
            int r4 = r2 % 128
            com.moat.analytics.mobile.aol.u.f209 = r4
            int r2 = r2 % 2
            android.graphics.Rect r2 = r1.f212     // Catch:{ Exception -> 0x0507 }
            r4 = r20
            boolean r2 = r4.equals(r2)     // Catch:{ Exception -> 0x0507 }
            if (r2 != 0) goto L_0x0407
        L_0x03f5:
            r1.f212 = r4     // Catch:{ Exception -> 0x0507 }
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x0507 }
            android.graphics.Rect r4 = m190(r4, r7)     // Catch:{ Exception -> 0x0507 }
            java.util.HashMap r4 = m195(r4)     // Catch:{ Exception -> 0x0507 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0507 }
            r1.f211 = r2     // Catch:{ Exception -> 0x0507 }
            r5 = 1
        L_0x0407:
            java.util.Map<java.lang.String, java.lang.Object> r2 = r1.f218     // Catch:{ Exception -> 0x0507 }
            if (r2 == 0) goto L_0x0410
            r14 = 95
            r2 = 93
            goto L_0x0414
        L_0x0410:
            r2 = 93
            r14 = 93
        L_0x0414:
            if (r14 == r2) goto L_0x0428
            java.util.Map<java.lang.String, java.lang.Object> r2 = r1.f218     // Catch:{ Exception -> 0x0507 }
            boolean r2 = r3.equals(r2)     // Catch:{ Exception -> 0x0507 }
            if (r2 != 0) goto L_0x0421
            r2 = 49
            goto L_0x0423
        L_0x0421:
            r2 = 30
        L_0x0423:
            r4 = 49
            if (r2 == r4) goto L_0x0428
            goto L_0x042b
        L_0x0428:
            r1.f218 = r3     // Catch:{ Exception -> 0x0507 }
            r5 = 1
        L_0x042b:
            com.moat.analytics.mobile.aol.n r2 = com.moat.analytics.mobile.aol.n.m118()     // Catch:{ Exception -> 0x0507 }
            android.location.Location r2 = r2.m127()     // Catch:{ Exception -> 0x0507 }
            android.location.Location r3 = r1.f210     // Catch:{ Exception -> 0x0507 }
            boolean r3 = com.moat.analytics.mobile.aol.n.m117(r2, r3)     // Catch:{ Exception -> 0x0507 }
            if (r3 != 0) goto L_0x0440
            r11 = 29
            r3 = 91
            goto L_0x0444
        L_0x0440:
            r3 = 91
            r11 = 91
        L_0x0444:
            if (r11 == r3) goto L_0x0449
            r1.f210 = r2     // Catch:{ Exception -> 0x0507 }
            r5 = 1
        L_0x0449:
            if (r5 == 0) goto L_0x044d
            r3 = 0
            goto L_0x044e
        L_0x044d:
            r3 = 1
        L_0x044e:
            if (r3 == 0) goto L_0x0452
            goto L_0x0506
        L_0x0452:
            int r3 = com.moat.analytics.mobile.aol.u.f209
            r4 = 21
            int r3 = r3 + r4
            int r4 = r3 % 128
            com.moat.analytics.mobile.aol.u.f208 = r4
            int r3 = r3 % 2
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x0507 }
            java.util.Map<java.lang.String, java.lang.Object> r4 = r1.f218     // Catch:{ Exception -> 0x0507 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0507 }
            java.lang.String r4 = "screen"
            org.json.JSONObject r5 = r1.f213     // Catch:{ Exception -> 0x0507 }
            r3.accumulate(r4, r5)     // Catch:{ Exception -> 0x0507 }
            java.lang.String r4 = "view"
            org.json.JSONObject r5 = r1.f211     // Catch:{ Exception -> 0x0507 }
            r3.accumulate(r4, r5)     // Catch:{ Exception -> 0x0507 }
            java.lang.String r4 = "visible"
            org.json.JSONObject r5 = r1.f214     // Catch:{ Exception -> 0x0507 }
            r3.accumulate(r4, r5)     // Catch:{ Exception -> 0x0507 }
            java.lang.String r4 = "maybe"
            org.json.JSONObject r5 = r1.f214     // Catch:{ Exception -> 0x0507 }
            r3.accumulate(r4, r5)     // Catch:{ Exception -> 0x0507 }
            java.lang.String r4 = "visiblePercent"
            com.moat.analytics.mobile.aol.u$c r5 = r1.f216     // Catch:{ Exception -> 0x0507 }
            double r5 = r5.f224     // Catch:{ Exception -> 0x0507 }
            java.lang.Double r5 = java.lang.Double.valueOf(r5)     // Catch:{ Exception -> 0x0507 }
            r3.accumulate(r4, r5)     // Catch:{ Exception -> 0x0507 }
            if (r2 == 0) goto L_0x0492
            r18 = 0
            goto L_0x0494
        L_0x0492:
            r18 = 1
        L_0x0494:
            if (r18 == 0) goto L_0x0497
            goto L_0x0500
        L_0x0497:
            java.lang.String r4 = "location"
            if (r2 != 0) goto L_0x049e
            r5 = 54
            goto L_0x04a0
        L_0x049e:
            r5 = 92
        L_0x04a0:
            r6 = 54
            if (r5 == r6) goto L_0x04de
            java.util.HashMap r15 = new java.util.HashMap     // Catch:{ Exception -> 0x0507 }
            r15.<init>()     // Catch:{ Exception -> 0x0507 }
            java.lang.String r5 = "latitude"
            double r6 = r2.getLatitude()     // Catch:{ Exception -> 0x0507 }
            java.lang.String r6 = java.lang.Double.toString(r6)     // Catch:{ Exception -> 0x0507 }
            r15.put(r5, r6)     // Catch:{ Exception -> 0x0507 }
            java.lang.String r5 = "longitude"
            double r6 = r2.getLongitude()     // Catch:{ Exception -> 0x0507 }
            java.lang.String r6 = java.lang.Double.toString(r6)     // Catch:{ Exception -> 0x0507 }
            r15.put(r5, r6)     // Catch:{ Exception -> 0x0507 }
            java.lang.String r5 = "timestamp"
            long r6 = r2.getTime()     // Catch:{ Exception -> 0x0507 }
            java.lang.String r6 = java.lang.Long.toString(r6)     // Catch:{ Exception -> 0x0507 }
            r15.put(r5, r6)     // Catch:{ Exception -> 0x0507 }
            java.lang.String r5 = "horizontalAccuracy"
            float r2 = r2.getAccuracy()     // Catch:{ Exception -> 0x0507 }
            java.lang.String r2 = java.lang.Float.toString(r2)     // Catch:{ Exception -> 0x0507 }
            r15.put(r5, r2)     // Catch:{ Exception -> 0x0507 }
            goto L_0x04e9
        L_0x04de:
            int r2 = com.moat.analytics.mobile.aol.u.f208
            int r2 = r2 + 25
            int r5 = r2 % 128
            com.moat.analytics.mobile.aol.u.f209 = r5
            int r2 = r2 % 2
            r15 = 0
        L_0x04e9:
            if (r15 != 0) goto L_0x04f0
            r2 = 28
            r10 = 28
            goto L_0x04f4
        L_0x04f0:
            r10 = 43
            r2 = 28
        L_0x04f4:
            if (r10 == r2) goto L_0x04fc
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x0507 }
            r2.<init>(r15)     // Catch:{ Exception -> 0x0507 }
            goto L_0x04fd
        L_0x04fc:
            r2 = 0
        L_0x04fd:
            r3.accumulate(r4, r2)     // Catch:{ Exception -> 0x0507 }
        L_0x0500:
            java.lang.String r4 = r3.toString()     // Catch:{ Exception -> 0x0507 }
            r1.f217 = r4     // Catch:{ Exception -> 0x005c }
        L_0x0506:
            return
        L_0x0507:
            r0 = move-exception
            r2 = r0
            r4 = r19
            goto L_0x0510
        L_0x050c:
            r0 = move-exception
            r19 = r4
        L_0x050f:
            r2 = r0
        L_0x0510:
            com.moat.analytics.mobile.aol.o.m132(r2)
            r1.f217 = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.aol.u.m198(java.lang.String, android.view.View):void");
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static float m188(View view) {
        float alpha = view.getAlpha();
        while (true) {
            boolean z = false;
            if (view == null) {
                break;
            }
            if (!(view.getParent() != null)) {
                break;
            }
            if ((((double) alpha) != 0.0d ? 'G' : 13) != 13) {
                if (!(view.getParent() instanceof View)) {
                    z = true;
                }
                if (z) {
                    break;
                }
                int i = f209 + 41;
                f208 = i % 128;
                if (i % 2 != 0) {
                }
                alpha *= ((View) view.getParent()).getAlpha();
                view = (View) view.getParent();
            } else {
                break;
            }
        }
        return alpha;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static Rect m191(View view) {
        return (view != null ? 'R' : 'G') != 'R' ? new Rect(0, 0, 0, 0) : m196(view);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0093, code lost:
        if ((r6.getBackground().getAlpha() == 0) != true) goto L_0x00a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00a6, code lost:
        if ((r6.getBackground().getAlpha() == 0 ? '\'' : 11) != 11) goto L_0x006a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0104 A[EDGE_INSN: B:114:0x0104->B:79:0x0104 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0036 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00e5  */
    /* renamed from: ˏ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m197(com.moat.analytics.mobile.aol.u.a r10, android.graphics.Rect r11, com.moat.analytics.mobile.aol.u.b r12) {
        /*
            android.view.View r0 = r10.f220
            boolean r1 = r0.isShown()
            r2 = 0
            r3 = 1
            if (r1 == 0) goto L_0x000c
            r1 = r3
            goto L_0x000d
        L_0x000c:
            r1 = r2
        L_0x000d:
            r4 = 2
            if (r1 == 0) goto L_0x0033
            float r0 = r0.getAlpha()
            double r0 = (double) r0
            r5 = 0
            int r7 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            r0 = 42
            if (r7 <= 0) goto L_0x0020
            r1 = 86
            goto L_0x0021
        L_0x0020:
            r1 = r0
        L_0x0021:
            if (r1 == r0) goto L_0x0033
            int r0 = com.moat.analytics.mobile.aol.u.f208
            int r0 = r0 + 111
            int r1 = r0 % 128
            com.moat.analytics.mobile.aol.u.f209 = r1
            int r0 = r0 % r4
            if (r0 != 0) goto L_0x0030
            r0 = r2
            goto L_0x0031
        L_0x0030:
            r0 = r3
        L_0x0031:
            r0 = r3
            goto L_0x0034
        L_0x0033:
            r0 = r2
        L_0x0034:
            if (r0 != 0) goto L_0x0037
            return
        L_0x0037:
            android.view.View r0 = r10.f220
            boolean r0 = r0 instanceof android.view.ViewGroup
            if (r0 == 0) goto L_0x003f
            r0 = r3
            goto L_0x0040
        L_0x003f:
            r0 = r2
        L_0x0040:
            r1 = 3
            if (r0 == 0) goto L_0x0103
            int r0 = com.moat.analytics.mobile.aol.u.f208
            r5 = 23
            int r0 = r0 + r5
            int r6 = r0 % 128
            com.moat.analytics.mobile.aol.u.f209 = r6
            int r0 = r0 % r4
            java.lang.Class<android.view.ViewGroup> r0 = android.view.ViewGroup.class
            android.view.View r6 = r10.f220
            java.lang.Class r6 = r6.getClass()
            java.lang.Class r6 = r6.getSuperclass()
            boolean r0 = r0.equals(r6)
            android.view.View r6 = r10.f220
            int r7 = android.os.Build.VERSION.SDK_INT
            r8 = 19
            if (r7 < r8) goto L_0x0067
            r7 = r2
            goto L_0x0068
        L_0x0067:
            r7 = r3
        L_0x0068:
            if (r7 == 0) goto L_0x006c
        L_0x006a:
            r5 = r3
            goto L_0x00aa
        L_0x006c:
            android.graphics.drawable.Drawable r7 = r6.getBackground()
            if (r7 == 0) goto L_0x0073
            goto L_0x0074
        L_0x0073:
            r5 = r1
        L_0x0074:
            if (r5 == r1) goto L_0x006a
            int r5 = com.moat.analytics.mobile.aol.u.f209
            int r5 = r5 + 93
            int r7 = r5 % 128
            com.moat.analytics.mobile.aol.u.f208 = r7
            int r5 = r5 % r4
            if (r5 == 0) goto L_0x0083
            r5 = r2
            goto L_0x0084
        L_0x0083:
            r5 = r3
        L_0x0084:
            if (r5 == 0) goto L_0x0096
            android.graphics.drawable.Drawable r5 = r6.getBackground()
            int r5 = r5.getAlpha()
            if (r5 != 0) goto L_0x0092
            r5 = r3
            goto L_0x0093
        L_0x0092:
            r5 = r2
        L_0x0093:
            if (r5 == r3) goto L_0x006a
            goto L_0x00a9
        L_0x0096:
            android.graphics.drawable.Drawable r5 = r6.getBackground()
            int r5 = r5.getAlpha()
            r6 = 11
            if (r5 != 0) goto L_0x00a5
            r5 = 39
            goto L_0x00a6
        L_0x00a5:
            r5 = r6
        L_0x00a6:
            if (r5 == r6) goto L_0x00a9
            goto L_0x006a
        L_0x00a9:
            r5 = r2
        L_0x00aa:
            if (r0 == 0) goto L_0x00ae
            r0 = r2
            goto L_0x00af
        L_0x00ae:
            r0 = r3
        L_0x00af:
            if (r0 == 0) goto L_0x00b2
            goto L_0x00d4
        L_0x00b2:
            int r0 = com.moat.analytics.mobile.aol.u.f209
            int r0 = r0 + 111
            int r6 = r0 % 128
            com.moat.analytics.mobile.aol.u.f208 = r6
            int r0 = r0 % r4
            if (r5 == 0) goto L_0x00bf
            r0 = r2
            goto L_0x00c0
        L_0x00bf:
            r0 = r3
        L_0x00c0:
            if (r0 == r3) goto L_0x00d4
            int r0 = com.moat.analytics.mobile.aol.u.f209
            int r0 = r0 + r3
            int r5 = r0 % 128
            com.moat.analytics.mobile.aol.u.f208 = r5
            int r0 = r0 % r4
            r5 = 75
            if (r0 == 0) goto L_0x00d0
            r0 = r5
            goto L_0x00d2
        L_0x00d0:
            r0 = 60
        L_0x00d2:
            r0 = r2
            goto L_0x00d5
        L_0x00d4:
            r0 = r3
        L_0x00d5:
            android.view.View r5 = r10.f220
            android.view.ViewGroup r5 = (android.view.ViewGroup) r5
            int r6 = r5.getChildCount()
            r7 = r2
        L_0x00de:
            if (r7 >= r6) goto L_0x00e2
            r8 = r3
            goto L_0x00e3
        L_0x00e2:
            r8 = r2
        L_0x00e3:
            if (r8 == 0) goto L_0x0104
            int r8 = r12.f223
            int r8 = r8 + r3
            r12.f223 = r8
            r9 = 500(0x1f4, float:7.0E-43)
            if (r8 <= r9) goto L_0x00ef
            return
        L_0x00ef:
            com.moat.analytics.mobile.aol.u$a r8 = new com.moat.analytics.mobile.aol.u$a
            android.view.View r9 = r5.getChildAt(r7)
            r8.<init>(r9, r10)
            m197(r8, r11, r12)
            boolean r8 = r12.f221
            if (r8 == 0) goto L_0x0100
            return
        L_0x0100:
            int r7 = r7 + 1
            goto L_0x00de
        L_0x0103:
            r0 = r3
        L_0x0104:
            if (r0 == 0) goto L_0x0108
            r0 = r3
            goto L_0x0109
        L_0x0108:
            r0 = r2
        L_0x0109:
            if (r0 == r3) goto L_0x010d
            goto L_0x01af
        L_0x010d:
            int r0 = com.moat.analytics.mobile.aol.u.f208
            int r0 = r0 + 9
            int r5 = r0 % 128
            com.moat.analytics.mobile.aol.u.f209 = r5
            int r0 = r0 % r4
            android.graphics.Rect r0 = r10.f219
            boolean r5 = r0.setIntersect(r11, r0)
            r6 = 7
            if (r5 == 0) goto L_0x0121
            r5 = r6
            goto L_0x0123
        L_0x0121:
            r5 = 68
        L_0x0123:
            if (r5 == r6) goto L_0x0127
            goto L_0x01af
        L_0x0127:
            int r5 = android.os.Build.VERSION.SDK_INT
            r6 = 22
            if (r5 < r6) goto L_0x012f
            r5 = r2
            goto L_0x0130
        L_0x012f:
            r5 = r3
        L_0x0130:
            if (r5 == r3) goto L_0x014f
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>(r2, r2, r2, r2)
            android.view.View r5 = r10.f220
            boolean r5 = m193(r5, r0)
            if (r5 == 0) goto L_0x0141
            r5 = r3
            goto L_0x0142
        L_0x0141:
            r5 = r2
        L_0x0142:
            if (r5 == r3) goto L_0x0145
            return
        L_0x0145:
            android.graphics.Rect r5 = r10.f219
            boolean r0 = r5.setIntersect(r0, r5)
            if (r0 != 0) goto L_0x014e
            return
        L_0x014e:
            r0 = r5
        L_0x014f:
            com.moat.analytics.mobile.aol.t r5 = com.moat.analytics.mobile.aol.t.m176()
            boolean r5 = r5.f184
            if (r5 == 0) goto L_0x0159
            r5 = r3
            goto L_0x015a
        L_0x0159:
            r5 = r2
        L_0x015a:
            if (r5 == 0) goto L_0x0194
            int r5 = com.moat.analytics.mobile.aol.u.f209
            int r5 = r5 + 41
            int r6 = r5 % 128
            com.moat.analytics.mobile.aol.u.f208 = r6
            int r5 = r5 % r4
            java.lang.String r5 = "VisibilityInfo"
            android.view.View r6 = r10.f220
            java.util.Locale r7 = java.util.Locale.ROOT
            java.lang.String r8 = "Covered by %s-%s alpha=%f"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            android.view.View r9 = r10.f220
            java.lang.Class r9 = r9.getClass()
            java.lang.String r9 = r9.getName()
            r1[r2] = r9
            java.lang.String r9 = r0.toString()
            r1[r3] = r9
            android.view.View r10 = r10.f220
            float r10 = r10.getAlpha()
            java.lang.Float r10 = java.lang.Float.valueOf(r10)
            r1[r4] = r10
            java.lang.String r10 = java.lang.String.format(r7, r8, r1)
            com.moat.analytics.mobile.aol.a.m9(r5, r6, r10)
        L_0x0194:
            java.util.Set<android.graphics.Rect> r10 = r12.f222
            r10.add(r0)
            boolean r10 = r0.contains(r11)
            if (r10 == 0) goto L_0x01a0
            goto L_0x01a1
        L_0x01a0:
            r2 = r3
        L_0x01a1:
            if (r2 == 0) goto L_0x01a4
            goto L_0x01af
        L_0x01a4:
            int r10 = com.moat.analytics.mobile.aol.u.f209
            int r10 = r10 + 97
            int r11 = r10 % 128
            com.moat.analytics.mobile.aol.u.f208 = r11
            int r10 = r10 % r4
            r12.f221 = r3
        L_0x01af:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.aol.u.m197(com.moat.analytics.mobile.aol.u$a, android.graphics.Rect, com.moat.analytics.mobile.aol.u$b):void");
    }

    static class b {

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f221 = false;

        /* renamed from: ˋ  reason: contains not printable characters */
        final Set<Rect> f222 = new HashSet();

        /* renamed from: ˎ  reason: contains not printable characters */
        int f223 = 0;

        b() {
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v14, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v12, resolved type: android.view.View} */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0181, code lost:
        if ((r8.getZ() >= r5.getZ() ? 15 : '&') != '&') goto L_0x0183;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x01bf, code lost:
        r2 = com.moat.analytics.mobile.aol.u.f209 + com.millennialmedia.internal.AdPlacementReporter.PLAYLIST_TIMED_OUT_IN_CACHE;
        com.moat.analytics.mobile.aol.u.f208 = r2 % 128;
        r2 = r2 % 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:?, code lost:
        com.moat.analytics.mobile.aol.a.m8(3, "VisibilityInfo", null, "Short-circuiting cover retrieval, reached max");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0034, code lost:
        r4 = com.moat.analytics.mobile.aol.u.f209 + 11;
        com.moat.analytics.mobile.aol.u.f208 = r4 % 128;
        r4 = r4 % 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        com.moat.analytics.mobile.aol.a.m8(3, "VisibilityInfo", null, "Short-circuiting chain retrieval, reached max");
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0186 A[Catch:{ Exception -> 0x01e9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0188 A[Catch:{ Exception -> 0x01e9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x018b A[Catch:{ Exception -> 0x01e9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x018e A[Catch:{ Exception -> 0x01e9 }] */
    @android.support.annotation.VisibleForTesting
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.moat.analytics.mobile.aol.u.b m192(android.graphics.Rect r19, @android.support.annotation.NonNull android.view.View r20) {
        /*
            com.moat.analytics.mobile.aol.u$b r1 = new com.moat.analytics.mobile.aol.u$b
            r1.<init>()
            java.util.ArrayDeque r2 = new java.util.ArrayDeque     // Catch:{ Exception -> 0x01e9 }
            r2.<init>()     // Catch:{ Exception -> 0x01e9 }
            r4 = r20
            r5 = 0
        L_0x000d:
            android.view.ViewParent r6 = r4.getParent()     // Catch:{ Exception -> 0x01e9 }
            r7 = 1
            if (r6 != 0) goto L_0x0016
            r6 = 0
            goto L_0x0017
        L_0x0016:
            r6 = r7
        L_0x0017:
            r8 = 3
            r10 = 0
            r11 = 62
            r12 = 21
            if (r6 == r7) goto L_0x002a
            android.view.View r6 = r20.getRootView()     // Catch:{ Exception -> 0x01e9 }
            if (r4 != r6) goto L_0x0027
            r6 = 0
            goto L_0x0028
        L_0x0027:
            r6 = r7
        L_0x0028:
            if (r6 == r7) goto L_0x005e
        L_0x002a:
            int r5 = r5 + r7
            r6 = 50
            if (r5 <= r6) goto L_0x0031
            r6 = 7
            goto L_0x0032
        L_0x0031:
            r6 = r11
        L_0x0032:
            if (r6 == r11) goto L_0x0046
            int r4 = com.moat.analytics.mobile.aol.u.f209
            int r4 = r4 + 11
            int r5 = r4 % 128
            com.moat.analytics.mobile.aol.u.f208 = r5
            int r4 = r4 % 2
            java.lang.String r4 = "VisibilityInfo"
            java.lang.String r5 = "Short-circuiting chain retrieval, reached max"
            com.moat.analytics.mobile.aol.a.m8(r8, r4, r10, r5)     // Catch:{ Exception -> 0x01e9 }
            goto L_0x005e
        L_0x0046:
            r2.add(r4)     // Catch:{ Exception -> 0x01e9 }
            android.view.ViewParent r6 = r4.getParent()     // Catch:{ Exception -> 0x01e9 }
            boolean r6 = r6 instanceof android.view.View     // Catch:{ Exception -> 0x01e9 }
            if (r6 == 0) goto L_0x0054
            r6 = 97
            goto L_0x0055
        L_0x0054:
            r6 = r12
        L_0x0055:
            if (r6 == r12) goto L_0x005e
            android.view.ViewParent r4 = r4.getParent()     // Catch:{ Exception -> 0x01e9 }
            android.view.View r4 = (android.view.View) r4     // Catch:{ Exception -> 0x01e9 }
            goto L_0x000d
        L_0x005e:
            boolean r4 = r2.isEmpty()     // Catch:{ Exception -> 0x01e9 }
            r5 = 67
            r6 = 53
            if (r4 == 0) goto L_0x006a
            r4 = r5
            goto L_0x006b
        L_0x006a:
            r4 = r6
        L_0x006b:
            if (r4 == r5) goto L_0x01de
            java.lang.String r4 = "VisibilityInfo"
            java.lang.String r5 = "starting covering rect search"
            r13 = r20
            com.moat.analytics.mobile.aol.a.m9(r4, r13, r5)     // Catch:{ Exception -> 0x01e9 }
            r4 = r10
        L_0x0077:
            boolean r5 = r2.isEmpty()     // Catch:{ Exception -> 0x01e9 }
            r13 = 47
            if (r5 != 0) goto L_0x0082
            r5 = 16
            goto L_0x0083
        L_0x0082:
            r5 = r13
        L_0x0083:
            if (r5 == r13) goto L_0x01ee
            int r5 = com.moat.analytics.mobile.aol.u.f208
            r13 = 99
            int r5 = r5 + r13
            int r14 = r5 % 128
            com.moat.analytics.mobile.aol.u.f209 = r14
            int r5 = r5 % 2
            r14 = 92
            if (r5 != 0) goto L_0x0096
            r5 = r14
            goto L_0x0098
        L_0x0096:
            r5 = 14
        L_0x0098:
            r15 = 14
            if (r5 == r15) goto L_0x00b5
            java.lang.Object r5 = r2.pollLast()     // Catch:{ Exception -> 0x01e9 }
            android.view.View r5 = (android.view.View) r5     // Catch:{ Exception -> 0x01e9 }
            com.moat.analytics.mobile.aol.u$a r15 = new com.moat.analytics.mobile.aol.u$a     // Catch:{ Exception -> 0x01e9 }
            r15.<init>(r5, r4)     // Catch:{ Exception -> 0x01e9 }
            android.view.ViewParent r16 = r5.getParent()     // Catch:{ Exception -> 0x01e9 }
            if (r16 == 0) goto L_0x00af
            r3 = r11
            goto L_0x00b0
        L_0x00af:
            r3 = r6
        L_0x00b0:
            if (r3 == r6) goto L_0x00b3
            goto L_0x00d1
        L_0x00b3:
            r4 = r15
            goto L_0x0077
        L_0x00b5:
            java.lang.Object r3 = r2.pollLast()     // Catch:{ Exception -> 0x01e9 }
            r5 = r3
            android.view.View r5 = (android.view.View) r5     // Catch:{ Exception -> 0x01e9 }
            com.moat.analytics.mobile.aol.u$a r3 = new com.moat.analytics.mobile.aol.u$a     // Catch:{ Exception -> 0x01e9 }
            r3.<init>(r5, r4)     // Catch:{ Exception -> 0x01e9 }
            android.view.ViewParent r15 = r5.getParent()     // Catch:{ Exception -> 0x01e9 }
            if (r15 == 0) goto L_0x00ca
            r15 = 35
            goto L_0x00cc
        L_0x00ca:
            r15 = 24
        L_0x00cc:
            r11 = 24
            if (r15 == r11) goto L_0x01d3
            r15 = r3
        L_0x00d1:
            android.view.ViewParent r3 = r5.getParent()     // Catch:{ Exception -> 0x01e9 }
            boolean r3 = r3 instanceof android.view.ViewGroup     // Catch:{ Exception -> 0x01e9 }
            if (r3 == 0) goto L_0x00db
            r3 = 0
            goto L_0x00dc
        L_0x00db:
            r3 = r7
        L_0x00dc:
            if (r3 == 0) goto L_0x00df
            goto L_0x0103
        L_0x00df:
            int r3 = com.moat.analytics.mobile.aol.u.f209
            int r3 = r3 + r6
            int r11 = r3 % 128
            com.moat.analytics.mobile.aol.u.f208 = r11
            int r3 = r3 % 2
            android.view.ViewParent r3 = r5.getParent()     // Catch:{ Exception -> 0x01e9 }
            android.view.ViewGroup r3 = (android.view.ViewGroup) r3     // Catch:{ Exception -> 0x01e9 }
            int r11 = r3.getChildCount()     // Catch:{ Exception -> 0x01e9 }
            r6 = 0
            r16 = 0
        L_0x00f5:
            if (r6 >= r11) goto L_0x00fc
            r17 = 80
        L_0x00f9:
            r8 = r17
            goto L_0x00ff
        L_0x00fc:
            r17 = 94
            goto L_0x00f9
        L_0x00ff:
            r10 = 80
            if (r8 == r10) goto L_0x010a
        L_0x0103:
            r4 = r15
            r6 = 53
            r8 = 3
            r10 = 0
            goto L_0x01da
        L_0x010a:
            int r8 = r1.f223     // Catch:{ Exception -> 0x01e9 }
            r10 = 500(0x1f4, float:7.0E-43)
            if (r8 < r10) goto L_0x0113
            r8 = 98
            goto L_0x0115
        L_0x0113:
            r8 = 20
        L_0x0115:
            r10 = 98
            if (r8 == r10) goto L_0x01bf
            android.view.View r8 = r3.getChildAt(r6)     // Catch:{ Exception -> 0x01e9 }
            if (r8 != r5) goto L_0x0121
            r10 = 0
            goto L_0x0122
        L_0x0121:
            r10 = r7
        L_0x0122:
            if (r10 == r7) goto L_0x012a
            r8 = r19
            r16 = r7
            goto L_0x01a2
        L_0x012a:
            int r10 = r1.f223     // Catch:{ Exception -> 0x01e9 }
            int r10 = r10 + r7
            r1.f223 = r10     // Catch:{ Exception -> 0x01e9 }
            r10 = 37
            if (r16 == 0) goto L_0x0135
            r9 = r10
            goto L_0x0139
        L_0x0135:
            r17 = 20
            r9 = r17
        L_0x0139:
            if (r9 == r10) goto L_0x0163
            int r9 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x01e9 }
            if (r9 < r12) goto L_0x0142
            r9 = 73
            goto L_0x0143
        L_0x0142:
            r9 = r10
        L_0x0143:
            if (r9 == r10) goto L_0x0161
            int r9 = com.moat.analytics.mobile.aol.u.f208
            int r9 = r9 + 23
            int r10 = r9 % 128
            com.moat.analytics.mobile.aol.u.f209 = r10
            int r9 = r9 % 2
            float r9 = r8.getZ()     // Catch:{ Exception -> 0x01e9 }
            float r10 = r5.getZ()     // Catch:{ Exception -> 0x01e9 }
            int r9 = (r9 > r10 ? 1 : (r9 == r10 ? 0 : -1))
            if (r9 <= 0) goto L_0x015d
            r9 = 0
            goto L_0x015e
        L_0x015d:
            r9 = r7
        L_0x015e:
            if (r9 == r7) goto L_0x0161
            goto L_0x0183
        L_0x0161:
            r9 = 0
            goto L_0x0184
        L_0x0163:
            int r9 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x01e9 }
            if (r9 < r12) goto L_0x0169
            r9 = r14
            goto L_0x016b
        L_0x0169:
            r9 = 71
        L_0x016b:
            if (r9 == r14) goto L_0x016e
            goto L_0x0183
        L_0x016e:
            float r9 = r8.getZ()     // Catch:{ Exception -> 0x01e9 }
            float r10 = r5.getZ()     // Catch:{ Exception -> 0x01e9 }
            int r9 = (r9 > r10 ? 1 : (r9 == r10 ? 0 : -1))
            if (r9 < 0) goto L_0x017d
            r9 = 15
            goto L_0x017f
        L_0x017d:
            r9 = 38
        L_0x017f:
            r10 = 38
            if (r9 == r10) goto L_0x0161
        L_0x0183:
            r9 = r7
        L_0x0184:
            if (r9 == 0) goto L_0x0188
            r9 = 0
            goto L_0x0189
        L_0x0188:
            r9 = r7
        L_0x0189:
            if (r9 == 0) goto L_0x018e
            r8 = r19
            goto L_0x01a2
        L_0x018e:
            com.moat.analytics.mobile.aol.u$a r9 = new com.moat.analytics.mobile.aol.u$a     // Catch:{ Exception -> 0x01e9 }
            r9.<init>(r8, r4)     // Catch:{ Exception -> 0x01e9 }
            r8 = r19
            m197(r9, r8, r1)     // Catch:{ Exception -> 0x01e9 }
            boolean r9 = r1.f221     // Catch:{ Exception -> 0x01e9 }
            if (r9 == 0) goto L_0x019e
            r9 = r13
            goto L_0x01a0
        L_0x019e:
            r9 = 49
        L_0x01a0:
            if (r9 == r13) goto L_0x01a8
        L_0x01a2:
            int r6 = r6 + 1
            r8 = 3
            r10 = 0
            goto L_0x00f5
        L_0x01a8:
            int r2 = com.moat.analytics.mobile.aol.u.f209
            int r2 = r2 + r13
            int r3 = r2 % 128
            com.moat.analytics.mobile.aol.u.f208 = r3
            int r2 = r2 % 2
            if (r2 == 0) goto L_0x01b8
            r4 = 97
            r9 = 97
            goto L_0x01bc
        L_0x01b8:
            r9 = 41
            r4 = 97
        L_0x01bc:
            if (r9 == r4) goto L_0x01be
        L_0x01be:
            return r1
        L_0x01bf:
            int r2 = com.moat.analytics.mobile.aol.u.f209
            int r2 = r2 + 113
            int r3 = r2 % 128
            com.moat.analytics.mobile.aol.u.f208 = r3
            int r2 = r2 % 2
            java.lang.String r2 = "VisibilityInfo"
            java.lang.String r3 = "Short-circuiting cover retrieval, reached max"
            r5 = 0
            r6 = 3
            com.moat.analytics.mobile.aol.a.m8(r6, r2, r5, r3)     // Catch:{ Exception -> 0x01e9 }
            goto L_0x01ee
        L_0x01d3:
            r6 = r8
            r8 = r19
            r4 = r3
            r8 = r6
            r6 = 53
        L_0x01da:
            r11 = 62
            goto L_0x0077
        L_0x01de:
            int r2 = com.moat.analytics.mobile.aol.u.f209
            int r2 = r2 + 83
            int r3 = r2 % 128
            com.moat.analytics.mobile.aol.u.f208 = r3
            int r2 = r2 % 2
            return r1
        L_0x01e9:
            r0 = move-exception
            r2 = r0
            com.moat.analytics.mobile.aol.o.m132(r2)
        L_0x01ee:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.aol.u.m192(android.graphics.Rect, android.view.View):com.moat.analytics.mobile.aol.u$b");
    }

    @VisibleForTesting
    /* renamed from: ˋ  reason: contains not printable characters */
    private static int m189(Rect rect, Set<Rect> set) {
        Rect rect2;
        if (set.isEmpty()) {
            return 0;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(set);
        Collections.sort(arrayList, new Comparator<Rect>() {
            public final /* synthetic */ int compare(Object obj, Object obj2) {
                return Integer.valueOf(((Rect) obj).top).compareTo(Integer.valueOf(((Rect) obj2).top));
            }
        });
        ArrayList arrayList2 = new ArrayList();
        Iterator it = arrayList.iterator();
        while (true) {
            if (it.hasNext() ? true : true) {
                break;
            }
            int i = f208 + 87;
            f209 = i % 128;
            int i2 = i % 2;
            Rect rect3 = (Rect) it.next();
            arrayList2.add(Integer.valueOf(rect3.left));
            arrayList2.add(Integer.valueOf(rect3.right));
        }
        Collections.sort(arrayList2);
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if ((i3 < arrayList2.size() - 1 ? 'G' : 'C') != 'G') {
                return i4;
            }
            int i5 = f208 + 35;
            f209 = i5 % 128;
            if ((i5 % 2 == 0 ? 23 : '=') != '=') {
                if (((Integer) arrayList2.get(i3)).equals(arrayList2.get(i3 + 1))) {
                    i3++;
                }
            } else {
                if ((!((Integer) arrayList2.get(i3)).equals(arrayList2.get(i3 + 1)) ? '%' : '[') == '[') {
                    i3++;
                }
            }
            Rect rect4 = new Rect(((Integer) arrayList2.get(i3)).intValue(), rect.top, ((Integer) arrayList2.get(i3 + 1)).intValue(), rect.bottom);
            int i6 = rect.top;
            Iterator it2 = arrayList.iterator();
            while (true) {
                if (it2.hasNext() ? true : true) {
                    int i7 = f209 + 13;
                    f208 = i7 % 128;
                    if ((i7 % 2 != 0 ? 'M' : ']') != ']') {
                        rect2 = (Rect) it2.next();
                        if (!(Rect.intersects(rect2, rect4))) {
                            continue;
                        }
                    } else {
                        rect2 = (Rect) it2.next();
                        if (!Rect.intersects(rect2, rect4)) {
                            continue;
                        }
                    }
                    if ((rect2.bottom > i6 ? '5' : 16) != 16) {
                        int i8 = f209 + 125;
                        f208 = i8 % 128;
                        if (i8 % 2 != 0) {
                        }
                        i4 += rect4.width() * (rect2.bottom - Math.max(i6, rect2.top));
                        i6 = rect2.bottom;
                    }
                    if (rect2.bottom == rect4.bottom) {
                        break;
                    }
                } else {
                    break;
                }
            }
            i3++;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static Map<String, String> m195(Rect rect) {
        HashMap hashMap = new HashMap();
        hashMap.put("x", String.valueOf(rect.left));
        hashMap.put("y", String.valueOf(rect.top));
        hashMap.put("w", String.valueOf(rect.right - rect.left));
        hashMap.put("h", String.valueOf(rect.bottom - rect.top));
        return hashMap;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Rect m190(Rect rect, DisplayMetrics displayMetrics) {
        float f = displayMetrics.density;
        if ((f == 0.0f ? 'S' : '1') == 'S') {
            return rect;
        }
        return new Rect(Math.round(((float) rect.left) / f), Math.round(((float) rect.top) / f), Math.round(((float) rect.right) / f), Math.round(((float) rect.bottom) / f));
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private static boolean m193(View view, Rect rect) {
        if (!view.getGlobalVisibleRect(rect)) {
            return false;
        }
        int[] iArr = {ExploreByTouchHelper.INVALID_ID, ExploreByTouchHelper.INVALID_ID};
        view.getLocationInWindow(iArr);
        int[] iArr2 = {ExploreByTouchHelper.INVALID_ID, ExploreByTouchHelper.INVALID_ID};
        view.getLocationOnScreen(iArr2);
        rect.offset(iArr2[0] - iArr[0], iArr2[1] - iArr[1]);
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public static Rect m196(View view) {
        int[] iArr = {ExploreByTouchHelper.INVALID_ID, ExploreByTouchHelper.INVALID_ID};
        view.getLocationOnScreen(iArr);
        int i = iArr[0];
        int i2 = iArr[1];
        return new Rect(i, i2, view.getWidth() + i, view.getHeight() + i2);
    }
}
