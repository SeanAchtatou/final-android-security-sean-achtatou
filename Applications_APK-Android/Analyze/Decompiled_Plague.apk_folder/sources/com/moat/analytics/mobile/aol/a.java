package com.moat.analytics.mobile.aol;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.moat.analytics.mobile.aol.j;
import com.mopub.mobileads.VastExtensionXmlManager;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class a {

    /* renamed from: ˊ  reason: contains not printable characters */
    final String f17;

    /* renamed from: ˋ  reason: contains not printable characters */
    WebView f18;
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean f19;

    /* renamed from: ˏ  reason: contains not printable characters */
    j f20;

    /* renamed from: ॱ  reason: contains not printable characters */
    private final int f21;

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    static final class d extends Enum<d> {

        /* renamed from: ˋ  reason: contains not printable characters */
        public static final int f24 = 2;

        /* renamed from: ˏ  reason: contains not printable characters */
        public static final int f25 = 1;

        static {
            int[] iArr = {1, 2};
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    a(Application application, int i) {
        this.f21 = i;
        this.f19 = false;
        this.f17 = String.format(Locale.ROOT, "_moatTracker%d", Integer.valueOf((int) (Math.random() * 1.0E8d)));
        this.f18 = new WebView(application);
        WebSettings settings = this.f18.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowContentAccess(false);
        settings.setAllowFileAccess(false);
        settings.setDatabaseEnabled(false);
        settings.setDomStorageEnabled(false);
        settings.setGeolocationEnabled(false);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);
        settings.setSaveFormData(false);
        if (Build.VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(false);
            settings.setAllowUniversalAccessFromFileURLs(false);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(1);
        }
        try {
            this.f20 = new j(this.f18, i == d.f24 ? j.e.f122 : j.e.f123);
        } catch (o e) {
            o.m132(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m12(String str) {
        if (this.f21 == d.f25) {
            this.f18.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    if (!a.this.f19) {
                        try {
                            boolean unused = a.this.f19 = true;
                            a.this.f20.m101();
                        } catch (Exception e) {
                            o.m132(e);
                        }
                    }
                }
            });
            WebView webView = this.f18;
            webView.loadData("<!DOCTYPE html>\n<html>\n<head lang=\"en\">\n   <meta charset=\"UTF-8\">\n   <title></title>\n</head>\n<body style=\"margin:0;padding:0;\">\n    <script src=\"https://z.moatads.com/" + str + "/moatad.js\" type=\"text/javascript\"></script>\n</body>\n</html>", "text/html", "utf-8");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m13(String str, Map<String, String> map, Integer num, Integer num2, Integer num3) {
        if (this.f21 == d.f24) {
            this.f18.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    if (!a.this.f19) {
                        try {
                            boolean unused = a.this.f19 = true;
                            a.this.f20.m101();
                            a.this.f20.m100(a.this.f17);
                        } catch (Exception e) {
                            o.m132(e);
                        }
                    }
                }
            });
            JSONObject jSONObject = new JSONObject(map);
            WebView webView = this.f18;
            String str2 = this.f17;
            webView.loadData(String.format(Locale.ROOT, "<html><head></head><body><div id=\"%s\" style=\"width: %dpx; height: %dpx;\"></div><script>(function initMoatTracking(apiname, pcode, ids, duration) {var events = [];window[pcode + '_moatElToTrack'] = document.getElementById('%s');var moatapi = {'dropTime':%d,'adData': {'ids': ids, 'duration': duration, 'url': 'n/a'},'dispatchEvent': function(ev) {if (this.sendEvent) {if (events) { events.push(ev); ev = events; events = false; }this.sendEvent(ev);} else {events.push(ev);}},'dispatchMany': function(evs){for (var i=0, l=evs.length; i<l; i++) {this.dispatchEvent(evs[i]);}}};Object.defineProperty(window, apiname, {'value': moatapi});var s = document.createElement('script');s.src = 'https://z.moatads.com/' + pcode + '/moatvideo.js?' + apiname + '#' + apiname;document.body.appendChild(s);})('%s', '%s', %s, %s);</script></body></html>", "mianahwvc", num, num2, "mianahwvc", Long.valueOf(System.currentTimeMillis()), str2, str, jSONObject.toString(), num3), "text/html", null);
        }
    }

    a() {
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static void m8(int i, String str, Object obj, String str2) {
        if (!t.m176().f187) {
            return;
        }
        if (obj == null) {
            Log.println(i, VastExtensionXmlManager.MOAT + str, String.format("message = %s", str2));
            return;
        }
        Log.println(i, VastExtensionXmlManager.MOAT + str, String.format("id = %s, message = %s", Integer.valueOf(obj.hashCode()), str2));
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static void m9(String str, Object obj, String str2) {
        Object obj2;
        if (t.m176().f184) {
            String str3 = VastExtensionXmlManager.MOAT + str;
            Object[] objArr = new Object[2];
            if (obj == null) {
                obj2 = "null";
            } else {
                obj2 = Integer.valueOf(obj.hashCode());
            }
            objArr[0] = obj2;
            objArr[1] = str2;
            Log.println(2, str3, String.format("id = %s, message = %s", objArr));
        }
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static void m10(String str, Object obj, String str2, Exception exc) {
        if (t.m176().f187) {
            Log.e(VastExtensionXmlManager.MOAT + str, String.format("id = %s, message = %s", Integer.valueOf(obj.hashCode()), str2), exc);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static void m5(String str, String str2) {
        if (!t.m176().f187 && ((f) MoatAnalytics.getInstance()).f63) {
            int i = 2;
            if (str.equals("[ERROR] ")) {
                i = 6;
            }
            Log.println(i, "MoatAnalytics", str + str2);
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static String m7(View view) {
        if (view == null) {
            return "null";
        }
        return view.getClass().getSimpleName() + "@" + view.hashCode();
    }
}
