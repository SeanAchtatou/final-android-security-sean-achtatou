package com.moat.analytics.mobile.aol;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class h {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final h f77 = new h();
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public ScheduledFuture<?> f78;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public final Map<j, String> f79 = new WeakHashMap();
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public ScheduledFuture<?> f80;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final ScheduledExecutorService f81 = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */

    /* renamed from: ॱ  reason: contains not printable characters */
    public final Map<d, String> f82 = new WeakHashMap();

    /* renamed from: ˊ  reason: contains not printable characters */
    static h m60() {
        return f77;
    }

    private h() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m66(final Context context, j jVar) {
        if (jVar != null) {
            this.f79.put(jVar, "");
            if (this.f78 == null || this.f78.isDone()) {
                a.m8(3, "JSUpdateLooper", this, "Starting metadata reporting loop");
                this.f78 = this.f81.scheduleWithFixedDelay(new Runnable() {
                    public final void run() {
                        try {
                            LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_METADATA"));
                            if (h.this.f79.isEmpty()) {
                                h.this.f78.cancel(true);
                            }
                        } catch (Exception e) {
                            o.m132(e);
                        }
                    }
                }, 0, 50, TimeUnit.MILLISECONDS);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m65(j jVar) {
        if (jVar != null) {
            a.m8(3, "JSUpdateLooper", this, "removeSetupNeededBridge" + jVar.hashCode());
            this.f79.remove(jVar);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m67(final Context context, d dVar) {
        if (dVar != null) {
            a.m8(3, "JSUpdateLooper", this, "addActiveTracker" + dVar.hashCode());
            if (!this.f82.containsKey(dVar)) {
                this.f82.put(dVar, "");
                if (this.f80 == null || this.f80.isDone()) {
                    a.m8(3, "JSUpdateLooper", this, "Starting view update loop");
                    this.f80 = this.f81.scheduleWithFixedDelay(new Runnable() {
                        public final void run() {
                            try {
                                LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_VIEW_INFO"));
                                if (h.this.f82.isEmpty()) {
                                    a.m8(3, "JSUpdateLooper", h.this, "No more active trackers");
                                    h.this.f80.cancel(true);
                                }
                            } catch (Exception e) {
                                o.m132(e);
                            }
                        }
                    }, 0, (long) t.m176().f182, TimeUnit.MILLISECONDS);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m68(d dVar) {
        if (dVar != null) {
            a.m8(3, "JSUpdateLooper", this, "removeActiveTracker" + dVar.hashCode());
            this.f82.remove(dVar);
        }
    }
}
