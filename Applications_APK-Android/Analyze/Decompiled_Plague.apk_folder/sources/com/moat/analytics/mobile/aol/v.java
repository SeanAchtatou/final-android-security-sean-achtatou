package com.moat.analytics.mobile.aol;

import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.webkit.WebView;

final class v extends d implements WebAdTracker {
    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m199() {
        return "WebAdTracker";
    }

    v(@Nullable ViewGroup viewGroup) {
        this(x.m204(viewGroup, false).orElse(null));
        if (viewGroup == null) {
            String str = "WebAdTracker initialization not successful, " + "Target ViewGroup is null";
            a.m8(3, "WebAdTracker", this, str);
            a.m5("[ERROR] ", str);
            this.f54 = new o("Target ViewGroup is null");
        }
        if (this.f52 == null) {
            String str2 = "WebAdTracker initialization not successful, " + "No WebView to track inside of ad container";
            a.m8(3, "WebAdTracker", this, str2);
            a.m5("[ERROR] ", str2);
            this.f54 = new o("No WebView to track inside of ad container");
        }
    }

    v(@Nullable WebView webView) {
        super(webView, false, false);
        a.m8(3, "WebAdTracker", this, "Initializing.");
        if (webView == null) {
            String str = "WebAdTracker initialization not successful, " + "WebView is null";
            a.m8(3, "WebAdTracker", this, str);
            a.m5("[ERROR] ", str);
            this.f54 = new o("WebView is null");
            return;
        }
        try {
            super.m43(webView);
            a.m5("[SUCCESS] ", "WebAdTracker created for " + m34());
        } catch (o e) {
            this.f54 = e;
        }
    }
}
