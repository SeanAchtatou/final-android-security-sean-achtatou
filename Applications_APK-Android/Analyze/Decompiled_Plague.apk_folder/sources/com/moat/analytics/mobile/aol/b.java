package com.moat.analytics.mobile.aol;

import android.os.Handler;
import android.support.annotation.CallSuper;
import android.text.TextUtils;
import android.view.View;
import com.moat.analytics.mobile.aol.a;
import com.mopub.mobileads.VastIconXmlManager;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

abstract class b extends d {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final MoatAdEventType[] f26 = {MoatAdEventType.AD_EVT_FIRST_QUARTILE, MoatAdEventType.AD_EVT_MID_POINT, MoatAdEventType.AD_EVT_THIRD_QUARTILE};

    /* renamed from: ʼ  reason: contains not printable characters */
    final Map<MoatAdEventType, Integer> f27;

    /* renamed from: ʼॱ  reason: contains not printable characters */
    private final String f28;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    WeakReference<View> f29;

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private boolean f30;

    /* renamed from: ˏॱ  reason: contains not printable characters */
    private Map<String, String> f31;

    /* renamed from: ͺ  reason: contains not printable characters */
    private Double f32;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ  reason: contains not printable characters */
    public VideoTrackerListener f33;

    /* renamed from: ॱˋ  reason: contains not printable characters */
    private final Set<MoatAdEventType> f34;
    /* access modifiers changed from: private */

    /* renamed from: ॱˎ  reason: contains not printable characters */
    public final a f35 = new a(c.m29(), a.d.f24);

    /* renamed from: ᐝ  reason: contains not printable characters */
    final Handler f36;

    /* access modifiers changed from: package-private */
    /* renamed from: ᐝ  reason: contains not printable characters */
    public abstract Map<String, Object> m25() throws o;

    b(String str) {
        super(null, false, true);
        a.m8(3, "BaseVideoTracker", this, "Initializing.");
        this.f28 = str;
        this.f51 = this.f35.f20;
        try {
            super.m43(this.f35.f18);
        } catch (o e) {
            this.f54 = e;
        }
        this.f27 = new HashMap();
        this.f34 = new HashSet();
        this.f36 = new Handler();
        this.f30 = false;
        this.f32 = Double.valueOf(1.0d);
    }

    public void setVideoListener(VideoTrackerListener videoTrackerListener) {
        this.f33 = videoTrackerListener;
    }

    public void removeVideoListener() {
        this.f33 = null;
    }

    @CallSuper
    /* renamed from: ॱ  reason: contains not printable characters */
    public boolean m23(Map<String, String> map, View view) {
        try {
            m40();
            m42();
            if (view == null) {
                a.m8(3, "BaseVideoTracker", this, "trackVideoAd received null video view instance");
            }
            this.f31 = map;
            this.f29 = new WeakReference<>(view);
            m21();
            String format = String.format("trackVideoAd tracking ids: %s | view: %s", new JSONObject(map).toString(), a.m7(view));
            a.m8(3, "BaseVideoTracker", this, format);
            a.m5("[SUCCESS] ", m38() + " " + format);
            if (this.f48 != null) {
                this.f48.onTrackingStarted(m34());
            }
            return true;
        } catch (Exception e) {
            m44("trackVideoAd", e);
            return false;
        }
    }

    public void changeTargetView(View view) {
        a.m8(3, "BaseVideoTracker", this, "changing view to " + a.m7(view));
        this.f29 = new WeakReference<>(view);
        try {
            super.changeTargetView(view);
        } catch (Exception e) {
            o.m132(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public void m18(List<String> list) throws o {
        if (this.f31 == null) {
            list.add("Null adIds object");
        }
        if (!list.isEmpty()) {
            throw new o(TextUtils.join(" and ", list));
        }
        super.m39(list);
    }

    public void stopTracking() {
        try {
            super.stopTracking();
            m22();
            if (this.f33 != null) {
                this.f33 = null;
            }
        } catch (Exception e) {
            o.m132(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˊ  reason: contains not printable characters */
    public final Double m24() {
        return this.f32;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public final void m21() throws o {
        super.changeTargetView(this.f29.get());
        super.m41();
        Map<String, Object> r0 = m25();
        Integer num = (Integer) r0.get("width");
        Integer num2 = (Integer) r0.get("height");
        Integer num3 = (Integer) r0.get(VastIconXmlManager.DURATION);
        a.m8(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "Player metadata: height = %d, width = %d, duration = %d", num2, num, num3));
        this.f35.m13(this.f28, this.f31, num, num2, num3);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public JSONObject m20(MoatAdEvent moatAdEvent) {
        if (Double.isNaN(moatAdEvent.f7.doubleValue())) {
            moatAdEvent.f7 = this.f32;
        }
        return new JSONObject(moatAdEvent.m2());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏॱ  reason: contains not printable characters */
    public final void m22() {
        if (!this.f30) {
            this.f30 = true;
            this.f36.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        a.m8(3, "BaseVideoTracker", this, "Shutting down.");
                        a r0 = b.this.f35;
                        a.m8(3, "GlobalWebView", r0, "Cleaning up");
                        r0.f20.m96();
                        r0.f20 = null;
                        r0.f18.destroy();
                        r0.f18 = null;
                        VideoTrackerListener unused = b.this.f33 = null;
                    } catch (Exception e) {
                        o.m132(e);
                    }
                }
            }, 500);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋॱ  reason: contains not printable characters */
    public final boolean m19() {
        return this.f27.containsKey(MoatAdEventType.AD_EVT_COMPLETE) || this.f27.containsKey(MoatAdEventType.AD_EVT_STOPPED) || this.f27.containsKey(MoatAdEventType.AD_EVT_SKIPPED);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static boolean m15(Integer num, Integer num2) {
        return ((double) Math.abs(num2.intValue() - num.intValue())) <= Math.min(750.0d, ((double) num2.intValue()) * 0.05d);
    }

    public void setPlayerVolume(Double d) {
        Double valueOf = Double.valueOf(this.f32.doubleValue() * r.m156());
        if (!d.equals(this.f32)) {
            a.m8(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "player volume changed to %f ", d));
            this.f32 = d;
            if (!valueOf.equals(Double.valueOf(this.f32.doubleValue() * r.m156()))) {
                dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_VOLUME_CHANGE, MoatAdEvent.f4, this.f32));
            }
        }
    }

    public void dispatchEvent(MoatAdEvent moatAdEvent) {
        try {
            JSONObject r0 = m20(moatAdEvent);
            boolean z = false;
            a.m8(3, "BaseVideoTracker", this, String.format("Received event: %s", r0.toString()));
            a.m5("[SUCCESS] ", m38() + String.format(" Received event: %s", r0.toString()));
            if (m37() && this.f51 != null) {
                this.f51.m104(this.f35.f17, r0);
                if (!this.f34.contains(moatAdEvent.f9)) {
                    this.f34.add(moatAdEvent.f9);
                    if (this.f33 != null) {
                        this.f33.onVideoEventReported(moatAdEvent.f9);
                    }
                }
            }
            MoatAdEventType moatAdEventType = moatAdEvent.f9;
            if (moatAdEventType == MoatAdEventType.AD_EVT_COMPLETE || moatAdEventType == MoatAdEventType.AD_EVT_STOPPED || moatAdEventType == MoatAdEventType.AD_EVT_SKIPPED) {
                z = true;
            }
            if (z) {
                this.f27.put(moatAdEventType, 1);
                if (this.f51 != null) {
                    this.f51.m98(this);
                }
                m22();
            }
        } catch (Exception e) {
            o.m132(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊॱ  reason: contains not printable characters */
    public final Double m17() {
        return Double.valueOf(this.f32.doubleValue() * r.m156());
    }
}
