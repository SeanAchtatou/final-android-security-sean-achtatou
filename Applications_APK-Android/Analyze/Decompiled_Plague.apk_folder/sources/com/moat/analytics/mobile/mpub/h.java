package com.moat.analytics.mobile.mpub;

import android.support.v4.widget.ExploreByTouchHelper;
import android.view.View;
import java.util.Map;
import org.json.JSONObject;

abstract class h extends c {
    int l = ExploreByTouchHelper.INVALID_ID;
    private a m = a.UNINITIALIZED;
    private int n = ExploreByTouchHelper.INVALID_ID;
    private double o = Double.NaN;
    private int p = ExploreByTouchHelper.INVALID_ID;
    private int q = 0;

    enum a {
        UNINITIALIZED,
        PAUSED,
        PLAYING,
        STOPPED,
        COMPLETED
    }

    h(String str) {
        super(str);
    }

    private void t() {
        this.i.postDelayed(new Runnable() {
            public void run() {
                h hVar;
                try {
                    if (!h.this.n() || h.this.m()) {
                        hVar = h.this;
                    } else if (Boolean.valueOf(h.this.s()).booleanValue()) {
                        h.this.i.postDelayed(this, 200);
                        return;
                    } else {
                        hVar = h.this;
                    }
                    hVar.l();
                } catch (Exception e) {
                    h.this.l();
                    m.a(e);
                }
            }
        }, 200);
    }

    /* access modifiers changed from: package-private */
    public JSONObject a(MoatAdEvent moatAdEvent) {
        Integer num;
        if (!moatAdEvent.b.equals(MoatAdEvent.a)) {
            num = moatAdEvent.b;
        } else {
            try {
                num = o();
            } catch (Exception unused) {
                num = Integer.valueOf(this.n);
            }
            moatAdEvent.b = num;
        }
        if (moatAdEvent.b.intValue() < 0 || (moatAdEvent.b.intValue() == 0 && moatAdEvent.d == MoatAdEventType.AD_EVT_COMPLETE && this.n > 0)) {
            num = Integer.valueOf(this.n);
            moatAdEvent.b = num;
        }
        if (moatAdEvent.d == MoatAdEventType.AD_EVT_COMPLETE) {
            if (num.intValue() == Integer.MIN_VALUE || this.l == Integer.MIN_VALUE || !a(num, Integer.valueOf(this.l))) {
                this.m = a.STOPPED;
                moatAdEvent.d = MoatAdEventType.AD_EVT_STOPPED;
            } else {
                this.m = a.COMPLETED;
            }
        }
        return super.a(moatAdEvent);
    }

    public boolean a(Map<String, String> map, View view) {
        try {
            boolean a2 = super.a(map, view);
            if (!a2 || !p()) {
                return a2;
            }
            t();
            return a2;
        } catch (Exception e) {
            p.a(3, "IntervalVideoTracker", this, "Problem with video loop");
            a("trackVideoAd", e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public abstract boolean n();

    /* access modifiers changed from: package-private */
    public abstract Integer o();

    /* access modifiers changed from: protected */
    public boolean p() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public abstract boolean q();

    /* access modifiers changed from: package-private */
    public abstract Integer r();

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0094 A[Catch:{ Exception -> 0x00cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0096 A[Catch:{ Exception -> 0x00cb }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00b6 A[Catch:{ Exception -> 0x00cb }] */
    @android.support.annotation.CallSuper
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean s() {
        /*
            r12 = this;
            boolean r0 = r12.n()
            r1 = 0
            if (r0 == 0) goto L_0x00d7
            boolean r0 = r12.m()
            if (r0 == 0) goto L_0x000e
            return r1
        L_0x000e:
            r0 = 1
            java.lang.Integer r2 = r12.o()     // Catch:{ Exception -> 0x00cb }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x00cb }
            int r3 = r12.n     // Catch:{ Exception -> 0x00cb }
            if (r3 < 0) goto L_0x001e
            if (r2 >= 0) goto L_0x001e
            return r1
        L_0x001e:
            r12.n = r2     // Catch:{ Exception -> 0x00cb }
            if (r2 != 0) goto L_0x0023
            return r0
        L_0x0023:
            java.lang.Integer r3 = r12.r()     // Catch:{ Exception -> 0x00cb }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x00cb }
            boolean r4 = r12.q()     // Catch:{ Exception -> 0x00cb }
            double r5 = (double) r3     // Catch:{ Exception -> 0x00cb }
            r7 = 4616189618054758400(0x4010000000000000, double:4.0)
            double r5 = r5 / r7
            java.lang.Double r7 = r12.j()     // Catch:{ Exception -> 0x00cb }
            double r7 = r7.doubleValue()     // Catch:{ Exception -> 0x00cb }
            r9 = 0
            int r10 = r12.p     // Catch:{ Exception -> 0x00cb }
            if (r2 <= r10) goto L_0x0042
            r12.p = r2     // Catch:{ Exception -> 0x00cb }
        L_0x0042:
            int r10 = r12.l     // Catch:{ Exception -> 0x00cb }
            r11 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r10 != r11) goto L_0x004a
            r12.l = r3     // Catch:{ Exception -> 0x00cb }
        L_0x004a:
            if (r4 == 0) goto L_0x0087
            com.moat.analytics.mobile.mpub.h$a r3 = r12.m     // Catch:{ Exception -> 0x00cb }
            com.moat.analytics.mobile.mpub.h$a r4 = com.moat.analytics.mobile.mpub.h.a.UNINITIALIZED     // Catch:{ Exception -> 0x00cb }
            if (r3 != r4) goto L_0x0059
            com.moat.analytics.mobile.mpub.MoatAdEventType r9 = com.moat.analytics.mobile.mpub.MoatAdEventType.AD_EVT_START     // Catch:{ Exception -> 0x00cb }
        L_0x0054:
            com.moat.analytics.mobile.mpub.h$a r3 = com.moat.analytics.mobile.mpub.h.a.PLAYING     // Catch:{ Exception -> 0x00cb }
        L_0x0056:
            r12.m = r3     // Catch:{ Exception -> 0x00cb }
            goto L_0x0092
        L_0x0059:
            com.moat.analytics.mobile.mpub.h$a r3 = r12.m     // Catch:{ Exception -> 0x00cb }
            com.moat.analytics.mobile.mpub.h$a r4 = com.moat.analytics.mobile.mpub.h.a.PAUSED     // Catch:{ Exception -> 0x00cb }
            if (r3 != r4) goto L_0x0062
            com.moat.analytics.mobile.mpub.MoatAdEventType r9 = com.moat.analytics.mobile.mpub.MoatAdEventType.AD_EVT_PLAYING     // Catch:{ Exception -> 0x00cb }
            goto L_0x0054
        L_0x0062:
            double r3 = (double) r2     // Catch:{ Exception -> 0x00cb }
            double r3 = r3 / r5
            double r3 = java.lang.Math.floor(r3)     // Catch:{ Exception -> 0x00cb }
            int r3 = (int) r3     // Catch:{ Exception -> 0x00cb }
            int r3 = r3 - r0
            r4 = -1
            if (r3 <= r4) goto L_0x0092
            r4 = 3
            if (r3 >= r4) goto L_0x0092
            com.moat.analytics.mobile.mpub.MoatAdEventType[] r4 = com.moat.analytics.mobile.mpub.h.g     // Catch:{ Exception -> 0x00cb }
            r3 = r4[r3]     // Catch:{ Exception -> 0x00cb }
            java.util.Map r4 = r12.h     // Catch:{ Exception -> 0x00cb }
            boolean r4 = r4.containsKey(r3)     // Catch:{ Exception -> 0x00cb }
            if (r4 != 0) goto L_0x0092
            java.util.Map r4 = r12.h     // Catch:{ Exception -> 0x00cb }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x00cb }
            r4.put(r3, r5)     // Catch:{ Exception -> 0x00cb }
            r9 = r3
            goto L_0x0092
        L_0x0087:
            com.moat.analytics.mobile.mpub.h$a r3 = r12.m     // Catch:{ Exception -> 0x00cb }
            com.moat.analytics.mobile.mpub.h$a r4 = com.moat.analytics.mobile.mpub.h.a.PAUSED     // Catch:{ Exception -> 0x00cb }
            if (r3 == r4) goto L_0x0092
            com.moat.analytics.mobile.mpub.MoatAdEventType r9 = com.moat.analytics.mobile.mpub.MoatAdEventType.AD_EVT_PAUSED     // Catch:{ Exception -> 0x00cb }
            com.moat.analytics.mobile.mpub.h$a r3 = com.moat.analytics.mobile.mpub.h.a.PAUSED     // Catch:{ Exception -> 0x00cb }
            goto L_0x0056
        L_0x0092:
            if (r9 == 0) goto L_0x0096
            r3 = r0
            goto L_0x0097
        L_0x0096:
            r3 = r1
        L_0x0097:
            if (r3 != 0) goto L_0x00b4
            double r4 = r12.o     // Catch:{ Exception -> 0x00cb }
            boolean r4 = java.lang.Double.isNaN(r4)     // Catch:{ Exception -> 0x00cb }
            if (r4 != 0) goto L_0x00b4
            double r4 = r12.o     // Catch:{ Exception -> 0x00cb }
            double r4 = r4 - r7
            double r4 = java.lang.Math.abs(r4)     // Catch:{ Exception -> 0x00cb }
            r10 = 4587366580439587226(0x3fa999999999999a, double:0.05)
            int r6 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r6 <= 0) goto L_0x00b4
            com.moat.analytics.mobile.mpub.MoatAdEventType r9 = com.moat.analytics.mobile.mpub.MoatAdEventType.AD_EVT_VOLUME_CHANGE     // Catch:{ Exception -> 0x00cb }
            r3 = r0
        L_0x00b4:
            if (r3 == 0) goto L_0x00c6
            com.moat.analytics.mobile.mpub.MoatAdEvent r3 = new com.moat.analytics.mobile.mpub.MoatAdEvent     // Catch:{ Exception -> 0x00cb }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x00cb }
            java.lang.Double r4 = r12.k()     // Catch:{ Exception -> 0x00cb }
            r3.<init>(r9, r2, r4)     // Catch:{ Exception -> 0x00cb }
            r12.dispatchEvent(r3)     // Catch:{ Exception -> 0x00cb }
        L_0x00c6:
            r12.o = r7     // Catch:{ Exception -> 0x00cb }
            r12.q = r1     // Catch:{ Exception -> 0x00cb }
            return r0
        L_0x00cb:
            int r2 = r12.q
            int r3 = r2 + 1
            r12.q = r3
            r3 = 5
            if (r2 >= r3) goto L_0x00d5
            return r0
        L_0x00d5:
            r0 = r1
            return r0
        L_0x00d7:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.mpub.h.s():boolean");
    }

    public void setPlayerVolume(Double d) {
        super.setPlayerVolume(d);
        this.o = j().doubleValue();
    }

    public void stopTracking() {
        try {
            dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE));
            super.stopTracking();
        } catch (Exception e) {
            m.a(e);
        }
    }
}
