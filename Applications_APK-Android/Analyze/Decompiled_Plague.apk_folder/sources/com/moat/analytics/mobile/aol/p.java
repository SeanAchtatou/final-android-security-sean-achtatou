package com.moat.analytics.mobile.aol;

import android.support.annotation.VisibleForTesting;
import com.moat.analytics.mobile.aol.base.asserts.Asserts;
import com.moat.analytics.mobile.aol.base.functional.Optional;
import com.moat.analytics.mobile.aol.t;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

class p<T> implements InvocationHandler {
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public static final Object[] f148 = new Object[0];

    /* renamed from: ˊ  reason: contains not printable characters */
    private final c<T> f149;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f150;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final LinkedList<p<T>.d> f151 = new LinkedList<>();

    /* renamed from: ॱ  reason: contains not printable characters */
    private final Class<T> f152;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private T f153;

    interface c<T> {
        /* renamed from: ˋ  reason: contains not printable characters */
        Optional<T> m140() throws o;
    }

    class d {
        /* access modifiers changed from: private */

        /* renamed from: ˊ  reason: contains not printable characters */
        public final WeakReference[] f155;
        /* access modifiers changed from: private */

        /* renamed from: ˋ  reason: contains not printable characters */
        public final Method f156;

        /* renamed from: ˎ  reason: contains not printable characters */
        private final LinkedList<Object> f157;

        /* synthetic */ d(p pVar, Method method, Object[] objArr, byte b) {
            this(method, objArr);
        }

        private d(Method method, Object... objArr) {
            this.f157 = new LinkedList<>();
            objArr = objArr == null ? p.f148 : objArr;
            int i = 0;
            WeakReference[] weakReferenceArr = new WeakReference[objArr.length];
            int length = objArr.length;
            int i2 = 0;
            while (i < length) {
                Object obj = objArr[i];
                if ((obj instanceof Map) || (obj instanceof Integer) || (obj instanceof Double)) {
                    this.f157.add(obj);
                }
                weakReferenceArr[i2] = new WeakReference(obj);
                i++;
                i2++;
            }
            this.f155 = weakReferenceArr;
            this.f156 = method;
        }
    }

    @VisibleForTesting
    private p(c<T> cVar, Class<T> cls) throws o {
        Asserts.checkNotNull(cVar);
        Asserts.checkNotNull(cls);
        this.f149 = cVar;
        this.f152 = cls;
        t.m176().m181(new t.b() {
            /* renamed from: ˎ  reason: contains not printable characters */
            public final void m139() throws o {
                p.this.m136();
            }
        });
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static <T> T m135(c<T> cVar, Class<T> cls) throws o {
        ClassLoader classLoader = cls.getClassLoader();
        p pVar = new p(cVar, cls);
        return Proxy.newProxyInstance(classLoader, new Class[]{cls}, pVar);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static Boolean m134(Method method) {
        try {
            if (Boolean.TYPE.equals(method.getReturnType())) {
                return true;
            }
            return null;
        } catch (Exception e) {
            o.m132(e);
            return null;
        }
    }

    public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
        try {
            Class<?> declaringClass = method.getDeclaringClass();
            t r0 = t.m176();
            if (Object.class.equals(declaringClass)) {
                String name = method.getName();
                if ("getClass".equals(name)) {
                    return this.f152;
                }
                if (!"toString".equals(name)) {
                    return method.invoke(this, objArr);
                }
                Object invoke = method.invoke(this, objArr);
                return String.valueOf(invoke).replace(p.class.getName(), this.f152.getName());
            } else if (!this.f150 || this.f153 != null) {
                if (r0.f186 == t.a.f197) {
                    m136();
                    if (this.f153 != null) {
                        return method.invoke(this.f153, objArr);
                    }
                }
                if (r0.f186 == t.a.f198 && (!this.f150 || this.f153 != null)) {
                    if (this.f151.size() >= 15) {
                        this.f151.remove(5);
                    }
                    this.f151.add(new d(this, method, objArr, (byte) 0));
                }
                return m134(method);
            } else {
                this.f151.clear();
                return m134(method);
            }
        } catch (Exception e) {
            o.m132(e);
            return m134(method);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m136() throws o {
        if (!this.f150) {
            try {
                this.f153 = this.f149.m140().orElse(null);
            } catch (Exception e) {
                a.m10("OnOffTrackerProxy", this, "Could not create instance", e);
                o.m132(e);
            }
            this.f150 = true;
        }
        if (this.f153 != null) {
            Iterator<p<T>.d> it = this.f151.iterator();
            while (it.hasNext()) {
                d next = it.next();
                try {
                    Object[] objArr = new Object[next.f155.length];
                    WeakReference[] r3 = next.f155;
                    int length = r3.length;
                    int i = 0;
                    int i2 = 0;
                    while (i < length) {
                        objArr[i2] = r3[i].get();
                        i++;
                        i2++;
                    }
                    next.f156.invoke(this.f153, objArr);
                } catch (Exception e2) {
                    o.m132(e2);
                }
            }
            this.f151.clear();
        }
    }
}
