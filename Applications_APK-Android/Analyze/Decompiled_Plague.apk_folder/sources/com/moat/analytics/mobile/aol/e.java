package com.moat.analytics.mobile.aol;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.google.android.gms.ads.AdActivity;
import com.moat.analytics.mobile.aol.base.functional.Optional;
import com.moat.analytics.mobile.aol.t;
import java.lang.ref.WeakReference;

final class e {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static WeakReference<Activity> f56 = new WeakReference<>(null);
    @Nullable

    /* renamed from: ˋ  reason: contains not printable characters */
    private static WebAdTracker f57;

    e() {
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static void m45(Activity activity) {
        try {
            if (t.m176().f186 != t.a.f198) {
                String name = activity.getClass().getName();
                a.m8(3, "GMAInterstitialHelper", activity, "Activity name: " + name);
                if (!name.contains(AdActivity.CLASS_NAME)) {
                    if (f57 != null) {
                        a.m8(3, "GMAInterstitialHelper", f56.get(), "Stopping to track GMA interstitial");
                        f57.stopTracking();
                        f57 = null;
                    }
                    f56 = new WeakReference<>(null);
                } else if (f56.get() == null || f56.get() != activity) {
                    View decorView = activity.getWindow().getDecorView();
                    if (decorView instanceof ViewGroup) {
                        Optional<WebView> r0 = x.m204((ViewGroup) decorView, true);
                        if (r0.isPresent()) {
                            f56 = new WeakReference<>(activity);
                            a.m8(3, "GMAInterstitialHelper", f56.get(), "Starting to track GMA interstitial");
                            WebAdTracker createWebAdTracker = MoatFactory.create().createWebAdTracker(r0.get());
                            f57 = createWebAdTracker;
                            createWebAdTracker.startTracking();
                            return;
                        }
                        a.m8(3, "GMAInterstitialHelper", activity, "Sorry, no WebView in this activity");
                    }
                }
            }
        } catch (Exception e) {
            o.m132(e);
        }
    }
}
