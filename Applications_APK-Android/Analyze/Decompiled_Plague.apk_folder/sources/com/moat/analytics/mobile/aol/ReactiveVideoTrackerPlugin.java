package com.moat.analytics.mobile.aol;

import android.app.Activity;
import android.view.View;
import com.moat.analytics.mobile.aol.base.functional.Optional;
import com.moat.analytics.mobile.aol.p;
import java.util.Map;

public class ReactiveVideoTrackerPlugin implements l<ReactiveVideoTracker> {
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public final String f15;

    public ReactiveVideoTrackerPlugin(String str) {
        this.f15 = str;
    }

    public ReactiveVideoTracker create() throws o {
        return (ReactiveVideoTracker) p.m135(new p.c<ReactiveVideoTracker>() {
            /* renamed from: ˋ  reason: contains not printable characters */
            public final Optional<ReactiveVideoTracker> m4() {
                a.m5("[INFO] ", "Attempting to create ReactiveVideoTracker");
                return Optional.of(new w(ReactiveVideoTrackerPlugin.this.f15));
            }
        }, ReactiveVideoTracker.class);
    }

    public ReactiveVideoTracker createNoOp() {
        return new d();
    }

    static class d implements ReactiveVideoTracker {
        public final void changeTargetView(View view) {
        }

        public final void dispatchEvent(MoatAdEvent moatAdEvent) {
        }

        public final void removeListener() {
        }

        public final void removeVideoListener() {
        }

        public final void setActivity(Activity activity) {
        }

        public final void setListener(TrackerListener trackerListener) {
        }

        public final void setPlayerVolume(Double d) {
        }

        public final void setVideoListener(VideoTrackerListener videoTrackerListener) {
        }

        public final void stopTracking() {
        }

        public final boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
            return false;
        }

        d() {
        }
    }
}
