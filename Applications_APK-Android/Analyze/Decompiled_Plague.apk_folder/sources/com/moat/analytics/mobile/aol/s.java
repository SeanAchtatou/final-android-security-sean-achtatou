package com.moat.analytics.mobile.aol;

import android.media.MediaPlayer;
import android.view.View;
import com.mopub.mobileads.VastIconXmlManager;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class s extends i implements NativeVideoTracker {

    /* renamed from: ॱˊ  reason: contains not printable characters */
    private WeakReference<MediaPlayer> f178;

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m162() {
        return "NativeVideoTracker";
    }

    s(String str) {
        super(str);
        a.m8(3, "NativeVideoTracker", this, "In initialization method.");
        if (str == null || str.isEmpty()) {
            StringBuilder sb = new StringBuilder("PartnerCode is ");
            sb.append(str == null ? "null" : "empty");
            String sb2 = sb.toString();
            String str2 = "NativeDisplayTracker creation problem, " + sb2;
            a.m8(3, "NativeVideoTracker", this, str2);
            a.m5("[ERROR] ", str2);
            this.f54 = new o(sb2);
        }
        a.m5("[SUCCESS] ", "NativeVideoTracker created");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ͺ  reason: contains not printable characters */
    public final boolean m164() {
        return (this.f178 == null || this.f178.get() == null) ? false : true;
    }

    public final boolean trackVideoAd(Map<String, String> map, MediaPlayer mediaPlayer, View view) {
        try {
            m40();
            m42();
            if (mediaPlayer == null) {
                throw new o("Null player instance");
            }
            mediaPlayer.getCurrentPosition();
            this.f178 = new WeakReference<>(mediaPlayer);
            return super.m72(map, view);
        } catch (Exception unused) {
            throw new o("Playback has already completed");
        } catch (Exception e) {
            o.m132(e);
            String r2 = o.m131("trackVideoAd", e);
            if (this.f48 != null) {
                this.f48.onTrackingFailedToStart(r2);
            }
            a.m8(3, "NativeVideoTracker", this, r2);
            a.m5("[ERROR] ", "NativeVideoTracker " + r2);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˋ  reason: contains not printable characters */
    public final Integer m165() {
        return Integer.valueOf(this.f178.get().getCurrentPosition());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˎ  reason: contains not printable characters */
    public final boolean m166() {
        return this.f178.get().isPlaying();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱᐝ  reason: contains not printable characters */
    public final Integer m167() {
        return Integer.valueOf(this.f178.get().getDuration());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐝ  reason: contains not printable characters */
    public final Map<String, Object> m168() throws o {
        MediaPlayer mediaPlayer = this.f178.get();
        HashMap hashMap = new HashMap();
        hashMap.put("width", Integer.valueOf(mediaPlayer.getVideoWidth()));
        hashMap.put("height", Integer.valueOf(mediaPlayer.getVideoHeight()));
        hashMap.put(VastIconXmlManager.DURATION, Integer.valueOf(mediaPlayer.getDuration()));
        return hashMap;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m163(List<String> list) throws o {
        if (!((this.f178 == null || this.f178.get() == null) ? false : true)) {
            list.add("Player is null");
        }
        super.m18(list);
    }
}
