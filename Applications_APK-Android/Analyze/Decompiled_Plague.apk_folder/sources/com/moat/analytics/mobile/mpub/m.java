package com.moat.analytics.mobile.mpub;

import android.util.Log;

class m extends Exception {
    private static final Long a = 60000L;
    private static Long b;
    private static Exception c;

    m(String str) {
        super(str);
    }

    static String a(String str, Exception exc) {
        if (exc instanceof m) {
            return str + " failed: " + exc.getMessage();
        }
        return str + " failed unexpectedly";
    }

    static void a() {
        if (c != null) {
            b(c);
            c = null;
        }
    }

    static void a(Exception exc) {
        if (w.a().b) {
            Log.e("MoatException", Log.getStackTraceString(exc));
        } else {
            b(exc);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0196 A[Catch:{ Exception -> 0x01b8 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void b(java.lang.Exception r12) {
        /*
            com.moat.analytics.mobile.mpub.w r0 = com.moat.analytics.mobile.mpub.w.a()     // Catch:{ Exception -> 0x01b8 }
            com.moat.analytics.mobile.mpub.w$d r0 = r0.a     // Catch:{ Exception -> 0x01b8 }
            com.moat.analytics.mobile.mpub.w$d r1 = com.moat.analytics.mobile.mpub.w.d.ON     // Catch:{ Exception -> 0x01b8 }
            if (r0 != r1) goto L_0x01b6
            com.moat.analytics.mobile.mpub.w r0 = com.moat.analytics.mobile.mpub.w.a()     // Catch:{ Exception -> 0x01b8 }
            int r0 = r0.e     // Catch:{ Exception -> 0x01b8 }
            if (r0 != 0) goto L_0x0013
            return
        L_0x0013:
            r1 = 100
            if (r0 >= r1) goto L_0x0024
            double r1 = (double) r0     // Catch:{ Exception -> 0x01b8 }
            r3 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r1 = r1 / r3
            double r3 = java.lang.Math.random()     // Catch:{ Exception -> 0x01b8 }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 >= 0) goto L_0x0024
            return
        L_0x0024:
            java.lang.String r1 = ""
            java.lang.String r2 = ""
            java.lang.String r3 = ""
            java.lang.String r4 = ""
            java.lang.String r5 = "https://px.moatads.com/pixel.gif?e=0&i=MOATSDK1&ac=1"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b8 }
            r6.<init>(r5)     // Catch:{ Exception -> 0x01b8 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b8 }
            r5.<init>()     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r7 = "&zt="
            r5.append(r7)     // Catch:{ Exception -> 0x01b8 }
            boolean r7 = r12 instanceof com.moat.analytics.mobile.mpub.m     // Catch:{ Exception -> 0x01b8 }
            r8 = 1
            r9 = 0
            if (r7 == 0) goto L_0x0045
            r7 = r8
            goto L_0x0046
        L_0x0045:
            r7 = r9
        L_0x0046:
            r5.append(r7)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01b8 }
            r6.append(r5)     // Catch:{ Exception -> 0x01b8 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b8 }
            r5.<init>()     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r7 = "&zr="
            r5.append(r7)     // Catch:{ Exception -> 0x01b8 }
            r5.append(r0)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r0 = r5.toString()     // Catch:{ Exception -> 0x01b8 }
            r6.append(r0)     // Catch:{ Exception -> 0x01b8 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bd }
            r0.<init>()     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = "&zm="
            r0.append(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = r12.getMessage()     // Catch:{ Exception -> 0x00bd }
            if (r5 != 0) goto L_0x0077
            java.lang.String r5 = "null"
            goto L_0x008b
        L_0x0077:
            java.lang.String r5 = r12.getMessage()     // Catch:{ Exception -> 0x00bd }
            java.lang.String r7 = "UTF-8"
            byte[] r5 = r5.getBytes(r7)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = android.util.Base64.encodeToString(r5, r9)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r7 = "UTF-8"
            java.lang.String r5 = java.net.URLEncoder.encode(r5, r7)     // Catch:{ Exception -> 0x00bd }
        L_0x008b:
            r0.append(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00bd }
            r6.append(r0)     // Catch:{ Exception -> 0x00bd }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bd }
            r0.<init>()     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = "&k="
            r0.append(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r12 = android.util.Log.getStackTraceString(r12)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = "UTF-8"
            byte[] r12 = r12.getBytes(r5)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r12 = android.util.Base64.encodeToString(r12, r9)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = "UTF-8"
            java.lang.String r12 = java.net.URLEncoder.encode(r12, r5)     // Catch:{ Exception -> 0x00bd }
            r0.append(r12)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r12 = r0.toString()     // Catch:{ Exception -> 0x00bd }
            r6.append(r12)     // Catch:{ Exception -> 0x00bd }
        L_0x00bd:
            java.lang.String r12 = "MPUB"
            java.lang.String r0 = "&zMoatMMAKv=9d24c90729bf464dce5d8fd1ace14a60656dbb2d"
            r6.append(r0)     // Catch:{ Exception -> 0x00f0 }
            java.lang.String r0 = "2.4.5"
            com.moat.analytics.mobile.mpub.s$a r1 = com.moat.analytics.mobile.mpub.s.d()     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ed }
            r3.<init>()     // Catch:{ Exception -> 0x00ed }
            java.lang.String r5 = "&zMoatMMAKan="
            r3.append(r5)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r5 = r1.a()     // Catch:{ Exception -> 0x00ed }
            r3.append(r5)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00ed }
            r6.append(r3)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r1 = r1.b()     // Catch:{ Exception -> 0x00ed }
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x00f2 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00f2 }
            goto L_0x00f3
        L_0x00ed:
            r1 = r2
            goto L_0x00f2
        L_0x00ef:
            r12 = r1
        L_0x00f0:
            r1 = r2
            r0 = r3
        L_0x00f2:
            r2 = r4
        L_0x00f3:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b8 }
            r3.<init>()     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r4 = "&d=Android:"
            r3.append(r4)     // Catch:{ Exception -> 0x01b8 }
            r3.append(r12)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r12 = ":"
            r3.append(r12)     // Catch:{ Exception -> 0x01b8 }
            r3.append(r1)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r12 = ":-"
            r3.append(r12)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r12 = r3.toString()     // Catch:{ Exception -> 0x01b8 }
            r6.append(r12)     // Catch:{ Exception -> 0x01b8 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b8 }
            r12.<init>()     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r1 = "&bo="
            r12.append(r1)     // Catch:{ Exception -> 0x01b8 }
            r12.append(r0)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x01b8 }
            r6.append(r12)     // Catch:{ Exception -> 0x01b8 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b8 }
            r12.<init>()     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r0 = "&bd="
            r12.append(r0)     // Catch:{ Exception -> 0x01b8 }
            r12.append(r2)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x01b8 }
            r6.append(r12)     // Catch:{ Exception -> 0x01b8 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01b8 }
            java.lang.Long r12 = java.lang.Long.valueOf(r0)     // Catch:{ Exception -> 0x01b8 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b8 }
            r0.<init>()     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r1 = "&t="
            r0.append(r1)     // Catch:{ Exception -> 0x01b8 }
            r0.append(r12)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01b8 }
            r6.append(r0)     // Catch:{ Exception -> 0x01b8 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b8 }
            r0.<init>()     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r1 = "&de="
            r0.append(r1)     // Catch:{ Exception -> 0x01b8 }
            java.util.Locale r1 = java.util.Locale.ROOT     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r2 = "%.0f"
            java.lang.Object[] r3 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x01b8 }
            double r4 = java.lang.Math.random()     // Catch:{ Exception -> 0x01b8 }
            r7 = 4621819117588971520(0x4024000000000000, double:10.0)
            r10 = 4622945017495814144(0x4028000000000000, double:12.0)
            double r7 = java.lang.Math.pow(r7, r10)     // Catch:{ Exception -> 0x01b8 }
            double r4 = r4 * r7
            double r4 = java.lang.Math.floor(r4)     // Catch:{ Exception -> 0x01b8 }
            java.lang.Double r4 = java.lang.Double.valueOf(r4)     // Catch:{ Exception -> 0x01b8 }
            r3[r9] = r4     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r1 = java.lang.String.format(r1, r2, r3)     // Catch:{ Exception -> 0x01b8 }
            r0.append(r1)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01b8 }
            r6.append(r0)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r0 = "&cs=0"
            r6.append(r0)     // Catch:{ Exception -> 0x01b8 }
            java.lang.Long r0 = com.moat.analytics.mobile.mpub.m.b     // Catch:{ Exception -> 0x01b8 }
            if (r0 == 0) goto L_0x01ac
            long r0 = r12.longValue()     // Catch:{ Exception -> 0x01b8 }
            java.lang.Long r2 = com.moat.analytics.mobile.mpub.m.b     // Catch:{ Exception -> 0x01b8 }
            long r2 = r2.longValue()     // Catch:{ Exception -> 0x01b8 }
            long r4 = r0 - r2
            java.lang.Long r0 = com.moat.analytics.mobile.mpub.m.a     // Catch:{ Exception -> 0x01b8 }
            long r0 = r0.longValue()     // Catch:{ Exception -> 0x01b8 }
            int r2 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x01b8
        L_0x01ac:
            java.lang.String r0 = r6.toString()     // Catch:{ Exception -> 0x01b8 }
            com.moat.analytics.mobile.mpub.q.b(r0)     // Catch:{ Exception -> 0x01b8 }
            com.moat.analytics.mobile.mpub.m.b = r12     // Catch:{ Exception -> 0x01b8 }
            return
        L_0x01b6:
            com.moat.analytics.mobile.mpub.m.c = r12     // Catch:{ Exception -> 0x01b8 }
        L_0x01b8:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.mpub.m.b(java.lang.Exception):void");
    }
}
