package com.moat.analytics.mobile.aol;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.aol.NoOp;
import com.moat.analytics.mobile.aol.base.functional.Optional;
import com.moat.analytics.mobile.aol.p;
import java.lang.ref.WeakReference;
import java.util.Map;

final class k extends MoatFactory {
    k() throws o {
        if (!((f) f.getInstance()).m47()) {
            String str = "Failed to initialize MoatFactory" + ", SDK was not started";
            a.m8(3, "Factory", this, str);
            a.m5("[ERROR] ", str);
            throw new o("Failed to initialize MoatFactory");
        }
    }

    public final WebAdTracker createWebAdTracker(@NonNull WebView webView) {
        try {
            final WeakReference weakReference = new WeakReference(webView);
            return (WebAdTracker) p.m135(new p.c<WebAdTracker>() {
                /* renamed from: ˋ  reason: contains not printable characters */
                public final Optional<WebAdTracker> m108() {
                    WebView webView = (WebView) weakReference.get();
                    String str = "Attempting to create WebAdTracker for " + a.m7(webView);
                    a.m8(3, "Factory", this, str);
                    a.m5("[INFO] ", str);
                    return Optional.of(new v(webView));
                }
            }, WebAdTracker.class);
        } catch (Exception e) {
            o.m132(e);
            return new NoOp.e();
        }
    }

    public final WebAdTracker createWebAdTracker(@NonNull ViewGroup viewGroup) {
        try {
            final WeakReference weakReference = new WeakReference(viewGroup);
            return (WebAdTracker) p.m135(new p.c<WebAdTracker>() {
                /* renamed from: ˋ  reason: contains not printable characters */
                public final Optional<WebAdTracker> m107() throws o {
                    ViewGroup viewGroup = (ViewGroup) weakReference.get();
                    String str = "Attempting to create WebAdTracker for adContainer " + a.m7(viewGroup);
                    a.m8(3, "Factory", this, str);
                    a.m5("[INFO] ", str);
                    return Optional.of(new v(viewGroup));
                }
            }, WebAdTracker.class);
        } catch (Exception e) {
            o.m132(e);
            return new NoOp.e();
        }
    }

    public final NativeDisplayTracker createNativeDisplayTracker(@NonNull View view, @NonNull final Map<String, String> map) {
        try {
            final WeakReference weakReference = new WeakReference(view);
            return (NativeDisplayTracker) p.m135(new p.c<NativeDisplayTracker>() {
                /* renamed from: ˋ  reason: contains not printable characters */
                public final Optional<NativeDisplayTracker> m105() {
                    View view = (View) weakReference.get();
                    String str = "Attempting to create NativeDisplayTracker for " + a.m7(view);
                    a.m8(3, "Factory", this, str);
                    a.m5("[INFO] ", str);
                    return Optional.of(new q(view, map));
                }
            }, NativeDisplayTracker.class);
        } catch (Exception e) {
            o.m132(e);
            return new NoOp.c();
        }
    }

    public final NativeVideoTracker createNativeVideoTracker(final String str) {
        try {
            return (NativeVideoTracker) p.m135(new p.c<NativeVideoTracker>() {
                /* renamed from: ˋ  reason: contains not printable characters */
                public final Optional<NativeVideoTracker> m106() {
                    a.m8(3, "Factory", this, "Attempting to create NativeVideoTracker");
                    a.m5("[INFO] ", "Attempting to create NativeVideoTracker");
                    return Optional.of(new s(str));
                }
            }, NativeVideoTracker.class);
        } catch (Exception e) {
            o.m132(e);
            return new NoOp.b();
        }
    }

    public final <T> T createCustomTracker(l<T> lVar) {
        try {
            return lVar.create();
        } catch (Exception e) {
            o.m132(e);
            return lVar.createNoOp();
        }
    }
}
