package com.moat.analytics.mobile.aol;

import android.view.View;
import com.mopub.mobileads.VastIconXmlManager;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class w extends b implements ReactiveVideoTracker {

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private Integer f227;

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m200() {
        return "ReactiveVideoTracker";
    }

    public w(String str) {
        super(str);
        a.m8(3, "ReactiveVideoTracker", this, "Initializing.");
        a.m5("[SUCCESS] ", "ReactiveVideoTracker created");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐝ  reason: contains not printable characters */
    public final Map<String, Object> m203() throws o {
        HashMap hashMap = new HashMap();
        View view = (View) this.f29.get();
        int i = 0;
        int i2 = 0;
        if (view != null) {
            i = Integer.valueOf(view.getWidth());
            i2 = Integer.valueOf(view.getHeight());
        }
        hashMap.put(VastIconXmlManager.DURATION, this.f227);
        hashMap.put("width", i);
        hashMap.put("height", i2);
        return hashMap;
    }

    public final boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
        try {
            m40();
            m42();
            this.f227 = num;
            return super.m23(map, view);
        } catch (Exception e) {
            m44("trackVideoAd", e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final JSONObject m202(MoatAdEvent moatAdEvent) {
        if (moatAdEvent.f9 == MoatAdEventType.AD_EVT_COMPLETE && !moatAdEvent.f8.equals(MoatAdEvent.f4) && !m15(moatAdEvent.f8, this.f227)) {
            moatAdEvent.f9 = MoatAdEventType.AD_EVT_STOPPED;
        }
        return super.m20(moatAdEvent);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m201(List<String> list) throws o {
        if (this.f227.intValue() < 1000) {
            throw new o(String.format(Locale.ROOT, "Invalid duration = %d. Please make sure duration is in milliseconds.", this.f227));
        }
        super.m18(list);
    }
}
