package com.moat.analytics.mobile.mpub;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.FloatRange;
import android.telephony.TelephonyManager;
import com.mopub.common.GpsHelper;
import java.lang.ref.WeakReference;

class s {
    /* access modifiers changed from: private */
    public static String a;
    private static a b;
    private static b c;

    static class a {
        /* access modifiers changed from: private */
        public boolean a;
        private String b;
        private String c;
        private String d;

        private a() {
            this.a = false;
            this.b = "_unknown_";
            this.c = "_unknown_";
            this.d = "_unknown_";
            try {
                Context c2 = s.c();
                if (c2 != null) {
                    this.a = true;
                    PackageManager packageManager = c2.getPackageManager();
                    this.c = c2.getPackageName();
                    this.b = packageManager.getApplicationLabel(c2.getApplicationInfo()).toString();
                    this.d = packageManager.getInstallerPackageName(this.c);
                    return;
                }
                p.a(3, "Util", this, "Can't get app name, appContext is null.");
            } catch (Exception e) {
                m.a(e);
            }
        }

        /* access modifiers changed from: package-private */
        public String a() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public String b() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public String c() {
            return this.d != null ? this.d : "_unknown_";
        }
    }

    static class b {
        String a;
        String b;
        Integer c;
        boolean d;
        boolean e;
        boolean f;

        private b() {
            this.a = "_unknown_";
            this.b = "_unknown_";
            this.c = -1;
            this.d = false;
            this.e = false;
            this.f = false;
            try {
                Context c2 = s.c();
                if (c2 != null) {
                    this.f = true;
                    TelephonyManager telephonyManager = (TelephonyManager) c2.getSystemService("phone");
                    this.a = telephonyManager.getSimOperatorName();
                    this.b = telephonyManager.getNetworkOperatorName();
                    this.c = Integer.valueOf(telephonyManager.getPhoneType());
                    this.d = s.i();
                    this.e = s.b(c2);
                }
            } catch (Exception e2) {
                m.a(e2);
            }
        }
    }

    s() {
    }

    @FloatRange(from = 0.0d, to = 1.0d)
    static double a() {
        try {
            return ((double) h()) / ((double) ((AudioManager) a.a().getSystemService("audio")).getStreamMaxVolume(3));
        } catch (Exception e) {
            m.a(e);
            return 0.0d;
        }
    }

    static void a(final Context context) {
        try {
            AsyncTask.execute(new Runnable() {
                public void run() {
                    String str;
                    String str2;
                    String str3;
                    String str4;
                    try {
                        Class<?> cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
                        Class<?> cls2 = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
                        Object invoke = cls.getMethod("getAdvertisingIdInfo", Context.class).invoke(null, context);
                        if (!((Boolean) cls2.getMethod(GpsHelper.IS_LIMIT_AD_TRACKING_ENABLED_KEY, new Class[0]).invoke(invoke, new Object[0])).booleanValue()) {
                            String unused = s.a = (String) cls2.getMethod("getId", new Class[0]).invoke(invoke, new Object[0]);
                            str3 = "Util";
                            str4 = "Retrieved Advertising ID = " + s.a;
                        } else {
                            str3 = "Util";
                            str4 = "User has limited ad tracking";
                        }
                        p.a(3, str3, this, str4);
                    } catch (ClassNotFoundException e) {
                        e = e;
                        str2 = "Util";
                        str = "ClassNotFoundException while retrieving Advertising ID";
                        p.a(str2, this, str, e);
                    } catch (NoSuchMethodException e2) {
                        e = e2;
                        str2 = "Util";
                        str = "NoSuchMethodException while retrieving Advertising ID";
                        p.a(str2, this, str, e);
                    } catch (Exception e3) {
                        m.a(e3);
                    }
                }
            });
        } catch (Exception e) {
            m.a(e);
        }
    }

    static String b() {
        return a;
    }

    static boolean b(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    static Context c() {
        WeakReference<Context> weakReference = ((k) MoatAnalytics.getInstance()).e;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    static a d() {
        if (b == null || !b.a) {
            b = new a();
        }
        return b;
    }

    static b e() {
        if (c == null || !c.f) {
            c = new b();
        }
        return c;
    }

    private static int h() {
        try {
            return ((AudioManager) a.a().getSystemService("audio")).getStreamVolume(3);
        } catch (Exception e) {
            m.a(e);
            return 0;
        }
    }

    /* access modifiers changed from: private */
    public static boolean i() {
        Context c2 = c();
        return (c2 != null ? Build.VERSION.SDK_INT >= 17 ? Settings.Global.getInt(c2.getContentResolver(), "adb_enabled", 0) : Settings.Secure.getInt(c2.getContentResolver(), "adb_enabled", 0) : 0) == 1;
    }
}
