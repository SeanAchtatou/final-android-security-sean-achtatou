package com.moat.analytics.mobile.aol.base.functional;

import java.util.NoSuchElementException;

public final class Optional<T> {

    /* renamed from: ˏ  reason: contains not printable characters */
    private static final Optional<?> f38 = new Optional<>();

    /* renamed from: ॱ  reason: contains not printable characters */
    private final T f39;

    private Optional() {
        this.f39 = null;
    }

    public static <T> Optional<T> empty() {
        return f38;
    }

    private Optional(T t) {
        if (t == null) {
            throw new NullPointerException("Optional of null value.");
        }
        this.f39 = t;
    }

    public static <T> Optional<T> of(T t) {
        return new Optional<>(t);
    }

    public static <T> Optional<T> ofNullable(T t) {
        return t == null ? empty() : of(t);
    }

    public final T get() {
        if (this.f39 != null) {
            return this.f39;
        }
        throw new NoSuchElementException("No value present");
    }

    public final boolean isPresent() {
        return this.f39 != null;
    }

    public final T orElse(T t) {
        return this.f39 != null ? this.f39 : t;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Optional)) {
            return false;
        }
        Optional optional = (Optional) obj;
        return this.f39 == optional.f39 || !(this.f39 == null || optional.f39 == null || !this.f39.equals(optional.f39));
    }

    public final int hashCode() {
        if (this.f39 == null) {
            return 0;
        }
        return this.f39.hashCode();
    }

    public final String toString() {
        if (this.f39 == null) {
            return "Optional.empty";
        }
        return String.format("Optional[%s]", this.f39);
    }
}
