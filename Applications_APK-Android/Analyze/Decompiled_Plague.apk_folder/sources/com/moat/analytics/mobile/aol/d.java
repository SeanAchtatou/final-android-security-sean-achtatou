package com.moat.analytics.mobile.aol;

import android.app.Activity;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import com.moat.analytics.mobile.aol.j;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

abstract class d {

    /* renamed from: ʻ  reason: contains not printable characters */
    private WeakReference<View> f45;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final boolean f46;

    /* renamed from: ʽ  reason: contains not printable characters */
    final boolean f47;

    /* renamed from: ˊ  reason: contains not printable characters */
    TrackerListener f48;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private boolean f49;

    /* renamed from: ˋ  reason: contains not printable characters */
    final String f50;

    /* renamed from: ˎ  reason: contains not printable characters */
    j f51;

    /* renamed from: ˏ  reason: contains not printable characters */
    WeakReference<WebView> f52;

    /* renamed from: ͺ  reason: contains not printable characters */
    private boolean f53;

    /* renamed from: ॱ  reason: contains not printable characters */
    o f54 = null;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final u f55;

    @Deprecated
    public void setActivity(Activity activity) {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public abstract String m38();

    d(@Nullable View view, boolean z, boolean z2) {
        String str;
        a.m8(3, "BaseTracker", this, "Initializing.");
        if (z) {
            str = "m" + hashCode();
        } else {
            str = "";
        }
        this.f50 = str;
        this.f45 = new WeakReference<>(view);
        this.f46 = z;
        this.f47 = z2;
        this.f49 = false;
        this.f53 = false;
        this.f55 = new u();
    }

    public void setListener(TrackerListener trackerListener) {
        this.f48 = trackerListener;
    }

    public void removeListener() {
        this.f48 = null;
    }

    public void startTracking() {
        try {
            a.m8(3, "BaseTracker", this, "In startTracking method.");
            m41();
            if (this.f48 != null) {
                this.f48.onTrackingStarted("Tracking started on " + a.m7(this.f45.get()));
            }
            String str = "startTracking succeeded for " + a.m7(this.f45.get());
            a.m8(3, "BaseTracker", this, str);
            a.m5("[SUCCESS] ", m38() + " " + str);
        } catch (Exception e) {
            m44("startTracking", e);
        }
    }

    @CallSuper
    public void stopTracking() {
        boolean z = false;
        try {
            a.m8(3, "BaseTracker", this, "In stopTracking method.");
            this.f53 = true;
            if (this.f51 != null) {
                this.f51.m98(this);
                z = true;
            }
        } catch (Exception e) {
            o.m132(e);
        }
        StringBuilder sb = new StringBuilder("Attempt to stop tracking ad impression was ");
        sb.append(z ? "" : "un");
        sb.append("successful.");
        a.m8(3, "BaseTracker", this, sb.toString());
        String str = z ? "[SUCCESS] " : "[ERROR] ";
        StringBuilder sb2 = new StringBuilder();
        sb2.append(m38());
        sb2.append(" stopTracking ");
        sb2.append(z ? "succeeded" : "failed");
        sb2.append(" for ");
        sb2.append(a.m7(this.f45.get()));
        a.m5(str, sb2.toString());
        if (this.f48 != null) {
            this.f48.onTrackingStopped("");
            this.f48 = null;
        }
    }

    @CallSuper
    public void changeTargetView(View view) {
        a.m8(3, "BaseTracker", this, "changing view to " + a.m7(view));
        this.f45 = new WeakReference<>(view);
    }

    /* access modifiers changed from: package-private */
    @CallSuper
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m41() throws o {
        a.m8(3, "BaseTracker", this, "Attempting to start impression.");
        m40();
        if (this.f49) {
            throw new o("Tracker already started");
        } else if (this.f53) {
            throw new o("Tracker already stopped");
        } else {
            m39(new ArrayList());
            if (this.f51 != null) {
                this.f51.m99(this);
                this.f49 = true;
                a.m8(3, "BaseTracker", this, "Impression started.");
                return;
            }
            a.m8(3, "BaseTracker", this, "Bridge is null, won't start tracking");
            throw new o("Bridge is null");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m43(WebView webView) throws o {
        if (webView != null) {
            this.f52 = new WeakReference<>(webView);
            if (this.f51 == null) {
                if (!(this.f46 || this.f47)) {
                    a.m8(3, "BaseTracker", this, "Attempting bridge installation.");
                    if (this.f52.get() != null) {
                        this.f51 = new j(this.f52.get(), j.e.f124);
                        a.m8(3, "BaseTracker", this, "Bridge installed.");
                    } else {
                        this.f51 = null;
                        a.m8(3, "BaseTracker", this, "Bridge not installed, WebView is null.");
                    }
                }
            }
            if (this.f51 != null) {
                this.f51.m97(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m40() throws o {
        if (this.f54 != null) {
            throw new o("Tracker initialization failed: " + this.f54.getMessage());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m37() {
        return this.f49 && !this.f53;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final View m35() {
        return this.f45.get();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m36() {
        this.f55.m198(this.f50, this.f45.get());
        return this.f55.f217;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m44(String str, Exception exc) {
        try {
            o.m132(exc);
            String r3 = o.m131(str, exc);
            if (this.f48 != null) {
                this.f48.onTrackingFailedToStart(r3);
            }
            a.m8(3, "BaseTracker", this, r3);
            a.m5("[ERROR] ", m38() + " " + r3);
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m42() throws o {
        if (this.f49) {
            throw new o("Tracker already started");
        } else if (this.f53) {
            throw new o("Tracker already stopped");
        }
    }

    /* access modifiers changed from: package-private */
    @CallSuper
    /* renamed from: ˋ  reason: contains not printable characters */
    public void m39(List<String> list) throws o {
        if (this.f45.get() == null && !this.f47) {
            list.add("Tracker's target view is null");
        }
        if (!list.isEmpty()) {
            throw new o(TextUtils.join(" and ", list));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m34() {
        return a.m7(this.f45.get());
    }
}
