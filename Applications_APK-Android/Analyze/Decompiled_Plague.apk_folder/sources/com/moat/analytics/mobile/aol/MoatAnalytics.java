package com.moat.analytics.mobile.aol;

import android.app.Application;
import android.support.annotation.UiThread;
import com.moat.analytics.mobile.aol.NoOp;

public abstract class MoatAnalytics {

    /* renamed from: ˏ  reason: contains not printable characters */
    private static MoatAnalytics f13;

    @UiThread
    public abstract void prepareNativeDisplayTracking(String str);

    public abstract void start(Application application);

    public abstract void start(MoatOptions moatOptions, Application application);

    public static synchronized MoatAnalytics getInstance() {
        MoatAnalytics moatAnalytics;
        synchronized (MoatAnalytics.class) {
            if (f13 == null) {
                try {
                    f13 = new f();
                } catch (Exception e) {
                    o.m132(e);
                    f13 = new NoOp.MoatAnalytics();
                }
            }
            moatAnalytics = f13;
        }
        return moatAnalytics;
    }
}
