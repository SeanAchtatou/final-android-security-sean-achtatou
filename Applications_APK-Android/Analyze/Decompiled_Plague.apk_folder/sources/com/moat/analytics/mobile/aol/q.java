package com.moat.analytics.mobile.aol;

import android.graphics.Rect;
import android.view.View;
import com.moat.analytics.mobile.aol.NativeDisplayTracker;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

final class q extends d implements NativeDisplayTracker {

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private final Map<String, String> f159;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final Set<NativeDisplayTracker.MoatUserInteractionType> f160 = new HashSet();

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m145() {
        return "NativeDisplayTracker";
    }

    q(View view, Map<String, String> map) {
        super(view, true, false);
        a.m8(3, "NativeDisplayTracker", this, "Initializing.");
        this.f159 = map;
        if (view == null) {
            String str = "NativeDisplayTracker initialization not successful, " + "Target view is null";
            a.m8(3, "NativeDisplayTracker", this, str);
            a.m5("[ERROR] ", str);
            this.f54 = new o("Target view is null");
        } else if (map == null || map.isEmpty()) {
            String str2 = "NativeDisplayTracker initialization not successful, " + "AdIds is null or empty";
            a.m8(3, "NativeDisplayTracker", this, str2);
            a.m5("[ERROR] ", str2);
            this.f54 = new o("AdIds is null or empty");
        } else {
            a aVar = ((f) f.getInstance()).f65;
            if (aVar == null) {
                String str3 = "NativeDisplayTracker initialization not successful, " + "prepareNativeDisplayTracking was not called successfully";
                a.m8(3, "NativeDisplayTracker", this, str3);
                a.m5("[ERROR] ", str3);
                this.f54 = new o("prepareNativeDisplayTracking was not called successfully");
                return;
            }
            this.f51 = aVar.f20;
            try {
                super.m43(aVar.f18);
                if (this.f51 != null) {
                    this.f51.m102(m143());
                }
                a.m5("[SUCCESS] ", "NativeDisplayTracker created for " + m34() + ", with adIds:" + map.toString());
            } catch (o e) {
                this.f54 = e;
            }
        }
    }

    public final void reportUserInteractionEvent(NativeDisplayTracker.MoatUserInteractionType moatUserInteractionType) {
        try {
            a.m8(3, "NativeDisplayTracker", this, "reportUserInteractionEvent:" + moatUserInteractionType.name());
            if (!this.f160.contains(moatUserInteractionType)) {
                this.f160.add(moatUserInteractionType);
                JSONObject jSONObject = new JSONObject();
                jSONObject.accumulate("adKey", this.f50);
                jSONObject.accumulate("event", moatUserInteractionType.name().toLowerCase());
                if (this.f51 != null) {
                    this.f51.m103(jSONObject.toString());
                }
            }
        } catch (JSONException e) {
            a.m9("NativeDisplayTracker", this, "Got JSON exception");
            o.m132(e);
        } catch (Exception e2) {
            o.m132(e2);
        }
    }

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private String m143() {
        try {
            Map<String, String> map = this.f159;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (int i = 0; i < 8; i++) {
                String str = "moatClientLevel" + i;
                if (map.containsKey(str)) {
                    linkedHashMap.put(str, map.get(str));
                }
            }
            for (int i2 = 0; i2 < 8; i2++) {
                String str2 = "moatClientSlicer" + i2;
                if (map.containsKey(str2)) {
                    linkedHashMap.put(str2, map.get(str2));
                }
            }
            for (String next : map.keySet()) {
                if (!linkedHashMap.containsKey(next)) {
                    linkedHashMap.put(next, map.get(next));
                }
            }
            String jSONObject = new JSONObject(linkedHashMap).toString();
            a.m8(3, "NativeDisplayTracker", this, "Parsed ad ids = " + jSONObject);
            return "{\"adIds\":" + jSONObject + ", \"adKey\":\"" + this.f50 + "\", \"adSize\":" + m144() + "}";
        } catch (Exception e) {
            o.m132(e);
            return "";
        }
    }

    /* renamed from: ᐝ  reason: contains not printable characters */
    private String m144() {
        try {
            Rect r0 = u.m191(super.m35());
            int width = r0.width();
            int height = r0.height();
            HashMap hashMap = new HashMap();
            hashMap.put("width", Integer.toString(width));
            hashMap.put("height", Integer.toString(height));
            return new JSONObject(hashMap).toString();
        } catch (Exception e) {
            o.m132(e);
            return null;
        }
    }
}
