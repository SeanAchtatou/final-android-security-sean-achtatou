package com.moat.analytics.mobile.mpub;

import android.app.Activity;
import android.graphics.Rect;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.widget.ExploreByTouchHelper;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

class z {
    String a = "{}";
    private c b = new c();
    private JSONObject c;
    private Rect d;
    private Rect e;
    private JSONObject f;
    private JSONObject g;
    private Location h;
    private Map<String, Object> i = new HashMap();

    static class a {
        int a = 0;
        final Set<Rect> b = new HashSet();
        boolean c = false;

        a() {
        }
    }

    private static class b {
        final View a;
        final Rect b;

        b(View view, b bVar) {
            this.a = view;
            this.b = bVar != null ? z.b(view, bVar.b.left, bVar.b.top) : z.k(view);
        }
    }

    private static class c {
        Rect a = new Rect(0, 0, 0, 0);
        double b = 0.0d;
        double c = 0.0d;

        c() {
        }
    }

    z() {
    }

    @VisibleForTesting
    static int a(Rect rect, Set<Rect> set) {
        int i2 = 0;
        if (set.isEmpty()) {
            return 0;
        }
        ArrayList<Rect> arrayList = new ArrayList<>();
        arrayList.addAll(set);
        Collections.sort(arrayList, new Comparator<Rect>() {
            /* renamed from: a */
            public int compare(Rect rect, Rect rect2) {
                return Integer.valueOf(rect.top).compareTo(Integer.valueOf(rect2.top));
            }
        });
        ArrayList arrayList2 = new ArrayList();
        for (Rect rect2 : arrayList) {
            arrayList2.add(Integer.valueOf(rect2.left));
            arrayList2.add(Integer.valueOf(rect2.right));
        }
        Collections.sort(arrayList2);
        int i3 = 0;
        while (i2 < arrayList2.size() - 1) {
            int i4 = i2 + 1;
            if (!((Integer) arrayList2.get(i2)).equals(arrayList2.get(i4))) {
                Rect rect3 = new Rect(((Integer) arrayList2.get(i2)).intValue(), rect.top, ((Integer) arrayList2.get(i4)).intValue(), rect.bottom);
                int i5 = rect.top;
                for (Rect rect4 : arrayList) {
                    if (Rect.intersects(rect4, rect3)) {
                        if (rect4.bottom > i5) {
                            i3 += rect3.width() * (rect4.bottom - Math.max(i5, rect4.top));
                            i5 = rect4.bottom;
                        }
                        if (rect4.bottom == rect3.bottom) {
                            break;
                        }
                    }
                }
            }
            i2 = i4;
        }
        return i3;
    }

    private static Rect a(DisplayMetrics displayMetrics) {
        return new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    static Rect a(View view) {
        return view != null ? k(view) : new Rect(0, 0, 0, 0);
    }

    @VisibleForTesting
    static a a(Rect rect, @NonNull View view) {
        a aVar = new a();
        try {
            ArrayDeque<View> i2 = i(view);
            if (i2 == null || i2.isEmpty()) {
                return aVar;
            }
            p.b(2, "VisibilityInfo", view, "starting covering rect search");
            b bVar = null;
            while (!i2.isEmpty()) {
                View pollLast = i2.pollLast();
                b bVar2 = new b(pollLast, bVar);
                if (pollLast.getParent() != null) {
                    if (pollLast.getParent() instanceof ViewGroup) {
                        ViewGroup viewGroup = (ViewGroup) pollLast.getParent();
                        int childCount = viewGroup.getChildCount();
                        boolean z = false;
                        for (int i3 = 0; i3 < childCount; i3++) {
                            if (aVar.a >= 500) {
                                p.a(3, "VisibilityInfo", (Object) null, "Short-circuiting cover retrieval, reached max");
                                return aVar;
                            }
                            View childAt = viewGroup.getChildAt(i3);
                            if (childAt == pollLast) {
                                z = true;
                            } else {
                                aVar.a++;
                                if (a(childAt, pollLast, z)) {
                                    b(new b(childAt, bVar), rect, aVar);
                                    if (aVar.c) {
                                        return aVar;
                                    }
                                } else {
                                    continue;
                                }
                            }
                        }
                        continue;
                    } else {
                        continue;
                    }
                }
                bVar = bVar2;
            }
            return aVar;
        } catch (Exception e2) {
            m.a(e2);
        }
    }

    private static c a(View view, Rect rect, boolean z, boolean z2, boolean z3) {
        c cVar = new c();
        int b2 = b(rect);
        if (view != null && z && z2 && !z3 && b2 > 0) {
            Rect rect2 = new Rect(0, 0, 0, 0);
            if (a(view, rect2)) {
                int b3 = b(rect2);
                if (b3 < b2) {
                    p.b(2, "VisibilityInfo", null, "Ad is clipped");
                }
                if (view.getRootView() instanceof ViewGroup) {
                    cVar.a = rect2;
                    a a2 = a(rect2, view);
                    if (a2.c) {
                        cVar.c = 1.0d;
                        return cVar;
                    }
                    int a3 = a(rect2, a2.b);
                    if (a3 > 0) {
                        cVar.c = ((double) a3) / (((double) b3) * 1.0d);
                    }
                    cVar.b = ((double) (b3 - a3)) / (((double) b2) * 1.0d);
                }
            }
        }
        return cVar;
    }

    private static Map<String, String> a(Rect rect) {
        HashMap hashMap = new HashMap();
        hashMap.put("x", String.valueOf(rect.left));
        hashMap.put("y", String.valueOf(rect.top));
        hashMap.put("w", String.valueOf(rect.right - rect.left));
        hashMap.put("h", String.valueOf(rect.bottom - rect.top));
        return hashMap;
    }

    private static Map<String, String> a(Rect rect, DisplayMetrics displayMetrics) {
        return a(b(rect, displayMetrics));
    }

    private static JSONObject a(Location location) {
        Map<String, String> b2 = b(location);
        if (b2 == null) {
            return null;
        }
        return new JSONObject(b2);
    }

    private static void a(b bVar, Rect rect, a aVar) {
        Rect rect2 = bVar.b;
        if (rect2.setIntersect(rect, rect2)) {
            if (Build.VERSION.SDK_INT >= 22) {
                Rect rect3 = new Rect(0, 0, 0, 0);
                if (a(bVar.a, rect3)) {
                    Rect rect4 = bVar.b;
                    if (rect4.setIntersect(rect3, rect4)) {
                        rect2 = rect4;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
            if (w.a().c) {
                p.b(2, "VisibilityInfo", bVar.a, String.format(Locale.ROOT, "Covered by %s-%s alpha=%f", bVar.a.getClass().getName(), rect2.toString(), Float.valueOf(bVar.a.getAlpha())));
            }
            aVar.b.add(rect2);
            if (rect2.contains(rect)) {
                aVar.c = true;
            }
        }
    }

    private static boolean a(View view, Rect rect) {
        if (!view.getGlobalVisibleRect(rect)) {
            return false;
        }
        int[] iArr = {ExploreByTouchHelper.INVALID_ID, ExploreByTouchHelper.INVALID_ID};
        view.getLocationInWindow(iArr);
        int[] iArr2 = {ExploreByTouchHelper.INVALID_ID, ExploreByTouchHelper.INVALID_ID};
        view.getLocationOnScreen(iArr2);
        rect.offset(iArr2[0] - iArr[0], iArr2[1] - iArr[1]);
        return true;
    }

    private static boolean a(View view, View view2, boolean z) {
        return z ? Build.VERSION.SDK_INT < 21 || view.getZ() >= view2.getZ() : Build.VERSION.SDK_INT >= 21 && view.getZ() > view2.getZ();
    }

    private static int b(Rect rect) {
        return rect.width() * rect.height();
    }

    private static Rect b(Rect rect, DisplayMetrics displayMetrics) {
        float f2 = displayMetrics.density;
        if (f2 == 0.0f) {
            return rect;
        }
        return new Rect(Math.round(((float) rect.left) / f2), Math.round(((float) rect.top) / f2), Math.round(((float) rect.right) / f2), Math.round(((float) rect.bottom) / f2));
    }

    /* access modifiers changed from: private */
    public static Rect b(View view, int i2, int i3) {
        int left = i2 + view.getLeft();
        int top = i3 + view.getTop();
        return new Rect(left, top, view.getWidth() + left, view.getHeight() + top);
    }

    private static Map<String, String> b(Location location) {
        if (location == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("latitude", Double.toString(location.getLatitude()));
        hashMap.put("longitude", Double.toString(location.getLongitude()));
        hashMap.put("timestamp", Long.toString(location.getTime()));
        hashMap.put("horizontalAccuracy", Float.toString(location.getAccuracy()));
        return hashMap;
    }

    private static void b(b bVar, Rect rect, a aVar) {
        boolean z;
        if (h(bVar.a)) {
            if (bVar.a instanceof ViewGroup) {
                int i2 = 0;
                z = !ViewGroup.class.equals(bVar.a.getClass().getSuperclass()) || !j(bVar.a);
                ViewGroup viewGroup = (ViewGroup) bVar.a;
                int childCount = viewGroup.getChildCount();
                while (i2 < childCount) {
                    int i3 = aVar.a + 1;
                    aVar.a = i3;
                    if (i3 <= 500) {
                        b(new b(viewGroup.getChildAt(i2), bVar), rect, aVar);
                        if (!aVar.c) {
                            i2++;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            } else {
                z = true;
            }
            if (z) {
                a(bVar, rect, aVar);
            }
        }
    }

    private static boolean c(View view) {
        return Build.VERSION.SDK_INT >= 19 ? view != null && view.isAttachedToWindow() : (view == null || view.getWindowToken() == null) ? false : true;
    }

    private static boolean d(View view) {
        return view != null && view.hasWindowFocus();
    }

    private static boolean e(View view) {
        return view == null || !view.isShown();
    }

    private static float f(View view) {
        if (view == null) {
            return 0.0f;
        }
        return g(view);
    }

    private static float g(View view) {
        float alpha = view.getAlpha();
        while (view != null && view.getParent() != null && ((double) alpha) != 0.0d && (view.getParent() instanceof View)) {
            alpha *= ((View) view.getParent()).getAlpha();
            view = (View) view.getParent();
        }
        return alpha;
    }

    private static boolean h(View view) {
        return view.isShown() && ((double) view.getAlpha()) > 0.0d;
    }

    private static ArrayDeque<View> i(@NonNull View view) {
        ArrayDeque<View> arrayDeque = new ArrayDeque<>();
        int i2 = 0;
        View view2 = view;
        while (true) {
            if (view2.getParent() == null && view2 != view.getRootView()) {
                break;
            }
            i2++;
            if (i2 <= 50) {
                arrayDeque.add(view2);
                if (!(view2.getParent() instanceof View)) {
                    break;
                }
                view2 = (View) view2.getParent();
            } else {
                p.a(3, "VisibilityInfo", (Object) null, "Short-circuiting chain retrieval, reached max");
                return arrayDeque;
            }
        }
        return arrayDeque;
    }

    private static boolean j(View view) {
        return Build.VERSION.SDK_INT < 19 || view.getBackground() == null || view.getBackground().getAlpha() == 0;
    }

    /* access modifiers changed from: private */
    public static Rect k(View view) {
        int[] iArr = {ExploreByTouchHelper.INVALID_ID, ExploreByTouchHelper.INVALID_ID};
        view.getLocationOnScreen(iArr);
        int i2 = iArr[0];
        int i3 = iArr[1];
        return new Rect(i2, i3, view.getWidth() + i2, view.getHeight() + i3);
    }

    private static DisplayMetrics l(View view) {
        Activity activity;
        if (Build.VERSION.SDK_INT < 17 || a.a == null || (activity = a.a.get()) == null) {
            return view.getContext().getResources().getDisplayMetrics();
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        return displayMetrics;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0102 A[Catch:{ Exception -> 0x0150 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0107 A[Catch:{ Exception -> 0x0150 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x014d A[SYNTHETIC, Splitter:B:42:0x014d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r12, android.view.View r13) {
        /*
            r11 = this;
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            java.lang.String r1 = "{}"
            if (r13 == 0) goto L_0x0156
            android.util.DisplayMetrics r2 = l(r13)     // Catch:{ Exception -> 0x0150 }
            boolean r3 = c(r13)     // Catch:{ Exception -> 0x0150 }
            boolean r4 = d(r13)     // Catch:{ Exception -> 0x0150 }
            boolean r5 = e(r13)     // Catch:{ Exception -> 0x0150 }
            float r6 = f(r13)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r7 = "dr"
            float r8 = r2.density     // Catch:{ Exception -> 0x0150 }
            java.lang.Float r8 = java.lang.Float.valueOf(r8)     // Catch:{ Exception -> 0x0150 }
            r0.put(r7, r8)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r7 = "dv"
            double r8 = com.moat.analytics.mobile.mpub.s.a()     // Catch:{ Exception -> 0x0150 }
            java.lang.Double r8 = java.lang.Double.valueOf(r8)     // Catch:{ Exception -> 0x0150 }
            r0.put(r7, r8)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r7 = "adKey"
            r0.put(r7, r12)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r12 = "isAttached"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0150 }
            r0.put(r12, r7)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r12 = "inFocus"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0150 }
            r0.put(r12, r7)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r12 = "isHidden"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0150 }
            r0.put(r12, r7)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r12 = "opacity"
            java.lang.Float r6 = java.lang.Float.valueOf(r6)     // Catch:{ Exception -> 0x0150 }
            r0.put(r12, r6)     // Catch:{ Exception -> 0x0150 }
            android.graphics.Rect r12 = a(r2)     // Catch:{ Exception -> 0x0150 }
            android.graphics.Rect r6 = a(r13)     // Catch:{ Exception -> 0x0150 }
            com.moat.analytics.mobile.mpub.z$c r13 = a(r13, r6, r3, r4, r5)     // Catch:{ Exception -> 0x0150 }
            org.json.JSONObject r3 = r11.c     // Catch:{ Exception -> 0x0150 }
            r4 = 1
            if (r3 == 0) goto L_0x0092
            double r7 = r13.b     // Catch:{ Exception -> 0x0150 }
            com.moat.analytics.mobile.mpub.z$c r3 = r11.b     // Catch:{ Exception -> 0x0150 }
            double r9 = r3.b     // Catch:{ Exception -> 0x0150 }
            int r3 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r3 != 0) goto L_0x0092
            android.graphics.Rect r3 = r13.a     // Catch:{ Exception -> 0x0150 }
            com.moat.analytics.mobile.mpub.z$c r5 = r11.b     // Catch:{ Exception -> 0x0150 }
            android.graphics.Rect r5 = r5.a     // Catch:{ Exception -> 0x0150 }
            boolean r3 = r3.equals(r5)     // Catch:{ Exception -> 0x0150 }
            if (r3 == 0) goto L_0x0092
            double r7 = r13.c     // Catch:{ Exception -> 0x0150 }
            com.moat.analytics.mobile.mpub.z$c r3 = r11.b     // Catch:{ Exception -> 0x0150 }
            double r9 = r3.c     // Catch:{ Exception -> 0x0150 }
            int r3 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r3 == 0) goto L_0x0090
            goto L_0x0092
        L_0x0090:
            r3 = 0
            goto L_0x00a4
        L_0x0092:
            r11.b = r13     // Catch:{ Exception -> 0x0150 }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x0150 }
            com.moat.analytics.mobile.mpub.z$c r5 = r11.b     // Catch:{ Exception -> 0x0150 }
            android.graphics.Rect r5 = r5.a     // Catch:{ Exception -> 0x0150 }
            java.util.Map r5 = a(r5, r2)     // Catch:{ Exception -> 0x0150 }
            r3.<init>(r5)     // Catch:{ Exception -> 0x0150 }
            r11.c = r3     // Catch:{ Exception -> 0x0150 }
            r3 = r4
        L_0x00a4:
            java.lang.String r5 = "coveredPercent"
            double r7 = r13.c     // Catch:{ Exception -> 0x0150 }
            java.lang.Double r13 = java.lang.Double.valueOf(r7)     // Catch:{ Exception -> 0x0150 }
            r0.put(r5, r13)     // Catch:{ Exception -> 0x0150 }
            org.json.JSONObject r13 = r11.g     // Catch:{ Exception -> 0x0150 }
            if (r13 == 0) goto L_0x00bb
            android.graphics.Rect r13 = r11.e     // Catch:{ Exception -> 0x0150 }
            boolean r13 = r12.equals(r13)     // Catch:{ Exception -> 0x0150 }
            if (r13 != 0) goto L_0x00c9
        L_0x00bb:
            r11.e = r12     // Catch:{ Exception -> 0x0150 }
            org.json.JSONObject r13 = new org.json.JSONObject     // Catch:{ Exception -> 0x0150 }
            java.util.Map r12 = a(r12, r2)     // Catch:{ Exception -> 0x0150 }
            r13.<init>(r12)     // Catch:{ Exception -> 0x0150 }
            r11.g = r13     // Catch:{ Exception -> 0x0150 }
            r3 = r4
        L_0x00c9:
            org.json.JSONObject r12 = r11.f     // Catch:{ Exception -> 0x0150 }
            if (r12 == 0) goto L_0x00d5
            android.graphics.Rect r12 = r11.d     // Catch:{ Exception -> 0x0150 }
            boolean r12 = r6.equals(r12)     // Catch:{ Exception -> 0x0150 }
            if (r12 != 0) goto L_0x00e3
        L_0x00d5:
            r11.d = r6     // Catch:{ Exception -> 0x0150 }
            org.json.JSONObject r12 = new org.json.JSONObject     // Catch:{ Exception -> 0x0150 }
            java.util.Map r13 = a(r6, r2)     // Catch:{ Exception -> 0x0150 }
            r12.<init>(r13)     // Catch:{ Exception -> 0x0150 }
            r11.f = r12     // Catch:{ Exception -> 0x0150 }
            r3 = r4
        L_0x00e3:
            java.util.Map<java.lang.String, java.lang.Object> r12 = r11.i     // Catch:{ Exception -> 0x0150 }
            if (r12 == 0) goto L_0x00ef
            java.util.Map<java.lang.String, java.lang.Object> r12 = r11.i     // Catch:{ Exception -> 0x0150 }
            boolean r12 = r0.equals(r12)     // Catch:{ Exception -> 0x0150 }
            if (r12 != 0) goto L_0x00f2
        L_0x00ef:
            r11.i = r0     // Catch:{ Exception -> 0x0150 }
            r3 = r4
        L_0x00f2:
            com.moat.analytics.mobile.mpub.o r12 = com.moat.analytics.mobile.mpub.o.a()     // Catch:{ Exception -> 0x0150 }
            android.location.Location r12 = r12.b()     // Catch:{ Exception -> 0x0150 }
            android.location.Location r13 = r11.h     // Catch:{ Exception -> 0x0150 }
            boolean r13 = com.moat.analytics.mobile.mpub.o.a(r12, r13)     // Catch:{ Exception -> 0x0150 }
            if (r13 != 0) goto L_0x0105
            r11.h = r12     // Catch:{ Exception -> 0x0150 }
            r3 = r4
        L_0x0105:
            if (r3 == 0) goto L_0x014d
            org.json.JSONObject r13 = new org.json.JSONObject     // Catch:{ Exception -> 0x0150 }
            java.util.Map<java.lang.String, java.lang.Object> r0 = r11.i     // Catch:{ Exception -> 0x0150 }
            r13.<init>(r0)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r0 = "screen"
            org.json.JSONObject r2 = r11.g     // Catch:{ Exception -> 0x0150 }
            r13.accumulate(r0, r2)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r0 = "view"
            org.json.JSONObject r2 = r11.f     // Catch:{ Exception -> 0x0150 }
            r13.accumulate(r0, r2)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r0 = "visible"
            org.json.JSONObject r2 = r11.c     // Catch:{ Exception -> 0x0150 }
            r13.accumulate(r0, r2)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r0 = "maybe"
            org.json.JSONObject r2 = r11.c     // Catch:{ Exception -> 0x0150 }
            r13.accumulate(r0, r2)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r0 = "visiblePercent"
            com.moat.analytics.mobile.mpub.z$c r2 = r11.b     // Catch:{ Exception -> 0x0150 }
            double r2 = r2.b     // Catch:{ Exception -> 0x0150 }
            java.lang.Double r2 = java.lang.Double.valueOf(r2)     // Catch:{ Exception -> 0x0150 }
            r13.accumulate(r0, r2)     // Catch:{ Exception -> 0x0150 }
            if (r12 == 0) goto L_0x0142
            java.lang.String r0 = "location"
            org.json.JSONObject r12 = a(r12)     // Catch:{ Exception -> 0x0150 }
            r13.accumulate(r0, r12)     // Catch:{ Exception -> 0x0150 }
        L_0x0142:
            java.lang.String r12 = r13.toString()     // Catch:{ Exception -> 0x0150 }
            r11.a = r12     // Catch:{ Exception -> 0x0149 }
            return
        L_0x0149:
            r13 = move-exception
            r1 = r12
            r12 = r13
            goto L_0x0151
        L_0x014d:
            java.lang.String r12 = r11.a     // Catch:{ Exception -> 0x0150 }
            return
        L_0x0150:
            r12 = move-exception
        L_0x0151:
            com.moat.analytics.mobile.mpub.m.a(r12)
            r11.a = r1
        L_0x0156:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.mpub.z.a(java.lang.String, android.view.View):void");
    }
}
