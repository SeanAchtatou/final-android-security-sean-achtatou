package com.moat.analytics.mobile.aol;

public interface VideoTrackerListener {
    void onVideoEventReported(MoatAdEventType moatAdEventType);
}
