package com.moat.analytics.mobile.aol;

import android.support.annotation.CallSuper;
import android.support.v4.widget.ExploreByTouchHelper;
import android.view.View;
import java.util.Map;
import org.json.JSONObject;

abstract class i extends b {

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private int f87 = c.f97;

    /* renamed from: ˏॱ  reason: contains not printable characters */
    private int f88 = ExploreByTouchHelper.INVALID_ID;

    /* renamed from: ͺ  reason: contains not printable characters */
    private int f89 = ExploreByTouchHelper.INVALID_ID;

    /* renamed from: ॱˊ  reason: contains not printable characters */
    private double f90 = Double.NaN;

    /* renamed from: ॱˋ  reason: contains not printable characters */
    private int f91 = ExploreByTouchHelper.INVALID_ID;

    /* renamed from: ॱˎ  reason: contains not printable characters */
    private int f92 = 0;

    /* access modifiers changed from: package-private */
    /* renamed from: ͺ  reason: contains not printable characters */
    public abstract boolean m71();

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˋ  reason: contains not printable characters */
    public abstract Integer m73();

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˎ  reason: contains not printable characters */
    public abstract boolean m74();

    /* access modifiers changed from: package-private */
    /* renamed from: ॱᐝ  reason: contains not printable characters */
    public abstract Integer m75();

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    static final class c extends Enum<c> {

        /* renamed from: ˊ  reason: contains not printable characters */
        public static final int f94 = 2;

        /* renamed from: ˋ  reason: contains not printable characters */
        public static final int f95 = 4;

        /* renamed from: ˎ  reason: contains not printable characters */
        public static final int f96 = 3;

        /* renamed from: ˏ  reason: contains not printable characters */
        public static final int f97 = 1;

        /* renamed from: ॱ  reason: contains not printable characters */
        public static final int f98 = 5;

        static {
            int[] iArr = {1, 2, 3, 4, 5};
        }
    }

    i(String str) {
        super(str);
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    public final boolean m72(Map<String, String> map, View view) {
        try {
            boolean r4 = super.m23(map, view);
            if (!r4) {
                return r4;
            }
            this.f36.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (!i.this.m71() || i.this.m19()) {
                            i.this.m22();
                        } else if (Boolean.valueOf(i.this.m69()).booleanValue()) {
                            i.this.f36.postDelayed(this, 200);
                        } else {
                            i.this.m22();
                        }
                    } catch (Exception e) {
                        i.this.m22();
                        o.m132(e);
                    }
                }
            }, 200);
            return r4;
        } catch (Exception e) {
            a.m8(3, "IntervalVideoTracker", this, "Problem with video loop");
            m44("trackVideoAd", e);
            return false;
        }
    }

    public void stopTracking() {
        try {
            dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE));
            super.stopTracking();
        } catch (Exception e) {
            o.m132(e);
        }
    }

    public void setPlayerVolume(Double d) {
        super.setPlayerVolume(d);
        this.f90 = m17().doubleValue();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final JSONObject m70(MoatAdEvent moatAdEvent) {
        Integer num;
        if (!moatAdEvent.f8.equals(MoatAdEvent.f4)) {
            num = moatAdEvent.f8;
        } else {
            try {
                num = m73();
            } catch (Exception unused) {
                num = Integer.valueOf(this.f88);
            }
            moatAdEvent.f8 = num;
        }
        if (moatAdEvent.f8.intValue() < 0 || (moatAdEvent.f8.intValue() == 0 && moatAdEvent.f9 == MoatAdEventType.AD_EVT_COMPLETE && this.f88 > 0)) {
            num = Integer.valueOf(this.f88);
            moatAdEvent.f8 = num;
        }
        if (moatAdEvent.f9 == MoatAdEventType.AD_EVT_COMPLETE) {
            if (num.intValue() == Integer.MIN_VALUE || this.f89 == Integer.MIN_VALUE || !m15(num, Integer.valueOf(this.f89))) {
                this.f87 = c.f95;
                moatAdEvent.f9 = MoatAdEventType.AD_EVT_STOPPED;
            } else {
                this.f87 = c.f98;
            }
        }
        return super.m20(moatAdEvent);
    }

    /* access modifiers changed from: package-private */
    @CallSuper
    /* renamed from: ʻॱ  reason: contains not printable characters */
    public final boolean m69() throws o {
        if (!m71() || m19()) {
            return false;
        }
        try {
            int intValue = m73().intValue();
            if (this.f88 >= 0 && intValue < 0) {
                return false;
            }
            this.f88 = intValue;
            if (intValue == 0) {
                return true;
            }
            int intValue2 = m75().intValue();
            boolean r4 = m74();
            double d = ((double) intValue2) / 4.0d;
            double doubleValue = m17().doubleValue();
            MoatAdEventType moatAdEventType = null;
            if (intValue > this.f91) {
                this.f91 = intValue;
            }
            if (this.f89 == Integer.MIN_VALUE) {
                this.f89 = intValue2;
            }
            if (r4) {
                if (this.f87 == c.f97) {
                    moatAdEventType = MoatAdEventType.AD_EVT_START;
                    this.f87 = c.f96;
                } else if (this.f87 == c.f94) {
                    moatAdEventType = MoatAdEventType.AD_EVT_PLAYING;
                    this.f87 = c.f96;
                } else {
                    int floor = ((int) Math.floor(((double) intValue) / d)) - 1;
                    if (floor >= 0 && floor < 3) {
                        MoatAdEventType moatAdEventType2 = f26[floor];
                        if (!this.f27.containsKey(moatAdEventType2)) {
                            this.f27.put(moatAdEventType2, 1);
                            moatAdEventType = moatAdEventType2;
                        }
                    }
                }
            } else if (this.f87 != c.f94) {
                moatAdEventType = MoatAdEventType.AD_EVT_PAUSED;
                this.f87 = c.f94;
            }
            boolean z = moatAdEventType != null;
            if (!z && !Double.isNaN(this.f90) && Math.abs(this.f90 - doubleValue) > 0.05d) {
                moatAdEventType = MoatAdEventType.AD_EVT_VOLUME_CHANGE;
                z = true;
            }
            if (z) {
                dispatchEvent(new MoatAdEvent(moatAdEventType, Integer.valueOf(intValue), m24()));
            }
            this.f90 = doubleValue;
            this.f92 = 0;
            return true;
        } catch (Exception unused) {
            int i = this.f92;
            this.f92 = i + 1;
            return i < 5;
        }
    }
}
