package com.moat.analytics.mobile.aol;

import android.support.v4.widget.ExploreByTouchHelper;
import java.util.HashMap;
import java.util.Map;

public class MoatAdEvent {
    public static final Double VOLUME_MUTED = Double.valueOf(0.0d);
    public static final Double VOLUME_UNMUTED = Double.valueOf(1.0d);

    /* renamed from: ˋ  reason: contains not printable characters */
    static final Integer f4 = Integer.valueOf((int) ExploreByTouchHelper.INVALID_ID);

    /* renamed from: ˎ  reason: contains not printable characters */
    private static final Double f5 = Double.valueOf(Double.NaN);

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Long f6;

    /* renamed from: ˊ  reason: contains not printable characters */
    Double f7;

    /* renamed from: ˏ  reason: contains not printable characters */
    Integer f8;

    /* renamed from: ॱ  reason: contains not printable characters */
    MoatAdEventType f9;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final Double f10;

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num, Double d) {
        this.f6 = Long.valueOf(System.currentTimeMillis());
        this.f9 = moatAdEventType;
        this.f7 = d;
        this.f8 = num;
        this.f10 = Double.valueOf(r.m156());
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num) {
        this(moatAdEventType, num, f5);
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType) {
        this(moatAdEventType, f4, f5);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public final Map<String, Object> m2() {
        HashMap hashMap = new HashMap();
        hashMap.put(com.moat.analytics.mobile.tjy.MoatAdEvent.EVENT_AD_VOLUME, this.f7);
        hashMap.put(com.moat.analytics.mobile.tjy.MoatAdEvent.EVENT_PLAY_HEAD, this.f8);
        hashMap.put(com.moat.analytics.mobile.tjy.MoatAdEvent.EVENT_TS, this.f6);
        hashMap.put("type", this.f9.toString());
        hashMap.put("deviceVolume", this.f10);
        return hashMap;
    }
}
