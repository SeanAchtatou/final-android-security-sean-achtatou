package com.moat.analytics.mobile.aol;

import android.os.Handler;
import android.os.Looper;
import com.tapjoy.TapjoyConstants;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

final class t {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static t f179;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final Queue<e> f180 = new ConcurrentLinkedQueue();
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public long f181 = 60000;

    /* renamed from: ˊ  reason: contains not printable characters */
    volatile int f182 = 200;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private long f183 = TapjoyConstants.SESSION_ID_INACTIVITY_TIME;

    /* renamed from: ˋ  reason: contains not printable characters */
    volatile boolean f184 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˋॱ  reason: contains not printable characters */
    public final AtomicBoolean f185 = new AtomicBoolean(false);

    /* renamed from: ˎ  reason: contains not printable characters */
    volatile int f186 = a.f198;

    /* renamed from: ˏ  reason: contains not printable characters */
    volatile boolean f187 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˏॱ  reason: contains not printable characters */
    public final AtomicBoolean f188 = new AtomicBoolean(false);

    /* renamed from: ॱ  reason: contains not printable characters */
    volatile int f189 = 10;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ  reason: contains not printable characters */
    public final AtomicInteger f190 = new AtomicInteger(0);
    /* access modifiers changed from: private */

    /* renamed from: ॱˋ  reason: contains not printable characters */
    public volatile long f191 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ᐝ  reason: contains not printable characters */
    public Handler f192;

    interface b {
        /* renamed from: ˎ  reason: contains not printable characters */
        void m184() throws o;
    }

    interface c {
        /* renamed from: ˏ  reason: contains not printable characters */
        void m185(g gVar) throws o;
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    static final class a extends Enum<a> {

        /* renamed from: ˎ  reason: contains not printable characters */
        public static final int f197 = 2;

        /* renamed from: ॱ  reason: contains not printable characters */
        public static final int f198 = 1;

        static {
            int[] iArr = {1, 2};
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static synchronized t m176() {
        t tVar;
        synchronized (t.class) {
            if (f179 == null) {
                f179 = new t();
            }
            tVar = f179;
        }
        return tVar;
    }

    private t() {
        try {
            this.f192 = new Handler(Looper.getMainLooper());
        } catch (Exception e2) {
            o.m132(e2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m182() {
        if (System.currentTimeMillis() - this.f191 > this.f183) {
            m177(0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m177(final long j) {
        if (this.f188.compareAndSet(false, true)) {
            a.m8(3, "OnOff", this, "Performing status check.");
            new Thread() {
                public final void run() {
                    Looper.prepare();
                    Handler handler = new Handler();
                    handler.postDelayed(new d(BuildConfig.NAMESPACE, handler, new c() {
                        /* renamed from: ˏ  reason: contains not printable characters */
                        public final void m183(g gVar) throws o {
                            synchronized (t.f180) {
                                boolean z = ((f) MoatAnalytics.getInstance()).f63;
                                if (t.this.f186 != gVar.m56() || (t.this.f186 == a.f198 && z)) {
                                    t.this.f186 = gVar.m56();
                                    if (t.this.f186 == a.f198 && z) {
                                        t.this.f186 = a.f197;
                                    }
                                    if (t.this.f186 == a.f197) {
                                        a.m8(3, "OnOff", this, "Moat enabled - Version 2.4.1");
                                    }
                                    for (e eVar : t.f180) {
                                        if (t.this.f186 == a.f197) {
                                            eVar.f207.m184();
                                        }
                                    }
                                }
                                while (!t.f180.isEmpty()) {
                                    t.f180.remove();
                                }
                            }
                        }
                    }), j);
                    Looper.loop();
                }
            }.start();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m181(b bVar) throws o {
        if (this.f186 == a.f197) {
            bVar.m184();
            return;
        }
        m170();
        f180.add(new e(Long.valueOf(System.currentTimeMillis()), bVar));
        if (this.f185.compareAndSet(false, true)) {
            this.f192.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (t.f180.size() > 0) {
                            t.m170();
                            t.this.f192.postDelayed(this, 60000);
                            return;
                        }
                        t.this.f185.compareAndSet(true, false);
                        t.this.f192.removeCallbacks(this);
                    } catch (Exception e) {
                        o.m132(e);
                    }
                }
            }, 60000);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public static void m170() {
        synchronized (f180) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator<e> it = f180.iterator();
            while (it.hasNext()) {
                if (currentTimeMillis - it.next().f205.longValue() >= 60000) {
                    it.remove();
                }
            }
            if (f180.size() >= 15) {
                for (int i = 0; i < 5; i++) {
                    f180.remove();
                }
            }
        }
    }

    class d implements Runnable {

        /* renamed from: ˎ  reason: contains not printable characters */
        private final String f200;
        /* access modifiers changed from: private */

        /* renamed from: ˏ  reason: contains not printable characters */
        public final AnonymousClass2.AnonymousClass2 f201;

        /* renamed from: ॱ  reason: contains not printable characters */
        private final Handler f202;

        private d(String str, Handler handler, AnonymousClass2.AnonymousClass2 r4) {
            this.f201 = r4;
            this.f202 = handler;
            this.f200 = "https://z.moatads.com/" + str + "/android/" + BuildConfig.REVISION.substring(0, 7) + "/status.json";
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        private String m186() {
            try {
                return m.m110(this.f200 + "?ts=" + System.currentTimeMillis() + "&v=2.4.1").get();
            } catch (Exception unused) {
                return null;
            }
        }

        public final void run() {
            try {
                String r0 = m186();
                final g gVar = new g(r0);
                t.this.f187 = gVar.m55();
                t.this.f184 = gVar.m59();
                t.this.f182 = gVar.m58();
                t.this.f189 = gVar.m57();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            d.this.f201.m185(gVar);
                        } catch (Exception e) {
                            o.m132(e);
                        }
                    }
                });
                long unused = t.this.f191 = System.currentTimeMillis();
                t.this.f188.compareAndSet(true, false);
                if (r0 != null) {
                    t.this.f190.set(0);
                } else if (t.this.f190.incrementAndGet() < 10) {
                    t.this.m177(t.this.f181);
                }
            } catch (Exception e) {
                o.m132(e);
            }
            this.f202.removeCallbacks(this);
            Looper myLooper = Looper.myLooper();
            if (myLooper != null) {
                myLooper.quit();
            }
        }
    }

    class e {

        /* renamed from: ˎ  reason: contains not printable characters */
        final Long f205;

        /* renamed from: ॱ  reason: contains not printable characters */
        final b f207;

        e(Long l, b bVar) {
            this.f205 = l;
            this.f207 = bVar;
        }
    }
}
