package com.moat.analytics.mobile.aol;

interface l<T> {
    T create() throws o;

    T createNoOp();
}
