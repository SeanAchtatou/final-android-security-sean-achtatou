package com.moat.analytics.mobile.aol;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import com.moat.analytics.mobile.aol.t;
import java.lang.ref.WeakReference;

final class c {
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public static boolean f40 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Application f41 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static boolean f42 = false;

    /* renamed from: ˏ  reason: contains not printable characters */
    static WeakReference<Activity> f43;
    /* access modifiers changed from: private */

    /* renamed from: ॱ  reason: contains not printable characters */
    public static int f44;

    c() {
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static void m31(Application application) {
        f41 = application;
        if (!f42) {
            f42 = true;
            f41.registerActivityLifecycleCallbacks(new a());
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static Application m29() {
        return f41;
    }

    static class a implements Application.ActivityLifecycleCallbacks {
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        a() {
        }

        public final void onActivityCreated(Activity activity, Bundle bundle) {
            int unused = c.f44 = 1;
        }

        public final void onActivityStarted(Activity activity) {
            try {
                c.f43 = new WeakReference<>(activity);
                int unused = c.f44 = 2;
                if (!c.f40) {
                    m33(true);
                }
                boolean unused2 = c.f40 = true;
                a.m8(3, "ActivityState", this, "Activity started: " + activity.getClass() + "@" + activity.hashCode());
            } catch (Exception e) {
                o.m132(e);
            }
        }

        public final void onActivityResumed(Activity activity) {
            try {
                c.f43 = new WeakReference<>(activity);
                int unused = c.f44 = 3;
                t.m176().m182();
                a.m8(3, "ActivityState", this, "Activity resumed: " + activity.getClass() + "@" + activity.hashCode());
                if (((f) MoatAnalytics.getInstance()).f64) {
                    e.m45(activity);
                }
            } catch (Exception e) {
                o.m132(e);
            }
        }

        public final void onActivityPaused(Activity activity) {
            try {
                int unused = c.f44 = 4;
                if (c.m27(activity)) {
                    c.f43 = new WeakReference<>(null);
                }
                a.m8(3, "ActivityState", this, "Activity paused: " + activity.getClass() + "@" + activity.hashCode());
            } catch (Exception e) {
                o.m132(e);
            }
        }

        public final void onActivityStopped(Activity activity) {
            try {
                if (c.f44 != 3) {
                    boolean unused = c.f40 = false;
                    m33(false);
                }
                int unused2 = c.f44 = 5;
                if (c.m27(activity)) {
                    c.f43 = new WeakReference<>(null);
                }
                a.m8(3, "ActivityState", this, "Activity stopped: " + activity.getClass() + "@" + activity.hashCode());
            } catch (Exception e) {
                o.m132(e);
            }
        }

        public final void onActivityDestroyed(Activity activity) {
            try {
                if (!(c.f44 == 3 || c.f44 == 5)) {
                    if (c.f40) {
                        m33(false);
                    }
                    boolean unused = c.f40 = false;
                }
                int unused2 = c.f44 = 6;
                a.m8(3, "ActivityState", this, "Activity destroyed: " + activity.getClass() + "@" + activity.hashCode());
                if (c.m27(activity)) {
                    c.f43 = new WeakReference<>(null);
                }
            } catch (Exception e) {
                o.m132(e);
            }
        }

        /* renamed from: ॱ  reason: contains not printable characters */
        private static void m33(boolean z) {
            if (z) {
                a.m8(3, "ActivityState", null, "App became visible");
                if (t.m176().f186 == t.a.f197 && !((f) MoatAnalytics.getInstance()).f62) {
                    n.m118().m128();
                    return;
                }
                return;
            }
            a.m8(3, "ActivityState", null, "App became invisible");
            if (t.m176().f186 == t.a.f197 && !((f) MoatAnalytics.getInstance()).f62) {
                n.m118().m129();
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static /* synthetic */ boolean m27(Activity activity) {
        return f43 != null && f43.get() == activity;
    }
}
