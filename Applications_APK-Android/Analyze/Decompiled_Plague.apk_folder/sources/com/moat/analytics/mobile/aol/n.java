package com.moat.analytics.mobile.aol;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import com.mopub.mobileads.GooglePlayServicesInterstitial;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class n implements LocationListener {

    /* renamed from: ˎ  reason: contains not printable characters */
    private static n f135;

    /* renamed from: ʻ  reason: contains not printable characters */
    private Location f136;

    /* renamed from: ˊ  reason: contains not printable characters */
    private ScheduledExecutorService f137;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private boolean f138;

    /* renamed from: ˋ  reason: contains not printable characters */
    private ScheduledFuture<?> f139;

    /* renamed from: ˏ  reason: contains not printable characters */
    private ScheduledFuture<?> f140;

    /* renamed from: ॱ  reason: contains not printable characters */
    private LocationManager f141;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private boolean f142;

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static n m118() {
        if (f135 == null) {
            f135 = new n();
        }
        return f135;
    }

    private n() {
        try {
            this.f138 = ((f) MoatAnalytics.getInstance()).f62;
            if (this.f138) {
                a.m8(3, "LocationManager", this, "Moat location services disabled");
                return;
            }
            this.f137 = Executors.newScheduledThreadPool(1);
            this.f141 = (LocationManager) c.m29().getSystemService(GooglePlayServicesInterstitial.LOCATION_KEY);
            if (this.f141.getAllProviders().size() == 0) {
                a.m8(3, "LocationManager", this, "Device has no location providers");
            } else {
                m116();
            }
        } catch (Exception e) {
            o.m132(e);
        }
    }

    /* access modifiers changed from: package-private */
    @Nullable
    /* renamed from: ˊ  reason: contains not printable characters */
    public final Location m127() {
        if (this.f138 || this.f141 == null) {
            return null;
        }
        return this.f136;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m128() {
        m116();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m129() {
        m123(false);
    }

    public final void onLocationChanged(Location location) {
        try {
            a.m8(3, "LocationManager", this, "Received an updated location = " + location.toString());
            float currentTimeMillis = (float) ((System.currentTimeMillis() - location.getTime()) / 1000);
            if (location.hasAccuracy() && location.getAccuracy() <= 100.0f && currentTimeMillis < 600.0f) {
                this.f136 = m121(this.f136, location);
                a.m8(3, "LocationManager", this, "fetchCompleted");
                m123(true);
            }
        } catch (Exception e) {
            o.m132(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m116() {
        try {
            if (this.f138) {
                return;
            }
            if (this.f141 != null) {
                if (this.f142) {
                    a.m8(3, "LocationManager", this, "already updating location");
                }
                a.m8(3, "LocationManager", this, "starting location fetch");
                this.f136 = m121(this.f136, m113());
                if (this.f136 != null) {
                    a.m8(3, "LocationManager", this, "Have a valid location, won't fetch = " + this.f136.toString());
                    m119();
                    return;
                }
                m111();
            }
        } catch (Exception e) {
            o.m132(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public void m123(boolean z) {
        try {
            a.m8(3, "LocationManager", this, "stopping location fetch");
            m112();
            m115();
            if (z) {
                m119();
            } else {
                m126();
            }
        } catch (Exception e) {
            o.m132(e);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private Location m113() {
        Location location;
        try {
            boolean r1 = m120();
            boolean r2 = m125();
            if (r1 && r2) {
                location = m121(this.f141.getLastKnownLocation("gps"), this.f141.getLastKnownLocation("network"));
            } else if (r1) {
                location = this.f141.getLastKnownLocation("gps");
            } else if (!r2) {
                return null;
            } else {
                location = this.f141.getLastKnownLocation("network");
            }
            return location;
        } catch (SecurityException e) {
            o.m132(e);
            return null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m111() {
        try {
            if (!this.f142) {
                a.m8(3, "LocationManager", this, "Attempting to start update");
                if (m120()) {
                    a.m8(3, "LocationManager", this, "start updating gps location");
                    this.f141.requestLocationUpdates("gps", 0, 0.0f, this, Looper.getMainLooper());
                    this.f142 = true;
                }
                if (m125()) {
                    a.m8(3, "LocationManager", this, "start updating network location");
                    this.f141.requestLocationUpdates("network", 0, 0.0f, this, Looper.getMainLooper());
                    this.f142 = true;
                }
                if (this.f142) {
                    m115();
                    this.f140 = this.f137.schedule(new Runnable() {
                        public final void run() {
                            try {
                                a.m8(3, "LocationManager", this, "fetchTimedOut");
                                n.this.m123(true);
                            } catch (Exception e) {
                                o.m132(e);
                            }
                        }
                    }, 60, TimeUnit.SECONDS);
                }
            }
        } catch (SecurityException e) {
            o.m132(e);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m112() {
        try {
            a.m8(3, "LocationManager", this, "Stopping to update location");
            boolean z = true;
            if (!(ContextCompat.checkSelfPermission(c.m29().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
                if (!(ContextCompat.checkSelfPermission(c.m29().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                    z = false;
                }
            }
            if (z && this.f141 != null) {
                this.f141.removeUpdates(this);
                this.f142 = false;
            }
        } catch (SecurityException e) {
            o.m132(e);
        }
    }

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private void m115() {
        if (this.f140 != null && !this.f140.isCancelled()) {
            this.f140.cancel(true);
            this.f140 = null;
        }
    }

    /* renamed from: ᐝ  reason: contains not printable characters */
    private void m126() {
        if (this.f139 != null && !this.f139.isCancelled()) {
            this.f139.cancel(true);
            this.f139 = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* renamed from: ˏॱ  reason: contains not printable characters */
    private void m119() {
        a.m8(3, "LocationManager", this, "Resetting fetch timer");
        m126();
        float f = 600.0f;
        if (this.f136 != null) {
            f = Math.max(600.0f - ((float) ((System.currentTimeMillis() - this.f136.getTime()) / 1000)), 0.0f);
        }
        this.f139 = this.f137.schedule(new Runnable() {
            public final void run() {
                try {
                    a.m8(3, "LocationManager", this, "fetchTimerCompleted");
                    n.this.m116();
                } catch (Exception e) {
                    o.m132(e);
                }
            }
        }, (long) f, TimeUnit.SECONDS);
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private static Location m121(Location location, Location location2) {
        boolean r0 = m124(location);
        boolean r1 = m124(location2);
        if (r0) {
            return (r1 && location.getAccuracy() >= location.getAccuracy()) ? location2 : location;
        }
        if (!r1) {
            return null;
        }
        return location2;
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private static boolean m124(Location location) {
        if (location == null) {
            return false;
        }
        if ((location.getLatitude() != 0.0d || location.getLongitude() != 0.0d) && location.getAccuracy() >= 0.0f && ((float) ((System.currentTimeMillis() - location.getTime()) / 1000)) < 600.0f) {
            return true;
        }
        return false;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static boolean m117(Location location, Location location2) {
        if (location == location2) {
            return true;
        }
        return (location == null || location2 == null || location.getTime() != location2.getTime()) ? false : true;
    }

    /* renamed from: ͺ  reason: contains not printable characters */
    private boolean m120() {
        return (ContextCompat.checkSelfPermission(c.m29().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) && this.f141.getProvider("gps") != null && this.f141.isProviderEnabled("gps");
    }

    /* renamed from: ॱˊ  reason: contains not printable characters */
    private boolean m125() {
        boolean z;
        if (!(ContextCompat.checkSelfPermission(c.m29().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
            if (!(ContextCompat.checkSelfPermission(c.m29().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                z = false;
                return !z && this.f141.getProvider("network") != null && this.f141.isProviderEnabled("network");
            }
        }
        z = true;
        if (!z) {
        }
    }
}
