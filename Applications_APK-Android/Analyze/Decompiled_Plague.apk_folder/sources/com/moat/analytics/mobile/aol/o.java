package com.moat.analytics.mobile.aol;

import android.util.Log;

final class o extends Exception {

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Exception f145;

    /* renamed from: ˏ  reason: contains not printable characters */
    private static final Long f146 = 60000L;

    /* renamed from: ॱ  reason: contains not printable characters */
    private static Long f147;

    o(String str) {
        super(str);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static String m131(String str, Exception exc) {
        if (exc instanceof o) {
            return str + " failed: " + exc.getMessage();
        }
        return str + " failed unexpectedly";
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static void m132(Exception exc) {
        if (t.m176().f187) {
            Log.e("MoatException", Log.getStackTraceString(exc));
        } else {
            m130(exc);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0178 A[Catch:{ Exception -> 0x01a0 }] */
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m130(java.lang.Exception r12) {
        /*
            com.moat.analytics.mobile.aol.t r0 = com.moat.analytics.mobile.aol.t.m176()     // Catch:{ Exception -> 0x01a0 }
            int r0 = r0.f186     // Catch:{ Exception -> 0x01a0 }
            int r1 = com.moat.analytics.mobile.aol.t.a.f197     // Catch:{ Exception -> 0x01a0 }
            if (r0 != r1) goto L_0x019d
            com.moat.analytics.mobile.aol.t r0 = com.moat.analytics.mobile.aol.t.m176()     // Catch:{ Exception -> 0x01a0 }
            int r0 = r0.f189     // Catch:{ Exception -> 0x01a0 }
            if (r0 != 0) goto L_0x0013
            return
        L_0x0013:
            r1 = 100
            if (r0 >= r1) goto L_0x0024
            double r1 = (double) r0     // Catch:{ Exception -> 0x01a0 }
            r3 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r1 = r1 / r3
            double r3 = java.lang.Math.random()     // Catch:{ Exception -> 0x01a0 }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 >= 0) goto L_0x0024
            return
        L_0x0024:
            java.lang.String r1 = ""
            java.lang.String r2 = ""
            java.lang.String r3 = ""
            java.lang.String r4 = ""
            java.lang.String r5 = "https://px.moatads.com/pixel.gif?e=0&i=MOATSDK1&ac=1"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a0 }
            r6.<init>(r5)     // Catch:{ Exception -> 0x01a0 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r7 = "&zt="
            r5.<init>(r7)     // Catch:{ Exception -> 0x01a0 }
            boolean r7 = r12 instanceof com.moat.analytics.mobile.aol.o     // Catch:{ Exception -> 0x01a0 }
            r8 = 1
            r9 = 0
            if (r7 == 0) goto L_0x0042
            r7 = r8
            goto L_0x0043
        L_0x0042:
            r7 = r9
        L_0x0043:
            r5.append(r7)     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01a0 }
            r6.append(r5)     // Catch:{ Exception -> 0x01a0 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r7 = "&zr="
            r5.<init>(r7)     // Catch:{ Exception -> 0x01a0 }
            r5.append(r0)     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r0 = r5.toString()     // Catch:{ Exception -> 0x01a0 }
            r6.append(r0)     // Catch:{ Exception -> 0x01a0 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r5 = "&zm="
            r0.<init>(r5)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r5 = r12.getMessage()     // Catch:{ Exception -> 0x00b1 }
            if (r5 != 0) goto L_0x006e
            java.lang.String r5 = "null"
            goto L_0x0082
        L_0x006e:
            java.lang.String r5 = r12.getMessage()     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r7 = "UTF-8"
            byte[] r5 = r5.getBytes(r7)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r5 = android.util.Base64.encodeToString(r5, r9)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r7 = "UTF-8"
            java.lang.String r5 = java.net.URLEncoder.encode(r5, r7)     // Catch:{ Exception -> 0x00b1 }
        L_0x0082:
            r0.append(r5)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00b1 }
            r6.append(r0)     // Catch:{ Exception -> 0x00b1 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r5 = "&k="
            r0.<init>(r5)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r12 = android.util.Log.getStackTraceString(r12)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r5 = "UTF-8"
            byte[] r12 = r12.getBytes(r5)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r12 = android.util.Base64.encodeToString(r12, r9)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r5 = "UTF-8"
            java.lang.String r12 = java.net.URLEncoder.encode(r12, r5)     // Catch:{ Exception -> 0x00b1 }
            r0.append(r12)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r12 = r0.toString()     // Catch:{ Exception -> 0x00b1 }
            r6.append(r12)     // Catch:{ Exception -> 0x00b1 }
        L_0x00b1:
            java.lang.String r12 = "AOL"
            java.lang.String r0 = "&zMoatMMAKv=35d482907bc2811c2e46b96f16eb5f9fe00185f3"
            r6.append(r0)     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r0 = "2.4.1"
            com.moat.analytics.mobile.aol.r$e r1 = com.moat.analytics.mobile.aol.r.m149()     // Catch:{ Exception -> 0x00de }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00de }
            java.lang.String r5 = "&zMoatMMAKan="
            r3.<init>(r5)     // Catch:{ Exception -> 0x00de }
            java.lang.String r5 = r1.m160()     // Catch:{ Exception -> 0x00de }
            r3.append(r5)     // Catch:{ Exception -> 0x00de }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00de }
            r6.append(r3)     // Catch:{ Exception -> 0x00de }
            java.lang.String r1 = r1.m159()     // Catch:{ Exception -> 0x00de }
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00e3 }
            goto L_0x00e4
        L_0x00de:
            r1 = r2
            goto L_0x00e3
        L_0x00e0:
            r12 = r1
        L_0x00e1:
            r1 = r2
            r0 = r3
        L_0x00e3:
            r2 = r4
        L_0x00e4:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r4 = "&d=Android:"
            r3.<init>(r4)     // Catch:{ Exception -> 0x01a0 }
            r3.append(r12)     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r12 = ":"
            r3.append(r12)     // Catch:{ Exception -> 0x01a0 }
            r3.append(r1)     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r12 = ":-"
            r3.append(r12)     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r12 = r3.toString()     // Catch:{ Exception -> 0x01a0 }
            r6.append(r12)     // Catch:{ Exception -> 0x01a0 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r1 = "&bo="
            r12.<init>(r1)     // Catch:{ Exception -> 0x01a0 }
            r12.append(r0)     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x01a0 }
            r6.append(r12)     // Catch:{ Exception -> 0x01a0 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r0 = "&bd="
            r12.<init>(r0)     // Catch:{ Exception -> 0x01a0 }
            r12.append(r2)     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x01a0 }
            r6.append(r12)     // Catch:{ Exception -> 0x01a0 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01a0 }
            java.lang.Long r12 = java.lang.Long.valueOf(r0)     // Catch:{ Exception -> 0x01a0 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r1 = "&t="
            r0.<init>(r1)     // Catch:{ Exception -> 0x01a0 }
            r0.append(r12)     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01a0 }
            r6.append(r0)     // Catch:{ Exception -> 0x01a0 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r1 = "&de="
            r0.<init>(r1)     // Catch:{ Exception -> 0x01a0 }
            java.util.Locale r1 = java.util.Locale.ROOT     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r2 = "%.0f"
            java.lang.Object[] r3 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x01a0 }
            double r4 = java.lang.Math.random()     // Catch:{ Exception -> 0x01a0 }
            r7 = 4621819117588971520(0x4024000000000000, double:10.0)
            r10 = 4622945017495814144(0x4028000000000000, double:12.0)
            double r7 = java.lang.Math.pow(r7, r10)     // Catch:{ Exception -> 0x01a0 }
            double r4 = r4 * r7
            double r4 = java.lang.Math.floor(r4)     // Catch:{ Exception -> 0x01a0 }
            java.lang.Double r4 = java.lang.Double.valueOf(r4)     // Catch:{ Exception -> 0x01a0 }
            r3[r9] = r4     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r1 = java.lang.String.format(r1, r2, r3)     // Catch:{ Exception -> 0x01a0 }
            r0.append(r1)     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01a0 }
            r6.append(r0)     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r0 = "&cs=0"
            r6.append(r0)     // Catch:{ Exception -> 0x01a0 }
            java.lang.Long r0 = com.moat.analytics.mobile.aol.o.f147     // Catch:{ Exception -> 0x01a0 }
            if (r0 == 0) goto L_0x018e
            long r0 = r12.longValue()     // Catch:{ Exception -> 0x01a0 }
            java.lang.Long r2 = com.moat.analytics.mobile.aol.o.f147     // Catch:{ Exception -> 0x01a0 }
            long r2 = r2.longValue()     // Catch:{ Exception -> 0x01a0 }
            long r4 = r0 - r2
            java.lang.Long r0 = com.moat.analytics.mobile.aol.o.f146     // Catch:{ Exception -> 0x01a0 }
            long r0 = r0.longValue()     // Catch:{ Exception -> 0x01a0 }
            int r2 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x019c
        L_0x018e:
            java.lang.String r0 = r6.toString()     // Catch:{ Exception -> 0x01a0 }
            com.moat.analytics.mobile.aol.m$2 r1 = new com.moat.analytics.mobile.aol.m$2     // Catch:{ Exception -> 0x01a0 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x01a0 }
            r1.start()     // Catch:{ Exception -> 0x01a0 }
            com.moat.analytics.mobile.aol.o.f147 = r12     // Catch:{ Exception -> 0x01a0 }
        L_0x019c:
            return
        L_0x019d:
            com.moat.analytics.mobile.aol.o.f145 = r12     // Catch:{ Exception -> 0x01a0 }
            return
        L_0x01a0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.aol.o.m130(java.lang.Exception):void");
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static void m133() {
        if (f145 != null) {
            m130(f145);
            f145 = null;
        }
    }
}
