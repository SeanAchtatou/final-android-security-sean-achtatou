package com.moat.analytics.mobile.aol;

import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import com.moat.analytics.mobile.aol.a;
import com.moat.analytics.mobile.aol.t;
import java.lang.ref.WeakReference;

final class f extends MoatAnalytics implements t.b {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f58 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f59;

    /* renamed from: ʽ  reason: contains not printable characters */
    private MoatOptions f60;

    /* renamed from: ˊ  reason: contains not printable characters */
    WeakReference<Context> f61;

    /* renamed from: ˋ  reason: contains not printable characters */
    boolean f62 = false;

    /* renamed from: ˎ  reason: contains not printable characters */
    boolean f63 = false;

    /* renamed from: ˏ  reason: contains not printable characters */
    boolean f64 = false;
    @Nullable

    /* renamed from: ॱ  reason: contains not printable characters */
    a f65;

    f() {
    }

    public final void start(Application application) {
        start(new MoatOptions(), application);
    }

    @UiThread
    public final void prepareNativeDisplayTracking(String str) {
        this.f59 = str;
        if (t.m176().f186 != t.a.f198) {
            try {
                m46();
            } catch (Exception e) {
                o.m132(e);
            }
        }
    }

    @UiThread
    /* renamed from: ˏ  reason: contains not printable characters */
    private void m46() {
        if (this.f65 == null) {
            this.f65 = new a(c.m29(), a.d.f25);
            this.f65.m12(this.f59);
            a.m8(3, "Analytics", this, "Preparing native display tracking with partner code " + this.f59);
            a.m5("[SUCCESS] ", "Prepared for native display tracking with partner code " + this.f59);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m47() {
        return this.f58;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final boolean m48() {
        return this.f60 != null && this.f60.disableLocationServices;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m49() throws o {
        o.m133();
        n.m118();
        if (this.f59 != null) {
            try {
                m46();
            } catch (Exception e) {
                o.m132(e);
            }
        }
    }

    public final void start(MoatOptions moatOptions, Application application) {
        try {
            if (this.f58) {
                a.m8(3, "Analytics", this, "Moat SDK has already been started.");
                return;
            }
            this.f60 = moatOptions;
            t.m176().m182();
            this.f62 = moatOptions.disableLocationServices;
            if (application == null) {
                throw new o("Moat Analytics SDK didn't start, application was null");
            }
            if (moatOptions.loggingEnabled && r.m151(application.getApplicationContext())) {
                this.f63 = true;
            }
            this.f61 = new WeakReference<>(application.getApplicationContext());
            this.f58 = true;
            this.f64 = moatOptions.autoTrackGMAInterstitials;
            c.m31(application);
            t.m176().m181(this);
            if (!moatOptions.disableAdIdCollection) {
                r.m154(application);
            }
            a.m5("[SUCCESS] ", "Moat Analytics SDK Version 2.4.1 started");
        } catch (Exception e) {
            o.m132(e);
        }
    }
}
