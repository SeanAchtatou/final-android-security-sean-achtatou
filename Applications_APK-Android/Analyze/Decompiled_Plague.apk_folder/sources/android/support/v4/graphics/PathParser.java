package android.support.v4.graphics;

import android.graphics.Path;
import android.support.annotation.RestrictTo;
import android.util.Log;
import com.google.ads.AdSize;
import com.millennialmedia.internal.AdPlacementReporter;
import com.miniclip.input.MCInput;
import com.mopub.mobileads.resource.DrawableConstants;
import java.util.ArrayList;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class PathParser {
    private static final String LOGTAG = "PathParser";

    static float[] copyOfRange(float[] fArr, int i, int i2) {
        if (i > i2) {
            throw new IllegalArgumentException();
        }
        int length = fArr.length;
        if (i < 0 || i > length) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int i3 = i2 - i;
        int min = Math.min(i3, length - i);
        float[] fArr2 = new float[i3];
        System.arraycopy(fArr, i, fArr2, 0, min);
        return fArr2;
    }

    public static Path createPathFromPathData(String str) {
        Path path = new Path();
        PathDataNode[] createNodesFromPathData = createNodesFromPathData(str);
        if (createNodesFromPathData == null) {
            return null;
        }
        try {
            PathDataNode.nodesToPath(createNodesFromPathData, path);
            return path;
        } catch (RuntimeException e) {
            throw new RuntimeException("Error in parsing " + str, e);
        }
    }

    public static PathDataNode[] createNodesFromPathData(String str) {
        if (str == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int i = 1;
        int i2 = 0;
        while (i < str.length()) {
            int nextStart = nextStart(str, i);
            String trim = str.substring(i2, nextStart).trim();
            if (trim.length() > 0) {
                addNode(arrayList, trim.charAt(0), getFloats(trim));
            }
            i2 = nextStart;
            i = nextStart + 1;
        }
        if (i - i2 == 1 && i2 < str.length()) {
            addNode(arrayList, str.charAt(i2), new float[0]);
        }
        return (PathDataNode[]) arrayList.toArray(new PathDataNode[arrayList.size()]);
    }

    public static PathDataNode[] deepCopyNodes(PathDataNode[] pathDataNodeArr) {
        if (pathDataNodeArr == null) {
            return null;
        }
        PathDataNode[] pathDataNodeArr2 = new PathDataNode[pathDataNodeArr.length];
        for (int i = 0; i < pathDataNodeArr.length; i++) {
            pathDataNodeArr2[i] = new PathDataNode(pathDataNodeArr[i]);
        }
        return pathDataNodeArr2;
    }

    public static boolean canMorph(PathDataNode[] pathDataNodeArr, PathDataNode[] pathDataNodeArr2) {
        if (pathDataNodeArr == null || pathDataNodeArr2 == null || pathDataNodeArr.length != pathDataNodeArr2.length) {
            return false;
        }
        for (int i = 0; i < pathDataNodeArr.length; i++) {
            if (pathDataNodeArr[i].mType != pathDataNodeArr2[i].mType || pathDataNodeArr[i].mParams.length != pathDataNodeArr2[i].mParams.length) {
                return false;
            }
        }
        return true;
    }

    public static void updateNodes(PathDataNode[] pathDataNodeArr, PathDataNode[] pathDataNodeArr2) {
        for (int i = 0; i < pathDataNodeArr2.length; i++) {
            pathDataNodeArr[i].mType = pathDataNodeArr2[i].mType;
            for (int i2 = 0; i2 < pathDataNodeArr2[i].mParams.length; i2++) {
                pathDataNodeArr[i].mParams[i2] = pathDataNodeArr2[i].mParams[i2];
            }
        }
    }

    private static int nextStart(String str, int i) {
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (((charAt - 'A') * (charAt - 'Z') <= 0 || (charAt - 'a') * (charAt - 'z') <= 0) && charAt != 'e' && charAt != 'E') {
                return i;
            }
            i++;
        }
        return i;
    }

    private static void addNode(ArrayList<PathDataNode> arrayList, char c, float[] fArr) {
        arrayList.add(new PathDataNode(c, fArr));
    }

    private static class ExtractFloatResult {
        int mEndPosition;
        boolean mEndWithNegOrDot;

        ExtractFloatResult() {
        }
    }

    private static float[] getFloats(String str) {
        if (str.charAt(0) == 'z' || str.charAt(0) == 'Z') {
            return new float[0];
        }
        try {
            float[] fArr = new float[str.length()];
            ExtractFloatResult extractFloatResult = new ExtractFloatResult();
            int length = str.length();
            int i = 1;
            int i2 = 0;
            while (i < length) {
                extract(str, i, extractFloatResult);
                int i3 = extractFloatResult.mEndPosition;
                if (i < i3) {
                    fArr[i2] = Float.parseFloat(str.substring(i, i3));
                    i2++;
                }
                i = extractFloatResult.mEndWithNegOrDot ? i3 : i3 + 1;
            }
            return copyOfRange(fArr, 0, i2);
        } catch (NumberFormatException e) {
            throw new RuntimeException("error in parsing \"" + str + "\"", e);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003a A[LOOP:0: B:1:0x0007->B:20:0x003a, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x003d A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void extract(java.lang.String r8, int r9, android.support.v4.graphics.PathParser.ExtractFloatResult r10) {
        /*
            r0 = 0
            r10.mEndWithNegOrDot = r0
            r1 = r9
            r2 = r0
            r3 = r2
            r4 = r3
        L_0x0007:
            int r5 = r8.length()
            if (r1 >= r5) goto L_0x003d
            char r5 = r8.charAt(r1)
            r6 = 32
            r7 = 1
            if (r5 == r6) goto L_0x0035
            r6 = 69
            if (r5 == r6) goto L_0x0033
            r6 = 101(0x65, float:1.42E-43)
            if (r5 == r6) goto L_0x0033
            switch(r5) {
                case 44: goto L_0x0035;
                case 45: goto L_0x002a;
                case 46: goto L_0x0022;
                default: goto L_0x0021;
            }
        L_0x0021:
            goto L_0x0031
        L_0x0022:
            if (r3 != 0) goto L_0x0027
            r2 = r0
            r3 = r7
            goto L_0x0037
        L_0x0027:
            r10.mEndWithNegOrDot = r7
            goto L_0x0035
        L_0x002a:
            if (r1 == r9) goto L_0x0031
            if (r2 != 0) goto L_0x0031
            r10.mEndWithNegOrDot = r7
            goto L_0x0035
        L_0x0031:
            r2 = r0
            goto L_0x0037
        L_0x0033:
            r2 = r7
            goto L_0x0037
        L_0x0035:
            r2 = r0
            r4 = r7
        L_0x0037:
            if (r4 == 0) goto L_0x003a
            goto L_0x003d
        L_0x003a:
            int r1 = r1 + 1
            goto L_0x0007
        L_0x003d:
            r10.mEndPosition = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.graphics.PathParser.extract(java.lang.String, int, android.support.v4.graphics.PathParser$ExtractFloatResult):void");
    }

    public static class PathDataNode {
        @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
        public float[] mParams;
        @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
        public char mType;

        PathDataNode(char c, float[] fArr) {
            this.mType = c;
            this.mParams = fArr;
        }

        PathDataNode(PathDataNode pathDataNode) {
            this.mType = pathDataNode.mType;
            this.mParams = PathParser.copyOfRange(pathDataNode.mParams, 0, pathDataNode.mParams.length);
        }

        public static void nodesToPath(PathDataNode[] pathDataNodeArr, Path path) {
            float[] fArr = new float[6];
            char c = 'm';
            for (int i = 0; i < pathDataNodeArr.length; i++) {
                addCommand(path, fArr, c, pathDataNodeArr[i].mType, pathDataNodeArr[i].mParams);
                c = pathDataNodeArr[i].mType;
            }
        }

        public void interpolatePathDataNode(PathDataNode pathDataNode, PathDataNode pathDataNode2, float f) {
            for (int i = 0; i < pathDataNode.mParams.length; i++) {
                this.mParams[i] = (pathDataNode.mParams[i] * (1.0f - f)) + (pathDataNode2.mParams[i] * f);
            }
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        private static void addCommand(Path path, float[] fArr, char c, char c2, float[] fArr2) {
            int i;
            int i2;
            int i3;
            float f;
            float f2;
            float f3;
            float f4;
            float f5;
            float f6;
            float f7;
            float f8;
            float f9;
            float f10;
            float f11;
            float f12;
            int i4;
            Path path2 = path;
            float[] fArr3 = fArr2;
            boolean z = false;
            float f13 = fArr[0];
            float f14 = fArr[1];
            float f15 = fArr[2];
            float f16 = fArr[3];
            float f17 = fArr[4];
            float f18 = fArr[5];
            switch (c2) {
                case 'A':
                case MCInput.KEYCODE_BUTTON_B /*97*/:
                    i4 = 7;
                    i = i4;
                    break;
                case 'C':
                case MCInput.KEYCODE_BUTTON_X /*99*/:
                    i4 = 6;
                    i = i4;
                    break;
                case DrawableConstants.GradientStrip.GRADIENT_STRIP_HEIGHT_DIPS /*72*/:
                case 'V':
                case MCInput.KEYCODE_BUTTON_L2 /*104*/:
                case 'v':
                    i = 1;
                    break;
                case 'L':
                case 'M':
                case 'T':
                case MCInput.KEYCODE_BUTTON_START /*108*/:
                case MCInput.KEYCODE_BUTTON_SELECT /*109*/:
                case 't':
                default:
                    i = 2;
                    break;
                case 'Q':
                case 'S':
                case AdPlacementReporter.PLAYLIST_TIMED_OUT_IN_CACHE /*113*/:
                case 's':
                    i = 4;
                    break;
                case AdSize.LARGE_AD_HEIGHT /*90*/:
                case 'z':
                    path.close();
                    path2.moveTo(f17, f18);
                    f13 = f17;
                    f15 = f13;
                    f14 = f18;
                    f16 = f14;
                    i = 2;
                    break;
            }
            float f19 = f13;
            float f20 = f14;
            float f21 = f17;
            float f22 = f18;
            int i5 = 0;
            char c3 = c;
            while (i5 < fArr3.length) {
                float f23 = 0.0f;
                switch (c2) {
                    case 'A':
                        i3 = i5;
                        int i6 = i3 + 5;
                        int i7 = i3 + 6;
                        drawArc(path2, f19, f20, fArr3[i6], fArr3[i7], fArr3[i3 + 0], fArr3[i3 + 1], fArr3[i3 + 2], fArr3[i3 + 3] != 0.0f, fArr3[i3 + 4] != 0.0f);
                        f = fArr3[i6];
                        f2 = fArr3[i7];
                        f16 = f20;
                        f15 = f19;
                        break;
                    case 'C':
                        i2 = i5;
                        int i8 = i2 + 2;
                        int i9 = i2 + 3;
                        int i10 = i2 + 4;
                        int i11 = i2 + 5;
                        path2.cubicTo(fArr3[i2 + 0], fArr3[i2 + 1], fArr3[i8], fArr3[i9], fArr3[i10], fArr3[i11]);
                        f19 = fArr3[i10];
                        float f24 = fArr3[i11];
                        float f25 = fArr3[i8];
                        float f26 = fArr3[i9];
                        f20 = f24;
                        f16 = f26;
                        f15 = f25;
                        break;
                    case DrawableConstants.GradientStrip.GRADIENT_STRIP_HEIGHT_DIPS /*72*/:
                        i2 = i5;
                        int i12 = i2 + 0;
                        path2.lineTo(fArr3[i12], f20);
                        f19 = fArr3[i12];
                        break;
                    case 'L':
                        i2 = i5;
                        int i13 = i2 + 0;
                        int i14 = i2 + 1;
                        path2.lineTo(fArr3[i13], fArr3[i14]);
                        f19 = fArr3[i13];
                        f20 = fArr3[i14];
                        break;
                    case 'M':
                        i2 = i5;
                        int i15 = i2 + 0;
                        f19 = fArr3[i15];
                        int i16 = i2 + 1;
                        f20 = fArr3[i16];
                        if (i2 <= 0) {
                            path2.moveTo(fArr3[i15], fArr3[i16]);
                            f22 = f20;
                            f21 = f19;
                            break;
                        } else {
                            path2.lineTo(fArr3[i15], fArr3[i16]);
                            break;
                        }
                    case 'Q':
                        i2 = i5;
                        int i17 = i2 + 0;
                        int i18 = i2 + 1;
                        int i19 = i2 + 2;
                        int i20 = i2 + 3;
                        path2.quadTo(fArr3[i17], fArr3[i18], fArr3[i19], fArr3[i20]);
                        f4 = fArr3[i17];
                        f3 = fArr3[i18];
                        f5 = fArr3[i19];
                        f6 = fArr3[i20];
                        f15 = f4;
                        f16 = f3;
                        break;
                    case 'S':
                        float f27 = f20;
                        float f28 = f19;
                        i2 = i5;
                        if (c3 == 'c' || c3 == 's' || c3 == 'C' || c3 == 'S') {
                            f8 = (2.0f * f28) - f15;
                            f7 = (2.0f * f27) - f16;
                        } else {
                            f8 = f28;
                            f7 = f27;
                        }
                        int i21 = i2 + 0;
                        int i22 = i2 + 1;
                        int i23 = i2 + 2;
                        int i24 = i2 + 3;
                        path2.cubicTo(f8, f7, fArr3[i21], fArr3[i22], fArr3[i23], fArr3[i24]);
                        f4 = fArr3[i21];
                        f3 = fArr3[i22];
                        f5 = fArr3[i23];
                        f6 = fArr3[i24];
                        f15 = f4;
                        f16 = f3;
                        break;
                    case 'T':
                        float f29 = f20;
                        float f30 = f19;
                        i2 = i5;
                        if (c3 == 'q' || c3 == 't' || c3 == 'Q' || c3 == 'T') {
                            f29 = (2.0f * f29) - f16;
                            f30 = (2.0f * f30) - f15;
                        }
                        int i25 = i2 + 0;
                        int i26 = i2 + 1;
                        path2.quadTo(f30, f29, fArr3[i25], fArr3[i26]);
                        f19 = fArr3[i25];
                        f20 = fArr3[i26];
                        f15 = f30;
                        f16 = f29;
                        break;
                    case 'V':
                        i2 = i5;
                        int i27 = i2 + 0;
                        path2.lineTo(f19, fArr3[i27]);
                        f20 = fArr3[i27];
                        break;
                    case MCInput.KEYCODE_BUTTON_B /*97*/:
                        int i28 = i5 + 5;
                        int i29 = i5 + 6;
                        i3 = i5;
                        drawArc(path2, f19, f20, fArr3[i28] + f19, fArr3[i29] + f20, fArr3[i5 + 0], fArr3[i5 + 1], fArr3[i5 + 2], fArr3[i5 + 3] != 0.0f, fArr3[i5 + 4] != 0.0f);
                        f = f19 + fArr3[i28];
                        f2 = f20 + fArr3[i29];
                        f16 = f20;
                        f15 = f19;
                        break;
                    case MCInput.KEYCODE_BUTTON_X /*99*/:
                        int i30 = i5 + 2;
                        int i31 = i5 + 3;
                        int i32 = i5 + 4;
                        int i33 = i5 + 5;
                        path2.rCubicTo(fArr3[i5 + 0], fArr3[i5 + 1], fArr3[i30], fArr3[i31], fArr3[i32], fArr3[i33]);
                        f10 = fArr3[i30] + f19;
                        f9 = fArr3[i31] + f20;
                        f19 += fArr3[i32];
                        f20 += fArr3[i33];
                        f15 = f10;
                        f16 = f9;
                        i2 = i5;
                        break;
                    case MCInput.KEYCODE_BUTTON_L2 /*104*/:
                        int i34 = i5 + 0;
                        path2.rLineTo(fArr3[i34], 0.0f);
                        f19 += fArr3[i34];
                        i2 = i5;
                        break;
                    case MCInput.KEYCODE_BUTTON_START /*108*/:
                        int i35 = i5 + 0;
                        int i36 = i5 + 1;
                        path2.rLineTo(fArr3[i35], fArr3[i36]);
                        f19 += fArr3[i35];
                        f20 += fArr3[i36];
                        i2 = i5;
                        break;
                    case MCInput.KEYCODE_BUTTON_SELECT /*109*/:
                        int i37 = i5 + 0;
                        f19 += fArr3[i37];
                        int i38 = i5 + 1;
                        f20 += fArr3[i38];
                        if (i5 > 0) {
                            path2.rLineTo(fArr3[i37], fArr3[i38]);
                        } else {
                            path2.rMoveTo(fArr3[i37], fArr3[i38]);
                            f22 = f20;
                            f21 = f19;
                        }
                        i2 = i5;
                        break;
                    case AdPlacementReporter.PLAYLIST_TIMED_OUT_IN_CACHE /*113*/:
                        int i39 = i5 + 0;
                        int i40 = i5 + 1;
                        int i41 = i5 + 2;
                        int i42 = i5 + 3;
                        path2.rQuadTo(fArr3[i39], fArr3[i40], fArr3[i41], fArr3[i42]);
                        f10 = fArr3[i39] + f19;
                        f9 = fArr3[i40] + f20;
                        f19 += fArr3[i41];
                        f20 += fArr3[i42];
                        f15 = f10;
                        f16 = f9;
                        i2 = i5;
                        break;
                    case 's':
                        if (c3 == 'c' || c3 == 's' || c3 == 'C' || c3 == 'S') {
                            float f31 = f19 - f15;
                            f11 = f20 - f16;
                            f23 = f31;
                        } else {
                            f11 = 0.0f;
                        }
                        int i43 = i5 + 0;
                        int i44 = i5 + 1;
                        int i45 = i5 + 2;
                        int i46 = i5 + 3;
                        path2.rCubicTo(f23, f11, fArr3[i43], fArr3[i44], fArr3[i45], fArr3[i46]);
                        f10 = fArr3[i43] + f19;
                        f9 = fArr3[i44] + f20;
                        f19 += fArr3[i45];
                        f20 += fArr3[i46];
                        f15 = f10;
                        f16 = f9;
                        i2 = i5;
                        break;
                    case 't':
                        if (c3 == 'q' || c3 == 't' || c3 == 'Q' || c3 == 'T') {
                            f23 = f19 - f15;
                            f12 = f20 - f16;
                        } else {
                            f12 = 0.0f;
                        }
                        int i47 = i5 + 0;
                        int i48 = i5 + 1;
                        path2.rQuadTo(f23, f12, fArr3[i47], fArr3[i48]);
                        float f32 = f23 + f19;
                        float f33 = f12 + f20;
                        f19 += fArr3[i47];
                        f20 += fArr3[i48];
                        f16 = f33;
                        f15 = f32;
                        i2 = i5;
                        break;
                    case 'v':
                        int i49 = i5 + 0;
                        path2.rLineTo(0.0f, fArr3[i49]);
                        f20 += fArr3[i49];
                        i2 = i5;
                        break;
                    default:
                        i2 = i5;
                        break;
                }
                i5 = i2 + i;
                c3 = c2;
                z = false;
            }
            fArr[z] = f19;
            fArr[1] = f20;
            fArr[2] = f15;
            fArr[3] = f16;
            fArr[4] = f21;
            fArr[5] = f22;
        }

        private static void drawArc(Path path, float f, float f2, float f3, float f4, float f5, float f6, float f7, boolean z, boolean z2) {
            double d;
            double d2;
            float f8 = f;
            float f9 = f3;
            float f10 = f5;
            float f11 = f6;
            double radians = Math.toRadians((double) f7);
            double cos = Math.cos(radians);
            double sin = Math.sin(radians);
            double d3 = (double) f8;
            double d4 = radians;
            double d5 = (double) f2;
            double d6 = d3;
            double d7 = (double) f10;
            double d8 = ((d3 * cos) + (d5 * sin)) / d7;
            double d9 = d5;
            double d10 = (double) f11;
            double d11 = ((((double) (-f8)) * sin) + (d5 * cos)) / d10;
            double d12 = (double) f4;
            double d13 = ((((double) f9) * cos) + (d12 * sin)) / d7;
            double d14 = d7;
            double d15 = ((((double) (-f9)) * sin) + (d12 * cos)) / d10;
            double d16 = d8 - d13;
            double d17 = d11 - d15;
            double d18 = (d8 + d13) / 2.0d;
            double d19 = (d11 + d15) / 2.0d;
            double d20 = sin;
            double d21 = (d16 * d16) + (d17 * d17);
            if (d21 == 0.0d) {
                Log.w(PathParser.LOGTAG, " Points are coincident");
                return;
            }
            double d22 = cos;
            double d23 = (1.0d / d21) - 0.25d;
            if (d23 < 0.0d) {
                Log.w(PathParser.LOGTAG, "Points are too far apart " + d21);
                float sqrt = (float) (Math.sqrt(d21) / 1.99999d);
                drawArc(path, f, f2, f9, f4, f10 * sqrt, f11 * sqrt, f7, z, z2);
                return;
            }
            boolean z3 = z2;
            double sqrt2 = Math.sqrt(d23);
            double d24 = d16 * sqrt2;
            double d25 = sqrt2 * d17;
            if (z == z3) {
                d2 = d18 - d25;
                d = d19 + d24;
            } else {
                d2 = d18 + d25;
                d = d19 - d24;
            }
            double atan2 = Math.atan2(d11 - d, d8 - d2);
            double atan22 = Math.atan2(d15 - d, d13 - d2) - atan2;
            if (z3 != (atan22 >= 0.0d)) {
                atan22 = atan22 > 0.0d ? atan22 - 6.283185307179586d : atan22 + 6.283185307179586d;
            }
            double d26 = d2 * d14;
            double d27 = d * d10;
            arcToBezier(path, (d26 * d22) - (d27 * d20), (d26 * d20) + (d27 * d22), d14, d10, d6, d9, d4, atan2, atan22);
        }

        private static void arcToBezier(Path path, double d, double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9) {
            double d10 = d3;
            int ceil = (int) Math.ceil(Math.abs((d9 * 4.0d) / 3.141592653589793d));
            double cos = Math.cos(d7);
            double sin = Math.sin(d7);
            double cos2 = Math.cos(d8);
            double sin2 = Math.sin(d8);
            double d11 = -d10;
            double d12 = d11 * cos;
            double d13 = d4 * sin;
            double d14 = d11 * sin;
            double d15 = d4 * cos;
            double d16 = (sin2 * d14) + (cos2 * d15);
            double d17 = d9 / ((double) ceil);
            int i = 0;
            double d18 = d6;
            double d19 = d16;
            double d20 = (d12 * sin2) - (d13 * cos2);
            double d21 = d5;
            double d22 = d8;
            while (i < ceil) {
                double d23 = d14;
                double d24 = d22 + d17;
                double sin3 = Math.sin(d24);
                double cos3 = Math.cos(d24);
                double d25 = d17;
                double d26 = (d + ((d10 * cos) * cos3)) - (d13 * sin3);
                double d27 = d2 + (d10 * sin * cos3) + (d15 * sin3);
                double d28 = (d12 * sin3) - (d13 * cos3);
                double d29 = (sin3 * d23) + (cos3 * d15);
                double d30 = d24 - d22;
                double d31 = d15;
                double tan = Math.tan(d30 / 2.0d);
                double d32 = d24;
                double sin4 = (Math.sin(d30) * (Math.sqrt(4.0d + ((3.0d * tan) * tan)) - 1.0d)) / 3.0d;
                double d33 = sin;
                Path path2 = path;
                path2.rLineTo(0.0f, 0.0f);
                path2.cubicTo((float) (d21 + (d20 * sin4)), (float) (d18 + (d19 * sin4)), (float) (d26 - (sin4 * d28)), (float) (d27 - (sin4 * d29)), (float) d26, (float) d27);
                i++;
                d18 = d27;
                d21 = d26;
                d14 = d23;
                d19 = d29;
                d20 = d28;
                d17 = d25;
                d15 = d31;
                d22 = d32;
                ceil = ceil;
                cos = cos;
                sin = d33;
                d10 = d3;
            }
        }
    }
}
