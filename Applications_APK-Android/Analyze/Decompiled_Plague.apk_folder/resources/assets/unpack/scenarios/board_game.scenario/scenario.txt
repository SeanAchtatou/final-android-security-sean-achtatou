# scenarios Data Export : 03/05/18 at 10:20 
Scenario Information:
 scen_identifier:board_game
 scen_title:Ultimate Board Games
 scen_description:Make a best-selling board game in this radically different scenario that has nothing to do with disease! Investors have given you two years to design, produce and distribute your very own game - but can you make it a best selling hit? *Insert witty subliminal marketing about Plague Inc: The Board Game here* :P
 scen_icon:event_boardgame
 scen_created_date:43208
 scen_link_title:About Plague Inc: The Board Game
 scen_link:https://www.plagueinc.com/gettheboardgame
 scen_plague_type:neurax
 scen_difficulty:0
 scen_name:0
 scen_genes:-1
 scen_nexus:0
 scen_nexus_pop_infected:0
 scen_nexus_pop_dead:0
 scen_nexus_pop_zombie:0
 scen_auto_load:0
 scen_free:1
 scen_init_popup1_title:Welcome to the Ultimate Board Games scenario
 scen_init_popup1_text:Investors have given you TWO YEARS to design, produce and distribute a new board game. Can you top the sales charts in that time?
 scen_init_popup2_title:Choose your company location
 scen_init_popup2_text:Once you've built the IKEA office furniture and grabbed yourself a coffee, you can immediately get started on designing your game. 
 scen_event_restriction:1
 scen_score_multiplier:0
 scen_game_win_tech:0
 scen_end_message_image:0
 scen_end_message_title:0
 scen_end_message_text:0
 scen_end_game_screen_title_pc:0
 scen_end_game_tagline:0


