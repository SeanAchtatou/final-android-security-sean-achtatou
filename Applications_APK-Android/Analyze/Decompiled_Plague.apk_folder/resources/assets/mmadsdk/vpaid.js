/*! Ad-SDK-JS-Bridge - 1.6.0 - 3e5a91b - 2018-03-09 */

(function(window, document) {
    if (window.MmJsBridge && window.MmJsBridge.vpaid) {
        return;
    }
    var GENERIC_NAMESPACE = null;
    var MMJS_API_MODULE = "mmjs";
    function capitalizeFirstLetter(string) {
        if (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        } else {
            return "";
        }
    }
    function copyObject(object) {
        var newObject = {};
        Object.keys(object).forEach(function(key) {
            newObject[key] = object[key];
        });
        return newObject;
    }
    function callNativeLayer(apiModule, action, parameters) {
        log.debug("Calling into the native layer with apiModule %s, action %s, and parameters %s", apiModule, action, parameters);
        var i;
        var injectedNamespace = window["MmInjectedFunctions" + capitalizeFirstLetter(apiModule)];
        if (injectedNamespace) {
            log.debug("Selected to communicate with native layer using an injected bridge function");
            var parameterMap = {};
            if (parameters && parameters.length > 0) {
                for (i = 0; i < parameters.length; i++) {
                    if (parameters[i].value !== null) {
                        parameterMap[parameters[i].name] = parameters[i].value;
                    }
                }
            }
            if (injectedNamespace[action]) {
                injectedNamespace[action](JSON.stringify(parameterMap));
            } else {
                log.error("The action %s is not available", action);
            }
        } else {
            log.debug("Selected to communicate with native layer using an iframe");
            var scheme = apiModule ? apiModule : "mmsdk";
            var url = scheme + "://" + action;
            if (parameters && parameters.length > 0) {
                var paramsAddedToUrl = 0;
                var value;
                for (i = 0; i < parameters.length; i++) {
                    value = parameters[i].value;
                    if (value !== null && typeof value == "object") {
                        value = JSON.stringify(value);
                    }
                    if (value !== null) {
                        if (paramsAddedToUrl === 0) {
                            url += "?";
                        } else {
                            url += "&";
                        }
                        url += encodeURIComponent(parameters[i].name) + "=" + encodeURIComponent(value);
                        paramsAddedToUrl++;
                    }
                }
            }
            var iframe = document.createElement("iframe");
            iframe.style.display = "none";
            iframe.src = url;
            setTimeout(function() {
                document.body.appendChild(iframe);
                document.body.removeChild(iframe);
            }, 0);
        }
        log.debug("Bottom of callNativeLayer");
    }
    var hasCommonLoaded = !!window.MmJsBridge;
    if (!hasCommonLoaded) {
        (function() {
            function getPopupFunction(name) {
                return function() {
                    log.error("Calling the function %s is not allowed", name);
                };
            }
            window.mmHiddenAlert = window.alert;
            Object.defineProperties(window, {
                alert: {
                    value: getPopupFunction("alert")
                },
                confirm: {
                    value: getPopupFunction("confirm")
                },
                prompt: {
                    value: getPopupFunction("prompt")
                }
            });
        })();
        window.MmJsBridge = {};
        (function() {
            var LOG_LEVELS = {
                ERROR: {
                    text: "ERROR",
                    level: 0
                },
                WARN: {
                    text: "WARN",
                    level: 1
                },
                INFO: {
                    text: "INFO",
                    level: 2
                },
                DEBUG: {
                    text: "DEBUG",
                    level: 3
                }
            };
            var $logLevel = LOG_LEVELS.INFO;
            var loggingSupported = window.console && console.log;
            function genericLog(args, logLevel) {
                if (loggingSupported && logLevel.level <= $logLevel.level) {
                    var message = args[0];
                    if (args.length > 1) {
                        for (var i = 1; i < args.length; i++) {
                            var replacement = args[i];
                            if (!exists(replacement)) {
                                replacement = "";
                            } else if (isObject(replacement)) {
                                replacement = JSON.stringify(replacement);
                            } else if (isFunction(replacement)) {
                                replacement = replacement.toString();
                            }
                            message = message.replace("%s", replacement);
                        }
                    }
                    console.log(logLevel.text + ": " + message);
                }
            }
            MmJsBridge.logging = {
                setLogLevel: function(logLevelString) {
                    if (LOG_LEVELS.hasOwnProperty(logLevelString)) {
                        $logLevel = LOG_LEVELS[logLevelString];
                    }
                },
                log: {
                    error: function() {
                        genericLog(arguments, LOG_LEVELS.ERROR);
                    },
                    warn: function() {
                        genericLog(arguments, LOG_LEVELS.WARN);
                    },
                    info: function() {
                        genericLog(arguments, LOG_LEVELS.INFO);
                    },
                    debug: function() {
                        genericLog(arguments, LOG_LEVELS.DEBUG);
                    }
                }
            };
        })();
        MmJsBridge.callbackManager = function() {
            var callbacks = [];
            return {
                callCallback: function(callbackId) {
                    log.debug("MmJsBridge.callbackManager.callCallback called with callbackId %s", callbackId);
                    var callbackIdNum = parseInt(callbackId, 10);
                    if (isNumber(callbackIdNum) && !isNaN(callbackIdNum) && callbackIdNum >= 0 && callbackIdNum < callbacks.length) {
                        var callback = callbacks[callbackIdNum];
                        var argsArray = Array.prototype.slice.call(arguments, 1);
                        log.debug("Found callback. Calling %s with arguments %s", callback, argsArray);
                        callback.apply(window, argsArray);
                    } else {
                        log.warn("Unable to call callback with id %s because it could not be found", callbackId);
                    }
                    log.debug("Bottom of MmJsBridge.callbackManager.callCallback");
                },
                generateCallbackId: function(callback) {
                    var callbackId;
                    var index = callbacks.indexOf(callback);
                    if (index >= 0) {
                        callbackId = index;
                    } else {
                        callbacks.push(callback);
                        callbackId = callbacks.length - 1;
                    }
                    log.debug("Callback id %s for callback %s", callbackId, callback);
                    return callbackId;
                }
            };
        }();
        setTimeout(function() {
            document.body.style.webkitTouchCallout = "none";
        }, 0);
    }
    var log = MmJsBridge.logging.log;
    function generateCallbackId(callback) {
        return MmJsBridge.callbackManager.generateCallbackId(callback);
    }
    function generateParameterObject(name, value) {
        return {
            name: name,
            value: defined(value) ? value : null
        };
    }
    function generateParameterArrayFromObject(obj) {
        var parameterArray = [];
        Object.keys(obj).forEach(function(key) {
            parameterArray.push(generateParameterObject(key, obj[key]));
        });
        return parameterArray;
    }
    function defined(variable) {
        return variable !== undefined;
    }
    function is(variable, type) {
        return typeof variable == type;
    }
    function isNumber(variable) {
        return is(variable, "number");
    }
    function isBoolean(variable) {
        return is(variable, "boolean");
    }
    function isString(variable) {
        return is(variable, "string");
    }
    function isNonEmptyString(variable) {
        return is(variable, "string") && variable.length > 0;
    }
    function isFunction(variable) {
        return is(variable, "function");
    }
    function isObject(variable) {
        return is(variable, "object");
    }
    function isArray(variable) {
        return Object.prototype.toString.call(variable) === "[object Array]";
    }
    function isNonEmptyArrayWithEachItemMatchingFilter(variable, filter) {
        return isArray(variable) && variable.length !== 0 && variable.filter(filter).length == variable.length;
    }
    function exists(param) {
        return param !== undefined && param !== null;
    }
    function getIsNumberInRangeFilter(min, max) {
        return function(variable) {
            return isNumber(variable) && variable >= min && variable <= max;
        };
    }
    function isValidCalendarEventOptions(options) {
        return options && isNonEmptyString(options.description) && isNonEmptyString(options.start) && (!exists(options.summary) || isNonEmptyString(options.summary)) && (!exists(options.location) || isNonEmptyString(options.location)) && (!exists(options.end) || isNonEmptyString(options.end)) && (!exists(options.transparency) || variableIsOneOf(options.transparency, [ "transparent", "opaque" ])) && (!exists(options.recurrence) || isObject(options.recurrence) && isNonEmptyString(options.recurrence.frequency) && variableIsOneOf(options.recurrence.frequency, [ "daily", "weekly", "monthly", "yearly" ]) && (!exists(options.recurrence.expires) || isNonEmptyString(options.recurrence.expires)) && (!exists(options.recurrence.daysInWeek) || isNonEmptyArrayWithEachItemMatchingFilter(options.recurrence.daysInWeek, getIsNumberInRangeFilter(0, 6))) && (!exists(options.recurrence.daysInMonth) || isNonEmptyArrayWithEachItemMatchingFilter(options.recurrence.daysInMonth, getIsNumberInRangeFilter(-30, 31))) && (!exists(options.recurrence.monthsInYear) || isNonEmptyArrayWithEachItemMatchingFilter(options.recurrence.monthsInYear, getIsNumberInRangeFilter(1, 12))) && (!exists(options.recurrence.daysInYear) || isNonEmptyArrayWithEachItemMatchingFilter(options.recurrence.daysInYear, getIsNumberInRangeFilter(-364, 365))) && (!exists(options.recurrence.interval) || isNumber(options.recurrence.interval))) && (!exists(options.url) || isNonEmptyString(options.url)) && (!exists(options.reminder) || isNonEmptyString(options.reminder));
    }
    function variableIsOneOf(variable, possibleValues) {
        return possibleValues.indexOf(variable) !== -1;
    }
    var ListenerManager = function() {
        var that = this;
        that._listeners = {};
        that._queue = [];
        that._inProgress = false;
    };
    ListenerManager.prototype = {
        constructor: ListenerManager,
        _enqueue: function(funcToExecute) {
            this._queue.push(funcToExecute);
        },
        _flushQueue: function() {
            var that = this;
            if (that._inProgress) {
                return;
            }
            that._inProgress = true;
            while (that._queue.length) {
                try {
                    var funcToExecute = that._queue.shift();
                    funcToExecute.call(that);
                } catch (err) {
                    log.error("Error executing listener. %s", err);
                }
            }
            that._inProgress = false;
        },
        addEventListener: function(event, listener) {
            var that = this;
            that._enqueue(function() {
                if (!that._listeners[event]) {
                    that._listeners[event] = [];
                }
                if (that._listeners[event].indexOf(listener) < 0) {
                    that._listeners[event].push(listener);
                }
            });
            that._flushQueue();
        },
        removeEventListener: function(event, listener) {
            var that = this;
            that._enqueue(function() {
                if (!that._listeners[event]) {
                    return;
                }
                if (!defined(listener)) {
                    delete that._listeners[event];
                    return;
                }
                var index = that._listeners[event].indexOf(listener);
                if (index >= 0) {
                    that._listeners[event].splice(index, 1);
                }
            });
            that._flushQueue();
        },
        callListeners: function(event, args) {
            var that = this;
            that._enqueue(function() {
                if (that._listeners[event]) {
                    that._listeners[event].forEach(function(listener) {
                        listener.apply(null, args);
                    });
                }
            });
            that._flushQueue();
        }
    };
    function httpGet(url, timeout, callback) {
        log.debug("httpGet called with url %s, timeout %s, and callback %s", url, timeout, callback);
        var callbackId = generateCallbackId(callback);
        callNativeLayer(MMJS_API_MODULE, "httpGet", [ generateParameterObject("url", url), generateParameterObject("timeout", timeout), generateParameterObject("callbackId", callbackId) ]);
    }
    (function() {
        var VPAID_API_MODULE = "vpaid";
        var VPAID_VERSION = "2.0";
        var DEFAULT_VIDEO_WIDTH = 800;
        var DEFAULT_VIDEO_HEIGHT = 450;
        var STATES = {
            loading: 0,
            ready: 1,
            playing: 2,
            done: 3
        };
        var $callbacks = {
            AdLoaded: onAdLoaded,
            AdStarted: onAdStarted,
            AdStopped: onAdStopped,
            AdSkipped: onAdSkipped,
            AdSkippableStateChange: onAdSkippableStateChange,
            AdPaused: onAdPaused,
            AdPlaying: onAdPlaying,
            AdVolumeChange: onAdVolumeChange,
            AdImpression: onAdImpression,
            AdClickThru: onAdClickThru,
            AdVideoStart: onAdVideoStart,
            AdVideoFirstQuartile: onAdVideoFirstQuartile,
            AdVideoMidpoint: onAdVideoMidpoint,
            AdVideoThirdQuartile: onAdVideoThirdQuartile,
            AdVideoComplete: onAdVideoComplete,
            AdUserAcceptInvitation: onAdUserAcceptInvitation,
            AdUserClose: onAdUserClose,
            AdError: onAdError,
            AdLog: onAdLog
        };
        var TRACKING_EVENTS = {
            creativeView: "creativeViewUrls",
            start: "videoStartUrls",
            firstQuartile: "firstQuartileUrls",
            midpoint: "midpointUrls",
            thirdQuartile: "thirdQuartileUrls",
            complete: "videoCompleteUrls",
            mute: "muteUrls",
            unmute: "unmuteUrls",
            skip: "skipUrls",
            pause: "pauseUrls",
            resume: "resumeUrls",
            close: "closeUrls",
            closeLinear: "closeUrls",
            acceptInvitation: "userAcceptInvitationUrls",
            acceptInvitationLinear: "userAcceptInvitationUrls"
        };
        var $creative, $creativeVersion, $volume, $videoWidth, $videoHeight, $actualVideoWidth, $actualVideoHeight, $videoWidthAtInit, $videoHeightAtInit, $minSkipOffset, $maxSkipOffset, $startAdTimeout, $skipAdTimeout, $startAdTimeoutTime, $skipAdTimeoutTime, $htmlEndCardTimeoutTime, $adStartTimestamp, $adPausedTimestamp, $totalAdPausedTime, $minSkipOffsetTimeout, $maxSkipOffsetTimeout, $adChoicesIconOffsetTimeout, $adChoicesIconDurationTimeout, $adChoicesDurationStartTimestamp, $adChoicesPausedTimeSinceIconDisplayed, $adChoicesImageElement, $spinnerImage;
        var $minSkipOffsetReached = false;
        var $adSkippable = false;
        var $shouldFireSkipUrls = false;
        var $adChoicesDurationReached = false;
        var $adChoicesStates = {
            loading: 0,
            waitingForOffsetToBeReached: 1,
            waitingForIconToLoad: 2,
            iconLoadedWhilePaused: 3,
            waitingForDurationToBeReached: 4,
            finished: 5
        };
        var $adChoicesState = $adChoicesStates.loading;
        var $videoContainer = document.createElement("div");
        hideElement($videoContainer);
        $videoContainer.id = "aolVideoContainer";
        $videoContainer.style.position = "absolute";
        $videoContainer.style.left = "0px";
        $videoContainer.style.top = "0px";
        $videoContainer.style.backgroundColor = "black";
        var $video = document.createElement("video");
        $videoContainer.appendChild($video);
        $video.id = "aolVideo";
        $video.style.position = "absolute";
        $video.style.zIndex = "1";
        $video.setAttribute("webkit-playsinline", "");
        $video.setAttribute("playsinline", "");
        var $html = document.createElement("div");
        $videoContainer.appendChild($html);
        $html.id = "aolHtml";
        $html.style.position = "absolute";
        $html.style.zIndex = "2";
        var $skipButton = document.createElement("img");
        $videoContainer.appendChild($skipButton);
        hideElement($skipButton);
        $skipButton.id = "aolSkipButton";
        $skipButton.style.width = "40px";
        $skipButton.style.height = "40px";
        $skipButton.style.position = "absolute";
        $skipButton.style.top = "0px";
        $skipButton.style.right = "0px";
        $skipButton.style.margin = "5px";
        $skipButton.addEventListener("click", onSkipButtonPressed);
        $skipButton.style.zIndex = "4";
        var $endcardContainer = document.createElement("div");
        hideElement($endcardContainer);
        $endcardContainer.id = "aolEndCardContainer";
        $endcardContainer.style.position = "absolute";
        $endcardContainer.style.left = "0";
        $endcardContainer.style.top = "0";
        $endcardContainer.style.backgroundColor = "white";
        var $closeButton = document.createElement("img");
        $endcardContainer.appendChild($closeButton);
        $closeButton.id = "aolCloseButton";
        $closeButton.style.width = "40px";
        $closeButton.style.height = "40px";
        $closeButton.style.position = "absolute";
        $closeButton.style.top = "0px";
        $closeButton.style.right = "0px";
        $closeButton.style.margin = "5px";
        $closeButton.addEventListener("click", onCloseButtonPressed);
        $closeButton.style.zIndex = "2";
        var $currentState = STATES.loading;
        var $parsedVast = {};
        MmJsBridge.vpaid = {
            init: function(options) {
                log.debug("MmJsBridge.vpaid.init called with options %s", options);
                if (!options || !options.vastDocs || options.vastDocs.length === 0 || typeof options.minSkipOffset != "number" || typeof options.maxSkipOffset != "number" || typeof options.desiredBitrate != "number" || typeof options.startAdTimeout != "number" || typeof options.skipAdTimeout != "number" || typeof options.adUnitTimeout != "number" || typeof options.htmlEndCardTimeout != "number" || typeof options.closeButtonImage != "string" || typeof options.skipButtonImage != "string" || typeof options.spinnerImage != "string") {
                    log.error("A required option was not set.");
                    callNativeLayer(VPAID_API_MODULE, "adLoadFailed");
                    return;
                }
                if (!parseVast(options.vastDocs)) {
                    log.error("Failed to parse VAST");
                    callNativeLayer(VPAID_API_MODULE, "adLoadFailed");
                    return;
                }
                $closeButton.src = options.closeButtonImage;
                $skipButton.src = options.skipButtonImage;
                $spinnerImage = options.spinnerImage;
                document.body.appendChild($videoContainer);
                document.body.appendChild($endcardContainer);
                $startAdTimeoutTime = options.startAdTimeout;
                $skipAdTimeoutTime = options.skipAdTimeout;
                $htmlEndCardTimeoutTime = options.htmlEndCardTimeout;
                log.debug("Finished parsing vast: %s", $parsedVast);
                var iframe = document.createElement("iframe");
                iframe.id = "aolVpaidJsIframe";
                iframe.style.display = "none";
                document.body.appendChild(iframe);
                var vpaidScript = document.createElement("script");
                var vpaidScriptTimedOut = false;
                var vpaidScriptTimeout = setTimeout(function() {
                    vpaidScriptTimedOut = true;
                    log.error("VPAID creative JS file did not load within " + options.adUnitTimeout + "ms");
                    callNativeLayer(VPAID_API_MODULE, "adLoadFailed");
                }, options.adUnitTimeout);
                vpaidScript.onload = function() {
                    log.debug("vpaidScript.onload called");
                    if (vpaidScriptTimedOut) {
                        return;
                    }
                    clearTimeout(vpaidScriptTimeout);
                    if (iframe.contentWindow.getVPAIDAd) {
                        log.debug("Found iframe.contentWindow.getVPAIDAd");
                        var fn = iframe.contentWindow.getVPAIDAd;
                        $creative = null;
                        if (fn && typeof fn == "function") {
                            $creative = fn();
                        }
                        if (!$creative) {
                            log.error("VPAID creative was null");
                            callNativeLayer(VPAID_API_MODULE, "adLoadFailed");
                            return;
                        }
                        $creativeVersion = $creative.handshakeVersion(VPAID_VERSION);
                        if (compareStringVersions($creativeVersion, "1.0") < 0) {
                            log.error("VPAID creative has invalid version: %s", $creativeVersion);
                            callNativeLayer(VPAID_API_MODULE, "adLoadFailed");
                            return;
                        }
                        for (var eventName in $callbacks) {
                            $creative.subscribe($callbacks[eventName], eventName);
                        }
                        sizeAndPositionElements();
                        var desiredBitrate = options.desiredBitrate;
                        var creativeData = {};
                        if ($parsedVast.adParameters) {
                            creativeData.AdParameters = $parsedVast.adParameters;
                        }
                        var environmentVars = {
                            videoSlot: $video,
                            videoSlotCanAutoPlay: true,
                            slot: $html
                        };
                        $video.addEventListener("loadedmetadata", function() {
                            $actualVideoWidth = this.videoWidth;
                            $actualVideoHeight = this.videoHeight;
                            sizeAndPositionElements();
                        });
                        $minSkipOffset = options.minSkipOffset;
                        $maxSkipOffset = options.maxSkipOffset;
                        $videoWidthAtInit = $videoWidth;
                        $videoHeightAtInit = $videoHeight;
                        log.debug("Calling $creative.initAd");
                        $creative.initAd($videoWidth, $videoHeight, "normal", desiredBitrate, creativeData, environmentVars);
                    } else {
                        log.error("VPAID creative JS file loaded but the function iframe.contentWindow.getVPAIDAd did not exist");
                        callNativeLayer(VPAID_API_MODULE, "adLoadFailed");
                    }
                };
                vpaidScript.onerror = vpaidScript.onabort = function() {
                    log.debug("vpaidScript.onerror or vpaidScript.onabort called");
                    if (vpaidScriptTimedOut) {
                        return;
                    }
                    clearTimeout(vpaidScriptTimeout);
                    log.error("VPAID creative JS file failed to load");
                    callNativeLayer(VPAID_API_MODULE, "adLoadFailed");
                };
                vpaidScript.src = $parsedVast.vpaidUrl;
                iframe.contentWindow.document.body.appendChild(vpaidScript);
            }
        };
        function onAdLoaded() {
            log.debug("onAdLoaded called");
            callNativeLayer(VPAID_API_MODULE, "adLoadSucceeded");
            $currentState = STATES.ready;
            if ($videoWidthAtInit != $videoWidth || $videoHeightAtInit != $videoHeight) {
                log.debug("Calling $creative.resizeAd");
                $creative.resizeAd($videoWidth, $videoHeight, "normal");
            }
            $volume = getCurrentVolume();
            if (!$parsedVast.endCard && $creative.getAdCompanions) {
                var adCompanions = $creative.getAdCompanions();
                if (arrayExistsAndIsNotEmpty(adCompanions)) {
                    var parser = new DOMParser();
                    var adCompanionsXml = parser.parseFromString(adCompanions, "text/xml");
                    if (adCompanionsXml) {
                        var companionAdsElements = adCompanionsXml.getElementsByTagName("CompanionAds");
                        if (arrayExistsAndIsNotEmpty(companionAdsElements)) {
                            log.debug("Found CompanionAds element in the adCompanions VPAID property");
                            parseCompanionAds(companionAdsElements[0]);
                        }
                    }
                }
            }
            if ($parsedVast.endCard && $parsedVast.endCard.type == "static") {
                log.debug("Building static end card");
                var endCardImage = document.createElement("img");
                $endcardContainer.appendChild(endCardImage);
                endCardImage.id = "aolEndCardImage";
                endCardImage.src = $parsedVast.endCard.url;
                endCardImage.style.width = $parsedVast.endCard.width + "px";
                endCardImage.style.height = $parsedVast.endCard.height + "px";
                endCardImage.style.position = "absolute";
                endCardImage.style.left = "50%";
                endCardImage.style.top = "50%";
                endCardImage.style.marginLeft = $parsedVast.endCard.width / 2 * -1 + "px";
                endCardImage.style.marginTop = $parsedVast.endCard.height / 2 * -1 + "px";
                endCardImage.style.zIndex = "1";
                if ($parsedVast.endCard.backgroundColor) {
                    $endcardContainer.style.backgroundColor = $parsedVast.endCard.backgroundColor;
                }
                if ($parsedVast.endCard.clickTracking || $parsedVast.wrapperCompanionClickTracking || $parsedVast.endCard.clickThrough) {
                    endCardImage.addEventListener("click", function() {
                        pingUrls($parsedVast.endCard.clickTracking);
                        pingUrls($parsedVast.wrapperCompanionClickTracking);
                        if ($parsedVast.endCard.clickThrough) {
                            mraid.open($parsedVast.endCard.clickThrough);
                        }
                    });
                }
            }
            showElement($videoContainer);
            function afterReady() {
                sizeAndPositionElements();
                mraid.addEventListener("sizeChange", sizeAndPositionElements);
                if (mraid.isViewable()) {
                    afterViewable();
                } else {
                    mraid.addEventListener("viewableChange", function viewableChangeListener(viewable) {
                        if (viewable) {
                            mraid.removeEventListener("viewableChange", viewableChangeListener);
                            afterViewable();
                        }
                    });
                }
            }
            function afterViewable() {
                startAd();
            }
            if (mraid.getState() == "loading") {
                mraid.addEventListener("ready", afterReady);
            } else {
                afterReady();
            }
        }
        function onAdStarted() {
            log.debug("onAdStarted called");
            clearTimeout($startAdTimeout);
            $currentState = STATES.playing;
            pingUrls($parsedVast.creativeViewUrls);
            $adStartTimestamp = Date.now();
            $totalAdPausedTime = 0;
            setSkipButtonTimeouts();
            setIconTimeouts();
            mraid.addEventListener("viewableChange", onViewableChangeWhilePlaying);
        }
        function onAdStopped() {
            log.debug("onAdStopped called");
            showEndCardOrClose();
        }
        function onAdSkipped() {
            log.debug("onAdSkipped called");
            $shouldFireSkipUrls = true;
            showEndCardOrClose();
        }
        function onAdSkippableStateChange() {
            log.debug("onAdSkippableStateChange called");
            if ($minSkipOffsetReached && $creative.getAdSkippableState()) {
                log.debug("Showing skip button");
                makeAdSkippable();
            }
        }
        function onAdPaused() {
            log.debug("onAdPaused called");
            clearTimeout($minSkipOffsetTimeout);
            clearTimeout($maxSkipOffsetTimeout);
            clearTimeout($adChoicesIconOffsetTimeout);
            clearTimeout($adChoicesIconDurationTimeout);
            $adPausedTimestamp = Date.now();
            pingUrls($parsedVast.pauseUrls);
        }
        function onAdPlaying() {
            log.debug("onAdPlaying called");
            if ($adPausedTimestamp) {
                $totalAdPausedTime += Date.now() - $adPausedTimestamp;
                if ($adChoicesDurationStartTimestamp) {
                    $adChoicesPausedTimeSinceIconDisplayed += Date.now() - $adPausedTimestamp;
                }
            }
            $adPausedTimestamp = null;
            setSkipButtonTimeouts();
            setIconTimeouts();
            pingUrls($parsedVast.resumeUrls);
        }
        function onAdVolumeChange() {
            log.debug("onAdVolumeChange called");
            var newVolume = getCurrentVolume();
            log.debug("New Volume: %s, Old Volume: %s", newVolume, $volume);
            if (newVolume === 0 && $volume > 0) {
                log.debug("Volume was muted");
                pingUrls($parsedVast.muteUrls);
            } else if (newVolume > 0 && $volume === 0) {
                log.debug("Volume was unmuted");
                pingUrls($parsedVast.unmuteUrls);
            }
            $volume = newVolume;
        }
        function onAdImpression() {
            log.debug("onAdImpression called");
            pingUrls($parsedVast.impressionUrls);
        }
        function onAdClickThru(url, id, playerHandles) {
            log.debug("onAdClickThru called with url %s, id %s, and playerHandles %s", url, id, playerHandles);
            pingUrls($parsedVast.videoClickTrackingUrls);
            if (playerHandles) {
                if (url) {
                    log.debug("Calling mraid.open with the url passed to onAdClickThru");
                    mraid.open(url);
                } else if ($parsedVast.videoClickThroughUrl) {
                    log.debug("Calling mraid.open with the url from parsedVast.videoClickThroughUrl, %s", $parsedVast.videoClickThroughUrl);
                    mraid.open($parsedVast.videoClickThroughUrl);
                }
            }
        }
        function onAdVideoStart() {
            log.debug("onAdVideoStart called");
            pingUrls($parsedVast.videoStartUrls);
        }
        function onAdVideoFirstQuartile() {
            log.debug("onAdVideoFirstQuartile called");
            pingUrls($parsedVast.firstQuartileUrls);
        }
        function onAdVideoMidpoint() {
            log.debug("onAdVideoMidpoint called");
            pingUrls($parsedVast.midpointUrls);
        }
        function onAdVideoThirdQuartile() {
            log.debug("onAdVideoThirdQuartile called");
            pingUrls($parsedVast.thirdQuartileUrls);
        }
        function onAdVideoComplete() {
            log.debug("onAdVideoComplete called");
            pingUrls($parsedVast.videoCompleteUrls);
            callNativeLayer(VPAID_API_MODULE, "onVideoComplete");
        }
        function onAdUserAcceptInvitation() {
            log.debug("onAdUserAcceptInvitation called");
            pingUrls($parsedVast.userAcceptInvitationUrls);
        }
        function onAdUserClose() {
            log.debug("onAdUserClose called");
            pingUrls($parsedVast.closeUrls);
        }
        function onAdError(message) {
            log.error("onAdError called with message %s", message);
            pingUrls($parsedVast.errorUrls);
        }
        function onAdLog(message) {
            log.info("onAdLog called with message %s", message);
        }
        function onMinSkipOffsetReached() {
            $minSkipOffsetReached = true;
            if (!$creative.skipAd || !$creative.getAdSkippableState || $creative.getAdSkippableState()) {
                makeAdSkippable();
            }
        }
        function onMaxSkipOffsetReached() {
            makeAdSkippable();
        }
        function setSkipButtonTimeouts() {
            var timeSinceAdStarted = Date.now() - $adStartTimestamp - $totalAdPausedTime;
            var minSkipOffsetTime = $minSkipOffset - timeSinceAdStarted;
            var maxSkipOffsetTime = $maxSkipOffset - timeSinceAdStarted;
            clearTimeout($minSkipOffsetTimeout);
            clearTimeout($maxSkipOffsetTimeout);
            if (minSkipOffsetTime <= 0) {
                onMinSkipOffsetReached();
            } else {
                $minSkipOffsetTimeout = setTimeout(onMinSkipOffsetReached, minSkipOffsetTime);
            }
            if (maxSkipOffsetTime <= 0) {
                onMaxSkipOffsetReached();
            } else {
                $maxSkipOffsetTimeout = setTimeout(onMaxSkipOffsetReached, maxSkipOffsetTime);
            }
        }
        function setIconTimeouts() {
            log.debug("setIconTimeouts called");
            if (!$parsedVast.icons || !$parsedVast.icons.AdChoices) {
                log.debug("No AdChoices icon found, exiting setIconTimeouts");
                return;
            }
            if ($creative.getAdIcons && $creative.getAdIcons()) {
                log.debug("Not displaying the AdChoices icon because the adIcons property of the creative is true.");
                return;
            }
            clearTimeout($adChoicesIconDurationTimeout);
            clearTimeout($adChoicesIconOffsetTimeout);
            var timeSinceAdStarted = Date.now() - $adStartTimestamp - $totalAdPausedTime;
            var remainingOffset = $parsedVast.icons.AdChoices.offset - timeSinceAdStarted;
            if ($adChoicesState == $adChoicesStates.loading || $adChoicesState == $adChoicesStates.waitingForOffsetToBeReached) {
                if (remainingOffset <= 0) {
                    onAdChoicesIconOffsetReached();
                } else {
                    $adChoicesIconOffsetTimeout = setTimeout(onAdChoicesIconOffsetReached, remainingOffset);
                }
            } else if ($adChoicesState == $adChoicesStates.iconLoadedWhilePaused) {
                displayAdChoicesIcon();
            } else if ($adChoicesState == $adChoicesStates.waitingForDurationToBeReached && $parsedVast.icons.AdChoices.duration >= 0) {
                var remainingDuration = $parsedVast.icons.AdChoices.duration - (Date.now() - $adChoicesDurationStartTimestamp - $adChoicesPausedTimeSinceIconDisplayed);
                if (remainingDuration <= 0) {
                    onAdChoicesIconDurationReachedOrVideoEnded();
                } else {
                    $adChoicesIconDurationTimeout = setTimeout(onAdChoicesIconDurationReachedOrVideoEnded, remainingDuration);
                }
            }
        }
        function onAdChoicesIconDurationReachedOrVideoEnded() {
            log.debug("onAdChoicesIconDurationReachedOrVideoEnded called");
            if ($adChoicesState !== $adChoicesStates.finished) {
                $adChoicesState = $adChoicesStates.finished;
                if ($adChoicesImageElement) {
                    hideElement($adChoicesImageElement);
                    $adChoicesImageElement.removeEventListener("load", onAdChoicesIconImageLoaded);
                }
            }
        }
        function onAdChoicesIconOffsetReached() {
            log.debug("onAdChoicesIconOffsetReached called");
            if ($adChoicesState > $adChoicesStates.waitingForOffsetToBeReached) {
                log.warn("onAdChoicesIconOffsetReached was called when the offset had already been reached.");
                return;
            }
            $adChoicesState = $adChoicesStates.waitingForIconToLoad;
            log.debug("Building AdChoices Icon");
            var adChoicesIconData = $parsedVast.icons.AdChoices;
            var iconImage = new Image();
            iconImage.id = "aolAdChoicesIconImage";
            iconImage.style.display = "none";
            iconImage.style.position = "absolute";
            iconImage.style.left = "0px";
            iconImage.style.top = "0px";
            iconImage.style.height = "20px";
            iconImage.style.zIndex = "3";
            $videoContainer.appendChild(iconImage);
            $adChoicesImageElement = iconImage;
            iconImage.onerror = iconImage.onabort = function() {
                log.warn("Failed to load AdChoices icon");
                $videoContainer.removeChild(iconImage);
            };
            iconImage.addEventListener("click", function() {
                log.debug("AdChoices icon clicked");
                pingUrls(adChoicesIconData.clickTracking);
                if (adChoicesIconData.clickThrough) {
                    mraid.open(adChoicesIconData.clickThrough);
                }
            });
            iconImage.addEventListener("load", onAdChoicesIconImageLoaded);
            iconImage.src = adChoicesIconData.image;
        }
        function onAdChoicesIconImageLoaded() {
            log.debug("onAdChoicesIconImageLoaded called");
            if ($adChoicesState !== $adChoicesStates.waitingForIconToLoad) {
                log.warn("onAdChoicesIconImageLoaded was called when the state was note waitingForIconToLoad. Ignoring the call.");
                return;
            }
            if (!$adPausedTimestamp) {
                $adChoicesState = $adChoicesStates.waitingForDurationToBeReached;
                displayAdChoicesIcon();
            } else {
                $adChoicesState = $adChoicesStates.iconLoadedWhilePaused;
            }
        }
        function displayAdChoicesIcon() {
            $adChoicesDurationStartTimestamp = Date.now();
            $adChoicesPausedTimeSinceIconDisplayed = 0;
            showElement($adChoicesImageElement);
            pingUrls($parsedVast.icons.AdChoices.viewTracking);
            if ($parsedVast.icons.AdChoices.duration >= 0) {
                $adChoicesIconDurationTimeout = setTimeout(onAdChoicesIconDurationReachedOrVideoEnded, $parsedVast.icons.AdChoices.duration);
            }
        }
        function showEndCardOrClose() {
            log.debug("showEndCardOrClose called");
            if ($currentState >= STATES.done) {
                return;
            }
            $currentState = STATES.done;
            clearTimeout($minSkipOffsetTimeout);
            clearTimeout($maxSkipOffsetTimeout);
            clearTimeout($skipAdTimeout);
            mraid.removeEventListener("viewableChange", onViewableChangeWhilePlaying);
            onAdChoicesIconDurationReachedOrVideoEnded();
            hideElement($videoContainer);
            $videoContainer.removeChild($video);
            if (!$parsedVast.endCard) {
                log.debug("No end card, closing");
                pingUrls($parsedVast.closeUrls);
                mraid.close();
                return;
            }
            if ($shouldFireSkipUrls) {
                pingUrls($parsedVast.skipUrls);
            }
            var spinner;
            function htmlContentFetchedOrTimeout() {
                log.debug("htmlContentFetchedOrTimeout called");
                if ($parsedVast.endCard.status != "ready") {
                    log.error("Failed to load HTMLResource URL, closing creative.");
                    mraid.close();
                    return;
                }
                var htmlContainer = document.createElement("div");
                htmlContainer.id = "aolEndCardHtmlContainer";
                htmlContainer.innerHTML = $parsedVast.endCard.html;
                htmlContainer.width = "100%";
                htmlContainer.height = "100%";
                htmlContainer.style.zIndex = "1";
                if ($parsedVast.endCard.backgroundColor) {
                    htmlContainer.style.backgroundColor = $parsedVast.endCard.backgroundColor;
                }
                if (spinner) {
                    $endcardContainer.removeChild(spinner);
                }
                $endcardContainer.appendChild(htmlContainer);
                executeScriptsInElement(htmlContainer);
                pingUrls($parsedVast.endCard.creativeViewUrls);
                pingUrls($parsedVast.wrapperCreativeViewUrls);
            }
            if ($parsedVast.endCard.type == "html") {
                log.debug("html end card");
                if ($parsedVast.endCard.status == "ready") {
                    log.debug("html end card ready");
                    htmlContentFetchedOrTimeout();
                } else if ($parsedVast.endCard.status == "failed") {
                    log.debug("html end card failed");
                    log.error("Failed to load HTMLResource URL, closing creative.");
                    mraid.close();
                    return;
                } else {
                    log.debug("html end card not yet ready, waiting");
                    $endcardContainer.addEventListener("htmlcontentfetched", htmlContentFetchedOrTimeout);
                    setTimeout(htmlContentFetchedOrTimeout, $htmlEndCardTimeoutTime);
                    spinner = document.createElement("img");
                    spinner.id = "aolSpinnerImage";
                    spinner.src = $spinnerImage;
                    spinner.style.width = "50px";
                    spinner.style.height = "50px";
                    spinner.style.position = "absolute";
                    spinner.style.left = "50%";
                    spinner.style.top = "50%";
                    spinner.style.marginLeft = "-25px";
                    spinner.style.marginTop = "-25px";
                    $endcardContainer.appendChild(spinner);
                }
            } else if ($parsedVast.endCard.type == "iframe") {
                log.debug("iframe end card");
                var iframe = document.createElement("iframe");
                iframe.id = "aolEndCardIframe";
                iframe.src = $parsedVast.endCard.url;
                iframe.style.border = 0;
                iframe.style.width = "100%;";
                iframe.style.height = "100%";
                iframe.style.zIndex = "1";
                $endcardContainer.appendChild(iframe);
                pingUrls($parsedVast.endCard.creativeViewUrls);
                pingUrls($parsedVast.wrapperCreativeViewUrls);
            } else {
                log.debug("static end card");
                pingUrls($parsedVast.endCard.creativeViewUrls);
                pingUrls($parsedVast.wrapperCreativeViewUrls);
            }
            showElement($endcardContainer);
        }
        function onCloseButtonPressed() {
            log.debug("onCloseButtonPressed called");
            pingUrls($parsedVast.closeUrls);
            mraid.close();
        }
        function onSkipButtonPressed() {
            log.debug("onSkipButtonPressed called");
            $shouldFireSkipUrls = true;
            $skipAdTimeout = setTimeout(function() {
                log.error("AdSkipped or AdStopped events were not fired before the timeout was reached.");
                onAdSkipped();
            }, $skipAdTimeoutTime);
            if (compareStringVersions($creativeVersion, "2.0") >= 0 && $creative.skipAd && $creative.getAdSkippableState && $creative.getAdSkippableState()) {
                log.debug("Calling $creative.skipAd");
                $creative.skipAd();
            } else {
                log.debug("Calling $creative.stopAd");
                $creative.stopAd();
            }
        }
        function onViewableChangeWhilePlaying(viewable) {
            log.debug("onViewableChangeWhilePlaying called with viewable %s", viewable);
            if (viewable) {
                log.debug("Calling $creative.resumeAd");
                $creative.resumeAd();
            } else {
                log.debug("Calling $creative.pauseAd");
                $creative.pauseAd();
            }
        }
        function startAd() {
            log.debug("startAd called");
            $startAdTimeout = setTimeout(function() {
                log.error("AdStarted not called within the timeout. Closing the creative.");
                mraid.close();
            }, $startAdTimeoutTime);
            log.debug("Calling $creative.startAd");
            $creative.startAd();
        }
        function scaleWidthAndHeight(unscaledVideoWidth, unscaledVideoHeight) {
            log.debug("scaleWidthAndHeight called with unscaledVideoWidth %s and unscaledVideoHeight %s", unscaledVideoWidth, unscaledVideoHeight);
            var currentPosition = mraid.getCurrentPosition();
            var windowWidth = currentPosition.width;
            var windowHeight = currentPosition.height;
            var windowWidthToVideoWidthRatio = windowWidth / unscaledVideoWidth;
            var windowHeightToVideoHeightRatio = windowHeight / unscaledVideoHeight;
            var multiplier = Math.min(windowWidthToVideoWidthRatio, windowHeightToVideoHeightRatio);
            var scaledWidthHeight = {
                width: unscaledVideoWidth * multiplier,
                height: unscaledVideoHeight * multiplier
            };
            log.debug("scaledWidthHeight: %s", scaledWidthHeight);
            return scaledWidthHeight;
        }
        function parseVast(vastDocs) {
            log.debug("parseVast called with vastDocs %s", vastDocs);
            var parser = new DOMParser();
            vastDocs.forEach(function(vastDoc) {
                var i, j, trackingElements, event;
                var vastXml = parser.parseFromString(vastDoc, "text/xml");
                var wrapperElements = vastXml.getElementsByTagName("Wrapper");
                var isWrapper = arrayExistsAndIsNotEmpty(wrapperElements);
                var impressionElements = vastXml.getElementsByTagName("Impression");
                if (arrayExistsAndIsNotEmpty(impressionElements)) {
                    $parsedVast.impressionUrls = $parsedVast.impressionUrls || [];
                    for (i = 0; i < impressionElements.length; i++) {
                        $parsedVast.impressionUrls.push(impressionElements[i].textContent.trim());
                    }
                }
                var errorElements = vastXml.getElementsByTagName("Error");
                if (arrayExistsAndIsNotEmpty(errorElements)) {
                    $parsedVast.errorUrls = $parsedVast.errorUrls || [];
                    for (i = 0; i < errorElements.length; i++) {
                        $parsedVast.errorUrls.push(errorElements[i].textContent.trim());
                    }
                }
                var linearElements = vastXml.getElementsByTagName("Linear");
                if (arrayExistsAndIsNotEmpty(linearElements)) {
                    var linearElement = linearElements[0];
                    var adParametersElement = getChildElementByTagName(linearElement, "AdParameters");
                    if (adParametersElement) {
                        $parsedVast.adParameters = adParametersElement.textContent.trim();
                    }
                    var trackingEventsElement = getChildElementByTagName(linearElement, "TrackingEvents");
                    if (trackingEventsElement) {
                        trackingElements = getChildElementsByTagName(trackingEventsElement, "Tracking");
                        if (arrayExistsAndIsNotEmpty(trackingElements)) {
                            for (i = 0; i < trackingElements.length; i++) {
                                event = trackingElements[i].getAttribute("event");
                                var parsedVastProperty = TRACKING_EVENTS[event];
                                if (parsedVastProperty) {
                                    $parsedVast[parsedVastProperty] = $parsedVast[parsedVastProperty] || [];
                                    $parsedVast[parsedVastProperty].push(trackingElements[i].textContent.trim());
                                }
                            }
                        }
                    }
                    var videoClicks = getChildElementByTagName(linearElement, "VideoClicks");
                    if (videoClicks) {
                        var clickThroughElements = getChildElementsByTagName(videoClicks, "ClickThrough");
                        if (arrayExistsAndIsNotEmpty(clickThroughElements)) {
                            $parsedVast.videoClickThroughUrl = clickThroughElements[0].textContent.trim();
                        }
                        var clickTrackingElements = getChildElementsByTagName(videoClicks, "ClickTracking");
                        if (arrayExistsAndIsNotEmpty(clickTrackingElements)) {
                            $parsedVast.videoClickTrackingUrls = $parsedVast.videoClickTrackingUrls || [];
                            for (i = 0; i < clickTrackingElements.length; i++) {
                                $parsedVast.videoClickTrackingUrls.push(clickTrackingElements[i].textContent.trim());
                            }
                        }
                        var customClickElements = getChildElementsByTagName(videoClicks, "CustomClick");
                        if (arrayExistsAndIsNotEmpty(customClickElements)) {
                            $parsedVast.videoCustomClickUrls = $parsedVast.videoCustomClickUrls || [];
                            for (i = 0; i < customClickElements.length; i++) {
                                $parsedVast.videoCustomClickUrls.push(customClickElements[i].textContent.trim());
                            }
                        }
                    }
                    var mediaFilesElement = getChildElementByTagName(linearElement, "MediaFiles");
                    var mediaFileElements = getChildElementsByTagName(mediaFilesElement, "MediaFile");
                    if (arrayExistsAndIsNotEmpty(mediaFileElements)) {
                        for (i = 0; i < mediaFileElements.length; i++) {
                            if (mediaFileElements[i].getAttribute("apiFramework") == "VPAID" && (mediaFileElements[i].getAttribute("type") == "application/javascript" || mediaFileElements[i].getAttribute("type") == "application/x-javascript" || mediaFileElements[i].getAttribute("type") == "text/javascript")) {
                                $parsedVast.vpaidUrl = mediaFileElements[i].textContent.trim();
                                $parsedVast.videoWidth = mediaFileElements[i].hasAttribute("width") ? parseInt(mediaFileElements[i].getAttribute("width"), 10) : 0;
                                $parsedVast.videoHeight = mediaFileElements[i].hasAttribute("height") ? parseInt(mediaFileElements[i].getAttribute("height"), 10) : 0;
                                break;
                            }
                        }
                    }
                    var iconsElement = getChildElementByTagName(linearElement, "Icons");
                    if (iconsElement) {
                        var iconElements = getChildElementsByTagName(iconsElement, "Icon");
                        if (arrayExistsAndIsNotEmpty(iconElements)) {
                            $parsedVast.icons = $parsedVast.icons || {};
                            for (i = 0; i < iconElements.length; i++) {
                                if (!iconElements[i].hasAttribute("program") || iconElements[i].getAttribute("program").toLowerCase().trim() != "adchoices") {
                                    log.debug("Ignoring Icon because it didn't have a program attribute or the program was not AdChoices.");
                                    continue;
                                }
                                var icon = {};
                                icon.program = "AdChoices";
                                var staticResource = getChildElementByTagName(iconElements[i], "StaticResource");
                                if (!staticResource) {
                                    log.debug("Ignoring Icon because it did not have a StaticResource.");
                                    continue;
                                }
                                icon.image = staticResource.textContent.trim();
                                if (!icon.image) {
                                    log.debug("Ignoring Icon because the StaticResource is empty.");
                                    continue;
                                }
                                if (iconElements[i].hasAttribute("offset")) {
                                    icon.offset = convertTimeStringToMs(iconElements[i].getAttribute("offset"));
                                    if (icon.offset === null || icon.offset < 0) {
                                        icon.offset = 0;
                                    }
                                } else {
                                    icon.offset = 0;
                                }
                                if (iconElements[i].hasAttribute("duration")) {
                                    icon.duration = convertTimeStringToMs(iconElements[i].getAttribute("duration"));
                                    if (icon.duration === null || icon.duration < 0) {
                                        icon.duration = -1;
                                    }
                                } else {
                                    icon.duration = -1;
                                }
                                var iconClicks = getChildElementByTagName(iconElements[i], "IconClicks");
                                if (iconClicks) {
                                    var iconClickThrough = getChildElementByTagName(iconClicks, "IconClickThrough");
                                    if (!iconClickThrough) {
                                        log.debug("Ignoring Icon because it did not have an IconClickThrough");
                                        continue;
                                    }
                                    icon.clickThrough = iconClickThrough.textContent.trim();
                                    if (!icon.clickThrough) {
                                        log.debug("Ignoring Icon because the IconClickThrough is empty");
                                        continue;
                                    }
                                    var iconClickTracking = getChildElementsByTagName(iconClicks, "IconClickTracking");
                                    if (arrayExistsAndIsNotEmpty(iconClickTracking)) {
                                        icon.clickTracking = [];
                                        for (j = 0; j < iconClickTracking.length; j++) {
                                            var iconClickTrackingUrl = iconClickTracking[j].textContent.trim();
                                            if (iconClickTrackingUrl) {
                                                icon.clickTracking.push(iconClickTrackingUrl);
                                            }
                                        }
                                    }
                                }
                                var iconViewTracking = getChildElementsByTagName(iconElements[i], "IconViewTracking");
                                if (arrayExistsAndIsNotEmpty(iconViewTracking)) {
                                    icon.viewTracking = [];
                                    for (j = 0; j < iconViewTracking.length; j++) {
                                        var iconViewTrackingUrl = iconViewTracking[j].textContent.trim();
                                        if (iconViewTrackingUrl) {
                                            icon.viewTracking.push(iconViewTrackingUrl);
                                        }
                                    }
                                }
                                $parsedVast.icons[icon.program] = icon;
                                break;
                            }
                        }
                    }
                }
                var companionAdsElements = vastXml.getElementsByTagName("CompanionAds");
                if (arrayExistsAndIsNotEmpty(companionAdsElements)) {
                    var companionAdsElement = companionAdsElements[0];
                    if (isWrapper) {
                        var companionClickTrackingElements = companionAdsElement.getElementsByTagName("CompanionClickTracking");
                        if (arrayExistsAndIsNotEmpty(companionClickTrackingElements)) {
                            $parsedVast.wrapperCompanionClickTracking = $parsedVast.wrapperCompanionClickTracking || [];
                            for (i = 0; i < companionClickTrackingElements.length; i++) {
                                $parsedVast.wrapperCompanionClickTracking.push(companionClickTrackingElements[i].textContent.trim());
                            }
                        }
                        trackingElements = companionAdsElement.getElementsByTagName("Tracking");
                        if (arrayExistsAndIsNotEmpty(trackingElements)) {
                            for (i = 0; i < trackingElements.length; i++) {
                                event = trackingElements[i].getAttribute("event");
                                if (event == "creativeView") {
                                    $parsedVast.wrapperCreativeViewUrls = $parsedVast.wrapperCreativeViewUrls || [];
                                    $parsedVast.wrapperCreativeViewUrls.push(trackingElements[i].textContent.trim());
                                }
                            }
                        }
                    } else {
                        parseCompanionAds(companionAdsElement);
                    }
                }
            });
            if (!$parsedVast.vpaidUrl) {
                log.debug("VAST Invalid because the vpaid URL could not be found");
                return false;
            }
            return true;
        }
        function parseCompanionAds(companionAdsElement) {
            log.debug("parseCompanionAds called");
            var companionResourcePriority = [ "static", "html", "iframe" ];
            var companionElement, resourceElement, resourceType, i;
            if (companionAdsElement && arrayExistsAndIsNotEmpty(companionAdsElement.childNodes)) {
                for (i = 0; i < companionAdsElement.childNodes.length; i++) {
                    var tempCompanionElement = companionAdsElement.childNodes[i];
                    var tempResourceElement, tempResourceType;
                    if (tempCompanionElement.tagName == "Companion" && parseInt(tempCompanionElement.getAttribute("width"), 10) >= 300 && parseInt(tempCompanionElement.getAttribute("height"), 10) >= 250) {
                        var staticResourceElements = companionAdsElement.childNodes[i].getElementsByTagName("StaticResource");
                        var htmlResourceElements = companionAdsElement.childNodes[i].getElementsByTagName("HTMLResource");
                        var iframeResourceElements = companionAdsElement.childNodes[i].getElementsByTagName("IFrameResource");
                        if (arrayExistsAndIsNotEmpty(iframeResourceElements)) {
                            tempResourceElement = iframeResourceElements[0];
                            tempResourceType = "iframe";
                        } else if (arrayExistsAndIsNotEmpty(htmlResourceElements)) {
                            tempResourceElement = htmlResourceElements[0];
                            tempResourceType = "html";
                        } else if (arrayExistsAndIsNotEmpty(staticResourceElements)) {
                            for (var staticResourceIndex = 0; staticResourceIndex < staticResourceElements.length; staticResourceIndex++) {
                                if (staticResourceElements[staticResourceIndex].getAttribute("creativeType") == "image/png" || staticResourceElements[staticResourceIndex].getAttribute("creativeType") == "image/jpeg" || staticResourceElements[staticResourceIndex].getAttribute("creativeType") == "image/bmp" || staticResourceElements[staticResourceIndex].getAttribute("creativeType") == "image/gif") {
                                    tempResourceElement = staticResourceElements[staticResourceIndex];
                                    tempResourceType = "static";
                                    break;
                                }
                            }
                        }
                        if (tempResourceType && (!companionElement || companionResourcePriority.indexOf(tempResourceType) > companionResourcePriority.indexOf(resourceType))) {
                            companionElement = tempCompanionElement;
                            resourceType = tempResourceType;
                            resourceElement = tempResourceElement;
                        }
                    }
                }
            }
            if (companionElement) {
                var companionJson = {};
                if (resourceType == "static") {
                    companionJson.type = "static";
                    companionJson.width = parseInt(companionElement.getAttribute("width"), 10);
                    companionJson.height = parseInt(companionElement.getAttribute("height"), 10);
                    if (resourceElement.hasAttribute("backgroundColor")) {
                        companionJson.backgroundColor = resourceElement.getAttribute("backgroundColor");
                    }
                    companionJson.url = resourceElement.textContent.trim();
                    var companionClickTrackingElements = companionElement.getElementsByTagName("CompanionClickTracking");
                    if (arrayExistsAndIsNotEmpty(companionClickTrackingElements)) {
                        companionJson.clickTracking = [];
                        for (i = 0; i < companionClickTrackingElements.length; i++) {
                            companionJson.clickTracking.push(companionClickTrackingElements[i].textContent.trim());
                        }
                    }
                    var companionClickThroughElements = companionElement.getElementsByTagName("CompanionClickThrough");
                    if (arrayExistsAndIsNotEmpty(companionClickThroughElements)) {
                        companionJson.clickThrough = companionClickThroughElements[0].textContent.trim();
                    }
                } else if (resourceType == "html") {
                    var htmlContentFetchedEvent;
                    if (window.CustomEvent) {
                        htmlContentFetchedEvent = new Event("htmlcontentfetched");
                    } else {
                        htmlContentFetchedEvent = document.createEvent("Event");
                        htmlContentFetchedEvent.initEvent("htmlcontentfetched", true, true);
                    }
                    companionJson.type = "html";
                    companionJson.status = "loading";
                    if (resourceElement.hasAttribute("backgroundColor")) {
                        companionJson.backgroundColor = resourceElement.getAttribute("backgroundColor");
                    }
                    httpGet(resourceElement.textContent.trim(), 3e4, function(response, message) {
                        log.debug("httpGet callback for HTMLResource called with response %s", response);
                        if (response) {
                            log.debug("httpGet was successful");
                            companionJson.status = "ready";
                            companionJson.html = response;
                        } else {
                            log.error("httpGet failed with message %s", message);
                            companionJson.status = "failed";
                        }
                        $endcardContainer.dispatchEvent(htmlContentFetchedEvent);
                    });
                } else if (resourceType == "iframe") {
                    companionJson.type = "iframe";
                    companionJson.url = resourceElement.textContent.trim();
                }
                var trackingElements = companionElement.getElementsByTagName("Tracking");
                if (arrayExistsAndIsNotEmpty(trackingElements)) {
                    for (i = 0; i < trackingElements.length; i++) {
                        var event = trackingElements[i].getAttribute("event");
                        if (event == "creativeView") {
                            companionJson.creativeViewUrls = companionJson.creativeViewUrls || [];
                            companionJson.creativeViewUrls.push(trackingElements[i].textContent.trim());
                        }
                    }
                }
                if (companionJson.type) {
                    $parsedVast.endCard = companionJson;
                }
            }
        }
        function sizeAndPositionElements() {
            log.debug("sizeAndPositionElements called");
            if (mraid.getState() == "loading") {
                log.debug("mraid state is still loading and we can't rely on mraid.getCurrentPosition(). Exiting sizeAndPositionElements().");
                return;
            }
            var currentPosition = mraid.getCurrentPosition();
            var fullscreenWidth = currentPosition.width + "px";
            var fullscreenHeight = currentPosition.height + "px";
            var isLandscape = currentPosition.width > currentPosition.height;
            $videoContainer.style.width = fullscreenWidth;
            $videoContainer.style.height = fullscreenHeight;
            $endcardContainer.style.width = fullscreenWidth;
            $endcardContainer.style.height = fullscreenHeight;
            var unscaledVideoWidth = $actualVideoWidth || $parsedVast.videoWidth || DEFAULT_VIDEO_WIDTH;
            var unscaledVideoHeight = $actualVideoHeight || $parsedVast.videoHeight || DEFAULT_VIDEO_HEIGHT;
            var scaledWidthHeight = scaleWidthAndHeight(unscaledVideoWidth, unscaledVideoHeight);
            if (scaledWidthHeight.width != $videoWidth || scaledWidthHeight.height != $videoHeight) {
                $videoWidth = scaledWidthHeight.width;
                $videoHeight = scaledWidthHeight.height;
                $video.style.width = $videoWidth + "px";
                $video.style.height = $videoHeight + "px";
                $html.style.width = $video.style.width;
                $html.style.height = $video.style.height;
                if ($currentState >= STATES.ready) {
                    log.debug("Calling $creative.resizeAd with width %s and height %s", $videoWidth, $videoHeight);
                    $creative.resizeAd($videoWidth, $videoHeight, "normal");
                }
            }
            $video.style.left = "50%";
            $video.style.marginLeft = $videoWidth / 2 * -1 + "px";
            if (isLandscape) {
                $video.style.top = "50%";
                $video.style.marginTop = $videoHeight / 2 * -1 + "px";
            } else {
                if ($videoHeight + 50 > fullscreenHeight) {
                    $video.style.top = "0px";
                } else {
                    $video.style.top = "50px";
                }
                $video.style.marginTop = "0px";
            }
            $html.style.left = $video.style.left;
            $html.style.top = $video.style.top;
            $html.style.marginLeft = $video.style.marginLeft;
            $html.style.marginTop = $video.style.marginTop;
        }
        function hideElement(element) {
            log.debug("hideElement called, element.id: %s", element.id);
            element.style.display = "none";
        }
        function showElement(element) {
            log.debug("showElement called, element.id: %s", element.id);
            element.style.display = "";
        }
        function compareStringVersions(firstVersion, secondVersion) {
            log.debug("compareStringVersions called with firstVersion %s and secondVersion %s", firstVersion, secondVersion);
            var firstVersionArray = firstVersion.split(".");
            var secondVersionArray = secondVersion.split(".");
            var longerVersionLength = Math.max(firstVersionArray.length, secondVersionArray.length);
            for (var i = 0; i < longerVersionLength; i++) {
                var firstVersionPart, secondVersionPart;
                if (i < firstVersionArray.length) {
                    firstVersionPart = parseInt(firstVersionArray[i], 10);
                } else {
                    firstVersionPart = 0;
                }
                if (i < secondVersionArray.length) {
                    secondVersionPart = parseInt(secondVersionArray[i], 10);
                } else {
                    secondVersionPart = 0;
                }
                if (firstVersionPart > secondVersionPart) {
                    log.debug("firstVersion greater than second version");
                    return 1;
                } else if (firstVersionPart < secondVersionPart) {
                    log.debug("firstVersion less than second version");
                    return -1;
                }
            }
            log.debug("versions equal");
            return 0;
        }
        function getCurrentVolume() {
            log.debug("getCurrentVolume called");
            return $creative.getAdVolume ? $creative.getAdVolume() : -1;
        }
        function pingUrls(urls) {
            log.debug("pingUrls called with urls %s", urls);
            if (urls) {
                urls.forEach(pingUrl);
            }
        }
        function pingUrl(url) {
            log.debug("pingUrl called with url %s", url);
            var image = document.createElement("img");
            image.src = url;
            var event;
            if (window.CustomEvent) {
                event = new CustomEvent("aolpingedurl", {
                    detail: url
                });
            } else {
                event = document.createEvent("CustomEvent");
                event.initCustomEvent("aolpingedurl", true, true, url);
            }
            document.dispatchEvent(event);
        }
        function arrayExistsAndIsNotEmpty(arr) {
            return arr && arr.length > 0;
        }
        function getChildElementByTagName(parentElement, tagName) {
            var childElements = getChildElementsByTagName(parentElement, tagName);
            if (arrayExistsAndIsNotEmpty(childElements)) {
                return childElements[0];
            } else {
                return null;
            }
        }
        function getChildElementsByTagName(parentElement, tagName) {
            if (!parentElement) {
                return [];
            }
            var descendantElements = parentElement.getElementsByTagName(tagName);
            if (!arrayExistsAndIsNotEmpty(descendantElements)) {
                return [];
            }
            descendantElements = Array.prototype.slice.call(descendantElements);
            descendantElements = descendantElements.filter(function(element) {
                return element.parentElement === parentElement;
            });
            return descendantElements;
        }
        function makeAdSkippable() {
            log.debug("makeAdSkippable called. $adSkippable: %s", $adSkippable);
            if ($adSkippable) {
                return;
            }
            $adSkippable = true;
            showElement($skipButton);
            callNativeLayer(VPAID_API_MODULE, "adSkippable");
        }
        function executeScriptsInElement(element) {
            var scripts = element.getElementsByTagName("script");
            if (arrayExistsAndIsNotEmpty(scripts)) {
                var i = 0;
                var callback = function() {
                    i++;
                    if (i < scripts.length) {
                        executeScript(scripts[i]);
                    }
                };
                var executeScript = function(script) {
                    if (script.src) {
                        fetchScriptAndExecute(script.src, callback);
                    } else {
                        executeScriptText(script.innerHTML);
                        callback();
                    }
                };
                executeScript(scripts[0]);
            }
        }
        function fetchScriptAndExecute(url, callback) {
            var script = document.createElement("script");
            script.onload = script.onerror = script.onabort = callback;
            script.src = url;
            document.body.appendChild(script);
        }
        function executeScriptText(scriptText) {
            window["eval"].call(window, scriptText);
        }
        function convertTimeStringToMs(timeString) {
            log.debug("Converting timeString %s to milliseconds.", timeString);
            var parts = timeString.split(":");
            if (parts.length != 3) {
                log.debug("timeString did not have three parts, return null");
                return null;
            }
            var timeMs = (parseInt(parts[0], 10) * 60 * 60 + parseInt(parts[1], 10) * 60 + parseFloat(parts[2])) * 1e3;
            log.debug("timeString %s converted to %s ms", timeString, timeMs);
            return timeMs;
        }
    })();
    callNativeLayer(GENERIC_NAMESPACE, "fileLoaded", [ generateParameterObject("filename", "vpaid.js") ]);
})(window, document);