package com.miniclip.notification;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.android.gms.drive.DriveFile;
import java.util.TimeZone;

public class BootReceiver extends BroadcastReceiver {
    private static String NotificationId = "plagueinc.notification.id";
    private static String PreferenceId = "plagueinc.preference.id";
    private static String PreferenceText = "plagueinc.notification.text";
    private static String PreferenceTitle = "plagueinc.notification.title";
    private static Activity activity;

    public static void init(Activity activity2) {
        activity = activity2;
    }

    public void onReceive(Context context, Intent intent) {
        try {
            Log.i("NOTIFICATION", "onreceive notification");
            int intExtra = intent.getIntExtra(NotificationId, 0);
            SharedPreferences sharedPreferences = context.getSharedPreferences(String.format("%d", Integer.valueOf(intExtra)), 0);
            String string = sharedPreferences.getString(PreferenceTitle, "Plague Inc.");
            String string2 = sharedPreferences.getString(PreferenceText, "Can you infect the world?");
            Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
            launchIntentForPackage.setAction("android.intent.action.MAIN");
            launchIntentForPackage.addCategory("android.intent.category.LAUNCHER");
            launchIntentForPackage.addFlags(603979776);
            ((NotificationManager) context.getSystemService("notification")).notify(intExtra, new NotificationCompat.Builder(activity, "miscellaneous").setSmallIcon(context.getResources().getIdentifier("icon", "drawable", context.getPackageName())).setContentTitle(string).setContentText(string2).setPriority(0).build());
        } catch (Exception e) {
            Log.d("BootReceiver", "An alarm was received but there was an error");
            e.printStackTrace();
        }
    }

    public static void setupAlarm(Context context, int i, String str, String str2, int i2) {
        Log.i("NOTIFICATION", String.format("id: %d title: %s text:%s", Integer.valueOf(i), str, str2));
        Log.i("NOTIFICATION", String.format("current time: %d", Long.valueOf(System.currentTimeMillis())));
        Log.i("NOTIFICATION", String.format("seconds: %d", Integer.valueOf(i2)));
        long j = (long) (i2 * 1000);
        Log.i("NOTIFICATION", String.format("current time: %d", Long.valueOf(System.currentTimeMillis() + j)));
        SharedPreferences.Editor edit = context.getSharedPreferences(String.format("%d", Integer.valueOf(i)), 0).edit();
        edit.putString(PreferenceTitle, str);
        edit.putString(PreferenceText, str2);
        edit.commit();
        Intent intent = new Intent(context, BootReceiver.class);
        intent.putExtra(NotificationId, i);
        long currentTimeMillis = System.currentTimeMillis() + j;
        long offset = currentTimeMillis - ((long) TimeZone.getDefault().getOffset(currentTimeMillis));
        Log.i("NOTIFICATION", String.format("triggerTime: %d", Long.valueOf(offset)));
        ((AlarmManager) context.getSystemService(NotificationCompat.CATEGORY_ALARM)).set(0, offset, PendingIntent.getBroadcast(context, 0, intent, DriveFile.MODE_READ_ONLY));
        Log.i("NOTIFICATION", "scheduled succesfully");
    }

    protected static void removeAlarm(Context context, int i) {
        Log.i("NOTIFICATION", String.format("cancel id: %d", Integer.valueOf(i)));
        Intent intent = new Intent(context, BootReceiver.class);
        intent.putExtra(NotificationId, i);
        ((AlarmManager) context.getSystemService(NotificationCompat.CATEGORY_ALARM)).cancel(PendingIntent.getBroadcast(context, 0, intent, DriveFile.MODE_READ_ONLY));
        ((NotificationManager) context.getSystemService("notification")).cancel(i);
    }

    public static void createCustomNotification(int i, String str, String str2, int i2) {
        Log.i("PlagueIncActivity", "create notification");
        setupAlarm(activity, i, str, str2, i2);
    }

    public static void cancelCustomNotification(int i) {
        Log.i("PlagueIncActivity", "cancel notification");
        removeAlarm(activity, i);
    }
}
