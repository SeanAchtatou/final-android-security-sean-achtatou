package com.millennialmedia.internal.task.handshake;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.AsyncTask;
import com.millennialmedia.MMSDK;
import com.millennialmedia.internal.Handshake;

@TargetApi(23)
public class HandshakeRequestService extends JobService {
    private HandshakeRequester handshakeRequester;

    private static class HandshakeRequester extends AsyncTask<Void, Void, Void> {
        private HandshakeRequester() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voidArr) {
            Handshake.request(false);
            return null;
        }
    }

    @SuppressLint({"StaticFieldLeak"})
    public boolean onStartJob(final JobParameters jobParameters) {
        if (!MMSDK.isInitialized()) {
            return false;
        }
        this.handshakeRequester = new HandshakeRequester() {
            /* access modifiers changed from: protected */
            public void onPostExecute(Void voidR) {
                HandshakeRequestService.this.jobFinished(jobParameters, false);
            }
        };
        this.handshakeRequester.execute(new Void[0]);
        return true;
    }

    public boolean onStopJob(JobParameters jobParameters) {
        if (this.handshakeRequester != null) {
            this.handshakeRequester.cancel(true);
        }
        return true;
    }
}
