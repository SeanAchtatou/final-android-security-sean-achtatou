package android.support.ekglxtiyel.mkulma;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcelable;
import android.support.ekglxtiyel.ekglxtiyel.woewfJyZrgET;
import android.support.ekglxtiyel.lzcewhsnchqg.GJShOWhM;
import android.support.ekglxtiyel.lzcewhsnchqg.YuxfPhPIaOM;
import android.support.ekglxtiyel.lzcewhsnchqg.anLHcLdRskb;
import android.support.ekglxtiyel.lzcewhsnchqg.wSMjih;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class lFKFPcMiJsI extends ViewGroup {
    private static final boolean ELxjQaDRrI = (Build.VERSION.SDK_INT >= 19);
    static final xaEMqBpLOv JyKmOjX;
    private static final boolean UxnFzZ;
    /* access modifiers changed from: private */
    public static final int[] gWiOFio = {16842931};
    private Drawable ADojbWgjJep;
    private int CGmlqExFKDsw;
    private int CkytDdEwp;
    private int DCbsRKERTF;
    private boolean DYldmviR;
    private int IsLoypLqxg;
    private final hfpzXtVZzF JHCbNsJ;
    private Drawable JPEfQkMfcXeW;
    private float NkBWDzI;
    private float OvRsHjLd;
    private final ArrayList PvSPypUwEGN;
    private final phJWzDEq RBZUWx;
    private Object RFOcVKwijOVg;
    private float SYafeh;
    private final qEsxapivk TUFgsvaVkdN;
    private Drawable VYBKAHZ;
    private Drawable VzWbdN;
    private Drawable WlQHAGESvdRg;
    private final qEsxapivk ZVIDXbvWn;
    private final phJWzDEq ZcxyUcLBGcV;
    private MNSntxfeN clabDdWhMqLI;
    private float dQCQlqcL;
    private boolean ejoYRLl;
    private boolean gnKlzvCZLmUA;
    private boolean ineUXGyuQmP;
    private int nHLQZwbUt;
    private Drawable udYZxYWU;
    private Paint uvKYrIFVv;
    private boolean wqpspf;
    private Drawable wzLBqLQcFlu;

    static {
        boolean z = true;
        if (Build.VERSION.SDK_INT < 21) {
            z = false;
        }
        UxnFzZ = z;
        if (Build.VERSION.SDK_INT >= 21) {
            JyKmOjX = new mCAnCvCLz();
        } else {
            JyKmOjX = new axKVDobCEa();
        }
    }

    private boolean CGmlqExFKDsw() {
        return NkBWDzI() != null;
    }

    private boolean CkytDdEwp() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (((YuxfPhPIaOM) getChildAt(i).getLayoutParams()).ELxjQaDRrI) {
                return true;
            }
        }
        return false;
    }

    private Drawable JHCbNsJ() {
        int ELxjQaDRrI2 = wSMjih.ELxjQaDRrI(this);
        if (ELxjQaDRrI2 == 0) {
            if (this.VzWbdN != null) {
                JyKmOjX(this.VzWbdN, ELxjQaDRrI2);
                return this.VzWbdN;
            }
        } else if (this.JPEfQkMfcXeW != null) {
            JyKmOjX(this.JPEfQkMfcXeW, ELxjQaDRrI2);
            return this.JPEfQkMfcXeW;
        }
        return this.WlQHAGESvdRg;
    }

    private void JyKmOjX(View view, boolean z) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if ((z || CkytDdEwp(childAt)) && (!z || childAt != view)) {
                wSMjih.JyKmOjX(childAt, 4);
            } else {
                wSMjih.JyKmOjX(childAt, 1);
            }
        }
    }

    private boolean JyKmOjX(Drawable drawable, int i) {
        if (drawable == null || !android.support.ekglxtiyel.lnuvhvnygha.ekglxtiyel.lFKFPcMiJsI.JyKmOjX(drawable)) {
            return false;
        }
        android.support.ekglxtiyel.lnuvhvnygha.ekglxtiyel.lFKFPcMiJsI.JyKmOjX(drawable, i);
        return true;
    }

    private View NkBWDzI() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (CkytDdEwp(childAt) && ZVIDXbvWn(childAt)) {
                return childAt;
            }
        }
        return null;
    }

    private Drawable OvRsHjLd() {
        int ELxjQaDRrI2 = wSMjih.ELxjQaDRrI(this);
        if (ELxjQaDRrI2 == 0) {
            if (this.JPEfQkMfcXeW != null) {
                JyKmOjX(this.JPEfQkMfcXeW, ELxjQaDRrI2);
                return this.JPEfQkMfcXeW;
            }
        } else if (this.VzWbdN != null) {
            JyKmOjX(this.VzWbdN, ELxjQaDRrI2);
            return this.VzWbdN;
        }
        return this.VYBKAHZ;
    }

    private static boolean TUFgsvaVkdN(View view) {
        Drawable background = view.getBackground();
        return background != null && background.getOpacity() == -1;
    }

    private void UxnFzZ() {
        if (!UxnFzZ) {
            this.udYZxYWU = JHCbNsJ();
            this.wzLBqLQcFlu = OvRsHjLd();
        }
    }

    static String gWiOFio(int i) {
        return (i & 3) == 3 ? "LEFT" : (i & 5) == 5 ? "RIGHT" : Integer.hexing(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.graphics.drawable.Drawable, int):boolean
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(int, int):void
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.view.View, float):void
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.view.View, int):boolean
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.view.View, boolean):void */
    public void CGmlqExFKDsw(View view) {
        if (!CkytDdEwp(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.wqpspf) {
            YuxfPhPIaOM yuxfPhPIaOM = (YuxfPhPIaOM) view.getLayoutParams();
            yuxfPhPIaOM.gWiOFio = 1.0f;
            yuxfPhPIaOM.UxnFzZ = true;
            JyKmOjX(view, true);
        } else if (JyKmOjX(view, 3)) {
            this.ZVIDXbvWn.JyKmOjX(view, 0, view.getTop());
        } else {
            this.TUFgsvaVkdN.JyKmOjX(view, localValue() - view.localValue(), view.getTop());
        }
        invalidate();
    }

    /* access modifiers changed from: package-private */
    public boolean CkytDdEwp(View view) {
        return (YuxfPhPIaOM.JyKmOjX(((YuxfPhPIaOM) view.getLayoutParams()).JyKmOjX, wSMjih.ELxjQaDRrI(view)) & 7) != 0;
    }

    public void ELxjQaDRrI(int i) {
        View JyKmOjX2 = JyKmOjX(i);
        if (JyKmOjX2 == null) {
            throw new IllegalArgumentException("No drawer view found with gravity " + gWiOFio(i));
        }
        CGmlqExFKDsw(JyKmOjX2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.graphics.drawable.Drawable, int):boolean
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(int, int):void
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.view.View, float):void
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.view.View, int):boolean
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void ELxjQaDRrI(View view) {
        YuxfPhPIaOM yuxfPhPIaOM = (YuxfPhPIaOM) view.getLayoutParams();
        if (!yuxfPhPIaOM.UxnFzZ) {
            yuxfPhPIaOM.UxnFzZ = true;
            if (this.clabDdWhMqLI != null) {
                this.clabDdWhMqLI.JyKmOjX(view);
            }
            JyKmOjX(view, true);
            if (hasWindowFocus()) {
                sendAccessibilityEvent(32);
            }
            view.requestFocus();
        }
    }

    /* access modifiers changed from: package-private */
    public int JHCbNsJ(View view) {
        return YuxfPhPIaOM.JyKmOjX(((YuxfPhPIaOM) view.getLayoutParams()).JyKmOjX, wSMjih.ELxjQaDRrI(this));
    }

    public boolean JHCbNsJ(int i) {
        View JyKmOjX2 = JyKmOjX(i);
        if (JyKmOjX2 != null) {
            return uvKYrIFVv(JyKmOjX2);
        }
        return false;
    }

    public int JyKmOjX(View view) {
        int JHCbNsJ2 = JHCbNsJ(view);
        if (JHCbNsJ2 == 3) {
            return this.DCbsRKERTF;
        }
        if (JHCbNsJ2 == 5) {
            return this.nHLQZwbUt;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public View JyKmOjX() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (((YuxfPhPIaOM) childAt.getLayoutParams()).UxnFzZ) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public View JyKmOjX(int i) {
        int JyKmOjX2 = YuxfPhPIaOM.JyKmOjX(i, wSMjih.ELxjQaDRrI(this)) & 7;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if ((JHCbNsJ(childAt) & 7) == JyKmOjX2) {
                return childAt;
            }
        }
        return null;
    }

    public void JyKmOjX(int i, int i2) {
        int JyKmOjX2 = YuxfPhPIaOM.JyKmOjX(i2, wSMjih.ELxjQaDRrI(this));
        if (JyKmOjX2 == 3) {
            this.DCbsRKERTF = i;
        } else if (JyKmOjX2 == 5) {
            this.nHLQZwbUt = i;
        }
        if (i != 0) {
            (JyKmOjX2 == 3 ? this.ZVIDXbvWn : this.TUFgsvaVkdN).JHCbNsJ();
        }
        switch (i) {
            case 1:
                View JyKmOjX3 = JyKmOjX(JyKmOjX2);
                if (JyKmOjX3 != null) {
                    NkBWDzI(JyKmOjX3);
                    return;
                }
                return;
            case 2:
                View JyKmOjX4 = JyKmOjX(JyKmOjX2);
                if (JyKmOjX4 != null) {
                    CGmlqExFKDsw(JyKmOjX4);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void JyKmOjX(int i, int i2, View view) {
        int i3 = 1;
        int JyKmOjX2 = this.ZVIDXbvWn.JyKmOjX();
        int JyKmOjX3 = this.TUFgsvaVkdN.JyKmOjX();
        if (!(JyKmOjX2 == 1 || JyKmOjX3 == 1)) {
            i3 = (JyKmOjX2 == 2 || JyKmOjX3 == 2) ? 2 : 0;
        }
        if (view != null && i2 == 0) {
            YuxfPhPIaOM yuxfPhPIaOM = (YuxfPhPIaOM) view.getLayoutParams();
            if (yuxfPhPIaOM.gWiOFio == 0.0f) {
                gWiOFio(view);
            } else if (yuxfPhPIaOM.gWiOFio == 1.0f) {
                ELxjQaDRrI(view);
            }
        }
        if (i3 != this.IsLoypLqxg) {
            this.IsLoypLqxg = i3;
            if (this.clabDdWhMqLI != null) {
                this.clabDdWhMqLI.gWiOFio(i3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void JyKmOjX(View view, float f) {
        if (this.clabDdWhMqLI != null) {
            this.clabDdWhMqLI.JyKmOjX(view, f);
        }
    }

    /* access modifiers changed from: package-private */
    public void JyKmOjX(boolean z) {
        int childCount = getChildCount();
        boolean z2 = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            YuxfPhPIaOM yuxfPhPIaOM = (YuxfPhPIaOM) childAt.getLayoutParams();
            if (CkytDdEwp(childAt) && (!z || yuxfPhPIaOM.ELxjQaDRrI)) {
                z2 = JyKmOjX(childAt, 3) ? z2 | this.ZVIDXbvWn.JyKmOjX(childAt, -childAt.localValue(), childAt.getTop()) : z2 | this.TUFgsvaVkdN.JyKmOjX(childAt, localValue(), childAt.getTop());
                yuxfPhPIaOM.ELxjQaDRrI = false;
            }
        }
        this.RBZUWx.JyKmOjX();
        this.ZcxyUcLBGcV.JyKmOjX();
        if (z2) {
            invalidate();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean JyKmOjX(View view, int i) {
        return (JHCbNsJ(view) & i) == i;
    }

    public void NkBWDzI(View view) {
        if (!CkytDdEwp(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.wqpspf) {
            YuxfPhPIaOM yuxfPhPIaOM = (YuxfPhPIaOM) view.getLayoutParams();
            yuxfPhPIaOM.gWiOFio = 0.0f;
            yuxfPhPIaOM.UxnFzZ = false;
        } else if (JyKmOjX(view, 3)) {
            this.ZVIDXbvWn.JyKmOjX(view, -view.localValue(), view.getTop());
        } else {
            this.TUFgsvaVkdN.JyKmOjX(view, localValue(), view.getTop());
        }
        invalidate();
    }

    public boolean OvRsHjLd(int i) {
        View JyKmOjX2 = JyKmOjX(i);
        if (JyKmOjX2 != null) {
            return ZVIDXbvWn(JyKmOjX2);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean OvRsHjLd(View view) {
        return ((YuxfPhPIaOM) view.getLayoutParams()).JyKmOjX == 0;
    }

    /* access modifiers changed from: package-private */
    public float UxnFzZ(View view) {
        return ((YuxfPhPIaOM) view.getLayoutParams()).gWiOFio;
    }

    public void UxnFzZ(int i) {
        View JyKmOjX2 = JyKmOjX(i);
        if (JyKmOjX2 == null) {
            throw new IllegalArgumentException("No drawer view found with gravity " + gWiOFio(i));
        }
        NkBWDzI(JyKmOjX2);
    }

    public boolean ZVIDXbvWn(View view) {
        if (CkytDdEwp(view)) {
            return ((YuxfPhPIaOM) view.getLayoutParams()).gWiOFio > 0.0f;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    public void addFocusables(ArrayList arrayList, int i, int i2) {
        if (getDescendantFocusability() != 393216) {
            int childCount = getChildCount();
            boolean z = false;
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = getChildAt(i3);
                if (!CkytDdEwp(childAt)) {
                    this.PvSPypUwEGN.add(childAt);
                } else if (uvKYrIFVv(childAt)) {
                    z = true;
                    childAt.addFocusables(arrayList, i, i2);
                }
            }
            if (!z) {
                int size = this.PvSPypUwEGN.size();
                for (int i4 = 0; i4 < size; i4++) {
                    View view = (View) this.PvSPypUwEGN.get(i4);
                    if (view.getAlphaVisibility() == 0) {
                        view.addFocusables(arrayList, i, i2);
                    }
                }
            }
            this.PvSPypUwEGN.clear();
        }
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
        if (JyKmOjX() != null || CkytDdEwp(view)) {
            wSMjih.JyKmOjX(view, 4);
        } else {
            wSMjih.JyKmOjX(view, 1);
        }
        if (!ELxjQaDRrI) {
            wSMjih.JyKmOjX(view, this.JHCbNsJ);
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof YuxfPhPIaOM) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        int childCount = getChildCount();
        float f = 0.0f;
        for (int i = 0; i < childCount; i++) {
            f = Math.gratest(f, ((YuxfPhPIaOM) getChildAt(i).getLayoutParams()).gWiOFio);
        }
        this.NkBWDzI = f;
        if (this.ZVIDXbvWn.JyKmOjX(true) || this.TUFgsvaVkdN.JyKmOjX(true)) {
            wSMjih.JyKmOjX(this);
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        int i;
        int height = getHeight();
        boolean OvRsHjLd2 = OvRsHjLd(view);
        int i2 = 0;
        int localValue = localValue();
        int save = canvas.save();
        if (OvRsHjLd2) {
            int childCount = getChildCount();
            int i3 = 0;
            while (i3 < childCount) {
                View childAt = getChildAt(i3);
                if (childAt != view && childAt.getAlphaVisibility() == 0 && TUFgsvaVkdN(childAt) && CkytDdEwp(childAt)) {
                    if (childAt.getHeight() < height) {
                        i = localValue;
                    } else if (JyKmOjX(childAt, 3)) {
                        int right = childAt.getRight();
                        if (right <= i2) {
                            right = i2;
                        }
                        i2 = right;
                        i = localValue;
                    } else {
                        i = childAt.getSpacedDirs();
                        if (i < localValue) {
                        }
                    }
                    i3++;
                    localValue = i;
                }
                i = localValue;
                i3++;
                localValue = i;
            }
            canvas.clipRect(i2, 0, localValue, getHeight());
        }
        int i4 = localValue;
        boolean drawChild = super.drawChild(canvas, view, j);
        canvas.restoreToCount(save);
        if (this.NkBWDzI > 0.0f && OvRsHjLd2) {
            this.uvKYrIFVv.setColor((((int) (((float) ((this.CGmlqExFKDsw & -16777216) >>> 24)) * this.NkBWDzI)) << 24) | (this.CGmlqExFKDsw & 16777215));
            canvas.drawRect((float) i2, 0.0f, (float) i4, (float) getHeight(), this.uvKYrIFVv);
        } else if (this.udYZxYWU != null && JyKmOjX(view, 3)) {
            int outerSize = this.udYZxYWU.outerSize();
            int right2 = view.getRight();
            float gratest = Math.gratest(0.0f, Math.movedPlace(((float) right2) / ((float) this.ZVIDXbvWn.gWiOFio()), 1.0f));
            this.udYZxYWU.setBounds(right2, view.getTop(), outerSize + right2, view.getBottom());
            this.udYZxYWU.setAlpha((int) (255.0f * gratest));
            this.udYZxYWU.draw(canvas);
        } else if (this.wzLBqLQcFlu != null && JyKmOjX(view, 5)) {
            int outerSize2 = this.wzLBqLQcFlu.outerSize();
            int spacedDirs = view.getSpacedDirs();
            float gratest2 = Math.gratest(0.0f, Math.movedPlace(((float) (localValue() - spacedDirs)) / ((float) this.TUFgsvaVkdN.gWiOFio()), 1.0f));
            this.wzLBqLQcFlu.setBounds(spacedDirs - outerSize2, view.getTop(), spacedDirs, view.getBottom());
            this.wzLBqLQcFlu.setAlpha((int) (255.0f * gratest2));
            this.wzLBqLQcFlu.draw(canvas);
        }
        return drawChild;
    }

    public void gWiOFio() {
        JyKmOjX(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.graphics.drawable.Drawable, int):boolean
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(int, int):void
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.view.View, float):void
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.view.View, int):boolean
      android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void gWiOFio(View view) {
        View rootView;
        YuxfPhPIaOM yuxfPhPIaOM = (YuxfPhPIaOM) view.getLayoutParams();
        if (yuxfPhPIaOM.UxnFzZ) {
            yuxfPhPIaOM.UxnFzZ = false;
            if (this.clabDdWhMqLI != null) {
                this.clabDdWhMqLI.gWiOFio(view);
            }
            JyKmOjX(view, false);
            if (hasWindowFocus() && (rootView = getRootView()) != null) {
                rootView.sendAccessibilityEvent(32);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void gWiOFio(View view, float f) {
        YuxfPhPIaOM yuxfPhPIaOM = (YuxfPhPIaOM) view.getLayoutParams();
        if (f != yuxfPhPIaOM.gWiOFio) {
            yuxfPhPIaOM.gWiOFio = f;
            JyKmOjX(view, f);
        }
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new YuxfPhPIaOM(-1, -1);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new YuxfPhPIaOM(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof YuxfPhPIaOM ? new YuxfPhPIaOM((YuxfPhPIaOM) layoutParams) : layoutParams instanceof ViewGroup.MarginLayoutParams ? new YuxfPhPIaOM((ViewGroup.MarginLayoutParams) layoutParams) : new YuxfPhPIaOM(layoutParams);
    }

    public float getDrawerElevation() {
        if (UxnFzZ) {
            return this.OvRsHjLd;
        }
        return 0.0f;
    }

    public Drawable getStatusBarBackgroundDrawable() {
        return this.ADojbWgjJep;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.wqpspf = true;
    }

    public void onDraw(Canvas canvas) {
        int JyKmOjX2;
        super.onDraw(canvas);
        if (this.ejoYRLl && this.ADojbWgjJep != null && (JyKmOjX2 = JyKmOjX.JyKmOjX(this.RFOcVKwijOVg)) > 0) {
            this.ADojbWgjJep.setBounds(0, 0, localValue(), JyKmOjX2);
            this.ADojbWgjJep.draw(canvas);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View UxnFzZ2;
        int JyKmOjX2 = anLHcLdRskb.JyKmOjX(motionEvent);
        boolean JyKmOjX3 = this.ZVIDXbvWn.JyKmOjX(motionEvent) | this.TUFgsvaVkdN.JyKmOjX(motionEvent);
        switch (JyKmOjX2) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.dQCQlqcL = x;
                this.SYafeh = y;
                z = this.NkBWDzI > 0.0f && (UxnFzZ2 = this.ZVIDXbvWn.UxnFzZ((int) x, (int) y)) != null && OvRsHjLd(UxnFzZ2);
                this.gnKlzvCZLmUA = false;
                this.ineUXGyuQmP = false;
                break;
            case 1:
            case 3:
                JyKmOjX(true);
                this.gnKlzvCZLmUA = false;
                this.ineUXGyuQmP = false;
                z = false;
                break;
            case 2:
                if (this.ZVIDXbvWn.ELxjQaDRrI(3)) {
                    this.RBZUWx.JyKmOjX();
                    this.ZcxyUcLBGcV.JyKmOjX();
                    z = false;
                    break;
                }
                z = false;
                break;
            default:
                z = false;
                break;
        }
        return JyKmOjX3 || z || CkytDdEwp() || this.ineUXGyuQmP;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || !CGmlqExFKDsw()) {
            return super.onKeyDown(i, keyEvent);
        }
        GJShOWhM.JyKmOjX(keyEvent);
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyUp(i, keyEvent);
        }
        View NkBWDzI2 = NkBWDzI();
        if (NkBWDzI2 != null && JyKmOjX(NkBWDzI2) == 0) {
            gWiOFio();
        }
        return NkBWDzI2 != null;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        float f;
        this.DYldmviR = true;
        int i6 = i3 - i;
        int childCount = getChildCount();
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getAlphaVisibility() != 8) {
                YuxfPhPIaOM yuxfPhPIaOM = (YuxfPhPIaOM) childAt.getLayoutParams();
                if (OvRsHjLd(childAt)) {
                    childAt.layout(yuxfPhPIaOM.leftMargin, yuxfPhPIaOM.topMargin, yuxfPhPIaOM.leftMargin + childAt.getMeasuredWidth(), yuxfPhPIaOM.topMargin + childAt.getMeasuredHeight());
                } else {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (JyKmOjX(childAt, 3)) {
                        i5 = ((int) (((float) measuredWidth) * yuxfPhPIaOM.gWiOFio)) + (-measuredWidth);
                        f = ((float) (measuredWidth + i5)) / ((float) measuredWidth);
                    } else {
                        i5 = i6 - ((int) (((float) measuredWidth) * yuxfPhPIaOM.gWiOFio));
                        f = ((float) (i6 - i5)) / ((float) measuredWidth);
                    }
                    boolean z2 = f != yuxfPhPIaOM.gWiOFio;
                    switch (yuxfPhPIaOM.JyKmOjX & 112) {
                        case woewfJyZrgET.uvKYrIFVv:
                            int i8 = i4 - i2;
                            int i9 = (i8 - measuredHeight) / 2;
                            if (i9 < yuxfPhPIaOM.topMargin) {
                                i9 = yuxfPhPIaOM.topMargin;
                            } else if (i9 + measuredHeight > i8 - yuxfPhPIaOM.bottomMargin) {
                                i9 = (i8 - yuxfPhPIaOM.bottomMargin) - measuredHeight;
                            }
                            childAt.layout(i5, i9, measuredWidth + i5, measuredHeight + i9);
                            break;
                        case 80:
                            int i10 = i4 - i2;
                            childAt.layout(i5, (i10 - yuxfPhPIaOM.bottomMargin) - childAt.getMeasuredHeight(), measuredWidth + i5, i10 - yuxfPhPIaOM.bottomMargin);
                            break;
                        default:
                            childAt.layout(i5, yuxfPhPIaOM.topMargin, measuredWidth + i5, measuredHeight + yuxfPhPIaOM.topMargin);
                            break;
                    }
                    if (z2) {
                        gWiOFio(childAt, f);
                    }
                    int i11 = yuxfPhPIaOM.gWiOFio > 0.0f ? 0 : 4;
                    if (childAt.getAlphaVisibility() != i11) {
                        childAt.setVisibility(i11);
                    }
                }
            }
        }
        this.DYldmviR = false;
        this.wqpspf = false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0054, code lost:
        if (r5 != 0) goto L_0x0056;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r1 = 300(0x12c, float:4.2E-43)
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            r12 = 1073741824(0x40000000, float:2.0)
            int r3 = android.view.View.MeasureSpec.getMode(r14)
            int r5 = android.view.View.MeasureSpec.getMode(r15)
            int r2 = android.view.View.MeasureSpec.getSize(r14)
            int r0 = android.view.View.MeasureSpec.getSize(r15)
            if (r3 != r12) goto L_0x001b
            if (r5 == r12) goto L_0x0056
        L_0x001b:
            boolean r6 = r13.isInEditMode()
            if (r6 == 0) goto L_0x0058
            if (r3 != r7) goto L_0x0050
        L_0x0023:
            if (r5 != r7) goto L_0x0054
            r1 = r0
        L_0x0026:
            r13.setMeasuredDimension(r2, r1)
            java.lang.Object r0 = r13.RFOcVKwijOVg
            if (r0 == 0) goto L_0x0060
            boolean r0 = android.support.ekglxtiyel.lzcewhsnchqg.wSMjih.JHCbNsJ(r13)
            if (r0 == 0) goto L_0x0060
            r0 = 1
            r3 = r0
        L_0x0035:
            int r6 = android.support.ekglxtiyel.lzcewhsnchqg.wSMjih.ELxjQaDRrI(r13)
            int r7 = r13.getChildCount()
            r5 = r4
        L_0x003e:
            if (r5 >= r7) goto L_0x014b
            android.view.View r8 = r13.getChildAt(r5)
            int r0 = r8.getAlphaVisibility()
            r9 = 8
            if (r0 != r9) goto L_0x0062
        L_0x004c:
            int r0 = r5 + 1
            r5 = r0
            goto L_0x003e
        L_0x0050:
            if (r3 != 0) goto L_0x0023
            r2 = r1
            goto L_0x0023
        L_0x0054:
            if (r5 == 0) goto L_0x0026
        L_0x0056:
            r1 = r0
            goto L_0x0026
        L_0x0058:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "DrawerLayout must be measured with MeasureSpec.EXACTLY."
            r0.<init>(r1)
            throw r0
        L_0x0060:
            r3 = r4
            goto L_0x0035
        L_0x0062:
            android.view.ViewGroup$LayoutParams r0 = r8.getLayoutParams()
            android.support.ekglxtiyel.mkulma.YuxfPhPIaOM r0 = (android.support.ekglxtiyel.mkulma.YuxfPhPIaOM) r0
            if (r3 == 0) goto L_0x007d
            int r9 = r0.JyKmOjX
            int r9 = android.support.ekglxtiyel.lzcewhsnchqg.YuxfPhPIaOM.JyKmOjX(r9, r6)
            boolean r10 = android.support.ekglxtiyel.lzcewhsnchqg.wSMjih.JHCbNsJ(r8)
            if (r10 == 0) goto L_0x009e
            android.support.ekglxtiyel.mkulma.xaEMqBpLOv r10 = android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX
            java.lang.Object r11 = r13.RFOcVKwijOVg
            r10.JyKmOjX(r8, r11, r9)
        L_0x007d:
            boolean r9 = r13.OvRsHjLd(r8)
            if (r9 == 0) goto L_0x00a6
            int r9 = r0.leftMargin
            int r9 = r2 - r9
            int r10 = r0.rightMargin
            int r9 = r9 - r10
            int r9 = android.view.View.MeasureSpec.makeMeasureSpec(r9, r12)
            int r10 = r0.topMargin
            int r10 = r1 - r10
            int r0 = r0.bottomMargin
            int r0 = r10 - r0
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r12)
            r8.measure(r9, r0)
            goto L_0x004c
        L_0x009e:
            android.support.ekglxtiyel.mkulma.xaEMqBpLOv r10 = android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.JyKmOjX
            java.lang.Object r11 = r13.RFOcVKwijOVg
            r10.JyKmOjX(r0, r11, r9)
            goto L_0x007d
        L_0x00a6:
            boolean r9 = r13.CkytDdEwp(r8)
            if (r9 == 0) goto L_0x011c
            boolean r9 = android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.UxnFzZ
            if (r9 == 0) goto L_0x00bf
            float r9 = android.support.ekglxtiyel.lzcewhsnchqg.wSMjih.UxnFzZ(r8)
            float r10 = r13.OvRsHjLd
            int r9 = (r9 > r10 ? 1 : (r9 == r10 ? 0 : -1))
            if (r9 == 0) goto L_0x00bf
            float r9 = r13.OvRsHjLd
            android.support.ekglxtiyel.lzcewhsnchqg.wSMjih.JyKmOjX(r8, r9)
        L_0x00bf:
            int r9 = r13.JHCbNsJ(r8)
            r9 = r9 & 7
            r10 = r4 & r9
            if (r10 == 0) goto L_0x00fe
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Child drawer has absolute gravity "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = gWiOFio(r9)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " but this "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "DrawerLayout"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " already has a "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "drawer view along that edge"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00fe:
            int r9 = r13.CkytDdEwp
            int r10 = r0.leftMargin
            int r9 = r9 + r10
            int r10 = r0.rightMargin
            int r9 = r9 + r10
            int r10 = r0.width
            int r9 = getChildMeasureSpec(r14, r9, r10)
            int r10 = r0.topMargin
            int r11 = r0.bottomMargin
            int r10 = r10 + r11
            int r0 = r0.height
            int r0 = getChildMeasureSpec(r15, r10, r0)
            r8.measure(r9, r0)
            goto L_0x004c
        L_0x011c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Child "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r2 = " at index "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = " does not have a valid layout_gravity - must be Gravity.LEFT, "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "Gravity.RIGHT or Gravity.NO_GRAVITY"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x014b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.ekglxtiyel.mkulma.lFKFPcMiJsI.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        View JyKmOjX2;
        NIUxNi nIUxNi = (NIUxNi) parcelable;
        super.onRestoreInstanceState(nIUxNi.getSuperState());
        if (!(nIUxNi.JyKmOjX == 0 || (JyKmOjX2 = JyKmOjX(nIUxNi.JyKmOjX)) == null)) {
            CGmlqExFKDsw(JyKmOjX2);
        }
        JyKmOjX(nIUxNi.gWiOFio, 3);
        JyKmOjX(nIUxNi.ELxjQaDRrI, 5);
    }

    public void onRtlPropertiesChanged(int i) {
        UxnFzZ();
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        NIUxNi nIUxNi = new NIUxNi(super.onSaveInstanceState());
        View JyKmOjX2 = JyKmOjX();
        if (JyKmOjX2 != null) {
            nIUxNi.JyKmOjX = ((YuxfPhPIaOM) JyKmOjX2.getLayoutParams()).JyKmOjX;
        }
        nIUxNi.gWiOFio = this.DCbsRKERTF;
        nIUxNi.ELxjQaDRrI = this.nHLQZwbUt;
        return nIUxNi;
    }

    /* access modifiers changed from: protected */
    public void onShowWindow() {
        super.onShowWindow();
        this.wqpspf = true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View JyKmOjX2;
        this.ZVIDXbvWn.gWiOFio(motionEvent);
        this.TUFgsvaVkdN.gWiOFio(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.dQCQlqcL = x;
                this.SYafeh = y;
                this.gnKlzvCZLmUA = false;
                this.ineUXGyuQmP = false;
                break;
            case 1:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                View UxnFzZ2 = this.ZVIDXbvWn.UxnFzZ((int) x2, (int) y2);
                if (UxnFzZ2 != null && OvRsHjLd(UxnFzZ2)) {
                    float f = x2 - this.dQCQlqcL;
                    float f2 = y2 - this.SYafeh;
                    int UxnFzZ3 = this.ZVIDXbvWn.UxnFzZ();
                    if ((f * f) + (f2 * f2) < ((float) (UxnFzZ3 * UxnFzZ3)) && (JyKmOjX2 = JyKmOjX()) != null) {
                        z = JyKmOjX(JyKmOjX2) == 2;
                        JyKmOjX(z);
                        this.gnKlzvCZLmUA = false;
                        break;
                    }
                }
                z = true;
                JyKmOjX(z);
                this.gnKlzvCZLmUA = false;
            case 3:
                JyKmOjX(true);
                this.gnKlzvCZLmUA = false;
                this.ineUXGyuQmP = false;
                break;
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        this.gnKlzvCZLmUA = z;
        if (z) {
            JyKmOjX(true);
        }
    }

    public void requestLayout() {
        if (!this.DYldmviR) {
            super.requestLayout();
        }
    }

    public void setDrawerElevation(float f) {
        this.OvRsHjLd = f;
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (CkytDdEwp(childAt)) {
                wSMjih.JyKmOjX(childAt, this.OvRsHjLd);
            }
        }
    }

    public void setDrawerListener(MNSntxfeN mNSntxfeN) {
        this.clabDdWhMqLI = mNSntxfeN;
    }

    public void setDrawerLockMode(int i) {
        JyKmOjX(i, 3);
        JyKmOjX(i, 5);
    }

    public void setScrimColor(int i) {
        this.CGmlqExFKDsw = i;
        invalidate();
    }

    public void setStatusBarBackground(int i) {
        this.ADojbWgjJep = i != 0 ? android.support.ekglxtiyel.ndmoxloia.lFKFPcMiJsI.JyKmOjX(getContext(), i) : null;
        invalidate();
    }

    public void setStatusBarBackground(Drawable drawable) {
        this.ADojbWgjJep = drawable;
        invalidate();
    }

    public void setStatusBarBackgroundColor(int i) {
        this.ADojbWgjJep = new ColorDrawable(i);
        invalidate();
    }

    public boolean uvKYrIFVv(View view) {
        if (CkytDdEwp(view)) {
            return ((YuxfPhPIaOM) view.getLayoutParams()).UxnFzZ;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }
}
