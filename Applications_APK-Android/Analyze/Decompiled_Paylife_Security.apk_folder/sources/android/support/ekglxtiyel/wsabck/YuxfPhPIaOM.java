package android.support.ekglxtiyel.wsabck;

import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

class YuxfPhPIaOM {
    private static Method JyKmOjX;
    private static Method gWiOFio;

    static {
        try {
            Class<?> cls = Class.forName("libcore.icu.ICU");
            if (cls != null) {
                JyKmOjX = cls.getMethod("getScript", new Class[]{String.class});
                gWiOFio = cls.getMethod("insertSubtags", new Class[]{String.class});
            }
        } catch (Exception e) {
            JyKmOjX = null;
            gWiOFio = null;
            Log.w("ICUCompatIcs", e);
        }
    }

    private static String JyKmOjX(String str) {
        try {
            if (JyKmOjX != null) {
                return (String) JyKmOjX.invoke(null, str);
            }
        } catch (IllegalAccessException e) {
            Log.w("ICUCompatIcs", e);
        } catch (InvocationTargetException e2) {
            Log.w("ICUCompatIcs", e2);
        }
        return null;
    }

    public static String JyKmOjX(Locale locale) {
        String gWiOFio2 = gWiOFio(locale);
        if (gWiOFio2 != null) {
            return JyKmOjX(gWiOFio2);
        }
        return null;
    }

    private static String gWiOFio(Locale locale) {
        String locale2 = locale.toString();
        try {
            if (gWiOFio != null) {
                return (String) gWiOFio.invoke(null, locale2);
            }
        } catch (IllegalAccessException e) {
            Log.w("ICUCompatIcs", e);
        } catch (InvocationTargetException e2) {
            Log.w("ICUCompatIcs", e2);
        }
        return locale2;
    }
}
