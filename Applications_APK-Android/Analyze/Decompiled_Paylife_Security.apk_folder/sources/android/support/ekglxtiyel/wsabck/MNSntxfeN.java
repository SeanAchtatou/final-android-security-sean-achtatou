package android.support.ekglxtiyel.wsabck;

import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

public class MNSntxfeN {
    private static Method JyKmOjX;

    static {
        try {
            JyKmOjX = Class.forName("libcore.icu.ICU").getMethod("insertSubtags", new Class[]{Locale.class});
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public static String JyKmOjX(Locale locale) {
        try {
            return ((Locale) JyKmOjX.invoke(null, locale)).getScript();
        } catch (InvocationTargetException e) {
            Log.w("ICUCompatIcs", e);
        } catch (IllegalAccessException e2) {
            Log.w("ICUCompatIcs", e2);
        }
        return locale.getScript();
    }
}
