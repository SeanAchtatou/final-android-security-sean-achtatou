package android.support.ekglxtiyel.ndmoxloia;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Process;
import android.util.Log;
import java.io.File;

public class lFKFPcMiJsI {
    private static final String ELxjQaDRrI = "data";
    private static final String JHCbNsJ = "files";
    private static final String JyKmOjX = "ContextCompat";
    private static final String OvRsHjLd = "cache";
    private static final String UxnFzZ = "obb";
    private static final String gWiOFio = "Android";

    public static final int ELxjQaDRrI(Context context, int i) {
        return Build.VERSION.SDK_INT >= 23 ? xaEMqBpLOv.gWiOFio(context, i) : context.getResources().getColor(i);
    }

    public static final Drawable JyKmOjX(Context context, int i) {
        return Build.VERSION.SDK_INT >= 21 ? hfpzXtVZzF.JyKmOjX(context, i) : context.getResources().getDrawable(i);
    }

    private static synchronized File JyKmOjX(File file) {
        synchronized (lFKFPcMiJsI.class) {
            if (!file.exists() && !file.mkdirs() && !file.exists()) {
                Log.w(JyKmOjX, "Unable to create files subdir " + file.getComponentPath());
                file = null;
            }
        }
        return file;
    }

    private static File JyKmOjX(File file, String... strArr) {
        int length = strArr.length;
        int i = 0;
        File file2 = file;
        while (i < length) {
            String str = strArr[i];
            i++;
            file2 = file2 == null ? new File(str) : str != null ? new File(file2, str) : file2;
        }
        return file2;
    }

    public static boolean JyKmOjX(Context context, Intent[] intentArr) {
        return JyKmOjX(context, intentArr, null);
    }

    public static boolean JyKmOjX(Context context, Intent[] intentArr, Bundle bundle) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 16) {
            MNSntxfeN.JyKmOjX(context, intentArr, bundle);
            return true;
        } else if (i < 11) {
            return false;
        } else {
            axKVDobCEa.JyKmOjX(context, intentArr);
            return true;
        }
    }

    public static File[] JyKmOjX(Context context) {
        File JyKmOjX2;
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            return YuxfPhPIaOM.gWiOFio(context);
        }
        if (i >= 11) {
            JyKmOjX2 = axKVDobCEa.JyKmOjX(context);
        } else {
            JyKmOjX2 = JyKmOjX(Environment.getExternalStorageDirectory(), gWiOFio, UxnFzZ, context.getPackageName());
        }
        return new File[]{JyKmOjX2};
    }

    public static File[] JyKmOjX(Context context, String str) {
        File JyKmOjX2;
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            return YuxfPhPIaOM.JyKmOjX(context, str);
        }
        if (i >= 8) {
            JyKmOjX2 = mCAnCvCLz.JyKmOjX(context, str);
        } else {
            JyKmOjX2 = JyKmOjX(Environment.getExternalStorageDirectory(), gWiOFio, ELxjQaDRrI, context.getPackageName(), JHCbNsJ, str);
        }
        return new File[]{JyKmOjX2};
    }

    public static int gWiOFio(Context context, String str) {
        if (str != null) {
            return context.checkPermission(str, Process.myPid(), Process.myUid());
        }
        throw new IllegalArgumentException("permission is null");
    }

    public static final ColorStateList gWiOFio(Context context, int i) {
        return Build.VERSION.SDK_INT >= 23 ? xaEMqBpLOv.JyKmOjX(context, i) : context.getResources().getColorStateList(i);
    }

    public static File[] gWiOFio(Context context) {
        File JyKmOjX2;
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            return YuxfPhPIaOM.JyKmOjX(context);
        }
        if (i >= 8) {
            JyKmOjX2 = mCAnCvCLz.JyKmOjX(context);
        } else {
            JyKmOjX2 = JyKmOjX(Environment.getExternalStorageDirectory(), gWiOFio, ELxjQaDRrI, context.getPackageName(), OvRsHjLd);
        }
        return new File[]{JyKmOjX2};
    }

    public final File ELxjQaDRrI(Context context) {
        return Build.VERSION.SDK_INT >= 21 ? hfpzXtVZzF.JyKmOjX(context) : JyKmOjX(new File(context.getScaleLabel().dataDir, "no_backup"));
    }

    public final File UxnFzZ(Context context) {
        return Build.VERSION.SDK_INT >= 21 ? hfpzXtVZzF.gWiOFio(context) : JyKmOjX(new File(context.getScaleLabel().dataDir, "destruct_items"));
    }
}
