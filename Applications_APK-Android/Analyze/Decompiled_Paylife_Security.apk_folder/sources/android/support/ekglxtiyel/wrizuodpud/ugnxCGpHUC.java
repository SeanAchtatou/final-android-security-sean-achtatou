package android.support.ekglxtiyel.wrizuodpud;

import java.util.Iterator;
import java.util.Map;

final class ugnxCGpHUC implements Iterator, Map.Entry {
    boolean ELxjQaDRrI = false;
    int JyKmOjX;
    final /* synthetic */ wiuPiJhywn UxnFzZ;
    int gWiOFio;

    ugnxCGpHUC(wiuPiJhywn wiupijhywn) {
        this.UxnFzZ = wiupijhywn;
        this.JyKmOjX = wiupijhywn.JyKmOjX() - 1;
        this.gWiOFio = -1;
    }

    /* renamed from: JyKmOjX */
    public Map.Entry next() {
        this.gWiOFio++;
        this.ELxjQaDRrI = true;
        return this;
    }

    public void clear() {
        if (!this.ELxjQaDRrI) {
            throw new IllegalStateException();
        }
        this.UxnFzZ.JyKmOjX(this.gWiOFio);
        this.gWiOFio--;
        this.JyKmOjX--;
        this.ELxjQaDRrI = false;
    }

    public final boolean equals(Object obj) {
        boolean z = true;
        if (!this.ELxjQaDRrI) {
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        } else if (!(obj instanceof Map.Entry)) {
            return false;
        } else {
            Map.Entry entry = (Map.Entry) obj;
            if (!MNSntxfeN.JyKmOjX(entry.getKey(), this.UxnFzZ.JyKmOjX(this.gWiOFio, 0)) || !MNSntxfeN.JyKmOjX(entry.getValue(), this.UxnFzZ.JyKmOjX(this.gWiOFio, 1))) {
                z = false;
            }
            return z;
        }
    }

    public Object getKey() {
        if (this.ELxjQaDRrI) {
            return this.UxnFzZ.JyKmOjX(this.gWiOFio, 0);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public Object getValue() {
        if (this.ELxjQaDRrI) {
            return this.UxnFzZ.JyKmOjX(this.gWiOFio, 1);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public boolean hasNext() {
        return this.gWiOFio < this.JyKmOjX;
    }

    public final int hashCode() {
        int i = 0;
        if (!this.ELxjQaDRrI) {
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }
        Object JyKmOjX2 = this.UxnFzZ.JyKmOjX(this.gWiOFio, 0);
        Object JyKmOjX3 = this.UxnFzZ.JyKmOjX(this.gWiOFio, 1);
        int hashCode = JyKmOjX2 == null ? 0 : JyKmOjX2.hashCode();
        if (JyKmOjX3 != null) {
            i = JyKmOjX3.hashCode();
        }
        return i ^ hashCode;
    }

    public Object setValue(Object obj) {
        if (this.ELxjQaDRrI) {
            return this.UxnFzZ.JyKmOjX(this.gWiOFio, obj);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public final String toString() {
        return getKey() + "=" + getValue();
    }
}
