package android.support.ekglxtiyel.wrizuodpud;

public final class axKVDobCEa {
    private int ELxjQaDRrI;
    private int[] JyKmOjX;
    private int UxnFzZ;
    private int gWiOFio;

    public axKVDobCEa() {
        this(8);
    }

    public axKVDobCEa(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException("capacity must be positive");
        }
        i = Integer.bitCount(i) != 1 ? 1 << (Integer.highestOneBit(i) + 1) : i;
        this.UxnFzZ = i - 1;
        this.JyKmOjX = new int[i];
    }

    private void CGmlqExFKDsw() {
        int length = this.JyKmOjX.length;
        int i = length - this.gWiOFio;
        int i2 = length << 1;
        if (i2 < 0) {
            throw new RuntimeException("Max array capacity exceeded");
        }
        int[] iArr = new int[i2];
        System.arraycopy(this.JyKmOjX, this.gWiOFio, iArr, 0, i);
        System.arraycopy(this.JyKmOjX, 0, iArr, i, this.gWiOFio);
        this.JyKmOjX = iArr;
        this.gWiOFio = 0;
        this.ELxjQaDRrI = length;
        this.UxnFzZ = i2 - 1;
    }

    public boolean CkytDdEwp() {
        return this.gWiOFio == this.ELxjQaDRrI;
    }

    public void ELxjQaDRrI() {
        this.ELxjQaDRrI = this.gWiOFio;
    }

    public void ELxjQaDRrI(int i) {
        if (i > 0) {
            if (i > OvRsHjLd()) {
                throw new ArrayIndexOutOfBoundsException();
            }
            this.gWiOFio = (this.gWiOFio + i) & this.UxnFzZ;
        }
    }

    public int JHCbNsJ() {
        if (this.gWiOFio != this.ELxjQaDRrI) {
            return this.JyKmOjX[(this.ELxjQaDRrI - 1) & this.UxnFzZ];
        }
        throw new ArrayIndexOutOfBoundsException();
    }

    public int JHCbNsJ(int i) {
        if (i >= 0 && i < OvRsHjLd()) {
            return this.JyKmOjX[(this.gWiOFio + i) & this.UxnFzZ];
        }
        throw new ArrayIndexOutOfBoundsException();
    }

    public int JyKmOjX() {
        if (this.gWiOFio == this.ELxjQaDRrI) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int i = this.JyKmOjX[this.gWiOFio];
        this.gWiOFio = (this.gWiOFio + 1) & this.UxnFzZ;
        return i;
    }

    public void JyKmOjX(int i) {
        this.gWiOFio = (this.gWiOFio - 1) & this.UxnFzZ;
        this.JyKmOjX[this.gWiOFio] = i;
        if (this.gWiOFio == this.ELxjQaDRrI) {
            CGmlqExFKDsw();
        }
    }

    public int OvRsHjLd() {
        return (this.ELxjQaDRrI - this.gWiOFio) & this.UxnFzZ;
    }

    public int UxnFzZ() {
        if (this.gWiOFio != this.ELxjQaDRrI) {
            return this.JyKmOjX[this.gWiOFio];
        }
        throw new ArrayIndexOutOfBoundsException();
    }

    public void UxnFzZ(int i) {
        if (i > 0) {
            if (i > OvRsHjLd()) {
                throw new ArrayIndexOutOfBoundsException();
            }
            this.ELxjQaDRrI = (this.ELxjQaDRrI - i) & this.UxnFzZ;
        }
    }

    public int gWiOFio() {
        if (this.gWiOFio == this.ELxjQaDRrI) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int i = (this.ELxjQaDRrI - 1) & this.UxnFzZ;
        int i2 = this.JyKmOjX[i];
        this.ELxjQaDRrI = i;
        return i2;
    }

    public void gWiOFio(int i) {
        this.JyKmOjX[this.ELxjQaDRrI] = i;
        this.ELxjQaDRrI = (this.ELxjQaDRrI + 1) & this.UxnFzZ;
        if (this.ELxjQaDRrI == this.gWiOFio) {
            CGmlqExFKDsw();
        }
    }
}
