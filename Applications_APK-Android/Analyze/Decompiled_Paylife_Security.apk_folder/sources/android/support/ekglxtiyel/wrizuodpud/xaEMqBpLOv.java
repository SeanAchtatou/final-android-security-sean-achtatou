package android.support.ekglxtiyel.wrizuodpud;

import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class xaEMqBpLOv {
    private final File JyKmOjX;
    private final File gWiOFio;

    public xaEMqBpLOv(File file) {
        this.JyKmOjX = file;
        this.gWiOFio = new File(file.getComponentPath() + ".bak");
    }

    static boolean ELxjQaDRrI(FileOutputStream fileOutputStream) {
        if (fileOutputStream != null) {
            try {
                fileOutputStream.getFD().sync();
            } catch (IOException e) {
                return false;
            }
        }
        return true;
    }

    public FileOutputStream ELxjQaDRrI() {
        if (this.JyKmOjX.exists()) {
            if (this.gWiOFio.exists()) {
                this.JyKmOjX.delete();
            } else if (!this.JyKmOjX.renameTo(this.gWiOFio)) {
                Log.w("AtomicFile", "Couldn't rename file " + this.JyKmOjX + " to backup file " + this.gWiOFio);
            }
        }
        try {
            return new FileOutputStream(this.JyKmOjX);
        } catch (FileNotFoundException e) {
            if (!this.JyKmOjX.getParentFile().mkdir()) {
                throw new IOException("Couldn't create directory " + this.JyKmOjX);
            }
            try {
                return new FileOutputStream(this.JyKmOjX);
            } catch (FileNotFoundException e2) {
                throw new IOException("Couldn't create " + this.JyKmOjX);
            }
        }
    }

    public byte[] JHCbNsJ() {
        byte[] bArr;
        int i = 0;
        FileInputStream UxnFzZ = UxnFzZ();
        try {
            byte[] bArr2 = new byte[UxnFzZ.available()];
            while (true) {
                int read = UxnFzZ.read(bArr2, i, bArr2.length - i);
                if (read <= 0) {
                    return bArr2;
                }
                int i2 = read + i;
                int available = UxnFzZ.available();
                if (available > bArr2.length - i2) {
                    bArr = new byte[(available + i2)];
                    System.arraycopy(bArr2, 0, bArr, 0, i2);
                } else {
                    bArr = bArr2;
                }
                bArr2 = bArr;
                i = i2;
            }
        } finally {
            UxnFzZ.close();
        }
    }

    public File JyKmOjX() {
        return this.JyKmOjX;
    }

    public void JyKmOjX(FileOutputStream fileOutputStream) {
        if (fileOutputStream != null) {
            ELxjQaDRrI(fileOutputStream);
            try {
                fileOutputStream.close();
                this.gWiOFio.delete();
            } catch (IOException e) {
                Log.w("AtomicFile", "finishWrite: Got exception:", e);
            }
        }
    }

    public FileInputStream UxnFzZ() {
        if (this.gWiOFio.exists()) {
            this.JyKmOjX.delete();
            this.gWiOFio.renameTo(this.JyKmOjX);
        }
        return new FileInputStream(this.JyKmOjX);
    }

    public void gWiOFio() {
        this.JyKmOjX.delete();
        this.gWiOFio.delete();
    }

    public void gWiOFio(FileOutputStream fileOutputStream) {
        if (fileOutputStream != null) {
            ELxjQaDRrI(fileOutputStream);
            try {
                fileOutputStream.close();
                this.JyKmOjX.delete();
                this.gWiOFio.renameTo(this.JyKmOjX);
            } catch (IOException e) {
                Log.w("AtomicFile", "failWrite: Got exception:", e);
            }
        }
    }
}
