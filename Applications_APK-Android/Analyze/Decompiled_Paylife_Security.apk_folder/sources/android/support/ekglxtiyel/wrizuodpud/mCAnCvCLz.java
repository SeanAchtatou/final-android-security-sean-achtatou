package android.support.ekglxtiyel.wrizuodpud;

public final class mCAnCvCLz {
    private int ELxjQaDRrI;
    private Object[] JyKmOjX;
    private int UxnFzZ;
    private int gWiOFio;

    public mCAnCvCLz() {
        this(8);
    }

    public mCAnCvCLz(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException("capacity must be positive");
        }
        i = Integer.bitCount(i) != 1 ? 1 << (Integer.highestOneBit(i) + 1) : i;
        this.UxnFzZ = i - 1;
        this.JyKmOjX = new Object[i];
    }

    private void CGmlqExFKDsw() {
        int length = this.JyKmOjX.length;
        int i = length - this.gWiOFio;
        int i2 = length << 1;
        if (i2 < 0) {
            throw new RuntimeException("Max array capacity exceeded");
        }
        Object[] objArr = new Object[i2];
        System.arraycopy(this.JyKmOjX, this.gWiOFio, objArr, 0, i);
        System.arraycopy(this.JyKmOjX, 0, objArr, i, this.gWiOFio);
        this.JyKmOjX = objArr;
        this.gWiOFio = 0;
        this.ELxjQaDRrI = length;
        this.UxnFzZ = i2 - 1;
    }

    public boolean CkytDdEwp() {
        return this.gWiOFio == this.ELxjQaDRrI;
    }

    public Object ELxjQaDRrI(int i) {
        if (i >= 0 && i < OvRsHjLd()) {
            return this.JyKmOjX[(this.gWiOFio + i) & this.UxnFzZ];
        }
        throw new ArrayIndexOutOfBoundsException();
    }

    public void ELxjQaDRrI() {
        JyKmOjX(OvRsHjLd());
    }

    public Object JHCbNsJ() {
        if (this.gWiOFio != this.ELxjQaDRrI) {
            return this.JyKmOjX[(this.ELxjQaDRrI - 1) & this.UxnFzZ];
        }
        throw new ArrayIndexOutOfBoundsException();
    }

    public Object JyKmOjX() {
        if (this.gWiOFio == this.ELxjQaDRrI) {
            throw new ArrayIndexOutOfBoundsException();
        }
        Object obj = this.JyKmOjX[this.gWiOFio];
        this.JyKmOjX[this.gWiOFio] = null;
        this.gWiOFio = (this.gWiOFio + 1) & this.UxnFzZ;
        return obj;
    }

    public void JyKmOjX(int i) {
        if (i > 0) {
            if (i > OvRsHjLd()) {
                throw new ArrayIndexOutOfBoundsException();
            }
            int length = this.JyKmOjX.length;
            if (i < length - this.gWiOFio) {
                length = this.gWiOFio + i;
            }
            for (int i2 = this.gWiOFio; i2 < length; i2++) {
                this.JyKmOjX[i2] = null;
            }
            int i3 = length - this.gWiOFio;
            int i4 = i - i3;
            this.gWiOFio = (i3 + this.gWiOFio) & this.UxnFzZ;
            if (i4 > 0) {
                for (int i5 = 0; i5 < i4; i5++) {
                    this.JyKmOjX[i5] = null;
                }
                this.gWiOFio = i4;
            }
        }
    }

    public void JyKmOjX(Object obj) {
        this.gWiOFio = (this.gWiOFio - 1) & this.UxnFzZ;
        this.JyKmOjX[this.gWiOFio] = obj;
        if (this.gWiOFio == this.ELxjQaDRrI) {
            CGmlqExFKDsw();
        }
    }

    public int OvRsHjLd() {
        return (this.ELxjQaDRrI - this.gWiOFio) & this.UxnFzZ;
    }

    public Object UxnFzZ() {
        if (this.gWiOFio != this.ELxjQaDRrI) {
            return this.JyKmOjX[this.gWiOFio];
        }
        throw new ArrayIndexOutOfBoundsException();
    }

    public Object gWiOFio() {
        if (this.gWiOFio == this.ELxjQaDRrI) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int i = (this.ELxjQaDRrI - 1) & this.UxnFzZ;
        Object obj = this.JyKmOjX[i];
        this.JyKmOjX[i] = null;
        this.ELxjQaDRrI = i;
        return obj;
    }

    public void gWiOFio(int i) {
        if (i > 0) {
            if (i > OvRsHjLd()) {
                throw new ArrayIndexOutOfBoundsException();
            }
            int i2 = 0;
            if (i < this.ELxjQaDRrI) {
                i2 = this.ELxjQaDRrI - i;
            }
            for (int i3 = i2; i3 < this.ELxjQaDRrI; i3++) {
                this.JyKmOjX[i3] = null;
            }
            int i4 = this.ELxjQaDRrI - i2;
            int i5 = i - i4;
            this.ELxjQaDRrI -= i4;
            if (i5 > 0) {
                this.ELxjQaDRrI = this.JyKmOjX.length;
                int i6 = this.ELxjQaDRrI - i5;
                for (int i7 = i6; i7 < this.ELxjQaDRrI; i7++) {
                    this.JyKmOjX[i7] = null;
                }
                this.ELxjQaDRrI = i6;
            }
        }
    }

    public void gWiOFio(Object obj) {
        this.JyKmOjX[this.ELxjQaDRrI] = obj;
        this.ELxjQaDRrI = (this.ELxjQaDRrI + 1) & this.UxnFzZ;
        if (this.ELxjQaDRrI == this.gWiOFio) {
            CGmlqExFKDsw();
        }
    }
}
