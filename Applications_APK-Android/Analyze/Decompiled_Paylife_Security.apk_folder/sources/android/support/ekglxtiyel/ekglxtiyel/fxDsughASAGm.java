package android.support.ekglxtiyel.ekglxtiyel;

import android.app.ActivityOptions;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

class fxDsughASAGm {
    private final ActivityOptions JyKmOjX;

    private fxDsughASAGm(ActivityOptions activityOptions) {
        this.JyKmOjX = activityOptions;
    }

    public static fxDsughASAGm JyKmOjX(Context context, int i, int i2) {
        return new fxDsughASAGm(ActivityOptions.makeCustomAnimation(context, i, i2));
    }

    public static fxDsughASAGm JyKmOjX(View view, int i, int i2, int i3, int i4) {
        return new fxDsughASAGm(ActivityOptions.makeScaleUpAnimation(view, i, i2, i3, i4));
    }

    public static fxDsughASAGm JyKmOjX(View view, Bitmap bitmap, int i, int i2) {
        return new fxDsughASAGm(ActivityOptions.makeThumbnailScaleUpAnimation(view, bitmap, i, i2));
    }

    public Bundle JyKmOjX() {
        return this.JyKmOjX.toBundle();
    }

    public void JyKmOjX(fxDsughASAGm fxdsughasagm) {
        this.JyKmOjX.update(fxdsughasagm.JyKmOjX);
    }
}
