package android.support.ekglxtiyel.ekglxtiyel;

import android.app.Activity;
import android.content.ComponentName;
import android.os.Build;
import android.view.Menu;
import android.view.MenuItem;

public class HKYHDMX {
    /* access modifiers changed from: private */
    public static QhmlCxzQg ELxjQaDRrI = null;
    public static final String JyKmOjX = "android.support.v4.app.EXTRA_CALLING_PACKAGE";
    public static final String gWiOFio = "android.support.v4.app.EXTRA_CALLING_ACTIVITY";

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            ELxjQaDRrI = new MNShuZwH();
        } else if (Build.VERSION.SDK_INT >= 14) {
            ELxjQaDRrI = new fMERfQCO();
        } else {
            ELxjQaDRrI = new pJaZBtJ();
        }
    }

    public static String JyKmOjX(Activity activity) {
        String callingPackage = activity.getCallingPackage();
        return callingPackage == null ? activity.getResizedRegion().getStringExtra("android.support.dialog.LAST_KEY_ARG") : callingPackage;
    }

    public static void JyKmOjX(Menu menu, int i, CgrQSEsTX cgrQSEsTX) {
        MenuItem findItem = menu.findItem(i);
        if (findItem == null) {
            throw new IllegalArgumentException("Could not find menu item with id " + i + " in the supplied menu");
        }
        JyKmOjX(findItem, cgrQSEsTX);
    }

    public static void JyKmOjX(MenuItem menuItem, CgrQSEsTX cgrQSEsTX) {
        ELxjQaDRrI.JyKmOjX(menuItem, cgrQSEsTX);
    }

    public static ComponentName gWiOFio(Activity activity) {
        ComponentName callingActivity = activity.getCallingActivity();
        return callingActivity == null ? (ComponentName) activity.getResizedRegion().getParcelableExtra(gWiOFio) : callingActivity;
    }
}
