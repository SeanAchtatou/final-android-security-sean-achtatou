package android.support.ekglxtiyel.ekglxtiyel;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.ekglxtiyel.lzcewhsnchqg.HmCbDs;
import android.support.ekglxtiyel.lzcewhsnchqg.wSMjih;
import android.support.ekglxtiyel.wrizuodpud.NIUxNi;
import android.support.ekglxtiyel.wrizuodpud.YuxfPhPIaOM;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

final class EgicqB extends rIENxmUl implements HmCbDs {
    static Field ADojbWgjJep = null;
    static final String CGmlqExFKDsw = "android:user_visible_hint";
    static final String CkytDdEwp = "android:view_state";
    static final String ELxjQaDRrI = "FragmentManager";
    static final int HtTkLrRUD = 220;
    static final String JHCbNsJ = "android:target_req_state";
    public static final int MZovfFrQwiu = 5;
    static final String OvRsHjLd = "android:target_state";
    static final Interpolator PvSPypUwEGN = new DecelerateInterpolator(2.5f);
    static final Interpolator TnGaPnXx = new AccelerateInterpolator(2.5f);
    static final boolean UxnFzZ;
    static final Interpolator aGPTGenhP = new AccelerateInterpolator(1.5f);
    public static final int cLzuMtBYK = 6;
    static boolean gWiOFio = UxnFzZ;
    public static final int ljAgNhVJhH = 2;
    public static final int tjNsHSCpE = 1;
    public static final int uMOUnQbdK = 3;
    public static final int xkreoLKotdYW = 4;
    static final Interpolator xyNalLRuamq = new DecelerateInterpolator(1.5f);
    ArrayList DCbsRKERTF;
    ArrayList DYldmviR;
    ArrayList IsLoypLqxg;
    Bundle JPEfQkMfcXeW = null;
    ArrayList NkBWDzI;
    ArrayList RBZUWx;
    boolean RFOcVKwijOVg;
    hBYomwKY SYafeh;
    ArrayList TUFgsvaVkdN;
    Runnable VYBKAHZ = new nMCRbYs(this);
    boolean VzWbdN;
    SparseArray WlQHAGESvdRg = null;
    boolean ZVIDXbvWn;
    ArrayList ZcxyUcLBGcV;
    CGNKZh clabDdWhMqLI;
    xRKNtyAZgG dQCQlqcL;
    String ejoYRLl;
    int gnKlzvCZLmUA = 0;
    MTZVRQn ineUXGyuQmP;
    ArrayList nHLQZwbUt;
    boolean udYZxYWU;
    Runnable[] uvKYrIFVv;
    ArrayList wqpspf;
    boolean wzLBqLQcFlu;

    static {
        boolean z = UxnFzZ;
        if (Build.VERSION.SDK_INT >= 11) {
            z = true;
        }
        UxnFzZ = z;
    }

    EgicqB() {
    }

    static Animation JyKmOjX(Context context, float f, float f2) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f, f2);
        alphaAnimation.setInterpolator(xyNalLRuamq);
        alphaAnimation.setDuration(220);
        return alphaAnimation;
    }

    static Animation JyKmOjX(Context context, float f, float f2, float f3, float f4) {
        AnimationSet animationSet = new AnimationSet(UxnFzZ);
        ScaleAnimation scaleAnimation = new ScaleAnimation(f, f2, f, f2, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(PvSPypUwEGN);
        scaleAnimation.setDuration(220);
        animationSet.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(f3, f4);
        alphaAnimation.setInterpolator(xyNalLRuamq);
        alphaAnimation.setDuration(220);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    private void JyKmOjX(RuntimeException runtimeException) {
        Log.e(ELxjQaDRrI, runtimeException.getMessage());
        Log.e(ELxjQaDRrI, "Activity state:");
        PrintWriter printWriter = new PrintWriter(new NIUxNi(ELxjQaDRrI));
        if (this.ineUXGyuQmP != null) {
            try {
                this.ineUXGyuQmP.JyKmOjX("  ", null, printWriter, new String[0]);
            } catch (Exception e) {
                Log.e(ELxjQaDRrI, "Failed dumping state", e);
            }
        } else {
            try {
                JyKmOjX("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e2) {
                Log.e(ELxjQaDRrI, "Failed dumping state", e2);
            }
        }
        throw runtimeException;
    }

    static boolean JyKmOjX(View view, Animation animation) {
        if (Build.VERSION.SDK_INT < 19 || wSMjih.gWiOFio(view) != 0 || !wSMjih.OvRsHjLd(view) || !JyKmOjX(animation)) {
            return UxnFzZ;
        }
        return true;
    }

    static boolean JyKmOjX(Animation animation) {
        if (animation instanceof AlphaAnimation) {
            return true;
        }
        if (!(animation instanceof AnimationSet)) {
            return UxnFzZ;
        }
        List<Animation> animations = ((AnimationSet) animation).getAnimations();
        for (int i = 0; i < animations.size(); i++) {
            if (animations.get(i) instanceof AlphaAnimation) {
                return true;
            }
        }
        return UxnFzZ;
    }

    public static int UxnFzZ(int i) {
        switch (i) {
            case KLCcZEc.TnGaPnXx /*4097*/:
                return KLCcZEc.aGPTGenhP;
            case KLCcZEc.HtTkLrRUD /*4099*/:
                return KLCcZEc.HtTkLrRUD;
            case KLCcZEc.aGPTGenhP /*8194*/:
                return KLCcZEc.TnGaPnXx;
            default:
                return 0;
        }
    }

    public static int gWiOFio(int i, boolean z) {
        switch (i) {
            case KLCcZEc.TnGaPnXx /*4097*/:
                return z ? 1 : 2;
            case KLCcZEc.HtTkLrRUD /*4099*/:
                return z ? 5 : 6;
            case KLCcZEc.aGPTGenhP /*8194*/:
                return z ? 3 : 4;
            default:
                return -1;
        }
    }

    private void gWiOFio(View view, Animation animation) {
        Animation.AnimationListener animationListener;
        if (view != null && animation != null && JyKmOjX(view, animation)) {
            try {
                if (ADojbWgjJep == null) {
                    ADojbWgjJep = Animation.class.getFirstField("mListener");
                    ADojbWgjJep.privateOnly(true);
                }
                animationListener = (Animation.AnimationListener) ADojbWgjJep.getProtoLink(animation);
            } catch (NoSuchFieldException e) {
                Log.e(ELxjQaDRrI, "No field with the name mListener is found in Animation class", e);
                animationListener = null;
            } catch (IllegalAccessException e2) {
                Log.e(ELxjQaDRrI, "Cannot access Animation's mListener field", e2);
                animationListener = null;
            }
            animation.setAnimationListener(new yuBxDfFoRSm(view, animation, animationListener));
        }
    }

    private void udYZxYWU() {
        if (this.wzLBqLQcFlu) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        } else if (this.ejoYRLl != null) {
            throw new IllegalStateException("Can not perform this action inside of " + this.ejoYRLl);
        }
    }

    /* access modifiers changed from: package-private */
    public HmCbDs ADojbWgjJep() {
        return this;
    }

    public boolean CGmlqExFKDsw() {
        return this.RFOcVKwijOVg;
    }

    /* access modifiers changed from: package-private */
    public Bundle CkytDdEwp(hBYomwKY hbyomwky) {
        Bundle bundle;
        if (this.JPEfQkMfcXeW == null) {
            this.JPEfQkMfcXeW = new Bundle();
        }
        hbyomwky.ZVIDXbvWn(this.JPEfQkMfcXeW);
        if (!this.JPEfQkMfcXeW.isEmpty()) {
            bundle = this.JPEfQkMfcXeW;
            this.JPEfQkMfcXeW = null;
        } else {
            bundle = null;
        }
        if (hbyomwky.wGguZKYQ != null) {
            OvRsHjLd(hbyomwky);
        }
        if (hbyomwky.ADojbWgjJep != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray(CkytDdEwp, hbyomwky.ADojbWgjJep);
        }
        if (!hbyomwky.eNRpHs) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean(CGmlqExFKDsw, hbyomwky.eNRpHs);
        }
        return bundle;
    }

    public List CkytDdEwp() {
        return this.TUFgsvaVkdN;
    }

    public void DCbsRKERTF() {
        this.wzLBqLQcFlu = UxnFzZ;
        JyKmOjX(5, (boolean) UxnFzZ);
    }

    public void DYldmviR() {
        this.wzLBqLQcFlu = UxnFzZ;
        JyKmOjX(2, (boolean) UxnFzZ);
    }

    public void ELxjQaDRrI(int i) {
        synchronized (this) {
            this.wqpspf.set(i, null);
            if (this.DCbsRKERTF == null) {
                this.DCbsRKERTF = new ArrayList();
            }
            if (gWiOFio) {
                Log.v(ELxjQaDRrI, "Freeing back stack index " + i);
            }
            this.DCbsRKERTF.add(Integer.valueOf(i));
        }
    }

    /* access modifiers changed from: package-private */
    public void ELxjQaDRrI(hBYomwKY hbyomwky) {
        JyKmOjX(hbyomwky, this.gnKlzvCZLmUA, 0, 0, (boolean) UxnFzZ);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.support.ekglxtiyel.ekglxtiyel.hBYomwKY, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.ekglxtiyel.ekglxtiyel.hBYomwKY, int, int, int]
     candidates:
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(int, int, int, boolean):void
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.os.Handler, java.lang.String, int, int):boolean
      android.support.ekglxtiyel.ekglxtiyel.rIENxmUl.JyKmOjX(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.ekglxtiyel.lzcewhsnchqg.HmCbDs.JyKmOjX(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.support.ekglxtiyel.ekglxtiyel.hBYomwKY, int, boolean, int):android.view.animation.Animation */
    public void ELxjQaDRrI(hBYomwKY hbyomwky, int i, int i2) {
        if (gWiOFio) {
            Log.v(ELxjQaDRrI, "show: " + hbyomwky);
        }
        if (hbyomwky.aZEOMfcrkmbI) {
            hbyomwky.aZEOMfcrkmbI = UxnFzZ;
            if (hbyomwky.wGguZKYQ != null) {
                Animation JyKmOjX = JyKmOjX(hbyomwky, i, true, i2);
                if (JyKmOjX != null) {
                    gWiOFio(hbyomwky.wGguZKYQ, JyKmOjX);
                    hbyomwky.wGguZKYQ.startAnimation(JyKmOjX);
                }
                hbyomwky.wGguZKYQ.setVisibility(0);
            }
            if (hbyomwky.WlQHAGESvdRg && hbyomwky.YRnebgHUCjYV && hbyomwky.iaMEMmG) {
                this.udYZxYWU = true;
            }
            hbyomwky.UxnFzZ((boolean) UxnFzZ);
        }
    }

    public boolean ELxjQaDRrI() {
        return uvKYrIFVv();
    }

    public void IsLoypLqxg() {
        this.wzLBqLQcFlu = UxnFzZ;
        JyKmOjX(1, (boolean) UxnFzZ);
    }

    /* access modifiers changed from: package-private */
    public void JHCbNsJ(hBYomwKY hbyomwky) {
        if (hbyomwky.udYZxYWU >= 0) {
            if (gWiOFio) {
                Log.v(ELxjQaDRrI, "Freeing fragment index " + hbyomwky);
            }
            this.TUFgsvaVkdN.set(hbyomwky.udYZxYWU, null);
            if (this.ZcxyUcLBGcV == null) {
                this.ZcxyUcLBGcV = new ArrayList();
            }
            this.ZcxyUcLBGcV.add(Integer.valueOf(hbyomwky.udYZxYWU));
            this.ineUXGyuQmP.gWiOFio(hbyomwky.wzLBqLQcFlu);
            hbyomwky.ljAgNhVJhH();
        }
    }

    public void JHCbNsJ(hBYomwKY hbyomwky, int i, int i2) {
        if (gWiOFio) {
            Log.v(ELxjQaDRrI, "attach: " + hbyomwky);
        }
        if (hbyomwky.GdBxFlWFwtk) {
            hbyomwky.GdBxFlWFwtk = UxnFzZ;
            if (!hbyomwky.WlQHAGESvdRg) {
                if (this.RBZUWx == null) {
                    this.RBZUWx = new ArrayList();
                }
                if (this.RBZUWx.contains(hbyomwky)) {
                    throw new IllegalStateException("Fragment already added: " + hbyomwky);
                }
                if (gWiOFio) {
                    Log.v(ELxjQaDRrI, "add from attach: " + hbyomwky);
                }
                this.RBZUWx.add(hbyomwky);
                hbyomwky.WlQHAGESvdRg = true;
                if (hbyomwky.YRnebgHUCjYV && hbyomwky.iaMEMmG) {
                    this.udYZxYWU = true;
                }
                JyKmOjX(hbyomwky, this.gnKlzvCZLmUA, i, i2, (boolean) UxnFzZ);
            }
        }
    }

    public boolean JHCbNsJ() {
        udYZxYWU();
        ELxjQaDRrI();
        return JyKmOjX(this.ineUXGyuQmP.uvKYrIFVv(), (String) null, -1, 0);
    }

    public int JyKmOjX(sznRcHTe sznrchte) {
        int i;
        synchronized (this) {
            if (this.DCbsRKERTF == null || this.DCbsRKERTF.size() <= 0) {
                if (this.wqpspf == null) {
                    this.wqpspf = new ArrayList();
                }
                i = this.wqpspf.size();
                if (gWiOFio) {
                    Log.v(ELxjQaDRrI, "Setting back stack index " + i + " to " + sznrchte);
                }
                this.wqpspf.add(sznrchte);
            } else {
                i = ((Integer) this.DCbsRKERTF.clear(this.DCbsRKERTF.size() - 1)).intValue();
                if (gWiOFio) {
                    Log.v(ELxjQaDRrI, "Adding back stack index " + i + " with " + sznrchte);
                }
                this.wqpspf.set(i, sznrchte);
            }
        }
        return i;
    }

    public KLCcZEc JyKmOjX() {
        return new sznRcHTe(this);
    }

    public TurizBcpCK JyKmOjX(hBYomwKY hbyomwky) {
        Bundle CkytDdEwp2;
        if (hbyomwky.udYZxYWU < 0) {
            JyKmOjX(new IllegalStateException("Fragment " + hbyomwky + " is not currently in the FragmentManager"));
        }
        if (hbyomwky.ineUXGyuQmP <= 0 || (CkytDdEwp2 = CkytDdEwp(hbyomwky)) == null) {
            return null;
        }
        return new TurizBcpCK(CkytDdEwp2);
    }

    public hBYomwKY JyKmOjX(int i) {
        if (this.RBZUWx != null) {
            for (int size = this.RBZUWx.size() - 1; size >= 0; size--) {
                hBYomwKY hbyomwky = (hBYomwKY) this.RBZUWx.get(size);
                if (hbyomwky != null && hbyomwky.MZovfFrQwiu == i) {
                    return hbyomwky;
                }
            }
        }
        if (this.TUFgsvaVkdN != null) {
            for (int size2 = this.TUFgsvaVkdN.size() - 1; size2 >= 0; size2--) {
                hBYomwKY hbyomwky2 = (hBYomwKY) this.TUFgsvaVkdN.get(size2);
                if (hbyomwky2 != null && hbyomwky2.MZovfFrQwiu == i) {
                    return hbyomwky2;
                }
            }
        }
        return null;
    }

    public hBYomwKY JyKmOjX(Bundle bundle, String str) {
        int i = bundle.getInt(str, -1);
        if (i == -1) {
            return null;
        }
        if (i >= this.TUFgsvaVkdN.size()) {
            JyKmOjX(new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i));
        }
        hBYomwKY hbyomwky = (hBYomwKY) this.TUFgsvaVkdN.get(i);
        if (hbyomwky != null) {
            return hbyomwky;
        }
        JyKmOjX(new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i));
        return hbyomwky;
    }

    public hBYomwKY JyKmOjX(String str) {
        if (!(this.RBZUWx == null || str == null)) {
            for (int size = this.RBZUWx.size() - 1; size >= 0; size--) {
                hBYomwKY hbyomwky = (hBYomwKY) this.RBZUWx.get(size);
                if (hbyomwky != null && str.equals(hbyomwky.rDdLHsxdG)) {
                    return hbyomwky;
                }
            }
        }
        if (!(this.TUFgsvaVkdN == null || str == null)) {
            for (int size2 = this.TUFgsvaVkdN.size() - 1; size2 >= 0; size2--) {
                hBYomwKY hbyomwky2 = (hBYomwKY) this.TUFgsvaVkdN.get(size2);
                if (hbyomwky2 != null && str.equals(hbyomwky2.rDdLHsxdG)) {
                    return hbyomwky2;
                }
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.support.ekglxtiyel.ekglxtiyel.hBYomwKY, boolean):void
     arg types: [android.support.ekglxtiyel.ekglxtiyel.hBYomwKY, int]
     candidates:
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.view.View, android.view.animation.Animation):boolean
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.os.Bundle, java.lang.String):android.support.ekglxtiyel.ekglxtiyel.hBYomwKY
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(int, int):void
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(int, android.support.ekglxtiyel.ekglxtiyel.sznRcHTe):void
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(int, boolean):void
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.os.Parcelable, java.util.List):void
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(java.lang.Runnable, boolean):void
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(java.lang.String, int):void
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.view.Menu, android.view.MenuInflater):boolean
      android.support.ekglxtiyel.ekglxtiyel.rIENxmUl.JyKmOjX(android.os.Bundle, java.lang.String):android.support.ekglxtiyel.ekglxtiyel.hBYomwKY
      android.support.ekglxtiyel.ekglxtiyel.rIENxmUl.JyKmOjX(int, int):void
      android.support.ekglxtiyel.ekglxtiyel.rIENxmUl.JyKmOjX(java.lang.String, int):void
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.support.ekglxtiyel.ekglxtiyel.hBYomwKY, boolean):void */
    public View JyKmOjX(View view, String str, Context context, AttributeSet attributeSet) {
        hBYomwKY hbyomwky;
        if (!"fragment".equals(str)) {
            return null;
        }
        String attributeValue = attributeSet.getAttributeValue(null, "class");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, QPkBfnsPhYw.JyKmOjX);
        String string = attributeValue == null ? obtainStyledAttributes.getString(0) : attributeValue;
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        String string2 = obtainStyledAttributes.getString(2);
        obtainStyledAttributes.recycle();
        if (!hBYomwKY.gWiOFio(this.ineUXGyuQmP.NkBWDzI(), string)) {
            return null;
        }
        int id = view != null ? view.getId() : 0;
        if (id == -1 && resourceId == -1 && string2 == null) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + string);
        }
        hBYomwKY JyKmOjX = resourceId != -1 ? JyKmOjX(resourceId) : null;
        if (JyKmOjX == null && string2 != null) {
            JyKmOjX = JyKmOjX(string2);
        }
        if (JyKmOjX == null && id != -1) {
            JyKmOjX = JyKmOjX(id);
        }
        if (gWiOFio) {
            Log.v(ELxjQaDRrI, "onCreateView: id=0x" + Integer.hexing(resourceId) + " fname=" + string + " existing=" + JyKmOjX);
        }
        if (JyKmOjX == null) {
            hBYomwKY JyKmOjX2 = hBYomwKY.JyKmOjX(context, string);
            JyKmOjX2.xyNalLRuamq = true;
            JyKmOjX2.MZovfFrQwiu = resourceId != 0 ? resourceId : id;
            JyKmOjX2.cLzuMtBYK = id;
            JyKmOjX2.rDdLHsxdG = string2;
            JyKmOjX2.TnGaPnXx = true;
            JyKmOjX2.tjNsHSCpE = this;
            JyKmOjX2.ljAgNhVJhH = this.ineUXGyuQmP;
            JyKmOjX2.JyKmOjX(this.ineUXGyuQmP.NkBWDzI(), attributeSet, JyKmOjX2.SYafeh);
            JyKmOjX(JyKmOjX2, true);
            hbyomwky = JyKmOjX2;
        } else if (JyKmOjX.TnGaPnXx) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.hexing(resourceId) + ", tag " + string2 + ", or parent id 0x" + Integer.hexing(id) + " with another fragment for " + string);
        } else {
            JyKmOjX.TnGaPnXx = true;
            if (!JyKmOjX.CwmNEYbDE) {
                JyKmOjX.JyKmOjX(this.ineUXGyuQmP.NkBWDzI(), attributeSet, JyKmOjX.SYafeh);
            }
            hbyomwky = JyKmOjX;
        }
        if (this.gnKlzvCZLmUA >= 1 || !hbyomwky.xyNalLRuamq) {
            ELxjQaDRrI(hbyomwky);
        } else {
            JyKmOjX(hbyomwky, 1, 0, 0, (boolean) UxnFzZ);
        }
        if (hbyomwky.wGguZKYQ == null) {
            throw new IllegalStateException("Fragment " + string + " did not create a view.");
        }
        if (resourceId != 0) {
            hbyomwky.wGguZKYQ.setId(resourceId);
        }
        if (hbyomwky.wGguZKYQ.marker() == null) {
            hbyomwky.wGguZKYQ.setTag(string2);
        }
        return hbyomwky.wGguZKYQ;
    }

    /* access modifiers changed from: package-private */
    public Animation JyKmOjX(hBYomwKY hbyomwky, int i, boolean z, int i2) {
        Animation loadAnimation;
        Animation JyKmOjX = hbyomwky.JyKmOjX(i, z, hbyomwky.ydeoLafXzx);
        if (JyKmOjX != null) {
            return JyKmOjX;
        }
        if (hbyomwky.ydeoLafXzx != 0 && (loadAnimation = AnimationUtils.loadAnimation(this.ineUXGyuQmP.NkBWDzI(), hbyomwky.ydeoLafXzx)) != null) {
            return loadAnimation;
        }
        if (i == 0) {
            return null;
        }
        int gWiOFio2 = gWiOFio(i, z);
        if (gWiOFio2 < 0) {
            return null;
        }
        switch (gWiOFio2) {
            case 1:
                return JyKmOjX(this.ineUXGyuQmP.NkBWDzI(), 1.125f, 1.0f, 0.0f, 1.0f);
            case 2:
                return JyKmOjX(this.ineUXGyuQmP.NkBWDzI(), 1.0f, 0.975f, 1.0f, 0.0f);
            case 3:
                return JyKmOjX(this.ineUXGyuQmP.NkBWDzI(), 0.975f, 1.0f, 0.0f, 1.0f);
            case 4:
                return JyKmOjX(this.ineUXGyuQmP.NkBWDzI(), 1.0f, 1.075f, 1.0f, 0.0f);
            case 5:
                return JyKmOjX(this.ineUXGyuQmP.NkBWDzI(), 0.0f, 1.0f);
            case cLzuMtBYK /*6*/:
                return JyKmOjX(this.ineUXGyuQmP.NkBWDzI(), 1.0f, 0.0f);
            default:
                if (i2 == 0 && this.ineUXGyuQmP.JHCbNsJ()) {
                    i2 = this.ineUXGyuQmP.OvRsHjLd();
                }
                return i2 == 0 ? null : null;
        }
    }

    public void JyKmOjX(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException("Bad id: " + i);
        }
        JyKmOjX(new tZgLwjycjvdO(this, i, i2), (boolean) UxnFzZ);
    }

    /* access modifiers changed from: package-private */
    public void JyKmOjX(int i, int i2, int i3, boolean z) {
        if (this.ineUXGyuQmP == null && i != 0) {
            throw new IllegalStateException("No host");
        } else if (z || this.gnKlzvCZLmUA != i) {
            this.gnKlzvCZLmUA = i;
            if (this.TUFgsvaVkdN != null) {
                int i4 = 0;
                boolean z2 = false;
                while (i4 < this.TUFgsvaVkdN.size()) {
                    hBYomwKY hbyomwky = (hBYomwKY) this.TUFgsvaVkdN.get(i4);
                    if (hbyomwky != null) {
                        JyKmOjX(hbyomwky, i, i2, i3, (boolean) UxnFzZ);
                        if (hbyomwky.wsRFIu != null) {
                            z2 |= hbyomwky.wsRFIu.JyKmOjX();
                        }
                    }
                    i4++;
                    z2 = z2;
                }
                if (!z2) {
                    NkBWDzI();
                }
                if (this.udYZxYWU && this.ineUXGyuQmP != null && this.gnKlzvCZLmUA == 5) {
                    this.ineUXGyuQmP.UxnFzZ();
                    this.udYZxYWU = UxnFzZ;
                }
            }
        }
    }

    public void JyKmOjX(int i, sznRcHTe sznrchte) {
        synchronized (this) {
            if (this.wqpspf == null) {
                this.wqpspf = new ArrayList();
            }
            int size = this.wqpspf.size();
            if (i < size) {
                if (gWiOFio) {
                    Log.v(ELxjQaDRrI, "Setting back stack index " + i + " to " + sznrchte);
                }
                this.wqpspf.set(i, sznrchte);
            } else {
                while (size < i) {
                    this.wqpspf.add(null);
                    if (this.DCbsRKERTF == null) {
                        this.DCbsRKERTF = new ArrayList();
                    }
                    if (gWiOFio) {
                        Log.v(ELxjQaDRrI, "Adding available back stack index " + size);
                    }
                    this.DCbsRKERTF.add(Integer.valueOf(size));
                    size++;
                }
                if (gWiOFio) {
                    Log.v(ELxjQaDRrI, "Adding back stack index " + i + " with " + sznrchte);
                }
                this.wqpspf.add(sznrchte);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void JyKmOjX(int i, boolean z) {
        JyKmOjX(i, 0, 0, z);
    }

    public void JyKmOjX(Configuration configuration) {
        if (this.RBZUWx != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.RBZUWx.size()) {
                    hBYomwKY hbyomwky = (hBYomwKY) this.RBZUWx.get(i2);
                    if (hbyomwky != null) {
                        hbyomwky.JyKmOjX(configuration);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void JyKmOjX(Bundle bundle, String str, hBYomwKY hbyomwky) {
        if (hbyomwky.udYZxYWU < 0) {
            JyKmOjX(new IllegalStateException("Fragment " + hbyomwky + " is not currently in the FragmentManager"));
        }
        bundle.putInt(str, hbyomwky.udYZxYWU);
    }

    /* access modifiers changed from: package-private */
    public void JyKmOjX(Parcelable parcelable, List list) {
        if (parcelable != null) {
            PiPrCiJYa piPrCiJYa = (PiPrCiJYa) parcelable;
            if (piPrCiJYa.JyKmOjX != null) {
                if (list != null) {
                    for (int i = 0; i < list.size(); i++) {
                        hBYomwKY hbyomwky = (hBYomwKY) list.get(i);
                        if (gWiOFio) {
                            Log.v(ELxjQaDRrI, "restoreAllState: re-attaching retained " + hbyomwky);
                        }
                        yguITe yguite = piPrCiJYa.JyKmOjX[hbyomwky.udYZxYWU];
                        yguite.ZVIDXbvWn = hbyomwky;
                        hbyomwky.ADojbWgjJep = null;
                        hbyomwky.HtTkLrRUD = 0;
                        hbyomwky.TnGaPnXx = UxnFzZ;
                        hbyomwky.WlQHAGESvdRg = UxnFzZ;
                        hbyomwky.ejoYRLl = null;
                        if (yguite.uvKYrIFVv != null) {
                            yguite.uvKYrIFVv.readyClass(this.ineUXGyuQmP.NkBWDzI().scout());
                            hbyomwky.ADojbWgjJep = yguite.uvKYrIFVv.getSparseParcelableArray(CkytDdEwp);
                            hbyomwky.SYafeh = yguite.uvKYrIFVv;
                        }
                    }
                }
                this.TUFgsvaVkdN = new ArrayList(piPrCiJYa.JyKmOjX.length);
                if (this.ZcxyUcLBGcV != null) {
                    this.ZcxyUcLBGcV.clear();
                }
                for (int i2 = 0; i2 < piPrCiJYa.JyKmOjX.length; i2++) {
                    yguITe yguite2 = piPrCiJYa.JyKmOjX[i2];
                    if (yguite2 != null) {
                        hBYomwKY JyKmOjX = yguite2.JyKmOjX(this.ineUXGyuQmP, this.SYafeh);
                        if (gWiOFio) {
                            Log.v(ELxjQaDRrI, "restoreAllState: active #" + i2 + ": " + JyKmOjX);
                        }
                        this.TUFgsvaVkdN.add(JyKmOjX);
                        yguite2.ZVIDXbvWn = null;
                    } else {
                        this.TUFgsvaVkdN.add(null);
                        if (this.ZcxyUcLBGcV == null) {
                            this.ZcxyUcLBGcV = new ArrayList();
                        }
                        if (gWiOFio) {
                            Log.v(ELxjQaDRrI, "restoreAllState: avail #" + i2);
                        }
                        this.ZcxyUcLBGcV.add(Integer.valueOf(i2));
                    }
                }
                if (list != null) {
                    for (int i3 = 0; i3 < list.size(); i3++) {
                        hBYomwKY hbyomwky2 = (hBYomwKY) list.get(i3);
                        if (hbyomwky2.VzWbdN >= 0) {
                            if (hbyomwky2.VzWbdN < this.TUFgsvaVkdN.size()) {
                                hbyomwky2.ejoYRLl = (hBYomwKY) this.TUFgsvaVkdN.get(hbyomwky2.VzWbdN);
                            } else {
                                Log.w(ELxjQaDRrI, "Re-attaching retained fragment " + hbyomwky2 + " target no longer exists: " + hbyomwky2.VzWbdN);
                                hbyomwky2.ejoYRLl = null;
                            }
                        }
                    }
                }
                if (piPrCiJYa.gWiOFio != null) {
                    this.RBZUWx = new ArrayList(piPrCiJYa.gWiOFio.length);
                    for (int i4 = 0; i4 < piPrCiJYa.gWiOFio.length; i4++) {
                        hBYomwKY hbyomwky3 = (hBYomwKY) this.TUFgsvaVkdN.get(piPrCiJYa.gWiOFio[i4]);
                        if (hbyomwky3 == null) {
                            JyKmOjX(new IllegalStateException("No instantiated fragment for index #" + piPrCiJYa.gWiOFio[i4]));
                        }
                        hbyomwky3.WlQHAGESvdRg = true;
                        if (gWiOFio) {
                            Log.v(ELxjQaDRrI, "restoreAllState: added #" + i4 + ": " + hbyomwky3);
                        }
                        if (this.RBZUWx.contains(hbyomwky3)) {
                            throw new IllegalStateException("Already added!");
                        }
                        this.RBZUWx.add(hbyomwky3);
                    }
                } else {
                    this.RBZUWx = null;
                }
                if (piPrCiJYa.ELxjQaDRrI != null) {
                    this.IsLoypLqxg = new ArrayList(piPrCiJYa.ELxjQaDRrI.length);
                    for (int i5 = 0; i5 < piPrCiJYa.ELxjQaDRrI.length; i5++) {
                        sznRcHTe JyKmOjX2 = piPrCiJYa.ELxjQaDRrI[i5].JyKmOjX(this);
                        if (gWiOFio) {
                            Log.v(ELxjQaDRrI, "restoreAllState: back stack #" + i5 + " (index " + JyKmOjX2.ADojbWgjJep + "): " + JyKmOjX2);
                            JyKmOjX2.JyKmOjX("  ", new PrintWriter(new NIUxNi(ELxjQaDRrI)), (boolean) UxnFzZ);
                        }
                        this.IsLoypLqxg.add(JyKmOjX2);
                        if (JyKmOjX2.ADojbWgjJep >= 0) {
                            JyKmOjX(JyKmOjX2.ADojbWgjJep, JyKmOjX2);
                        }
                    }
                    return;
                }
                this.IsLoypLqxg = null;
            }
        }
    }

    public void JyKmOjX(MTZVRQn mTZVRQn, xRKNtyAZgG xrkntyazgg, hBYomwKY hbyomwky) {
        if (this.ineUXGyuQmP != null) {
            throw new IllegalStateException("Already attached");
        }
        this.ineUXGyuQmP = mTZVRQn;
        this.dQCQlqcL = xrkntyazgg;
        this.SYafeh = hbyomwky;
    }

    public void JyKmOjX(UyLYGdXPJIC uyLYGdXPJIC) {
        if (this.nHLQZwbUt == null) {
            this.nHLQZwbUt = new ArrayList();
        }
        this.nHLQZwbUt.add(uyLYGdXPJIC);
    }

    public void JyKmOjX(hBYomwKY hbyomwky, int i, int i2) {
        if (gWiOFio) {
            Log.v(ELxjQaDRrI, "remove: " + hbyomwky + " nesting=" + hbyomwky.HtTkLrRUD);
        }
        boolean z = !hbyomwky.ZVIDXbvWn();
        if (!hbyomwky.GdBxFlWFwtk || z) {
            if (this.RBZUWx != null) {
                this.RBZUWx.clear(hbyomwky);
            }
            if (hbyomwky.YRnebgHUCjYV && hbyomwky.iaMEMmG) {
                this.udYZxYWU = true;
            }
            hbyomwky.WlQHAGESvdRg = UxnFzZ;
            hbyomwky.VYBKAHZ = true;
            JyKmOjX(hbyomwky, z ? 0 : 1, i, i2, (boolean) UxnFzZ);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.support.ekglxtiyel.ekglxtiyel.hBYomwKY, int, int, int, boolean):void
     arg types: [android.support.ekglxtiyel.ekglxtiyel.hBYomwKY, int, int, int, int]
     candidates:
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.support.ekglxtiyel.ekglxtiyel.hBYomwKY, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.support.ekglxtiyel.ekglxtiyel.hBYomwKY, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.ekglxtiyel.ekglxtiyel.hBYomwKY, int, int, int]
     candidates:
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(int, int, int, boolean):void
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.os.Handler, java.lang.String, int, int):boolean
      android.support.ekglxtiyel.ekglxtiyel.rIENxmUl.JyKmOjX(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.ekglxtiyel.lzcewhsnchqg.HmCbDs.JyKmOjX(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.ekglxtiyel.ekglxtiyel.EgicqB.JyKmOjX(android.support.ekglxtiyel.ekglxtiyel.hBYomwKY, int, boolean, int):android.view.animation.Animation */
    /* access modifiers changed from: package-private */
    public void JyKmOjX(hBYomwKY hbyomwky, int i, int i2, int i3, boolean z) {
        ViewGroup viewGroup;
        if ((!hbyomwky.WlQHAGESvdRg || hbyomwky.GdBxFlWFwtk) && i > 1) {
            i = 1;
        }
        if (hbyomwky.VYBKAHZ && i > hbyomwky.ineUXGyuQmP) {
            i = hbyomwky.ineUXGyuQmP;
        }
        if (hbyomwky.GSaTJAXLDK && hbyomwky.ineUXGyuQmP < 4 && i > 3) {
            i = 3;
        }
        if (hbyomwky.ineUXGyuQmP >= i) {
            if (hbyomwky.ineUXGyuQmP > i) {
                switch (hbyomwky.ineUXGyuQmP) {
                    case 5:
                        if (i < 5) {
                            if (gWiOFio) {
                                Log.v(ELxjQaDRrI, "movefrom RESUMED: " + hbyomwky);
                            }
                            hbyomwky.mTCZSJVnXKlh();
                            hbyomwky.PvSPypUwEGN = UxnFzZ;
                        }
                    case 4:
                        if (i < 4) {
                            if (gWiOFio) {
                                Log.v(ELxjQaDRrI, "movefrom STARTED: " + hbyomwky);
                            }
                            hbyomwky.wGguZKYQ();
                        }
                    case 3:
                        if (i < 3) {
                            if (gWiOFio) {
                                Log.v(ELxjQaDRrI, "movefrom STOPPED: " + hbyomwky);
                            }
                            hbyomwky.GtjRlhyKkDco();
                        }
                    case 2:
                        if (i < 2) {
                            if (gWiOFio) {
                                Log.v(ELxjQaDRrI, "movefrom ACTIVITY_CREATED: " + hbyomwky);
                            }
                            if (hbyomwky.wGguZKYQ != null && this.ineUXGyuQmP.JyKmOjX(hbyomwky) && hbyomwky.ADojbWgjJep == null) {
                                OvRsHjLd(hbyomwky);
                            }
                            hbyomwky.GSaTJAXLDK();
                            if (!(hbyomwky.wGguZKYQ == null || hbyomwky.mTCZSJVnXKlh == null)) {
                                Animation JyKmOjX = (this.gnKlzvCZLmUA <= 0 || this.RFOcVKwijOVg) ? null : JyKmOjX(hbyomwky, i2, (boolean) UxnFzZ, i3);
                                if (JyKmOjX != null) {
                                    hbyomwky.clabDdWhMqLI = hbyomwky.wGguZKYQ;
                                    hbyomwky.dQCQlqcL = i;
                                    JyKmOjX.setAnimationListener(new TFTztEV(this, hbyomwky.wGguZKYQ, JyKmOjX, hbyomwky));
                                    hbyomwky.wGguZKYQ.startAnimation(JyKmOjX);
                                }
                                hbyomwky.mTCZSJVnXKlh.removeView(hbyomwky.wGguZKYQ);
                            }
                            hbyomwky.mTCZSJVnXKlh = null;
                            hbyomwky.wGguZKYQ = null;
                            hbyomwky.GtjRlhyKkDco = null;
                        }
                        break;
                    case 1:
                        if (i < 1) {
                            if (this.RFOcVKwijOVg && hbyomwky.clabDdWhMqLI != null) {
                                View view = hbyomwky.clabDdWhMqLI;
                                hbyomwky.clabDdWhMqLI = null;
                                view.clearAnimation();
                            }
                            if (hbyomwky.clabDdWhMqLI == null) {
                                if (gWiOFio) {
                                    Log.v(ELxjQaDRrI, "movefrom CREATED: " + hbyomwky);
                                }
                                if (!hbyomwky.CwmNEYbDE) {
                                    hbyomwky.eNRpHs();
                                }
                                hbyomwky.ywktOEwHxQa = UxnFzZ;
                                hbyomwky.CkytDdEwp();
                                if (hbyomwky.ywktOEwHxQa) {
                                    if (!z) {
                                        if (hbyomwky.CwmNEYbDE) {
                                            hbyomwky.ljAgNhVJhH = null;
                                            hbyomwky.xkreoLKotdYW = null;
                                            hbyomwky.tjNsHSCpE = null;
                                            hbyomwky.uMOUnQbdK = null;
                                            break;
                                        } else {
                                            JHCbNsJ(hbyomwky);
                                            break;
                                        }
                                    }
                                } else {
                                    throw new gmgwOCp("Fragment " + hbyomwky + " did not call through to super.onDetach()");
                                }
                            } else {
                                hbyomwky.dQCQlqcL = i;
                                i = 1;
                                break;
                            }
                        }
                        break;
                }
            }
        } else if (!hbyomwky.xyNalLRuamq || hbyomwky.TnGaPnXx) {
            if (hbyomwky.clabDdWhMqLI != null) {
                hbyomwky.clabDdWhMqLI = null;
                JyKmOjX(hbyomwky, hbyomwky.dQCQlqcL, 0, 0, true);
            }
            switch (hbyomwky.ineUXGyuQmP) {
                case 0:
                    if (gWiOFio) {
                        Log.v(ELxjQaDRrI, "moveto CREATED: " + hbyomwky);
                    }
                    if (hbyomwky.SYafeh != null) {
                        hbyomwky.SYafeh.readyClass(this.ineUXGyuQmP.NkBWDzI().scout());
                        hbyomwky.ADojbWgjJep = hbyomwky.SYafeh.getSparseParcelableArray(CkytDdEwp);
                        hbyomwky.ejoYRLl = JyKmOjX(hbyomwky.SYafeh, OvRsHjLd);
                        if (hbyomwky.ejoYRLl != null) {
                            hbyomwky.JPEfQkMfcXeW = hbyomwky.SYafeh.getInt(JHCbNsJ, 0);
                        }
                        hbyomwky.eNRpHs = hbyomwky.SYafeh.getBoolean(CGmlqExFKDsw, true);
                        if (!hbyomwky.eNRpHs) {
                            hbyomwky.GSaTJAXLDK = true;
                            if (i > 3) {
                                i = 3;
                            }
                        }
                    }
                    hbyomwky.ljAgNhVJhH = this.ineUXGyuQmP;
                    hbyomwky.xkreoLKotdYW = this.SYafeh;
                    hbyomwky.tjNsHSCpE = this.SYafeh != null ? this.SYafeh.uMOUnQbdK : this.ineUXGyuQmP.ZVIDXbvWn();
                    hbyomwky.ywktOEwHxQa = UxnFzZ;
                    hbyomwky.JyKmOjX(this.ineUXGyuQmP.NkBWDzI());
                    if (!hbyomwky.ywktOEwHxQa) {
                        throw new gmgwOCp("Fragment " + hbyomwky + " did not call through to super.onAttach()");
                    }
                    if (hbyomwky.xkreoLKotdYW == null) {
                        this.ineUXGyuQmP.gWiOFio(hbyomwky);
                    }
                    if (!hbyomwky.CwmNEYbDE) {
                        hbyomwky.NkBWDzI(hbyomwky.SYafeh);
                    }
                    hbyomwky.CwmNEYbDE = UxnFzZ;
                    if (hbyomwky.xyNalLRuamq) {
                        hbyomwky.wGguZKYQ = hbyomwky.gWiOFio(hbyomwky.gWiOFio(hbyomwky.SYafeh), null, hbyomwky.SYafeh);
                        if (hbyomwky.wGguZKYQ != null) {
                            hbyomwky.GtjRlhyKkDco = hbyomwky.wGguZKYQ;
                            if (Build.VERSION.SDK_INT >= 11) {
                                wSMjih.JyKmOjX(hbyomwky.wGguZKYQ, (boolean) UxnFzZ);
                            } else {
                                hbyomwky.wGguZKYQ = GdgwkUvRd.JyKmOjX(hbyomwky.wGguZKYQ);
                            }
                            if (hbyomwky.aZEOMfcrkmbI) {
                                hbyomwky.wGguZKYQ.setVisibility(8);
                            }
                            hbyomwky.JyKmOjX(hbyomwky.wGguZKYQ, hbyomwky.SYafeh);
                        } else {
                            hbyomwky.GtjRlhyKkDco = null;
                        }
                    }
                case 1:
                    if (i > 1) {
                        if (gWiOFio) {
                            Log.v(ELxjQaDRrI, "moveto ACTIVITY_CREATED: " + hbyomwky);
                        }
                        if (!hbyomwky.xyNalLRuamq) {
                            if (hbyomwky.cLzuMtBYK != 0) {
                                viewGroup = (ViewGroup) this.dQCQlqcL.JyKmOjX(hbyomwky.cLzuMtBYK);
                                if (viewGroup == null && !hbyomwky.aGPTGenhP) {
                                    JyKmOjX(new IllegalArgumentException("No view found for id 0x" + Integer.hexing(hbyomwky.cLzuMtBYK) + " (" + hbyomwky.gnKlzvCZLmUA().getResourceName(hbyomwky.cLzuMtBYK) + ") for fragment " + hbyomwky));
                                }
                            } else {
                                viewGroup = null;
                            }
                            hbyomwky.mTCZSJVnXKlh = viewGroup;
                            hbyomwky.wGguZKYQ = hbyomwky.gWiOFio(hbyomwky.gWiOFio(hbyomwky.SYafeh), viewGroup, hbyomwky.SYafeh);
                            if (hbyomwky.wGguZKYQ != null) {
                                hbyomwky.GtjRlhyKkDco = hbyomwky.wGguZKYQ;
                                if (Build.VERSION.SDK_INT >= 11) {
                                    wSMjih.JyKmOjX(hbyomwky.wGguZKYQ, (boolean) UxnFzZ);
                                } else {
                                    hbyomwky.wGguZKYQ = GdgwkUvRd.JyKmOjX(hbyomwky.wGguZKYQ);
                                }
                                if (viewGroup != null) {
                                    Animation JyKmOjX2 = JyKmOjX(hbyomwky, i2, true, i3);
                                    if (JyKmOjX2 != null) {
                                        gWiOFio(hbyomwky.wGguZKYQ, JyKmOjX2);
                                        hbyomwky.wGguZKYQ.startAnimation(JyKmOjX2);
                                    }
                                    viewGroup.addView(hbyomwky.wGguZKYQ);
                                }
                                if (hbyomwky.aZEOMfcrkmbI) {
                                    hbyomwky.wGguZKYQ.setVisibility(8);
                                }
                                hbyomwky.JyKmOjX(hbyomwky.wGguZKYQ, hbyomwky.SYafeh);
                            } else {
                                hbyomwky.GtjRlhyKkDco = null;
                            }
                        }
                        hbyomwky.uvKYrIFVv(hbyomwky.SYafeh);
                        if (hbyomwky.wGguZKYQ != null) {
                            hbyomwky.OvRsHjLd(hbyomwky.SYafeh);
                        }
                        hbyomwky.SYafeh = null;
                    }
                case 2:
                case 3:
                    if (i > 3) {
                        if (gWiOFio) {
                            Log.v(ELxjQaDRrI, "moveto STARTED: " + hbyomwky);
                        }
                        hbyomwky.iaMEMmG();
                    }
                case 4:
                    if (i > 4) {
                        if (gWiOFio) {
                            Log.v(ELxjQaDRrI, "moveto RESUMED: " + hbyomwky);
                        }
                        hbyomwky.PvSPypUwEGN = true;
                        hbyomwky.ywktOEwHxQa();
                        hbyomwky.SYafeh = null;
                        hbyomwky.ADojbWgjJep = null;
                        break;
                    }
                    break;
            }
        } else {
            return;
        }
        hbyomwky.ineUXGyuQmP = i;
    }

    public void JyKmOjX(hBYomwKY hbyomwky, boolean z) {
        if (this.RBZUWx == null) {
            this.RBZUWx = new ArrayList();
        }
        if (gWiOFio) {
            Log.v(ELxjQaDRrI, "add: " + hbyomwky);
        }
        UxnFzZ(hbyomwky);
        if (hbyomwky.GdBxFlWFwtk) {
            return;
        }
        if (this.RBZUWx.contains(hbyomwky)) {
            throw new IllegalStateException("Fragment already added: " + hbyomwky);
        }
        this.RBZUWx.add(hbyomwky);
        hbyomwky.WlQHAGESvdRg = true;
        hbyomwky.VYBKAHZ = UxnFzZ;
        if (hbyomwky.YRnebgHUCjYV && hbyomwky.iaMEMmG) {
            this.udYZxYWU = true;
        }
        if (z) {
            ELxjQaDRrI(hbyomwky);
        }
    }

    public void JyKmOjX(Runnable runnable, boolean z) {
        if (!z) {
            udYZxYWU();
        }
        synchronized (this) {
            if (this.RFOcVKwijOVg || this.ineUXGyuQmP == null) {
                throw new IllegalStateException("Activity has been destroyed");
            }
            if (this.NkBWDzI == null) {
                this.NkBWDzI = new ArrayList();
            }
            this.NkBWDzI.add(runnable);
            if (this.NkBWDzI.size() == 1) {
                this.ineUXGyuQmP.uvKYrIFVv().removeCallbacks(this.VYBKAHZ);
                this.ineUXGyuQmP.uvKYrIFVv().post(this.VYBKAHZ);
            }
        }
    }

    public void JyKmOjX(String str, int i) {
        JyKmOjX(new cbIbnNpbV(this, str, i), (boolean) UxnFzZ);
    }

    public void JyKmOjX(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int size;
        int size2;
        int size3;
        int size4;
        int size5;
        int size6;
        String str2 = str + "    ";
        if (this.TUFgsvaVkdN != null && (size6 = this.TUFgsvaVkdN.size()) > 0) {
            printWriter.print(str);
            printWriter.print("Active Fragments in ");
            printWriter.print(Integer.hexing(System.uniqueHCode(this)));
            printWriter.println(":");
            for (int i = 0; i < size6; i++) {
                hBYomwKY hbyomwky = (hBYomwKY) this.TUFgsvaVkdN.get(i);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.println(hbyomwky);
                if (hbyomwky != null) {
                    hbyomwky.JyKmOjX(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
        if (this.RBZUWx != null && (size5 = this.RBZUWx.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i2 = 0; i2 < size5; i2++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(((hBYomwKY) this.RBZUWx.get(i2)).toString());
            }
        }
        if (this.DYldmviR != null && (size4 = this.DYldmviR.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Fragments Created Menus:");
            for (int i3 = 0; i3 < size4; i3++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(((hBYomwKY) this.DYldmviR.get(i3)).toString());
            }
        }
        if (this.IsLoypLqxg != null && (size3 = this.IsLoypLqxg.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Back Stack:");
            for (int i4 = 0; i4 < size3; i4++) {
                sznRcHTe sznrchte = (sznRcHTe) this.IsLoypLqxg.get(i4);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i4);
                printWriter.print(": ");
                printWriter.println(sznrchte.toString());
                sznrchte.JyKmOjX(str2, fileDescriptor, printWriter, strArr);
            }
        }
        synchronized (this) {
            if (this.wqpspf != null && (size2 = this.wqpspf.size()) > 0) {
                printWriter.print(str);
                printWriter.println("Back Stack Indices:");
                for (int i5 = 0; i5 < size2; i5++) {
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i5);
                    printWriter.print(": ");
                    printWriter.println((sznRcHTe) this.wqpspf.get(i5));
                }
            }
            if (this.DCbsRKERTF != null && this.DCbsRKERTF.size() > 0) {
                printWriter.print(str);
                printWriter.print("mAvailBackStackIndices: ");
                printWriter.println(Arrays.toString(this.DCbsRKERTF.toArray()));
            }
        }
        if (this.NkBWDzI != null && (size = this.NkBWDzI.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Pending Actions:");
            for (int i6 = 0; i6 < size; i6++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i6);
                printWriter.print(": ");
                printWriter.println((Runnable) this.NkBWDzI.get(i6));
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mHost=");
        printWriter.println(this.ineUXGyuQmP);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.dQCQlqcL);
        if (this.SYafeh != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.SYafeh);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.gnKlzvCZLmUA);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.wzLBqLQcFlu);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.RFOcVKwijOVg);
        if (this.udYZxYWU) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.udYZxYWU);
        }
        if (this.ejoYRLl != null) {
            printWriter.print(str);
            printWriter.print("  mNoTransactionsBecause=");
            printWriter.println(this.ejoYRLl);
        }
        if (this.ZcxyUcLBGcV != null && this.ZcxyUcLBGcV.size() > 0) {
            printWriter.print(str);
            printWriter.print("  mAvailIndices: ");
            printWriter.println(Arrays.toString(this.ZcxyUcLBGcV.toArray()));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.ekglxtiyel.ekglxtiyel.sznRcHTe.JyKmOjX(boolean, android.support.ekglxtiyel.ekglxtiyel.wSMjih, android.util.SparseArray, android.util.SparseArray):android.support.ekglxtiyel.ekglxtiyel.wSMjih
     arg types: [int, ?[OBJECT, ARRAY], android.util.SparseArray, android.util.SparseArray]
     candidates:
      android.support.ekglxtiyel.ekglxtiyel.sznRcHTe.JyKmOjX(android.support.ekglxtiyel.ekglxtiyel.sznRcHTe, android.support.ekglxtiyel.ekglxtiyel.wSMjih, boolean, android.support.ekglxtiyel.ekglxtiyel.hBYomwKY):android.support.ekglxtiyel.wrizuodpud.lFKFPcMiJsI
      android.support.ekglxtiyel.ekglxtiyel.sznRcHTe.JyKmOjX(int, android.support.ekglxtiyel.ekglxtiyel.hBYomwKY, java.lang.String, int):void
      android.support.ekglxtiyel.ekglxtiyel.sznRcHTe.JyKmOjX(android.support.ekglxtiyel.ekglxtiyel.sznRcHTe, android.support.ekglxtiyel.ekglxtiyel.wSMjih, int, java.lang.Object):void
      android.support.ekglxtiyel.ekglxtiyel.sznRcHTe.JyKmOjX(android.view.View, android.support.ekglxtiyel.ekglxtiyel.wSMjih, int, java.lang.Object):void
      android.support.ekglxtiyel.ekglxtiyel.sznRcHTe.JyKmOjX(int, int, int, int):android.support.ekglxtiyel.ekglxtiyel.KLCcZEc
      android.support.ekglxtiyel.ekglxtiyel.sznRcHTe.JyKmOjX(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.ekglxtiyel.ekglxtiyel.KLCcZEc.JyKmOjX(int, int, int, int):android.support.ekglxtiyel.ekglxtiyel.KLCcZEc
      android.support.ekglxtiyel.ekglxtiyel.sznRcHTe.JyKmOjX(boolean, android.support.ekglxtiyel.ekglxtiyel.wSMjih, android.util.SparseArray, android.util.SparseArray):android.support.ekglxtiyel.ekglxtiyel.wSMjih */
    /* access modifiers changed from: package-private */
    public boolean JyKmOjX(Handler handler, String str, int i, int i2) {
        int i3;
        if (this.IsLoypLqxg == null) {
            return UxnFzZ;
        }
        if (str == null && i < 0 && (i2 & 1) == 0) {
            int size = this.IsLoypLqxg.size() - 1;
            if (size < 0) {
                return UxnFzZ;
            }
            sznRcHTe sznrchte = (sznRcHTe) this.IsLoypLqxg.clear(size);
            SparseArray sparseArray = new SparseArray();
            SparseArray sparseArray2 = new SparseArray();
            sznrchte.JyKmOjX(sparseArray, sparseArray2);
            sznrchte.JyKmOjX(true, (wSMjih) null, sparseArray, sparseArray2);
            ZVIDXbvWn();
        } else {
            int i4 = -1;
            if (str != null || i >= 0) {
                int size2 = this.IsLoypLqxg.size() - 1;
                while (i3 >= 0) {
                    sznRcHTe sznrchte2 = (sznRcHTe) this.IsLoypLqxg.get(i3);
                    if ((str != null && str.equals(sznrchte2.uvKYrIFVv())) || (i >= 0 && i == sznrchte2.ADojbWgjJep)) {
                        break;
                    }
                    size2 = i3 - 1;
                }
                if (i3 < 0) {
                    return UxnFzZ;
                }
                if ((i2 & 1) != 0) {
                    i3--;
                    while (i3 >= 0) {
                        sznRcHTe sznrchte3 = (sznRcHTe) this.IsLoypLqxg.get(i3);
                        if ((str == null || !str.equals(sznrchte3.uvKYrIFVv())) && (i < 0 || i != sznrchte3.ADojbWgjJep)) {
                            break;
                        }
                        i3--;
                    }
                }
                i4 = i3;
            }
            if (i4 == this.IsLoypLqxg.size() - 1) {
                return UxnFzZ;
            }
            ArrayList arrayList = new ArrayList();
            for (int size3 = this.IsLoypLqxg.size() - 1; size3 > i4; size3--) {
                arrayList.add(this.IsLoypLqxg.clear(size3));
            }
            int size4 = arrayList.size() - 1;
            SparseArray sparseArray3 = new SparseArray();
            SparseArray sparseArray4 = new SparseArray();
            for (int i5 = 0; i5 <= size4; i5++) {
                ((sznRcHTe) arrayList.get(i5)).JyKmOjX(sparseArray3, sparseArray4);
            }
            wSMjih wsmjih = null;
            int i6 = 0;
            while (i6 <= size4) {
                if (gWiOFio) {
                    Log.v(ELxjQaDRrI, "Popping back stack state: " + arrayList.get(i6));
                }
                i6++;
                wsmjih = ((sznRcHTe) arrayList.get(i6)).JyKmOjX(i6 == size4, wsmjih, sparseArray3, sparseArray4);
            }
            ZVIDXbvWn();
        }
        return true;
    }

    public boolean JyKmOjX(Menu menu) {
        if (this.RBZUWx == null) {
            return UxnFzZ;
        }
        boolean z = false;
        for (int i = 0; i < this.RBZUWx.size(); i++) {
            hBYomwKY hbyomwky = (hBYomwKY) this.RBZUWx.get(i);
            if (hbyomwky != null && hbyomwky.ELxjQaDRrI(menu)) {
                z = true;
            }
        }
        return z;
    }

    public boolean JyKmOjX(Menu menu, MenuInflater menuInflater) {
        boolean z;
        ArrayList arrayList = null;
        if (this.RBZUWx != null) {
            int i = 0;
            z = false;
            while (i < this.RBZUWx.size()) {
                hBYomwKY hbyomwky = (hBYomwKY) this.RBZUWx.get(i);
                if (hbyomwky != null && hbyomwky.gWiOFio(menu, menuInflater)) {
                    z = true;
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(hbyomwky);
                }
                i++;
                z = z;
            }
        } else {
            z = false;
        }
        if (this.DYldmviR != null) {
            for (int i2 = 0; i2 < this.DYldmviR.size(); i2++) {
                hBYomwKY hbyomwky2 = (hBYomwKY) this.DYldmviR.get(i2);
                if (arrayList == null || !arrayList.contains(hbyomwky2)) {
                    hbyomwky2.uMOUnQbdK();
                }
            }
        }
        this.DYldmviR = arrayList;
        return z;
    }

    public boolean JyKmOjX(MenuItem menuItem) {
        if (this.RBZUWx == null) {
            return UxnFzZ;
        }
        for (int i = 0; i < this.RBZUWx.size(); i++) {
            hBYomwKY hbyomwky = (hBYomwKY) this.RBZUWx.get(i);
            if (hbyomwky != null && hbyomwky.ELxjQaDRrI(menuItem)) {
                return true;
            }
        }
        return UxnFzZ;
    }

    /* access modifiers changed from: package-private */
    public void NkBWDzI() {
        if (this.TUFgsvaVkdN != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.TUFgsvaVkdN.size()) {
                    hBYomwKY hbyomwky = (hBYomwKY) this.TUFgsvaVkdN.get(i2);
                    if (hbyomwky != null) {
                        gWiOFio(hbyomwky);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public int OvRsHjLd() {
        if (this.IsLoypLqxg != null) {
            return this.IsLoypLqxg.size();
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void OvRsHjLd(hBYomwKY hbyomwky) {
        if (hbyomwky.GtjRlhyKkDco != null) {
            if (this.WlQHAGESvdRg == null) {
                this.WlQHAGESvdRg = new SparseArray();
            } else {
                this.WlQHAGESvdRg.clear();
            }
            hbyomwky.GtjRlhyKkDco.saveHierarchyState(this.WlQHAGESvdRg);
            if (this.WlQHAGESvdRg.size() > 0) {
                hbyomwky.ADojbWgjJep = this.WlQHAGESvdRg;
                this.WlQHAGESvdRg = null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Parcelable RBZUWx() {
        int[] iArr;
        int size;
        int size2;
        boolean z;
        hnkcxFMAi[] hnkcxfmaiArr = null;
        uvKYrIFVv();
        if (UxnFzZ) {
            this.wzLBqLQcFlu = true;
        }
        if (this.TUFgsvaVkdN == null || this.TUFgsvaVkdN.size() <= 0) {
            return null;
        }
        int size3 = this.TUFgsvaVkdN.size();
        yguITe[] yguiteArr = new yguITe[size3];
        int i = 0;
        boolean z2 = false;
        while (i < size3) {
            hBYomwKY hbyomwky = (hBYomwKY) this.TUFgsvaVkdN.get(i);
            if (hbyomwky != null) {
                if (hbyomwky.udYZxYWU < 0) {
                    JyKmOjX(new IllegalStateException("Failure saving state: active " + hbyomwky + " has cleared index: " + hbyomwky.udYZxYWU));
                }
                yguITe yguite = new yguITe(hbyomwky);
                yguiteArr[i] = yguite;
                if (hbyomwky.ineUXGyuQmP <= 0 || yguite.uvKYrIFVv != null) {
                    yguite.uvKYrIFVv = hbyomwky.SYafeh;
                } else {
                    yguite.uvKYrIFVv = CkytDdEwp(hbyomwky);
                    if (hbyomwky.ejoYRLl != null) {
                        if (hbyomwky.ejoYRLl.udYZxYWU < 0) {
                            JyKmOjX(new IllegalStateException("Failure saving state: " + hbyomwky + " has target not in fragment manager: " + hbyomwky.ejoYRLl));
                        }
                        if (yguite.uvKYrIFVv == null) {
                            yguite.uvKYrIFVv = new Bundle();
                        }
                        JyKmOjX(yguite.uvKYrIFVv, OvRsHjLd, hbyomwky.ejoYRLl);
                        if (hbyomwky.JPEfQkMfcXeW != 0) {
                            yguite.uvKYrIFVv.putInt(JHCbNsJ, hbyomwky.JPEfQkMfcXeW);
                        }
                    }
                }
                if (gWiOFio) {
                    Log.v(ELxjQaDRrI, "Saved state of " + hbyomwky + ": " + yguite.uvKYrIFVv);
                }
                z = true;
            } else {
                z = z2;
            }
            i++;
            z2 = z;
        }
        if (z2) {
            if (this.RBZUWx == null || (size2 = this.RBZUWx.size()) <= 0) {
                iArr = null;
            } else {
                iArr = new int[size2];
                for (int i2 = 0; i2 < size2; i2++) {
                    iArr[i2] = ((hBYomwKY) this.RBZUWx.get(i2)).udYZxYWU;
                    if (iArr[i2] < 0) {
                        JyKmOjX(new IllegalStateException("Failure saving state: active " + this.RBZUWx.get(i2) + " has cleared index: " + iArr[i2]));
                    }
                    if (gWiOFio) {
                        Log.v(ELxjQaDRrI, "saveAllState: adding fragment #" + i2 + ": " + this.RBZUWx.get(i2));
                    }
                }
            }
            if (this.IsLoypLqxg != null && (size = this.IsLoypLqxg.size()) > 0) {
                hnkcxfmaiArr = new hnkcxFMAi[size];
                for (int i3 = 0; i3 < size; i3++) {
                    hnkcxfmaiArr[i3] = new hnkcxFMAi((sznRcHTe) this.IsLoypLqxg.get(i3));
                    if (gWiOFio) {
                        Log.v(ELxjQaDRrI, "saveAllState: adding back stack #" + i3 + ": " + this.IsLoypLqxg.get(i3));
                    }
                }
            }
            PiPrCiJYa piPrCiJYa = new PiPrCiJYa();
            piPrCiJYa.JyKmOjX = yguiteArr;
            piPrCiJYa.gWiOFio = iArr;
            piPrCiJYa.ELxjQaDRrI = hnkcxfmaiArr;
            return piPrCiJYa;
        } else if (!gWiOFio) {
            return null;
        } else {
            Log.v(ELxjQaDRrI, "saveAllState: no fragments!");
            return null;
        }
    }

    public void SYafeh() {
        if (this.RBZUWx != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.RBZUWx.size()) {
                    hBYomwKY hbyomwky = (hBYomwKY) this.RBZUWx.get(i2);
                    if (hbyomwky != null) {
                        hbyomwky.ydeoLafXzx();
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList TUFgsvaVkdN() {
        ArrayList arrayList = null;
        if (this.TUFgsvaVkdN != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.TUFgsvaVkdN.size()) {
                    break;
                }
                hBYomwKY hbyomwky = (hBYomwKY) this.TUFgsvaVkdN.get(i2);
                if (hbyomwky != null && hbyomwky.rpGpZhiREoO) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(hbyomwky);
                    hbyomwky.CwmNEYbDE = true;
                    hbyomwky.VzWbdN = hbyomwky.ejoYRLl != null ? hbyomwky.ejoYRLl.udYZxYWU : -1;
                    if (gWiOFio) {
                        Log.v(ELxjQaDRrI, "retainNonConfig: keeping retained " + hbyomwky);
                    }
                }
                i = i2 + 1;
            }
        }
        return arrayList;
    }

    public void UxnFzZ() {
        JyKmOjX(new pLAJYQs(this), (boolean) UxnFzZ);
    }

    /* access modifiers changed from: package-private */
    public void UxnFzZ(hBYomwKY hbyomwky) {
        if (hbyomwky.udYZxYWU < 0) {
            if (this.ZcxyUcLBGcV == null || this.ZcxyUcLBGcV.size() <= 0) {
                if (this.TUFgsvaVkdN == null) {
                    this.TUFgsvaVkdN = new ArrayList();
                }
                hbyomwky.JyKmOjX(this.TUFgsvaVkdN.size(), this.SYafeh);
                this.TUFgsvaVkdN.add(hbyomwky);
            } else {
                hbyomwky.JyKmOjX(((Integer) this.ZcxyUcLBGcV.clear(this.ZcxyUcLBGcV.size() - 1)).intValue(), this.SYafeh);
                this.TUFgsvaVkdN.set(hbyomwky.udYZxYWU, hbyomwky);
            }
            if (gWiOFio) {
                Log.v(ELxjQaDRrI, "Allocated fragment index " + hbyomwky);
            }
        }
    }

    public void UxnFzZ(hBYomwKY hbyomwky, int i, int i2) {
        if (gWiOFio) {
            Log.v(ELxjQaDRrI, "detach: " + hbyomwky);
        }
        if (!hbyomwky.GdBxFlWFwtk) {
            hbyomwky.GdBxFlWFwtk = true;
            if (hbyomwky.WlQHAGESvdRg) {
                if (this.RBZUWx != null) {
                    if (gWiOFio) {
                        Log.v(ELxjQaDRrI, "remove from detach: " + hbyomwky);
                    }
                    this.RBZUWx.clear(hbyomwky);
                }
                if (hbyomwky.YRnebgHUCjYV && hbyomwky.iaMEMmG) {
                    this.udYZxYWU = true;
                }
                hbyomwky.WlQHAGESvdRg = UxnFzZ;
                JyKmOjX(hbyomwky, 1, i, i2, (boolean) UxnFzZ);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ZVIDXbvWn() {
        if (this.nHLQZwbUt != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.nHLQZwbUt.size()) {
                    ((UyLYGdXPJIC) this.nHLQZwbUt.get(i2)).JyKmOjX();
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void ZcxyUcLBGcV() {
        this.wzLBqLQcFlu = UxnFzZ;
    }

    public void clabDdWhMqLI() {
        JyKmOjX(1, (boolean) UxnFzZ);
    }

    public void dQCQlqcL() {
        this.RFOcVKwijOVg = true;
        uvKYrIFVv();
        JyKmOjX(0, (boolean) UxnFzZ);
        this.ineUXGyuQmP = null;
        this.dQCQlqcL = null;
        this.SYafeh = null;
    }

    public hBYomwKY gWiOFio(String str) {
        hBYomwKY gWiOFio2;
        if (!(this.TUFgsvaVkdN == null || str == null)) {
            for (int size = this.TUFgsvaVkdN.size() - 1; size >= 0; size--) {
                hBYomwKY hbyomwky = (hBYomwKY) this.TUFgsvaVkdN.get(size);
                if (hbyomwky != null && (gWiOFio2 = hbyomwky.gWiOFio(str)) != null) {
                    return gWiOFio2;
                }
            }
        }
        return null;
    }

    public pMhcTV gWiOFio(int i) {
        return (pMhcTV) this.IsLoypLqxg.get(i);
    }

    public void gWiOFio(UyLYGdXPJIC uyLYGdXPJIC) {
        if (this.nHLQZwbUt != null) {
            this.nHLQZwbUt.clear(uyLYGdXPJIC);
        }
    }

    public void gWiOFio(hBYomwKY hbyomwky) {
        if (!hbyomwky.GSaTJAXLDK) {
            return;
        }
        if (this.ZVIDXbvWn) {
            this.VzWbdN = true;
            return;
        }
        hbyomwky.GSaTJAXLDK = UxnFzZ;
        JyKmOjX(hbyomwky, this.gnKlzvCZLmUA, 0, 0, (boolean) UxnFzZ);
    }

    public void gWiOFio(hBYomwKY hbyomwky, int i, int i2) {
        if (gWiOFio) {
            Log.v(ELxjQaDRrI, "hide: " + hbyomwky);
        }
        if (!hbyomwky.aZEOMfcrkmbI) {
            hbyomwky.aZEOMfcrkmbI = true;
            if (hbyomwky.wGguZKYQ != null) {
                Animation JyKmOjX = JyKmOjX(hbyomwky, i, (boolean) UxnFzZ, i2);
                if (JyKmOjX != null) {
                    gWiOFio(hbyomwky.wGguZKYQ, JyKmOjX);
                    hbyomwky.wGguZKYQ.startAnimation(JyKmOjX);
                }
                hbyomwky.wGguZKYQ.setVisibility(8);
            }
            if (hbyomwky.WlQHAGESvdRg && hbyomwky.YRnebgHUCjYV && hbyomwky.iaMEMmG) {
                this.udYZxYWU = true;
            }
            hbyomwky.UxnFzZ(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void gWiOFio(sznRcHTe sznrchte) {
        if (this.IsLoypLqxg == null) {
            this.IsLoypLqxg = new ArrayList();
        }
        this.IsLoypLqxg.add(sznrchte);
        ZVIDXbvWn();
    }

    public void gWiOFio(Menu menu) {
        if (this.RBZUWx != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.RBZUWx.size()) {
                    hBYomwKY hbyomwky = (hBYomwKY) this.RBZUWx.get(i2);
                    if (hbyomwky != null) {
                        hbyomwky.UxnFzZ(menu);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public boolean gWiOFio(int i, int i2) {
        udYZxYWU();
        ELxjQaDRrI();
        if (i >= 0) {
            return JyKmOjX(this.ineUXGyuQmP.uvKYrIFVv(), (String) null, i, i2);
        }
        throw new IllegalArgumentException("Bad id: " + i);
    }

    public boolean gWiOFio(MenuItem menuItem) {
        if (this.RBZUWx == null) {
            return UxnFzZ;
        }
        for (int i = 0; i < this.RBZUWx.size(); i++) {
            hBYomwKY hbyomwky = (hBYomwKY) this.RBZUWx.get(i);
            if (hbyomwky != null && hbyomwky.UxnFzZ(menuItem)) {
                return true;
            }
        }
        return UxnFzZ;
    }

    public boolean gWiOFio(String str, int i) {
        udYZxYWU();
        ELxjQaDRrI();
        return JyKmOjX(this.ineUXGyuQmP.uvKYrIFVv(), str, -1, i);
    }

    public void gnKlzvCZLmUA() {
        this.wzLBqLQcFlu = true;
        JyKmOjX(3, (boolean) UxnFzZ);
    }

    public void ineUXGyuQmP() {
        JyKmOjX(2, (boolean) UxnFzZ);
    }

    public void nHLQZwbUt() {
        JyKmOjX(4, (boolean) UxnFzZ);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((int) woewfJyZrgET.RBZUWx);
        sb.append("FragmentManager{");
        sb.append(Integer.hexing(System.uniqueHCode(this)));
        sb.append(" in ");
        if (this.SYafeh != null) {
            YuxfPhPIaOM.JyKmOjX(this.SYafeh, sb);
        } else {
            YuxfPhPIaOM.JyKmOjX(this.ineUXGyuQmP, sb);
        }
        sb.append("}}");
        return sb.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0087, code lost:
        r6.ZVIDXbvWn = true;
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x008a, code lost:
        if (r1 >= r3) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x008c, code lost:
        r6.uvKYrIFVv[r1].run();
        r6.uvKYrIFVv[r1] = null;
        r1 = r1 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean uvKYrIFVv() {
        /*
            r6 = this;
            r0 = 1
            r2 = 0
            boolean r1 = r6.ZVIDXbvWn
            if (r1 == 0) goto L_0x000e
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Recursive entry to executePendingTransactions"
            r0.<init>(r1)
            throw r0
        L_0x000e:
            android.os.Looper r1 = android.os.Looper.myLooper()
            android.support.ekglxtiyel.ekglxtiyel.MTZVRQn r3 = r6.ineUXGyuQmP
            android.os.Handler r3 = r3.uvKYrIFVv()
            android.os.Looper r3 = r3.getLooper()
            if (r1 == r3) goto L_0x0026
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Must be called from main thread of process"
            r0.<init>(r1)
            throw r0
        L_0x0026:
            r1 = r2
        L_0x0027:
            monitor-enter(r6)
            java.util.ArrayList r3 = r6.NkBWDzI     // Catch:{ all -> 0x009b }
            if (r3 == 0) goto L_0x0034
            java.util.ArrayList r3 = r6.NkBWDzI     // Catch:{ all -> 0x009b }
            int r3 = r3.size()     // Catch:{ all -> 0x009b }
            if (r3 != 0) goto L_0x005c
        L_0x0034:
            monitor-exit(r6)     // Catch:{ all -> 0x009b }
            boolean r0 = r6.VzWbdN
            if (r0 == 0) goto L_0x00a9
            r3 = r2
            r4 = r2
        L_0x003b:
            java.util.ArrayList r0 = r6.TUFgsvaVkdN
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x00a2
            java.util.ArrayList r0 = r6.TUFgsvaVkdN
            java.lang.Object r0 = r0.get(r3)
            android.support.ekglxtiyel.ekglxtiyel.hBYomwKY r0 = (android.support.ekglxtiyel.ekglxtiyel.hBYomwKY) r0
            if (r0 == 0) goto L_0x0058
            android.support.ekglxtiyel.ekglxtiyel.JQnsJjHwnBt r5 = r0.wsRFIu
            if (r5 == 0) goto L_0x0058
            android.support.ekglxtiyel.ekglxtiyel.JQnsJjHwnBt r0 = r0.wsRFIu
            boolean r0 = r0.JyKmOjX()
            r4 = r4 | r0
        L_0x0058:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x003b
        L_0x005c:
            java.util.ArrayList r1 = r6.NkBWDzI     // Catch:{ all -> 0x009b }
            int r3 = r1.size()     // Catch:{ all -> 0x009b }
            java.lang.Runnable[] r1 = r6.uvKYrIFVv     // Catch:{ all -> 0x009b }
            if (r1 == 0) goto L_0x006b
            java.lang.Runnable[] r1 = r6.uvKYrIFVv     // Catch:{ all -> 0x009b }
            int r1 = r1.length     // Catch:{ all -> 0x009b }
            if (r1 >= r3) goto L_0x006f
        L_0x006b:
            java.lang.Runnable[] r1 = new java.lang.Runnable[r3]     // Catch:{ all -> 0x009b }
            r6.uvKYrIFVv = r1     // Catch:{ all -> 0x009b }
        L_0x006f:
            java.util.ArrayList r1 = r6.NkBWDzI     // Catch:{ all -> 0x009b }
            java.lang.Runnable[] r4 = r6.uvKYrIFVv     // Catch:{ all -> 0x009b }
            r1.toArray(r4)     // Catch:{ all -> 0x009b }
            java.util.ArrayList r1 = r6.NkBWDzI     // Catch:{ all -> 0x009b }
            r1.clear()     // Catch:{ all -> 0x009b }
            android.support.ekglxtiyel.ekglxtiyel.MTZVRQn r1 = r6.ineUXGyuQmP     // Catch:{ all -> 0x009b }
            android.os.Handler r1 = r1.uvKYrIFVv()     // Catch:{ all -> 0x009b }
            java.lang.Runnable r4 = r6.VYBKAHZ     // Catch:{ all -> 0x009b }
            r1.removeCallbacks(r4)     // Catch:{ all -> 0x009b }
            monitor-exit(r6)     // Catch:{ all -> 0x009b }
            r6.ZVIDXbvWn = r0
            r1 = r2
        L_0x008a:
            if (r1 >= r3) goto L_0x009e
            java.lang.Runnable[] r4 = r6.uvKYrIFVv
            r4 = r4[r1]
            r4.run()
            java.lang.Runnable[] r4 = r6.uvKYrIFVv
            r5 = 0
            r4[r1] = r5
            int r1 = r1 + 1
            goto L_0x008a
        L_0x009b:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x009b }
            throw r0
        L_0x009e:
            r6.ZVIDXbvWn = r2
            r1 = r0
            goto L_0x0027
        L_0x00a2:
            if (r4 != 0) goto L_0x00a9
            r6.VzWbdN = r2
            r6.NkBWDzI()
        L_0x00a9:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.ekglxtiyel.ekglxtiyel.EgicqB.uvKYrIFVv():boolean");
    }

    public void wqpspf() {
        this.wzLBqLQcFlu = UxnFzZ;
        JyKmOjX(4, (boolean) UxnFzZ);
    }
}
