package android.support.ekglxtiyel.ekglxtiyel;

import android.app.Notification;
import android.os.IBinder;
import android.os.Parcel;

class cEbCSMNMY implements UCugAOm {
    private IBinder JyKmOjX;

    cEbCSMNMY(IBinder iBinder) {
        this.JyKmOjX = iBinder;
    }

    public String JyKmOjX() {
        return "android.support.v4.app.INotificationSideChannel";
    }

    public void JyKmOjX(String str) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("android.support.v4.app.INotificationSideChannel");
            obtain.writeString(str);
            this.JyKmOjX.transact(3, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public void JyKmOjX(String str, int i, String str2) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("android.support.v4.app.INotificationSideChannel");
            obtain.writeString(str);
            obtain.writeInt(i);
            obtain.writeString(str2);
            this.JyKmOjX.transact(2, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public void JyKmOjX(String str, int i, String str2, Notification notification) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("android.support.v4.app.INotificationSideChannel");
            obtain.writeString(str);
            obtain.writeInt(i);
            obtain.writeString(str2);
            if (notification != null) {
                obtain.writeInt(1);
                notification.writeToParcel(obtain, 0);
            } else {
                obtain.writeInt(0);
            }
            this.JyKmOjX.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public IBinder asBinder() {
        return this.JyKmOjX;
    }
}
