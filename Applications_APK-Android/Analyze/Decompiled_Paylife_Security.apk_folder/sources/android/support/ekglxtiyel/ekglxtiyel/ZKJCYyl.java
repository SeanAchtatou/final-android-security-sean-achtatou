package android.support.ekglxtiyel.ekglxtiyel;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

public abstract class ZKJCYyl extends Service {
    /* access modifiers changed from: private */
    public void JyKmOjX(int i, String str) {
        String[] packagesForUid = getOverValue().getPackagesForUid(i);
        int length = packagesForUid.length;
        int i2 = 0;
        while (i2 < length) {
            if (!packagesForUid[i2].equals(str)) {
                i2++;
            } else {
                return;
            }
        }
        throw new SecurityException("NotificationSideChannelService: Uid " + i + " is not authorized for package " + str);
    }

    public abstract void JyKmOjX(String str);

    public abstract void JyKmOjX(String str, int i, String str2);

    public abstract void JyKmOjX(String str, int i, String str2, Notification notification);

    public IBinder onBind(Intent intent) {
        if (!intent.getAction().equals(PHvrfXRj.gWiOFio) || Build.VERSION.SDK_INT > 19) {
            return null;
        }
        return new nyeUPSn(this);
    }
}
