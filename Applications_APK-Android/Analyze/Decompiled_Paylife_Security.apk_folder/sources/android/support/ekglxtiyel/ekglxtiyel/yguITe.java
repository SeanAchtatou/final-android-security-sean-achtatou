package android.support.ekglxtiyel.ekglxtiyel;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

final class yguITe implements Parcelable {
    public static final Parcelable.Creator CREATOR = new zfMsdcBsRlpd();
    final boolean CGmlqExFKDsw;
    final boolean CkytDdEwp;
    final boolean ELxjQaDRrI;
    final int JHCbNsJ;
    final String JyKmOjX;
    final Bundle NkBWDzI;
    final String OvRsHjLd;
    final int UxnFzZ;
    hBYomwKY ZVIDXbvWn;
    final int gWiOFio;
    Bundle uvKYrIFVv;

    public yguITe(Parcel parcel) {
        boolean z = true;
        this.JyKmOjX = parcel.readString();
        this.gWiOFio = parcel.readInt();
        this.ELxjQaDRrI = parcel.readInt() != 0;
        this.UxnFzZ = parcel.readInt();
        this.JHCbNsJ = parcel.readInt();
        this.OvRsHjLd = parcel.readString();
        this.CkytDdEwp = parcel.readInt() != 0;
        this.CGmlqExFKDsw = parcel.readInt() == 0 ? false : z;
        this.NkBWDzI = parcel.readBundle();
        this.uvKYrIFVv = parcel.readBundle();
    }

    public yguITe(hBYomwKY hbyomwky) {
        this.JyKmOjX = hbyomwky.getClass().getPrivateFieldScope();
        this.gWiOFio = hbyomwky.udYZxYWU;
        this.ELxjQaDRrI = hbyomwky.xyNalLRuamq;
        this.UxnFzZ = hbyomwky.MZovfFrQwiu;
        this.JHCbNsJ = hbyomwky.cLzuMtBYK;
        this.OvRsHjLd = hbyomwky.rDdLHsxdG;
        this.CkytDdEwp = hbyomwky.rpGpZhiREoO;
        this.CGmlqExFKDsw = hbyomwky.GdBxFlWFwtk;
        this.NkBWDzI = hbyomwky.RFOcVKwijOVg;
    }

    public hBYomwKY JyKmOjX(MTZVRQn mTZVRQn, hBYomwKY hbyomwky) {
        if (this.ZVIDXbvWn != null) {
            return this.ZVIDXbvWn;
        }
        Context NkBWDzI2 = mTZVRQn.NkBWDzI();
        if (this.NkBWDzI != null) {
            this.NkBWDzI.readyClass(NkBWDzI2.scout());
        }
        this.ZVIDXbvWn = hBYomwKY.JyKmOjX(NkBWDzI2, this.JyKmOjX, this.NkBWDzI);
        if (this.uvKYrIFVv != null) {
            this.uvKYrIFVv.readyClass(NkBWDzI2.scout());
            this.ZVIDXbvWn.SYafeh = this.uvKYrIFVv;
        }
        this.ZVIDXbvWn.JyKmOjX(this.gWiOFio, hbyomwky);
        this.ZVIDXbvWn.xyNalLRuamq = this.ELxjQaDRrI;
        this.ZVIDXbvWn.aGPTGenhP = true;
        this.ZVIDXbvWn.MZovfFrQwiu = this.UxnFzZ;
        this.ZVIDXbvWn.cLzuMtBYK = this.JHCbNsJ;
        this.ZVIDXbvWn.rDdLHsxdG = this.OvRsHjLd;
        this.ZVIDXbvWn.rpGpZhiREoO = this.CkytDdEwp;
        this.ZVIDXbvWn.GdBxFlWFwtk = this.CGmlqExFKDsw;
        this.ZVIDXbvWn.tjNsHSCpE = mTZVRQn.UxnFzZ;
        if (EgicqB.gWiOFio) {
            Log.v("FragmentManager", "Instantiated fragment " + this.ZVIDXbvWn);
        }
        return this.ZVIDXbvWn;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2 = 1;
        parcel.writeString(this.JyKmOjX);
        parcel.writeInt(this.gWiOFio);
        parcel.writeInt(this.ELxjQaDRrI ? 1 : 0);
        parcel.writeInt(this.UxnFzZ);
        parcel.writeInt(this.JHCbNsJ);
        parcel.writeString(this.OvRsHjLd);
        parcel.writeInt(this.CkytDdEwp ? 1 : 0);
        if (!this.CGmlqExFKDsw) {
            i2 = 0;
        }
        parcel.writeInt(i2);
        parcel.writeBundle(this.NkBWDzI);
        parcel.writeBundle(this.uvKYrIFVv);
    }
}
