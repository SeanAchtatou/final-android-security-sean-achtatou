package android.support.ekglxtiyel.ekglxtiyel;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import java.util.HashSet;
import java.util.Set;

public class PHvrfXRj {
    /* access modifiers changed from: private */
    public static final int CGmlqExFKDsw = DYldmviR.JyKmOjX();
    private static final String CkytDdEwp = "created_notify_listeners";
    private static final ezcMVyVzVGv DYldmviR;
    static final int ELxjQaDRrI = 19;
    private static QzdaVju IsLoypLqxg = null;
    private static final int JHCbNsJ = 1000;
    public static final String JyKmOjX = "android.support.useSideChannel";
    private static final Object NkBWDzI = new Object();
    private static final int OvRsHjLd = 6;
    private static final String UxnFzZ = "NotifManCompat";
    private static Set ZVIDXbvWn = new HashSet();
    private static final Object ZcxyUcLBGcV = new Object();
    public static final String gWiOFio = "android.support.BIND_NOTIFICATION_SIDE_CHANNEL";
    private static String uvKYrIFVv;
    private final NotificationManager RBZUWx = ((NotificationManager) this.TUFgsvaVkdN.getSystemService("notification"));
    private final Context TUFgsvaVkdN;

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            DYldmviR = new lbtfFNvwhs();
        } else if (Build.VERSION.SDK_INT >= 5) {
            DYldmviR = new XMmDOaVgx();
        } else {
            DYldmviR = new zXKGpdVnS();
        }
    }

    private PHvrfXRj(Context context) {
        this.TUFgsvaVkdN = context;
    }

    public static PHvrfXRj JyKmOjX(Context context) {
        return new PHvrfXRj(context);
    }

    private void JyKmOjX(jfobUqIR jfobuqir) {
        synchronized (ZcxyUcLBGcV) {
            if (IsLoypLqxg == null) {
                IsLoypLqxg = new QzdaVju(this.TUFgsvaVkdN.getApplicationContext());
            }
        }
        IsLoypLqxg.JyKmOjX(jfobuqir);
    }

    private static boolean JyKmOjX(Notification notification) {
        Bundle JyKmOjX2 = woewfJyZrgET.JyKmOjX(notification);
        return JyKmOjX2 != null && JyKmOjX2.getBoolean(JyKmOjX);
    }

    public static Set gWiOFio(Context context) {
        String string = Settings.Secure.getString(context.getContentResolver(), CkytDdEwp);
        if (string != null && !string.equals(uvKYrIFVv)) {
            String[] split = string.split(":");
            HashSet hashSet = new HashSet(split.length);
            for (String unflattenFromString : split) {
                ComponentName unflattenFromString2 = ComponentName.unflattenFromString(unflattenFromString);
                if (unflattenFromString2 != null) {
                    hashSet.add(unflattenFromString2.getPackageName());
                }
            }
            synchronized (NkBWDzI) {
                ZVIDXbvWn = hashSet;
                uvKYrIFVv = string;
            }
        }
        return ZVIDXbvWn;
    }

    public void JyKmOjX() {
        this.RBZUWx.cancelAll();
        if (Build.VERSION.SDK_INT <= 19) {
            JyKmOjX(new AipZHVhwD(this.TUFgsvaVkdN.getPackageName()));
        }
    }

    public void JyKmOjX(int i) {
        JyKmOjX((String) null, i);
    }

    public void JyKmOjX(int i, Notification notification) {
        JyKmOjX(null, i, notification);
    }

    public void JyKmOjX(String str, int i) {
        DYldmviR.JyKmOjX(this.RBZUWx, str, i);
        if (Build.VERSION.SDK_INT <= 19) {
            JyKmOjX(new AipZHVhwD(this.TUFgsvaVkdN.getPackageName(), i, str));
        }
    }

    public void JyKmOjX(String str, int i, Notification notification) {
        if (JyKmOjX(notification)) {
            JyKmOjX(new YkDjAprOSoan(this.TUFgsvaVkdN.getPackageName(), i, str, notification));
            DYldmviR.JyKmOjX(this.RBZUWx, str, i);
            return;
        }
        DYldmviR.JyKmOjX(this.RBZUWx, str, i, notification);
    }
}
