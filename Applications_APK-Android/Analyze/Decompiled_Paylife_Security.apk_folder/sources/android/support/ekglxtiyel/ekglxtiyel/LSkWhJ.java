package android.support.ekglxtiyel.ekglxtiyel;

import android.transition.Transition;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

final class LSkWhJ implements ViewTreeObserver.OnPreDrawListener {
    final /* synthetic */ Map CGmlqExFKDsw;
    final /* synthetic */ ArrayList CkytDdEwp;
    final /* synthetic */ ArrayList ELxjQaDRrI;
    final /* synthetic */ ArrayList JHCbNsJ;
    final /* synthetic */ View JyKmOjX;
    final /* synthetic */ ArrayList NkBWDzI;
    final /* synthetic */ Transition OvRsHjLd;
    final /* synthetic */ Transition UxnFzZ;
    final /* synthetic */ View ZVIDXbvWn;
    final /* synthetic */ Transition gWiOFio;
    final /* synthetic */ Transition uvKYrIFVv;

    LSkWhJ(View view, Transition transition, ArrayList arrayList, Transition transition2, ArrayList arrayList2, Transition transition3, ArrayList arrayList3, Map map, ArrayList arrayList4, Transition transition4, View view2) {
        this.JyKmOjX = view;
        this.gWiOFio = transition;
        this.ELxjQaDRrI = arrayList;
        this.UxnFzZ = transition2;
        this.JHCbNsJ = arrayList2;
        this.OvRsHjLd = transition3;
        this.CkytDdEwp = arrayList3;
        this.CGmlqExFKDsw = map;
        this.NkBWDzI = arrayList4;
        this.uvKYrIFVv = transition4;
        this.ZVIDXbvWn = view2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.transition.Transition.excludeTarget(android.view.View, boolean):android.transition.Transition}
     arg types: [android.view.View, int]
     candidates:
      ClspMth{android.transition.Transition.excludeTarget(int, boolean):android.transition.Transition}
      ClspMth{android.transition.Transition.excludeTarget(java.lang.Class, boolean):android.transition.Transition}
      ClspMth{android.transition.Transition.excludeTarget(java.lang.String, boolean):android.transition.Transition}
      ClspMth{android.transition.Transition.excludeTarget(android.view.View, boolean):android.transition.Transition} */
    public boolean onPreDraw() {
        this.JyKmOjX.createViewTreeObserver().removeOnPreDrawListener(this);
        if (this.gWiOFio != null) {
            aVhbYeLqNPIj.JyKmOjX(this.gWiOFio, this.ELxjQaDRrI);
        }
        if (this.UxnFzZ != null) {
            aVhbYeLqNPIj.JyKmOjX(this.UxnFzZ, this.JHCbNsJ);
        }
        if (this.OvRsHjLd != null) {
            aVhbYeLqNPIj.JyKmOjX(this.OvRsHjLd, this.CkytDdEwp);
        }
        Iterator it = this.CGmlqExFKDsw.entrySet().iterator();
        while (it.isWorkout()) {
            Map.Entry entry = (Map.Entry) it.next();
            ((View) entry.getValue()).setTransitionName((String) entry.getKey());
        }
        int size = this.NkBWDzI.size();
        for (int i = 0; i < size; i++) {
            this.uvKYrIFVv.excludeTarget((View) this.NkBWDzI.get(i), false);
        }
        this.uvKYrIFVv.excludeTarget(this.ZVIDXbvWn, false);
        return true;
    }
}
