package android.support.ekglxtiyel.ekglxtiyel;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Build;
import android.support.ekglxtiyel.lzcewhsnchqg.wSMjih;

class JlIJBcH extends InsetDrawable implements Drawable.Callback {
    private final Rect ELxjQaDRrI;
    private float JHCbNsJ;
    final /* synthetic */ lFKFPcMiJsI JyKmOjX;
    private float UxnFzZ;
    private final boolean gWiOFio;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private JlIJBcH(lFKFPcMiJsI lfkfpcmijsi, Drawable drawable) {
        super(drawable, 0);
        boolean z = false;
        this.JyKmOjX = lfkfpcmijsi;
        this.gWiOFio = Build.VERSION.SDK_INT > 18 ? true : z;
        this.ELxjQaDRrI = new Rect();
    }

    public float JyKmOjX() {
        return this.UxnFzZ;
    }

    public void JyKmOjX(float f) {
        this.UxnFzZ = f;
        invalidateSelf();
    }

    public void draw(Canvas canvas) {
        int i = 1;
        copyBounds(this.ELxjQaDRrI);
        canvas.save();
        boolean z = wSMjih.ELxjQaDRrI(this.JyKmOjX.UxnFzZ.getWindow().getDecorView()) == 1;
        if (z) {
            i = -1;
        }
        int width = this.ELxjQaDRrI.width();
        canvas.translate(((float) i) * (-this.JHCbNsJ) * ((float) width) * this.UxnFzZ, 0.0f);
        if (z && !this.gWiOFio) {
            canvas.translate((float) width, 0.0f);
            canvas.scale(-1.0f, 1.0f);
        }
        super.draw(canvas);
        canvas.restore();
    }

    public void gWiOFio(float f) {
        this.JHCbNsJ = f;
        invalidateSelf();
    }
}
