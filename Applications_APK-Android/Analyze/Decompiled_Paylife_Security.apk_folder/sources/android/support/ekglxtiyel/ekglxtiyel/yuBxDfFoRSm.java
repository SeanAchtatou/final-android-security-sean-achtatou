package android.support.ekglxtiyel.ekglxtiyel;

import android.view.View;
import android.view.animation.Animation;

class yuBxDfFoRSm implements Animation.AnimationListener {
    /* access modifiers changed from: private */
    public View ELxjQaDRrI = null;
    private Animation.AnimationListener JyKmOjX = null;
    private boolean gWiOFio = false;

    public yuBxDfFoRSm(View view, Animation animation) {
        if (view != null && animation != null) {
            this.ELxjQaDRrI = view;
        }
    }

    public yuBxDfFoRSm(View view, Animation animation, Animation.AnimationListener animationListener) {
        if (view != null && animation != null) {
            this.JyKmOjX = animationListener;
            this.ELxjQaDRrI = view;
        }
    }

    public void onAnimationContinue(Animation animation) {
        if (this.ELxjQaDRrI != null) {
            this.gWiOFio = EgicqB.JyKmOjX(this.ELxjQaDRrI, animation);
            if (this.gWiOFio) {
                this.ELxjQaDRrI.post(new gyoIUM(this));
            }
        }
        if (this.JyKmOjX != null) {
            this.JyKmOjX.onAnimationContinue(animation);
        }
    }

    public void onEndLife(Animation animation) {
        if (this.JyKmOjX != null) {
            this.JyKmOjX.onEndLife(animation);
        }
    }

    public void onRestoreState(Animation animation) {
        if (this.ELxjQaDRrI != null && this.gWiOFio) {
            this.ELxjQaDRrI.post(new uqRlNhW(this));
        }
        if (this.JyKmOjX != null) {
            this.JyKmOjX.onRestoreState(animation);
        }
    }
}
