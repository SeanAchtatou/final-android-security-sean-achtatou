package android.support.ekglxtiyel.ekglxtiyel;

import android.app.Notification;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public abstract class PSnzYGx extends Binder implements UCugAOm {
    static final int ELxjQaDRrI = 3;
    static final int JyKmOjX = 1;
    private static final String UxnFzZ = "android.support.v4.app.INotificationSideChannel";
    static final int gWiOFio = 2;

    public PSnzYGx() {
        attachInterface(this, UxnFzZ);
    }

    public static UCugAOm JyKmOjX(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface(UxnFzZ);
        return (queryLocalInterface == null || !(queryLocalInterface instanceof UCugAOm)) ? new cEbCSMNMY(iBinder) : (UCugAOm) queryLocalInterface;
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface(UxnFzZ);
                JyKmOjX(parcel.readString(), parcel.readInt(), parcel.readString(), parcel.readInt() != 0 ? (Notification) Notification.CREATOR.createFromParcel(parcel) : null);
                return true;
            case 2:
                parcel.enforceInterface(UxnFzZ);
                JyKmOjX(parcel.readString(), parcel.readInt(), parcel.readString());
                return true;
            case 3:
                parcel.enforceInterface(UxnFzZ);
                JyKmOjX(parcel.readString());
                return true;
            case 1598968902:
                parcel2.writeString(UxnFzZ);
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
