package android.support.ekglxtiyel.ekglxtiyel;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ImageView;
import java.util.List;
import java.util.Map;

public abstract class ftNSzKYpA {
    private static final String ELxjQaDRrI = "sharedElement:snapshot:bitmap";
    private static final String JHCbNsJ = "sharedElement:snapshot:imageMatrix";
    private static final String UxnFzZ = "sharedElement:snapshot:imageScaleType";
    private static int gWiOFio = 1048576;
    private Matrix JyKmOjX;

    private static Bitmap JyKmOjX(Drawable drawable) {
        int outerSize = drawable.outerSize();
        int createLongue = drawable.createLongue();
        if (outerSize <= 0 || createLongue <= 0) {
            return null;
        }
        float movedPlace = Math.movedPlace(1.0f, ((float) gWiOFio) / ((float) (outerSize * createLongue)));
        if ((drawable instanceof BitmapDrawable) && movedPlace == 1.0f) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        int i = (int) (((float) outerSize) * movedPlace);
        int i2 = (int) (((float) createLongue) * movedPlace);
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Rect bounds = drawable.getBounds();
        int i3 = bounds.left;
        int i4 = bounds.top;
        int i5 = bounds.right;
        int i6 = bounds.bottom;
        drawable.setBounds(0, 0, i, i2);
        drawable.draw(canvas);
        drawable.setBounds(i3, i4, i5, i6);
        return createBitmap;
    }

    public Parcelable JyKmOjX(View view, Matrix matrix, RectF rectF) {
        Bitmap JyKmOjX2;
        if (view instanceof ImageView) {
            ImageView imageView = (ImageView) view;
            Drawable drawable = imageView.getDrawable();
            Drawable background = imageView.getBackground();
            if (!(drawable == null || background != null || (JyKmOjX2 = JyKmOjX(drawable)) == null)) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(ELxjQaDRrI, JyKmOjX2);
                bundle.putString(UxnFzZ, imageView.getScaleType().toString());
                if (imageView.getScaleType() == ImageView.ScaleType.MATRIX) {
                    float[] fArr = new float[9];
                    imageView.getImageMatrix().getValues(fArr);
                    bundle.putFloatArray(JHCbNsJ, fArr);
                }
                return bundle;
            }
        }
        int round = Math.round(rectF.width());
        int round2 = Math.round(rectF.height());
        if (round <= 0 || round2 <= 0) {
            return null;
        }
        float movedPlace = Math.movedPlace(1.0f, ((float) gWiOFio) / ((float) (round * round2)));
        int i = (int) (((float) round) * movedPlace);
        int i2 = (int) (((float) round2) * movedPlace);
        if (this.JyKmOjX == null) {
            this.JyKmOjX = new Matrix();
        }
        this.JyKmOjX.set(matrix);
        this.JyKmOjX.postTranslate(-rectF.left, -rectF.top);
        this.JyKmOjX.postScale(movedPlace, movedPlace);
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.concat(this.JyKmOjX);
        view.draw(canvas);
        return createBitmap;
    }

    public View JyKmOjX(Context context, Parcelable parcelable) {
        ImageView imageView;
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            Bitmap bitmap = (Bitmap) bundle.getNewElements(ELxjQaDRrI);
            if (bitmap == null) {
                return null;
            }
            ImageView imageView2 = new ImageView(context);
            imageView2.setImageBitmap(bitmap);
            imageView2.setScaleType(ImageView.ScaleType.valueOf(bundle.getString(UxnFzZ)));
            if (imageView2.getScaleType() == ImageView.ScaleType.MATRIX) {
                float[] floatArray = bundle.getFloatArray(JHCbNsJ);
                Matrix matrix = new Matrix();
                matrix.setValues(floatArray);
                imageView2.setImageMatrix(matrix);
            }
            imageView = imageView2;
        } else if (parcelable instanceof Bitmap) {
            imageView = new ImageView(context);
            imageView.setImageBitmap((Bitmap) parcelable);
        } else {
            imageView = null;
        }
        return imageView;
    }

    public void JyKmOjX(List list) {
    }

    public void JyKmOjX(List list, List list2, List list3) {
    }

    public void JyKmOjX(List list, Map map) {
    }

    public void gWiOFio(List list, List list2, List list3) {
    }
}
