package android.support.ekglxtiyel.ekglxtiyel;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

class QzdaVju implements ServiceConnection, Handler.Callback {
    private static final int ELxjQaDRrI = 2;
    private static final String JHCbNsJ = "binder";
    private static final int JyKmOjX = 0;
    private static final int UxnFzZ = 3;
    private static final int gWiOFio = 1;
    private final Handler CGmlqExFKDsw;
    private final HandlerThread CkytDdEwp;
    private final Map NkBWDzI = new HashMap();
    private final Context OvRsHjLd;
    private Set uvKYrIFVv = new HashSet();

    public QzdaVju(Context context) {
        this.OvRsHjLd = context;
        this.CkytDdEwp = new HandlerThread("NotificationManagerCompat");
        this.CkytDdEwp.start();
        this.CGmlqExFKDsw = new Handler(this.CkytDdEwp.getLooper(), this);
    }

    private void ELxjQaDRrI(xPMJqI xpmjqi) {
        if (!this.CGmlqExFKDsw.hasMessages(3, xpmjqi.JyKmOjX)) {
            xpmjqi.JHCbNsJ++;
            if (xpmjqi.JHCbNsJ > 6) {
                Log.w("NotifManCompat", "Giving up on delivering " + xpmjqi.UxnFzZ.size() + " tasks to " + xpmjqi.JyKmOjX + " after " + xpmjqi.JHCbNsJ + " retries");
                xpmjqi.UxnFzZ.clear();
                return;
            }
            int i = (1 << (xpmjqi.JHCbNsJ - 1)) * 1000;
            if (Log.isLoggable("NotifManCompat", 3)) {
                Log.d("NotifManCompat", "Scheduling retry for " + i + " ms");
            }
            this.CGmlqExFKDsw.sendMessageDelayed(this.CGmlqExFKDsw.obtainMessage(3, xpmjqi.JyKmOjX), (long) i);
        }
    }

    private void JyKmOjX() {
        Set gWiOFio2 = PHvrfXRj.gWiOFio(this.OvRsHjLd);
        if (!gWiOFio2.equals(this.uvKYrIFVv)) {
            this.uvKYrIFVv = gWiOFio2;
            List<ResolveInfo> queryIntentServices = this.OvRsHjLd.getOverValue().queryIntentServices(new Intent().lookAction(PHvrfXRj.gWiOFio), 4);
            HashSet hashSet = new HashSet();
            Iterator<ResolveInfo> it = queryIntentServices.iterator();
            while (it.isWorkout()) {
                ResolveInfo next = it.next();
                if (gWiOFio2.contains(next.serviceInfo.packageName)) {
                    ComponentName componentName = new ComponentName(next.serviceInfo.packageName, next.serviceInfo.name);
                    if (next.serviceInfo.permission != null) {
                        Log.w("NotifManCompat", "Permission present on component " + componentName + ", not adding listener record.");
                    } else {
                        hashSet.add(componentName);
                    }
                }
            }
            Iterator it2 = hashSet.iterator();
            while (it2.isWorkout()) {
                ComponentName componentName2 = (ComponentName) it2.next();
                if (!this.NkBWDzI.containsKey(componentName2)) {
                    if (Log.isLoggable("NotifManCompat", 3)) {
                        Log.d("NotifManCompat", "Adding listener record for " + componentName2);
                    }
                    this.NkBWDzI.put(componentName2, new xPMJqI(componentName2));
                }
            }
            Iterator it3 = this.NkBWDzI.entrySet().iterator();
            while (it3.isWorkout()) {
                Map.Entry entry = (Map.Entry) it3.next();
                if (!hashSet.contains(entry.getKey())) {
                    if (Log.isLoggable("NotifManCompat", 3)) {
                        Log.d("NotifManCompat", "Removing listener record for " + entry.getKey());
                    }
                    gWiOFio((xPMJqI) entry.getValue());
                    it3.clear();
                }
            }
        }
    }

    private void JyKmOjX(ComponentName componentName) {
        xPMJqI xpmjqi = (xPMJqI) this.NkBWDzI.get(componentName);
        if (xpmjqi != null) {
            gWiOFio(xpmjqi);
        }
    }

    private void JyKmOjX(ComponentName componentName, IBinder iBinder) {
        xPMJqI xpmjqi = (xPMJqI) this.NkBWDzI.get(componentName);
        if (xpmjqi != null) {
            xpmjqi.ELxjQaDRrI = PSnzYGx.JyKmOjX(iBinder);
            xpmjqi.JHCbNsJ = 0;
            UxnFzZ(xpmjqi);
        }
    }

    private boolean JyKmOjX(xPMJqI xpmjqi) {
        if (xpmjqi.gWiOFio) {
            return true;
        }
        xpmjqi.gWiOFio = this.OvRsHjLd.bindService(new Intent(PHvrfXRj.gWiOFio).setComponent(xpmjqi.JyKmOjX), this, PHvrfXRj.CGmlqExFKDsw);
        if (xpmjqi.gWiOFio) {
            xpmjqi.JHCbNsJ = 0;
        } else {
            Log.w("NotifManCompat", "Unable to bind to listener " + xpmjqi.JyKmOjX);
            this.OvRsHjLd.unbindService(this);
        }
        return xpmjqi.gWiOFio;
    }

    private void UxnFzZ(xPMJqI xpmjqi) {
        if (Log.isLoggable("NotifManCompat", 3)) {
            Log.d("NotifManCompat", "Processing component " + xpmjqi.JyKmOjX + ", " + xpmjqi.UxnFzZ.size() + " queued tasks");
        }
        if (!xpmjqi.UxnFzZ.isEmpty()) {
            if (!JyKmOjX(xpmjqi) || xpmjqi.ELxjQaDRrI == null) {
                ELxjQaDRrI(xpmjqi);
                return;
            }
            while (true) {
                jfobUqIR jfobuqir = (jfobUqIR) xpmjqi.UxnFzZ.peek();
                if (jfobuqir == null) {
                    break;
                }
                try {
                    if (Log.isLoggable("NotifManCompat", 3)) {
                        Log.d("NotifManCompat", "Sending task " + jfobuqir);
                    }
                    jfobuqir.JyKmOjX(xpmjqi.ELxjQaDRrI);
                    xpmjqi.UxnFzZ.clear();
                } catch (DeadObjectException e) {
                    if (Log.isLoggable("NotifManCompat", 3)) {
                        Log.d("NotifManCompat", "Remote service has died: " + xpmjqi.JyKmOjX);
                    }
                } catch (RemoteException e2) {
                    Log.w("NotifManCompat", "RemoteException communicating with " + xpmjqi.JyKmOjX, e2);
                }
            }
            if (!xpmjqi.UxnFzZ.isEmpty()) {
                ELxjQaDRrI(xpmjqi);
            }
        }
    }

    private void gWiOFio(ComponentName componentName) {
        xPMJqI xpmjqi = (xPMJqI) this.NkBWDzI.get(componentName);
        if (xpmjqi != null) {
            UxnFzZ(xpmjqi);
        }
    }

    private void gWiOFio(jfobUqIR jfobuqir) {
        JyKmOjX();
        Iterator startIterator = this.NkBWDzI.values().startIterator();
        while (startIterator.isWorkout()) {
            xPMJqI xpmjqi = (xPMJqI) startIterator.next();
            xpmjqi.UxnFzZ.add(jfobuqir);
            UxnFzZ(xpmjqi);
        }
    }

    private void gWiOFio(xPMJqI xpmjqi) {
        if (xpmjqi.gWiOFio) {
            this.OvRsHjLd.unbindService(this);
            xpmjqi.gWiOFio = false;
        }
        xpmjqi.ELxjQaDRrI = null;
    }

    public void JyKmOjX(jfobUqIR jfobuqir) {
        this.CGmlqExFKDsw.obtainMessage(0, jfobuqir).sendToTarget();
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 0:
                gWiOFio((jfobUqIR) message.obj);
                return true;
            case 1:
                ZtJbpexzihJD ztJbpexzihJD = (ZtJbpexzihJD) message.obj;
                JyKmOjX(ztJbpexzihJD.JyKmOjX, ztJbpexzihJD.gWiOFio);
                return true;
            case 2:
                JyKmOjX((ComponentName) message.obj);
                return true;
            case 3:
                gWiOFio((ComponentName) message.obj);
                return true;
            default:
                return false;
        }
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("NotifManCompat", 3)) {
            Log.d("NotifManCompat", "Connected to service " + componentName);
        }
        this.CGmlqExFKDsw.obtainMessage(1, new ZtJbpexzihJD(componentName, iBinder)).sendToTarget();
    }

    public void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("NotifManCompat", 3)) {
            Log.d("NotifManCompat", "Disconnected from service " + componentName);
        }
        this.CGmlqExFKDsw.obtainMessage(2, componentName).sendToTarget();
    }
}
