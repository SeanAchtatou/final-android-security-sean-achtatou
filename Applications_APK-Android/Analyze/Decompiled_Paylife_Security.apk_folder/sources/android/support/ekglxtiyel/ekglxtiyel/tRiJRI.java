package android.support.ekglxtiyel.ekglxtiyel;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

abstract class tRiJRI extends Activity {
    tRiJRI() {
    }

    /* access modifiers changed from: package-private */
    public abstract View JyKmOjX(View view, String str, Context context, AttributeSet attributeSet);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if (Build.VERSION.SDK_INT < 11 && getLayoutInflater().getFactory() == null) {
            getLayoutInflater().setFactory(this);
        }
        super.onCreate(bundle);
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        View JyKmOjX = JyKmOjX(null, str, context, attributeSet);
        return JyKmOjX == null ? super.onCreateView(str, context, attributeSet) : JyKmOjX;
    }
}
