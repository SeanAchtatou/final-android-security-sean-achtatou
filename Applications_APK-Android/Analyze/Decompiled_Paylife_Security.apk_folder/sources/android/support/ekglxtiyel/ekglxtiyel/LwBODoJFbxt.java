package android.support.ekglxtiyel.ekglxtiyel;

import android.app.Activity;
import android.app.ActivityOptions;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;

class LwBODoJFbxt {
    private final ActivityOptions JyKmOjX;

    private LwBODoJFbxt(ActivityOptions activityOptions) {
        this.JyKmOjX = activityOptions;
    }

    public static LwBODoJFbxt JyKmOjX(Activity activity, View view, String str) {
        return new LwBODoJFbxt(ActivityOptions.makeSceneTransitionAnimation(activity, view, str));
    }

    public static LwBODoJFbxt JyKmOjX(Activity activity, View[] viewArr, String[] strArr) {
        Pair[] pairArr = null;
        if (viewArr != null) {
            Pair[] pairArr2 = new Pair[viewArr.length];
            for (int i = 0; i < pairArr2.length; i++) {
                pairArr2[i] = Pair.create(viewArr[i], strArr[i]);
            }
            pairArr = pairArr2;
        }
        return new LwBODoJFbxt(ActivityOptions.makeSceneTransitionAnimation(activity, pairArr));
    }

    public Bundle JyKmOjX() {
        return this.JyKmOjX.toBundle();
    }

    public void JyKmOjX(LwBODoJFbxt lwBODoJFbxt) {
        this.JyKmOjX.update(lwBODoJFbxt.JyKmOjX);
    }
}
