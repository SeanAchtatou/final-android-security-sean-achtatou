package android.support.ekglxtiyel.ekglxtiyel;

import android.app.PendingIntent;
import android.os.Bundle;
import java.util.ArrayList;

public final class mMDQStg {
    private final PendingIntent ELxjQaDRrI;
    private ArrayList JHCbNsJ;
    private final int JyKmOjX;
    private final Bundle UxnFzZ;
    private final CharSequence gWiOFio;

    public mMDQStg(int i, CharSequence charSequence, PendingIntent pendingIntent) {
        this(i, charSequence, pendingIntent, new Bundle());
    }

    private mMDQStg(int i, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle) {
        this.JyKmOjX = i;
        this.gWiOFio = yhoOAS.OvRsHjLd(charSequence);
        this.ELxjQaDRrI = pendingIntent;
        this.UxnFzZ = bundle;
    }

    public mMDQStg(HDwcDioG hDwcDioG) {
        this(hDwcDioG.JyKmOjX, hDwcDioG.gWiOFio, hDwcDioG.ELxjQaDRrI, new Bundle(hDwcDioG.JHCbNsJ));
    }

    public Bundle JyKmOjX() {
        return this.UxnFzZ;
    }

    public mMDQStg JyKmOjX(Bundle bundle) {
        if (bundle != null) {
            this.UxnFzZ.putAll(bundle);
        }
        return this;
    }

    public mMDQStg JyKmOjX(WZNPYZ wznpyz) {
        if (this.JHCbNsJ == null) {
            this.JHCbNsJ = new ArrayList();
        }
        this.JHCbNsJ.add(wznpyz);
        return this;
    }

    public mMDQStg JyKmOjX(lTNSCJu ltnscju) {
        ltnscju.JyKmOjX(this);
        return this;
    }

    public HDwcDioG gWiOFio() {
        return new HDwcDioG(this.JyKmOjX, this.gWiOFio, this.ELxjQaDRrI, this.UxnFzZ, this.JHCbNsJ != null ? (WZNPYZ[]) this.JHCbNsJ.toArray(new WZNPYZ[this.JHCbNsJ.size()]) : null);
    }
}
