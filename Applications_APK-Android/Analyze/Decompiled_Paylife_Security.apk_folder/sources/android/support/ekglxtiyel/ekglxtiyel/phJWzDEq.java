package android.support.ekglxtiyel.ekglxtiyel;

import android.app.ActionBar;
import android.app.Activity;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;

class phJWzDEq {
    private static final String JyKmOjX = "ActionBarDrawerToggleHoneycomb";
    private static final int[] gWiOFio = {16843531};

    phJWzDEq() {
    }

    public static Drawable JyKmOjX(Activity activity) {
        TypedArray obtainStyledAttributes = activity.obtainStyledAttributes(gWiOFio);
        Drawable drawable = obtainStyledAttributes.getDrawable(0);
        obtainStyledAttributes.recycle();
        return drawable;
    }

    public static Object JyKmOjX(Object obj, Activity activity, int i) {
        Object wiupijhywn = obj == null ? new wiuPiJhywn(activity) : obj;
        wiuPiJhywn wiupijhywn2 = (wiuPiJhywn) wiupijhywn;
        if (wiupijhywn2.JyKmOjX != null) {
            try {
                ActionBar actionBar = activity.getActionBar();
                wiupijhywn2.gWiOFio.invoke(actionBar, Integer.valueOf(i));
                if (Build.VERSION.SDK_INT <= 19) {
                    actionBar.blendColorName(actionBar.getSubtitle());
                }
            } catch (Exception e) {
                Log.w(JyKmOjX, "Couldn't set content description via JB-MR2 API", e);
            }
        }
        return wiupijhywn;
    }

    public static Object JyKmOjX(Object obj, Activity activity, Drawable drawable, int i) {
        Object wiupijhywn = obj == null ? new wiuPiJhywn(activity) : obj;
        wiuPiJhywn wiupijhywn2 = (wiuPiJhywn) wiupijhywn;
        if (wiupijhywn2.JyKmOjX != null) {
            try {
                ActionBar actionBar = activity.getActionBar();
                wiupijhywn2.JyKmOjX.invoke(actionBar, drawable);
                wiupijhywn2.gWiOFio.invoke(actionBar, Integer.valueOf(i));
            } catch (Exception e) {
                Log.w(JyKmOjX, "Couldn't set home-as-up indicator via JB-MR2 API", e);
            }
        } else if (wiupijhywn2.ELxjQaDRrI != null) {
            wiupijhywn2.ELxjQaDRrI.setImageDrawable(drawable);
        } else {
            Log.w(JyKmOjX, "Couldn't set home-as-up indicator");
        }
        return wiupijhywn;
    }
}
