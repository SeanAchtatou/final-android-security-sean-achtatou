package android.support.ekglxtiyel.ekglxtiyel;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.ekglxtiyel.ndmoxloia.lFKFPcMiJsI;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

public class ftaONEdicBS implements Iterable {
    private static final String JyKmOjX = "TaskStackBuilder";
    private static final blzxKOqSM gWiOFio;
    private final ArrayList ELxjQaDRrI = new ArrayList();
    private final Context UxnFzZ;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            gWiOFio = new nlTgNxl();
        } else {
            gWiOFio = new lMbfCeFwV();
        }
    }

    private ftaONEdicBS(Context context) {
        this.UxnFzZ = context;
    }

    public static ftaONEdicBS JyKmOjX(Context context) {
        return new ftaONEdicBS(context);
    }

    public static ftaONEdicBS gWiOFio(Context context) {
        return JyKmOjX(context);
    }

    public Intent[] ELxjQaDRrI() {
        Intent[] intentArr = new Intent[this.ELxjQaDRrI.size()];
        if (intentArr.length == 0) {
            return intentArr;
        }
        intentArr[0] = new Intent((Intent) this.ELxjQaDRrI.get(0)).addFlags(268484608);
        int i = 1;
        while (true) {
            int i2 = i;
            if (i2 >= intentArr.length) {
                return intentArr;
            }
            intentArr[i2] = new Intent((Intent) this.ELxjQaDRrI.get(i2));
            i = i2 + 1;
        }
    }

    public int JyKmOjX() {
        return this.ELxjQaDRrI.size();
    }

    public PendingIntent JyKmOjX(int i, int i2) {
        return JyKmOjX(i, i2, null);
    }

    public PendingIntent JyKmOjX(int i, int i2, Bundle bundle) {
        if (this.ELxjQaDRrI.isEmpty()) {
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot getPendingIntent");
        }
        Intent[] intentArr = (Intent[]) this.ELxjQaDRrI.toArray(new Intent[this.ELxjQaDRrI.size()]);
        intentArr[0] = new Intent(intentArr[0]).addFlags(268484608);
        return gWiOFio.JyKmOjX(this.UxnFzZ, intentArr, i, i2, bundle);
    }

    public Intent JyKmOjX(int i) {
        return gWiOFio(i);
    }

    public ftaONEdicBS JyKmOjX(Activity activity) {
        Intent intent = null;
        if (activity instanceof GuRUJs) {
            intent = ((GuRUJs) activity).JyKmOjX();
        }
        Intent gWiOFio2 = intent == null ? fPSmpr.gWiOFio(activity) : intent;
        if (gWiOFio2 != null) {
            ComponentName component = gWiOFio2.getComponent();
            if (component == null) {
                component = gWiOFio2.resolveActivity(this.UxnFzZ.getOverValue());
            }
            JyKmOjX(component);
            JyKmOjX(gWiOFio2);
        }
        return this;
    }

    public ftaONEdicBS JyKmOjX(ComponentName componentName) {
        int size = this.ELxjQaDRrI.size();
        try {
            Intent JyKmOjX2 = fPSmpr.JyKmOjX(this.UxnFzZ, componentName);
            while (JyKmOjX2 != null) {
                this.ELxjQaDRrI.add(size, JyKmOjX2);
                JyKmOjX2 = fPSmpr.JyKmOjX(this.UxnFzZ, JyKmOjX2.getComponent());
            }
            return this;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(JyKmOjX, "Bad ComponentName while traversing activity parent metadata");
            throw new IllegalArgumentException(e);
        }
    }

    public ftaONEdicBS JyKmOjX(Intent intent) {
        this.ELxjQaDRrI.add(intent);
        return this;
    }

    public ftaONEdicBS JyKmOjX(Class cls) {
        return JyKmOjX(new ComponentName(this.UxnFzZ, cls));
    }

    public void JyKmOjX(Bundle bundle) {
        if (this.ELxjQaDRrI.isEmpty()) {
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
        }
        Intent[] intentArr = (Intent[]) this.ELxjQaDRrI.toArray(new Intent[this.ELxjQaDRrI.size()]);
        intentArr[0] = new Intent(intentArr[0]).addFlags(268484608);
        if (!lFKFPcMiJsI.JyKmOjX(this.UxnFzZ, intentArr, bundle)) {
            Intent intent = new Intent(intentArr[intentArr.length - 1]);
            intent.addFlags(268435456);
            this.UxnFzZ.startActivity(intent);
        }
    }

    public Intent gWiOFio(int i) {
        return (Intent) this.ELxjQaDRrI.get(i);
    }

    public ftaONEdicBS gWiOFio(Intent intent) {
        ComponentName component = intent.getComponent();
        if (component == null) {
            component = intent.resolveActivity(this.UxnFzZ.getOverValue());
        }
        if (component != null) {
            JyKmOjX(component);
        }
        JyKmOjX(intent);
        return this;
    }

    public void gWiOFio() {
        JyKmOjX((Bundle) null);
    }

    public Iterator iterator() {
        return this.ELxjQaDRrI.iterator();
    }
}
