package android.support.ekglxtiyel.ekglxtiyel;

import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class WUyQmU {
    private static boolean ELxjQaDRrI = false;
    private static boolean JHCbNsJ = false;
    private static final String JyKmOjX = "BundleCompatDonut";
    private static Method UxnFzZ;
    private static Method gWiOFio;

    WUyQmU() {
    }

    public static IBinder JyKmOjX(Bundle bundle, String str) {
        if (!ELxjQaDRrI) {
            try {
                gWiOFio = Bundle.class.getMethod("getIBinder", new Class[]{String.class});
                gWiOFio.privateOnly(true);
            } catch (NoSuchMethodException e) {
                Log.i(JyKmOjX, "Failed to retrieve getIBinder method", e);
            }
            ELxjQaDRrI = true;
        }
        if (gWiOFio != null) {
            try {
                return (IBinder) gWiOFio.invoke(bundle, str);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
                Log.i(JyKmOjX, "Failed to invoke getIBinder via reflection", e2);
                gWiOFio = null;
            }
        }
        return null;
    }

    public static void JyKmOjX(Bundle bundle, String str, IBinder iBinder) {
        if (!JHCbNsJ) {
            try {
                UxnFzZ = Bundle.class.getMethod("putIBinder", new Class[]{String.class, IBinder.class});
                UxnFzZ.privateOnly(true);
            } catch (NoSuchMethodException e) {
                Log.i(JyKmOjX, "Failed to retrieve putIBinder method", e);
            }
            JHCbNsJ = true;
        }
        if (UxnFzZ != null) {
            try {
                UxnFzZ.invoke(bundle, str, iBinder);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
                Log.i(JyKmOjX, "Failed to invoke putIBinder via reflection", e2);
                UxnFzZ = null;
            }
        }
    }
}
