package android.support.ekglxtiyel.ekglxtiyel;

import android.transition.Transition;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

final class dabyLUyQ implements ViewTreeObserver.OnPreDrawListener {
    final /* synthetic */ ArrayList CkytDdEwp;
    final /* synthetic */ View ELxjQaDRrI;
    final /* synthetic */ Map JHCbNsJ;
    final /* synthetic */ View JyKmOjX;
    final /* synthetic */ Map OvRsHjLd;
    final /* synthetic */ jvrTDKaQVq UxnFzZ;
    final /* synthetic */ Transition gWiOFio;

    dabyLUyQ(View view, Transition transition, View view2, jvrTDKaQVq jvrtdkaqvq, Map map, Map map2, ArrayList arrayList) {
        this.JyKmOjX = view;
        this.gWiOFio = transition;
        this.ELxjQaDRrI = view2;
        this.UxnFzZ = jvrtdkaqvq;
        this.JHCbNsJ = map;
        this.OvRsHjLd = map2;
        this.CkytDdEwp = arrayList;
    }

    public boolean onPreDraw() {
        this.JyKmOjX.createViewTreeObserver().removeOnPreDrawListener(this);
        if (this.gWiOFio != null) {
            this.gWiOFio.removeTarget(this.ELxjQaDRrI);
        }
        View JyKmOjX2 = this.UxnFzZ.JyKmOjX();
        if (JyKmOjX2 == null) {
            return true;
        }
        if (!this.JHCbNsJ.isEmpty()) {
            aVhbYeLqNPIj.JyKmOjX(this.OvRsHjLd, JyKmOjX2);
            this.OvRsHjLd.keySet().retainAll(this.JHCbNsJ.values());
            Iterator it = this.JHCbNsJ.entrySet().iterator();
            while (it.isWorkout()) {
                Map.Entry entry = (Map.Entry) it.next();
                View view = (View) this.OvRsHjLd.get((String) entry.getValue());
                if (view != null) {
                    view.setTransitionName((String) entry.getKey());
                }
            }
        }
        if (this.gWiOFio == null) {
            return true;
        }
        aVhbYeLqNPIj.gWiOFio(this.CkytDdEwp, JyKmOjX2);
        this.CkytDdEwp.removeAll(this.OvRsHjLd.values());
        this.CkytDdEwp.add(this.ELxjQaDRrI);
        aVhbYeLqNPIj.gWiOFio(this.gWiOFio, this.CkytDdEwp);
        return true;
    }
}
