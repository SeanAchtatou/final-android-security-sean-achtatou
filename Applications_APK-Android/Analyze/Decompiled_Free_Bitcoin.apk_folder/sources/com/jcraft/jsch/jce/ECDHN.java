package com.jcraft.jsch.jce;

import com.jcraft.jsch.ECDH;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECFieldFp;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.EllipticCurve;
import javax.crypto.KeyAgreement;

public class ECDHN implements ECDH {
    private static BigInteger sdvsdvsvsevsvsev = serfwefewfwefewef.add(BigInteger.ONE);
    private static BigInteger serfwefewfwefewef = BigInteger.ONE.add(BigInteger.ONE);
    byte[] fsafsdfsfsdfsfsdfsd;
    ECPublicKey sdfsdfsdfsdf;
    private KeyAgreement zxcvxvsdvsvsvs;

    private byte[] zxcvxvsdvsvsvs(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[(bArr.length + 1 + bArr2.length)];
        bArr3[0] = 4;
        System.arraycopy(bArr, 0, bArr3, 1, bArr.length);
        System.arraycopy(bArr2, 0, bArr3, bArr.length + 1, bArr2.length);
        return bArr3;
    }

    public void fsafsdfsfsdfsfsdfsd(int i) {
        this.zxcvxvsdvsvsvs = KeyAgreement.getInstance("ECDH");
        KeyPairGenECDSA keyPairGenECDSA = new KeyPairGenECDSA();
        keyPairGenECDSA.fsafsdfsfsdfsfsdfsd(i);
        this.sdfsdfsdfsdf = keyPairGenECDSA.zxcvxvsdvsvsvs();
        this.fsafsdfsfsdfsfsdfsd = zxcvxvsdvsvsvs(keyPairGenECDSA.fsafsdfsfsdfsfsdfsd(), keyPairGenECDSA.sdfsdfsdfsdf());
        this.zxcvxvsdvsvsvs.init(keyPairGenECDSA.serfwefewfwefewef());
    }

    public byte[] fsafsdfsfsdfsfsdfsd() {
        return this.fsafsdfsfsdfsfsdfsd;
    }

    public byte[] fsafsdfsfsdfsfsdfsd(byte[] bArr, byte[] bArr2) {
        this.zxcvxvsdvsvsvs.doPhase(KeyFactory.getInstance("EC").generatePublic(new ECPublicKeySpec(new ECPoint(new BigInteger(1, bArr), new BigInteger(1, bArr2)), this.sdfsdfsdfsdf.getParams())), true);
        return this.zxcvxvsdvsvsvs.generateSecret();
    }

    public boolean sdfsdfsdfsdf(byte[] bArr, byte[] bArr2) {
        BigInteger bigInteger = new BigInteger(1, bArr);
        BigInteger bigInteger2 = new BigInteger(1, bArr2);
        if (new ECPoint(bigInteger, bigInteger2).equals(ECPoint.POINT_INFINITY)) {
            return false;
        }
        EllipticCurve curve = this.sdfsdfsdfsdf.getParams().getCurve();
        BigInteger p = ((ECFieldFp) curve.getField()).getP();
        BigInteger subtract = p.subtract(BigInteger.ONE);
        if (bigInteger.compareTo(subtract) > 0 || bigInteger2.compareTo(subtract) > 0) {
            return false;
        }
        return bigInteger2.modPow(serfwefewfwefewef, p).equals(bigInteger.multiply(curve.getA()).add(curve.getB()).add(bigInteger.modPow(sdvsdvsvsevsvsev, p)).mod(p));
    }
}
