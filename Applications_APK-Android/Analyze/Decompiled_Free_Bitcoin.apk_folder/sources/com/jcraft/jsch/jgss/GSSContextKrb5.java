package com.jcraft.jsch.jgss;

import com.jcraft.jsch.GSSContext;
import com.jcraft.jsch.JSchException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSException;
import org.ietf.jgss.GSSManager;
import org.ietf.jgss.MessageProp;
import org.ietf.jgss.Oid;

public class GSSContextKrb5 implements GSSContext {
    private static String fsafsdfsfsdfsfsdfsd = fsafsdfsfsdfsfsdfsd("javax.security.auth.useSubjectCredsOnly");
    private org.ietf.jgss.GSSContext sdfsdfsdfsdf = null;

    private static String fsafsdfsfsdfsfsdfsd(String str) {
        try {
            return System.getProperty(str);
        } catch (Exception e) {
            return null;
        }
    }

    private static void sdfsdfsdfsdf(String str, String str2) {
        try {
            System.setProperty(str, str2);
        } catch (Exception e) {
        }
    }

    public void fsafsdfsfsdfsfsdfsd(String str, String str2) {
        try {
            Oid oid = new Oid("1.2.840.113554.1.2.2");
            Oid oid2 = new Oid("1.2.840.113554.1.2.2.1");
            GSSManager instance = GSSManager.getInstance();
            try {
                str2 = InetAddress.getByName(str2).getCanonicalHostName();
            } catch (UnknownHostException e) {
            }
            this.sdfsdfsdfsdf = instance.createContext(instance.createName(new StringBuffer().append("host/").append(str2).toString(), oid2), oid, (GSSCredential) null, 0);
            this.sdfsdfsdfsdf.requestMutualAuth(true);
            this.sdfsdfsdfsdf.requestConf(true);
            this.sdfsdfsdfsdf.requestInteg(true);
            this.sdfsdfsdfsdf.requestCredDeleg(true);
            this.sdfsdfsdfsdf.requestAnonymity(false);
        } catch (GSSException e2) {
            throw new JSchException(e2.toString());
        }
    }

    public boolean fsafsdfsfsdfsfsdfsd() {
        return this.sdfsdfsdfsdf.isEstablished();
    }

    public byte[] fsafsdfsfsdfsfsdfsd(byte[] bArr, int i, int i2) {
        try {
            if (fsafsdfsfsdfsfsdfsd == null) {
                sdfsdfsdfsdf("javax.security.auth.useSubjectCredsOnly", "false");
            }
            byte[] initSecContext = this.sdfsdfsdfsdf.initSecContext(bArr, 0, i2);
            if (fsafsdfsfsdfsfsdfsd == null) {
                sdfsdfsdfsdf("javax.security.auth.useSubjectCredsOnly", "true");
            }
            return initSecContext;
        } catch (GSSException e) {
            throw new JSchException(e.toString());
        } catch (SecurityException e2) {
            throw new JSchException(e2.toString());
        } catch (Throwable th) {
            if (fsafsdfsfsdfsfsdfsd == null) {
                sdfsdfsdfsdf("javax.security.auth.useSubjectCredsOnly", "true");
            }
            throw th;
        }
    }

    public void sdfsdfsdfsdf() {
        try {
            this.sdfsdfsdfsdf.dispose();
        } catch (GSSException e) {
        }
    }

    public byte[] sdfsdfsdfsdf(byte[] bArr, int i, int i2) {
        try {
            return this.sdfsdfsdfsdf.getMIC(bArr, i, i2, new MessageProp(0, true));
        } catch (GSSException e) {
            return null;
        }
    }
}
