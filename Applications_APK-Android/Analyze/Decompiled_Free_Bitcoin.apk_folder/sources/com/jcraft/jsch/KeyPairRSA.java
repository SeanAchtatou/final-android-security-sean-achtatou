package com.jcraft.jsch;

import java.math.BigInteger;

public class KeyPairRSA extends KeyPair {
    private static final byte[] b = Util.fsafsdfsfsdfsfsdfsd("-----BEGIN RSA PRIVATE KEY-----");
    private static final byte[] c = Util.fsafsdfsfsdfsfsdfsd("-----END RSA PRIVATE KEY-----");
    private static final byte[] d = Util.fsafsdfsfsdfsfsdfsd("ssh-rsa");
    private int a;
    private byte[] ertertetetetete;
    private byte[] eryryhrtytujrtujrurt;
    private byte[] fgjyukukuiki;
    private byte[] htyjyukuilulululu;
    private byte[] reyeyreyeyeyey;
    private byte[] rthrthrtjrtjrjtdcbcvbc;
    private byte[] uiiliuluiiuluilulu;
    private byte[] wrrgehehehehe;

    public KeyPairRSA(JSch jSch) {
        this(jSch, null, null, null);
    }

    public KeyPairRSA(JSch jSch, byte[] bArr, byte[] bArr2, byte[] bArr3) {
        super(jSch);
        this.a = 1024;
        this.eryryhrtytujrtujrurt = bArr;
        this.rthrthrtjrtjrjtdcbcvbc = bArr2;
        this.wrrgehehehehe = bArr3;
        if (bArr != null) {
            this.a = new BigInteger(bArr).bitLength();
        }
    }

    private byte[] eryryhrtytujrtujrurt() {
        if (this.ertertetetetete == null) {
            this.ertertetetetete = new BigInteger(this.wrrgehehehehe).mod(new BigInteger(this.fgjyukukuiki).subtract(BigInteger.ONE)).toByteArray();
        }
        return this.ertertetetetete;
    }

    static KeyPair fsafsdfsfsdfsfsdfsd(JSch jSch, Buffer buffer) {
        byte[][] fsafsdfsfsdfsfsdfsd = buffer.fsafsdfsfsdfsfsdfsd(8, "invalid key format");
        KeyPairRSA keyPairRSA = new KeyPairRSA(jSch, fsafsdfsfsdfsfsdfsd[1], fsafsdfsfsdfsfsdfsd[2], fsafsdfsfsdfsfsdfsd[3]);
        keyPairRSA.reyeyreyeyeyey = fsafsdfsfsdfsfsdfsd[4];
        keyPairRSA.fgjyukukuiki = fsafsdfsfsdfsfsdfsd[5];
        keyPairRSA.htyjyukuilulululu = fsafsdfsfsdfsfsdfsd[6];
        keyPairRSA.sdfsdfsdfsdf = new String(fsafsdfsfsdfsfsdfsd[7]);
        keyPairRSA.fsafsdfsfsdfsfsdfsd = 0;
        return keyPairRSA;
    }

    private byte[] rthrthrtjrtjrjtdcbcvbc() {
        if (this.uiiliuluiiuluilulu == null) {
            this.uiiliuluiiuluilulu = new BigInteger(this.wrrgehehehehe).mod(new BigInteger(this.htyjyukuilulululu).subtract(BigInteger.ONE)).toByteArray();
        }
        return this.uiiliuluiiuluilulu;
    }

    private byte[] wrrgehehehehe() {
        if (this.reyeyreyeyeyey == null) {
            this.reyeyreyeyeyey = new BigInteger(this.htyjyukuilulululu).modInverse(new BigInteger(this.fgjyukukuiki)).toByteArray();
        }
        return this.reyeyreyeyeyey;
    }

    public byte[] fsafsdfsfsdfsfsdfsd() {
        if (sdvsdvsvsevsvsev()) {
            throw new JSchException("key is encrypted.");
        }
        Buffer buffer = new Buffer();
        buffer.sdfsdfsdfsdf(d);
        buffer.sdfsdfsdfsdf(this.eryryhrtytujrtujrurt);
        buffer.sdfsdfsdfsdf(this.rthrthrtjrtjrjtdcbcvbc);
        buffer.sdfsdfsdfsdf(this.wrrgehehehehe);
        buffer.sdfsdfsdfsdf(wrrgehehehehe());
        buffer.sdfsdfsdfsdf(this.fgjyukukuiki);
        buffer.sdfsdfsdfsdf(this.htyjyukuilulululu);
        buffer.sdfsdfsdfsdf(Util.fsafsdfsfsdfsfsdfsd(this.sdfsdfsdfsdf));
        byte[] bArr = new byte[buffer.fsafsdfsfsdfsfsdfsd()];
        buffer.zxcvxvsdvsvsvs(bArr, 0, bArr.length);
        return bArr;
    }

    public byte[] fsafsdfsfsdfsfsdfsd(byte[] bArr) {
        try {
            JSch jSch = this.zxcvxvsdvsvsvs;
            SignatureRSA signatureRSA = (SignatureRSA) Class.forName(JSch.sdfsdfsdfsdf("signature.rsa")).newInstance();
            signatureRSA.fsafsdfsfsdfsfsdfsd();
            signatureRSA.sdfsdfsdfsdf(this.wrrgehehehehe, this.eryryhrtytujrtujrurt);
            signatureRSA.fsafsdfsfsdfsfsdfsd(bArr);
            return Buffer.fsafsdfsfsdfsfsdfsd(new byte[][]{d, signatureRSA.sdfsdfsdfsdf()}).sdfsdfsdfsdf;
        } catch (Exception e) {
            return null;
        }
    }

    public void hukuiluliulu() {
        super.hukuiluliulu();
        Util.zxcvxvsdvsvsvs(this.wrrgehehehehe);
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:91:0x00cc */
    /* JADX WARN: Type inference failed for: r2v8, types: [int] */
    /* JADX WARN: Type inference failed for: r2v95, types: [int] */
    /* JADX WARN: Type inference failed for: r5v16, types: [int] */
    /* JADX WARN: Type inference failed for: r5v17, types: [int] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean sdfsdfsdfsdf(byte[] r8) {
        /*
            r7 = this;
            r3 = 2
            r0 = 1
            r1 = 0
            r2 = 0
            int r4 = r7.fsafsdfsfsdfsfsdfsd     // Catch:{ Exception -> 0x0081 }
            if (r4 != r3) goto L_0x0036
            com.jcraft.jsch.Buffer r2 = new com.jcraft.jsch.Buffer     // Catch:{ Exception -> 0x0081 }
            r2.<init>(r8)     // Catch:{ Exception -> 0x0081 }
            int r3 = r8.length     // Catch:{ Exception -> 0x0081 }
            r2.sdfsdfsdfsdf(r3)     // Catch:{ Exception -> 0x0081 }
            r3 = 4
            java.lang.String r4 = ""
            byte[][] r2 = r2.fsafsdfsfsdfsfsdfsd(r3, r4)     // Catch:{ JSchException -> 0x0033 }
            r3 = 0
            r3 = r2[r3]     // Catch:{ JSchException -> 0x0033 }
            r7.wrrgehehehehe = r3     // Catch:{ JSchException -> 0x0033 }
            r3 = 1
            r3 = r2[r3]     // Catch:{ JSchException -> 0x0033 }
            r7.fgjyukukuiki = r3     // Catch:{ JSchException -> 0x0033 }
            r3 = 2
            r3 = r2[r3]     // Catch:{ JSchException -> 0x0033 }
            r7.htyjyukuilulululu = r3     // Catch:{ JSchException -> 0x0033 }
            r3 = 3
            r2 = r2[r3]     // Catch:{ JSchException -> 0x0033 }
            r7.reyeyreyeyeyey = r2     // Catch:{ JSchException -> 0x0033 }
            r7.eryryhrtytujrtujrurt()     // Catch:{ Exception -> 0x0081 }
            r7.rthrthrtjrtjrjtdcbcvbc()     // Catch:{ Exception -> 0x0081 }
        L_0x0032:
            return r0
        L_0x0033:
            r0 = move-exception
            r0 = r1
            goto L_0x0032
        L_0x0036:
            int r4 = r7.fsafsdfsfsdfsfsdfsd     // Catch:{ Exception -> 0x0081 }
            if (r4 != r0) goto L_0x0086
            byte r2 = r8[r2]     // Catch:{ Exception -> 0x0081 }
            r3 = 48
            if (r2 == r3) goto L_0x0084
            com.jcraft.jsch.Buffer r2 = new com.jcraft.jsch.Buffer     // Catch:{ Exception -> 0x0081 }
            r2.<init>(r8)     // Catch:{ Exception -> 0x0081 }
            byte[] r3 = r2.rthrthrtjrtjrjtdcbcvbc()     // Catch:{ Exception -> 0x0081 }
            r7.rthrthrtjrtjrjtdcbcvbc = r3     // Catch:{ Exception -> 0x0081 }
            byte[] r3 = r2.rthrthrtjrtjrjtdcbcvbc()     // Catch:{ Exception -> 0x0081 }
            r7.wrrgehehehehe = r3     // Catch:{ Exception -> 0x0081 }
            byte[] r3 = r2.rthrthrtjrtjrjtdcbcvbc()     // Catch:{ Exception -> 0x0081 }
            r7.eryryhrtytujrtujrurt = r3     // Catch:{ Exception -> 0x0081 }
            r2.rthrthrtjrtjrjtdcbcvbc()     // Catch:{ Exception -> 0x0081 }
            byte[] r3 = r2.rthrthrtjrtjrjtdcbcvbc()     // Catch:{ Exception -> 0x0081 }
            r7.fgjyukukuiki = r3     // Catch:{ Exception -> 0x0081 }
            byte[] r2 = r2.rthrthrtjrtjrjtdcbcvbc()     // Catch:{ Exception -> 0x0081 }
            r7.htyjyukuilulululu = r2     // Catch:{ Exception -> 0x0081 }
            byte[] r2 = r7.eryryhrtytujrtujrurt     // Catch:{ Exception -> 0x0081 }
            if (r2 == 0) goto L_0x0077
            java.math.BigInteger r2 = new java.math.BigInteger     // Catch:{ Exception -> 0x0081 }
            byte[] r3 = r7.eryryhrtytujrtujrurt     // Catch:{ Exception -> 0x0081 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0081 }
            int r2 = r2.bitLength()     // Catch:{ Exception -> 0x0081 }
            r7.a = r2     // Catch:{ Exception -> 0x0081 }
        L_0x0077:
            r7.eryryhrtytujrtujrurt()     // Catch:{ Exception -> 0x0081 }
            r7.rthrthrtjrtjrjtdcbcvbc()     // Catch:{ Exception -> 0x0081 }
            r7.wrrgehehehehe()     // Catch:{ Exception -> 0x0081 }
            goto L_0x0032
        L_0x0081:
            r0 = move-exception
            r0 = r1
            goto L_0x0032
        L_0x0084:
            r0 = r1
            goto L_0x0032
        L_0x0086:
            r2 = 1
            byte r2 = r8[r2]     // Catch:{ Exception -> 0x0081 }
            r2 = r2 & 255(0xff, float:3.57E-43)
            r4 = r2 & 128(0x80, float:1.794E-43)
            if (r4 == 0) goto L_0x00a5
            r2 = r2 & 127(0x7f, float:1.78E-43)
            r4 = r2
            r6 = r1
            r2 = r3
        L_0x0094:
            int r5 = r4 + -1
            if (r4 <= 0) goto L_0x00a6
            int r6 = r6 << 8
            int r4 = r2 + 1
            byte r2 = r8[r2]     // Catch:{ Exception -> 0x0081 }
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 + r6
            r6 = r2
            r2 = r4
            r4 = r5
            goto L_0x0094
        L_0x00a5:
            r2 = r3
        L_0x00a6:
            byte r4 = r8[r2]     // Catch:{ Exception -> 0x0081 }
            if (r4 == r3) goto L_0x00ac
            r0 = r1
            goto L_0x0032
        L_0x00ac:
            int r2 = r2 + 1
            int r3 = r2 + 1
            byte r2 = r8[r2]     // Catch:{ Exception -> 0x0081 }
            r2 = r2 & 255(0xff, float:3.57E-43)
            r4 = r2 & 128(0x80, float:1.794E-43)
            if (r4 == 0) goto L_0x00cc
            r2 = r2 & 127(0x7f, float:1.78E-43)
            r4 = r2
            r2 = r1
        L_0x00bc:
            int r5 = r4 + -1
            if (r4 <= 0) goto L_0x00cc
            int r2 = r2 << 8
            int r4 = r3 + 1
            byte r3 = r8[r3]     // Catch:{ Exception -> 0x0081 }
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r2 = r2 + r3
            r3 = r4
            r4 = r5
            goto L_0x00bc
        L_0x00cc:
            int r2 = r2 + r3
            int r2 = r2 + 1
            int r3 = r2 + 1
            byte r2 = r8[r2]     // Catch:{ Exception -> 0x0081 }
            r2 = r2 & 255(0xff, float:3.57E-43)
            r4 = r2 & 128(0x80, float:1.794E-43)
            if (r4 == 0) goto L_0x00ed
            r2 = r2 & 127(0x7f, float:1.78E-43)
            r4 = r2
            r2 = r1
        L_0x00dd:
            int r5 = r4 + -1
            if (r4 <= 0) goto L_0x00ed
            int r2 = r2 << 8
            int r4 = r3 + 1
            byte r3 = r8[r3]     // Catch:{ Exception -> 0x0081 }
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r2 = r2 + r3
            r3 = r4
            r4 = r5
            goto L_0x00dd
        L_0x00ed:
            byte[] r4 = new byte[r2]     // Catch:{ Exception -> 0x0081 }
            r7.eryryhrtytujrtujrurt = r4     // Catch:{ Exception -> 0x0081 }
            byte[] r4 = r7.eryryhrtytujrtujrurt     // Catch:{ Exception -> 0x0081 }
            r5 = 0
            java.lang.System.arraycopy(r8, r3, r4, r5, r2)     // Catch:{ Exception -> 0x0081 }
            int r2 = r2 + r3
            int r2 = r2 + 1
            int r3 = r2 + 1
            byte r2 = r8[r2]     // Catch:{ Exception -> 0x0081 }
            r2 = r2 & 255(0xff, float:3.57E-43)
            r4 = r2 & 128(0x80, float:1.794E-43)
            if (r4 == 0) goto L_0x0118
            r2 = r2 & 127(0x7f, float:1.78E-43)
            r4 = r2
            r2 = r1
        L_0x0108:
            int r5 = r4 + -1
            if (r4 <= 0) goto L_0x0118
            int r2 = r2 << 8
            int r4 = r3 + 1
            byte r3 = r8[r3]     // Catch:{ Exception -> 0x0081 }
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r2 = r2 + r3
            r3 = r4
            r4 = r5
            goto L_0x0108
        L_0x0118:
            byte[] r4 = new byte[r2]     // Catch:{ Exception -> 0x0081 }
            r7.rthrthrtjrtjrjtdcbcvbc = r4     // Catch:{ Exception -> 0x0081 }
            byte[] r4 = r7.rthrthrtjrtjrjtdcbcvbc     // Catch:{ Exception -> 0x0081 }
            r5 = 0
            java.lang.System.arraycopy(r8, r3, r4, r5, r2)     // Catch:{ Exception -> 0x0081 }
            int r2 = r2 + r3
            int r2 = r2 + 1
            int r3 = r2 + 1
            byte r2 = r8[r2]     // Catch:{ Exception -> 0x0081 }
            r2 = r2 & 255(0xff, float:3.57E-43)
            r4 = r2 & 128(0x80, float:1.794E-43)
            if (r4 == 0) goto L_0x0143
            r2 = r2 & 127(0x7f, float:1.78E-43)
            r4 = r2
            r2 = r1
        L_0x0133:
            int r5 = r4 + -1
            if (r4 <= 0) goto L_0x0143
            int r2 = r2 << 8
            int r4 = r3 + 1
            byte r3 = r8[r3]     // Catch:{ Exception -> 0x0081 }
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r2 = r2 + r3
            r3 = r4
            r4 = r5
            goto L_0x0133
        L_0x0143:
            byte[] r4 = new byte[r2]     // Catch:{ Exception -> 0x0081 }
            r7.wrrgehehehehe = r4     // Catch:{ Exception -> 0x0081 }
            byte[] r4 = r7.wrrgehehehehe     // Catch:{ Exception -> 0x0081 }
            r5 = 0
            java.lang.System.arraycopy(r8, r3, r4, r5, r2)     // Catch:{ Exception -> 0x0081 }
            int r2 = r2 + r3
            int r2 = r2 + 1
            int r3 = r2 + 1
            byte r2 = r8[r2]     // Catch:{ Exception -> 0x0081 }
            r2 = r2 & 255(0xff, float:3.57E-43)
            r4 = r2 & 128(0x80, float:1.794E-43)
            if (r4 == 0) goto L_0x016e
            r2 = r2 & 127(0x7f, float:1.78E-43)
            r4 = r2
            r2 = r1
        L_0x015e:
            int r5 = r4 + -1
            if (r4 <= 0) goto L_0x016e
            int r2 = r2 << 8
            int r4 = r3 + 1
            byte r3 = r8[r3]     // Catch:{ Exception -> 0x0081 }
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r2 = r2 + r3
            r3 = r4
            r4 = r5
            goto L_0x015e
        L_0x016e:
            byte[] r4 = new byte[r2]     // Catch:{ Exception -> 0x0081 }
            r7.fgjyukukuiki = r4     // Catch:{ Exception -> 0x0081 }
            byte[] r4 = r7.fgjyukukuiki     // Catch:{ Exception -> 0x0081 }
            r5 = 0
            java.lang.System.arraycopy(r8, r3, r4, r5, r2)     // Catch:{ Exception -> 0x0081 }
            int r2 = r2 + r3
            int r2 = r2 + 1
            int r3 = r2 + 1
            byte r2 = r8[r2]     // Catch:{ Exception -> 0x0081 }
            r2 = r2 & 255(0xff, float:3.57E-43)
            r4 = r2 & 128(0x80, float:1.794E-43)
            if (r4 == 0) goto L_0x0199
            r2 = r2 & 127(0x7f, float:1.78E-43)
            r4 = r2
            r2 = r1
        L_0x0189:
            int r5 = r4 + -1
            if (r4 <= 0) goto L_0x0199
            int r2 = r2 << 8
            int r4 = r3 + 1
            byte r3 = r8[r3]     // Catch:{ Exception -> 0x0081 }
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r2 = r2 + r3
            r3 = r4
            r4 = r5
            goto L_0x0189
        L_0x0199:
            byte[] r4 = new byte[r2]     // Catch:{ Exception -> 0x0081 }
            r7.htyjyukuilulululu = r4     // Catch:{ Exception -> 0x0081 }
            byte[] r4 = r7.htyjyukuilulululu     // Catch:{ Exception -> 0x0081 }
            r5 = 0
            java.lang.System.arraycopy(r8, r3, r4, r5, r2)     // Catch:{ Exception -> 0x0081 }
            int r2 = r2 + r3
            int r2 = r2 + 1
            int r3 = r2 + 1
            byte r2 = r8[r2]     // Catch:{ Exception -> 0x0081 }
            r2 = r2 & 255(0xff, float:3.57E-43)
            r4 = r2 & 128(0x80, float:1.794E-43)
            if (r4 == 0) goto L_0x01c4
            r2 = r2 & 127(0x7f, float:1.78E-43)
            r4 = r2
            r2 = r1
        L_0x01b4:
            int r5 = r4 + -1
            if (r4 <= 0) goto L_0x01c4
            int r2 = r2 << 8
            int r4 = r3 + 1
            byte r3 = r8[r3]     // Catch:{ Exception -> 0x0081 }
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r2 = r2 + r3
            r3 = r4
            r4 = r5
            goto L_0x01b4
        L_0x01c4:
            byte[] r4 = new byte[r2]     // Catch:{ Exception -> 0x0081 }
            r7.ertertetetetete = r4     // Catch:{ Exception -> 0x0081 }
            byte[] r4 = r7.ertertetetetete     // Catch:{ Exception -> 0x0081 }
            r5 = 0
            java.lang.System.arraycopy(r8, r3, r4, r5, r2)     // Catch:{ Exception -> 0x0081 }
            int r2 = r2 + r3
            int r2 = r2 + 1
            int r3 = r2 + 1
            byte r2 = r8[r2]     // Catch:{ Exception -> 0x0081 }
            r2 = r2 & 255(0xff, float:3.57E-43)
            r4 = r2 & 128(0x80, float:1.794E-43)
            if (r4 == 0) goto L_0x01ef
            r2 = r2 & 127(0x7f, float:1.78E-43)
            r4 = r2
            r2 = r1
        L_0x01df:
            int r5 = r4 + -1
            if (r4 <= 0) goto L_0x01ef
            int r2 = r2 << 8
            int r4 = r3 + 1
            byte r3 = r8[r3]     // Catch:{ Exception -> 0x0081 }
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r2 = r2 + r3
            r3 = r4
            r4 = r5
            goto L_0x01df
        L_0x01ef:
            byte[] r4 = new byte[r2]     // Catch:{ Exception -> 0x0081 }
            r7.uiiliuluiiuluilulu = r4     // Catch:{ Exception -> 0x0081 }
            byte[] r4 = r7.uiiliuluiiuluilulu     // Catch:{ Exception -> 0x0081 }
            r5 = 0
            java.lang.System.arraycopy(r8, r3, r4, r5, r2)     // Catch:{ Exception -> 0x0081 }
            int r2 = r2 + r3
            int r2 = r2 + 1
            int r3 = r2 + 1
            byte r2 = r8[r2]     // Catch:{ Exception -> 0x0081 }
            r2 = r2 & 255(0xff, float:3.57E-43)
            r4 = r2 & 128(0x80, float:1.794E-43)
            if (r4 == 0) goto L_0x021a
            r2 = r2 & 127(0x7f, float:1.78E-43)
            r4 = r2
            r2 = r1
        L_0x020a:
            int r5 = r4 + -1
            if (r4 <= 0) goto L_0x021a
            int r2 = r2 << 8
            int r4 = r3 + 1
            byte r3 = r8[r3]     // Catch:{ Exception -> 0x0081 }
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r2 = r2 + r3
            r3 = r4
            r4 = r5
            goto L_0x020a
        L_0x021a:
            byte[] r4 = new byte[r2]     // Catch:{ Exception -> 0x0081 }
            r7.reyeyreyeyeyey = r4     // Catch:{ Exception -> 0x0081 }
            byte[] r4 = r7.reyeyreyeyeyey     // Catch:{ Exception -> 0x0081 }
            r5 = 0
            java.lang.System.arraycopy(r8, r3, r4, r5, r2)     // Catch:{ Exception -> 0x0081 }
            int r2 = r2 + r3
            byte[] r2 = r7.eryryhrtytujrtujrurt     // Catch:{ Exception -> 0x0081 }
            if (r2 == 0) goto L_0x0032
            java.math.BigInteger r2 = new java.math.BigInteger     // Catch:{ Exception -> 0x0081 }
            byte[] r3 = r7.eryryhrtytujrtujrurt     // Catch:{ Exception -> 0x0081 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0081 }
            int r2 = r2.bitLength()     // Catch:{ Exception -> 0x0081 }
            r7.a = r2     // Catch:{ Exception -> 0x0081 }
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jcraft.jsch.KeyPairRSA.sdfsdfsdfsdf(byte[]):boolean");
    }

    /* access modifiers changed from: package-private */
    public byte[] sdfsdfsdfsdf() {
        int fsafsdfsfsdfsfsdfsd = fsafsdfsfsdfsfsdfsd(1) + 1 + 1 + 1 + fsafsdfsfsdfsfsdfsd(this.eryryhrtytujrtujrurt.length) + this.eryryhrtytujrtujrurt.length + 1 + fsafsdfsfsdfsfsdfsd(this.rthrthrtjrtjrjtdcbcvbc.length) + this.rthrthrtjrtjrjtdcbcvbc.length + 1 + fsafsdfsfsdfsfsdfsd(this.wrrgehehehehe.length) + this.wrrgehehehehe.length + 1 + fsafsdfsfsdfsfsdfsd(this.fgjyukukuiki.length) + this.fgjyukukuiki.length + 1 + fsafsdfsfsdfsfsdfsd(this.htyjyukuilulululu.length) + this.htyjyukuilulululu.length + 1 + fsafsdfsfsdfsfsdfsd(this.ertertetetetete.length) + this.ertertetetetete.length + 1 + fsafsdfsfsdfsfsdfsd(this.uiiliuluiiuluilulu.length) + this.uiiliuluiiuluilulu.length + 1 + fsafsdfsfsdfsfsdfsd(this.reyeyreyeyeyey.length) + this.reyeyreyeyeyey.length;
        byte[] bArr = new byte[(fsafsdfsfsdfsfsdfsd(fsafsdfsfsdfsfsdfsd) + 1 + fsafsdfsfsdfsfsdfsd)];
        fsafsdfsfsdfsfsdfsd(bArr, fsafsdfsfsdfsfsdfsd(bArr, fsafsdfsfsdfsfsdfsd(bArr, fsafsdfsfsdfsfsdfsd(bArr, fsafsdfsfsdfsfsdfsd(bArr, fsafsdfsfsdfsfsdfsd(bArr, fsafsdfsfsdfsfsdfsd(bArr, fsafsdfsfsdfsfsdfsd(bArr, fsafsdfsfsdfsfsdfsd(bArr, fsafsdfsfsdfsfsdfsd(bArr, 0, fsafsdfsfsdfsfsdfsd), new byte[1]), this.eryryhrtytujrtujrurt), this.rthrthrtjrtjrjtdcbcvbc), this.wrrgehehehehe), this.fgjyukukuiki), this.htyjyukuilulululu), this.ertertetetetete), this.uiiliuluiiuluilulu), this.reyeyreyeyeyey);
        return bArr;
    }

    public byte[] serfwefewfwefewef() {
        byte[] serfwefewfwefewef = super.serfwefewfwefewef();
        if (serfwefewfwefewef != null) {
            return serfwefewfwefewef;
        }
        if (this.rthrthrtjrtjrjtdcbcvbc == null) {
            return null;
        }
        return Buffer.fsafsdfsfsdfsfsdfsd(new byte[][]{d, this.rthrthrtjrtjrjtdcbcvbc, this.eryryhrtytujrtujrurt}).sdfsdfsdfsdf;
    }

    /* access modifiers changed from: package-private */
    public byte[] zxcvxvsdvsvsvs() {
        return d;
    }
}
