package com.jcraft.jsch;

import com.jcraft.jsch.Channel;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedOutputStream;
import java.util.Hashtable;

public class ChannelSftp extends ChannelSession {
    private static final String F = File.separator;
    private static final char G = File.separatorChar;
    private static boolean H = (((byte) File.separatorChar) == 92);
    private Hashtable A = null;
    private InputStream B = null;
    private boolean C = false;
    private boolean D = false;
    private boolean E = false;
    private String I;
    private String J = "UTF-8";
    private boolean K = true;
    private RequestQueue L = new RequestQueue(this, 16);
    private boolean g = false;
    private int r = 1;
    private int[] s = new int[1];
    private Buffer t;
    private Packet u;
    private Buffer v;
    private Packet w;
    private int x = 3;
    private int y = 3;
    private String z = String.valueOf(this.x);

    /* renamed from: com.jcraft.jsch.ChannelSftp$1  reason: invalid class name */
    class AnonymousClass1 extends OutputStream {
        private final SftpProgressMonitor ertertetetetete;
        private int eryryhrtytujrtujrurt;
        private final byte[] fgjyukukuiki;
        byte[] fsafsdfsfsdfsfsdfsd;
        private final long[] htyjyukuilulululu;
        private int hukuiluliulu;
        private int rthrthrtjrtjrjtdcbcvbc;
        private boolean sdfsdfsdfsdf;
        private int sdvsdvsvsevsvsev;
        private int[] serfwefewfwefewef;
        private final ChannelSftp uiiliuluiiuluilulu;
        private Header wrrgehehehehe;
        private boolean zxcvxvsdvsvsvs;

        public void close() {
            if (!this.zxcvxvsdvsvsvs) {
                flush();
                if (this.ertertetetetete != null) {
                    this.ertertetetetete.fsafsdfsfsdfsfsdfsd();
                }
                try {
                    ChannelSftp.fsafsdfsfsdfsfsdfsd(this.uiiliuluiiuluilulu, this.fgjyukukuiki, this.wrrgehehehehe);
                    this.zxcvxvsdvsvsvs = true;
                } catch (IOException e) {
                    throw e;
                } catch (Exception e2) {
                    throw new IOException(e2.toString());
                }
            }
        }

        public void flush() {
            if (this.zxcvxvsdvsvsvs) {
                throw new IOException("stream already closed");
            } else if (!this.sdfsdfsdfsdf) {
                while (this.rthrthrtjrtjrjtdcbcvbc > this.eryryhrtytujrtujrurt && ChannelSftp.fsafsdfsfsdfsfsdfsd(this.uiiliuluiiuluilulu, (int[]) null, this.wrrgehehehehe)) {
                    try {
                        this.eryryhrtytujrtujrurt++;
                    } catch (SftpException e) {
                        throw new IOException(e.toString());
                    }
                }
            }
        }

        public void write(int i) {
            this.fsafsdfsfsdfsfsdfsd[0] = (byte) i;
            write(this.fsafsdfsfsdfsfsdfsd, 0, 1);
        }

        public void write(byte[] bArr) {
            write(bArr, 0, bArr.length);
        }

        public void write(byte[] bArr, int i, int i2) {
            if (this.sdfsdfsdfsdf) {
                this.sdvsdvsvsevsvsev = ChannelSftp.fsafsdfsfsdfsfsdfsd(this.uiiliuluiiuluilulu);
                this.hukuiluliulu = ChannelSftp.fsafsdfsfsdfsfsdfsd(this.uiiliuluiiuluilulu);
                this.sdfsdfsdfsdf = false;
            }
            if (this.zxcvxvsdvsvsvs) {
                throw new IOException("stream already closed");
            }
            int i3 = i2;
            int i4 = i;
            while (i3 > 0) {
                try {
                    int fsafsdfsfsdfsfsdfsd2 = ChannelSftp.fsafsdfsfsdfsfsdfsd(this.uiiliuluiiuluilulu, this.fgjyukukuiki, this.htyjyukuilulululu[0], bArr, i4, i3);
                    this.rthrthrtjrtjrjtdcbcvbc++;
                    long[] jArr = this.htyjyukuilulululu;
                    jArr[0] = jArr[0] + ((long) fsafsdfsfsdfsfsdfsd2);
                    i4 += fsafsdfsfsdfsfsdfsd2;
                    i3 -= fsafsdfsfsdfsfsdfsd2;
                    if (ChannelSftp.fsafsdfsfsdfsfsdfsd(this.uiiliuluiiuluilulu) - 1 == this.sdvsdvsvsevsvsev || ChannelSftp.sdfsdfsdfsdf(this.uiiliuluiiuluilulu).available() >= 1024) {
                        while (ChannelSftp.sdfsdfsdfsdf(this.uiiliuluiiuluilulu).available() > 0 && ChannelSftp.fsafsdfsfsdfsfsdfsd(this.uiiliuluiiuluilulu, this.serfwefewfwefewef, this.wrrgehehehehe)) {
                            this.hukuiluliulu = this.serfwefewfwefewef[0];
                            if (this.sdvsdvsvsevsvsev > this.hukuiluliulu || this.hukuiluliulu > ChannelSftp.fsafsdfsfsdfsfsdfsd(this.uiiliuluiiuluilulu) - 1) {
                                throw new SftpException(4, "");
                            }
                            this.eryryhrtytujrtujrurt++;
                        }
                    }
                } catch (IOException e) {
                    throw e;
                } catch (Exception e2) {
                    throw new IOException(e2.toString());
                }
            }
            if (this.ertertetetetete != null && !this.ertertetetetete.fsafsdfsfsdfsfsdfsd((long) i2)) {
                close();
                throw new IOException("canceled");
            }
        }
    }

    /* renamed from: com.jcraft.jsch.ChannelSftp$2  reason: invalid class name */
    class AnonymousClass2 extends InputStream {
        int eryryhrtytujrtujrurt;
        private final byte[] fgjyukukuiki;
        long fsafsdfsfsdfsfsdfsd;
        private final ChannelSftp htyjyukuilulululu;
        Header hukuiluliulu;
        long rthrthrtjrtjrjtdcbcvbc;
        boolean sdfsdfsdfsdf;
        byte[] sdvsdvsvsevsvsev;
        byte[] serfwefewfwefewef;
        private final SftpProgressMonitor wrrgehehehehe;
        int zxcvxvsdvsvsvs;

        public void close() {
            if (!this.sdfsdfsdfsdf) {
                this.sdfsdfsdfsdf = true;
                if (this.wrrgehehehehe != null) {
                    this.wrrgehehehehe.fsafsdfsfsdfsfsdfsd();
                }
                ChannelSftp.sdvsdvsvsevsvsev(this.htyjyukuilulululu).fsafsdfsfsdfsfsdfsd(this.hukuiluliulu, ChannelSftp.zxcvxvsdvsvsvs(this.htyjyukuilulululu));
                try {
                    ChannelSftp.fsafsdfsfsdfsfsdfsd(this.htyjyukuilulululu, this.fgjyukukuiki, this.hukuiluliulu);
                } catch (Exception e) {
                    throw new IOException("error");
                }
            }
        }

        public int read() {
            if (!this.sdfsdfsdfsdf && read(this.serfwefewfwefewef, 0, 1) != -1) {
                return this.serfwefewfwefewef[0] & 255;
            }
            return -1;
        }

        public int read(byte[] bArr) {
            if (this.sdfsdfsdfsdf) {
                return -1;
            }
            return read(bArr, 0, bArr.length);
        }

        public int read(byte[] bArr, int i, int i2) {
            int i3 = 0;
            if (this.sdfsdfsdfsdf) {
                return -1;
            }
            if (bArr == null) {
                throw new NullPointerException();
            } else if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
                throw new IndexOutOfBoundsException();
            } else if (i2 == 0) {
                return 0;
            } else {
                if (this.zxcvxvsdvsvsvs > 0) {
                    int i4 = this.zxcvxvsdvsvsvs;
                    if (i4 <= i2) {
                        i2 = i4;
                    }
                    System.arraycopy(this.sdvsdvsvsevsvsev, 0, bArr, i, i2);
                    if (i2 != this.zxcvxvsdvsvsvs) {
                        System.arraycopy(this.sdvsdvsvsevsvsev, i2, this.sdvsdvsvsevsvsev, 0, this.zxcvxvsdvsvsvs - i2);
                    }
                    if (this.wrrgehehehehe == null || this.wrrgehehehehe.fsafsdfsfsdfsfsdfsd((long) i2)) {
                        this.zxcvxvsdvsvsvs -= i2;
                        return i2;
                    }
                    close();
                    return -1;
                }
                if (ChannelSftp.zxcvxvsdvsvsvs(this.htyjyukuilulululu).sdfsdfsdfsdf.length - 13 < i2) {
                    i2 = ChannelSftp.zxcvxvsdvsvsvs(this.htyjyukuilulululu).sdfsdfsdfsdf.length - 13;
                }
                if (ChannelSftp.serfwefewfwefewef(this.htyjyukuilulululu) == 0 && i2 > 1024) {
                    i2 = 1024;
                }
                if (ChannelSftp.sdvsdvsvsevsvsev(this.htyjyukuilulululu).sdfsdfsdfsdf() != 0) {
                }
                int length = ChannelSftp.zxcvxvsdvsvsvs(this.htyjyukuilulululu).sdfsdfsdfsdf.length - 13;
                if (ChannelSftp.serfwefewfwefewef(this.htyjyukuilulululu) == 0) {
                    length = 1024;
                }
                while (ChannelSftp.sdvsdvsvsevsvsev(this.htyjyukuilulululu).sdfsdfsdfsdf() < this.eryryhrtytujrtujrurt) {
                    try {
                        ChannelSftp.fsafsdfsfsdfsfsdfsd(this.htyjyukuilulululu, this.fgjyukukuiki, this.rthrthrtjrtjrjtdcbcvbc, length, ChannelSftp.sdvsdvsvsevsvsev(this.htyjyukuilulululu));
                        this.rthrthrtjrtjrjtdcbcvbc += (long) length;
                    } catch (Exception e) {
                        throw new IOException("error");
                    }
                }
                this.hukuiluliulu = ChannelSftp.fsafsdfsfsdfsfsdfsd(this.htyjyukuilulululu, ChannelSftp.zxcvxvsdvsvsvs(this.htyjyukuilulululu), this.hukuiluliulu);
                this.zxcvxvsdvsvsvs = this.hukuiluliulu.fsafsdfsfsdfsfsdfsd;
                int i5 = this.hukuiluliulu.sdfsdfsdfsdf;
                int i6 = this.hukuiluliulu.zxcvxvsdvsvsvs;
                try {
                    RequestQueue.Request fsafsdfsfsdfsfsdfsd2 = ChannelSftp.sdvsdvsvsevsvsev(this.htyjyukuilulululu).fsafsdfsfsdfsfsdfsd(this.hukuiluliulu.zxcvxvsdvsvsvs);
                    if (i5 != 101 && i5 != 103) {
                        throw new IOException("error");
                    } else if (i5 == 101) {
                        ChannelSftp.fsafsdfsfsdfsfsdfsd(this.htyjyukuilulululu, ChannelSftp.zxcvxvsdvsvsvs(this.htyjyukuilulululu), this.zxcvxvsdvsvsvs);
                        int zxcvxvsdvsvsvs2 = ChannelSftp.zxcvxvsdvsvsvs(this.htyjyukuilulululu).zxcvxvsdvsvsvs();
                        this.zxcvxvsdvsvsvs = 0;
                        if (zxcvxvsdvsvsvs2 == 1) {
                            close();
                            return -1;
                        }
                        throw new IOException("error");
                    } else {
                        ChannelSftp.zxcvxvsdvsvsvs(this.htyjyukuilulululu).ertertetetetete();
                        ChannelSftp.fsafsdfsfsdfsfsdfsd(this.htyjyukuilulululu, ChannelSftp.zxcvxvsdvsvsvs(this.htyjyukuilulululu).sdfsdfsdfsdf, 0, 4);
                        int zxcvxvsdvsvsvs3 = ChannelSftp.zxcvxvsdvsvsvs(this.htyjyukuilulululu).zxcvxvsdvsvsvs();
                        this.zxcvxvsdvsvsvs -= 4;
                        int i7 = this.zxcvxvsdvsvsvs - zxcvxvsdvsvsvs3;
                        this.fsafsdfsfsdfsfsdfsd += (long) zxcvxvsdvsvsvs3;
                        if (zxcvxvsdvsvsvs3 <= 0) {
                            return 0;
                        }
                        if (zxcvxvsdvsvsvs3 <= i2) {
                            i2 = zxcvxvsdvsvsvs3;
                        }
                        int read = ChannelSftp.sdfsdfsdfsdf(this.htyjyukuilulululu).read(bArr, i, i2);
                        if (read < 0) {
                            return -1;
                        }
                        int i8 = zxcvxvsdvsvsvs3 - read;
                        this.zxcvxvsdvsvsvs = i8;
                        if (i8 > 0) {
                            if (this.sdvsdvsvsevsvsev.length < i8) {
                                this.sdvsdvsvsevsvsev = new byte[i8];
                            }
                            while (i8 > 0) {
                                int read2 = ChannelSftp.sdfsdfsdfsdf(this.htyjyukuilulululu).read(this.sdvsdvsvsevsvsev, i3, i8);
                                if (read2 <= 0) {
                                    break;
                                }
                                i3 += read2;
                                i8 -= read2;
                            }
                        }
                        if (i7 > 0) {
                            ChannelSftp.sdfsdfsdfsdf(this.htyjyukuilulululu).skip((long) i7);
                        }
                        if (((long) zxcvxvsdvsvsvs3) < fsafsdfsfsdfsfsdfsd2.zxcvxvsdvsvsvs) {
                            ChannelSftp.sdvsdvsvsevsvsev(this.htyjyukuilulululu).fsafsdfsfsdfsfsdfsd(this.hukuiluliulu, ChannelSftp.zxcvxvsdvsvsvs(this.htyjyukuilulululu));
                            try {
                                ChannelSftp.fsafsdfsfsdfsfsdfsd(this.htyjyukuilulululu, this.fgjyukukuiki, fsafsdfsfsdfsfsdfsd2.sdfsdfsdfsdf + ((long) zxcvxvsdvsvsvs3), (int) (fsafsdfsfsdfsfsdfsd2.zxcvxvsdvsvsvs - ((long) zxcvxvsdvsvsvs3)), ChannelSftp.sdvsdvsvsevsvsev(this.htyjyukuilulululu));
                                this.rthrthrtjrtjrjtdcbcvbc = fsafsdfsfsdfsfsdfsd2.sdfsdfsdfsdf + fsafsdfsfsdfsfsdfsd2.zxcvxvsdvsvsvs;
                            } catch (Exception e2) {
                                throw new IOException("error");
                            }
                        }
                        if (this.eryryhrtytujrtujrurt < ChannelSftp.sdvsdvsvsevsvsev(this.htyjyukuilulululu).zxcvxvsdvsvsvs()) {
                            this.eryryhrtytujrtujrurt++;
                        }
                        if (this.wrrgehehehehe == null || this.wrrgehehehehe.fsafsdfsfsdfsfsdfsd((long) read)) {
                            return read;
                        }
                        close();
                        return -1;
                    }
                } catch (RequestQueue.OutOfOrderException e3) {
                    this.rthrthrtjrtjrjtdcbcvbc = e3.fsafsdfsfsdfsfsdfsd;
                    skip((long) this.hukuiluliulu.fsafsdfsfsdfsfsdfsd);
                    ChannelSftp.sdvsdvsvsevsvsev(this.htyjyukuilulululu).fsafsdfsfsdfsfsdfsd(this.hukuiluliulu, ChannelSftp.zxcvxvsdvsvsvs(this.htyjyukuilulululu));
                    return 0;
                } catch (SftpException e4) {
                    throw new IOException(new StringBuffer().append("error: ").append(e4.toString()).toString());
                }
            }
        }
    }

    /* renamed from: com.jcraft.jsch.ChannelSftp$3  reason: invalid class name */
    class AnonymousClass3 implements LsEntrySelector {
    }

    class Header {
        int fsafsdfsfsdfsfsdfsd;
        int sdfsdfsdfsdf;
        private final ChannelSftp serfwefewfwefewef;
        int zxcvxvsdvsvsvs;

        Header(ChannelSftp channelSftp) {
            this.serfwefewfwefewef = channelSftp;
        }
    }

    public class LsEntry implements Comparable {
        private String fsafsdfsfsdfsfsdfsd;
        private String sdfsdfsdfsdf;

        public int compareTo(Object obj) {
            if (obj instanceof LsEntry) {
                return this.fsafsdfsfsdfsfsdfsd.compareTo(((LsEntry) obj).fsafsdfsfsdfsfsdfsd());
            }
            throw new ClassCastException("a decendent of LsEntry must be given.");
        }

        public String fsafsdfsfsdfsfsdfsd() {
            return this.fsafsdfsfsdfsfsdfsd;
        }

        public String toString() {
            return this.sdfsdfsdfsdf;
        }
    }

    public interface LsEntrySelector {
    }

    class RequestQueue {
        Request[] fsafsdfsfsdfsfsdfsd = null;
        int sdfsdfsdfsdf;
        private final ChannelSftp serfwefewfwefewef;
        int zxcvxvsdvsvsvs;

        class OutOfOrderException extends Exception {
            long fsafsdfsfsdfsfsdfsd;
            private final RequestQueue sdfsdfsdfsdf;

            OutOfOrderException(RequestQueue requestQueue, long j) {
                this.sdfsdfsdfsdf = requestQueue;
                this.fsafsdfsfsdfsfsdfsd = j;
            }
        }

        class Request {
            int fsafsdfsfsdfsfsdfsd;
            long sdfsdfsdfsdf;
            private final RequestQueue serfwefewfwefewef;
            long zxcvxvsdvsvsvs;

            Request(RequestQueue requestQueue) {
                this.serfwefewfwefewef = requestQueue;
            }
        }

        RequestQueue(ChannelSftp channelSftp, int i) {
            this.serfwefewfwefewef = channelSftp;
            this.fsafsdfsfsdfsfsdfsd = new Request[i];
            for (int i2 = 0; i2 < this.fsafsdfsfsdfsfsdfsd.length; i2++) {
                this.fsafsdfsfsdfsfsdfsd[i2] = new Request(this);
            }
            fsafsdfsfsdfsfsdfsd();
        }

        /* access modifiers changed from: package-private */
        public Request fsafsdfsfsdfsfsdfsd(int i) {
            boolean z = false;
            this.zxcvxvsdvsvsvs--;
            int i2 = this.sdfsdfsdfsdf;
            this.sdfsdfsdfsdf++;
            if (this.sdfsdfsdfsdf == this.fsafsdfsfsdfsfsdfsd.length) {
                this.sdfsdfsdfsdf = 0;
            }
            if (this.fsafsdfsfsdfsfsdfsd[i2].fsafsdfsfsdfsfsdfsd != i) {
                long serfwefewfwefewef2 = serfwefewfwefewef();
                int i3 = 0;
                while (true) {
                    if (i3 >= this.fsafsdfsfsdfsfsdfsd.length) {
                        break;
                    } else if (this.fsafsdfsfsdfsfsdfsd[i3].fsafsdfsfsdfsfsdfsd == i) {
                        this.fsafsdfsfsdfsfsdfsd[i3].fsafsdfsfsdfsfsdfsd = 0;
                        z = true;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (z) {
                    throw new OutOfOrderException(this, serfwefewfwefewef2);
                }
                throw new SftpException(4, new StringBuffer().append("RequestQueue: unknown request id ").append(i).toString());
            }
            this.fsafsdfsfsdfsfsdfsd[i2].fsafsdfsfsdfsfsdfsd = 0;
            return this.fsafsdfsfsdfsfsdfsd[i2];
        }

        /* access modifiers changed from: package-private */
        public void fsafsdfsfsdfsfsdfsd() {
            this.zxcvxvsdvsvsvs = 0;
            this.sdfsdfsdfsdf = 0;
        }

        /* access modifiers changed from: package-private */
        public void fsafsdfsfsdfsfsdfsd(int i, long j, int i2) {
            if (this.zxcvxvsdvsvsvs == 0) {
                this.sdfsdfsdfsdf = 0;
            }
            int i3 = this.sdfsdfsdfsdf + this.zxcvxvsdvsvsvs;
            if (i3 >= this.fsafsdfsfsdfsfsdfsd.length) {
                i3 -= this.fsafsdfsfsdfsfsdfsd.length;
            }
            this.fsafsdfsfsdfsfsdfsd[i3].fsafsdfsfsdfsfsdfsd = i;
            this.fsafsdfsfsdfsfsdfsd[i3].sdfsdfsdfsdf = j;
            this.fsafsdfsfsdfsfsdfsd[i3].zxcvxvsdvsvsvs = (long) i2;
            this.zxcvxvsdvsvsvs++;
        }

        /* access modifiers changed from: package-private */
        public void fsafsdfsfsdfsfsdfsd(Header header, Buffer buffer) {
            int i = this.zxcvxvsdvsvsvs;
            for (int i2 = 0; i2 < i; i2++) {
                header = ChannelSftp.fsafsdfsfsdfsfsdfsd(this.serfwefewfwefewef, buffer, header);
                int i3 = header.fsafsdfsfsdfsfsdfsd;
                int i4 = 0;
                while (true) {
                    if (i4 >= this.fsafsdfsfsdfsfsdfsd.length) {
                        break;
                    } else if (this.fsafsdfsfsdfsfsdfsd[i4].fsafsdfsfsdfsfsdfsd == header.zxcvxvsdvsvsvs) {
                        this.fsafsdfsfsdfsfsdfsd[i4].fsafsdfsfsdfsfsdfsd = 0;
                        break;
                    } else {
                        i4++;
                    }
                }
                ChannelSftp.fsafsdfsfsdfsfsdfsd(this.serfwefewfwefewef, (long) i3);
            }
            fsafsdfsfsdfsfsdfsd();
        }

        /* access modifiers changed from: package-private */
        public int sdfsdfsdfsdf() {
            return this.zxcvxvsdvsvsvs;
        }

        /* access modifiers changed from: package-private */
        public long serfwefewfwefewef() {
            long j = Long.MAX_VALUE;
            for (int i = 0; i < this.fsafsdfsfsdfsfsdfsd.length; i++) {
                if (this.fsafsdfsfsdfsfsdfsd[i].fsafsdfsfsdfsfsdfsd != 0 && j > this.fsafsdfsfsdfsfsdfsd[i].sdfsdfsdfsdf) {
                    j = this.fsafsdfsfsdfsfsdfsd[i].sdfsdfsdfsdf;
                }
            }
            return j;
        }

        /* access modifiers changed from: package-private */
        public int zxcvxvsdvsvsvs() {
            return this.fsafsdfsfsdfsfsdfsd.length;
        }
    }

    public ChannelSftp() {
        zxcvxvsdvsvsvs(2097152);
        serfwefewfwefewef(2097152);
        sdvsdvsvsevsvsev(32768);
    }

    private void a() {
        this.u.fsafsdfsfsdfsfsdfsd();
        fsafsdfsfsdfsfsdfsd((byte) 1, 5);
        this.t.fsafsdfsfsdfsfsdfsd(3);
        fgjyukukuiki().fsafsdfsfsdfsfsdfsd(this.u, this, 9);
    }

    static int fsafsdfsfsdfsfsdfsd(ChannelSftp channelSftp) {
        return channelSftp.r;
    }

    static int fsafsdfsfsdfsfsdfsd(ChannelSftp channelSftp, byte[] bArr, int i, int i2) {
        return channelSftp.zxcvxvsdvsvsvs(bArr, i, i2);
    }

    static int fsafsdfsfsdfsfsdfsd(ChannelSftp channelSftp, byte[] bArr, long j, byte[] bArr2, int i, int i2) {
        return channelSftp.fsafsdfsfsdfsfsdfsd(bArr, j, bArr2, i, i2);
    }

    private int fsafsdfsfsdfsfsdfsd(byte[] bArr, long j, byte[] bArr2, int i, int i2) {
        this.w.fsafsdfsfsdfsfsdfsd();
        if (this.v.sdfsdfsdfsdf.length < this.v.zxcvxvsdvsvsvs + 13 + 21 + bArr.length + i2 + 128) {
            i2 = this.v.sdfsdfsdfsdf.length - ((((this.v.zxcvxvsdvsvsvs + 13) + 21) + bArr.length) + 128);
        }
        fsafsdfsfsdfsfsdfsd(this.v, (byte) 6, bArr.length + 21 + i2);
        Buffer buffer = this.v;
        int i3 = this.r;
        this.r = i3 + 1;
        buffer.fsafsdfsfsdfsfsdfsd(i3);
        this.v.sdfsdfsdfsdf(bArr);
        this.v.fsafsdfsfsdfsfsdfsd(j);
        if (this.v.sdfsdfsdfsdf != bArr2) {
            this.v.sdfsdfsdfsdf(bArr2, i, i2);
        } else {
            this.v.fsafsdfsfsdfsfsdfsd(i2);
            this.v.sdfsdfsdfsdf(i2);
        }
        fgjyukukuiki().fsafsdfsfsdfsfsdfsd(this.w, this, bArr.length + 21 + i2 + 4);
        return i2;
    }

    private Header fsafsdfsfsdfsfsdfsd(Buffer buffer, Header header) {
        buffer.ertertetetetete();
        zxcvxvsdvsvsvs(buffer.sdfsdfsdfsdf, 0, 9);
        header.fsafsdfsfsdfsfsdfsd = buffer.zxcvxvsdvsvsvs() - 5;
        header.sdfsdfsdfsdf = buffer.hukuiluliulu() & 255;
        header.zxcvxvsdvsvsvs = buffer.zxcvxvsdvsvsvs();
        return header;
    }

    static Header fsafsdfsfsdfsfsdfsd(ChannelSftp channelSftp, Buffer buffer, Header header) {
        return channelSftp.fsafsdfsfsdfsfsdfsd(buffer, header);
    }

    private void fsafsdfsfsdfsfsdfsd(byte b, int i) {
        fsafsdfsfsdfsfsdfsd(this.t, b, i);
    }

    private void fsafsdfsfsdfsfsdfsd(byte b, byte[] bArr) {
        fsafsdfsfsdfsfsdfsd(b, bArr, (String) null);
    }

    private void fsafsdfsfsdfsfsdfsd(byte b, byte[] bArr, String str) {
        this.u.fsafsdfsfsdfsfsdfsd();
        int length = bArr.length + 9;
        if (str == null) {
            fsafsdfsfsdfsfsdfsd(b, length);
            Buffer buffer = this.t;
            int i = this.r;
            this.r = i + 1;
            buffer.fsafsdfsfsdfsfsdfsd(i);
        } else {
            length += str.length() + 4;
            fsafsdfsfsdfsfsdfsd((byte) -56, length);
            Buffer buffer2 = this.t;
            int i2 = this.r;
            this.r = i2 + 1;
            buffer2.fsafsdfsfsdfsfsdfsd(i2);
            this.t.sdfsdfsdfsdf(Util.fsafsdfsfsdfsfsdfsd(str));
        }
        this.t.sdfsdfsdfsdf(bArr);
        fgjyukukuiki().fsafsdfsfsdfsfsdfsd(this.u, this, length + 4);
    }

    private void fsafsdfsfsdfsfsdfsd(Buffer buffer, byte b, int i) {
        buffer.fsafsdfsfsdfsfsdfsd((byte) 94);
        buffer.fsafsdfsfsdfsfsdfsd(this.zxcvxvsdvsvsvs);
        buffer.fsafsdfsfsdfsfsdfsd(i + 4);
        buffer.fsafsdfsfsdfsfsdfsd(i);
        buffer.fsafsdfsfsdfsfsdfsd(b);
    }

    private void fsafsdfsfsdfsfsdfsd(Buffer buffer, int i) {
        if (this.y < 3 || buffer.fsafsdfsfsdfsfsdfsd() < 4) {
            throw new SftpException(i, "Failure");
        }
        throw new SftpException(i, Util.fsafsdfsfsdfsfsdfsd(buffer.wrrgehehehehe(), "UTF-8"));
    }

    static void fsafsdfsfsdfsfsdfsd(ChannelSftp channelSftp, long j) {
        channelSftp.zxcvxvsdvsvsvs(j);
    }

    static void fsafsdfsfsdfsfsdfsd(ChannelSftp channelSftp, Buffer buffer, int i) {
        channelSftp.sdfsdfsdfsdf(buffer, i);
    }

    static void fsafsdfsfsdfsfsdfsd(ChannelSftp channelSftp, byte[] bArr, long j, int i, RequestQueue requestQueue) {
        channelSftp.fsafsdfsfsdfsfsdfsd(bArr, j, i, requestQueue);
    }

    private void fsafsdfsfsdfsfsdfsd(byte[] bArr) {
        fsafsdfsfsdfsfsdfsd((byte) 4, bArr);
    }

    private void fsafsdfsfsdfsfsdfsd(byte[] bArr, long j, int i, RequestQueue requestQueue) {
        this.u.fsafsdfsfsdfsfsdfsd();
        fsafsdfsfsdfsfsdfsd((byte) 5, bArr.length + 21);
        Buffer buffer = this.t;
        int i2 = this.r;
        this.r = i2 + 1;
        buffer.fsafsdfsfsdfsfsdfsd(i2);
        this.t.sdfsdfsdfsdf(bArr);
        this.t.fsafsdfsfsdfsfsdfsd(j);
        this.t.fsafsdfsfsdfsfsdfsd(i);
        fgjyukukuiki().fsafsdfsfsdfsfsdfsd(this.u, this, bArr.length + 21 + 4);
        if (requestQueue != null) {
            requestQueue.fsafsdfsfsdfsfsdfsd(this.r - 1, j, i);
        }
    }

    static boolean fsafsdfsfsdfsfsdfsd(ChannelSftp channelSftp, byte[] bArr, Header header) {
        return channelSftp.fsafsdfsfsdfsfsdfsd(bArr, header);
    }

    static boolean fsafsdfsfsdfsfsdfsd(ChannelSftp channelSftp, int[] iArr, Header header) {
        return channelSftp.fsafsdfsfsdfsfsdfsd(iArr, header);
    }

    private boolean fsafsdfsfsdfsfsdfsd(byte[] bArr, Header header) {
        fsafsdfsfsdfsfsdfsd(bArr);
        return fsafsdfsfsdfsfsdfsd((int[]) null, header);
    }

    private boolean fsafsdfsfsdfsfsdfsd(int[] iArr, Header header) {
        Header fsafsdfsfsdfsfsdfsd = fsafsdfsfsdfsfsdfsd(this.t, header);
        int i = fsafsdfsfsdfsfsdfsd.fsafsdfsfsdfsfsdfsd;
        int i2 = fsafsdfsfsdfsfsdfsd.sdfsdfsdfsdf;
        if (iArr != null) {
            iArr[0] = fsafsdfsfsdfsfsdfsd.zxcvxvsdvsvsvs;
        }
        sdfsdfsdfsdf(this.t, i);
        if (i2 != 101) {
            throw new SftpException(4, "");
        }
        int zxcvxvsdvsvsvs = this.t.zxcvxvsdvsvsvs();
        if (zxcvxvsdvsvsvs == 0) {
            return true;
        }
        fsafsdfsfsdfsfsdfsd(this.t, zxcvxvsdvsvsvs);
        return true;
    }

    static InputStream sdfsdfsdfsdf(ChannelSftp channelSftp) {
        return channelSftp.B;
    }

    private void sdfsdfsdfsdf(Buffer buffer, int i) {
        buffer.fgjyukukuiki();
        zxcvxvsdvsvsvs(buffer.sdfsdfsdfsdf, 0, i);
        buffer.sdfsdfsdfsdf(i);
    }

    static RequestQueue sdvsdvsvsevsvsev(ChannelSftp channelSftp) {
        return channelSftp.L;
    }

    static int serfwefewfwefewef(ChannelSftp channelSftp) {
        return channelSftp.y;
    }

    private int zxcvxvsdvsvsvs(byte[] bArr, int i, int i2) {
        int i3 = i;
        while (i2 > 0) {
            int read = this.B.read(bArr, i3, i2);
            if (read <= 0) {
                throw new IOException("inputstream is closed");
            }
            i3 += read;
            i2 -= read;
        }
        return i3 - i;
    }

    static Buffer zxcvxvsdvsvsvs(ChannelSftp channelSftp) {
        return channelSftp.t;
    }

    private void zxcvxvsdvsvsvs(long j) {
        while (j > 0) {
            long skip = this.B.skip(j);
            if (skip > 0) {
                j -= skip;
            } else {
                return;
            }
        }
    }

    public void rthrthrtjrtjrjtdcbcvbc() {
        super.rthrthrtjrtjrjtdcbcvbc();
    }

    /* access modifiers changed from: package-private */
    public void sdfsdfsdfsdf() {
    }

    public void zxcvxvsdvsvsvs() {
        try {
            PipedOutputStream pipedOutputStream = new PipedOutputStream();
            this.fgjyukukuiki.fsafsdfsfsdfsfsdfsd(pipedOutputStream);
            this.fgjyukukuiki.fsafsdfsfsdfsfsdfsd(new Channel.MyPipedInputStream(this, pipedOutputStream, this.wrrgehehehehe));
            this.B = this.fgjyukukuiki.fsafsdfsfsdfsfsdfsd;
            if (this.B == null) {
                throw new JSchException("channel is down");
            }
            new RequestSftp().fsafsdfsfsdfsfsdfsd(fgjyukukuiki(), this);
            this.t = new Buffer(this.eryryhrtytujrtujrurt);
            this.u = new Packet(this.t);
            this.v = new Buffer(this.wrrgehehehehe);
            this.w = new Packet(this.v);
            a();
            Header fsafsdfsfsdfsfsdfsd = fsafsdfsfsdfsfsdfsd(this.t, new Header(this));
            int i = fsafsdfsfsdfsfsdfsd.fsafsdfsfsdfsfsdfsd;
            if (i > 262144) {
                throw new SftpException(4, new StringBuffer().append("Received message is too long: ").append(i).toString());
            }
            int i2 = fsafsdfsfsdfsfsdfsd.sdfsdfsdfsdf;
            this.y = fsafsdfsfsdfsfsdfsd.zxcvxvsdvsvsvs;
            this.A = new Hashtable();
            if (i > 0) {
                sdfsdfsdfsdf(this.t, i);
                while (i > 0) {
                    byte[] wrrgehehehehe = this.t.wrrgehehehehe();
                    int length = i - (wrrgehehehehe.length + 4);
                    byte[] wrrgehehehehe2 = this.t.wrrgehehehehe();
                    i = length - (wrrgehehehehe2.length + 4);
                    this.A.put(Util.fsafsdfsfsdfsfsdfsd(wrrgehehehehe), Util.fsafsdfsfsdfsfsdfsd(wrrgehehehehe2));
                }
            }
            if (this.A.get("posix-rename@openssh.com") != null && this.A.get("posix-rename@openssh.com").equals("1")) {
                this.C = true;
            }
            if (this.A.get("statvfs@openssh.com") != null && this.A.get("statvfs@openssh.com").equals("2")) {
                this.D = true;
            }
            if (this.A.get("hardlink@openssh.com") != null && this.A.get("hardlink@openssh.com").equals("1")) {
                this.E = true;
            }
            this.I = new File(".").getCanonicalPath();
        } catch (Exception e) {
            if (e instanceof JSchException) {
                throw ((JSchException) e);
            } else if (e instanceof Throwable) {
                throw new JSchException(e.toString(), e);
            } else {
                throw new JSchException(e.toString());
            }
        }
    }
}
