package com.jcraft.jsch;

import com.jcraft.jsch.ConfigRepository;
import java.util.Hashtable;
import java.util.Vector;

public class OpenSSHConfig implements ConfigRepository {
    private static final Hashtable sdvsdvsvsevsvsev = new Hashtable();
    private final Vector serfwefewfwefewef;
    private final Hashtable zxcvxvsdvsvsvs;

    class MyConfig implements ConfigRepository.Config {
        private String fsafsdfsfsdfsfsdfsd;
        private Vector sdfsdfsdfsdf = new Vector();
        private final OpenSSHConfig zxcvxvsdvsvsvs;

        MyConfig(OpenSSHConfig openSSHConfig, String str) {
            boolean z;
            this.zxcvxvsdvsvsvs = openSSHConfig;
            this.fsafsdfsfsdfsfsdfsd = str;
            this.sdfsdfsdfsdf.addElement(OpenSSHConfig.fsafsdfsfsdfsfsdfsd(openSSHConfig).get(""));
            byte[] fsafsdfsfsdfsfsdfsd2 = Util.fsafsdfsfsdfsfsdfsd(str);
            if (OpenSSHConfig.sdfsdfsdfsdf(openSSHConfig).size() > 1) {
                for (int i = 1; i < OpenSSHConfig.sdfsdfsdfsdf(openSSHConfig).size(); i++) {
                    String[] split = ((String) OpenSSHConfig.sdfsdfsdfsdf(openSSHConfig).elementAt(i)).split("[ \t]");
                    for (String trim : split) {
                        String trim2 = trim.trim();
                        if (trim2.startsWith("!")) {
                            trim2 = trim2.substring(1).trim();
                            z = true;
                        } else {
                            z = false;
                        }
                        if (Util.fsafsdfsfsdfsfsdfsd(Util.fsafsdfsfsdfsfsdfsd(trim2), fsafsdfsfsdfsfsdfsd2)) {
                            if (!z) {
                                this.sdfsdfsdfsdf.addElement(OpenSSHConfig.fsafsdfsfsdfsfsdfsd(openSSHConfig).get((String) OpenSSHConfig.sdfsdfsdfsdf(openSSHConfig).elementAt(i)));
                            }
                        } else if (z) {
                            this.sdfsdfsdfsdf.addElement(OpenSSHConfig.fsafsdfsfsdfsfsdfsd(openSSHConfig).get((String) OpenSSHConfig.sdfsdfsdfsdf(openSSHConfig).elementAt(i)));
                        }
                    }
                }
            }
        }

        private String[] serfwefewfwefewef(String str) {
            String str2;
            String upperCase = str.toUpperCase();
            Vector vector = new Vector();
            for (int i = 0; i < this.sdfsdfsdfsdf.size(); i++) {
                Vector vector2 = (Vector) this.sdfsdfsdfsdf.elementAt(i);
                for (int i2 = 0; i2 < vector2.size(); i2++) {
                    String[] strArr = (String[]) vector2.elementAt(i2);
                    if (strArr[0].toUpperCase().equals(upperCase) && (str2 = strArr[1]) != null) {
                        vector.remove(str2);
                        vector.addElement(str2);
                    }
                }
            }
            String[] strArr2 = new String[vector.size()];
            vector.toArray(strArr2);
            return strArr2;
        }

        private String zxcvxvsdvsvsvs(String str) {
            String str2;
            String upperCase = (OpenSSHConfig.fsafsdfsfsdfsfsdfsd().get(str) != null ? (String) OpenSSHConfig.fsafsdfsfsdfsfsdfsd().get(str) : str).toUpperCase();
            int i = 0;
            String str3 = null;
            while (i < this.sdfsdfsdfsdf.size()) {
                Vector vector = (Vector) this.sdfsdfsdfsdf.elementAt(i);
                int i2 = 0;
                while (true) {
                    if (i2 >= vector.size()) {
                        str2 = str3;
                        break;
                    }
                    String[] strArr = (String[]) vector.elementAt(i2);
                    if (strArr[0].toUpperCase().equals(upperCase)) {
                        str2 = strArr[1];
                        break;
                    }
                    i2++;
                }
                if (str2 != null) {
                    return str2;
                }
                i++;
                str3 = str2;
            }
            return str3;
        }

        public String fsafsdfsfsdfsfsdfsd() {
            return zxcvxvsdvsvsvs("Hostname");
        }

        public String fsafsdfsfsdfsfsdfsd(String str) {
            if (!str.equals("compression.s2c") && !str.equals("compression.c2s")) {
                return zxcvxvsdvsvsvs(str);
            }
            String zxcvxvsdvsvsvs2 = zxcvxvsdvsvsvs(str);
            return (zxcvxvsdvsvsvs2 == null || zxcvxvsdvsvsvs2.equals("no")) ? "none,zlib@openssh.com,zlib" : "zlib@openssh.com,zlib,none";
        }

        public String sdfsdfsdfsdf() {
            return zxcvxvsdvsvsvs("User");
        }

        public String[] sdfsdfsdfsdf(String str) {
            return serfwefewfwefewef(str);
        }

        public int zxcvxvsdvsvsvs() {
            try {
                return Integer.parseInt(zxcvxvsdvsvsvs("Port"));
            } catch (NumberFormatException e) {
                return -1;
            }
        }
    }

    static {
        sdvsdvsvsevsvsev.put("kex", "KexAlgorithms");
        sdvsdvsvsevsvsev.put("server_host_key", "HostKeyAlgorithms");
        sdvsdvsvsevsvsev.put("cipher.c2s", "Ciphers");
        sdvsdvsvsevsvsev.put("cipher.s2c", "Ciphers");
        sdvsdvsvsevsvsev.put("mac.c2s", "Macs");
        sdvsdvsvsevsvsev.put("mac.s2c", "Macs");
        sdvsdvsvsevsvsev.put("compression.s2c", "Compression");
        sdvsdvsvsevsvsev.put("compression.c2s", "Compression");
        sdvsdvsvsevsvsev.put("compression_level", "CompressionLevel");
        sdvsdvsvsevsvsev.put("MaxAuthTries", "NumberOfPasswordPrompts");
    }

    static Hashtable fsafsdfsfsdfsfsdfsd() {
        return sdvsdvsvsevsvsev;
    }

    static Hashtable fsafsdfsfsdfsfsdfsd(OpenSSHConfig openSSHConfig) {
        return openSSHConfig.zxcvxvsdvsvsvs;
    }

    static Vector sdfsdfsdfsdf(OpenSSHConfig openSSHConfig) {
        return openSSHConfig.serfwefewfwefewef;
    }

    public ConfigRepository.Config fsafsdfsfsdfsfsdfsd(String str) {
        return new MyConfig(this, str);
    }
}
