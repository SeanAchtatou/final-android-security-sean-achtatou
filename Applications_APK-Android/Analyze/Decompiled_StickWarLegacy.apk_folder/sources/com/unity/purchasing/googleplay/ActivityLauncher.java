package com.unity.purchasing.googleplay;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;

public class ActivityLauncher implements IActivityLauncher {
    public void startIntentSenderForResult(Activity activity, PendingIntent pendingIntent, int i, Intent intent, String str) throws IntentSender.SendIntentException {
        Integer num = 0;
        activity.startIntentSenderForResult(pendingIntent.getIntentSender(), i, new Intent(), num.intValue(), num.intValue(), num.intValue());
    }

    public void startActivity(Context context, Intent intent) {
        context.startActivity(intent);
    }
}
