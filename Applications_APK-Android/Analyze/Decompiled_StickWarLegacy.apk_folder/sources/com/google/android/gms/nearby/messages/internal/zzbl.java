package com.google.android.gms.nearby.messages.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.PublishOptions;

final class zzbl extends zzbv {
    private final /* synthetic */ Message zzil;
    private final /* synthetic */ zzbt zzim;
    private final /* synthetic */ PublishOptions zzin;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbl(zzbi zzbi, GoogleApiClient googleApiClient, Message message, zzbt zzbt, PublishOptions publishOptions) {
        super(googleApiClient);
        this.zzil = message;
        this.zzim = zzbt;
        this.zzin = publishOptions;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzah) anyClient).zza(zzah(), zzaf.zza(this.zzil), this.zzim, this.zzin);
    }
}
