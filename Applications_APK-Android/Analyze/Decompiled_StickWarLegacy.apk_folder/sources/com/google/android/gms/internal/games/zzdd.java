package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzdd extends zzdm {
    private final /* synthetic */ String zzdl;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdd(zzcz zzcz, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient, null);
        this.zzdl = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzd(this, this.zzdl);
    }
}
