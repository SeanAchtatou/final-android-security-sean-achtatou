package com.google.android.gms.internal.nearby;

import android.util.Log;
import com.google.android.gms.nearby.connection.Connections;
import com.google.android.gms.nearby.connection.Payload;

final class zzas extends zzau<Connections.MessageListener> {
    private final /* synthetic */ zzev zzbu;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzas(zzar zzar, zzev zzev) {
        super();
        this.zzbu = zzev;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        Connections.MessageListener messageListener = (Connections.MessageListener) obj;
        Payload zza = zzfl.zza(this.zzbu.zzl());
        if (zza == null) {
            Log.w("NearbyConnectionsClient", String.format("Failed to convert incoming ParcelablePayload %d to Payload.", Long.valueOf(this.zzbu.zzl().getId())));
        } else if (zza.getType() == 1) {
            messageListener.onMessageReceived(this.zzbu.zzg(), zza.asBytes(), this.zzbu.zzm());
        }
    }
}
