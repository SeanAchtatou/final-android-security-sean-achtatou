package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zzad implements BaseImplementation.ResultHolder<Status> {
    private final /* synthetic */ BaseGmsClient.SignOutCallbacks zzhj;

    zzad(BaseGmsClient.SignOutCallbacks signOutCallbacks) {
        this.zzhj = signOutCallbacks;
    }

    public final void setFailedResult(Status status) {
        this.zzhj.onSignOutComplete();
    }

    public final /* synthetic */ void setResult(Object obj) {
        this.zzhj.onSignOutComplete();
    }
}
