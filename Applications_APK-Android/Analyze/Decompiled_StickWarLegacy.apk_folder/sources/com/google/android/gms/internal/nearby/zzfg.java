package com.google.android.gms.internal.nearby;

import android.util.Log;
import com.google.android.gms.common.util.IOUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

final class zzfg implements Runnable {
    private final /* synthetic */ long zzcx;
    private final /* synthetic */ InputStream zzdr;
    private final /* synthetic */ OutputStream zzds;
    private final /* synthetic */ OutputStream zzdt;
    private final /* synthetic */ zzff zzdu;

    zzfg(zzff zzff, InputStream inputStream, OutputStream outputStream, long j, OutputStream outputStream2) {
        this.zzdu = zzff;
        this.zzdr = inputStream;
        this.zzds = outputStream;
        this.zzcx = j;
        this.zzdt = outputStream2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.nearby.zzff.zza(com.google.android.gms.internal.nearby.zzff, java.io.OutputStream, boolean, long):void
     arg types: [com.google.android.gms.internal.nearby.zzff, java.io.OutputStream, int, long]
     candidates:
      com.google.android.gms.internal.nearby.zzff.zza(java.io.InputStream, java.io.OutputStream, java.io.OutputStream, long):void
      com.google.android.gms.internal.nearby.zzff.zza(com.google.android.gms.internal.nearby.zzff, java.io.OutputStream, boolean, long):void */
    public final void run() {
        InputStream unused = this.zzdu.zzdp = this.zzdr;
        boolean z = true;
        try {
            IOUtils.copyStream(this.zzdr, this.zzds, false, 65536);
            IOUtils.closeQuietly(this.zzdr);
            zzff.zza(this.zzdt, false, this.zzcx);
        } catch (IOException e) {
            if (!this.zzdu.zzdq) {
                Log.w("NearbyConnections", String.format("Exception copying stream for Payload %d", Long.valueOf(this.zzcx)), e);
            } else {
                Log.d("NearbyConnections", String.format("Terminating copying stream for Payload %d due to shutdown of OutgoingPayloadStreamer.", Long.valueOf(this.zzcx)));
            }
            IOUtils.closeQuietly(this.zzdr);
            zzff.zza(this.zzdt, true, this.zzcx);
        } catch (Throwable th) {
            th = th;
            IOUtils.closeQuietly(this.zzdr);
            zzff.zza(this.zzdt, z, this.zzcx);
            IOUtils.closeQuietly(this.zzds);
            InputStream unused2 = this.zzdu.zzdp = null;
            throw th;
        }
        IOUtils.closeQuietly(this.zzds);
        InputStream unused3 = this.zzdu.zzdp = null;
    }
}
