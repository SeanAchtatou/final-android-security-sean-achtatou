package com.google.android.gms.nearby.connection;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@SafeParcelable.Class(creator = "DiscoveryOptionsCreator")
@SafeParcelable.Reserved({1000})
public final class DiscoveryOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<DiscoveryOptions> CREATOR = new zzg();
    /* access modifiers changed from: private */
    @SafeParcelable.Field(getter = "getStrategy", id = 1)
    public Strategy zzh;
    /* access modifiers changed from: private */
    @SafeParcelable.Field(defaultValue = "true", getter = "getEnableBluetooth", id = 3)
    public boolean zzk;
    /* access modifiers changed from: private */
    @SafeParcelable.Field(defaultValue = "true", getter = "getEnableBle", id = 4)
    public boolean zzl;
    /* access modifiers changed from: private */
    @SafeParcelable.Field(defaultValue = "false", getter = "getForwardUnrecognizedBluetoothDevices", id = 2)
    public boolean zzw;

    public static final class Builder {
        private final DiscoveryOptions zzx = new DiscoveryOptions();

        public Builder() {
        }

        public Builder(DiscoveryOptions discoveryOptions) {
            Strategy unused = this.zzx.zzh = discoveryOptions.zzh;
            boolean unused2 = this.zzx.zzw = discoveryOptions.zzw;
            boolean unused3 = this.zzx.zzk = discoveryOptions.zzk;
            boolean unused4 = this.zzx.zzl = discoveryOptions.zzl;
        }

        public final DiscoveryOptions build() {
            return this.zzx;
        }

        public final Builder setStrategy(Strategy strategy) {
            Strategy unused = this.zzx.zzh = strategy;
            return this;
        }
    }

    private DiscoveryOptions() {
        this.zzw = false;
        this.zzk = true;
        this.zzl = true;
    }

    @Deprecated
    public DiscoveryOptions(Strategy strategy) {
        this.zzw = false;
        this.zzk = true;
        this.zzl = true;
        this.zzh = strategy;
    }

    @SafeParcelable.Constructor
    DiscoveryOptions(@SafeParcelable.Param(id = 1) Strategy strategy, @SafeParcelable.Param(id = 2) boolean z, @SafeParcelable.Param(id = 3) boolean z2, @SafeParcelable.Param(id = 4) boolean z3) {
        this.zzw = false;
        this.zzk = true;
        this.zzl = true;
        this.zzh = strategy;
        this.zzw = z;
        this.zzk = z2;
        this.zzl = z3;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof DiscoveryOptions) {
            DiscoveryOptions discoveryOptions = (DiscoveryOptions) obj;
            return Objects.equal(this.zzh, discoveryOptions.zzh) && Objects.equal(Boolean.valueOf(this.zzw), Boolean.valueOf(discoveryOptions.zzw)) && Objects.equal(Boolean.valueOf(this.zzk), Boolean.valueOf(discoveryOptions.zzk)) && Objects.equal(Boolean.valueOf(this.zzl), Boolean.valueOf(discoveryOptions.zzl));
        }
    }

    public final Strategy getStrategy() {
        return this.zzh;
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzh, Boolean.valueOf(this.zzw), Boolean.valueOf(this.zzk), Boolean.valueOf(this.zzl));
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, getStrategy(), i, false);
        SafeParcelWriter.writeBoolean(parcel, 2, this.zzw);
        SafeParcelWriter.writeBoolean(parcel, 3, this.zzk);
        SafeParcelWriter.writeBoolean(parcel, 4, this.zzl);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
