package com.google.android.gms.internal.nearby;

import com.google.android.gms.nearby.connection.Connections;

final class zzat extends zzau<Connections.MessageListener> {
    private final /* synthetic */ zzep zzbk;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzat(zzar zzar, zzep zzep) {
        super();
        this.zzbk = zzep;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((Connections.MessageListener) obj).onDisconnected(this.zzbk.zzg());
    }
}
