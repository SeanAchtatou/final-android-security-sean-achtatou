package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.internal.zze;

final class zzg extends zze.zzat<Status> {
    zzg(BaseImplementation.ResultHolder resultHolder) {
        super(resultHolder);
    }

    public final void onSignOutComplete() {
        setResult(GamesStatusCodes.zza(0));
    }
}
