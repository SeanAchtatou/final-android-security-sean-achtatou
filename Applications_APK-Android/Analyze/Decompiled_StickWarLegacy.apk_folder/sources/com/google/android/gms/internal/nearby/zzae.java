package com.google.android.gms.internal.nearby;

import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;

final class zzae extends zzau<ConnectionLifecycleCallback> {
    private final /* synthetic */ String zzbm;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzae(zzz zzz, String str) {
        super();
        this.zzbm = str;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((ConnectionLifecycleCallback) obj).onConnectionResult(this.zzbm, new ConnectionResolution(zzx.zza(13)));
    }
}
