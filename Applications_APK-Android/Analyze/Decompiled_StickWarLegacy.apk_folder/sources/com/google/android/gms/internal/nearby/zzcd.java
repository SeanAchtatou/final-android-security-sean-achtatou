package com.google.android.gms.internal.nearby;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.nearby.connection.Payload;
import java.util.List;

final class zzcd extends zzcy {
    private final /* synthetic */ Payload zzbx;
    private final /* synthetic */ List zzcw;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcd(zzca zzca, GoogleApiClient googleApiClient, List list, Payload payload) {
        super(googleApiClient, null);
        this.zzcw = list;
        this.zzbx = payload;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.nearby.zzx.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.common.api.Status>, java.lang.String[], com.google.android.gms.nearby.connection.Payload, boolean):void
     arg types: [com.google.android.gms.internal.nearby.zzcd, java.lang.String[], com.google.android.gms.nearby.connection.Payload, int]
     candidates:
      com.google.android.gms.internal.nearby.zzx.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.common.api.Status>, java.lang.String, com.google.android.gms.common.api.internal.ListenerHolder<com.google.android.gms.nearby.connection.EndpointDiscoveryCallback>, com.google.android.gms.nearby.connection.DiscoveryOptions):void
      com.google.android.gms.internal.nearby.zzx.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.common.api.Status>, java.lang.String, java.lang.String, com.google.android.gms.common.api.internal.ListenerHolder<com.google.android.gms.nearby.connection.ConnectionLifecycleCallback>):void
      com.google.android.gms.common.internal.BaseGmsClient.zza(com.google.android.gms.common.internal.BaseGmsClient, int, int, android.os.IInterface):boolean
      com.google.android.gms.internal.nearby.zzx.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.common.api.Status>, java.lang.String[], com.google.android.gms.nearby.connection.Payload, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzx) anyClient).zza((BaseImplementation.ResultHolder<Status>) this, (String[]) this.zzcw.toArray(new String[0]), this.zzbx, false);
    }
}
