package com.google.android.gms.internal.nearby;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@SafeParcelable.Class(creator = "OnBandwidthChangedParamsCreator")
public final class zzef extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzef> CREATOR = new zzeg();
    @SafeParcelable.Field(getter = "getQuality", id = 2)
    private int quality;
    @SafeParcelable.Field(getter = "getRemoteEndpointId", id = 1)
    private String zzat;

    private zzef() {
    }

    @SafeParcelable.Constructor
    zzef(@SafeParcelable.Param(id = 1) String str, @SafeParcelable.Param(id = 2) int i) {
        this.zzat = str;
        this.quality = i;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof zzef) {
            zzef zzef = (zzef) obj;
            return Objects.equal(this.zzat, zzef.zzat) && Objects.equal(Integer.valueOf(this.quality), Integer.valueOf(zzef.quality));
        }
    }

    public final int getQuality() {
        return this.quality;
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzat, Integer.valueOf(this.quality));
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.zzat, false);
        SafeParcelWriter.writeInt(parcel, 2, this.quality);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    public final String zzg() {
        return this.zzat;
    }
}
