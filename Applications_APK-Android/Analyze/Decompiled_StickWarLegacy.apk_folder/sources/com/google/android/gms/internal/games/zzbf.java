package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.internal.zze;

final class zzbf extends zzbl {
    private final /* synthetic */ boolean zzjz;
    private final /* synthetic */ String zzkj;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbf(zzbd zzbd, GoogleApiClient googleApiClient, String str, boolean z) {
        super(googleApiClient);
        this.zzkj = str;
        this.zzjz = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza((BaseImplementation.ResultHolder<Players.LoadPlayersResult>) this, this.zzkj, this.zzjz);
    }
}
