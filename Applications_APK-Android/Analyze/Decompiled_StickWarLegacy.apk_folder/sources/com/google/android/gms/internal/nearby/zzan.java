package com.google.android.gms.internal.nearby;

import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;

final class zzan extends zzau<EndpointDiscoveryCallback> {
    private final /* synthetic */ String zzbm;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzan(zzak zzak, String str) {
        super();
        this.zzbm = str;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((EndpointDiscoveryCallback) obj).onEndpointLost(this.zzbm);
    }
}
