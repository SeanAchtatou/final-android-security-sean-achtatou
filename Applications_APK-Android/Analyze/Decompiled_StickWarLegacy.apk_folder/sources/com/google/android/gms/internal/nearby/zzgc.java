package com.google.android.gms.internal.nearby;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.nearby.connection.DiscoveryOptions;

@SafeParcelable.Class(creator = "StartDiscoveryParamsCreator")
@SafeParcelable.Reserved({1000})
public final class zzgc extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzgc> CREATOR = new zzgf();
    /* access modifiers changed from: private */
    @SafeParcelable.Field(getter = "getDurationMillis", id = 4)
    public long durationMillis;
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getResultListenerAsBinder", id = 1, type = "android.os.IBinder")
    public zzdz zzar;
    @Nullable
    @SafeParcelable.Field(getter = "getCallbackAsBinder", id = 2, type = "android.os.IBinder")
    private zzdp zzel;
    /* access modifiers changed from: private */
    @SafeParcelable.Field(getter = "getOptions", id = 5)
    public DiscoveryOptions zzem;
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getDiscoveryListenerAsBinder", id = 6, type = "android.os.IBinder")
    public zzdr zzen;
    /* access modifiers changed from: private */
    @SafeParcelable.Field(getter = "getServiceId", id = 3)
    public String zzu;

    private zzgc() {
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [android.os.IInterface] */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor
    /* Code decompiled incorrectly, please refer to instructions dump. */
    zzgc(@android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 1) android.os.IBinder r14, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 2) android.os.IBinder r15, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 3) java.lang.String r16, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 4) long r17, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 5) com.google.android.gms.nearby.connection.DiscoveryOptions r19, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 6) android.os.IBinder r20) {
        /*
            r13 = this;
            r0 = r14
            r1 = r15
            r2 = r20
            r3 = 0
            if (r0 != 0) goto L_0x0009
            r6 = r3
            goto L_0x001e
        L_0x0009:
            java.lang.String r4 = "com.google.android.gms.nearby.internal.connection.IResultListener"
            android.os.IInterface r4 = r14.queryLocalInterface(r4)
            boolean r5 = r4 instanceof com.google.android.gms.internal.nearby.zzdz
            if (r5 == 0) goto L_0x0018
            r0 = r4
            com.google.android.gms.internal.nearby.zzdz r0 = (com.google.android.gms.internal.nearby.zzdz) r0
            r6 = r0
            goto L_0x001e
        L_0x0018:
            com.google.android.gms.internal.nearby.zzeb r4 = new com.google.android.gms.internal.nearby.zzeb
            r4.<init>(r14)
            r6 = r4
        L_0x001e:
            if (r1 != 0) goto L_0x0022
            r7 = r3
            goto L_0x0035
        L_0x0022:
            java.lang.String r0 = "com.google.android.gms.nearby.internal.connection.IDiscoveryCallback"
            android.os.IInterface r0 = r15.queryLocalInterface(r0)
            boolean r4 = r0 instanceof com.google.android.gms.internal.nearby.zzdp
            if (r4 == 0) goto L_0x002f
            com.google.android.gms.internal.nearby.zzdp r0 = (com.google.android.gms.internal.nearby.zzdp) r0
            goto L_0x0034
        L_0x002f:
            com.google.android.gms.internal.nearby.zzdq r0 = new com.google.android.gms.internal.nearby.zzdq
            r0.<init>(r15)
        L_0x0034:
            r7 = r0
        L_0x0035:
            if (r2 != 0) goto L_0x0039
        L_0x0037:
            r12 = r3
            goto L_0x004d
        L_0x0039:
            java.lang.String r0 = "com.google.android.gms.nearby.internal.connection.IDiscoveryListener"
            android.os.IInterface r0 = r2.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.internal.nearby.zzdr
            if (r1 == 0) goto L_0x0047
            r3 = r0
            com.google.android.gms.internal.nearby.zzdr r3 = (com.google.android.gms.internal.nearby.zzdr) r3
            goto L_0x0037
        L_0x0047:
            com.google.android.gms.internal.nearby.zzdt r3 = new com.google.android.gms.internal.nearby.zzdt
            r3.<init>(r2)
            goto L_0x0037
        L_0x004d:
            r5 = r13
            r8 = r16
            r9 = r17
            r11 = r19
            r5.<init>(r6, r7, r8, r9, r11, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.nearby.zzgc.<init>(android.os.IBinder, android.os.IBinder, java.lang.String, long, com.google.android.gms.nearby.connection.DiscoveryOptions, android.os.IBinder):void");
    }

    private zzgc(@Nullable zzdz zzdz, @Nullable zzdp zzdp, String str, long j, DiscoveryOptions discoveryOptions, @Nullable zzdr zzdr) {
        this.zzar = zzdz;
        this.zzel = zzdp;
        this.zzu = str;
        this.durationMillis = j;
        this.zzem = discoveryOptions;
        this.zzen = zzdr;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof zzgc) {
            zzgc zzgc = (zzgc) obj;
            return Objects.equal(this.zzar, zzgc.zzar) && Objects.equal(this.zzel, zzgc.zzel) && Objects.equal(this.zzu, zzgc.zzu) && Objects.equal(Long.valueOf(this.durationMillis), Long.valueOf(zzgc.durationMillis)) && Objects.equal(this.zzem, zzgc.zzem) && Objects.equal(this.zzen, zzgc.zzen);
        }
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzar, this.zzel, this.zzu, Long.valueOf(this.durationMillis), this.zzem, this.zzen);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        zzdz zzdz = this.zzar;
        IBinder iBinder = null;
        SafeParcelWriter.writeIBinder(parcel, 1, zzdz == null ? null : zzdz.asBinder(), false);
        zzdp zzdp = this.zzel;
        SafeParcelWriter.writeIBinder(parcel, 2, zzdp == null ? null : zzdp.asBinder(), false);
        SafeParcelWriter.writeString(parcel, 3, this.zzu, false);
        SafeParcelWriter.writeLong(parcel, 4, this.durationMillis);
        SafeParcelWriter.writeParcelable(parcel, 5, this.zzem, i, false);
        zzdr zzdr = this.zzen;
        if (zzdr != null) {
            iBinder = zzdr.asBinder();
        }
        SafeParcelWriter.writeIBinder(parcel, 6, iBinder, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
