package com.google.android.gms.nearby.messages.internal;

import com.google.android.gms.common.api.internal.ListenerHolder;

final class zzav extends zzbe {
    private final /* synthetic */ ListenerHolder zzhz;
    private final /* synthetic */ zzak zzia;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzav(zzak zzak, ListenerHolder listenerHolder, ListenerHolder listenerHolder2) {
        super(listenerHolder);
        this.zzia = zzak;
        this.zzhz = listenerHolder2;
    }

    public final void onExpired() {
        this.zzia.doUnregisterEventListener(this.zzhz.getListenerKey());
        super.onExpired();
    }
}
