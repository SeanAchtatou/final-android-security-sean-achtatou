package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzbo extends zzbs {
    private final /* synthetic */ String zzkl;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbo(zzbn zzbn, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient, null);
        this.zzkl = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzh(this, this.zzkl);
    }
}
