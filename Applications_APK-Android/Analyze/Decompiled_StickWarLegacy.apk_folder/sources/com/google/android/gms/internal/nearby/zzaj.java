package com.google.android.gms.internal.nearby;

import com.google.android.gms.nearby.connection.Connections;

final class zzaj extends zzau<Connections.ConnectionResponseCallback> {
    private final /* synthetic */ zzel zzbp;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzaj(zzai zzai, zzel zzel) {
        super();
        this.zzbp = zzel;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((Connections.ConnectionResponseCallback) obj).onConnectionResponse(this.zzbp.zzg(), zzx.zza(this.zzbp.getStatusCode()), this.zzbp.zzj());
    }
}
