package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.util.VisibleForTesting;

@SafeParcelable.Class(creator = "RegisterStatusCallbackRequestCreator")
public final class zzcb extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzcb> CREATOR = new zzcc();
    @SafeParcelable.VersionField(id = 1)
    private final int versionCode;
    @Nullable
    @SafeParcelable.Field(id = 5)
    @Deprecated
    private String zzff;
    @SafeParcelable.Field(getter = "getCallbackAsBinder", id = 2, type = "android.os.IBinder")
    private final zzp zzhh;
    @Nullable
    @SafeParcelable.Field(id = 6)
    @Deprecated
    private final ClientAppContext zzhi;
    @SafeParcelable.Field(getter = "getStatusCallbackAsBinder", id = 3, type = "android.os.IBinder")
    private final zzx zziw;
    @SafeParcelable.Field(id = 4)
    public boolean zzix;

    /* JADX WARN: Type inference failed for: r0v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor
    /* Code decompiled incorrectly, please refer to instructions dump. */
    zzcb(@com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 1) int r3, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 2) android.os.IBinder r4, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 3) android.os.IBinder r5, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 4) boolean r6, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 5) java.lang.String r7, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 6) com.google.android.gms.nearby.messages.internal.ClientAppContext r8) {
        /*
            r2 = this;
            r2.<init>()
            r2.versionCode = r3
            r3 = 0
            if (r4 != 0) goto L_0x000a
            r4 = r3
            goto L_0x001e
        L_0x000a:
            java.lang.String r0 = "com.google.android.gms.nearby.messages.internal.INearbyMessagesCallback"
            android.os.IInterface r0 = r4.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.nearby.messages.internal.zzp
            if (r1 == 0) goto L_0x0018
            r4 = r0
            com.google.android.gms.nearby.messages.internal.zzp r4 = (com.google.android.gms.nearby.messages.internal.zzp) r4
            goto L_0x001e
        L_0x0018:
            com.google.android.gms.nearby.messages.internal.zzr r0 = new com.google.android.gms.nearby.messages.internal.zzr
            r0.<init>(r4)
            r4 = r0
        L_0x001e:
            r2.zzhh = r4
            if (r5 != 0) goto L_0x0024
            r4 = r3
            goto L_0x0036
        L_0x0024:
            java.lang.String r4 = "com.google.android.gms.nearby.messages.internal.IStatusCallback"
            android.os.IInterface r4 = r5.queryLocalInterface(r4)
            boolean r0 = r4 instanceof com.google.android.gms.nearby.messages.internal.zzx
            if (r0 == 0) goto L_0x0031
            com.google.android.gms.nearby.messages.internal.zzx r4 = (com.google.android.gms.nearby.messages.internal.zzx) r4
            goto L_0x0036
        L_0x0031:
            com.google.android.gms.nearby.messages.internal.zzz r4 = new com.google.android.gms.nearby.messages.internal.zzz
            r4.<init>(r5)
        L_0x0036:
            r2.zziw = r4
            r2.zzix = r6
            r2.zzff = r7
            r4 = 0
            com.google.android.gms.nearby.messages.internal.ClientAppContext r3 = com.google.android.gms.nearby.messages.internal.ClientAppContext.zza(r8, r3, r7, r4)
            r2.zzhi = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.nearby.messages.internal.zzcb.<init>(int, android.os.IBinder, android.os.IBinder, boolean, java.lang.String, com.google.android.gms.nearby.messages.internal.ClientAppContext):void");
    }

    @VisibleForTesting
    public zzcb(IBinder iBinder, IBinder iBinder2) {
        this(1, iBinder, iBinder2, false, null, null);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.versionCode);
        SafeParcelWriter.writeIBinder(parcel, 2, this.zzhh.asBinder(), false);
        SafeParcelWriter.writeIBinder(parcel, 3, this.zziw.asBinder(), false);
        SafeParcelWriter.writeBoolean(parcel, 4, this.zzix);
        SafeParcelWriter.writeString(parcel, 5, this.zzff, false);
        SafeParcelWriter.writeParcelable(parcel, 6, this.zzhi, i, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
