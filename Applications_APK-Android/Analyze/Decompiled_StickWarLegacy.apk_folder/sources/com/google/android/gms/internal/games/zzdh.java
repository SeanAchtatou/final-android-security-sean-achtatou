package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzdh extends zzdo {
    private final /* synthetic */ String zzey;
    private final /* synthetic */ String zzlb;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdh(zzcz zzcz, GoogleApiClient googleApiClient, String str, String str2) {
        super(googleApiClient, null);
        this.zzey = str;
        this.zzlb = str2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzey, this.zzlb);
    }
}
