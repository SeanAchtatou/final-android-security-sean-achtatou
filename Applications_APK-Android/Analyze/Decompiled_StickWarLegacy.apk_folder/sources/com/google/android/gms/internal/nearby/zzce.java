package com.google.android.gms.internal.nearby;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzce extends zzcy {
    private final /* synthetic */ long zzcx;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzce(zzca zzca, GoogleApiClient googleApiClient, long j) {
        super(googleApiClient, null);
        this.zzcx = j;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzx) anyClient).zza(this, this.zzcx);
    }
}
