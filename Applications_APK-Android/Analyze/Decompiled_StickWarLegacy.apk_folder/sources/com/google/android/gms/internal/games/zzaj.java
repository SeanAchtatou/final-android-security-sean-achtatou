package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.multiplayer.Invitations;

abstract class zzaj extends Games.zza<Invitations.LoadInvitationsResult> {
    private zzaj(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzak(this, status);
    }

    /* synthetic */ zzaj(GoogleApiClient googleApiClient, zzai zzai) {
        this(googleApiClient);
    }
}
