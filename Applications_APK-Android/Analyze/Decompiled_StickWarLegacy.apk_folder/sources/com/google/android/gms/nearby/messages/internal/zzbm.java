package com.google.android.gms.nearby.messages.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.nearby.messages.Message;

final class zzbm extends zzbv {
    private final /* synthetic */ Message zzil;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbm(zzbi zzbi, GoogleApiClient googleApiClient, Message message) {
        super(googleApiClient);
        this.zzil = message;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzah) anyClient).zza(zzah(), zzaf.zza(this.zzil));
    }
}
