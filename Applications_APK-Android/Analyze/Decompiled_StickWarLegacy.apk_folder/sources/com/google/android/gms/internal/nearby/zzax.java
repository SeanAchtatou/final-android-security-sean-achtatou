package com.google.android.gms.internal.nearby;

import com.google.android.gms.nearby.connection.PayloadCallback;

final class zzax extends zzau<PayloadCallback> {
    private final /* synthetic */ zzex zzby;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzax(zzav zzav, zzex zzex) {
        super();
        this.zzby = zzex;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((PayloadCallback) obj).onPayloadTransferUpdate(this.zzby.zzg(), this.zzby.zzn());
    }
}
