package com.google.android.gms.nearby.messages.internal;

import android.os.Parcelable;

public final class zzca implements Parcelable.Creator<zzbz> {
    /* JADX WARN: Type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v5, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r17) {
        /*
            r16 = this;
            r0 = r17
            int r1 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.validateObjectHeader(r17)
            r2 = 0
            r3 = 0
            r6 = r3
            r7 = r6
            r8 = r7
            r9 = r8
            r10 = r9
            r12 = r10
            r14 = r12
            r5 = 0
            r11 = 0
            r13 = 0
            r15 = 0
        L_0x0013:
            int r2 = r17.dataPosition()
            if (r2 >= r1) goto L_0x006e
            int r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readHeader(r17)
            int r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.getFieldId(r2)
            switch(r3) {
                case 1: goto L_0x0069;
                case 2: goto L_0x005f;
                case 3: goto L_0x0055;
                case 4: goto L_0x0050;
                case 5: goto L_0x004b;
                case 6: goto L_0x0046;
                case 7: goto L_0x0041;
                case 8: goto L_0x003c;
                case 9: goto L_0x0037;
                case 10: goto L_0x002d;
                case 11: goto L_0x0028;
                default: goto L_0x0024;
            }
        L_0x0024:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.skipUnknownField(r0, r2)
            goto L_0x0013
        L_0x0028:
            int r15 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readInt(r0, r2)
            goto L_0x0013
        L_0x002d:
            android.os.Parcelable$Creator<com.google.android.gms.nearby.messages.internal.ClientAppContext> r3 = com.google.android.gms.nearby.messages.internal.ClientAppContext.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcelable(r0, r2, r3)
            r14 = r2
            com.google.android.gms.nearby.messages.internal.ClientAppContext r14 = (com.google.android.gms.nearby.messages.internal.ClientAppContext) r14
            goto L_0x0013
        L_0x0037:
            boolean r13 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readBoolean(r0, r2)
            goto L_0x0013
        L_0x003c:
            android.os.IBinder r12 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readIBinder(r0, r2)
            goto L_0x0013
        L_0x0041:
            boolean r11 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readBoolean(r0, r2)
            goto L_0x0013
        L_0x0046:
            java.lang.String r10 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createString(r0, r2)
            goto L_0x0013
        L_0x004b:
            java.lang.String r9 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createString(r0, r2)
            goto L_0x0013
        L_0x0050:
            android.os.IBinder r8 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readIBinder(r0, r2)
            goto L_0x0013
        L_0x0055:
            android.os.Parcelable$Creator<com.google.android.gms.nearby.messages.Strategy> r3 = com.google.android.gms.nearby.messages.Strategy.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcelable(r0, r2, r3)
            r7 = r2
            com.google.android.gms.nearby.messages.Strategy r7 = (com.google.android.gms.nearby.messages.Strategy) r7
            goto L_0x0013
        L_0x005f:
            android.os.Parcelable$Creator<com.google.android.gms.nearby.messages.internal.zzaf> r3 = com.google.android.gms.nearby.messages.internal.zzaf.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcelable(r0, r2, r3)
            r6 = r2
            com.google.android.gms.nearby.messages.internal.zzaf r6 = (com.google.android.gms.nearby.messages.internal.zzaf) r6
            goto L_0x0013
        L_0x0069:
            int r5 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readInt(r0, r2)
            goto L_0x0013
        L_0x006e:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.ensureAtEnd(r0, r1)
            com.google.android.gms.nearby.messages.internal.zzbz r0 = new com.google.android.gms.nearby.messages.internal.zzbz
            r4 = r0
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.nearby.messages.internal.zzca.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbz[i];
    }
}
