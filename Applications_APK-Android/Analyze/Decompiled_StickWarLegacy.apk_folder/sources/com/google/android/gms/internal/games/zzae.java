package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesMetadata;

abstract class zzae extends Games.zza<GamesMetadata.LoadGamesResult> {
    private zzae(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzaf(this, status);
    }

    /* synthetic */ zzae(GoogleApiClient googleApiClient, zzad zzad) {
        this(googleApiClient);
    }
}
