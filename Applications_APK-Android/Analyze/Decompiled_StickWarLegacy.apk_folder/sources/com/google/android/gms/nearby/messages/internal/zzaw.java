package com.google.android.gms.nearby.messages.internal;

import com.google.android.gms.common.api.internal.ListenerHolder;

final class zzaw extends zzbg {
    private final /* synthetic */ ListenerHolder zzco;
    private final /* synthetic */ zzak zzia;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzaw(zzak zzak, ListenerHolder listenerHolder, ListenerHolder listenerHolder2) {
        super(listenerHolder);
        this.zzia = zzak;
        this.zzco = listenerHolder2;
    }

    public final void onExpired() {
        this.zzia.doUnregisterEventListener(this.zzco.getListenerKey());
        super.onExpired();
    }
}
