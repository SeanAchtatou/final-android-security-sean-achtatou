package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.video.Videos;

final class zzr extends zze.zzat<Videos.CaptureAvailableResult> {
    zzr(BaseImplementation.ResultHolder resultHolder) {
        super(resultHolder);
    }

    public final void zza(int i, boolean z) {
        setResult(new zze.zzd(new Status(i), z));
    }
}
