package com.google.android.gms.internal.nearby;

import android.app.Activity;
import android.content.Context;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.DiscoveryOptions;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.tasks.Task;
import java.util.List;

public final class zzbd extends ConnectionsClient {
    private static final Api.AbstractClientBuilder<zzx, Api.ApiOptions.NoOptions> CLIENT_BUILDER = new zzbm();
    private static final Api.ClientKey<zzx> CLIENT_KEY = new Api.ClientKey<>();
    private static final Api<Api.ApiOptions.NoOptions> CONNECTIONS_API = new Api<>("Nearby.CONNECTIONS_API", CLIENT_BUILDER, CLIENT_KEY);
    private final zzk zzcd = zzk.zza();

    public zzbd(Activity activity) {
        super(activity, CONNECTIONS_API, GoogleApi.Settings.DEFAULT_SETTINGS);
    }

    public zzbd(Context context) {
        super(context, CONNECTIONS_API, GoogleApi.Settings.DEFAULT_SETTINGS);
    }

    private final Task<Void> zza(zzbw zzbw) {
        return doWrite(new zzbv(this, zzbw));
    }

    private final Task<Void> zza(zzbz zzbz) {
        return doWrite(new zzbn(this, zzbz));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.nearby.zzk.zza(com.google.android.gms.common.api.GoogleApi, java.lang.String, java.lang.String):com.google.android.gms.common.api.internal.ListenerHolder<java.lang.String>
     arg types: [com.google.android.gms.internal.nearby.zzbd, java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.internal.nearby.zzk.zza(com.google.android.gms.common.api.GoogleApi, java.lang.Object, java.lang.String):com.google.android.gms.common.api.internal.ListenerHolder<T>
      com.google.android.gms.internal.nearby.zzk.zza(com.google.android.gms.common.api.GoogleApi, com.google.android.gms.common.api.internal.RegisterListenerMethod, com.google.android.gms.common.api.internal.UnregisterListenerMethod):com.google.android.gms.tasks.Task<java.lang.Void>
      com.google.android.gms.internal.nearby.zzk.zza(com.google.android.gms.common.api.GoogleApi, java.lang.String, java.lang.String):com.google.android.gms.common.api.internal.ListenerHolder<java.lang.String> */
    /* access modifiers changed from: private */
    public final void zzb(String str) {
        ListenerHolder<String> zza = this.zzcd.zza((GoogleApi) this, str, "connection");
        this.zzcd.zza(this, new zzbt(this, zza), new zzbu(this, zza.getListenerKey()));
    }

    /* access modifiers changed from: private */
    public final void zzc(String str) {
        zzk zzk = this.zzcd;
        zzk.zza(this, zzk.zzb(this, str, "connection"));
    }

    public final Task<Void> acceptConnection(String str, PayloadCallback payloadCallback) {
        return zza(new zzbf(str, registerListener(payloadCallback, PayloadCallback.class.getName())));
    }

    public final Task<Void> cancelPayload(long j) {
        return zza(new zzbj(j));
    }

    public final void disconnectFromEndpoint(String str) {
        zza(new zzbk(str));
        zzc(str);
    }

    public final Task<Void> rejectConnection(String str) {
        return zza(new zzbg(str));
    }

    public final Task<Void> requestConnection(String str, String str2, ConnectionLifecycleCallback connectionLifecycleCallback) {
        ListenerHolder registerListener = registerListener(new zzbx(this, connectionLifecycleCallback), ConnectionLifecycleCallback.class.getName());
        zzb(str2);
        return zza(new zzbe(str, str2, registerListener)).addOnFailureListener(new zzbs(this, str2));
    }

    public final Task<Void> sendPayload(String str, Payload payload) {
        return zza(new zzbh(str, payload));
    }

    public final Task<Void> sendPayload(List<String> list, Payload payload) {
        return zza(new zzbi(list, payload));
    }

    public final Task<Void> startAdvertising(String str, String str2, ConnectionLifecycleCallback connectionLifecycleCallback, AdvertisingOptions advertisingOptions) {
        ListenerHolder registerListener = registerListener(new zzbx(this, connectionLifecycleCallback), ConnectionLifecycleCallback.class.getName());
        ListenerHolder zza = this.zzcd.zza(this, new Object(), "advertising");
        return this.zzcd.zza(this, new zzbo(this, zza, str, str2, registerListener, advertisingOptions), new zzbp(this, zza.getListenerKey()));
    }

    public final Task<Void> startDiscovery(String str, EndpointDiscoveryCallback endpointDiscoveryCallback, DiscoveryOptions discoveryOptions) {
        ListenerHolder zza = this.zzcd.zza(this, endpointDiscoveryCallback, "discovery");
        return this.zzcd.zza(this, new zzbq(this, zza, str, zza, discoveryOptions), new zzbr(this, zza.getListenerKey()));
    }

    public final void stopAdvertising() {
        this.zzcd.zza(this, "advertising");
    }

    public final void stopAllEndpoints() {
        stopAdvertising();
        stopDiscovery();
        zza(zzbl.zzcl);
        this.zzcd.zza(this, "connection");
    }

    public final void stopDiscovery() {
        this.zzcd.zza(this, "discovery");
    }
}
