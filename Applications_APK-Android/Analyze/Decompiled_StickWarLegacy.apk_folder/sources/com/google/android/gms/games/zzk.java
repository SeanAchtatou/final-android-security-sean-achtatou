package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.internal.zze;

final class zzk extends Games.zzc {
    private final /* synthetic */ String zzaq;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzk(GoogleApiClient googleApiClient, String str) {
        super(googleApiClient, null);
        this.zzaq = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this.zzaq, this);
    }
}
