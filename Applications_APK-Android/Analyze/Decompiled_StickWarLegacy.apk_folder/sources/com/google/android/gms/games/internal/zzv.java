package com.google.android.gms.games.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.internal.zze;

final class zzv extends zze.zzv<T> {
    private final /* synthetic */ zze.zzaw zzhe;
    private final /* synthetic */ DataHolder zzhf;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzv(zze.zzaw zzaw, DataHolder dataHolder) {
        super(null);
        this.zzhe = zzaw;
        this.zzhf = dataHolder;
    }

    public final void notifyListener(T t) {
        this.zzhe.zza(t, zze.zzay(this.zzhf));
    }
}
