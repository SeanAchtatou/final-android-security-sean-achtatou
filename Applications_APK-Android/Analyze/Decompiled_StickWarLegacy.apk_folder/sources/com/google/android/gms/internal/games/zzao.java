package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzao extends zzav {
    private final /* synthetic */ String zzbr;
    private final /* synthetic */ int zzke;
    private final /* synthetic */ int zzkf;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzao(zzal zzal, GoogleApiClient googleApiClient, String str, int i, int i2) {
        super(googleApiClient, null);
        this.zzbr = str;
        this.zzke = i;
        this.zzkf = i2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, (String) null, this.zzbr, this.zzke, this.zzkf);
    }
}
