package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.snapshot.Snapshots;

final class zzn extends zze.zzat<Snapshots.DeleteSnapshotResult> {
    zzn(BaseImplementation.ResultHolder resultHolder) {
        super(resultHolder);
    }

    public final void zzd(int i, String str) {
        setResult(new zze.zzi(i, str));
    }
}
