package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.leaderboard.Leaderboards;

final class zzz extends zze.zzat<Leaderboards.LoadPlayerScoreResult> {
    zzz(BaseImplementation.ResultHolder resultHolder) {
        super(resultHolder);
    }

    public final void zzac(DataHolder dataHolder) {
        setResult(new zze.zzac(dataHolder));
    }
}
