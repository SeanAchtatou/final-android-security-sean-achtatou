package com.google.android.gms.internal.nearby;

import com.google.android.gms.nearby.connection.Connections;

final class zzap extends zzau<Connections.EndpointDiscoveryListener> {
    private final /* synthetic */ zzer zzbr;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzap(zzao zzao, zzer zzer) {
        super();
        this.zzbr = zzer;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((Connections.EndpointDiscoveryListener) obj).onEndpointFound(this.zzbr.zze(), this.zzbr.getServiceId(), this.zzbr.getEndpointName());
    }
}
