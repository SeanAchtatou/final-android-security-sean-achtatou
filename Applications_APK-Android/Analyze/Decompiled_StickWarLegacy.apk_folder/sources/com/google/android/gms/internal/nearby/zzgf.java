package com.google.android.gms.internal.nearby;

import android.os.Parcelable;

public final class zzgf implements Parcelable.Creator<zzgc> {
    /* JADX WARN: Type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r13) {
        /*
            r12 = this;
            int r0 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.validateObjectHeader(r13)
            r1 = 0
            r2 = 0
            r5 = r1
            r6 = r5
            r7 = r6
            r10 = r7
            r11 = r10
            r8 = r2
        L_0x000d:
            int r1 = r13.dataPosition()
            if (r1 >= r0) goto L_0x0045
            int r1 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readHeader(r13)
            int r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.getFieldId(r1)
            switch(r2) {
                case 1: goto L_0x0040;
                case 2: goto L_0x003b;
                case 3: goto L_0x0036;
                case 4: goto L_0x0031;
                case 5: goto L_0x0027;
                case 6: goto L_0x0022;
                default: goto L_0x001e;
            }
        L_0x001e:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.skipUnknownField(r13, r1)
            goto L_0x000d
        L_0x0022:
            android.os.IBinder r11 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readIBinder(r13, r1)
            goto L_0x000d
        L_0x0027:
            android.os.Parcelable$Creator<com.google.android.gms.nearby.connection.DiscoveryOptions> r2 = com.google.android.gms.nearby.connection.DiscoveryOptions.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcelable(r13, r1, r2)
            r10 = r1
            com.google.android.gms.nearby.connection.DiscoveryOptions r10 = (com.google.android.gms.nearby.connection.DiscoveryOptions) r10
            goto L_0x000d
        L_0x0031:
            long r8 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readLong(r13, r1)
            goto L_0x000d
        L_0x0036:
            java.lang.String r7 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createString(r13, r1)
            goto L_0x000d
        L_0x003b:
            android.os.IBinder r6 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readIBinder(r13, r1)
            goto L_0x000d
        L_0x0040:
            android.os.IBinder r5 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readIBinder(r13, r1)
            goto L_0x000d
        L_0x0045:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.ensureAtEnd(r13, r0)
            com.google.android.gms.internal.nearby.zzgc r13 = new com.google.android.gms.internal.nearby.zzgc
            r4 = r13
            r4.<init>(r5, r6, r7, r8, r10, r11)
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.nearby.zzgf.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzgc[i];
    }
}
