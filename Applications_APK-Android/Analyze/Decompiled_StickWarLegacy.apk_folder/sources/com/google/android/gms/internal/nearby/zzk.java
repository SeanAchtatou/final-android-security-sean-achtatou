package com.google.android.gms.internal.nearby;

import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.ListenerHolders;
import com.google.android.gms.common.api.internal.RegisterListenerMethod;
import com.google.android.gms.common.api.internal.UnregisterListenerMethod;
import com.google.android.gms.tasks.Task;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class zzk {
    private static zzk zzal;
    private final Map<String, Set<ListenerHolder.ListenerKey<?>>> zzam = new HashMap();
    /* access modifiers changed from: private */
    public final Set<ListenerHolder.ListenerKey<?>> zzan = new HashSet();
    private final Map<String, ListenerHolder<String>> zzao = new HashMap();

    private zzk() {
    }

    public static synchronized zzk zza() {
        zzk zzk;
        synchronized (zzk.class) {
            if (zzal == null) {
                zzal = new zzk();
            }
            zzk = zzal;
        }
        return zzk;
    }

    private final void zza(String str, ListenerHolder.ListenerKey<?> listenerKey) {
        Set set = this.zzam.get(str);
        if (set == null) {
            set = new HashSet();
            this.zzam.put(str, set);
        }
        set.add(listenerKey);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.nearby.zzk.zza(java.lang.String, com.google.android.gms.common.api.internal.ListenerHolder$ListenerKey<?>):void
     arg types: [java.lang.String, com.google.android.gms.common.api.internal.ListenerHolder$ListenerKey<T>]
     candidates:
      com.google.android.gms.internal.nearby.zzk.zza(com.google.android.gms.common.api.GoogleApi, com.google.android.gms.common.api.internal.ListenerHolder$ListenerKey<?>):com.google.android.gms.tasks.Task<java.lang.Boolean>
      com.google.android.gms.internal.nearby.zzk.zza(com.google.android.gms.common.api.GoogleApi, java.lang.String):void
      com.google.android.gms.internal.nearby.zzk.zza(java.lang.String, com.google.android.gms.common.api.internal.ListenerHolder$ListenerKey<?>):void */
    public final synchronized <T> ListenerHolder<T> zza(GoogleApi googleApi, Object obj, String str) {
        ListenerHolder<T> registerListener;
        registerListener = googleApi.registerListener(obj, str);
        zza(str, (ListenerHolder.ListenerKey<?>) registerListener.getListenerKey());
        return registerListener;
    }

    public final synchronized ListenerHolder<String> zza(GoogleApi googleApi, String str, String str2) {
        if (!this.zzao.containsKey(str) || !this.zzao.get(str).hasListener()) {
            ListenerHolder<String> registerListener = googleApi.registerListener(str, str2);
            zza(str2, registerListener.getListenerKey());
            this.zzao.put(str, registerListener);
            return registerListener;
        }
        return this.zzao.get(str);
    }

    public final synchronized Task<Boolean> zza(GoogleApi googleApi, ListenerHolder.ListenerKey<?> listenerKey) {
        this.zzan.remove(listenerKey);
        return googleApi.doUnregisterEventListener(listenerKey);
    }

    public final synchronized Task<Void> zza(GoogleApi googleApi, RegisterListenerMethod registerListenerMethod, UnregisterListenerMethod unregisterListenerMethod) {
        this.zzan.add(registerListenerMethod.getListenerKey());
        return googleApi.doRegisterEventListener(registerListenerMethod, unregisterListenerMethod).addOnFailureListener(new zzl(this, registerListenerMethod));
    }

    public final synchronized void zza(GoogleApi googleApi, String str) {
        Set<ListenerHolder.ListenerKey> set = this.zzam.get(str);
        if (set != null) {
            for (ListenerHolder.ListenerKey listenerKey : set) {
                if (this.zzan.contains(listenerKey)) {
                    zza(googleApi, listenerKey);
                }
            }
            this.zzam.remove(str);
        }
    }

    public final synchronized <T> ListenerHolder.ListenerKey<T> zzb(GoogleApi googleApi, T t, String str) {
        if (t instanceof String) {
            return zza(googleApi, (String) t, str).getListenerKey();
        }
        return ListenerHolders.createListenerKey(t, str);
    }
}
