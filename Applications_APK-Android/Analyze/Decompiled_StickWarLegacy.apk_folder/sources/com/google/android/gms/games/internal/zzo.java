package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.games.video.Videos;

final class zzo extends zze.zzat<Videos.CaptureCapabilitiesResult> {
    zzo(BaseImplementation.ResultHolder resultHolder) {
        super(resultHolder);
    }

    public final void zza(int i, VideoCapabilities videoCapabilities) {
        setResult(new zze.C0001zze(new Status(i), videoCapabilities));
    }
}
