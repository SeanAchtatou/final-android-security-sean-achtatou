package com.google.android.gms.internal.nearby;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.Arrays;

@SafeParcelable.Class(creator = "AcceptConnectionRequestParamsCreator")
@SafeParcelable.Reserved({1000})
public final class zzm extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzm> CREATOR = new zzp();
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getResultListenerAsBinder", id = 1, type = "android.os.IBinder")
    public zzdz zzar;
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getConnectionEventListenerAsBinder", id = 2, type = "android.os.IBinder")
    public zzdg zzas;
    /* access modifiers changed from: private */
    @SafeParcelable.Field(getter = "getRemoteEndpointId", id = 3)
    public String zzat;
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getHandshakeData", id = 4)
    public byte[] zzau;
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getPayloadListenerAsBinder", id = 5, type = "android.os.IBinder")
    public zzdw zzav;

    private zzm() {
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [android.os.IInterface] */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor
    /* Code decompiled incorrectly, please refer to instructions dump. */
    zzm(@android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 1) android.os.IBinder r9, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 2) android.os.IBinder r10, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 3) java.lang.String r11, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 4) byte[] r12, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 5) android.os.IBinder r13) {
        /*
            r8 = this;
            r0 = 0
            if (r9 != 0) goto L_0x0005
            r3 = r0
            goto L_0x001a
        L_0x0005:
            java.lang.String r1 = "com.google.android.gms.nearby.internal.connection.IResultListener"
            android.os.IInterface r1 = r9.queryLocalInterface(r1)
            boolean r2 = r1 instanceof com.google.android.gms.internal.nearby.zzdz
            if (r2 == 0) goto L_0x0014
            r9 = r1
            com.google.android.gms.internal.nearby.zzdz r9 = (com.google.android.gms.internal.nearby.zzdz) r9
            r3 = r9
            goto L_0x001a
        L_0x0014:
            com.google.android.gms.internal.nearby.zzeb r1 = new com.google.android.gms.internal.nearby.zzeb
            r1.<init>(r9)
            r3 = r1
        L_0x001a:
            if (r10 != 0) goto L_0x001e
            r4 = r0
            goto L_0x0031
        L_0x001e:
            java.lang.String r9 = "com.google.android.gms.nearby.internal.connection.IConnectionEventListener"
            android.os.IInterface r9 = r10.queryLocalInterface(r9)
            boolean r1 = r9 instanceof com.google.android.gms.internal.nearby.zzdg
            if (r1 == 0) goto L_0x002b
            com.google.android.gms.internal.nearby.zzdg r9 = (com.google.android.gms.internal.nearby.zzdg) r9
            goto L_0x0030
        L_0x002b:
            com.google.android.gms.internal.nearby.zzdi r9 = new com.google.android.gms.internal.nearby.zzdi
            r9.<init>(r10)
        L_0x0030:
            r4 = r9
        L_0x0031:
            if (r13 != 0) goto L_0x0035
        L_0x0033:
            r7 = r0
            goto L_0x0049
        L_0x0035:
            java.lang.String r9 = "com.google.android.gms.nearby.internal.connection.IPayloadListener"
            android.os.IInterface r9 = r13.queryLocalInterface(r9)
            boolean r10 = r9 instanceof com.google.android.gms.internal.nearby.zzdw
            if (r10 == 0) goto L_0x0043
            r0 = r9
            com.google.android.gms.internal.nearby.zzdw r0 = (com.google.android.gms.internal.nearby.zzdw) r0
            goto L_0x0033
        L_0x0043:
            com.google.android.gms.internal.nearby.zzdy r0 = new com.google.android.gms.internal.nearby.zzdy
            r0.<init>(r13)
            goto L_0x0033
        L_0x0049:
            r2 = r8
            r5 = r11
            r6 = r12
            r2.<init>(r3, r4, r5, r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.nearby.zzm.<init>(android.os.IBinder, android.os.IBinder, java.lang.String, byte[], android.os.IBinder):void");
    }

    private zzm(@Nullable zzdz zzdz, @Nullable zzdg zzdg, String str, @Nullable byte[] bArr, @Nullable zzdw zzdw) {
        this.zzar = zzdz;
        this.zzas = zzdg;
        this.zzat = str;
        this.zzau = bArr;
        this.zzav = zzdw;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof zzm) {
            zzm zzm = (zzm) obj;
            return Objects.equal(this.zzar, zzm.zzar) && Objects.equal(this.zzas, zzm.zzas) && Objects.equal(this.zzat, zzm.zzat) && Arrays.equals(this.zzau, zzm.zzau) && Objects.equal(this.zzav, zzm.zzav);
        }
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzar, this.zzas, this.zzat, Integer.valueOf(Arrays.hashCode(this.zzau)), this.zzav);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        zzdz zzdz = this.zzar;
        IBinder iBinder = null;
        SafeParcelWriter.writeIBinder(parcel, 1, zzdz == null ? null : zzdz.asBinder(), false);
        zzdg zzdg = this.zzas;
        SafeParcelWriter.writeIBinder(parcel, 2, zzdg == null ? null : zzdg.asBinder(), false);
        SafeParcelWriter.writeString(parcel, 3, this.zzat, false);
        SafeParcelWriter.writeByteArray(parcel, 4, this.zzau, false);
        zzdw zzdw = this.zzav;
        if (zzdw != null) {
            iBinder = zzdw.asBinder();
        }
        SafeParcelWriter.writeIBinder(parcel, 5, iBinder, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
