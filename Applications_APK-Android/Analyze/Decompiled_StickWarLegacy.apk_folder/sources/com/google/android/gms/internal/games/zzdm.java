package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

abstract class zzdm extends Games.zza<TurnBasedMultiplayer.InitiateMatchResult> {
    private zzdm(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzdn(this, status);
    }

    /* synthetic */ zzdm(GoogleApiClient googleApiClient, zzda zzda) {
        this(googleApiClient);
    }
}
