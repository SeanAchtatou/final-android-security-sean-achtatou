package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;

final class zzck extends zzcn {
    private final /* synthetic */ Snapshot zzeh;
    private final /* synthetic */ SnapshotMetadataChange zzku;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzck(zzch zzch, GoogleApiClient googleApiClient, Snapshot snapshot, SnapshotMetadataChange snapshotMetadataChange) {
        super(googleApiClient, null);
        this.zzeh = snapshot;
        this.zzku = snapshotMetadataChange;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzeh, this.zzku);
    }
}
