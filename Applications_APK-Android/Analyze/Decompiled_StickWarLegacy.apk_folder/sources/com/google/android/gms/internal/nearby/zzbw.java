package com.google.android.gms.internal.nearby;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;

interface zzbw {
    void zza(zzx zzx, BaseImplementation.ResultHolder<Status> resultHolder) throws RemoteException;
}
