package com.google.android.gms.nearby.messages.internal;

import android.app.PendingIntent;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.nearby.messages.SubscribeOptions;

final class zzbo extends zzbv {
    private final /* synthetic */ zzbw zzio;
    private final /* synthetic */ SubscribeOptions zzip;
    private final /* synthetic */ PendingIntent zziq;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbo(zzbi zzbi, GoogleApiClient googleApiClient, PendingIntent pendingIntent, zzbw zzbw, SubscribeOptions subscribeOptions) {
        super(googleApiClient);
        this.zziq = pendingIntent;
        this.zzio = zzbw;
        this.zzip = subscribeOptions;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzah) anyClient).zza(zzah(), this.zziq, this.zzio, this.zzip);
    }
}
