package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzdz extends zzea {
    private final /* synthetic */ int zzlg;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdz(zzdw zzdw, GoogleApiClient googleApiClient, int i) {
        super(googleApiClient, null);
        this.zzlg = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzb(this, this.zzlg);
    }
}
