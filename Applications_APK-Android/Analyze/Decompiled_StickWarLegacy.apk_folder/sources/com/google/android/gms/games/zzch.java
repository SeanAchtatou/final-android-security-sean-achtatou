package com.google.android.gms.games;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzch implements PendingResultUtil.ResultConverter<TurnBasedMultiplayer.CancelMatchResult, String> {
    zzch() {
    }

    public final /* synthetic */ Object convert(Result result) {
        TurnBasedMultiplayer.CancelMatchResult cancelMatchResult = (TurnBasedMultiplayer.CancelMatchResult) result;
        if (cancelMatchResult == null) {
            return null;
        }
        return cancelMatchResult.getMatchId();
    }
}
