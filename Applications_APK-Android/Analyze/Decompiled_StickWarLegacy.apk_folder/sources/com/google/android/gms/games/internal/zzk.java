package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.quest.Quests;

final class zzk extends zze.zzat<Quests.AcceptQuestResult> {
    zzk(BaseImplementation.ResultHolder resultHolder) {
        super(resultHolder);
    }

    public final void zzaj(DataHolder dataHolder) {
        setResult(new zze.zza(dataHolder));
    }
}
