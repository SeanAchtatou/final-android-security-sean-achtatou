package com.google.android.gms.internal.nearby;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.nearby.connection.Payload;

final /* synthetic */ class zzbh implements zzbw {
    private final String zzce;
    private final Payload zzci;

    zzbh(String str, Payload payload) {
        this.zzce = str;
        this.zzci = payload;
    }

    public final void zza(zzx zzx, BaseImplementation.ResultHolder resultHolder) {
        zzx.zza((BaseImplementation.ResultHolder<Status>) resultHolder, new String[]{this.zzce}, this.zzci, false);
    }
}
