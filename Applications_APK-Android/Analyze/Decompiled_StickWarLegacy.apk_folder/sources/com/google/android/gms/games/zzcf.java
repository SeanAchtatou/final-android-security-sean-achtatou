package com.google.android.gms.games;

import android.support.annotation.NonNull;
import com.google.android.gms.games.internal.zzbm;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzcf implements zzbm<TurnBasedMultiplayer.LoadMatchesResult> {
    zzcf() {
    }

    public final /* synthetic */ void release(@NonNull Object obj) {
        TurnBasedMultiplayer.LoadMatchesResult loadMatchesResult = (TurnBasedMultiplayer.LoadMatchesResult) obj;
        if (loadMatchesResult.getMatches() != null) {
            loadMatchesResult.getMatches().release();
        }
    }
}
