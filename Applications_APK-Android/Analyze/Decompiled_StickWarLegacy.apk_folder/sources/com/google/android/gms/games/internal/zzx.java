package com.google.android.gms.games.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.internal.zze;

final class zzx extends zze.zzv<T> {
    private final /* synthetic */ DataHolder zzhf;
    private final /* synthetic */ zze.zzaz zzhi;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzx(zze.zzaz zzaz, DataHolder dataHolder) {
        super(null);
        this.zzhi = zzaz;
        this.zzhf = dataHolder;
    }

    public final void notifyListener(T t) {
        this.zzhi.zza(t, this.zzhf.getStatusCode(), zze.zzay(this.zzhf));
    }
}
