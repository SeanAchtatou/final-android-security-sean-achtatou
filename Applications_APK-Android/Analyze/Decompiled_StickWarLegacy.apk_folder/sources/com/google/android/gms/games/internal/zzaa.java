package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.leaderboard.Leaderboards;

final class zzaa extends zze.zzat<Leaderboards.SubmitScoreResult> {
    zzaa(BaseImplementation.ResultHolder resultHolder) {
        super(resultHolder);
    }

    public final void zzd(DataHolder dataHolder) {
        setResult(new zze.zzba(dataHolder));
    }
}
