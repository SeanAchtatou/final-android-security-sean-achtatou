package com.google.android.gms.games;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.games.Players;

final class zzaz implements PendingResultUtil.ResultConverter<Players.LoadPlayersResult, Player> {
    zzaz() {
    }

    private static Player zza(@Nullable Players.LoadPlayersResult loadPlayersResult) {
        if (loadPlayersResult == null) {
            return null;
        }
        PlayerBuffer players = loadPlayersResult.getPlayers();
        if (players != null) {
            try {
                if (players.getCount() > 0) {
                    return (Player) ((Player) players.get(0)).freeze();
                }
            } finally {
                if (players != null) {
                    players.release();
                }
            }
        }
        if (players != null) {
            players.release();
        }
        return null;
    }

    public final /* synthetic */ Object convert(@Nullable Result result) {
        return zza((Players.LoadPlayersResult) result);
    }
}
