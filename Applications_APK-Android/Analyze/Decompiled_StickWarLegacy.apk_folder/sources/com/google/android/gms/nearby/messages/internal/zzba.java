package com.google.android.gms.nearby.messages.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.UnregisterListenerMethod;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzba extends UnregisterListenerMethod<zzah, T> {
    private final /* synthetic */ zzak zzia;
    private final /* synthetic */ zzbd zzie;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzba(zzak zzak, ListenerHolder.ListenerKey listenerKey, zzbd zzbd) {
        super(listenerKey);
        this.zzia = zzak;
        this.zzie = zzbd;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void unregisterListener(Api.AnyClient anyClient, TaskCompletionSource taskCompletionSource) throws RemoteException {
        this.zzie.zza((zzah) anyClient, this.zzia.zza(taskCompletionSource));
    }
}
