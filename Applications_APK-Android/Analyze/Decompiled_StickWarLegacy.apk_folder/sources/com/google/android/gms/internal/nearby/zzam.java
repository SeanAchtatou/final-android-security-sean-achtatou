package com.google.android.gms.internal.nearby;

import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;

final class zzam extends zzau<EndpointDiscoveryCallback> {
    private final /* synthetic */ zzet zzbs;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzam(zzak zzak, zzet zzet) {
        super();
        this.zzbs = zzet;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((EndpointDiscoveryCallback) obj).onEndpointLost(this.zzbs.zze());
    }
}
