package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzcc extends zzcd {
    private final /* synthetic */ int zzkd;
    private final /* synthetic */ int zzkp;
    private final /* synthetic */ int zzkq;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcc(zzbz zzbz, GoogleApiClient googleApiClient, int i, int i2, int i3) {
        super(googleApiClient, null);
        this.zzkp = i;
        this.zzkq = i2;
        this.zzkd = i3;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzkp, this.zzkq, this.zzkd);
    }
}
