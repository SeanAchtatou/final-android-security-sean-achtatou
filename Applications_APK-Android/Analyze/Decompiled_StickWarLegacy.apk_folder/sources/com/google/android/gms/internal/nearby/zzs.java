package com.google.android.gms.internal.nearby;

public final class zzs {
    private final zzq zzax = new zzq();

    public final zzs zza(long j) {
        long unused = this.zzax.zzaf = j;
        return this;
    }

    public final zzs zzb(zzdz zzdz) {
        zzdz unused = this.zzax.zzar = zzdz;
        return this;
    }

    public final zzq zzc() {
        return this.zzax;
    }
}
