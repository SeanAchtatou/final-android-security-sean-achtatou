package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.apps.common.proguard.UsedByReflection;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.games.internal.zzd;
import java.util.ArrayList;
import java.util.List;

@UsedByReflection("GamesClientImpl.java")
@SafeParcelable.Class(creator = "AppContentSectionEntityCreator")
@SafeParcelable.Reserved({1000})
public final class AppContentSectionEntity extends zzd implements zzi {
    public static final Parcelable.Creator<AppContentSectionEntity> CREATOR = new zzj();
    @SafeParcelable.Field(getter = "getExtras", id = 5)
    private final Bundle extras;
    @SafeParcelable.Field(getter = "getType", id = 8)
    private final String type;
    @SafeParcelable.Field(getter = "getTitle", id = 7)
    private final String zzcd;
    @SafeParcelable.Field(getter = "getContentDescription", id = 4)
    private final String zzfq;
    @SafeParcelable.Field(getter = "getId", id = 9)
    private final String zzfr;
    @SafeParcelable.Field(getter = "getActions", id = 1)
    private final ArrayList<AppContentActionEntity> zzga;
    @SafeParcelable.Field(getter = "getAnnotations", id = 14)
    private final ArrayList<AppContentAnnotationEntity> zzgb;
    @SafeParcelable.Field(getter = "getSubtitle", id = 6)
    private final String zzgd;
    @SafeParcelable.Field(getter = "getCards", id = 3)
    private final ArrayList<AppContentCardEntity> zzgj;
    @SafeParcelable.Field(getter = "getCardType", id = 10)
    private final String zzgk;

    @SafeParcelable.Constructor
    AppContentSectionEntity(@SafeParcelable.Param(id = 1) ArrayList<AppContentActionEntity> arrayList, @SafeParcelable.Param(id = 3) ArrayList<AppContentCardEntity> arrayList2, @SafeParcelable.Param(id = 4) String str, @SafeParcelable.Param(id = 5) Bundle bundle, @SafeParcelable.Param(id = 6) String str2, @SafeParcelable.Param(id = 7) String str3, @SafeParcelable.Param(id = 8) String str4, @SafeParcelable.Param(id = 9) String str5, @SafeParcelable.Param(id = 10) String str6, @SafeParcelable.Param(id = 14) ArrayList<AppContentAnnotationEntity> arrayList3) {
        this.zzga = arrayList;
        this.zzgb = arrayList3;
        this.zzgj = arrayList2;
        this.zzgk = str6;
        this.zzfq = str;
        this.extras = bundle;
        this.zzfr = str5;
        this.zzgd = str2;
        this.zzcd = str3;
        this.type = str4;
    }

    public final /* bridge */ /* synthetic */ Object freeze() {
        return this;
    }

    public final boolean isDataValid() {
        return true;
    }

    public final List<zza> getActions() {
        return new ArrayList(this.zzga);
    }

    public final List<zzc> zzai() {
        return new ArrayList(this.zzgb);
    }

    public final List<zze> zzaq() {
        return new ArrayList(this.zzgj);
    }

    public final String zzar() {
        return this.zzgk;
    }

    public final String zzaa() {
        return this.zzfq;
    }

    public final Bundle getExtras() {
        return this.extras;
    }

    public final String zzak() {
        return this.zzgd;
    }

    public final String getId() {
        return this.zzfr;
    }

    public final String getTitle() {
        return this.zzcd;
    }

    public final String getType() {
        return this.type;
    }

    public final int hashCode() {
        return Objects.hashCode(getActions(), zzai(), zzaq(), zzar(), zzaa(), Integer.valueOf(zzc.zza(getExtras())), getId(), zzak(), getTitle(), getType());
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzi)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        zzi zzi = (zzi) obj;
        if (!Objects.equal(zzi.getActions(), getActions()) || !Objects.equal(zzi.zzai(), zzai()) || !Objects.equal(zzi.zzaq(), zzaq()) || !Objects.equal(zzi.zzar(), zzar()) || !Objects.equal(zzi.zzaa(), zzaa()) || !zzc.zza(zzi.getExtras(), getExtras()) || !Objects.equal(zzi.getId(), getId()) || !Objects.equal(zzi.zzak(), zzak()) || !Objects.equal(zzi.getTitle(), getTitle()) || !Objects.equal(zzi.getType(), getType())) {
            return false;
        }
        return true;
    }

    public final String toString() {
        return Objects.toStringHelper(this).add("Actions", getActions()).add("Annotations", zzai()).add("Cards", zzaq()).add("CardType", zzar()).add("ContentDescription", zzaa()).add("Extras", getExtras()).add("Id", getId()).add("Subtitle", zzak()).add("Title", getTitle()).add("Type", getType()).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeTypedList(parcel, 1, getActions(), false);
        SafeParcelWriter.writeTypedList(parcel, 3, zzaq(), false);
        SafeParcelWriter.writeString(parcel, 4, this.zzfq, false);
        SafeParcelWriter.writeBundle(parcel, 5, this.extras, false);
        SafeParcelWriter.writeString(parcel, 6, this.zzgd, false);
        SafeParcelWriter.writeString(parcel, 7, this.zzcd, false);
        SafeParcelWriter.writeString(parcel, 8, this.type, false);
        SafeParcelWriter.writeString(parcel, 9, this.zzfr, false);
        SafeParcelWriter.writeString(parcel, 10, this.zzgk, false);
        SafeParcelWriter.writeTypedList(parcel, 14, zzai(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
