package com.google.android.gms.games.internal;

import com.google.android.gms.games.internal.zze;

final class zzu extends zze.zzv<T> {
    private final /* synthetic */ zze.zzap zzhd;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzu(zze.zzap zzap) {
        super(null);
        this.zzhd = zzap;
    }

    public final void notifyListener(T t) {
        this.zzhd.accept(t);
    }
}
