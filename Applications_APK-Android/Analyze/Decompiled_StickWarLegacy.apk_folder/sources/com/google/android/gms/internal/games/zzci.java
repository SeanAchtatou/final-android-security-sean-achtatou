package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzci extends zzcr {
    private final /* synthetic */ boolean zzjz;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzci(zzch zzch, GoogleApiClient googleApiClient, boolean z) {
        super(googleApiClient, null);
        this.zzjz = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzf(this, this.zzjz);
    }
}
