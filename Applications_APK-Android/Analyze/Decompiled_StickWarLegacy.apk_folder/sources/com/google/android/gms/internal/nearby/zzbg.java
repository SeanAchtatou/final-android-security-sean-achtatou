package com.google.android.gms.internal.nearby;

import com.google.android.gms.common.api.internal.BaseImplementation;

final /* synthetic */ class zzbg implements zzbw {
    private final String zzce;

    zzbg(String str) {
        this.zzce = str;
    }

    public final void zza(zzx zzx, BaseImplementation.ResultHolder resultHolder) {
        zzx.zza(resultHolder, this.zzce);
    }
}
