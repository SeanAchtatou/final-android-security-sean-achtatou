package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.internal.zze;

final class zzbe extends zzbl {
    private final /* synthetic */ String zzkj;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbe(zzbd zzbd, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient);
        this.zzkj = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.Players$LoadPlayersResult>, java.lang.String, boolean):void
     arg types: [com.google.android.gms.internal.games.zzbe, java.lang.String, int]
     candidates:
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.data.DataHolder, java.lang.String[], com.google.android.gms.games.internal.zze$zzav):com.google.android.gms.common.api.internal.ListenerHolder$Notifier<T>
      com.google.android.gms.games.internal.zze.zza(byte[], java.lang.String, java.lang.String[]):int
      com.google.android.gms.games.internal.zze.zza(int, int, boolean):android.content.Intent
      com.google.android.gms.games.internal.zze.zza(java.lang.String, int, int):android.content.Intent
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer$LoadMatchesResult>, int, int[]):void
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.snapshot.Snapshots$CommitSnapshotResult>, com.google.android.gms.games.snapshot.Snapshot, com.google.android.gms.games.snapshot.SnapshotMetadataChange):void
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.achievement.Achievements$UpdateAchievementResult>, java.lang.String, int):void
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer$LeaveMatchResult>, java.lang.String, java.lang.String):void
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.event.Events$LoadEventsResult>, boolean, java.lang.String[]):void
      com.google.android.gms.common.internal.BaseGmsClient.zza(com.google.android.gms.common.internal.BaseGmsClient, int, android.os.IInterface):void
      com.google.android.gms.common.internal.BaseGmsClient.zza(int, int, android.os.IInterface):boolean
      com.google.android.gms.common.internal.BaseGmsClient.zza(int, android.os.Bundle, int):void
      com.google.android.gms.games.internal.zze.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.Players$LoadPlayersResult>, java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza((BaseImplementation.ResultHolder<Players.LoadPlayersResult>) this, this.zzkj, false);
    }
}
