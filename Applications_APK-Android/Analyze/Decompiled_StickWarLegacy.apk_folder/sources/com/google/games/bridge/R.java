package com.google.games.bridge;

public final class R {
    private R() {
    }

    public static final class attr {
        public static final int buttonSize = 2130771968;
        public static final int circleCrop = 2130771969;
        public static final int colorScheme = 2130771970;
        public static final int font = 2130771971;
        public static final int fontProviderAuthority = 2130771972;
        public static final int fontProviderCerts = 2130771973;
        public static final int fontProviderFetchStrategy = 2130771974;
        public static final int fontProviderFetchTimeout = 2130771975;
        public static final int fontProviderPackage = 2130771976;
        public static final int fontProviderQuery = 2130771977;
        public static final int fontStyle = 2130771978;
        public static final int fontWeight = 2130771979;
        public static final int imageAspectRatio = 2130771980;
        public static final int imageAspectRatioAdjust = 2130771981;
        public static final int scopeUris = 2130771982;

        private attr() {
        }
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2130837504;

        private bool() {
        }
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2130903040;
        public static final int common_google_signin_btn_text_dark_default = 2130903041;
        public static final int common_google_signin_btn_text_dark_disabled = 2130903042;
        public static final int common_google_signin_btn_text_dark_focused = 2130903043;
        public static final int common_google_signin_btn_text_dark_pressed = 2130903044;
        public static final int common_google_signin_btn_text_light = 2130903045;
        public static final int common_google_signin_btn_text_light_default = 2130903046;
        public static final int common_google_signin_btn_text_light_disabled = 2130903047;
        public static final int common_google_signin_btn_text_light_focused = 2130903048;
        public static final int common_google_signin_btn_text_light_pressed = 2130903049;
        public static final int common_google_signin_btn_tint = 2130903050;
        public static final int notification_action_color_filter = 2130903051;
        public static final int notification_icon_bg_color = 2130903052;
        public static final int notification_material_background_media_default_color = 2130903053;
        public static final int primary_text_default_material_dark = 2130903054;
        public static final int ripple_material_light = 2130903055;
        public static final int secondary_text_default_material_dark = 2130903056;
        public static final int secondary_text_default_material_light = 2130903057;

        private color() {
        }
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2130968576;
        public static final int compat_button_inset_vertical_material = 2130968577;
        public static final int compat_button_padding_horizontal_material = 2130968578;
        public static final int compat_button_padding_vertical_material = 2130968579;
        public static final int compat_control_corner_material = 2130968580;
        public static final int notification_action_icon_size = 2130968581;
        public static final int notification_action_text_size = 2130968582;
        public static final int notification_big_circle_margin = 2130968583;
        public static final int notification_content_margin_start = 2130968584;
        public static final int notification_large_icon_height = 2130968585;
        public static final int notification_large_icon_width = 2130968586;
        public static final int notification_main_column_padding_top = 2130968587;
        public static final int notification_media_narrow_margin = 2130968588;
        public static final int notification_right_icon_size = 2130968589;
        public static final int notification_right_side_padding_top = 2130968590;
        public static final int notification_small_icon_background_padding = 2130968591;
        public static final int notification_small_icon_size_as_large = 2130968592;
        public static final int notification_subtext_size = 2130968593;
        public static final int notification_top_pad = 2130968594;
        public static final int notification_top_pad_large_text = 2130968595;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131034112;
        public static final int common_google_signin_btn_icon_dark = 2131034113;
        public static final int common_google_signin_btn_icon_dark_focused = 2131034114;
        public static final int common_google_signin_btn_icon_dark_normal = 2131034115;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131034116;
        public static final int common_google_signin_btn_icon_disabled = 2131034117;
        public static final int common_google_signin_btn_icon_light = 2131034118;
        public static final int common_google_signin_btn_icon_light_focused = 2131034119;
        public static final int common_google_signin_btn_icon_light_normal = 2131034120;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131034121;
        public static final int common_google_signin_btn_text_dark = 2131034122;
        public static final int common_google_signin_btn_text_dark_focused = 2131034123;
        public static final int common_google_signin_btn_text_dark_normal = 2131034124;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131034125;
        public static final int common_google_signin_btn_text_disabled = 2131034126;
        public static final int common_google_signin_btn_text_light = 2131034127;
        public static final int common_google_signin_btn_text_light_focused = 2131034128;
        public static final int common_google_signin_btn_text_light_normal = 2131034129;
        public static final int common_google_signin_btn_text_light_normal_background = 2131034130;
        public static final int googleg_disabled_color_18 = 2131034131;
        public static final int googleg_standard_color_18 = 2131034132;
        public static final int ic_launcher = 2131034133;
        public static final int notification_action_background = 2131034134;
        public static final int notification_bg = 2131034135;
        public static final int notification_bg_low = 2131034136;
        public static final int notification_bg_low_normal = 2131034137;
        public static final int notification_bg_low_pressed = 2131034138;
        public static final int notification_bg_normal = 2131034139;
        public static final int notification_bg_normal_pressed = 2131034140;
        public static final int notification_icon_background = 2131034141;
        public static final int notification_template_icon_bg = 2131034142;
        public static final int notification_template_icon_low_bg = 2131034143;
        public static final int notification_tile_bg = 2131034144;
        public static final int notify_panel_notification_icon_bg = 2131034145;

        private drawable() {
        }
    }

    public static final class id {
        public static final int action0 = 2131099648;
        public static final int action_container = 2131099649;
        public static final int action_divider = 2131099650;
        public static final int action_image = 2131099651;
        public static final int action_text = 2131099652;
        public static final int actions = 2131099653;
        public static final int adjust_height = 2131099654;
        public static final int adjust_width = 2131099655;
        public static final int async = 2131099656;
        public static final int auto = 2131099657;
        public static final int blocking = 2131099658;
        public static final int cancel_action = 2131099659;
        public static final int chronometer = 2131099660;
        public static final int dark = 2131099661;
        public static final int end_padder = 2131099662;
        public static final int forever = 2131099663;
        public static final int icon = 2131099664;
        public static final int icon_group = 2131099665;
        public static final int icon_only = 2131099666;
        public static final int info = 2131099667;
        public static final int italic = 2131099668;
        public static final int light = 2131099669;
        public static final int line1 = 2131099670;
        public static final int line3 = 2131099671;
        public static final int media_actions = 2131099672;
        public static final int none = 2131099673;
        public static final int normal = 2131099674;
        public static final int notification_background = 2131099675;
        public static final int notification_main_column = 2131099676;
        public static final int notification_main_column_container = 2131099677;
        public static final int right_icon = 2131099678;
        public static final int right_side = 2131099679;
        public static final int standard = 2131099680;
        public static final int status_bar_latest_event_content = 2131099681;
        public static final int text = 2131099682;
        public static final int text2 = 2131099683;
        public static final int time = 2131099684;
        public static final int title = 2131099685;
        public static final int wide = 2131099686;

        private id() {
        }
    }

    public static final class integer {
        public static final int cancel_button_image_alpha = 2131165184;
        public static final int google_play_services_version = 2131165185;
        public static final int status_bar_notification_info_maxnum = 2131165186;

        private integer() {
        }
    }

    public static final class layout {
        public static final int notification_action = 2131230720;
        public static final int notification_action_tombstone = 2131230721;
        public static final int notification_media_action = 2131230722;
        public static final int notification_media_cancel_action = 2131230723;
        public static final int notification_template_big_media = 2131230724;
        public static final int notification_template_big_media_custom = 2131230725;
        public static final int notification_template_big_media_narrow = 2131230726;
        public static final int notification_template_big_media_narrow_custom = 2131230727;
        public static final int notification_template_custom_big = 2131230728;
        public static final int notification_template_icon_group = 2131230729;
        public static final int notification_template_lines_media = 2131230730;
        public static final int notification_template_media = 2131230731;
        public static final int notification_template_media_custom = 2131230732;
        public static final int notification_template_part_chronometer = 2131230733;
        public static final int notification_template_part_time = 2131230734;

        private layout() {
        }
    }

    public static final class string {
        public static final int app_name = 2131361792;
        public static final int common_google_play_services_enable_button = 2131361793;
        public static final int common_google_play_services_enable_text = 2131361794;
        public static final int common_google_play_services_enable_title = 2131361795;
        public static final int common_google_play_services_install_button = 2131361796;
        public static final int common_google_play_services_install_text = 2131361797;
        public static final int common_google_play_services_install_title = 2131361798;
        public static final int common_google_play_services_notification_channel_name = 2131361799;
        public static final int common_google_play_services_notification_ticker = 2131361800;
        public static final int common_google_play_services_unknown_issue = 2131361801;
        public static final int common_google_play_services_unsupported_text = 2131361802;
        public static final int common_google_play_services_update_button = 2131361803;
        public static final int common_google_play_services_update_text = 2131361804;
        public static final int common_google_play_services_update_title = 2131361805;
        public static final int common_google_play_services_updating_text = 2131361806;
        public static final int common_google_play_services_wear_update_text = 2131361807;
        public static final int common_open_on_phone = 2131361808;
        public static final int common_signin_button_text = 2131361809;
        public static final int common_signin_button_text_long = 2131361810;
        public static final int status_bar_notification_info_overflow = 2131361812;

        private string() {
        }
    }

    public static final class style {
        public static final int AppBaseTheme = 2131427328;
        public static final int AppTheme = 2131427329;
        public static final int TextAppearance_Compat_Notification = 2131427331;
        public static final int TextAppearance_Compat_Notification_Info = 2131427332;
        public static final int TextAppearance_Compat_Notification_Info_Media = 2131427333;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131427334;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 2131427335;
        public static final int TextAppearance_Compat_Notification_Media = 2131427336;
        public static final int TextAppearance_Compat_Notification_Time = 2131427337;
        public static final int TextAppearance_Compat_Notification_Time_Media = 2131427338;
        public static final int TextAppearance_Compat_Notification_Title = 2131427339;
        public static final int TextAppearance_Compat_Notification_Title_Media = 2131427340;
        public static final int Widget_Compat_NotificationActionContainer = 2131427344;
        public static final int Widget_Compat_NotificationActionText = 2131427345;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] FontFamily = {com.maxgames.stickwarlegacy.R.attr.fontProviderAuthority, com.maxgames.stickwarlegacy.R.attr.fontProviderCerts, com.maxgames.stickwarlegacy.R.attr.fontProviderFetchStrategy, com.maxgames.stickwarlegacy.R.attr.fontProviderFetchTimeout, com.maxgames.stickwarlegacy.R.attr.fontProviderPackage, com.maxgames.stickwarlegacy.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {com.maxgames.stickwarlegacy.R.attr.font, com.maxgames.stickwarlegacy.R.attr.fontStyle, com.maxgames.stickwarlegacy.R.attr.fontWeight};
        public static final int FontFamilyFont_font = 0;
        public static final int FontFamilyFont_fontStyle = 1;
        public static final int FontFamilyFont_fontWeight = 2;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] LoadingImageView = {com.maxgames.stickwarlegacy.R.attr.circleCrop, com.maxgames.stickwarlegacy.R.attr.imageAspectRatio, com.maxgames.stickwarlegacy.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.maxgames.stickwarlegacy.R.attr.buttonSize, com.maxgames.stickwarlegacy.R.attr.colorScheme, com.maxgames.stickwarlegacy.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }
}
