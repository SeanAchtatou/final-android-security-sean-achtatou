package com.maxgames.stickwarlegacy;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.maxgames.stickwarlegacy";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 11252;
    public static final String VERSION_NAME = "1.11.152";
}
