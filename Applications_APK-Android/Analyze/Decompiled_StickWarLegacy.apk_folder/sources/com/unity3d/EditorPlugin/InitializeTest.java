package com.unity3d.EditorPlugin;

import android.app.Activity;
import android.util.Log;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;

public class InitializeTest {
    public static void Initialize(Activity activity, String str, IUnityAdsListener iUnityAdsListener) {
        UnityAds.initialize(activity, str, iUnityAdsListener);
        Log.d("UnityAdsAndroidPlugin", "UnityAds Initializing");
    }
}
