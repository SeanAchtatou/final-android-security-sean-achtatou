package android.support.coreutils;

public final class R {
    private R() {
    }

    public static final class attr {
        public static final int font = 2130771971;
        public static final int fontProviderAuthority = 2130771972;
        public static final int fontProviderCerts = 2130771973;
        public static final int fontProviderFetchStrategy = 2130771974;
        public static final int fontProviderFetchTimeout = 2130771975;
        public static final int fontProviderPackage = 2130771976;
        public static final int fontProviderQuery = 2130771977;
        public static final int fontStyle = 2130771978;
        public static final int fontWeight = 2130771979;

        private attr() {
        }
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2130837504;

        private bool() {
        }
    }

    public static final class color {
        public static final int notification_action_color_filter = 2130903051;
        public static final int notification_icon_bg_color = 2130903052;
        public static final int ripple_material_light = 2130903055;
        public static final int secondary_text_default_material_light = 2130903057;

        private color() {
        }
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2130968576;
        public static final int compat_button_inset_vertical_material = 2130968577;
        public static final int compat_button_padding_horizontal_material = 2130968578;
        public static final int compat_button_padding_vertical_material = 2130968579;
        public static final int compat_control_corner_material = 2130968580;
        public static final int notification_action_icon_size = 2130968581;
        public static final int notification_action_text_size = 2130968582;
        public static final int notification_big_circle_margin = 2130968583;
        public static final int notification_content_margin_start = 2130968584;
        public static final int notification_large_icon_height = 2130968585;
        public static final int notification_large_icon_width = 2130968586;
        public static final int notification_main_column_padding_top = 2130968587;
        public static final int notification_media_narrow_margin = 2130968588;
        public static final int notification_right_icon_size = 2130968589;
        public static final int notification_right_side_padding_top = 2130968590;
        public static final int notification_small_icon_background_padding = 2130968591;
        public static final int notification_small_icon_size_as_large = 2130968592;
        public static final int notification_subtext_size = 2130968593;
        public static final int notification_top_pad = 2130968594;
        public static final int notification_top_pad_large_text = 2130968595;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int notification_action_background = 2131034134;
        public static final int notification_bg = 2131034135;
        public static final int notification_bg_low = 2131034136;
        public static final int notification_bg_low_normal = 2131034137;
        public static final int notification_bg_low_pressed = 2131034138;
        public static final int notification_bg_normal = 2131034139;
        public static final int notification_bg_normal_pressed = 2131034140;
        public static final int notification_icon_background = 2131034141;
        public static final int notification_template_icon_bg = 2131034142;
        public static final int notification_template_icon_low_bg = 2131034143;
        public static final int notification_tile_bg = 2131034144;
        public static final int notify_panel_notification_icon_bg = 2131034145;

        private drawable() {
        }
    }

    public static final class id {
        public static final int action_container = 2131099649;
        public static final int action_divider = 2131099650;
        public static final int action_image = 2131099651;
        public static final int action_text = 2131099652;
        public static final int actions = 2131099653;
        public static final int async = 2131099656;
        public static final int blocking = 2131099658;
        public static final int chronometer = 2131099660;
        public static final int forever = 2131099663;
        public static final int icon = 2131099664;
        public static final int icon_group = 2131099665;
        public static final int info = 2131099667;
        public static final int italic = 2131099668;
        public static final int line1 = 2131099670;
        public static final int line3 = 2131099671;
        public static final int normal = 2131099674;
        public static final int notification_background = 2131099675;
        public static final int notification_main_column = 2131099676;
        public static final int notification_main_column_container = 2131099677;
        public static final int right_icon = 2131099678;
        public static final int right_side = 2131099679;
        public static final int text = 2131099682;
        public static final int text2 = 2131099683;
        public static final int time = 2131099684;
        public static final int title = 2131099685;

        private id() {
        }
    }

    public static final class integer {
        public static final int status_bar_notification_info_maxnum = 2131165186;

        private integer() {
        }
    }

    public static final class layout {
        public static final int notification_action = 2131230720;
        public static final int notification_action_tombstone = 2131230721;
        public static final int notification_template_custom_big = 2131230728;
        public static final int notification_template_icon_group = 2131230729;
        public static final int notification_template_part_chronometer = 2131230733;
        public static final int notification_template_part_time = 2131230734;

        private layout() {
        }
    }

    public static final class string {
        public static final int status_bar_notification_info_overflow = 2131361812;

        private string() {
        }
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131427331;
        public static final int TextAppearance_Compat_Notification_Info = 2131427332;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131427334;
        public static final int TextAppearance_Compat_Notification_Time = 2131427337;
        public static final int TextAppearance_Compat_Notification_Title = 2131427339;
        public static final int Widget_Compat_NotificationActionContainer = 2131427344;
        public static final int Widget_Compat_NotificationActionText = 2131427345;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] FontFamily = {com.maxgames.stickwarlegacy.R.attr.fontProviderAuthority, com.maxgames.stickwarlegacy.R.attr.fontProviderCerts, com.maxgames.stickwarlegacy.R.attr.fontProviderFetchStrategy, com.maxgames.stickwarlegacy.R.attr.fontProviderFetchTimeout, com.maxgames.stickwarlegacy.R.attr.fontProviderPackage, com.maxgames.stickwarlegacy.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {com.maxgames.stickwarlegacy.R.attr.font, com.maxgames.stickwarlegacy.R.attr.fontStyle, com.maxgames.stickwarlegacy.R.attr.fontWeight};
        public static final int FontFamilyFont_font = 0;
        public static final int FontFamilyFont_fontStyle = 1;
        public static final int FontFamilyFont_fontWeight = 2;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;

        private styleable() {
        }
    }
}
