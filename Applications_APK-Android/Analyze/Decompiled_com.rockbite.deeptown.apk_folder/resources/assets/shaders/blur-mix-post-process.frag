#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

//RADIUS of our vignette, where 0.5 results in a circle fitting the screen
const float RADIUS = 0.75;
//softness of our vignette, between 0.0 and 1.0
const float SOFTNESS = 0.55;


varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform sampler2D blur_texture;
uniform float u_time;
uniform float vignette;
uniform float blurVal;
uniform float saturation;
uniform float brightness;

uniform float bottomAlpha;
uniform float bottomLight;
uniform vec4 bottomColor;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);
    vec4 blur = texture2D(blur_texture, v_texCoords);

    float vignetteRadius = RADIUS + (1.0 - vignette)*0.25;

    float radiusTm = vignetteRadius + sin(u_time*2.0)/50.0;


    float blMix = smoothstep(0.10, 0.35, abs(0.5 - v_texCoords.y)) * 1.0 * blurVal;
    color.rgb = mix(color.rgb, blur.rgb, blMix);

    float luminosity = color.r * 0.2126 + color.g * 0.7152 + color.b * 0.0722;
    vec3 bw = vec3(luminosity, luminosity, luminosity);
    color.rgb = mix(bw.rgb, color.rgb, saturation);

    color.rgb *= brightness;

    // Vignette
    //determine center
    vec2 position = (v_texCoords.xy) - vec2(0.5);
    //determine the vector length from center
    float len = length(position);
    //our vignette effect, using smoothstep
    float vignette = smoothstep(radiusTm, radiusTm-SOFTNESS, len);
    //apply our vignette
    color.rgb *= vignette;


    /*
    The bottom light/dark
    */
    vec4 preLightColor = color;
    float bottomPercent = 1.0 - smoothstep(0.1, 0.5, v_texCoords.y);
    bottomPercent = bottomPercent * bottomAlpha;
    color = mix(color, bottomColor, bottomPercent);
    color += (preLightColor*preLightColor*5.0) * (bottomPercent) * bottomLight;

    gl_FragColor =  color;
}
