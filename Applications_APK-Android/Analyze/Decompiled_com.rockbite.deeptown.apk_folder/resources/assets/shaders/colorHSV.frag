#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);
    color.a *= v_color.a;

    //vec3 clr = rgb2hsv(color.rgb);
    //clr.x = clr.x + (v_color.r-1.0);
    //color = vec4(hsv2rgb(clr), color.a);

    // saturation
    vec4 blackWhite = vec4(color);
    float gray = dot(color.rgb, vec3(0.299, 0.587, 0.114));
    blackWhite.rgb = vec3(gray);
    color = mix(color, blackWhite, 1.0-v_color.g);

    gl_FragColor =  color;
}
