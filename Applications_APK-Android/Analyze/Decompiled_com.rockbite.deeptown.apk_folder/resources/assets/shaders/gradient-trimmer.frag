#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;

uniform float progress;
uniform vec2 mainUV;
uniform vec2 mainU2V2;
uniform vec2 gradientUV;
uniform vec2 gradientU2V2;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);

    vec2 mainP = (mainU2V2-v_texCoords) /(mainU2V2 - mainUV);
    vec2 gradCoords = mainP * (gradientU2V2-gradientUV) + gradientUV;

    vec4 grad = texture2D(u_texture, gradCoords);

    float mixVal = smoothstep(progress-0.015, progress+0.015, grad.r);
    vec3 black = color.rgb * 0.2;
    color.rgb = mix(color.rgb, black.rgb, mixVal) * grad.a;

	gl_FragColor = color * v_color;
}
