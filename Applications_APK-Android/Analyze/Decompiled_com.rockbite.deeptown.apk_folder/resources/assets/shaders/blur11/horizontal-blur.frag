#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;

uniform float targetWidth;

const float contrast = 0.3;

varying vec2 blurTextureCoords[11];

void main() {

    // blur kernel
    vec4 color = vec4(0.0);

    color += texture2D(u_texture, blurTextureCoords[0]) * 0.0093;
    color += texture2D(u_texture, blurTextureCoords[1]) * 0.028002;
    color += texture2D(u_texture, blurTextureCoords[2]) * 0.065984;
    color += texture2D(u_texture, blurTextureCoords[3]) * 0.121703;
    color += texture2D(u_texture, blurTextureCoords[4]) * 0.175713;
    color += texture2D(u_texture, blurTextureCoords[5]) * 0.198596;
    color += texture2D(u_texture, blurTextureCoords[6]) * 0.175713;
    color += texture2D(u_texture, blurTextureCoords[7]) * 0.121703;
    color += texture2D(u_texture, blurTextureCoords[8]) * 0.065984;
    color += texture2D(u_texture, blurTextureCoords[9]) * 0.028002;
    color += texture2D(u_texture, blurTextureCoords[10])* 0.0093;

	gl_FragColor = color;
}

