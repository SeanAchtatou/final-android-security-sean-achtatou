attribute vec4 a_position;
attribute vec4 a_color;
attribute vec2 a_texCoord0;

uniform mat4 u_projTrans;

varying vec4 v_color;
varying vec2 v_texCoords;

uniform vec2 res;

uniform vec3 gravity_points[10];

void main() {
	v_color = a_color;
	v_texCoords = 	a_texCoord0;

	vec4 offset = vec4(0.0);

	vec2 sum_distortion = vec2(0.0);

	for(int i = 0; i < 10; i++) {
        vec2 distortion = vec2(a_position.x - gravity_points[i].x * res.x, a_position.y - gravity_points[i].y * res.y);
        float power = 1.0 - smoothstep(0.0, res.x * gravity_points[i].z, length(distortion));
        distortion = normalize(distortion) * gravity_points[i].z * power * (res.x / 24.0);
        sum_distortion = sum_distortion + distortion;
	}

	vec4 position = a_position;

	position.xy = position.xy + sum_distortion;

	vec4 currCord = u_projTrans * position;

	gl_Position = currCord;
}