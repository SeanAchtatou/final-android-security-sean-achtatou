#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform float mixValue;
uniform vec4 colorValue;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);
    float luminosity = color.r * 0.2126 + color.g * 0.7152 + color.b * 0.0722;
    vec4 bw = vec4(luminosity, luminosity, luminosity, color.a);
    vec4 tinted = bw * colorValue;
    vec4 finalColor = mix(tinted, color, mixValue);


	gl_FragColor = finalColor * v_color;
}
