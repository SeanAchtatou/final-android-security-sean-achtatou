#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;

uniform vec4 colorValue;
uniform float grayMix;
uniform float brightnessAdd;
uniform float brightnessMul;
uniform float progress;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);

    vec4 newColor = color;
    float luminosity = color.r * 0.2126 + color.g * 0.7152 + color.b * 0.0722;
    vec3 bw = vec3(luminosity, luminosity, luminosity);

    vec3 tint = vec3(colorValue.rgb); //ice

    newColor.rgb = bw * tint * 2.4;

    newColor.rgb = mix(bw, newColor.rgb, grayMix);
    newColor.rgb += brightnessAdd;
    newColor.rgb *= brightnessMul;

    color.rgb = mix(color.rgb, newColor.rgb, progress);

	gl_FragColor = color * v_color;
}