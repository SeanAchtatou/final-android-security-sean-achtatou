#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);

    vec3 stars = vec3(1.0, 1.0, 1.0);

    float lum = color.a * (color.r+color.g+color.b)/3.0;
    lum = smoothstep(0.1 - (1.0-v_texCoords.y)*0.1, 1.0, lum);


	gl_FragColor = vec4(stars.rgb, lum) * v_color;
}
