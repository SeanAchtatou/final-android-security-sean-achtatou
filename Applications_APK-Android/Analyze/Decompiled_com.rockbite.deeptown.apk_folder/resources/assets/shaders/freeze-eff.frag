#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;

uniform float progress;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);

    vec4 newColor = color;
    float luminosity = color.r * 0.2126 + color.g * 0.7152 + color.b * 0.0722;
    vec3 bw = vec3(luminosity, luminosity, luminosity);
    vec3 tint = vec3(0.0, 0.4, 1.0); //ice

    newColor.rgb = bw * tint * 2.4;

    newColor.rgb = mix(bw, newColor.rgb, 0.4);
    newColor.rgb +=0.05;
    newColor.rgb *=2.42;

    color.rgb = mix(color, newColor.rgb, progress);


	gl_FragColor = color * v_color;
}