#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;

uniform float mixValue;
uniform vec4 colorValue;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);

    vec4 newColor = color;
    float luminosity = color.r * 0.2126 + color.g * 0.7152 + color.b * 0.0722;
    vec3 bw = vec3(luminosity, luminosity, luminosity);
    vec3 blue = colorValue.rgb;
    newColor.rgb = bw * blue * 2.4;

    color = mix(color, newColor, mixValue);
    color.a *= colorValue.a;

	gl_FragColor = color * v_color;
}

