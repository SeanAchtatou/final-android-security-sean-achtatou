#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

//RADIUS of our vignette, where 0.5 results in a circle fitting the screen
const float RADIUS = 0.75;
//softness of our vignette, between 0.0 and 1.0
const float SOFTNESS = 0.55;



varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform float u_time;
//uniform float upper_alpha;
uniform float vignette;
uniform float saturation;

const vec3 SEPIA = vec3(1.2, 1.0, 0.8);
uniform float sepia;


uniform float brightness;


uniform float bottomAlpha;
uniform float bottomLight;
uniform vec4 bottomColor;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);

    float vignetteRadius = RADIUS + (1.0 - vignette)*0.25;

    float radiusTm = vignetteRadius + sin(u_time*2.0)/50.0;

    // Vignette
    //determine center
    vec2 position = (v_texCoords.xy) - vec2(0.5);
    //determine the vector length from center
    float len = length(position);
    //our vignette effect, using smoothstep
    float vignette = smoothstep(radiusTm, radiusTm-SOFTNESS, len);
    //apply our vignette
    color.rgb *= vignette;



    // calculate bw for all
    float luminosity = color.r * 0.2126 + color.g * 0.7152 + color.b * 0.0722;
    vec3 bw = vec3(luminosity, luminosity, luminosity);

    color.rgb = mix(bw, color.rgb, saturation);

    color.rgb *= brightness;

    /*
    // Film-grain
    float xGrain = (v_texCoords.x + 4.0 ) * (v_texCoords.y + 4.0 ) * (u_time * 10.0);
    vec4 grain = vec4(mod((mod(xGrain, 13.0) + 1.0) * (mod(xGrain, 123.0) + 1.0), 0.01)-0.005) * 8.0;
    color = color + grain;
    */

    /* Sepia */
    float gray = dot(color.rgb, vec3(0.299, 0.587, 0.114));
    vec3 sepiaColor = vec3(gray) * SEPIA;
    color.rgb = mix(color.rgb, sepiaColor, sepia);


    /*
    The bottom light/dark
    */
    vec4 preLightColor = color;
    float bottomPercent = 1.0 - smoothstep(0.1, 0.5, v_texCoords.y);
    bottomPercent = bottomPercent * bottomAlpha;
    color = mix(color, bottomColor, bottomPercent);
    color += (preLightColor*preLightColor*5.0) * (bottomPercent) * bottomLight;

    gl_FragColor =  color * v_color;
}
