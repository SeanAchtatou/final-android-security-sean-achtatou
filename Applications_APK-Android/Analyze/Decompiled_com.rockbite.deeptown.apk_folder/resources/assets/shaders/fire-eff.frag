#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);

    vec4 newColor = color;
    float luminosity = color.r * 0.2126 + color.g * 0.7152 + color.b * 0.0722;
    vec3 bw = vec3(luminosity, luminosity, luminosity);
    vec3 tint = vec3(1.0, 0.4, 0.0); //gold

    newColor.rgb = bw * tint * 2.4;

    color.rgb = mix(bw, newColor.rgb, 0.8);
    color.rgb -=0.1;
    color.rgb *=2.4;

	gl_FragColor = color * v_color;
}

