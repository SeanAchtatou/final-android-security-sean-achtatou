#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform float u_width;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);
    vec3 shadow = vec3(0.0, 0.0, 0.0);

    // shadow
    float shadowStrength = abs((gl_FragCoord.x/u_width) - 0.5) * 1.5;
    color.rgb = mix(color.rgb * v_color.rgb, shadow, shadowStrength);


	gl_FragColor = color;
}
