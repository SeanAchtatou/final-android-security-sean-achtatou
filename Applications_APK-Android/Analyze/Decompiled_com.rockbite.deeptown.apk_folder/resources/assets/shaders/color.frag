#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform float u_time;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);

    vec4 multiply = vec4(cos(u_time*2.0)+1.0, sin(u_time*2.0)+1.0, 0.0/255.0, 1);
    vec4 finalColor = vec4(color.b,color.b, color.b, 1.0) * multiply;

	gl_FragColor = finalColor;
}
