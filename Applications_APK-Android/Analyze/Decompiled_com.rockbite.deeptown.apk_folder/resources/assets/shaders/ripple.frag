#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform float u_time;

void main() {

    float time = u_time;

    vec2 dir = v_texCoords - vec2(0.5);

    float dist = distance(v_texCoords, vec2(0.5, 0.5));
    vec2 offset = dir * (sin(dist * 20.0 - time*17.0) + 0.5) / 50.0;

    vec2 texCoord = v_texCoords + offset;
    vec4 color = texture2D(u_texture, texCoord);

	gl_FragColor = color;
}
