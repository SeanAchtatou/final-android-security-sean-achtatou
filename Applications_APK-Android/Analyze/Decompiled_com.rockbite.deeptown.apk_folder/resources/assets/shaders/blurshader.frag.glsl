#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

uniform sampler2D u_texture;

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform float u_blurStrength;

const float maxBlur = 0.01;

void main () {

    float blur = u_blurStrength * maxBlur;

    vec2 dir = v_texCoords - vec2(0.5, 0.5);

    float hstep = dir.x;
    float vstep = dir.y;

    vec4 sum = vec4(0.0);

	sum += texture2D(u_texture, vec2(v_texCoords.x - 4.0*blur*hstep, v_texCoords.y - 4.0*blur*vstep)) * 0.0162162162;
	sum += texture2D(u_texture, vec2(v_texCoords.x - 3.0*blur*hstep, v_texCoords.y - 3.0*blur*vstep)) * 0.0540540541;
	sum += texture2D(u_texture, vec2(v_texCoords.x - 2.0*blur*hstep, v_texCoords.y - 2.0*blur*vstep)) * 0.1216216216;
	sum += texture2D(u_texture, vec2(v_texCoords.x - 1.0*blur*hstep, v_texCoords.y - 1.0*blur*vstep)) * 0.1945945946;

	sum += texture2D(u_texture, vec2(v_texCoords.x, v_texCoords.y)) * 0.2270270270;

	sum += texture2D(u_texture, vec2(v_texCoords.x + 1.0*blur*hstep, v_texCoords.y + 1.0*blur*vstep)) * 0.1945945946;
	sum += texture2D(u_texture, vec2(v_texCoords.x + 2.0*blur*hstep, v_texCoords.y + 2.0*blur*vstep)) * 0.1216216216;
	sum += texture2D(u_texture, vec2(v_texCoords.x + 3.0*blur*hstep, v_texCoords.y + 3.0*blur*vstep)) * 0.0540540541;
	sum += texture2D(u_texture, vec2(v_texCoords.x + 4.0*blur*hstep, v_texCoords.y + 4.0*blur*vstep)) * 0.0162162162;


	gl_FragColor = v_color * sum;
}