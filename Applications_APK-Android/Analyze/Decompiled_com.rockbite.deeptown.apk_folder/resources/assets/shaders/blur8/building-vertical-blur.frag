#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;

uniform float targetWidth;
//uniform vec4 colorValue;

const float contrast = 0.4;

varying vec2 blurTextureCoords[7];

void main() {

    // blur kernel
    vec4 color = vec4(0.0);

    color += texture2D(u_texture, blurTextureCoords[0])* 0.071303;
    color += texture2D(u_texture, blurTextureCoords[1])* 0.131514;
    color += texture2D(u_texture, blurTextureCoords[2])* 0.189879;
    color += texture2D(u_texture, blurTextureCoords[3])* 0.214607;
    color += texture2D(u_texture, blurTextureCoords[4])* 0.189879;
    color += texture2D(u_texture, blurTextureCoords[5])* 0.131514;
    color += texture2D(u_texture, blurTextureCoords[6])* 0.071303;


    // contrast
    color.rgb = (color.rgb - 0.5) * (1.0 + contrast) + 0.5;

    //vec3 blue = colorValue.rgb;
    float luminosity = color.r * 0.2126 + color.g * 0.7152 + color.b * 0.0722;
    color.rgb = color.rgb * 1.4;
    color *= v_color;

	gl_FragColor = color;
}

