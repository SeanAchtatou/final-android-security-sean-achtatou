attribute vec4 a_position;
attribute vec4 a_color;
attribute vec2 a_texCoord0;

uniform mat4 u_projTrans;

varying vec4 v_color;
varying vec2 v_texCoords;


uniform float targetWidth;
varying vec2 blurTextureCoords[7];

void main() {
	v_color = a_color;
	v_texCoords = 	a_texCoord0;
	gl_Position = u_projTrans * a_position;

    float pixelSize = 1.0 / targetWidth;

    // horizontal blur kernel
    for(int i = -3; i <= 3; i++ ) {
        blurTextureCoords[i+3] = v_texCoords + vec2(0.0, pixelSize * float(i));
    }
}