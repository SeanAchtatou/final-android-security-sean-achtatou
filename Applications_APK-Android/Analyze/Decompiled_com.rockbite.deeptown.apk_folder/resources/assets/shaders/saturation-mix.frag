#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);

    // calculate bw for all
    float luminosity = color.r * 0.2126 + color.g * 0.7152 + color.b * 0.0722;
    vec3 bw = vec3(luminosity, luminosity, luminosity);

    color.rgb = mix(color.rgb, bw, v_color.r);

    color.a *= v_color.a;

	gl_FragColor = color;
}
