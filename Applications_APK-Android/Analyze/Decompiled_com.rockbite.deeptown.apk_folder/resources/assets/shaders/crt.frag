#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform float u_time;

void main() {
    vec2 pos = v_texCoords;

    float parts = 100.0;
    float size = 1.0/parts;
    float r = size/2.0;
    vec2 circle = floor(pos/size)*size;
    circle.x+=r;
    circle.y+=r;
    float check = sqrt((pos.x-circle.x)*(pos.x-circle.x)+(pos.y-circle.y)*(pos.y-circle.y));
    vec4 color = texture2D(u_texture, pos);
    vec4 initialColor = color;
    vec4 black = vec4(0.0 ,0.0 ,0.0, 1.0);
    vec4 multiply = color*color;

    color = mix(color, black, smoothstep(0.0, 0.5, (check/r)/4.0));
    color+= mix(color, multiply, smoothstep(0.0, 0.2, (check/r)));

    vec4 final = mix(initialColor, color, 0.2+(sin(u_time*6.0)+1.0)/4.0);


	gl_FragColor = final;
}
