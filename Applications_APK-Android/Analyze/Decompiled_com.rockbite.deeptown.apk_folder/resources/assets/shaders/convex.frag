#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform float u_time;

void main() {

    float scaleX = 2 - v_texCoords.x;
    //float scaleX = (2);
    float scaleY = 0.5;

    float offsetX =0.5;

    float time = u_time;

    float x = (v_texCoords.x * scaleX) - 0.0;
    float y = v_texCoords.y;


    vec4 color = texture2D(u_texture, vec2(x, y));

    gl_FragColor = color;
}
