#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif


//RADIUS of our vignette, where 0.5 results in a circle fitting the screen
const float RADIUS = 0.85;
//softness of our vignette, between 0.0 and 1.0
const float SOFTNESS = 0.55;

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform float u_hue;
uniform float u_sat;
uniform float u_time;

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
    vec4 color = v_color * texture2D(u_texture, v_texCoords);

     vec3 clr = rgb2hsv(color.rgb);
     clr.x = clr.x + u_hue;
     color = vec4(hsv2rgb(clr), color.a);

     // saturation
     vec4 blackWhite = vec4(color);
     float avg = (color.r+color.g+color.b)/3.0;
     blackWhite.rgb = vec3(avg, avg, avg);
     color = mix(blackWhite, color, 1.0+u_sat);


    // Vignette
    //determine center
    vec2 position = (v_texCoords.xy) - vec2(0.5);
    //determine the vector length from center
    float len = length(position);
    //our vignette effect, using smoothstep
    float vignette = smoothstep(RADIUS, RADIUS-SOFTNESS, len);
    //apply our vignette
    color.rgb *= vignette;

    gl_FragColor =  color;
}
