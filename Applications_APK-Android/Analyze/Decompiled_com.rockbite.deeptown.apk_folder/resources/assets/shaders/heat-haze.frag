#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

const float speed = 7.0;

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform float u_time;
uniform float strength;

void main() {

    float frequency = 70.0;
    float amplitude = 0.0007 + 0.0015*(1.0-v_texCoords.y);

    float distortion = sin(v_texCoords.y * frequency - u_time * speed) * amplitude * strength;

    vec2 position = vec2(v_texCoords.x + distortion, v_texCoords.y);

    vec4 color = texture2D(u_texture, position);

	gl_FragColor = color * v_color;
}
