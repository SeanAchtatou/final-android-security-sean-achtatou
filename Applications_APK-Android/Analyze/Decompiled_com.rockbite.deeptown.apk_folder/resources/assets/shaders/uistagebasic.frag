#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;

void main() {
    vec4 color = texture2D(u_texture, v_texCoords);

    color.a *= v_color.a;

    float bw = step(0.0, ((v_color.r-0.99)*100.0));
    bw *= 1.0 - (step(0.0, ((v_color.g-0.99)*100.0+98.0)));
    bw *= 1.0 - (step(0.0, ((v_color.b-0.99)*100.0+98.0)));

    float luminosity = color.r * 0.2126 + color.g * 0.7152 + color.b * 0.0722;

    vec4 new_v_color = v_color;

    new_v_color.g = new_v_color.g + bw * (1.0 - new_v_color.g);
    new_v_color.b = new_v_color.b + bw * (1.0 - new_v_color.b);

    vec3 color_bw = vec3(luminosity, luminosity, luminosity);
    vec3 color_clr = color.rgb * new_v_color.rgb;

    color.rgb = mix(color_clr, color_bw, bw);
	gl_FragColor = color;
}
