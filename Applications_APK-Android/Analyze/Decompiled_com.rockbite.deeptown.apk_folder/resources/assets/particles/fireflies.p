fire
- Delay -
active: true
lowMin: 500.0
lowMax: 1500.0
- Duration - 
lowMin: 300.0
lowMax: 300.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: true
lowMin: 200.0
lowMax: 200.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 2.0
lowMax: 2.0
highMin: 40.0
highMax: 40.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.6179775
scaling2: 1.0
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.54537445
timeline2: 0.69955945
timeline3: 1.0
- Velocity - 
active: true
lowMin: 400.0
lowMax: 400.0
highMin: 1400.0
highMax: 1400.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.9726027
- Angle - 
active: true
lowMin: -30.0
lowMax: -30.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -360.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 1.0
colors1: 0.12156863
colors2: 0.047058824
colors3: 1.0
colors4: 0.12156863
colors5: 0.047058824
colors6: 1.0
colors7: 0.8509804
colors8: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.33677992
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 0.505618
scaling2: 1.0
scaling3: 0.33146068
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.6
timeline2: 0.69911504
timeline3: 0.7734513
timeline4: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
vfx-cloud.png


fire
- Delay -
active: true
lowMin: 500.0
lowMax: 1500.0
- Duration - 
lowMin: 300.0
lowMax: 300.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 600.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: true
lowMin: 240.0
lowMax: 240.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: true
lowMin: 60.0
lowMax: 60.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.5882353
scaling2: 1.0
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.5753425
timeline2: 0.760274
timeline3: 1.0
- Velocity - 
active: true
lowMin: -200.0
lowMax: -200.0
highMin: -1400.0
highMax: -1400.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.9726027
- Angle - 
active: true
lowMin: 120.0
lowMax: 120.0
highMin: 20.0
highMax: 20.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -360.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 1.0
colors1: 0.12156863
colors2: 0.047058824
colors3: 1.0
colors4: 0.12156863
colors5: 0.047058824
colors6: 1.0
colors7: 0.8509804
colors8: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.33677992
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 0.49438202
scaling2: 1.0
scaling3: 0.3508772
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.6
timeline2: 0.70530975
timeline3: 0.7671233
timeline4: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
vfx-cloud.png


fire
- Delay -
active: true
lowMin: 500.0
lowMax: 1500.0
- Duration - 
lowMin: 300.0
lowMax: 300.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 70.0
highMax: 70.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: true
lowMin: -100.0
lowMax: -100.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: true
lowMin: -40.0
lowMax: -40.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 2.0
lowMax: 2.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.5686275
scaling2: 1.0
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.5753425
timeline2: 0.760274
timeline3: 1.0
- Velocity - 
active: true
lowMin: -400.0
lowMax: -400.0
highMin: -1600.0
highMax: -1600.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.9726027
- Angle - 
active: true
lowMin: -90.0
lowMax: -90.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -360.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 1.0
colors1: 0.12156863
colors2: 0.047058824
colors3: 1.0
colors4: 0.12156863
colors5: 0.047058824
colors6: 1.0
colors7: 0.8509804
colors8: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.33677992
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 0.49438202
scaling2: 1.0
scaling3: 0.33333334
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.600885
timeline2: 0.70088494
timeline3: 0.7671233
timeline4: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
vfx-cloud.png


fire
- Delay -
active: true
lowMin: 500.0
lowMax: 1500.0
- Duration - 
lowMin: 300.0
lowMax: 300.0
- Count - 
min: 0
max: 25
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: true
lowMin: -240.0
lowMax: -240.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: true
lowMin: 50.0
lowMax: 50.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 2.0
lowMax: 2.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.54901963
scaling2: 1.0
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.5684931
timeline2: 0.760274
timeline3: 1.0
- Velocity - 
active: true
lowMin: 400.0
lowMax: 400.0
highMin: 1800.0
highMax: 1800.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.9726027
- Angle - 
active: true
lowMin: -90.0
lowMax: -90.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -360.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 1.0
colors1: 0.12156863
colors2: 0.047058824
colors3: 1.0
colors4: 0.12156863
colors5: 0.047058824
colors6: 1.0
colors7: 0.8509804
colors8: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.33677992
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 0.5224719
scaling2: 1.0
scaling3: 0.3508772
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.6
timeline2: 0.69911504
timeline3: 0.7671233
timeline4: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
vfx-cloud.png


fire
- Delay -
active: true
lowMin: 500.0
lowMax: 1500.0
- Duration - 
lowMin: 300.0
lowMax: 300.0
- Count - 
min: 0
max: 50
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: true
lowMin: -440.0
lowMax: -440.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: true
lowMin: 200.0
lowMax: 200.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 2.0
lowMax: 2.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.54901963
scaling2: 1.0
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.5684931
timeline2: 0.760274
timeline3: 1.0
- Velocity - 
active: true
lowMin: -400.0
lowMax: -400.0
highMin: -1600.0
highMax: -1600.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.9726027
- Angle - 
active: true
lowMin: 180.0
lowMax: 180.0
highMin: 80.0
highMax: 80.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 1.0
scaling2: 0.8627451
timelineCount: 3
timeline0: 0.0
timeline1: 0.84931505
timeline2: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -360.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 1.0
colors1: 0.12156863
colors2: 0.047058824
colors3: 1.0
colors4: 0.12156863
colors5: 0.047058824
colors6: 1.0
colors7: 0.8509804
colors8: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.33677992
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 0.49438202
scaling2: 1.0
scaling3: 0.36842105
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.59823006
timeline2: 0.7
timeline3: 0.7671233
timeline4: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
vfx-cloud.png


fire
- Delay -
active: true
lowMin: 500.0
lowMax: 1500.0
- Duration - 
lowMin: 300.0
lowMax: 300.0
- Count - 
min: 0
max: 50
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: true
lowMin: -300.0
lowMax: -300.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: true
lowMin: 200.0
lowMax: 200.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 2.0
lowMax: 2.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.5882353
scaling2: 1.0
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.5684931
timeline2: 0.760274
timeline3: 1.0
- Velocity - 
active: true
lowMin: -400.0
lowMax: -400.0
highMin: -1400.0
highMax: -1400.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.9726027
- Angle - 
active: true
lowMin: 180.0
lowMax: 180.0
highMin: 60.0
highMax: 60.0
relative: false
scalingCount: 2
scaling0: 0.74509805
scaling1: 0.9607843
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -360.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 1.0
colors1: 0.12156863
colors2: 0.047058824
colors3: 1.0
colors4: 0.12156863
colors5: 0.047058824
colors6: 1.0
colors7: 0.8509804
colors8: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.33677992
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 0.505618
scaling2: 1.0
scaling3: 0.36842105
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.59646016
timeline2: 0.69911504
timeline3: 0.7671233
timeline4: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
vfx-cloud.png


fire
- Delay -
active: true
lowMin: 500.0
lowMax: 1500.0
- Duration - 
lowMin: 300.0
lowMax: 300.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 80.0
highMax: 80.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: true
lowMin: -200.0
lowMax: -200.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: true
lowMin: 140.0
lowMax: 140.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 2.0
lowMax: 2.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.54901963
scaling2: 1.0
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.5684931
timeline2: 0.760274
timeline3: 1.0
- Velocity - 
active: true
lowMin: -400.0
lowMax: -400.0
highMin: -1400.0
highMax: -1400.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.9726027
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 210.0
highMax: 210.0
relative: false
scalingCount: 2
scaling0: 0.74509805
scaling1: 0.9607843
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -360.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 1.0
colors1: 0.12156863
colors2: 0.047058824
colors3: 1.0
colors4: 0.12156863
colors5: 0.047058824
colors6: 1.0
colors7: 0.8509804
colors8: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.33677992
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 0.5
scaling2: 1.0
scaling3: 0.36842105
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.6026549
timeline2: 0.70353985
timeline3: 0.760274
timeline4: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
vfx-cloud.png
