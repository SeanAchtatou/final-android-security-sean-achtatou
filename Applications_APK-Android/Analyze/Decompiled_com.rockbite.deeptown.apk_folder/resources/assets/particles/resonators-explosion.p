smoke
- Delay -
active: true
lowMin: 200.0
lowMax: 200.0
- Duration - 
lowMin: 250.0
lowMax: 250.0
- Count - 
min: 0
max: 50
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 80.0
highMax: 80.0
relative: false
scalingCount: 2
scaling0: 0.3469388
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1500.0
highMax: 1500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 200.0
relative: false
scalingCount: 3
scaling0: 0.3137255
scaling1: 1.0
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.33561644
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 400.0
relative: false
scalingCount: 3
scaling0: 0.2857143
scaling1: 0.53061223
scaling2: 0.24489796
timelineCount: 3
timeline0: 0.0
timeline1: 0.23287672
timeline2: 0.9726027
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.040816326
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.29452056
timeline2: 1.0
- Tint - 
colorsCount: 6
colors0: 1.0
colors1: 0.0
colors2: 0.20392157
colors3: 1.0
colors4: 0.8
colors5: 0.2
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.33333334
scaling1: 0.61403507
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.43150684
timeline2: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
vfx-explosion-smoke.png


ball
- Delay -
active: true
lowMin: 150.0
lowMax: 150.0
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 30.0
highMax: 30.0
relative: false
scalingCount: 3
scaling0: 0.54901963
scaling1: 1.0
scaling2: 0.43137255
timelineCount: 3
timeline0: 0.0
timeline1: 0.5753425
timeline2: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 600.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 15.0
lowMax: 15.0
highMin: 175.0
highMax: 175.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.11764706
scaling2: 0.8235294
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.34931508
timeline2: 0.65068495
timeline3: 1.0
- Y Scale - 
active: false
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 180.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.8235294
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 6
colors0: 0.32941177
colors1: 0.047058824
colors2: 1.0
colors3: 0.047058824
colors4: 0.8862745
colors5: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.8596491
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.19863014
timeline2: 0.5479452
timeline3: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: random
- Image Paths -
vfx-shockwave-two.png
vfx-shockwave-one.png


boom
- Delay -
active: false
- Duration - 
lowMin: 50.0
lowMax: 50.0
- Count - 
min: 0
max: 50
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 5.0
lowMax: 5.0
highMin: 200.0
highMax: 250.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.11764706
scaling2: 0.78431374
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.51369864
timeline2: 0.70547944
timeline3: 1.0
- Y Scale - 
active: false
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 360.0
highMax: -360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 0.3019608
colors1: 0.047058824
colors2: 1.0
colors3: 0.3019608
colors4: 0.047058824
colors5: 1.0
colors6: 0.4627451
colors7: 0.8352941
colors8: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.39070567
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.19298245
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.42465752
timeline2: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
vfx-receiver-flash.png


smoke
- Delay -
active: true
lowMin: 200.0
lowMax: 200.0
- Duration - 
lowMin: 250.0
lowMax: 250.0
- Count - 
min: 0
max: 50
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 80.0
highMax: 80.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1500.0
highMax: 1500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: true
lowMin: 240.0
lowMax: 240.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 200.0
relative: false
scalingCount: 3
scaling0: 0.3137255
scaling1: 1.0
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.33561644
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 400.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.6122449
scaling2: 0.3265306
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.2260274
timeline2: 0.41095892
timeline3: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.0
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.5479452
timeline2: 1.0
- Tint - 
colorsCount: 6
colors0: 1.0
colors1: 0.2
colors2: 0.2
colors3: 1.0
colors4: 0.39607844
colors5: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.22807017
scaling1: 0.61403507
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.43150684
timeline2: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
vfx-explosion-smoke.png


ball
- Delay -
active: true
lowMin: 150.0
lowMax: 150.0
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 30.0
highMax: 30.0
relative: false
scalingCount: 3
scaling0: 0.54901963
scaling1: 1.0
scaling2: 0.43137255
timelineCount: 3
timeline0: 0.0
timeline1: 0.5753425
timeline2: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 600.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: true
lowMin: 240.0
lowMax: 240.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 15.0
lowMax: 15.0
highMin: 175.0
highMax: 175.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.11764706
scaling2: 0.8235294
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.34931508
timeline2: 0.65068495
timeline3: 1.0
- Y Scale - 
active: false
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 180.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.8235294
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 6
colors0: 0.32941177
colors1: 0.047058824
colors2: 1.0
colors3: 0.047058824
colors4: 0.8862745
colors5: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.8596491
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.19863014
timeline2: 0.5479452
timeline3: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: random
- Image Paths -
vfx-shockwave-two.png
vfx-shockwave-one.png


boom
- Delay -
active: false
- Duration - 
lowMin: 50.0
lowMax: 50.0
- Count - 
min: 0
max: 50
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: true
lowMin: 240.0
lowMax: 240.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 5.0
lowMax: 5.0
highMin: 200.0
highMax: 250.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.11764706
scaling2: 0.78431374
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.51369864
timeline2: 0.70547944
timeline3: 1.0
- Y Scale - 
active: false
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 360.0
highMax: -360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 0.3019608
colors1: 0.047058824
colors2: 1.0
colors3: 0.3019608
colors4: 0.047058824
colors5: 1.0
colors6: 0.4627451
colors7: 0.8352941
colors8: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.39070567
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.1754386
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.31506848
timeline2: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
vfx-receiver-flash.png


smoke
- Delay -
active: true
lowMin: 200.0
lowMax: 200.0
- Duration - 
lowMin: 250.0
lowMax: 250.0
- Count - 
min: 0
max: 50
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 80.0
highMax: 80.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1500.0
highMax: 1500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: true
lowMin: -240.0
lowMax: -240.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 200.0
relative: false
scalingCount: 3
scaling0: 0.3137255
scaling1: 1.0
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.33561644
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 400.0
relative: false
scalingCount: 5
scaling0: 0.30612245
scaling1: 0.46938777
scaling2: 0.26530612
scaling3: 0.14285715
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.23287672
timeline2: 0.43835616
timeline3: 0.58219177
timeline4: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.0
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.5479452
timeline2: 1.0
- Tint - 
colorsCount: 6
colors0: 1.0
colors1: 0.0
colors2: 0.20392157
colors3: 1.0
colors4: 0.59607846
colors5: 0.2
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.33333334
scaling1: 0.68421054
scaling2: 0.36842105
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.4520548
timeline2: 0.75342464
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
vfx-explosion-smoke.png


ball
- Delay -
active: true
lowMin: 150.0
lowMax: 150.0
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 30.0
highMax: 30.0
relative: false
scalingCount: 3
scaling0: 0.54901963
scaling1: 1.0
scaling2: 0.43137255
timelineCount: 3
timeline0: 0.0
timeline1: 0.5753425
timeline2: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 600.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: true
lowMin: -240.0
lowMax: -240.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 15.0
lowMax: 15.0
highMin: 175.0
highMax: 175.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.11764706
scaling2: 0.8235294
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.34931508
timeline2: 0.65068495
timeline3: 1.0
- Y Scale - 
active: false
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 180.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.8235294
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 6
colors0: 0.32941177
colors1: 0.047058824
colors2: 1.0
colors3: 0.047058824
colors4: 0.8862745
colors5: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.8596491
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.19863014
timeline2: 0.5479452
timeline3: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: random
- Image Paths -
vfx-shockwave-two.png
vfx-shockwave-one.png


boom
- Delay -
active: false
- Duration - 
lowMin: 50.0
lowMax: 50.0
- Count - 
min: 0
max: 50
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: true
lowMin: -240.0
lowMax: -240.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 5.0
lowMax: 5.0
highMin: 200.0
highMax: 250.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.11764706
scaling2: 0.78431374
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.51369864
timeline2: 0.70547944
timeline3: 1.0
- Y Scale - 
active: false
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 360.0
highMax: -360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 0.3019608
colors1: 0.047058824
colors2: 1.0
colors3: 0.3019608
colors4: 0.047058824
colors5: 1.0
colors6: 0.4627451
colors7: 0.8352941
colors8: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.39070567
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.1754386
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.31506848
timeline2: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
vfx-receiver-flash.png


mainsmoke
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 0
max: 25
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 200.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 2000.0
highMax: 2000.0
relative: false
scalingCount: 2
scaling0: 0.59183675
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
independent: false
- Life Offset - 
active: false
independent: false
- X Offset - 
active: true
lowMin: -250.0
lowMax: 250.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Y Offset - 
active: true
lowMin: -20.0
lowMax: 20.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 350.0
relative: false
scalingCount: 3
scaling0: 0.18367347
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.21917808
timeline2: 1.0
- Y Scale - 
active: false
- Velocity - 
active: false
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 12
colors0: 1.0
colors1: 0.0
colors2: 0.20392157
colors3: 0.4
colors4: 0.6
colors5: 1.0
colors6: 1.0
colors7: 0.2
colors8: 0.2
colors9: 1.0
colors10: 0.12156863
colors11: 0.047058824
timelineCount: 4
timeline0: 0.0
timeline1: 0.27635887
timeline2: 0.32678455
timeline3: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.1754386
scaling1: 0.75438595
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.12328767
timeline2: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
- Image Paths -
vfx-explosion-smoke.png

