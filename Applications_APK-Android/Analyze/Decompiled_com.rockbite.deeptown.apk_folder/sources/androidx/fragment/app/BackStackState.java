package androidx.fragment.app;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import androidx.fragment.app.k;
import androidx.lifecycle.e;
import java.util.ArrayList;

@SuppressLint({"BanParcelableUsage"})
final class BackStackState implements Parcelable {
    public static final Parcelable.Creator<BackStackState> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    final int[] f1439a;

    /* renamed from: b  reason: collision with root package name */
    final ArrayList<String> f1440b;

    /* renamed from: c  reason: collision with root package name */
    final int[] f1441c;

    /* renamed from: d  reason: collision with root package name */
    final int[] f1442d;

    /* renamed from: e  reason: collision with root package name */
    final int f1443e;

    /* renamed from: f  reason: collision with root package name */
    final int f1444f;

    /* renamed from: g  reason: collision with root package name */
    final String f1445g;

    /* renamed from: h  reason: collision with root package name */
    final int f1446h;

    /* renamed from: i  reason: collision with root package name */
    final int f1447i;

    /* renamed from: j  reason: collision with root package name */
    final CharSequence f1448j;

    /* renamed from: k  reason: collision with root package name */
    final int f1449k;
    final CharSequence l;
    final ArrayList<String> m;
    final ArrayList<String> n;
    final boolean o;

    static class a implements Parcelable.Creator<BackStackState> {
        a() {
        }

        public BackStackState createFromParcel(Parcel parcel) {
            return new BackStackState(parcel);
        }

        public BackStackState[] newArray(int i2) {
            return new BackStackState[i2];
        }
    }

    public BackStackState(a aVar) {
        int size = aVar.f1540a.size();
        this.f1439a = new int[(size * 5)];
        if (aVar.f1547h) {
            this.f1440b = new ArrayList<>(size);
            this.f1441c = new int[size];
            this.f1442d = new int[size];
            int i2 = 0;
            int i3 = 0;
            while (i2 < size) {
                k.a aVar2 = aVar.f1540a.get(i2);
                int i4 = i3 + 1;
                this.f1439a[i3] = aVar2.f1551a;
                ArrayList<String> arrayList = this.f1440b;
                Fragment fragment = aVar2.f1552b;
                arrayList.add(fragment != null ? fragment.mWho : null);
                int[] iArr = this.f1439a;
                int i5 = i4 + 1;
                iArr[i4] = aVar2.f1553c;
                int i6 = i5 + 1;
                iArr[i5] = aVar2.f1554d;
                int i7 = i6 + 1;
                iArr[i6] = aVar2.f1555e;
                iArr[i7] = aVar2.f1556f;
                this.f1441c[i2] = aVar2.f1557g.ordinal();
                this.f1442d[i2] = aVar2.f1558h.ordinal();
                i2++;
                i3 = i7 + 1;
            }
            this.f1443e = aVar.f1545f;
            this.f1444f = aVar.f1546g;
            this.f1445g = aVar.f1548i;
            this.f1446h = aVar.t;
            this.f1447i = aVar.f1549j;
            this.f1448j = aVar.f1550k;
            this.f1449k = aVar.l;
            this.l = aVar.m;
            this.m = aVar.n;
            this.n = aVar.o;
            this.o = aVar.p;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }

    public a a(i iVar) {
        a aVar = new a(iVar);
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.f1439a.length) {
            k.a aVar2 = new k.a();
            int i4 = i2 + 1;
            aVar2.f1551a = this.f1439a[i2];
            if (i.H) {
                Log.v("FragmentManager", "Instantiate " + aVar + " op #" + i3 + " base fragment #" + this.f1439a[i4]);
            }
            String str = this.f1440b.get(i3);
            if (str != null) {
                aVar2.f1552b = iVar.f1497g.get(str);
            } else {
                aVar2.f1552b = null;
            }
            aVar2.f1557g = e.b.values()[this.f1441c[i3]];
            aVar2.f1558h = e.b.values()[this.f1442d[i3]];
            int[] iArr = this.f1439a;
            int i5 = i4 + 1;
            aVar2.f1553c = iArr[i4];
            int i6 = i5 + 1;
            aVar2.f1554d = iArr[i5];
            int i7 = i6 + 1;
            aVar2.f1555e = iArr[i6];
            aVar2.f1556f = iArr[i7];
            aVar.f1541b = aVar2.f1553c;
            aVar.f1542c = aVar2.f1554d;
            aVar.f1543d = aVar2.f1555e;
            aVar.f1544e = aVar2.f1556f;
            aVar.a(aVar2);
            i3++;
            i2 = i7 + 1;
        }
        aVar.f1545f = this.f1443e;
        aVar.f1546g = this.f1444f;
        aVar.f1548i = this.f1445g;
        aVar.t = this.f1446h;
        aVar.f1547h = true;
        aVar.f1549j = this.f1447i;
        aVar.f1550k = this.f1448j;
        aVar.l = this.f1449k;
        aVar.m = this.l;
        aVar.n = this.m;
        aVar.o = this.n;
        aVar.p = this.o;
        aVar.a(1);
        return aVar;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeIntArray(this.f1439a);
        parcel.writeStringList(this.f1440b);
        parcel.writeIntArray(this.f1441c);
        parcel.writeIntArray(this.f1442d);
        parcel.writeInt(this.f1443e);
        parcel.writeInt(this.f1444f);
        parcel.writeString(this.f1445g);
        parcel.writeInt(this.f1446h);
        parcel.writeInt(this.f1447i);
        TextUtils.writeToParcel(this.f1448j, parcel, 0);
        parcel.writeInt(this.f1449k);
        TextUtils.writeToParcel(this.l, parcel, 0);
        parcel.writeStringList(this.m);
        parcel.writeStringList(this.n);
        parcel.writeInt(this.o ? 1 : 0);
    }

    public BackStackState(Parcel parcel) {
        this.f1439a = parcel.createIntArray();
        this.f1440b = parcel.createStringArrayList();
        this.f1441c = parcel.createIntArray();
        this.f1442d = parcel.createIntArray();
        this.f1443e = parcel.readInt();
        this.f1444f = parcel.readInt();
        this.f1445g = parcel.readString();
        this.f1446h = parcel.readInt();
        this.f1447i = parcel.readInt();
        this.f1448j = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f1449k = parcel.readInt();
        this.l = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.m = parcel.createStringArrayList();
        this.n = parcel.createStringArrayList();
        this.o = parcel.readInt() != 0;
    }
}
