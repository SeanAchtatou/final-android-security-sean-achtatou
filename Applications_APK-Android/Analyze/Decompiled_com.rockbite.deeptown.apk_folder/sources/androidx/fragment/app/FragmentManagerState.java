package androidx.fragment.app;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

@SuppressLint({"BanParcelableUsage"})
final class FragmentManagerState implements Parcelable {
    public static final Parcelable.Creator<FragmentManagerState> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    ArrayList<FragmentState> f1466a;

    /* renamed from: b  reason: collision with root package name */
    ArrayList<String> f1467b;

    /* renamed from: c  reason: collision with root package name */
    BackStackState[] f1468c;

    /* renamed from: d  reason: collision with root package name */
    String f1469d = null;

    /* renamed from: e  reason: collision with root package name */
    int f1470e;

    static class a implements Parcelable.Creator<FragmentManagerState> {
        a() {
        }

        public FragmentManagerState createFromParcel(Parcel parcel) {
            return new FragmentManagerState(parcel);
        }

        public FragmentManagerState[] newArray(int i2) {
            return new FragmentManagerState[i2];
        }
    }

    public FragmentManagerState() {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeTypedList(this.f1466a);
        parcel.writeStringList(this.f1467b);
        parcel.writeTypedArray(this.f1468c, i2);
        parcel.writeString(this.f1469d);
        parcel.writeInt(this.f1470e);
    }

    public FragmentManagerState(Parcel parcel) {
        this.f1466a = parcel.createTypedArrayList(FragmentState.CREATOR);
        this.f1467b = parcel.createStringArrayList();
        this.f1468c = (BackStackState[]) parcel.createTypedArray(BackStackState.CREATOR);
        this.f1469d = parcel.readString();
        this.f1470e = parcel.readInt();
    }
}
