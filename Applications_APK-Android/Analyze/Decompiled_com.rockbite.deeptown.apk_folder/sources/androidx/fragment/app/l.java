package androidx.fragment.app;

import android.graphics.Rect;
import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.app.n;
import b.e.m.r;
import b.e.m.u;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/* compiled from: FragmentTransition */
class l {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f1559a = {0, 3, 0, 1, 5, 4, 7, 6, 9, 8, 10};

    /* renamed from: b  reason: collision with root package name */
    private static final n f1560b = (Build.VERSION.SDK_INT >= 21 ? new m() : null);

    /* renamed from: c  reason: collision with root package name */
    private static final n f1561c = a();

    /* compiled from: FragmentTransition */
    static class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ ArrayList f1562a;

        a(ArrayList arrayList) {
            this.f1562a = arrayList;
        }

        public void run() {
            l.a(this.f1562a, 4);
        }
    }

    /* compiled from: FragmentTransition */
    static class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Object f1563a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ n f1564b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ View f1565c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ Fragment f1566d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ ArrayList f1567e;

        /* renamed from: f  reason: collision with root package name */
        final /* synthetic */ ArrayList f1568f;

        /* renamed from: g  reason: collision with root package name */
        final /* synthetic */ ArrayList f1569g;

        /* renamed from: h  reason: collision with root package name */
        final /* synthetic */ Object f1570h;

        b(Object obj, n nVar, View view, Fragment fragment, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, Object obj2) {
            this.f1563a = obj;
            this.f1564b = nVar;
            this.f1565c = view;
            this.f1566d = fragment;
            this.f1567e = arrayList;
            this.f1568f = arrayList2;
            this.f1569g = arrayList3;
            this.f1570h = obj2;
        }

        public void run() {
            Object obj = this.f1563a;
            if (obj != null) {
                this.f1564b.b(obj, this.f1565c);
                this.f1568f.addAll(l.a(this.f1564b, this.f1563a, this.f1566d, this.f1567e, this.f1565c));
            }
            if (this.f1569g != null) {
                if (this.f1570h != null) {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(this.f1565c);
                    this.f1564b.a(this.f1570h, (ArrayList<View>) this.f1569g, (ArrayList<View>) arrayList);
                }
                this.f1569g.clear();
                this.f1569g.add(this.f1565c);
            }
        }
    }

    /* compiled from: FragmentTransition */
    static class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Fragment f1571a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Fragment f1572b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ boolean f1573c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ b.d.a f1574d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ View f1575e;

        /* renamed from: f  reason: collision with root package name */
        final /* synthetic */ n f1576f;

        /* renamed from: g  reason: collision with root package name */
        final /* synthetic */ Rect f1577g;

        c(Fragment fragment, Fragment fragment2, boolean z, b.d.a aVar, View view, n nVar, Rect rect) {
            this.f1571a = fragment;
            this.f1572b = fragment2;
            this.f1573c = z;
            this.f1574d = aVar;
            this.f1575e = view;
            this.f1576f = nVar;
            this.f1577g = rect;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.fragment.app.l.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a<java.lang.String, android.view.View>, boolean):void
         arg types: [androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a, int]
         candidates:
          androidx.fragment.app.l.a(int, java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int):b.d.a<java.lang.String, java.lang.String>
          androidx.fragment.app.l.a(androidx.fragment.app.n, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View>
          androidx.fragment.app.l.a(androidx.fragment.app.a, androidx.fragment.app.k$a, android.util.SparseArray<androidx.fragment.app.l$e>, boolean, boolean):void
          androidx.fragment.app.l.a(androidx.fragment.app.i, int, androidx.fragment.app.l$e, android.view.View, b.d.a<java.lang.String, java.lang.String>):void
          androidx.fragment.app.l.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a<java.lang.String, android.view.View>, boolean):void */
        public void run() {
            l.a(this.f1571a, this.f1572b, this.f1573c, (b.d.a<String, View>) this.f1574d, false);
            View view = this.f1575e;
            if (view != null) {
                this.f1576f.a(view, this.f1577g);
            }
        }
    }

    /* compiled from: FragmentTransition */
    static class d implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ n f1578a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ b.d.a f1579b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ Object f1580c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ e f1581d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ ArrayList f1582e;

        /* renamed from: f  reason: collision with root package name */
        final /* synthetic */ View f1583f;

        /* renamed from: g  reason: collision with root package name */
        final /* synthetic */ Fragment f1584g;

        /* renamed from: h  reason: collision with root package name */
        final /* synthetic */ Fragment f1585h;

        /* renamed from: i  reason: collision with root package name */
        final /* synthetic */ boolean f1586i;

        /* renamed from: j  reason: collision with root package name */
        final /* synthetic */ ArrayList f1587j;

        /* renamed from: k  reason: collision with root package name */
        final /* synthetic */ Object f1588k;
        final /* synthetic */ Rect l;

        d(n nVar, b.d.a aVar, Object obj, e eVar, ArrayList arrayList, View view, Fragment fragment, Fragment fragment2, boolean z, ArrayList arrayList2, Object obj2, Rect rect) {
            this.f1578a = nVar;
            this.f1579b = aVar;
            this.f1580c = obj;
            this.f1581d = eVar;
            this.f1582e = arrayList;
            this.f1583f = view;
            this.f1584g = fragment;
            this.f1585h = fragment2;
            this.f1586i = z;
            this.f1587j = arrayList2;
            this.f1588k = obj2;
            this.l = rect;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.fragment.app.l.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a<java.lang.String, android.view.View>, boolean):void
         arg types: [androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a<java.lang.String, android.view.View>, int]
         candidates:
          androidx.fragment.app.l.a(int, java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int):b.d.a<java.lang.String, java.lang.String>
          androidx.fragment.app.l.a(androidx.fragment.app.n, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View>
          androidx.fragment.app.l.a(androidx.fragment.app.a, androidx.fragment.app.k$a, android.util.SparseArray<androidx.fragment.app.l$e>, boolean, boolean):void
          androidx.fragment.app.l.a(androidx.fragment.app.i, int, androidx.fragment.app.l$e, android.view.View, b.d.a<java.lang.String, java.lang.String>):void
          androidx.fragment.app.l.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a<java.lang.String, android.view.View>, boolean):void */
        public void run() {
            b.d.a<String, View> a2 = l.a(this.f1578a, this.f1579b, this.f1580c, this.f1581d);
            if (a2 != null) {
                this.f1582e.addAll(a2.values());
                this.f1582e.add(this.f1583f);
            }
            l.a(this.f1584g, this.f1585h, this.f1586i, a2, false);
            Object obj = this.f1580c;
            if (obj != null) {
                this.f1578a.b(obj, (ArrayList<View>) this.f1587j, (ArrayList<View>) this.f1582e);
                View a3 = l.a(a2, this.f1581d, this.f1588k, this.f1586i);
                if (a3 != null) {
                    this.f1578a.a(a3, this.l);
                }
            }
        }
    }

    /* compiled from: FragmentTransition */
    static class e {

        /* renamed from: a  reason: collision with root package name */
        public Fragment f1589a;

        /* renamed from: b  reason: collision with root package name */
        public boolean f1590b;

        /* renamed from: c  reason: collision with root package name */
        public a f1591c;

        /* renamed from: d  reason: collision with root package name */
        public Fragment f1592d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f1593e;

        /* renamed from: f  reason: collision with root package name */
        public a f1594f;

        e() {
        }
    }

    private static n a() {
        try {
            return (n) Class.forName("androidx.transition.FragmentTransitionSupport").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    private static void b(i iVar, int i2, e eVar, View view, b.d.a<String, String> aVar) {
        Fragment fragment;
        Fragment fragment2;
        n a2;
        Object obj;
        i iVar2 = iVar;
        e eVar2 = eVar;
        View view2 = view;
        ViewGroup viewGroup = iVar2.r.a() ? (ViewGroup) iVar2.r.a(i2) : null;
        if (viewGroup != null && (a2 = a((fragment2 = eVar2.f1592d), fragment)) != null) {
            boolean z = eVar2.f1590b;
            boolean z2 = eVar2.f1593e;
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            Object a3 = a(a2, (fragment = eVar2.f1589a), z);
            Object b2 = b(a2, fragment2, z2);
            ViewGroup viewGroup2 = viewGroup;
            ArrayList arrayList3 = arrayList2;
            Object b3 = b(a2, viewGroup, view, aVar, eVar, arrayList2, arrayList, a3, b2);
            Object obj2 = a3;
            if (obj2 == null && b3 == null) {
                obj = b2;
                if (obj == null) {
                    return;
                }
            } else {
                obj = b2;
            }
            ArrayList<View> a4 = a(a2, obj, fragment2, arrayList3, view2);
            ArrayList<View> a5 = a(a2, obj2, fragment, arrayList, view2);
            a(a5, 4);
            Fragment fragment3 = fragment;
            ArrayList<View> arrayList4 = a4;
            Object a6 = a(a2, obj2, obj, b3, fragment3, z);
            if (a6 != null) {
                a(a2, obj, fragment2, arrayList4);
                ArrayList<String> a7 = a2.a((ArrayList<View>) arrayList);
                a2.a(a6, obj2, a5, obj, arrayList4, b3, arrayList);
                ViewGroup viewGroup3 = viewGroup2;
                a2.a(viewGroup3, a6);
                a2.a(viewGroup3, arrayList3, arrayList, a7, aVar);
                a(a5, 0);
                a2.b(b3, (ArrayList<View>) arrayList3, (ArrayList<View>) arrayList);
            }
        }
    }

    static void a(i iVar, ArrayList<a> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3, boolean z) {
        if (iVar.p >= 1) {
            SparseArray sparseArray = new SparseArray();
            for (int i4 = i2; i4 < i3; i4++) {
                a aVar = arrayList.get(i4);
                if (arrayList2.get(i4).booleanValue()) {
                    b(aVar, sparseArray, z);
                } else {
                    a(aVar, sparseArray, z);
                }
            }
            if (sparseArray.size() != 0) {
                View view = new View(iVar.q.c());
                int size = sparseArray.size();
                for (int i5 = 0; i5 < size; i5++) {
                    int keyAt = sparseArray.keyAt(i5);
                    b.d.a<String, String> a2 = a(keyAt, arrayList, arrayList2, i2, i3);
                    e eVar = (e) sparseArray.valueAt(i5);
                    if (z) {
                        b(iVar, keyAt, eVar, view, a2);
                    } else {
                        a(iVar, keyAt, eVar, view, a2);
                    }
                }
            }
        }
    }

    private static b.d.a<String, String> a(int i2, ArrayList<a> arrayList, ArrayList<Boolean> arrayList2, int i3, int i4) {
        ArrayList<String> arrayList3;
        ArrayList<String> arrayList4;
        b.d.a<String, String> aVar = new b.d.a<>();
        for (int i5 = i4 - 1; i5 >= i3; i5--) {
            a aVar2 = arrayList.get(i5);
            if (aVar2.b(i2)) {
                boolean booleanValue = arrayList2.get(i5).booleanValue();
                ArrayList<String> arrayList5 = aVar2.n;
                if (arrayList5 != null) {
                    int size = arrayList5.size();
                    if (booleanValue) {
                        ArrayList<String> arrayList6 = aVar2.n;
                        arrayList3 = aVar2.o;
                        arrayList4 = arrayList6;
                    } else {
                        arrayList3 = aVar2.n;
                        arrayList4 = aVar2.o;
                    }
                    for (int i6 = 0; i6 < size; i6++) {
                        String str = (String) arrayList3.get(i6);
                        String str2 = (String) arrayList4.get(i6);
                        String remove = aVar.remove(str2);
                        if (remove != null) {
                            aVar.put(str, remove);
                        } else {
                            aVar.put(str, str2);
                        }
                    }
                }
            }
        }
        return aVar;
    }

    private static Object b(n nVar, Fragment fragment, boolean z) {
        Object obj;
        if (fragment == null) {
            return null;
        }
        if (z) {
            obj = fragment.getReturnTransition();
        } else {
            obj = fragment.getExitTransition();
        }
        return nVar.b(obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.l.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a<java.lang.String, android.view.View>, boolean):void
     arg types: [androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a<java.lang.String, android.view.View>, int]
     candidates:
      androidx.fragment.app.l.a(int, java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int):b.d.a<java.lang.String, java.lang.String>
      androidx.fragment.app.l.a(androidx.fragment.app.n, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View>
      androidx.fragment.app.l.a(androidx.fragment.app.a, androidx.fragment.app.k$a, android.util.SparseArray<androidx.fragment.app.l$e>, boolean, boolean):void
      androidx.fragment.app.l.a(androidx.fragment.app.i, int, androidx.fragment.app.l$e, android.view.View, b.d.a<java.lang.String, java.lang.String>):void
      androidx.fragment.app.l.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a<java.lang.String, android.view.View>, boolean):void */
    private static Object b(n nVar, ViewGroup viewGroup, View view, b.d.a<String, String> aVar, e eVar, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object obj3;
        Object obj4;
        Rect rect;
        View view2;
        View view3 = view;
        b.d.a<String, String> aVar2 = aVar;
        e eVar2 = eVar;
        ArrayList<View> arrayList3 = arrayList;
        ArrayList<View> arrayList4 = arrayList2;
        Object obj5 = obj;
        Fragment fragment = eVar2.f1589a;
        Fragment fragment2 = eVar2.f1592d;
        if (fragment != null) {
            fragment.requireView().setVisibility(0);
        }
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = eVar2.f1590b;
        if (aVar.isEmpty()) {
            obj3 = null;
        } else {
            obj3 = a(nVar, fragment, fragment2, z);
        }
        b.d.a<String, View> b2 = b(nVar, aVar2, obj3, eVar2);
        b.d.a<String, View> a2 = a(nVar, aVar2, obj3, eVar2);
        if (aVar.isEmpty()) {
            if (b2 != null) {
                b2.clear();
            }
            if (a2 != null) {
                a2.clear();
            }
            obj4 = null;
        } else {
            a(arrayList3, b2, aVar.keySet());
            a(arrayList4, a2, aVar.values());
            obj4 = obj3;
        }
        if (obj5 == null && obj2 == null && obj4 == null) {
            return null;
        }
        a(fragment, fragment2, z, b2, true);
        if (obj4 != null) {
            arrayList4.add(view3);
            nVar.b(obj4, view3, arrayList3);
            a(nVar, obj4, obj2, b2, eVar2.f1593e, eVar2.f1594f);
            Rect rect2 = new Rect();
            View a3 = a(a2, eVar2, obj5, z);
            if (a3 != null) {
                nVar.a(obj5, rect2);
            }
            rect = rect2;
            view2 = a3;
        } else {
            view2 = null;
            rect = null;
        }
        r.a(viewGroup, new c(fragment, fragment2, z, a2, view2, nVar, rect));
        return obj4;
    }

    private static void a(n nVar, Object obj, Fragment fragment, ArrayList<View> arrayList) {
        if (fragment != null && obj != null && fragment.mAdded && fragment.mHidden && fragment.mHiddenChanged) {
            fragment.setHideReplaced(true);
            nVar.a(obj, fragment.getView(), arrayList);
            r.a(fragment.mContainer, new a(arrayList));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.n.a(android.view.View, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
     arg types: [android.view.ViewGroup, java.util.ArrayList, b.d.a<java.lang.String, java.lang.String>]
     candidates:
      androidx.fragment.app.n.a(java.util.List<android.view.View>, android.view.View, int):boolean
      androidx.fragment.app.n.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      androidx.fragment.app.n.a(android.view.ViewGroup, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
      androidx.fragment.app.n.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      androidx.fragment.app.n.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void
      androidx.fragment.app.n.a(android.view.View, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.n.a(android.view.ViewGroup, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
     arg types: [android.view.ViewGroup, java.util.ArrayList, b.d.a<java.lang.String, java.lang.String>]
     candidates:
      androidx.fragment.app.n.a(java.util.List<android.view.View>, android.view.View, int):boolean
      androidx.fragment.app.n.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      androidx.fragment.app.n.a(android.view.View, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
      androidx.fragment.app.n.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      androidx.fragment.app.n.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void
      androidx.fragment.app.n.a(android.view.ViewGroup, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void */
    private static void a(i iVar, int i2, e eVar, View view, b.d.a<String, String> aVar) {
        Fragment fragment;
        Fragment fragment2;
        n a2;
        Object obj;
        i iVar2 = iVar;
        e eVar2 = eVar;
        View view2 = view;
        b.d.a<String, String> aVar2 = aVar;
        ViewGroup viewGroup = iVar2.r.a() ? (ViewGroup) iVar2.r.a(i2) : null;
        if (viewGroup != null && (a2 = a((fragment2 = eVar2.f1592d), fragment)) != null) {
            boolean z = eVar2.f1590b;
            boolean z2 = eVar2.f1593e;
            Object a3 = a(a2, (fragment = eVar2.f1589a), z);
            Object b2 = b(a2, fragment2, z2);
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = arrayList;
            Object obj2 = b2;
            n nVar = a2;
            Object a4 = a(a2, viewGroup, view, aVar, eVar, arrayList, arrayList2, a3, obj2);
            Object obj3 = a3;
            if (obj3 == null && a4 == null) {
                obj = obj2;
                if (obj == null) {
                    return;
                }
            } else {
                obj = obj2;
            }
            ArrayList<View> a5 = a(nVar, obj, fragment2, arrayList3, view2);
            Object obj4 = (a5 == null || a5.isEmpty()) ? null : obj;
            nVar.a(obj3, view2);
            Object a6 = a(nVar, obj3, obj4, a4, fragment, eVar2.f1590b);
            if (a6 != null) {
                ArrayList arrayList4 = new ArrayList();
                n nVar2 = nVar;
                nVar2.a(a6, obj3, arrayList4, obj4, a5, a4, arrayList2);
                a(nVar2, viewGroup, fragment, view, arrayList2, obj3, arrayList4, obj4, a5);
                ArrayList arrayList5 = arrayList2;
                nVar.a((View) viewGroup, (ArrayList<View>) arrayList5, (Map<String, String>) aVar2);
                nVar.a(viewGroup, a6);
                nVar.a(viewGroup, (ArrayList<View>) arrayList5, (Map<String, String>) aVar2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.n.a(java.util.Map<java.lang.String, android.view.View>, android.view.View):void
     arg types: [b.d.a<java.lang.String, android.view.View>, android.view.View]
     candidates:
      androidx.fragment.app.n.a(java.util.Map<java.lang.String, java.lang.String>, java.lang.String):java.lang.String
      androidx.fragment.app.n.a(java.util.List<android.view.View>, android.view.View):void
      androidx.fragment.app.n.a(android.view.View, android.graphics.Rect):void
      androidx.fragment.app.n.a(android.view.ViewGroup, java.lang.Object):void
      androidx.fragment.app.n.a(java.lang.Object, android.graphics.Rect):void
      androidx.fragment.app.n.a(java.lang.Object, android.view.View):void
      androidx.fragment.app.n.a(java.lang.Object, java.util.ArrayList<android.view.View>):void
      androidx.fragment.app.n.a(java.util.ArrayList<android.view.View>, android.view.View):void
      androidx.fragment.app.n.a(java.util.Map<java.lang.String, android.view.View>, android.view.View):void */
    private static b.d.a<String, View> b(n nVar, b.d.a<String, String> aVar, Object obj, e eVar) {
        n nVar2;
        ArrayList<String> arrayList;
        if (aVar.isEmpty() || obj == null) {
            aVar.clear();
            return null;
        }
        Fragment fragment = eVar.f1592d;
        b.d.a<String, View> aVar2 = new b.d.a<>();
        nVar.a((Map<String, View>) aVar2, fragment.requireView());
        a aVar3 = eVar.f1594f;
        if (eVar.f1593e) {
            nVar2 = fragment.getEnterTransitionCallback();
            arrayList = aVar3.o;
        } else {
            nVar2 = fragment.getExitTransitionCallback();
            arrayList = aVar3.n;
        }
        aVar2.a((Collection<?>) arrayList);
        if (nVar2 != null) {
            nVar2.a(arrayList, aVar2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = (String) arrayList.get(size);
                View view = aVar2.get(str);
                if (view == null) {
                    aVar.remove(str);
                } else if (!str.equals(u.n(view))) {
                    aVar.put(u.n(view), aVar.remove(str));
                }
            }
        } else {
            aVar.a((Collection<?>) aVar2.keySet());
        }
        return aVar2;
    }

    private static void a(n nVar, ViewGroup viewGroup, Fragment fragment, View view, ArrayList<View> arrayList, Object obj, ArrayList<View> arrayList2, Object obj2, ArrayList<View> arrayList3) {
        r.a(viewGroup, new b(obj, nVar, view, fragment, arrayList, arrayList2, arrayList3, obj2));
    }

    private static n a(Fragment fragment, Fragment fragment2) {
        ArrayList arrayList = new ArrayList();
        if (fragment != null) {
            Object exitTransition = fragment.getExitTransition();
            if (exitTransition != null) {
                arrayList.add(exitTransition);
            }
            Object returnTransition = fragment.getReturnTransition();
            if (returnTransition != null) {
                arrayList.add(returnTransition);
            }
            Object sharedElementReturnTransition = fragment.getSharedElementReturnTransition();
            if (sharedElementReturnTransition != null) {
                arrayList.add(sharedElementReturnTransition);
            }
        }
        if (fragment2 != null) {
            Object enterTransition = fragment2.getEnterTransition();
            if (enterTransition != null) {
                arrayList.add(enterTransition);
            }
            Object reenterTransition = fragment2.getReenterTransition();
            if (reenterTransition != null) {
                arrayList.add(reenterTransition);
            }
            Object sharedElementEnterTransition = fragment2.getSharedElementEnterTransition();
            if (sharedElementEnterTransition != null) {
                arrayList.add(sharedElementEnterTransition);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        n nVar = f1560b;
        if (nVar != null && a(nVar, arrayList)) {
            return f1560b;
        }
        n nVar2 = f1561c;
        if (nVar2 != null && a(nVar2, arrayList)) {
            return f1561c;
        }
        if (f1560b == null && f1561c == null) {
            return null;
        }
        throw new IllegalArgumentException("Invalid Transition types");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.l.a(androidx.fragment.app.a, androidx.fragment.app.k$a, android.util.SparseArray<androidx.fragment.app.l$e>, boolean, boolean):void
     arg types: [androidx.fragment.app.a, androidx.fragment.app.k$a, android.util.SparseArray<androidx.fragment.app.l$e>, int, boolean]
     candidates:
      androidx.fragment.app.l.a(int, java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int):b.d.a<java.lang.String, java.lang.String>
      androidx.fragment.app.l.a(androidx.fragment.app.n, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View>
      androidx.fragment.app.l.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a<java.lang.String, android.view.View>, boolean):void
      androidx.fragment.app.l.a(androidx.fragment.app.i, int, androidx.fragment.app.l$e, android.view.View, b.d.a<java.lang.String, java.lang.String>):void
      androidx.fragment.app.l.a(androidx.fragment.app.a, androidx.fragment.app.k$a, android.util.SparseArray<androidx.fragment.app.l$e>, boolean, boolean):void */
    public static void b(a aVar, SparseArray<e> sparseArray, boolean z) {
        if (aVar.r.r.a()) {
            for (int size = aVar.f1540a.size() - 1; size >= 0; size--) {
                a(aVar, aVar.f1540a.get(size), sparseArray, true, z);
            }
        }
    }

    private static boolean a(n nVar, List<Object> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (!nVar.a(list.get(i2))) {
                return false;
            }
        }
        return true;
    }

    private static Object a(n nVar, Fragment fragment, Fragment fragment2, boolean z) {
        Object obj;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        if (z) {
            obj = fragment2.getSharedElementReturnTransition();
        } else {
            obj = fragment.getSharedElementEnterTransition();
        }
        return nVar.c(nVar.b(obj));
    }

    private static Object a(n nVar, Fragment fragment, boolean z) {
        Object obj;
        if (fragment == null) {
            return null;
        }
        if (z) {
            obj = fragment.getReenterTransition();
        } else {
            obj = fragment.getEnterTransition();
        }
        return nVar.b(obj);
    }

    private static void a(ArrayList<View> arrayList, b.d.a<String, View> aVar, Collection<String> collection) {
        for (int size = aVar.size() - 1; size >= 0; size--) {
            View d2 = aVar.d(size);
            if (collection.contains(u.n(d2))) {
                arrayList.add(d2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.l.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a<java.lang.String, android.view.View>, boolean):void
     arg types: [androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a<java.lang.String, android.view.View>, int]
     candidates:
      androidx.fragment.app.l.a(int, java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int):b.d.a<java.lang.String, java.lang.String>
      androidx.fragment.app.l.a(androidx.fragment.app.n, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View>
      androidx.fragment.app.l.a(androidx.fragment.app.a, androidx.fragment.app.k$a, android.util.SparseArray<androidx.fragment.app.l$e>, boolean, boolean):void
      androidx.fragment.app.l.a(androidx.fragment.app.i, int, androidx.fragment.app.l$e, android.view.View, b.d.a<java.lang.String, java.lang.String>):void
      androidx.fragment.app.l.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a<java.lang.String, android.view.View>, boolean):void */
    private static Object a(n nVar, ViewGroup viewGroup, View view, b.d.a<String, String> aVar, e eVar, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        b.d.a<String, String> aVar2;
        Object obj3;
        Object obj4;
        Rect rect;
        n nVar2 = nVar;
        e eVar2 = eVar;
        ArrayList<View> arrayList3 = arrayList;
        Object obj5 = obj;
        Fragment fragment = eVar2.f1589a;
        Fragment fragment2 = eVar2.f1592d;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = eVar2.f1590b;
        if (aVar.isEmpty()) {
            aVar2 = aVar;
            obj3 = null;
        } else {
            obj3 = a(nVar2, fragment, fragment2, z);
            aVar2 = aVar;
        }
        b.d.a<String, View> b2 = b(nVar2, aVar2, obj3, eVar2);
        if (aVar.isEmpty()) {
            obj4 = null;
        } else {
            arrayList3.addAll(b2.values());
            obj4 = obj3;
        }
        if (obj5 == null && obj2 == null && obj4 == null) {
            return null;
        }
        a(fragment, fragment2, z, b2, true);
        if (obj4 != null) {
            rect = new Rect();
            nVar2.b(obj4, view, arrayList3);
            a(nVar, obj4, obj2, b2, eVar2.f1593e, eVar2.f1594f);
            if (obj5 != null) {
                nVar2.a(obj5, rect);
            }
        } else {
            rect = null;
        }
        d dVar = r0;
        d dVar2 = new d(nVar, aVar, obj4, eVar, arrayList2, view, fragment, fragment2, z, arrayList, obj, rect);
        r.a(viewGroup, dVar);
        return obj4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.n.a(java.util.Map<java.lang.String, android.view.View>, android.view.View):void
     arg types: [b.d.a<java.lang.String, android.view.View>, android.view.View]
     candidates:
      androidx.fragment.app.n.a(java.util.Map<java.lang.String, java.lang.String>, java.lang.String):java.lang.String
      androidx.fragment.app.n.a(java.util.List<android.view.View>, android.view.View):void
      androidx.fragment.app.n.a(android.view.View, android.graphics.Rect):void
      androidx.fragment.app.n.a(android.view.ViewGroup, java.lang.Object):void
      androidx.fragment.app.n.a(java.lang.Object, android.graphics.Rect):void
      androidx.fragment.app.n.a(java.lang.Object, android.view.View):void
      androidx.fragment.app.n.a(java.lang.Object, java.util.ArrayList<android.view.View>):void
      androidx.fragment.app.n.a(java.util.ArrayList<android.view.View>, android.view.View):void
      androidx.fragment.app.n.a(java.util.Map<java.lang.String, android.view.View>, android.view.View):void */
    static b.d.a<String, View> a(n nVar, b.d.a<String, String> aVar, Object obj, e eVar) {
        n nVar2;
        ArrayList<String> arrayList;
        String a2;
        Fragment fragment = eVar.f1589a;
        View view = fragment.getView();
        if (aVar.isEmpty() || obj == null || view == null) {
            aVar.clear();
            return null;
        }
        b.d.a<String, View> aVar2 = new b.d.a<>();
        nVar.a((Map<String, View>) aVar2, view);
        a aVar3 = eVar.f1591c;
        if (eVar.f1590b) {
            nVar2 = fragment.getExitTransitionCallback();
            arrayList = aVar3.n;
        } else {
            nVar2 = fragment.getEnterTransitionCallback();
            arrayList = aVar3.o;
        }
        if (arrayList != null) {
            aVar2.a((Collection<?>) arrayList);
            aVar2.a((Collection<?>) aVar.values());
        }
        if (nVar2 != null) {
            nVar2.a(arrayList, aVar2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = (String) arrayList.get(size);
                View view2 = aVar2.get(str);
                if (view2 == null) {
                    String a3 = a(aVar, str);
                    if (a3 != null) {
                        aVar.remove(a3);
                    }
                } else if (!str.equals(u.n(view2)) && (a2 = a(aVar, str)) != null) {
                    aVar.put(a2, u.n(view2));
                }
            }
        } else {
            a(aVar, aVar2);
        }
        return aVar2;
    }

    private static String a(b.d.a<String, String> aVar, String str) {
        int size = aVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (str.equals(aVar.d(i2))) {
                return aVar.b(i2);
            }
        }
        return null;
    }

    static View a(b.d.a<String, View> aVar, e eVar, Object obj, boolean z) {
        ArrayList<String> arrayList;
        String str;
        a aVar2 = eVar.f1591c;
        if (obj == null || aVar == null || (arrayList = aVar2.n) == null || arrayList.isEmpty()) {
            return null;
        }
        if (z) {
            str = aVar2.n.get(0);
        } else {
            str = aVar2.o.get(0);
        }
        return aVar.get(str);
    }

    private static void a(n nVar, Object obj, Object obj2, b.d.a<String, View> aVar, boolean z, a aVar2) {
        String str;
        ArrayList<String> arrayList = aVar2.n;
        if (arrayList != null && !arrayList.isEmpty()) {
            if (z) {
                str = aVar2.o.get(0);
            } else {
                str = aVar2.n.get(0);
            }
            View view = aVar.get(str);
            nVar.c(obj, view);
            if (obj2 != null) {
                nVar.c(obj2, view);
            }
        }
    }

    private static void a(b.d.a<String, String> aVar, b.d.a<String, View> aVar2) {
        for (int size = aVar.size() - 1; size >= 0; size--) {
            if (!aVar2.containsKey(aVar.d(size))) {
                aVar.c(size);
            }
        }
    }

    static void a(Fragment fragment, Fragment fragment2, boolean z, b.d.a<String, View> aVar, boolean z2) {
        n nVar;
        int i2;
        if (z) {
            nVar = fragment2.getEnterTransitionCallback();
        } else {
            nVar = fragment.getEnterTransitionCallback();
        }
        if (nVar != null) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (aVar == null) {
                i2 = 0;
            } else {
                i2 = aVar.size();
            }
            for (int i3 = 0; i3 < i2; i3++) {
                arrayList2.add(aVar.b(i3));
                arrayList.add(aVar.d(i3));
            }
            if (z2) {
                nVar.b(arrayList2, arrayList, null);
            } else {
                nVar.a(arrayList2, arrayList, (List<View>) null);
            }
        }
    }

    static ArrayList<View> a(n nVar, Object obj, Fragment fragment, ArrayList<View> arrayList, View view) {
        if (obj == null) {
            return null;
        }
        ArrayList<View> arrayList2 = new ArrayList<>();
        View view2 = fragment.getView();
        if (view2 != null) {
            nVar.a(arrayList2, view2);
        }
        if (arrayList != null) {
            arrayList2.removeAll(arrayList);
        }
        if (arrayList2.isEmpty()) {
            return arrayList2;
        }
        arrayList2.add(view);
        nVar.a(obj, arrayList2);
        return arrayList2;
    }

    static void a(ArrayList<View> arrayList, int i2) {
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                arrayList.get(size).setVisibility(i2);
            }
        }
    }

    private static Object a(n nVar, Object obj, Object obj2, Object obj3, Fragment fragment, boolean z) {
        boolean z2;
        if (obj == null || obj2 == null || fragment == null) {
            z2 = true;
        } else {
            z2 = z ? fragment.getAllowReturnTransitionOverlap() : fragment.getAllowEnterTransitionOverlap();
        }
        if (z2) {
            return nVar.b(obj2, obj, obj3);
        }
        return nVar.a(obj2, obj, obj3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.l.a(androidx.fragment.app.a, androidx.fragment.app.k$a, android.util.SparseArray<androidx.fragment.app.l$e>, boolean, boolean):void
     arg types: [androidx.fragment.app.a, androidx.fragment.app.k$a, android.util.SparseArray<androidx.fragment.app.l$e>, int, boolean]
     candidates:
      androidx.fragment.app.l.a(int, java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int):b.d.a<java.lang.String, java.lang.String>
      androidx.fragment.app.l.a(androidx.fragment.app.n, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View>
      androidx.fragment.app.l.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, b.d.a<java.lang.String, android.view.View>, boolean):void
      androidx.fragment.app.l.a(androidx.fragment.app.i, int, androidx.fragment.app.l$e, android.view.View, b.d.a<java.lang.String, java.lang.String>):void
      androidx.fragment.app.l.a(androidx.fragment.app.a, androidx.fragment.app.k$a, android.util.SparseArray<androidx.fragment.app.l$e>, boolean, boolean):void */
    public static void a(a aVar, SparseArray<e> sparseArray, boolean z) {
        int size = aVar.f1540a.size();
        for (int i2 = 0; i2 < size; i2++) {
            a(aVar, aVar.f1540a.get(i2), sparseArray, false, z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, b.d.b<androidx.fragment.app.Fragment>):int
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0041, code lost:
        if (r10.mAdded != false) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0076, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0092, code lost:
        if (r10.mHidden == false) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0094, code lost:
        r1 = true;
     */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x00e7 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(androidx.fragment.app.a r16, androidx.fragment.app.k.a r17, android.util.SparseArray<androidx.fragment.app.l.e> r18, boolean r19, boolean r20) {
        /*
            r0 = r16
            r1 = r17
            r2 = r18
            r3 = r19
            androidx.fragment.app.Fragment r10 = r1.f1552b
            if (r10 != 0) goto L_0x000d
            return
        L_0x000d:
            int r11 = r10.mContainerId
            if (r11 != 0) goto L_0x0012
            return
        L_0x0012:
            if (r3 == 0) goto L_0x001b
            int[] r4 = androidx.fragment.app.l.f1559a
            int r1 = r1.f1551a
            r1 = r4[r1]
            goto L_0x001d
        L_0x001b:
            int r1 = r1.f1551a
        L_0x001d:
            r4 = 0
            r5 = 1
            if (r1 == r5) goto L_0x0087
            r6 = 3
            if (r1 == r6) goto L_0x005f
            r6 = 4
            if (r1 == r6) goto L_0x0047
            r6 = 5
            if (r1 == r6) goto L_0x0035
            r6 = 6
            if (r1 == r6) goto L_0x005f
            r6 = 7
            if (r1 == r6) goto L_0x0087
            r1 = 0
        L_0x0031:
            r12 = 0
            r13 = 0
            goto L_0x009a
        L_0x0035:
            if (r20 == 0) goto L_0x0044
            boolean r1 = r10.mHiddenChanged
            if (r1 == 0) goto L_0x0096
            boolean r1 = r10.mHidden
            if (r1 != 0) goto L_0x0096
            boolean r1 = r10.mAdded
            if (r1 == 0) goto L_0x0096
            goto L_0x0094
        L_0x0044:
            boolean r1 = r10.mHidden
            goto L_0x0097
        L_0x0047:
            if (r20 == 0) goto L_0x0056
            boolean r1 = r10.mHiddenChanged
            if (r1 == 0) goto L_0x0078
            boolean r1 = r10.mAdded
            if (r1 == 0) goto L_0x0078
            boolean r1 = r10.mHidden
            if (r1 == 0) goto L_0x0078
        L_0x0055:
            goto L_0x0076
        L_0x0056:
            boolean r1 = r10.mAdded
            if (r1 == 0) goto L_0x0078
            boolean r1 = r10.mHidden
            if (r1 != 0) goto L_0x0078
            goto L_0x0055
        L_0x005f:
            if (r20 == 0) goto L_0x007a
            boolean r1 = r10.mAdded
            if (r1 != 0) goto L_0x0078
            android.view.View r1 = r10.mView
            if (r1 == 0) goto L_0x0078
            int r1 = r1.getVisibility()
            if (r1 != 0) goto L_0x0078
            float r1 = r10.mPostponedAlpha
            r6 = 0
            int r1 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r1 < 0) goto L_0x0078
        L_0x0076:
            r1 = 1
            goto L_0x0083
        L_0x0078:
            r1 = 0
            goto L_0x0083
        L_0x007a:
            boolean r1 = r10.mAdded
            if (r1 == 0) goto L_0x0078
            boolean r1 = r10.mHidden
            if (r1 != 0) goto L_0x0078
            goto L_0x0076
        L_0x0083:
            r13 = r1
            r1 = 0
            r12 = 1
            goto L_0x009a
        L_0x0087:
            if (r20 == 0) goto L_0x008c
            boolean r1 = r10.mIsNewlyAdded
            goto L_0x0097
        L_0x008c:
            boolean r1 = r10.mAdded
            if (r1 != 0) goto L_0x0096
            boolean r1 = r10.mHidden
            if (r1 != 0) goto L_0x0096
        L_0x0094:
            r1 = 1
            goto L_0x0097
        L_0x0096:
            r1 = 0
        L_0x0097:
            r4 = r1
            r1 = 1
            goto L_0x0031
        L_0x009a:
            java.lang.Object r6 = r2.get(r11)
            androidx.fragment.app.l$e r6 = (androidx.fragment.app.l.e) r6
            if (r4 == 0) goto L_0x00ac
            androidx.fragment.app.l$e r6 = a(r6, r2, r11)
            r6.f1589a = r10
            r6.f1590b = r3
            r6.f1591c = r0
        L_0x00ac:
            r14 = r6
            r15 = 0
            if (r20 != 0) goto L_0x00d3
            if (r1 == 0) goto L_0x00d3
            if (r14 == 0) goto L_0x00ba
            androidx.fragment.app.Fragment r1 = r14.f1592d
            if (r1 != r10) goto L_0x00ba
            r14.f1592d = r15
        L_0x00ba:
            androidx.fragment.app.i r4 = r0.r
            int r1 = r10.mState
            if (r1 >= r5) goto L_0x00d3
            int r1 = r4.p
            if (r1 < r5) goto L_0x00d3
            boolean r1 = r0.p
            if (r1 != 0) goto L_0x00d3
            r4.j(r10)
            r6 = 1
            r7 = 0
            r8 = 0
            r9 = 0
            r5 = r10
            r4.a(r5, r6, r7, r8, r9)
        L_0x00d3:
            if (r13 == 0) goto L_0x00e5
            if (r14 == 0) goto L_0x00db
            androidx.fragment.app.Fragment r1 = r14.f1592d
            if (r1 != 0) goto L_0x00e5
        L_0x00db:
            androidx.fragment.app.l$e r14 = a(r14, r2, r11)
            r14.f1592d = r10
            r14.f1593e = r3
            r14.f1594f = r0
        L_0x00e5:
            if (r20 != 0) goto L_0x00f1
            if (r12 == 0) goto L_0x00f1
            if (r14 == 0) goto L_0x00f1
            androidx.fragment.app.Fragment r0 = r14.f1589a
            if (r0 != r10) goto L_0x00f1
            r14.f1589a = r15
        L_0x00f1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.l.a(androidx.fragment.app.a, androidx.fragment.app.k$a, android.util.SparseArray, boolean, boolean):void");
    }

    private static e a(e eVar, SparseArray<e> sparseArray, int i2) {
        if (eVar != null) {
            return eVar;
        }
        e eVar2 = new e();
        sparseArray.put(i2, eVar2);
        return eVar2;
    }
}
