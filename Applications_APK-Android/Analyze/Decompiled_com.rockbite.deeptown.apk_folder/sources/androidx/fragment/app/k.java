package androidx.fragment.app;

import androidx.lifecycle.e;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

/* compiled from: FragmentTransaction */
public abstract class k {

    /* renamed from: a  reason: collision with root package name */
    ArrayList<a> f1540a = new ArrayList<>();

    /* renamed from: b  reason: collision with root package name */
    int f1541b;

    /* renamed from: c  reason: collision with root package name */
    int f1542c;

    /* renamed from: d  reason: collision with root package name */
    int f1543d;

    /* renamed from: e  reason: collision with root package name */
    int f1544e;

    /* renamed from: f  reason: collision with root package name */
    int f1545f;

    /* renamed from: g  reason: collision with root package name */
    int f1546g;

    /* renamed from: h  reason: collision with root package name */
    boolean f1547h;

    /* renamed from: i  reason: collision with root package name */
    String f1548i;

    /* renamed from: j  reason: collision with root package name */
    int f1549j;

    /* renamed from: k  reason: collision with root package name */
    CharSequence f1550k;
    int l;
    CharSequence m;
    ArrayList<String> n;
    ArrayList<String> o;
    boolean p = false;
    ArrayList<Runnable> q;

    /* compiled from: FragmentTransaction */
    static final class a {

        /* renamed from: a  reason: collision with root package name */
        int f1551a;

        /* renamed from: b  reason: collision with root package name */
        Fragment f1552b;

        /* renamed from: c  reason: collision with root package name */
        int f1553c;

        /* renamed from: d  reason: collision with root package name */
        int f1554d;

        /* renamed from: e  reason: collision with root package name */
        int f1555e;

        /* renamed from: f  reason: collision with root package name */
        int f1556f;

        /* renamed from: g  reason: collision with root package name */
        e.b f1557g;

        /* renamed from: h  reason: collision with root package name */
        e.b f1558h;

        a() {
        }

        a(int i2, Fragment fragment) {
            this.f1551a = i2;
            this.f1552b = fragment;
            e.b bVar = e.b.RESUMED;
            this.f1557g = bVar;
            this.f1558h = bVar;
        }
    }

    public abstract int a();

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        this.f1540a.add(aVar);
        aVar.f1553c = this.f1541b;
        aVar.f1554d = this.f1542c;
        aVar.f1555e = this.f1543d;
        aVar.f1556f = this.f1544e;
    }

    public abstract int b();

    public abstract void c();

    public k d() {
        if (!this.f1547h) {
            return this;
        }
        throw new IllegalStateException("This transaction is already being added to the back stack");
    }

    public k a(Fragment fragment, String str) {
        a(0, fragment, str, 1);
        return this;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, Fragment fragment, String str, int i3) {
        Class<?> cls = fragment.getClass();
        int modifiers = cls.getModifiers();
        if (cls.isAnonymousClass() || !Modifier.isPublic(modifiers) || (cls.isMemberClass() && !Modifier.isStatic(modifiers))) {
            throw new IllegalStateException("Fragment " + cls.getCanonicalName() + " must be a public static class to be  properly recreated from instance state.");
        }
        if (str != null) {
            String str2 = fragment.mTag;
            if (str2 == null || str.equals(str2)) {
                fragment.mTag = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.mTag + " now " + str);
            }
        }
        if (i2 != 0) {
            if (i2 != -1) {
                int i4 = fragment.mFragmentId;
                if (i4 == 0 || i4 == i2) {
                    fragment.mFragmentId = i2;
                    fragment.mContainerId = i2;
                } else {
                    throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.mFragmentId + " now " + i2);
                }
            } else {
                throw new IllegalArgumentException("Can't add fragment " + fragment + " with tag " + str + " to container view with no id");
            }
        }
        a(new a(i3, fragment));
    }

    public k a(int i2, Fragment fragment) {
        a(i2, fragment, null);
        return this;
    }

    public k a(int i2, Fragment fragment, String str) {
        if (i2 != 0) {
            a(i2, fragment, str, 2);
            return this;
        }
        throw new IllegalArgumentException("Must use non-zero containerViewId");
    }

    public k a(Fragment fragment) {
        a(new a(3, fragment));
        return this;
    }
}
