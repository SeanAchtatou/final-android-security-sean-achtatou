package androidx.fragment.app;

import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.h;
import androidx.fragment.app.i;
import androidx.fragment.app.k;
import b.e.l.b;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import java.io.PrintWriter;
import java.util.ArrayList;

/* compiled from: BackStackRecord */
final class a extends k implements h.a, i.k {
    final i r;
    boolean s;
    int t = -1;

    public a(i iVar) {
        this.r = iVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.a.a(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      androidx.fragment.app.a.a(java.util.ArrayList<androidx.fragment.app.a>, int, int):boolean
      androidx.fragment.app.k.a(int, androidx.fragment.app.Fragment, java.lang.String):androidx.fragment.app.k
      androidx.fragment.app.a.a(java.lang.String, java.io.PrintWriter, boolean):void */
    public void a(String str, PrintWriter printWriter) {
        a(str, printWriter, true);
    }

    public int b() {
        return a(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.b(androidx.fragment.app.i$k, boolean):void
     arg types: [androidx.fragment.app.a, int]
     candidates:
      androidx.fragment.app.i.b(int, boolean):int
      androidx.fragment.app.i.b(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):boolean
      androidx.fragment.app.i.b(androidx.fragment.app.Fragment, boolean):void
      androidx.fragment.app.i.b(androidx.fragment.app.i$k, boolean):void */
    public void c() {
        d();
        this.r.b((i.k) this, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      androidx.fragment.app.i.a(float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):void
      androidx.fragment.app.i.a(android.os.Bundle, java.lang.String):androidx.fragment.app.Fragment
      androidx.fragment.app.i.a(int, int):void
      androidx.fragment.app.i.a(int, androidx.fragment.app.a):void
      androidx.fragment.app.i.a(int, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, androidx.lifecycle.e$b):void
      androidx.fragment.app.i.a(androidx.fragment.app.i$k, boolean):void
      androidx.fragment.app.i.a(android.view.Menu, android.view.MenuInflater):boolean
      androidx.fragment.app.h.a(int, int):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(int, boolean):void
     arg types: [int, int]
     candidates:
      androidx.fragment.app.i.a(float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):void
      androidx.fragment.app.i.a(android.os.Bundle, java.lang.String):androidx.fragment.app.Fragment
      androidx.fragment.app.i.a(int, int):void
      androidx.fragment.app.i.a(int, androidx.fragment.app.a):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, androidx.lifecycle.e$b):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.i$k, boolean):void
      androidx.fragment.app.i.a(android.view.Menu, android.view.MenuInflater):boolean
      androidx.fragment.app.h.a(int, int):void
      androidx.fragment.app.i.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public void e() {
        int size = this.f1540a.size();
        for (int i2 = 0; i2 < size; i2++) {
            k.a aVar = this.f1540a.get(i2);
            Fragment fragment = aVar.f1552b;
            if (fragment != null) {
                fragment.setNextTransition(this.f1545f, this.f1546g);
            }
            switch (aVar.f1551a) {
                case 1:
                    fragment.setNextAnim(aVar.f1553c);
                    this.r.a(fragment, false);
                    break;
                case 2:
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.f1551a);
                case 3:
                    fragment.setNextAnim(aVar.f1554d);
                    this.r.o(fragment);
                    break;
                case 4:
                    fragment.setNextAnim(aVar.f1554d);
                    this.r.h(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(aVar.f1553c);
                    this.r.t(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(aVar.f1554d);
                    this.r.d(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(aVar.f1553c);
                    this.r.b(fragment);
                    break;
                case 8:
                    this.r.s(fragment);
                    break;
                case 9:
                    this.r.s(null);
                    break;
                case 10:
                    this.r.a(fragment, aVar.f1558h);
                    break;
            }
            if (!(this.p || aVar.f1551a == 1 || fragment == null)) {
                this.r.l(fragment);
            }
        }
        if (!this.p) {
            i iVar = this.r;
            iVar.a(iVar.p, true);
        }
    }

    public String f() {
        return this.f1548i;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        for (int i2 = 0; i2 < this.f1540a.size(); i2++) {
            if (b(this.f1540a.get(i2))) {
                return true;
            }
        }
        return false;
    }

    public void h() {
        if (this.q != null) {
            for (int i2 = 0; i2 < this.q.size(); i2++) {
                this.q.get(i2).run();
            }
            this.q = null;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.t >= 0) {
            sb.append(" #");
            sb.append(this.t);
        }
        if (this.f1548i != null) {
            sb.append(RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER);
            sb.append(this.f1548i);
        }
        sb.append("}");
        return sb.toString();
    }

    public void a(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.f1548i);
            printWriter.print(" mIndex=");
            printWriter.print(this.t);
            printWriter.print(" mCommitted=");
            printWriter.println(this.s);
            if (this.f1545f != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.f1545f));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(this.f1546g));
            }
            if (!(this.f1541b == 0 && this.f1542c == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.f1541b));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.f1542c));
            }
            if (!(this.f1543d == 0 && this.f1544e == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.f1543d));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.f1544e));
            }
            if (!(this.f1549j == 0 && this.f1550k == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.f1549j));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.f1550k);
            }
            if (!(this.l == 0 && this.m == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.l));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.m);
            }
        }
        if (!this.f1540a.isEmpty()) {
            printWriter.print(str);
            printWriter.println("Operations:");
            int size = this.f1540a.size();
            for (int i2 = 0; i2 < size; i2++) {
                k.a aVar = this.f1540a.get(i2);
                switch (aVar.f1551a) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    case 8:
                        str2 = "SET_PRIMARY_NAV";
                        break;
                    case 9:
                        str2 = "UNSET_PRIMARY_NAV";
                        break;
                    case 10:
                        str2 = "OP_SET_MAX_LIFECYCLE";
                        break;
                    default:
                        str2 = "cmd=" + aVar.f1551a;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER);
                printWriter.println(aVar.f1552b);
                if (z) {
                    if (!(aVar.f1553c == 0 && aVar.f1554d == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.f1553c));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.f1554d));
                    }
                    if (aVar.f1555e != 0 || aVar.f1556f != 0) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.f1555e));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.f1556f));
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i2) {
        int size = this.f1540a.size();
        for (int i3 = 0; i3 < size; i3++) {
            Fragment fragment = this.f1540a.get(i3).f1552b;
            int i4 = fragment != null ? fragment.mContainerId : 0;
            if (i4 != 0 && i4 == i2) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      androidx.fragment.app.i.a(float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):void
      androidx.fragment.app.i.a(android.os.Bundle, java.lang.String):androidx.fragment.app.Fragment
      androidx.fragment.app.i.a(int, int):void
      androidx.fragment.app.i.a(int, androidx.fragment.app.a):void
      androidx.fragment.app.i.a(int, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, androidx.lifecycle.e$b):void
      androidx.fragment.app.i.a(androidx.fragment.app.i$k, boolean):void
      androidx.fragment.app.i.a(android.view.Menu, android.view.MenuInflater):boolean
      androidx.fragment.app.h.a(int, int):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(int, boolean):void
     arg types: [int, int]
     candidates:
      androidx.fragment.app.i.a(float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):void
      androidx.fragment.app.i.a(android.os.Bundle, java.lang.String):androidx.fragment.app.Fragment
      androidx.fragment.app.i.a(int, int):void
      androidx.fragment.app.i.a(int, androidx.fragment.app.a):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, androidx.lifecycle.e$b):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.i$k, boolean):void
      androidx.fragment.app.i.a(android.view.Menu, android.view.MenuInflater):boolean
      androidx.fragment.app.h.a(int, int):void
      androidx.fragment.app.i.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        for (int size = this.f1540a.size() - 1; size >= 0; size--) {
            k.a aVar = this.f1540a.get(size);
            Fragment fragment = aVar.f1552b;
            if (fragment != null) {
                fragment.setNextTransition(i.e(this.f1545f), this.f1546g);
            }
            switch (aVar.f1551a) {
                case 1:
                    fragment.setNextAnim(aVar.f1556f);
                    this.r.o(fragment);
                    break;
                case 2:
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.f1551a);
                case 3:
                    fragment.setNextAnim(aVar.f1555e);
                    this.r.a(fragment, false);
                    break;
                case 4:
                    fragment.setNextAnim(aVar.f1555e);
                    this.r.t(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(aVar.f1556f);
                    this.r.h(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(aVar.f1555e);
                    this.r.b(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(aVar.f1556f);
                    this.r.d(fragment);
                    break;
                case 8:
                    this.r.s(null);
                    break;
                case 9:
                    this.r.s(fragment);
                    break;
                case 10:
                    this.r.a(fragment, aVar.f1557g);
                    break;
            }
            if (!(this.p || aVar.f1551a == 3 || fragment == null)) {
                this.r.l(fragment);
            }
        }
        if (!this.p && z) {
            i iVar = this.r;
            iVar.a(iVar.p, true);
        }
    }

    /* access modifiers changed from: package-private */
    public Fragment b(ArrayList<Fragment> arrayList, Fragment fragment) {
        for (int size = this.f1540a.size() - 1; size >= 0; size--) {
            k.a aVar = this.f1540a.get(size);
            int i2 = aVar.f1551a;
            if (i2 != 1) {
                if (i2 != 3) {
                    switch (i2) {
                        case 8:
                            fragment = null;
                            break;
                        case 9:
                            fragment = aVar.f1552b;
                            break;
                        case 10:
                            aVar.f1558h = aVar.f1557g;
                            break;
                    }
                }
                arrayList.add(aVar.f1552b);
            }
            arrayList.remove(aVar.f1552b);
        }
        return fragment;
    }

    private static boolean b(k.a aVar) {
        Fragment fragment = aVar.f1552b;
        return fragment != null && fragment.mAdded && fragment.mView != null && !fragment.mDetached && !fragment.mHidden && fragment.isPostponed();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, Fragment fragment, String str, int i3) {
        super.a(i2, fragment, str, i3);
        fragment.mFragmentManager = this.r;
    }

    public k a(Fragment fragment) {
        i iVar = fragment.mFragmentManager;
        if (iVar == null || iVar == this.r) {
            super.a(fragment);
            return this;
        }
        throw new IllegalStateException("Cannot remove Fragment attached to a different FragmentManager. Fragment " + fragment.toString() + " is already attached to a FragmentManager.");
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (this.f1547h) {
            if (i.H) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i2);
            }
            int size = this.f1540a.size();
            for (int i3 = 0; i3 < size; i3++) {
                k.a aVar = this.f1540a.get(i3);
                Fragment fragment = aVar.f1552b;
                if (fragment != null) {
                    fragment.mBackStackNesting += i2;
                    if (i.H) {
                        Log.v("FragmentManager", "Bump nesting of " + aVar.f1552b + " to " + aVar.f1552b.mBackStackNesting);
                    }
                }
            }
        }
    }

    public int a() {
        return a(false);
    }

    /* access modifiers changed from: package-private */
    public int a(boolean z) {
        if (!this.s) {
            if (i.H) {
                Log.v("FragmentManager", "Commit: " + this);
                PrintWriter printWriter = new PrintWriter(new b("FragmentManager"));
                a("  ", printWriter);
                printWriter.close();
            }
            this.s = true;
            if (this.f1547h) {
                this.t = this.r.b(this);
            } else {
                this.t = -1;
            }
            this.r.a(this, z);
            return this.t;
        }
        throw new IllegalStateException("commit already called");
    }

    public boolean a(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2) {
        if (i.H) {
            Log.v("FragmentManager", "Run: " + this);
        }
        arrayList.add(this);
        arrayList2.add(false);
        if (!this.f1547h) {
            return true;
        }
        this.r.a(this);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(ArrayList<a> arrayList, int i2, int i3) {
        if (i3 == i2) {
            return false;
        }
        int size = this.f1540a.size();
        int i4 = -1;
        for (int i5 = 0; i5 < size; i5++) {
            Fragment fragment = this.f1540a.get(i5).f1552b;
            int i6 = fragment != null ? fragment.mContainerId : 0;
            if (!(i6 == 0 || i6 == i4)) {
                for (int i7 = i2; i7 < i3; i7++) {
                    a aVar = arrayList.get(i7);
                    int size2 = aVar.f1540a.size();
                    for (int i8 = 0; i8 < size2; i8++) {
                        Fragment fragment2 = aVar.f1540a.get(i8).f1552b;
                        if ((fragment2 != null ? fragment2.mContainerId : 0) == i6) {
                            return true;
                        }
                    }
                }
                i4 = i6;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public Fragment a(ArrayList<Fragment> arrayList, Fragment fragment) {
        ArrayList<Fragment> arrayList2 = arrayList;
        Fragment fragment2 = fragment;
        int i2 = 0;
        while (i2 < this.f1540a.size()) {
            k.a aVar = this.f1540a.get(i2);
            int i3 = aVar.f1551a;
            if (i3 != 1) {
                if (i3 == 2) {
                    Fragment fragment3 = aVar.f1552b;
                    int i4 = fragment3.mContainerId;
                    Fragment fragment4 = fragment2;
                    int i5 = i2;
                    boolean z = false;
                    for (int size = arrayList.size() - 1; size >= 0; size--) {
                        Fragment fragment5 = arrayList2.get(size);
                        if (fragment5.mContainerId == i4) {
                            if (fragment5 == fragment3) {
                                z = true;
                            } else {
                                if (fragment5 == fragment4) {
                                    this.f1540a.add(i5, new k.a(9, fragment5));
                                    i5++;
                                    fragment4 = null;
                                }
                                k.a aVar2 = new k.a(3, fragment5);
                                aVar2.f1553c = aVar.f1553c;
                                aVar2.f1555e = aVar.f1555e;
                                aVar2.f1554d = aVar.f1554d;
                                aVar2.f1556f = aVar.f1556f;
                                this.f1540a.add(i5, aVar2);
                                arrayList2.remove(fragment5);
                                i5++;
                            }
                        }
                    }
                    if (z) {
                        this.f1540a.remove(i5);
                        i5--;
                    } else {
                        aVar.f1551a = 1;
                        arrayList2.add(fragment3);
                    }
                    i2 = i5;
                    fragment2 = fragment4;
                } else if (i3 == 3 || i3 == 6) {
                    arrayList2.remove(aVar.f1552b);
                    Fragment fragment6 = aVar.f1552b;
                    if (fragment6 == fragment2) {
                        this.f1540a.add(i2, new k.a(9, fragment6));
                        i2++;
                        fragment2 = null;
                    }
                } else if (i3 != 7) {
                    if (i3 == 8) {
                        this.f1540a.add(i2, new k.a(9, fragment2));
                        i2++;
                        fragment2 = aVar.f1552b;
                    }
                }
                i2++;
            }
            arrayList2.add(aVar.f1552b);
            i2++;
        }
        return fragment2;
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment.f fVar) {
        for (int i2 = 0; i2 < this.f1540a.size(); i2++) {
            k.a aVar = this.f1540a.get(i2);
            if (b(aVar)) {
                aVar.f1552b.setOnStartEnterTransitionListener(fVar);
            }
        }
    }
}
