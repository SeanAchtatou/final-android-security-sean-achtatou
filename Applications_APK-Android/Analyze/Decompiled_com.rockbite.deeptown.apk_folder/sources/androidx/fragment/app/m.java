package androidx.fragment.app;

import android.graphics.Rect;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

/* compiled from: FragmentTransitionCompat21 */
class m extends n {

    /* compiled from: FragmentTransitionCompat21 */
    class a extends Transition.EpicenterCallback {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Rect f1595a;

        a(m mVar, Rect rect) {
            this.f1595a = rect;
        }

        public Rect onGetEpicenter(Transition transition) {
            return this.f1595a;
        }
    }

    /* compiled from: FragmentTransitionCompat21 */
    class b implements Transition.TransitionListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ View f1596a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ ArrayList f1597b;

        b(m mVar, View view, ArrayList arrayList) {
            this.f1596a = view;
            this.f1597b = arrayList;
        }

        public void onTransitionCancel(Transition transition) {
        }

        public void onTransitionEnd(Transition transition) {
            transition.removeListener(this);
            this.f1596a.setVisibility(8);
            int size = this.f1597b.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((View) this.f1597b.get(i2)).setVisibility(0);
            }
        }

        public void onTransitionPause(Transition transition) {
        }

        public void onTransitionResume(Transition transition) {
        }

        public void onTransitionStart(Transition transition) {
        }
    }

    /* compiled from: FragmentTransitionCompat21 */
    class c implements Transition.TransitionListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Object f1598a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ ArrayList f1599b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ Object f1600c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ ArrayList f1601d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ Object f1602e;

        /* renamed from: f  reason: collision with root package name */
        final /* synthetic */ ArrayList f1603f;

        c(Object obj, ArrayList arrayList, Object obj2, ArrayList arrayList2, Object obj3, ArrayList arrayList3) {
            this.f1598a = obj;
            this.f1599b = arrayList;
            this.f1600c = obj2;
            this.f1601d = arrayList2;
            this.f1602e = obj3;
            this.f1603f = arrayList3;
        }

        public void onTransitionCancel(Transition transition) {
        }

        public void onTransitionEnd(Transition transition) {
            transition.removeListener(this);
        }

        public void onTransitionPause(Transition transition) {
        }

        public void onTransitionResume(Transition transition) {
        }

        public void onTransitionStart(Transition transition) {
            Object obj = this.f1598a;
            if (obj != null) {
                m.this.a(obj, (ArrayList<View>) this.f1599b, (ArrayList<View>) null);
            }
            Object obj2 = this.f1600c;
            if (obj2 != null) {
                m.this.a(obj2, (ArrayList<View>) this.f1601d, (ArrayList<View>) null);
            }
            Object obj3 = this.f1602e;
            if (obj3 != null) {
                m.this.a(obj3, (ArrayList<View>) this.f1603f, (ArrayList<View>) null);
            }
        }
    }

    /* compiled from: FragmentTransitionCompat21 */
    class d extends Transition.EpicenterCallback {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Rect f1605a;

        d(m mVar, Rect rect) {
            this.f1605a = rect;
        }

        public Rect onGetEpicenter(Transition transition) {
            Rect rect = this.f1605a;
            if (rect == null || rect.isEmpty()) {
                return null;
            }
            return this.f1605a;
        }
    }

    m() {
    }

    public boolean a(Object obj) {
        return obj instanceof Transition;
    }

    public Object b(Object obj) {
        if (obj != null) {
            return ((Transition) obj).clone();
        }
        return null;
    }

    public Object c(Object obj) {
        if (obj == null) {
            return null;
        }
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.addTransition((Transition) obj);
        return transitionSet;
    }

    public void a(Object obj, ArrayList<View> arrayList) {
        Transition transition = (Transition) obj;
        if (transition != null) {
            int i2 = 0;
            if (transition instanceof TransitionSet) {
                TransitionSet transitionSet = (TransitionSet) transition;
                int transitionCount = transitionSet.getTransitionCount();
                while (i2 < transitionCount) {
                    a(transitionSet.getTransitionAt(i2), arrayList);
                    i2++;
                }
            } else if (!a(transition) && n.a((List) transition.getTargets())) {
                int size = arrayList.size();
                while (i2 < size) {
                    transition.addTarget(arrayList.get(i2));
                    i2++;
                }
            }
        }
    }

    public void b(Object obj, View view, ArrayList<View> arrayList) {
        TransitionSet transitionSet = (TransitionSet) obj;
        List<View> targets = transitionSet.getTargets();
        targets.clear();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            n.a(targets, arrayList.get(i2));
        }
        targets.add(view);
        arrayList.add(view);
        a(transitionSet, arrayList);
    }

    public void c(Object obj, View view) {
        if (view != null) {
            Rect rect = new Rect();
            a(view, rect);
            ((Transition) obj).setEpicenterCallback(new a(this, rect));
        }
    }

    public Object b(Object obj, Object obj2, Object obj3) {
        TransitionSet transitionSet = new TransitionSet();
        if (obj != null) {
            transitionSet.addTransition((Transition) obj);
        }
        if (obj2 != null) {
            transitionSet.addTransition((Transition) obj2);
        }
        if (obj3 != null) {
            transitionSet.addTransition((Transition) obj3);
        }
        return transitionSet;
    }

    private static boolean a(Transition transition) {
        return !n.a(transition.getTargetIds()) || !n.a(transition.getTargetNames()) || !n.a(transition.getTargetTypes());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.m.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void
     arg types: [android.transition.TransitionSet, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>]
     candidates:
      androidx.fragment.app.m.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      androidx.fragment.app.m.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      androidx.fragment.app.n.a(java.util.List<android.view.View>, android.view.View, int):boolean
      androidx.fragment.app.n.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      androidx.fragment.app.n.a(android.view.View, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
      androidx.fragment.app.n.a(android.view.ViewGroup, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
      androidx.fragment.app.n.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      androidx.fragment.app.m.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void */
    public void b(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        TransitionSet transitionSet = (TransitionSet) obj;
        if (transitionSet != null) {
            transitionSet.getTargets().clear();
            transitionSet.getTargets().addAll(arrayList2);
            a((Object) transitionSet, arrayList, arrayList2);
        }
    }

    public void a(Object obj, View view, ArrayList<View> arrayList) {
        ((Transition) obj).addListener(new b(this, view, arrayList));
    }

    public Object a(Object obj, Object obj2, Object obj3) {
        Transition transition = (Transition) obj;
        Transition transition2 = (Transition) obj2;
        Transition transition3 = (Transition) obj3;
        if (transition != null && transition2 != null) {
            transition = new TransitionSet().addTransition(transition).addTransition(transition2).setOrdering(1);
        } else if (transition == null) {
            transition = transition2 != null ? transition2 : null;
        }
        if (transition3 == null) {
            return transition;
        }
        TransitionSet transitionSet = new TransitionSet();
        if (transition != null) {
            transitionSet.addTransition(transition);
        }
        transitionSet.addTransition(transition3);
        return transitionSet;
    }

    public void b(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).removeTarget(view);
        }
    }

    public void a(ViewGroup viewGroup, Object obj) {
        TransitionManager.beginDelayedTransition(viewGroup, (Transition) obj);
    }

    public void a(Object obj, Object obj2, ArrayList<View> arrayList, Object obj3, ArrayList<View> arrayList2, Object obj4, ArrayList<View> arrayList3) {
        ((Transition) obj).addListener(new c(obj2, arrayList, obj3, arrayList2, obj4, arrayList3));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.m.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void
     arg types: [android.transition.Transition, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>]
     candidates:
      androidx.fragment.app.m.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      androidx.fragment.app.m.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      androidx.fragment.app.n.a(java.util.List<android.view.View>, android.view.View, int):boolean
      androidx.fragment.app.n.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      androidx.fragment.app.n.a(android.view.View, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
      androidx.fragment.app.n.a(android.view.ViewGroup, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
      androidx.fragment.app.n.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      androidx.fragment.app.m.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void */
    public void a(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        List<View> targets;
        int i2;
        Transition transition = (Transition) obj;
        int i3 = 0;
        if (transition instanceof TransitionSet) {
            TransitionSet transitionSet = (TransitionSet) transition;
            int transitionCount = transitionSet.getTransitionCount();
            while (i3 < transitionCount) {
                a((Object) transitionSet.getTransitionAt(i3), arrayList, arrayList2);
                i3++;
            }
        } else if (!a(transition) && (targets = transition.getTargets()) != null && targets.size() == arrayList.size() && targets.containsAll(arrayList)) {
            if (arrayList2 == null) {
                i2 = 0;
            } else {
                i2 = arrayList2.size();
            }
            while (i3 < i2) {
                transition.addTarget(arrayList2.get(i3));
                i3++;
            }
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                transition.removeTarget(arrayList.get(size));
            }
        }
    }

    public void a(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).addTarget(view);
        }
    }

    public void a(Object obj, Rect rect) {
        if (obj != null) {
            ((Transition) obj).setEpicenterCallback(new d(this, rect));
        }
    }
}
