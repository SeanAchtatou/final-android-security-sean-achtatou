package androidx.fragment.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import androidx.core.app.a;
import b.e.l.f;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: FragmentHostCallback */
public abstract class g<E> extends d {

    /* renamed from: a  reason: collision with root package name */
    private final Activity f1486a;

    /* renamed from: b  reason: collision with root package name */
    private final Context f1487b;

    /* renamed from: c  reason: collision with root package name */
    private final Handler f1488c;

    /* renamed from: d  reason: collision with root package name */
    private final int f1489d;

    /* renamed from: e  reason: collision with root package name */
    final i f1490e;

    g(c cVar) {
        this(cVar, cVar, new Handler(), 0);
    }

    public View a(int i2) {
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment) {
    }

    public void a(Fragment fragment, @SuppressLint({"UnknownNullness"}) Intent intent, int i2, Bundle bundle) {
        if (i2 == -1) {
            this.f1487b.startActivity(intent);
            return;
        }
        throw new IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
    }

    public void a(Fragment fragment, String[] strArr, int i2) {
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    public boolean a() {
        return true;
    }

    public boolean a(String str) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public Activity b() {
        return this.f1486a;
    }

    public boolean b(Fragment fragment) {
        return true;
    }

    /* access modifiers changed from: package-private */
    public Context c() {
        return this.f1487b;
    }

    /* access modifiers changed from: package-private */
    public Handler d() {
        return this.f1488c;
    }

    public abstract E e();

    public LayoutInflater f() {
        return LayoutInflater.from(this.f1487b);
    }

    public int g() {
        return this.f1489d;
    }

    public boolean h() {
        return true;
    }

    public void i() {
    }

    g(Activity activity, Context context, Handler handler, int i2) {
        this.f1490e = new i();
        this.f1486a = activity;
        f.a(context, "context == null");
        this.f1487b = context;
        f.a(handler, "handler == null");
        this.f1488c = handler;
        this.f1489d = i2;
    }

    public void a(Fragment fragment, @SuppressLint({"UnknownNullness"}) IntentSender intentSender, int i2, Intent intent, int i3, int i4, int i5, Bundle bundle) throws IntentSender.SendIntentException {
        if (i2 == -1) {
            a.a(this.f1486a, intentSender, i2, intent, i3, i4, i5, bundle);
            return;
        }
        throw new IllegalStateException("Starting intent sender with a requestCode requires a FragmentActivity host");
    }
}
