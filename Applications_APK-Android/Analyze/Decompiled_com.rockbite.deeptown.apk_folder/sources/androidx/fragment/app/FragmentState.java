package androidx.fragment.app;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import androidx.lifecycle.e;

@SuppressLint({"BanParcelableUsage"})
final class FragmentState implements Parcelable {
    public static final Parcelable.Creator<FragmentState> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    final String f1471a;

    /* renamed from: b  reason: collision with root package name */
    final String f1472b;

    /* renamed from: c  reason: collision with root package name */
    final boolean f1473c;

    /* renamed from: d  reason: collision with root package name */
    final int f1474d;

    /* renamed from: e  reason: collision with root package name */
    final int f1475e;

    /* renamed from: f  reason: collision with root package name */
    final String f1476f;

    /* renamed from: g  reason: collision with root package name */
    final boolean f1477g;

    /* renamed from: h  reason: collision with root package name */
    final boolean f1478h;

    /* renamed from: i  reason: collision with root package name */
    final boolean f1479i;

    /* renamed from: j  reason: collision with root package name */
    final Bundle f1480j;

    /* renamed from: k  reason: collision with root package name */
    final boolean f1481k;
    final int l;
    Bundle m;
    Fragment n;

    static class a implements Parcelable.Creator<FragmentState> {
        a() {
        }

        public FragmentState createFromParcel(Parcel parcel) {
            return new FragmentState(parcel);
        }

        public FragmentState[] newArray(int i2) {
            return new FragmentState[i2];
        }
    }

    FragmentState(Fragment fragment) {
        this.f1471a = fragment.getClass().getName();
        this.f1472b = fragment.mWho;
        this.f1473c = fragment.mFromLayout;
        this.f1474d = fragment.mFragmentId;
        this.f1475e = fragment.mContainerId;
        this.f1476f = fragment.mTag;
        this.f1477g = fragment.mRetainInstance;
        this.f1478h = fragment.mRemoving;
        this.f1479i = fragment.mDetached;
        this.f1480j = fragment.mArguments;
        this.f1481k = fragment.mHidden;
        this.l = fragment.mMaxState.ordinal();
    }

    public Fragment a(ClassLoader classLoader, f fVar) {
        if (this.n == null) {
            Bundle bundle = this.f1480j;
            if (bundle != null) {
                bundle.setClassLoader(classLoader);
            }
            this.n = fVar.a(classLoader, this.f1471a);
            this.n.setArguments(this.f1480j);
            Bundle bundle2 = this.m;
            if (bundle2 != null) {
                bundle2.setClassLoader(classLoader);
                this.n.mSavedFragmentState = this.m;
            } else {
                this.n.mSavedFragmentState = new Bundle();
            }
            Fragment fragment = this.n;
            fragment.mWho = this.f1472b;
            fragment.mFromLayout = this.f1473c;
            fragment.mRestored = true;
            fragment.mFragmentId = this.f1474d;
            fragment.mContainerId = this.f1475e;
            fragment.mTag = this.f1476f;
            fragment.mRetainInstance = this.f1477g;
            fragment.mRemoving = this.f1478h;
            fragment.mDetached = this.f1479i;
            fragment.mHidden = this.f1481k;
            fragment.mMaxState = e.b.values()[this.l];
            if (i.H) {
                Log.v("FragmentManager", "Instantiated fragment " + this.n);
            }
        }
        return this.n;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentState{");
        sb.append(this.f1471a);
        sb.append(" (");
        sb.append(this.f1472b);
        sb.append(")}:");
        if (this.f1473c) {
            sb.append(" fromLayout");
        }
        if (this.f1475e != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(this.f1475e));
        }
        String str = this.f1476f;
        if (str != null && !str.isEmpty()) {
            sb.append(" tag=");
            sb.append(this.f1476f);
        }
        if (this.f1477g) {
            sb.append(" retainInstance");
        }
        if (this.f1478h) {
            sb.append(" removing");
        }
        if (this.f1479i) {
            sb.append(" detached");
        }
        if (this.f1481k) {
            sb.append(" hidden");
        }
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.f1471a);
        parcel.writeString(this.f1472b);
        parcel.writeInt(this.f1473c ? 1 : 0);
        parcel.writeInt(this.f1474d);
        parcel.writeInt(this.f1475e);
        parcel.writeString(this.f1476f);
        parcel.writeInt(this.f1477g ? 1 : 0);
        parcel.writeInt(this.f1478h ? 1 : 0);
        parcel.writeInt(this.f1479i ? 1 : 0);
        parcel.writeBundle(this.f1480j);
        parcel.writeInt(this.f1481k ? 1 : 0);
        parcel.writeBundle(this.m);
        parcel.writeInt(this.l);
    }

    FragmentState(Parcel parcel) {
        this.f1471a = parcel.readString();
        this.f1472b = parcel.readString();
        boolean z = true;
        this.f1473c = parcel.readInt() != 0;
        this.f1474d = parcel.readInt();
        this.f1475e = parcel.readInt();
        this.f1476f = parcel.readString();
        this.f1477g = parcel.readInt() != 0;
        this.f1478h = parcel.readInt() != 0;
        this.f1479i = parcel.readInt() != 0;
        this.f1480j = parcel.readBundle();
        this.f1481k = parcel.readInt() == 0 ? false : z;
        this.m = parcel.readBundle();
        this.l = parcel.readInt();
    }
}
