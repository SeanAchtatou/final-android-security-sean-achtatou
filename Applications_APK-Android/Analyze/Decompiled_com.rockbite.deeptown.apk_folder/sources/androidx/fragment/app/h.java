package androidx.fragment.app;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;

/* compiled from: FragmentManager */
public abstract class h {

    /* renamed from: b  reason: collision with root package name */
    static final f f1491b = new f();

    /* renamed from: a  reason: collision with root package name */
    private f f1492a = null;

    /* compiled from: FragmentManager */
    public interface a {
    }

    /* compiled from: FragmentManager */
    public static abstract class b {
        public abstract void a(h hVar, Fragment fragment);

        public abstract void a(h hVar, Fragment fragment, Context context);

        public abstract void a(h hVar, Fragment fragment, Bundle bundle);

        public abstract void a(h hVar, Fragment fragment, View view, Bundle bundle);

        public abstract void b(h hVar, Fragment fragment);

        public abstract void b(h hVar, Fragment fragment, Context context);

        public abstract void b(h hVar, Fragment fragment, Bundle bundle);

        public abstract void c(h hVar, Fragment fragment);

        public abstract void c(h hVar, Fragment fragment, Bundle bundle);

        public abstract void d(h hVar, Fragment fragment);

        public abstract void d(h hVar, Fragment fragment, Bundle bundle);

        public abstract void e(h hVar, Fragment fragment);

        public abstract void f(h hVar, Fragment fragment);

        public abstract void g(h hVar, Fragment fragment);
    }

    /* compiled from: FragmentManager */
    public interface c {
        void a();
    }

    public abstract Fragment a(String str);

    public abstract k a();

    public abstract void a(int i2, int i3);

    public void a(f fVar) {
        this.f1492a = fVar;
    }

    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    public f b() {
        if (this.f1492a == null) {
            this.f1492a = f1491b;
        }
        return this.f1492a;
    }

    public abstract List<Fragment> c();

    public abstract boolean d();
}
