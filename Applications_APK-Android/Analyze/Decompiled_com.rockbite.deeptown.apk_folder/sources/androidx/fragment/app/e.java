package androidx.fragment.app;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.lifecycle.v;
import b.e.l.f;

/* compiled from: FragmentController */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private final g<?> f1484a;

    private e(g<?> gVar) {
        this.f1484a = gVar;
    }

    public static e a(g<?> gVar) {
        f.a(gVar, "callbacks == null");
        return new e(gVar);
    }

    public void b() {
        this.f1484a.f1490e.g();
    }

    public void c() {
        this.f1484a.f1490e.h();
    }

    public void d() {
        this.f1484a.f1490e.j();
    }

    public void e() {
        this.f1484a.f1490e.k();
    }

    public void f() {
        this.f1484a.f1490e.m();
    }

    public void g() {
        this.f1484a.f1490e.n();
    }

    public void h() {
        this.f1484a.f1490e.o();
    }

    public boolean i() {
        return this.f1484a.f1490e.q();
    }

    public h j() {
        return this.f1484a.f1490e;
    }

    public void k() {
        this.f1484a.f1490e.x();
    }

    public Parcelable l() {
        return this.f1484a.f1490e.z();
    }

    public Fragment a(String str) {
        return this.f1484a.f1490e.b(str);
    }

    public void b(boolean z) {
        this.f1484a.f1490e.b(z);
    }

    public void a(Fragment fragment) {
        g<?> gVar = this.f1484a;
        gVar.f1490e.a(gVar, gVar, fragment);
    }

    public boolean b(Menu menu) {
        return this.f1484a.f1490e.b(menu);
    }

    public View a(View view, String str, Context context, AttributeSet attributeSet) {
        return this.f1484a.f1490e.onCreateView(view, str, context, attributeSet);
    }

    public boolean b(MenuItem menuItem) {
        return this.f1484a.f1490e.b(menuItem);
    }

    public void a(Parcelable parcelable) {
        g<?> gVar = this.f1484a;
        if (gVar instanceof v) {
            gVar.f1490e.a(parcelable);
            return;
        }
        throw new IllegalStateException("Your FragmentHostCallback must implement ViewModelStoreOwner to call restoreSaveState(). Call restoreAllState()  if you're still using retainNestedNonConfig().");
    }

    public void a() {
        this.f1484a.f1490e.f();
    }

    public void a(boolean z) {
        this.f1484a.f1490e.a(z);
    }

    public void a(Configuration configuration) {
        this.f1484a.f1490e.a(configuration);
    }

    public boolean a(Menu menu, MenuInflater menuInflater) {
        return this.f1484a.f1490e.a(menu, menuInflater);
    }

    public boolean a(MenuItem menuItem) {
        return this.f1484a.f1490e.a(menuItem);
    }

    public void a(Menu menu) {
        this.f1484a.f1490e.a(menu);
    }
}
