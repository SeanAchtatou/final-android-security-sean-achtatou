package androidx.fragment.app;

import androidx.lifecycle.e;
import androidx.lifecycle.h;
import androidx.lifecycle.i;

/* compiled from: FragmentViewLifecycleOwner */
class o implements h {

    /* renamed from: a  reason: collision with root package name */
    private i f1615a = null;

    o() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.f1615a == null) {
            this.f1615a = new i(this);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.f1615a != null;
    }

    public e getLifecycle() {
        a();
        return this.f1615a;
    }

    /* access modifiers changed from: package-private */
    public void a(e.a aVar) {
        this.f1615a.a(aVar);
    }
}
