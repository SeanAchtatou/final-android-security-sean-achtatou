package androidx.fragment.app;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import androidx.activity.OnBackPressedDispatcher;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.h;
import androidx.lifecycle.e;
import androidx.lifecycle.u;
import b.e.m.r;
import com.esotericsoftware.spine.Animation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: FragmentManagerImpl */
final class i extends h implements LayoutInflater.Factory2 {
    static boolean H = false;
    static final Interpolator I = new DecelerateInterpolator(2.5f);
    static final Interpolator J = new DecelerateInterpolator(1.5f);
    ArrayList<Boolean> A;
    ArrayList<Fragment> B;
    Bundle C = null;
    SparseArray<Parcelable> D = null;
    ArrayList<m> E;
    private j F;
    Runnable G = new b();

    /* renamed from: c  reason: collision with root package name */
    ArrayList<k> f1493c;

    /* renamed from: d  reason: collision with root package name */
    boolean f1494d;

    /* renamed from: e  reason: collision with root package name */
    int f1495e = 0;

    /* renamed from: f  reason: collision with root package name */
    final ArrayList<Fragment> f1496f = new ArrayList<>();

    /* renamed from: g  reason: collision with root package name */
    final HashMap<String, Fragment> f1497g = new HashMap<>();

    /* renamed from: h  reason: collision with root package name */
    ArrayList<a> f1498h;

    /* renamed from: i  reason: collision with root package name */
    ArrayList<Fragment> f1499i;

    /* renamed from: j  reason: collision with root package name */
    private OnBackPressedDispatcher f1500j;

    /* renamed from: k  reason: collision with root package name */
    private final androidx.activity.b f1501k = new a(false);
    ArrayList<a> l;
    ArrayList<Integer> m;
    ArrayList<h.c> n;
    private final CopyOnWriteArrayList<C0015i> o = new CopyOnWriteArrayList<>();
    int p = 0;
    g q;
    d r;
    Fragment s;
    Fragment t;
    boolean u;
    boolean v;
    boolean w;
    boolean x;
    boolean y;
    ArrayList<a> z;

    /* compiled from: FragmentManagerImpl */
    class a extends androidx.activity.b {
        a(boolean z) {
            super(z);
        }

        public void a() {
            i.this.u();
        }
    }

    /* compiled from: FragmentManagerImpl */
    class b implements Runnable {
        b() {
        }

        public void run() {
            i.this.q();
        }
    }

    /* compiled from: FragmentManagerImpl */
    class c implements Animation.AnimationListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ ViewGroup f1504a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Fragment f1505b;

        /* compiled from: FragmentManagerImpl */
        class a implements Runnable {
            a() {
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
             arg types: [androidx.fragment.app.Fragment, int, int, int, int]
             candidates:
              androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, b.d.b<androidx.fragment.app.Fragment>):int
              androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
              androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
            public void run() {
                if (c.this.f1505b.getAnimatingAway() != null) {
                    c.this.f1505b.setAnimatingAway(null);
                    c cVar = c.this;
                    i iVar = i.this;
                    Fragment fragment = cVar.f1505b;
                    iVar.a(fragment, fragment.getStateAfterAnimating(), 0, 0, false);
                }
            }
        }

        c(ViewGroup viewGroup, Fragment fragment) {
            this.f1504a = viewGroup;
            this.f1505b = fragment;
        }

        public void onAnimationEnd(Animation animation) {
            this.f1504a.post(new a());
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    /* compiled from: FragmentManagerImpl */
    class d extends AnimatorListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ ViewGroup f1508a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ View f1509b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ Fragment f1510c;

        d(ViewGroup viewGroup, View view, Fragment fragment) {
            this.f1508a = viewGroup;
            this.f1509b = view;
            this.f1510c = fragment;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
         arg types: [androidx.fragment.app.Fragment, int, int, int, int]
         candidates:
          androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, b.d.b<androidx.fragment.app.Fragment>):int
          androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
          androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
        public void onAnimationEnd(Animator animator) {
            this.f1508a.endViewTransition(this.f1509b);
            Animator animator2 = this.f1510c.getAnimator();
            this.f1510c.setAnimator(null);
            if (animator2 != null && this.f1508a.indexOfChild(this.f1509b) < 0) {
                i iVar = i.this;
                Fragment fragment = this.f1510c;
                iVar.a(fragment, fragment.getStateAfterAnimating(), 0, 0, false);
            }
        }
    }

    /* compiled from: FragmentManagerImpl */
    class e extends AnimatorListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ ViewGroup f1512a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ View f1513b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ Fragment f1514c;

        e(i iVar, ViewGroup viewGroup, View view, Fragment fragment) {
            this.f1512a = viewGroup;
            this.f1513b = view;
            this.f1514c = fragment;
        }

        public void onAnimationEnd(Animator animator) {
            this.f1512a.endViewTransition(this.f1513b);
            animator.removeListener(this);
            Fragment fragment = this.f1514c;
            View view = fragment.mView;
            if (view != null && fragment.mHidden) {
                view.setVisibility(8);
            }
        }
    }

    /* compiled from: FragmentManagerImpl */
    class f extends f {
        f() {
        }

        public Fragment a(ClassLoader classLoader, String str) {
            g gVar = i.this.q;
            return gVar.a(gVar.c(), str, (Bundle) null);
        }
    }

    /* renamed from: androidx.fragment.app.i$i  reason: collision with other inner class name */
    /* compiled from: FragmentManagerImpl */
    private static final class C0015i {

        /* renamed from: a  reason: collision with root package name */
        final h.b f1523a;

        /* renamed from: b  reason: collision with root package name */
        final boolean f1524b;
    }

    /* compiled from: FragmentManagerImpl */
    static class j {

        /* renamed from: a  reason: collision with root package name */
        public static final int[] f1525a = {16842755, 16842960, 16842961};
    }

    /* compiled from: FragmentManagerImpl */
    interface k {
        boolean a(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2);
    }

    /* compiled from: FragmentManagerImpl */
    private class l implements k {

        /* renamed from: a  reason: collision with root package name */
        final String f1526a;

        /* renamed from: b  reason: collision with root package name */
        final int f1527b;

        /* renamed from: c  reason: collision with root package name */
        final int f1528c;

        l(String str, int i2, int i3) {
            this.f1526a = str;
            this.f1527b = i2;
            this.f1528c = i3;
        }

        public boolean a(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2) {
            Fragment fragment = i.this.t;
            if (fragment != null && this.f1527b < 0 && this.f1526a == null && fragment.getChildFragmentManager().d()) {
                return false;
            }
            return i.this.a(arrayList, arrayList2, this.f1526a, this.f1527b, this.f1528c);
        }
    }

    /* compiled from: FragmentManagerImpl */
    static class m implements Fragment.f {

        /* renamed from: a  reason: collision with root package name */
        final boolean f1530a;

        /* renamed from: b  reason: collision with root package name */
        final a f1531b;

        /* renamed from: c  reason: collision with root package name */
        private int f1532c;

        m(a aVar, boolean z) {
            this.f1530a = z;
            this.f1531b = aVar;
        }

        public void a() {
            this.f1532c++;
        }

        public void b() {
            this.f1532c--;
            if (this.f1532c == 0) {
                this.f1531b.r.A();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.fragment.app.i.a(androidx.fragment.app.a, boolean, boolean, boolean):void
         arg types: [androidx.fragment.app.a, boolean, int, int]
         candidates:
          androidx.fragment.app.i.a(float, float, float, float):androidx.fragment.app.i$g
          androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int):void
          androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, boolean, int):androidx.fragment.app.i$g
          androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void
          androidx.fragment.app.i.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
          androidx.fragment.app.h.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
          androidx.fragment.app.i.a(androidx.fragment.app.a, boolean, boolean, boolean):void */
        public void c() {
            a aVar = this.f1531b;
            aVar.r.a(aVar, this.f1530a, false, false);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.fragment.app.i.a(androidx.fragment.app.a, boolean, boolean, boolean):void
         arg types: [androidx.fragment.app.a, boolean, boolean, int]
         candidates:
          androidx.fragment.app.i.a(float, float, float, float):androidx.fragment.app.i$g
          androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int):void
          androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, boolean, int):androidx.fragment.app.i$g
          androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void
          androidx.fragment.app.i.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
          androidx.fragment.app.h.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
          androidx.fragment.app.i.a(androidx.fragment.app.a, boolean, boolean, boolean):void */
        public void d() {
            boolean z = this.f1532c > 0;
            i iVar = this.f1531b.r;
            int size = iVar.f1496f.size();
            for (int i2 = 0; i2 < size; i2++) {
                Fragment fragment = iVar.f1496f.get(i2);
                fragment.setOnStartEnterTransitionListener(null);
                if (z && fragment.isPostponed()) {
                    fragment.startPostponedEnterTransition();
                }
            }
            a aVar = this.f1531b;
            aVar.r.a(aVar, this.f1530a, !z, true);
        }

        public boolean e() {
            return this.f1532c == 0;
        }
    }

    i() {
    }

    private void C() {
        this.f1497g.values().removeAll(Collections.singleton(null));
    }

    private void D() {
        if (w()) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        }
    }

    private void E() {
        this.f1494d = false;
        this.A.clear();
        this.z.clear();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, b.d.b<androidx.fragment.app.Fragment>):int
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    private void F() {
        for (Fragment next : this.f1497g.values()) {
            if (next != null) {
                if (next.getAnimatingAway() != null) {
                    int stateAfterAnimating = next.getStateAfterAnimating();
                    View animatingAway = next.getAnimatingAway();
                    Animation animation = animatingAway.getAnimation();
                    if (animation != null) {
                        animation.cancel();
                        animatingAway.clearAnimation();
                    }
                    next.setAnimatingAway(null);
                    a(next, stateAfterAnimating, 0, 0, false);
                } else if (next.getAnimator() != null) {
                    next.getAnimator().end();
                }
            }
        }
    }

    private void G() {
        if (this.E != null) {
            while (!this.E.isEmpty()) {
                this.E.remove(0).d();
            }
        }
    }

    private void H() {
        ArrayList<k> arrayList = this.f1493c;
        boolean z2 = true;
        if (arrayList == null || arrayList.isEmpty()) {
            androidx.activity.b bVar = this.f1501k;
            if (r() <= 0 || !i(this.s)) {
                z2 = false;
            }
            bVar.a(z2);
            return;
        }
        this.f1501k.a(true);
    }

    private void a(RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new b.e.l.b("FragmentManager"));
        g gVar = this.q;
        if (gVar != null) {
            try {
                gVar.a("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e2) {
                Log.e("FragmentManager", "Failed dumping state", e2);
            }
        } else {
            try {
                a("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e3) {
                Log.e("FragmentManager", "Failed dumping state", e3);
            }
        }
        throw runtimeException;
    }

    public static int b(int i2, boolean z2) {
        if (i2 == 4097) {
            return z2 ? 1 : 2;
        }
        if (i2 == 4099) {
            return z2 ? 5 : 6;
        }
        if (i2 != 8194) {
            return -1;
        }
        return z2 ? 3 : 4;
    }

    public static int e(int i2) {
        if (i2 == 4097) {
            return 8194;
        }
        if (i2 != 4099) {
            return i2 != 8194 ? 0 : 4097;
        }
        return 4099;
    }

    /* access modifiers changed from: package-private */
    public void A() {
        synchronized (this) {
            boolean z2 = false;
            boolean z3 = this.E != null && !this.E.isEmpty();
            if (this.f1493c != null && this.f1493c.size() == 1) {
                z2 = true;
            }
            if (z3 || z2) {
                this.q.d().removeCallbacks(this.G);
                this.q.d().post(this.G);
                H();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void B() {
        for (Fragment next : this.f1497g.values()) {
            if (next != null) {
                n(next);
            }
        }
    }

    public void b(Fragment fragment) {
        if (H) {
            Log.v("FragmentManager", "attach: " + fragment);
        }
        if (fragment.mDetached) {
            fragment.mDetached = false;
            if (fragment.mAdded) {
                return;
            }
            if (!this.f1496f.contains(fragment)) {
                if (H) {
                    Log.v("FragmentManager", "add from attach: " + fragment);
                }
                synchronized (this.f1496f) {
                    this.f1496f.add(fragment);
                }
                fragment.mAdded = true;
                if (w(fragment)) {
                    this.u = true;
                    return;
                }
                return;
            }
            throw new IllegalStateException("Fragment already added: " + fragment);
        }
    }

    public List<Fragment> c() {
        List<Fragment> list;
        if (this.f1496f.isEmpty()) {
            return Collections.emptyList();
        }
        synchronized (this.f1496f) {
            list = (List) this.f1496f.clone();
        }
        return list;
    }

    public boolean d() {
        D();
        return a((String) null, -1, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, int]
     candidates:
      androidx.fragment.app.i.a(float, float, float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, boolean, int):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(androidx.fragment.app.a, boolean, boolean, boolean):void
      androidx.fragment.app.i.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      androidx.fragment.app.h.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void */
    /* access modifiers changed from: package-private */
    public void e(Fragment fragment) {
        if (fragment.mFromLayout && !fragment.mPerformedCreateView) {
            fragment.performCreateView(fragment.performGetLayoutInflater(fragment.mSavedFragmentState), null, fragment.mSavedFragmentState);
            View view = fragment.mView;
            if (view != null) {
                fragment.mInnerView = view;
                view.setSaveFromParentEnabled(false);
                if (fragment.mHidden) {
                    fragment.mView.setVisibility(8);
                }
                fragment.onViewCreated(fragment.mView, fragment.mSavedFragmentState);
                a(fragment, fragment.mView, fragment.mSavedFragmentState, false);
                return;
            }
            fragment.mInnerView = null;
        }
    }

    /* access modifiers changed from: package-private */
    public j f(Fragment fragment) {
        return this.F.c(fragment);
    }

    /* access modifiers changed from: package-private */
    public u g(Fragment fragment) {
        return this.F.d(fragment);
    }

    public void h(Fragment fragment) {
        if (H) {
            Log.v("FragmentManager", "hide: " + fragment);
        }
        if (!fragment.mHidden) {
            fragment.mHidden = true;
            fragment.mHiddenChanged = true ^ fragment.mHiddenChanged;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean i(Fragment fragment) {
        if (fragment == null) {
            return true;
        }
        i iVar = fragment.mFragmentManager;
        if (fragment != iVar.t() || !i(iVar.s)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void j(Fragment fragment) {
        if (this.f1497g.get(fragment.mWho) == null) {
            this.f1497g.put(fragment.mWho, fragment);
            if (fragment.mRetainInstanceChangedWhileDetached) {
                if (fragment.mRetainInstance) {
                    a(fragment);
                } else {
                    p(fragment);
                }
                fragment.mRetainInstanceChangedWhileDetached = false;
            }
            if (H) {
                Log.v("FragmentManager", "Added fragment to active set " + fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void k(Fragment fragment) {
        if (this.f1497g.get(fragment.mWho) != null) {
            if (H) {
                Log.v("FragmentManager", "Removed fragment from active set " + fragment);
            }
            for (Fragment next : this.f1497g.values()) {
                if (next != null && fragment.mWho.equals(next.mTargetWho)) {
                    next.mTarget = fragment;
                    next.mTargetWho = null;
                }
            }
            this.f1497g.put(fragment.mWho, null);
            p(fragment);
            String str = fragment.mTargetWho;
            if (str != null) {
                fragment.mTarget = this.f1497g.get(str);
            }
            fragment.initState();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, b.d.b<androidx.fragment.app.Fragment>):int
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, boolean, int):androidx.fragment.app.i$g
     arg types: [androidx.fragment.app.Fragment, int, int, int]
     candidates:
      androidx.fragment.app.i.a(float, float, float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.a, boolean, boolean, boolean):void
      androidx.fragment.app.i.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      androidx.fragment.app.h.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, boolean, int):androidx.fragment.app.i$g */
    /* access modifiers changed from: package-private */
    public void l(Fragment fragment) {
        ViewGroup viewGroup;
        int indexOfChild;
        int indexOfChild2;
        if (fragment != null) {
            if (this.f1497g.containsKey(fragment.mWho)) {
                int i2 = this.p;
                if (fragment.mRemoving) {
                    if (fragment.isInBackStack()) {
                        i2 = Math.min(i2, 1);
                    } else {
                        i2 = Math.min(i2, 0);
                    }
                }
                a(fragment, i2, fragment.getNextTransition(), fragment.getNextTransitionStyle(), false);
                if (fragment.mView != null) {
                    Fragment v2 = v(fragment);
                    if (v2 != null && (indexOfChild2 = viewGroup.indexOfChild(fragment.mView)) < (indexOfChild = (viewGroup = fragment.mContainer).indexOfChild(v2.mView))) {
                        viewGroup.removeViewAt(indexOfChild2);
                        viewGroup.addView(fragment.mView, indexOfChild);
                    }
                    if (fragment.mIsNewlyAdded && fragment.mContainer != null) {
                        float f2 = fragment.mPostponedAlpha;
                        if (f2 > Animation.CurveTimeline.LINEAR) {
                            fragment.mView.setAlpha(f2);
                        }
                        fragment.mPostponedAlpha = Animation.CurveTimeline.LINEAR;
                        fragment.mIsNewlyAdded = false;
                        g a2 = a(fragment, fragment.getNextTransition(), true, fragment.getNextTransitionStyle());
                        if (a2 != null) {
                            android.view.animation.Animation animation = a2.f1516a;
                            if (animation != null) {
                                fragment.mView.startAnimation(animation);
                            } else {
                                a2.f1517b.setTarget(fragment.mView);
                                a2.f1517b.start();
                            }
                        }
                    }
                }
                if (fragment.mHiddenChanged) {
                    c(fragment);
                }
            } else if (H) {
                Log.v("FragmentManager", "Ignoring moving " + fragment + " to state " + this.p + "since it is not added to " + this);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, b.d.b<androidx.fragment.app.Fragment>):int
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public void m(Fragment fragment) {
        a(fragment, this.p, 0, 0, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, b.d.b<androidx.fragment.app.Fragment>):int
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    public void n(Fragment fragment) {
        if (!fragment.mDeferStart) {
            return;
        }
        if (this.f1494d) {
            this.y = true;
            return;
        }
        fragment.mDeferStart = false;
        a(fragment, this.p, 0, 0, false);
    }

    public void o(Fragment fragment) {
        if (H) {
            Log.v("FragmentManager", "remove: " + fragment + " nesting=" + fragment.mBackStackNesting);
        }
        boolean z2 = !fragment.isInBackStack();
        if (!fragment.mDetached || z2) {
            synchronized (this.f1496f) {
                this.f1496f.remove(fragment);
            }
            if (w(fragment)) {
                this.u = true;
            }
            fragment.mAdded = false;
            fragment.mRemoving = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      androidx.fragment.app.i.a(float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):void
      androidx.fragment.app.i.a(android.os.Bundle, java.lang.String):androidx.fragment.app.Fragment
      androidx.fragment.app.i.a(int, int):void
      androidx.fragment.app.i.a(int, androidx.fragment.app.a):void
      androidx.fragment.app.i.a(int, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, androidx.lifecycle.e$b):void
      androidx.fragment.app.i.a(androidx.fragment.app.i$k, boolean):void
      androidx.fragment.app.i.a(android.view.Menu, android.view.MenuInflater):boolean
      androidx.fragment.app.h.a(int, int):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, b.d.b<androidx.fragment.app.Fragment>):int
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        Fragment fragment;
        AttributeSet attributeSet2 = attributeSet;
        Fragment fragment2 = null;
        if (!"fragment".equals(str)) {
            return null;
        }
        String attributeValue = attributeSet2.getAttributeValue(null, "class");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet2, j.f1525a);
        int i2 = 0;
        if (attributeValue == null) {
            attributeValue = obtainStyledAttributes.getString(0);
        }
        String str2 = attributeValue;
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        String string = obtainStyledAttributes.getString(2);
        obtainStyledAttributes.recycle();
        if (str2 == null || !f.b(context.getClassLoader(), str2)) {
            return null;
        }
        if (view != null) {
            i2 = view.getId();
        }
        if (i2 == -1 && resourceId == -1 && string == null) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + str2);
        }
        if (resourceId != -1) {
            fragment2 = a(resourceId);
        }
        if (fragment2 == null && string != null) {
            fragment2 = a(string);
        }
        if (fragment2 == null && i2 != -1) {
            fragment2 = a(i2);
        }
        if (H) {
            Log.v("FragmentManager", "onCreateView: id=0x" + Integer.toHexString(resourceId) + " fname=" + str2 + " existing=" + fragment2);
        }
        if (fragment2 == null) {
            Fragment a2 = b().a(context.getClassLoader(), str2);
            a2.mFromLayout = true;
            a2.mFragmentId = resourceId != 0 ? resourceId : i2;
            a2.mContainerId = i2;
            a2.mTag = string;
            a2.mInLayout = true;
            a2.mFragmentManager = this;
            g gVar = this.q;
            a2.mHost = gVar;
            a2.onInflate(gVar.c(), attributeSet2, a2.mSavedFragmentState);
            a(a2, true);
            fragment = a2;
        } else if (!fragment2.mInLayout) {
            fragment2.mInLayout = true;
            g gVar2 = this.q;
            fragment2.mHost = gVar2;
            fragment2.onInflate(gVar2.c(), attributeSet2, fragment2.mSavedFragmentState);
            fragment = fragment2;
        } else {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(resourceId) + ", tag " + string + ", or parent id 0x" + Integer.toHexString(i2) + " with another fragment for " + str2);
        }
        if (this.p >= 1 || !fragment.mFromLayout) {
            m(fragment);
        } else {
            a(fragment, 1, 0, 0, false);
        }
        View view2 = fragment.mView;
        if (view2 != null) {
            if (resourceId != 0) {
                view2.setId(resourceId);
            }
            if (fragment.mView.getTag() == null) {
                fragment.mView.setTag(string);
            }
            return fragment.mView;
        }
        throw new IllegalStateException("Fragment " + str2 + " did not create a view.");
    }

    /* access modifiers changed from: package-private */
    public void p(Fragment fragment) {
        if (w()) {
            if (H) {
                Log.v("FragmentManager", "Ignoring removeRetainedFragment as the state is already saved");
            }
        } else if (this.F.e(fragment) && H) {
            Log.v("FragmentManager", "Updating retained Fragments: Removed " + fragment);
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean q() {
        c(true);
        boolean z2 = false;
        while (b(this.z, this.A)) {
            this.f1494d = true;
            try {
                c(this.z, this.A);
                E();
                z2 = true;
            } catch (Throwable th) {
                E();
                throw th;
            }
        }
        H();
        p();
        C();
        return z2;
    }

    public int r() {
        ArrayList<a> arrayList = this.f1498h;
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public LayoutInflater.Factory2 s() {
        return this;
    }

    public void s(Fragment fragment) {
        if (fragment == null || (this.f1497g.get(fragment.mWho) == fragment && (fragment.mHost == null || fragment.getFragmentManager() == this))) {
            Fragment fragment2 = this.t;
            this.t = fragment;
            u(fragment2);
            u(this.t);
            return;
        }
        throw new IllegalArgumentException("Fragment " + fragment + " is not an active fragment of FragmentManager " + this);
    }

    public void t(Fragment fragment) {
        if (H) {
            Log.v("FragmentManager", "show: " + fragment);
        }
        if (fragment.mHidden) {
            fragment.mHidden = false;
            fragment.mHiddenChanged = !fragment.mHiddenChanged;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        Fragment fragment = this.s;
        if (fragment != null) {
            b.e.l.a.a(fragment, sb);
        } else {
            b.e.l.a.a(this.q, sb);
        }
        sb.append("}}");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public void u() {
        q();
        if (this.f1501k.b()) {
            d();
        } else {
            this.f1500j.a();
        }
    }

    public boolean v() {
        return this.x;
    }

    public boolean w() {
        return this.v || this.w;
    }

    public void x() {
        this.v = false;
        this.w = false;
        int size = this.f1496f.size();
        for (int i2 = 0; i2 < size; i2++) {
            Fragment fragment = this.f1496f.get(i2);
            if (fragment != null) {
                fragment.noteStateNotSaved();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void y() {
        if (this.n != null) {
            for (int i2 = 0; i2 < this.n.size(); i2++) {
                this.n.get(i2).a();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Parcelable z() {
        ArrayList<String> arrayList;
        int size;
        G();
        F();
        q();
        this.v = true;
        BackStackState[] backStackStateArr = null;
        if (this.f1497g.isEmpty()) {
            return null;
        }
        ArrayList<FragmentState> arrayList2 = new ArrayList<>(this.f1497g.size());
        boolean z2 = false;
        for (Fragment next : this.f1497g.values()) {
            if (next != null) {
                if (next.mFragmentManager == this) {
                    FragmentState fragmentState = new FragmentState(next);
                    arrayList2.add(fragmentState);
                    if (next.mState <= 0 || fragmentState.m != null) {
                        fragmentState.m = next.mSavedFragmentState;
                    } else {
                        fragmentState.m = q(next);
                        String str = next.mTargetWho;
                        if (str != null) {
                            Fragment fragment = this.f1497g.get(str);
                            if (fragment != null) {
                                if (fragmentState.m == null) {
                                    fragmentState.m = new Bundle();
                                }
                                a(fragmentState.m, "android:target_state", fragment);
                                int i2 = next.mTargetRequestCode;
                                if (i2 != 0) {
                                    fragmentState.m.putInt("android:target_req_state", i2);
                                }
                            } else {
                                a(new IllegalStateException("Failure saving state: " + next + " has target not in fragment manager: " + next.mTargetWho));
                                throw null;
                            }
                        }
                    }
                    if (H) {
                        Log.v("FragmentManager", "Saved state of " + next + ": " + fragmentState.m);
                    }
                    z2 = true;
                } else {
                    a(new IllegalStateException("Failure saving state: active " + next + " was removed from the FragmentManager"));
                    throw null;
                }
            }
        }
        if (!z2) {
            if (H) {
                Log.v("FragmentManager", "saveAllState: no fragments!");
            }
            return null;
        }
        int size2 = this.f1496f.size();
        if (size2 > 0) {
            arrayList = new ArrayList<>(size2);
            Iterator<Fragment> it = this.f1496f.iterator();
            while (it.hasNext()) {
                Fragment next2 = it.next();
                arrayList.add(next2.mWho);
                if (next2.mFragmentManager != this) {
                    a(new IllegalStateException("Failure saving state: active " + next2 + " was removed from the FragmentManager"));
                    throw null;
                } else if (H) {
                    Log.v("FragmentManager", "saveAllState: adding fragment (" + next2.mWho + "): " + next2);
                }
            }
        } else {
            arrayList = null;
        }
        ArrayList<a> arrayList3 = this.f1498h;
        if (arrayList3 != null && (size = arrayList3.size()) > 0) {
            backStackStateArr = new BackStackState[size];
            for (int i3 = 0; i3 < size; i3++) {
                backStackStateArr[i3] = new BackStackState(this.f1498h.get(i3));
                if (H) {
                    Log.v("FragmentManager", "saveAllState: adding back stack #" + i3 + ": " + this.f1498h.get(i3));
                }
            }
        }
        FragmentManagerState fragmentManagerState = new FragmentManagerState();
        fragmentManagerState.f1466a = arrayList2;
        fragmentManagerState.f1467b = arrayList;
        fragmentManagerState.f1468c = backStackStateArr;
        Fragment fragment2 = this.t;
        if (fragment2 != null) {
            fragmentManagerState.f1469d = fragment2.mWho;
        }
        fragmentManagerState.f1470e = this.f1495e;
        return fragmentManagerState;
    }

    private Fragment v(Fragment fragment) {
        ViewGroup viewGroup = fragment.mContainer;
        View view = fragment.mView;
        if (!(viewGroup == null || view == null)) {
            for (int indexOf = this.f1496f.indexOf(fragment) - 1; indexOf >= 0; indexOf--) {
                Fragment fragment2 = this.f1496f.get(indexOf);
                if (fragment2.mContainer == viewGroup && fragment2.mView != null) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    private boolean w(Fragment fragment) {
        return (fragment.mHasMenu && fragment.mMenuVisible) || fragment.mChildFragmentManager.e();
    }

    public void f() {
        this.v = false;
        this.w = false;
        d(2);
    }

    public void g() {
        this.v = false;
        this.w = false;
        d(1);
    }

    public void m() {
        this.v = false;
        this.w = false;
        d(4);
    }

    /* access modifiers changed from: package-private */
    public void r(Fragment fragment) {
        if (fragment.mInnerView != null) {
            SparseArray<Parcelable> sparseArray = this.D;
            if (sparseArray == null) {
                this.D = new SparseArray<>();
            } else {
                sparseArray.clear();
            }
            fragment.mInnerView.saveHierarchyState(this.D);
            if (this.D.size() > 0) {
                fragment.mSavedViewState = this.D;
                this.D = null;
            }
        }
    }

    public void d(Fragment fragment) {
        if (H) {
            Log.v("FragmentManager", "detach: " + fragment);
        }
        if (!fragment.mDetached) {
            fragment.mDetached = true;
            if (fragment.mAdded) {
                if (H) {
                    Log.v("FragmentManager", "remove from detach: " + fragment);
                }
                synchronized (this.f1496f) {
                    this.f1496f.remove(fragment);
                }
                if (w(fragment)) {
                    this.u = true;
                }
                fragment.mAdded = false;
            }
        }
    }

    /* compiled from: FragmentManagerImpl */
    private static class g {

        /* renamed from: a  reason: collision with root package name */
        public final android.view.animation.Animation f1516a;

        /* renamed from: b  reason: collision with root package name */
        public final Animator f1517b;

        g(android.view.animation.Animation animation) {
            this.f1516a = animation;
            this.f1517b = null;
            if (animation == null) {
                throw new IllegalStateException("Animation cannot be null");
            }
        }

        g(Animator animator) {
            this.f1516a = null;
            this.f1517b = animator;
            if (animator == null) {
                throw new IllegalStateException("Animator cannot be null");
            }
        }
    }

    private void u(Fragment fragment) {
        if (fragment != null && this.f1497g.get(fragment.mWho) == fragment) {
            fragment.performPrimaryNavigationFragmentChanged();
        }
    }

    /* access modifiers changed from: package-private */
    public void f(Fragment fragment, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).f(fragment, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.e(this, fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void g(Fragment fragment, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).g(fragment, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.f(this, fragment);
            }
        }
    }

    public void h() {
        this.x = true;
        q();
        d(0);
        this.q = null;
        this.r = null;
        this.s = null;
        if (this.f1500j != null) {
            this.f1501k.c();
            this.f1500j = null;
        }
    }

    public void i() {
        d(1);
    }

    public Fragment t() {
        return this.t;
    }

    /* compiled from: FragmentManagerImpl */
    private static class h extends AnimationSet implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final ViewGroup f1518a;

        /* renamed from: b  reason: collision with root package name */
        private final View f1519b;

        /* renamed from: c  reason: collision with root package name */
        private boolean f1520c;

        /* renamed from: d  reason: collision with root package name */
        private boolean f1521d;

        /* renamed from: e  reason: collision with root package name */
        private boolean f1522e = true;

        h(android.view.animation.Animation animation, ViewGroup viewGroup, View view) {
            super(false);
            this.f1518a = viewGroup;
            this.f1519b = view;
            addAnimation(animation);
            this.f1518a.post(this);
        }

        public boolean getTransformation(long j2, Transformation transformation) {
            this.f1522e = true;
            if (this.f1520c) {
                return !this.f1521d;
            }
            if (!super.getTransformation(j2, transformation)) {
                this.f1520c = true;
                r.a(this.f1518a, this);
            }
            return true;
        }

        public void run() {
            if (this.f1520c || !this.f1522e) {
                this.f1518a.endViewTransition(this.f1519b);
                this.f1521d = true;
                return;
            }
            this.f1522e = false;
            this.f1518a.post(this);
        }

        public boolean getTransformation(long j2, Transformation transformation, float f2) {
            this.f1522e = true;
            if (this.f1520c) {
                return !this.f1521d;
            }
            if (!super.getTransformation(j2, transformation, f2)) {
                this.f1520c = true;
                r.a(this.f1518a, this);
            }
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c(int i2) {
        return this.p >= i2;
    }

    public void n() {
        this.v = false;
        this.w = false;
        d(3);
    }

    /* access modifiers changed from: package-private */
    public void c(Fragment fragment) {
        Animator animator;
        if (fragment.mView != null) {
            g a2 = a(fragment, fragment.getNextTransition(), !fragment.mHidden, fragment.getNextTransitionStyle());
            if (a2 == null || (animator = a2.f1517b) == null) {
                if (a2 != null) {
                    fragment.mView.startAnimation(a2.f1516a);
                    a2.f1516a.start();
                }
                fragment.mView.setVisibility((!fragment.mHidden || fragment.isHideReplaced()) ? 0 : 8);
                if (fragment.isHideReplaced()) {
                    fragment.setHideReplaced(false);
                }
            } else {
                animator.setTarget(fragment.mView);
                if (!fragment.mHidden) {
                    fragment.mView.setVisibility(0);
                } else if (fragment.isHideReplaced()) {
                    fragment.setHideReplaced(false);
                } else {
                    ViewGroup viewGroup = fragment.mContainer;
                    View view = fragment.mView;
                    viewGroup.startViewTransition(view);
                    a2.f1517b.addListener(new e(this, viewGroup, view, fragment));
                }
                a2.f1517b.start();
            }
        }
        if (fragment.mAdded && w(fragment)) {
            this.u = true;
        }
        fragment.mHiddenChanged = false;
        fragment.onHiddenChanged(fragment.mHidden);
    }

    /* access modifiers changed from: package-private */
    public void p() {
        if (this.y) {
            this.y = false;
            B();
        }
    }

    public void j() {
        for (int i2 = 0; i2 < this.f1496f.size(); i2++) {
            Fragment fragment = this.f1496f.get(i2);
            if (fragment != null) {
                fragment.performLowMemory();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void e(Fragment fragment, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).e(fragment, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.d(this, fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Bundle q(Fragment fragment) {
        Bundle bundle;
        if (this.C == null) {
            this.C = new Bundle();
        }
        fragment.performSaveInstanceState(this.C);
        d(fragment, this.C, false);
        if (!this.C.isEmpty()) {
            bundle = this.C;
            this.C = null;
        } else {
            bundle = null;
        }
        if (fragment.mView != null) {
            r(fragment);
        }
        if (fragment.mSavedViewState != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", fragment.mSavedViewState);
        }
        if (!fragment.mUserVisibleHint) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android:user_visible_hint", fragment.mUserVisibleHint);
        }
        return bundle;
    }

    public k a() {
        return new a(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.i$k, boolean):void
     arg types: [androidx.fragment.app.i$l, int]
     candidates:
      androidx.fragment.app.i.a(float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):void
      androidx.fragment.app.i.a(android.os.Bundle, java.lang.String):androidx.fragment.app.Fragment
      androidx.fragment.app.i.a(int, int):void
      androidx.fragment.app.i.a(int, androidx.fragment.app.a):void
      androidx.fragment.app.i.a(int, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, androidx.lifecycle.e$b):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, boolean):void
      androidx.fragment.app.i.a(android.view.Menu, android.view.MenuInflater):boolean
      androidx.fragment.app.h.a(int, int):void
      androidx.fragment.app.i.a(androidx.fragment.app.i$k, boolean):void */
    public void a(int i2, int i3) {
        if (i2 >= 0) {
            a((k) new l(null, i2, i3), false);
            return;
        }
        throw new IllegalArgumentException("Bad id: " + i2);
    }

    public void k() {
        d(3);
    }

    public void o() {
        this.w = true;
        d(2);
    }

    private boolean a(String str, int i2, int i3) {
        q();
        c(true);
        Fragment fragment = this.t;
        if (fragment != null && i2 < 0 && str == null && fragment.getChildFragmentManager().d()) {
            return true;
        }
        boolean a2 = a(this.z, this.A, str, i2, i3);
        if (a2) {
            this.f1494d = true;
            try {
                c(this.z, this.A);
            } finally {
                E();
            }
        }
        H();
        p();
        C();
        return a2;
    }

    /* access modifiers changed from: package-private */
    public void h(Fragment fragment, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).h(fragment, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.g(this, fragment);
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(int, boolean):void
     arg types: [int, int]
     candidates:
      androidx.fragment.app.i.a(float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):void
      androidx.fragment.app.i.a(android.os.Bundle, java.lang.String):androidx.fragment.app.Fragment
      androidx.fragment.app.i.a(int, int):void
      androidx.fragment.app.i.a(int, androidx.fragment.app.a):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, androidx.lifecycle.e$b):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.i$k, boolean):void
      androidx.fragment.app.i.a(android.view.Menu, android.view.MenuInflater):boolean
      androidx.fragment.app.h.a(int, int):void
      androidx.fragment.app.i.a(int, boolean):void */
    private void d(int i2) {
        try {
            this.f1494d = true;
            a(i2, false);
            this.f1494d = false;
            q();
        } catch (Throwable th) {
            this.f1494d = false;
            throw th;
        }
    }

    public Fragment b(String str) {
        Fragment findFragmentByWho;
        for (Fragment next : this.f1497g.values()) {
            if (next != null && (findFragmentByWho = next.findFragmentByWho(str)) != null) {
                return findFragmentByWho;
            }
        }
        return null;
    }

    public int b(a aVar) {
        synchronized (this) {
            if (this.m != null) {
                if (this.m.size() > 0) {
                    int intValue = this.m.remove(this.m.size() - 1).intValue();
                    if (H) {
                        Log.v("FragmentManager", "Adding back stack index " + intValue + " with " + aVar);
                    }
                    this.l.set(intValue, aVar);
                    return intValue;
                }
            }
            if (this.l == null) {
                this.l = new ArrayList<>();
            }
            int size = this.l.size();
            if (H) {
                Log.v("FragmentManager", "Setting back stack index " + size + " to " + aVar);
            }
            this.l.add(aVar);
            return size;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        boolean z2 = false;
        for (Fragment next : this.f1497g.values()) {
            if (next != null) {
                z2 = w(next);
                continue;
            }
            if (z2) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void d(Fragment fragment, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).d(fragment, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.c(this, fragment);
            }
        }
    }

    public void a(Bundle bundle, String str, Fragment fragment) {
        if (fragment.mFragmentManager == this) {
            bundle.putString(str, fragment.mWho);
            return;
        }
        a(new IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        throw null;
    }

    /* access modifiers changed from: package-private */
    public void d(Fragment fragment, Bundle bundle, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).d(fragment, bundle, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.d(this, fragment, bundle);
            }
        }
    }

    public Fragment a(Bundle bundle, String str) {
        String string = bundle.getString(str);
        if (string == null) {
            return null;
        }
        Fragment fragment = this.f1497g.get(string);
        if (fragment != null) {
            return fragment;
        }
        a(new IllegalStateException("Fragment no longer exists for key " + str + ": unique id " + string));
        throw null;
    }

    public void b(int i2) {
        synchronized (this) {
            this.l.set(i2, null);
            if (this.m == null) {
                this.m = new ArrayList<>();
            }
            if (H) {
                Log.v("FragmentManager", "Freeing back stack index " + i2);
            }
            this.m.add(Integer.valueOf(i2));
        }
    }

    private void c(boolean z2) {
        if (this.f1494d) {
            throw new IllegalStateException("FragmentManager is already executing transactions");
        } else if (this.q == null) {
            throw new IllegalStateException("Fragment host has been destroyed");
        } else if (Looper.myLooper() == this.q.d().getLooper()) {
            if (!z2) {
                D();
            }
            if (this.z == null) {
                this.z = new ArrayList<>();
                this.A = new ArrayList<>();
            }
            this.f1494d = true;
            try {
                a((ArrayList<a>) null, (ArrayList<Boolean>) null);
            } finally {
                this.f1494d = false;
            }
        } else {
            throw new IllegalStateException("Must be called from main thread of fragment host");
        }
    }

    /* access modifiers changed from: package-private */
    public void l() {
        H();
        u(this.t);
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment) {
        if (w()) {
            if (H) {
                Log.v("FragmentManager", "Ignoring addRetainedFragment as the state is already saved");
            }
        } else if (this.F.a(fragment) && H) {
            Log.v("FragmentManager", "Updating retained Fragments: Added " + fragment);
        }
    }

    public void b(k kVar, boolean z2) {
        if (!z2 || (this.q != null && !this.x)) {
            c(z2);
            if (kVar.a(this.z, this.A)) {
                this.f1494d = true;
                try {
                    c(this.z, this.A);
                } finally {
                    E();
                }
            }
            H();
            p();
            C();
        }
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int size;
        int size2;
        int size3;
        int size4;
        String str2 = str + "    ";
        if (!this.f1497g.isEmpty()) {
            printWriter.print(str);
            printWriter.print("Active Fragments in ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this)));
            printWriter.println(":");
            for (Fragment next : this.f1497g.values()) {
                printWriter.print(str);
                printWriter.println(next);
                if (next != null) {
                    next.dump(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
        int size5 = this.f1496f.size();
        if (size5 > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i2 = 0; i2 < size5; i2++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(this.f1496f.get(i2).toString());
            }
        }
        ArrayList<Fragment> arrayList = this.f1499i;
        if (arrayList != null && (size4 = arrayList.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Fragments Created Menus:");
            for (int i3 = 0; i3 < size4; i3++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(this.f1499i.get(i3).toString());
            }
        }
        ArrayList<a> arrayList2 = this.f1498h;
        if (arrayList2 != null && (size3 = arrayList2.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Back Stack:");
            for (int i4 = 0; i4 < size3; i4++) {
                a aVar = this.f1498h.get(i4);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i4);
                printWriter.print(": ");
                printWriter.println(aVar.toString());
                aVar.a(str2, printWriter);
            }
        }
        synchronized (this) {
            if (this.l != null && (size2 = this.l.size()) > 0) {
                printWriter.print(str);
                printWriter.println("Back Stack Indices:");
                for (int i5 = 0; i5 < size2; i5++) {
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i5);
                    printWriter.print(": ");
                    printWriter.println(this.l.get(i5));
                }
            }
            if (this.m != null && this.m.size() > 0) {
                printWriter.print(str);
                printWriter.print("mAvailBackStackIndices: ");
                printWriter.println(Arrays.toString(this.m.toArray()));
            }
        }
        ArrayList<k> arrayList3 = this.f1493c;
        if (arrayList3 != null && (size = arrayList3.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Pending Actions:");
            for (int i6 = 0; i6 < size; i6++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i6);
                printWriter.print(": ");
                printWriter.println(this.f1493c.get(i6));
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mHost=");
        printWriter.println(this.q);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.r);
        if (this.s != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.s);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.p);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.v);
        printWriter.print(" mStopped=");
        printWriter.print(this.w);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.x);
        if (this.u) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.u);
        }
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    private void c(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2) {
        if (arrayList != null && !arrayList.isEmpty()) {
            if (arrayList2 == null || arrayList.size() != arrayList2.size()) {
                throw new IllegalStateException("Internal error with the back stack records");
            }
            a(arrayList, arrayList2);
            int size = arrayList.size();
            int i2 = 0;
            int i3 = 0;
            while (i2 < size) {
                if (!arrayList.get(i2).p) {
                    if (i3 != i2) {
                        b(arrayList, arrayList2, i3, i2);
                    }
                    i3 = i2 + 1;
                    if (arrayList2.get(i2).booleanValue()) {
                        while (i3 < size && arrayList2.get(i3).booleanValue() && !arrayList.get(i3).p) {
                            i3++;
                        }
                    }
                    b(arrayList, arrayList2, i2, i3);
                    i2 = i3 - 1;
                }
                i2++;
            }
            if (i3 != size) {
                b(arrayList, arrayList2, i3, size);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.l.a(androidx.fragment.app.i, java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, boolean):void
     arg types: [androidx.fragment.app.i, java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, int]
     candidates:
      androidx.fragment.app.l.a(androidx.fragment.app.n, java.lang.Object, java.lang.Object, java.lang.Object, androidx.fragment.app.Fragment, boolean):java.lang.Object
      androidx.fragment.app.l.a(androidx.fragment.app.n, java.lang.Object, java.lang.Object, b.d.a<java.lang.String, android.view.View>, boolean, androidx.fragment.app.a):void
      androidx.fragment.app.l.a(androidx.fragment.app.i, java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(int, boolean):void
     arg types: [int, int]
     candidates:
      androidx.fragment.app.i.a(float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):void
      androidx.fragment.app.i.a(android.os.Bundle, java.lang.String):androidx.fragment.app.Fragment
      androidx.fragment.app.i.a(int, int):void
      androidx.fragment.app.i.a(int, androidx.fragment.app.a):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, androidx.lifecycle.e$b):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.i$k, boolean):void
      androidx.fragment.app.i.a(android.view.Menu, android.view.MenuInflater):boolean
      androidx.fragment.app.h.a(int, int):void
      androidx.fragment.app.i.a(int, boolean):void */
    private void b(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        int i4;
        int i5;
        ArrayList<a> arrayList3 = arrayList;
        ArrayList<Boolean> arrayList4 = arrayList2;
        int i6 = i2;
        int i7 = i3;
        boolean z2 = arrayList3.get(i6).p;
        ArrayList<Fragment> arrayList5 = this.B;
        if (arrayList5 == null) {
            this.B = new ArrayList<>();
        } else {
            arrayList5.clear();
        }
        this.B.addAll(this.f1496f);
        Fragment t2 = t();
        boolean z3 = false;
        for (int i8 = i6; i8 < i7; i8++) {
            a aVar = arrayList3.get(i8);
            if (!arrayList4.get(i8).booleanValue()) {
                t2 = aVar.a(this.B, t2);
            } else {
                t2 = aVar.b(this.B, t2);
            }
            z3 = z3 || aVar.f1547h;
        }
        this.B.clear();
        if (!z2) {
            l.a(this, arrayList, arrayList2, i2, i3, false);
        }
        a(arrayList, arrayList2, i2, i3);
        if (z2) {
            b.d.b bVar = new b.d.b();
            a(bVar);
            int a2 = a(arrayList, arrayList2, i2, i3, bVar);
            b(bVar);
            i4 = a2;
        } else {
            i4 = i7;
        }
        if (i4 != i6 && z2) {
            l.a(this, arrayList, arrayList2, i2, i4, true);
            a(this.p, true);
        }
        while (i6 < i7) {
            a aVar2 = arrayList3.get(i6);
            if (arrayList4.get(i6).booleanValue() && (i5 = aVar2.t) >= 0) {
                b(i5);
                aVar2.t = -1;
            }
            aVar2.h();
            i6++;
        }
        if (z3) {
            y();
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Fragment fragment, Bundle bundle, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).c(fragment, bundle, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.c(this, fragment, bundle);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.c(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      androidx.fragment.app.i.c(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):void
      androidx.fragment.app.i.c(androidx.fragment.app.Fragment, boolean):void */
    /* access modifiers changed from: package-private */
    public void c(Fragment fragment, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).c(fragment, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.b(this, fragment);
            }
        }
    }

    private void b(b.d.b<Fragment> bVar) {
        int size = bVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            Fragment e2 = bVar.e(i2);
            if (!e2.mAdded) {
                View requireView = e2.requireView();
                e2.mPostponedAlpha = requireView.getAlpha();
                requireView.setAlpha(Animation.CurveTimeline.LINEAR);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003b, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(java.util.ArrayList<androidx.fragment.app.a> r5, java.util.ArrayList<java.lang.Boolean> r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.ArrayList<androidx.fragment.app.i$k> r0 = r4.f1493c     // Catch:{ all -> 0x003c }
            r1 = 0
            if (r0 == 0) goto L_0x003a
            java.util.ArrayList<androidx.fragment.app.i$k> r0 = r4.f1493c     // Catch:{ all -> 0x003c }
            int r0 = r0.size()     // Catch:{ all -> 0x003c }
            if (r0 != 0) goto L_0x000f
            goto L_0x003a
        L_0x000f:
            java.util.ArrayList<androidx.fragment.app.i$k> r0 = r4.f1493c     // Catch:{ all -> 0x003c }
            int r0 = r0.size()     // Catch:{ all -> 0x003c }
            r2 = 0
        L_0x0016:
            if (r1 >= r0) goto L_0x0028
            java.util.ArrayList<androidx.fragment.app.i$k> r3 = r4.f1493c     // Catch:{ all -> 0x003c }
            java.lang.Object r3 = r3.get(r1)     // Catch:{ all -> 0x003c }
            androidx.fragment.app.i$k r3 = (androidx.fragment.app.i.k) r3     // Catch:{ all -> 0x003c }
            boolean r3 = r3.a(r5, r6)     // Catch:{ all -> 0x003c }
            r2 = r2 | r3
            int r1 = r1 + 1
            goto L_0x0016
        L_0x0028:
            java.util.ArrayList<androidx.fragment.app.i$k> r5 = r4.f1493c     // Catch:{ all -> 0x003c }
            r5.clear()     // Catch:{ all -> 0x003c }
            androidx.fragment.app.g r5 = r4.q     // Catch:{ all -> 0x003c }
            android.os.Handler r5 = r5.d()     // Catch:{ all -> 0x003c }
            java.lang.Runnable r6 = r4.G     // Catch:{ all -> 0x003c }
            r5.removeCallbacks(r6)     // Catch:{ all -> 0x003c }
            monitor-exit(r4)     // Catch:{ all -> 0x003c }
            return r2
        L_0x003a:
            monitor-exit(r4)     // Catch:{ all -> 0x003c }
            return r1
        L_0x003c:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x003c }
            goto L_0x0040
        L_0x003f:
            throw r5
        L_0x0040:
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.i.b(java.util.ArrayList, java.util.ArrayList):boolean");
    }

    public void b(boolean z2) {
        for (int size = this.f1496f.size() - 1; size >= 0; size--) {
            Fragment fragment = this.f1496f.get(size);
            if (fragment != null) {
                fragment.performPictureInPictureModeChanged(z2);
            }
        }
    }

    public boolean b(Menu menu) {
        if (this.p < 1) {
            return false;
        }
        boolean z2 = false;
        for (int i2 = 0; i2 < this.f1496f.size(); i2++) {
            Fragment fragment = this.f1496f.get(i2);
            if (fragment != null && fragment.performPrepareOptionsMenu(menu)) {
                z2 = true;
            }
        }
        return z2;
    }

    public boolean b(MenuItem menuItem) {
        if (this.p < 1) {
            return false;
        }
        for (int i2 = 0; i2 < this.f1496f.size(); i2++) {
            Fragment fragment = this.f1496f.get(i2);
            if (fragment != null && fragment.performOptionsItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }

    static g a(float f2, float f3, float f4, float f5) {
        AnimationSet animationSet = new AnimationSet(false);
        ScaleAnimation scaleAnimation = new ScaleAnimation(f2, f3, f2, f3, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(I);
        scaleAnimation.setDuration(220);
        animationSet.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(f4, f5);
        alphaAnimation.setInterpolator(J);
        alphaAnimation.setDuration(220);
        animationSet.addAnimation(alphaAnimation);
        return new g(animationSet);
    }

    public f b() {
        if (super.b() == h.f1491b) {
            Fragment fragment = this.s;
            if (fragment != null) {
                return fragment.mFragmentManager.b();
            }
            a(new f());
        }
        return super.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.b(androidx.fragment.app.Fragment, android.content.Context, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.content.Context, int]
     candidates:
      androidx.fragment.app.i.b(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
      androidx.fragment.app.i.b(androidx.fragment.app.Fragment, android.content.Context, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(Fragment fragment, Context context, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).b(fragment, context, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.b(this, fragment, context);
            }
        }
    }

    static g a(float f2, float f3) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f3);
        alphaAnimation.setInterpolator(J);
        alphaAnimation.setDuration(220);
        return new g(alphaAnimation);
    }

    /* access modifiers changed from: package-private */
    public g a(Fragment fragment, int i2, boolean z2, int i3) {
        int b2;
        int nextAnim = fragment.getNextAnim();
        boolean z3 = false;
        fragment.setNextAnim(0);
        ViewGroup viewGroup = fragment.mContainer;
        if (viewGroup != null && viewGroup.getLayoutTransition() != null) {
            return null;
        }
        android.view.animation.Animation onCreateAnimation = fragment.onCreateAnimation(i2, z2, nextAnim);
        if (onCreateAnimation != null) {
            return new g(onCreateAnimation);
        }
        Animator onCreateAnimator = fragment.onCreateAnimator(i2, z2, nextAnim);
        if (onCreateAnimator != null) {
            return new g(onCreateAnimator);
        }
        if (nextAnim != 0) {
            boolean equals = "anim".equals(this.q.c().getResources().getResourceTypeName(nextAnim));
            if (equals) {
                try {
                    android.view.animation.Animation loadAnimation = AnimationUtils.loadAnimation(this.q.c(), nextAnim);
                    if (loadAnimation != null) {
                        return new g(loadAnimation);
                    }
                    z3 = true;
                } catch (Resources.NotFoundException e2) {
                    throw e2;
                } catch (RuntimeException unused) {
                }
            }
            if (!z3) {
                try {
                    Animator loadAnimator = AnimatorInflater.loadAnimator(this.q.c(), nextAnim);
                    if (loadAnimator != null) {
                        return new g(loadAnimator);
                    }
                } catch (RuntimeException e3) {
                    if (!equals) {
                        android.view.animation.Animation loadAnimation2 = AnimationUtils.loadAnimation(this.q.c(), nextAnim);
                        if (loadAnimation2 != null) {
                            return new g(loadAnimation2);
                        }
                    } else {
                        throw e3;
                    }
                }
            }
        }
        if (i2 == 0 || (b2 = b(i2, z2)) < 0) {
            return null;
        }
        switch (b2) {
            case 1:
                return a(1.125f, 1.0f, (float) Animation.CurveTimeline.LINEAR, 1.0f);
            case 2:
                return a(1.0f, 0.975f, 1.0f, (float) Animation.CurveTimeline.LINEAR);
            case 3:
                return a(0.975f, 1.0f, (float) Animation.CurveTimeline.LINEAR, 1.0f);
            case 4:
                return a(1.0f, 1.075f, 1.0f, (float) Animation.CurveTimeline.LINEAR);
            case 5:
                return a((float) Animation.CurveTimeline.LINEAR, 1.0f);
            case 6:
                return a(1.0f, (float) Animation.CurveTimeline.LINEAR);
            default:
                if (i3 == 0 && this.q.h()) {
                    i3 = this.q.g();
                }
                if (i3 == 0) {
                }
                return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.b(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.os.Bundle, int]
     candidates:
      androidx.fragment.app.i.b(androidx.fragment.app.Fragment, android.content.Context, boolean):void
      androidx.fragment.app.i.b(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(Fragment fragment, Bundle bundle, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).b(fragment, bundle, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.b(this, fragment, bundle);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.b(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      androidx.fragment.app.i.b(int, boolean):int
      androidx.fragment.app.i.b(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):boolean
      androidx.fragment.app.i.b(androidx.fragment.app.i$k, boolean):void
      androidx.fragment.app.i.b(androidx.fragment.app.Fragment, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(Fragment fragment, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).b(fragment, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.a(this, fragment);
            }
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:103:0x01ec */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r11v1 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, b.d.b<androidx.fragment.app.Fragment>):int
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.b(androidx.fragment.app.Fragment, android.content.Context, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.content.Context, int]
     candidates:
      androidx.fragment.app.i.b(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
      androidx.fragment.app.i.b(androidx.fragment.app.Fragment, android.content.Context, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.content.Context, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.content.Context, int]
     candidates:
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, androidx.fragment.app.i$g, int):void
      androidx.fragment.app.i.a(java.lang.String, int, int):boolean
      androidx.fragment.app.i.a(android.os.Bundle, java.lang.String, androidx.fragment.app.Fragment):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.g, androidx.fragment.app.d, androidx.fragment.app.Fragment):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.content.Context, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.b(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.os.Bundle, int]
     candidates:
      androidx.fragment.app.i.b(androidx.fragment.app.Fragment, android.content.Context, boolean):void
      androidx.fragment.app.i.b(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, int]
     candidates:
      androidx.fragment.app.i.a(float, float, float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, boolean, int):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(androidx.fragment.app.a, boolean, boolean, boolean):void
      androidx.fragment.app.i.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      androidx.fragment.app.h.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.os.Bundle, int]
     candidates:
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, androidx.fragment.app.i$g, int):void
      androidx.fragment.app.i.a(java.lang.String, int, int):boolean
      androidx.fragment.app.i.a(android.os.Bundle, java.lang.String, androidx.fragment.app.Fragment):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.content.Context, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.g, androidx.fragment.app.d, androidx.fragment.app.Fragment):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, boolean, int):androidx.fragment.app.i$g
     arg types: [androidx.fragment.app.Fragment, int, int, int]
     candidates:
      androidx.fragment.app.i.a(float, float, float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.a, boolean, boolean, boolean):void
      androidx.fragment.app.i.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      androidx.fragment.app.h.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, boolean, int):androidx.fragment.app.i$g */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.b(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      androidx.fragment.app.i.b(int, boolean):int
      androidx.fragment.app.i.b(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):boolean
      androidx.fragment.app.i.b(androidx.fragment.app.i$k, boolean):void
      androidx.fragment.app.i.b(androidx.fragment.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.c(androidx.fragment.app.Fragment, boolean):void
     arg types: [androidx.fragment.app.Fragment, int]
     candidates:
      androidx.fragment.app.i.c(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):void
      androidx.fragment.app.i.c(androidx.fragment.app.Fragment, boolean):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0084, code lost:
        if (r0 != 3) goto L_0x04d6;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x01e9  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01ee  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x02df  */
    /* JADX WARNING: Removed duplicated region for block: B:262:0x04db  */
    /* JADX WARNING: Removed duplicated region for block: B:264:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(androidx.fragment.app.Fragment r19, int r20, int r21, int r22, boolean r23) {
        /*
            r18 = this;
            r6 = r18
            r7 = r19
            boolean r0 = r7.mAdded
            r8 = 1
            if (r0 == 0) goto L_0x0011
            boolean r0 = r7.mDetached
            if (r0 == 0) goto L_0x000e
            goto L_0x0011
        L_0x000e:
            r0 = r20
            goto L_0x0016
        L_0x0011:
            r0 = r20
            if (r0 <= r8) goto L_0x0016
            r0 = 1
        L_0x0016:
            boolean r1 = r7.mRemoving
            if (r1 == 0) goto L_0x002a
            int r1 = r7.mState
            if (r0 <= r1) goto L_0x002a
            if (r1 != 0) goto L_0x0028
            boolean r0 = r19.isInBackStack()
            if (r0 == 0) goto L_0x0028
            r0 = 1
            goto L_0x002a
        L_0x0028:
            int r0 = r7.mState
        L_0x002a:
            boolean r1 = r7.mDeferStart
            r9 = 3
            r10 = 2
            if (r1 == 0) goto L_0x0037
            int r1 = r7.mState
            if (r1 >= r9) goto L_0x0037
            if (r0 <= r10) goto L_0x0037
            r0 = 2
        L_0x0037:
            androidx.lifecycle.e$b r1 = r7.mMaxState
            androidx.lifecycle.e$b r2 = androidx.lifecycle.e.b.CREATED
            if (r1 != r2) goto L_0x0042
            int r0 = java.lang.Math.min(r0, r8)
            goto L_0x004a
        L_0x0042:
            int r1 = r1.ordinal()
            int r0 = java.lang.Math.min(r0, r1)
        L_0x004a:
            r11 = r0
            int r0 = r7.mState
            java.lang.String r12 = "FragmentManager"
            r13 = 0
            r14 = 0
            if (r0 > r11) goto L_0x0325
            boolean r0 = r7.mFromLayout
            if (r0 == 0) goto L_0x005c
            boolean r0 = r7.mInLayout
            if (r0 != 0) goto L_0x005c
            return
        L_0x005c:
            android.view.View r0 = r19.getAnimatingAway()
            if (r0 != 0) goto L_0x0068
            android.animation.Animator r0 = r19.getAnimator()
            if (r0 == 0) goto L_0x007c
        L_0x0068:
            r7.setAnimatingAway(r14)
            r7.setAnimator(r14)
            int r2 = r19.getStateAfterAnimating()
            r3 = 0
            r4 = 0
            r5 = 1
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3, r4, r5)
        L_0x007c:
            int r0 = r7.mState
            if (r0 == 0) goto L_0x008e
            if (r0 == r8) goto L_0x01e7
            if (r0 == r10) goto L_0x008b
            if (r0 == r9) goto L_0x0088
            goto L_0x04d6
        L_0x0088:
            r0 = 3
            goto L_0x02ff
        L_0x008b:
            r0 = 2
            goto L_0x02dd
        L_0x008e:
            if (r11 <= 0) goto L_0x01e7
            boolean r0 = androidx.fragment.app.i.H
            if (r0 == 0) goto L_0x00a8
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "moveto CREATED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r12, r0)
        L_0x00a8:
            android.os.Bundle r0 = r7.mSavedFragmentState
            if (r0 == 0) goto L_0x0101
            androidx.fragment.app.g r1 = r6.q
            android.content.Context r1 = r1.c()
            java.lang.ClassLoader r1 = r1.getClassLoader()
            r0.setClassLoader(r1)
            android.os.Bundle r0 = r7.mSavedFragmentState
            java.lang.String r1 = "android:view_state"
            android.util.SparseArray r0 = r0.getSparseParcelableArray(r1)
            r7.mSavedViewState = r0
            android.os.Bundle r0 = r7.mSavedFragmentState
            java.lang.String r1 = "android:target_state"
            androidx.fragment.app.Fragment r0 = r6.a(r0, r1)
            if (r0 == 0) goto L_0x00d0
            java.lang.String r0 = r0.mWho
            goto L_0x00d1
        L_0x00d0:
            r0 = r14
        L_0x00d1:
            r7.mTargetWho = r0
            java.lang.String r0 = r7.mTargetWho
            if (r0 == 0) goto L_0x00e1
            android.os.Bundle r0 = r7.mSavedFragmentState
            java.lang.String r1 = "android:target_req_state"
            int r0 = r0.getInt(r1, r13)
            r7.mTargetRequestCode = r0
        L_0x00e1:
            java.lang.Boolean r0 = r7.mSavedUserVisibleHint
            if (r0 == 0) goto L_0x00ee
            boolean r0 = r0.booleanValue()
            r7.mUserVisibleHint = r0
            r7.mSavedUserVisibleHint = r14
            goto L_0x00f8
        L_0x00ee:
            android.os.Bundle r0 = r7.mSavedFragmentState
            java.lang.String r1 = "android:user_visible_hint"
            boolean r0 = r0.getBoolean(r1, r8)
            r7.mUserVisibleHint = r0
        L_0x00f8:
            boolean r0 = r7.mUserVisibleHint
            if (r0 != 0) goto L_0x0101
            r7.mDeferStart = r8
            if (r11 <= r10) goto L_0x0101
            r11 = 2
        L_0x0101:
            androidx.fragment.app.g r0 = r6.q
            r7.mHost = r0
            androidx.fragment.app.Fragment r1 = r6.s
            r7.mParentFragment = r1
            if (r1 == 0) goto L_0x010e
            androidx.fragment.app.i r0 = r1.mChildFragmentManager
            goto L_0x0110
        L_0x010e:
            androidx.fragment.app.i r0 = r0.f1490e
        L_0x0110:
            r7.mFragmentManager = r0
            androidx.fragment.app.Fragment r0 = r7.mTarget
            java.lang.String r15 = " that does not belong to this FragmentManager!"
            java.lang.String r5 = " declared target fragment "
            java.lang.String r4 = "Fragment "
            if (r0 == 0) goto L_0x016b
            java.util.HashMap<java.lang.String, androidx.fragment.app.Fragment> r1 = r6.f1497g
            java.lang.String r0 = r0.mWho
            java.lang.Object r0 = r1.get(r0)
            androidx.fragment.app.Fragment r1 = r7.mTarget
            if (r0 != r1) goto L_0x0149
            int r0 = r1.mState
            if (r0 >= r8) goto L_0x013e
            r2 = 1
            r3 = 0
            r16 = 0
            r17 = 1
            r0 = r18
            r9 = r4
            r4 = r16
            r10 = r5
            r5 = r17
            r0.a(r1, r2, r3, r4, r5)
            goto L_0x0140
        L_0x013e:
            r9 = r4
            r10 = r5
        L_0x0140:
            androidx.fragment.app.Fragment r0 = r7.mTarget
            java.lang.String r0 = r0.mWho
            r7.mTargetWho = r0
            r7.mTarget = r14
            goto L_0x016d
        L_0x0149:
            r9 = r4
            r10 = r5
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r9)
            r1.append(r7)
            r1.append(r10)
            androidx.fragment.app.Fragment r2 = r7.mTarget
            r1.append(r2)
            r1.append(r15)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x016b:
            r9 = r4
            r10 = r5
        L_0x016d:
            java.lang.String r0 = r7.mTargetWho
            if (r0 == 0) goto L_0x01aa
            java.util.HashMap<java.lang.String, androidx.fragment.app.Fragment> r1 = r6.f1497g
            java.lang.Object r0 = r1.get(r0)
            r1 = r0
            androidx.fragment.app.Fragment r1 = (androidx.fragment.app.Fragment) r1
            if (r1 == 0) goto L_0x018a
            int r0 = r1.mState
            if (r0 >= r8) goto L_0x01aa
            r2 = 1
            r3 = 0
            r4 = 0
            r5 = 1
            r0 = r18
            r0.a(r1, r2, r3, r4, r5)
            goto L_0x01aa
        L_0x018a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r9)
            r1.append(r7)
            r1.append(r10)
            java.lang.String r2 = r7.mTargetWho
            r1.append(r2)
            r1.append(r15)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x01aa:
            androidx.fragment.app.g r0 = r6.q
            android.content.Context r0 = r0.c()
            r6.b(r7, r0, r13)
            r19.performAttach()
            androidx.fragment.app.Fragment r0 = r7.mParentFragment
            if (r0 != 0) goto L_0x01c0
            androidx.fragment.app.g r0 = r6.q
            r0.a(r7)
            goto L_0x01c3
        L_0x01c0:
            r0.onAttachFragment(r7)
        L_0x01c3:
            androidx.fragment.app.g r0 = r6.q
            android.content.Context r0 = r0.c()
            r6.a(r7, r0, r13)
            boolean r0 = r7.mIsCreated
            if (r0 != 0) goto L_0x01e0
            android.os.Bundle r0 = r7.mSavedFragmentState
            r6.c(r7, r0, r13)
            android.os.Bundle r0 = r7.mSavedFragmentState
            r7.performCreate(r0)
            android.os.Bundle r0 = r7.mSavedFragmentState
            r6.b(r7, r0, r13)
            goto L_0x01e7
        L_0x01e0:
            android.os.Bundle r0 = r7.mSavedFragmentState
            r7.restoreChildFragmentState(r0)
            r7.mState = r8
        L_0x01e7:
            if (r11 <= 0) goto L_0x01ec
            r18.e(r19)
        L_0x01ec:
            if (r11 <= r8) goto L_0x008b
            boolean r0 = androidx.fragment.app.i.H
            if (r0 == 0) goto L_0x0206
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "moveto ACTIVITY_CREATED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r12, r0)
        L_0x0206:
            boolean r0 = r7.mFromLayout
            if (r0 != 0) goto L_0x02c6
            int r0 = r7.mContainerId
            if (r0 == 0) goto L_0x027c
            r1 = -1
            if (r0 == r1) goto L_0x025d
            androidx.fragment.app.d r1 = r6.r
            android.view.View r0 = r1.a(r0)
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            if (r0 != 0) goto L_0x027d
            boolean r1 = r7.mRestored
            if (r1 == 0) goto L_0x0220
            goto L_0x027d
        L_0x0220:
            android.content.res.Resources r0 = r19.getResources()     // Catch:{ NotFoundException -> 0x022b }
            int r1 = r7.mContainerId     // Catch:{ NotFoundException -> 0x022b }
            java.lang.String r0 = r0.getResourceName(r1)     // Catch:{ NotFoundException -> 0x022b }
            goto L_0x022d
        L_0x022b:
            java.lang.String r0 = "unknown"
        L_0x022d:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "No view found for id 0x"
            r2.append(r3)
            int r3 = r7.mContainerId
            java.lang.String r3 = java.lang.Integer.toHexString(r3)
            r2.append(r3)
            java.lang.String r3 = " ("
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = ") for fragment "
            r2.append(r0)
            r2.append(r7)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            r6.a(r1)
            throw r14
        L_0x025d:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Cannot create fragment "
            r1.append(r2)
            r1.append(r7)
            java.lang.String r2 = " for a container view with no id"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            r6.a(r0)
            throw r14
        L_0x027c:
            r0 = r14
        L_0x027d:
            r7.mContainer = r0
            android.os.Bundle r1 = r7.mSavedFragmentState
            android.view.LayoutInflater r1 = r7.performGetLayoutInflater(r1)
            android.os.Bundle r2 = r7.mSavedFragmentState
            r7.performCreateView(r1, r0, r2)
            android.view.View r1 = r7.mView
            if (r1 == 0) goto L_0x02c4
            r7.mInnerView = r1
            r1.setSaveFromParentEnabled(r13)
            if (r0 == 0) goto L_0x029a
            android.view.View r1 = r7.mView
            r0.addView(r1)
        L_0x029a:
            boolean r0 = r7.mHidden
            if (r0 == 0) goto L_0x02a5
            android.view.View r0 = r7.mView
            r1 = 8
            r0.setVisibility(r1)
        L_0x02a5:
            android.view.View r0 = r7.mView
            android.os.Bundle r1 = r7.mSavedFragmentState
            r7.onViewCreated(r0, r1)
            android.view.View r0 = r7.mView
            android.os.Bundle r1 = r7.mSavedFragmentState
            r6.a(r7, r0, r1, r13)
            android.view.View r0 = r7.mView
            int r0 = r0.getVisibility()
            if (r0 != 0) goto L_0x02c0
            android.view.ViewGroup r0 = r7.mContainer
            if (r0 == 0) goto L_0x02c0
            goto L_0x02c1
        L_0x02c0:
            r8 = 0
        L_0x02c1:
            r7.mIsNewlyAdded = r8
            goto L_0x02c6
        L_0x02c4:
            r7.mInnerView = r14
        L_0x02c6:
            android.os.Bundle r0 = r7.mSavedFragmentState
            r7.performActivityCreated(r0)
            android.os.Bundle r0 = r7.mSavedFragmentState
            r6.a(r7, r0, r13)
            android.view.View r0 = r7.mView
            if (r0 == 0) goto L_0x02d9
            android.os.Bundle r0 = r7.mSavedFragmentState
            r7.restoreViewState(r0)
        L_0x02d9:
            r7.mSavedFragmentState = r14
            goto L_0x008b
        L_0x02dd:
            if (r11 <= r0) goto L_0x0088
            boolean r0 = androidx.fragment.app.i.H
            if (r0 == 0) goto L_0x02f7
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "moveto STARTED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r12, r0)
        L_0x02f7:
            r19.performStart()
            r6.f(r7, r13)
            goto L_0x0088
        L_0x02ff:
            if (r11 <= r0) goto L_0x04d6
            boolean r0 = androidx.fragment.app.i.H
            if (r0 == 0) goto L_0x0319
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "moveto RESUMED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r12, r0)
        L_0x0319:
            r19.performResume()
            r6.e(r7, r13)
            r7.mSavedFragmentState = r14
            r7.mSavedViewState = r14
            goto L_0x04d6
        L_0x0325:
            if (r0 <= r11) goto L_0x04d6
            if (r0 == r8) goto L_0x0405
            r1 = 2
            if (r0 == r1) goto L_0x0375
            r1 = 3
            if (r0 == r1) goto L_0x0354
            r1 = 4
            if (r0 == r1) goto L_0x0334
            goto L_0x04d6
        L_0x0334:
            if (r11 >= r1) goto L_0x0354
            boolean r0 = androidx.fragment.app.i.H
            if (r0 == 0) goto L_0x034e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "movefrom RESUMED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r12, r0)
        L_0x034e:
            r19.performPause()
            r6.d(r7, r13)
        L_0x0354:
            r0 = 3
            if (r11 >= r0) goto L_0x0375
            boolean r0 = androidx.fragment.app.i.H
            if (r0 == 0) goto L_0x036f
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "movefrom STARTED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r12, r0)
        L_0x036f:
            r19.performStop()
            r6.g(r7, r13)
        L_0x0375:
            r0 = 2
            if (r11 >= r0) goto L_0x0405
            boolean r0 = androidx.fragment.app.i.H
            if (r0 == 0) goto L_0x0390
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "movefrom ACTIVITY_CREATED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r12, r0)
        L_0x0390:
            android.view.View r0 = r7.mView
            if (r0 == 0) goto L_0x03a3
            androidx.fragment.app.g r0 = r6.q
            boolean r0 = r0.b(r7)
            if (r0 == 0) goto L_0x03a3
            android.util.SparseArray<android.os.Parcelable> r0 = r7.mSavedViewState
            if (r0 != 0) goto L_0x03a3
            r18.r(r19)
        L_0x03a3:
            r19.performDestroyView()
            r6.h(r7, r13)
            android.view.View r0 = r7.mView
            if (r0 == 0) goto L_0x03f6
            android.view.ViewGroup r1 = r7.mContainer
            if (r1 == 0) goto L_0x03f6
            r1.endViewTransition(r0)
            android.view.View r0 = r7.mView
            r0.clearAnimation()
            androidx.fragment.app.Fragment r0 = r19.getParentFragment()
            if (r0 == 0) goto L_0x03c7
            androidx.fragment.app.Fragment r0 = r19.getParentFragment()
            boolean r0 = r0.mRemoving
            if (r0 != 0) goto L_0x03f6
        L_0x03c7:
            int r0 = r6.p
            r1 = 0
            if (r0 <= 0) goto L_0x03e7
            boolean r0 = r6.x
            if (r0 != 0) goto L_0x03e7
            android.view.View r0 = r7.mView
            int r0 = r0.getVisibility()
            if (r0 != 0) goto L_0x03e7
            float r0 = r7.mPostponedAlpha
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x03e7
            r0 = r21
            r2 = r22
            androidx.fragment.app.i$g r0 = r6.a(r7, r0, r13, r2)
            goto L_0x03e8
        L_0x03e7:
            r0 = r14
        L_0x03e8:
            r7.mPostponedAlpha = r1
            if (r0 == 0) goto L_0x03ef
            r6.a(r7, r0, r11)
        L_0x03ef:
            android.view.ViewGroup r0 = r7.mContainer
            android.view.View r1 = r7.mView
            r0.removeView(r1)
        L_0x03f6:
            r7.mContainer = r14
            r7.mView = r14
            r7.mViewLifecycleOwner = r14
            androidx.lifecycle.n<androidx.lifecycle.h> r0 = r7.mViewLifecycleOwnerLiveData
            r0.b(r14)
            r7.mInnerView = r14
            r7.mInLayout = r13
        L_0x0405:
            if (r11 >= r8) goto L_0x04d6
            boolean r0 = r6.x
            if (r0 == 0) goto L_0x042c
            android.view.View r0 = r19.getAnimatingAway()
            if (r0 == 0) goto L_0x041c
            android.view.View r0 = r19.getAnimatingAway()
            r7.setAnimatingAway(r14)
            r0.clearAnimation()
            goto L_0x042c
        L_0x041c:
            android.animation.Animator r0 = r19.getAnimator()
            if (r0 == 0) goto L_0x042c
            android.animation.Animator r0 = r19.getAnimator()
            r7.setAnimator(r14)
            r0.cancel()
        L_0x042c:
            android.view.View r0 = r19.getAnimatingAway()
            if (r0 != 0) goto L_0x04d2
            android.animation.Animator r0 = r19.getAnimator()
            if (r0 == 0) goto L_0x043a
            goto L_0x04d2
        L_0x043a:
            boolean r0 = androidx.fragment.app.i.H
            if (r0 == 0) goto L_0x0452
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "movefrom CREATED: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r12, r0)
        L_0x0452:
            boolean r0 = r7.mRemoving
            if (r0 == 0) goto L_0x045e
            boolean r0 = r19.isInBackStack()
            if (r0 != 0) goto L_0x045e
            r0 = 1
            goto L_0x045f
        L_0x045e:
            r0 = 0
        L_0x045f:
            if (r0 != 0) goto L_0x046d
            androidx.fragment.app.j r1 = r6.F
            boolean r1 = r1.f(r7)
            if (r1 == 0) goto L_0x046a
            goto L_0x046d
        L_0x046a:
            r7.mState = r13
            goto L_0x049e
        L_0x046d:
            androidx.fragment.app.g r1 = r6.q
            boolean r2 = r1 instanceof androidx.lifecycle.v
            if (r2 == 0) goto L_0x047a
            androidx.fragment.app.j r1 = r6.F
            boolean r8 = r1.d()
            goto L_0x048f
        L_0x047a:
            android.content.Context r1 = r1.c()
            boolean r1 = r1 instanceof android.app.Activity
            if (r1 == 0) goto L_0x048f
            androidx.fragment.app.g r1 = r6.q
            android.content.Context r1 = r1.c()
            android.app.Activity r1 = (android.app.Activity) r1
            boolean r1 = r1.isChangingConfigurations()
            r8 = r8 ^ r1
        L_0x048f:
            if (r0 != 0) goto L_0x0493
            if (r8 == 0) goto L_0x0498
        L_0x0493:
            androidx.fragment.app.j r1 = r6.F
            r1.b(r7)
        L_0x0498:
            r19.performDestroy()
            r6.b(r7, r13)
        L_0x049e:
            r19.performDetach()
            r6.c(r7, r13)
            if (r23 != 0) goto L_0x04d6
            if (r0 != 0) goto L_0x04ce
            androidx.fragment.app.j r0 = r6.F
            boolean r0 = r0.f(r7)
            if (r0 == 0) goto L_0x04b1
            goto L_0x04ce
        L_0x04b1:
            r7.mHost = r14
            r7.mParentFragment = r14
            r7.mFragmentManager = r14
            java.lang.String r0 = r7.mTargetWho
            if (r0 == 0) goto L_0x04d6
            java.util.HashMap<java.lang.String, androidx.fragment.app.Fragment> r1 = r6.f1497g
            java.lang.Object r0 = r1.get(r0)
            androidx.fragment.app.Fragment r0 = (androidx.fragment.app.Fragment) r0
            if (r0 == 0) goto L_0x04d6
            boolean r1 = r0.getRetainInstance()
            if (r1 == 0) goto L_0x04d6
            r7.mTarget = r0
            goto L_0x04d6
        L_0x04ce:
            r18.k(r19)
            goto L_0x04d6
        L_0x04d2:
            r7.setStateAfterAnimating(r11)
            goto L_0x04d7
        L_0x04d6:
            r8 = r11
        L_0x04d7:
            int r0 = r7.mState
            if (r0 == r8) goto L_0x0503
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "moveToState: Fragment state for "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r1 = " not updated inline; expected state "
            r0.append(r1)
            r0.append(r8)
            java.lang.String r1 = " found "
            r0.append(r1)
            int r1 = r7.mState
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.w(r12, r0)
            r7.mState = r8
        L_0x0503:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void");
    }

    private void a(Fragment fragment, g gVar, int i2) {
        View view = fragment.mView;
        ViewGroup viewGroup = fragment.mContainer;
        viewGroup.startViewTransition(view);
        fragment.setStateAfterAnimating(i2);
        android.view.animation.Animation animation = gVar.f1516a;
        if (animation != null) {
            h hVar = new h(animation, viewGroup, view);
            fragment.setAnimatingAway(fragment.mView);
            hVar.setAnimationListener(new c(viewGroup, fragment));
            fragment.mView.startAnimation(hVar);
            return;
        }
        Animator animator = gVar.f1517b;
        fragment.setAnimator(animator);
        animator.addListener(new d(viewGroup, view, fragment));
        animator.setTarget(fragment.mView);
        animator.start();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2) {
        g gVar;
        if (this.q == null && i2 != 0) {
            throw new IllegalStateException("No activity");
        } else if (z2 || i2 != this.p) {
            this.p = i2;
            int size = this.f1496f.size();
            for (int i3 = 0; i3 < size; i3++) {
                l(this.f1496f.get(i3));
            }
            for (Fragment next : this.f1497g.values()) {
                if (next != null && ((next.mRemoving || next.mDetached) && !next.mIsNewlyAdded)) {
                    l(next);
                }
            }
            B();
            if (this.u && (gVar = this.q) != null && this.p == 4) {
                gVar.i();
                this.u = false;
            }
        }
    }

    public void a(Fragment fragment, boolean z2) {
        if (H) {
            Log.v("FragmentManager", "add: " + fragment);
        }
        j(fragment);
        if (fragment.mDetached) {
            return;
        }
        if (!this.f1496f.contains(fragment)) {
            synchronized (this.f1496f) {
                this.f1496f.add(fragment);
            }
            fragment.mAdded = true;
            fragment.mRemoving = false;
            if (fragment.mView == null) {
                fragment.mHiddenChanged = false;
            }
            if (w(fragment)) {
                this.u = true;
            }
            if (z2) {
                m(fragment);
                return;
            }
            return;
        }
        throw new IllegalStateException("Fragment already added: " + fragment);
    }

    public Fragment a(int i2) {
        for (int size = this.f1496f.size() - 1; size >= 0; size--) {
            Fragment fragment = this.f1496f.get(size);
            if (fragment != null && fragment.mFragmentId == i2) {
                return fragment;
            }
        }
        for (Fragment next : this.f1497g.values()) {
            if (next != null && next.mFragmentId == i2) {
                return next;
            }
        }
        return null;
    }

    public Fragment a(String str) {
        if (str != null) {
            for (int size = this.f1496f.size() - 1; size >= 0; size--) {
                Fragment fragment = this.f1496f.get(size);
                if (fragment != null && str.equals(fragment.mTag)) {
                    return fragment;
                }
            }
        }
        if (str == null) {
            return null;
        }
        for (Fragment next : this.f1497g.values()) {
            if (next != null && str.equals(next.mTag)) {
                return next;
            }
        }
        return null;
    }

    public void a(k kVar, boolean z2) {
        if (!z2) {
            D();
        }
        synchronized (this) {
            if (!this.x) {
                if (this.q != null) {
                    if (this.f1493c == null) {
                        this.f1493c = new ArrayList<>();
                    }
                    this.f1493c.add(kVar);
                    A();
                    return;
                }
            }
            if (!z2) {
                throw new IllegalStateException("Activity has been destroyed");
            }
        }
    }

    public void a(int i2, a aVar) {
        synchronized (this) {
            if (this.l == null) {
                this.l = new ArrayList<>();
            }
            int size = this.l.size();
            if (i2 < size) {
                if (H) {
                    Log.v("FragmentManager", "Setting back stack index " + i2 + " to " + aVar);
                }
                this.l.set(i2, aVar);
            } else {
                while (size < i2) {
                    this.l.add(null);
                    if (this.m == null) {
                        this.m = new ArrayList<>();
                    }
                    if (H) {
                        Log.v("FragmentManager", "Adding available back stack index " + size);
                    }
                    this.m.add(Integer.valueOf(size));
                    size++;
                }
                if (H) {
                    Log.v("FragmentManager", "Adding back stack index " + i2 + " with " + aVar);
                }
                this.l.add(aVar);
            }
        }
    }

    private void a(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2) {
        int indexOf;
        int indexOf2;
        ArrayList<m> arrayList3 = this.E;
        int size = arrayList3 == null ? 0 : arrayList3.size();
        int i2 = 0;
        while (i2 < size) {
            m mVar = this.E.get(i2);
            if (arrayList != null && !mVar.f1530a && (indexOf2 = arrayList.indexOf(mVar.f1531b)) != -1 && arrayList2.get(indexOf2).booleanValue()) {
                this.E.remove(i2);
                i2--;
                size--;
                mVar.c();
            } else if (mVar.e() || (arrayList != null && mVar.f1531b.a(arrayList, 0, arrayList.size()))) {
                this.E.remove(i2);
                i2--;
                size--;
                if (arrayList == null || mVar.f1530a || (indexOf = arrayList.indexOf(mVar.f1531b)) == -1 || !arrayList2.get(indexOf).booleanValue()) {
                    mVar.d();
                } else {
                    mVar.c();
                }
            }
            i2++;
        }
    }

    private int a(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3, b.d.b<Fragment> bVar) {
        int i4 = i3;
        for (int i5 = i3 - 1; i5 >= i2; i5--) {
            a aVar = arrayList.get(i5);
            boolean booleanValue = arrayList2.get(i5).booleanValue();
            if (aVar.g() && !aVar.a(arrayList, i5 + 1, i3)) {
                if (this.E == null) {
                    this.E = new ArrayList<>();
                }
                m mVar = new m(aVar, booleanValue);
                this.E.add(mVar);
                aVar.a(mVar);
                if (booleanValue) {
                    aVar.e();
                } else {
                    aVar.b(false);
                }
                i4--;
                if (i5 != i4) {
                    arrayList.remove(i5);
                    arrayList.add(i4, aVar);
                }
                a(bVar);
            }
        }
        return i4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.l.a(androidx.fragment.app.i, java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, boolean):void
     arg types: [androidx.fragment.app.i, java.util.ArrayList, java.util.ArrayList, int, int, int]
     candidates:
      androidx.fragment.app.l.a(androidx.fragment.app.n, java.lang.Object, java.lang.Object, java.lang.Object, androidx.fragment.app.Fragment, boolean):java.lang.Object
      androidx.fragment.app.l.a(androidx.fragment.app.n, java.lang.Object, java.lang.Object, b.d.a<java.lang.String, android.view.View>, boolean, androidx.fragment.app.a):void
      androidx.fragment.app.l.a(androidx.fragment.app.i, java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(int, boolean):void
     arg types: [int, int]
     candidates:
      androidx.fragment.app.i.a(float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>):void
      androidx.fragment.app.i.a(android.os.Bundle, java.lang.String):androidx.fragment.app.Fragment
      androidx.fragment.app.i.a(int, int):void
      androidx.fragment.app.i.a(int, androidx.fragment.app.a):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, androidx.lifecycle.e$b):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.i$k, boolean):void
      androidx.fragment.app.i.a(android.view.Menu, android.view.MenuInflater):boolean
      androidx.fragment.app.h.a(int, int):void
      androidx.fragment.app.i.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(a aVar, boolean z2, boolean z3, boolean z4) {
        if (z2) {
            aVar.b(z4);
        } else {
            aVar.e();
        }
        ArrayList arrayList = new ArrayList(1);
        ArrayList arrayList2 = new ArrayList(1);
        arrayList.add(aVar);
        arrayList2.add(Boolean.valueOf(z2));
        if (z3) {
            l.a(this, (ArrayList<a>) arrayList, (ArrayList<Boolean>) arrayList2, 0, 1, true);
        }
        if (z4) {
            a(this.p, true);
        }
        for (Fragment next : this.f1497g.values()) {
            if (next != null && next.mView != null && next.mIsNewlyAdded && aVar.b(next.mContainerId)) {
                float f2 = next.mPostponedAlpha;
                if (f2 > Animation.CurveTimeline.LINEAR) {
                    next.mView.setAlpha(f2);
                }
                if (z4) {
                    next.mPostponedAlpha = Animation.CurveTimeline.LINEAR;
                } else {
                    next.mPostponedAlpha = -1.0f;
                    next.mIsNewlyAdded = false;
                }
            }
        }
    }

    private static void a(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        while (i2 < i3) {
            a aVar = arrayList.get(i2);
            boolean z2 = true;
            if (arrayList2.get(i2).booleanValue()) {
                aVar.a(-1);
                if (i2 != i3 - 1) {
                    z2 = false;
                }
                aVar.b(z2);
            } else {
                aVar.a(1);
                aVar.e();
            }
            i2++;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, b.d.b<androidx.fragment.app.Fragment>):int
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    private void a(b.d.b<Fragment> bVar) {
        int i2 = this.p;
        if (i2 >= 1) {
            int min = Math.min(i2, 3);
            int size = this.f1496f.size();
            for (int i3 = 0; i3 < size; i3++) {
                Fragment fragment = this.f1496f.get(i3);
                if (fragment.mState < min) {
                    a(fragment, min, fragment.getNextAnim(), fragment.getNextTransition(), false);
                    if (fragment.mView != null && !fragment.mHidden && fragment.mIsNewlyAdded) {
                        bVar.add(fragment);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        if (this.f1498h == null) {
            this.f1498h = new ArrayList<>();
        }
        this.f1498h.add(aVar);
    }

    /* access modifiers changed from: package-private */
    public boolean a(ArrayList<a> arrayList, ArrayList<Boolean> arrayList2, String str, int i2, int i3) {
        int i4;
        ArrayList<a> arrayList3 = this.f1498h;
        if (arrayList3 == null) {
            return false;
        }
        if (str == null && i2 < 0 && (i3 & 1) == 0) {
            int size = arrayList3.size() - 1;
            if (size < 0) {
                return false;
            }
            arrayList.add(this.f1498h.remove(size));
            arrayList2.add(true);
        } else {
            if (str != null || i2 >= 0) {
                i4 = this.f1498h.size() - 1;
                while (i4 >= 0) {
                    a aVar = this.f1498h.get(i4);
                    if ((str != null && str.equals(aVar.f())) || (i2 >= 0 && i2 == aVar.t)) {
                        break;
                    }
                    i4--;
                }
                if (i4 < 0) {
                    return false;
                }
                if ((i3 & 1) != 0) {
                    while (true) {
                        i4--;
                        if (i4 < 0) {
                            break;
                        }
                        a aVar2 = this.f1498h.get(i4);
                        if ((str == null || !str.equals(aVar2.f())) && (i2 < 0 || i2 != aVar2.t)) {
                            break;
                        }
                    }
                }
            } else {
                i4 = -1;
            }
            if (i4 == this.f1498h.size() - 1) {
                return false;
            }
            for (int size2 = this.f1498h.size() - 1; size2 > i4; size2--) {
                arrayList.add(this.f1498h.remove(size2));
                arrayList2.add(true);
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int, b.d.b<androidx.fragment.app.Fragment>):int
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.a.a(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      androidx.fragment.app.a.a(java.util.ArrayList<androidx.fragment.app.a>, int, int):boolean
      androidx.fragment.app.k.a(int, androidx.fragment.app.Fragment, java.lang.String):androidx.fragment.app.k
      androidx.fragment.app.a.a(java.lang.String, java.io.PrintWriter, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(Parcelable parcelable) {
        FragmentState fragmentState;
        if (parcelable != null) {
            FragmentManagerState fragmentManagerState = (FragmentManagerState) parcelable;
            if (fragmentManagerState.f1466a != null) {
                for (Fragment next : this.F.c()) {
                    if (H) {
                        Log.v("FragmentManager", "restoreSaveState: re-attaching retained " + next);
                    }
                    Iterator<FragmentState> it = fragmentManagerState.f1466a.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            fragmentState = null;
                            break;
                        }
                        fragmentState = it.next();
                        if (fragmentState.f1472b.equals(next.mWho)) {
                            break;
                        }
                    }
                    if (fragmentState == null) {
                        if (H) {
                            Log.v("FragmentManager", "Discarding retained Fragment " + next + " that was not found in the set of active Fragments " + fragmentManagerState.f1466a);
                        }
                        Fragment fragment = next;
                        a(fragment, 1, 0, 0, false);
                        next.mRemoving = true;
                        a(fragment, 0, 0, 0, false);
                    } else {
                        fragmentState.n = next;
                        next.mSavedViewState = null;
                        next.mBackStackNesting = 0;
                        next.mInLayout = false;
                        next.mAdded = false;
                        Fragment fragment2 = next.mTarget;
                        next.mTargetWho = fragment2 != null ? fragment2.mWho : null;
                        next.mTarget = null;
                        Bundle bundle = fragmentState.m;
                        if (bundle != null) {
                            bundle.setClassLoader(this.q.c().getClassLoader());
                            next.mSavedViewState = fragmentState.m.getSparseParcelableArray("android:view_state");
                            next.mSavedFragmentState = fragmentState.m;
                        }
                    }
                }
                this.f1497g.clear();
                Iterator<FragmentState> it2 = fragmentManagerState.f1466a.iterator();
                while (it2.hasNext()) {
                    FragmentState next2 = it2.next();
                    if (next2 != null) {
                        Fragment a2 = next2.a(this.q.c().getClassLoader(), b());
                        a2.mFragmentManager = this;
                        if (H) {
                            Log.v("FragmentManager", "restoreSaveState: active (" + a2.mWho + "): " + a2);
                        }
                        this.f1497g.put(a2.mWho, a2);
                        next2.n = null;
                    }
                }
                this.f1496f.clear();
                ArrayList<String> arrayList = fragmentManagerState.f1467b;
                if (arrayList != null) {
                    Iterator<String> it3 = arrayList.iterator();
                    while (it3.hasNext()) {
                        String next3 = it3.next();
                        Fragment fragment3 = this.f1497g.get(next3);
                        if (fragment3 != null) {
                            fragment3.mAdded = true;
                            if (H) {
                                Log.v("FragmentManager", "restoreSaveState: added (" + next3 + "): " + fragment3);
                            }
                            if (!this.f1496f.contains(fragment3)) {
                                synchronized (this.f1496f) {
                                    this.f1496f.add(fragment3);
                                }
                            } else {
                                throw new IllegalStateException("Already added " + fragment3);
                            }
                        } else {
                            a(new IllegalStateException("No instantiated fragment for (" + next3 + ")"));
                            throw null;
                        }
                    }
                }
                BackStackState[] backStackStateArr = fragmentManagerState.f1468c;
                if (backStackStateArr != null) {
                    this.f1498h = new ArrayList<>(backStackStateArr.length);
                    int i2 = 0;
                    while (true) {
                        BackStackState[] backStackStateArr2 = fragmentManagerState.f1468c;
                        if (i2 >= backStackStateArr2.length) {
                            break;
                        }
                        a a3 = backStackStateArr2[i2].a(this);
                        if (H) {
                            Log.v("FragmentManager", "restoreAllState: back stack #" + i2 + " (index " + a3.t + "): " + a3);
                            PrintWriter printWriter = new PrintWriter(new b.e.l.b("FragmentManager"));
                            a3.a("  ", printWriter, false);
                            printWriter.close();
                        }
                        this.f1498h.add(a3);
                        int i3 = a3.t;
                        if (i3 >= 0) {
                            a(i3, a3);
                        }
                        i2++;
                    }
                } else {
                    this.f1498h = null;
                }
                String str = fragmentManagerState.f1469d;
                if (str != null) {
                    this.t = this.f1497g.get(str);
                    u(this.t);
                }
                this.f1495e = fragmentManagerState.f1470e;
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: androidx.activity.c} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v8, resolved type: androidx.fragment.app.Fragment} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v9, resolved type: androidx.fragment.app.Fragment} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: androidx.fragment.app.Fragment} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(androidx.fragment.app.g r3, androidx.fragment.app.d r4, androidx.fragment.app.Fragment r5) {
        /*
            r2 = this;
            androidx.fragment.app.g r0 = r2.q
            if (r0 != 0) goto L_0x004d
            r2.q = r3
            r2.r = r4
            r2.s = r5
            androidx.fragment.app.Fragment r4 = r2.s
            if (r4 == 0) goto L_0x0011
            r2.H()
        L_0x0011:
            boolean r4 = r3 instanceof androidx.activity.c
            if (r4 == 0) goto L_0x0028
            r4 = r3
            androidx.activity.c r4 = (androidx.activity.c) r4
            androidx.activity.OnBackPressedDispatcher r0 = r4.getOnBackPressedDispatcher()
            r2.f1500j = r0
            if (r5 == 0) goto L_0x0021
            r4 = r5
        L_0x0021:
            androidx.activity.OnBackPressedDispatcher r0 = r2.f1500j
            androidx.activity.b r1 = r2.f1501k
            r0.a(r4, r1)
        L_0x0028:
            if (r5 == 0) goto L_0x0033
            androidx.fragment.app.i r3 = r5.mFragmentManager
            androidx.fragment.app.j r3 = r3.f(r5)
            r2.F = r3
            goto L_0x004c
        L_0x0033:
            boolean r4 = r3 instanceof androidx.lifecycle.v
            if (r4 == 0) goto L_0x0044
            androidx.lifecycle.v r3 = (androidx.lifecycle.v) r3
            androidx.lifecycle.u r3 = r3.getViewModelStore()
            androidx.fragment.app.j r3 = androidx.fragment.app.j.a(r3)
            r2.F = r3
            goto L_0x004c
        L_0x0044:
            androidx.fragment.app.j r3 = new androidx.fragment.app.j
            r4 = 0
            r3.<init>(r4)
            r2.F = r3
        L_0x004c:
            return
        L_0x004d:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.String r4 = "Already attached"
            r3.<init>(r4)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.fragment.app.i.a(androidx.fragment.app.g, androidx.fragment.app.d, androidx.fragment.app.Fragment):void");
    }

    public void a(boolean z2) {
        for (int size = this.f1496f.size() - 1; size >= 0; size--) {
            Fragment fragment = this.f1496f.get(size);
            if (fragment != null) {
                fragment.performMultiWindowModeChanged(z2);
            }
        }
    }

    public void a(Configuration configuration) {
        for (int i2 = 0; i2 < this.f1496f.size(); i2++) {
            Fragment fragment = this.f1496f.get(i2);
            if (fragment != null) {
                fragment.performConfigurationChanged(configuration);
            }
        }
    }

    public boolean a(Menu menu, MenuInflater menuInflater) {
        if (this.p < 1) {
            return false;
        }
        ArrayList<Fragment> arrayList = null;
        boolean z2 = false;
        for (int i2 = 0; i2 < this.f1496f.size(); i2++) {
            Fragment fragment = this.f1496f.get(i2);
            if (fragment != null && fragment.performCreateOptionsMenu(menu, menuInflater)) {
                if (arrayList == null) {
                    arrayList = new ArrayList<>();
                }
                arrayList.add(fragment);
                z2 = true;
            }
        }
        if (this.f1499i != null) {
            for (int i3 = 0; i3 < this.f1499i.size(); i3++) {
                Fragment fragment2 = this.f1499i.get(i3);
                if (arrayList == null || !arrayList.contains(fragment2)) {
                    fragment2.onDestroyOptionsMenu();
                }
            }
        }
        this.f1499i = arrayList;
        return z2;
    }

    public boolean a(MenuItem menuItem) {
        if (this.p < 1) {
            return false;
        }
        for (int i2 = 0; i2 < this.f1496f.size(); i2++) {
            Fragment fragment = this.f1496f.get(i2);
            if (fragment != null && fragment.performContextItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }

    public void a(Menu menu) {
        if (this.p >= 1) {
            for (int i2 = 0; i2 < this.f1496f.size(); i2++) {
                Fragment fragment = this.f1496f.get(i2);
                if (fragment != null) {
                    fragment.performOptionsMenuClosed(menu);
                }
            }
        }
    }

    public void a(Fragment fragment, e.b bVar) {
        if (this.f1497g.get(fragment.mWho) == fragment && (fragment.mHost == null || fragment.getFragmentManager() == this)) {
            fragment.mMaxState = bVar;
            return;
        }
        throw new IllegalArgumentException("Fragment " + fragment + " is not an active fragment of FragmentManager " + this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.content.Context, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.content.Context, int]
     candidates:
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, androidx.fragment.app.i$g, int):void
      androidx.fragment.app.i.a(java.lang.String, int, int):boolean
      androidx.fragment.app.i.a(android.os.Bundle, java.lang.String, androidx.fragment.app.Fragment):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.g, androidx.fragment.app.d, androidx.fragment.app.Fragment):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.content.Context, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, Context context, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).a(fragment, context, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.a(this, fragment, context);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.os.Bundle, int]
     candidates:
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, androidx.fragment.app.i$g, int):void
      androidx.fragment.app.i.a(java.lang.String, int, int):boolean
      androidx.fragment.app.i.a(android.os.Bundle, java.lang.String, androidx.fragment.app.Fragment):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.content.Context, boolean):void
      androidx.fragment.app.i.a(androidx.fragment.app.g, androidx.fragment.app.d, androidx.fragment.app.Fragment):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.os.Bundle, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, Bundle bundle, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).a(fragment, bundle, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.a(this, fragment, bundle);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void
     arg types: [androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, int]
     candidates:
      androidx.fragment.app.i.a(float, float, float, float):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(java.util.ArrayList<androidx.fragment.app.a>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, int, boolean, int):androidx.fragment.app.i$g
      androidx.fragment.app.i.a(androidx.fragment.app.a, boolean, boolean, boolean):void
      androidx.fragment.app.i.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      androidx.fragment.app.h.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      androidx.fragment.app.i.a(androidx.fragment.app.Fragment, android.view.View, android.os.Bundle, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, View view, Bundle bundle, boolean z2) {
        Fragment fragment2 = this.s;
        if (fragment2 != null) {
            h fragmentManager = fragment2.getFragmentManager();
            if (fragmentManager instanceof i) {
                ((i) fragmentManager).a(fragment, view, bundle, true);
            }
        }
        Iterator<C0015i> it = this.o.iterator();
        while (it.hasNext()) {
            C0015i next = it.next();
            if (!z2 || next.f1524b) {
                next.f1523a.a(this, fragment, view, bundle);
            }
        }
    }
}
