package androidx.fragment.app;

import android.util.Log;
import androidx.lifecycle.s;
import androidx.lifecycle.t;
import androidx.lifecycle.u;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/* compiled from: FragmentManagerViewModel */
class j extends s {

    /* renamed from: h  reason: collision with root package name */
    private static final t.a f1533h = new a();

    /* renamed from: b  reason: collision with root package name */
    private final HashSet<Fragment> f1534b = new HashSet<>();

    /* renamed from: c  reason: collision with root package name */
    private final HashMap<String, j> f1535c = new HashMap<>();

    /* renamed from: d  reason: collision with root package name */
    private final HashMap<String, u> f1536d = new HashMap<>();

    /* renamed from: e  reason: collision with root package name */
    private final boolean f1537e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f1538f = false;

    /* renamed from: g  reason: collision with root package name */
    private boolean f1539g = false;

    /* compiled from: FragmentManagerViewModel */
    static class a implements t.a {
        a() {
        }

        public <T extends s> T a(Class<T> cls) {
            return new j(true);
        }
    }

    j(boolean z) {
        this.f1537e = z;
    }

    static j a(u uVar) {
        return (j) new t(uVar, f1533h).a(j.class);
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (i.H) {
            Log.d("FragmentManager", "onCleared called for " + this);
        }
        this.f1538f = true;
    }

    /* access modifiers changed from: package-private */
    public Collection<Fragment> c() {
        return this.f1534b;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.f1538f;
    }

    /* access modifiers changed from: package-private */
    public boolean e(Fragment fragment) {
        return this.f1534b.remove(fragment);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || j.class != obj.getClass()) {
            return false;
        }
        j jVar = (j) obj;
        if (!this.f1534b.equals(jVar.f1534b) || !this.f1535c.equals(jVar.f1535c) || !this.f1536d.equals(jVar.f1536d)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean f(Fragment fragment) {
        if (!this.f1534b.contains(fragment)) {
            return true;
        }
        if (this.f1537e) {
            return this.f1538f;
        }
        return !this.f1539g;
    }

    public int hashCode() {
        return (((this.f1534b.hashCode() * 31) + this.f1535c.hashCode()) * 31) + this.f1536d.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("FragmentManagerViewModel{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("} Fragments (");
        Iterator<Fragment> it = this.f1534b.iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") Child Non Config (");
        Iterator<String> it2 = this.f1535c.keySet().iterator();
        while (it2.hasNext()) {
            sb.append(it2.next());
            if (it2.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") ViewModelStores (");
        Iterator<String> it3 = this.f1536d.keySet().iterator();
        while (it3.hasNext()) {
            sb.append(it3.next());
            if (it3.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(')');
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public j c(Fragment fragment) {
        j jVar = this.f1535c.get(fragment.mWho);
        if (jVar != null) {
            return jVar;
        }
        j jVar2 = new j(this.f1537e);
        this.f1535c.put(fragment.mWho, jVar2);
        return jVar2;
    }

    /* access modifiers changed from: package-private */
    public u d(Fragment fragment) {
        u uVar = this.f1536d.get(fragment.mWho);
        if (uVar != null) {
            return uVar;
        }
        u uVar2 = new u();
        this.f1536d.put(fragment.mWho, uVar2);
        return uVar2;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Fragment fragment) {
        return this.f1534b.add(fragment);
    }

    /* access modifiers changed from: package-private */
    public void b(Fragment fragment) {
        if (i.H) {
            Log.d("FragmentManager", "Clearing non-config state for " + fragment);
        }
        j jVar = this.f1535c.get(fragment.mWho);
        if (jVar != null) {
            jVar.b();
            this.f1535c.remove(fragment.mWho);
        }
        u uVar = this.f1536d.get(fragment.mWho);
        if (uVar != null) {
            uVar.a();
            this.f1536d.remove(fragment.mWho);
        }
    }
}
