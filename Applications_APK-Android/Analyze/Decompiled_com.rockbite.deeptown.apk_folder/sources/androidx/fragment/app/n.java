package androidx.fragment.app;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import b.e.m.r;
import b.e.m.u;
import b.e.m.w;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SuppressLint({"UnknownNullness"})
/* compiled from: FragmentTransitionImpl */
public abstract class n {

    /* compiled from: FragmentTransitionImpl */
    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ int f1606a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ ArrayList f1607b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ ArrayList f1608c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ ArrayList f1609d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ ArrayList f1610e;

        a(n nVar, int i2, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, ArrayList arrayList4) {
            this.f1606a = i2;
            this.f1607b = arrayList;
            this.f1608c = arrayList2;
            this.f1609d = arrayList3;
            this.f1610e = arrayList4;
        }

        public void run() {
            for (int i2 = 0; i2 < this.f1606a; i2++) {
                u.a((View) this.f1607b.get(i2), (String) this.f1608c.get(i2));
                u.a((View) this.f1609d.get(i2), (String) this.f1610e.get(i2));
            }
        }
    }

    /* compiled from: FragmentTransitionImpl */
    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ ArrayList f1611a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Map f1612b;

        b(n nVar, ArrayList arrayList, Map map) {
            this.f1611a = arrayList;
            this.f1612b = map;
        }

        public void run() {
            int size = this.f1611a.size();
            for (int i2 = 0; i2 < size; i2++) {
                View view = (View) this.f1611a.get(i2);
                String n = u.n(view);
                if (n != null) {
                    u.a(view, n.a(this.f1612b, n));
                }
            }
        }
    }

    /* compiled from: FragmentTransitionImpl */
    class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ ArrayList f1613a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Map f1614b;

        c(n nVar, ArrayList arrayList, Map map) {
            this.f1613a = arrayList;
            this.f1614b = map;
        }

        public void run() {
            int size = this.f1613a.size();
            for (int i2 = 0; i2 < size; i2++) {
                View view = (View) this.f1613a.get(i2);
                u.a(view, (String) this.f1614b.get(u.n(view)));
            }
        }
    }

    public abstract Object a(Object obj, Object obj2, Object obj3);

    /* access modifiers changed from: protected */
    public void a(View view, Rect rect) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        rect.set(iArr[0], iArr[1], iArr[0] + view.getWidth(), iArr[1] + view.getHeight());
    }

    public abstract void a(ViewGroup viewGroup, Object obj);

    public abstract void a(Object obj, Rect rect);

    public abstract void a(Object obj, View view);

    public abstract void a(Object obj, View view, ArrayList<View> arrayList);

    public abstract void a(Object obj, Object obj2, ArrayList<View> arrayList, Object obj3, ArrayList<View> arrayList2, Object obj4, ArrayList<View> arrayList3);

    public abstract void a(Object obj, ArrayList<View> arrayList);

    public abstract void a(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2);

    public abstract boolean a(Object obj);

    public abstract Object b(Object obj);

    public abstract Object b(Object obj, Object obj2, Object obj3);

    public abstract void b(Object obj, View view);

    public abstract void b(Object obj, View view, ArrayList<View> arrayList);

    public abstract void b(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2);

    public abstract Object c(Object obj);

    public abstract void c(Object obj, View view);

    /* access modifiers changed from: package-private */
    public ArrayList<String> a(ArrayList<View> arrayList) {
        ArrayList<String> arrayList2 = new ArrayList<>();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = arrayList.get(i2);
            arrayList2.add(u.n(view));
            u.a(view, (String) null);
        }
        return arrayList2;
    }

    /* access modifiers changed from: package-private */
    public void a(View view, ArrayList<View> arrayList, ArrayList<View> arrayList2, ArrayList<String> arrayList3, Map<String, String> map) {
        int size = arrayList2.size();
        ArrayList arrayList4 = new ArrayList();
        for (int i2 = 0; i2 < size; i2++) {
            View view2 = arrayList.get(i2);
            String n = u.n(view2);
            arrayList4.add(n);
            if (n != null) {
                u.a(view2, (String) null);
                String str = map.get(n);
                int i3 = 0;
                while (true) {
                    if (i3 >= size) {
                        break;
                    } else if (str.equals(arrayList3.get(i3))) {
                        u.a(arrayList2.get(i3), n);
                        break;
                    } else {
                        i3++;
                    }
                }
            }
        }
        r.a(view, new a(this, size, arrayList2, arrayList3, arrayList, arrayList4));
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList<View> arrayList, View view) {
        if (view.getVisibility() != 0) {
            return;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            if (w.a(viewGroup)) {
                arrayList.add(viewGroup);
                return;
            }
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                a(arrayList, viewGroup.getChildAt(i2));
            }
            return;
        }
        arrayList.add(view);
    }

    /* access modifiers changed from: package-private */
    public void a(Map<String, View> map, View view) {
        if (view.getVisibility() == 0) {
            String n = u.n(view);
            if (n != null) {
                map.put(n, view);
            }
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                int childCount = viewGroup.getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    a(map, viewGroup.getChildAt(i2));
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view, ArrayList<View> arrayList, Map<String, String> map) {
        r.a(view, new b(this, arrayList, map));
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup viewGroup, ArrayList<View> arrayList, Map<String, String> map) {
        r.a(viewGroup, new c(this, arrayList, map));
    }

    protected static void a(List<View> list, View view) {
        int size = list.size();
        if (!a(list, view, size)) {
            list.add(view);
            for (int i2 = size; i2 < list.size(); i2++) {
                View view2 = list.get(i2);
                if (view2 instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) view2;
                    int childCount = viewGroup.getChildCount();
                    for (int i3 = 0; i3 < childCount; i3++) {
                        View childAt = viewGroup.getChildAt(i3);
                        if (!a(list, childAt, size)) {
                            list.add(childAt);
                        }
                    }
                }
            }
        }
    }

    private static boolean a(List<View> list, View view, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            if (list.get(i3) == view) {
                return true;
            }
        }
        return false;
    }

    protected static boolean a(List list) {
        return list == null || list.isEmpty();
    }

    static String a(Map<String, String> map, String str) {
        for (Map.Entry next : map.entrySet()) {
            if (str.equals(next.getValue())) {
                return (String) next.getKey();
            }
        }
        return null;
    }
}
