package androidx.work;

import android.content.Context;
import androidx.work.impl.i;
import java.util.Collections;
import java.util.List;

/* compiled from: WorkManager */
public abstract class s {
    protected s() {
    }

    public static s a(Context context) {
        return i.a(context);
    }

    public abstract n a(String str);

    public abstract n a(String str, f fVar, List<m> list);

    public static void a(Context context, b bVar) {
        i.a(context, bVar);
    }

    public n a(String str, f fVar, m mVar) {
        return a(str, fVar, Collections.singletonList(mVar));
    }
}
