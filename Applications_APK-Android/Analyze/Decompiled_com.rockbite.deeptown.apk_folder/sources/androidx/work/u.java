package androidx.work;

import android.content.Context;

/* compiled from: WorkerFactory */
public abstract class u {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2593a = k.a("WorkerFactory");

    /* compiled from: WorkerFactory */
    static class a extends u {
        a() {
        }

        public ListenableWorker a(Context context, String str, WorkerParameters workerParameters) {
            return null;
        }
    }

    public static u a() {
        return new a();
    }

    public abstract ListenableWorker a(Context context, String str, WorkerParameters workerParameters);

    public final ListenableWorker b(Context context, String str, WorkerParameters workerParameters) {
        ListenableWorker listenableWorker;
        ListenableWorker a2 = a(context, str, workerParameters);
        if (a2 == null) {
            Class<? extends U> cls = null;
            try {
                cls = Class.forName(str).asSubclass(ListenableWorker.class);
            } catch (ClassNotFoundException unused) {
                k a3 = k.a();
                String str2 = f2593a;
                a3.b(str2, "Class not found: " + str, new Throwable[0]);
            }
            if (cls != null) {
                try {
                    listenableWorker = (ListenableWorker) cls.getDeclaredConstructor(Context.class, WorkerParameters.class).newInstance(context, workerParameters);
                } catch (Exception e2) {
                    k a4 = k.a();
                    String str3 = f2593a;
                    a4.b(str3, "Could not instantiate " + str, e2);
                }
                if (listenableWorker != null || !listenableWorker.g()) {
                    return listenableWorker;
                }
                throw new IllegalStateException(String.format("WorkerFactory (%s) returned an instance of a ListenableWorker (%s) which has already been invoked. createWorker() must always return a new instance of a ListenableWorker.", getClass().getName(), str));
            }
        }
        listenableWorker = a2;
        if (listenableWorker != null) {
        }
        return listenableWorker;
    }
}
