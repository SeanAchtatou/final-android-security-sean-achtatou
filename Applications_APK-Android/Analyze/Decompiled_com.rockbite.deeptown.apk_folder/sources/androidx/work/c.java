package androidx.work;

import android.os.Build;

/* compiled from: Constraints */
public final class c {

    /* renamed from: i  reason: collision with root package name */
    public static final c f2189i = new a().a();

    /* renamed from: a  reason: collision with root package name */
    private l f2190a = l.NOT_REQUIRED;

    /* renamed from: b  reason: collision with root package name */
    private boolean f2191b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f2192c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f2193d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f2194e;

    /* renamed from: f  reason: collision with root package name */
    private long f2195f = -1;

    /* renamed from: g  reason: collision with root package name */
    private long f2196g = -1;

    /* renamed from: h  reason: collision with root package name */
    private d f2197h = new d();

    /* compiled from: Constraints */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        boolean f2198a = false;

        /* renamed from: b  reason: collision with root package name */
        boolean f2199b = false;

        /* renamed from: c  reason: collision with root package name */
        l f2200c = l.NOT_REQUIRED;

        /* renamed from: d  reason: collision with root package name */
        boolean f2201d = false;

        /* renamed from: e  reason: collision with root package name */
        boolean f2202e = false;

        /* renamed from: f  reason: collision with root package name */
        long f2203f = -1;

        /* renamed from: g  reason: collision with root package name */
        long f2204g = -1;

        /* renamed from: h  reason: collision with root package name */
        d f2205h = new d();

        public c a() {
            return new c(this);
        }
    }

    public c() {
    }

    public void a(l lVar) {
        this.f2190a = lVar;
    }

    public l b() {
        return this.f2190a;
    }

    public void c(boolean z) {
        this.f2192c = z;
    }

    public void d(boolean z) {
        this.f2194e = z;
    }

    public boolean e() {
        return this.f2197h.b() > 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || c.class != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        if (this.f2191b == cVar.f2191b && this.f2192c == cVar.f2192c && this.f2193d == cVar.f2193d && this.f2194e == cVar.f2194e && this.f2195f == cVar.f2195f && this.f2196g == cVar.f2196g && this.f2190a == cVar.f2190a) {
            return this.f2197h.equals(cVar.f2197h);
        }
        return false;
    }

    public boolean f() {
        return this.f2193d;
    }

    public boolean g() {
        return this.f2191b;
    }

    public boolean h() {
        return this.f2192c;
    }

    public int hashCode() {
        long j2 = this.f2195f;
        long j3 = this.f2196g;
        return (((((((((((((this.f2190a.hashCode() * 31) + (this.f2191b ? 1 : 0)) * 31) + (this.f2192c ? 1 : 0)) * 31) + (this.f2193d ? 1 : 0)) * 31) + (this.f2194e ? 1 : 0)) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + this.f2197h.hashCode();
    }

    public boolean i() {
        return this.f2194e;
    }

    public void a(boolean z) {
        this.f2193d = z;
    }

    public void b(boolean z) {
        this.f2191b = z;
    }

    public long c() {
        return this.f2195f;
    }

    public long d() {
        return this.f2196g;
    }

    public void a(long j2) {
        this.f2195f = j2;
    }

    public void b(long j2) {
        this.f2196g = j2;
    }

    public void a(d dVar) {
        this.f2197h = dVar;
    }

    public d a() {
        return this.f2197h;
    }

    c(a aVar) {
        this.f2191b = aVar.f2198a;
        this.f2192c = Build.VERSION.SDK_INT >= 23 && aVar.f2199b;
        this.f2190a = aVar.f2200c;
        this.f2193d = aVar.f2201d;
        this.f2194e = aVar.f2202e;
        if (Build.VERSION.SDK_INT >= 24) {
            this.f2197h = aVar.f2205h;
            this.f2195f = aVar.f2203f;
            this.f2196g = aVar.f2204g;
        }
    }

    public c(c cVar) {
        this.f2191b = cVar.f2191b;
        this.f2192c = cVar.f2192c;
        this.f2190a = cVar.f2190a;
        this.f2193d = cVar.f2193d;
        this.f2194e = cVar.f2194e;
        this.f2197h = cVar.f2197h;
    }
}
