package androidx.work;

import android.os.Build;
import com.google.android.gms.common.api.Api;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* compiled from: Configuration */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    final Executor f2173a;

    /* renamed from: b  reason: collision with root package name */
    final Executor f2174b;

    /* renamed from: c  reason: collision with root package name */
    final u f2175c;

    /* renamed from: d  reason: collision with root package name */
    final j f2176d;

    /* renamed from: e  reason: collision with root package name */
    final int f2177e;

    /* renamed from: f  reason: collision with root package name */
    final int f2178f;

    /* renamed from: g  reason: collision with root package name */
    final int f2179g;

    /* renamed from: h  reason: collision with root package name */
    final int f2180h;

    /* compiled from: Configuration */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        Executor f2181a;

        /* renamed from: b  reason: collision with root package name */
        u f2182b;

        /* renamed from: c  reason: collision with root package name */
        j f2183c;

        /* renamed from: d  reason: collision with root package name */
        Executor f2184d;

        /* renamed from: e  reason: collision with root package name */
        int f2185e = 4;

        /* renamed from: f  reason: collision with root package name */
        int f2186f = 0;

        /* renamed from: g  reason: collision with root package name */
        int f2187g = Api.BaseClientBuilder.API_PRIORITY_OTHER;

        /* renamed from: h  reason: collision with root package name */
        int f2188h = 20;

        public b a() {
            return new b(this);
        }
    }

    /* renamed from: androidx.work.b$b  reason: collision with other inner class name */
    /* compiled from: Configuration */
    public interface C0028b {
        b a();
    }

    b(a aVar) {
        Executor executor = aVar.f2181a;
        if (executor == null) {
            this.f2173a = i();
        } else {
            this.f2173a = executor;
        }
        Executor executor2 = aVar.f2184d;
        if (executor2 == null) {
            this.f2174b = i();
        } else {
            this.f2174b = executor2;
        }
        u uVar = aVar.f2182b;
        if (uVar == null) {
            this.f2175c = u.a();
        } else {
            this.f2175c = uVar;
        }
        j jVar = aVar.f2183c;
        if (jVar == null) {
            this.f2176d = j.a();
        } else {
            this.f2176d = jVar;
        }
        this.f2177e = aVar.f2185e;
        this.f2178f = aVar.f2186f;
        this.f2179g = aVar.f2187g;
        this.f2180h = aVar.f2188h;
    }

    private Executor i() {
        return Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4)));
    }

    public Executor a() {
        return this.f2173a;
    }

    public j b() {
        return this.f2176d;
    }

    public int c() {
        return this.f2179g;
    }

    public int d() {
        if (Build.VERSION.SDK_INT == 23) {
            return this.f2180h / 2;
        }
        return this.f2180h;
    }

    public int e() {
        return this.f2178f;
    }

    public int f() {
        return this.f2177e;
    }

    public Executor g() {
        return this.f2174b;
    }

    public u h() {
        return this.f2175c;
    }
}
