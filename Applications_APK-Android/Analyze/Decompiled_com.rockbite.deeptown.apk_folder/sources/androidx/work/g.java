package androidx.work;

import android.app.Notification;

/* compiled from: ForegroundInfo */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    private final int f2217a;

    /* renamed from: b  reason: collision with root package name */
    private final int f2218b;

    /* renamed from: c  reason: collision with root package name */
    private final Notification f2219c;

    public g(int i2, Notification notification, int i3) {
        this.f2217a = i2;
        this.f2219c = notification;
        this.f2218b = i3;
    }

    public int a() {
        return this.f2218b;
    }

    public Notification b() {
        return this.f2219c;
    }

    public int c() {
        return this.f2217a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || g.class != obj.getClass()) {
            return false;
        }
        g gVar = (g) obj;
        if (this.f2217a == gVar.f2217a && this.f2218b == gVar.f2218b) {
            return this.f2219c.equals(gVar.f2219c);
        }
        return false;
    }

    public int hashCode() {
        return (((this.f2217a * 31) + this.f2218b) * 31) + this.f2219c.hashCode();
    }

    public String toString() {
        return "ForegroundInfo{" + "mNotificationId=" + this.f2217a + ", mForegroundServiceType=" + this.f2218b + ", mNotification=" + this.f2219c + '}';
    }
}
