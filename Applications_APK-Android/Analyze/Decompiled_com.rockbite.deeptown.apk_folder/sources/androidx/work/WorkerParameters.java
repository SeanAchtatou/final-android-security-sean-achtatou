package androidx.work;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.Executor;

public final class WorkerParameters {

    /* renamed from: a  reason: collision with root package name */
    private UUID f2166a;

    /* renamed from: b  reason: collision with root package name */
    private e f2167b;

    /* renamed from: c  reason: collision with root package name */
    private Executor f2168c;

    /* renamed from: d  reason: collision with root package name */
    private u f2169d;

    public static class a {
        public a() {
            Collections.emptyList();
            Collections.emptyList();
        }
    }

    public WorkerParameters(UUID uuid, e eVar, Collection<String> collection, a aVar, int i2, Executor executor, androidx.work.impl.utils.n.a aVar2, u uVar, o oVar, h hVar) {
        this.f2166a = uuid;
        this.f2167b = eVar;
        new HashSet(collection);
        this.f2168c = executor;
        this.f2169d = uVar;
    }

    public Executor a() {
        return this.f2168c;
    }

    public UUID b() {
        return this.f2166a;
    }

    public e c() {
        return this.f2167b;
    }

    public u d() {
        return this.f2169d;
    }
}
