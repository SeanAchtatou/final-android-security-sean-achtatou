package androidx.work.impl.workers;

import android.content.Context;
import android.text.TextUtils;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.i;
import androidx.work.impl.l.c;
import androidx.work.impl.l.d;
import androidx.work.impl.m.p;
import androidx.work.k;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Collections;
import java.util.List;

public class ConstraintTrackingWorker extends ListenableWorker implements c {

    /* renamed from: j  reason: collision with root package name */
    private static final String f2558j = k.a("ConstraintTrkngWrkr");

    /* renamed from: e  reason: collision with root package name */
    private WorkerParameters f2559e;

    /* renamed from: f  reason: collision with root package name */
    final Object f2560f = new Object();

    /* renamed from: g  reason: collision with root package name */
    volatile boolean f2561g = false;

    /* renamed from: h  reason: collision with root package name */
    androidx.work.impl.utils.m.c<ListenableWorker.a> f2562h = androidx.work.impl.utils.m.c.d();

    /* renamed from: i  reason: collision with root package name */
    private ListenableWorker f2563i;

    class a implements Runnable {
        a() {
        }

        public void run() {
            ConstraintTrackingWorker.this.p();
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ ListenableFuture f2565a;

        b(ListenableFuture listenableFuture) {
            this.f2565a = listenableFuture;
        }

        public void run() {
            synchronized (ConstraintTrackingWorker.this.f2560f) {
                if (ConstraintTrackingWorker.this.f2561g) {
                    ConstraintTrackingWorker.this.o();
                } else {
                    ConstraintTrackingWorker.this.f2562h.a((ListenableFuture<? extends ListenableWorker.a>) this.f2565a);
                }
            }
        }
    }

    public ConstraintTrackingWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        this.f2559e = workerParameters;
    }

    public void a(List<String> list) {
        k.a().a(f2558j, String.format("Constraints changed for %s", list), new Throwable[0]);
        synchronized (this.f2560f) {
            this.f2561g = true;
        }
    }

    public void b(List<String> list) {
    }

    public void h() {
        super.h();
        ListenableWorker listenableWorker = this.f2563i;
        if (listenableWorker != null) {
            listenableWorker.k();
        }
    }

    public ListenableFuture<ListenableWorker.a> j() {
        b().execute(new a());
        return this.f2562h;
    }

    public androidx.work.impl.utils.n.a l() {
        return i.a(a()).g();
    }

    public WorkDatabase m() {
        return i.a(a()).f();
    }

    /* access modifiers changed from: package-private */
    public void n() {
        this.f2562h.a(ListenableWorker.a.a());
    }

    /* access modifiers changed from: package-private */
    public void o() {
        this.f2562h.a(ListenableWorker.a.b());
    }

    /* access modifiers changed from: package-private */
    public void p() {
        String a2 = d().a("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME");
        if (TextUtils.isEmpty(a2)) {
            k.a().b(f2558j, "No worker to delegate to.", new Throwable[0]);
            n();
            return;
        }
        this.f2563i = e().b(a(), a2, this.f2559e);
        if (this.f2563i == null) {
            k.a().a(f2558j, "No worker to delegate to.", new Throwable[0]);
            n();
            return;
        }
        p e2 = m().q().e(c().toString());
        if (e2 == null) {
            n();
            return;
        }
        d dVar = new d(a(), l(), this);
        dVar.a((Iterable<p>) Collections.singletonList(e2));
        if (dVar.a(c().toString())) {
            k.a().a(f2558j, String.format("Constraints met for delegate %s", a2), new Throwable[0]);
            try {
                ListenableFuture<ListenableWorker.a> j2 = this.f2563i.j();
                j2.addListener(new b(j2), b());
            } catch (Throwable th) {
                k.a().a(f2558j, String.format("Delegated worker %s threw exception in startWork.", a2), th);
                synchronized (this.f2560f) {
                    if (this.f2561g) {
                        k.a().a(f2558j, "Constraints were unmet, Retrying.", new Throwable[0]);
                        o();
                    } else {
                        n();
                    }
                }
            }
        } else {
            k.a().a(f2558j, String.format("Constraints not met for delegate %s. Requesting retry.", a2), new Throwable[0]);
            o();
        }
    }
}
