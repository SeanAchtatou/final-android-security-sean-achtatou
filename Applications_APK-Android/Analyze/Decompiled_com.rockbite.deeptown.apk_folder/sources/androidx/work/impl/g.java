package androidx.work.impl;

import android.content.Context;
import android.os.Build;

/* compiled from: WorkDatabaseMigrations */
public class g {

    /* renamed from: a  reason: collision with root package name */
    public static androidx.room.q.a f2338a = new a(1, 2);

    /* renamed from: b  reason: collision with root package name */
    public static androidx.room.q.a f2339b = new b(3, 4);

    /* renamed from: c  reason: collision with root package name */
    public static androidx.room.q.a f2340c = new c(4, 5);

    /* renamed from: d  reason: collision with root package name */
    public static androidx.room.q.a f2341d = new d(6, 7);

    /* renamed from: e  reason: collision with root package name */
    public static androidx.room.q.a f2342e = new e(7, 8);

    /* renamed from: f  reason: collision with root package name */
    public static androidx.room.q.a f2343f = new f(8, 9);

    /* compiled from: WorkDatabaseMigrations */
    static class a extends androidx.room.q.a {
        a(int i2, int i3) {
            super(i2, i3);
        }

        public void a(b.m.a.b bVar) {
            bVar.e("CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
            bVar.e("INSERT INTO SystemIdInfo(work_spec_id, system_id) SELECT work_spec_id, alarm_id AS system_id FROM alarmInfo");
            bVar.e("DROP TABLE IF EXISTS alarmInfo");
            bVar.e("INSERT OR IGNORE INTO worktag(tag, work_spec_id) SELECT worker_class_name AS tag, id AS work_spec_id FROM workspec");
        }
    }

    /* compiled from: WorkDatabaseMigrations */
    static class b extends androidx.room.q.a {
        b(int i2, int i3) {
            super(i2, i3);
        }

        public void a(b.m.a.b bVar) {
            if (Build.VERSION.SDK_INT >= 23) {
                bVar.e("UPDATE workspec SET schedule_requested_at=0 WHERE state NOT IN (2, 3, 5) AND schedule_requested_at=-1 AND interval_duration<>0");
            }
        }
    }

    /* compiled from: WorkDatabaseMigrations */
    static class c extends androidx.room.q.a {
        c(int i2, int i3) {
            super(i2, i3);
        }

        public void a(b.m.a.b bVar) {
            bVar.e("ALTER TABLE workspec ADD COLUMN `trigger_content_update_delay` INTEGER NOT NULL DEFAULT -1");
            bVar.e("ALTER TABLE workspec ADD COLUMN `trigger_max_content_delay` INTEGER NOT NULL DEFAULT -1");
        }
    }

    /* compiled from: WorkDatabaseMigrations */
    static class d extends androidx.room.q.a {
        d(int i2, int i3) {
            super(i2, i3);
        }

        public void a(b.m.a.b bVar) {
            bVar.e("CREATE TABLE IF NOT EXISTS `WorkProgress` (`work_spec_id` TEXT NOT NULL, `progress` BLOB NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        }
    }

    /* compiled from: WorkDatabaseMigrations */
    static class e extends androidx.room.q.a {
        e(int i2, int i3) {
            super(i2, i3);
        }

        public void a(b.m.a.b bVar) {
            bVar.e("CREATE INDEX IF NOT EXISTS `index_WorkSpec_period_start_time` ON `workspec` (`period_start_time`)");
        }
    }

    /* compiled from: WorkDatabaseMigrations */
    static class f extends androidx.room.q.a {
        f(int i2, int i3) {
            super(i2, i3);
        }

        public void a(b.m.a.b bVar) {
            bVar.e("ALTER TABLE workspec ADD COLUMN `run_in_foreground` INTEGER NOT NULL DEFAULT 0");
        }
    }

    /* renamed from: androidx.work.impl.g$g  reason: collision with other inner class name */
    /* compiled from: WorkDatabaseMigrations */
    public static class C0031g extends androidx.room.q.a {

        /* renamed from: c  reason: collision with root package name */
        final Context f2344c;

        public C0031g(Context context, int i2, int i3) {
            super(i2, i3);
            this.f2344c = context;
        }

        public void a(b.m.a.b bVar) {
            if (this.f2079b >= 10) {
                bVar.a("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"reschedule_needed", 1});
                return;
            }
            this.f2344c.getSharedPreferences("androidx.work.util.preferences", 0).edit().putBoolean("reschedule_needed", true).apply();
        }
    }

    /* compiled from: WorkDatabaseMigrations */
    public static class h extends androidx.room.q.a {

        /* renamed from: c  reason: collision with root package name */
        final Context f2345c;

        public h(Context context) {
            super(9, 10);
            this.f2345c = context;
        }

        public void a(b.m.a.b bVar) {
            bVar.e("CREATE TABLE IF NOT EXISTS `Preference` (`key` TEXT NOT NULL, `long_value` INTEGER, PRIMARY KEY(`key`))");
            androidx.work.impl.utils.e.a(this.f2345c, bVar);
            androidx.work.impl.utils.c.a(this.f2345c, bVar);
        }
    }
}
