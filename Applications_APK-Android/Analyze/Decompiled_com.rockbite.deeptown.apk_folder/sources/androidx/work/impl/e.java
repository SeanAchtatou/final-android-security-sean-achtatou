package androidx.work.impl;

import android.content.Context;
import android.os.Build;
import androidx.work.b;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import androidx.work.impl.background.systemalarm.f;
import androidx.work.impl.background.systemjob.SystemJobService;
import androidx.work.impl.m.p;
import androidx.work.impl.m.q;
import androidx.work.impl.utils.d;
import androidx.work.k;
import java.util.List;

/* compiled from: Schedulers */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2299a = k.a("Schedulers");

    public static void a(b bVar, WorkDatabase workDatabase, List<d> list) {
        if (list != null && list.size() != 0) {
            q q = workDatabase.q();
            workDatabase.c();
            try {
                List<p> a2 = q.a(bVar.d());
                if (a2 != null && a2.size() > 0) {
                    long currentTimeMillis = System.currentTimeMillis();
                    for (p pVar : a2) {
                        q.a(pVar.f2453a, currentTimeMillis);
                    }
                }
                workDatabase.k();
                if (a2 != null && a2.size() > 0) {
                    p[] pVarArr = (p[]) a2.toArray(new p[0]);
                    for (d a3 : list) {
                        a3.a(pVarArr);
                    }
                }
            } finally {
                workDatabase.e();
            }
        }
    }

    static d a(Context context, i iVar) {
        if (Build.VERSION.SDK_INT >= 23) {
            androidx.work.impl.background.systemjob.b bVar = new androidx.work.impl.background.systemjob.b(context, iVar);
            d.a(context, SystemJobService.class, true);
            k.a().a(f2299a, "Created SystemJobScheduler and enabled SystemJobService", new Throwable[0]);
            return bVar;
        }
        d a2 = a(context);
        if (a2 != null) {
            return a2;
        }
        f fVar = new f(context);
        d.a(context, SystemAlarmService.class, true);
        k.a().a(f2299a, "Created SystemAlarmScheduler", new Throwable[0]);
        return fVar;
    }

    private static d a(Context context) {
        try {
            d dVar = (d) Class.forName("androidx.work.impl.background.gcm.GcmScheduler").getConstructor(Context.class).newInstance(context);
            k.a().a(f2299a, String.format("Created %s", "androidx.work.impl.background.gcm.GcmScheduler"), new Throwable[0]);
            return dVar;
        } catch (Throwable th) {
            k.a().a(f2299a, "Unable to create GCM Scheduler", th);
            return null;
        }
    }
}
