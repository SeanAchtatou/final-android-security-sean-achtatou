package androidx.work.impl;

import android.text.TextUtils;
import androidx.work.impl.utils.b;
import androidx.work.k;
import androidx.work.n;
import androidx.work.q;
import androidx.work.t;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: WorkContinuationImpl */
public class f extends q {

    /* renamed from: j  reason: collision with root package name */
    private static final String f2300j = k.a("WorkContinuationImpl");

    /* renamed from: a  reason: collision with root package name */
    private final i f2301a;

    /* renamed from: b  reason: collision with root package name */
    private final String f2302b;

    /* renamed from: c  reason: collision with root package name */
    private final androidx.work.f f2303c;

    /* renamed from: d  reason: collision with root package name */
    private final List<? extends t> f2304d;

    /* renamed from: e  reason: collision with root package name */
    private final List<String> f2305e;

    /* renamed from: f  reason: collision with root package name */
    private final List<String> f2306f;

    /* renamed from: g  reason: collision with root package name */
    private final List<f> f2307g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f2308h;

    /* renamed from: i  reason: collision with root package name */
    private n f2309i;

    f(i iVar, String str, androidx.work.f fVar, List<? extends t> list) {
        this(iVar, str, fVar, list, null);
    }

    public n a() {
        if (!this.f2308h) {
            b bVar = new b(this);
            this.f2301a.g().a(bVar);
            this.f2309i = bVar.b();
        } else {
            k.a().e(f2300j, String.format("Already enqueued work ids (%s)", TextUtils.join(", ", this.f2305e)), new Throwable[0]);
        }
        return this.f2309i;
    }

    public androidx.work.f b() {
        return this.f2303c;
    }

    public List<String> c() {
        return this.f2305e;
    }

    public String d() {
        return this.f2302b;
    }

    public List<f> e() {
        return this.f2307g;
    }

    public List<? extends t> f() {
        return this.f2304d;
    }

    public i g() {
        return this.f2301a;
    }

    public boolean h() {
        return a(this, new HashSet());
    }

    public boolean i() {
        return this.f2308h;
    }

    public void j() {
        this.f2308h = true;
    }

    f(i iVar, String str, androidx.work.f fVar, List<? extends t> list, List<f> list2) {
        this.f2301a = iVar;
        this.f2302b = str;
        this.f2303c = fVar;
        this.f2304d = list;
        this.f2307g = list2;
        this.f2305e = new ArrayList(this.f2304d.size());
        this.f2306f = new ArrayList();
        if (list2 != null) {
            for (f fVar2 : list2) {
                this.f2306f.addAll(fVar2.f2306f);
            }
        }
        for (int i2 = 0; i2 < list.size(); i2++) {
            String a2 = ((t) list.get(i2)).a();
            this.f2305e.add(a2);
            this.f2306f.add(a2);
        }
    }

    private static boolean a(f fVar, Set<String> set) {
        set.addAll(fVar.c());
        Set<String> a2 = a(fVar);
        for (String contains : set) {
            if (a2.contains(contains)) {
                return true;
            }
        }
        List<f> e2 = fVar.e();
        if (e2 != null && !e2.isEmpty()) {
            for (f a3 : e2) {
                if (a(a3, set)) {
                    return true;
                }
            }
        }
        set.removeAll(fVar.c());
        return false;
    }

    public static Set<String> a(f fVar) {
        HashSet hashSet = new HashSet();
        List<f> e2 = fVar.e();
        if (e2 != null && !e2.isEmpty()) {
            for (f c2 : e2) {
                hashSet.addAll(c2.c());
            }
        }
        return hashSet;
    }
}
