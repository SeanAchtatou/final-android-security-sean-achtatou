package androidx.work.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build;
import androidx.work.WorkerParameters;
import androidx.work.b;
import androidx.work.f;
import androidx.work.impl.utils.ForceStopRunnable;
import androidx.work.impl.utils.e;
import androidx.work.impl.utils.g;
import androidx.work.impl.utils.h;
import androidx.work.impl.utils.n.a;
import androidx.work.k;
import androidx.work.m;
import androidx.work.n;
import androidx.work.p;
import androidx.work.s;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/* compiled from: WorkManagerImpl */
public class i extends s {

    /* renamed from: j  reason: collision with root package name */
    private static i f2348j;

    /* renamed from: k  reason: collision with root package name */
    private static i f2349k;
    private static final Object l = new Object();

    /* renamed from: a  reason: collision with root package name */
    private Context f2350a;

    /* renamed from: b  reason: collision with root package name */
    private b f2351b;

    /* renamed from: c  reason: collision with root package name */
    private WorkDatabase f2352c;

    /* renamed from: d  reason: collision with root package name */
    private a f2353d;

    /* renamed from: e  reason: collision with root package name */
    private List<d> f2354e;

    /* renamed from: f  reason: collision with root package name */
    private c f2355f;

    /* renamed from: g  reason: collision with root package name */
    private e f2356g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f2357h;

    /* renamed from: i  reason: collision with root package name */
    private BroadcastReceiver.PendingResult f2358i;

    public i(Context context, b bVar, a aVar) {
        this(context, bVar, aVar, context.getResources().getBoolean(p.workmanager_test_configuration));
    }

    public static i a(Context context) {
        i j2;
        synchronized (l) {
            j2 = j();
            if (j2 == null) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext instanceof b.C0028b) {
                    a(applicationContext, ((b.C0028b) applicationContext).a());
                    j2 = a(applicationContext);
                } else {
                    throw new IllegalStateException("WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider.");
                }
            }
        }
        return j2;
    }

    @Deprecated
    public static i j() {
        synchronized (l) {
            if (f2348j != null) {
                i iVar = f2348j;
                return iVar;
            }
            i iVar2 = f2349k;
            return iVar2;
        }
    }

    public b b() {
        return this.f2351b;
    }

    public e c() {
        return this.f2356g;
    }

    public c d() {
        return this.f2355f;
    }

    public List<d> e() {
        return this.f2354e;
    }

    public WorkDatabase f() {
        return this.f2352c;
    }

    public a g() {
        return this.f2353d;
    }

    public void h() {
        synchronized (l) {
            this.f2357h = true;
            if (this.f2358i != null) {
                this.f2358i.finish();
                this.f2358i = null;
            }
        }
    }

    public void i() {
        if (Build.VERSION.SDK_INT >= 23) {
            androidx.work.impl.background.systemjob.b.a(a());
        }
        f().q().d();
        e.a(b(), f(), e());
    }

    public void b(String str) {
        a(str, (WorkerParameters.a) null);
    }

    public void c(String str) {
        this.f2353d.a(new h(this, str, true));
    }

    public void d(String str) {
        this.f2353d.a(new h(this, str, false));
    }

    public i(Context context, b bVar, a aVar, boolean z) {
        this(context, bVar, aVar, WorkDatabase.a(context.getApplicationContext(), aVar.b(), z));
    }

    public i(Context context, b bVar, a aVar, WorkDatabase workDatabase) {
        Context applicationContext = context.getApplicationContext();
        k.a(new k.a(bVar.f()));
        Context context2 = context;
        b bVar2 = bVar;
        a aVar2 = aVar;
        WorkDatabase workDatabase2 = workDatabase;
        List<d> a2 = a(applicationContext, aVar);
        a(context2, bVar2, aVar2, workDatabase2, a2, new c(context2, bVar2, aVar2, workDatabase2, a2));
    }

    public static void a(Context context, b bVar) {
        synchronized (l) {
            if (f2348j != null) {
                if (f2349k != null) {
                    throw new IllegalStateException("WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class level Javadoc for more information.");
                }
            }
            if (f2348j == null) {
                Context applicationContext = context.getApplicationContext();
                if (f2349k == null) {
                    f2349k = new i(applicationContext, bVar, new androidx.work.impl.utils.n.b(bVar.g()));
                }
                f2348j = f2349k;
            }
        }
    }

    public Context a() {
        return this.f2350a;
    }

    public n a(String str, f fVar, List<m> list) {
        return new f(this, str, fVar, list).a();
    }

    public n a(UUID uuid) {
        androidx.work.impl.utils.a a2 = androidx.work.impl.utils.a.a(uuid, this);
        this.f2353d.a(a2);
        return a2.a();
    }

    public n a(String str) {
        androidx.work.impl.utils.a a2 = androidx.work.impl.utils.a.a(str, this);
        this.f2353d.a(a2);
        return a2.a();
    }

    public void a(String str, WorkerParameters.a aVar) {
        this.f2353d.a(new g(this, str, aVar));
    }

    public void a(BroadcastReceiver.PendingResult pendingResult) {
        synchronized (l) {
            this.f2358i = pendingResult;
            if (this.f2357h) {
                this.f2358i.finish();
                this.f2358i = null;
            }
        }
    }

    private void a(Context context, b bVar, a aVar, WorkDatabase workDatabase, List<d> list, c cVar) {
        Context applicationContext = context.getApplicationContext();
        this.f2350a = applicationContext;
        this.f2351b = bVar;
        this.f2353d = aVar;
        this.f2352c = workDatabase;
        this.f2354e = list;
        this.f2355f = cVar;
        this.f2356g = new e(workDatabase);
        this.f2357h = false;
        this.f2353d.a(new ForceStopRunnable(applicationContext, this));
    }

    public List<d> a(Context context, a aVar) {
        return Arrays.asList(e.a(context, this), new androidx.work.impl.k.a.a(context, aVar, this));
    }
}
