package androidx.work.impl.m;

/* compiled from: SystemIdInfo */
public class g {

    /* renamed from: a  reason: collision with root package name */
    public final String f2439a;

    /* renamed from: b  reason: collision with root package name */
    public final int f2440b;

    public g(String str, int i2) {
        this.f2439a = str;
        this.f2440b = i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        if (this.f2440b != gVar.f2440b) {
            return false;
        }
        return this.f2439a.equals(gVar.f2439a);
    }

    public int hashCode() {
        return (this.f2439a.hashCode() * 31) + this.f2440b;
    }
}
