package androidx.work.impl.m;

import androidx.work.c;
import androidx.work.e;
import androidx.work.k;
import androidx.work.r;
import java.util.List;

/* compiled from: WorkSpec */
public final class p {

    /* renamed from: a  reason: collision with root package name */
    public String f2453a;

    /* renamed from: b  reason: collision with root package name */
    public r f2454b = r.ENQUEUED;

    /* renamed from: c  reason: collision with root package name */
    public String f2455c;

    /* renamed from: d  reason: collision with root package name */
    public String f2456d;

    /* renamed from: e  reason: collision with root package name */
    public e f2457e;

    /* renamed from: f  reason: collision with root package name */
    public e f2458f;

    /* renamed from: g  reason: collision with root package name */
    public long f2459g;

    /* renamed from: h  reason: collision with root package name */
    public long f2460h;

    /* renamed from: i  reason: collision with root package name */
    public long f2461i;

    /* renamed from: j  reason: collision with root package name */
    public c f2462j;

    /* renamed from: k  reason: collision with root package name */
    public int f2463k;
    public androidx.work.a l;
    public long m;
    public long n;
    public long o;
    public long p;
    public boolean q;

    /* compiled from: WorkSpec */
    static class a implements b.b.a.c.a<List<Object>, List<Object>> {
        a() {
        }
    }

    /* compiled from: WorkSpec */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public String f2464a;

        /* renamed from: b  reason: collision with root package name */
        public r f2465b;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (this.f2465b != bVar.f2465b) {
                return false;
            }
            return this.f2464a.equals(bVar.f2464a);
        }

        public int hashCode() {
            return (this.f2464a.hashCode() * 31) + this.f2465b.hashCode();
        }
    }

    static {
        k.a("WorkSpec");
        new a();
    }

    public p(String str, String str2) {
        e eVar = e.f2210c;
        this.f2457e = eVar;
        this.f2458f = eVar;
        this.f2462j = c.f2189i;
        this.l = androidx.work.a.EXPONENTIAL;
        this.m = 30000;
        this.p = -1;
        this.f2453a = str;
        this.f2455c = str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public long a() {
        long j2;
        boolean z = false;
        if (c()) {
            if (this.l == androidx.work.a.LINEAR) {
                z = true;
            }
            if (z) {
                j2 = this.m * ((long) this.f2463k);
            } else {
                j2 = (long) Math.scalb((float) this.m, this.f2463k - 1);
            }
            return this.n + Math.min(18000000L, j2);
        }
        long j3 = 0;
        if (d()) {
            long currentTimeMillis = System.currentTimeMillis();
            long j4 = this.n;
            if (j4 == 0) {
                j4 = this.f2459g + currentTimeMillis;
            }
            if (this.f2461i != this.f2460h) {
                z = true;
            }
            if (z) {
                if (this.n == 0) {
                    j3 = this.f2461i * -1;
                }
                return j4 + this.f2460h + j3;
            }
            if (this.n != 0) {
                j3 = this.f2460h;
            }
            return j4 + j3;
        }
        long j5 = this.n;
        if (j5 == 0) {
            j5 = System.currentTimeMillis();
        }
        return j5 + this.f2459g;
    }

    public boolean b() {
        return !c.f2189i.equals(this.f2462j);
    }

    public boolean c() {
        return this.f2454b == r.ENQUEUED && this.f2463k > 0;
    }

    public boolean d() {
        return this.f2460h != 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof p)) {
            return false;
        }
        p pVar = (p) obj;
        if (this.f2459g != pVar.f2459g || this.f2460h != pVar.f2460h || this.f2461i != pVar.f2461i || this.f2463k != pVar.f2463k || this.m != pVar.m || this.n != pVar.n || this.o != pVar.o || this.p != pVar.p || this.q != pVar.q || !this.f2453a.equals(pVar.f2453a) || this.f2454b != pVar.f2454b || !this.f2455c.equals(pVar.f2455c)) {
            return false;
        }
        String str = this.f2456d;
        if (str == null ? pVar.f2456d != null : !str.equals(pVar.f2456d)) {
            return false;
        }
        if (this.f2457e.equals(pVar.f2457e) && this.f2458f.equals(pVar.f2458f) && this.f2462j.equals(pVar.f2462j) && this.l == pVar.l) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = ((((this.f2453a.hashCode() * 31) + this.f2454b.hashCode()) * 31) + this.f2455c.hashCode()) * 31;
        String str = this.f2456d;
        int hashCode2 = str != null ? str.hashCode() : 0;
        long j2 = this.f2459g;
        long j3 = this.f2460h;
        long j4 = this.f2461i;
        long j5 = this.m;
        long j6 = this.n;
        long j7 = this.o;
        long j8 = this.p;
        return ((((((((((((((((((((((((((hashCode + hashCode2) * 31) + this.f2457e.hashCode()) * 31) + this.f2458f.hashCode()) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + this.f2462j.hashCode()) * 31) + this.f2463k) * 31) + this.l.hashCode()) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31) + ((int) (j7 ^ (j7 >>> 32)))) * 31) + ((int) (j8 ^ (j8 >>> 32)))) * 31) + (this.q ? 1 : 0);
    }

    public String toString() {
        return "{WorkSpec: " + this.f2453a + "}";
    }

    public p(p pVar) {
        e eVar = e.f2210c;
        this.f2457e = eVar;
        this.f2458f = eVar;
        this.f2462j = c.f2189i;
        this.l = androidx.work.a.EXPONENTIAL;
        this.m = 30000;
        this.p = -1;
        this.f2453a = pVar.f2453a;
        this.f2455c = pVar.f2455c;
        this.f2454b = pVar.f2454b;
        this.f2456d = pVar.f2456d;
        this.f2457e = new e(pVar.f2457e);
        this.f2458f = new e(pVar.f2458f);
        this.f2459g = pVar.f2459g;
        this.f2460h = pVar.f2460h;
        this.f2461i = pVar.f2461i;
        this.f2462j = new c(pVar.f2462j);
        this.f2463k = pVar.f2463k;
        this.l = pVar.l;
        this.m = pVar.m;
        this.n = pVar.n;
        this.o = pVar.o;
        this.p = pVar.p;
        this.q = pVar.q;
    }
}
