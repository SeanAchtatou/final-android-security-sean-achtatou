package androidx.work.impl.m;

import androidx.room.b;
import androidx.room.i;
import b.m.a.f;

/* compiled from: WorkNameDao_Impl */
public final class l implements k {

    /* renamed from: a  reason: collision with root package name */
    private final i f2446a;

    /* renamed from: b  reason: collision with root package name */
    private final b<j> f2447b;

    /* compiled from: WorkNameDao_Impl */
    class a extends b<j> {
        a(l lVar, i iVar) {
            super(iVar);
        }

        public String c() {
            return "INSERT OR IGNORE INTO `WorkName` (`name`,`work_spec_id`) VALUES (?,?)";
        }

        public void a(f fVar, j jVar) {
            String str = jVar.f2444a;
            if (str == null) {
                fVar.c(1);
            } else {
                fVar.a(1, str);
            }
            String str2 = jVar.f2445b;
            if (str2 == null) {
                fVar.c(2);
            } else {
                fVar.a(2, str2);
            }
        }
    }

    public l(i iVar) {
        this.f2446a = iVar;
        this.f2447b = new a(this, iVar);
    }

    public void a(j jVar) {
        this.f2446a.b();
        this.f2446a.c();
        try {
            this.f2447b.a(jVar);
            this.f2446a.k();
        } finally {
            this.f2446a.e();
        }
    }
}
