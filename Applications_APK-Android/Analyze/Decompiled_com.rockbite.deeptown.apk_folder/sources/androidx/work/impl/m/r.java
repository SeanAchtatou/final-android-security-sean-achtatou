package androidx.work.impl.m;

import android.database.Cursor;
import androidx.room.l;
import androidx.room.o;
import androidx.work.impl.m.p;
import java.util.ArrayList;
import java.util.List;

/* compiled from: WorkSpecDao_Impl */
public final class r implements q {

    /* renamed from: a  reason: collision with root package name */
    private final androidx.room.i f2466a;

    /* renamed from: b  reason: collision with root package name */
    private final androidx.room.b<p> f2467b;

    /* renamed from: c  reason: collision with root package name */
    private final o f2468c;

    /* renamed from: d  reason: collision with root package name */
    private final o f2469d;

    /* renamed from: e  reason: collision with root package name */
    private final o f2470e;

    /* renamed from: f  reason: collision with root package name */
    private final o f2471f;

    /* renamed from: g  reason: collision with root package name */
    private final o f2472g;

    /* renamed from: h  reason: collision with root package name */
    private final o f2473h;

    /* renamed from: i  reason: collision with root package name */
    private final o f2474i;

    /* compiled from: WorkSpecDao_Impl */
    class a extends androidx.room.b<p> {
        a(r rVar, androidx.room.i iVar) {
            super(iVar);
        }

        public String c() {
            return "INSERT OR IGNORE INTO `WorkSpec` (`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`period_start_time`,`minimum_retention_duration`,`schedule_requested_at`,`run_in_foreground`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        public void a(b.m.a.f fVar, p pVar) {
            String str = pVar.f2453a;
            if (str == null) {
                fVar.c(1);
            } else {
                fVar.a(1, str);
            }
            fVar.a(2, (long) v.a(pVar.f2454b));
            String str2 = pVar.f2455c;
            if (str2 == null) {
                fVar.c(3);
            } else {
                fVar.a(3, str2);
            }
            String str3 = pVar.f2456d;
            if (str3 == null) {
                fVar.c(4);
            } else {
                fVar.a(4, str3);
            }
            byte[] a2 = androidx.work.e.a(pVar.f2457e);
            if (a2 == null) {
                fVar.c(5);
            } else {
                fVar.a(5, a2);
            }
            byte[] a3 = androidx.work.e.a(pVar.f2458f);
            if (a3 == null) {
                fVar.c(6);
            } else {
                fVar.a(6, a3);
            }
            fVar.a(7, pVar.f2459g);
            fVar.a(8, pVar.f2460h);
            fVar.a(9, pVar.f2461i);
            fVar.a(10, (long) pVar.f2463k);
            fVar.a(11, (long) v.a(pVar.l));
            fVar.a(12, pVar.m);
            fVar.a(13, pVar.n);
            fVar.a(14, pVar.o);
            fVar.a(15, pVar.p);
            fVar.a(16, pVar.q ? 1 : 0);
            androidx.work.c cVar = pVar.f2462j;
            if (cVar != null) {
                fVar.a(17, (long) v.a(cVar.b()));
                fVar.a(18, cVar.g() ? 1 : 0);
                fVar.a(19, cVar.h() ? 1 : 0);
                fVar.a(20, cVar.f() ? 1 : 0);
                fVar.a(21, cVar.i() ? 1 : 0);
                fVar.a(22, cVar.c());
                fVar.a(23, cVar.d());
                byte[] a4 = v.a(cVar.a());
                if (a4 == null) {
                    fVar.c(24);
                } else {
                    fVar.a(24, a4);
                }
            } else {
                fVar.c(17);
                fVar.c(18);
                fVar.c(19);
                fVar.c(20);
                fVar.c(21);
                fVar.c(22);
                fVar.c(23);
                fVar.c(24);
            }
        }
    }

    /* compiled from: WorkSpecDao_Impl */
    class b extends o {
        b(r rVar, androidx.room.i iVar) {
            super(iVar);
        }

        public String c() {
            return "DELETE FROM workspec WHERE id=?";
        }
    }

    /* compiled from: WorkSpecDao_Impl */
    class c extends o {
        c(r rVar, androidx.room.i iVar) {
            super(iVar);
        }

        public String c() {
            return "UPDATE workspec SET output=? WHERE id=?";
        }
    }

    /* compiled from: WorkSpecDao_Impl */
    class d extends o {
        d(r rVar, androidx.room.i iVar) {
            super(iVar);
        }

        public String c() {
            return "UPDATE workspec SET period_start_time=? WHERE id=?";
        }
    }

    /* compiled from: WorkSpecDao_Impl */
    class e extends o {
        e(r rVar, androidx.room.i iVar) {
            super(iVar);
        }

        public String c() {
            return "UPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?";
        }
    }

    /* compiled from: WorkSpecDao_Impl */
    class f extends o {
        f(r rVar, androidx.room.i iVar) {
            super(iVar);
        }

        public String c() {
            return "UPDATE workspec SET run_attempt_count=0 WHERE id=?";
        }
    }

    /* compiled from: WorkSpecDao_Impl */
    class g extends o {
        g(r rVar, androidx.room.i iVar) {
            super(iVar);
        }

        public String c() {
            return "UPDATE workspec SET schedule_requested_at=? WHERE id=?";
        }
    }

    /* compiled from: WorkSpecDao_Impl */
    class h extends o {
        h(r rVar, androidx.room.i iVar) {
            super(iVar);
        }

        public String c() {
            return "UPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)";
        }
    }

    /* compiled from: WorkSpecDao_Impl */
    class i extends o {
        i(r rVar, androidx.room.i iVar) {
            super(iVar);
        }

        public String c() {
            return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
        }
    }

    public r(androidx.room.i iVar) {
        this.f2466a = iVar;
        this.f2467b = new a(this, iVar);
        this.f2468c = new b(this, iVar);
        this.f2469d = new c(this, iVar);
        this.f2470e = new d(this, iVar);
        this.f2471f = new e(this, iVar);
        this.f2472g = new f(this, iVar);
        this.f2473h = new g(this, iVar);
        this.f2474i = new h(this, iVar);
        new i(this, iVar);
    }

    public void a(p pVar) {
        this.f2466a.b();
        this.f2466a.c();
        try {
            this.f2467b.a(pVar);
            this.f2466a.k();
        } finally {
            this.f2466a.e();
        }
    }

    public void b(String str, long j2) {
        this.f2466a.b();
        b.m.a.f a2 = this.f2470e.a();
        a2.a(1, j2);
        if (str == null) {
            a2.c(2);
        } else {
            a2.a(2, str);
        }
        this.f2466a.c();
        try {
            a2.F();
            this.f2466a.k();
        } finally {
            this.f2466a.e();
            this.f2470e.a(a2);
        }
    }

    public List<String> c(String str) {
        l b2 = l.b("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            b2.c(1);
        } else {
            b2.a(1, str);
        }
        this.f2466a.b();
        Cursor a2 = androidx.room.r.c.a(this.f2466a, b2, false, null);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.b();
        }
    }

    public int d() {
        this.f2466a.b();
        b.m.a.f a2 = this.f2474i.a();
        this.f2466a.c();
        try {
            int F = a2.F();
            this.f2466a.k();
            return F;
        } finally {
            this.f2466a.e();
            this.f2474i.a(a2);
        }
    }

    public p e(String str) {
        l lVar;
        p pVar;
        String str2 = str;
        l b2 = l.b("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE id=?", 1);
        if (str2 == null) {
            b2.c(1);
        } else {
            b2.a(1, str2);
        }
        this.f2466a.b();
        Cursor a2 = androidx.room.r.c.a(this.f2466a, b2, false, null);
        try {
            int a3 = androidx.room.r.b.a(a2, "required_network_type");
            int a4 = androidx.room.r.b.a(a2, "requires_charging");
            int a5 = androidx.room.r.b.a(a2, "requires_device_idle");
            int a6 = androidx.room.r.b.a(a2, "requires_battery_not_low");
            int a7 = androidx.room.r.b.a(a2, "requires_storage_not_low");
            int a8 = androidx.room.r.b.a(a2, "trigger_content_update_delay");
            int a9 = androidx.room.r.b.a(a2, "trigger_max_content_delay");
            int a10 = androidx.room.r.b.a(a2, "content_uri_triggers");
            int a11 = androidx.room.r.b.a(a2, "id");
            int a12 = androidx.room.r.b.a(a2, "state");
            int a13 = androidx.room.r.b.a(a2, "worker_class_name");
            int a14 = androidx.room.r.b.a(a2, "input_merger_class_name");
            int a15 = androidx.room.r.b.a(a2, "input");
            int a16 = androidx.room.r.b.a(a2, "output");
            lVar = b2;
            try {
                int a17 = androidx.room.r.b.a(a2, "initial_delay");
                int a18 = androidx.room.r.b.a(a2, "interval_duration");
                int a19 = androidx.room.r.b.a(a2, "flex_duration");
                int a20 = androidx.room.r.b.a(a2, "run_attempt_count");
                int a21 = androidx.room.r.b.a(a2, "backoff_policy");
                int a22 = androidx.room.r.b.a(a2, "backoff_delay_duration");
                int a23 = androidx.room.r.b.a(a2, "period_start_time");
                int a24 = androidx.room.r.b.a(a2, "minimum_retention_duration");
                int a25 = androidx.room.r.b.a(a2, "schedule_requested_at");
                int a26 = androidx.room.r.b.a(a2, "run_in_foreground");
                if (a2.moveToFirst()) {
                    String string = a2.getString(a11);
                    String string2 = a2.getString(a13);
                    int i2 = a26;
                    androidx.work.c cVar = new androidx.work.c();
                    cVar.a(v.b(a2.getInt(a3)));
                    cVar.b(a2.getInt(a4) != 0);
                    cVar.c(a2.getInt(a5) != 0);
                    cVar.a(a2.getInt(a6) != 0);
                    cVar.d(a2.getInt(a7) != 0);
                    cVar.a(a2.getLong(a8));
                    cVar.b(a2.getLong(a9));
                    cVar.a(v.a(a2.getBlob(a10)));
                    pVar = new p(string, string2);
                    pVar.f2454b = v.c(a2.getInt(a12));
                    pVar.f2456d = a2.getString(a14);
                    pVar.f2457e = androidx.work.e.b(a2.getBlob(a15));
                    pVar.f2458f = androidx.work.e.b(a2.getBlob(a16));
                    pVar.f2459g = a2.getLong(a17);
                    pVar.f2460h = a2.getLong(a18);
                    pVar.f2461i = a2.getLong(a19);
                    pVar.f2463k = a2.getInt(a20);
                    pVar.l = v.a(a2.getInt(a21));
                    pVar.m = a2.getLong(a22);
                    pVar.n = a2.getLong(a23);
                    pVar.o = a2.getLong(a24);
                    pVar.p = a2.getLong(a25);
                    pVar.q = a2.getInt(i2) != 0;
                    pVar.f2462j = cVar;
                } else {
                    pVar = null;
                }
                a2.close();
                lVar.b();
                return pVar;
            } catch (Throwable th) {
                th = th;
                a2.close();
                lVar.b();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            lVar = b2;
            a2.close();
            lVar.b();
            throw th;
        }
    }

    public int f(String str) {
        this.f2466a.b();
        b.m.a.f a2 = this.f2472g.a();
        if (str == null) {
            a2.c(1);
        } else {
            a2.a(1, str);
        }
        this.f2466a.c();
        try {
            int F = a2.F();
            this.f2466a.k();
            return F;
        } finally {
            this.f2466a.e();
            this.f2472g.a(a2);
        }
    }

    public List<String> g(String str) {
        l b2 = l.b("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM worktag WHERE tag=?)", 1);
        if (str == null) {
            b2.c(1);
        } else {
            b2.a(1, str);
        }
        this.f2466a.b();
        Cursor a2 = androidx.room.r.c.a(this.f2466a, b2, false, null);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.b();
        }
    }

    public List<androidx.work.e> h(String str) {
        l b2 = l.b("SELECT output FROM workspec WHERE id IN (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)", 1);
        if (str == null) {
            b2.c(1);
        } else {
            b2.a(1, str);
        }
        this.f2466a.b();
        Cursor a2 = androidx.room.r.c.a(this.f2466a, b2, false, null);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(androidx.work.e.b(a2.getBlob(0)));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.b();
        }
    }

    public int i(String str) {
        this.f2466a.b();
        b.m.a.f a2 = this.f2471f.a();
        if (str == null) {
            a2.c(1);
        } else {
            a2.a(1, str);
        }
        this.f2466a.c();
        try {
            int F = a2.F();
            this.f2466a.k();
            return F;
        } finally {
            this.f2466a.e();
            this.f2471f.a(a2);
        }
    }

    public void a(String str) {
        this.f2466a.b();
        b.m.a.f a2 = this.f2468c.a();
        if (str == null) {
            a2.c(1);
        } else {
            a2.a(1, str);
        }
        this.f2466a.c();
        try {
            a2.F();
            this.f2466a.k();
        } finally {
            this.f2466a.e();
            this.f2468c.a(a2);
        }
    }

    public androidx.work.r d(String str) {
        l b2 = l.b("SELECT state FROM workspec WHERE id=?", 1);
        if (str == null) {
            b2.c(1);
        } else {
            b2.a(1, str);
        }
        this.f2466a.b();
        androidx.work.r rVar = null;
        Cursor a2 = androidx.room.r.c.a(this.f2466a, b2, false, null);
        try {
            if (a2.moveToFirst()) {
                rVar = v.c(a2.getInt(0));
            }
            return rVar;
        } finally {
            a2.close();
            b2.b();
        }
    }

    public List<p.b> b(String str) {
        l b2 = l.b("SELECT id, state FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            b2.c(1);
        } else {
            b2.a(1, str);
        }
        this.f2466a.b();
        Cursor a2 = androidx.room.r.c.a(this.f2466a, b2, false, null);
        try {
            int a3 = androidx.room.r.b.a(a2, "id");
            int a4 = androidx.room.r.b.a(a2, "state");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                p.b bVar = new p.b();
                bVar.f2464a = a2.getString(a3);
                bVar.f2465b = v.c(a2.getInt(a4));
                arrayList.add(bVar);
            }
            return arrayList;
        } finally {
            a2.close();
            b2.b();
        }
    }

    public List<String> c() {
        l b2 = l.b("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5)", 0);
        this.f2466a.b();
        Cursor a2 = androidx.room.r.c.a(this.f2466a, b2, false, null);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.b();
        }
    }

    public void a(String str, androidx.work.e eVar) {
        this.f2466a.b();
        b.m.a.f a2 = this.f2469d.a();
        byte[] a3 = androidx.work.e.a(eVar);
        if (a3 == null) {
            a2.c(1);
        } else {
            a2.a(1, a3);
        }
        if (str == null) {
            a2.c(2);
        } else {
            a2.a(2, str);
        }
        this.f2466a.c();
        try {
            a2.F();
            this.f2466a.k();
        } finally {
            this.f2466a.e();
            this.f2469d.a(a2);
        }
    }

    public List<p> b() {
        l lVar;
        l b2 = l.b("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=1", 0);
        this.f2466a.b();
        Cursor a2 = androidx.room.r.c.a(this.f2466a, b2, false, null);
        try {
            int a3 = androidx.room.r.b.a(a2, "required_network_type");
            int a4 = androidx.room.r.b.a(a2, "requires_charging");
            int a5 = androidx.room.r.b.a(a2, "requires_device_idle");
            int a6 = androidx.room.r.b.a(a2, "requires_battery_not_low");
            int a7 = androidx.room.r.b.a(a2, "requires_storage_not_low");
            int a8 = androidx.room.r.b.a(a2, "trigger_content_update_delay");
            int a9 = androidx.room.r.b.a(a2, "trigger_max_content_delay");
            int a10 = androidx.room.r.b.a(a2, "content_uri_triggers");
            int a11 = androidx.room.r.b.a(a2, "id");
            int a12 = androidx.room.r.b.a(a2, "state");
            int a13 = androidx.room.r.b.a(a2, "worker_class_name");
            int a14 = androidx.room.r.b.a(a2, "input_merger_class_name");
            int a15 = androidx.room.r.b.a(a2, "input");
            int a16 = androidx.room.r.b.a(a2, "output");
            lVar = b2;
            try {
                int a17 = androidx.room.r.b.a(a2, "initial_delay");
                int a18 = androidx.room.r.b.a(a2, "interval_duration");
                int a19 = androidx.room.r.b.a(a2, "flex_duration");
                int a20 = androidx.room.r.b.a(a2, "run_attempt_count");
                int a21 = androidx.room.r.b.a(a2, "backoff_policy");
                int a22 = androidx.room.r.b.a(a2, "backoff_delay_duration");
                int a23 = androidx.room.r.b.a(a2, "period_start_time");
                int a24 = androidx.room.r.b.a(a2, "minimum_retention_duration");
                int a25 = androidx.room.r.b.a(a2, "schedule_requested_at");
                int a26 = androidx.room.r.b.a(a2, "run_in_foreground");
                int i2 = a16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(a11);
                    int i3 = a11;
                    String string2 = a2.getString(a13);
                    int i4 = a13;
                    androidx.work.c cVar = new androidx.work.c();
                    int i5 = a3;
                    cVar.a(v.b(a2.getInt(a3)));
                    cVar.b(a2.getInt(a4) != 0);
                    cVar.c(a2.getInt(a5) != 0);
                    cVar.a(a2.getInt(a6) != 0);
                    cVar.d(a2.getInt(a7) != 0);
                    int i6 = a4;
                    cVar.a(a2.getLong(a8));
                    cVar.b(a2.getLong(a9));
                    cVar.a(v.a(a2.getBlob(a10)));
                    p pVar = new p(string, string2);
                    pVar.f2454b = v.c(a2.getInt(a12));
                    pVar.f2456d = a2.getString(a14);
                    pVar.f2457e = androidx.work.e.b(a2.getBlob(a15));
                    int i7 = i2;
                    pVar.f2458f = androidx.work.e.b(a2.getBlob(i7));
                    int i8 = a15;
                    i2 = i7;
                    int i9 = a17;
                    pVar.f2459g = a2.getLong(i9);
                    a17 = i9;
                    int i10 = a18;
                    pVar.f2460h = a2.getLong(i10);
                    a18 = i10;
                    int i11 = a19;
                    pVar.f2461i = a2.getLong(i11);
                    int i12 = a20;
                    pVar.f2463k = a2.getInt(i12);
                    int i13 = a21;
                    a20 = i12;
                    pVar.l = v.a(a2.getInt(i13));
                    a19 = i11;
                    int i14 = a5;
                    int i15 = a22;
                    pVar.m = a2.getLong(i15);
                    a22 = i15;
                    int i16 = a23;
                    pVar.n = a2.getLong(i16);
                    a23 = i16;
                    int i17 = a24;
                    pVar.o = a2.getLong(i17);
                    a24 = i17;
                    int i18 = i13;
                    int i19 = a25;
                    pVar.p = a2.getLong(i19);
                    int i20 = a26;
                    pVar.q = a2.getInt(i20) != 0;
                    pVar.f2462j = cVar;
                    arrayList.add(pVar);
                    a26 = i20;
                    a25 = i19;
                    a15 = i8;
                    a11 = i3;
                    a13 = i4;
                    a3 = i5;
                    a4 = i6;
                    int i21 = i14;
                    a21 = i18;
                    a5 = i21;
                }
                a2.close();
                lVar.b();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                lVar.b();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            lVar = b2;
            a2.close();
            lVar.b();
            throw th;
        }
    }

    public int a(String str, long j2) {
        this.f2466a.b();
        b.m.a.f a2 = this.f2473h.a();
        a2.a(1, j2);
        if (str == null) {
            a2.c(2);
        } else {
            a2.a(2, str);
        }
        this.f2466a.c();
        try {
            int F = a2.F();
            this.f2466a.k();
            return F;
        } finally {
            this.f2466a.e();
            this.f2473h.a(a2);
        }
    }

    public List<p> a(int i2) {
        l lVar;
        l b2 = l.b("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=0 AND schedule_requested_at=-1 ORDER BY period_start_time LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND state NOT IN (2, 3, 5))", 1);
        b2.a(1, (long) i2);
        this.f2466a.b();
        Cursor a2 = androidx.room.r.c.a(this.f2466a, b2, false, null);
        try {
            int a3 = androidx.room.r.b.a(a2, "required_network_type");
            int a4 = androidx.room.r.b.a(a2, "requires_charging");
            int a5 = androidx.room.r.b.a(a2, "requires_device_idle");
            int a6 = androidx.room.r.b.a(a2, "requires_battery_not_low");
            int a7 = androidx.room.r.b.a(a2, "requires_storage_not_low");
            int a8 = androidx.room.r.b.a(a2, "trigger_content_update_delay");
            int a9 = androidx.room.r.b.a(a2, "trigger_max_content_delay");
            int a10 = androidx.room.r.b.a(a2, "content_uri_triggers");
            int a11 = androidx.room.r.b.a(a2, "id");
            int a12 = androidx.room.r.b.a(a2, "state");
            int a13 = androidx.room.r.b.a(a2, "worker_class_name");
            int a14 = androidx.room.r.b.a(a2, "input_merger_class_name");
            int a15 = androidx.room.r.b.a(a2, "input");
            int a16 = androidx.room.r.b.a(a2, "output");
            lVar = b2;
            try {
                int a17 = androidx.room.r.b.a(a2, "initial_delay");
                int a18 = androidx.room.r.b.a(a2, "interval_duration");
                int a19 = androidx.room.r.b.a(a2, "flex_duration");
                int a20 = androidx.room.r.b.a(a2, "run_attempt_count");
                int a21 = androidx.room.r.b.a(a2, "backoff_policy");
                int a22 = androidx.room.r.b.a(a2, "backoff_delay_duration");
                int a23 = androidx.room.r.b.a(a2, "period_start_time");
                int a24 = androidx.room.r.b.a(a2, "minimum_retention_duration");
                int a25 = androidx.room.r.b.a(a2, "schedule_requested_at");
                int a26 = androidx.room.r.b.a(a2, "run_in_foreground");
                int i3 = a16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(a11);
                    int i4 = a11;
                    String string2 = a2.getString(a13);
                    int i5 = a13;
                    androidx.work.c cVar = new androidx.work.c();
                    int i6 = a3;
                    cVar.a(v.b(a2.getInt(a3)));
                    cVar.b(a2.getInt(a4) != 0);
                    cVar.c(a2.getInt(a5) != 0);
                    cVar.a(a2.getInt(a6) != 0);
                    cVar.d(a2.getInt(a7) != 0);
                    int i7 = a4;
                    cVar.a(a2.getLong(a8));
                    cVar.b(a2.getLong(a9));
                    cVar.a(v.a(a2.getBlob(a10)));
                    p pVar = new p(string, string2);
                    pVar.f2454b = v.c(a2.getInt(a12));
                    pVar.f2456d = a2.getString(a14);
                    pVar.f2457e = androidx.work.e.b(a2.getBlob(a15));
                    int i8 = i3;
                    pVar.f2458f = androidx.work.e.b(a2.getBlob(i8));
                    int i9 = a14;
                    i3 = i8;
                    int i10 = a17;
                    pVar.f2459g = a2.getLong(i10);
                    a17 = i10;
                    int i11 = a18;
                    pVar.f2460h = a2.getLong(i11);
                    a18 = i11;
                    int i12 = a19;
                    pVar.f2461i = a2.getLong(i12);
                    int i13 = a20;
                    pVar.f2463k = a2.getInt(i13);
                    int i14 = a21;
                    a20 = i13;
                    pVar.l = v.a(a2.getInt(i14));
                    a19 = i12;
                    int i15 = a5;
                    int i16 = a22;
                    pVar.m = a2.getLong(i16);
                    a22 = i16;
                    int i17 = a23;
                    pVar.n = a2.getLong(i17);
                    a23 = i17;
                    int i18 = a24;
                    pVar.o = a2.getLong(i18);
                    a24 = i18;
                    int i19 = i14;
                    int i20 = a25;
                    pVar.p = a2.getLong(i20);
                    int i21 = a26;
                    pVar.q = a2.getInt(i21) != 0;
                    pVar.f2462j = cVar;
                    arrayList.add(pVar);
                    a26 = i21;
                    a25 = i20;
                    a14 = i9;
                    a11 = i4;
                    a13 = i5;
                    a4 = i7;
                    a3 = i6;
                    int i22 = i15;
                    a21 = i19;
                    a5 = i22;
                }
                a2.close();
                lVar.b();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                lVar.b();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            lVar = b2;
            a2.close();
            lVar.b();
            throw th;
        }
    }

    public List<p> a() {
        l lVar;
        l b2 = l.b("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=0 AND schedule_requested_at<>-1", 0);
        this.f2466a.b();
        Cursor a2 = androidx.room.r.c.a(this.f2466a, b2, false, null);
        try {
            int a3 = androidx.room.r.b.a(a2, "required_network_type");
            int a4 = androidx.room.r.b.a(a2, "requires_charging");
            int a5 = androidx.room.r.b.a(a2, "requires_device_idle");
            int a6 = androidx.room.r.b.a(a2, "requires_battery_not_low");
            int a7 = androidx.room.r.b.a(a2, "requires_storage_not_low");
            int a8 = androidx.room.r.b.a(a2, "trigger_content_update_delay");
            int a9 = androidx.room.r.b.a(a2, "trigger_max_content_delay");
            int a10 = androidx.room.r.b.a(a2, "content_uri_triggers");
            int a11 = androidx.room.r.b.a(a2, "id");
            int a12 = androidx.room.r.b.a(a2, "state");
            int a13 = androidx.room.r.b.a(a2, "worker_class_name");
            int a14 = androidx.room.r.b.a(a2, "input_merger_class_name");
            int a15 = androidx.room.r.b.a(a2, "input");
            int a16 = androidx.room.r.b.a(a2, "output");
            lVar = b2;
            try {
                int a17 = androidx.room.r.b.a(a2, "initial_delay");
                int a18 = androidx.room.r.b.a(a2, "interval_duration");
                int a19 = androidx.room.r.b.a(a2, "flex_duration");
                int a20 = androidx.room.r.b.a(a2, "run_attempt_count");
                int a21 = androidx.room.r.b.a(a2, "backoff_policy");
                int a22 = androidx.room.r.b.a(a2, "backoff_delay_duration");
                int a23 = androidx.room.r.b.a(a2, "period_start_time");
                int a24 = androidx.room.r.b.a(a2, "minimum_retention_duration");
                int a25 = androidx.room.r.b.a(a2, "schedule_requested_at");
                int a26 = androidx.room.r.b.a(a2, "run_in_foreground");
                int i2 = a16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(a11);
                    int i3 = a11;
                    String string2 = a2.getString(a13);
                    int i4 = a13;
                    androidx.work.c cVar = new androidx.work.c();
                    int i5 = a3;
                    cVar.a(v.b(a2.getInt(a3)));
                    cVar.b(a2.getInt(a4) != 0);
                    cVar.c(a2.getInt(a5) != 0);
                    cVar.a(a2.getInt(a6) != 0);
                    cVar.d(a2.getInt(a7) != 0);
                    int i6 = a4;
                    cVar.a(a2.getLong(a8));
                    cVar.b(a2.getLong(a9));
                    cVar.a(v.a(a2.getBlob(a10)));
                    p pVar = new p(string, string2);
                    pVar.f2454b = v.c(a2.getInt(a12));
                    pVar.f2456d = a2.getString(a14);
                    pVar.f2457e = androidx.work.e.b(a2.getBlob(a15));
                    int i7 = i2;
                    pVar.f2458f = androidx.work.e.b(a2.getBlob(i7));
                    int i8 = a15;
                    i2 = i7;
                    int i9 = a17;
                    pVar.f2459g = a2.getLong(i9);
                    a17 = i9;
                    int i10 = a18;
                    pVar.f2460h = a2.getLong(i10);
                    a18 = i10;
                    int i11 = a19;
                    pVar.f2461i = a2.getLong(i11);
                    int i12 = a20;
                    pVar.f2463k = a2.getInt(i12);
                    int i13 = a21;
                    a20 = i12;
                    pVar.l = v.a(a2.getInt(i13));
                    a19 = i11;
                    int i14 = a5;
                    int i15 = a22;
                    pVar.m = a2.getLong(i15);
                    a22 = i15;
                    int i16 = a23;
                    pVar.n = a2.getLong(i16);
                    a23 = i16;
                    int i17 = a24;
                    pVar.o = a2.getLong(i17);
                    a24 = i17;
                    int i18 = i13;
                    int i19 = a25;
                    pVar.p = a2.getLong(i19);
                    int i20 = a26;
                    pVar.q = a2.getInt(i20) != 0;
                    pVar.f2462j = cVar;
                    arrayList.add(pVar);
                    a26 = i20;
                    a25 = i19;
                    a15 = i8;
                    a11 = i3;
                    a13 = i4;
                    a3 = i5;
                    a4 = i6;
                    int i21 = i14;
                    a21 = i18;
                    a5 = i21;
                }
                a2.close();
                lVar.b();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                lVar.b();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            lVar = b2;
            a2.close();
            lVar.b();
            throw th;
        }
    }

    public int a(androidx.work.r rVar, String... strArr) {
        this.f2466a.b();
        StringBuilder a2 = androidx.room.r.e.a();
        a2.append("UPDATE workspec SET state=");
        a2.append("?");
        a2.append(" WHERE id IN (");
        androidx.room.r.e.a(a2, strArr.length);
        a2.append(")");
        b.m.a.f a3 = this.f2466a.a(a2.toString());
        a3.a(1, (long) v.a(rVar));
        int i2 = 2;
        for (String str : strArr) {
            if (str == null) {
                a3.c(i2);
            } else {
                a3.a(i2, str);
            }
            i2++;
        }
        this.f2466a.c();
        try {
            int F = a3.F();
            this.f2466a.k();
            return F;
        } finally {
            this.f2466a.e();
        }
    }
}
