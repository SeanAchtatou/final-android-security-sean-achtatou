package androidx.work.impl.m;

import android.database.Cursor;
import androidx.room.b;
import androidx.room.i;
import androidx.room.l;
import b.m.a.f;
import java.util.ArrayList;
import java.util.List;

/* compiled from: DependencyDao_Impl */
public final class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private final i f2433a;

    /* renamed from: b  reason: collision with root package name */
    private final b<a> f2434b;

    /* compiled from: DependencyDao_Impl */
    class a extends b<a> {
        a(c cVar, i iVar) {
            super(iVar);
        }

        public String c() {
            return "INSERT OR IGNORE INTO `Dependency` (`work_spec_id`,`prerequisite_id`) VALUES (?,?)";
        }

        public void a(f fVar, a aVar) {
            String str = aVar.f2431a;
            if (str == null) {
                fVar.c(1);
            } else {
                fVar.a(1, str);
            }
            String str2 = aVar.f2432b;
            if (str2 == null) {
                fVar.c(2);
            } else {
                fVar.a(2, str2);
            }
        }
    }

    public c(i iVar) {
        this.f2433a = iVar;
        this.f2434b = new a(this, iVar);
    }

    public void a(a aVar) {
        this.f2433a.b();
        this.f2433a.c();
        try {
            this.f2434b.a(aVar);
            this.f2433a.k();
        } finally {
            this.f2433a.e();
        }
    }

    public boolean b(String str) {
        l b2 = l.b("SELECT COUNT(*)=0 FROM dependency WHERE work_spec_id=? AND prerequisite_id IN (SELECT id FROM workspec WHERE state!=2)", 1);
        if (str == null) {
            b2.c(1);
        } else {
            b2.a(1, str);
        }
        this.f2433a.b();
        boolean z = false;
        Cursor a2 = androidx.room.r.c.a(this.f2433a, b2, false, null);
        try {
            if (a2.moveToFirst() && a2.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            a2.close();
            b2.b();
        }
    }

    public boolean c(String str) {
        l b2 = l.b("SELECT COUNT(*)>0 FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            b2.c(1);
        } else {
            b2.a(1, str);
        }
        this.f2433a.b();
        boolean z = false;
        Cursor a2 = androidx.room.r.c.a(this.f2433a, b2, false, null);
        try {
            if (a2.moveToFirst() && a2.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            a2.close();
            b2.b();
        }
    }

    public List<String> a(String str) {
        l b2 = l.b("SELECT work_spec_id FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            b2.c(1);
        } else {
            b2.a(1, str);
        }
        this.f2433a.b();
        Cursor a2 = androidx.room.r.c.a(this.f2433a, b2, false, null);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.b();
        }
    }
}
