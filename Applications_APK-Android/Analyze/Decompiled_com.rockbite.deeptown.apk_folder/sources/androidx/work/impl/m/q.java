package androidx.work.impl.m;

import androidx.work.e;
import androidx.work.impl.m.p;
import androidx.work.r;
import java.util.List;

/* compiled from: WorkSpecDao */
public interface q {
    int a(r rVar, String... strArr);

    int a(String str, long j2);

    List<p> a();

    List<p> a(int i2);

    void a(p pVar);

    void a(String str);

    void a(String str, e eVar);

    List<p> b();

    List<p.b> b(String str);

    void b(String str, long j2);

    List<String> c();

    List<String> c(String str);

    int d();

    r d(String str);

    p e(String str);

    int f(String str);

    List<String> g(String str);

    List<e> h(String str);

    int i(String str);
}
