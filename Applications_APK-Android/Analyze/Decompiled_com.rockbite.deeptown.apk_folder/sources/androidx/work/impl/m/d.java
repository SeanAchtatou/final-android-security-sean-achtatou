package androidx.work.impl.m;

/* compiled from: Preference */
public class d {

    /* renamed from: a  reason: collision with root package name */
    public String f2435a;

    /* renamed from: b  reason: collision with root package name */
    public Long f2436b;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public d(String str, boolean z) {
        this(str, z ? 1 : 0);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        if (!this.f2435a.equals(dVar.f2435a)) {
            return false;
        }
        Long l = this.f2436b;
        Long l2 = dVar.f2436b;
        if (l != null) {
            return l.equals(l2);
        }
        if (l2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = this.f2435a.hashCode() * 31;
        Long l = this.f2436b;
        return hashCode + (l != null ? l.hashCode() : 0);
    }

    public d(String str, long j2) {
        this.f2435a = str;
        this.f2436b = Long.valueOf(j2);
    }
}
