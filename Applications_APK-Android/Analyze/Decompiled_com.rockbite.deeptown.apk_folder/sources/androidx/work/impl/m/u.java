package androidx.work.impl.m;

import android.database.Cursor;
import androidx.room.b;
import androidx.room.i;
import androidx.room.l;
import androidx.room.r.c;
import b.m.a.f;
import java.util.ArrayList;
import java.util.List;

/* compiled from: WorkTagDao_Impl */
public final class u implements t {

    /* renamed from: a  reason: collision with root package name */
    private final i f2477a;

    /* renamed from: b  reason: collision with root package name */
    private final b<s> f2478b;

    /* compiled from: WorkTagDao_Impl */
    class a extends b<s> {
        a(u uVar, i iVar) {
            super(iVar);
        }

        public String c() {
            return "INSERT OR IGNORE INTO `WorkTag` (`tag`,`work_spec_id`) VALUES (?,?)";
        }

        public void a(f fVar, s sVar) {
            String str = sVar.f2475a;
            if (str == null) {
                fVar.c(1);
            } else {
                fVar.a(1, str);
            }
            String str2 = sVar.f2476b;
            if (str2 == null) {
                fVar.c(2);
            } else {
                fVar.a(2, str2);
            }
        }
    }

    public u(i iVar) {
        this.f2477a = iVar;
        this.f2478b = new a(this, iVar);
    }

    public void a(s sVar) {
        this.f2477a.b();
        this.f2477a.c();
        try {
            this.f2478b.a(sVar);
            this.f2477a.k();
        } finally {
            this.f2477a.e();
        }
    }

    public List<String> a(String str) {
        l b2 = l.b("SELECT DISTINCT tag FROM worktag WHERE work_spec_id=?", 1);
        if (str == null) {
            b2.c(1);
        } else {
            b2.a(1, str);
        }
        this.f2477a.b();
        Cursor a2 = c.a(this.f2477a, b2, false, null);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.b();
        }
    }
}
