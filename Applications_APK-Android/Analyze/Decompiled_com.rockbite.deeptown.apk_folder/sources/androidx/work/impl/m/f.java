package androidx.work.impl.m;

import android.database.Cursor;
import androidx.room.b;
import androidx.room.i;
import androidx.room.l;
import androidx.room.r.c;

/* compiled from: PreferenceDao_Impl */
public final class f implements e {

    /* renamed from: a  reason: collision with root package name */
    private final i f2437a;

    /* renamed from: b  reason: collision with root package name */
    private final b<d> f2438b;

    /* compiled from: PreferenceDao_Impl */
    class a extends b<d> {
        a(f fVar, i iVar) {
            super(iVar);
        }

        public String c() {
            return "INSERT OR REPLACE INTO `Preference` (`key`,`long_value`) VALUES (?,?)";
        }

        public void a(b.m.a.f fVar, d dVar) {
            String str = dVar.f2435a;
            if (str == null) {
                fVar.c(1);
            } else {
                fVar.a(1, str);
            }
            Long l = dVar.f2436b;
            if (l == null) {
                fVar.c(2);
            } else {
                fVar.a(2, l.longValue());
            }
        }
    }

    public f(i iVar) {
        this.f2437a = iVar;
        this.f2438b = new a(this, iVar);
    }

    public void a(d dVar) {
        this.f2437a.b();
        this.f2437a.c();
        try {
            this.f2438b.a(dVar);
            this.f2437a.k();
        } finally {
            this.f2437a.e();
        }
    }

    public Long a(String str) {
        l b2 = l.b("SELECT long_value FROM Preference where `key`=?", 1);
        if (str == null) {
            b2.c(1);
        } else {
            b2.a(1, str);
        }
        this.f2437a.b();
        Long l = null;
        Cursor a2 = c.a(this.f2437a, b2, false, null);
        try {
            if (a2.moveToFirst()) {
                if (!a2.isNull(0)) {
                    l = Long.valueOf(a2.getLong(0));
                }
            }
            return l;
        } finally {
            a2.close();
            b2.b();
        }
    }
}
