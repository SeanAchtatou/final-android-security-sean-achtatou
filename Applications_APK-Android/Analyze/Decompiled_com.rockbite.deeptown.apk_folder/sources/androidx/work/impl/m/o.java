package androidx.work.impl.m;

import androidx.room.i;
import androidx.work.e;
import b.m.a.f;

/* compiled from: WorkProgressDao_Impl */
public final class o implements n {

    /* renamed from: a  reason: collision with root package name */
    private final i f2450a;

    /* renamed from: b  reason: collision with root package name */
    private final androidx.room.o f2451b;

    /* renamed from: c  reason: collision with root package name */
    private final androidx.room.o f2452c;

    /* compiled from: WorkProgressDao_Impl */
    class a extends androidx.room.b<m> {
        a(o oVar, i iVar) {
            super(iVar);
        }

        public String c() {
            return "INSERT OR REPLACE INTO `WorkProgress` (`work_spec_id`,`progress`) VALUES (?,?)";
        }

        public void a(f fVar, m mVar) {
            String str = mVar.f2448a;
            if (str == null) {
                fVar.c(1);
            } else {
                fVar.a(1, str);
            }
            byte[] a2 = e.a(mVar.f2449b);
            if (a2 == null) {
                fVar.c(2);
            } else {
                fVar.a(2, a2);
            }
        }
    }

    /* compiled from: WorkProgressDao_Impl */
    class b extends androidx.room.o {
        b(o oVar, i iVar) {
            super(iVar);
        }

        public String c() {
            return "DELETE from WorkProgress where work_spec_id=?";
        }
    }

    /* compiled from: WorkProgressDao_Impl */
    class c extends androidx.room.o {
        c(o oVar, i iVar) {
            super(iVar);
        }

        public String c() {
            return "DELETE FROM WorkProgress";
        }
    }

    public o(i iVar) {
        this.f2450a = iVar;
        new a(this, iVar);
        this.f2451b = new b(this, iVar);
        this.f2452c = new c(this, iVar);
    }

    public void a(String str) {
        this.f2450a.b();
        f a2 = this.f2451b.a();
        if (str == null) {
            a2.c(1);
        } else {
            a2.a(1, str);
        }
        this.f2450a.c();
        try {
            a2.F();
            this.f2450a.k();
        } finally {
            this.f2450a.e();
            this.f2451b.a(a2);
        }
    }

    public void a() {
        this.f2450a.b();
        f a2 = this.f2452c.a();
        this.f2450a.c();
        try {
            a2.F();
            this.f2450a.k();
        } finally {
            this.f2450a.e();
            this.f2452c.a(a2);
        }
    }
}
