package androidx.work.impl.m;

import android.database.Cursor;
import androidx.room.l;
import androidx.room.o;
import androidx.room.r.c;
import b.m.a.f;

/* compiled from: SystemIdInfoDao_Impl */
public final class i implements h {

    /* renamed from: a  reason: collision with root package name */
    private final androidx.room.i f2441a;

    /* renamed from: b  reason: collision with root package name */
    private final androidx.room.b<g> f2442b;

    /* renamed from: c  reason: collision with root package name */
    private final o f2443c;

    /* compiled from: SystemIdInfoDao_Impl */
    class a extends androidx.room.b<g> {
        a(i iVar, androidx.room.i iVar2) {
            super(iVar2);
        }

        public String c() {
            return "INSERT OR REPLACE INTO `SystemIdInfo` (`work_spec_id`,`system_id`) VALUES (?,?)";
        }

        public void a(f fVar, g gVar) {
            String str = gVar.f2439a;
            if (str == null) {
                fVar.c(1);
            } else {
                fVar.a(1, str);
            }
            fVar.a(2, (long) gVar.f2440b);
        }
    }

    /* compiled from: SystemIdInfoDao_Impl */
    class b extends o {
        b(i iVar, androidx.room.i iVar2) {
            super(iVar2);
        }

        public String c() {
            return "DELETE FROM SystemIdInfo where work_spec_id=?";
        }
    }

    public i(androidx.room.i iVar) {
        this.f2441a = iVar;
        this.f2442b = new a(this, iVar);
        this.f2443c = new b(this, iVar);
    }

    public void a(g gVar) {
        this.f2441a.b();
        this.f2441a.c();
        try {
            this.f2442b.a(gVar);
            this.f2441a.k();
        } finally {
            this.f2441a.e();
        }
    }

    public void b(String str) {
        this.f2441a.b();
        f a2 = this.f2443c.a();
        if (str == null) {
            a2.c(1);
        } else {
            a2.a(1, str);
        }
        this.f2441a.c();
        try {
            a2.F();
            this.f2441a.k();
        } finally {
            this.f2441a.e();
            this.f2443c.a(a2);
        }
    }

    public g a(String str) {
        l b2 = l.b("SELECT `SystemIdInfo`.`work_spec_id` AS `work_spec_id`, `SystemIdInfo`.`system_id` AS `system_id` FROM SystemIdInfo WHERE work_spec_id=?", 1);
        if (str == null) {
            b2.c(1);
        } else {
            b2.a(1, str);
        }
        this.f2441a.b();
        g gVar = null;
        Cursor a2 = c.a(this.f2441a, b2, false, null);
        try {
            int a3 = androidx.room.r.b.a(a2, "work_spec_id");
            int a4 = androidx.room.r.b.a(a2, "system_id");
            if (a2.moveToFirst()) {
                gVar = new g(a2.getString(a3), a2.getInt(a4));
            }
            return gVar;
        } finally {
            a2.close();
            b2.b();
        }
    }
}
