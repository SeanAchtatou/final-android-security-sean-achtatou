package androidx.work.impl.background.systemalarm;

import android.content.Intent;
import androidx.lifecycle.k;
import androidx.work.impl.background.systemalarm.e;
import androidx.work.impl.utils.i;

public class SystemAlarmService extends k implements e.c {

    /* renamed from: d  reason: collision with root package name */
    private static final String f2233d = androidx.work.k.a("SystemAlarmService");

    /* renamed from: b  reason: collision with root package name */
    private e f2234b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f2235c;

    private void b() {
        this.f2234b = new e(this);
        this.f2234b.a(this);
    }

    public void a() {
        this.f2235c = true;
        androidx.work.k.a().a(f2233d, "All commands completed in dispatcher", new Throwable[0]);
        i.a();
        stopSelf();
    }

    public void onCreate() {
        super.onCreate();
        b();
        this.f2235c = false;
    }

    public void onDestroy() {
        super.onDestroy();
        this.f2235c = true;
        this.f2234b.f();
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        super.onStartCommand(intent, i2, i3);
        if (this.f2235c) {
            androidx.work.k.a().c(f2233d, "Re-initializing SystemAlarmDispatcher after a request to shut-down.", new Throwable[0]);
            this.f2234b.f();
            b();
            this.f2235c = false;
        }
        if (intent == null) {
            return 3;
        }
        this.f2234b.a(intent, i3);
        return 3;
    }
}
