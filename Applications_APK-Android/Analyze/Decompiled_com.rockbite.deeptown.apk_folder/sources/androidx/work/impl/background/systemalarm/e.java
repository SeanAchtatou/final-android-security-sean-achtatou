package androidx.work.impl.background.systemalarm;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.text.TextUtils;
import androidx.work.impl.i;
import androidx.work.impl.utils.f;
import androidx.work.impl.utils.l;
import androidx.work.k;
import java.util.ArrayList;
import java.util.List;

/* compiled from: SystemAlarmDispatcher */
public class e implements androidx.work.impl.a {

    /* renamed from: k  reason: collision with root package name */
    static final String f2256k = k.a("SystemAlarmDispatcher");

    /* renamed from: a  reason: collision with root package name */
    final Context f2257a;

    /* renamed from: b  reason: collision with root package name */
    private final androidx.work.impl.utils.n.a f2258b;

    /* renamed from: c  reason: collision with root package name */
    private final l f2259c;

    /* renamed from: d  reason: collision with root package name */
    private final androidx.work.impl.c f2260d;

    /* renamed from: e  reason: collision with root package name */
    private final i f2261e;

    /* renamed from: f  reason: collision with root package name */
    final b f2262f;

    /* renamed from: g  reason: collision with root package name */
    private final Handler f2263g;

    /* renamed from: h  reason: collision with root package name */
    final List<Intent> f2264h;

    /* renamed from: i  reason: collision with root package name */
    Intent f2265i;

    /* renamed from: j  reason: collision with root package name */
    private c f2266j;

    /* compiled from: SystemAlarmDispatcher */
    class a implements Runnable {
        a() {
        }

        public void run() {
            d dVar;
            e eVar;
            synchronized (e.this.f2264h) {
                e.this.f2265i = e.this.f2264h.get(0);
            }
            Intent intent = e.this.f2265i;
            if (intent != null) {
                String action = intent.getAction();
                int intExtra = e.this.f2265i.getIntExtra("KEY_START_ID", 0);
                k.a().a(e.f2256k, String.format("Processing command %s, %s", e.this.f2265i, Integer.valueOf(intExtra)), new Throwable[0]);
                PowerManager.WakeLock a2 = androidx.work.impl.utils.i.a(e.this.f2257a, String.format("%s (%s)", action, Integer.valueOf(intExtra)));
                try {
                    k.a().a(e.f2256k, String.format("Acquiring operation wake lock (%s) %s", action, a2), new Throwable[0]);
                    a2.acquire();
                    e.this.f2262f.a(e.this.f2265i, intExtra, e.this);
                    k.a().a(e.f2256k, String.format("Releasing operation wake lock (%s) %s", action, a2), new Throwable[0]);
                    a2.release();
                    eVar = e.this;
                    dVar = new d(eVar);
                } catch (Throwable th) {
                    k.a().a(e.f2256k, String.format("Releasing operation wake lock (%s) %s", action, a2), new Throwable[0]);
                    a2.release();
                    e eVar2 = e.this;
                    eVar2.a(new d(eVar2));
                    throw th;
                }
                eVar.a(dVar);
            }
        }
    }

    /* compiled from: SystemAlarmDispatcher */
    static class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final e f2268a;

        /* renamed from: b  reason: collision with root package name */
        private final Intent f2269b;

        /* renamed from: c  reason: collision with root package name */
        private final int f2270c;

        b(e eVar, Intent intent, int i2) {
            this.f2268a = eVar;
            this.f2269b = intent;
            this.f2270c = i2;
        }

        public void run() {
            this.f2268a.a(this.f2269b, this.f2270c);
        }
    }

    /* compiled from: SystemAlarmDispatcher */
    interface c {
        void a();
    }

    /* compiled from: SystemAlarmDispatcher */
    static class d implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final e f2271a;

        d(e eVar) {
            this.f2271a = eVar;
        }

        public void run() {
            this.f2271a.a();
        }
    }

    e(Context context) {
        this(context, null, null);
    }

    private void g() {
        if (this.f2263g.getLooper().getThread() != Thread.currentThread()) {
            throw new IllegalStateException("Needs to be invoked on the main thread.");
        }
    }

    private void h() {
        g();
        PowerManager.WakeLock a2 = androidx.work.impl.utils.i.a(this.f2257a, "ProcessCommand");
        try {
            a2.acquire();
            this.f2261e.g().a(new a());
        } finally {
            a2.release();
        }
    }

    public void a(String str, boolean z) {
        a(new b(this, b.a(this.f2257a, str, z), 0));
    }

    /* access modifiers changed from: package-private */
    public androidx.work.impl.c b() {
        return this.f2260d;
    }

    /* access modifiers changed from: package-private */
    public androidx.work.impl.utils.n.a c() {
        return this.f2258b;
    }

    /* access modifiers changed from: package-private */
    public i d() {
        return this.f2261e;
    }

    /* access modifiers changed from: package-private */
    public l e() {
        return this.f2259c;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        k.a().a(f2256k, "Destroying SystemAlarmDispatcher", new Throwable[0]);
        this.f2260d.b(this);
        this.f2259c.a();
        this.f2266j = null;
    }

    e(Context context, androidx.work.impl.c cVar, i iVar) {
        this.f2257a = context.getApplicationContext();
        this.f2262f = new b(this.f2257a);
        this.f2259c = new l();
        this.f2261e = iVar == null ? i.a(context) : iVar;
        this.f2260d = cVar == null ? this.f2261e.d() : cVar;
        this.f2258b = this.f2261e.g();
        this.f2260d.a(this);
        this.f2264h = new ArrayList();
        this.f2265i = null;
        this.f2263g = new Handler(Looper.getMainLooper());
    }

    public boolean a(Intent intent, int i2) {
        boolean z = false;
        k.a().a(f2256k, String.format("Adding command %s (%s)", intent, Integer.valueOf(i2)), new Throwable[0]);
        g();
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            k.a().e(f2256k, "Unknown command. Ignoring", new Throwable[0]);
            return false;
        } else if ("ACTION_CONSTRAINTS_CHANGED".equals(action) && a("ACTION_CONSTRAINTS_CHANGED")) {
            return false;
        } else {
            intent.putExtra("KEY_START_ID", i2);
            synchronized (this.f2264h) {
                if (!this.f2264h.isEmpty()) {
                    z = true;
                }
                this.f2264h.add(intent);
                if (!z) {
                    h();
                }
            }
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar) {
        if (this.f2266j != null) {
            k.a().b(f2256k, "A completion listener for SystemAlarmDispatcher already exists.", new Throwable[0]);
        } else {
            this.f2266j = cVar;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Runnable runnable) {
        this.f2263g.post(runnable);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        k.a().a(f2256k, "Checking if commands are complete.", new Throwable[0]);
        g();
        synchronized (this.f2264h) {
            if (this.f2265i != null) {
                k.a().a(f2256k, String.format("Removing command %s", this.f2265i), new Throwable[0]);
                if (this.f2264h.remove(0).equals(this.f2265i)) {
                    this.f2265i = null;
                } else {
                    throw new IllegalStateException("Dequeue-d command is not the first.");
                }
            }
            f b2 = this.f2258b.b();
            if (!this.f2262f.a() && this.f2264h.isEmpty() && !b2.a()) {
                k.a().a(f2256k, "No more commands & intents.", new Throwable[0]);
                if (this.f2266j != null) {
                    this.f2266j.a();
                }
            } else if (!this.f2264h.isEmpty()) {
                h();
            }
        }
    }

    private boolean a(String str) {
        g();
        synchronized (this.f2264h) {
            for (Intent action : this.f2264h) {
                if (str.equals(action.getAction())) {
                    return true;
                }
            }
            return false;
        }
    }
}
