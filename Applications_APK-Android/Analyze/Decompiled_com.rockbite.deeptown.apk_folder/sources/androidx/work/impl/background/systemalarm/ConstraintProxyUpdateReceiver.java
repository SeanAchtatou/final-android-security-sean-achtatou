package androidx.work.impl.background.systemalarm;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import androidx.work.impl.background.systemalarm.ConstraintProxy;
import androidx.work.impl.i;
import androidx.work.impl.utils.d;
import androidx.work.k;

public class ConstraintProxyUpdateReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    static final String f2228a = k.a("ConstrntProxyUpdtRecvr");

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Intent f2229a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Context f2230b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ BroadcastReceiver.PendingResult f2231c;

        a(ConstraintProxyUpdateReceiver constraintProxyUpdateReceiver, Intent intent, Context context, BroadcastReceiver.PendingResult pendingResult) {
            this.f2229a = intent;
            this.f2230b = context;
            this.f2231c = pendingResult;
        }

        public void run() {
            try {
                boolean booleanExtra = this.f2229a.getBooleanExtra("KEY_BATTERY_NOT_LOW_PROXY_ENABLED", false);
                boolean booleanExtra2 = this.f2229a.getBooleanExtra("KEY_BATTERY_CHARGING_PROXY_ENABLED", false);
                boolean booleanExtra3 = this.f2229a.getBooleanExtra("KEY_STORAGE_NOT_LOW_PROXY_ENABLED", false);
                boolean booleanExtra4 = this.f2229a.getBooleanExtra("KEY_NETWORK_STATE_PROXY_ENABLED", false);
                k.a().a(ConstraintProxyUpdateReceiver.f2228a, String.format("Updating proxies: BatteryNotLowProxy enabled (%s), BatteryChargingProxy enabled (%s), StorageNotLowProxy (%s), NetworkStateProxy enabled (%s)", Boolean.valueOf(booleanExtra), Boolean.valueOf(booleanExtra2), Boolean.valueOf(booleanExtra3), Boolean.valueOf(booleanExtra4)), new Throwable[0]);
                d.a(this.f2230b, ConstraintProxy.BatteryNotLowProxy.class, booleanExtra);
                d.a(this.f2230b, ConstraintProxy.BatteryChargingProxy.class, booleanExtra2);
                d.a(this.f2230b, ConstraintProxy.StorageNotLowProxy.class, booleanExtra3);
                d.a(this.f2230b, ConstraintProxy.NetworkStateProxy.class, booleanExtra4);
            } finally {
                this.f2231c.finish();
            }
        }
    }

    public static Intent a(Context context, boolean z, boolean z2, boolean z3, boolean z4) {
        Intent intent = new Intent("androidx.work.impl.background.systemalarm.UpdateProxies");
        intent.setComponent(new ComponentName(context, ConstraintProxyUpdateReceiver.class));
        intent.putExtra("KEY_BATTERY_NOT_LOW_PROXY_ENABLED", z).putExtra("KEY_BATTERY_CHARGING_PROXY_ENABLED", z2).putExtra("KEY_STORAGE_NOT_LOW_PROXY_ENABLED", z3).putExtra("KEY_NETWORK_STATE_PROXY_ENABLED", z4);
        return intent;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        if (!"androidx.work.impl.background.systemalarm.UpdateProxies".equals(action)) {
            k.a().a(f2228a, String.format("Ignoring unknown action %s", action), new Throwable[0]);
        } else {
            i.a(context).g().a(new a(this, intent, context, goAsync()));
        }
    }
}
