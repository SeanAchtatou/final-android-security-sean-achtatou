package androidx.work.impl.background.systemalarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.work.impl.i;
import androidx.work.k;

public class RescheduleReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2232a = k.a("RescheduleReceiver");

    public void onReceive(Context context, Intent intent) {
        k.a().a(f2232a, String.format("Received intent %s", intent), new Throwable[0]);
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                i.a(context).a(goAsync());
            } catch (IllegalStateException unused) {
                k.a().b(f2232a, "Cannot reschedule jobs. WorkManager needs to be initialized via a ContentProvider#onCreate() or an Application#onCreate().", new Throwable[0]);
            }
        } else {
            context.startService(b.b(context));
        }
    }
}
