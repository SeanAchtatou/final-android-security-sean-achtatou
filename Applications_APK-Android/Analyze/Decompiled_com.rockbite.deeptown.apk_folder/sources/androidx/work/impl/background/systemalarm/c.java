package androidx.work.impl.background.systemalarm;

import android.content.Context;
import android.content.Intent;
import androidx.work.impl.background.systemalarm.e;
import androidx.work.impl.l.d;
import androidx.work.impl.m.p;
import androidx.work.k;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ConstraintsCommandHandler */
class c {

    /* renamed from: e  reason: collision with root package name */
    private static final String f2241e = k.a("ConstraintsCmdHandler");

    /* renamed from: a  reason: collision with root package name */
    private final Context f2242a;

    /* renamed from: b  reason: collision with root package name */
    private final int f2243b;

    /* renamed from: c  reason: collision with root package name */
    private final e f2244c;

    /* renamed from: d  reason: collision with root package name */
    private final d f2245d = new d(this.f2242a, this.f2244c.c(), null);

    c(Context context, int i2, e eVar) {
        this.f2242a = context;
        this.f2243b = i2;
        this.f2244c = eVar;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        List<p> a2 = this.f2244c.d().f().q().a();
        ConstraintProxy.a(this.f2242a, a2);
        this.f2245d.a(a2);
        ArrayList<p> arrayList = new ArrayList<>(a2.size());
        long currentTimeMillis = System.currentTimeMillis();
        for (p next : a2) {
            String str = next.f2453a;
            if (currentTimeMillis >= next.a() && (!next.b() || this.f2245d.a(str))) {
                arrayList.add(next);
            }
        }
        for (p pVar : arrayList) {
            String str2 = pVar.f2453a;
            Intent a3 = b.a(this.f2242a, str2);
            k.a().a(f2241e, String.format("Creating a delay_met command for workSpec with id (%s)", str2), new Throwable[0]);
            e eVar = this.f2244c;
            eVar.a(new e.b(eVar, a3, this.f2243b));
        }
        this.f2245d.a();
    }
}
