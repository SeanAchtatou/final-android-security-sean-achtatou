package androidx.work.impl.background.systemjob;

import android.app.job.JobInfo;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import androidx.work.c;
import androidx.work.d;
import androidx.work.impl.m.p;
import androidx.work.k;
import androidx.work.l;

/* compiled from: SystemJobInfoConverter */
class a {

    /* renamed from: b  reason: collision with root package name */
    private static final String f2277b = k.a("SystemJobInfoConverter");

    /* renamed from: a  reason: collision with root package name */
    private final ComponentName f2278a;

    /* renamed from: androidx.work.impl.background.systemjob.a$a  reason: collision with other inner class name */
    /* compiled from: SystemJobInfoConverter */
    static /* synthetic */ class C0029a {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2279a = new int[l.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                androidx.work.l[] r0 = androidx.work.l.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                androidx.work.impl.background.systemjob.a.C0029a.f2279a = r0
                int[] r0 = androidx.work.impl.background.systemjob.a.C0029a.f2279a     // Catch:{ NoSuchFieldError -> 0x0014 }
                androidx.work.l r1 = androidx.work.l.NOT_REQUIRED     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = androidx.work.impl.background.systemjob.a.C0029a.f2279a     // Catch:{ NoSuchFieldError -> 0x001f }
                androidx.work.l r1 = androidx.work.l.CONNECTED     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = androidx.work.impl.background.systemjob.a.C0029a.f2279a     // Catch:{ NoSuchFieldError -> 0x002a }
                androidx.work.l r1 = androidx.work.l.UNMETERED     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = androidx.work.impl.background.systemjob.a.C0029a.f2279a     // Catch:{ NoSuchFieldError -> 0x0035 }
                androidx.work.l r1 = androidx.work.l.NOT_ROAMING     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = androidx.work.impl.background.systemjob.a.C0029a.f2279a     // Catch:{ NoSuchFieldError -> 0x0040 }
                androidx.work.l r1 = androidx.work.l.METERED     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.work.impl.background.systemjob.a.C0029a.<clinit>():void");
        }
    }

    a(Context context) {
        this.f2278a = new ComponentName(context.getApplicationContext(), SystemJobService.class);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: package-private */
    public JobInfo a(p pVar, int i2) {
        c cVar = pVar.f2462j;
        int a2 = a(cVar.b());
        PersistableBundle persistableBundle = new PersistableBundle();
        persistableBundle.putString("EXTRA_WORK_SPEC_ID", pVar.f2453a);
        persistableBundle.putBoolean("EXTRA_IS_PERIODIC", pVar.d());
        JobInfo.Builder extras = new JobInfo.Builder(i2, this.f2278a).setRequiredNetworkType(a2).setRequiresCharging(cVar.g()).setRequiresDeviceIdle(cVar.h()).setExtras(persistableBundle);
        if (!cVar.h()) {
            extras.setBackoffCriteria(pVar.m, pVar.l == androidx.work.a.LINEAR ? 0 : 1);
        }
        long max = Math.max(pVar.a() - System.currentTimeMillis(), 0L);
        if (Build.VERSION.SDK_INT <= 28) {
            extras.setMinimumLatency(max);
        } else if (max > 0) {
            extras.setMinimumLatency(max);
        } else {
            extras.setImportantWhileForeground(true);
        }
        if (Build.VERSION.SDK_INT >= 24 && cVar.e()) {
            for (d.a a3 : cVar.a().a()) {
                extras.addTriggerContentUri(a(a3));
            }
            extras.setTriggerContentUpdateDelay(cVar.c());
            extras.setTriggerContentMaxDelay(cVar.d());
        }
        extras.setPersisted(false);
        if (Build.VERSION.SDK_INT >= 26) {
            extras.setRequiresBatteryNotLow(cVar.f());
            extras.setRequiresStorageNotLow(cVar.i());
        }
        return extras.build();
    }

    private static JobInfo.TriggerContentUri a(d.a aVar) {
        return new JobInfo.TriggerContentUri(aVar.a(), aVar.b() ? 1 : 0);
    }

    static int a(l lVar) {
        int i2 = C0029a.f2279a[lVar.ordinal()];
        if (i2 == 1) {
            return 0;
        }
        if (i2 == 2) {
            return 1;
        }
        if (i2 == 3) {
            return 2;
        }
        if (i2 != 4) {
            if (i2 == 5 && Build.VERSION.SDK_INT >= 26) {
                return 4;
            }
        } else if (Build.VERSION.SDK_INT >= 24) {
            return 3;
        }
        k.a().a(f2277b, String.format("API version too low. Cannot convert network type value %s", lVar), new Throwable[0]);
        return 1;
    }
}
