package androidx.work.impl.background.systemjob;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.d;
import androidx.work.impl.i;
import androidx.work.impl.m.g;
import androidx.work.impl.m.p;
import androidx.work.impl.utils.c;
import androidx.work.k;
import androidx.work.r;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* compiled from: SystemJobScheduler */
public class b implements d {

    /* renamed from: e  reason: collision with root package name */
    private static final String f2280e = k.a("SystemJobScheduler");

    /* renamed from: a  reason: collision with root package name */
    private final Context f2281a;

    /* renamed from: b  reason: collision with root package name */
    private final JobScheduler f2282b;

    /* renamed from: c  reason: collision with root package name */
    private final i f2283c;

    /* renamed from: d  reason: collision with root package name */
    private final a f2284d;

    public b(Context context, i iVar) {
        this(context, iVar, (JobScheduler) context.getSystemService("jobscheduler"), new a(context));
    }

    public static void b(Context context) {
        List<JobInfo> a2;
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        if (jobScheduler != null && (a2 = a(context, jobScheduler)) != null && !a2.isEmpty()) {
            for (JobInfo next : a2) {
                if (a(next) == null) {
                    a(jobScheduler, next.getId());
                }
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void a(p... pVarArr) {
        int i2;
        List<Integer> a2;
        int i3;
        WorkDatabase f2 = this.f2283c.f();
        c cVar = new c(f2);
        int length = pVarArr.length;
        int i4 = 0;
        while (i4 < length) {
            p pVar = pVarArr[i4];
            f2.c();
            try {
                p e2 = f2.q().e(pVar.f2453a);
                if (e2 == null) {
                    k a3 = k.a();
                    String str = f2280e;
                    a3.e(str, "Skipping scheduling " + pVar.f2453a + " because it's no longer in the DB", new Throwable[0]);
                    f2.k();
                } else if (e2.f2454b != r.ENQUEUED) {
                    k a4 = k.a();
                    String str2 = f2280e;
                    a4.e(str2, "Skipping scheduling " + pVar.f2453a + " because it is no longer enqueued", new Throwable[0]);
                    f2.k();
                } else {
                    g a5 = f2.n().a(pVar.f2453a);
                    if (a5 != null) {
                        i2 = a5.f2440b;
                    } else {
                        i2 = cVar.a(this.f2283c.b().e(), this.f2283c.b().c());
                    }
                    if (a5 == null) {
                        this.f2283c.f().n().a(new g(pVar.f2453a, i2));
                    }
                    a(pVar, i2);
                    if (Build.VERSION.SDK_INT == 23 && (a2 = a(this.f2281a, this.f2282b, pVar.f2453a)) != null) {
                        int indexOf = a2.indexOf(Integer.valueOf(i2));
                        if (indexOf >= 0) {
                            a2.remove(indexOf);
                        }
                        if (!a2.isEmpty()) {
                            i3 = a2.get(0).intValue();
                        } else {
                            i3 = cVar.a(this.f2283c.b().e(), this.f2283c.b().c());
                        }
                        a(pVar, i3);
                    }
                    f2.k();
                }
                f2.e();
                i4++;
            } catch (Throwable th) {
                f2.e();
                throw th;
            }
        }
    }

    public b(Context context, i iVar, JobScheduler jobScheduler, a aVar) {
        this.f2281a = context;
        this.f2283c = iVar;
        this.f2282b = jobScheduler;
        this.f2284d = aVar;
    }

    public void a(p pVar, int i2) {
        JobInfo a2 = this.f2284d.a(pVar, i2);
        k.a().a(f2280e, String.format("Scheduling work ID %s Job ID %s", pVar.f2453a, Integer.valueOf(i2)), new Throwable[0]);
        try {
            this.f2282b.schedule(a2);
        } catch (IllegalStateException e2) {
            List<JobInfo> a3 = a(this.f2281a, this.f2282b);
            String format = String.format(Locale.getDefault(), "JobScheduler 100 job limit exceeded.  We count %d WorkManager jobs in JobScheduler; we have %d tracked jobs in our DB; our Configuration limit is %d.", Integer.valueOf(a3 != null ? a3.size() : 0), Integer.valueOf(this.f2283c.f().q().a().size()), Integer.valueOf(this.f2283c.b().d()));
            k.a().b(f2280e, format, new Throwable[0]);
            throw new IllegalStateException(format, e2);
        } catch (Throwable th) {
            k.a().b(f2280e, String.format("Unable to schedule %s", pVar), th);
        }
    }

    public void a(String str) {
        List<Integer> a2 = a(this.f2281a, this.f2282b, str);
        if (a2 != null && !a2.isEmpty()) {
            for (Integer intValue : a2) {
                a(this.f2282b, intValue.intValue());
            }
            this.f2283c.f().n().b(str);
        }
    }

    private static void a(JobScheduler jobScheduler, int i2) {
        try {
            jobScheduler.cancel(i2);
        } catch (Throwable th) {
            k.a().b(f2280e, String.format(Locale.getDefault(), "Exception while trying to cancel job (%d)", Integer.valueOf(i2)), th);
        }
    }

    public static void a(Context context) {
        List<JobInfo> a2;
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        if (jobScheduler != null && (a2 = a(context, jobScheduler)) != null && !a2.isEmpty()) {
            for (JobInfo id : a2) {
                a(jobScheduler, id.getId());
            }
        }
    }

    private static List<JobInfo> a(Context context, JobScheduler jobScheduler) {
        List<JobInfo> list;
        try {
            list = jobScheduler.getAllPendingJobs();
        } catch (Throwable th) {
            k.a().b(f2280e, "getAllPendingJobs() is not reliable on this device.", th);
            list = null;
        }
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        ComponentName componentName = new ComponentName(context, SystemJobService.class);
        for (JobInfo jobInfo : list) {
            if (componentName.equals(jobInfo.getService())) {
                arrayList.add(jobInfo);
            }
        }
        return arrayList;
    }

    private static List<Integer> a(Context context, JobScheduler jobScheduler, String str) {
        List<JobInfo> a2 = a(context, jobScheduler);
        if (a2 == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(2);
        for (JobInfo next : a2) {
            if (str.equals(a(next))) {
                arrayList.add(Integer.valueOf(next.getId()));
            }
        }
        return arrayList;
    }

    private static String a(JobInfo jobInfo) {
        PersistableBundle extras = jobInfo.getExtras();
        if (extras == null) {
            return null;
        }
        try {
            if (extras.containsKey("EXTRA_WORK_SPEC_ID")) {
                return extras.getString("EXTRA_WORK_SPEC_ID");
            }
            return null;
        } catch (NullPointerException unused) {
            return null;
        }
    }
}
