package androidx.work.impl.background.systemalarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.i;
import androidx.work.impl.m.g;
import androidx.work.impl.m.h;
import androidx.work.impl.utils.c;
import androidx.work.k;
import com.google.android.gms.drive.DriveFile;

/* compiled from: Alarms */
class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2236a = k.a("Alarms");

    public static void a(Context context, i iVar, String str, long j2) {
        WorkDatabase f2 = iVar.f();
        h n = f2.n();
        g a2 = n.a(str);
        if (a2 != null) {
            a(context, str, a2.f2440b);
            a(context, str, a2.f2440b, j2);
            return;
        }
        int a3 = new c(f2).a();
        n.a(new g(str, a3));
        a(context, str, a3, j2);
    }

    public static void a(Context context, i iVar, String str) {
        h n = iVar.f().n();
        g a2 = n.a(str);
        if (a2 != null) {
            a(context, str, a2.f2440b);
            k.a().a(f2236a, String.format("Removing SystemIdInfo for workSpecId (%s)", str), new Throwable[0]);
            n.b(str);
        }
    }

    private static void a(Context context, String str, int i2) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        PendingIntent service = PendingIntent.getService(context, i2, b.a(context, str), DriveFile.MODE_WRITE_ONLY);
        if (service != null && alarmManager != null) {
            k.a().a(f2236a, String.format("Cancelling existing alarm with (workSpecId, systemId) (%s, %s)", str, Integer.valueOf(i2)), new Throwable[0]);
            alarmManager.cancel(service);
        }
    }

    private static void a(Context context, String str, int i2, long j2) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        PendingIntent service = PendingIntent.getService(context, i2, b.a(context, str), 134217728);
        if (alarmManager == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(0, j2, service);
        } else {
            alarmManager.set(0, j2, service);
        }
    }
}
