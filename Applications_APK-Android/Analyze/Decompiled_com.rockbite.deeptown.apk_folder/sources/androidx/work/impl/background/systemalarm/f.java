package androidx.work.impl.background.systemalarm;

import android.content.Context;
import androidx.work.impl.d;
import androidx.work.impl.m.p;
import androidx.work.k;

/* compiled from: SystemAlarmScheduler */
public class f implements d {

    /* renamed from: b  reason: collision with root package name */
    private static final String f2272b = k.a("SystemAlarmScheduler");

    /* renamed from: a  reason: collision with root package name */
    private final Context f2273a;

    public f(Context context) {
        this.f2273a = context.getApplicationContext();
    }

    public void a(p... pVarArr) {
        for (p a2 : pVarArr) {
            a(a2);
        }
    }

    public void a(String str) {
        this.f2273a.startService(b.c(this.f2273a, str));
    }

    private void a(p pVar) {
        k.a().a(f2272b, String.format("Scheduling work with workSpecId %s", pVar.f2453a), new Throwable[0]);
        this.f2273a.startService(b.b(this.f2273a, pVar.f2453a));
    }
}
