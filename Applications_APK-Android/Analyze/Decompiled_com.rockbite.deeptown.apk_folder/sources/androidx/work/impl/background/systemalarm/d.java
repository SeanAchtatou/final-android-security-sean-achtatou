package androidx.work.impl.background.systemalarm;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import androidx.work.impl.a;
import androidx.work.impl.background.systemalarm.e;
import androidx.work.impl.l.c;
import androidx.work.impl.m.p;
import androidx.work.impl.utils.i;
import androidx.work.impl.utils.l;
import androidx.work.k;
import java.util.Collections;
import java.util.List;

/* compiled from: DelayMetCommandHandler */
public class d implements c, a, l.b {

    /* renamed from: j  reason: collision with root package name */
    private static final String f2246j = k.a("DelayMetCommandHandler");

    /* renamed from: a  reason: collision with root package name */
    private final Context f2247a;

    /* renamed from: b  reason: collision with root package name */
    private final int f2248b;

    /* renamed from: c  reason: collision with root package name */
    private final String f2249c;

    /* renamed from: d  reason: collision with root package name */
    private final e f2250d;

    /* renamed from: e  reason: collision with root package name */
    private final androidx.work.impl.l.d f2251e;

    /* renamed from: f  reason: collision with root package name */
    private final Object f2252f = new Object();

    /* renamed from: g  reason: collision with root package name */
    private int f2253g = 0;

    /* renamed from: h  reason: collision with root package name */
    private PowerManager.WakeLock f2254h;

    /* renamed from: i  reason: collision with root package name */
    private boolean f2255i = false;

    d(Context context, int i2, String str, e eVar) {
        this.f2247a = context;
        this.f2248b = i2;
        this.f2250d = eVar;
        this.f2249c = str;
        this.f2251e = new androidx.work.impl.l.d(this.f2247a, eVar.c(), this);
    }

    private void c() {
        synchronized (this.f2252f) {
            if (this.f2253g < 2) {
                this.f2253g = 2;
                k.a().a(f2246j, String.format("Stopping work for WorkSpec %s", this.f2249c), new Throwable[0]);
                this.f2250d.a(new e.b(this.f2250d, b.c(this.f2247a, this.f2249c), this.f2248b));
                if (this.f2250d.b().c(this.f2249c)) {
                    k.a().a(f2246j, String.format("WorkSpec %s needs to be rescheduled", this.f2249c), new Throwable[0]);
                    this.f2250d.a(new e.b(this.f2250d, b.b(this.f2247a, this.f2249c), this.f2248b));
                } else {
                    k.a().a(f2246j, String.format("Processor does not have WorkSpec %s. No need to reschedule ", this.f2249c), new Throwable[0]);
                }
            } else {
                k.a().a(f2246j, String.format("Already stopped work for %s", this.f2249c), new Throwable[0]);
            }
        }
    }

    public void a(String str, boolean z) {
        k.a().a(f2246j, String.format("onExecuted %s, %s", str, Boolean.valueOf(z)), new Throwable[0]);
        b();
        if (z) {
            Intent b2 = b.b(this.f2247a, this.f2249c);
            e eVar = this.f2250d;
            eVar.a(new e.b(eVar, b2, this.f2248b));
        }
        if (this.f2255i) {
            Intent a2 = b.a(this.f2247a);
            e eVar2 = this.f2250d;
            eVar2.a(new e.b(eVar2, a2, this.f2248b));
        }
    }

    public void b(List<String> list) {
        if (list.contains(this.f2249c)) {
            synchronized (this.f2252f) {
                if (this.f2253g == 0) {
                    this.f2253g = 1;
                    k.a().a(f2246j, String.format("onAllConstraintsMet for %s", this.f2249c), new Throwable[0]);
                    if (this.f2250d.b().d(this.f2249c)) {
                        this.f2250d.e().a(this.f2249c, 600000, this);
                    } else {
                        b();
                    }
                } else {
                    k.a().a(f2246j, String.format("Already started work for %s", this.f2249c), new Throwable[0]);
                }
            }
        }
    }

    public void a(String str) {
        k.a().a(f2246j, String.format("Exceeded time limits on execution for %s", str), new Throwable[0]);
        c();
    }

    private void b() {
        synchronized (this.f2252f) {
            this.f2251e.a();
            this.f2250d.e().a(this.f2249c);
            if (this.f2254h != null && this.f2254h.isHeld()) {
                k.a().a(f2246j, String.format("Releasing wakelock %s for WorkSpec %s", this.f2254h, this.f2249c), new Throwable[0]);
                this.f2254h.release();
            }
        }
    }

    public void a(List<String> list) {
        c();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f2254h = i.a(this.f2247a, String.format("%s (%s)", this.f2249c, Integer.valueOf(this.f2248b)));
        k.a().a(f2246j, String.format("Acquiring wakelock %s for WorkSpec %s", this.f2254h, this.f2249c), new Throwable[0]);
        this.f2254h.acquire();
        p e2 = this.f2250d.d().f().q().e(this.f2249c);
        if (e2 == null) {
            c();
            return;
        }
        this.f2255i = e2.b();
        if (!this.f2255i) {
            k.a().a(f2246j, String.format("No constraints for %s", this.f2249c), new Throwable[0]);
            b(Collections.singletonList(this.f2249c));
            return;
        }
        this.f2251e.a((Iterable<p>) Collections.singletonList(e2));
    }
}
