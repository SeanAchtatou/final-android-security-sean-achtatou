package androidx.work.impl.background.systemalarm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.a;
import androidx.work.impl.background.systemalarm.e;
import androidx.work.impl.m.p;
import androidx.work.k;
import java.util.HashMap;
import java.util.Map;

/* compiled from: CommandHandler */
public class b implements a {

    /* renamed from: d  reason: collision with root package name */
    private static final String f2237d = k.a("CommandHandler");

    /* renamed from: a  reason: collision with root package name */
    private final Context f2238a;

    /* renamed from: b  reason: collision with root package name */
    private final Map<String, a> f2239b = new HashMap();

    /* renamed from: c  reason: collision with root package name */
    private final Object f2240c = new Object();

    b(Context context) {
        this.f2238a = context;
    }

    static Intent a(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_DELAY_MET");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    static Intent b(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_SCHEDULE_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    static Intent c(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_STOP_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    private void d(Intent intent, int i2, e eVar) {
        k.a().a(f2237d, String.format("Handling reschedule %s, %s", intent, Integer.valueOf(i2)), new Throwable[0]);
        eVar.d().i();
    }

    private void e(Intent intent, int i2, e eVar) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        k.a().a(f2237d, String.format("Handling schedule work for %s", string), new Throwable[0]);
        WorkDatabase f2 = eVar.d().f();
        f2.c();
        try {
            p e2 = f2.q().e(string);
            if (e2 == null) {
                k a2 = k.a();
                String str = f2237d;
                a2.e(str, "Skipping scheduling " + string + " because it's no longer in the DB", new Throwable[0]);
            } else if (e2.f2454b.a()) {
                k a3 = k.a();
                String str2 = f2237d;
                a3.e(str2, "Skipping scheduling " + string + "because it is finished.", new Throwable[0]);
                f2.e();
            } else {
                long a4 = e2.a();
                if (!e2.b()) {
                    k.a().a(f2237d, String.format("Setting up Alarms for %s at %s", string, Long.valueOf(a4)), new Throwable[0]);
                    a.a(this.f2238a, eVar.d(), string, a4);
                } else {
                    k.a().a(f2237d, String.format("Opportunistically setting an alarm for %s at %s", string, Long.valueOf(a4)), new Throwable[0]);
                    a.a(this.f2238a, eVar.d(), string, a4);
                    eVar.a(new e.b(eVar, a(this.f2238a), i2));
                }
                f2.k();
                f2.e();
            }
        } finally {
            f2.e();
        }
    }

    static Intent a(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_CONSTRAINTS_CHANGED");
        return intent;
    }

    static Intent b(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_RESCHEDULE");
        return intent;
    }

    private void c(Intent intent, int i2, e eVar) {
        Bundle extras = intent.getExtras();
        synchronized (this.f2240c) {
            String string = extras.getString("KEY_WORKSPEC_ID");
            k.a().a(f2237d, String.format("Handing delay met for %s", string), new Throwable[0]);
            if (!this.f2239b.containsKey(string)) {
                d dVar = new d(this.f2238a, i2, string, eVar);
                this.f2239b.put(string, dVar);
                dVar.a();
            } else {
                k.a().a(f2237d, String.format("WorkSpec %s is already being handled for ACTION_DELAY_MET", string), new Throwable[0]);
            }
        }
    }

    static Intent a(Context context, String str, boolean z) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_EXECUTION_COMPLETED");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        intent.putExtra("KEY_NEEDS_RESCHEDULE", z);
        return intent;
    }

    private void b(Intent intent, int i2, e eVar) {
        k.a().a(f2237d, String.format("Handling constraints changed %s", intent), new Throwable[0]);
        new c(this.f2238a, i2, eVar).a();
    }

    public void a(String str, boolean z) {
        synchronized (this.f2240c) {
            a remove = this.f2239b.remove(str);
            if (remove != null) {
                remove.a(str, z);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        boolean z;
        synchronized (this.f2240c) {
            z = !this.f2239b.isEmpty();
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void a(Intent intent, int i2, e eVar) {
        String action = intent.getAction();
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action)) {
            b(intent, i2, eVar);
        } else if ("ACTION_RESCHEDULE".equals(action)) {
            d(intent, i2, eVar);
        } else {
            if (!a(intent.getExtras(), "KEY_WORKSPEC_ID")) {
                k.a().b(f2237d, String.format("Invalid request for %s, requires %s.", action, "KEY_WORKSPEC_ID"), new Throwable[0]);
            } else if ("ACTION_SCHEDULE_WORK".equals(action)) {
                e(intent, i2, eVar);
            } else if ("ACTION_DELAY_MET".equals(action)) {
                c(intent, i2, eVar);
            } else if ("ACTION_STOP_WORK".equals(action)) {
                a(intent, eVar);
            } else if ("ACTION_EXECUTION_COMPLETED".equals(action)) {
                a(intent, i2);
            } else {
                k.a().e(f2237d, String.format("Ignoring intent %s", intent), new Throwable[0]);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.work.impl.background.systemalarm.e.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      androidx.work.impl.background.systemalarm.e.a(android.content.Intent, int):boolean
      androidx.work.impl.background.systemalarm.e.a(java.lang.String, boolean):void */
    private void a(Intent intent, e eVar) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        k.a().a(f2237d, String.format("Handing stopWork work for %s", string), new Throwable[0]);
        eVar.d().d(string);
        a.a(this.f2238a, eVar.d(), string);
        eVar.a(string, false);
    }

    private void a(Intent intent, int i2) {
        Bundle extras = intent.getExtras();
        String string = extras.getString("KEY_WORKSPEC_ID");
        boolean z = extras.getBoolean("KEY_NEEDS_RESCHEDULE");
        k.a().a(f2237d, String.format("Handling onExecutionCompleted %s, %s", intent, Integer.valueOf(i2)), new Throwable[0]);
        a(string, z);
    }

    private static boolean a(Bundle bundle, String... strArr) {
        if (bundle == null || bundle.isEmpty()) {
            return false;
        }
        for (String str : strArr) {
            if (bundle.get(str) == null) {
                return false;
            }
        }
        return true;
    }
}
