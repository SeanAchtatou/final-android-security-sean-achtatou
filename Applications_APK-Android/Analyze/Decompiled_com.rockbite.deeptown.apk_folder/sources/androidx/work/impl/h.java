package androidx.work.impl;

import android.content.Context;
import android.os.Build;
import androidx.work.k;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/* compiled from: WorkDatabasePathHelper */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2346a = k.a("WrkDbPathHelper");

    /* renamed from: b  reason: collision with root package name */
    private static final String[] f2347b = {"-journal", "-shm", "-wal"};

    public static File a(Context context) {
        if (Build.VERSION.SDK_INT < 23) {
            return b(context);
        }
        return a(context, "androidx.work.workdb");
    }

    public static String a() {
        return "androidx.work.workdb";
    }

    public static File b(Context context) {
        return context.getDatabasePath("androidx.work.workdb");
    }

    public static void c(Context context) {
        String str;
        File b2 = b(context);
        if (Build.VERSION.SDK_INT >= 23 && b2.exists()) {
            k.a().a(f2346a, "Migrating WorkDatabase to the no-backup directory", new Throwable[0]);
            Map<File, File> d2 = d(context);
            for (File next : d2.keySet()) {
                File file = d2.get(next);
                if (next.exists() && file != null) {
                    if (file.exists()) {
                        k.a().e(f2346a, String.format("Over-writing contents of %s", file), new Throwable[0]);
                    }
                    if (next.renameTo(file)) {
                        str = String.format("Migrated %s to %s", next, file);
                    } else {
                        str = String.format("Renaming %s to %s failed", next, file);
                    }
                    k.a().a(f2346a, str, new Throwable[0]);
                }
            }
        }
    }

    public static Map<File, File> d(Context context) {
        HashMap hashMap = new HashMap();
        if (Build.VERSION.SDK_INT >= 23) {
            File b2 = b(context);
            File a2 = a(context);
            hashMap.put(b2, a2);
            for (String str : f2347b) {
                hashMap.put(new File(b2.getPath() + str), new File(a2.getPath() + str));
            }
        }
        return hashMap;
    }

    private static File a(Context context, String str) {
        return new File(context.getNoBackupFilesDir(), str);
    }
}
