package androidx.work.impl;

import android.content.Context;
import android.os.PowerManager;
import androidx.work.WorkerParameters;
import androidx.work.b;
import androidx.work.impl.j;
import androidx.work.k;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/* compiled from: Processor */
public class c implements a, androidx.work.impl.foreground.a {
    private static final String l = k.a("Processor");

    /* renamed from: a  reason: collision with root package name */
    private PowerManager.WakeLock f2285a;

    /* renamed from: b  reason: collision with root package name */
    private Context f2286b;

    /* renamed from: c  reason: collision with root package name */
    private b f2287c;

    /* renamed from: d  reason: collision with root package name */
    private androidx.work.impl.utils.n.a f2288d;

    /* renamed from: e  reason: collision with root package name */
    private WorkDatabase f2289e;

    /* renamed from: f  reason: collision with root package name */
    private Map<String, j> f2290f = new HashMap();

    /* renamed from: g  reason: collision with root package name */
    private Map<String, j> f2291g = new HashMap();

    /* renamed from: h  reason: collision with root package name */
    private List<d> f2292h;

    /* renamed from: i  reason: collision with root package name */
    private Set<String> f2293i;

    /* renamed from: j  reason: collision with root package name */
    private final List<a> f2294j;

    /* renamed from: k  reason: collision with root package name */
    private final Object f2295k;

    /* compiled from: Processor */
    private static class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private a f2296a;

        /* renamed from: b  reason: collision with root package name */
        private String f2297b;

        /* renamed from: c  reason: collision with root package name */
        private ListenableFuture<Boolean> f2298c;

        a(a aVar, String str, ListenableFuture<Boolean> listenableFuture) {
            this.f2296a = aVar;
            this.f2297b = str;
            this.f2298c = listenableFuture;
        }

        public void run() {
            boolean z;
            try {
                z = this.f2298c.get().booleanValue();
            } catch (InterruptedException | ExecutionException unused) {
                z = true;
            }
            this.f2296a.a(this.f2297b, z);
        }
    }

    public c(Context context, b bVar, androidx.work.impl.utils.n.a aVar, WorkDatabase workDatabase, List<d> list) {
        this.f2286b = context;
        this.f2287c = bVar;
        this.f2288d = aVar;
        this.f2289e = workDatabase;
        this.f2292h = list;
        this.f2293i = new HashSet();
        this.f2294j = new ArrayList();
        this.f2285a = null;
        this.f2295k = new Object();
    }

    public boolean a(String str, WorkerParameters.a aVar) {
        synchronized (this.f2295k) {
            if (this.f2291g.containsKey(str)) {
                k.a().a(l, String.format("Work %s is already enqueued for processing", str), new Throwable[0]);
                return false;
            }
            j.c cVar = new j.c(this.f2286b, this.f2287c, this.f2288d, this, this.f2289e, str);
            cVar.a(this.f2292h);
            cVar.a(aVar);
            j a2 = cVar.a();
            ListenableFuture<Boolean> a3 = a2.a();
            a3.addListener(new a(this, str, a3), this.f2288d.a());
            this.f2291g.put(str, a2);
            this.f2288d.b().execute(a2);
            k.a().a(l, String.format("%s: processing %s", c.class.getSimpleName(), str), new Throwable[0]);
            return true;
        }
    }

    public boolean b(String str) {
        boolean contains;
        synchronized (this.f2295k) {
            contains = this.f2293i.contains(str);
        }
        return contains;
    }

    public boolean c(String str) {
        boolean z;
        synchronized (this.f2295k) {
            if (!this.f2291g.containsKey(str)) {
                if (!this.f2290f.containsKey(str)) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    public boolean d(String str) {
        return a(str, (WorkerParameters.a) null);
    }

    public boolean e(String str) {
        boolean a2;
        synchronized (this.f2295k) {
            boolean z = true;
            k.a().a(l, String.format("Processor cancelling %s", str), new Throwable[0]);
            this.f2293i.add(str);
            j remove = this.f2290f.remove(str);
            if (remove == null) {
                z = false;
            }
            if (remove == null) {
                remove = this.f2291g.remove(str);
            }
            a2 = a(str, remove);
            if (z) {
                a();
            }
        }
        return a2;
    }

    public boolean f(String str) {
        boolean a2;
        synchronized (this.f2295k) {
            k.a().a(l, String.format("Processor stopping foreground work %s", str), new Throwable[0]);
            a2 = a(str, this.f2290f.remove(str));
        }
        return a2;
    }

    public boolean g(String str) {
        boolean a2;
        synchronized (this.f2295k) {
            k.a().a(l, String.format("Processor stopping background work %s", str), new Throwable[0]);
            a2 = a(str, this.f2291g.remove(str));
        }
        return a2;
    }

    public void b(a aVar) {
        synchronized (this.f2295k) {
            this.f2294j.remove(aVar);
        }
    }

    public void a(String str) {
        synchronized (this.f2295k) {
            this.f2290f.remove(str);
            a();
        }
    }

    public void a(a aVar) {
        synchronized (this.f2295k) {
            this.f2294j.add(aVar);
        }
    }

    public void a(String str, boolean z) {
        synchronized (this.f2295k) {
            this.f2291g.remove(str);
            k.a().a(l, String.format("%s %s executed; reschedule = %s", getClass().getSimpleName(), str, Boolean.valueOf(z)), new Throwable[0]);
            for (a a2 : this.f2294j) {
                a2.a(str, z);
            }
        }
    }

    private void a() {
        synchronized (this.f2295k) {
            if (!(!this.f2290f.isEmpty())) {
                k.a().a(l, "No more foreground work. Stopping SystemForegroundService", new Throwable[0]);
                this.f2286b.startService(androidx.work.impl.foreground.b.a(this.f2286b));
                if (this.f2285a != null) {
                    this.f2285a.release();
                    this.f2285a = null;
                }
            }
        }
    }

    private static boolean a(String str, j jVar) {
        if (jVar != null) {
            jVar.b();
            k.a().a(l, String.format("WorkerWrapper interrupted for %s", str), new Throwable[0]);
            return true;
        }
        k.a().a(l, String.format("WorkerWrapper could not be found for %s", str), new Throwable[0]);
        return false;
    }
}
