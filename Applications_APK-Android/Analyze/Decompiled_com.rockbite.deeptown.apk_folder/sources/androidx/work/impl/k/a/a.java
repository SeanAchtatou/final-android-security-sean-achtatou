package androidx.work.impl.k.a;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.text.TextUtils;
import androidx.work.impl.d;
import androidx.work.impl.i;
import androidx.work.impl.l.c;
import androidx.work.impl.m.p;
import androidx.work.k;
import androidx.work.r;
import java.util.ArrayList;
import java.util.List;

/* compiled from: GreedyScheduler */
public class a implements d, c, androidx.work.impl.a {

    /* renamed from: h  reason: collision with root package name */
    private static final String f2384h = k.a("GreedyScheduler");

    /* renamed from: a  reason: collision with root package name */
    private final Context f2385a;

    /* renamed from: b  reason: collision with root package name */
    private final i f2386b;

    /* renamed from: c  reason: collision with root package name */
    private final androidx.work.impl.l.d f2387c;

    /* renamed from: d  reason: collision with root package name */
    private List<p> f2388d = new ArrayList();

    /* renamed from: e  reason: collision with root package name */
    private boolean f2389e;

    /* renamed from: f  reason: collision with root package name */
    private final Object f2390f;

    /* renamed from: g  reason: collision with root package name */
    private Boolean f2391g;

    public a(Context context, androidx.work.impl.utils.n.a aVar, i iVar) {
        this.f2385a = context;
        this.f2386b = iVar;
        this.f2387c = new androidx.work.impl.l.d(context, aVar, this);
        this.f2390f = new Object();
    }

    public void a(p... pVarArr) {
        if (this.f2391g == null) {
            this.f2391g = Boolean.valueOf(TextUtils.equals(this.f2385a.getPackageName(), a()));
        }
        if (!this.f2391g.booleanValue()) {
            k.a().c(f2384h, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        b();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (p pVar : pVarArr) {
            if (pVar.f2454b == r.ENQUEUED && !pVar.d() && pVar.f2459g == 0 && !pVar.c()) {
                if (!pVar.b()) {
                    k.a().a(f2384h, String.format("Starting work for %s", pVar.f2453a), new Throwable[0]);
                    this.f2386b.b(pVar.f2453a);
                } else if (Build.VERSION.SDK_INT >= 23 && pVar.f2462j.h()) {
                    k.a().a(f2384h, String.format("Ignoring WorkSpec %s, Requires device idle.", pVar), new Throwable[0]);
                } else if (Build.VERSION.SDK_INT < 24 || !pVar.f2462j.e()) {
                    arrayList.add(pVar);
                    arrayList2.add(pVar.f2453a);
                } else {
                    k.a().a(f2384h, String.format("Ignoring WorkSpec %s, Requires ContentUri triggers.", pVar), new Throwable[0]);
                }
            }
        }
        synchronized (this.f2390f) {
            if (!arrayList.isEmpty()) {
                k.a().a(f2384h, String.format("Starting tracking for [%s]", TextUtils.join(",", arrayList2)), new Throwable[0]);
                this.f2388d.addAll(arrayList);
                this.f2387c.a(this.f2388d);
            }
        }
    }

    public void b(List<String> list) {
        for (String next : list) {
            k.a().a(f2384h, String.format("Constraints met: Scheduling work ID %s", next), new Throwable[0]);
            this.f2386b.b(next);
        }
    }

    private void b(String str) {
        synchronized (this.f2390f) {
            int size = this.f2388d.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    break;
                } else if (this.f2388d.get(i2).f2453a.equals(str)) {
                    k.a().a(f2384h, String.format("Stopping tracking for %s", str), new Throwable[0]);
                    this.f2388d.remove(i2);
                    this.f2387c.a(this.f2388d);
                    break;
                } else {
                    i2++;
                }
            }
        }
    }

    private void b() {
        if (!this.f2389e) {
            this.f2386b.d().a(this);
            this.f2389e = true;
        }
    }

    public void a(String str) {
        if (this.f2391g == null) {
            this.f2391g = Boolean.valueOf(TextUtils.equals(this.f2385a.getPackageName(), a()));
        }
        if (!this.f2391g.booleanValue()) {
            k.a().c(f2384h, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        b();
        k.a().a(f2384h, String.format("Cancelling work ID %s", str), new Throwable[0]);
        this.f2386b.d(str);
    }

    public void a(List<String> list) {
        for (String next : list) {
            k.a().a(f2384h, String.format("Constraints not met: Cancelling work ID %s", next), new Throwable[0]);
            this.f2386b.d(next);
        }
    }

    public void a(String str, boolean z) {
        b(str);
    }

    private String a() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        int myPid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) this.f2385a.getSystemService("activity");
        if (activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null || runningAppProcesses.isEmpty()) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.pid == myPid) {
                return next.processName;
            }
        }
        return null;
    }
}
