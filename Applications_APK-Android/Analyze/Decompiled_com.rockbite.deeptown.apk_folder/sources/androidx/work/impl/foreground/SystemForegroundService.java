package androidx.work.impl.foreground;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.lifecycle.k;
import androidx.work.impl.foreground.b;

public class SystemForegroundService extends k implements b.C0030b {

    /* renamed from: f  reason: collision with root package name */
    private static final String f2310f = androidx.work.k.a("SystemFgService");

    /* renamed from: b  reason: collision with root package name */
    private b f2311b;

    /* renamed from: c  reason: collision with root package name */
    private Handler f2312c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f2313d;

    /* renamed from: e  reason: collision with root package name */
    NotificationManager f2314e;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ int f2315a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Notification f2316b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ int f2317c;

        a(int i2, Notification notification, int i3) {
            this.f2315a = i2;
            this.f2316b = notification;
            this.f2317c = i3;
        }

        public void run() {
            if (Build.VERSION.SDK_INT >= 29) {
                SystemForegroundService.this.startForeground(this.f2315a, this.f2316b, this.f2317c);
            } else {
                SystemForegroundService.this.startForeground(this.f2315a, this.f2316b);
            }
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ int f2319a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Notification f2320b;

        b(int i2, Notification notification) {
            this.f2319a = i2;
            this.f2320b = notification;
        }

        public void run() {
            SystemForegroundService.this.f2314e.notify(this.f2319a, this.f2320b);
        }
    }

    class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ int f2322a;

        c(int i2) {
            this.f2322a = i2;
        }

        public void run() {
            SystemForegroundService.this.f2314e.cancel(this.f2322a);
        }
    }

    private void b() {
        this.f2312c = new Handler(Looper.getMainLooper());
        this.f2314e = (NotificationManager) getApplicationContext().getSystemService("notification");
        this.f2311b = new b(getApplicationContext());
        this.f2311b.a((b.C0030b) this);
    }

    public void a(int i2, int i3, Notification notification) {
        this.f2312c.post(new a(i2, notification, i3));
    }

    public void onCreate() {
        super.onCreate();
        b();
    }

    public void onDestroy() {
        super.onDestroy();
        this.f2311b.a();
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        super.onStartCommand(intent, i2, i3);
        if (this.f2313d) {
            androidx.work.k.a().c(f2310f, "Re-initializing SystemForegroundService after a request to shut-down.", new Throwable[0]);
            this.f2311b.a();
            b();
            this.f2313d = false;
        }
        if (intent == null) {
            return 3;
        }
        this.f2311b.a(intent);
        return 3;
    }

    public void stop() {
        this.f2313d = true;
        androidx.work.k.a().a(f2310f, "All commands completed.", new Throwable[0]);
        if (Build.VERSION.SDK_INT >= 26) {
            stopForeground(true);
        }
        stopSelf();
    }

    public void a(int i2, Notification notification) {
        this.f2312c.post(new b(i2, notification));
    }

    public void a(int i2) {
        this.f2312c.post(new c(i2));
    }
}
