package androidx.work.impl.foreground;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import androidx.work.g;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.i;
import androidx.work.impl.l.c;
import androidx.work.impl.l.d;
import androidx.work.impl.m.p;
import androidx.work.k;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/* compiled from: SystemForegroundDispatcher */
public class b implements c, androidx.work.impl.a {
    static final String l = k.a("SystemFgDispatcher");

    /* renamed from: a  reason: collision with root package name */
    private Context f2324a;

    /* renamed from: b  reason: collision with root package name */
    private i f2325b = i.a(this.f2324a);

    /* renamed from: c  reason: collision with root package name */
    private final androidx.work.impl.utils.n.a f2326c = this.f2325b.g();

    /* renamed from: d  reason: collision with root package name */
    final Object f2327d = new Object();

    /* renamed from: e  reason: collision with root package name */
    String f2328e = null;

    /* renamed from: f  reason: collision with root package name */
    g f2329f = null;

    /* renamed from: g  reason: collision with root package name */
    final Map<String, g> f2330g = new LinkedHashMap();

    /* renamed from: h  reason: collision with root package name */
    final Map<String, p> f2331h = new HashMap();

    /* renamed from: i  reason: collision with root package name */
    final Set<p> f2332i = new HashSet();

    /* renamed from: j  reason: collision with root package name */
    final d f2333j = new d(this.f2324a, this.f2326c, this);

    /* renamed from: k  reason: collision with root package name */
    private C0030b f2334k;

    /* compiled from: SystemForegroundDispatcher */
    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WorkDatabase f2335a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f2336b;

        a(WorkDatabase workDatabase, String str) {
            this.f2335a = workDatabase;
            this.f2336b = str;
        }

        public void run() {
            p e2 = this.f2335a.q().e(this.f2336b);
            if (e2 != null && e2.b()) {
                synchronized (b.this.f2327d) {
                    b.this.f2331h.put(this.f2336b, e2);
                    b.this.f2332i.add(e2);
                }
                b bVar = b.this;
                bVar.f2333j.a(bVar.f2332i);
            }
        }
    }

    /* renamed from: androidx.work.impl.foreground.b$b  reason: collision with other inner class name */
    /* compiled from: SystemForegroundDispatcher */
    interface C0030b {
        void a(int i2);

        void a(int i2, int i3, Notification notification);

        void a(int i2, Notification notification);

        void stop();
    }

    b(Context context) {
        this.f2324a = context;
        this.f2325b.d().a(this);
    }

    private void b(Intent intent) {
        k.a().c(l, String.format("Stopping foreground work for %s", intent), new Throwable[0]);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        if (stringExtra != null && !TextUtils.isEmpty(stringExtra)) {
            this.f2325b.a(UUID.fromString(stringExtra));
        }
    }

    private void c(Intent intent) {
        int i2 = 0;
        int intExtra = intent.getIntExtra("KEY_NOTIFICATION_ID", 0);
        int intExtra2 = intent.getIntExtra("KEY_FOREGROUND_SERVICE_TYPE", 0);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        Notification notification = (Notification) intent.getParcelableExtra("KEY_NOTIFICATION");
        k.a().a(l, String.format("Notifying with (id: %s, workSpecId: %s, notificationType: %s)", Integer.valueOf(intExtra), stringExtra, Integer.valueOf(intExtra2)), new Throwable[0]);
        if (notification != null && this.f2334k != null) {
            this.f2330g.put(stringExtra, new g(intExtra, notification, intExtra2));
            if (TextUtils.isEmpty(this.f2328e)) {
                this.f2328e = stringExtra;
                this.f2334k.a(intExtra, intExtra2, notification);
                return;
            }
            this.f2334k.a(intExtra, notification);
            if (intExtra2 != 0 && Build.VERSION.SDK_INT >= 29) {
                for (Map.Entry<String, g> value : this.f2330g.entrySet()) {
                    i2 |= ((g) value.getValue()).a();
                }
                g gVar = this.f2330g.get(this.f2328e);
                if (gVar != null) {
                    this.f2334k.a(gVar.c(), i2, gVar.b());
                }
            }
        }
    }

    private void d(Intent intent) {
        k.a().c(l, String.format("Started foreground service %s", intent), new Throwable[0]);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        this.f2326c.a(new a(this.f2325b.f(), stringExtra));
    }

    private void e(Intent intent) {
        k.a().c(l, String.format("Stopping foreground service %s", intent), new Throwable[0]);
        C0030b bVar = this.f2334k;
        if (bVar != null) {
            g gVar = this.f2329f;
            if (gVar != null) {
                bVar.a(gVar.c());
                this.f2329f = null;
            }
            this.f2334k.stop();
        }
    }

    public void a(String str, boolean z) {
        boolean remove;
        Map.Entry entry;
        synchronized (this.f2327d) {
            p remove2 = this.f2331h.remove(str);
            remove = remove2 != null ? this.f2332i.remove(remove2) : false;
        }
        if (remove) {
            this.f2333j.a(this.f2332i);
        }
        this.f2329f = this.f2330g.remove(str);
        if (str.equals(this.f2328e) && this.f2330g.size() > 0) {
            Iterator<Map.Entry<String, g>> it = this.f2330g.entrySet().iterator();
            Map.Entry<String, g> next = it.next();
            while (true) {
                entry = next;
                if (!it.hasNext()) {
                    break;
                }
                next = it.next();
            }
            this.f2328e = (String) entry.getKey();
            if (this.f2334k != null) {
                g gVar = (g) entry.getValue();
                this.f2334k.a(gVar.c(), gVar.a(), gVar.b());
                this.f2334k.a(gVar.c());
                this.f2334k.a(this.f2329f.c());
            }
        }
    }

    public void b(List<String> list) {
    }

    /* access modifiers changed from: package-private */
    public void a(C0030b bVar) {
        if (this.f2334k != null) {
            k.a().b(l, "A callback already exists.", new Throwable[0]);
        } else {
            this.f2334k = bVar;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Intent intent) {
        String action = intent.getAction();
        if ("ACTION_START_FOREGROUND".equals(action)) {
            d(intent);
        } else if ("ACTION_STOP_FOREGROUND".equals(action)) {
            e(intent);
        } else if ("ACTION_NOTIFY".equals(action)) {
            c(intent);
        } else if ("ACTION_CANCEL_WORK".equals(action)) {
            b(intent);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f2334k = null;
        this.f2333j.a();
        this.f2325b.d().b(this);
    }

    public void a(List<String> list) {
        if (!list.isEmpty()) {
            for (String next : list) {
                k.a().a(l, String.format("Constraints unmet for WorkSpec %s", next), new Throwable[0]);
                this.f2325b.c(next);
            }
        }
    }

    public static Intent a(Context context) {
        Intent intent = new Intent(context, SystemForegroundService.class);
        intent.setAction("ACTION_STOP_FOREGROUND");
        return intent;
    }
}
