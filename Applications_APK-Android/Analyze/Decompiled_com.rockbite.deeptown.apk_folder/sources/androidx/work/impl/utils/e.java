package androidx.work.impl.utils;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.m.d;
import b.m.a.b;

/* compiled from: PreferenceUtils */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private final WorkDatabase f2500a;

    public e(WorkDatabase workDatabase) {
        this.f2500a = workDatabase;
    }

    public boolean a() {
        Long a2 = this.f2500a.m().a("reschedule_needed");
        return a2 != null && a2.longValue() == 1;
    }

    public void a(boolean z) {
        this.f2500a.m().a(new d("reschedule_needed", z));
    }

    public static void a(Context context, b bVar) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("androidx.work.util.preferences", 0);
        if (sharedPreferences.contains("reschedule_needed") || sharedPreferences.contains("last_cancel_all_time_ms")) {
            long j2 = 0;
            long j3 = sharedPreferences.getLong("last_cancel_all_time_ms", 0);
            if (sharedPreferences.getBoolean("reschedule_needed", false)) {
                j2 = 1;
            }
            bVar.J();
            try {
                bVar.a("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"last_cancel_all_time_ms", Long.valueOf(j3)});
                bVar.a("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"reschedule_needed", Long.valueOf(j2)});
                sharedPreferences.edit().clear().apply();
                bVar.L();
            } finally {
                bVar.M();
            }
        }
    }
}
