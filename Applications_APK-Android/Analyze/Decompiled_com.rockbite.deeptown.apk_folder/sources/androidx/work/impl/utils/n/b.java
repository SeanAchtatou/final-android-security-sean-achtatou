package androidx.work.impl.utils.n;

import android.os.Handler;
import android.os.Looper;
import androidx.work.impl.utils.f;
import java.util.concurrent.Executor;

/* compiled from: WorkManagerTaskExecutor */
public class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private final f f2554a;

    /* renamed from: b  reason: collision with root package name */
    private final Handler f2555b = new Handler(Looper.getMainLooper());

    /* renamed from: c  reason: collision with root package name */
    private final Executor f2556c = new a();

    /* compiled from: WorkManagerTaskExecutor */
    class a implements Executor {
        a() {
        }

        public void execute(Runnable runnable) {
            b.this.b(runnable);
        }
    }

    public b(Executor executor) {
        this.f2554a = new f(executor);
    }

    public Executor a() {
        return this.f2556c;
    }

    public void b(Runnable runnable) {
        this.f2555b.post(runnable);
    }

    public void a(Runnable runnable) {
        this.f2554a.execute(runnable);
    }

    public f b() {
        return this.f2554a;
    }
}
