package androidx.work.impl.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteAccessPermException;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemjob.b;
import androidx.work.impl.e;
import androidx.work.impl.h;
import androidx.work.impl.i;
import androidx.work.impl.m.n;
import androidx.work.impl.m.p;
import androidx.work.impl.m.q;
import androidx.work.k;
import androidx.work.r;
import com.google.android.gms.drive.DriveFile;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ForceStopRunnable implements Runnable {

    /* renamed from: c  reason: collision with root package name */
    private static final String f2482c = k.a("ForceStopRunnable");

    /* renamed from: d  reason: collision with root package name */
    private static final long f2483d = TimeUnit.DAYS.toMillis(3650);

    /* renamed from: a  reason: collision with root package name */
    private final Context f2484a;

    /* renamed from: b  reason: collision with root package name */
    private final i f2485b;

    public static class BroadcastReceiver extends android.content.BroadcastReceiver {

        /* renamed from: a  reason: collision with root package name */
        private static final String f2486a = k.a("ForceStopRunnable$Rcvr");

        public void onReceive(Context context, Intent intent) {
            if (intent != null && "ACTION_FORCE_STOP_RESCHEDULE".equals(intent.getAction())) {
                k.a().d(f2486a, "Rescheduling alarm that keeps track of force-stops.", new Throwable[0]);
                ForceStopRunnable.b(context);
            }
        }
    }

    public ForceStopRunnable(Context context, i iVar) {
        this.f2484a = context.getApplicationContext();
        this.f2485b = iVar;
    }

    public boolean a() {
        if (Build.VERSION.SDK_INT >= 23) {
            b.b(this.f2484a);
        }
        WorkDatabase f2 = this.f2485b.f();
        q q = f2.q();
        n p = f2.p();
        f2.c();
        try {
            List<p> b2 = q.b();
            boolean z = b2 != null && !b2.isEmpty();
            if (z) {
                for (p next : b2) {
                    q.a(r.ENQUEUED, next.f2453a);
                    q.a(next.f2453a, -1);
                }
            }
            p.a();
            f2.k();
            return z;
        } finally {
            f2.e();
        }
    }

    public boolean b() {
        if (a(this.f2484a, DriveFile.MODE_WRITE_ONLY) != null) {
            return false;
        }
        b(this.f2484a);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.f2485b.c().a();
    }

    public void run() {
        h.c(this.f2484a);
        k.a().a(f2482c, "Performing cleanup operations.", new Throwable[0]);
        try {
            boolean a2 = a();
            if (c()) {
                k.a().a(f2482c, "Rescheduling Workers.", new Throwable[0]);
                this.f2485b.i();
                this.f2485b.c().a(false);
            } else if (b()) {
                k.a().a(f2482c, "Application was force-stopped, rescheduling.", new Throwable[0]);
                this.f2485b.i();
            } else if (a2) {
                k.a().a(f2482c, "Found unfinished work, scheduling it.", new Throwable[0]);
                e.a(this.f2485b.b(), this.f2485b.f(), this.f2485b.e());
            }
            this.f2485b.h();
        } catch (SQLiteAccessPermException | SQLiteCantOpenDatabaseException | SQLiteDatabaseCorruptException e2) {
            k.a().b(f2482c, "The file system on the device is in a bad state. WorkManager cannot access the app's internal data store.", e2);
            throw new IllegalStateException("The file system on the device is in a bad state. WorkManager cannot access the app's internal data store.", e2);
        }
    }

    static void b(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        PendingIntent a2 = a(context, 134217728);
        long currentTimeMillis = System.currentTimeMillis() + f2483d;
        if (alarmManager == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(0, currentTimeMillis, a2);
        } else {
            alarmManager.set(0, currentTimeMillis, a2);
        }
    }

    private static PendingIntent a(Context context, int i2) {
        return PendingIntent.getBroadcast(context, -1, a(context), i2);
    }

    static Intent a(Context context) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(context, BroadcastReceiver.class));
        intent.setAction("ACTION_FORCE_STOP_RESCHEDULE");
        return intent;
    }
}
