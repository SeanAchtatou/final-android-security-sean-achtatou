package androidx.work.impl.utils;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;

/* compiled from: SerialExecutor */
public class f implements Executor {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayDeque<a> f2501a = new ArrayDeque<>();

    /* renamed from: b  reason: collision with root package name */
    private final Executor f2502b;

    /* renamed from: c  reason: collision with root package name */
    private final Object f2503c = new Object();

    /* renamed from: d  reason: collision with root package name */
    private volatile Runnable f2504d;

    /* compiled from: SerialExecutor */
    static class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final f f2505a;

        /* renamed from: b  reason: collision with root package name */
        final Runnable f2506b;

        a(f fVar, Runnable runnable) {
            this.f2505a = fVar;
            this.f2506b = runnable;
        }

        public void run() {
            try {
                this.f2506b.run();
            } finally {
                this.f2505a.b();
            }
        }
    }

    public f(Executor executor) {
        this.f2502b = executor;
    }

    public boolean a() {
        boolean z;
        synchronized (this.f2503c) {
            z = !this.f2501a.isEmpty();
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        synchronized (this.f2503c) {
            Runnable poll = this.f2501a.poll();
            this.f2504d = poll;
            if (poll != null) {
                this.f2502b.execute(this.f2504d);
            }
        }
    }

    public void execute(Runnable runnable) {
        synchronized (this.f2503c) {
            this.f2501a.add(new a(this, runnable));
            if (this.f2504d == null) {
                b();
            }
        }
    }
}
