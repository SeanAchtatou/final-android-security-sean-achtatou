package androidx.work.impl.utils;

import android.text.TextUtils;
import androidx.work.c;
import androidx.work.e;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import androidx.work.impl.d;
import androidx.work.impl.e;
import androidx.work.impl.f;
import androidx.work.impl.i;
import androidx.work.impl.m.p;
import androidx.work.impl.workers.ConstraintTrackingWorker;
import androidx.work.k;
import androidx.work.n;
import java.util.List;

/* compiled from: EnqueueRunnable */
public class b implements Runnable {

    /* renamed from: c  reason: collision with root package name */
    private static final String f2495c = k.a("EnqueueRunnable");

    /* renamed from: a  reason: collision with root package name */
    private final f f2496a;

    /* renamed from: b  reason: collision with root package name */
    private final androidx.work.impl.b f2497b = new androidx.work.impl.b();

    public b(f fVar) {
        this.f2496a = fVar;
    }

    public boolean a() {
        WorkDatabase f2 = this.f2496a.g().f();
        f2.c();
        try {
            boolean b2 = b(this.f2496a);
            f2.k();
            return b2;
        } finally {
            f2.e();
        }
    }

    public n b() {
        return this.f2497b;
    }

    public void c() {
        i g2 = this.f2496a.g();
        e.a(g2.b(), g2.f(), g2.e());
    }

    public void run() {
        try {
            if (!this.f2496a.h()) {
                if (a()) {
                    d.a(this.f2496a.g().a(), RescheduleReceiver.class, true);
                    c();
                }
                this.f2497b.a(n.f2576a);
                return;
            }
            throw new IllegalStateException(String.format("WorkContinuation has cycles (%s)", this.f2496a));
        } catch (Throwable th) {
            this.f2497b.a(new n.b.a(th));
        }
    }

    private static boolean b(f fVar) {
        List<f> e2 = fVar.e();
        boolean z = false;
        if (e2 != null) {
            boolean z2 = false;
            for (f next : e2) {
                if (!next.i()) {
                    z2 |= b(next);
                } else {
                    k.a().e(f2495c, String.format("Already enqueued work ids (%s).", TextUtils.join(", ", next.c())), new Throwable[0]);
                }
            }
            z = z2;
        }
        return a(fVar) | z;
    }

    private static boolean a(f fVar) {
        boolean a2 = a(fVar.g(), fVar.f(), (String[]) f.a(fVar).toArray(new String[0]), fVar.d(), fVar.b());
        fVar.j();
        return a2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x0176  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0181  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x01a8 A[LOOP:6: B:107:0x01a2->B:109:0x01a8, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x01c1  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x01d1 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x011f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(androidx.work.impl.i r19, java.util.List<? extends androidx.work.t> r20, java.lang.String[] r21, java.lang.String r22, androidx.work.f r23) {
        /*
            r0 = r19
            r1 = r21
            r2 = r22
            r3 = r23
            long r4 = java.lang.System.currentTimeMillis()
            androidx.work.impl.WorkDatabase r6 = r19.f()
            r8 = 1
            if (r1 == 0) goto L_0x0018
            int r9 = r1.length
            if (r9 <= 0) goto L_0x0018
            r9 = 1
            goto L_0x0019
        L_0x0018:
            r9 = 0
        L_0x0019:
            if (r9 == 0) goto L_0x005d
            int r10 = r1.length
            r11 = 0
            r12 = 1
            r13 = 0
            r14 = 0
        L_0x0020:
            if (r11 >= r10) goto L_0x0060
            r15 = r1[r11]
            androidx.work.impl.m.q r7 = r6.q()
            androidx.work.impl.m.p r7 = r7.e(r15)
            if (r7 != 0) goto L_0x0045
            androidx.work.k r0 = androidx.work.k.a()
            java.lang.String r1 = androidx.work.impl.utils.b.f2495c
            java.lang.Object[] r2 = new java.lang.Object[r8]
            r3 = 0
            r2[r3] = r15
            java.lang.String r4 = "Prerequisite %s doesn't exist; not enqueuing"
            java.lang.String r2 = java.lang.String.format(r4, r2)
            java.lang.Throwable[] r4 = new java.lang.Throwable[r3]
            r0.b(r1, r2, r4)
            return r3
        L_0x0045:
            androidx.work.r r7 = r7.f2454b
            androidx.work.r r15 = androidx.work.r.SUCCEEDED
            if (r7 != r15) goto L_0x004d
            r15 = 1
            goto L_0x004e
        L_0x004d:
            r15 = 0
        L_0x004e:
            r12 = r12 & r15
            androidx.work.r r15 = androidx.work.r.FAILED
            if (r7 != r15) goto L_0x0055
            r13 = 1
            goto L_0x005a
        L_0x0055:
            androidx.work.r r15 = androidx.work.r.CANCELLED
            if (r7 != r15) goto L_0x005a
            r14 = 1
        L_0x005a:
            int r11 = r11 + 1
            goto L_0x0020
        L_0x005d:
            r12 = 1
            r13 = 0
            r14 = 0
        L_0x0060:
            boolean r7 = android.text.TextUtils.isEmpty(r22)
            r7 = r7 ^ r8
            if (r7 == 0) goto L_0x006b
            if (r9 != 0) goto L_0x006b
            r10 = 1
            goto L_0x006c
        L_0x006b:
            r10 = 0
        L_0x006c:
            if (r10 == 0) goto L_0x00cc
            androidx.work.impl.m.q r10 = r6.q()
            java.util.List r10 = r10.b(r2)
            boolean r11 = r10.isEmpty()
            if (r11 != 0) goto L_0x00cc
            androidx.work.f r11 = androidx.work.f.APPEND
            if (r3 != r11) goto L_0x00ce
            androidx.work.impl.m.b r3 = r6.l()
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            java.util.Iterator r10 = r10.iterator()
        L_0x008d:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x00c0
            java.lang.Object r11 = r10.next()
            androidx.work.impl.m.p$b r11 = (androidx.work.impl.m.p.b) r11
            java.lang.String r15 = r11.f2464a
            boolean r15 = r3.c(r15)
            if (r15 != 0) goto L_0x00be
            androidx.work.r r15 = r11.f2465b
            androidx.work.r r8 = androidx.work.r.SUCCEEDED
            if (r15 != r8) goto L_0x00a9
            r8 = 1
            goto L_0x00aa
        L_0x00a9:
            r8 = 0
        L_0x00aa:
            r8 = r8 & r12
            androidx.work.r r12 = r11.f2465b
            androidx.work.r r15 = androidx.work.r.FAILED
            if (r12 != r15) goto L_0x00b3
            r13 = 1
            goto L_0x00b8
        L_0x00b3:
            androidx.work.r r15 = androidx.work.r.CANCELLED
            if (r12 != r15) goto L_0x00b8
            r14 = 1
        L_0x00b8:
            java.lang.String r11 = r11.f2464a
            r9.add(r11)
            r12 = r8
        L_0x00be:
            r8 = 1
            goto L_0x008d
        L_0x00c0:
            java.lang.Object[] r1 = r9.toArray(r1)
            java.lang.String[] r1 = (java.lang.String[]) r1
            int r3 = r1.length
            if (r3 <= 0) goto L_0x00cb
            r9 = 1
            goto L_0x00cc
        L_0x00cb:
            r9 = 0
        L_0x00cc:
            r3 = 0
            goto L_0x0113
        L_0x00ce:
            androidx.work.f r8 = androidx.work.f.KEEP
            if (r3 != r8) goto L_0x00ee
            java.util.Iterator r3 = r10.iterator()
        L_0x00d6:
            boolean r8 = r3.hasNext()
            if (r8 == 0) goto L_0x00ee
            java.lang.Object r8 = r3.next()
            androidx.work.impl.m.p$b r8 = (androidx.work.impl.m.p.b) r8
            androidx.work.r r8 = r8.f2465b
            androidx.work.r r11 = androidx.work.r.ENQUEUED
            if (r8 == r11) goto L_0x00ec
            androidx.work.r r11 = androidx.work.r.RUNNING
            if (r8 != r11) goto L_0x00d6
        L_0x00ec:
            r3 = 0
            return r3
        L_0x00ee:
            r3 = 0
            androidx.work.impl.utils.a r8 = androidx.work.impl.utils.a.a(r2, r0, r3)
            r8.run()
            androidx.work.impl.m.q r8 = r6.q()
            java.util.Iterator r10 = r10.iterator()
        L_0x00fe:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x0110
            java.lang.Object r11 = r10.next()
            androidx.work.impl.m.p$b r11 = (androidx.work.impl.m.p.b) r11
            java.lang.String r11 = r11.f2464a
            r8.a(r11)
            goto L_0x00fe
        L_0x0110:
            r16 = 1
            goto L_0x0115
        L_0x0113:
            r16 = 0
        L_0x0115:
            java.util.Iterator r8 = r20.iterator()
        L_0x0119:
            boolean r10 = r8.hasNext()
            if (r10 == 0) goto L_0x01d6
            java.lang.Object r10 = r8.next()
            androidx.work.t r10 = (androidx.work.t) r10
            androidx.work.impl.m.p r11 = r10.c()
            if (r9 == 0) goto L_0x0140
            if (r12 != 0) goto L_0x0140
            if (r13 == 0) goto L_0x0134
            androidx.work.r r15 = androidx.work.r.FAILED
            r11.f2454b = r15
            goto L_0x0148
        L_0x0134:
            if (r14 == 0) goto L_0x013b
            androidx.work.r r15 = androidx.work.r.CANCELLED
            r11.f2454b = r15
            goto L_0x0148
        L_0x013b:
            androidx.work.r r15 = androidx.work.r.BLOCKED
            r11.f2454b = r15
            goto L_0x0148
        L_0x0140:
            boolean r15 = r11.d()
            if (r15 != 0) goto L_0x014b
            r11.n = r4
        L_0x0148:
            r17 = r4
            goto L_0x0151
        L_0x014b:
            r17 = r4
            r3 = 0
            r11.n = r3
        L_0x0151:
            int r3 = android.os.Build.VERSION.SDK_INT
            r4 = 23
            if (r3 < r4) goto L_0x015f
            r4 = 25
            if (r3 > r4) goto L_0x015f
            a(r11)
            goto L_0x0170
        L_0x015f:
            int r3 = android.os.Build.VERSION.SDK_INT
            r4 = 22
            if (r3 > r4) goto L_0x0170
            java.lang.String r3 = "androidx.work.impl.background.gcm.GcmScheduler"
            boolean r3 = a(r0, r3)
            if (r3 == 0) goto L_0x0170
            a(r11)
        L_0x0170:
            androidx.work.r r3 = r11.f2454b
            androidx.work.r r4 = androidx.work.r.ENQUEUED
            if (r3 != r4) goto L_0x0178
            r16 = 1
        L_0x0178:
            androidx.work.impl.m.q r3 = r6.q()
            r3.a(r11)
            if (r9 == 0) goto L_0x019a
            int r3 = r1.length
            r4 = 0
        L_0x0183:
            if (r4 >= r3) goto L_0x019a
            r5 = r1[r4]
            androidx.work.impl.m.a r11 = new androidx.work.impl.m.a
            java.lang.String r15 = r10.a()
            r11.<init>(r15, r5)
            androidx.work.impl.m.b r5 = r6.l()
            r5.a(r11)
            int r4 = r4 + 1
            goto L_0x0183
        L_0x019a:
            java.util.Set r3 = r10.b()
            java.util.Iterator r3 = r3.iterator()
        L_0x01a2:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x01bf
            java.lang.Object r4 = r3.next()
            java.lang.String r4 = (java.lang.String) r4
            androidx.work.impl.m.t r5 = r6.r()
            androidx.work.impl.m.s r11 = new androidx.work.impl.m.s
            java.lang.String r15 = r10.a()
            r11.<init>(r4, r15)
            r5.a(r11)
            goto L_0x01a2
        L_0x01bf:
            if (r7 == 0) goto L_0x01d1
            androidx.work.impl.m.k r3 = r6.o()
            androidx.work.impl.m.j r4 = new androidx.work.impl.m.j
            java.lang.String r5 = r10.a()
            r4.<init>(r2, r5)
            r3.a(r4)
        L_0x01d1:
            r4 = r17
            r3 = 0
            goto L_0x0119
        L_0x01d6:
            return r16
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.work.impl.utils.b.a(androidx.work.impl.i, java.util.List, java.lang.String[], java.lang.String, androidx.work.f):boolean");
    }

    private static void a(p pVar) {
        c cVar = pVar.f2462j;
        if (cVar.f() || cVar.i()) {
            String str = pVar.f2455c;
            e.a aVar = new e.a();
            aVar.a(pVar.f2457e);
            aVar.a("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME", str);
            pVar.f2455c = ConstraintTrackingWorker.class.getName();
            pVar.f2457e = aVar.a();
        }
    }

    private static boolean a(i iVar, String str) {
        try {
            Class<?> cls = Class.forName(str);
            for (d dVar : iVar.e()) {
                if (cls.isAssignableFrom(dVar.getClass())) {
                    return true;
                }
            }
        } catch (ClassNotFoundException unused) {
        }
        return false;
    }
}
