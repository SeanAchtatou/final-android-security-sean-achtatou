package androidx.work.impl.utils.m;

import androidx.work.ListenableWorker;
import com.google.common.util.concurrent.ListenableFuture;

/* compiled from: SettableFuture */
public final class c<V> extends a<V> {
    private c() {
    }

    public static <V> c<V> d() {
        return new c<>();
    }

    public boolean a(ListenableWorker.a aVar) {
        return super.a(aVar);
    }

    public boolean a(Throwable th) {
        return super.a(th);
    }

    public boolean a(ListenableFuture<? extends ListenableWorker.a> listenableFuture) {
        return super.a(listenableFuture);
    }
}
