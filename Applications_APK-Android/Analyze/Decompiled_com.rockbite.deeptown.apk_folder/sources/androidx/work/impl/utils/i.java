package androidx.work.impl.utils;

import android.content.Context;
import android.os.PowerManager;
import androidx.work.k;
import java.util.HashMap;
import java.util.WeakHashMap;

/* compiled from: WakeLocks */
public class i {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2514a = k.a("WakeLocks");

    /* renamed from: b  reason: collision with root package name */
    private static final WeakHashMap<PowerManager.WakeLock, String> f2515b = new WeakHashMap<>();

    public static PowerManager.WakeLock a(Context context, String str) {
        String str2 = "WorkManager: " + str;
        PowerManager.WakeLock newWakeLock = ((PowerManager) context.getApplicationContext().getSystemService("power")).newWakeLock(1, str2);
        synchronized (f2515b) {
            f2515b.put(newWakeLock, str2);
        }
        return newWakeLock;
    }

    public static void a() {
        HashMap hashMap = new HashMap();
        synchronized (f2515b) {
            hashMap.putAll(f2515b);
        }
        for (PowerManager.WakeLock wakeLock : hashMap.keySet()) {
            if (wakeLock != null && wakeLock.isHeld()) {
                k.a().e(f2514a, String.format("WakeLock held for %s", hashMap.get(wakeLock)), new Throwable[0]);
            }
        }
    }
}
