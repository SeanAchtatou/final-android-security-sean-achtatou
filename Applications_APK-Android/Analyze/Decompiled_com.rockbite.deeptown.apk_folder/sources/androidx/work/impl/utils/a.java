package androidx.work.impl.utils;

import androidx.work.impl.WorkDatabase;
import androidx.work.impl.d;
import androidx.work.impl.e;
import androidx.work.impl.i;
import androidx.work.impl.m.q;
import androidx.work.n;
import androidx.work.r;
import java.util.LinkedList;
import java.util.UUID;

/* compiled from: CancelWorkRunnable */
public abstract class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final androidx.work.impl.b f2487a = new androidx.work.impl.b();

    /* renamed from: androidx.work.impl.utils.a$a  reason: collision with other inner class name */
    /* compiled from: CancelWorkRunnable */
    static class C0032a extends a {

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ i f2488b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ UUID f2489c;

        C0032a(i iVar, UUID uuid) {
            this.f2488b = iVar;
            this.f2489c = uuid;
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: package-private */
        public void b() {
            WorkDatabase f2 = this.f2488b.f();
            f2.c();
            try {
                a(this.f2488b, this.f2489c.toString());
                f2.k();
                f2.e();
                a(this.f2488b);
            } catch (Throwable th) {
                f2.e();
                throw th;
            }
        }
    }

    /* compiled from: CancelWorkRunnable */
    static class b extends a {

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ i f2490b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ String f2491c;

        b(i iVar, String str) {
            this.f2490b = iVar;
            this.f2491c = str;
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: package-private */
        public void b() {
            WorkDatabase f2 = this.f2490b.f();
            f2.c();
            try {
                for (String a2 : f2.q().g(this.f2491c)) {
                    a(this.f2490b, a2);
                }
                f2.k();
                f2.e();
                a(this.f2490b);
            } catch (Throwable th) {
                f2.e();
                throw th;
            }
        }
    }

    /* compiled from: CancelWorkRunnable */
    static class c extends a {

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ i f2492b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ String f2493c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ boolean f2494d;

        c(i iVar, String str, boolean z) {
            this.f2492b = iVar;
            this.f2493c = str;
            this.f2494d = z;
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: package-private */
        public void b() {
            WorkDatabase f2 = this.f2492b.f();
            f2.c();
            try {
                for (String a2 : f2.q().c(this.f2493c)) {
                    a(this.f2492b, a2);
                }
                f2.k();
                f2.e();
                if (this.f2494d) {
                    a(this.f2492b);
                }
            } catch (Throwable th) {
                f2.e();
                throw th;
            }
        }
    }

    public n a() {
        return this.f2487a;
    }

    /* access modifiers changed from: package-private */
    public abstract void b();

    public void run() {
        try {
            b();
            this.f2487a.a(n.f2576a);
        } catch (Throwable th) {
            this.f2487a.a(new n.b.a(th));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(i iVar, String str) {
        a(iVar.f(), str);
        iVar.d().e(str);
        for (d a2 : iVar.e()) {
            a2.a(str);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(i iVar) {
        e.a(iVar.b(), iVar.f(), iVar.e());
    }

    private void a(WorkDatabase workDatabase, String str) {
        q q = workDatabase.q();
        androidx.work.impl.m.b l = workDatabase.l();
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            r d2 = q.d(str2);
            if (!(d2 == r.SUCCEEDED || d2 == r.FAILED)) {
                q.a(r.CANCELLED, str2);
            }
            linkedList.addAll(l.a(str2));
        }
    }

    public static a a(UUID uuid, i iVar) {
        return new C0032a(iVar, uuid);
    }

    public static a a(String str, i iVar) {
        return new b(iVar, str);
    }

    public static a a(String str, i iVar, boolean z) {
        return new c(iVar, str, z);
    }
}
