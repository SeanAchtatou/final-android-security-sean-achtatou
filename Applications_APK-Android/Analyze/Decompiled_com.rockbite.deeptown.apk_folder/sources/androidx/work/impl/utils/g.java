package androidx.work.impl.utils;

import androidx.work.WorkerParameters;
import androidx.work.impl.i;

/* compiled from: StartWorkRunnable */
public class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private i f2507a;

    /* renamed from: b  reason: collision with root package name */
    private String f2508b;

    /* renamed from: c  reason: collision with root package name */
    private WorkerParameters.a f2509c;

    public g(i iVar, String str, WorkerParameters.a aVar) {
        this.f2507a = iVar;
        this.f2508b = str;
        this.f2509c = aVar;
    }

    public void run() {
        this.f2507a.d().a(this.f2508b, this.f2509c);
    }
}
