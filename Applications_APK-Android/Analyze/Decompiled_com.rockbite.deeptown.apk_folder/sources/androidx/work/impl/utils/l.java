package androidx.work.impl.utils;

import androidx.work.k;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/* compiled from: WorkTimer */
public class l {

    /* renamed from: f  reason: collision with root package name */
    private static final String f2516f = k.a("WorkTimer");

    /* renamed from: a  reason: collision with root package name */
    private final ThreadFactory f2517a = new a(this);

    /* renamed from: b  reason: collision with root package name */
    private final ScheduledExecutorService f2518b = Executors.newSingleThreadScheduledExecutor(this.f2517a);

    /* renamed from: c  reason: collision with root package name */
    final Map<String, c> f2519c = new HashMap();

    /* renamed from: d  reason: collision with root package name */
    final Map<String, b> f2520d = new HashMap();

    /* renamed from: e  reason: collision with root package name */
    final Object f2521e = new Object();

    /* compiled from: WorkTimer */
    class a implements ThreadFactory {

        /* renamed from: a  reason: collision with root package name */
        private int f2522a = 0;

        a(l lVar) {
        }

        public Thread newThread(Runnable runnable) {
            Thread newThread = Executors.defaultThreadFactory().newThread(runnable);
            newThread.setName("WorkManager-WorkTimer-thread-" + this.f2522a);
            this.f2522a = this.f2522a + 1;
            return newThread;
        }
    }

    /* compiled from: WorkTimer */
    public interface b {
        void a(String str);
    }

    /* compiled from: WorkTimer */
    public static class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final l f2523a;

        /* renamed from: b  reason: collision with root package name */
        private final String f2524b;

        c(l lVar, String str) {
            this.f2523a = lVar;
            this.f2524b = str;
        }

        public void run() {
            synchronized (this.f2523a.f2521e) {
                if (this.f2523a.f2519c.remove(this.f2524b) != null) {
                    b remove = this.f2523a.f2520d.remove(this.f2524b);
                    if (remove != null) {
                        remove.a(this.f2524b);
                    }
                } else {
                    k.a().a("WrkTimerRunnable", String.format("Timer with %s is already marked as complete.", this.f2524b), new Throwable[0]);
                }
            }
        }
    }

    public void a(String str, long j2, b bVar) {
        synchronized (this.f2521e) {
            k.a().a(f2516f, String.format("Starting timer for %s", str), new Throwable[0]);
            a(str);
            c cVar = new c(this, str);
            this.f2519c.put(str, cVar);
            this.f2520d.put(str, bVar);
            this.f2518b.schedule(cVar, j2, TimeUnit.MILLISECONDS);
        }
    }

    public void a(String str) {
        synchronized (this.f2521e) {
            if (this.f2519c.remove(str) != null) {
                k.a().a(f2516f, String.format("Stopping timer for %s", str), new Throwable[0]);
                this.f2520d.remove(str);
            }
        }
    }

    public void a() {
        if (!this.f2518b.isShutdown()) {
            this.f2518b.shutdownNow();
        }
    }
}
