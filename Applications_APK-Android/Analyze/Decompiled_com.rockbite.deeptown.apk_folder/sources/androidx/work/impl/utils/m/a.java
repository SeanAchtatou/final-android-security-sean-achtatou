package androidx.work.impl.utils.m;

import androidx.work.ListenableWorker;
import com.google.common.util.concurrent.ListenableFuture;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import java.util.Locale;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: AbstractFuture */
public abstract class a<V> implements ListenableFuture<V> {

    /* renamed from: d  reason: collision with root package name */
    static final boolean f2525d = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));

    /* renamed from: e  reason: collision with root package name */
    private static final Logger f2526e = Logger.getLogger(a.class.getName());

    /* renamed from: f  reason: collision with root package name */
    static final b f2527f;

    /* renamed from: g  reason: collision with root package name */
    private static final Object f2528g = new Object();

    /* renamed from: a  reason: collision with root package name */
    volatile Object f2529a;

    /* renamed from: b  reason: collision with root package name */
    volatile e f2530b;

    /* renamed from: c  reason: collision with root package name */
    volatile i f2531c;

    /* compiled from: AbstractFuture */
    private static abstract class b {
        private b() {
        }

        /* access modifiers changed from: package-private */
        public abstract void a(i iVar, i iVar2);

        /* access modifiers changed from: package-private */
        public abstract void a(i iVar, Thread thread);

        /* access modifiers changed from: package-private */
        public abstract boolean a(a<?> aVar, e eVar, e eVar2);

        /* access modifiers changed from: package-private */
        public abstract boolean a(a<?> aVar, i iVar, i iVar2);

        /* access modifiers changed from: package-private */
        public abstract boolean a(a<?> aVar, Object obj, Object obj2);
    }

    /* compiled from: AbstractFuture */
    private static final class c {

        /* renamed from: c  reason: collision with root package name */
        static final c f2532c;

        /* renamed from: d  reason: collision with root package name */
        static final c f2533d;

        /* renamed from: a  reason: collision with root package name */
        final boolean f2534a;

        /* renamed from: b  reason: collision with root package name */
        final Throwable f2535b;

        static {
            if (a.f2525d) {
                f2533d = null;
                f2532c = null;
                return;
            }
            f2533d = new c(false, null);
            f2532c = new c(true, null);
        }

        c(boolean z, Throwable th) {
            this.f2534a = z;
            this.f2535b = th;
        }
    }

    /* compiled from: AbstractFuture */
    private static final class d {

        /* renamed from: b  reason: collision with root package name */
        static final d f2536b = new d(new C0034a("Failure occurred while trying to finish a future."));

        /* renamed from: a  reason: collision with root package name */
        final Throwable f2537a;

        /* renamed from: androidx.work.impl.utils.m.a$d$a  reason: collision with other inner class name */
        /* compiled from: AbstractFuture */
        static class C0034a extends Throwable {
            C0034a(String str) {
                super(str);
            }

            public synchronized Throwable fillInStackTrace() {
                return this;
            }
        }

        d(Throwable th) {
            a.b(th);
            this.f2537a = th;
        }
    }

    /* compiled from: AbstractFuture */
    private static final class e {

        /* renamed from: d  reason: collision with root package name */
        static final e f2538d = new e(null, null);

        /* renamed from: a  reason: collision with root package name */
        final Runnable f2539a;

        /* renamed from: b  reason: collision with root package name */
        final Executor f2540b;

        /* renamed from: c  reason: collision with root package name */
        e f2541c;

        e(Runnable runnable, Executor executor) {
            this.f2539a = runnable;
            this.f2540b = executor;
        }
    }

    /* compiled from: AbstractFuture */
    private static final class f extends b {

        /* renamed from: a  reason: collision with root package name */
        final AtomicReferenceFieldUpdater<i, Thread> f2542a;

        /* renamed from: b  reason: collision with root package name */
        final AtomicReferenceFieldUpdater<i, i> f2543b;

        /* renamed from: c  reason: collision with root package name */
        final AtomicReferenceFieldUpdater<a, i> f2544c;

        /* renamed from: d  reason: collision with root package name */
        final AtomicReferenceFieldUpdater<a, e> f2545d;

        /* renamed from: e  reason: collision with root package name */
        final AtomicReferenceFieldUpdater<a, Object> f2546e;

        f(AtomicReferenceFieldUpdater<i, Thread> atomicReferenceFieldUpdater, AtomicReferenceFieldUpdater<i, i> atomicReferenceFieldUpdater2, AtomicReferenceFieldUpdater<a, i> atomicReferenceFieldUpdater3, AtomicReferenceFieldUpdater<a, e> atomicReferenceFieldUpdater4, AtomicReferenceFieldUpdater<a, Object> atomicReferenceFieldUpdater5) {
            super();
            this.f2542a = atomicReferenceFieldUpdater;
            this.f2543b = atomicReferenceFieldUpdater2;
            this.f2544c = atomicReferenceFieldUpdater3;
            this.f2545d = atomicReferenceFieldUpdater4;
            this.f2546e = atomicReferenceFieldUpdater5;
        }

        /* access modifiers changed from: package-private */
        public void a(i iVar, Thread thread) {
            this.f2542a.lazySet(iVar, thread);
        }

        /* access modifiers changed from: package-private */
        public void a(i iVar, i iVar2) {
            this.f2543b.lazySet(iVar, iVar2);
        }

        /* access modifiers changed from: package-private */
        public boolean a(a<?> aVar, i iVar, i iVar2) {
            return this.f2544c.compareAndSet(aVar, iVar, iVar2);
        }

        /* access modifiers changed from: package-private */
        public boolean a(a<?> aVar, e eVar, e eVar2) {
            return this.f2545d.compareAndSet(aVar, eVar, eVar2);
        }

        /* access modifiers changed from: package-private */
        public boolean a(a<?> aVar, Object obj, Object obj2) {
            return this.f2546e.compareAndSet(aVar, obj, obj2);
        }
    }

    /* compiled from: AbstractFuture */
    private static final class g<V> implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final a<V> f2547a;

        /* renamed from: b  reason: collision with root package name */
        final ListenableFuture<? extends V> f2548b;

        g(a<V> aVar, ListenableFuture<? extends V> listenableFuture) {
            this.f2547a = aVar;
            this.f2548b = listenableFuture;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.work.impl.utils.m.a.b.a(androidx.work.impl.utils.m.a<?>, java.lang.Object, java.lang.Object):boolean
         arg types: [androidx.work.impl.utils.m.a<V>, androidx.work.impl.utils.m.a$g, java.lang.Object]
         candidates:
          androidx.work.impl.utils.m.a.b.a(androidx.work.impl.utils.m.a<?>, androidx.work.impl.utils.m.a$e, androidx.work.impl.utils.m.a$e):boolean
          androidx.work.impl.utils.m.a.b.a(androidx.work.impl.utils.m.a<?>, androidx.work.impl.utils.m.a$i, androidx.work.impl.utils.m.a$i):boolean
          androidx.work.impl.utils.m.a.b.a(androidx.work.impl.utils.m.a<?>, java.lang.Object, java.lang.Object):boolean */
        public void run() {
            if (this.f2547a.f2529a == this) {
                if (a.f2527f.a((a<?>) this.f2547a, (Object) this, a.b((ListenableFuture<?>) this.f2548b))) {
                    a.a((a<?>) this.f2547a);
                }
            }
        }
    }

    /* compiled from: AbstractFuture */
    private static final class h extends b {
        h() {
            super();
        }

        /* access modifiers changed from: package-private */
        public void a(i iVar, Thread thread) {
            iVar.f2550a = thread;
        }

        /* access modifiers changed from: package-private */
        public void a(i iVar, i iVar2) {
            iVar.f2551b = iVar2;
        }

        /* access modifiers changed from: package-private */
        public boolean a(a<?> aVar, i iVar, i iVar2) {
            synchronized (aVar) {
                if (aVar.f2531c != iVar) {
                    return false;
                }
                aVar.f2531c = iVar2;
                return true;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(a<?> aVar, e eVar, e eVar2) {
            synchronized (aVar) {
                if (aVar.f2530b != eVar) {
                    return false;
                }
                aVar.f2530b = eVar2;
                return true;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(a<?> aVar, Object obj, Object obj2) {
            synchronized (aVar) {
                if (aVar.f2529a != obj) {
                    return false;
                }
                aVar.f2529a = obj2;
                return true;
            }
        }
    }

    /* compiled from: AbstractFuture */
    private static final class i {

        /* renamed from: c  reason: collision with root package name */
        static final i f2549c = new i(false);

        /* renamed from: a  reason: collision with root package name */
        volatile Thread f2550a;

        /* renamed from: b  reason: collision with root package name */
        volatile i f2551b;

        i(boolean z) {
        }

        /* access modifiers changed from: package-private */
        public void a(i iVar) {
            a.f2527f.a(this, iVar);
        }

        i() {
            a.f2527f.a(this, Thread.currentThread());
        }

        /* access modifiers changed from: package-private */
        public void a() {
            Thread thread = this.f2550a;
            if (thread != null) {
                this.f2550a = null;
                LockSupport.unpark(thread);
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: androidx.work.impl.utils.m.a$f} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: androidx.work.impl.utils.m.a$h} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: androidx.work.impl.utils.m.a$f} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: androidx.work.impl.utils.m.a$f} */
    /* JADX WARNING: Multi-variable type inference failed */
    static {
        /*
            java.lang.Class<androidx.work.impl.utils.m.a$i> r0 = androidx.work.impl.utils.m.a.i.class
            java.lang.String r1 = "guava.concurrent.generate_cancellation_cause"
            java.lang.String r2 = "false"
            java.lang.String r1 = java.lang.System.getProperty(r1, r2)
            boolean r1 = java.lang.Boolean.parseBoolean(r1)
            androidx.work.impl.utils.m.a.f2525d = r1
            java.lang.Class<androidx.work.impl.utils.m.a> r1 = androidx.work.impl.utils.m.a.class
            java.lang.String r1 = r1.getName()
            java.util.logging.Logger r1 = java.util.logging.Logger.getLogger(r1)
            androidx.work.impl.utils.m.a.f2526e = r1
            androidx.work.impl.utils.m.a$f r1 = new androidx.work.impl.utils.m.a$f     // Catch:{ all -> 0x004e }
            java.lang.Class<java.lang.Thread> r2 = java.lang.Thread.class
            java.lang.String r3 = "a"
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r3 = java.util.concurrent.atomic.AtomicReferenceFieldUpdater.newUpdater(r0, r2, r3)     // Catch:{ all -> 0x004e }
            java.lang.String r2 = "b"
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r4 = java.util.concurrent.atomic.AtomicReferenceFieldUpdater.newUpdater(r0, r0, r2)     // Catch:{ all -> 0x004e }
            java.lang.Class<androidx.work.impl.utils.m.a> r2 = androidx.work.impl.utils.m.a.class
            java.lang.String r5 = "c"
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r5 = java.util.concurrent.atomic.AtomicReferenceFieldUpdater.newUpdater(r2, r0, r5)     // Catch:{ all -> 0x004e }
            java.lang.Class<androidx.work.impl.utils.m.a> r0 = androidx.work.impl.utils.m.a.class
            java.lang.Class<androidx.work.impl.utils.m.a$e> r2 = androidx.work.impl.utils.m.a.e.class
            java.lang.String r6 = "b"
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r6 = java.util.concurrent.atomic.AtomicReferenceFieldUpdater.newUpdater(r0, r2, r6)     // Catch:{ all -> 0x004e }
            java.lang.Class<androidx.work.impl.utils.m.a> r0 = androidx.work.impl.utils.m.a.class
            java.lang.Class<java.lang.Object> r2 = java.lang.Object.class
            java.lang.String r7 = "a"
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r7 = java.util.concurrent.atomic.AtomicReferenceFieldUpdater.newUpdater(r0, r2, r7)     // Catch:{ all -> 0x004e }
            r2 = r1
            r2.<init>(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x004e }
            r0 = 0
            goto L_0x0054
        L_0x004e:
            r0 = move-exception
            androidx.work.impl.utils.m.a$h r1 = new androidx.work.impl.utils.m.a$h
            r1.<init>()
        L_0x0054:
            androidx.work.impl.utils.m.a.f2527f = r1
            java.lang.Class<java.util.concurrent.locks.LockSupport> r1 = java.util.concurrent.locks.LockSupport.class
            if (r0 == 0) goto L_0x0063
            java.util.logging.Logger r1 = androidx.work.impl.utils.m.a.f2526e
            java.util.logging.Level r2 = java.util.logging.Level.SEVERE
            java.lang.String r3 = "SafeAtomicHelper is broken!"
            r1.log(r2, r3, r0)
        L_0x0063:
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            androidx.work.impl.utils.m.a.f2528g = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.work.impl.utils.m.a.<clinit>():void");
    }

    protected a() {
    }

    private void a(i iVar) {
        iVar.f2550a = null;
        while (true) {
            i iVar2 = this.f2531c;
            if (iVar2 != i.f2549c) {
                i iVar3 = null;
                while (iVar2 != null) {
                    i iVar4 = iVar2.f2551b;
                    if (iVar2.f2550a != null) {
                        iVar3 = iVar2;
                    } else if (iVar3 != null) {
                        iVar3.f2551b = iVar4;
                        if (iVar3.f2550a == null) {
                        }
                    } else if (!f2527f.a((a<?>) this, iVar2, iVar4)) {
                    }
                    iVar2 = iVar4;
                }
                return;
            }
            return;
        }
    }

    static Object b(ListenableFuture<?> listenableFuture) {
        if (listenableFuture instanceof a) {
            Object obj = ((a) listenableFuture).f2529a;
            if (!(obj instanceof c)) {
                return obj;
            }
            c cVar = (c) obj;
            if (!cVar.f2534a) {
                return obj;
            }
            Throwable th = cVar.f2535b;
            return th != null ? new c(false, th) : c.f2533d;
        }
        boolean isCancelled = listenableFuture.isCancelled();
        if ((!f2525d) && isCancelled) {
            return c.f2533d;
        }
        try {
            Object a2 = a(listenableFuture);
            return a2 == null ? f2528g : a2;
        } catch (ExecutionException e2) {
            return new d(e2.getCause());
        } catch (CancellationException e3) {
            if (isCancelled) {
                return new c(false, e3);
            }
            return new d(new IllegalArgumentException("get() threw CancellationException, despite reporting isCancelled() == false: " + listenableFuture, e3));
        } catch (Throwable th2) {
            return new d(th2);
        }
    }

    private V c(Object obj) throws ExecutionException {
        if (obj instanceof c) {
            throw a("Task was cancelled.", ((c) obj).f2535b);
        } else if (obj instanceof d) {
            throw new ExecutionException(((d) obj).f2537a);
        } else if (obj == f2528g) {
            return null;
        } else {
            return obj;
        }
    }

    private void d() {
        i iVar;
        do {
            iVar = this.f2531c;
        } while (!f2527f.a((a<?>) this, iVar, i.f2549c));
        while (iVar != null) {
            iVar.a();
            iVar = iVar.f2551b;
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    public final void addListener(Runnable runnable, Executor executor) {
        b(runnable);
        b(executor);
        e eVar = this.f2530b;
        if (eVar != e.f2538d) {
            e eVar2 = new e(runnable, executor);
            do {
                eVar2.f2541c = eVar;
                if (!f2527f.a((a<?>) this, eVar, eVar2)) {
                    eVar = this.f2530b;
                } else {
                    return;
                }
            } while (eVar != e.f2538d);
        }
        a(runnable, executor);
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    public final boolean cancel(boolean z) {
        Object obj = this.f2529a;
        if (!(obj == null) && !(obj instanceof g)) {
            return false;
        }
        c cVar = f2525d ? new c(z, new CancellationException("Future.cancel() was called.")) : z ? c.f2532c : c.f2533d;
        boolean z2 = false;
        Object obj2 = obj;
        a aVar = this;
        while (true) {
            if (f2527f.a(aVar, obj2, cVar)) {
                if (z) {
                    aVar.b();
                }
                a((a<?>) aVar);
                if (!(obj2 instanceof g)) {
                    return true;
                }
                ListenableFuture<? extends V> listenableFuture = ((g) obj2).f2548b;
                if (listenableFuture instanceof a) {
                    aVar = (a) listenableFuture;
                    obj2 = aVar.f2529a;
                    if (!(obj2 == null) && !(obj2 instanceof g)) {
                        return true;
                    }
                    z2 = true;
                } else {
                    listenableFuture.cancel(z);
                    return true;
                }
            } else {
                obj2 = aVar.f2529a;
                if (!(obj2 instanceof g)) {
                    return z2;
                }
            }
        }
    }

    public final V get(long j2, TimeUnit timeUnit) throws InterruptedException, TimeoutException, ExecutionException {
        long j3 = j2;
        TimeUnit timeUnit2 = timeUnit;
        long nanos = timeUnit2.toNanos(j3);
        if (!Thread.interrupted()) {
            Object obj = this.f2529a;
            if ((obj != null) && (!(obj instanceof g))) {
                return c(obj);
            }
            long nanoTime = nanos > 0 ? System.nanoTime() + nanos : 0;
            if (nanos >= 1000) {
                i iVar = this.f2531c;
                if (iVar != i.f2549c) {
                    i iVar2 = new i();
                    do {
                        iVar2.a(iVar);
                        if (f2527f.a((a<?>) this, iVar, iVar2)) {
                            do {
                                LockSupport.parkNanos(this, nanos);
                                if (!Thread.interrupted()) {
                                    Object obj2 = this.f2529a;
                                    if ((obj2 != null) && (!(obj2 instanceof g))) {
                                        return c(obj2);
                                    }
                                    nanos = nanoTime - System.nanoTime();
                                } else {
                                    a(iVar2);
                                    throw new InterruptedException();
                                }
                            } while (nanos >= 1000);
                            a(iVar2);
                        } else {
                            iVar = this.f2531c;
                        }
                    } while (iVar != i.f2549c);
                }
                return c(this.f2529a);
            }
            while (nanos > 0) {
                Object obj3 = this.f2529a;
                if ((obj3 != null) && (!(obj3 instanceof g))) {
                    return c(obj3);
                }
                if (!Thread.interrupted()) {
                    nanos = nanoTime - System.nanoTime();
                } else {
                    throw new InterruptedException();
                }
            }
            String aVar = toString();
            String lowerCase = timeUnit.toString().toLowerCase(Locale.ROOT);
            String str = "Waited " + j3 + RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER + timeUnit.toString().toLowerCase(Locale.ROOT);
            if (nanos + 1000 < 0) {
                String str2 = str + " (plus ";
                long j4 = -nanos;
                long convert = timeUnit2.convert(j4, TimeUnit.NANOSECONDS);
                long nanos2 = j4 - timeUnit2.toNanos(convert);
                boolean z = convert == 0 || nanos2 > 1000;
                if (convert > 0) {
                    String str3 = str2 + convert + RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER + lowerCase;
                    if (z) {
                        str3 = str3 + ",";
                    }
                    str2 = str3 + RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER;
                }
                if (z) {
                    str2 = str2 + nanos2 + " nanoseconds ";
                }
                str = str2 + "delay)";
            }
            if (isDone()) {
                throw new TimeoutException(str + " but future completed as timeout expired");
            }
            throw new TimeoutException(str + " for " + aVar);
        }
        throw new InterruptedException();
    }

    public final boolean isCancelled() {
        return this.f2529a instanceof c;
    }

    public final boolean isDone() {
        Object obj = this.f2529a;
        return (!(obj instanceof g)) & (obj != null);
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("[status=");
        if (isCancelled()) {
            sb.append("CANCELLED");
        } else if (isDone()) {
            a(sb);
        } else {
            try {
                str = c();
            } catch (RuntimeException e2) {
                str = "Exception thrown from implementation: " + e2.getClass();
            }
            if (str != null && !str.isEmpty()) {
                sb.append("PENDING, info=[");
                sb.append(str);
                sb.append("]");
            } else if (isDone()) {
                a(sb);
            } else {
                sb.append("PENDING");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    private String d(Object obj) {
        return obj == this ? "this future" : String.valueOf(obj);
    }

    /* access modifiers changed from: protected */
    public String c() {
        Object obj = this.f2529a;
        if (obj instanceof g) {
            return "setFuture=[" + d(((g) obj).f2548b) + "]";
        } else if (!(this instanceof ScheduledFuture)) {
            return null;
        } else {
            return "remaining delay=[" + ((ScheduledFuture) this).getDelay(TimeUnit.MILLISECONDS) + " ms]";
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:0:0x0000 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: androidx.work.ListenableWorker$a} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: androidx.work.ListenableWorker$a} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(androidx.work.ListenableWorker.a r3) {
        /*
            r2 = this;
            if (r3 != 0) goto L_0x0004
            java.lang.Object r3 = androidx.work.impl.utils.m.a.f2528g
        L_0x0004:
            androidx.work.impl.utils.m.a$b r0 = androidx.work.impl.utils.m.a.f2527f
            r1 = 0
            boolean r3 = r0.a(r2, r1, r3)
            if (r3 == 0) goto L_0x0012
            a(r2)
            r3 = 1
            return r3
        L_0x0012:
            r3 = 0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.work.impl.utils.m.a.a(java.lang.Object):boolean");
    }

    /* access modifiers changed from: protected */
    public boolean a(Throwable th) {
        b(th);
        if (!f2527f.a(this, (Object) null, new d(th))) {
            return false;
        }
        a((a<?>) this);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean a(ListenableFuture<? extends ListenableWorker.a> listenableFuture) {
        g gVar;
        d dVar;
        b((Object) listenableFuture);
        Object obj = this.f2529a;
        if (obj == null) {
            if (listenableFuture.isDone()) {
                if (!f2527f.a(this, (Object) null, b((ListenableFuture<?>) listenableFuture))) {
                    return false;
                }
                a((a<?>) this);
                return true;
            }
            gVar = new g(this, listenableFuture);
            if (f2527f.a(this, (Object) null, gVar)) {
                try {
                    listenableFuture.addListener(gVar, b.INSTANCE);
                } catch (Throwable unused) {
                    dVar = d.f2536b;
                }
                return true;
            }
            obj = this.f2529a;
        }
        if (obj instanceof c) {
            listenableFuture.cancel(((c) obj).f2534a);
        }
        return false;
        f2527f.a(this, gVar, dVar);
        return true;
    }

    static <T> T b(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }

    private static <V> V a(Future<ListenableWorker.a> future) throws ExecutionException {
        V v;
        boolean z = false;
        while (true) {
            try {
                v = future.get();
                break;
            } catch (InterruptedException unused) {
                z = true;
            } catch (Throwable th) {
                if (z) {
                    Thread.currentThread().interrupt();
                }
                throw th;
            }
        }
        if (z) {
            Thread.currentThread().interrupt();
        }
        return v;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:13:0x0001 */
    static void a(a<?> aVar) {
        e eVar = null;
        a aVar2 = aVar;
        while (true) {
            aVar2.d();
            aVar2.a();
            e a2 = aVar2.a(eVar);
            while (true) {
                if (a2 != null) {
                    eVar = a2.f2541c;
                    Runnable runnable = a2.f2539a;
                    if (runnable instanceof g) {
                        g gVar = (g) runnable;
                        aVar2 = gVar.f2547a;
                        if (aVar2.f2529a == gVar) {
                            if (f2527f.a(aVar2, gVar, b((ListenableFuture<?>) gVar.f2548b))) {
                            }
                        } else {
                            continue;
                        }
                    } else {
                        a(runnable, a2.f2540b);
                    }
                    a2 = eVar;
                } else {
                    return;
                }
            }
        }
    }

    private e a(e eVar) {
        e eVar2;
        do {
            eVar2 = this.f2530b;
        } while (!f2527f.a((a<?>) this, eVar2, e.f2538d));
        e eVar3 = eVar2;
        e eVar4 = eVar;
        e eVar5 = eVar3;
        while (eVar5 != null) {
            e eVar6 = eVar5.f2541c;
            eVar5.f2541c = eVar4;
            eVar4 = eVar5;
            eVar5 = eVar6;
        }
        return eVar4;
    }

    public final V get() throws InterruptedException, ExecutionException {
        Object obj;
        if (!Thread.interrupted()) {
            Object obj2 = this.f2529a;
            if ((obj2 != null) && (!(obj2 instanceof g))) {
                return c(obj2);
            }
            i iVar = this.f2531c;
            if (iVar != i.f2549c) {
                i iVar2 = new i();
                do {
                    iVar2.a(iVar);
                    if (f2527f.a((a<?>) this, iVar, iVar2)) {
                        do {
                            LockSupport.park(this);
                            if (!Thread.interrupted()) {
                                obj = this.f2529a;
                            } else {
                                a(iVar2);
                                throw new InterruptedException();
                            }
                        } while (!((obj != null) & (!(obj instanceof g))));
                        return c(obj);
                    }
                    iVar = this.f2531c;
                } while (iVar != i.f2549c);
            }
            return c(this.f2529a);
        }
        throw new InterruptedException();
    }

    private void a(StringBuilder sb) {
        try {
            Object a2 = a((Future<ListenableWorker.a>) this);
            sb.append("SUCCESS, result=[");
            sb.append(d(a2));
            sb.append("]");
        } catch (ExecutionException e2) {
            sb.append("FAILURE, cause=[");
            sb.append(e2.getCause());
            sb.append("]");
        } catch (CancellationException unused) {
            sb.append("CANCELLED");
        } catch (RuntimeException e3) {
            sb.append("UNKNOWN, cause=[");
            sb.append(e3.getClass());
            sb.append(" thrown from get()]");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.RuntimeException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static void a(Runnable runnable, Executor executor) {
        try {
            executor.execute(runnable);
        } catch (RuntimeException e2) {
            Logger logger = f2526e;
            Level level = Level.SEVERE;
            logger.log(level, "RuntimeException while executing runnable " + runnable + " with executor " + executor, (Throwable) e2);
        }
    }

    private static CancellationException a(String str, Throwable th) {
        CancellationException cancellationException = new CancellationException(str);
        cancellationException.initCause(th);
        return cancellationException;
    }
}
