package androidx.work.impl.utils;

import androidx.work.impl.WorkDatabase;
import androidx.work.impl.i;
import androidx.work.impl.m.q;
import androidx.work.k;
import androidx.work.r;

/* compiled from: StopWorkRunnable */
public class h implements Runnable {

    /* renamed from: d  reason: collision with root package name */
    private static final String f2510d = k.a("StopWorkRunnable");

    /* renamed from: a  reason: collision with root package name */
    private final i f2511a;

    /* renamed from: b  reason: collision with root package name */
    private final String f2512b;

    /* renamed from: c  reason: collision with root package name */
    private final boolean f2513c;

    public h(i iVar, String str, boolean z) {
        this.f2511a = iVar;
        this.f2512b = str;
        this.f2513c = z;
    }

    public void run() {
        boolean z;
        WorkDatabase f2 = this.f2511a.f();
        q q = f2.q();
        f2.c();
        try {
            if (q.d(this.f2512b) == r.RUNNING) {
                q.a(r.ENQUEUED, this.f2512b);
            }
            if (this.f2513c) {
                z = this.f2511a.d().f(this.f2512b);
            } else {
                z = this.f2511a.d().g(this.f2512b);
            }
            k.a().a(f2510d, String.format("StopWorkRunnable for %s; Processor.stopWork = %s", this.f2512b, Boolean.valueOf(z)), new Throwable[0]);
            f2.k();
        } finally {
            f2.e();
        }
    }
}
