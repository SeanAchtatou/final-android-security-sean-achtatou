package androidx.work.impl;

import android.content.Context;
import androidx.room.h;
import androidx.room.i;
import androidx.work.impl.g;
import androidx.work.impl.m.e;
import androidx.work.impl.m.k;
import androidx.work.impl.m.n;
import androidx.work.impl.m.q;
import androidx.work.impl.m.t;
import b.m.a.c;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

public abstract class WorkDatabase extends i {

    /* renamed from: j  reason: collision with root package name */
    private static final long f2221j = TimeUnit.DAYS.toMillis(7);

    static class a implements c.C0063c {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Context f2222a;

        a(Context context) {
            this.f2222a = context;
        }

        public c a(c.b bVar) {
            c.b.a a2 = c.b.a(this.f2222a);
            a2.a(bVar.f3043b);
            a2.a(bVar.f3044c);
            a2.a(true);
            return new b.m.a.g.c().a(a2.a());
        }
    }

    static class b extends i.b {
        b() {
        }

        public void c(b.m.a.b bVar) {
            super.c(bVar);
            bVar.J();
            try {
                bVar.e(WorkDatabase.u());
                bVar.L();
            } finally {
                bVar.M();
            }
        }
    }

    public static WorkDatabase a(Context context, Executor executor, boolean z) {
        i.a aVar;
        Class<WorkDatabase> cls = WorkDatabase.class;
        if (z) {
            aVar = h.a(context, cls);
            aVar.a();
        } else {
            aVar = h.a(context, cls, h.a());
            aVar.a(new a(context));
        }
        aVar.a(executor);
        aVar.a(s());
        aVar.a(g.f2338a);
        aVar.a(new g.C0031g(context, 2, 3));
        aVar.a(g.f2339b);
        aVar.a(g.f2340c);
        aVar.a(new g.C0031g(context, 5, 6));
        aVar.a(g.f2341d);
        aVar.a(g.f2342e);
        aVar.a(g.f2343f);
        aVar.a(new g.h(context));
        aVar.c();
        return (WorkDatabase) aVar.b();
    }

    static i.b s() {
        return new b();
    }

    static long t() {
        return System.currentTimeMillis() - f2221j;
    }

    static String u() {
        return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (period_start_time + minimum_retention_duration) < " + t() + " AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
    }

    public abstract androidx.work.impl.m.b l();

    public abstract e m();

    public abstract androidx.work.impl.m.h n();

    public abstract k o();

    public abstract n p();

    public abstract q q();

    public abstract t r();
}
