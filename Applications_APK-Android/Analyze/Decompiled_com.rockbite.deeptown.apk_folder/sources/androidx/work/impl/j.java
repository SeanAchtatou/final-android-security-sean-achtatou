package androidx.work.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import androidx.work.e;
import androidx.work.i;
import androidx.work.impl.m.p;
import androidx.work.impl.m.q;
import androidx.work.impl.m.t;
import androidx.work.k;
import androidx.work.r;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

/* compiled from: WorkerWrapper */
public class j implements Runnable {
    static final String t = k.a("WorkerWrapper");

    /* renamed from: a  reason: collision with root package name */
    Context f2359a;

    /* renamed from: b  reason: collision with root package name */
    private String f2360b;

    /* renamed from: c  reason: collision with root package name */
    private List<d> f2361c;

    /* renamed from: d  reason: collision with root package name */
    private WorkerParameters.a f2362d;

    /* renamed from: e  reason: collision with root package name */
    p f2363e;

    /* renamed from: f  reason: collision with root package name */
    ListenableWorker f2364f;

    /* renamed from: g  reason: collision with root package name */
    ListenableWorker.a f2365g = ListenableWorker.a.a();

    /* renamed from: h  reason: collision with root package name */
    private androidx.work.b f2366h;

    /* renamed from: i  reason: collision with root package name */
    private androidx.work.impl.utils.n.a f2367i;

    /* renamed from: j  reason: collision with root package name */
    private androidx.work.impl.foreground.a f2368j;

    /* renamed from: k  reason: collision with root package name */
    private WorkDatabase f2369k;
    private q l;
    private androidx.work.impl.m.b m;
    private t n;
    private List<String> o;
    private String p;
    androidx.work.impl.utils.m.c<Boolean> q = androidx.work.impl.utils.m.c.d();
    ListenableFuture<ListenableWorker.a> r = null;
    private volatile boolean s;

    /* compiled from: WorkerWrapper */
    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ androidx.work.impl.utils.m.c f2370a;

        a(androidx.work.impl.utils.m.c cVar) {
            this.f2370a = cVar;
        }

        public void run() {
            try {
                k.a().a(j.t, String.format("Starting work for %s", j.this.f2363e.f2455c), new Throwable[0]);
                j.this.r = j.this.f2364f.j();
                this.f2370a.a((ListenableFuture<? extends ListenableWorker.a>) j.this.r);
            } catch (Throwable th) {
                this.f2370a.a(th);
            }
        }
    }

    /* compiled from: WorkerWrapper */
    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ androidx.work.impl.utils.m.c f2372a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f2373b;

        b(androidx.work.impl.utils.m.c cVar, String str) {
            this.f2372a = cVar;
            this.f2373b = str;
        }

        @SuppressLint({"SyntheticAccessor"})
        public void run() {
            try {
                ListenableWorker.a aVar = (ListenableWorker.a) this.f2372a.get();
                if (aVar == null) {
                    k.a().b(j.t, String.format("%s returned a null result. Treating it as a failure.", j.this.f2363e.f2455c), new Throwable[0]);
                } else {
                    k.a().a(j.t, String.format("%s returned a %s result.", j.this.f2363e.f2455c, aVar), new Throwable[0]);
                    j.this.f2365g = aVar;
                }
            } catch (CancellationException e2) {
                k.a().c(j.t, String.format("%s was cancelled", this.f2373b), e2);
            } catch (InterruptedException | ExecutionException e3) {
                k.a().b(j.t, String.format("%s failed because it threw an exception/error", this.f2373b), e3);
            } catch (Throwable th) {
                j.this.c();
                throw th;
            }
            j.this.c();
        }
    }

    /* compiled from: WorkerWrapper */
    public static class c {

        /* renamed from: a  reason: collision with root package name */
        Context f2375a;

        /* renamed from: b  reason: collision with root package name */
        ListenableWorker f2376b;

        /* renamed from: c  reason: collision with root package name */
        androidx.work.impl.foreground.a f2377c;

        /* renamed from: d  reason: collision with root package name */
        androidx.work.impl.utils.n.a f2378d;

        /* renamed from: e  reason: collision with root package name */
        androidx.work.b f2379e;

        /* renamed from: f  reason: collision with root package name */
        WorkDatabase f2380f;

        /* renamed from: g  reason: collision with root package name */
        String f2381g;

        /* renamed from: h  reason: collision with root package name */
        List<d> f2382h;

        /* renamed from: i  reason: collision with root package name */
        WorkerParameters.a f2383i = new WorkerParameters.a();

        public c(Context context, androidx.work.b bVar, androidx.work.impl.utils.n.a aVar, androidx.work.impl.foreground.a aVar2, WorkDatabase workDatabase, String str) {
            this.f2375a = context.getApplicationContext();
            this.f2378d = aVar;
            this.f2377c = aVar2;
            this.f2379e = bVar;
            this.f2380f = workDatabase;
            this.f2381g = str;
        }

        public c a(List<d> list) {
            this.f2382h = list;
            return this;
        }

        public c a(WorkerParameters.a aVar) {
            if (aVar != null) {
                this.f2383i = aVar;
            }
            return this;
        }

        public j a() {
            return new j(this);
        }
    }

    j(c cVar) {
        this.f2359a = cVar.f2375a;
        this.f2367i = cVar.f2378d;
        this.f2368j = cVar.f2377c;
        this.f2360b = cVar.f2381g;
        this.f2361c = cVar.f2382h;
        this.f2362d = cVar.f2383i;
        this.f2364f = cVar.f2376b;
        this.f2366h = cVar.f2379e;
        this.f2369k = cVar.f2380f;
        this.l = this.f2369k.q();
        this.m = this.f2369k.l();
        this.n = this.f2369k.r();
    }

    private void e() {
        this.f2369k.c();
        try {
            this.l.a(r.ENQUEUED, this.f2360b);
            this.l.b(this.f2360b, System.currentTimeMillis());
            this.l.a(this.f2360b, -1);
            this.f2369k.k();
        } finally {
            this.f2369k.e();
            a(true);
        }
    }

    private void f() {
        this.f2369k.c();
        try {
            this.l.b(this.f2360b, System.currentTimeMillis());
            this.l.a(r.ENQUEUED, this.f2360b);
            this.l.f(this.f2360b);
            this.l.a(this.f2360b, -1);
            this.f2369k.k();
        } finally {
            this.f2369k.e();
            a(false);
        }
    }

    private void g() {
        r d2 = this.l.d(this.f2360b);
        if (d2 == r.RUNNING) {
            k.a().a(t, String.format("Status for %s is RUNNING;not doing any work and rescheduling for later execution", this.f2360b), new Throwable[0]);
            a(true);
            return;
        }
        k.a().a(t, String.format("Status for %s is %s; not doing any work", this.f2360b, d2), new Throwable[0]);
        a(false);
    }

    private void h() {
        e a2;
        if (!j()) {
            this.f2369k.c();
            try {
                this.f2363e = this.l.e(this.f2360b);
                if (this.f2363e == null) {
                    k.a().b(t, String.format("Didn't find WorkSpec for id %s", this.f2360b), new Throwable[0]);
                    a(false);
                } else if (this.f2363e.f2454b != r.ENQUEUED) {
                    g();
                    this.f2369k.k();
                    k.a().a(t, String.format("%s is not in ENQUEUED state. Nothing more to do.", this.f2363e.f2455c), new Throwable[0]);
                    this.f2369k.e();
                } else {
                    if (this.f2363e.d() || this.f2363e.c()) {
                        long currentTimeMillis = System.currentTimeMillis();
                        if (!(this.f2363e.n == 0) && currentTimeMillis < this.f2363e.a()) {
                            k.a().a(t, String.format("Delaying execution for %s because it is being executed before schedule.", this.f2363e.f2455c), new Throwable[0]);
                            a(true);
                            this.f2369k.e();
                            return;
                        }
                    }
                    this.f2369k.k();
                    this.f2369k.e();
                    if (this.f2363e.d()) {
                        a2 = this.f2363e.f2457e;
                    } else {
                        i b2 = this.f2366h.b().b(this.f2363e.f2456d);
                        if (b2 == null) {
                            k.a().b(t, String.format("Could not create Input Merger %s", this.f2363e.f2456d), new Throwable[0]);
                            d();
                            return;
                        }
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(this.f2363e.f2457e);
                        arrayList.addAll(this.l.h(this.f2360b));
                        a2 = b2.a(arrayList);
                    }
                    WorkerParameters workerParameters = new WorkerParameters(UUID.fromString(this.f2360b), a2, this.o, this.f2362d, this.f2363e.f2463k, this.f2366h.a(), this.f2367i, this.f2366h.h(), new androidx.work.impl.utils.k(this.f2369k, this.f2367i), new androidx.work.impl.utils.j(this.f2368j, this.f2367i));
                    if (this.f2364f == null) {
                        this.f2364f = this.f2366h.h().b(this.f2359a, this.f2363e.f2455c, workerParameters);
                    }
                    ListenableWorker listenableWorker = this.f2364f;
                    if (listenableWorker == null) {
                        k.a().b(t, String.format("Could not create Worker %s", this.f2363e.f2455c), new Throwable[0]);
                        d();
                    } else if (listenableWorker.g()) {
                        k.a().b(t, String.format("Received an already-used Worker %s; WorkerFactory should return new instances", this.f2363e.f2455c), new Throwable[0]);
                        d();
                    } else {
                        this.f2364f.i();
                        if (!k()) {
                            g();
                        } else if (!j()) {
                            androidx.work.impl.utils.m.c d2 = androidx.work.impl.utils.m.c.d();
                            this.f2367i.a().execute(new a(d2));
                            d2.addListener(new b(d2, this.p), this.f2367i.b());
                        }
                    }
                }
            } finally {
                this.f2369k.e();
            }
        }
    }

    private void i() {
        this.f2369k.c();
        try {
            this.l.a(r.SUCCEEDED, this.f2360b);
            this.l.a(this.f2360b, ((ListenableWorker.a.c) this.f2365g).d());
            long currentTimeMillis = System.currentTimeMillis();
            for (String next : this.m.a(this.f2360b)) {
                if (this.l.d(next) == r.BLOCKED && this.m.b(next)) {
                    k.a().c(t, String.format("Setting status to enqueued for %s", next), new Throwable[0]);
                    this.l.a(r.ENQUEUED, next);
                    this.l.b(next, currentTimeMillis);
                }
            }
            this.f2369k.k();
        } finally {
            this.f2369k.e();
            a(false);
        }
    }

    private boolean j() {
        if (!this.s) {
            return false;
        }
        k.a().a(t, String.format("Work interrupted for %s", this.p), new Throwable[0]);
        r d2 = this.l.d(this.f2360b);
        if (d2 == null) {
            a(false);
        } else {
            a(!d2.a());
        }
        return true;
    }

    private boolean k() {
        this.f2369k.c();
        try {
            boolean z = true;
            if (this.l.d(this.f2360b) == r.ENQUEUED) {
                this.l.a(r.RUNNING, this.f2360b);
                this.l.i(this.f2360b);
            } else {
                z = false;
            }
            this.f2369k.k();
            return z;
        } finally {
            this.f2369k.e();
        }
    }

    public ListenableFuture<Boolean> a() {
        return this.q;
    }

    public void b() {
        boolean z;
        this.s = true;
        j();
        ListenableFuture<ListenableWorker.a> listenableFuture = this.r;
        if (listenableFuture != null) {
            z = listenableFuture.isDone();
            this.r.cancel(true);
        } else {
            z = false;
        }
        ListenableWorker listenableWorker = this.f2364f;
        if (listenableWorker == null || z) {
            k.a().a(t, String.format("WorkSpec %s is already done. Not interrupting.", this.f2363e), new Throwable[0]);
            return;
        }
        listenableWorker.k();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        boolean z = false;
        if (!j()) {
            this.f2369k.c();
            try {
                r d2 = this.l.d(this.f2360b);
                this.f2369k.p().a(this.f2360b);
                if (d2 == null) {
                    a(false);
                    z = true;
                } else if (d2 == r.RUNNING) {
                    a(this.f2365g);
                    z = this.l.d(this.f2360b).a();
                } else if (!d2.a()) {
                    e();
                }
                this.f2369k.k();
            } finally {
                this.f2369k.e();
            }
        }
        List<d> list = this.f2361c;
        if (list != null) {
            if (z) {
                for (d a2 : list) {
                    a2.a(this.f2360b);
                }
            }
            e.a(this.f2366h, this.f2369k, this.f2361c);
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.f2369k.c();
        try {
            a(this.f2360b);
            this.l.a(this.f2360b, ((ListenableWorker.a.C0027a) this.f2365g).d());
            this.f2369k.k();
        } finally {
            this.f2369k.e();
            a(false);
        }
    }

    public void run() {
        this.o = this.n.a(this.f2360b);
        this.p = a(this.o);
        h();
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001e A[Catch:{ all -> 0x005b }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0037 A[Catch:{ all -> 0x005b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r5) {
        /*
            r4 = this;
            androidx.work.impl.WorkDatabase r0 = r4.f2369k
            r0.c()
            androidx.work.impl.WorkDatabase r0 = r4.f2369k     // Catch:{ all -> 0x005b }
            androidx.work.impl.m.q r0 = r0.q()     // Catch:{ all -> 0x005b }
            java.util.List r0 = r0.c()     // Catch:{ all -> 0x005b }
            r1 = 0
            if (r0 == 0) goto L_0x001b
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x005b }
            if (r0 == 0) goto L_0x0019
            goto L_0x001b
        L_0x0019:
            r0 = 0
            goto L_0x001c
        L_0x001b:
            r0 = 1
        L_0x001c:
            if (r0 == 0) goto L_0x0025
            android.content.Context r0 = r4.f2359a     // Catch:{ all -> 0x005b }
            java.lang.Class<androidx.work.impl.background.systemalarm.RescheduleReceiver> r2 = androidx.work.impl.background.systemalarm.RescheduleReceiver.class
            androidx.work.impl.utils.d.a(r0, r2, r1)     // Catch:{ all -> 0x005b }
        L_0x0025:
            androidx.work.impl.m.p r0 = r4.f2363e     // Catch:{ all -> 0x005b }
            if (r0 == 0) goto L_0x0047
            androidx.work.ListenableWorker r0 = r4.f2364f     // Catch:{ all -> 0x005b }
            if (r0 == 0) goto L_0x0047
            androidx.work.ListenableWorker r0 = r4.f2364f     // Catch:{ all -> 0x005b }
            boolean r0 = r0.f()     // Catch:{ all -> 0x005b }
            if (r0 == 0) goto L_0x0047
            if (r5 == 0) goto L_0x0040
            androidx.work.impl.m.q r0 = r4.l     // Catch:{ all -> 0x005b }
            java.lang.String r1 = r4.f2360b     // Catch:{ all -> 0x005b }
            r2 = -1
            r0.a(r1, r2)     // Catch:{ all -> 0x005b }
        L_0x0040:
            androidx.work.impl.foreground.a r0 = r4.f2368j     // Catch:{ all -> 0x005b }
            java.lang.String r1 = r4.f2360b     // Catch:{ all -> 0x005b }
            r0.a(r1)     // Catch:{ all -> 0x005b }
        L_0x0047:
            androidx.work.impl.WorkDatabase r0 = r4.f2369k     // Catch:{ all -> 0x005b }
            r0.k()     // Catch:{ all -> 0x005b }
            androidx.work.impl.WorkDatabase r0 = r4.f2369k
            r0.e()
            androidx.work.impl.utils.m.c<java.lang.Boolean> r0 = r4.q
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r0.a(r5)
            return
        L_0x005b:
            r5 = move-exception
            androidx.work.impl.WorkDatabase r0 = r4.f2369k
            r0.e()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.work.impl.j.a(boolean):void");
    }

    private void a(ListenableWorker.a aVar) {
        if (aVar instanceof ListenableWorker.a.c) {
            k.a().c(t, String.format("Worker result SUCCESS for %s", this.p), new Throwable[0]);
            if (this.f2363e.d()) {
                f();
            } else {
                i();
            }
        } else if (aVar instanceof ListenableWorker.a.b) {
            k.a().c(t, String.format("Worker result RETRY for %s", this.p), new Throwable[0]);
            e();
        } else {
            k.a().c(t, String.format("Worker result FAILURE for %s", this.p), new Throwable[0]);
            if (this.f2363e.d()) {
                f();
            } else {
                d();
            }
        }
    }

    private void a(String str) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            if (this.l.d(str2) != r.CANCELLED) {
                this.l.a(r.FAILED, str2);
            }
            linkedList.addAll(this.m.a(str2));
        }
    }

    private String a(List<String> list) {
        StringBuilder sb = new StringBuilder("Work [ id=");
        sb.append(this.f2360b);
        sb.append(", tags={ ");
        boolean z = true;
        for (String next : list) {
            if (z) {
                z = false;
            } else {
                sb.append(", ");
            }
            sb.append(next);
        }
        sb.append(" } ]");
        return sb.toString();
    }
}
