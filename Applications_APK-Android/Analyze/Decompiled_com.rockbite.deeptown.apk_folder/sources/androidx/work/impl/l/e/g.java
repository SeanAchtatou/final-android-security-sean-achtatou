package androidx.work.impl.l.e;

import android.content.Context;
import androidx.work.impl.l.b;
import androidx.work.impl.m.p;
import androidx.work.impl.utils.n.a;
import androidx.work.l;

/* compiled from: NetworkUnmeteredController */
public class g extends c<b> {
    public g(Context context, a aVar) {
        super(androidx.work.impl.l.f.g.a(context, aVar).c());
    }

    /* access modifiers changed from: package-private */
    public boolean a(p pVar) {
        return pVar.f2462j.b() == l.UNMETERED;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean b(b bVar) {
        return !bVar.a() || bVar.b();
    }
}
