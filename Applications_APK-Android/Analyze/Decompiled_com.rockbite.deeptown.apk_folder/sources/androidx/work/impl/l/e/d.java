package androidx.work.impl.l.e;

import android.content.Context;
import android.os.Build;
import androidx.work.impl.l.b;
import androidx.work.impl.l.f.g;
import androidx.work.impl.m.p;
import androidx.work.impl.utils.n.a;
import androidx.work.l;

/* compiled from: NetworkConnectedController */
public class d extends c<b> {
    public d(Context context, a aVar) {
        super(g.a(context, aVar).c());
    }

    /* access modifiers changed from: package-private */
    public boolean a(p pVar) {
        return pVar.f2462j.b() == l.CONNECTED;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean b(b bVar) {
        if (Build.VERSION.SDK_INT < 26) {
            return !bVar.a();
        }
        if (!bVar.a() || !bVar.d()) {
            return true;
        }
        return false;
    }
}
