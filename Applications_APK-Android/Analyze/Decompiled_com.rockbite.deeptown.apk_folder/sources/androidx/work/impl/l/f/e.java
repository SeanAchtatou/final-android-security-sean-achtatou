package androidx.work.impl.l.f;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import androidx.work.k;

/* compiled from: NetworkStateTracker */
public class e extends d<androidx.work.impl.l.b> {

    /* renamed from: j  reason: collision with root package name */
    static final String f2419j = k.a("NetworkStateTracker");

    /* renamed from: g  reason: collision with root package name */
    private final ConnectivityManager f2420g = ((ConnectivityManager) this.f2413b.getSystemService("connectivity"));

    /* renamed from: h  reason: collision with root package name */
    private b f2421h;

    /* renamed from: i  reason: collision with root package name */
    private a f2422i;

    /* compiled from: NetworkStateTracker */
    private class a extends BroadcastReceiver {
        a() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null && intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                k.a().a(e.f2419j, "Network broadcast received", new Throwable[0]);
                e eVar = e.this;
                eVar.a(eVar.d());
            }
        }
    }

    /* compiled from: NetworkStateTracker */
    private class b extends ConnectivityManager.NetworkCallback {
        b() {
        }

        public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
            k.a().a(e.f2419j, String.format("Network capabilities changed: %s", networkCapabilities), new Throwable[0]);
            e eVar = e.this;
            eVar.a(eVar.d());
        }

        public void onLost(Network network) {
            k.a().a(e.f2419j, "Network connection lost", new Throwable[0]);
            e eVar = e.this;
            eVar.a(eVar.d());
        }
    }

    public e(Context context, androidx.work.impl.utils.n.a aVar) {
        super(context, aVar);
        if (f()) {
            this.f2421h = new b();
        } else {
            this.f2422i = new a();
        }
    }

    private boolean e() {
        NetworkCapabilities networkCapabilities;
        if (Build.VERSION.SDK_INT >= 23 && (networkCapabilities = this.f2420g.getNetworkCapabilities(this.f2420g.getActiveNetwork())) != null && networkCapabilities.hasCapability(16)) {
            return true;
        }
        return false;
    }

    private static boolean f() {
        return Build.VERSION.SDK_INT >= 24;
    }

    public void b() {
        if (f()) {
            try {
                k.a().a(f2419j, "Registering network callback", new Throwable[0]);
                this.f2420g.registerDefaultNetworkCallback(this.f2421h);
            } catch (IllegalArgumentException e2) {
                k.a().b(f2419j, "Received exception while unregistering network callback", e2);
            }
        } else {
            k.a().a(f2419j, "Registering broadcast receiver", new Throwable[0]);
            this.f2413b.registerReceiver(this.f2422i, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    public void c() {
        if (f()) {
            try {
                k.a().a(f2419j, "Unregistering network callback", new Throwable[0]);
                this.f2420g.unregisterNetworkCallback(this.f2421h);
            } catch (IllegalArgumentException e2) {
                k.a().b(f2419j, "Received exception while unregistering network callback", e2);
            }
        } else {
            k.a().a(f2419j, "Unregistering broadcast receiver", new Throwable[0]);
            this.f2413b.unregisterReceiver(this.f2422i);
        }
    }

    /* access modifiers changed from: package-private */
    public androidx.work.impl.l.b d() {
        NetworkInfo activeNetworkInfo = this.f2420g.getActiveNetworkInfo();
        boolean z = true;
        boolean z2 = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        boolean e2 = e();
        boolean a2 = b.e.h.a.a(this.f2420g);
        if (activeNetworkInfo == null || activeNetworkInfo.isRoaming()) {
            z = false;
        }
        return new androidx.work.impl.l.b(z2, e2, a2, z);
    }

    public androidx.work.impl.l.b a() {
        return d();
    }
}
