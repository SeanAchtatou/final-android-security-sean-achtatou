package androidx.work.impl.l.f;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.work.impl.utils.n.a;
import androidx.work.k;

/* compiled from: StorageNotLowTracker */
public class f extends c<Boolean> {

    /* renamed from: i  reason: collision with root package name */
    private static final String f2425i = k.a("StorageNotLowTracker");

    public f(Context context, a aVar) {
        super(context, aVar);
    }

    public IntentFilter d() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_OK");
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_LOW");
        return intentFilter;
    }

    public Boolean a() {
        Intent registerReceiver = this.f2413b.registerReceiver(null, d());
        if (!(registerReceiver == null || registerReceiver.getAction() == null)) {
            String action = registerReceiver.getAction();
            char c2 = 65535;
            int hashCode = action.hashCode();
            if (hashCode != -1181163412) {
                if (hashCode == -730838620 && action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                    c2 = 0;
                }
            } else if (action.equals("android.intent.action.DEVICE_STORAGE_LOW")) {
                c2 = 1;
            }
            if (c2 != 0) {
                if (c2 != 1) {
                    return null;
                }
                return false;
            }
        }
        return true;
    }

    public void a(Context context, Intent intent) {
        if (intent.getAction() != null) {
            k.a().a(f2425i, String.format("Received %s", intent.getAction()), new Throwable[0]);
            String action = intent.getAction();
            char c2 = 65535;
            int hashCode = action.hashCode();
            if (hashCode != -1181163412) {
                if (hashCode == -730838620 && action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                    c2 = 0;
                }
            } else if (action.equals("android.intent.action.DEVICE_STORAGE_LOW")) {
                c2 = 1;
            }
            if (c2 == 0) {
                a((Object) true);
            } else if (c2 == 1) {
                a((Object) false);
            }
        }
    }
}
