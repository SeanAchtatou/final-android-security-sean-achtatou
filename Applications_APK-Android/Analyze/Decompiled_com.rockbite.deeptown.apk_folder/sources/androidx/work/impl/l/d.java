package androidx.work.impl.l;

import android.content.Context;
import androidx.work.impl.l.e.b;
import androidx.work.impl.l.e.c;
import androidx.work.impl.l.e.e;
import androidx.work.impl.l.e.f;
import androidx.work.impl.l.e.g;
import androidx.work.impl.l.e.h;
import androidx.work.impl.m.p;
import androidx.work.impl.utils.n.a;
import androidx.work.k;
import java.util.ArrayList;
import java.util.List;

/* compiled from: WorkConstraintsTracker */
public class d implements c.a {

    /* renamed from: d  reason: collision with root package name */
    private static final String f2396d = k.a("WorkConstraintsTracker");

    /* renamed from: a  reason: collision with root package name */
    private final c f2397a;

    /* renamed from: b  reason: collision with root package name */
    private final c<?>[] f2398b;

    /* renamed from: c  reason: collision with root package name */
    private final Object f2399c = new Object();

    public d(Context context, a aVar, c cVar) {
        Context applicationContext = context.getApplicationContext();
        this.f2397a = cVar;
        this.f2398b = new c[]{new androidx.work.impl.l.e.a(applicationContext, aVar), new b(applicationContext, aVar), new h(applicationContext, aVar), new androidx.work.impl.l.e.d(applicationContext, aVar), new g(applicationContext, aVar), new f(applicationContext, aVar), new e(applicationContext, aVar)};
    }

    public void a(Iterable<p> iterable) {
        synchronized (this.f2399c) {
            for (c<?> a2 : this.f2398b) {
                a2.a((c.a) null);
            }
            for (c<?> a3 : this.f2398b) {
                a3.a(iterable);
            }
            for (c<?> a4 : this.f2398b) {
                a4.a((c.a) this);
            }
        }
    }

    public void b(List<String> list) {
        synchronized (this.f2399c) {
            if (this.f2397a != null) {
                this.f2397a.a(list);
            }
        }
    }

    public void a() {
        synchronized (this.f2399c) {
            for (c<?> a2 : this.f2398b) {
                a2.a();
            }
        }
    }

    public boolean a(String str) {
        synchronized (this.f2399c) {
            for (c<?> cVar : this.f2398b) {
                if (cVar.a(str)) {
                    k.a().a(f2396d, String.format("Work %s constrained by %s", str, cVar.getClass().getSimpleName()), new Throwable[0]);
                    return false;
                }
            }
            return true;
        }
    }

    public void a(List<String> list) {
        synchronized (this.f2399c) {
            ArrayList arrayList = new ArrayList();
            for (String next : list) {
                if (a(next)) {
                    k.a().a(f2396d, String.format("Constraints met for %s", next), new Throwable[0]);
                    arrayList.add(next);
                }
            }
            if (this.f2397a != null) {
                this.f2397a.b(arrayList);
            }
        }
    }
}
