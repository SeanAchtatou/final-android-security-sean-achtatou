package androidx.work.impl.l.e;

import android.content.Context;
import androidx.work.impl.l.f.g;
import androidx.work.impl.m.p;
import androidx.work.impl.utils.n.a;

/* compiled from: BatteryNotLowController */
public class b extends c<Boolean> {
    public b(Context context, a aVar) {
        super(g.a(context, aVar).b());
    }

    /* access modifiers changed from: package-private */
    public boolean a(p pVar) {
        return pVar.f2462j.f();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean b(Boolean bool) {
        return !bool.booleanValue();
    }
}
