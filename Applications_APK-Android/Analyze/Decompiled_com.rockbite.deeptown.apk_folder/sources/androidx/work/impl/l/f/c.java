package androidx.work.impl.l.f;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.work.k;

/* compiled from: BroadcastReceiverConstraintTracker */
public abstract class c<T> extends d<T> {

    /* renamed from: h  reason: collision with root package name */
    private static final String f2408h = k.a("BrdcstRcvrCnstrntTrckr");

    /* renamed from: g  reason: collision with root package name */
    private final BroadcastReceiver f2409g = new a();

    /* compiled from: BroadcastReceiverConstraintTracker */
    class a extends BroadcastReceiver {
        a() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                c.this.a(context, intent);
            }
        }
    }

    public c(Context context, androidx.work.impl.utils.n.a aVar) {
        super(context, aVar);
    }

    public abstract void a(Context context, Intent intent);

    public void b() {
        k.a().a(f2408h, String.format("%s: registering receiver", getClass().getSimpleName()), new Throwable[0]);
        this.f2413b.registerReceiver(this.f2409g, d());
    }

    public void c() {
        k.a().a(f2408h, String.format("%s: unregistering receiver", getClass().getSimpleName()), new Throwable[0]);
        this.f2413b.unregisterReceiver(this.f2409g);
    }

    public abstract IntentFilter d();
}
