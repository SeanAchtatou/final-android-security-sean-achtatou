package androidx.work.impl.l.f;

import android.content.Context;
import androidx.work.impl.utils.n.a;

/* compiled from: Trackers */
public class g {

    /* renamed from: e  reason: collision with root package name */
    private static g f2426e;

    /* renamed from: a  reason: collision with root package name */
    private a f2427a;

    /* renamed from: b  reason: collision with root package name */
    private b f2428b;

    /* renamed from: c  reason: collision with root package name */
    private e f2429c;

    /* renamed from: d  reason: collision with root package name */
    private f f2430d;

    private g(Context context, a aVar) {
        Context applicationContext = context.getApplicationContext();
        this.f2427a = new a(applicationContext, aVar);
        this.f2428b = new b(applicationContext, aVar);
        this.f2429c = new e(applicationContext, aVar);
        this.f2430d = new f(applicationContext, aVar);
    }

    public static synchronized g a(Context context, a aVar) {
        g gVar;
        synchronized (g.class) {
            if (f2426e == null) {
                f2426e = new g(context, aVar);
            }
            gVar = f2426e;
        }
        return gVar;
    }

    public b b() {
        return this.f2428b;
    }

    public e c() {
        return this.f2429c;
    }

    public f d() {
        return this.f2430d;
    }

    public a a() {
        return this.f2427a;
    }
}
