package androidx.work.impl.l.e;

import androidx.work.impl.l.f.d;
import androidx.work.impl.m.p;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ConstraintController */
public abstract class c<T> implements androidx.work.impl.l.a<T> {

    /* renamed from: a  reason: collision with root package name */
    private final List<String> f2400a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private T f2401b;

    /* renamed from: c  reason: collision with root package name */
    private d<T> f2402c;

    /* renamed from: d  reason: collision with root package name */
    private a f2403d;

    /* compiled from: ConstraintController */
    public interface a {
        void a(List<String> list);

        void b(List<String> list);
    }

    c(d<T> dVar) {
        this.f2402c = dVar;
    }

    private void b() {
        if (!this.f2400a.isEmpty() && this.f2403d != null) {
            T t = this.f2401b;
            if (t == null || b(t)) {
                this.f2403d.b(this.f2400a);
            } else {
                this.f2403d.a(this.f2400a);
            }
        }
    }

    public void a(a aVar) {
        if (this.f2403d != aVar) {
            this.f2403d = aVar;
            b();
        }
    }

    /* access modifiers changed from: package-private */
    public abstract boolean a(p pVar);

    /* access modifiers changed from: package-private */
    public abstract boolean b(T t);

    public void a(Iterable<p> iterable) {
        this.f2400a.clear();
        for (p next : iterable) {
            if (a(next)) {
                this.f2400a.add(next.f2453a);
            }
        }
        if (this.f2400a.isEmpty()) {
            this.f2402c.b(this);
        } else {
            this.f2402c.a((androidx.work.impl.l.a) this);
        }
        b();
    }

    public void a() {
        if (!this.f2400a.isEmpty()) {
            this.f2400a.clear();
            this.f2402c.b(this);
        }
    }

    public boolean a(String str) {
        T t = this.f2401b;
        return t != null && b(t) && this.f2400a.contains(str);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [T, ?] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.Object r1) {
        /*
            r0 = this;
            r0.f2401b = r1
            r0.b()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.work.impl.l.e.c.a(java.lang.Object):void");
    }
}
