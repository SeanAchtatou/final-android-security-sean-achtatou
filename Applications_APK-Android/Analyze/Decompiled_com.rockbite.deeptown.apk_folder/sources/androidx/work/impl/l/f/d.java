package androidx.work.impl.l.f;

import android.content.Context;
import androidx.work.k;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/* compiled from: ConstraintTracker */
public abstract class d<T> {

    /* renamed from: f  reason: collision with root package name */
    private static final String f2411f = k.a("ConstraintTracker");

    /* renamed from: a  reason: collision with root package name */
    protected final androidx.work.impl.utils.n.a f2412a;

    /* renamed from: b  reason: collision with root package name */
    protected final Context f2413b;

    /* renamed from: c  reason: collision with root package name */
    private final Object f2414c = new Object();

    /* renamed from: d  reason: collision with root package name */
    private final Set<androidx.work.impl.l.a<T>> f2415d = new LinkedHashSet();

    /* renamed from: e  reason: collision with root package name */
    T f2416e;

    /* compiled from: ConstraintTracker */
    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ List f2417a;

        a(List list) {
            this.f2417a = list;
        }

        public void run() {
            for (androidx.work.impl.l.a a2 : this.f2417a) {
                a2.a(d.this.f2416e);
            }
        }
    }

    d(Context context, androidx.work.impl.utils.n.a aVar) {
        this.f2413b = context.getApplicationContext();
        this.f2412a = aVar;
    }

    public abstract T a();

    public void a(androidx.work.impl.l.a aVar) {
        synchronized (this.f2414c) {
            if (this.f2415d.add(aVar)) {
                if (this.f2415d.size() == 1) {
                    this.f2416e = a();
                    k.a().a(f2411f, String.format("%s: initial state = %s", getClass().getSimpleName(), this.f2416e), new Throwable[0]);
                    b();
                }
                aVar.a(this.f2416e);
            }
        }
    }

    public abstract void b();

    public void b(androidx.work.impl.l.a<T> aVar) {
        synchronized (this.f2414c) {
            if (this.f2415d.remove(aVar) && this.f2415d.isEmpty()) {
                c();
            }
        }
    }

    public abstract void c();

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void a(java.lang.Object r4) {
        /*
            r3 = this;
            java.lang.Object r0 = r3.f2414c
            monitor-enter(r0)
            T r1 = r3.f2416e     // Catch:{ all -> 0x002f }
            if (r1 == r4) goto L_0x002d
            T r1 = r3.f2416e     // Catch:{ all -> 0x002f }
            if (r1 == 0) goto L_0x0014
            T r1 = r3.f2416e     // Catch:{ all -> 0x002f }
            boolean r1 = r1.equals(r4)     // Catch:{ all -> 0x002f }
            if (r1 == 0) goto L_0x0014
            goto L_0x002d
        L_0x0014:
            r3.f2416e = r4     // Catch:{ all -> 0x002f }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x002f }
            java.util.Set<androidx.work.impl.l.a<T>> r1 = r3.f2415d     // Catch:{ all -> 0x002f }
            r4.<init>(r1)     // Catch:{ all -> 0x002f }
            androidx.work.impl.utils.n.a r1 = r3.f2412a     // Catch:{ all -> 0x002f }
            java.util.concurrent.Executor r1 = r1.a()     // Catch:{ all -> 0x002f }
            androidx.work.impl.l.f.d$a r2 = new androidx.work.impl.l.f.d$a     // Catch:{ all -> 0x002f }
            r2.<init>(r4)     // Catch:{ all -> 0x002f }
            r1.execute(r2)     // Catch:{ all -> 0x002f }
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            return
        L_0x002d:
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            return
        L_0x002f:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.work.impl.l.f.d.a(java.lang.Object):void");
    }
}
