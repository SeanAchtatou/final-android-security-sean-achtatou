package androidx.work.impl.l;

import com.google.protobuf.CodedOutputStream;

/* compiled from: NetworkState */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2392a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f2393b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f2394c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f2395d;

    public b(boolean z, boolean z2, boolean z3, boolean z4) {
        this.f2392a = z;
        this.f2393b = z2;
        this.f2394c = z3;
        this.f2395d = z4;
    }

    public boolean a() {
        return this.f2392a;
    }

    public boolean b() {
        return this.f2394c;
    }

    public boolean c() {
        return this.f2395d;
    }

    public boolean d() {
        return this.f2393b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (this.f2392a == bVar.f2392a && this.f2393b == bVar.f2393b && this.f2394c == bVar.f2394c && this.f2395d == bVar.f2395d) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i2 = this.f2392a ? 1 : 0;
        if (this.f2393b) {
            i2 += 16;
        }
        if (this.f2394c) {
            i2 += 256;
        }
        return this.f2395d ? i2 + CodedOutputStream.DEFAULT_BUFFER_SIZE : i2;
    }

    public String toString() {
        return String.format("[ Connected=%b Validated=%b Metered=%b NotRoaming=%b ]", Boolean.valueOf(this.f2392a), Boolean.valueOf(this.f2393b), Boolean.valueOf(this.f2394c), Boolean.valueOf(this.f2395d));
    }
}
