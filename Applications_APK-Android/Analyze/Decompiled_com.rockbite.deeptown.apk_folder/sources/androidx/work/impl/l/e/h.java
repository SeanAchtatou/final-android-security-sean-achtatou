package androidx.work.impl.l.e;

import android.content.Context;
import androidx.work.impl.l.f.g;
import androidx.work.impl.m.p;
import androidx.work.impl.utils.n.a;

/* compiled from: StorageNotLowController */
public class h extends c<Boolean> {
    public h(Context context, a aVar) {
        super(g.a(context, aVar).d());
    }

    /* access modifiers changed from: package-private */
    public boolean a(p pVar) {
        return pVar.f2462j.i();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean b(Boolean bool) {
        return !bool.booleanValue();
    }
}
