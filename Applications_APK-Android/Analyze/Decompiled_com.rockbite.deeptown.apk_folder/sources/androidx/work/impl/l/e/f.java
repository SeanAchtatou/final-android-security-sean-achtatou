package androidx.work.impl.l.e;

import android.content.Context;
import android.os.Build;
import androidx.work.impl.l.b;
import androidx.work.impl.l.f.g;
import androidx.work.impl.m.p;
import androidx.work.impl.utils.n.a;
import androidx.work.k;
import androidx.work.l;

/* compiled from: NetworkNotRoamingController */
public class f extends c<b> {

    /* renamed from: e  reason: collision with root package name */
    private static final String f2405e = k.a("NetworkNotRoamingCtrlr");

    public f(Context context, a aVar) {
        super(g.a(context, aVar).c());
    }

    /* access modifiers changed from: package-private */
    public boolean a(p pVar) {
        return pVar.f2462j.b() == l.NOT_ROAMING;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean b(b bVar) {
        if (Build.VERSION.SDK_INT < 24) {
            k.a().a(f2405e, "Not-roaming network constraint is not supported before API 24, only checking for connected state.", new Throwable[0]);
            return !bVar.a();
        } else if (!bVar.a() || !bVar.c()) {
            return true;
        } else {
            return false;
        }
    }
}
