package androidx.work.impl.l.e;

import android.content.Context;
import androidx.work.impl.l.f.g;
import androidx.work.impl.m.p;

/* compiled from: BatteryChargingController */
public class a extends c<Boolean> {
    public a(Context context, androidx.work.impl.utils.n.a aVar) {
        super(g.a(context, aVar).a());
    }

    /* access modifiers changed from: package-private */
    public boolean a(p pVar) {
        return pVar.f2462j.g();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean b(Boolean bool) {
        return !bool.booleanValue();
    }
}
