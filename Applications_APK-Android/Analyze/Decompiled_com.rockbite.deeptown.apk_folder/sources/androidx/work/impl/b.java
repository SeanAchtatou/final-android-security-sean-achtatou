package androidx.work.impl;

import androidx.work.ListenableWorker;
import androidx.work.impl.utils.m.c;
import androidx.work.n;

/* compiled from: OperationImpl */
public class b implements n {

    /* renamed from: c  reason: collision with root package name */
    private final androidx.lifecycle.n<n.b> f2225c = new androidx.lifecycle.n<>();

    /* renamed from: d  reason: collision with root package name */
    private final c<n.b.c> f2226d = c.d();

    public b() {
        a(n.f2577b);
    }

    public void a(n.b bVar) {
        this.f2225c.a(bVar);
        if (bVar instanceof n.b.c) {
            this.f2226d.a((ListenableWorker.a) ((n.b.c) bVar));
        } else if (bVar instanceof n.b.a) {
            this.f2226d.a(((n.b.a) bVar).a());
        }
    }
}
