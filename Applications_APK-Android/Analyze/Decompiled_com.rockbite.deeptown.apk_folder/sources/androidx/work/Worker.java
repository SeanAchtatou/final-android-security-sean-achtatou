package androidx.work;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.Keep;
import androidx.work.ListenableWorker;
import androidx.work.impl.utils.m.c;
import com.google.common.util.concurrent.ListenableFuture;

public abstract class Worker extends ListenableWorker {

    /* renamed from: e  reason: collision with root package name */
    c<ListenableWorker.a> f2164e;

    class a implements Runnable {
        a() {
        }

        public void run() {
            try {
                Worker.this.f2164e.a(Worker.this.l());
            } catch (Throwable th) {
                Worker.this.f2164e.a(th);
            }
        }
    }

    @SuppressLint({"BanKeepAnnotation"})
    @Keep
    public Worker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
    }

    public final ListenableFuture<ListenableWorker.a> j() {
        this.f2164e = c.d();
        b().execute(new a());
        return this.f2164e;
    }

    public abstract ListenableWorker.a l();
}
