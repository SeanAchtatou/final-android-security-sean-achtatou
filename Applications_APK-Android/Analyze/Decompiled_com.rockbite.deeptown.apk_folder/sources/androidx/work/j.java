package androidx.work;

/* compiled from: InputMergerFactory */
public abstract class j {

    /* compiled from: InputMergerFactory */
    static class a extends j {
        a() {
        }

        public i a(String str) {
            return null;
        }
    }

    public static j a() {
        return new a();
    }

    public abstract i a(String str);

    public final i b(String str) {
        i a2 = a(str);
        return a2 == null ? i.a(str) : a2;
    }
}
