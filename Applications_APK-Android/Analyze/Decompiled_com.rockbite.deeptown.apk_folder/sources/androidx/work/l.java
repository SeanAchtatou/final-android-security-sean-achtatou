package androidx.work;

/* compiled from: NetworkType */
public enum l {
    NOT_REQUIRED,
    CONNECTED,
    UNMETERED,
    NOT_ROAMING,
    METERED
}
