package androidx.work;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.Keep;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.UUID;
import java.util.concurrent.Executor;

public abstract class ListenableWorker {

    /* renamed from: a  reason: collision with root package name */
    private Context f2158a;

    /* renamed from: b  reason: collision with root package name */
    private WorkerParameters f2159b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f2160c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f2161d;

    public static abstract class a {

        /* renamed from: androidx.work.ListenableWorker$a$a  reason: collision with other inner class name */
        public static final class C0027a extends a {

            /* renamed from: a  reason: collision with root package name */
            private final e f2162a;

            public C0027a() {
                this(e.f2210c);
            }

            public e d() {
                return this.f2162a;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null || C0027a.class != obj.getClass()) {
                    return false;
                }
                return this.f2162a.equals(((C0027a) obj).f2162a);
            }

            public int hashCode() {
                return (C0027a.class.getName().hashCode() * 31) + this.f2162a.hashCode();
            }

            public String toString() {
                return "Failure {mOutputData=" + this.f2162a + '}';
            }

            public C0027a(e eVar) {
                this.f2162a = eVar;
            }
        }

        public static final class b extends a {
            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return obj != null && b.class == obj.getClass();
            }

            public int hashCode() {
                return b.class.getName().hashCode();
            }

            public String toString() {
                return "Retry";
            }
        }

        public static final class c extends a {

            /* renamed from: a  reason: collision with root package name */
            private final e f2163a;

            public c() {
                this(e.f2210c);
            }

            public e d() {
                return this.f2163a;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null || c.class != obj.getClass()) {
                    return false;
                }
                return this.f2163a.equals(((c) obj).f2163a);
            }

            public int hashCode() {
                return (c.class.getName().hashCode() * 31) + this.f2163a.hashCode();
            }

            public String toString() {
                return "Success {mOutputData=" + this.f2163a + '}';
            }

            public c(e eVar) {
                this.f2163a = eVar;
            }
        }

        a() {
        }

        public static a a(e eVar) {
            return new c(eVar);
        }

        public static a b() {
            return new b();
        }

        public static a c() {
            return new c();
        }

        public static a a() {
            return new C0027a();
        }
    }

    @SuppressLint({"BanKeepAnnotation"})
    @Keep
    public ListenableWorker(Context context, WorkerParameters workerParameters) {
        if (context == null) {
            throw new IllegalArgumentException("Application Context is null");
        } else if (workerParameters != null) {
            this.f2158a = context;
            this.f2159b = workerParameters;
        } else {
            throw new IllegalArgumentException("WorkerParameters is null");
        }
    }

    public final Context a() {
        return this.f2158a;
    }

    public Executor b() {
        return this.f2159b.a();
    }

    public final UUID c() {
        return this.f2159b.b();
    }

    public final e d() {
        return this.f2159b.c();
    }

    public u e() {
        return this.f2159b.d();
    }

    public final boolean f() {
        return this.f2161d;
    }

    public final boolean g() {
        return this.f2160c;
    }

    public void h() {
    }

    public final void i() {
        this.f2160c = true;
    }

    public abstract ListenableFuture<a> j();

    public final void k() {
        h();
    }
}
