package androidx.work;

import android.os.Build;
import androidx.work.impl.m.p;
import androidx.work.t;

/* compiled from: OneTimeWorkRequest */
public final class m extends t {

    /* compiled from: OneTimeWorkRequest */
    public static final class a extends t.a<a, m> {
        public a(Class<? extends ListenableWorker> cls) {
            super(cls);
            this.f2591c.f2456d = OverwritingInputMerger.class.getName();
        }

        /* access modifiers changed from: package-private */
        public a c() {
            return this;
        }

        /* access modifiers changed from: package-private */
        public m b() {
            if (!this.f2589a || Build.VERSION.SDK_INT < 23 || !this.f2591c.f2462j.h()) {
                p pVar = this.f2591c;
                if (!pVar.q || Build.VERSION.SDK_INT < 23 || !pVar.f2462j.h()) {
                    return new m(this);
                }
                throw new IllegalArgumentException("Cannot run in foreground with an idle mode constraint");
            }
            throw new IllegalArgumentException("Cannot set backoff criteria on an idle mode job");
        }
    }

    m(a aVar) {
        super(aVar.f2590b, aVar.f2591c, aVar.f2592d);
    }
}
