package androidx.work;

import androidx.work.impl.m.p;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/* compiled from: WorkRequest */
public abstract class t {

    /* renamed from: a  reason: collision with root package name */
    private UUID f2586a;

    /* renamed from: b  reason: collision with root package name */
    private p f2587b;

    /* renamed from: c  reason: collision with root package name */
    private Set<String> f2588c;

    protected t(UUID uuid, p pVar, Set<String> set) {
        this.f2586a = uuid;
        this.f2587b = pVar;
        this.f2588c = set;
    }

    public String a() {
        return this.f2586a.toString();
    }

    public Set<String> b() {
        return this.f2588c;
    }

    public p c() {
        return this.f2587b;
    }

    /* compiled from: WorkRequest */
    public static abstract class a<B extends a<?, ?>, W extends t> {

        /* renamed from: a  reason: collision with root package name */
        boolean f2589a = false;

        /* renamed from: b  reason: collision with root package name */
        UUID f2590b = UUID.randomUUID();

        /* renamed from: c  reason: collision with root package name */
        p f2591c;

        /* renamed from: d  reason: collision with root package name */
        Set<String> f2592d = new HashSet();

        a(Class<? extends ListenableWorker> cls) {
            this.f2591c = new p(this.f2590b.toString(), cls.getName());
            a(cls.getName());
        }

        public final B a(e eVar) {
            this.f2591c.f2457e = eVar;
            c();
            return this;
        }

        /* access modifiers changed from: package-private */
        public abstract W b();

        /* access modifiers changed from: package-private */
        public abstract B c();

        public final B a(String str) {
            this.f2592d.add(str);
            c();
            return this;
        }

        public B a(long j2, TimeUnit timeUnit) {
            this.f2591c.f2459g = timeUnit.toMillis(j2);
            c();
            return this;
        }

        public final W a() {
            W b2 = b();
            this.f2590b = UUID.randomUUID();
            this.f2591c = new p(this.f2591c);
            this.f2591c.f2453a = this.f2590b.toString();
            return b2;
        }
    }
}
