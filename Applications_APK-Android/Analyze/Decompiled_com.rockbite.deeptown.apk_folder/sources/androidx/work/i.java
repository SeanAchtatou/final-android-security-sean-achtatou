package androidx.work;

import java.util.List;

/* compiled from: InputMerger */
public abstract class i {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2220a = k.a("InputMerger");

    public static i a(String str) {
        try {
            return (i) Class.forName(str).newInstance();
        } catch (Exception e2) {
            k a2 = k.a();
            String str2 = f2220a;
            a2.b(str2, "Trouble instantiating + " + str, e2);
            return null;
        }
    }

    public abstract e a(List<e> list);
}
