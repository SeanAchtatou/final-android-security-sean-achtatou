package androidx.work;

import android.annotation.SuppressLint;

/* compiled from: Operation */
public interface n {
    @SuppressLint({"SyntheticAccessor"})

    /* renamed from: a  reason: collision with root package name */
    public static final b.c f2576a = new b.c();
    @SuppressLint({"SyntheticAccessor"})

    /* renamed from: b  reason: collision with root package name */
    public static final b.C0035b f2577b = new b.C0035b();

    /* compiled from: Operation */
    public static abstract class b {

        /* compiled from: Operation */
        public static final class a extends b {

            /* renamed from: a  reason: collision with root package name */
            private final Throwable f2578a;

            public a(Throwable th) {
                this.f2578a = th;
            }

            public Throwable a() {
                return this.f2578a;
            }

            public String toString() {
                return String.format("FAILURE (%s)", this.f2578a.getMessage());
            }
        }

        /* renamed from: androidx.work.n$b$b  reason: collision with other inner class name */
        /* compiled from: Operation */
        public static final class C0035b extends b {
            public String toString() {
                return "IN_PROGRESS";
            }

            private C0035b() {
            }
        }

        /* compiled from: Operation */
        public static final class c extends b {
            public String toString() {
                return "SUCCESS";
            }

            private c() {
            }
        }

        b() {
        }
    }
}
