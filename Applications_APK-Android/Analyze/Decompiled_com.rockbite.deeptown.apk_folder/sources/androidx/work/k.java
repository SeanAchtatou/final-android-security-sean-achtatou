package androidx.work;

import android.util.Log;

/* compiled from: Logger */
public abstract class k {

    /* renamed from: a  reason: collision with root package name */
    private static k f2567a = null;

    /* renamed from: b  reason: collision with root package name */
    private static final int f2568b = 20;

    /* compiled from: Logger */
    public static class a extends k {

        /* renamed from: c  reason: collision with root package name */
        private int f2569c;

        public a(int i2) {
            super(i2);
            this.f2569c = i2;
        }

        public void a(String str, String str2, Throwable... thArr) {
            if (this.f2569c > 3) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.d(str, str2);
            } else {
                Log.d(str, str2, thArr[0]);
            }
        }

        public void b(String str, String str2, Throwable... thArr) {
            if (this.f2569c > 6) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.e(str, str2);
            } else {
                Log.e(str, str2, thArr[0]);
            }
        }

        public void c(String str, String str2, Throwable... thArr) {
            if (this.f2569c > 4) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.i(str, str2);
            } else {
                Log.i(str, str2, thArr[0]);
            }
        }

        public void d(String str, String str2, Throwable... thArr) {
            if (this.f2569c > 2) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.v(str, str2);
            } else {
                Log.v(str, str2, thArr[0]);
            }
        }

        public void e(String str, String str2, Throwable... thArr) {
            if (this.f2569c > 5) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.w(str, str2);
            } else {
                Log.w(str, str2, thArr[0]);
            }
        }
    }

    public k(int i2) {
    }

    public static synchronized void a(k kVar) {
        synchronized (k.class) {
            f2567a = kVar;
        }
    }

    public abstract void a(String str, String str2, Throwable... thArr);

    public abstract void b(String str, String str2, Throwable... thArr);

    public abstract void c(String str, String str2, Throwable... thArr);

    public abstract void d(String str, String str2, Throwable... thArr);

    public abstract void e(String str, String str2, Throwable... thArr);

    public static String a(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder(23);
        sb.append("WM-");
        int i2 = f2568b;
        if (length >= i2) {
            sb.append(str.substring(0, i2));
        } else {
            sb.append(str);
        }
        return sb.toString();
    }

    public static synchronized k a() {
        k kVar;
        synchronized (k.class) {
            if (f2567a == null) {
                f2567a = new a(3);
            }
            kVar = f2567a;
        }
        return kVar;
    }
}
