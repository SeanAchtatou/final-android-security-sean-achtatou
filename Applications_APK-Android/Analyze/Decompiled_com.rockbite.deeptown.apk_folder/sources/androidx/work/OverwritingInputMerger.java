package androidx.work;

import androidx.work.e;
import java.util.HashMap;
import java.util.List;

public final class OverwritingInputMerger extends i {
    public e a(List<e> list) {
        e.a aVar = new e.a();
        HashMap hashMap = new HashMap();
        for (e a2 : list) {
            hashMap.putAll(a2.a());
        }
        aVar.a(hashMap);
        return aVar.a();
    }
}
