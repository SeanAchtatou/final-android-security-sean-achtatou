package androidx.work;

import android.net.Uri;
import java.util.HashSet;
import java.util.Set;

/* compiled from: ContentUriTriggers */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final Set<a> f2206a = new HashSet();

    /* compiled from: ContentUriTriggers */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        private final Uri f2207a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f2208b;

        a(Uri uri, boolean z) {
            this.f2207a = uri;
            this.f2208b = z;
        }

        public Uri a() {
            return this.f2207a;
        }

        public boolean b() {
            return this.f2208b;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (this.f2208b != aVar.f2208b || !this.f2207a.equals(aVar.f2207a)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (this.f2207a.hashCode() * 31) + (this.f2208b ? 1 : 0);
        }
    }

    public void a(Uri uri, boolean z) {
        this.f2206a.add(new a(uri, z));
    }

    public int b() {
        return this.f2206a.size();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || d.class != obj.getClass()) {
            return false;
        }
        return this.f2206a.equals(((d) obj).f2206a);
    }

    public int hashCode() {
        return this.f2206a.hashCode();
    }

    public Set<a> a() {
        return this.f2206a;
    }
}
