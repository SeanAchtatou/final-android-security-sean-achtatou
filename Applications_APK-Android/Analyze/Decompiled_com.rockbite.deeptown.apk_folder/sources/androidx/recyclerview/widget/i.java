package androidx.recyclerview.widget;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: OrientationHelper */
public abstract class i {

    /* renamed from: a  reason: collision with root package name */
    protected final RecyclerView.o f1938a;

    /* renamed from: b  reason: collision with root package name */
    private int f1939b;

    /* renamed from: c  reason: collision with root package name */
    final Rect f1940c;

    /* compiled from: OrientationHelper */
    static class a extends i {
        a(RecyclerView.o oVar) {
            super(oVar, null);
        }

        public int a() {
            return this.f1938a.q();
        }

        public int b() {
            return this.f1938a.q() - this.f1938a.o();
        }

        public int c(View view) {
            RecyclerView.p pVar = (RecyclerView.p) view.getLayoutParams();
            return this.f1938a.g(view) + pVar.topMargin + pVar.bottomMargin;
        }

        public int d(View view) {
            return this.f1938a.f(view) - ((RecyclerView.p) view.getLayoutParams()).leftMargin;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
         arg types: [android.view.View, int, android.graphics.Rect]
         candidates:
          androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, int, android.view.View):void
          androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
          androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$p):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.accessibility.AccessibilityEvent):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, b.e.m.d0.c):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, android.os.Bundle):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, boolean):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.view.View):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void */
        public int e(View view) {
            this.f1938a.a(view, true, this.f1940c);
            return this.f1940c.right;
        }

        public int f() {
            return this.f1938a.n();
        }

        public int g() {
            return (this.f1938a.q() - this.f1938a.n()) - this.f1938a.o();
        }

        public void a(int i2) {
            this.f1938a.d(i2);
        }

        public int b(View view) {
            RecyclerView.p pVar = (RecyclerView.p) view.getLayoutParams();
            return this.f1938a.h(view) + pVar.leftMargin + pVar.rightMargin;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
         arg types: [android.view.View, int, android.graphics.Rect]
         candidates:
          androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, int, android.view.View):void
          androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
          androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$p):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.accessibility.AccessibilityEvent):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, b.e.m.d0.c):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, android.os.Bundle):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, boolean):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.view.View):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void */
        public int f(View view) {
            this.f1938a.a(view, true, this.f1940c);
            return this.f1940c.left;
        }

        public int a(View view) {
            return this.f1938a.i(view) + ((RecyclerView.p) view.getLayoutParams()).rightMargin;
        }

        public int c() {
            return this.f1938a.o();
        }

        public int d() {
            return this.f1938a.r();
        }

        public int e() {
            return this.f1938a.i();
        }
    }

    /* compiled from: OrientationHelper */
    static class b extends i {
        b(RecyclerView.o oVar) {
            super(oVar, null);
        }

        public int a() {
            return this.f1938a.h();
        }

        public int b() {
            return this.f1938a.h() - this.f1938a.m();
        }

        public int c(View view) {
            RecyclerView.p pVar = (RecyclerView.p) view.getLayoutParams();
            return this.f1938a.h(view) + pVar.leftMargin + pVar.rightMargin;
        }

        public int d(View view) {
            return this.f1938a.j(view) - ((RecyclerView.p) view.getLayoutParams()).topMargin;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
         arg types: [android.view.View, int, android.graphics.Rect]
         candidates:
          androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, int, android.view.View):void
          androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
          androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$p):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.accessibility.AccessibilityEvent):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, b.e.m.d0.c):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, android.os.Bundle):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, boolean):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.view.View):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void */
        public int e(View view) {
            this.f1938a.a(view, true, this.f1940c);
            return this.f1940c.bottom;
        }

        public int f() {
            return this.f1938a.p();
        }

        public int g() {
            return (this.f1938a.h() - this.f1938a.p()) - this.f1938a.m();
        }

        public void a(int i2) {
            this.f1938a.e(i2);
        }

        public int b(View view) {
            RecyclerView.p pVar = (RecyclerView.p) view.getLayoutParams();
            return this.f1938a.g(view) + pVar.topMargin + pVar.bottomMargin;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
         arg types: [android.view.View, int, android.graphics.Rect]
         candidates:
          androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, int, android.view.View):void
          androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
          androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$p):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.accessibility.AccessibilityEvent):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, b.e.m.d0.c):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, android.os.Bundle):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, boolean):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.view.View):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void */
        public int f(View view) {
            this.f1938a.a(view, true, this.f1940c);
            return this.f1940c.top;
        }

        public int a(View view) {
            return this.f1938a.e(view) + ((RecyclerView.p) view.getLayoutParams()).bottomMargin;
        }

        public int c() {
            return this.f1938a.m();
        }

        public int d() {
            return this.f1938a.i();
        }

        public int e() {
            return this.f1938a.r();
        }
    }

    /* synthetic */ i(RecyclerView.o oVar, a aVar) {
        this(oVar);
    }

    public static i a(RecyclerView.o oVar, int i2) {
        if (i2 == 0) {
            return a(oVar);
        }
        if (i2 == 1) {
            return b(oVar);
        }
        throw new IllegalArgumentException("invalid orientation");
    }

    public static i b(RecyclerView.o oVar) {
        return new b(oVar);
    }

    public abstract int a();

    public abstract int a(View view);

    public abstract void a(int i2);

    public abstract int b();

    public abstract int b(View view);

    public abstract int c();

    public abstract int c(View view);

    public abstract int d();

    public abstract int d(View view);

    public abstract int e();

    public abstract int e(View view);

    public abstract int f();

    public abstract int f(View view);

    public abstract int g();

    public int h() {
        if (Integer.MIN_VALUE == this.f1939b) {
            return 0;
        }
        return g() - this.f1939b;
    }

    public void i() {
        this.f1939b = g();
    }

    private i(RecyclerView.o oVar) {
        this.f1939b = Integer.MIN_VALUE;
        this.f1940c = new Rect();
        this.f1938a = oVar;
    }

    public static i a(RecyclerView.o oVar) {
        return new a(oVar);
    }
}
