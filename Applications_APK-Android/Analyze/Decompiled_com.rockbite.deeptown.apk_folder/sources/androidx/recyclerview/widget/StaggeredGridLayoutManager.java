package androidx.recyclerview.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import androidx.recyclerview.widget.RecyclerView;
import b.e.m.d0.c;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.common.api.Api;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class StaggeredGridLayoutManager extends RecyclerView.o implements RecyclerView.y.a {
    boolean A = false;
    private BitSet B;
    int C = -1;
    int D = Integer.MIN_VALUE;
    LazySpanLookup E = new LazySpanLookup();
    private int F = 2;
    private boolean G;
    private boolean H;
    private SavedState I;
    private int J;
    private final Rect K = new Rect();
    private final b L = new b();
    private boolean M = false;
    private boolean N = true;
    private int[] O;
    private final Runnable P = new a();
    private int s = -1;
    d[] t;
    i u;
    i v;
    private int w;
    private int x;
    private final g y;
    boolean z = false;

    @SuppressLint({"BanParcelableUsage"})
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();

        /* renamed from: a  reason: collision with root package name */
        int f1811a;

        /* renamed from: b  reason: collision with root package name */
        int f1812b;

        /* renamed from: c  reason: collision with root package name */
        int f1813c;

        /* renamed from: d  reason: collision with root package name */
        int[] f1814d;

        /* renamed from: e  reason: collision with root package name */
        int f1815e;

        /* renamed from: f  reason: collision with root package name */
        int[] f1816f;

        /* renamed from: g  reason: collision with root package name */
        List<LazySpanLookup.FullSpanItem> f1817g;

        /* renamed from: h  reason: collision with root package name */
        boolean f1818h;

        /* renamed from: i  reason: collision with root package name */
        boolean f1819i;

        /* renamed from: j  reason: collision with root package name */
        boolean f1820j;

        static class a implements Parcelable.Creator<SavedState> {
            a() {
            }

            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] newArray(int i2) {
                return new SavedState[i2];
            }
        }

        public SavedState() {
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f1814d = null;
            this.f1813c = 0;
            this.f1815e = 0;
            this.f1816f = null;
            this.f1817g = null;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeInt(this.f1811a);
            parcel.writeInt(this.f1812b);
            parcel.writeInt(this.f1813c);
            if (this.f1813c > 0) {
                parcel.writeIntArray(this.f1814d);
            }
            parcel.writeInt(this.f1815e);
            if (this.f1815e > 0) {
                parcel.writeIntArray(this.f1816f);
            }
            parcel.writeInt(this.f1818h ? 1 : 0);
            parcel.writeInt(this.f1819i ? 1 : 0);
            parcel.writeInt(this.f1820j ? 1 : 0);
            parcel.writeList(this.f1817g);
        }

        SavedState(Parcel parcel) {
            this.f1811a = parcel.readInt();
            this.f1812b = parcel.readInt();
            this.f1813c = parcel.readInt();
            int i2 = this.f1813c;
            if (i2 > 0) {
                this.f1814d = new int[i2];
                parcel.readIntArray(this.f1814d);
            }
            this.f1815e = parcel.readInt();
            int i3 = this.f1815e;
            if (i3 > 0) {
                this.f1816f = new int[i3];
                parcel.readIntArray(this.f1816f);
            }
            boolean z = false;
            this.f1818h = parcel.readInt() == 1;
            this.f1819i = parcel.readInt() == 1;
            this.f1820j = parcel.readInt() == 1 ? true : z;
            this.f1817g = parcel.readArrayList(LazySpanLookup.FullSpanItem.class.getClassLoader());
        }

        public SavedState(SavedState savedState) {
            this.f1813c = savedState.f1813c;
            this.f1811a = savedState.f1811a;
            this.f1812b = savedState.f1812b;
            this.f1814d = savedState.f1814d;
            this.f1815e = savedState.f1815e;
            this.f1816f = savedState.f1816f;
            this.f1818h = savedState.f1818h;
            this.f1819i = savedState.f1819i;
            this.f1820j = savedState.f1820j;
            this.f1817g = savedState.f1817g;
        }
    }

    class a implements Runnable {
        a() {
        }

        public void run() {
            StaggeredGridLayoutManager.this.F();
        }
    }

    public static class c extends RecyclerView.p {

        /* renamed from: e  reason: collision with root package name */
        d f1829e;

        /* renamed from: f  reason: collision with root package name */
        boolean f1830f;

        public c(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public final int e() {
            d dVar = this.f1829e;
            if (dVar == null) {
                return -1;
            }
            return dVar.f1835e;
        }

        public boolean f() {
            return this.f1830f;
        }

        public c(int i2, int i3) {
            super(i2, i3);
        }

        public c(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public c(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    class d {

        /* renamed from: a  reason: collision with root package name */
        ArrayList<View> f1831a = new ArrayList<>();

        /* renamed from: b  reason: collision with root package name */
        int f1832b = Integer.MIN_VALUE;

        /* renamed from: c  reason: collision with root package name */
        int f1833c = Integer.MIN_VALUE;

        /* renamed from: d  reason: collision with root package name */
        int f1834d = 0;

        /* renamed from: e  reason: collision with root package name */
        final int f1835e;

        d(int i2) {
            this.f1835e = i2;
        }

        /* access modifiers changed from: package-private */
        public int a(int i2) {
            int i3 = this.f1833c;
            if (i3 != Integer.MIN_VALUE) {
                return i3;
            }
            if (this.f1831a.size() == 0) {
                return i2;
            }
            a();
            return this.f1833c;
        }

        /* access modifiers changed from: package-private */
        public int b(int i2) {
            int i3 = this.f1832b;
            if (i3 != Integer.MIN_VALUE) {
                return i3;
            }
            if (this.f1831a.size() == 0) {
                return i2;
            }
            b();
            return this.f1832b;
        }

        /* access modifiers changed from: package-private */
        public void c(View view) {
            c b2 = b(view);
            b2.f1829e = this;
            this.f1831a.add(0, view);
            this.f1832b = Integer.MIN_VALUE;
            if (this.f1831a.size() == 1) {
                this.f1833c = Integer.MIN_VALUE;
            }
            if (b2.c() || b2.b()) {
                this.f1834d += StaggeredGridLayoutManager.this.u.b(view);
            }
        }

        /* access modifiers changed from: package-private */
        public void d(int i2) {
            this.f1832b = i2;
            this.f1833c = i2;
        }

        public int e() {
            if (StaggeredGridLayoutManager.this.z) {
                return a(0, this.f1831a.size(), true);
            }
            return a(this.f1831a.size() - 1, -1, true);
        }

        public int f() {
            return this.f1834d;
        }

        /* access modifiers changed from: package-private */
        public int g() {
            int i2 = this.f1833c;
            if (i2 != Integer.MIN_VALUE) {
                return i2;
            }
            a();
            return this.f1833c;
        }

        /* access modifiers changed from: package-private */
        public int h() {
            int i2 = this.f1832b;
            if (i2 != Integer.MIN_VALUE) {
                return i2;
            }
            b();
            return this.f1832b;
        }

        /* access modifiers changed from: package-private */
        public void i() {
            this.f1832b = Integer.MIN_VALUE;
            this.f1833c = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        public void j() {
            int size = this.f1831a.size();
            View remove = this.f1831a.remove(size - 1);
            c b2 = b(remove);
            b2.f1829e = null;
            if (b2.c() || b2.b()) {
                this.f1834d -= StaggeredGridLayoutManager.this.u.b(remove);
            }
            if (size == 1) {
                this.f1832b = Integer.MIN_VALUE;
            }
            this.f1833c = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        public void k() {
            View remove = this.f1831a.remove(0);
            c b2 = b(remove);
            b2.f1829e = null;
            if (this.f1831a.size() == 0) {
                this.f1833c = Integer.MIN_VALUE;
            }
            if (b2.c() || b2.b()) {
                this.f1834d -= StaggeredGridLayoutManager.this.u.b(remove);
            }
            this.f1832b = Integer.MIN_VALUE;
        }

        public int d() {
            if (StaggeredGridLayoutManager.this.z) {
                return a(this.f1831a.size() - 1, -1, true);
            }
            return a(0, this.f1831a.size(), true);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            LazySpanLookup.FullSpanItem c2;
            ArrayList<View> arrayList = this.f1831a;
            View view = arrayList.get(arrayList.size() - 1);
            c b2 = b(view);
            this.f1833c = StaggeredGridLayoutManager.this.u.a(view);
            if (b2.f1830f && (c2 = StaggeredGridLayoutManager.this.E.c(b2.a())) != null && c2.f1808b == 1) {
                this.f1833c += c2.a(this.f1835e);
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            LazySpanLookup.FullSpanItem c2;
            View view = this.f1831a.get(0);
            c b2 = b(view);
            this.f1832b = StaggeredGridLayoutManager.this.u.d(view);
            if (b2.f1830f && (c2 = StaggeredGridLayoutManager.this.E.c(b2.a())) != null && c2.f1808b == -1) {
                this.f1832b -= c2.a(this.f1835e);
            }
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.f1831a.clear();
            i();
            this.f1834d = 0;
        }

        /* access modifiers changed from: package-private */
        public void c(int i2) {
            int i3 = this.f1832b;
            if (i3 != Integer.MIN_VALUE) {
                this.f1832b = i3 + i2;
            }
            int i4 = this.f1833c;
            if (i4 != Integer.MIN_VALUE) {
                this.f1833c = i4 + i2;
            }
        }

        /* access modifiers changed from: package-private */
        public void a(View view) {
            c b2 = b(view);
            b2.f1829e = this;
            this.f1831a.add(view);
            this.f1833c = Integer.MIN_VALUE;
            if (this.f1831a.size() == 1) {
                this.f1832b = Integer.MIN_VALUE;
            }
            if (b2.c() || b2.b()) {
                this.f1834d += StaggeredGridLayoutManager.this.u.b(view);
            }
        }

        /* access modifiers changed from: package-private */
        public c b(View view) {
            return (c) view.getLayoutParams();
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z, int i2) {
            int i3;
            if (z) {
                i3 = a(Integer.MIN_VALUE);
            } else {
                i3 = b(Integer.MIN_VALUE);
            }
            c();
            if (i3 != Integer.MIN_VALUE) {
                if (z && i3 < StaggeredGridLayoutManager.this.u.b()) {
                    return;
                }
                if (z || i3 <= StaggeredGridLayoutManager.this.u.f()) {
                    if (i2 != Integer.MIN_VALUE) {
                        i3 += i2;
                    }
                    this.f1833c = i3;
                    this.f1832b = i3;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public int a(int i2, int i3, boolean z, boolean z2, boolean z3) {
            int f2 = StaggeredGridLayoutManager.this.u.f();
            int b2 = StaggeredGridLayoutManager.this.u.b();
            int i4 = i3 > i2 ? 1 : -1;
            while (i2 != i3) {
                View view = this.f1831a.get(i2);
                int d2 = StaggeredGridLayoutManager.this.u.d(view);
                int a2 = StaggeredGridLayoutManager.this.u.a(view);
                boolean z4 = false;
                boolean z5 = !z3 ? d2 < b2 : d2 <= b2;
                if (!z3 ? a2 > f2 : a2 >= f2) {
                    z4 = true;
                }
                if (z5 && z4) {
                    if (!z || !z2) {
                        if (z2) {
                            return StaggeredGridLayoutManager.this.l(view);
                        }
                        if (d2 < f2 || a2 > b2) {
                            return StaggeredGridLayoutManager.this.l(view);
                        }
                    } else if (d2 >= f2 && a2 <= b2) {
                        return StaggeredGridLayoutManager.this.l(view);
                    }
                }
                i2 += i4;
            }
            return -1;
        }

        /* access modifiers changed from: package-private */
        public int a(int i2, int i3, boolean z) {
            return a(i2, i3, false, false, z);
        }

        public View a(int i2, int i3) {
            View view = null;
            if (i3 != -1) {
                int size = this.f1831a.size() - 1;
                while (size >= 0) {
                    View view2 = this.f1831a.get(size);
                    StaggeredGridLayoutManager staggeredGridLayoutManager = StaggeredGridLayoutManager.this;
                    if (staggeredGridLayoutManager.z && staggeredGridLayoutManager.l(view2) >= i2) {
                        break;
                    }
                    StaggeredGridLayoutManager staggeredGridLayoutManager2 = StaggeredGridLayoutManager.this;
                    if ((!staggeredGridLayoutManager2.z && staggeredGridLayoutManager2.l(view2) <= i2) || !view2.hasFocusable()) {
                        break;
                    }
                    size--;
                    view = view2;
                }
            } else {
                int size2 = this.f1831a.size();
                int i4 = 0;
                while (i4 < size2) {
                    View view3 = this.f1831a.get(i4);
                    StaggeredGridLayoutManager staggeredGridLayoutManager3 = StaggeredGridLayoutManager.this;
                    if (staggeredGridLayoutManager3.z && staggeredGridLayoutManager3.l(view3) <= i2) {
                        break;
                    }
                    StaggeredGridLayoutManager staggeredGridLayoutManager4 = StaggeredGridLayoutManager.this;
                    if ((!staggeredGridLayoutManager4.z && staggeredGridLayoutManager4.l(view3) >= i2) || !view3.hasFocusable()) {
                        break;
                    }
                    i4++;
                    view = view3;
                }
            }
            return view;
        }
    }

    public StaggeredGridLayoutManager(Context context, AttributeSet attributeSet, int i2, int i3) {
        RecyclerView.o.d a2 = RecyclerView.o.a(context, attributeSet, i2, i3);
        h(a2.f1770a);
        i(a2.f1771b);
        c(a2.f1772c);
        this.y = new g();
        M();
    }

    private void M() {
        this.u = i.a(this, this.w);
        this.v = i.a(this, 1 - this.w);
    }

    private void N() {
        if (this.v.d() != 1073741824) {
            int e2 = e();
            float f2 = Animation.CurveTimeline.LINEAR;
            for (int i2 = 0; i2 < e2; i2++) {
                View c2 = c(i2);
                float b2 = (float) this.v.b(c2);
                if (b2 >= f2) {
                    if (((c) c2.getLayoutParams()).f()) {
                        b2 = (b2 * 1.0f) / ((float) this.s);
                    }
                    f2 = Math.max(f2, b2);
                }
            }
            int i3 = this.x;
            int round = Math.round(f2 * ((float) this.s));
            if (this.v.d() == Integer.MIN_VALUE) {
                round = Math.min(round, this.v.g());
            }
            j(round);
            if (this.x != i3) {
                for (int i4 = 0; i4 < e2; i4++) {
                    View c3 = c(i4);
                    c cVar = (c) c3.getLayoutParams();
                    if (!cVar.f1830f) {
                        if (!L() || this.w != 1) {
                            int i5 = cVar.f1829e.f1835e;
                            int i6 = this.x * i5;
                            int i7 = i5 * i3;
                            if (this.w == 1) {
                                c3.offsetLeftAndRight(i6 - i7);
                            } else {
                                c3.offsetTopAndBottom(i6 - i7);
                            }
                        } else {
                            int i8 = this.s;
                            int i9 = cVar.f1829e.f1835e;
                            c3.offsetLeftAndRight(((-((i8 - 1) - i9)) * this.x) - ((-((i8 - 1) - i9)) * i3));
                        }
                    }
                }
            }
        }
    }

    private void O() {
        if (this.w == 1 || !L()) {
            this.A = this.z;
        } else {
            this.A = !this.z;
        }
    }

    private boolean a(d dVar) {
        if (this.A) {
            if (dVar.g() < this.u.b()) {
                ArrayList<View> arrayList = dVar.f1831a;
                return !dVar.b(arrayList.get(arrayList.size() - 1)).f1830f;
            }
        } else if (dVar.h() > this.u.f()) {
            return !dVar.b(dVar.f1831a.get(0)).f1830f;
        }
        return false;
    }

    private int k(int i2) {
        if (e() != 0) {
            if ((i2 < H()) != this.A) {
                return -1;
            }
            return 1;
        } else if (this.A) {
            return 1;
        } else {
            return -1;
        }
    }

    private int l(int i2) {
        if (i2 == 1) {
            return (this.w != 1 && L()) ? 1 : -1;
        }
        if (i2 == 2) {
            return (this.w != 1 && L()) ? -1 : 1;
        }
        if (i2 != 17) {
            if (i2 != 33) {
                if (i2 != 66) {
                    return (i2 == 130 && this.w == 1) ? 1 : Integer.MIN_VALUE;
                }
                if (this.w == 0) {
                    return 1;
                }
                return Integer.MIN_VALUE;
            } else if (this.w == 1) {
                return -1;
            } else {
                return Integer.MIN_VALUE;
            }
        } else if (this.w == 0) {
            return -1;
        } else {
            return Integer.MIN_VALUE;
        }
    }

    private LazySpanLookup.FullSpanItem m(int i2) {
        LazySpanLookup.FullSpanItem fullSpanItem = new LazySpanLookup.FullSpanItem();
        fullSpanItem.f1809c = new int[this.s];
        for (int i3 = 0; i3 < this.s; i3++) {
            fullSpanItem.f1809c[i3] = i2 - this.t[i3].a(i2);
        }
        return fullSpanItem;
    }

    private LazySpanLookup.FullSpanItem n(int i2) {
        LazySpanLookup.FullSpanItem fullSpanItem = new LazySpanLookup.FullSpanItem();
        fullSpanItem.f1809c = new int[this.s];
        for (int i3 = 0; i3 < this.s; i3++) {
            fullSpanItem.f1809c[i3] = this.t[i3].b(i2) - i2;
        }
        return fullSpanItem;
    }

    private int o(int i2) {
        int e2 = e();
        for (int i3 = 0; i3 < e2; i3++) {
            int l = l(c(i3));
            if (l >= 0 && l < i2) {
                return l;
            }
        }
        return 0;
    }

    private void p(View view) {
        for (int i2 = this.s - 1; i2 >= 0; i2--) {
            this.t[i2].a(view);
        }
    }

    private void q(View view) {
        for (int i2 = this.s - 1; i2 >= 0; i2--) {
            this.t[i2].c(view);
        }
    }

    private int r(int i2) {
        int b2 = this.t[0].b(i2);
        for (int i3 = 1; i3 < this.s; i3++) {
            int b3 = this.t[i3].b(i2);
            if (b3 > b2) {
                b2 = b3;
            }
        }
        return b2;
    }

    private int s(int i2) {
        int a2 = this.t[0].a(i2);
        for (int i3 = 1; i3 < this.s; i3++) {
            int a3 = this.t[i3].a(i2);
            if (a3 < a2) {
                a2 = a3;
            }
        }
        return a2;
    }

    private int t(int i2) {
        int b2 = this.t[0].b(i2);
        for (int i3 = 1; i3 < this.s; i3++) {
            int b3 = this.t[i3].b(i2);
            if (b3 < b2) {
                b2 = b3;
            }
        }
        return b2;
    }

    private void v(int i2) {
        g gVar = this.y;
        gVar.f1932e = i2;
        int i3 = 1;
        if (this.A != (i2 == -1)) {
            i3 = -1;
        }
        gVar.f1931d = i3;
    }

    public boolean C() {
        return this.I == null;
    }

    /* access modifiers changed from: package-private */
    public boolean D() {
        int a2 = this.t[0].a(Integer.MIN_VALUE);
        for (int i2 = 1; i2 < this.s; i2++) {
            if (this.t[i2].a(Integer.MIN_VALUE) != a2) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean E() {
        int b2 = this.t[0].b(Integer.MIN_VALUE);
        for (int i2 = 1; i2 < this.s; i2++) {
            if (this.t[i2].b(Integer.MIN_VALUE) != b2) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean F() {
        int i2;
        int i3;
        if (e() == 0 || this.F == 0 || !t()) {
            return false;
        }
        if (this.A) {
            i3 = I();
            i2 = H();
        } else {
            i3 = H();
            i2 = I();
        }
        if (i3 == 0 && J() != null) {
            this.E.a();
            z();
            y();
            return true;
        } else if (!this.M) {
            return false;
        } else {
            int i4 = this.A ? -1 : 1;
            int i5 = i2 + 1;
            LazySpanLookup.FullSpanItem a2 = this.E.a(i3, i5, i4, true);
            if (a2 == null) {
                this.M = false;
                this.E.b(i5);
                return false;
            }
            LazySpanLookup.FullSpanItem a3 = this.E.a(i3, a2.f1807a, i4 * -1, true);
            if (a3 == null) {
                this.E.b(a2.f1807a);
            } else {
                this.E.b(a3.f1807a + 1);
            }
            z();
            y();
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public int G() {
        View view;
        if (this.A) {
            view = a(true);
        } else {
            view = b(true);
        }
        if (view == null) {
            return -1;
        }
        return l(view);
    }

    /* access modifiers changed from: package-private */
    public int H() {
        if (e() == 0) {
            return 0;
        }
        return l(c(0));
    }

    /* access modifiers changed from: package-private */
    public int I() {
        int e2 = e();
        if (e2 == 0) {
            return 0;
        }
        return l(c(e2 - 1));
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0074, code lost:
        if (r10 == r11) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0086, code lost:
        if (r10 == r11) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008a, code lost:
        r10 = false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View J() {
        /*
            r12 = this;
            int r0 = r12.e()
            r1 = 1
            int r0 = r0 - r1
            java.util.BitSet r2 = new java.util.BitSet
            int r3 = r12.s
            r2.<init>(r3)
            int r3 = r12.s
            r4 = 0
            r2.set(r4, r3, r1)
            int r3 = r12.w
            r5 = -1
            if (r3 != r1) goto L_0x0020
            boolean r3 = r12.L()
            if (r3 == 0) goto L_0x0020
            r3 = 1
            goto L_0x0021
        L_0x0020:
            r3 = -1
        L_0x0021:
            boolean r6 = r12.A
            if (r6 == 0) goto L_0x0027
            r6 = -1
            goto L_0x002b
        L_0x0027:
            int r0 = r0 + 1
            r6 = r0
            r0 = 0
        L_0x002b:
            if (r0 >= r6) goto L_0x002e
            r5 = 1
        L_0x002e:
            if (r0 == r6) goto L_0x00ab
            android.view.View r7 = r12.c(r0)
            android.view.ViewGroup$LayoutParams r8 = r7.getLayoutParams()
            androidx.recyclerview.widget.StaggeredGridLayoutManager$c r8 = (androidx.recyclerview.widget.StaggeredGridLayoutManager.c) r8
            androidx.recyclerview.widget.StaggeredGridLayoutManager$d r9 = r8.f1829e
            int r9 = r9.f1835e
            boolean r9 = r2.get(r9)
            if (r9 == 0) goto L_0x0054
            androidx.recyclerview.widget.StaggeredGridLayoutManager$d r9 = r8.f1829e
            boolean r9 = r12.a(r9)
            if (r9 == 0) goto L_0x004d
            return r7
        L_0x004d:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$d r9 = r8.f1829e
            int r9 = r9.f1835e
            r2.clear(r9)
        L_0x0054:
            boolean r9 = r8.f1830f
            if (r9 == 0) goto L_0x0059
            goto L_0x00a9
        L_0x0059:
            int r9 = r0 + r5
            if (r9 == r6) goto L_0x00a9
            android.view.View r9 = r12.c(r9)
            boolean r10 = r12.A
            if (r10 == 0) goto L_0x0077
            androidx.recyclerview.widget.i r10 = r12.u
            int r10 = r10.a(r7)
            androidx.recyclerview.widget.i r11 = r12.u
            int r11 = r11.a(r9)
            if (r10 >= r11) goto L_0x0074
            return r7
        L_0x0074:
            if (r10 != r11) goto L_0x008a
            goto L_0x0088
        L_0x0077:
            androidx.recyclerview.widget.i r10 = r12.u
            int r10 = r10.d(r7)
            androidx.recyclerview.widget.i r11 = r12.u
            int r11 = r11.d(r9)
            if (r10 <= r11) goto L_0x0086
            return r7
        L_0x0086:
            if (r10 != r11) goto L_0x008a
        L_0x0088:
            r10 = 1
            goto L_0x008b
        L_0x008a:
            r10 = 0
        L_0x008b:
            if (r10 == 0) goto L_0x00a9
            android.view.ViewGroup$LayoutParams r9 = r9.getLayoutParams()
            androidx.recyclerview.widget.StaggeredGridLayoutManager$c r9 = (androidx.recyclerview.widget.StaggeredGridLayoutManager.c) r9
            androidx.recyclerview.widget.StaggeredGridLayoutManager$d r8 = r8.f1829e
            int r8 = r8.f1835e
            androidx.recyclerview.widget.StaggeredGridLayoutManager$d r9 = r9.f1829e
            int r9 = r9.f1835e
            int r8 = r8 - r9
            if (r8 >= 0) goto L_0x00a0
            r8 = 1
            goto L_0x00a1
        L_0x00a0:
            r8 = 0
        L_0x00a1:
            if (r3 >= 0) goto L_0x00a5
            r9 = 1
            goto L_0x00a6
        L_0x00a5:
            r9 = 0
        L_0x00a6:
            if (r8 == r9) goto L_0x00a9
            return r7
        L_0x00a9:
            int r0 = r0 + r5
            goto L_0x002e
        L_0x00ab:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.J():android.view.View");
    }

    public void K() {
        this.E.a();
        y();
    }

    /* access modifiers changed from: package-private */
    public boolean L() {
        return j() == 1;
    }

    public void b(RecyclerView recyclerView, RecyclerView.v vVar) {
        super.b(recyclerView, vVar);
        a(this.P);
        for (int i2 = 0; i2 < this.s; i2++) {
            this.t[i2].c();
        }
        recyclerView.requestLayout();
    }

    public void c(boolean z2) {
        a((String) null);
        SavedState savedState = this.I;
        if (!(savedState == null || savedState.f1818h == z2)) {
            savedState.f1818h = z2;
        }
        this.z = z2;
        y();
    }

    public int d(RecyclerView.z zVar) {
        return h(zVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.StaggeredGridLayoutManager.c(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):void
     arg types: [androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int]
     candidates:
      androidx.recyclerview.widget.StaggeredGridLayoutManager.c(int, int, int):int
      androidx.recyclerview.widget.StaggeredGridLayoutManager.c(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.c(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.c(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):void */
    public void e(RecyclerView.v vVar, RecyclerView.z zVar) {
        c(vVar, zVar, true);
    }

    public void f(int i2) {
        if (i2 == 0) {
            F();
        }
    }

    public void g(RecyclerView.z zVar) {
        super.g(zVar);
        this.C = -1;
        this.D = Integer.MIN_VALUE;
        this.I = null;
        this.L.b();
    }

    public void h(int i2) {
        if (i2 == 0 || i2 == 1) {
            a((String) null);
            if (i2 != this.w) {
                this.w = i2;
                i iVar = this.u;
                this.u = this.v;
                this.v = iVar;
                y();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("invalid orientation.");
    }

    public void i(int i2) {
        a((String) null);
        if (i2 != this.s) {
            K();
            this.s = i2;
            this.B = new BitSet(this.s);
            this.t = new d[this.s];
            for (int i3 = 0; i3 < this.s; i3++) {
                this.t[i3] = new d(i3);
            }
            y();
        }
    }

    /* access modifiers changed from: package-private */
    public void j(int i2) {
        this.x = i2 / this.s;
        this.J = View.MeasureSpec.makeMeasureSpec(i2, this.v.d());
    }

    public boolean u() {
        return this.F != 0;
    }

    public Parcelable x() {
        int i2;
        int i3;
        int i4;
        int[] iArr;
        SavedState savedState = this.I;
        if (savedState != null) {
            return new SavedState(savedState);
        }
        SavedState savedState2 = new SavedState();
        savedState2.f1818h = this.z;
        savedState2.f1819i = this.G;
        savedState2.f1820j = this.H;
        LazySpanLookup lazySpanLookup = this.E;
        if (lazySpanLookup == null || (iArr = lazySpanLookup.f1805a) == null) {
            savedState2.f1815e = 0;
        } else {
            savedState2.f1816f = iArr;
            savedState2.f1815e = savedState2.f1816f.length;
            savedState2.f1817g = lazySpanLookup.f1806b;
        }
        if (e() > 0) {
            if (this.G) {
                i2 = I();
            } else {
                i2 = H();
            }
            savedState2.f1811a = i2;
            savedState2.f1812b = G();
            int i5 = this.s;
            savedState2.f1813c = i5;
            savedState2.f1814d = new int[i5];
            for (int i6 = 0; i6 < this.s; i6++) {
                if (this.G) {
                    i3 = this.t[i6].a(Integer.MIN_VALUE);
                    if (i3 != Integer.MIN_VALUE) {
                        i4 = this.u.b();
                    } else {
                        savedState2.f1814d[i6] = i3;
                    }
                } else {
                    i3 = this.t[i6].b(Integer.MIN_VALUE);
                    if (i3 != Integer.MIN_VALUE) {
                        i4 = this.u.f();
                    } else {
                        savedState2.f1814d[i6] = i3;
                    }
                }
                i3 -= i4;
                savedState2.f1814d[i6] = i3;
            }
        } else {
            savedState2.f1811a = -1;
            savedState2.f1812b = -1;
            savedState2.f1813c = 0;
        }
        return savedState2;
    }

    static class LazySpanLookup {

        /* renamed from: a  reason: collision with root package name */
        int[] f1805a;

        /* renamed from: b  reason: collision with root package name */
        List<FullSpanItem> f1806b;

        LazySpanLookup() {
        }

        private void c(int i2, int i3) {
            List<FullSpanItem> list = this.f1806b;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    FullSpanItem fullSpanItem = this.f1806b.get(size);
                    int i4 = fullSpanItem.f1807a;
                    if (i4 >= i2) {
                        fullSpanItem.f1807a = i4 + i3;
                    }
                }
            }
        }

        private int g(int i2) {
            if (this.f1806b == null) {
                return -1;
            }
            FullSpanItem c2 = c(i2);
            if (c2 != null) {
                this.f1806b.remove(c2);
            }
            int size = this.f1806b.size();
            int i3 = 0;
            while (true) {
                if (i3 >= size) {
                    i3 = -1;
                    break;
                } else if (this.f1806b.get(i3).f1807a >= i2) {
                    break;
                } else {
                    i3++;
                }
            }
            if (i3 == -1) {
                return -1;
            }
            this.f1806b.remove(i3);
            return this.f1806b.get(i3).f1807a;
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, d dVar) {
            a(i2);
            this.f1805a[i2] = dVar.f1835e;
        }

        /* access modifiers changed from: package-private */
        public int b(int i2) {
            List<FullSpanItem> list = this.f1806b;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    if (this.f1806b.get(size).f1807a >= i2) {
                        this.f1806b.remove(size);
                    }
                }
            }
            return e(i2);
        }

        /* access modifiers changed from: package-private */
        public int d(int i2) {
            int[] iArr = this.f1805a;
            if (iArr == null || i2 >= iArr.length) {
                return -1;
            }
            return iArr[i2];
        }

        /* access modifiers changed from: package-private */
        public int e(int i2) {
            int[] iArr = this.f1805a;
            if (iArr == null || i2 >= iArr.length) {
                return -1;
            }
            int g2 = g(i2);
            if (g2 == -1) {
                int[] iArr2 = this.f1805a;
                Arrays.fill(iArr2, i2, iArr2.length, -1);
                return this.f1805a.length;
            }
            int i3 = g2 + 1;
            Arrays.fill(this.f1805a, i2, i3, -1);
            return i3;
        }

        /* access modifiers changed from: package-private */
        public int f(int i2) {
            int length = this.f1805a.length;
            while (length <= i2) {
                length *= 2;
            }
            return length;
        }

        private void d(int i2, int i3) {
            List<FullSpanItem> list = this.f1806b;
            if (list != null) {
                int i4 = i2 + i3;
                for (int size = list.size() - 1; size >= 0; size--) {
                    FullSpanItem fullSpanItem = this.f1806b.get(size);
                    int i5 = fullSpanItem.f1807a;
                    if (i5 >= i2) {
                        if (i5 < i4) {
                            this.f1806b.remove(size);
                        } else {
                            fullSpanItem.f1807a = i5 - i3;
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            int[] iArr = this.f1805a;
            if (iArr == null) {
                this.f1805a = new int[(Math.max(i2, 10) + 1)];
                Arrays.fill(this.f1805a, -1);
            } else if (i2 >= iArr.length) {
                this.f1805a = new int[f(i2)];
                System.arraycopy(iArr, 0, this.f1805a, 0, iArr.length);
                int[] iArr2 = this.f1805a;
                Arrays.fill(iArr2, iArr.length, iArr2.length, -1);
            }
        }

        public FullSpanItem c(int i2) {
            List<FullSpanItem> list = this.f1806b;
            if (list == null) {
                return null;
            }
            for (int size = list.size() - 1; size >= 0; size--) {
                FullSpanItem fullSpanItem = this.f1806b.get(size);
                if (fullSpanItem.f1807a == i2) {
                    return fullSpanItem;
                }
            }
            return null;
        }

        @SuppressLint({"BanParcelableUsage"})
        static class FullSpanItem implements Parcelable {
            public static final Parcelable.Creator<FullSpanItem> CREATOR = new a();

            /* renamed from: a  reason: collision with root package name */
            int f1807a;

            /* renamed from: b  reason: collision with root package name */
            int f1808b;

            /* renamed from: c  reason: collision with root package name */
            int[] f1809c;

            /* renamed from: d  reason: collision with root package name */
            boolean f1810d;

            static class a implements Parcelable.Creator<FullSpanItem> {
                a() {
                }

                public FullSpanItem createFromParcel(Parcel parcel) {
                    return new FullSpanItem(parcel);
                }

                public FullSpanItem[] newArray(int i2) {
                    return new FullSpanItem[i2];
                }
            }

            FullSpanItem(Parcel parcel) {
                this.f1807a = parcel.readInt();
                this.f1808b = parcel.readInt();
                this.f1810d = parcel.readInt() != 1 ? false : true;
                int readInt = parcel.readInt();
                if (readInt > 0) {
                    this.f1809c = new int[readInt];
                    parcel.readIntArray(this.f1809c);
                }
            }

            /* access modifiers changed from: package-private */
            public int a(int i2) {
                int[] iArr = this.f1809c;
                if (iArr == null) {
                    return 0;
                }
                return iArr[i2];
            }

            public int describeContents() {
                return 0;
            }

            public String toString() {
                return "FullSpanItem{mPosition=" + this.f1807a + ", mGapDir=" + this.f1808b + ", mHasUnwantedGapAfter=" + this.f1810d + ", mGapPerSpan=" + Arrays.toString(this.f1809c) + '}';
            }

            public void writeToParcel(Parcel parcel, int i2) {
                parcel.writeInt(this.f1807a);
                parcel.writeInt(this.f1808b);
                parcel.writeInt(this.f1810d ? 1 : 0);
                int[] iArr = this.f1809c;
                if (iArr == null || iArr.length <= 0) {
                    parcel.writeInt(0);
                    return;
                }
                parcel.writeInt(iArr.length);
                parcel.writeIntArray(this.f1809c);
            }

            FullSpanItem() {
            }
        }

        /* access modifiers changed from: package-private */
        public void b(int i2, int i3) {
            int[] iArr = this.f1805a;
            if (iArr != null && i2 < iArr.length) {
                int i4 = i2 + i3;
                a(i4);
                int[] iArr2 = this.f1805a;
                System.arraycopy(iArr2, i4, iArr2, i2, (iArr2.length - i2) - i3);
                int[] iArr3 = this.f1805a;
                Arrays.fill(iArr3, iArr3.length - i3, iArr3.length, -1);
                d(i2, i3);
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            int[] iArr = this.f1805a;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            this.f1806b = null;
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, int i3) {
            int[] iArr = this.f1805a;
            if (iArr != null && i2 < iArr.length) {
                int i4 = i2 + i3;
                a(i4);
                int[] iArr2 = this.f1805a;
                System.arraycopy(iArr2, i2, iArr2, i4, (iArr2.length - i2) - i3);
                Arrays.fill(this.f1805a, i2, i4, -1);
                c(i2, i3);
            }
        }

        public void a(FullSpanItem fullSpanItem) {
            if (this.f1806b == null) {
                this.f1806b = new ArrayList();
            }
            int size = this.f1806b.size();
            for (int i2 = 0; i2 < size; i2++) {
                FullSpanItem fullSpanItem2 = this.f1806b.get(i2);
                if (fullSpanItem2.f1807a == fullSpanItem.f1807a) {
                    this.f1806b.remove(i2);
                }
                if (fullSpanItem2.f1807a >= fullSpanItem.f1807a) {
                    this.f1806b.add(i2, fullSpanItem);
                    return;
                }
            }
            this.f1806b.add(fullSpanItem);
        }

        public FullSpanItem a(int i2, int i3, int i4, boolean z) {
            List<FullSpanItem> list = this.f1806b;
            if (list == null) {
                return null;
            }
            int size = list.size();
            for (int i5 = 0; i5 < size; i5++) {
                FullSpanItem fullSpanItem = this.f1806b.get(i5);
                int i6 = fullSpanItem.f1807a;
                if (i6 >= i3) {
                    return null;
                }
                if (i6 >= i2 && (i4 == 0 || fullSpanItem.f1808b == i4 || (z && fullSpanItem.f1810d))) {
                    return fullSpanItem;
                }
            }
            return null;
        }
    }

    private boolean u(int i2) {
        if (this.w == 0) {
            if ((i2 == -1) != this.A) {
                return true;
            }
            return false;
        }
        if (((i2 == -1) == this.A) == L()) {
            return true;
        }
        return false;
    }

    public void d(int i2) {
        super.d(i2);
        for (int i3 = 0; i3 < this.s; i3++) {
            this.t[i3].c(i2);
        }
    }

    public int e(RecyclerView.z zVar) {
        return i(zVar);
    }

    public int f(RecyclerView.z zVar) {
        return j(zVar);
    }

    private int p(int i2) {
        for (int e2 = e() - 1; e2 >= 0; e2--) {
            int l = l(c(e2));
            if (l >= 0 && l < i2) {
                return l;
            }
        }
        return 0;
    }

    private int q(int i2) {
        int a2 = this.t[0].a(i2);
        for (int i3 = 1; i3 < this.s; i3++) {
            int a3 = this.t[i3].a(i2);
            if (a3 > a2) {
                a2 = a3;
            }
        }
        return a2;
    }

    public void e(int i2) {
        super.e(i2);
        for (int i3 = 0; i3 < this.s; i3++) {
            this.t[i3].c(i2);
        }
    }

    class b {

        /* renamed from: a  reason: collision with root package name */
        int f1822a;

        /* renamed from: b  reason: collision with root package name */
        int f1823b;

        /* renamed from: c  reason: collision with root package name */
        boolean f1824c;

        /* renamed from: d  reason: collision with root package name */
        boolean f1825d;

        /* renamed from: e  reason: collision with root package name */
        boolean f1826e;

        /* renamed from: f  reason: collision with root package name */
        int[] f1827f;

        b() {
            b();
        }

        /* access modifiers changed from: package-private */
        public void a(d[] dVarArr) {
            int length = dVarArr.length;
            int[] iArr = this.f1827f;
            if (iArr == null || iArr.length < length) {
                this.f1827f = new int[StaggeredGridLayoutManager.this.t.length];
            }
            for (int i2 = 0; i2 < length; i2++) {
                this.f1827f[i2] = dVarArr[i2].b(Integer.MIN_VALUE);
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f1822a = -1;
            this.f1823b = Integer.MIN_VALUE;
            this.f1824c = false;
            this.f1825d = false;
            this.f1826e = false;
            int[] iArr = this.f1827f;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            int i2;
            if (this.f1824c) {
                i2 = StaggeredGridLayoutManager.this.u.b();
            } else {
                i2 = StaggeredGridLayoutManager.this.u.f();
            }
            this.f1823b = i2;
        }

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            if (this.f1824c) {
                this.f1823b = StaggeredGridLayoutManager.this.u.b() - i2;
            } else {
                this.f1823b = StaggeredGridLayoutManager.this.u.f() + i2;
            }
        }
    }

    private int j(RecyclerView.z zVar) {
        if (e() == 0) {
            return 0;
        }
        return k.b(zVar, this.u, b(!this.N), a(!this.N), this, this.N);
    }

    public void d(RecyclerView recyclerView) {
        this.E.a();
        y();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.StaggeredGridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):void
     arg types: [androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int]
     candidates:
      androidx.recyclerview.widget.StaggeredGridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.g, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.StaggeredGridLayoutManager.a(android.view.View, androidx.recyclerview.widget.StaggeredGridLayoutManager$c, androidx.recyclerview.widget.g):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.a(android.view.View, androidx.recyclerview.widget.StaggeredGridLayoutManager$c, boolean):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.a(androidx.recyclerview.widget.StaggeredGridLayoutManager$d, int, int):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.StaggeredGridLayoutManager.a(android.graphics.Rect, int, int):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.a(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, int, android.view.View):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$p):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.accessibility.AccessibilityEvent):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.StaggeredGridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.StaggeredGridLayoutManager.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):void
     arg types: [androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int]
     candidates:
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, int, int):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(int, int, int):boolean
      androidx.recyclerview.widget.RecyclerView.o.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.StaggeredGridLayoutManager.c(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):void
     arg types: [androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int]
     candidates:
      androidx.recyclerview.widget.StaggeredGridLayoutManager.c(int, int, int):int
      androidx.recyclerview.widget.StaggeredGridLayoutManager.c(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.c(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.c(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0157, code lost:
        if (F() != false) goto L_0x015b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c(androidx.recyclerview.widget.RecyclerView.v r9, androidx.recyclerview.widget.RecyclerView.z r10, boolean r11) {
        /*
            r8 = this;
            androidx.recyclerview.widget.StaggeredGridLayoutManager$b r0 = r8.L
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r1 = r8.I
            r2 = -1
            if (r1 != 0) goto L_0x000b
            int r1 = r8.C
            if (r1 == r2) goto L_0x0018
        L_0x000b:
            int r1 = r10.a()
            if (r1 != 0) goto L_0x0018
            r8.b(r9)
            r0.b()
            return
        L_0x0018:
            boolean r1 = r0.f1826e
            r3 = 0
            r4 = 1
            if (r1 == 0) goto L_0x0029
            int r1 = r8.C
            if (r1 != r2) goto L_0x0029
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r1 = r8.I
            if (r1 == 0) goto L_0x0027
            goto L_0x0029
        L_0x0027:
            r1 = 0
            goto L_0x002a
        L_0x0029:
            r1 = 1
        L_0x002a:
            if (r1 == 0) goto L_0x0043
            r0.b()
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r5 = r8.I
            if (r5 == 0) goto L_0x0037
            r8.a(r0)
            goto L_0x003e
        L_0x0037:
            r8.O()
            boolean r5 = r8.A
            r0.f1824c = r5
        L_0x003e:
            r8.b(r10, r0)
            r0.f1826e = r4
        L_0x0043:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r5 = r8.I
            if (r5 != 0) goto L_0x0060
            int r5 = r8.C
            if (r5 != r2) goto L_0x0060
            boolean r5 = r0.f1824c
            boolean r6 = r8.G
            if (r5 != r6) goto L_0x0059
            boolean r5 = r8.L()
            boolean r6 = r8.H
            if (r5 == r6) goto L_0x0060
        L_0x0059:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r5 = r8.E
            r5.a()
            r0.f1825d = r4
        L_0x0060:
            int r5 = r8.e()
            if (r5 <= 0) goto L_0x00c9
            androidx.recyclerview.widget.StaggeredGridLayoutManager$SavedState r5 = r8.I
            if (r5 == 0) goto L_0x006e
            int r5 = r5.f1813c
            if (r5 >= r4) goto L_0x00c9
        L_0x006e:
            boolean r5 = r0.f1825d
            if (r5 == 0) goto L_0x008e
            r1 = 0
        L_0x0073:
            int r5 = r8.s
            if (r1 >= r5) goto L_0x00c9
            androidx.recyclerview.widget.StaggeredGridLayoutManager$d[] r5 = r8.t
            r5 = r5[r1]
            r5.c()
            int r5 = r0.f1823b
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r5 == r6) goto L_0x008b
            androidx.recyclerview.widget.StaggeredGridLayoutManager$d[] r6 = r8.t
            r6 = r6[r1]
            r6.d(r5)
        L_0x008b:
            int r1 = r1 + 1
            goto L_0x0073
        L_0x008e:
            if (r1 != 0) goto L_0x00af
            androidx.recyclerview.widget.StaggeredGridLayoutManager$b r1 = r8.L
            int[] r1 = r1.f1827f
            if (r1 != 0) goto L_0x0097
            goto L_0x00af
        L_0x0097:
            r1 = 0
        L_0x0098:
            int r5 = r8.s
            if (r1 >= r5) goto L_0x00c9
            androidx.recyclerview.widget.StaggeredGridLayoutManager$d[] r5 = r8.t
            r5 = r5[r1]
            r5.c()
            androidx.recyclerview.widget.StaggeredGridLayoutManager$b r6 = r8.L
            int[] r6 = r6.f1827f
            r6 = r6[r1]
            r5.d(r6)
            int r1 = r1 + 1
            goto L_0x0098
        L_0x00af:
            r1 = 0
        L_0x00b0:
            int r5 = r8.s
            if (r1 >= r5) goto L_0x00c2
            androidx.recyclerview.widget.StaggeredGridLayoutManager$d[] r5 = r8.t
            r5 = r5[r1]
            boolean r6 = r8.A
            int r7 = r0.f1823b
            r5.a(r6, r7)
            int r1 = r1 + 1
            goto L_0x00b0
        L_0x00c2:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$b r1 = r8.L
            androidx.recyclerview.widget.StaggeredGridLayoutManager$d[] r5 = r8.t
            r1.a(r5)
        L_0x00c9:
            r8.a(r9)
            androidx.recyclerview.widget.g r1 = r8.y
            r1.f1928a = r3
            r8.M = r3
            androidx.recyclerview.widget.i r1 = r8.v
            int r1 = r1.g()
            r8.j(r1)
            int r1 = r0.f1822a
            r8.b(r1, r10)
            boolean r1 = r0.f1824c
            if (r1 == 0) goto L_0x00fc
            r8.v(r2)
            androidx.recyclerview.widget.g r1 = r8.y
            r8.a(r9, r1, r10)
            r8.v(r4)
            androidx.recyclerview.widget.g r1 = r8.y
            int r2 = r0.f1822a
            int r5 = r1.f1931d
            int r2 = r2 + r5
            r1.f1930c = r2
            r8.a(r9, r1, r10)
            goto L_0x0113
        L_0x00fc:
            r8.v(r4)
            androidx.recyclerview.widget.g r1 = r8.y
            r8.a(r9, r1, r10)
            r8.v(r2)
            androidx.recyclerview.widget.g r1 = r8.y
            int r2 = r0.f1822a
            int r5 = r1.f1931d
            int r2 = r2 + r5
            r1.f1930c = r2
            r8.a(r9, r1, r10)
        L_0x0113:
            r8.N()
            int r1 = r8.e()
            if (r1 <= 0) goto L_0x012d
            boolean r1 = r8.A
            if (r1 == 0) goto L_0x0127
            r8.a(r9, r10, r4)
            r8.b(r9, r10, r3)
            goto L_0x012d
        L_0x0127:
            r8.b(r9, r10, r4)
            r8.a(r9, r10, r3)
        L_0x012d:
            if (r11 == 0) goto L_0x015a
            boolean r11 = r10.d()
            if (r11 != 0) goto L_0x015a
            int r11 = r8.F
            if (r11 == 0) goto L_0x014b
            int r11 = r8.e()
            if (r11 <= 0) goto L_0x014b
            boolean r11 = r8.M
            if (r11 != 0) goto L_0x0149
            android.view.View r11 = r8.J()
            if (r11 == 0) goto L_0x014b
        L_0x0149:
            r11 = 1
            goto L_0x014c
        L_0x014b:
            r11 = 0
        L_0x014c:
            if (r11 == 0) goto L_0x015a
            java.lang.Runnable r11 = r8.P
            r8.a(r11)
            boolean r11 = r8.F()
            if (r11 == 0) goto L_0x015a
            goto L_0x015b
        L_0x015a:
            r4 = 0
        L_0x015b:
            boolean r11 = r10.d()
            if (r11 == 0) goto L_0x0166
            androidx.recyclerview.widget.StaggeredGridLayoutManager$b r11 = r8.L
            r11.b()
        L_0x0166:
            boolean r11 = r0.f1824c
            r8.G = r11
            boolean r11 = r8.L()
            r8.H = r11
            if (r4 == 0) goto L_0x017a
            androidx.recyclerview.widget.StaggeredGridLayoutManager$b r11 = r8.L
            r11.b()
            r8.c(r9, r10, r3)
        L_0x017a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.c(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):void");
    }

    private void e(int i2, int i3) {
        for (int i4 = 0; i4 < this.s; i4++) {
            if (!this.t[i4].f1831a.isEmpty()) {
                a(this.t[i4], i2, i3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(RecyclerView.z zVar, b bVar) {
        if (!a(zVar, bVar) && !c(zVar, bVar)) {
            bVar.a();
            bVar.f1822a = 0;
        }
    }

    private int h(RecyclerView.z zVar) {
        if (e() == 0) {
            return 0;
        }
        return k.a(zVar, this.u, b(!this.N), a(!this.N), this, this.N);
    }

    private int i(RecyclerView.z zVar) {
        if (e() == 0) {
            return 0;
        }
        return k.a(zVar, this.u, b(!this.N), a(!this.N), this, this.N, this.A);
    }

    public void a(String str) {
        if (this.I == null) {
            super.a(str);
        }
    }

    public int b(RecyclerView.z zVar) {
        return i(zVar);
    }

    public int b(RecyclerView.v vVar, RecyclerView.z zVar) {
        if (this.w == 0) {
            return this.s;
        }
        return super.b(vVar, zVar);
    }

    public void a(Rect rect, int i2, int i3) {
        int i4;
        int i5;
        int n = n() + o();
        int p = p() + m();
        if (this.w == 1) {
            i5 = RecyclerView.o.a(i3, rect.height() + p, k());
            i4 = RecyclerView.o.a(i2, (this.x * this.s) + n, l());
        } else {
            i4 = RecyclerView.o.a(i2, rect.width() + n, l());
            i5 = RecyclerView.o.a(i3, (this.x * this.s) + p, k());
        }
        c(i4, i5);
    }

    /* access modifiers changed from: package-private */
    public View b(boolean z2) {
        int f2 = this.u.f();
        int b2 = this.u.b();
        int e2 = e();
        View view = null;
        for (int i2 = 0; i2 < e2; i2++) {
            View c2 = c(i2);
            int d2 = this.u.d(c2);
            if (this.u.a(c2) > f2 && d2 < b2) {
                if (d2 >= f2 || !z2) {
                    return c2;
                }
                if (view == null) {
                    view = c2;
                }
            }
        }
        return view;
    }

    private void b(RecyclerView.v vVar, RecyclerView.z zVar, boolean z2) {
        int f2;
        int t2 = t(Api.BaseClientBuilder.API_PRIORITY_OTHER);
        if (t2 != Integer.MAX_VALUE && (f2 = t2 - this.u.f()) > 0) {
            int c2 = f2 - c(f2, vVar, zVar);
            if (z2 && c2 > 0) {
                this.u.a(-c2);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r5, androidx.recyclerview.widget.RecyclerView.z r6) {
        /*
            r4 = this;
            androidx.recyclerview.widget.g r0 = r4.y
            r1 = 0
            r0.f1929b = r1
            r0.f1930c = r5
            boolean r0 = r4.w()
            r2 = 1
            if (r0 == 0) goto L_0x002e
            int r6 = r6.b()
            r0 = -1
            if (r6 == r0) goto L_0x002e
            boolean r0 = r4.A
            if (r6 >= r5) goto L_0x001b
            r5 = 1
            goto L_0x001c
        L_0x001b:
            r5 = 0
        L_0x001c:
            if (r0 != r5) goto L_0x0025
            androidx.recyclerview.widget.i r5 = r4.u
            int r5 = r5.g()
            goto L_0x002f
        L_0x0025:
            androidx.recyclerview.widget.i r5 = r4.u
            int r5 = r5.g()
            r6 = r5
            r5 = 0
            goto L_0x0030
        L_0x002e:
            r5 = 0
        L_0x002f:
            r6 = 0
        L_0x0030:
            boolean r0 = r4.f()
            if (r0 == 0) goto L_0x004d
            androidx.recyclerview.widget.g r0 = r4.y
            androidx.recyclerview.widget.i r3 = r4.u
            int r3 = r3.f()
            int r3 = r3 - r6
            r0.f1933f = r3
            androidx.recyclerview.widget.g r6 = r4.y
            androidx.recyclerview.widget.i r0 = r4.u
            int r0 = r0.b()
            int r0 = r0 + r5
            r6.f1934g = r0
            goto L_0x005d
        L_0x004d:
            androidx.recyclerview.widget.g r0 = r4.y
            androidx.recyclerview.widget.i r3 = r4.u
            int r3 = r3.a()
            int r3 = r3 + r5
            r0.f1934g = r3
            androidx.recyclerview.widget.g r5 = r4.y
            int r6 = -r6
            r5.f1933f = r6
        L_0x005d:
            androidx.recyclerview.widget.g r5 = r4.y
            r5.f1935h = r1
            r5.f1928a = r2
            androidx.recyclerview.widget.i r6 = r4.u
            int r6 = r6.d()
            if (r6 != 0) goto L_0x0074
            androidx.recyclerview.widget.i r6 = r4.u
            int r6 = r6.a()
            if (r6 != 0) goto L_0x0074
            r1 = 1
        L_0x0074:
            r5.f1936i = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, androidx.recyclerview.widget.RecyclerView$z):void");
    }

    private void a(b bVar) {
        int i2;
        SavedState savedState = this.I;
        int i3 = savedState.f1813c;
        if (i3 > 0) {
            if (i3 == this.s) {
                for (int i4 = 0; i4 < this.s; i4++) {
                    this.t[i4].c();
                    SavedState savedState2 = this.I;
                    int i5 = savedState2.f1814d[i4];
                    if (i5 != Integer.MIN_VALUE) {
                        if (savedState2.f1819i) {
                            i2 = this.u.b();
                        } else {
                            i2 = this.u.f();
                        }
                        i5 += i2;
                    }
                    this.t[i4].d(i5);
                }
            } else {
                savedState.a();
                SavedState savedState3 = this.I;
                savedState3.f1811a = savedState3.f1812b;
            }
        }
        SavedState savedState4 = this.I;
        this.H = savedState4.f1820j;
        c(savedState4.f1818h);
        O();
        SavedState savedState5 = this.I;
        int i6 = savedState5.f1811a;
        if (i6 != -1) {
            this.C = i6;
            bVar.f1824c = savedState5.f1819i;
        } else {
            bVar.f1824c = this.A;
        }
        SavedState savedState6 = this.I;
        if (savedState6.f1815e > 1) {
            LazySpanLookup lazySpanLookup = this.E;
            lazySpanLookup.f1805a = savedState6.f1816f;
            lazySpanLookup.f1806b = savedState6.f1817g;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, int, int):void
     arg types: [int, int, int]
     candidates:
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(int, int, int):boolean
      androidx.recyclerview.widget.RecyclerView.o.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, int, int):void */
    public void b(RecyclerView recyclerView, int i2, int i3) {
        b(i2, i3, 2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0045 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0046  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r7, int r8, int r9) {
        /*
            r6 = this;
            boolean r0 = r6.A
            if (r0 == 0) goto L_0x0009
            int r0 = r6.I()
            goto L_0x000d
        L_0x0009:
            int r0 = r6.H()
        L_0x000d:
            r1 = 8
            if (r9 != r1) goto L_0x001b
            if (r7 >= r8) goto L_0x0016
            int r2 = r8 + 1
            goto L_0x001d
        L_0x0016:
            int r2 = r7 + 1
            r3 = r2
            r2 = r8
            goto L_0x001f
        L_0x001b:
            int r2 = r7 + r8
        L_0x001d:
            r3 = r2
            r2 = r7
        L_0x001f:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r4 = r6.E
            r4.e(r2)
            r4 = 1
            if (r9 == r4) goto L_0x003e
            r5 = 2
            if (r9 == r5) goto L_0x0038
            if (r9 == r1) goto L_0x002d
            goto L_0x0043
        L_0x002d:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r9 = r6.E
            r9.b(r7, r4)
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r7 = r6.E
            r7.a(r8, r4)
            goto L_0x0043
        L_0x0038:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r9 = r6.E
            r9.b(r7, r8)
            goto L_0x0043
        L_0x003e:
            androidx.recyclerview.widget.StaggeredGridLayoutManager$LazySpanLookup r9 = r6.E
            r9.a(r7, r8)
        L_0x0043:
            if (r3 > r0) goto L_0x0046
            return
        L_0x0046:
            boolean r7 = r6.A
            if (r7 == 0) goto L_0x004f
            int r7 = r6.H()
            goto L_0x0053
        L_0x004f:
            int r7 = r6.I()
        L_0x0053:
            if (r2 > r7) goto L_0x0058
            r6.y()
        L_0x0058:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, int, int):void");
    }

    /* access modifiers changed from: package-private */
    public boolean a(RecyclerView.z zVar, b bVar) {
        int i2;
        int i3;
        int i4;
        boolean z2 = false;
        if (!zVar.d() && (i2 = this.C) != -1) {
            if (i2 < 0 || i2 >= zVar.a()) {
                this.C = -1;
                this.D = Integer.MIN_VALUE;
            } else {
                SavedState savedState = this.I;
                if (savedState == null || savedState.f1811a == -1 || savedState.f1813c < 1) {
                    View b2 = b(this.C);
                    if (b2 != null) {
                        if (this.A) {
                            i3 = I();
                        } else {
                            i3 = H();
                        }
                        bVar.f1822a = i3;
                        if (this.D != Integer.MIN_VALUE) {
                            if (bVar.f1824c) {
                                bVar.f1823b = (this.u.b() - this.D) - this.u.a(b2);
                            } else {
                                bVar.f1823b = (this.u.f() + this.D) - this.u.d(b2);
                            }
                            return true;
                        } else if (this.u.b(b2) > this.u.g()) {
                            if (bVar.f1824c) {
                                i4 = this.u.b();
                            } else {
                                i4 = this.u.f();
                            }
                            bVar.f1823b = i4;
                            return true;
                        } else {
                            int d2 = this.u.d(b2) - this.u.f();
                            if (d2 < 0) {
                                bVar.f1823b = -d2;
                                return true;
                            }
                            int b3 = this.u.b() - this.u.a(b2);
                            if (b3 < 0) {
                                bVar.f1823b = b3;
                                return true;
                            }
                            bVar.f1823b = Integer.MIN_VALUE;
                        }
                    } else {
                        bVar.f1822a = this.C;
                        int i5 = this.D;
                        if (i5 == Integer.MIN_VALUE) {
                            if (k(bVar.f1822a) == 1) {
                                z2 = true;
                            }
                            bVar.f1824c = z2;
                            bVar.a();
                        } else {
                            bVar.a(i5);
                        }
                        bVar.f1825d = true;
                    }
                } else {
                    bVar.f1823b = Integer.MIN_VALUE;
                    bVar.f1822a = this.C;
                }
                return true;
            }
        }
        return false;
    }

    private void b(RecyclerView.v vVar, int i2) {
        while (e() > 0) {
            View c2 = c(0);
            if (this.u.a(c2) <= i2 && this.u.e(c2) <= i2) {
                c cVar = (c) c2.getLayoutParams();
                if (cVar.f1830f) {
                    int i3 = 0;
                    while (i3 < this.s) {
                        if (this.t[i3].f1831a.size() != 1) {
                            i3++;
                        } else {
                            return;
                        }
                    }
                    for (int i4 = 0; i4 < this.s; i4++) {
                        this.t[i4].k();
                    }
                } else if (cVar.f1829e.f1831a.size() != 1) {
                    cVar.f1829e.k();
                } else {
                    return;
                }
                a(c2, vVar);
            } else {
                return;
            }
        }
    }

    public boolean b() {
        return this.w == 1;
    }

    public int b(int i2, RecyclerView.v vVar, RecyclerView.z zVar) {
        return c(i2, vVar, zVar);
    }

    private boolean c(RecyclerView.z zVar, b bVar) {
        int i2;
        if (this.G) {
            i2 = p(zVar.a());
        } else {
            i2 = o(zVar.a());
        }
        bVar.f1822a = i2;
        bVar.f1823b = Integer.MIN_VALUE;
        return true;
    }

    public int c(RecyclerView.z zVar) {
        return j(zVar);
    }

    private int c(int i2, int i3, int i4) {
        if (i3 == 0 && i4 == 0) {
            return i2;
        }
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == Integer.MIN_VALUE || mode == 1073741824) {
            return View.MeasureSpec.makeMeasureSpec(Math.max(0, (View.MeasureSpec.getSize(i2) - i3) - i4), mode);
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    public int c(int i2, RecyclerView.v vVar, RecyclerView.z zVar) {
        if (e() == 0 || i2 == 0) {
            return 0;
        }
        a(i2, zVar);
        int a2 = a(vVar, this.y, zVar);
        if (this.y.f1929b >= a2) {
            i2 = i2 < 0 ? -a2 : a2;
        }
        this.u.a(-i2);
        this.G = this.A;
        g gVar = this.y;
        gVar.f1929b = 0;
        a(vVar, gVar);
        return i2;
    }

    public int a(RecyclerView.z zVar) {
        return h(zVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.o.a(int, int, int, int, boolean):int
     arg types: [int, int, int, int, int]
     candidates:
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int, int, boolean):int */
    private void a(View view, c cVar, boolean z2) {
        if (cVar.f1830f) {
            if (this.w == 1) {
                a(view, this.J, RecyclerView.o.a(h(), i(), p() + m(), cVar.height, true), z2);
            } else {
                a(view, RecyclerView.o.a(q(), r(), n() + o(), cVar.width, true), this.J, z2);
            }
        } else if (this.w == 1) {
            a(view, RecyclerView.o.a(this.x, r(), 0, cVar.width, false), RecyclerView.o.a(h(), i(), p() + m(), cVar.height, true), z2);
        } else {
            a(view, RecyclerView.o.a(q(), r(), n() + o(), cVar.width, true), RecyclerView.o.a(this.x, i(), 0, cVar.height, false), z2);
        }
    }

    public RecyclerView.p c() {
        if (this.w == 0) {
            return new c(-2, -1);
        }
        return new c(-1, -2);
    }

    private void a(View view, int i2, int i3, boolean z2) {
        boolean z3;
        a(view, this.K);
        c cVar = (c) view.getLayoutParams();
        int i4 = cVar.leftMargin;
        Rect rect = this.K;
        int c2 = c(i2, i4 + rect.left, cVar.rightMargin + rect.right);
        int i5 = cVar.topMargin;
        Rect rect2 = this.K;
        int c3 = c(i3, i5 + rect2.top, cVar.bottomMargin + rect2.bottom);
        if (z2) {
            z3 = b(view, c2, c3, cVar);
        } else {
            z3 = a(view, c2, c3, cVar);
        }
        if (z3) {
            view.measure(c2, c3);
        }
    }

    public void a(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.I = (SavedState) parcelable;
            y();
        }
    }

    public void a(RecyclerView.v vVar, RecyclerView.z zVar, View view, b.e.m.d0.c cVar) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof c)) {
            super.a(view, cVar);
            return;
        }
        c cVar2 = (c) layoutParams;
        if (this.w == 0) {
            cVar.b(c.C0053c.a(cVar2.e(), cVar2.f1830f ? this.s : 1, -1, -1, false, false));
        } else {
            cVar.b(c.C0053c.a(-1, -1, cVar2.e(), cVar2.f1830f ? this.s : 1, false, false));
        }
    }

    public void a(AccessibilityEvent accessibilityEvent) {
        super.a(accessibilityEvent);
        if (e() > 0) {
            View b2 = b(false);
            View a2 = a(false);
            if (b2 != null && a2 != null) {
                int l = l(b2);
                int l2 = l(a2);
                if (l < l2) {
                    accessibilityEvent.setFromIndex(l);
                    accessibilityEvent.setToIndex(l2);
                    return;
                }
                accessibilityEvent.setFromIndex(l2);
                accessibilityEvent.setToIndex(l);
            }
        }
    }

    public int a(RecyclerView.v vVar, RecyclerView.z zVar) {
        if (this.w == 1) {
            return this.s;
        }
        return super.a(vVar, zVar);
    }

    /* access modifiers changed from: package-private */
    public View a(boolean z2) {
        int f2 = this.u.f();
        int b2 = this.u.b();
        View view = null;
        for (int e2 = e() - 1; e2 >= 0; e2--) {
            View c2 = c(e2);
            int d2 = this.u.d(c2);
            int a2 = this.u.a(c2);
            if (a2 > f2 && d2 < b2) {
                if (a2 <= b2 || !z2) {
                    return c2;
                }
                if (view == null) {
                    view = c2;
                }
            }
        }
        return view;
    }

    private void a(RecyclerView.v vVar, RecyclerView.z zVar, boolean z2) {
        int b2;
        int q = q(Integer.MIN_VALUE);
        if (q != Integer.MIN_VALUE && (b2 = this.u.b() - q) > 0) {
            int i2 = b2 - (-c(-b2, vVar, zVar));
            if (z2 && i2 > 0) {
                this.u.a(i2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, int, int):void
     arg types: [int, int, int]
     candidates:
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(int, int, int):boolean
      androidx.recyclerview.widget.RecyclerView.o.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, int, int):void */
    public void a(RecyclerView recyclerView, int i2, int i3) {
        b(i2, i3, 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, int, int):void
     arg types: [int, int, int]
     candidates:
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(int, int, int):boolean
      androidx.recyclerview.widget.RecyclerView.o.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, int, int):void */
    public void a(RecyclerView recyclerView, int i2, int i3, int i4) {
        b(i2, i3, 8);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, int, int):void
     arg types: [int, int, int]
     candidates:
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(int, int, int):boolean
      androidx.recyclerview.widget.RecyclerView.o.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.StaggeredGridLayoutManager.b(int, int, int):void */
    public void a(RecyclerView recyclerView, int i2, int i3, Object obj) {
        b(i2, i3, 4);
    }

    /* JADX WARN: Type inference failed for: r9v0 */
    /* JADX WARN: Type inference failed for: r9v1, types: [boolean, int] */
    /* JADX WARN: Type inference failed for: r9v4 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    private int a(RecyclerView.v vVar, g gVar, RecyclerView.z zVar) {
        int i2;
        int i3;
        int i4;
        d dVar;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        boolean z2;
        int i11;
        int i12;
        int i13;
        RecyclerView.v vVar2 = vVar;
        g gVar2 = gVar;
        ? r9 = 0;
        this.B.set(0, this.s, true);
        if (this.y.f1936i) {
            i2 = gVar2.f1932e == 1 ? Api.BaseClientBuilder.API_PRIORITY_OTHER : Integer.MIN_VALUE;
        } else {
            if (gVar2.f1932e == 1) {
                i13 = gVar2.f1934g + gVar2.f1929b;
            } else {
                i13 = gVar2.f1933f - gVar2.f1929b;
            }
            i2 = i13;
        }
        e(gVar2.f1932e, i2);
        if (this.A) {
            i3 = this.u.b();
        } else {
            i3 = this.u.f();
        }
        int i14 = i3;
        boolean z3 = false;
        while (gVar.a(zVar) && (this.y.f1936i || !this.B.isEmpty())) {
            View a2 = gVar2.a(vVar2);
            c cVar = (c) a2.getLayoutParams();
            int a3 = cVar.a();
            int d2 = this.E.d(a3);
            boolean z4 = d2 == -1;
            if (z4) {
                dVar = cVar.f1830f ? this.t[r9] : a(gVar2);
                this.E.a(a3, dVar);
            } else {
                dVar = this.t[d2];
            }
            d dVar2 = dVar;
            cVar.f1829e = dVar2;
            if (gVar2.f1932e == 1) {
                b(a2);
            } else {
                b(a2, (int) r9);
            }
            a(a2, cVar, (boolean) r9);
            if (gVar2.f1932e == 1) {
                if (cVar.f1830f) {
                    i12 = q(i14);
                } else {
                    i12 = dVar2.a(i14);
                }
                int b2 = this.u.b(a2) + i12;
                if (z4 && cVar.f1830f) {
                    LazySpanLookup.FullSpanItem m = m(i12);
                    m.f1808b = -1;
                    m.f1807a = a3;
                    this.E.a(m);
                }
                i5 = b2;
                i6 = i12;
            } else {
                if (cVar.f1830f) {
                    i11 = t(i14);
                } else {
                    i11 = dVar2.b(i14);
                }
                i6 = i11 - this.u.b(a2);
                if (z4 && cVar.f1830f) {
                    LazySpanLookup.FullSpanItem n = n(i11);
                    n.f1808b = 1;
                    n.f1807a = a3;
                    this.E.a(n);
                }
                i5 = i11;
            }
            if (cVar.f1830f && gVar2.f1931d == -1) {
                if (z4) {
                    this.M = true;
                } else {
                    if (gVar2.f1932e == 1) {
                        z2 = D();
                    } else {
                        z2 = E();
                    }
                    if (!z2) {
                        LazySpanLookup.FullSpanItem c2 = this.E.c(a3);
                        if (c2 != null) {
                            c2.f1810d = true;
                        }
                        this.M = true;
                    }
                }
            }
            a(a2, cVar, gVar2);
            if (!L() || this.w != 1) {
                if (cVar.f1830f) {
                    i9 = this.v.f();
                } else {
                    i9 = (dVar2.f1835e * this.x) + this.v.f();
                }
                i8 = i9;
                i7 = this.v.b(a2) + i9;
            } else {
                if (cVar.f1830f) {
                    i10 = this.v.b();
                } else {
                    i10 = this.v.b() - (((this.s - 1) - dVar2.f1835e) * this.x);
                }
                i7 = i10;
                i8 = i10 - this.v.b(a2);
            }
            if (this.w == 1) {
                a(a2, i8, i6, i7, i5);
            } else {
                a(a2, i6, i8, i5, i7);
            }
            if (cVar.f1830f) {
                e(this.y.f1932e, i2);
            } else {
                a(dVar2, this.y.f1932e, i2);
            }
            a(vVar2, this.y);
            if (this.y.f1935h && a2.hasFocusable()) {
                if (cVar.f1830f) {
                    this.B.clear();
                } else {
                    this.B.set(dVar2.f1835e, false);
                    z3 = true;
                    r9 = 0;
                }
            }
            z3 = true;
            r9 = 0;
        }
        if (!z3) {
            a(vVar2, this.y);
        }
        if (this.y.f1932e == -1) {
            i4 = this.u.f() - t(this.u.f());
        } else {
            i4 = q(this.u.b()) - this.u.b();
        }
        if (i4 > 0) {
            return Math.min(gVar2.f1929b, i4);
        }
        return 0;
    }

    private void a(View view, c cVar, g gVar) {
        if (gVar.f1932e == 1) {
            if (cVar.f1830f) {
                p(view);
            } else {
                cVar.f1829e.a(view);
            }
        } else if (cVar.f1830f) {
            q(view);
        } else {
            cVar.f1829e.c(view);
        }
    }

    private void a(RecyclerView.v vVar, g gVar) {
        int i2;
        int i3;
        if (gVar.f1928a && !gVar.f1936i) {
            if (gVar.f1929b == 0) {
                if (gVar.f1932e == -1) {
                    a(vVar, gVar.f1934g);
                } else {
                    b(vVar, gVar.f1933f);
                }
            } else if (gVar.f1932e == -1) {
                int i4 = gVar.f1933f;
                int r = i4 - r(i4);
                if (r < 0) {
                    i3 = gVar.f1934g;
                } else {
                    i3 = gVar.f1934g - Math.min(r, gVar.f1929b);
                }
                a(vVar, i3);
            } else {
                int s2 = s(gVar.f1934g) - gVar.f1934g;
                if (s2 < 0) {
                    i2 = gVar.f1933f;
                } else {
                    i2 = Math.min(s2, gVar.f1929b) + gVar.f1933f;
                }
                b(vVar, i2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    private void a(d dVar, int i2, int i3) {
        int f2 = dVar.f();
        if (i2 == -1) {
            if (dVar.h() + f2 <= i3) {
                this.B.set(dVar.f1835e, false);
            }
        } else if (dVar.g() - f2 >= i3) {
            this.B.set(dVar.f1835e, false);
        }
    }

    private void a(RecyclerView.v vVar, int i2) {
        int e2 = e() - 1;
        while (e2 >= 0) {
            View c2 = c(e2);
            if (this.u.d(c2) >= i2 && this.u.f(c2) >= i2) {
                c cVar = (c) c2.getLayoutParams();
                if (cVar.f1830f) {
                    int i3 = 0;
                    while (i3 < this.s) {
                        if (this.t[i3].f1831a.size() != 1) {
                            i3++;
                        } else {
                            return;
                        }
                    }
                    for (int i4 = 0; i4 < this.s; i4++) {
                        this.t[i4].j();
                    }
                } else if (cVar.f1829e.f1831a.size() != 1) {
                    cVar.f1829e.j();
                } else {
                    return;
                }
                a(c2, vVar);
                e2--;
            } else {
                return;
            }
        }
    }

    private d a(g gVar) {
        int i2;
        int i3;
        int i4 = -1;
        if (u(gVar.f1932e)) {
            i3 = this.s - 1;
            i2 = -1;
        } else {
            i3 = 0;
            i4 = this.s;
            i2 = 1;
        }
        d dVar = null;
        if (gVar.f1932e == 1) {
            int i5 = Api.BaseClientBuilder.API_PRIORITY_OTHER;
            int f2 = this.u.f();
            while (i3 != i4) {
                d dVar2 = this.t[i3];
                int a2 = dVar2.a(f2);
                if (a2 < i5) {
                    dVar = dVar2;
                    i5 = a2;
                }
                i3 += i2;
            }
            return dVar;
        }
        int i6 = Integer.MIN_VALUE;
        int b2 = this.u.b();
        while (i3 != i4) {
            d dVar3 = this.t[i3];
            int b3 = dVar3.b(b2);
            if (b3 > i6) {
                dVar = dVar3;
                i6 = b3;
            }
            i3 += i2;
        }
        return dVar;
    }

    public boolean a() {
        return this.w == 0;
    }

    public int a(int i2, RecyclerView.v vVar, RecyclerView.z zVar) {
        return c(i2, vVar, zVar);
    }

    public void a(int i2, int i3, RecyclerView.z zVar, RecyclerView.o.c cVar) {
        int i4;
        int i5;
        if (this.w != 0) {
            i2 = i3;
        }
        if (e() != 0 && i2 != 0) {
            a(i2, zVar);
            int[] iArr = this.O;
            if (iArr == null || iArr.length < this.s) {
                this.O = new int[this.s];
            }
            int i6 = 0;
            for (int i7 = 0; i7 < this.s; i7++) {
                g gVar = this.y;
                if (gVar.f1931d == -1) {
                    i5 = gVar.f1933f;
                    i4 = this.t[i7].b(i5);
                } else {
                    i5 = this.t[i7].a(gVar.f1934g);
                    i4 = this.y.f1934g;
                }
                int i8 = i5 - i4;
                if (i8 >= 0) {
                    this.O[i6] = i8;
                    i6++;
                }
            }
            Arrays.sort(this.O, 0, i6);
            for (int i9 = 0; i9 < i6 && this.y.a(zVar); i9++) {
                cVar.a(this.y.f1930c, this.O[i9]);
                g gVar2 = this.y;
                gVar2.f1930c += gVar2.f1931d;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, RecyclerView.z zVar) {
        int i3;
        int i4;
        if (i2 > 0) {
            i4 = I();
            i3 = 1;
        } else {
            i4 = H();
            i3 = -1;
        }
        this.y.f1928a = true;
        b(i4, zVar);
        v(i3);
        g gVar = this.y;
        gVar.f1930c = i4 + gVar.f1931d;
        gVar.f1929b = Math.abs(i2);
    }

    public RecyclerView.p a(Context context, AttributeSet attributeSet) {
        return new c(context, attributeSet);
    }

    public RecyclerView.p a(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new c((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new c(layoutParams);
    }

    public boolean a(RecyclerView.p pVar) {
        return pVar instanceof c;
    }

    public View a(View view, int i2, RecyclerView.v vVar, RecyclerView.z zVar) {
        View c2;
        int i3;
        int i4;
        int i5;
        int i6;
        View a2;
        if (e() == 0 || (c2 = c(view)) == null) {
            return null;
        }
        O();
        int l = l(i2);
        if (l == Integer.MIN_VALUE) {
            return null;
        }
        c cVar = (c) c2.getLayoutParams();
        boolean z2 = cVar.f1830f;
        d dVar = cVar.f1829e;
        if (l == 1) {
            i3 = I();
        } else {
            i3 = H();
        }
        b(i3, zVar);
        v(l);
        g gVar = this.y;
        gVar.f1930c = gVar.f1931d + i3;
        gVar.f1929b = (int) (((float) this.u.g()) * 0.33333334f);
        g gVar2 = this.y;
        gVar2.f1935h = true;
        gVar2.f1928a = false;
        a(vVar, gVar2, zVar);
        this.G = this.A;
        if (!z2 && (a2 = dVar.a(i3, l)) != null && a2 != c2) {
            return a2;
        }
        if (u(l)) {
            for (int i7 = this.s - 1; i7 >= 0; i7--) {
                View a3 = this.t[i7].a(i3, l);
                if (a3 != null && a3 != c2) {
                    return a3;
                }
            }
        } else {
            for (int i8 = 0; i8 < this.s; i8++) {
                View a4 = this.t[i8].a(i3, l);
                if (a4 != null && a4 != c2) {
                    return a4;
                }
            }
        }
        boolean z3 = (this.z ^ true) == (l == -1);
        if (!z2) {
            if (z3) {
                i6 = dVar.d();
            } else {
                i6 = dVar.e();
            }
            View b2 = b(i6);
            if (!(b2 == null || b2 == c2)) {
                return b2;
            }
        }
        if (u(l)) {
            for (int i9 = this.s - 1; i9 >= 0; i9--) {
                if (i9 != dVar.f1835e) {
                    if (z3) {
                        i5 = this.t[i9].d();
                    } else {
                        i5 = this.t[i9].e();
                    }
                    View b3 = b(i5);
                    if (!(b3 == null || b3 == c2)) {
                        return b3;
                    }
                }
            }
        } else {
            for (int i10 = 0; i10 < this.s; i10++) {
                if (z3) {
                    i4 = this.t[i10].d();
                } else {
                    i4 = this.t[i10].e();
                }
                View b4 = b(i4);
                if (b4 != null && b4 != c2) {
                    return b4;
                }
            }
        }
        return null;
    }
}
