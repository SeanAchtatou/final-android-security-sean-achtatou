package androidx.recyclerview.widget;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.h;
import b.e.l.d;
import b.e.l.e;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AdapterHelper */
class a implements h.a {

    /* renamed from: a  reason: collision with root package name */
    private d<b> f1837a;

    /* renamed from: b  reason: collision with root package name */
    final ArrayList<b> f1838b;

    /* renamed from: c  reason: collision with root package name */
    final ArrayList<b> f1839c;

    /* renamed from: d  reason: collision with root package name */
    final C0020a f1840d;

    /* renamed from: e  reason: collision with root package name */
    Runnable f1841e;

    /* renamed from: f  reason: collision with root package name */
    final boolean f1842f;

    /* renamed from: g  reason: collision with root package name */
    final h f1843g;

    /* renamed from: h  reason: collision with root package name */
    private int f1844h;

    /* renamed from: androidx.recyclerview.widget.a$a  reason: collision with other inner class name */
    /* compiled from: AdapterHelper */
    interface C0020a {
        RecyclerView.c0 a(int i2);

        void a(int i2, int i3);

        void a(int i2, int i3, Object obj);

        void a(b bVar);

        void b(int i2, int i3);

        void b(b bVar);

        void c(int i2, int i3);

        void d(int i2, int i3);
    }

    /* compiled from: AdapterHelper */
    static class b {

        /* renamed from: a  reason: collision with root package name */
        int f1845a;

        /* renamed from: b  reason: collision with root package name */
        int f1846b;

        /* renamed from: c  reason: collision with root package name */
        Object f1847c;

        /* renamed from: d  reason: collision with root package name */
        int f1848d;

        b(int i2, int i3, int i4, Object obj) {
            this.f1845a = i2;
            this.f1846b = i3;
            this.f1848d = i4;
            this.f1847c = obj;
        }

        /* access modifiers changed from: package-private */
        public String a() {
            int i2 = this.f1845a;
            if (i2 == 1) {
                return "add";
            }
            if (i2 == 2) {
                return "rm";
            }
            if (i2 != 4) {
                return i2 != 8 ? "??" : "mv";
            }
            return "up";
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            int i2 = this.f1845a;
            if (i2 != bVar.f1845a) {
                return false;
            }
            if (i2 == 8 && Math.abs(this.f1848d - this.f1846b) == 1 && this.f1848d == bVar.f1846b && this.f1846b == bVar.f1848d) {
                return true;
            }
            if (this.f1848d != bVar.f1848d || this.f1846b != bVar.f1846b) {
                return false;
            }
            Object obj2 = this.f1847c;
            if (obj2 != null) {
                if (!obj2.equals(bVar.f1847c)) {
                    return false;
                }
            } else if (bVar.f1847c != null) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (((this.f1845a * 31) + this.f1846b) * 31) + this.f1848d;
        }

        public String toString() {
            return Integer.toHexString(System.identityHashCode(this)) + "[" + a() + ",s:" + this.f1846b + "c:" + this.f1848d + ",p:" + this.f1847c + "]";
        }
    }

    a(C0020a aVar) {
        this(aVar, false);
    }

    private void b(b bVar) {
        g(bVar);
    }

    private void c(b bVar) {
        g(bVar);
    }

    private void d(b bVar) {
        char c2;
        boolean z;
        boolean z2;
        int i2 = bVar.f1846b;
        int i3 = bVar.f1848d + i2;
        int i4 = 0;
        char c3 = 65535;
        int i5 = i2;
        while (i5 < i3) {
            if (this.f1840d.a(i5) != null || d(i5)) {
                if (c3 == 0) {
                    f(a(2, i2, i4, null));
                    z2 = true;
                } else {
                    z2 = false;
                }
                c2 = 1;
            } else {
                if (c3 == 1) {
                    g(a(2, i2, i4, null));
                    z = true;
                } else {
                    z = false;
                }
                c2 = 0;
            }
            if (z) {
                i5 -= i4;
                i3 -= i4;
                i4 = 1;
            } else {
                i4++;
            }
            i5++;
            c3 = c2;
        }
        if (i4 != bVar.f1848d) {
            a(bVar);
            bVar = a(2, i2, i4, null);
        }
        if (c3 == 0) {
            f(bVar);
        } else {
            g(bVar);
        }
    }

    private void g(b bVar) {
        this.f1839c.add(bVar);
        int i2 = bVar.f1845a;
        if (i2 == 1) {
            this.f1840d.c(bVar.f1846b, bVar.f1848d);
        } else if (i2 == 2) {
            this.f1840d.b(bVar.f1846b, bVar.f1848d);
        } else if (i2 == 4) {
            this.f1840d.a(bVar.f1846b, bVar.f1848d, bVar.f1847c);
        } else if (i2 == 8) {
            this.f1840d.a(bVar.f1846b, bVar.f1848d);
        } else {
            throw new IllegalArgumentException("Unknown update op type for " + bVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        int size = this.f1839c.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.f1840d.b(this.f1839c.get(i2));
        }
        a(this.f1839c);
        this.f1844h = 0;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.f1843g.a(this.f1838b);
        int size = this.f1838b.size();
        for (int i2 = 0; i2 < size; i2++) {
            b bVar = this.f1838b.get(i2);
            int i3 = bVar.f1845a;
            if (i3 == 1) {
                b(bVar);
            } else if (i3 == 2) {
                d(bVar);
            } else if (i3 == 4) {
                e(bVar);
            } else if (i3 == 8) {
                c(bVar);
            }
            Runnable runnable = this.f1841e;
            if (runnable != null) {
                runnable.run();
            }
        }
        this.f1838b.clear();
    }

    /* access modifiers changed from: package-private */
    public void f() {
        a(this.f1838b);
        a(this.f1839c);
        this.f1844h = 0;
    }

    a(C0020a aVar, boolean z) {
        this.f1837a = new e(30);
        this.f1838b = new ArrayList<>();
        this.f1839c = new ArrayList<>();
        this.f1844h = 0;
        this.f1840d = aVar;
        this.f1842f = z;
        this.f1843g = new h(this);
    }

    /* access modifiers changed from: package-private */
    public int b(int i2) {
        return a(i2, 0);
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.f1838b.size() > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i2, int i3) {
        if (i3 < 1) {
            return false;
        }
        this.f1838b.add(a(1, i2, i3, null));
        this.f1844h |= 1;
        if (this.f1838b.size() == 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean c(int i2) {
        return (i2 & this.f1844h) != 0;
    }

    private void f(b bVar) {
        int i2;
        int i3 = bVar.f1845a;
        if (i3 == 1 || i3 == 8) {
            throw new IllegalArgumentException("should not dispatch add or move for pre layout");
        }
        int d2 = d(bVar.f1846b, i3);
        int i4 = bVar.f1846b;
        int i5 = bVar.f1845a;
        if (i5 == 2) {
            i2 = 0;
        } else if (i5 == 4) {
            i2 = 1;
        } else {
            throw new IllegalArgumentException("op should be remove or update." + bVar);
        }
        int i6 = d2;
        int i7 = i4;
        int i8 = 1;
        for (int i9 = 1; i9 < bVar.f1848d; i9++) {
            int d3 = d(bVar.f1846b + (i2 * i9), bVar.f1845a);
            int i10 = bVar.f1845a;
            if (i10 == 2 ? d3 == i6 : i10 == 4 && d3 == i6 + 1) {
                i8++;
            } else {
                b a2 = a(bVar.f1845a, i6, i8, bVar.f1847c);
                a(a2, i7);
                a(a2);
                if (bVar.f1845a == 4) {
                    i7 += i8;
                }
                i6 = d3;
                i8 = 1;
            }
        }
        Object obj = bVar.f1847c;
        a(bVar);
        if (i8 > 0) {
            b a3 = a(bVar.f1845a, i6, i8, obj);
            a(a3, i7);
            a(a3);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c(int i2, int i3) {
        if (i3 < 1) {
            return false;
        }
        this.f1838b.add(a(2, i2, i3, null));
        this.f1844h |= 2;
        if (this.f1838b.size() == 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(b bVar, int i2) {
        this.f1840d.a(bVar);
        int i3 = bVar.f1845a;
        if (i3 == 2) {
            this.f1840d.d(i2, bVar.f1848d);
        } else if (i3 == 4) {
            this.f1840d.a(i2, bVar.f1848d, bVar.f1847c);
        } else {
            throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a();
        int size = this.f1838b.size();
        for (int i2 = 0; i2 < size; i2++) {
            b bVar = this.f1838b.get(i2);
            int i3 = bVar.f1845a;
            if (i3 == 1) {
                this.f1840d.b(bVar);
                this.f1840d.c(bVar.f1846b, bVar.f1848d);
            } else if (i3 == 2) {
                this.f1840d.b(bVar);
                this.f1840d.d(bVar.f1846b, bVar.f1848d);
            } else if (i3 == 4) {
                this.f1840d.b(bVar);
                this.f1840d.a(bVar.f1846b, bVar.f1848d, bVar.f1847c);
            } else if (i3 == 8) {
                this.f1840d.b(bVar);
                this.f1840d.a(bVar.f1846b, bVar.f1848d);
            }
            Runnable runnable = this.f1841e;
            if (runnable != null) {
                runnable.run();
            }
        }
        a(this.f1838b);
        this.f1844h = 0;
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, int i3) {
        int size = this.f1839c.size();
        while (i3 < size) {
            b bVar = this.f1839c.get(i3);
            int i4 = bVar.f1845a;
            if (i4 == 8) {
                int i5 = bVar.f1846b;
                if (i5 == i2) {
                    i2 = bVar.f1848d;
                } else {
                    if (i5 < i2) {
                        i2--;
                    }
                    if (bVar.f1848d <= i2) {
                        i2++;
                    }
                }
            } else {
                int i6 = bVar.f1846b;
                if (i6 > i2) {
                    continue;
                } else if (i4 == 2) {
                    int i7 = bVar.f1848d;
                    if (i2 < i6 + i7) {
                        return -1;
                    }
                    i2 -= i7;
                } else if (i4 == 1) {
                    i2 += bVar.f1848d;
                }
            }
            i3++;
        }
        return i2;
    }

    private void e(b bVar) {
        int i2 = bVar.f1846b;
        int i3 = bVar.f1848d + i2;
        int i4 = i2;
        int i5 = 0;
        char c2 = 65535;
        while (i2 < i3) {
            if (this.f1840d.a(i2) != null || d(i2)) {
                if (c2 == 0) {
                    f(a(4, i4, i5, bVar.f1847c));
                    i4 = i2;
                    i5 = 0;
                }
                c2 = 1;
            } else {
                if (c2 == 1) {
                    g(a(4, i4, i5, bVar.f1847c));
                    i4 = i2;
                    i5 = 0;
                }
                c2 = 0;
            }
            i5++;
            i2++;
        }
        if (i5 != bVar.f1848d) {
            Object obj = bVar.f1847c;
            a(bVar);
            bVar = a(4, i4, i5, obj);
        }
        if (c2 == 0) {
            f(bVar);
        } else {
            g(bVar);
        }
    }

    private int d(int i2, int i3) {
        for (int size = this.f1839c.size() - 1; size >= 0; size--) {
            b bVar = this.f1839c.get(size);
            int i4 = bVar.f1845a;
            if (i4 == 8) {
                int i5 = bVar.f1846b;
                int i6 = bVar.f1848d;
                if (i5 >= i6) {
                    int i7 = i6;
                    i6 = i5;
                    i5 = i7;
                }
                if (i2 < i5 || i2 > i6) {
                    int i8 = bVar.f1846b;
                    if (i2 < i8) {
                        if (i3 == 1) {
                            bVar.f1846b = i8 + 1;
                            bVar.f1848d++;
                        } else if (i3 == 2) {
                            bVar.f1846b = i8 - 1;
                            bVar.f1848d--;
                        }
                    }
                } else {
                    int i9 = bVar.f1846b;
                    if (i5 == i9) {
                        if (i3 == 1) {
                            bVar.f1848d++;
                        } else if (i3 == 2) {
                            bVar.f1848d--;
                        }
                        i2++;
                    } else {
                        if (i3 == 1) {
                            bVar.f1846b = i9 + 1;
                        } else if (i3 == 2) {
                            bVar.f1846b = i9 - 1;
                        }
                        i2--;
                    }
                }
            } else {
                int i10 = bVar.f1846b;
                if (i10 <= i2) {
                    if (i4 == 1) {
                        i2 -= bVar.f1848d;
                    } else if (i4 == 2) {
                        i2 += bVar.f1848d;
                    }
                } else if (i3 == 1) {
                    bVar.f1846b = i10 + 1;
                } else if (i3 == 2) {
                    bVar.f1846b = i10 - 1;
                }
            }
        }
        for (int size2 = this.f1839c.size() - 1; size2 >= 0; size2--) {
            b bVar2 = this.f1839c.get(size2);
            if (bVar2.f1845a == 8) {
                int i11 = bVar2.f1848d;
                if (i11 == bVar2.f1846b || i11 < 0) {
                    this.f1839c.remove(size2);
                    a(bVar2);
                }
            } else if (bVar2.f1848d <= 0) {
                this.f1839c.remove(size2);
                a(bVar2);
            }
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, int i3, Object obj) {
        if (i3 < 1) {
            return false;
        }
        this.f1838b.add(a(4, i2, i3, obj));
        this.f1844h |= 4;
        if (this.f1838b.size() == 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, int i3, int i4) {
        if (i2 == i3) {
            return false;
        }
        if (i4 == 1) {
            this.f1838b.add(a(8, i2, i3, null));
            this.f1844h |= 8;
            if (this.f1838b.size() == 1) {
                return true;
            }
            return false;
        }
        throw new IllegalArgumentException("Moving more than 1 item is not supported yet");
    }

    public int a(int i2) {
        int size = this.f1838b.size();
        for (int i3 = 0; i3 < size; i3++) {
            b bVar = this.f1838b.get(i3);
            int i4 = bVar.f1845a;
            if (i4 != 1) {
                if (i4 == 2) {
                    int i5 = bVar.f1846b;
                    if (i5 <= i2) {
                        int i6 = bVar.f1848d;
                        if (i5 + i6 > i2) {
                            return -1;
                        }
                        i2 -= i6;
                    } else {
                        continue;
                    }
                } else if (i4 == 8) {
                    int i7 = bVar.f1846b;
                    if (i7 == i2) {
                        i2 = bVar.f1848d;
                    } else {
                        if (i7 < i2) {
                            i2--;
                        }
                        if (bVar.f1848d <= i2) {
                            i2++;
                        }
                    }
                }
            } else if (bVar.f1846b <= i2) {
                i2 += bVar.f1848d;
            }
        }
        return i2;
    }

    public b a(int i2, int i3, int i4, Object obj) {
        b a2 = this.f1837a.a();
        if (a2 == null) {
            return new b(i2, i3, i4, obj);
        }
        a2.f1845a = i2;
        a2.f1846b = i3;
        a2.f1848d = i4;
        a2.f1847c = obj;
        return a2;
    }

    private boolean d(int i2) {
        int size = this.f1839c.size();
        for (int i3 = 0; i3 < size; i3++) {
            b bVar = this.f1839c.get(i3);
            int i4 = bVar.f1845a;
            if (i4 == 8) {
                if (a(bVar.f1848d, i3 + 1) == i2) {
                    return true;
                }
            } else if (i4 == 1) {
                int i5 = bVar.f1846b;
                int i6 = bVar.f1848d + i5;
                while (i5 < i6) {
                    if (a(i5, i3 + 1) == i2) {
                        return true;
                    }
                    i5++;
                }
                continue;
            } else {
                continue;
            }
        }
        return false;
    }

    public void a(b bVar) {
        if (!this.f1842f) {
            bVar.f1847c = null;
            this.f1837a.a(bVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(List<b> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            a(list.get(i2));
        }
        list.clear();
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return !this.f1839c.isEmpty() && !this.f1838b.isEmpty();
    }
}
