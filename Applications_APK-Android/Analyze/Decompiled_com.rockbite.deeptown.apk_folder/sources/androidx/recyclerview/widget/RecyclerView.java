package androidx.recyclerview.widget;

import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Observable;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.FocusFinder;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.OverScroller;
import androidx.customview.view.AbsSavedState;
import androidx.recyclerview.widget.a;
import androidx.recyclerview.widget.b;
import androidx.recyclerview.widget.e;
import androidx.recyclerview.widget.j;
import androidx.recyclerview.widget.m;
import androidx.recyclerview.widget.n;
import b.e.m.d0.c;
import com.esotericsoftware.spine.Animation;
import com.facebook.internal.Utility;
import com.google.android.gms.common.api.Api;
import com.google.protobuf.CodedOutputStream;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecyclerView extends ViewGroup implements b.e.m.s, b.e.m.i, b.e.m.j {
    static final boolean A0 = (Build.VERSION.SDK_INT >= 23);
    static final boolean B0 = (Build.VERSION.SDK_INT >= 16);
    static final boolean C0 = (Build.VERSION.SDK_INT >= 21);
    private static final boolean D0 = (Build.VERSION.SDK_INT <= 15);
    private static final boolean E0 = (Build.VERSION.SDK_INT <= 15);
    private static final Class<?>[] F0;
    static final Interpolator G0 = new c();
    private static final int[] y0 = {16843830};
    static final boolean z0;
    boolean A;
    private final AccessibilityManager B;
    private List<q> C;
    boolean D;
    boolean E;
    private int F;
    private int G;
    private k H;
    private EdgeEffect I;
    private EdgeEffect J;
    private EdgeEffect K;
    private EdgeEffect L;
    l M;
    private int N;
    private int O;
    private VelocityTracker P;
    private int Q;
    private int R;
    private int S;
    private int T;
    private int U;
    private r V;
    private final int W;

    /* renamed from: a  reason: collision with root package name */
    private final x f1724a;
    private final int a0;

    /* renamed from: b  reason: collision with root package name */
    final v f1725b;
    private float b0;

    /* renamed from: c  reason: collision with root package name */
    private SavedState f1726c;
    private float c0;

    /* renamed from: d  reason: collision with root package name */
    a f1727d;
    private boolean d0;

    /* renamed from: e  reason: collision with root package name */
    b f1728e;
    final b0 e0;

    /* renamed from: f  reason: collision with root package name */
    final n f1729f;
    e f0;

    /* renamed from: g  reason: collision with root package name */
    boolean f1730g;
    e.b g0;

    /* renamed from: h  reason: collision with root package name */
    final Runnable f1731h;
    final z h0;

    /* renamed from: i  reason: collision with root package name */
    final Rect f1732i;
    private t i0;

    /* renamed from: j  reason: collision with root package name */
    private final Rect f1733j;
    private List<t> j0;

    /* renamed from: k  reason: collision with root package name */
    final RectF f1734k;
    boolean k0;
    g l;
    boolean l0;
    o m;
    private l.b m0;
    w n;
    boolean n0;
    final ArrayList<n> o;
    j o0;
    private final ArrayList<s> p;
    private j p0;
    private s q;
    private final int[] q0;
    boolean r;
    private b.e.m.l r0;
    boolean s;
    private final int[] s0;
    boolean t;
    private final int[] t0;
    boolean u;
    final int[] u0;
    private int v;
    final List<c0> v0;
    boolean w;
    private Runnable w0;
    boolean x;
    private final n.b x0;
    private boolean y;
    private int z;

    class a implements Runnable {
        a() {
        }

        public void run() {
            RecyclerView recyclerView = RecyclerView.this;
            if (recyclerView.u && !recyclerView.isLayoutRequested()) {
                RecyclerView recyclerView2 = RecyclerView.this;
                if (!recyclerView2.r) {
                    recyclerView2.requestLayout();
                } else if (recyclerView2.x) {
                    recyclerView2.w = true;
                } else {
                    recyclerView2.b();
                }
            }
        }
    }

    public static abstract class a0 {
        public abstract View a(v vVar, int i2, int i3);
    }

    class b implements Runnable {
        b() {
        }

        public void run() {
            l lVar = RecyclerView.this.M;
            if (lVar != null) {
                lVar.i();
            }
            RecyclerView.this.n0 = false;
        }
    }

    static class c implements Interpolator {
        c() {
        }

        public float getInterpolation(float f2) {
            float f3 = f2 - 1.0f;
            return (f3 * f3 * f3 * f3 * f3) + 1.0f;
        }
    }

    public static abstract class c0 {
        static final int FLAG_ADAPTER_FULLUPDATE = 1024;
        static final int FLAG_ADAPTER_POSITION_UNKNOWN = 512;
        static final int FLAG_APPEARED_IN_PRE_LAYOUT = 4096;
        static final int FLAG_BOUNCED_FROM_HIDDEN_LIST = 8192;
        static final int FLAG_BOUND = 1;
        static final int FLAG_IGNORE = 128;
        static final int FLAG_INVALID = 4;
        static final int FLAG_MOVED = 2048;
        static final int FLAG_NOT_RECYCLABLE = 16;
        static final int FLAG_REMOVED = 8;
        static final int FLAG_RETURNED_FROM_SCRAP = 32;
        static final int FLAG_TMP_DETACHED = 256;
        static final int FLAG_UPDATE = 2;
        private static final List<Object> FULLUPDATE_PAYLOADS = Collections.emptyList();
        static final int PENDING_ACCESSIBILITY_STATE_NOT_SET = -1;
        public final View itemView;
        int mFlags;
        boolean mInChangeScrap = false;
        private int mIsRecyclableCount = 0;
        long mItemId = -1;
        int mItemViewType = -1;
        WeakReference<RecyclerView> mNestedRecyclerView;
        int mOldPosition = -1;
        RecyclerView mOwnerRecyclerView;
        List<Object> mPayloads = null;
        int mPendingAccessibilityState = -1;
        int mPosition = -1;
        int mPreLayoutPosition = -1;
        v mScrapContainer = null;
        c0 mShadowedHolder = null;
        c0 mShadowingHolder = null;
        List<Object> mUnmodifiedPayloads = null;
        private int mWasImportantForAccessibilityBeforeHidden = 0;

        public c0(View view) {
            if (view != null) {
                this.itemView = view;
                return;
            }
            throw new IllegalArgumentException("itemView may not be null");
        }

        private void createPayloadsIfNeeded() {
            if (this.mPayloads == null) {
                this.mPayloads = new ArrayList();
                this.mUnmodifiedPayloads = Collections.unmodifiableList(this.mPayloads);
            }
        }

        /* access modifiers changed from: package-private */
        public void addChangePayload(Object obj) {
            if (obj == null) {
                addFlags(FLAG_ADAPTER_FULLUPDATE);
            } else if ((FLAG_ADAPTER_FULLUPDATE & this.mFlags) == 0) {
                createPayloadsIfNeeded();
                this.mPayloads.add(obj);
            }
        }

        /* access modifiers changed from: package-private */
        public void addFlags(int i2) {
            this.mFlags = i2 | this.mFlags;
        }

        /* access modifiers changed from: package-private */
        public void clearOldPosition() {
            this.mOldPosition = -1;
            this.mPreLayoutPosition = -1;
        }

        /* access modifiers changed from: package-private */
        public void clearPayload() {
            List<Object> list = this.mPayloads;
            if (list != null) {
                list.clear();
            }
            this.mFlags &= -1025;
        }

        /* access modifiers changed from: package-private */
        public void clearReturnedFromScrapFlag() {
            this.mFlags &= -33;
        }

        /* access modifiers changed from: package-private */
        public void clearTmpDetachFlag() {
            this.mFlags &= -257;
        }

        /* access modifiers changed from: package-private */
        public boolean doesTransientStatePreventRecycling() {
            return (this.mFlags & 16) == 0 && b.e.m.u.q(this.itemView);
        }

        /* access modifiers changed from: package-private */
        public void flagRemovedAndOffsetPosition(int i2, int i3, boolean z) {
            addFlags(8);
            offsetPosition(i3, z);
            this.mPosition = i2;
        }

        public final int getAdapterPosition() {
            RecyclerView recyclerView = this.mOwnerRecyclerView;
            if (recyclerView == null) {
                return -1;
            }
            return recyclerView.b(this);
        }

        public final long getItemId() {
            return this.mItemId;
        }

        public final int getItemViewType() {
            return this.mItemViewType;
        }

        public final int getLayoutPosition() {
            int i2 = this.mPreLayoutPosition;
            return i2 == -1 ? this.mPosition : i2;
        }

        public final int getOldPosition() {
            return this.mOldPosition;
        }

        @Deprecated
        public final int getPosition() {
            int i2 = this.mPreLayoutPosition;
            return i2 == -1 ? this.mPosition : i2;
        }

        /* access modifiers changed from: package-private */
        public List<Object> getUnmodifiedPayloads() {
            if ((this.mFlags & FLAG_ADAPTER_FULLUPDATE) != 0) {
                return FULLUPDATE_PAYLOADS;
            }
            List<Object> list = this.mPayloads;
            if (list == null || list.size() == 0) {
                return FULLUPDATE_PAYLOADS;
            }
            return this.mUnmodifiedPayloads;
        }

        /* access modifiers changed from: package-private */
        public boolean hasAnyOfTheFlags(int i2) {
            return (i2 & this.mFlags) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isAdapterPositionUnknown() {
            return (this.mFlags & 512) != 0 || isInvalid();
        }

        /* access modifiers changed from: package-private */
        public boolean isAttachedToTransitionOverlay() {
            return (this.itemView.getParent() == null || this.itemView.getParent() == this.mOwnerRecyclerView) ? false : true;
        }

        /* access modifiers changed from: package-private */
        public boolean isBound() {
            return (this.mFlags & 1) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isInvalid() {
            return (this.mFlags & 4) != 0;
        }

        public final boolean isRecyclable() {
            return (this.mFlags & 16) == 0 && !b.e.m.u.q(this.itemView);
        }

        /* access modifiers changed from: package-private */
        public boolean isRemoved() {
            return (this.mFlags & 8) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isScrap() {
            return this.mScrapContainer != null;
        }

        /* access modifiers changed from: package-private */
        public boolean isTmpDetached() {
            return (this.mFlags & 256) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isUpdated() {
            return (this.mFlags & 2) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean needsUpdate() {
            return (this.mFlags & 2) != 0;
        }

        /* access modifiers changed from: package-private */
        public void offsetPosition(int i2, boolean z) {
            if (this.mOldPosition == -1) {
                this.mOldPosition = this.mPosition;
            }
            if (this.mPreLayoutPosition == -1) {
                this.mPreLayoutPosition = this.mPosition;
            }
            if (z) {
                this.mPreLayoutPosition += i2;
            }
            this.mPosition += i2;
            if (this.itemView.getLayoutParams() != null) {
                ((p) this.itemView.getLayoutParams()).f1776c = true;
            }
        }

        /* access modifiers changed from: package-private */
        public void onEnteredHiddenState(RecyclerView recyclerView) {
            int i2 = this.mPendingAccessibilityState;
            if (i2 != -1) {
                this.mWasImportantForAccessibilityBeforeHidden = i2;
            } else {
                this.mWasImportantForAccessibilityBeforeHidden = b.e.m.u.i(this.itemView);
            }
            recyclerView.a(this, 4);
        }

        /* access modifiers changed from: package-private */
        public void onLeftHiddenState(RecyclerView recyclerView) {
            recyclerView.a(this, this.mWasImportantForAccessibilityBeforeHidden);
            this.mWasImportantForAccessibilityBeforeHidden = 0;
        }

        /* access modifiers changed from: package-private */
        public void resetInternal() {
            this.mFlags = 0;
            this.mPosition = -1;
            this.mOldPosition = -1;
            this.mItemId = -1;
            this.mPreLayoutPosition = -1;
            this.mIsRecyclableCount = 0;
            this.mShadowedHolder = null;
            this.mShadowingHolder = null;
            clearPayload();
            this.mWasImportantForAccessibilityBeforeHidden = 0;
            this.mPendingAccessibilityState = -1;
            RecyclerView.e(this);
        }

        /* access modifiers changed from: package-private */
        public void saveOldPosition() {
            if (this.mOldPosition == -1) {
                this.mOldPosition = this.mPosition;
            }
        }

        /* access modifiers changed from: package-private */
        public void setFlags(int i2, int i3) {
            this.mFlags = (i2 & i3) | (this.mFlags & (i3 ^ -1));
        }

        public final void setIsRecyclable(boolean z) {
            int i2 = this.mIsRecyclableCount;
            this.mIsRecyclableCount = z ? i2 - 1 : i2 + 1;
            int i3 = this.mIsRecyclableCount;
            if (i3 < 0) {
                this.mIsRecyclableCount = 0;
                Log.e("View", "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for " + this);
            } else if (!z && i3 == 1) {
                this.mFlags |= 16;
            } else if (z && this.mIsRecyclableCount == 0) {
                this.mFlags &= -17;
            }
        }

        /* access modifiers changed from: package-private */
        public void setScrapContainer(v vVar, boolean z) {
            this.mScrapContainer = vVar;
            this.mInChangeScrap = z;
        }

        /* access modifiers changed from: package-private */
        public boolean shouldBeKeptAsChild() {
            return (this.mFlags & 16) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean shouldIgnore() {
            return (this.mFlags & FLAG_IGNORE) != 0;
        }

        /* access modifiers changed from: package-private */
        public void stopIgnoring() {
            this.mFlags &= -129;
        }

        public String toString() {
            String simpleName = getClass().isAnonymousClass() ? "ViewHolder" : getClass().getSimpleName();
            StringBuilder sb = new StringBuilder(simpleName + "{" + Integer.toHexString(hashCode()) + " position=" + this.mPosition + " id=" + this.mItemId + ", oldPos=" + this.mOldPosition + ", pLpos:" + this.mPreLayoutPosition);
            if (isScrap()) {
                sb.append(" scrap ");
                sb.append(this.mInChangeScrap ? "[changeScrap]" : "[attachedScrap]");
            }
            if (isInvalid()) {
                sb.append(" invalid");
            }
            if (!isBound()) {
                sb.append(" unbound");
            }
            if (needsUpdate()) {
                sb.append(" update");
            }
            if (isRemoved()) {
                sb.append(" removed");
            }
            if (shouldIgnore()) {
                sb.append(" ignored");
            }
            if (isTmpDetached()) {
                sb.append(" tmpDetached");
            }
            if (!isRecyclable()) {
                sb.append(" not recyclable(" + this.mIsRecyclableCount + ")");
            }
            if (isAdapterPositionUnknown()) {
                sb.append(" undefined adapter position");
            }
            if (this.itemView.getParent() == null) {
                sb.append(" no parent");
            }
            sb.append("}");
            return sb.toString();
        }

        /* access modifiers changed from: package-private */
        public void unScrap() {
            this.mScrapContainer.c(this);
        }

        /* access modifiers changed from: package-private */
        public boolean wasReturnedFromScrap() {
            return (this.mFlags & 32) != 0;
        }
    }

    class d implements n.b {
        d() {
        }

        public void a(c0 c0Var, l.c cVar, l.c cVar2) {
            RecyclerView.this.a(c0Var, cVar, cVar2);
        }

        public void b(c0 c0Var, l.c cVar, l.c cVar2) {
            RecyclerView.this.f1725b.c(c0Var);
            RecyclerView.this.b(c0Var, cVar, cVar2);
        }

        public void c(c0 c0Var, l.c cVar, l.c cVar2) {
            c0Var.setIsRecyclable(false);
            RecyclerView recyclerView = RecyclerView.this;
            if (recyclerView.D) {
                if (recyclerView.M.a(c0Var, c0Var, cVar, cVar2)) {
                    RecyclerView.this.s();
                }
            } else if (recyclerView.M.c(c0Var, cVar, cVar2)) {
                RecyclerView.this.s();
            }
        }

        public void a(c0 c0Var) {
            RecyclerView recyclerView = RecyclerView.this;
            recyclerView.m.a(c0Var.itemView, recyclerView.f1725b);
        }
    }

    class e implements b.C0021b {
        e() {
        }

        public int a() {
            return RecyclerView.this.getChildCount();
        }

        public void addView(View view, int i2) {
            RecyclerView.this.addView(view, i2);
            RecyclerView.this.a(view);
        }

        public int b(View view) {
            return RecyclerView.this.indexOfChild(view);
        }

        public void c(int i2) {
            View childAt = RecyclerView.this.getChildAt(i2);
            if (childAt != null) {
                RecyclerView.this.b(childAt);
                childAt.clearAnimation();
            }
            RecyclerView.this.removeViewAt(i2);
        }

        public void d(View view) {
            c0 k2 = RecyclerView.k(view);
            if (k2 != null) {
                k2.onLeftHiddenState(RecyclerView.this);
            }
        }

        public View a(int i2) {
            return RecyclerView.this.getChildAt(i2);
        }

        public void b() {
            int a2 = a();
            for (int i2 = 0; i2 < a2; i2++) {
                View a3 = a(i2);
                RecyclerView.this.b(a3);
                a3.clearAnimation();
            }
            RecyclerView.this.removeAllViews();
        }

        public void a(View view, int i2, ViewGroup.LayoutParams layoutParams) {
            c0 k2 = RecyclerView.k(view);
            if (k2 != null) {
                if (k2.isTmpDetached() || k2.shouldIgnore()) {
                    k2.clearTmpDetachFlag();
                } else {
                    throw new IllegalArgumentException("Called attach on a child which is not detached: " + k2 + RecyclerView.this.i());
                }
            }
            RecyclerView.this.attachViewToParent(view, i2, layoutParams);
        }

        public c0 c(View view) {
            return RecyclerView.k(view);
        }

        public void b(int i2) {
            c0 k2;
            View a2 = a(i2);
            if (!(a2 == null || (k2 = RecyclerView.k(a2)) == null)) {
                if (!k2.isTmpDetached() || k2.shouldIgnore()) {
                    k2.addFlags(256);
                } else {
                    throw new IllegalArgumentException("called detach on an already detached child " + k2 + RecyclerView.this.i());
                }
            }
            RecyclerView.this.detachViewFromParent(i2);
        }

        public void a(View view) {
            c0 k2 = RecyclerView.k(view);
            if (k2 != null) {
                k2.onEnteredHiddenState(RecyclerView.this);
            }
        }
    }

    public static abstract class g<VH extends c0> {
        private boolean mHasStableIds = false;
        private final h mObservable = new h();

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: VH
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public final void bindViewHolder(VH r3, int r4) {
            /*
                r2 = this;
                r3.mPosition = r4
                boolean r0 = r2.hasStableIds()
                if (r0 == 0) goto L_0x000e
                long r0 = r2.getItemId(r4)
                r3.mItemId = r0
            L_0x000e:
                r0 = 519(0x207, float:7.27E-43)
                r1 = 1
                r3.setFlags(r1, r0)
                java.lang.String r0 = "RV OnBindView"
                b.e.i.c.a(r0)
                java.util.List r0 = r3.getUnmodifiedPayloads()
                r2.onBindViewHolder(r3, r4, r0)
                r3.clearPayload()
                android.view.View r3 = r3.itemView
                android.view.ViewGroup$LayoutParams r3 = r3.getLayoutParams()
                boolean r4 = r3 instanceof androidx.recyclerview.widget.RecyclerView.p
                if (r4 == 0) goto L_0x0031
                androidx.recyclerview.widget.RecyclerView$p r3 = (androidx.recyclerview.widget.RecyclerView.p) r3
                r3.f1776c = r1
            L_0x0031:
                b.e.i.c.a()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.g.bindViewHolder(androidx.recyclerview.widget.RecyclerView$c0, int):void");
        }

        public final VH createViewHolder(ViewGroup viewGroup, int i2) {
            try {
                b.e.i.c.a("RV CreateView");
                VH onCreateViewHolder = onCreateViewHolder(viewGroup, i2);
                if (onCreateViewHolder.itemView.getParent() == null) {
                    onCreateViewHolder.mItemViewType = i2;
                    return onCreateViewHolder;
                }
                throw new IllegalStateException("ViewHolder views must not be attached when created. Ensure that you are not passing 'true' to the attachToRoot parameter of LayoutInflater.inflate(..., boolean attachToRoot)");
            } finally {
                b.e.i.c.a();
            }
        }

        public abstract int getItemCount();

        public long getItemId(int i2) {
            return -1;
        }

        public int getItemViewType(int i2) {
            return 0;
        }

        public final boolean hasObservers() {
            return this.mObservable.a();
        }

        public final boolean hasStableIds() {
            return this.mHasStableIds;
        }

        public final void notifyDataSetChanged() {
            this.mObservable.b();
        }

        public final void notifyItemChanged(int i2) {
            this.mObservable.b(i2, 1);
        }

        public final void notifyItemInserted(int i2) {
            this.mObservable.c(i2, 1);
        }

        public final void notifyItemMoved(int i2, int i3) {
            this.mObservable.a(i2, i3);
        }

        public final void notifyItemRangeChanged(int i2, int i3) {
            this.mObservable.b(i2, i3);
        }

        public final void notifyItemRangeInserted(int i2, int i3) {
            this.mObservable.c(i2, i3);
        }

        public final void notifyItemRangeRemoved(int i2, int i3) {
            this.mObservable.d(i2, i3);
        }

        public final void notifyItemRemoved(int i2) {
            this.mObservable.d(i2, 1);
        }

        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        }

        public abstract void onBindViewHolder(VH vh, int i2);

        public void onBindViewHolder(VH vh, int i2, List<Object> list) {
            onBindViewHolder(vh, i2);
        }

        public abstract VH onCreateViewHolder(ViewGroup viewGroup, int i2);

        public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        }

        public boolean onFailedToRecycleView(VH vh) {
            return false;
        }

        public void onViewAttachedToWindow(VH vh) {
        }

        public void onViewDetachedFromWindow(VH vh) {
        }

        public void onViewRecycled(VH vh) {
        }

        public void registerAdapterDataObserver(i iVar) {
            this.mObservable.registerObserver(iVar);
        }

        public void setHasStableIds(boolean z) {
            if (!hasObservers()) {
                this.mHasStableIds = z;
                return;
            }
            throw new IllegalStateException("Cannot change whether this adapter has stable IDs while the adapter has registered observers.");
        }

        public void unregisterAdapterDataObserver(i iVar) {
            this.mObservable.unregisterObserver(iVar);
        }

        public final void notifyItemChanged(int i2, Object obj) {
            this.mObservable.a(i2, 1, obj);
        }

        public final void notifyItemRangeChanged(int i2, int i3, Object obj) {
            this.mObservable.a(i2, i3, obj);
        }
    }

    static class h extends Observable<i> {
        h() {
        }

        public boolean a() {
            return !this.mObservers.isEmpty();
        }

        public void b() {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((i) this.mObservers.get(size)).a();
            }
        }

        public void c(int i2, int i3) {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((i) this.mObservers.get(size)).b(i2, i3);
            }
        }

        public void d(int i2, int i3) {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((i) this.mObservers.get(size)).c(i2, i3);
            }
        }

        public void a(int i2, int i3, Object obj) {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((i) this.mObservers.get(size)).a(i2, i3, obj);
            }
        }

        public void b(int i2, int i3) {
            a(i2, i3, null);
        }

        public void a(int i2, int i3) {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((i) this.mObservers.get(size)).a(i2, i3, 1);
            }
        }
    }

    public static abstract class i {
        public void a() {
        }

        public void a(int i2, int i3) {
        }

        public void a(int i2, int i3, int i4) {
        }

        public void a(int i2, int i3, Object obj) {
            a(i2, i3);
        }

        public void b(int i2, int i3) {
        }

        public void c(int i2, int i3) {
        }
    }

    public interface j {
        int a(int i2, int i3);
    }

    public static class k {
        /* access modifiers changed from: protected */
        public EdgeEffect a(RecyclerView recyclerView, int i2) {
            return new EdgeEffect(recyclerView.getContext());
        }
    }

    public static abstract class l {

        /* renamed from: a  reason: collision with root package name */
        private b f1748a = null;

        /* renamed from: b  reason: collision with root package name */
        private ArrayList<a> f1749b = new ArrayList<>();

        /* renamed from: c  reason: collision with root package name */
        private long f1750c = 120;

        /* renamed from: d  reason: collision with root package name */
        private long f1751d = 120;

        /* renamed from: e  reason: collision with root package name */
        private long f1752e = 250;

        /* renamed from: f  reason: collision with root package name */
        private long f1753f = 250;

        public interface a {
            void a();
        }

        interface b {
            void a(c0 c0Var);
        }

        public static class c {

            /* renamed from: a  reason: collision with root package name */
            public int f1754a;

            /* renamed from: b  reason: collision with root package name */
            public int f1755b;

            public c a(c0 c0Var) {
                a(c0Var, 0);
                return this;
            }

            public c a(c0 c0Var, int i2) {
                View view = c0Var.itemView;
                this.f1754a = view.getLeft();
                this.f1755b = view.getTop();
                view.getRight();
                view.getBottom();
                return this;
            }
        }

        /* access modifiers changed from: package-private */
        public void a(b bVar) {
            this.f1748a = bVar;
        }

        public abstract boolean a(c0 c0Var);

        public abstract boolean a(c0 c0Var, c0 c0Var2, c cVar, c cVar2);

        public abstract boolean a(c0 c0Var, c cVar, c cVar2);

        public abstract void b();

        public final void b(c0 c0Var) {
            d(c0Var);
            b bVar = this.f1748a;
            if (bVar != null) {
                bVar.a(c0Var);
            }
        }

        public abstract boolean b(c0 c0Var, c cVar, c cVar2);

        public long c() {
            return this.f1750c;
        }

        public abstract void c(c0 c0Var);

        public abstract boolean c(c0 c0Var, c cVar, c cVar2);

        public long d() {
            return this.f1753f;
        }

        public void d(c0 c0Var) {
        }

        public long e() {
            return this.f1752e;
        }

        public long f() {
            return this.f1751d;
        }

        public abstract boolean g();

        public c h() {
            return new c();
        }

        public abstract void i();

        static int e(c0 c0Var) {
            int i2 = c0Var.mFlags & 14;
            if (c0Var.isInvalid()) {
                return 4;
            }
            if ((i2 & 4) != 0) {
                return i2;
            }
            int oldPosition = c0Var.getOldPosition();
            int adapterPosition = c0Var.getAdapterPosition();
            return (oldPosition == -1 || adapterPosition == -1 || oldPosition == adapterPosition) ? i2 : i2 | 2048;
        }

        public c a(z zVar, c0 c0Var, int i2, List<Object> list) {
            c h2 = h();
            h2.a(c0Var);
            return h2;
        }

        public c a(z zVar, c0 c0Var) {
            c h2 = h();
            h2.a(c0Var);
            return h2;
        }

        public boolean a(c0 c0Var, List<Object> list) {
            return a(c0Var);
        }

        public final void a() {
            int size = this.f1749b.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.f1749b.get(i2).a();
            }
            this.f1749b.clear();
        }
    }

    private class m implements l.b {
        m() {
        }

        public void a(c0 c0Var) {
            c0Var.setIsRecyclable(true);
            if (c0Var.mShadowedHolder != null && c0Var.mShadowingHolder == null) {
                c0Var.mShadowedHolder = null;
            }
            c0Var.mShadowingHolder = null;
            if (!c0Var.shouldBeKeptAsChild() && !RecyclerView.this.i(c0Var.itemView) && c0Var.isTmpDetached()) {
                RecyclerView.this.removeDetachedView(c0Var.itemView, false);
            }
        }
    }

    public static abstract class n {
        @Deprecated
        public void a(Canvas canvas, RecyclerView recyclerView) {
        }

        public void a(Canvas canvas, RecyclerView recyclerView, z zVar) {
            a(canvas, recyclerView);
        }

        @Deprecated
        public void b(Canvas canvas, RecyclerView recyclerView) {
        }

        public void b(Canvas canvas, RecyclerView recyclerView, z zVar) {
            b(canvas, recyclerView);
        }

        @Deprecated
        public void a(Rect rect, int i2, RecyclerView recyclerView) {
            rect.set(0, 0, 0, 0);
        }

        public void a(Rect rect, View view, RecyclerView recyclerView, z zVar) {
            a(rect, ((p) view.getLayoutParams()).a(), recyclerView);
        }
    }

    public static abstract class o {

        /* renamed from: a  reason: collision with root package name */
        b f1757a;

        /* renamed from: b  reason: collision with root package name */
        RecyclerView f1758b;

        /* renamed from: c  reason: collision with root package name */
        private final m.b f1759c = new a();

        /* renamed from: d  reason: collision with root package name */
        private final m.b f1760d = new b();

        /* renamed from: e  reason: collision with root package name */
        m f1761e = new m(this.f1759c);

        /* renamed from: f  reason: collision with root package name */
        m f1762f = new m(this.f1760d);

        /* renamed from: g  reason: collision with root package name */
        y f1763g;

        /* renamed from: h  reason: collision with root package name */
        boolean f1764h = false;

        /* renamed from: i  reason: collision with root package name */
        boolean f1765i = false;

        /* renamed from: j  reason: collision with root package name */
        boolean f1766j = false;

        /* renamed from: k  reason: collision with root package name */
        private boolean f1767k = true;
        private boolean l = true;
        int m;
        boolean n;
        private int o;
        private int p;
        private int q;
        private int r;

        class a implements m.b {
            a() {
            }

            public View a(int i2) {
                return o.this.c(i2);
            }

            public int b() {
                return o.this.q() - o.this.o();
            }

            public int a() {
                return o.this.n();
            }

            public int b(View view) {
                return o.this.i(view) + ((p) view.getLayoutParams()).rightMargin;
            }

            public int a(View view) {
                return o.this.f(view) - ((p) view.getLayoutParams()).leftMargin;
            }
        }

        class b implements m.b {
            b() {
            }

            public View a(int i2) {
                return o.this.c(i2);
            }

            public int b() {
                return o.this.h() - o.this.m();
            }

            public int a() {
                return o.this.p();
            }

            public int a(View view) {
                return o.this.j(view) - ((p) view.getLayoutParams()).topMargin;
            }

            public int b(View view) {
                return o.this.e(view) + ((p) view.getLayoutParams()).bottomMargin;
            }
        }

        public interface c {
            void a(int i2, int i3);
        }

        public static class d {

            /* renamed from: a  reason: collision with root package name */
            public int f1770a;

            /* renamed from: b  reason: collision with root package name */
            public int f1771b;

            /* renamed from: c  reason: collision with root package name */
            public boolean f1772c;

            /* renamed from: d  reason: collision with root package name */
            public boolean f1773d;
        }

        /* access modifiers changed from: package-private */
        public boolean A() {
            return false;
        }

        /* access modifiers changed from: package-private */
        public void B() {
            y yVar = this.f1763g;
            if (yVar != null) {
                yVar.d();
            }
        }

        public boolean C() {
            return false;
        }

        public int a(int i2, v vVar, z zVar) {
            return 0;
        }

        public int a(z zVar) {
            return 0;
        }

        public View a(View view, int i2, v vVar, z zVar) {
            return null;
        }

        public void a(int i2, int i3, z zVar, c cVar) {
        }

        public void a(int i2, c cVar) {
        }

        public void a(Rect rect, int i2, int i3) {
            c(a(i2, rect.width() + n() + o(), l()), a(i3, rect.height() + p() + m(), k()));
        }

        public void a(Parcelable parcelable) {
        }

        public void a(g gVar, g gVar2) {
        }

        public void a(RecyclerView recyclerView, int i2, int i3) {
        }

        public void a(RecyclerView recyclerView, int i2, int i3, int i4) {
        }

        public boolean a() {
            return false;
        }

        public boolean a(p pVar) {
            return pVar != null;
        }

        public boolean a(v vVar, z zVar, View view, int i2, Bundle bundle) {
            return false;
        }

        public boolean a(RecyclerView recyclerView, ArrayList<View> arrayList, int i2, int i3) {
            return false;
        }

        public int b(int i2, v vVar, z zVar) {
            return 0;
        }

        public int b(z zVar) {
            return 0;
        }

        /* access modifiers changed from: package-private */
        public void b(int i2, int i3) {
            this.q = View.MeasureSpec.getSize(i2);
            this.o = View.MeasureSpec.getMode(i2);
            if (this.o == 0 && !RecyclerView.A0) {
                this.q = 0;
            }
            this.r = View.MeasureSpec.getSize(i3);
            this.p = View.MeasureSpec.getMode(i3);
            if (this.p == 0 && !RecyclerView.A0) {
                this.r = 0;
            }
        }

        public void b(RecyclerView recyclerView) {
        }

        public void b(RecyclerView recyclerView, int i2, int i3) {
        }

        public boolean b() {
            return false;
        }

        public int c(v vVar, z zVar) {
            return 0;
        }

        public int c(z zVar) {
            return 0;
        }

        public View c(View view) {
            View c2;
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView == null || (c2 = recyclerView.c(view)) == null || this.f1757a.c(c2)) {
                return null;
            }
            return c2;
        }

        public abstract p c();

        @Deprecated
        public void c(RecyclerView recyclerView) {
        }

        public void c(RecyclerView recyclerView, int i2, int i3) {
        }

        public int d() {
            return -1;
        }

        public int d(z zVar) {
            return 0;
        }

        public View d(View view, int i2) {
            return null;
        }

        /* access modifiers changed from: package-private */
        public void d(int i2, int i3) {
            int e2 = e();
            if (e2 == 0) {
                this.f1758b.c(i2, i3);
                return;
            }
            int i4 = Api.BaseClientBuilder.API_PRIORITY_OTHER;
            int i5 = Api.BaseClientBuilder.API_PRIORITY_OTHER;
            int i6 = Integer.MIN_VALUE;
            int i7 = Integer.MIN_VALUE;
            for (int i8 = 0; i8 < e2; i8++) {
                View c2 = c(i8);
                Rect rect = this.f1758b.f1732i;
                b(c2, rect);
                int i9 = rect.left;
                if (i9 < i4) {
                    i4 = i9;
                }
                int i10 = rect.right;
                if (i10 > i6) {
                    i6 = i10;
                }
                int i11 = rect.top;
                if (i11 < i5) {
                    i5 = i11;
                }
                int i12 = rect.bottom;
                if (i12 > i7) {
                    i7 = i12;
                }
            }
            this.f1758b.f1732i.set(i4, i5, i6, i7);
            a(this.f1758b.f1732i, i2, i3);
        }

        public void d(RecyclerView recyclerView) {
        }

        public boolean d(v vVar, z zVar) {
            return false;
        }

        public int e(z zVar) {
            return 0;
        }

        public void e(v vVar, z zVar) {
            Log.e("RecyclerView", "You must override onLayoutChildren(Recycler recycler, State state) ");
        }

        public int f(z zVar) {
            return 0;
        }

        public void f(int i2) {
        }

        /* access modifiers changed from: package-private */
        public void f(RecyclerView recyclerView) {
            if (recyclerView == null) {
                this.f1758b = null;
                this.f1757a = null;
                this.q = 0;
                this.r = 0;
            } else {
                this.f1758b = recyclerView;
                this.f1757a = recyclerView.f1728e;
                this.q = recyclerView.getWidth();
                this.r = recyclerView.getHeight();
            }
            this.o = 1073741824;
            this.p = 1073741824;
        }

        public void g(int i2) {
            if (c(i2) != null) {
                this.f1757a.e(i2);
            }
        }

        public void g(z zVar) {
        }

        public int h() {
            return this.r;
        }

        public int i() {
            return this.p;
        }

        public int j() {
            return b.e.m.u.k(this.f1758b);
        }

        public int k(View view) {
            return ((p) view.getLayoutParams()).f1775b.left;
        }

        public int l(View view) {
            return ((p) view.getLayoutParams()).a();
        }

        public int m() {
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView != null) {
                return recyclerView.getPaddingBottom();
            }
            return 0;
        }

        public int n() {
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView != null) {
                return recyclerView.getPaddingLeft();
            }
            return 0;
        }

        public void o(View view) {
            this.f1757a.d(view);
        }

        public int p() {
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView != null) {
                return recyclerView.getPaddingTop();
            }
            return 0;
        }

        public int q() {
            return this.q;
        }

        public int r() {
            return this.o;
        }

        /* access modifiers changed from: package-private */
        public boolean s() {
            int e2 = e();
            for (int i2 = 0; i2 < e2; i2++) {
                ViewGroup.LayoutParams layoutParams = c(i2).getLayoutParams();
                if (layoutParams.width < 0 && layoutParams.height < 0) {
                    return true;
                }
            }
            return false;
        }

        public boolean t() {
            return this.f1765i;
        }

        public boolean u() {
            return this.f1766j;
        }

        public final boolean v() {
            return this.l;
        }

        public boolean w() {
            y yVar = this.f1763g;
            return yVar != null && yVar.c();
        }

        public Parcelable x() {
            return null;
        }

        public void y() {
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView != null) {
                recyclerView.requestLayout();
            }
        }

        public void z() {
            this.f1764h = true;
        }

        public int e() {
            b bVar = this.f1757a;
            if (bVar != null) {
                return bVar.a();
            }
            return 0;
        }

        public int h(View view) {
            Rect rect = ((p) view.getLayoutParams()).f1775b;
            return view.getMeasuredWidth() + rect.left + rect.right;
        }

        public int i(View view) {
            return view.getRight() + m(view);
        }

        public int j(View view) {
            return view.getTop() - n(view);
        }

        public int k() {
            return b.e.m.u.l(this.f1758b);
        }

        public int l() {
            return b.e.m.u.m(this.f1758b);
        }

        public int m(View view) {
            return ((p) view.getLayoutParams()).f1775b.right;
        }

        public int n(View view) {
            return ((p) view.getLayoutParams()).f1775b.top;
        }

        public int o() {
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView != null) {
                return recyclerView.getPaddingRight();
            }
            return 0;
        }

        public void e(int i2) {
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView != null) {
                recyclerView.d(i2);
            }
        }

        public View g() {
            View focusedChild;
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView == null || (focusedChild = recyclerView.getFocusedChild()) == null || this.f1757a.c(focusedChild)) {
                return null;
            }
            return focusedChild;
        }

        public void c(View view, int i2) {
            a(view, i2, (p) view.getLayoutParams());
        }

        public View c(int i2) {
            b bVar = this.f1757a;
            if (bVar != null) {
                return bVar.c(i2);
            }
            return null;
        }

        public int e(View view) {
            return view.getBottom() + d(view);
        }

        public static int a(int i2, int i3, int i4) {
            int mode = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i2);
            if (mode != Integer.MIN_VALUE) {
                return mode != 1073741824 ? Math.max(i3, i4) : size;
            }
            return Math.min(size, Math.max(i3, i4));
        }

        /* access modifiers changed from: package-private */
        public void c(v vVar) {
            int e2 = vVar.e();
            for (int i2 = e2 - 1; i2 >= 0; i2--) {
                View c2 = vVar.c(i2);
                c0 k2 = RecyclerView.k(c2);
                if (!k2.shouldIgnore()) {
                    k2.setIsRecyclable(false);
                    if (k2.isTmpDetached()) {
                        this.f1758b.removeDetachedView(c2, false);
                    }
                    l lVar = this.f1758b.M;
                    if (lVar != null) {
                        lVar.c(k2);
                    }
                    k2.setIsRecyclable(true);
                    vVar.a(c2);
                }
            }
            vVar.c();
            if (e2 > 0) {
                this.f1758b.invalidate();
            }
        }

        /* access modifiers changed from: package-private */
        public void e(RecyclerView recyclerView) {
            b(View.MeasureSpec.makeMeasureSpec(recyclerView.getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(recyclerView.getHeight(), 1073741824));
        }

        public int g(View view) {
            Rect rect = ((p) view.getLayoutParams()).f1775b;
            return view.getMeasuredHeight() + rect.top + rect.bottom;
        }

        public void b(RecyclerView recyclerView, v vVar) {
            c(recyclerView);
        }

        public void a(String str) {
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView != null) {
                recyclerView.a(str);
            }
        }

        public void b(View view) {
            b(view, -1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
         arg types: [android.view.View, int, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, int, android.view.View):void
          androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
          androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$p):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.accessibility.AccessibilityEvent):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, b.e.m.d0.c):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, android.os.Bundle):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, boolean):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.view.View):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void */
        public void b(View view, int i2) {
            a(view, i2, false);
        }

        public boolean f() {
            RecyclerView recyclerView = this.f1758b;
            return recyclerView != null && recyclerView.f1730g;
        }

        /* access modifiers changed from: package-private */
        public void a(RecyclerView recyclerView) {
            this.f1765i = true;
            b(recyclerView);
        }

        public View b(int i2) {
            int e2 = e();
            for (int i3 = 0; i3 < e2; i3++) {
                View c2 = c(i3);
                c0 k2 = RecyclerView.k(c2);
                if (k2 != null && k2.getLayoutPosition() == i2 && !k2.shouldIgnore() && (this.f1758b.h0.d() || !k2.isRemoved())) {
                    return c2;
                }
            }
            return null;
        }

        public void d(int i2) {
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView != null) {
                recyclerView.c(i2);
            }
        }

        public int f(View view) {
            return view.getLeft() - k(view);
        }

        /* access modifiers changed from: package-private */
        public void a(RecyclerView recyclerView, v vVar) {
            this.f1765i = false;
            b(recyclerView, vVar);
        }

        public int d(View view) {
            return ((p) view.getLayoutParams()).f1775b.bottom;
        }

        private boolean d(RecyclerView recyclerView, int i2, int i3) {
            View focusedChild = recyclerView.getFocusedChild();
            if (focusedChild == null) {
                return false;
            }
            int n2 = n();
            int p2 = p();
            int q2 = q() - o();
            int h2 = h() - m();
            Rect rect = this.f1758b.f1732i;
            b(focusedChild, rect);
            if (rect.left - i2 >= q2 || rect.right - i2 <= n2 || rect.top - i3 >= h2 || rect.bottom - i3 <= p2) {
                return false;
            }
            return true;
        }

        public boolean a(Runnable runnable) {
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView != null) {
                return recyclerView.removeCallbacks(runnable);
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean b(View view, int i2, int i3, p pVar) {
            return !this.f1767k || !b(view.getMeasuredWidth(), i2, pVar.width) || !b(view.getMeasuredHeight(), i3, pVar.height);
        }

        public p a(ViewGroup.LayoutParams layoutParams) {
            if (layoutParams instanceof p) {
                return new p((p) layoutParams);
            }
            if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                return new p((ViewGroup.MarginLayoutParams) layoutParams);
            }
            return new p(layoutParams);
        }

        private int[] c(View view, Rect rect) {
            int[] iArr = new int[2];
            int n2 = n();
            int p2 = p();
            int q2 = q() - o();
            int h2 = h() - m();
            int left = (view.getLeft() + rect.left) - view.getScrollX();
            int top = (view.getTop() + rect.top) - view.getScrollY();
            int width = rect.width() + left;
            int height = rect.height() + top;
            int i2 = left - n2;
            int min = Math.min(0, i2);
            int i3 = top - p2;
            int min2 = Math.min(0, i3);
            int i4 = width - q2;
            int max = Math.max(0, i4);
            int max2 = Math.max(0, height - h2);
            if (j() != 1) {
                if (min == 0) {
                    min = Math.min(i2, max);
                }
                max = min;
            } else if (max == 0) {
                max = Math.max(min, i4);
            }
            if (min2 == 0) {
                min2 = Math.min(i3, max2);
            }
            iArr[0] = max;
            iArr[1] = min2;
            return iArr;
        }

        private static boolean b(int i2, int i3, int i4) {
            int mode = View.MeasureSpec.getMode(i3);
            int size = View.MeasureSpec.getSize(i3);
            if (i4 > 0 && i2 != i4) {
                return false;
            }
            if (mode == Integer.MIN_VALUE) {
                return size >= i2;
            }
            if (mode != 0) {
                return mode == 1073741824 && size == i2;
            }
            return true;
        }

        public void b(View view, Rect rect) {
            RecyclerView.a(view, rect);
        }

        public p a(Context context, AttributeSet attributeSet) {
            return new p(context, attributeSet);
        }

        public void b(v vVar) {
            for (int e2 = e() - 1; e2 >= 0; e2--) {
                if (!RecyclerView.k(c(e2)).shouldIgnore()) {
                    a(e2, vVar);
                }
            }
        }

        public void a(View view) {
            a(view, -1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
         arg types: [android.view.View, int, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, int, android.view.View):void
          androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
          androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$p):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.accessibility.AccessibilityEvent):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, b.e.m.d0.c):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, android.os.Bundle):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, boolean):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.view.View):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void */
        public void a(View view, int i2) {
            a(view, i2, true);
        }

        private void a(View view, int i2, boolean z) {
            c0 k2 = RecyclerView.k(view);
            if (z || k2.isRemoved()) {
                this.f1758b.f1729f.a(k2);
            } else {
                this.f1758b.f1729f.g(k2);
            }
            p pVar = (p) view.getLayoutParams();
            if (k2.wasReturnedFromScrap() || k2.isScrap()) {
                if (k2.isScrap()) {
                    k2.unScrap();
                } else {
                    k2.clearReturnedFromScrapFlag();
                }
                this.f1757a.a(view, i2, view.getLayoutParams(), false);
            } else if (view.getParent() == this.f1758b) {
                int b2 = this.f1757a.b(view);
                if (i2 == -1) {
                    i2 = this.f1757a.a();
                }
                if (b2 == -1) {
                    throw new IllegalStateException("Added View has RecyclerView as parent but view is not a real child. Unfiltered index:" + this.f1758b.indexOfChild(view) + this.f1758b.i());
                } else if (b2 != i2) {
                    this.f1758b.m.a(b2, i2);
                }
            } else {
                this.f1757a.a(view, i2, false);
                pVar.f1776c = true;
                y yVar = this.f1763g;
                if (yVar != null && yVar.c()) {
                    this.f1763g.a(view);
                }
            }
            if (pVar.f1777d) {
                k2.itemView.invalidate();
                pVar.f1777d = false;
            }
        }

        public int b(v vVar, z zVar) {
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView == null || recyclerView.l == null || !b()) {
                return 1;
            }
            return this.f1758b.l.getItemCount();
        }

        public void c(int i2, int i3) {
            this.f1758b.setMeasuredDimension(i2, i3);
        }

        public void a(int i2) {
            a(i2, c(i2));
        }

        private void a(int i2, View view) {
            this.f1757a.a(i2);
        }

        public void a(View view, int i2, p pVar) {
            c0 k2 = RecyclerView.k(view);
            if (k2.isRemoved()) {
                this.f1758b.f1729f.a(k2);
            } else {
                this.f1758b.f1729f.g(k2);
            }
            this.f1757a.a(view, i2, pVar, k2.isRemoved());
        }

        public void a(int i2, int i3) {
            View c2 = c(i2);
            if (c2 != null) {
                a(i2);
                c(c2, i3);
                return;
            }
            throw new IllegalArgumentException("Cannot move a child from non-existing index:" + i2 + this.f1758b.toString());
        }

        public void a(View view, v vVar) {
            o(view);
            vVar.b(view);
        }

        public void a(int i2, v vVar) {
            View c2 = c(i2);
            g(i2);
            vVar.b(c2);
        }

        public void a(v vVar) {
            for (int e2 = e() - 1; e2 >= 0; e2--) {
                a(vVar, e2, c(e2));
            }
        }

        private void a(v vVar, int i2, View view) {
            c0 k2 = RecyclerView.k(view);
            if (!k2.shouldIgnore()) {
                if (!k2.isInvalid() || k2.isRemoved() || this.f1758b.l.hasStableIds()) {
                    a(i2);
                    vVar.c(view);
                    this.f1758b.f1729f.d(k2);
                    return;
                }
                g(i2);
                vVar.b(k2);
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(View view, int i2, int i3, p pVar) {
            return view.isLayoutRequested() || !this.f1767k || !b(view.getWidth(), i2, pVar.width) || !b(view.getHeight(), i3, pVar.height);
        }

        public void a(View view, int i2, int i3) {
            p pVar = (p) view.getLayoutParams();
            Rect f2 = this.f1758b.f(view);
            int i4 = i2 + f2.left + f2.right;
            int i5 = i3 + f2.top + f2.bottom;
            int a2 = a(q(), r(), n() + o() + pVar.leftMargin + pVar.rightMargin + i4, pVar.width, a());
            int a3 = a(h(), i(), p() + m() + pVar.topMargin + pVar.bottomMargin + i5, pVar.height, b());
            if (a(view, a2, a3, pVar)) {
                view.measure(a2, a3);
            }
        }

        public static int a(int i2, int i3, int i4, int i5, boolean z) {
            int i6;
            int i7 = i2 - i4;
            int i8 = 0;
            int max = Math.max(0, i7);
            if (z) {
                if (i5 < 0) {
                    if (i5 == -1) {
                        if (i3 == Integer.MIN_VALUE || (i3 != 0 && i3 == 1073741824)) {
                            i6 = max;
                        } else {
                            i3 = 0;
                            i6 = 0;
                        }
                        i8 = i3;
                        max = i6;
                        return View.MeasureSpec.makeMeasureSpec(max, i8);
                    }
                    max = 0;
                    return View.MeasureSpec.makeMeasureSpec(max, i8);
                }
            } else if (i5 < 0) {
                if (i5 == -1) {
                    i8 = i3;
                } else {
                    if (i5 == -2) {
                        if (i3 == Integer.MIN_VALUE || i3 == 1073741824) {
                            i8 = Integer.MIN_VALUE;
                        }
                    }
                    max = 0;
                }
                return View.MeasureSpec.makeMeasureSpec(max, i8);
            }
            max = i5;
            i8 = 1073741824;
            return View.MeasureSpec.makeMeasureSpec(max, i8);
        }

        public void a(View view, int i2, int i3, int i4, int i5) {
            p pVar = (p) view.getLayoutParams();
            Rect rect = pVar.f1775b;
            view.layout(i2 + rect.left + pVar.leftMargin, i3 + rect.top + pVar.topMargin, (i4 - rect.right) - pVar.rightMargin, (i5 - rect.bottom) - pVar.bottomMargin);
        }

        public void a(View view, boolean z, Rect rect) {
            Matrix matrix;
            if (z) {
                Rect rect2 = ((p) view.getLayoutParams()).f1775b;
                rect.set(-rect2.left, -rect2.top, view.getWidth() + rect2.right, view.getHeight() + rect2.bottom);
            } else {
                rect.set(0, 0, view.getWidth(), view.getHeight());
            }
            if (!(this.f1758b == null || (matrix = view.getMatrix()) == null || matrix.isIdentity())) {
                RectF rectF = this.f1758b.f1734k;
                rectF.set(rect);
                matrix.mapRect(rectF);
                rect.set((int) Math.floor((double) rectF.left), (int) Math.floor((double) rectF.top), (int) Math.ceil((double) rectF.right), (int) Math.ceil((double) rectF.bottom));
            }
            rect.offset(view.getLeft(), view.getTop());
        }

        public void a(View view, Rect rect) {
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView == null) {
                rect.set(0, 0, 0, 0);
            } else {
                rect.set(recyclerView.f(view));
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean
         arg types: [androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.o.a(int, int, int, int, boolean):int
          androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, int, int):void
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, int, android.os.Bundle):boolean
          androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean */
        public boolean a(RecyclerView recyclerView, View view, Rect rect, boolean z) {
            return a(recyclerView, view, rect, z, false);
        }

        public boolean a(RecyclerView recyclerView, View view, Rect rect, boolean z, boolean z2) {
            int[] c2 = c(view, rect);
            int i2 = c2[0];
            int i3 = c2[1];
            if ((z2 && !d(recyclerView, i2, i3)) || (i2 == 0 && i3 == 0)) {
                return false;
            }
            if (z) {
                recyclerView.scrollBy(i2, i3);
            } else {
                recyclerView.i(i2, i3);
            }
            return true;
        }

        public boolean a(View view, boolean z, boolean z2) {
            boolean z3 = this.f1761e.a(view, 24579) && this.f1762f.a(view, 24579);
            return z ? z3 : !z3;
        }

        @Deprecated
        public boolean a(RecyclerView recyclerView, View view, View view2) {
            return w() || recyclerView.n();
        }

        public boolean a(RecyclerView recyclerView, z zVar, View view, View view2) {
            return a(recyclerView, view, view2);
        }

        public void a(RecyclerView recyclerView, int i2, int i3, Object obj) {
            c(recyclerView, i2, i3);
        }

        public void a(v vVar, z zVar, int i2, int i3) {
            this.f1758b.c(i2, i3);
        }

        /* access modifiers changed from: package-private */
        public void a(b.e.m.d0.c cVar) {
            RecyclerView recyclerView = this.f1758b;
            a(recyclerView.f1725b, recyclerView.h0, cVar);
        }

        public void a(v vVar, z zVar, b.e.m.d0.c cVar) {
            if (this.f1758b.canScrollVertically(-1) || this.f1758b.canScrollHorizontally(-1)) {
                cVar.a((int) Utility.DEFAULT_STREAM_BUFFER_SIZE);
                cVar.c(true);
            }
            if (this.f1758b.canScrollVertically(1) || this.f1758b.canScrollHorizontally(1)) {
                cVar.a((int) CodedOutputStream.DEFAULT_BUFFER_SIZE);
                cVar.c(true);
            }
            cVar.a(c.b.a(b(vVar, zVar), a(vVar, zVar), d(vVar, zVar), c(vVar, zVar)));
        }

        public void a(AccessibilityEvent accessibilityEvent) {
            RecyclerView recyclerView = this.f1758b;
            a(recyclerView.f1725b, recyclerView.h0, accessibilityEvent);
        }

        public void a(v vVar, z zVar, AccessibilityEvent accessibilityEvent) {
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView != null && accessibilityEvent != null) {
                boolean z = true;
                if (!recyclerView.canScrollVertically(1) && !this.f1758b.canScrollVertically(-1) && !this.f1758b.canScrollHorizontally(-1) && !this.f1758b.canScrollHorizontally(1)) {
                    z = false;
                }
                accessibilityEvent.setScrollable(z);
                g gVar = this.f1758b.l;
                if (gVar != null) {
                    accessibilityEvent.setItemCount(gVar.getItemCount());
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(View view, b.e.m.d0.c cVar) {
            c0 k2 = RecyclerView.k(view);
            if (k2 != null && !k2.isRemoved() && !this.f1757a.c(k2.itemView)) {
                RecyclerView recyclerView = this.f1758b;
                a(recyclerView.f1725b, recyclerView.h0, view, cVar);
            }
        }

        public void a(v vVar, z zVar, View view, b.e.m.d0.c cVar) {
            cVar.b(c.C0053c.a(b() ? l(view) : 0, 1, a() ? l(view) : 0, 1, false, false));
        }

        public int a(v vVar, z zVar) {
            RecyclerView recyclerView = this.f1758b;
            if (recyclerView == null || recyclerView.l == null || !a()) {
                return 1;
            }
            return this.f1758b.l.getItemCount();
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i2, Bundle bundle) {
            RecyclerView recyclerView = this.f1758b;
            return a(recyclerView.f1725b, recyclerView.h0, i2, bundle);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.a(int, int, android.view.animation.Interpolator, int, boolean):void
         arg types: [int, int, ?[OBJECT, ARRAY], int, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.a(android.content.Context, java.lang.String, android.util.AttributeSet, int, int):void
          androidx.recyclerview.widget.RecyclerView.a(int, int, int[], int[], int):boolean
          androidx.recyclerview.widget.RecyclerView.a(int, int, android.view.animation.Interpolator, int, boolean):void */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0075 A[ADDED_TO_REGION] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(androidx.recyclerview.widget.RecyclerView.v r8, androidx.recyclerview.widget.RecyclerView.z r9, int r10, android.os.Bundle r11) {
            /*
                r7 = this;
                androidx.recyclerview.widget.RecyclerView r8 = r7.f1758b
                r9 = 0
                if (r8 != 0) goto L_0x0006
                return r9
            L_0x0006:
                r11 = 4096(0x1000, float:5.74E-42)
                r0 = 1
                if (r10 == r11) goto L_0x0042
                r11 = 8192(0x2000, float:1.14794E-41)
                if (r10 == r11) goto L_0x0012
                r2 = 0
                r3 = 0
                goto L_0x0073
            L_0x0012:
                r10 = -1
                boolean r8 = r8.canScrollVertically(r10)
                if (r8 == 0) goto L_0x0029
                int r8 = r7.h()
                int r11 = r7.p()
                int r8 = r8 - r11
                int r11 = r7.m()
                int r8 = r8 - r11
                int r8 = -r8
                goto L_0x002a
            L_0x0029:
                r8 = 0
            L_0x002a:
                androidx.recyclerview.widget.RecyclerView r11 = r7.f1758b
                boolean r10 = r11.canScrollHorizontally(r10)
                if (r10 == 0) goto L_0x0071
                int r10 = r7.q()
                int r11 = r7.n()
                int r10 = r10 - r11
                int r11 = r7.o()
                int r10 = r10 - r11
                int r10 = -r10
                goto L_0x006e
            L_0x0042:
                boolean r8 = r8.canScrollVertically(r0)
                if (r8 == 0) goto L_0x0057
                int r8 = r7.h()
                int r10 = r7.p()
                int r8 = r8 - r10
                int r10 = r7.m()
                int r8 = r8 - r10
                goto L_0x0058
            L_0x0057:
                r8 = 0
            L_0x0058:
                androidx.recyclerview.widget.RecyclerView r10 = r7.f1758b
                boolean r10 = r10.canScrollHorizontally(r0)
                if (r10 == 0) goto L_0x0071
                int r10 = r7.q()
                int r11 = r7.n()
                int r10 = r10 - r11
                int r11 = r7.o()
                int r10 = r10 - r11
            L_0x006e:
                r3 = r8
                r2 = r10
                goto L_0x0073
            L_0x0071:
                r3 = r8
                r2 = 0
            L_0x0073:
                if (r3 != 0) goto L_0x0078
                if (r2 != 0) goto L_0x0078
                return r9
            L_0x0078:
                androidx.recyclerview.widget.RecyclerView r1 = r7.f1758b
                r4 = 0
                r5 = -2147483648(0xffffffff80000000, float:-0.0)
                r6 = 1
                r1.a(r2, r3, r4, r5, r6)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, android.os.Bundle):boolean");
        }

        /* access modifiers changed from: package-private */
        public boolean a(View view, int i2, Bundle bundle) {
            RecyclerView recyclerView = this.f1758b;
            return a(recyclerView.f1725b, recyclerView.h0, view, i2, bundle);
        }

        public static d a(Context context, AttributeSet attributeSet, int i2, int i3) {
            d dVar = new d();
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.l.c.RecyclerView, i2, i3);
            dVar.f1770a = obtainStyledAttributes.getInt(b.l.c.RecyclerView_android_orientation, 1);
            dVar.f1771b = obtainStyledAttributes.getInt(b.l.c.RecyclerView_spanCount, 1);
            dVar.f1772c = obtainStyledAttributes.getBoolean(b.l.c.RecyclerView_reverseLayout, false);
            dVar.f1773d = obtainStyledAttributes.getBoolean(b.l.c.RecyclerView_stackFromEnd, false);
            obtainStyledAttributes.recycle();
            return dVar;
        }
    }

    public interface q {
        void a(View view);

        void b(View view);
    }

    public static abstract class r {
        public abstract boolean a(int i2, int i3);
    }

    public interface s {
        void a(RecyclerView recyclerView, MotionEvent motionEvent);

        void a(boolean z);

        boolean b(RecyclerView recyclerView, MotionEvent motionEvent);
    }

    public static abstract class t {
        public void a(RecyclerView recyclerView, int i2) {
        }

        public void a(RecyclerView recyclerView, int i2, int i3) {
        }
    }

    public final class v {

        /* renamed from: a  reason: collision with root package name */
        final ArrayList<c0> f1784a = new ArrayList<>();

        /* renamed from: b  reason: collision with root package name */
        ArrayList<c0> f1785b = null;

        /* renamed from: c  reason: collision with root package name */
        final ArrayList<c0> f1786c = new ArrayList<>();

        /* renamed from: d  reason: collision with root package name */
        private final List<c0> f1787d = Collections.unmodifiableList(this.f1784a);

        /* renamed from: e  reason: collision with root package name */
        private int f1788e = 2;

        /* renamed from: f  reason: collision with root package name */
        int f1789f = 2;

        /* renamed from: g  reason: collision with root package name */
        u f1790g;

        /* renamed from: h  reason: collision with root package name */
        private a0 f1791h;

        public v() {
        }

        private void e(c0 c0Var) {
            if (RecyclerView.this.m()) {
                View view = c0Var.itemView;
                if (b.e.m.u.i(view) == 0) {
                    b.e.m.u.b(view, 1);
                }
                j jVar = RecyclerView.this.o0;
                if (jVar != null) {
                    b.e.m.a b2 = jVar.b();
                    if (b2 instanceof j.a) {
                        ((j.a) b2).d(view);
                    }
                    b.e.m.u.a(view, b2);
                }
            }
        }

        public void a() {
            this.f1784a.clear();
            i();
        }

        /* access modifiers changed from: package-private */
        public View b(int i2, boolean z) {
            return a(i2, z, Long.MAX_VALUE).itemView;
        }

        /* access modifiers changed from: package-private */
        public void c(View view) {
            c0 k2 = RecyclerView.k(view);
            if (!k2.hasAnyOfTheFlags(12) && k2.isUpdated() && !RecyclerView.this.a(k2)) {
                if (this.f1785b == null) {
                    this.f1785b = new ArrayList<>();
                }
                k2.setScrapContainer(this, true);
                this.f1785b.add(k2);
            } else if (!k2.isInvalid() || k2.isRemoved() || RecyclerView.this.l.hasStableIds()) {
                k2.setScrapContainer(this, false);
                this.f1784a.add(k2);
            } else {
                throw new IllegalArgumentException("Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool." + RecyclerView.this.i());
            }
        }

        /* access modifiers changed from: package-private */
        public boolean d(c0 c0Var) {
            if (c0Var.isRemoved()) {
                return RecyclerView.this.h0.d();
            }
            int i2 = c0Var.mPosition;
            if (i2 < 0 || i2 >= RecyclerView.this.l.getItemCount()) {
                throw new IndexOutOfBoundsException("Inconsistency detected. Invalid view holder adapter position" + c0Var + RecyclerView.this.i());
            } else if (!RecyclerView.this.h0.d() && RecyclerView.this.l.getItemViewType(c0Var.mPosition) != c0Var.getItemViewType()) {
                return false;
            } else {
                if (!RecyclerView.this.l.hasStableIds() || c0Var.getItemId() == RecyclerView.this.l.getItemId(c0Var.mPosition)) {
                    return true;
                }
                return false;
            }
        }

        public void f(int i2) {
            this.f1788e = i2;
            j();
        }

        /* access modifiers changed from: package-private */
        public void g() {
            int size = this.f1786c.size();
            for (int i2 = 0; i2 < size; i2++) {
                p pVar = (p) this.f1786c.get(i2).itemView.getLayoutParams();
                if (pVar != null) {
                    pVar.f1776c = true;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void h() {
            int size = this.f1786c.size();
            for (int i2 = 0; i2 < size; i2++) {
                c0 c0Var = this.f1786c.get(i2);
                if (c0Var != null) {
                    c0Var.addFlags(6);
                    c0Var.addChangePayload(null);
                }
            }
            g gVar = RecyclerView.this.l;
            if (gVar == null || !gVar.hasStableIds()) {
                i();
            }
        }

        /* access modifiers changed from: package-private */
        public void i() {
            for (int size = this.f1786c.size() - 1; size >= 0; size--) {
                e(size);
            }
            this.f1786c.clear();
            if (RecyclerView.C0) {
                RecyclerView.this.g0.a();
            }
        }

        /* access modifiers changed from: package-private */
        public void j() {
            o oVar = RecyclerView.this.m;
            this.f1789f = this.f1788e + (oVar != null ? oVar.m : 0);
            for (int size = this.f1786c.size() - 1; size >= 0 && this.f1786c.size() > this.f1789f; size--) {
                e(size);
            }
        }

        public void b(View view) {
            c0 k2 = RecyclerView.k(view);
            if (k2.isTmpDetached()) {
                RecyclerView.this.removeDetachedView(view, false);
            }
            if (k2.isScrap()) {
                k2.unScrap();
            } else if (k2.wasReturnedFromScrap()) {
                k2.clearReturnedFromScrapFlag();
            }
            b(k2);
            if (RecyclerView.this.M != null && !k2.isRecyclable()) {
                RecyclerView.this.M.c(k2);
            }
        }

        private boolean a(c0 c0Var, int i2, int i3, long j2) {
            c0Var.mOwnerRecyclerView = RecyclerView.this;
            int itemViewType = c0Var.getItemViewType();
            long nanoTime = RecyclerView.this.getNanoTime();
            if (j2 != Long.MAX_VALUE && !this.f1790g.a(itemViewType, nanoTime, j2)) {
                return false;
            }
            RecyclerView.this.l.bindViewHolder(c0Var, i2);
            this.f1790g.a(c0Var.getItemViewType(), RecyclerView.this.getNanoTime() - nanoTime);
            e(c0Var);
            if (!RecyclerView.this.h0.d()) {
                return true;
            }
            c0Var.mPreLayoutPosition = i3;
            return true;
        }

        public List<c0> f() {
            return this.f1787d;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void
         arg types: [android.view.ViewGroup, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.v.a(int, boolean):androidx.recyclerview.widget.RecyclerView$c0
          androidx.recyclerview.widget.RecyclerView.v.a(int, int):void
          androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$c0, boolean):void
          androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void */
        private void f(c0 c0Var) {
            View view = c0Var.itemView;
            if (view instanceof ViewGroup) {
                a((ViewGroup) view, false);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.v.b(int, boolean):android.view.View
         arg types: [int, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.v.b(int, int):void
          androidx.recyclerview.widget.RecyclerView.v.b(int, boolean):android.view.View */
        public View d(int i2) {
            return b(i2, false);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$c0, boolean):void
         arg types: [androidx.recyclerview.widget.RecyclerView$c0, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void
          androidx.recyclerview.widget.RecyclerView.v.a(int, boolean):androidx.recyclerview.widget.RecyclerView$c0
          androidx.recyclerview.widget.RecyclerView.v.a(int, int):void
          androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$c0, boolean):void */
        /* access modifiers changed from: package-private */
        public void e(int i2) {
            a(this.f1786c.get(i2), true);
            this.f1786c.remove(i2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$c0, boolean):void
         arg types: [androidx.recyclerview.widget.RecyclerView$c0, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void
          androidx.recyclerview.widget.RecyclerView.v.a(int, boolean):androidx.recyclerview.widget.RecyclerView$c0
          androidx.recyclerview.widget.RecyclerView.v.a(int, int):void
          androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$c0, boolean):void */
        /* access modifiers changed from: package-private */
        public void b(c0 c0Var) {
            boolean z;
            boolean z2 = false;
            if (c0Var.isScrap() || c0Var.itemView.getParent() != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Scrapped or attached views may not be recycled. isScrap:");
                sb.append(c0Var.isScrap());
                sb.append(" isAttached:");
                if (c0Var.itemView.getParent() != null) {
                    z2 = true;
                }
                sb.append(z2);
                sb.append(RecyclerView.this.i());
                throw new IllegalArgumentException(sb.toString());
            } else if (c0Var.isTmpDetached()) {
                throw new IllegalArgumentException("Tmp detached view should be removed from RecyclerView before it can be recycled: " + c0Var + RecyclerView.this.i());
            } else if (!c0Var.shouldIgnore()) {
                boolean doesTransientStatePreventRecycling = c0Var.doesTransientStatePreventRecycling();
                g gVar = RecyclerView.this.l;
                if ((gVar != null && doesTransientStatePreventRecycling && gVar.onFailedToRecycleView(c0Var)) || c0Var.isRecyclable()) {
                    if (this.f1789f <= 0 || c0Var.hasAnyOfTheFlags(526)) {
                        z = false;
                    } else {
                        int size = this.f1786c.size();
                        if (size >= this.f1789f && size > 0) {
                            e(0);
                            size--;
                        }
                        if (RecyclerView.C0 && size > 0 && !RecyclerView.this.g0.a(c0Var.mPosition)) {
                            int i2 = size - 1;
                            while (i2 >= 0) {
                                if (!RecyclerView.this.g0.a(this.f1786c.get(i2).mPosition)) {
                                    break;
                                }
                                i2--;
                            }
                            size = i2 + 1;
                        }
                        this.f1786c.add(size, c0Var);
                        z = true;
                    }
                    if (!z) {
                        a(c0Var, true);
                        z2 = true;
                    }
                } else {
                    z = false;
                }
                RecyclerView.this.f1729f.h(c0Var);
                if (!z && !z2 && doesTransientStatePreventRecycling) {
                    c0Var.mOwnerRecyclerView = null;
                }
            } else {
                throw new IllegalArgumentException("Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle." + RecyclerView.this.i());
            }
        }

        /* access modifiers changed from: package-private */
        public u d() {
            if (this.f1790g == null) {
                this.f1790g = new u();
            }
            return this.f1790g;
        }

        /* access modifiers changed from: package-private */
        public void c(c0 c0Var) {
            if (c0Var.mInChangeScrap) {
                this.f1785b.remove(c0Var);
            } else {
                this.f1784a.remove(c0Var);
            }
            c0Var.mScrapContainer = null;
            c0Var.mInChangeScrap = false;
            c0Var.clearReturnedFromScrapFlag();
        }

        public int a(int i2) {
            if (i2 < 0 || i2 >= RecyclerView.this.h0.a()) {
                throw new IndexOutOfBoundsException("invalid position " + i2 + ". State item count is " + RecyclerView.this.h0.a() + RecyclerView.this.i());
            } else if (!RecyclerView.this.h0.d()) {
                return i2;
            } else {
                return RecyclerView.this.f1727d.b(i2);
            }
        }

        /* access modifiers changed from: package-private */
        public int e() {
            return this.f1784a.size();
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0037  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x005c  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x005f  */
        /* JADX WARNING: Removed duplicated region for block: B:78:0x01a1  */
        /* JADX WARNING: Removed duplicated region for block: B:81:0x01c4  */
        /* JADX WARNING: Removed duplicated region for block: B:94:0x01fd  */
        /* JADX WARNING: Removed duplicated region for block: B:95:0x020b  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public androidx.recyclerview.widget.RecyclerView.c0 a(int r17, boolean r18, long r19) {
            /*
                r16 = this;
                r6 = r16
                r3 = r17
                r0 = r18
                if (r3 < 0) goto L_0x022e
                androidx.recyclerview.widget.RecyclerView r1 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$z r1 = r1.h0
                int r1 = r1.a()
                if (r3 >= r1) goto L_0x022e
                androidx.recyclerview.widget.RecyclerView r1 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$z r1 = r1.h0
                boolean r1 = r1.d()
                r2 = 0
                r7 = 1
                r8 = 0
                if (r1 == 0) goto L_0x0027
                androidx.recyclerview.widget.RecyclerView$c0 r1 = r16.b(r17)
                if (r1 == 0) goto L_0x0028
                r4 = 1
                goto L_0x0029
            L_0x0027:
                r1 = r2
            L_0x0028:
                r4 = 0
            L_0x0029:
                if (r1 != 0) goto L_0x005d
                androidx.recyclerview.widget.RecyclerView$c0 r1 = r16.a(r17, r18)
                if (r1 == 0) goto L_0x005d
                boolean r5 = r6.d(r1)
                if (r5 != 0) goto L_0x005c
                if (r0 != 0) goto L_0x005a
                r5 = 4
                r1.addFlags(r5)
                boolean r5 = r1.isScrap()
                if (r5 == 0) goto L_0x004e
                androidx.recyclerview.widget.RecyclerView r5 = androidx.recyclerview.widget.RecyclerView.this
                android.view.View r9 = r1.itemView
                r5.removeDetachedView(r9, r8)
                r1.unScrap()
                goto L_0x0057
            L_0x004e:
                boolean r5 = r1.wasReturnedFromScrap()
                if (r5 == 0) goto L_0x0057
                r1.clearReturnedFromScrapFlag()
            L_0x0057:
                r6.b(r1)
            L_0x005a:
                r1 = r2
                goto L_0x005d
            L_0x005c:
                r4 = 1
            L_0x005d:
                if (r1 != 0) goto L_0x0180
                androidx.recyclerview.widget.RecyclerView r5 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.a r5 = r5.f1727d
                int r5 = r5.b(r3)
                if (r5 < 0) goto L_0x0148
                androidx.recyclerview.widget.RecyclerView r9 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r9 = r9.l
                int r9 = r9.getItemCount()
                if (r5 >= r9) goto L_0x0148
                androidx.recyclerview.widget.RecyclerView r9 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r9 = r9.l
                int r9 = r9.getItemViewType(r5)
                androidx.recyclerview.widget.RecyclerView r10 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r10 = r10.l
                boolean r10 = r10.hasStableIds()
                if (r10 == 0) goto L_0x0096
                androidx.recyclerview.widget.RecyclerView r1 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r1 = r1.l
                long r10 = r1.getItemId(r5)
                androidx.recyclerview.widget.RecyclerView$c0 r1 = r6.a(r10, r9, r0)
                if (r1 == 0) goto L_0x0096
                r1.mPosition = r5
                r4 = 1
            L_0x0096:
                if (r1 != 0) goto L_0x00eb
                androidx.recyclerview.widget.RecyclerView$a0 r0 = r6.f1791h
                if (r0 == 0) goto L_0x00eb
                android.view.View r0 = r0.a(r6, r3, r9)
                if (r0 == 0) goto L_0x00eb
                androidx.recyclerview.widget.RecyclerView r1 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$c0 r1 = r1.e(r0)
                if (r1 == 0) goto L_0x00ce
                boolean r0 = r1.shouldIgnore()
                if (r0 != 0) goto L_0x00b1
                goto L_0x00eb
            L_0x00b1:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view."
                r1.append(r2)
                androidx.recyclerview.widget.RecyclerView r2 = androidx.recyclerview.widget.RecyclerView.this
                java.lang.String r2 = r2.i()
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            L_0x00ce:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "getViewForPositionAndType returned a view which does not have a ViewHolder"
                r1.append(r2)
                androidx.recyclerview.widget.RecyclerView r2 = androidx.recyclerview.widget.RecyclerView.this
                java.lang.String r2 = r2.i()
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            L_0x00eb:
                if (r1 != 0) goto L_0x0101
                androidx.recyclerview.widget.RecyclerView$u r0 = r16.d()
                androidx.recyclerview.widget.RecyclerView$c0 r1 = r0.a(r9)
                if (r1 == 0) goto L_0x0101
                r1.resetInternal()
                boolean r0 = androidx.recyclerview.widget.RecyclerView.z0
                if (r0 == 0) goto L_0x0101
                r6.f(r1)
            L_0x0101:
                if (r1 != 0) goto L_0x0180
                androidx.recyclerview.widget.RecyclerView r0 = androidx.recyclerview.widget.RecyclerView.this
                long r0 = r0.getNanoTime()
                r10 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                int r5 = (r19 > r10 ? 1 : (r19 == r10 ? 0 : -1))
                if (r5 == 0) goto L_0x011f
                androidx.recyclerview.widget.RecyclerView$u r10 = r6.f1790g
                r11 = r9
                r12 = r0
                r14 = r19
                boolean r5 = r10.b(r11, r12, r14)
                if (r5 != 0) goto L_0x011f
                return r2
            L_0x011f:
                androidx.recyclerview.widget.RecyclerView r2 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$g r5 = r2.l
                androidx.recyclerview.widget.RecyclerView$c0 r2 = r5.createViewHolder(r2, r9)
                boolean r5 = androidx.recyclerview.widget.RecyclerView.C0
                if (r5 == 0) goto L_0x013a
                android.view.View r5 = r2.itemView
                androidx.recyclerview.widget.RecyclerView r5 = androidx.recyclerview.widget.RecyclerView.j(r5)
                if (r5 == 0) goto L_0x013a
                java.lang.ref.WeakReference r10 = new java.lang.ref.WeakReference
                r10.<init>(r5)
                r2.mNestedRecyclerView = r10
            L_0x013a:
                androidx.recyclerview.widget.RecyclerView r5 = androidx.recyclerview.widget.RecyclerView.this
                long r10 = r5.getNanoTime()
                androidx.recyclerview.widget.RecyclerView$u r5 = r6.f1790g
                long r10 = r10 - r0
                r5.b(r9, r10)
                r10 = r2
                goto L_0x0181
            L_0x0148:
                java.lang.IndexOutOfBoundsException r0 = new java.lang.IndexOutOfBoundsException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "Inconsistency detected. Invalid item position "
                r1.append(r2)
                r1.append(r3)
                java.lang.String r2 = "(offset:"
                r1.append(r2)
                r1.append(r5)
                java.lang.String r2 = ").state:"
                r1.append(r2)
                androidx.recyclerview.widget.RecyclerView r2 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$z r2 = r2.h0
                int r2 = r2.a()
                r1.append(r2)
                androidx.recyclerview.widget.RecyclerView r2 = androidx.recyclerview.widget.RecyclerView.this
                java.lang.String r2 = r2.i()
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            L_0x0180:
                r10 = r1
            L_0x0181:
                r9 = r4
                if (r9 == 0) goto L_0x01ba
                androidx.recyclerview.widget.RecyclerView r0 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$z r0 = r0.h0
                boolean r0 = r0.d()
                if (r0 != 0) goto L_0x01ba
                r0 = 8192(0x2000, float:1.14794E-41)
                boolean r1 = r10.hasAnyOfTheFlags(r0)
                if (r1 == 0) goto L_0x01ba
                r10.setFlags(r8, r0)
                androidx.recyclerview.widget.RecyclerView r0 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$z r0 = r0.h0
                boolean r0 = r0.f1804k
                if (r0 == 0) goto L_0x01ba
                int r0 = androidx.recyclerview.widget.RecyclerView.l.e(r10)
                r0 = r0 | 4096(0x1000, float:5.74E-42)
                androidx.recyclerview.widget.RecyclerView r1 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$l r2 = r1.M
                androidx.recyclerview.widget.RecyclerView$z r1 = r1.h0
                java.util.List r4 = r10.getUnmodifiedPayloads()
                androidx.recyclerview.widget.RecyclerView$l$c r0 = r2.a(r1, r10, r0, r4)
                androidx.recyclerview.widget.RecyclerView r1 = androidx.recyclerview.widget.RecyclerView.this
                r1.a(r10, r0)
            L_0x01ba:
                androidx.recyclerview.widget.RecyclerView r0 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$z r0 = r0.h0
                boolean r0 = r0.d()
                if (r0 == 0) goto L_0x01cd
                boolean r0 = r10.isBound()
                if (r0 == 0) goto L_0x01cd
                r10.mPreLayoutPosition = r3
                goto L_0x01e0
            L_0x01cd:
                boolean r0 = r10.isBound()
                if (r0 == 0) goto L_0x01e2
                boolean r0 = r10.needsUpdate()
                if (r0 != 0) goto L_0x01e2
                boolean r0 = r10.isInvalid()
                if (r0 == 0) goto L_0x01e0
                goto L_0x01e2
            L_0x01e0:
                r0 = 0
                goto L_0x01f5
            L_0x01e2:
                androidx.recyclerview.widget.RecyclerView r0 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.a r0 = r0.f1727d
                int r2 = r0.b(r3)
                r0 = r16
                r1 = r10
                r3 = r17
                r4 = r19
                boolean r0 = r0.a(r1, r2, r3, r4)
            L_0x01f5:
                android.view.View r1 = r10.itemView
                android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()
                if (r1 != 0) goto L_0x020b
                androidx.recyclerview.widget.RecyclerView r1 = androidx.recyclerview.widget.RecyclerView.this
                android.view.ViewGroup$LayoutParams r1 = r1.generateDefaultLayoutParams()
                androidx.recyclerview.widget.RecyclerView$p r1 = (androidx.recyclerview.widget.RecyclerView.p) r1
                android.view.View r2 = r10.itemView
                r2.setLayoutParams(r1)
                goto L_0x0223
            L_0x020b:
                androidx.recyclerview.widget.RecyclerView r2 = androidx.recyclerview.widget.RecyclerView.this
                boolean r2 = r2.checkLayoutParams(r1)
                if (r2 != 0) goto L_0x0221
                androidx.recyclerview.widget.RecyclerView r2 = androidx.recyclerview.widget.RecyclerView.this
                android.view.ViewGroup$LayoutParams r1 = r2.generateLayoutParams(r1)
                androidx.recyclerview.widget.RecyclerView$p r1 = (androidx.recyclerview.widget.RecyclerView.p) r1
                android.view.View r2 = r10.itemView
                r2.setLayoutParams(r1)
                goto L_0x0223
            L_0x0221:
                androidx.recyclerview.widget.RecyclerView$p r1 = (androidx.recyclerview.widget.RecyclerView.p) r1
            L_0x0223:
                r1.f1774a = r10
                if (r9 == 0) goto L_0x022a
                if (r0 == 0) goto L_0x022a
                goto L_0x022b
            L_0x022a:
                r7 = 0
            L_0x022b:
                r1.f1777d = r7
                return r10
            L_0x022e:
                java.lang.IndexOutOfBoundsException r0 = new java.lang.IndexOutOfBoundsException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "Invalid item position "
                r1.append(r2)
                r1.append(r3)
                java.lang.String r2 = "("
                r1.append(r2)
                r1.append(r3)
                java.lang.String r2 = "). Item count:"
                r1.append(r2)
                androidx.recyclerview.widget.RecyclerView r2 = androidx.recyclerview.widget.RecyclerView.this
                androidx.recyclerview.widget.RecyclerView$z r2 = r2.h0
                int r2 = r2.a()
                r1.append(r2)
                androidx.recyclerview.widget.RecyclerView r2 = androidx.recyclerview.widget.RecyclerView.this
                java.lang.String r2 = r2.i()
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.v.a(int, boolean, long):androidx.recyclerview.widget.RecyclerView$c0");
        }

        /* access modifiers changed from: package-private */
        public View c(int i2) {
            return this.f1784a.get(i2).itemView;
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.f1784a.clear();
            ArrayList<c0> arrayList = this.f1785b;
            if (arrayList != null) {
                arrayList.clear();
            }
        }

        /* access modifiers changed from: package-private */
        public void c(int i2, int i3) {
            int i4;
            int i5 = i3 + i2;
            for (int size = this.f1786c.size() - 1; size >= 0; size--) {
                c0 c0Var = this.f1786c.get(size);
                if (c0Var != null && (i4 = c0Var.mPosition) >= i2 && i4 < i5) {
                    c0Var.addFlags(2);
                    e(size);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public c0 b(int i2) {
            int size;
            int b2;
            ArrayList<c0> arrayList = this.f1785b;
            if (!(arrayList == null || (size = arrayList.size()) == 0)) {
                int i3 = 0;
                int i4 = 0;
                while (i4 < size) {
                    c0 c0Var = this.f1785b.get(i4);
                    if (c0Var.wasReturnedFromScrap() || c0Var.getLayoutPosition() != i2) {
                        i4++;
                    } else {
                        c0Var.addFlags(32);
                        return c0Var;
                    }
                }
                if (RecyclerView.this.l.hasStableIds() && (b2 = RecyclerView.this.f1727d.b(i2)) > 0 && b2 < RecyclerView.this.l.getItemCount()) {
                    long itemId = RecyclerView.this.l.getItemId(b2);
                    while (i3 < size) {
                        c0 c0Var2 = this.f1785b.get(i3);
                        if (c0Var2.wasReturnedFromScrap() || c0Var2.getItemId() != itemId) {
                            i3++;
                        } else {
                            c0Var2.addFlags(32);
                            return c0Var2;
                        }
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public void b(int i2, int i3) {
            int i4;
            int i5;
            int i6;
            int i7;
            if (i2 < i3) {
                i6 = i2;
                i5 = i3;
                i4 = -1;
            } else {
                i5 = i2;
                i6 = i3;
                i4 = 1;
            }
            int size = this.f1786c.size();
            for (int i8 = 0; i8 < size; i8++) {
                c0 c0Var = this.f1786c.get(i8);
                if (c0Var != null && (i7 = c0Var.mPosition) >= i6 && i7 <= i5) {
                    if (i7 == i2) {
                        c0Var.offsetPosition(i3 - i2, false);
                    } else {
                        c0Var.offsetPosition(i4, false);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            int size = this.f1786c.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.f1786c.get(i2).clearOldPosition();
            }
            int size2 = this.f1784a.size();
            for (int i3 = 0; i3 < size2; i3++) {
                this.f1784a.get(i3).clearOldPosition();
            }
            ArrayList<c0> arrayList = this.f1785b;
            if (arrayList != null) {
                int size3 = arrayList.size();
                for (int i4 = 0; i4 < size3; i4++) {
                    this.f1785b.get(i4).clearOldPosition();
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void
         arg types: [android.view.ViewGroup, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.v.a(int, boolean):androidx.recyclerview.widget.RecyclerView$c0
          androidx.recyclerview.widget.RecyclerView.v.a(int, int):void
          androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$c0, boolean):void
          androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void */
        private void a(ViewGroup viewGroup, boolean z) {
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (childAt instanceof ViewGroup) {
                    a((ViewGroup) childAt, true);
                }
            }
            if (z) {
                if (viewGroup.getVisibility() == 4) {
                    viewGroup.setVisibility(0);
                    viewGroup.setVisibility(4);
                    return;
                }
                int visibility = viewGroup.getVisibility();
                viewGroup.setVisibility(4);
                viewGroup.setVisibility(visibility);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(c0 c0Var, boolean z) {
            RecyclerView.e(c0Var);
            View view = c0Var.itemView;
            j jVar = RecyclerView.this.o0;
            if (jVar != null) {
                b.e.m.a b2 = jVar.b();
                b.e.m.u.a(view, b2 instanceof j.a ? ((j.a) b2).c(view) : null);
            }
            if (z) {
                a(c0Var);
            }
            c0Var.mOwnerRecyclerView = null;
            d().a(c0Var);
        }

        /* access modifiers changed from: package-private */
        public void a(View view) {
            c0 k2 = RecyclerView.k(view);
            k2.mScrapContainer = null;
            k2.mInChangeScrap = false;
            k2.clearReturnedFromScrapFlag();
            b(k2);
        }

        /* access modifiers changed from: package-private */
        public c0 a(int i2, boolean z) {
            View b2;
            int size = this.f1784a.size();
            int i3 = 0;
            int i4 = 0;
            while (i4 < size) {
                c0 c0Var = this.f1784a.get(i4);
                if (c0Var.wasReturnedFromScrap() || c0Var.getLayoutPosition() != i2 || c0Var.isInvalid() || (!RecyclerView.this.h0.f1801h && c0Var.isRemoved())) {
                    i4++;
                } else {
                    c0Var.addFlags(32);
                    return c0Var;
                }
            }
            if (z || (b2 = RecyclerView.this.f1728e.b(i2)) == null) {
                int size2 = this.f1786c.size();
                while (i3 < size2) {
                    c0 c0Var2 = this.f1786c.get(i3);
                    if (c0Var2.isInvalid() || c0Var2.getLayoutPosition() != i2 || c0Var2.isAttachedToTransitionOverlay()) {
                        i3++;
                    } else {
                        if (!z) {
                            this.f1786c.remove(i3);
                        }
                        return c0Var2;
                    }
                }
                return null;
            }
            c0 k2 = RecyclerView.k(b2);
            RecyclerView.this.f1728e.f(b2);
            int b3 = RecyclerView.this.f1728e.b(b2);
            if (b3 != -1) {
                RecyclerView.this.f1728e.a(b3);
                c(b2);
                k2.addFlags(8224);
                return k2;
            }
            throw new IllegalStateException("layout index should not be -1 after unhiding a view:" + k2 + RecyclerView.this.i());
        }

        /* access modifiers changed from: package-private */
        public c0 a(long j2, int i2, boolean z) {
            for (int size = this.f1784a.size() - 1; size >= 0; size--) {
                c0 c0Var = this.f1784a.get(size);
                if (c0Var.getItemId() == j2 && !c0Var.wasReturnedFromScrap()) {
                    if (i2 == c0Var.getItemViewType()) {
                        c0Var.addFlags(32);
                        if (c0Var.isRemoved() && !RecyclerView.this.h0.d()) {
                            c0Var.setFlags(2, 14);
                        }
                        return c0Var;
                    } else if (!z) {
                        this.f1784a.remove(size);
                        RecyclerView.this.removeDetachedView(c0Var.itemView, false);
                        a(c0Var.itemView);
                    }
                }
            }
            int size2 = this.f1786c.size();
            while (true) {
                size2--;
                if (size2 < 0) {
                    return null;
                }
                c0 c0Var2 = this.f1786c.get(size2);
                if (c0Var2.getItemId() == j2 && !c0Var2.isAttachedToTransitionOverlay()) {
                    if (i2 == c0Var2.getItemViewType()) {
                        if (!z) {
                            this.f1786c.remove(size2);
                        }
                        return c0Var2;
                    } else if (!z) {
                        e(size2);
                        return null;
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(c0 c0Var) {
            w wVar = RecyclerView.this.n;
            if (wVar != null) {
                wVar.a(c0Var);
            }
            g gVar = RecyclerView.this.l;
            if (gVar != null) {
                gVar.onViewRecycled(c0Var);
            }
            RecyclerView recyclerView = RecyclerView.this;
            if (recyclerView.h0 != null) {
                recyclerView.f1729f.h(c0Var);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(g gVar, g gVar2, boolean z) {
            a();
            d().a(gVar, gVar2, z);
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, int i3) {
            int size = this.f1786c.size();
            for (int i4 = 0; i4 < size; i4++) {
                c0 c0Var = this.f1786c.get(i4);
                if (c0Var != null && c0Var.mPosition >= i2) {
                    c0Var.offsetPosition(i3, true);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, int i3, boolean z) {
            int i4 = i2 + i3;
            for (int size = this.f1786c.size() - 1; size >= 0; size--) {
                c0 c0Var = this.f1786c.get(size);
                if (c0Var != null) {
                    int i5 = c0Var.mPosition;
                    if (i5 >= i4) {
                        c0Var.offsetPosition(-i3, z);
                    } else if (i5 >= i2) {
                        c0Var.addFlags(8);
                        e(size);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(a0 a0Var) {
            this.f1791h = a0Var;
        }

        /* access modifiers changed from: package-private */
        public void a(u uVar) {
            u uVar2 = this.f1790g;
            if (uVar2 != null) {
                uVar2.c();
            }
            this.f1790g = uVar;
            if (this.f1790g != null && RecyclerView.this.getAdapter() != null) {
                this.f1790g.a();
            }
        }
    }

    public interface w {
        void a(c0 c0Var);
    }

    public static abstract class y {

        public interface a {
        }

        public abstract int a();

        public abstract void a(int i2);

        /* access modifiers changed from: package-private */
        public abstract void a(int i2, int i3);

        /* access modifiers changed from: protected */
        public abstract void a(View view);

        public abstract boolean b();

        public abstract boolean c();

        /* access modifiers changed from: protected */
        public final void d() {
            throw null;
        }
    }

    static {
        int i2 = Build.VERSION.SDK_INT;
        z0 = i2 == 18 || i2 == 19 || i2 == 20;
        Class<?> cls = Integer.TYPE;
        F0 = new Class[]{Context.class, AttributeSet.class, cls, cls};
    }

    public RecyclerView(Context context) {
        this(context, null);
    }

    private void A() {
        boolean z2 = true;
        this.h0.a(1);
        a(this.h0);
        this.h0.f1803j = false;
        w();
        this.f1729f.a();
        q();
        I();
        N();
        z zVar = this.h0;
        if (!zVar.f1804k || !this.l0) {
            z2 = false;
        }
        zVar.f1802i = z2;
        this.l0 = false;
        this.k0 = false;
        z zVar2 = this.h0;
        zVar2.f1801h = zVar2.l;
        zVar2.f1799f = this.l.getItemCount();
        a(this.q0);
        if (this.h0.f1804k) {
            int a2 = this.f1728e.a();
            for (int i2 = 0; i2 < a2; i2++) {
                c0 k2 = k(this.f1728e.c(i2));
                if (!k2.shouldIgnore() && (!k2.isInvalid() || this.l.hasStableIds())) {
                    this.f1729f.c(k2, this.M.a(this.h0, k2, l.e(k2), k2.getUnmodifiedPayloads()));
                    if (this.h0.f1802i && k2.isUpdated() && !k2.isRemoved() && !k2.shouldIgnore() && !k2.isInvalid()) {
                        this.f1729f.a(c(k2), k2);
                    }
                }
            }
        }
        if (this.h0.l) {
            v();
            z zVar3 = this.h0;
            boolean z3 = zVar3.f1800g;
            zVar3.f1800g = false;
            this.m.e(this.f1725b, zVar3);
            this.h0.f1800g = z3;
            for (int i3 = 0; i3 < this.f1728e.a(); i3++) {
                c0 k3 = k(this.f1728e.c(i3));
                if (!k3.shouldIgnore() && !this.f1729f.c(k3)) {
                    int e2 = l.e(k3);
                    boolean hasAnyOfTheFlags = k3.hasAnyOfTheFlags(Utility.DEFAULT_STREAM_BUFFER_SIZE);
                    if (!hasAnyOfTheFlags) {
                        e2 |= CodedOutputStream.DEFAULT_BUFFER_SIZE;
                    }
                    l.c a3 = this.M.a(this.h0, k3, e2, k3.getUnmodifiedPayloads());
                    if (hasAnyOfTheFlags) {
                        a(k3, a3);
                    } else {
                        this.f1729f.a(k3, a3);
                    }
                }
            }
            a();
        } else {
            a();
        }
        r();
        c(false);
        this.h0.f1798e = 2;
    }

    private void B() {
        w();
        q();
        this.h0.a(6);
        this.f1727d.b();
        this.h0.f1799f = this.l.getItemCount();
        z zVar = this.h0;
        zVar.f1797d = 0;
        zVar.f1801h = false;
        this.m.e(this.f1725b, zVar);
        z zVar2 = this.h0;
        zVar2.f1800g = false;
        this.f1726c = null;
        zVar2.f1804k = zVar2.f1804k && this.M != null;
        this.h0.f1798e = 4;
        r();
        c(false);
    }

    private void C() {
        this.h0.a(4);
        w();
        q();
        z zVar = this.h0;
        zVar.f1798e = 1;
        if (zVar.f1804k) {
            for (int a2 = this.f1728e.a() - 1; a2 >= 0; a2--) {
                c0 k2 = k(this.f1728e.c(a2));
                if (!k2.shouldIgnore()) {
                    long c2 = c(k2);
                    l.c a3 = this.M.a(this.h0, k2);
                    c0 a4 = this.f1729f.a(c2);
                    if (a4 == null || a4.shouldIgnore()) {
                        this.f1729f.b(k2, a3);
                    } else {
                        boolean b2 = this.f1729f.b(a4);
                        boolean b3 = this.f1729f.b(k2);
                        if (!b2 || a4 != k2) {
                            l.c f2 = this.f1729f.f(a4);
                            this.f1729f.b(k2, a3);
                            l.c e2 = this.f1729f.e(k2);
                            if (f2 == null) {
                                a(c2, k2, a4);
                            } else {
                                a(a4, k2, f2, e2, b2, b3);
                            }
                        } else {
                            this.f1729f.b(k2, a3);
                        }
                    }
                }
            }
            this.f1729f.a(this.x0);
        }
        this.m.c(this.f1725b);
        z zVar2 = this.h0;
        zVar2.f1796c = zVar2.f1799f;
        this.D = false;
        this.E = false;
        zVar2.f1804k = false;
        zVar2.l = false;
        this.m.f1764h = false;
        ArrayList<c0> arrayList = this.f1725b.f1785b;
        if (arrayList != null) {
            arrayList.clear();
        }
        o oVar = this.m;
        if (oVar.n) {
            oVar.m = 0;
            oVar.n = false;
            this.f1725b.j();
        }
        this.m.g(this.h0);
        r();
        c(false);
        this.f1729f.a();
        int[] iArr = this.q0;
        if (k(iArr[0], iArr[1])) {
            d(0, 0);
        }
        J();
        L();
    }

    private View D() {
        c0 b2;
        int i2 = this.h0.m;
        if (i2 == -1) {
            i2 = 0;
        }
        int a2 = this.h0.a();
        int i3 = i2;
        while (i3 < a2) {
            c0 b3 = b(i3);
            if (b3 == null) {
                break;
            } else if (b3.itemView.hasFocusable()) {
                return b3.itemView;
            } else {
                i3++;
            }
        }
        int min = Math.min(a2, i2);
        while (true) {
            min--;
            if (min < 0 || (b2 = b(min)) == null) {
                return null;
            }
            if (b2.itemView.hasFocusable()) {
                return b2.itemView;
            }
        }
    }

    private boolean E() {
        int a2 = this.f1728e.a();
        for (int i2 = 0; i2 < a2; i2++) {
            c0 k2 = k(this.f1728e.c(i2));
            if (k2 != null && !k2.shouldIgnore() && k2.isUpdated()) {
                return true;
            }
        }
        return false;
    }

    @SuppressLint({"InlinedApi"})
    private void F() {
        if (b.e.m.u.j(this) == 0) {
            b.e.m.u.c(this, 8);
        }
    }

    private void G() {
        this.f1728e = new b(new e());
    }

    private boolean H() {
        return this.M != null && this.m.C();
    }

    private void I() {
        if (this.D) {
            this.f1727d.f();
            if (this.E) {
                this.m.d(this);
            }
        }
        if (H()) {
            this.f1727d.e();
        } else {
            this.f1727d.b();
        }
        boolean z2 = false;
        boolean z3 = this.k0 || this.l0;
        this.h0.f1804k = this.u && this.M != null && (this.D || z3 || this.m.f1764h) && (!this.D || this.l.hasStableIds());
        z zVar = this.h0;
        if (zVar.f1804k && z3 && !this.D && H()) {
            z2 = true;
        }
        zVar.l = z2;
    }

    private void J() {
        View view;
        if (this.d0 && this.l != null && hasFocus() && getDescendantFocusability() != 393216) {
            if (getDescendantFocusability() != 131072 || !isFocused()) {
                if (!isFocused()) {
                    View focusedChild = getFocusedChild();
                    if (!E0 || (focusedChild.getParent() != null && focusedChild.hasFocus())) {
                        if (!this.f1728e.c(focusedChild)) {
                            return;
                        }
                    } else if (this.f1728e.a() == 0) {
                        requestFocus();
                        return;
                    }
                }
                View view2 = null;
                c0 a2 = (this.h0.n == -1 || !this.l.hasStableIds()) ? null : a(this.h0.n);
                if (a2 != null && !this.f1728e.c(a2.itemView) && a2.itemView.hasFocusable()) {
                    view2 = a2.itemView;
                } else if (this.f1728e.a() > 0) {
                    view2 = D();
                }
                if (view2 != null) {
                    int i2 = this.h0.o;
                    if (((long) i2) == -1 || (view = view2.findViewById(i2)) == null || !view.isFocusable()) {
                        view = view2;
                    }
                    view.requestFocus();
                }
            }
        }
    }

    private void K() {
        boolean z2;
        EdgeEffect edgeEffect = this.I;
        if (edgeEffect != null) {
            edgeEffect.onRelease();
            z2 = this.I.isFinished();
        } else {
            z2 = false;
        }
        EdgeEffect edgeEffect2 = this.J;
        if (edgeEffect2 != null) {
            edgeEffect2.onRelease();
            z2 |= this.J.isFinished();
        }
        EdgeEffect edgeEffect3 = this.K;
        if (edgeEffect3 != null) {
            edgeEffect3.onRelease();
            z2 |= this.K.isFinished();
        }
        EdgeEffect edgeEffect4 = this.L;
        if (edgeEffect4 != null) {
            edgeEffect4.onRelease();
            z2 |= this.L.isFinished();
        }
        if (z2) {
            b.e.m.u.v(this);
        }
    }

    private void L() {
        z zVar = this.h0;
        zVar.n = -1;
        zVar.m = -1;
        zVar.o = -1;
    }

    private void M() {
        VelocityTracker velocityTracker = this.P;
        if (velocityTracker != null) {
            velocityTracker.clear();
        }
        f(0);
        K();
    }

    private void N() {
        int i2;
        c0 c0Var = null;
        View focusedChild = (!this.d0 || !hasFocus() || this.l == null) ? null : getFocusedChild();
        if (focusedChild != null) {
            c0Var = d(focusedChild);
        }
        if (c0Var == null) {
            L();
            return;
        }
        this.h0.n = this.l.hasStableIds() ? c0Var.getItemId() : -1;
        z zVar = this.h0;
        if (this.D) {
            i2 = -1;
        } else if (c0Var.isRemoved()) {
            i2 = c0Var.mOldPosition;
        } else {
            i2 = c0Var.getAdapterPosition();
        }
        zVar.m = i2;
        this.h0.o = l(c0Var.itemView);
    }

    private void O() {
        this.e0.b();
        o oVar = this.m;
        if (oVar != null) {
            oVar.B();
        }
    }

    private void d(c0 c0Var) {
        View view = c0Var.itemView;
        boolean z2 = view.getParent() == this;
        this.f1725b.c(e(view));
        if (c0Var.isTmpDetached()) {
            this.f1728e.a(view, -1, view.getLayoutParams(), true);
        } else if (!z2) {
            this.f1728e.a(view, true);
        } else {
            this.f1728e.a(view);
        }
    }

    private b.e.m.l getScrollingChildHelper() {
        if (this.r0 == null) {
            this.r0 = new b.e.m.l(this);
        }
        return this.r0;
    }

    private void y() {
        M();
        setScrollState(0);
    }

    private void z() {
        int i2 = this.z;
        this.z = 0;
        if (i2 != 0 && m()) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain();
            obtain.setEventType(2048);
            b.e.m.d0.b.a(obtain, i2);
            sendAccessibilityEventUnchecked(obtain);
        }
    }

    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        o oVar = this.m;
        if (oVar == null || !oVar.a(this, arrayList, i2, i3)) {
            super.addFocusables(arrayList, i2, i3);
        }
    }

    public void b(n nVar) {
        o oVar = this.m;
        if (oVar != null) {
            oVar.a("Cannot remove item decoration during a scroll  or layout");
        }
        this.o.remove(nVar);
        if (this.o.isEmpty()) {
            setWillNotDraw(getOverScrollMode() == 2);
        }
        o();
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z2) {
        if (this.v < 1) {
            this.v = 1;
        }
        if (!z2 && !this.x) {
            this.w = false;
        }
        if (this.v == 1) {
            if (z2 && this.w && !this.x && this.m != null && this.l != null) {
                c();
            }
            if (!this.x) {
                this.w = false;
            }
        }
        this.v--;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof p) && this.m.a((p) layoutParams);
    }

    public int computeHorizontalScrollExtent() {
        o oVar = this.m;
        if (oVar != null && oVar.a()) {
            return this.m.a(this.h0);
        }
        return 0;
    }

    public int computeHorizontalScrollOffset() {
        o oVar = this.m;
        if (oVar != null && oVar.a()) {
            return this.m.b(this.h0);
        }
        return 0;
    }

    public int computeHorizontalScrollRange() {
        o oVar = this.m;
        if (oVar != null && oVar.a()) {
            return this.m.c(this.h0);
        }
        return 0;
    }

    public int computeVerticalScrollExtent() {
        o oVar = this.m;
        if (oVar != null && oVar.b()) {
            return this.m.d(this.h0);
        }
        return 0;
    }

    public int computeVerticalScrollOffset() {
        o oVar = this.m;
        if (oVar != null && oVar.b()) {
            return this.m.e(this.h0);
        }
        return 0;
    }

    public int computeVerticalScrollRange() {
        o oVar = this.m;
        if (oVar != null && oVar.b()) {
            return this.m.f(this.h0);
        }
        return 0;
    }

    public boolean dispatchNestedFling(float f2, float f3, boolean z2) {
        return getScrollingChildHelper().a(f2, f3, z2);
    }

    public boolean dispatchNestedPreFling(float f2, float f3) {
        return getScrollingChildHelper().a(f2, f3);
    }

    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return getScrollingChildHelper().a(i2, i3, iArr, iArr2);
    }

    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return getScrollingChildHelper().a(i2, i3, i4, i5, iArr);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        onPopulateAccessibilityEvent(accessibilityEvent);
        return true;
    }

    /* access modifiers changed from: protected */
    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    public void dispatchSaveInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    public void draw(Canvas canvas) {
        boolean z2;
        boolean z3;
        super.draw(canvas);
        int size = this.o.size();
        boolean z4 = false;
        for (int i2 = 0; i2 < size; i2++) {
            this.o.get(i2).b(canvas, this, this.h0);
        }
        EdgeEffect edgeEffect = this.I;
        if (edgeEffect == null || edgeEffect.isFinished()) {
            z2 = false;
        } else {
            int save = canvas.save();
            int paddingBottom = this.f1730g ? getPaddingBottom() : 0;
            canvas.rotate(270.0f);
            canvas.translate((float) ((-getHeight()) + paddingBottom), Animation.CurveTimeline.LINEAR);
            EdgeEffect edgeEffect2 = this.I;
            z2 = edgeEffect2 != null && edgeEffect2.draw(canvas);
            canvas.restoreToCount(save);
        }
        EdgeEffect edgeEffect3 = this.J;
        if (edgeEffect3 != null && !edgeEffect3.isFinished()) {
            int save2 = canvas.save();
            if (this.f1730g) {
                canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
            }
            EdgeEffect edgeEffect4 = this.J;
            z2 |= edgeEffect4 != null && edgeEffect4.draw(canvas);
            canvas.restoreToCount(save2);
        }
        EdgeEffect edgeEffect5 = this.K;
        if (edgeEffect5 != null && !edgeEffect5.isFinished()) {
            int save3 = canvas.save();
            int width = getWidth();
            int paddingTop = this.f1730g ? getPaddingTop() : 0;
            canvas.rotate(90.0f);
            canvas.translate((float) (-paddingTop), (float) (-width));
            EdgeEffect edgeEffect6 = this.K;
            z2 |= edgeEffect6 != null && edgeEffect6.draw(canvas);
            canvas.restoreToCount(save3);
        }
        EdgeEffect edgeEffect7 = this.L;
        if (edgeEffect7 == null || edgeEffect7.isFinished()) {
            z3 = z2;
        } else {
            int save4 = canvas.save();
            canvas.rotate(180.0f);
            if (this.f1730g) {
                canvas.translate((float) ((-getWidth()) + getPaddingRight()), (float) ((-getHeight()) + getPaddingBottom()));
            } else {
                canvas.translate((float) (-getWidth()), (float) (-getHeight()));
            }
            EdgeEffect edgeEffect8 = this.L;
            if (edgeEffect8 != null && edgeEffect8.draw(canvas)) {
                z4 = true;
            }
            z3 = z4 | z2;
            canvas.restoreToCount(save4);
        }
        if (!z3 && this.M != null && this.o.size() > 0 && this.M.g()) {
            z3 = true;
        }
        if (z3) {
            b.e.m.u.v(this);
        }
    }

    public boolean drawChild(Canvas canvas, View view, long j2) {
        return super.drawChild(canvas, view, j2);
    }

    public void e(int i2) {
    }

    public boolean e(int i2, int i3) {
        o oVar = this.m;
        int i4 = 0;
        if (oVar == null) {
            Log.e("RecyclerView", "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument.");
            return false;
        } else if (this.x) {
            return false;
        } else {
            boolean a2 = oVar.a();
            boolean b2 = this.m.b();
            if (!a2 || Math.abs(i2) < this.W) {
                i2 = 0;
            }
            if (!b2 || Math.abs(i3) < this.W) {
                i3 = 0;
            }
            if (i2 == 0 && i3 == 0) {
                return false;
            }
            float f2 = (float) i2;
            float f3 = (float) i3;
            if (!dispatchNestedPreFling(f2, f3)) {
                boolean z2 = a2 || b2;
                dispatchNestedFling(f2, f3, z2);
                r rVar = this.V;
                if (rVar != null && rVar.a(i2, i3)) {
                    return true;
                }
                if (z2) {
                    if (a2) {
                        i4 = 1;
                    }
                    if (b2) {
                        i4 |= 2;
                    }
                    j(i4, 1);
                    int i5 = this.a0;
                    int max = Math.max(-i5, Math.min(i2, i5));
                    int i6 = this.a0;
                    this.e0.a(max, Math.max(-i6, Math.min(i3, i6)));
                    return true;
                }
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.I == null) {
            this.I = this.H.a(this, 0);
            if (this.f1730g) {
                this.I.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.I.setSize(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    public View focusSearch(View view, int i2) {
        View view2;
        boolean z2;
        View d2 = this.m.d(view, i2);
        if (d2 != null) {
            return d2;
        }
        boolean z3 = this.l != null && this.m != null && !n() && !this.x;
        FocusFinder instance = FocusFinder.getInstance();
        if (!z3 || !(i2 == 2 || i2 == 1)) {
            View findNextFocus = instance.findNextFocus(this, view, i2);
            if (findNextFocus != null || !z3) {
                view2 = findNextFocus;
            } else {
                b();
                if (c(view) == null) {
                    return null;
                }
                w();
                view2 = this.m.a(view, i2, this.f1725b, this.h0);
                c(false);
            }
        } else {
            if (this.m.b()) {
                int i3 = i2 == 2 ? 130 : 33;
                z2 = instance.findNextFocus(this, view, i3) == null;
                if (D0) {
                    i2 = i3;
                }
            } else {
                z2 = false;
            }
            if (!z2 && this.m.a()) {
                int i4 = (this.m.j() == 1) ^ (i2 == 2) ? 66 : 17;
                z2 = instance.findNextFocus(this, view, i4) == null;
                if (D0) {
                    i2 = i4;
                }
            }
            if (z2) {
                b();
                if (c(view) == null) {
                    return null;
                }
                w();
                this.m.a(view, i2, this.f1725b, this.h0);
                c(false);
            }
            view2 = instance.findNextFocus(this, view, i2);
        }
        if (view2 == null || view2.hasFocusable()) {
            return a(view, view2, i2) ? view2 : super.focusSearch(view, i2);
        }
        if (getFocusedChild() == null) {
            return super.focusSearch(view, i2);
        }
        a(view2, (View) null);
        return view;
    }

    /* access modifiers changed from: package-private */
    public void g() {
        if (this.K == null) {
            this.K = this.H.a(this, 2);
            if (this.f1730g) {
                this.K.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.K.setSize(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    public void g(View view) {
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        o oVar = this.m;
        if (oVar != null) {
            return oVar.c();
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + i());
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        o oVar = this.m;
        if (oVar != null) {
            return oVar.a(getContext(), attributeSet);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + i());
    }

    public CharSequence getAccessibilityClassName() {
        return "androidx.recyclerview.widget.RecyclerView";
    }

    public g getAdapter() {
        return this.l;
    }

    public int getBaseline() {
        o oVar = this.m;
        if (oVar != null) {
            return oVar.d();
        }
        return super.getBaseline();
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        j jVar = this.p0;
        if (jVar == null) {
            return super.getChildDrawingOrder(i2, i3);
        }
        return jVar.a(i2, i3);
    }

    public boolean getClipToPadding() {
        return this.f1730g;
    }

    public j getCompatAccessibilityDelegate() {
        return this.o0;
    }

    public k getEdgeEffectFactory() {
        return this.H;
    }

    public l getItemAnimator() {
        return this.M;
    }

    public int getItemDecorationCount() {
        return this.o.size();
    }

    public o getLayoutManager() {
        return this.m;
    }

    public int getMaxFlingVelocity() {
        return this.a0;
    }

    public int getMinFlingVelocity() {
        return this.W;
    }

    /* access modifiers changed from: package-private */
    public long getNanoTime() {
        if (C0) {
            return System.nanoTime();
        }
        return 0;
    }

    public r getOnFlingListener() {
        return this.V;
    }

    public boolean getPreserveFocusAfterLayout() {
        return this.d0;
    }

    public u getRecycledViewPool() {
        return this.f1725b.d();
    }

    public int getScrollState() {
        return this.N;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (this.J == null) {
            this.J = this.H.a(this, 1);
            if (this.f1730g) {
                this.J.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.J.setSize(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    public void h(int i2, int i3) {
    }

    public void h(View view) {
    }

    public boolean hasNestedScrollingParent() {
        return getScrollingChildHelper().a();
    }

    /* access modifiers changed from: package-private */
    public String i() {
        return RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER + super.toString() + ", adapter:" + this.l + ", layout:" + this.m + ", context:" + getContext();
    }

    public boolean isAttachedToWindow() {
        return this.r;
    }

    public final boolean isLayoutSuppressed() {
        return this.x;
    }

    public boolean isNestedScrollingEnabled() {
        return getScrollingChildHelper().b();
    }

    public boolean j() {
        return !this.u || this.D || this.f1727d.c();
    }

    /* access modifiers changed from: package-private */
    public void k() {
        this.f1727d = new a(new f());
    }

    /* access modifiers changed from: package-private */
    public void l() {
        this.L = null;
        this.J = null;
        this.K = null;
        this.I = null;
    }

    /* access modifiers changed from: package-private */
    public boolean m() {
        AccessibilityManager accessibilityManager = this.B;
        return accessibilityManager != null && accessibilityManager.isEnabled();
    }

    public boolean n() {
        return this.F > 0;
    }

    /* access modifiers changed from: package-private */
    public void o() {
        int b2 = this.f1728e.b();
        for (int i2 = 0; i2 < b2; i2++) {
            ((p) this.f1728e.d(i2).getLayoutParams()).f1776c = true;
        }
        this.f1725b.g();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004f, code lost:
        if (r0 >= 30.0f) goto L_0x0054;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAttachedToWindow() {
        /*
            r4 = this;
            super.onAttachedToWindow()
            r0 = 0
            r4.F = r0
            r1 = 1
            r4.r = r1
            boolean r2 = r4.u
            if (r2 == 0) goto L_0x0014
            boolean r2 = r4.isLayoutRequested()
            if (r2 != 0) goto L_0x0014
            goto L_0x0015
        L_0x0014:
            r1 = 0
        L_0x0015:
            r4.u = r1
            androidx.recyclerview.widget.RecyclerView$o r1 = r4.m
            if (r1 == 0) goto L_0x001e
            r1.a(r4)
        L_0x001e:
            r4.n0 = r0
            boolean r0 = androidx.recyclerview.widget.RecyclerView.C0
            if (r0 == 0) goto L_0x0067
            java.lang.ThreadLocal<androidx.recyclerview.widget.e> r0 = androidx.recyclerview.widget.e.f1913e
            java.lang.Object r0 = r0.get()
            androidx.recyclerview.widget.e r0 = (androidx.recyclerview.widget.e) r0
            r4.f0 = r0
            androidx.recyclerview.widget.e r0 = r4.f0
            if (r0 != 0) goto L_0x0062
            androidx.recyclerview.widget.e r0 = new androidx.recyclerview.widget.e
            r0.<init>()
            r4.f0 = r0
            android.view.Display r0 = b.e.m.u.h(r4)
            r1 = 1114636288(0x42700000, float:60.0)
            boolean r2 = r4.isInEditMode()
            if (r2 != 0) goto L_0x0052
            if (r0 == 0) goto L_0x0052
            float r0 = r0.getRefreshRate()
            r2 = 1106247680(0x41f00000, float:30.0)
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 < 0) goto L_0x0052
            goto L_0x0054
        L_0x0052:
            r0 = 1114636288(0x42700000, float:60.0)
        L_0x0054:
            androidx.recyclerview.widget.e r1 = r4.f0
            r2 = 1315859240(0x4e6e6b28, float:1.0E9)
            float r2 = r2 / r0
            long r2 = (long) r2
            r1.f1917c = r2
            java.lang.ThreadLocal<androidx.recyclerview.widget.e> r0 = androidx.recyclerview.widget.e.f1913e
            r0.set(r1)
        L_0x0062:
            androidx.recyclerview.widget.e r0 = r4.f0
            r0.a(r4)
        L_0x0067:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.onAttachedToWindow():void");
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        e eVar;
        super.onDetachedFromWindow();
        l lVar = this.M;
        if (lVar != null) {
            lVar.b();
        }
        x();
        this.r = false;
        o oVar = this.m;
        if (oVar != null) {
            oVar.a(this, this.f1725b);
        }
        this.v0.clear();
        removeCallbacks(this.w0);
        this.f1729f.b();
        if (C0 && (eVar = this.f0) != null) {
            eVar.b(this);
            this.f0 = null;
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int size = this.o.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.o.get(i2).a(canvas, this, this.h0);
        }
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        float f2;
        float f3;
        if (this.m != null && !this.x && motionEvent.getAction() == 8) {
            if ((motionEvent.getSource() & 2) != 0) {
                f3 = this.m.b() ? -motionEvent.getAxisValue(9) : Animation.CurveTimeline.LINEAR;
                if (this.m.a()) {
                    f2 = motionEvent.getAxisValue(10);
                    if (!(f3 == Animation.CurveTimeline.LINEAR && f2 == Animation.CurveTimeline.LINEAR)) {
                        a((int) (f2 * this.b0), (int) (f3 * this.c0), motionEvent);
                    }
                }
            } else {
                if ((motionEvent.getSource() & 4194304) != 0) {
                    float axisValue = motionEvent.getAxisValue(26);
                    if (this.m.b()) {
                        f3 = -axisValue;
                    } else if (this.m.a()) {
                        f2 = axisValue;
                        f3 = Animation.CurveTimeline.LINEAR;
                        a((int) (f2 * this.b0), (int) (f3 * this.c0), motionEvent);
                    }
                }
                f3 = Animation.CurveTimeline.LINEAR;
            }
            f2 = Animation.CurveTimeline.LINEAR;
            a((int) (f2 * this.b0), (int) (f3 * this.c0), motionEvent);
        }
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        if (this.x) {
            return false;
        }
        this.q = null;
        if (b(motionEvent)) {
            y();
            return true;
        }
        o oVar = this.m;
        if (oVar == null) {
            return false;
        }
        boolean a2 = oVar.a();
        boolean b2 = this.m.b();
        if (this.P == null) {
            this.P = VelocityTracker.obtain();
        }
        this.P.addMovement(motionEvent);
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            if (this.y) {
                this.y = false;
            }
            this.O = motionEvent.getPointerId(0);
            int x2 = (int) (motionEvent.getX() + 0.5f);
            this.S = x2;
            this.Q = x2;
            int y2 = (int) (motionEvent.getY() + 0.5f);
            this.T = y2;
            this.R = y2;
            if (this.N == 2) {
                getParent().requestDisallowInterceptTouchEvent(true);
                setScrollState(1);
                f(1);
            }
            int[] iArr = this.t0;
            iArr[1] = 0;
            iArr[0] = 0;
            int i2 = a2 ? 1 : 0;
            if (b2) {
                i2 |= 2;
            }
            j(i2, 0);
        } else if (actionMasked == 1) {
            this.P.clear();
            f(0);
        } else if (actionMasked == 2) {
            int findPointerIndex = motionEvent.findPointerIndex(this.O);
            if (findPointerIndex < 0) {
                Log.e("RecyclerView", "Error processing scroll; pointer index for id " + this.O + " not found. Did any MotionEvents get skipped?");
                return false;
            }
            int x3 = (int) (motionEvent.getX(findPointerIndex) + 0.5f);
            int y3 = (int) (motionEvent.getY(findPointerIndex) + 0.5f);
            if (this.N != 1) {
                int i3 = x3 - this.Q;
                int i4 = y3 - this.R;
                if (!a2 || Math.abs(i3) <= this.U) {
                    z2 = false;
                } else {
                    this.S = x3;
                    z2 = true;
                }
                if (b2 && Math.abs(i4) > this.U) {
                    this.T = y3;
                    z2 = true;
                }
                if (z2) {
                    setScrollState(1);
                }
            }
        } else if (actionMasked == 3) {
            y();
        } else if (actionMasked == 5) {
            this.O = motionEvent.getPointerId(actionIndex);
            int x4 = (int) (motionEvent.getX(actionIndex) + 0.5f);
            this.S = x4;
            this.Q = x4;
            int y4 = (int) (motionEvent.getY(actionIndex) + 0.5f);
            this.T = y4;
            this.R = y4;
        } else if (actionMasked == 6) {
            c(motionEvent);
        }
        if (this.N == 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        b.e.i.c.a("RV OnLayout");
        c();
        b.e.i.c.a();
        this.u = true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        o oVar = this.m;
        if (oVar == null) {
            c(i2, i3);
            return;
        }
        boolean z2 = false;
        if (oVar.u()) {
            int mode = View.MeasureSpec.getMode(i2);
            int mode2 = View.MeasureSpec.getMode(i3);
            this.m.a(this.f1725b, this.h0, i2, i3);
            if (mode == 1073741824 && mode2 == 1073741824) {
                z2 = true;
            }
            if (!z2 && this.l != null) {
                if (this.h0.f1798e == 1) {
                    A();
                }
                this.m.b(i2, i3);
                this.h0.f1803j = true;
                B();
                this.m.d(i2, i3);
                if (this.m.A()) {
                    this.m.b(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824));
                    this.h0.f1803j = true;
                    B();
                    this.m.d(i2, i3);
                }
            }
        } else if (this.s) {
            this.m.a(this.f1725b, this.h0, i2, i3);
        } else {
            if (this.A) {
                w();
                q();
                I();
                r();
                z zVar = this.h0;
                if (zVar.l) {
                    zVar.f1801h = true;
                } else {
                    this.f1727d.b();
                    this.h0.f1801h = false;
                }
                this.A = false;
                c(false);
            } else if (this.h0.l) {
                setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
                return;
            }
            g gVar = this.l;
            if (gVar != null) {
                this.h0.f1799f = gVar.getItemCount();
            } else {
                this.h0.f1799f = 0;
            }
            w();
            this.m.a(this.f1725b, this.h0, i2, i3);
            c(false);
            this.h0.f1801h = false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        if (n()) {
            return false;
        }
        return super.onRequestFocusInDescendants(i2, rect);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        Parcelable parcelable2;
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        this.f1726c = (SavedState) parcelable;
        super.onRestoreInstanceState(this.f1726c.a());
        o oVar = this.m;
        if (oVar != null && (parcelable2 = this.f1726c.f1735c) != null) {
            oVar.a(parcelable2);
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        SavedState savedState2 = this.f1726c;
        if (savedState2 != null) {
            savedState.a(savedState2);
        } else {
            o oVar = this.m;
            if (oVar != null) {
                savedState.f1735c = oVar.x();
            } else {
                savedState.f1735c = null;
            }
        }
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4 || i3 != i5) {
            l();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00f8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r18) {
        /*
            r17 = this;
            r6 = r17
            r7 = r18
            boolean r0 = r6.x
            r8 = 0
            if (r0 != 0) goto L_0x01e8
            boolean r0 = r6.y
            if (r0 == 0) goto L_0x000f
            goto L_0x01e8
        L_0x000f:
            boolean r0 = r17.a(r18)
            r9 = 1
            if (r0 == 0) goto L_0x001a
            r17.y()
            return r9
        L_0x001a:
            androidx.recyclerview.widget.RecyclerView$o r0 = r6.m
            if (r0 != 0) goto L_0x001f
            return r8
        L_0x001f:
            boolean r10 = r0.a()
            androidx.recyclerview.widget.RecyclerView$o r0 = r6.m
            boolean r11 = r0.b()
            android.view.VelocityTracker r0 = r6.P
            if (r0 != 0) goto L_0x0033
            android.view.VelocityTracker r0 = android.view.VelocityTracker.obtain()
            r6.P = r0
        L_0x0033:
            int r0 = r18.getActionMasked()
            int r1 = r18.getActionIndex()
            if (r0 != 0) goto L_0x0043
            int[] r2 = r6.t0
            r2[r9] = r8
            r2[r8] = r8
        L_0x0043:
            android.view.MotionEvent r12 = android.view.MotionEvent.obtain(r18)
            int[] r2 = r6.t0
            r3 = r2[r8]
            float r3 = (float) r3
            r2 = r2[r9]
            float r2 = (float) r2
            r12.offsetLocation(r3, r2)
            r2 = 1056964608(0x3f000000, float:0.5)
            if (r0 == 0) goto L_0x01b7
            if (r0 == r9) goto L_0x0175
            r3 = 2
            if (r0 == r3) goto L_0x008c
            r3 = 3
            if (r0 == r3) goto L_0x0087
            r3 = 5
            if (r0 == r3) goto L_0x006b
            r1 = 6
            if (r0 == r1) goto L_0x0066
            goto L_0x01dd
        L_0x0066:
            r17.c(r18)
            goto L_0x01dd
        L_0x006b:
            int r0 = r7.getPointerId(r1)
            r6.O = r0
            float r0 = r7.getX(r1)
            float r0 = r0 + r2
            int r0 = (int) r0
            r6.S = r0
            r6.Q = r0
            float r0 = r7.getY(r1)
            float r0 = r0 + r2
            int r0 = (int) r0
            r6.T = r0
            r6.R = r0
            goto L_0x01dd
        L_0x0087:
            r17.y()
            goto L_0x01dd
        L_0x008c:
            int r0 = r6.O
            int r0 = r7.findPointerIndex(r0)
            if (r0 >= 0) goto L_0x00b2
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Error processing scroll; pointer index for id "
            r0.append(r1)
            int r1 = r6.O
            r0.append(r1)
            java.lang.String r1 = " not found. Did any MotionEvents get skipped?"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "RecyclerView"
            android.util.Log.e(r1, r0)
            return r8
        L_0x00b2:
            float r1 = r7.getX(r0)
            float r1 = r1 + r2
            int r13 = (int) r1
            float r0 = r7.getY(r0)
            float r0 = r0 + r2
            int r14 = (int) r0
            int r0 = r6.S
            int r0 = r0 - r13
            int r1 = r6.T
            int r1 = r1 - r14
            int r2 = r6.N
            if (r2 == r9) goto L_0x00fb
            if (r10 == 0) goto L_0x00df
            if (r0 <= 0) goto L_0x00d4
            int r2 = r6.U
            int r0 = r0 - r2
            int r0 = java.lang.Math.max(r8, r0)
            goto L_0x00db
        L_0x00d4:
            int r2 = r6.U
            int r0 = r0 + r2
            int r0 = java.lang.Math.min(r8, r0)
        L_0x00db:
            if (r0 == 0) goto L_0x00df
            r2 = 1
            goto L_0x00e0
        L_0x00df:
            r2 = 0
        L_0x00e0:
            if (r11 == 0) goto L_0x00f6
            if (r1 <= 0) goto L_0x00ec
            int r3 = r6.U
            int r1 = r1 - r3
            int r1 = java.lang.Math.max(r8, r1)
            goto L_0x00f3
        L_0x00ec:
            int r3 = r6.U
            int r1 = r1 + r3
            int r1 = java.lang.Math.min(r8, r1)
        L_0x00f3:
            if (r1 == 0) goto L_0x00f6
            r2 = 1
        L_0x00f6:
            if (r2 == 0) goto L_0x00fb
            r6.setScrollState(r9)
        L_0x00fb:
            r15 = r0
            r16 = r1
            int r0 = r6.N
            if (r0 != r9) goto L_0x01dd
            int[] r0 = r6.u0
            r0[r8] = r8
            r0[r9] = r8
            if (r10 == 0) goto L_0x010c
            r1 = r15
            goto L_0x010d
        L_0x010c:
            r1 = 0
        L_0x010d:
            if (r11 == 0) goto L_0x0112
            r2 = r16
            goto L_0x0113
        L_0x0112:
            r2 = 0
        L_0x0113:
            int[] r3 = r6.u0
            int[] r4 = r6.s0
            r5 = 0
            r0 = r17
            boolean r0 = r0.a(r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x0142
            int[] r0 = r6.u0
            r1 = r0[r8]
            int r15 = r15 - r1
            r0 = r0[r9]
            int r16 = r16 - r0
            int[] r0 = r6.t0
            r1 = r0[r8]
            int[] r2 = r6.s0
            r3 = r2[r8]
            int r1 = r1 + r3
            r0[r8] = r1
            r1 = r0[r9]
            r2 = r2[r9]
            int r1 = r1 + r2
            r0[r9] = r1
            android.view.ViewParent r0 = r17.getParent()
            r0.requestDisallowInterceptTouchEvent(r9)
        L_0x0142:
            r0 = r16
            int[] r1 = r6.s0
            r2 = r1[r8]
            int r13 = r13 - r2
            r6.S = r13
            r1 = r1[r9]
            int r14 = r14 - r1
            r6.T = r14
            if (r10 == 0) goto L_0x0154
            r1 = r15
            goto L_0x0155
        L_0x0154:
            r1 = 0
        L_0x0155:
            if (r11 == 0) goto L_0x0159
            r2 = r0
            goto L_0x015a
        L_0x0159:
            r2 = 0
        L_0x015a:
            boolean r1 = r6.a(r1, r2, r7)
            if (r1 == 0) goto L_0x0167
            android.view.ViewParent r1 = r17.getParent()
            r1.requestDisallowInterceptTouchEvent(r9)
        L_0x0167:
            androidx.recyclerview.widget.e r1 = r6.f0
            if (r1 == 0) goto L_0x01dd
            if (r15 != 0) goto L_0x016f
            if (r0 == 0) goto L_0x01dd
        L_0x016f:
            androidx.recyclerview.widget.e r1 = r6.f0
            r1.a(r6, r15, r0)
            goto L_0x01dd
        L_0x0175:
            android.view.VelocityTracker r0 = r6.P
            r0.addMovement(r12)
            android.view.VelocityTracker r0 = r6.P
            r1 = 1000(0x3e8, float:1.401E-42)
            int r2 = r6.a0
            float r2 = (float) r2
            r0.computeCurrentVelocity(r1, r2)
            r0 = 0
            if (r10 == 0) goto L_0x0191
            android.view.VelocityTracker r1 = r6.P
            int r2 = r6.O
            float r1 = r1.getXVelocity(r2)
            float r1 = -r1
            goto L_0x0192
        L_0x0191:
            r1 = 0
        L_0x0192:
            if (r11 == 0) goto L_0x019e
            android.view.VelocityTracker r2 = r6.P
            int r3 = r6.O
            float r2 = r2.getYVelocity(r3)
            float r2 = -r2
            goto L_0x019f
        L_0x019e:
            r2 = 0
        L_0x019f:
            int r3 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r3 != 0) goto L_0x01a7
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 == 0) goto L_0x01af
        L_0x01a7:
            int r0 = (int) r1
            int r1 = (int) r2
            boolean r0 = r6.e(r0, r1)
            if (r0 != 0) goto L_0x01b2
        L_0x01af:
            r6.setScrollState(r8)
        L_0x01b2:
            r17.M()
            r8 = 1
            goto L_0x01dd
        L_0x01b7:
            int r0 = r7.getPointerId(r8)
            r6.O = r0
            float r0 = r18.getX()
            float r0 = r0 + r2
            int r0 = (int) r0
            r6.S = r0
            r6.Q = r0
            float r0 = r18.getY()
            float r0 = r0 + r2
            int r0 = (int) r0
            r6.T = r0
            r6.R = r0
            if (r10 == 0) goto L_0x01d5
            r0 = 1
            goto L_0x01d6
        L_0x01d5:
            r0 = 0
        L_0x01d6:
            if (r11 == 0) goto L_0x01da
            r0 = r0 | 2
        L_0x01da:
            r6.j(r0, r8)
        L_0x01dd:
            if (r8 != 0) goto L_0x01e4
            android.view.VelocityTracker r0 = r6.P
            r0.addMovement(r12)
        L_0x01e4:
            r12.recycle()
            return r9
        L_0x01e8:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    /* access modifiers changed from: package-private */
    public void p() {
        int b2 = this.f1728e.b();
        for (int i2 = 0; i2 < b2; i2++) {
            c0 k2 = k(this.f1728e.d(i2));
            if (k2 != null && !k2.shouldIgnore()) {
                k2.addFlags(6);
            }
        }
        o();
        this.f1725b.h();
    }

    /* access modifiers changed from: package-private */
    public void q() {
        this.F++;
    }

    /* access modifiers changed from: package-private */
    public void r() {
        a(true);
    }

    /* access modifiers changed from: protected */
    public void removeDetachedView(View view, boolean z2) {
        c0 k2 = k(view);
        if (k2 != null) {
            if (k2.isTmpDetached()) {
                k2.clearTmpDetachFlag();
            } else if (!k2.shouldIgnore()) {
                throw new IllegalArgumentException("Called removeDetachedView with a view which is not flagged as tmp detached." + k2 + i());
            }
        }
        view.clearAnimation();
        b(view);
        super.removeDetachedView(view, z2);
    }

    public void requestChildFocus(View view, View view2) {
        if (!this.m.a(this, this.h0, view, view2) && view2 != null) {
            a(view, view2);
        }
        super.requestChildFocus(view, view2);
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z2) {
        return this.m.a(this, view, rect, z2);
    }

    public void requestDisallowInterceptTouchEvent(boolean z2) {
        int size = this.p.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.p.get(i2).a(z2);
        }
        super.requestDisallowInterceptTouchEvent(z2);
    }

    public void requestLayout() {
        if (this.v != 0 || this.x) {
            this.w = true;
        } else {
            super.requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void s() {
        if (!this.n0 && this.r) {
            b.e.m.u.a(this, this.w0);
            this.n0 = true;
        }
    }

    public void scrollBy(int i2, int i3) {
        o oVar = this.m;
        if (oVar == null) {
            Log.e("RecyclerView", "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.x) {
            boolean a2 = oVar.a();
            boolean b2 = this.m.b();
            if (a2 || b2) {
                if (!a2) {
                    i2 = 0;
                }
                if (!b2) {
                    i3 = 0;
                }
                a(i2, i3, (MotionEvent) null);
            }
        }
    }

    public void scrollTo(int i2, int i3) {
        Log.w("RecyclerView", "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead");
    }

    public void sendAccessibilityEventUnchecked(AccessibilityEvent accessibilityEvent) {
        if (!a(accessibilityEvent)) {
            super.sendAccessibilityEventUnchecked(accessibilityEvent);
        }
    }

    public void setAccessibilityDelegateCompat(j jVar) {
        this.o0 = jVar;
        b.e.m.u.a(this, this.o0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView$g, boolean, boolean):void
     arg types: [androidx.recyclerview.widget.RecyclerView$g, int, int]
     candidates:
      androidx.recyclerview.widget.RecyclerView.a(long, androidx.recyclerview.widget.RecyclerView$c0, androidx.recyclerview.widget.RecyclerView$c0):void
      androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.a(android.view.View, android.view.View, int):boolean
      androidx.recyclerview.widget.RecyclerView.a(int, int, android.view.animation.Interpolator):void
      androidx.recyclerview.widget.RecyclerView.a(int, int, java.lang.Object):void
      androidx.recyclerview.widget.RecyclerView.a(int, int, boolean):void
      androidx.recyclerview.widget.RecyclerView.a(int, int, int[]):void
      androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView$c0, androidx.recyclerview.widget.RecyclerView$l$c, androidx.recyclerview.widget.RecyclerView$l$c):void
      androidx.recyclerview.widget.RecyclerView.a(int, int, android.view.MotionEvent):boolean
      androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView$g, boolean, boolean):void */
    public void setAdapter(g gVar) {
        setLayoutFrozen(false);
        a(gVar, false, true);
        b(false);
        requestLayout();
    }

    public void setChildDrawingOrderCallback(j jVar) {
        if (jVar != this.p0) {
            this.p0 = jVar;
            setChildrenDrawingOrderEnabled(this.p0 != null);
        }
    }

    public void setClipToPadding(boolean z2) {
        if (z2 != this.f1730g) {
            l();
        }
        this.f1730g = z2;
        super.setClipToPadding(z2);
        if (this.u) {
            requestLayout();
        }
    }

    public void setEdgeEffectFactory(k kVar) {
        b.e.l.f.a(kVar);
        this.H = kVar;
        l();
    }

    public void setHasFixedSize(boolean z2) {
        this.s = z2;
    }

    public void setItemAnimator(l lVar) {
        l lVar2 = this.M;
        if (lVar2 != null) {
            lVar2.b();
            this.M.a((l.b) null);
        }
        this.M = lVar;
        l lVar3 = this.M;
        if (lVar3 != null) {
            lVar3.a(this.m0);
        }
    }

    public void setItemViewCacheSize(int i2) {
        this.f1725b.f(i2);
    }

    @Deprecated
    public void setLayoutFrozen(boolean z2) {
        suppressLayout(z2);
    }

    public void setLayoutManager(o oVar) {
        if (oVar != this.m) {
            x();
            if (this.m != null) {
                l lVar = this.M;
                if (lVar != null) {
                    lVar.b();
                }
                this.m.b(this.f1725b);
                this.m.c(this.f1725b);
                this.f1725b.a();
                if (this.r) {
                    this.m.a(this, this.f1725b);
                }
                this.m.f((RecyclerView) null);
                this.m = null;
            } else {
                this.f1725b.a();
            }
            this.f1728e.c();
            this.m = oVar;
            if (oVar != null) {
                if (oVar.f1758b == null) {
                    this.m.f(this);
                    if (this.r) {
                        this.m.a(this);
                    }
                } else {
                    throw new IllegalArgumentException("LayoutManager " + oVar + " is already attached to a RecyclerView:" + oVar.f1758b.i());
                }
            }
            this.f1725b.j();
            requestLayout();
        }
    }

    @Deprecated
    public void setLayoutTransition(LayoutTransition layoutTransition) {
        if (Build.VERSION.SDK_INT < 18) {
            if (layoutTransition == null) {
                suppressLayout(false);
                return;
            } else if (layoutTransition.getAnimator(0) == null && layoutTransition.getAnimator(1) == null && layoutTransition.getAnimator(2) == null && layoutTransition.getAnimator(3) == null && layoutTransition.getAnimator(4) == null) {
                suppressLayout(true);
                return;
            }
        }
        if (layoutTransition == null) {
            super.setLayoutTransition(null);
            return;
        }
        throw new IllegalArgumentException("Providing a LayoutTransition into RecyclerView is not supported. Please use setItemAnimator() instead for animating changes to the items in this RecyclerView");
    }

    public void setNestedScrollingEnabled(boolean z2) {
        getScrollingChildHelper().a(z2);
    }

    public void setOnFlingListener(r rVar) {
        this.V = rVar;
    }

    @Deprecated
    public void setOnScrollListener(t tVar) {
        this.i0 = tVar;
    }

    public void setPreserveFocusAfterLayout(boolean z2) {
        this.d0 = z2;
    }

    public void setRecycledViewPool(u uVar) {
        this.f1725b.a(uVar);
    }

    public void setRecyclerListener(w wVar) {
        this.n = wVar;
    }

    /* access modifiers changed from: package-private */
    public void setScrollState(int i2) {
        if (i2 != this.N) {
            this.N = i2;
            if (i2 != 2) {
                O();
            }
            a(i2);
        }
    }

    public void setScrollingTouchSlop(int i2) {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        if (i2 != 0) {
            if (i2 != 1) {
                Log.w("RecyclerView", "setScrollingTouchSlop(): bad argument constant " + i2 + "; using default value");
            } else {
                this.U = viewConfiguration.getScaledPagingTouchSlop();
                return;
            }
        }
        this.U = viewConfiguration.getScaledTouchSlop();
    }

    public void setViewCacheExtension(a0 a0Var) {
        this.f1725b.a(a0Var);
    }

    public boolean startNestedScroll(int i2) {
        return getScrollingChildHelper().b(i2);
    }

    public void stopNestedScroll() {
        getScrollingChildHelper().c();
    }

    public final void suppressLayout(boolean z2) {
        if (z2 != this.x) {
            a("Do not suppressLayout in layout or scroll");
            if (!z2) {
                this.x = false;
                if (!(!this.w || this.m == null || this.l == null)) {
                    requestLayout();
                }
                this.w = false;
                return;
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            onTouchEvent(MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0));
            this.x = true;
            this.y = true;
            x();
        }
    }

    /* access modifiers changed from: package-private */
    public void t() {
        l lVar = this.M;
        if (lVar != null) {
            lVar.b();
        }
        o oVar = this.m;
        if (oVar != null) {
            oVar.b(this.f1725b);
            this.m.c(this.f1725b);
        }
        this.f1725b.a();
    }

    /* access modifiers changed from: package-private */
    public void u() {
        c0 c0Var;
        int a2 = this.f1728e.a();
        for (int i2 = 0; i2 < a2; i2++) {
            View c2 = this.f1728e.c(i2);
            c0 e2 = e(c2);
            if (!(e2 == null || (c0Var = e2.mShadowingHolder) == null)) {
                View view = c0Var.itemView;
                int left = c2.getLeft();
                int top = c2.getTop();
                if (left != view.getLeft() || top != view.getTop()) {
                    view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void v() {
        int b2 = this.f1728e.b();
        for (int i2 = 0; i2 < b2; i2++) {
            c0 k2 = k(this.f1728e.d(i2));
            if (!k2.shouldIgnore()) {
                k2.saveOldPosition();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void w() {
        this.v++;
        if (this.v == 1 && !this.x) {
            this.w = false;
        }
    }

    public void x() {
        setScrollState(0);
        O();
    }

    class f implements a.C0020a {
        f() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.a(int, boolean):androidx.recyclerview.widget.RecyclerView$c0
         arg types: [int, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.a(android.content.Context, java.lang.String):java.lang.String
          androidx.recyclerview.widget.RecyclerView.a(android.view.View, android.graphics.Rect):void
          androidx.recyclerview.widget.RecyclerView.a(android.view.View, android.view.View):void
          androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView, int):void
          androidx.recyclerview.widget.RecyclerView.a(int, int):void
          androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView$c0, androidx.recyclerview.widget.RecyclerView$l$c):void
          androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView$n, int):void
          androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView$c0, int):boolean
          androidx.recyclerview.widget.RecyclerView.a(int, boolean):androidx.recyclerview.widget.RecyclerView$c0 */
        public c0 a(int i2) {
            c0 a2 = RecyclerView.this.a(i2, true);
            if (a2 != null && !RecyclerView.this.f1728e.c(a2.itemView)) {
                return a2;
            }
            return null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.a(int, int, boolean):void
         arg types: [int, int, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.a(long, androidx.recyclerview.widget.RecyclerView$c0, androidx.recyclerview.widget.RecyclerView$c0):void
          androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView$g, boolean, boolean):void
          androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView, int, int):void
          androidx.recyclerview.widget.RecyclerView.a(android.view.View, android.view.View, int):boolean
          androidx.recyclerview.widget.RecyclerView.a(int, int, android.view.animation.Interpolator):void
          androidx.recyclerview.widget.RecyclerView.a(int, int, java.lang.Object):void
          androidx.recyclerview.widget.RecyclerView.a(int, int, int[]):void
          androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView$c0, androidx.recyclerview.widget.RecyclerView$l$c, androidx.recyclerview.widget.RecyclerView$l$c):void
          androidx.recyclerview.widget.RecyclerView.a(int, int, android.view.MotionEvent):boolean
          androidx.recyclerview.widget.RecyclerView.a(int, int, boolean):void */
        public void b(int i2, int i3) {
            RecyclerView.this.a(i2, i3, false);
            RecyclerView.this.k0 = true;
        }

        /* access modifiers changed from: package-private */
        public void c(a.b bVar) {
            int i2 = bVar.f1845a;
            if (i2 == 1) {
                RecyclerView recyclerView = RecyclerView.this;
                recyclerView.m.a(recyclerView, bVar.f1846b, bVar.f1848d);
            } else if (i2 == 2) {
                RecyclerView recyclerView2 = RecyclerView.this;
                recyclerView2.m.b(recyclerView2, bVar.f1846b, bVar.f1848d);
            } else if (i2 == 4) {
                RecyclerView recyclerView3 = RecyclerView.this;
                recyclerView3.m.a(recyclerView3, bVar.f1846b, bVar.f1848d, bVar.f1847c);
            } else if (i2 == 8) {
                RecyclerView recyclerView4 = RecyclerView.this;
                recyclerView4.m.a(recyclerView4, bVar.f1846b, bVar.f1848d, 1);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.RecyclerView.a(int, int, boolean):void
         arg types: [int, int, int]
         candidates:
          androidx.recyclerview.widget.RecyclerView.a(long, androidx.recyclerview.widget.RecyclerView$c0, androidx.recyclerview.widget.RecyclerView$c0):void
          androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView$g, boolean, boolean):void
          androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView, int, int):void
          androidx.recyclerview.widget.RecyclerView.a(android.view.View, android.view.View, int):boolean
          androidx.recyclerview.widget.RecyclerView.a(int, int, android.view.animation.Interpolator):void
          androidx.recyclerview.widget.RecyclerView.a(int, int, java.lang.Object):void
          androidx.recyclerview.widget.RecyclerView.a(int, int, int[]):void
          androidx.recyclerview.widget.RecyclerView.a(androidx.recyclerview.widget.RecyclerView$c0, androidx.recyclerview.widget.RecyclerView$l$c, androidx.recyclerview.widget.RecyclerView$l$c):void
          androidx.recyclerview.widget.RecyclerView.a(int, int, android.view.MotionEvent):boolean
          androidx.recyclerview.widget.RecyclerView.a(int, int, boolean):void */
        public void d(int i2, int i3) {
            RecyclerView.this.a(i2, i3, true);
            RecyclerView recyclerView = RecyclerView.this;
            recyclerView.k0 = true;
            recyclerView.h0.f1797d += i3;
        }

        public void a(int i2, int i3, Object obj) {
            RecyclerView.this.a(i2, i3, obj);
            RecyclerView.this.l0 = true;
        }

        public void b(a.b bVar) {
            c(bVar);
        }

        public void a(a.b bVar) {
            c(bVar);
        }

        public void a(int i2, int i3) {
            RecyclerView.this.g(i2, i3);
            RecyclerView.this.k0 = true;
        }

        public void c(int i2, int i3) {
            RecyclerView.this.f(i2, i3);
            RecyclerView.this.k0 = true;
        }
    }

    public RecyclerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.l.a.recyclerViewStyle);
    }

    private boolean k(int i2, int i3) {
        a(this.q0);
        int[] iArr = this.q0;
        return (iArr[0] == i2 && iArr[1] == i3) ? false : true;
    }

    private int l(View view) {
        int id = view.getId();
        while (!view.isFocused() && (view instanceof ViewGroup) && view.hasFocus()) {
            view = ((ViewGroup) view).getFocusedChild();
            if (view.getId() != -1) {
                id = view.getId();
            }
        }
        return id;
    }

    class b0 implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private int f1738a;

        /* renamed from: b  reason: collision with root package name */
        private int f1739b;

        /* renamed from: c  reason: collision with root package name */
        OverScroller f1740c;

        /* renamed from: d  reason: collision with root package name */
        Interpolator f1741d = RecyclerView.G0;

        /* renamed from: e  reason: collision with root package name */
        private boolean f1742e = false;

        /* renamed from: f  reason: collision with root package name */
        private boolean f1743f = false;

        b0() {
            this.f1740c = new OverScroller(RecyclerView.this.getContext(), RecyclerView.G0);
        }

        private void c() {
            RecyclerView.this.removeCallbacks(this);
            b.e.m.u.a(RecyclerView.this, this);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (this.f1742e) {
                this.f1743f = true;
            } else {
                c();
            }
        }

        public void b() {
            RecyclerView.this.removeCallbacks(this);
            this.f1740c.abortAnimation();
        }

        public void run() {
            int i2;
            int i3;
            RecyclerView recyclerView = RecyclerView.this;
            if (recyclerView.m == null) {
                b();
                return;
            }
            this.f1743f = false;
            this.f1742e = true;
            recyclerView.b();
            OverScroller overScroller = this.f1740c;
            if (overScroller.computeScrollOffset()) {
                int currX = overScroller.getCurrX();
                int currY = overScroller.getCurrY();
                int i4 = currX - this.f1738a;
                int i5 = currY - this.f1739b;
                this.f1738a = currX;
                this.f1739b = currY;
                RecyclerView recyclerView2 = RecyclerView.this;
                int[] iArr = recyclerView2.u0;
                iArr[0] = 0;
                iArr[1] = 0;
                if (recyclerView2.a(i4, i5, iArr, (int[]) null, 1)) {
                    int[] iArr2 = RecyclerView.this.u0;
                    i4 -= iArr2[0];
                    i5 -= iArr2[1];
                }
                if (RecyclerView.this.getOverScrollMode() != 2) {
                    RecyclerView.this.b(i4, i5);
                }
                RecyclerView recyclerView3 = RecyclerView.this;
                if (recyclerView3.l != null) {
                    int[] iArr3 = recyclerView3.u0;
                    iArr3[0] = 0;
                    iArr3[1] = 0;
                    recyclerView3.a(i4, i5, iArr3);
                    RecyclerView recyclerView4 = RecyclerView.this;
                    int[] iArr4 = recyclerView4.u0;
                    i2 = iArr4[0];
                    i3 = iArr4[1];
                    i4 -= i2;
                    i5 -= i3;
                    y yVar = recyclerView4.m.f1763g;
                    if (yVar != null && !yVar.b() && yVar.c()) {
                        int a2 = RecyclerView.this.h0.a();
                        if (a2 == 0) {
                            yVar.d();
                        } else if (yVar.a() >= a2) {
                            yVar.a(a2 - 1);
                            yVar.a(i2, i3);
                        } else {
                            yVar.a(i2, i3);
                        }
                    }
                } else {
                    i3 = 0;
                    i2 = 0;
                }
                if (!RecyclerView.this.o.isEmpty()) {
                    RecyclerView.this.invalidate();
                }
                RecyclerView recyclerView5 = RecyclerView.this;
                int[] iArr5 = recyclerView5.u0;
                iArr5[0] = 0;
                iArr5[1] = 0;
                recyclerView5.a(i2, i3, i4, i5, null, 1, iArr5);
                int[] iArr6 = RecyclerView.this.u0;
                int i6 = i4 - iArr6[0];
                int i7 = i5 - iArr6[1];
                if (!(i2 == 0 && i3 == 0)) {
                    RecyclerView.this.d(i2, i3);
                }
                if (!RecyclerView.this.awakenScrollBars()) {
                    RecyclerView.this.invalidate();
                }
                boolean z = overScroller.isFinished() || (((overScroller.getCurrX() == overScroller.getFinalX()) || i6 != 0) && ((overScroller.getCurrY() == overScroller.getFinalY()) || i7 != 0));
                y yVar2 = RecyclerView.this.m.f1763g;
                if ((yVar2 != null && yVar2.b()) || !z) {
                    a();
                    RecyclerView recyclerView6 = RecyclerView.this;
                    e eVar = recyclerView6.f0;
                    if (eVar != null) {
                        eVar.a(recyclerView6, i2, i3);
                    }
                } else {
                    if (RecyclerView.this.getOverScrollMode() != 2) {
                        int currVelocity = (int) overScroller.getCurrVelocity();
                        int i8 = i6 < 0 ? -currVelocity : i6 > 0 ? currVelocity : 0;
                        if (i7 < 0) {
                            currVelocity = -currVelocity;
                        } else if (i7 <= 0) {
                            currVelocity = 0;
                        }
                        RecyclerView.this.a(i8, currVelocity);
                    }
                    if (RecyclerView.C0) {
                        RecyclerView.this.g0.a();
                    }
                }
            }
            y yVar3 = RecyclerView.this.m.f1763g;
            if (yVar3 != null && yVar3.b()) {
                yVar3.a(0, 0);
            }
            this.f1742e = false;
            if (this.f1743f) {
                c();
                return;
            }
            RecyclerView.this.setScrollState(0);
            RecyclerView.this.f(1);
        }

        public void a(int i2, int i3) {
            RecyclerView.this.setScrollState(2);
            this.f1739b = 0;
            this.f1738a = 0;
            Interpolator interpolator = this.f1741d;
            Interpolator interpolator2 = RecyclerView.G0;
            if (interpolator != interpolator2) {
                this.f1741d = interpolator2;
                this.f1740c = new OverScroller(RecyclerView.this.getContext(), RecyclerView.G0);
            }
            this.f1740c.fling(0, 0, i2, i3, Integer.MIN_VALUE, Api.BaseClientBuilder.API_PRIORITY_OTHER, Integer.MIN_VALUE, Api.BaseClientBuilder.API_PRIORITY_OTHER);
            a();
        }

        public void a(int i2, int i3, int i4, Interpolator interpolator) {
            if (i4 == Integer.MIN_VALUE) {
                i4 = a(i2, i3, 0, 0);
            }
            int i5 = i4;
            if (interpolator == null) {
                interpolator = RecyclerView.G0;
            }
            if (this.f1741d != interpolator) {
                this.f1741d = interpolator;
                this.f1740c = new OverScroller(RecyclerView.this.getContext(), interpolator);
            }
            this.f1739b = 0;
            this.f1738a = 0;
            RecyclerView.this.setScrollState(2);
            this.f1740c.startScroll(0, 0, i2, i3, i5);
            if (Build.VERSION.SDK_INT < 23) {
                this.f1740c.computeScrollOffset();
            }
            a();
        }

        private float a(float f2) {
            return (float) Math.sin((double) ((f2 - 0.5f) * 0.47123894f));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        private int a(int i2, int i3, int i4, int i5) {
            int i6;
            int abs = Math.abs(i2);
            int abs2 = Math.abs(i3);
            boolean z = abs > abs2;
            int sqrt = (int) Math.sqrt((double) ((i4 * i4) + (i5 * i5)));
            int sqrt2 = (int) Math.sqrt((double) ((i2 * i2) + (i3 * i3)));
            RecyclerView recyclerView = RecyclerView.this;
            int width = z ? recyclerView.getWidth() : recyclerView.getHeight();
            int i7 = width / 2;
            float f2 = (float) width;
            float f3 = (float) i7;
            float a2 = f3 + (a(Math.min(1.0f, (((float) sqrt2) * 1.0f) / f2)) * f3);
            if (sqrt > 0) {
                i6 = Math.round(Math.abs(a2 / ((float) sqrt)) * 1000.0f) * 4;
            } else {
                if (!z) {
                    abs = abs2;
                }
                i6 = (int) (((((float) abs) / f2) + 1.0f) * 300.0f);
            }
            return Math.min(i6, 2000);
        }
    }

    public static class u {

        /* renamed from: a  reason: collision with root package name */
        SparseArray<a> f1778a = new SparseArray<>();

        /* renamed from: b  reason: collision with root package name */
        private int f1779b = 0;

        static class a {

            /* renamed from: a  reason: collision with root package name */
            final ArrayList<c0> f1780a = new ArrayList<>();

            /* renamed from: b  reason: collision with root package name */
            int f1781b = 5;

            /* renamed from: c  reason: collision with root package name */
            long f1782c = 0;

            /* renamed from: d  reason: collision with root package name */
            long f1783d = 0;

            a() {
            }
        }

        public c0 a(int i2) {
            a aVar = this.f1778a.get(i2);
            if (aVar == null || aVar.f1780a.isEmpty()) {
                return null;
            }
            ArrayList<c0> arrayList = aVar.f1780a;
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                if (!arrayList.get(size).isAttachedToTransitionOverlay()) {
                    return arrayList.remove(size);
                }
            }
            return null;
        }

        public void b() {
            for (int i2 = 0; i2 < this.f1778a.size(); i2++) {
                this.f1778a.valueAt(i2).f1780a.clear();
            }
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.f1779b--;
        }

        /* access modifiers changed from: package-private */
        public void b(int i2, long j2) {
            a b2 = b(i2);
            b2.f1782c = a(b2.f1782c, j2);
        }

        /* access modifiers changed from: package-private */
        public boolean b(int i2, long j2, long j3) {
            long j4 = b(i2).f1782c;
            return j4 == 0 || j2 + j4 < j3;
        }

        private a b(int i2) {
            a aVar = this.f1778a.get(i2);
            if (aVar != null) {
                return aVar;
            }
            a aVar2 = new a();
            this.f1778a.put(i2, aVar2);
            return aVar2;
        }

        public void a(c0 c0Var) {
            int itemViewType = c0Var.getItemViewType();
            ArrayList<c0> arrayList = b(itemViewType).f1780a;
            if (this.f1778a.get(itemViewType).f1781b > arrayList.size()) {
                c0Var.resetInternal();
                arrayList.add(c0Var);
            }
        }

        /* access modifiers changed from: package-private */
        public long a(long j2, long j3) {
            return j2 == 0 ? j3 : ((j2 / 4) * 3) + (j3 / 4);
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, long j2) {
            a b2 = b(i2);
            b2.f1783d = a(b2.f1783d, j2);
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i2, long j2, long j3) {
            long j4 = b(i2).f1783d;
            return j4 == 0 || j2 + j4 < j3;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f1779b++;
        }

        /* access modifiers changed from: package-private */
        public void a(g gVar, g gVar2, boolean z) {
            if (gVar != null) {
                c();
            }
            if (!z && this.f1779b == 0) {
                b();
            }
            if (gVar2 != null) {
                a();
            }
        }
    }

    private class x extends i {
        x() {
        }

        public void a() {
            RecyclerView.this.a((String) null);
            RecyclerView recyclerView = RecyclerView.this;
            recyclerView.h0.f1800g = true;
            recyclerView.b(true);
            if (!RecyclerView.this.f1727d.c()) {
                RecyclerView.this.requestLayout();
            }
        }

        public void b(int i2, int i3) {
            RecyclerView.this.a((String) null);
            if (RecyclerView.this.f1727d.b(i2, i3)) {
                b();
            }
        }

        public void c(int i2, int i3) {
            RecyclerView.this.a((String) null);
            if (RecyclerView.this.f1727d.c(i2, i3)) {
                b();
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            if (RecyclerView.B0) {
                RecyclerView recyclerView = RecyclerView.this;
                if (recyclerView.s && recyclerView.r) {
                    b.e.m.u.a(recyclerView, recyclerView.f1731h);
                    return;
                }
            }
            RecyclerView recyclerView2 = RecyclerView.this;
            recyclerView2.A = true;
            recyclerView2.requestLayout();
        }

        public void a(int i2, int i3, Object obj) {
            RecyclerView.this.a((String) null);
            if (RecyclerView.this.f1727d.a(i2, i3, obj)) {
                b();
            }
        }

        public void a(int i2, int i3, int i4) {
            RecyclerView.this.a((String) null);
            if (RecyclerView.this.f1727d.a(i2, i3, i4)) {
                b();
            }
        }
    }

    public RecyclerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f1724a = new x();
        this.f1725b = new v();
        this.f1729f = new n();
        this.f1731h = new a();
        this.f1732i = new Rect();
        this.f1733j = new Rect();
        this.f1734k = new RectF();
        this.o = new ArrayList<>();
        this.p = new ArrayList<>();
        this.v = 0;
        this.D = false;
        this.E = false;
        this.F = 0;
        this.G = 0;
        this.H = new k();
        this.M = new c();
        this.N = 0;
        this.O = -1;
        this.b0 = Float.MIN_VALUE;
        this.c0 = Float.MIN_VALUE;
        boolean z2 = true;
        this.d0 = true;
        this.e0 = new b0();
        this.g0 = C0 ? new e.b() : null;
        this.h0 = new z();
        this.k0 = false;
        this.l0 = false;
        this.m0 = new m();
        this.n0 = false;
        this.q0 = new int[2];
        this.s0 = new int[2];
        this.t0 = new int[2];
        this.u0 = new int[2];
        this.v0 = new ArrayList();
        this.w0 = new b();
        this.x0 = new d();
        setScrollContainer(true);
        setFocusableInTouchMode(true);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.U = viewConfiguration.getScaledTouchSlop();
        this.b0 = b.e.m.v.b(viewConfiguration, context);
        this.c0 = b.e.m.v.c(viewConfiguration, context);
        this.W = viewConfiguration.getScaledMinimumFlingVelocity();
        this.a0 = viewConfiguration.getScaledMaximumFlingVelocity();
        setWillNotDraw(getOverScrollMode() == 2);
        this.M.a(this.m0);
        k();
        G();
        F();
        if (b.e.m.u.i(this) == 0) {
            b.e.m.u.b(this, 1);
        }
        this.B = (AccessibilityManager) getContext().getSystemService("accessibility");
        setAccessibilityDelegateCompat(new j(this));
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.l.c.RecyclerView, i2, 0);
        if (Build.VERSION.SDK_INT >= 29) {
            saveAttributeDataForStyleable(context, b.l.c.RecyclerView, attributeSet, obtainStyledAttributes, i2, 0);
        }
        String string = obtainStyledAttributes.getString(b.l.c.RecyclerView_layoutManager);
        if (obtainStyledAttributes.getInt(b.l.c.RecyclerView_android_descendantFocusability, -1) == -1) {
            setDescendantFocusability(262144);
        }
        this.f1730g = obtainStyledAttributes.getBoolean(b.l.c.RecyclerView_android_clipToPadding, true);
        this.t = obtainStyledAttributes.getBoolean(b.l.c.RecyclerView_fastScrollEnabled, false);
        if (this.t) {
            a((StateListDrawable) obtainStyledAttributes.getDrawable(b.l.c.RecyclerView_fastScrollVerticalThumbDrawable), obtainStyledAttributes.getDrawable(b.l.c.RecyclerView_fastScrollVerticalTrackDrawable), (StateListDrawable) obtainStyledAttributes.getDrawable(b.l.c.RecyclerView_fastScrollHorizontalThumbDrawable), obtainStyledAttributes.getDrawable(b.l.c.RecyclerView_fastScrollHorizontalTrackDrawable));
        }
        obtainStyledAttributes.recycle();
        a(context, string, attributeSet, i2, 0);
        if (Build.VERSION.SDK_INT >= 21) {
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, y0, i2, 0);
            if (Build.VERSION.SDK_INT >= 29) {
                saveAttributeDataForStyleable(context, y0, attributeSet, obtainStyledAttributes2, i2, 0);
            }
            z2 = obtainStyledAttributes2.getBoolean(0, true);
            obtainStyledAttributes2.recycle();
        }
        setNestedScrollingEnabled(z2);
    }

    static RecyclerView j(View view) {
        if (!(view instanceof ViewGroup)) {
            return null;
        }
        if (view instanceof RecyclerView) {
            return (RecyclerView) view;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            RecyclerView j2 = j(viewGroup.getChildAt(i2));
            if (j2 != null) {
                return j2;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean i(View view) {
        w();
        boolean e2 = this.f1728e.e(view);
        if (e2) {
            c0 k2 = k(view);
            this.f1725b.c(k2);
            this.f1725b.b(k2);
        }
        c(!e2);
        return e2;
    }

    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();

        /* renamed from: c  reason: collision with root package name */
        Parcelable f1735c;

        static class a implements Parcelable.ClassLoaderCreator<SavedState> {
            a() {
            }

            public SavedState[] newArray(int i2) {
                return new SavedState[i2];
            }

            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }
        }

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f1735c = parcel.readParcelable(classLoader == null ? o.class.getClassLoader() : classLoader);
        }

        /* access modifiers changed from: package-private */
        public void a(SavedState savedState) {
            this.f1735c = savedState.f1735c;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            super.writeToParcel(parcel, i2);
            parcel.writeParcelable(this.f1735c, 0);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }
    }

    public static class p extends ViewGroup.MarginLayoutParams {

        /* renamed from: a  reason: collision with root package name */
        c0 f1774a;

        /* renamed from: b  reason: collision with root package name */
        final Rect f1775b = new Rect();

        /* renamed from: c  reason: collision with root package name */
        boolean f1776c = true;

        /* renamed from: d  reason: collision with root package name */
        boolean f1777d = false;

        public p(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public int a() {
            return this.f1774a.getLayoutPosition();
        }

        public boolean b() {
            return this.f1774a.isUpdated();
        }

        public boolean c() {
            return this.f1774a.isRemoved();
        }

        public boolean d() {
            return this.f1774a.isInvalid();
        }

        public p(int i2, int i3) {
            super(i2, i3);
        }

        public p(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public p(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public p(p pVar) {
            super((ViewGroup.LayoutParams) pVar);
        }
    }

    public static class z {

        /* renamed from: a  reason: collision with root package name */
        int f1794a = -1;

        /* renamed from: b  reason: collision with root package name */
        private SparseArray<Object> f1795b;

        /* renamed from: c  reason: collision with root package name */
        int f1796c = 0;

        /* renamed from: d  reason: collision with root package name */
        int f1797d = 0;

        /* renamed from: e  reason: collision with root package name */
        int f1798e = 1;

        /* renamed from: f  reason: collision with root package name */
        int f1799f = 0;

        /* renamed from: g  reason: collision with root package name */
        boolean f1800g = false;

        /* renamed from: h  reason: collision with root package name */
        boolean f1801h = false;

        /* renamed from: i  reason: collision with root package name */
        boolean f1802i = false;

        /* renamed from: j  reason: collision with root package name */
        boolean f1803j = false;

        /* renamed from: k  reason: collision with root package name */
        boolean f1804k = false;
        boolean l = false;
        int m;
        long n;
        int o;
        int p;
        int q;

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            if ((this.f1798e & i2) == 0) {
                throw new IllegalStateException("Layout state should be one of " + Integer.toBinaryString(i2) + " but it is " + Integer.toBinaryString(this.f1798e));
            }
        }

        public int b() {
            return this.f1794a;
        }

        public boolean c() {
            return this.f1794a != -1;
        }

        public boolean d() {
            return this.f1801h;
        }

        public boolean e() {
            return this.l;
        }

        public String toString() {
            return "State{mTargetPosition=" + this.f1794a + ", mData=" + this.f1795b + ", mItemCount=" + this.f1799f + ", mIsMeasuring=" + this.f1803j + ", mPreviousLayoutItemCount=" + this.f1796c + ", mDeletedInvisibleItemCountSincePreviousLayout=" + this.f1797d + ", mStructureChanged=" + this.f1800g + ", mInPreLayout=" + this.f1801h + ", mRunSimpleAnimations=" + this.f1804k + ", mRunPredictiveAnimations=" + this.l + '}';
        }

        /* access modifiers changed from: package-private */
        public void a(g gVar) {
            this.f1798e = 1;
            this.f1799f = gVar.getItemCount();
            this.f1801h = false;
            this.f1802i = false;
            this.f1803j = false;
        }

        public int a() {
            return this.f1801h ? this.f1796c - this.f1797d : this.f1799f;
        }
    }

    static c0 k(View view) {
        if (view == null) {
            return null;
        }
        return ((p) view.getLayoutParams()).f1774a;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        o oVar = this.m;
        if (oVar != null) {
            return oVar.a(layoutParams);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager" + i());
    }

    private void a(Context context, String str, AttributeSet attributeSet, int i2, int i3) {
        ClassLoader classLoader;
        Constructor<? extends U> constructor;
        if (str != null) {
            String trim = str.trim();
            if (!trim.isEmpty()) {
                String a2 = a(context, trim);
                try {
                    if (isInEditMode()) {
                        classLoader = getClass().getClassLoader();
                    } else {
                        classLoader = context.getClassLoader();
                    }
                    Class<? extends U> asSubclass = Class.forName(a2, false, classLoader).asSubclass(o.class);
                    Object[] objArr = null;
                    try {
                        constructor = asSubclass.getConstructor(F0);
                        objArr = new Object[]{context, attributeSet, Integer.valueOf(i2), Integer.valueOf(i3)};
                    } catch (NoSuchMethodException e2) {
                        constructor = asSubclass.getConstructor(new Class[0]);
                    }
                    constructor.setAccessible(true);
                    setLayoutManager((o) constructor.newInstance(objArr));
                } catch (NoSuchMethodException e3) {
                    e3.initCause(e2);
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Error creating LayoutManager " + a2, e3);
                } catch (ClassNotFoundException e4) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Unable to find LayoutManager " + a2, e4);
                } catch (InvocationTargetException e5) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + a2, e5);
                } catch (InstantiationException e6) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + a2, e6);
                } catch (IllegalAccessException e7) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Cannot access non-public constructor " + a2, e7);
                } catch (ClassCastException e8) {
                    throw new IllegalStateException(attributeSet.getPositionDescription() + ": Class is not a LayoutManager " + a2, e8);
                }
            }
        }
    }

    public void b(t tVar) {
        List<t> list = this.j0;
        if (list != null) {
            list.remove(tVar);
        }
    }

    public c0 d(View view) {
        View c2 = c(view);
        if (c2 == null) {
            return null;
        }
        return e(c2);
    }

    /* access modifiers changed from: package-private */
    public void f(int i2, int i3) {
        int b2 = this.f1728e.b();
        for (int i4 = 0; i4 < b2; i4++) {
            c0 k2 = k(this.f1728e.d(i4));
            if (k2 != null && !k2.shouldIgnore() && k2.mPosition >= i2) {
                k2.offsetPosition(i3, false);
                this.h0.f1800g = true;
            }
        }
        this.f1725b.a(i2, i3);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void g(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int i7;
        int b2 = this.f1728e.b();
        if (i2 < i3) {
            i6 = i2;
            i5 = i3;
            i4 = -1;
        } else {
            i5 = i2;
            i6 = i3;
            i4 = 1;
        }
        for (int i8 = 0; i8 < b2; i8++) {
            c0 k2 = k(this.f1728e.d(i8));
            if (k2 != null && (i7 = k2.mPosition) >= i6 && i7 <= i5) {
                if (i7 == i2) {
                    k2.offsetPosition(i3 - i2, false);
                } else {
                    k2.offsetPosition(i4, false);
                }
                this.h0.f1800g = true;
            }
        }
        this.f1725b.b(i2, i3);
        requestLayout();
    }

    public void i(int i2, int i3) {
        a(i2, i3, (Interpolator) null);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (!this.u || this.D) {
            b.e.i.c.a("RV FullInvalidate");
            c();
            b.e.i.c.a();
        } else if (this.f1727d.c()) {
            if (this.f1727d.c(4) && !this.f1727d.c(11)) {
                b.e.i.c.a("RV PartialInvalidate");
                w();
                q();
                this.f1727d.e();
                if (!this.w) {
                    if (E()) {
                        c();
                    } else {
                        this.f1727d.a();
                    }
                }
                c(true);
                r();
                b.e.i.c.a();
            } else if (this.f1727d.c()) {
                b.e.i.c.a("RV FullInvalidate");
                c();
                b.e.i.c.a();
            }
        }
    }

    public void d(int i2) {
        int a2 = this.f1728e.a();
        for (int i3 = 0; i3 < a2; i3++) {
            this.f1728e.c(i3).offsetTopAndBottom(i2);
        }
    }

    public boolean j(int i2, int i3) {
        return getScrollingChildHelper().a(i2, i3);
    }

    private void c(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.O) {
            int i2 = actionIndex == 0 ? 1 : 0;
            this.O = motionEvent.getPointerId(i2);
            int x2 = (int) (motionEvent.getX(i2) + 0.5f);
            this.S = x2;
            this.Q = x2;
            int y2 = (int) (motionEvent.getY(i2) + 0.5f);
            this.T = y2;
            this.R = y2;
        }
    }

    /* access modifiers changed from: package-private */
    public void d(int i2, int i3) {
        this.G++;
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        onScrollChanged(scrollX, scrollY, scrollX - i2, scrollY - i3);
        h(i2, i3);
        t tVar = this.i0;
        if (tVar != null) {
            tVar.a(this, i2, i3);
        }
        List<t> list = this.j0;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.j0.get(size).a(this, i2, i3);
            }
        }
        this.G--;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.L == null) {
            this.L = this.H.a(this, 3);
            if (this.f1730g) {
                this.L.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.L.setSize(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Rect f(View view) {
        p pVar = (p) view.getLayoutParams();
        if (!pVar.f1776c) {
            return pVar.f1775b;
        }
        if (this.h0.d() && (pVar.b() || pVar.d())) {
            return pVar.f1775b;
        }
        Rect rect = pVar.f1775b;
        rect.set(0, 0, 0, 0);
        int size = this.o.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.f1732i.set(0, 0, 0, 0);
            this.o.get(i2).a(this.f1732i, view, this, this.h0);
            int i3 = rect.left;
            Rect rect2 = this.f1732i;
            rect.left = i3 + rect2.left;
            rect.top += rect2.top;
            rect.right += rect2.right;
            rect.bottom += rect2.bottom;
        }
        pVar.f1776c = false;
        return rect;
    }

    /* access modifiers changed from: package-private */
    public void c(int i2, int i3) {
        setMeasuredDimension(o.a(i2, getPaddingLeft() + getPaddingRight(), b.e.m.u.m(this)), o.a(i3, getPaddingTop() + getPaddingBottom(), b.e.m.u.l(this)));
    }

    public c0 e(View view) {
        ViewParent parent = view.getParent();
        if (parent == null || parent == this) {
            return k(view);
        }
        throw new IllegalArgumentException("View " + view + " is not a direct child of " + this);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.l == null) {
            Log.e("RecyclerView", "No adapter attached; skipping layout");
        } else if (this.m == null) {
            Log.e("RecyclerView", "No layout manager attached; skipping layout");
        } else {
            z zVar = this.h0;
            zVar.f1803j = false;
            if (zVar.f1798e == 1) {
                A();
                this.m.e(this);
                B();
            } else if (!this.f1727d.d() && this.m.q() == getWidth() && this.m.h() == getHeight()) {
                this.m.e(this);
            } else {
                this.m.e(this);
                B();
            }
            C();
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        int i2;
        for (int size = this.v0.size() - 1; size >= 0; size--) {
            c0 c0Var = this.v0.get(size);
            if (c0Var.itemView.getParent() == this && !c0Var.shouldIgnore() && (i2 = c0Var.mPendingAccessibilityState) != -1) {
                b.e.m.u.b(c0Var.itemView, i2);
                c0Var.mPendingAccessibilityState = -1;
            }
        }
        this.v0.clear();
    }

    static void e(c0 c0Var) {
        WeakReference<RecyclerView> weakReference = c0Var.mNestedRecyclerView;
        if (weakReference != null) {
            View view = weakReference.get();
            while (view != null) {
                if (view != c0Var.itemView) {
                    ViewParent parent = view.getParent();
                    view = parent instanceof View ? (View) parent : null;
                } else {
                    return;
                }
            }
            c0Var.mNestedRecyclerView = null;
        }
    }

    private String a(Context context, String str) {
        if (str.charAt(0) == '.') {
            return context.getPackageName() + str;
        } else if (str.contains(".")) {
            return str;
        } else {
            return RecyclerView.class.getPackage().getName() + '.' + str;
        }
    }

    private void a(g gVar, boolean z2, boolean z3) {
        g gVar2 = this.l;
        if (gVar2 != null) {
            gVar2.unregisterAdapterDataObserver(this.f1724a);
            this.l.onDetachedFromRecyclerView(this);
        }
        if (!z2 || z3) {
            t();
        }
        this.f1727d.f();
        g gVar3 = this.l;
        this.l = gVar;
        if (gVar != null) {
            gVar.registerAdapterDataObserver(this.f1724a);
            gVar.onAttachedToRecyclerView(this);
        }
        o oVar = this.m;
        if (oVar != null) {
            oVar.a(gVar3, this.l);
        }
        this.f1725b.a(gVar3, this.l, z2);
        this.h0.f1800g = true;
    }

    public void f(int i2) {
        getScrollingChildHelper().c(i2);
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, int i3) {
        boolean z2;
        EdgeEffect edgeEffect = this.I;
        if (edgeEffect == null || edgeEffect.isFinished() || i2 <= 0) {
            z2 = false;
        } else {
            this.I.onRelease();
            z2 = this.I.isFinished();
        }
        EdgeEffect edgeEffect2 = this.K;
        if (edgeEffect2 != null && !edgeEffect2.isFinished() && i2 < 0) {
            this.K.onRelease();
            z2 |= this.K.isFinished();
        }
        EdgeEffect edgeEffect3 = this.J;
        if (edgeEffect3 != null && !edgeEffect3.isFinished() && i3 > 0) {
            this.J.onRelease();
            z2 |= this.J.isFinished();
        }
        EdgeEffect edgeEffect4 = this.L;
        if (edgeEffect4 != null && !edgeEffect4.isFinished() && i3 < 0) {
            this.L.onRelease();
            z2 |= this.L.isFinished();
        }
        if (z2) {
            b.e.m.u.v(this);
        }
    }

    /* access modifiers changed from: package-private */
    public long c(c0 c0Var) {
        return this.l.hasStableIds() ? c0Var.getItemId() : (long) c0Var.mPosition;
    }

    public View c(View view) {
        ViewParent parent = view.getParent();
        while (parent != null && parent != this && (parent instanceof View)) {
            view = (View) parent;
            parent = view.getParent();
        }
        if (parent == this) {
            return view;
        }
        return null;
    }

    public void a(n nVar, int i2) {
        o oVar = this.m;
        if (oVar != null) {
            oVar.a("Cannot add item decoration during a scroll  or layout");
        }
        if (this.o.isEmpty()) {
            setWillNotDraw(false);
        }
        if (i2 < 0) {
            this.o.add(nVar);
        } else {
            this.o.add(i2, nVar);
        }
        o();
        requestLayout();
    }

    public void c(int i2) {
        int a2 = this.f1728e.a();
        for (int i3 = 0; i3 < a2; i3++) {
            this.f1728e.c(i3).offsetLeftAndRight(i2);
        }
    }

    public void b(s sVar) {
        this.p.remove(sVar);
        if (this.q == sVar) {
            this.q = null;
        }
    }

    private boolean b(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int size = this.p.size();
        int i2 = 0;
        while (i2 < size) {
            s sVar = this.p.get(i2);
            if (!sVar.b(this, motionEvent) || action == 3) {
                i2++;
            } else {
                this.q = sVar;
                return true;
            }
        }
        return false;
    }

    public void a(n nVar) {
        a(nVar, -1);
    }

    public void a(t tVar) {
        if (this.j0 == null) {
            this.j0 = new ArrayList();
        }
        this.j0.add(tVar);
    }

    /* access modifiers changed from: package-private */
    public void b(c0 c0Var, l.c cVar, l.c cVar2) {
        d(c0Var);
        c0Var.setIsRecyclable(false);
        if (this.M.b(c0Var, cVar, cVar2)) {
            s();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int[] iArr) {
        w();
        q();
        b.e.i.c.a("RV Scroll");
        a(this.h0);
        int a2 = i2 != 0 ? this.m.a(i2, this.f1725b, this.h0) : 0;
        int b2 = i3 != 0 ? this.m.b(i3, this.f1725b, this.h0) : 0;
        b.e.i.c.a();
        u();
        r();
        c(false);
        if (iArr != null) {
            iArr[0] = a2;
            iArr[1] = b2;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        this.E = z2 | this.E;
        this.D = true;
        p();
    }

    public c0 b(int i2) {
        c0 c0Var = null;
        if (this.D) {
            return null;
        }
        int b2 = this.f1728e.b();
        for (int i3 = 0; i3 < b2; i3++) {
            c0 k2 = k(this.f1728e.d(i3));
            if (k2 != null && !k2.isRemoved() && b(k2) == i2) {
                if (!this.f1728e.c(k2.itemView)) {
                    return k2;
                }
                c0Var = k2;
            }
        }
        return c0Var;
    }

    /* access modifiers changed from: package-private */
    public void b(View view) {
        c0 k2 = k(view);
        h(view);
        g gVar = this.l;
        if (!(gVar == null || k2 == null)) {
            gVar.onViewDetachedFromWindow(k2);
        }
        List<q> list = this.C;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.C.get(size).a(view);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, int i3, MotionEvent motionEvent) {
        int i4;
        int i5;
        int i6;
        int i7;
        int i8 = i2;
        int i9 = i3;
        MotionEvent motionEvent2 = motionEvent;
        b();
        if (this.l != null) {
            int[] iArr = this.u0;
            iArr[0] = 0;
            iArr[1] = 0;
            a(i8, i9, iArr);
            int[] iArr2 = this.u0;
            int i10 = iArr2[0];
            int i11 = iArr2[1];
            i6 = i11;
            i7 = i10;
            i5 = i8 - i10;
            i4 = i9 - i11;
        } else {
            i7 = 0;
            i6 = 0;
            i5 = 0;
            i4 = 0;
        }
        if (!this.o.isEmpty()) {
            invalidate();
        }
        int[] iArr3 = this.u0;
        iArr3[0] = 0;
        iArr3[1] = 0;
        int i12 = i7;
        a(i7, i6, i5, i4, this.s0, 0, iArr3);
        int[] iArr4 = this.u0;
        int i13 = i5 - iArr4[0];
        int i14 = i4 - iArr4[1];
        boolean z2 = (iArr4[0] == 0 && iArr4[1] == 0) ? false : true;
        int i15 = this.S;
        int[] iArr5 = this.s0;
        this.S = i15 - iArr5[0];
        this.T -= iArr5[1];
        int[] iArr6 = this.t0;
        iArr6[0] = iArr6[0] + iArr5[0];
        iArr6[1] = iArr6[1] + iArr5[1];
        if (getOverScrollMode() != 2) {
            if (motionEvent2 != null && !b.e.m.h.a(motionEvent2, 8194)) {
                a(motionEvent.getX(), (float) i13, motionEvent.getY(), (float) i14);
            }
            b(i2, i3);
        }
        int i16 = i12;
        if (!(i16 == 0 && i6 == 0)) {
            d(i16, i6);
        }
        if (!awakenScrollBars()) {
            invalidate();
        }
        if (!z2 && i16 == 0 && i6 == 0) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public int b(c0 c0Var) {
        if (c0Var.hasAnyOfTheFlags(524) || !c0Var.isBound()) {
            return -1;
        }
        return this.f1727d.a(c0Var.mPosition);
    }

    public void a(int i2, int i3, Interpolator interpolator) {
        a(i2, i3, interpolator, Integer.MIN_VALUE);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.a(int, int, android.view.animation.Interpolator, int, boolean):void
     arg types: [int, int, android.view.animation.Interpolator, int, int]
     candidates:
      androidx.recyclerview.widget.RecyclerView.a(android.content.Context, java.lang.String, android.util.AttributeSet, int, int):void
      androidx.recyclerview.widget.RecyclerView.a(int, int, int[], int[], int):boolean
      androidx.recyclerview.widget.RecyclerView.a(int, int, android.view.animation.Interpolator, int, boolean):void */
    public void a(int i2, int i3, Interpolator interpolator, int i4) {
        a(i2, i3, interpolator, i4, false);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, Interpolator interpolator, int i4, boolean z2) {
        o oVar = this.m;
        if (oVar == null) {
            Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.x) {
            int i5 = 0;
            if (!oVar.a()) {
                i2 = 0;
            }
            if (!this.m.b()) {
                i3 = 0;
            }
            if (i2 != 0 || i3 != 0) {
                if (i4 == Integer.MIN_VALUE || i4 > 0) {
                    if (z2) {
                        if (i2 != 0) {
                            i5 = 1;
                        }
                        if (i3 != 0) {
                            i5 |= 2;
                        }
                        j(i5, 1);
                    }
                    this.e0.a(i2, i3, i4, interpolator);
                    return;
                }
                scrollBy(i2, i3);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0056  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(float r7, float r8, float r9, float r10) {
        /*
            r6 = this;
            r0 = 1065353216(0x3f800000, float:1.0)
            r1 = 1
            r2 = 0
            int r3 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r3 >= 0) goto L_0x0021
            r6.f()
            android.widget.EdgeEffect r3 = r6.I
            float r4 = -r8
            int r5 = r6.getWidth()
            float r5 = (float) r5
            float r4 = r4 / r5
            int r5 = r6.getHeight()
            float r5 = (float) r5
            float r9 = r9 / r5
            float r9 = r0 - r9
            androidx.core.widget.d.a(r3, r4, r9)
        L_0x001f:
            r9 = 1
            goto L_0x003c
        L_0x0021:
            int r3 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r3 <= 0) goto L_0x003b
            r6.g()
            android.widget.EdgeEffect r3 = r6.K
            int r4 = r6.getWidth()
            float r4 = (float) r4
            float r4 = r8 / r4
            int r5 = r6.getHeight()
            float r5 = (float) r5
            float r9 = r9 / r5
            androidx.core.widget.d.a(r3, r4, r9)
            goto L_0x001f
        L_0x003b:
            r9 = 0
        L_0x003c:
            int r3 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r3 >= 0) goto L_0x0056
            r6.h()
            android.widget.EdgeEffect r9 = r6.J
            float r0 = -r10
            int r3 = r6.getHeight()
            float r3 = (float) r3
            float r0 = r0 / r3
            int r3 = r6.getWidth()
            float r3 = (float) r3
            float r7 = r7 / r3
            androidx.core.widget.d.a(r9, r0, r7)
            goto L_0x0072
        L_0x0056:
            int r3 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r3 <= 0) goto L_0x0071
            r6.e()
            android.widget.EdgeEffect r9 = r6.L
            int r3 = r6.getHeight()
            float r3 = (float) r3
            float r3 = r10 / r3
            int r4 = r6.getWidth()
            float r4 = (float) r4
            float r7 = r7 / r4
            float r0 = r0 - r7
            androidx.core.widget.d.a(r9, r3, r0)
            goto L_0x0072
        L_0x0071:
            r1 = r9
        L_0x0072:
            if (r1 != 0) goto L_0x007c
            int r7 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r7 != 0) goto L_0x007c
            int r7 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r7 == 0) goto L_0x007f
        L_0x007c:
            b.e.m.u.v(r6)
        L_0x007f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.a(float, float, float, float):void");
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        if (i2 < 0) {
            f();
            if (this.I.isFinished()) {
                this.I.onAbsorb(-i2);
            }
        } else if (i2 > 0) {
            g();
            if (this.K.isFinished()) {
                this.K.onAbsorb(i2);
            }
        }
        if (i3 < 0) {
            h();
            if (this.J.isFinished()) {
                this.J.onAbsorb(-i3);
            }
        } else if (i3 > 0) {
            e();
            if (this.L.isFinished()) {
                this.L.onAbsorb(i3);
            }
        }
        if (i2 != 0 || i3 != 0) {
            b.e.m.u.v(this);
        }
    }

    private boolean a(View view, View view2, int i2) {
        int i3;
        if (view2 == null || view2 == this || c(view2) == null) {
            return false;
        }
        if (view == null || c(view) == null) {
            return true;
        }
        this.f1732i.set(0, 0, view.getWidth(), view.getHeight());
        this.f1733j.set(0, 0, view2.getWidth(), view2.getHeight());
        offsetDescendantRectToMyCoords(view, this.f1732i);
        offsetDescendantRectToMyCoords(view2, this.f1733j);
        char c2 = 65535;
        int i4 = this.m.j() == 1 ? -1 : 1;
        Rect rect = this.f1732i;
        int i5 = rect.left;
        int i6 = this.f1733j.left;
        if ((i5 < i6 || rect.right <= i6) && this.f1732i.right < this.f1733j.right) {
            i3 = 1;
        } else {
            Rect rect2 = this.f1732i;
            int i7 = rect2.right;
            int i8 = this.f1733j.right;
            i3 = ((i7 > i8 || rect2.left >= i8) && this.f1732i.left > this.f1733j.left) ? -1 : 0;
        }
        Rect rect3 = this.f1732i;
        int i9 = rect3.top;
        int i10 = this.f1733j.top;
        if ((i9 < i10 || rect3.bottom <= i10) && this.f1732i.bottom < this.f1733j.bottom) {
            c2 = 1;
        } else {
            Rect rect4 = this.f1732i;
            int i11 = rect4.bottom;
            int i12 = this.f1733j.bottom;
            if ((i11 <= i12 && rect4.top < i12) || this.f1732i.top <= this.f1733j.top) {
                c2 = 0;
            }
        }
        if (i2 != 1) {
            if (i2 != 2) {
                if (i2 != 17) {
                    if (i2 != 33) {
                        if (i2 != 66) {
                            if (i2 != 130) {
                                throw new IllegalArgumentException("Invalid direction: " + i2 + i());
                            } else if (c2 > 0) {
                                return true;
                            } else {
                                return false;
                            }
                        } else if (i3 > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    } else if (c2 < 0) {
                        return true;
                    } else {
                        return false;
                    }
                } else if (i3 < 0) {
                    return true;
                } else {
                    return false;
                }
            } else if (c2 > 0 || (c2 == 0 && i3 * i4 >= 0)) {
                return true;
            } else {
                return false;
            }
        } else if (c2 < 0 || (c2 == 0 && i3 * i4 <= 0)) {
            return true;
        } else {
            return false;
        }
    }

    private void a(View view, View view2) {
        View view3 = view2 != null ? view2 : view;
        this.f1732i.set(0, 0, view3.getWidth(), view3.getHeight());
        ViewGroup.LayoutParams layoutParams = view3.getLayoutParams();
        if (layoutParams instanceof p) {
            p pVar = (p) layoutParams;
            if (!pVar.f1776c) {
                Rect rect = pVar.f1775b;
                Rect rect2 = this.f1732i;
                rect2.left -= rect.left;
                rect2.right += rect.right;
                rect2.top -= rect.top;
                rect2.bottom += rect.bottom;
            }
        }
        if (view2 != null) {
            offsetDescendantRectToMyCoords(view2, this.f1732i);
            offsetRectIntoDescendantCoords(view, this.f1732i);
        }
        this.m.a(this, view, this.f1732i, !this.u, view2 == null);
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (n()) {
            if (str == null) {
                throw new IllegalStateException("Cannot call this method while RecyclerView is computing a layout or scrolling" + i());
            }
            throw new IllegalStateException(str);
        } else if (this.G > 0) {
            Log.w("RecyclerView", "Cannot call this method in a scroll callback. Scroll callbacks mightbe run during a measure & layout pass where you cannot change theRecyclerView data. Any method call that might change the structureof the RecyclerView or the adapter contents should be postponed tothe next frame.", new IllegalStateException("" + i()));
        }
    }

    public void a(s sVar) {
        this.p.add(sVar);
    }

    private boolean a(MotionEvent motionEvent) {
        s sVar = this.q;
        if (sVar != null) {
            sVar.a(this, motionEvent);
            int action = motionEvent.getAction();
            if (action == 3 || action == 1) {
                this.q = null;
            }
            return true;
        } else if (motionEvent.getAction() == 0) {
            return false;
        } else {
            return b(motionEvent);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        this.F--;
        if (this.F < 1) {
            this.F = 0;
            if (z2) {
                z();
                d();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(AccessibilityEvent accessibilityEvent) {
        if (!n()) {
            return false;
        }
        int a2 = accessibilityEvent != null ? b.e.m.d0.b.a(accessibilityEvent) : 0;
        if (a2 == 0) {
            a2 = 0;
        }
        this.z = a2 | this.z;
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void a(z zVar) {
        if (getScrollState() == 2) {
            OverScroller overScroller = this.e0.f1740c;
            zVar.p = overScroller.getFinalX() - overScroller.getCurrX();
            zVar.q = overScroller.getFinalY() - overScroller.getCurrY();
            return;
        }
        zVar.p = 0;
        zVar.q = 0;
    }

    private void a(long j2, c0 c0Var, c0 c0Var2) {
        int a2 = this.f1728e.a();
        for (int i2 = 0; i2 < a2; i2++) {
            c0 k2 = k(this.f1728e.c(i2));
            if (k2 != c0Var && c(k2) == j2) {
                g gVar = this.l;
                if (gVar == null || !gVar.hasStableIds()) {
                    throw new IllegalStateException("Two different ViewHolders have the same change ID. This might happen due to inconsistent Adapter update events or if the LayoutManager lays out the same View multiple times.\n ViewHolder 1:" + k2 + " \n View Holder 2:" + c0Var + i());
                }
                throw new IllegalStateException("Two different ViewHolders have the same stable ID. Stable IDs in your adapter MUST BE unique and SHOULD NOT change.\n ViewHolder 1:" + k2 + " \n View Holder 2:" + c0Var + i());
            }
        }
        Log.e("RecyclerView", "Problem while matching changed view holders with the newones. The pre-layout information for the change holder " + c0Var2 + " cannot be found but it is necessary for " + c0Var + i());
    }

    /* access modifiers changed from: package-private */
    public void a(c0 c0Var, l.c cVar) {
        c0Var.setFlags(0, Utility.DEFAULT_STREAM_BUFFER_SIZE);
        if (this.h0.f1802i && c0Var.isUpdated() && !c0Var.isRemoved() && !c0Var.shouldIgnore()) {
            this.f1729f.a(c(c0Var), c0Var);
        }
        this.f1729f.c(c0Var, cVar);
    }

    private void a(int[] iArr) {
        int a2 = this.f1728e.a();
        if (a2 == 0) {
            iArr[0] = -1;
            iArr[1] = -1;
            return;
        }
        int i2 = Api.BaseClientBuilder.API_PRIORITY_OTHER;
        int i3 = Integer.MIN_VALUE;
        for (int i4 = 0; i4 < a2; i4++) {
            c0 k2 = k(this.f1728e.c(i4));
            if (!k2.shouldIgnore()) {
                int layoutPosition = k2.getLayoutPosition();
                if (layoutPosition < i2) {
                    i2 = layoutPosition;
                }
                if (layoutPosition > i3) {
                    i3 = layoutPosition;
                }
            }
        }
        iArr[0] = i2;
        iArr[1] = i3;
    }

    /* access modifiers changed from: package-private */
    public void a(c0 c0Var, l.c cVar, l.c cVar2) {
        c0Var.setIsRecyclable(false);
        if (this.M.a(c0Var, cVar, cVar2)) {
            s();
        }
    }

    private void a(c0 c0Var, c0 c0Var2, l.c cVar, l.c cVar2, boolean z2, boolean z3) {
        c0Var.setIsRecyclable(false);
        if (z2) {
            d(c0Var);
        }
        if (c0Var != c0Var2) {
            if (z3) {
                d(c0Var2);
            }
            c0Var.mShadowedHolder = c0Var2;
            d(c0Var);
            this.f1725b.c(c0Var);
            c0Var2.setIsRecyclable(false);
            c0Var2.mShadowingHolder = c0Var;
        }
        if (this.M.a(c0Var, c0Var2, cVar, cVar2)) {
            s();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        int b2 = this.f1728e.b();
        for (int i2 = 0; i2 < b2; i2++) {
            c0 k2 = k(this.f1728e.d(i2));
            if (!k2.shouldIgnore()) {
                k2.clearOldPosition();
            }
        }
        this.f1725b.b();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, boolean z2) {
        int i4 = i2 + i3;
        int b2 = this.f1728e.b();
        for (int i5 = 0; i5 < b2; i5++) {
            c0 k2 = k(this.f1728e.d(i5));
            if (k2 != null && !k2.shouldIgnore()) {
                int i6 = k2.mPosition;
                if (i6 >= i4) {
                    k2.offsetPosition(-i3, z2);
                    this.h0.f1800g = true;
                } else if (i6 >= i2) {
                    k2.flagRemovedAndOffsetPosition(i2 - 1, -i3, z2);
                    this.h0.f1800g = true;
                }
            }
        }
        this.f1725b.a(i2, i3, z2);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, Object obj) {
        int i4;
        int b2 = this.f1728e.b();
        int i5 = i2 + i3;
        for (int i6 = 0; i6 < b2; i6++) {
            View d2 = this.f1728e.d(i6);
            c0 k2 = k(d2);
            if (k2 != null && !k2.shouldIgnore() && (i4 = k2.mPosition) >= i2 && i4 < i5) {
                k2.addFlags(2);
                k2.addChangePayload(obj);
                ((p) d2.getLayoutParams()).f1776c = true;
            }
        }
        this.f1725b.c(i2, i3);
    }

    /* access modifiers changed from: package-private */
    public boolean a(c0 c0Var) {
        l lVar = this.M;
        return lVar == null || lVar.a(c0Var, c0Var.getUnmodifiedPayloads());
    }

    /* access modifiers changed from: package-private */
    public c0 a(int i2, boolean z2) {
        int b2 = this.f1728e.b();
        c0 c0Var = null;
        for (int i3 = 0; i3 < b2; i3++) {
            c0 k2 = k(this.f1728e.d(i3));
            if (k2 != null && !k2.isRemoved()) {
                if (z2) {
                    if (k2.mPosition != i2) {
                        continue;
                    }
                } else if (k2.getLayoutPosition() != i2) {
                    continue;
                }
                if (!this.f1728e.c(k2.itemView)) {
                    return k2;
                }
                c0Var = k2;
            }
        }
        return c0Var;
    }

    public c0 a(long j2) {
        g gVar = this.l;
        c0 c0Var = null;
        if (gVar != null && gVar.hasStableIds()) {
            int b2 = this.f1728e.b();
            for (int i2 = 0; i2 < b2; i2++) {
                c0 k2 = k(this.f1728e.d(i2));
                if (k2 != null && !k2.isRemoved() && k2.getItemId() == j2) {
                    if (!this.f1728e.c(k2.itemView)) {
                        return k2;
                    }
                    c0Var = k2;
                }
            }
        }
        return c0Var;
    }

    static void a(View view, Rect rect) {
        p pVar = (p) view.getLayoutParams();
        Rect rect2 = pVar.f1775b;
        rect.set((view.getLeft() - rect2.left) - pVar.leftMargin, (view.getTop() - rect2.top) - pVar.topMargin, view.getRight() + rect2.right + pVar.rightMargin, view.getBottom() + rect2.bottom + pVar.bottomMargin);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        o oVar = this.m;
        if (oVar != null) {
            oVar.f(i2);
        }
        e(i2);
        t tVar = this.i0;
        if (tVar != null) {
            tVar.a(this, i2);
        }
        List<t> list = this.j0;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.j0.get(size).a(this, i2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        c0 k2 = k(view);
        g(view);
        g gVar = this.l;
        if (!(gVar == null || k2 == null)) {
            gVar.onViewAttachedToWindow(k2);
        }
        List<q> list = this.C;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.C.get(size).b(view);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(c0 c0Var, int i2) {
        if (n()) {
            c0Var.mPendingAccessibilityState = i2;
            this.v0.add(c0Var);
            return false;
        }
        b.e.m.u.b(c0Var.itemView, i2);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(StateListDrawable stateListDrawable, Drawable drawable, StateListDrawable stateListDrawable2, Drawable drawable2) {
        if (stateListDrawable == null || drawable == null || stateListDrawable2 == null || drawable2 == null) {
            throw new IllegalArgumentException("Trying to set fast scroller without both required drawables." + i());
        }
        Resources resources = getContext().getResources();
        new d(this, stateListDrawable, drawable, stateListDrawable2, drawable2, resources.getDimensionPixelSize(b.l.b.fastscroll_default_thickness), resources.getDimensionPixelSize(b.l.b.fastscroll_minimum_range), resources.getDimensionPixelOffset(b.l.b.fastscroll_margin));
    }

    public final void a(int i2, int i3, int i4, int i5, int[] iArr, int i6, int[] iArr2) {
        getScrollingChildHelper().a(i2, i3, i4, i5, iArr, i6, iArr2);
    }

    public boolean a(int i2, int i3, int[] iArr, int[] iArr2, int i4) {
        return getScrollingChildHelper().a(i2, i3, iArr, iArr2, i4);
    }
}
