package androidx.recyclerview.widget;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ChildHelper */
class b {

    /* renamed from: a  reason: collision with root package name */
    final C0021b f1849a;

    /* renamed from: b  reason: collision with root package name */
    final a f1850b = new a();

    /* renamed from: c  reason: collision with root package name */
    final List<View> f1851c = new ArrayList();

    /* renamed from: androidx.recyclerview.widget.b$b  reason: collision with other inner class name */
    /* compiled from: ChildHelper */
    interface C0021b {
        int a();

        View a(int i2);

        void a(View view);

        void a(View view, int i2, ViewGroup.LayoutParams layoutParams);

        void addView(View view, int i2);

        int b(View view);

        void b();

        void b(int i2);

        RecyclerView.c0 c(View view);

        void c(int i2);

        void d(View view);
    }

    b(C0021b bVar) {
        this.f1849a = bVar;
    }

    private int f(int i2) {
        if (i2 < 0) {
            return -1;
        }
        int a2 = this.f1849a.a();
        int i3 = i2;
        while (i3 < a2) {
            int b2 = i2 - (i3 - this.f1850b.b(i3));
            if (b2 == 0) {
                while (this.f1850b.c(i3)) {
                    i3++;
                }
                return i3;
            }
            i3 += b2;
        }
        return -1;
    }

    private void g(View view) {
        this.f1851c.add(view);
        this.f1849a.a(view);
    }

    private boolean h(View view) {
        if (!this.f1851c.remove(view)) {
            return false;
        }
        this.f1849a.d(view);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(View view, boolean z) {
        a(view, -1, z);
    }

    /* access modifiers changed from: package-private */
    public View b(int i2) {
        int size = this.f1851c.size();
        for (int i3 = 0; i3 < size; i3++) {
            View view = this.f1851c.get(i3);
            RecyclerView.c0 c2 = this.f1849a.c(view);
            if (c2.getLayoutPosition() == i2 && !c2.isInvalid() && !c2.isRemoved()) {
                return view;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public View c(int i2) {
        return this.f1849a.a(f(i2));
    }

    /* access modifiers changed from: package-private */
    public void d(View view) {
        int b2 = this.f1849a.b(view);
        if (b2 >= 0) {
            if (this.f1850b.d(b2)) {
                h(view);
            }
            this.f1849a.c(b2);
        }
    }

    /* access modifiers changed from: package-private */
    public void e(int i2) {
        int f2 = f(i2);
        View a2 = this.f1849a.a(f2);
        if (a2 != null) {
            if (this.f1850b.d(f2)) {
                h(a2);
            }
            this.f1849a.c(f2);
        }
    }

    public String toString() {
        return this.f1850b.toString() + ", hidden list:" + this.f1851c.size();
    }

    /* compiled from: ChildHelper */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        long f1852a = 0;

        /* renamed from: b  reason: collision with root package name */
        a f1853b;

        a() {
        }

        private void b() {
            if (this.f1853b == null) {
                this.f1853b = new a();
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            if (i2 >= 64) {
                a aVar = this.f1853b;
                if (aVar != null) {
                    aVar.a(i2 - 64);
                    return;
                }
                return;
            }
            this.f1852a &= (1 << i2) ^ -1;
        }

        /* access modifiers changed from: package-private */
        public boolean c(int i2) {
            if (i2 < 64) {
                return (this.f1852a & (1 << i2)) != 0;
            }
            b();
            return this.f1853b.c(i2 - 64);
        }

        /* access modifiers changed from: package-private */
        public boolean d(int i2) {
            if (i2 >= 64) {
                b();
                return this.f1853b.d(i2 - 64);
            }
            long j2 = 1 << i2;
            boolean z = (this.f1852a & j2) != 0;
            this.f1852a &= j2 ^ -1;
            long j3 = j2 - 1;
            long j4 = this.f1852a;
            this.f1852a = Long.rotateRight(j4 & (j3 ^ -1), 1) | (j4 & j3);
            a aVar = this.f1853b;
            if (aVar != null) {
                if (aVar.c(0)) {
                    e(63);
                }
                this.f1853b.d(0);
            }
            return z;
        }

        /* access modifiers changed from: package-private */
        public void e(int i2) {
            if (i2 >= 64) {
                b();
                this.f1853b.e(i2 - 64);
                return;
            }
            this.f1852a |= 1 << i2;
        }

        public String toString() {
            if (this.f1853b == null) {
                return Long.toBinaryString(this.f1852a);
            }
            return this.f1853b.toString() + "xx" + Long.toBinaryString(this.f1852a);
        }

        /* access modifiers changed from: package-private */
        public int b(int i2) {
            a aVar = this.f1853b;
            if (aVar == null) {
                if (i2 >= 64) {
                    return Long.bitCount(this.f1852a);
                }
                return Long.bitCount(this.f1852a & ((1 << i2) - 1));
            } else if (i2 < 64) {
                return Long.bitCount(this.f1852a & ((1 << i2) - 1));
            } else {
                return aVar.b(i2 - 64) + Long.bitCount(this.f1852a);
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f1852a = 0;
            a aVar = this.f1853b;
            if (aVar != null) {
                aVar.a();
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, boolean z) {
            if (i2 >= 64) {
                b();
                this.f1853b.a(i2 - 64, z);
                return;
            }
            boolean z2 = (this.f1852a & Long.MIN_VALUE) != 0;
            long j2 = (1 << i2) - 1;
            long j3 = this.f1852a;
            this.f1852a = ((j3 & (j2 ^ -1)) << 1) | (j3 & j2);
            if (z) {
                e(i2);
            } else {
                a(i2);
            }
            if (z2 || this.f1853b != null) {
                b();
                this.f1853b.a(0, z2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view, int i2, boolean z) {
        int i3;
        if (i2 < 0) {
            i3 = this.f1849a.a();
        } else {
            i3 = f(i2);
        }
        this.f1850b.a(i3, z);
        if (z) {
            g(view);
        }
        this.f1849a.addView(view, i3);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.f1850b.a();
        for (int size = this.f1851c.size() - 1; size >= 0; size--) {
            this.f1849a.d(this.f1851c.get(size));
            this.f1851c.remove(size);
        }
        this.f1849a.b();
    }

    /* access modifiers changed from: package-private */
    public void f(View view) {
        int b2 = this.f1849a.b(view);
        if (b2 < 0) {
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        } else if (this.f1850b.c(b2)) {
            this.f1850b.a(b2);
            h(view);
        } else {
            throw new RuntimeException("trying to unhide a view that was not hidden" + view);
        }
    }

    /* access modifiers changed from: package-private */
    public View d(int i2) {
        return this.f1849a.a(i2);
    }

    /* access modifiers changed from: package-private */
    public boolean e(View view) {
        int b2 = this.f1849a.b(view);
        if (b2 == -1) {
            h(view);
            return true;
        } else if (!this.f1850b.c(b2)) {
            return false;
        } else {
            this.f1850b.d(b2);
            h(view);
            this.f1849a.c(b2);
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view, int i2, ViewGroup.LayoutParams layoutParams, boolean z) {
        int i3;
        if (i2 < 0) {
            i3 = this.f1849a.a();
        } else {
            i3 = f(i2);
        }
        this.f1850b.a(i3, z);
        if (z) {
            g(view);
        }
        this.f1849a.a(view, i3, layoutParams);
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.f1849a.a();
    }

    /* access modifiers changed from: package-private */
    public int b(View view) {
        int b2 = this.f1849a.b(view);
        if (b2 != -1 && !this.f1850b.c(b2)) {
            return b2 - this.f1850b.b(b2);
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public boolean c(View view) {
        return this.f1851c.contains(view);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.f1849a.a() - this.f1851c.size();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        int f2 = f(i2);
        this.f1850b.d(f2);
        this.f1849a.b(f2);
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        int b2 = this.f1849a.b(view);
        if (b2 >= 0) {
            this.f1850b.e(b2);
            g(view);
            return;
        }
        throw new IllegalArgumentException("view is not a child, cannot hide " + view);
    }
}
