package androidx.recyclerview.widget;

import android.annotation.SuppressLint;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

/* compiled from: GapWorker */
final class e implements Runnable {

    /* renamed from: e  reason: collision with root package name */
    static final ThreadLocal<e> f1913e = new ThreadLocal<>();

    /* renamed from: f  reason: collision with root package name */
    static Comparator<c> f1914f = new a();

    /* renamed from: a  reason: collision with root package name */
    ArrayList<RecyclerView> f1915a = new ArrayList<>();

    /* renamed from: b  reason: collision with root package name */
    long f1916b;

    /* renamed from: c  reason: collision with root package name */
    long f1917c;

    /* renamed from: d  reason: collision with root package name */
    private ArrayList<c> f1918d = new ArrayList<>();

    /* compiled from: GapWorker */
    static class a implements Comparator<c> {
        a() {
        }

        /* renamed from: a */
        public int compare(c cVar, c cVar2) {
            if ((cVar.f1926d == null) == (cVar2.f1926d == null)) {
                boolean z = cVar.f1923a;
                if (z == cVar2.f1923a) {
                    int i2 = cVar2.f1924b - cVar.f1924b;
                    if (i2 != 0) {
                        return i2;
                    }
                    int i3 = cVar.f1925c - cVar2.f1925c;
                    if (i3 != 0) {
                        return i3;
                    }
                    return 0;
                } else if (z) {
                    return -1;
                } else {
                    return 1;
                }
            } else if (cVar.f1926d == null) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    /* compiled from: GapWorker */
    static class c {

        /* renamed from: a  reason: collision with root package name */
        public boolean f1923a;

        /* renamed from: b  reason: collision with root package name */
        public int f1924b;

        /* renamed from: c  reason: collision with root package name */
        public int f1925c;

        /* renamed from: d  reason: collision with root package name */
        public RecyclerView f1926d;

        /* renamed from: e  reason: collision with root package name */
        public int f1927e;

        c() {
        }

        public void a() {
            this.f1923a = false;
            this.f1924b = 0;
            this.f1925c = 0;
            this.f1926d = null;
            this.f1927e = 0;
        }
    }

    e() {
    }

    public void a(RecyclerView recyclerView) {
        this.f1915a.add(recyclerView);
    }

    public void b(RecyclerView recyclerView) {
        this.f1915a.remove(recyclerView);
    }

    public void run() {
        try {
            b.e.i.c.a("RV Prefetch");
            if (!this.f1915a.isEmpty()) {
                int size = this.f1915a.size();
                long j2 = 0;
                for (int i2 = 0; i2 < size; i2++) {
                    RecyclerView recyclerView = this.f1915a.get(i2);
                    if (recyclerView.getWindowVisibility() == 0) {
                        j2 = Math.max(recyclerView.getDrawingTime(), j2);
                    }
                }
                if (j2 != 0) {
                    a(TimeUnit.MILLISECONDS.toNanos(j2) + this.f1917c);
                    this.f1916b = 0;
                    b.e.i.c.a();
                }
            }
        } finally {
            this.f1916b = 0;
            b.e.i.c.a();
        }
    }

    private void b(long j2) {
        int i2 = 0;
        while (i2 < this.f1918d.size()) {
            c cVar = this.f1918d.get(i2);
            if (cVar.f1926d != null) {
                a(cVar, j2);
                cVar.a();
                i2++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView recyclerView, int i2, int i3) {
        if (recyclerView.isAttachedToWindow() && this.f1916b == 0) {
            this.f1916b = recyclerView.getNanoTime();
            recyclerView.post(this);
        }
        recyclerView.g0.b(i2, i3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.e.b.a(androidx.recyclerview.widget.RecyclerView, boolean):void
     arg types: [androidx.recyclerview.widget.RecyclerView, int]
     candidates:
      androidx.recyclerview.widget.e.b.a(int, int):void
      androidx.recyclerview.widget.RecyclerView.o.c.a(int, int):void
      androidx.recyclerview.widget.e.b.a(androidx.recyclerview.widget.RecyclerView, boolean):void */
    private void a() {
        c cVar;
        int size = this.f1915a.size();
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            RecyclerView recyclerView = this.f1915a.get(i3);
            if (recyclerView.getWindowVisibility() == 0) {
                recyclerView.g0.a(recyclerView, false);
                i2 += recyclerView.g0.f1922d;
            }
        }
        this.f1918d.ensureCapacity(i2);
        int i4 = 0;
        for (int i5 = 0; i5 < size; i5++) {
            RecyclerView recyclerView2 = this.f1915a.get(i5);
            if (recyclerView2.getWindowVisibility() == 0) {
                b bVar = recyclerView2.g0;
                int abs = Math.abs(bVar.f1919a) + Math.abs(bVar.f1920b);
                int i6 = i4;
                for (int i7 = 0; i7 < bVar.f1922d * 2; i7 += 2) {
                    if (i6 >= this.f1918d.size()) {
                        cVar = new c();
                        this.f1918d.add(cVar);
                    } else {
                        cVar = this.f1918d.get(i6);
                    }
                    int i8 = bVar.f1921c[i7 + 1];
                    cVar.f1923a = i8 <= abs;
                    cVar.f1924b = abs;
                    cVar.f1925c = i8;
                    cVar.f1926d = recyclerView2;
                    cVar.f1927e = bVar.f1921c[i7];
                    i6++;
                }
                i4 = i6;
            }
        }
        Collections.sort(this.f1918d, f1914f);
    }

    @SuppressLint({"VisibleForTests"})
    /* compiled from: GapWorker */
    static class b implements RecyclerView.o.c {

        /* renamed from: a  reason: collision with root package name */
        int f1919a;

        /* renamed from: b  reason: collision with root package name */
        int f1920b;

        /* renamed from: c  reason: collision with root package name */
        int[] f1921c;

        /* renamed from: d  reason: collision with root package name */
        int f1922d;

        b() {
        }

        /* access modifiers changed from: package-private */
        public void a(RecyclerView recyclerView, boolean z) {
            this.f1922d = 0;
            int[] iArr = this.f1921c;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            RecyclerView.o oVar = recyclerView.m;
            if (recyclerView.l != null && oVar != null && oVar.v()) {
                if (z) {
                    if (!recyclerView.f1727d.c()) {
                        oVar.a(recyclerView.l.getItemCount(), this);
                    }
                } else if (!recyclerView.j()) {
                    oVar.a(this.f1919a, this.f1920b, recyclerView.h0, this);
                }
                int i2 = this.f1922d;
                if (i2 > oVar.m) {
                    oVar.m = i2;
                    oVar.n = z;
                    recyclerView.f1725b.j();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void b(int i2, int i3) {
            this.f1919a = i2;
            this.f1920b = i3;
        }

        public void a(int i2, int i3) {
            if (i2 < 0) {
                throw new IllegalArgumentException("Layout positions must be non-negative");
            } else if (i3 >= 0) {
                int i4 = this.f1922d * 2;
                int[] iArr = this.f1921c;
                if (iArr == null) {
                    this.f1921c = new int[4];
                    Arrays.fill(this.f1921c, -1);
                } else if (i4 >= iArr.length) {
                    this.f1921c = new int[(i4 * 2)];
                    System.arraycopy(iArr, 0, this.f1921c, 0, iArr.length);
                }
                int[] iArr2 = this.f1921c;
                iArr2[i4] = i2;
                iArr2[i4 + 1] = i3;
                this.f1922d++;
            } else {
                throw new IllegalArgumentException("Pixel distance must be non-negative");
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i2) {
            if (this.f1921c != null) {
                int i3 = this.f1922d * 2;
                for (int i4 = 0; i4 < i3; i4 += 2) {
                    if (this.f1921c[i4] == i2) {
                        return true;
                    }
                }
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            int[] iArr = this.f1921c;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            this.f1922d = 0;
        }
    }

    static boolean a(RecyclerView recyclerView, int i2) {
        int b2 = recyclerView.f1728e.b();
        for (int i3 = 0; i3 < b2; i3++) {
            RecyclerView.c0 k2 = RecyclerView.k(recyclerView.f1728e.d(i3));
            if (k2.mPosition == i2 && !k2.isInvalid()) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.v.a(int, boolean, long):androidx.recyclerview.widget.RecyclerView$c0
     arg types: [int, int, long]
     candidates:
      androidx.recyclerview.widget.RecyclerView.v.a(long, int, boolean):androidx.recyclerview.widget.RecyclerView$c0
      androidx.recyclerview.widget.RecyclerView.v.a(int, int, boolean):void
      androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$g, androidx.recyclerview.widget.RecyclerView$g, boolean):void
      androidx.recyclerview.widget.RecyclerView.v.a(int, boolean, long):androidx.recyclerview.widget.RecyclerView$c0 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$c0, boolean):void
     arg types: [androidx.recyclerview.widget.RecyclerView$c0, int]
     candidates:
      androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void
      androidx.recyclerview.widget.RecyclerView.v.a(int, boolean):androidx.recyclerview.widget.RecyclerView$c0
      androidx.recyclerview.widget.RecyclerView.v.a(int, int):void
      androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$c0, boolean):void */
    private RecyclerView.c0 a(RecyclerView recyclerView, int i2, long j2) {
        if (a(recyclerView, i2)) {
            return null;
        }
        RecyclerView.v vVar = recyclerView.f1725b;
        try {
            recyclerView.q();
            RecyclerView.c0 a2 = vVar.a(i2, false, j2);
            if (a2 != null) {
                if (!a2.isBound() || a2.isInvalid()) {
                    vVar.a(a2, false);
                } else {
                    vVar.b(a2.itemView);
                }
            }
            return a2;
        } finally {
            recyclerView.a(false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.e.b.a(androidx.recyclerview.widget.RecyclerView, boolean):void
     arg types: [androidx.recyclerview.widget.RecyclerView, int]
     candidates:
      androidx.recyclerview.widget.e.b.a(int, int):void
      androidx.recyclerview.widget.RecyclerView.o.c.a(int, int):void
      androidx.recyclerview.widget.e.b.a(androidx.recyclerview.widget.RecyclerView, boolean):void */
    private void a(RecyclerView recyclerView, long j2) {
        if (recyclerView != null) {
            if (recyclerView.D && recyclerView.f1728e.b() != 0) {
                recyclerView.t();
            }
            b bVar = recyclerView.g0;
            bVar.a(recyclerView, true);
            if (bVar.f1922d != 0) {
                try {
                    b.e.i.c.a("RV Nested Prefetch");
                    recyclerView.h0.a(recyclerView.l);
                    for (int i2 = 0; i2 < bVar.f1922d * 2; i2 += 2) {
                        a(recyclerView, bVar.f1921c[i2], j2);
                    }
                } finally {
                    b.e.i.c.a();
                }
            }
        }
    }

    private void a(c cVar, long j2) {
        RecyclerView.c0 a2 = a(cVar.f1926d, cVar.f1927e, cVar.f1923a ? Long.MAX_VALUE : j2);
        if (a2 != null && a2.mNestedRecyclerView != null && a2.isBound() && !a2.isInvalid()) {
            a(a2.mNestedRecyclerView.get(), j2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        a();
        b(j2);
    }
}
