package androidx.recyclerview.widget;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import b.e.m.d0.c;
import b.e.m.d0.d;
import b.e.m.u;
import java.util.Map;
import java.util.WeakHashMap;

/* compiled from: RecyclerViewAccessibilityDelegate */
public class j extends b.e.m.a {

    /* renamed from: d  reason: collision with root package name */
    final RecyclerView f1941d;

    /* renamed from: e  reason: collision with root package name */
    private final a f1942e;

    /* compiled from: RecyclerViewAccessibilityDelegate */
    public static class a extends b.e.m.a {

        /* renamed from: d  reason: collision with root package name */
        final j f1943d;

        /* renamed from: e  reason: collision with root package name */
        private Map<View, b.e.m.a> f1944e = new WeakHashMap();

        public a(j jVar) {
            this.f1943d = jVar;
        }

        public void a(View view, c cVar) {
            if (this.f1943d.c() || this.f1943d.f1941d.getLayoutManager() == null) {
                super.a(view, cVar);
                return;
            }
            this.f1943d.f1941d.getLayoutManager().a(view, cVar);
            b.e.m.a aVar = this.f1944e.get(view);
            if (aVar != null) {
                aVar.a(view, cVar);
            } else {
                super.a(view, cVar);
            }
        }

        public void b(View view, AccessibilityEvent accessibilityEvent) {
            b.e.m.a aVar = this.f1944e.get(view);
            if (aVar != null) {
                aVar.b(view, accessibilityEvent);
            } else {
                super.b(view, accessibilityEvent);
            }
        }

        /* access modifiers changed from: package-private */
        public b.e.m.a c(View view) {
            return this.f1944e.remove(view);
        }

        /* access modifiers changed from: package-private */
        public void d(View view) {
            b.e.m.a b2 = u.b(view);
            if (b2 != null && b2 != this) {
                this.f1944e.put(view, b2);
            }
        }

        public void c(View view, AccessibilityEvent accessibilityEvent) {
            b.e.m.a aVar = this.f1944e.get(view);
            if (aVar != null) {
                aVar.c(view, accessibilityEvent);
            } else {
                super.c(view, accessibilityEvent);
            }
        }

        public void d(View view, AccessibilityEvent accessibilityEvent) {
            b.e.m.a aVar = this.f1944e.get(view);
            if (aVar != null) {
                aVar.d(view, accessibilityEvent);
            } else {
                super.d(view, accessibilityEvent);
            }
        }

        public boolean a(View view, int i2, Bundle bundle) {
            if (this.f1943d.c() || this.f1943d.f1941d.getLayoutManager() == null) {
                return super.a(view, i2, bundle);
            }
            b.e.m.a aVar = this.f1944e.get(view);
            if (aVar != null) {
                if (aVar.a(view, i2, bundle)) {
                    return true;
                }
            } else if (super.a(view, i2, bundle)) {
                return true;
            }
            return this.f1943d.f1941d.getLayoutManager().a(view, i2, bundle);
        }

        public void a(View view, int i2) {
            b.e.m.a aVar = this.f1944e.get(view);
            if (aVar != null) {
                aVar.a(view, i2);
            } else {
                super.a(view, i2);
            }
        }

        public boolean a(View view, AccessibilityEvent accessibilityEvent) {
            b.e.m.a aVar = this.f1944e.get(view);
            if (aVar != null) {
                return aVar.a(view, accessibilityEvent);
            }
            return super.a(view, accessibilityEvent);
        }

        public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            b.e.m.a aVar = this.f1944e.get(viewGroup);
            if (aVar != null) {
                return aVar.a(viewGroup, view, accessibilityEvent);
            }
            return super.a(viewGroup, view, accessibilityEvent);
        }

        public d a(View view) {
            b.e.m.a aVar = this.f1944e.get(view);
            if (aVar != null) {
                return aVar.a(view);
            }
            return super.a(view);
        }
    }

    public j(RecyclerView recyclerView) {
        this.f1941d = recyclerView;
        b.e.m.a b2 = b();
        if (b2 == null || !(b2 instanceof a)) {
            this.f1942e = new a(this);
        } else {
            this.f1942e = (a) b2;
        }
    }

    public boolean a(View view, int i2, Bundle bundle) {
        if (super.a(view, i2, bundle)) {
            return true;
        }
        if (c() || this.f1941d.getLayoutManager() == null) {
            return false;
        }
        return this.f1941d.getLayoutManager().a(i2, bundle);
    }

    public void b(View view, AccessibilityEvent accessibilityEvent) {
        super.b(view, accessibilityEvent);
        if ((view instanceof RecyclerView) && !c()) {
            RecyclerView recyclerView = (RecyclerView) view;
            if (recyclerView.getLayoutManager() != null) {
                recyclerView.getLayoutManager().a(accessibilityEvent);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.f1941d.j();
    }

    public void a(View view, c cVar) {
        super.a(view, cVar);
        if (!c() && this.f1941d.getLayoutManager() != null) {
            this.f1941d.getLayoutManager().a(cVar);
        }
    }

    public b.e.m.a b() {
        return this.f1942e;
    }
}
