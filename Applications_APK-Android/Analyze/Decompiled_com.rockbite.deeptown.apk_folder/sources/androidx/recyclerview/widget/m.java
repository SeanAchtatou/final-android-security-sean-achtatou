package androidx.recyclerview.widget;

import android.view.View;

/* compiled from: ViewBoundsCheck */
class m {

    /* renamed from: a  reason: collision with root package name */
    final b f1946a;

    /* renamed from: b  reason: collision with root package name */
    a f1947b = new a();

    /* compiled from: ViewBoundsCheck */
    interface b {
        int a();

        int a(View view);

        View a(int i2);

        int b();

        int b(View view);
    }

    m(b bVar) {
        this.f1946a = bVar;
    }

    /* access modifiers changed from: package-private */
    public View a(int i2, int i3, int i4, int i5) {
        int a2 = this.f1946a.a();
        int b2 = this.f1946a.b();
        int i6 = i3 > i2 ? 1 : -1;
        View view = null;
        while (i2 != i3) {
            View a3 = this.f1946a.a(i2);
            this.f1947b.a(a2, b2, this.f1946a.a(a3), this.f1946a.b(a3));
            if (i4 != 0) {
                this.f1947b.b();
                this.f1947b.a(i4);
                if (this.f1947b.a()) {
                    return a3;
                }
            }
            if (i5 != 0) {
                this.f1947b.b();
                this.f1947b.a(i5);
                if (this.f1947b.a()) {
                    view = a3;
                }
            }
            i2 += i6;
        }
        return view;
    }

    /* compiled from: ViewBoundsCheck */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        int f1948a = 0;

        /* renamed from: b  reason: collision with root package name */
        int f1949b;

        /* renamed from: c  reason: collision with root package name */
        int f1950c;

        /* renamed from: d  reason: collision with root package name */
        int f1951d;

        /* renamed from: e  reason: collision with root package name */
        int f1952e;

        a() {
        }

        /* access modifiers changed from: package-private */
        public int a(int i2, int i3) {
            if (i2 > i3) {
                return 1;
            }
            return i2 == i3 ? 2 : 4;
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, int i3, int i4, int i5) {
            this.f1949b = i2;
            this.f1950c = i3;
            this.f1951d = i4;
            this.f1952e = i5;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f1948a = 0;
        }

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            this.f1948a = i2 | this.f1948a;
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            int i2 = this.f1948a;
            if ((i2 & 7) != 0 && (i2 & (a(this.f1951d, this.f1949b) << 0)) == 0) {
                return false;
            }
            int i3 = this.f1948a;
            if ((i3 & 112) != 0 && (i3 & (a(this.f1951d, this.f1950c) << 4)) == 0) {
                return false;
            }
            int i4 = this.f1948a;
            if ((i4 & 1792) != 0 && (i4 & (a(this.f1952e, this.f1949b) << 8)) == 0) {
                return false;
            }
            int i5 = this.f1948a;
            if ((i5 & 28672) == 0 || (i5 & (a(this.f1952e, this.f1950c) << 12)) != 0) {
                return true;
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view, int i2) {
        this.f1947b.a(this.f1946a.a(), this.f1946a.b(), this.f1946a.a(view), this.f1946a.b(view));
        if (i2 == 0) {
            return false;
        }
        this.f1947b.b();
        this.f1947b.a(i2);
        return this.f1947b.a();
    }
}
