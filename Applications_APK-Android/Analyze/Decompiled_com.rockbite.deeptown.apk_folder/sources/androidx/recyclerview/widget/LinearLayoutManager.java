package androidx.recyclerview.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.gms.common.api.Api;
import java.util.List;

public class LinearLayoutManager extends RecyclerView.o implements f, RecyclerView.y.a {
    int A = -1;
    int B = Integer.MIN_VALUE;
    private boolean C;
    SavedState D = null;
    final a E = new a();
    private final b F = new b();
    private int G = 2;
    private int[] H = new int[2];
    int s = 1;
    private c t;
    i u;
    private boolean v;
    private boolean w = false;
    boolean x = false;
    private boolean y = false;
    private boolean z = true;

    @SuppressLint({"BanParcelableUsage"})
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();

        /* renamed from: a  reason: collision with root package name */
        int f1701a;

        /* renamed from: b  reason: collision with root package name */
        int f1702b;

        /* renamed from: c  reason: collision with root package name */
        boolean f1703c;

        static class a implements Parcelable.Creator<SavedState> {
            a() {
            }

            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] newArray(int i2) {
                return new SavedState[i2];
            }
        }

        public SavedState() {
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return this.f1701a >= 0;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f1701a = -1;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeInt(this.f1701a);
            parcel.writeInt(this.f1702b);
            parcel.writeInt(this.f1703c ? 1 : 0);
        }

        SavedState(Parcel parcel) {
            this.f1701a = parcel.readInt();
            this.f1702b = parcel.readInt();
            this.f1703c = parcel.readInt() != 1 ? false : true;
        }

        public SavedState(SavedState savedState) {
            this.f1701a = savedState.f1701a;
            this.f1702b = savedState.f1702b;
            this.f1703c = savedState.f1703c;
        }
    }

    protected static class b {

        /* renamed from: a  reason: collision with root package name */
        public int f1709a;

        /* renamed from: b  reason: collision with root package name */
        public boolean f1710b;

        /* renamed from: c  reason: collision with root package name */
        public boolean f1711c;

        /* renamed from: d  reason: collision with root package name */
        public boolean f1712d;

        protected b() {
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f1709a = 0;
            this.f1710b = false;
            this.f1711c = false;
            this.f1712d = false;
        }
    }

    static class c {

        /* renamed from: a  reason: collision with root package name */
        boolean f1713a = true;

        /* renamed from: b  reason: collision with root package name */
        int f1714b;

        /* renamed from: c  reason: collision with root package name */
        int f1715c;

        /* renamed from: d  reason: collision with root package name */
        int f1716d;

        /* renamed from: e  reason: collision with root package name */
        int f1717e;

        /* renamed from: f  reason: collision with root package name */
        int f1718f;

        /* renamed from: g  reason: collision with root package name */
        int f1719g;

        /* renamed from: h  reason: collision with root package name */
        int f1720h = 0;

        /* renamed from: i  reason: collision with root package name */
        int f1721i = 0;

        /* renamed from: j  reason: collision with root package name */
        boolean f1722j;

        /* renamed from: k  reason: collision with root package name */
        int f1723k;
        List<RecyclerView.c0> l = null;
        boolean m;

        c() {
        }

        private View b() {
            int size = this.l.size();
            for (int i2 = 0; i2 < size; i2++) {
                View view = this.l.get(i2).itemView;
                RecyclerView.p pVar = (RecyclerView.p) view.getLayoutParams();
                if (!pVar.c() && this.f1716d == pVar.a()) {
                    a(view);
                    return view;
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public boolean a(RecyclerView.z zVar) {
            int i2 = this.f1716d;
            return i2 >= 0 && i2 < zVar.a();
        }

        /* access modifiers changed from: package-private */
        public View a(RecyclerView.v vVar) {
            if (this.l != null) {
                return b();
            }
            View d2 = vVar.d(this.f1716d);
            this.f1716d += this.f1717e;
            return d2;
        }

        public void a() {
            a((View) null);
        }

        public void a(View view) {
            View b2 = b(view);
            if (b2 == null) {
                this.f1716d = -1;
            } else {
                this.f1716d = ((RecyclerView.p) b2.getLayoutParams()).a();
            }
        }

        public View b(View view) {
            int a2;
            int size = this.l.size();
            View view2 = null;
            int i2 = Api.BaseClientBuilder.API_PRIORITY_OTHER;
            for (int i3 = 0; i3 < size; i3++) {
                View view3 = this.l.get(i3).itemView;
                RecyclerView.p pVar = (RecyclerView.p) view3.getLayoutParams();
                if (view3 != view && !pVar.c() && (a2 = (pVar.a() - this.f1716d) * this.f1717e) >= 0 && a2 < i2) {
                    view2 = view3;
                    if (a2 == 0) {
                        break;
                    }
                    i2 = a2;
                }
            }
            return view2;
        }
    }

    public LinearLayoutManager(Context context, int i2, boolean z2) {
        i(i2);
        a(z2);
    }

    private View L() {
        return e(0, e());
    }

    private View M() {
        return e(e() - 1, -1);
    }

    private View N() {
        if (this.x) {
            return L();
        }
        return M();
    }

    private View O() {
        if (this.x) {
            return M();
        }
        return L();
    }

    private View P() {
        return c(this.x ? 0 : e() - 1);
    }

    private View Q() {
        return c(this.x ? e() - 1 : 0);
    }

    private void R() {
        if (this.s == 1 || !I()) {
            this.x = this.w;
        } else {
            this.x = !this.w;
        }
    }

    private void f(int i2, int i3) {
        this.t.f1715c = this.u.b() - i3;
        this.t.f1717e = this.x ? -1 : 1;
        c cVar = this.t;
        cVar.f1716d = i2;
        cVar.f1718f = 1;
        cVar.f1714b = i3;
        cVar.f1719g = Integer.MIN_VALUE;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.b(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.b(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.b(int, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(android.view.View, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(android.view.View, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.LinearLayoutManager.b(boolean, boolean):android.view.View */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, int[]):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, android.view.View):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet):androidx.recyclerview.widget.RecyclerView$p
      androidx.recyclerview.widget.RecyclerView.o.a(int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$g, androidx.recyclerview.widget.RecyclerView$g):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, android.os.Bundle):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(boolean, boolean):android.view.View */
    private int j(RecyclerView.z zVar) {
        if (e() == 0) {
            return 0;
        }
        E();
        i iVar = this.u;
        View b2 = b(!this.z, true);
        return k.a(zVar, iVar, b2, a(!this.z, true), this, this.z, this.x);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.b(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.b(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.b(int, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(android.view.View, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(android.view.View, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.LinearLayoutManager.b(boolean, boolean):android.view.View */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, int[]):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, android.view.View):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet):androidx.recyclerview.widget.RecyclerView$p
      androidx.recyclerview.widget.RecyclerView.o.a(int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$g, androidx.recyclerview.widget.RecyclerView$g):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, android.os.Bundle):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(boolean, boolean):android.view.View */
    private int k(RecyclerView.z zVar) {
        if (e() == 0) {
            return 0;
        }
        E();
        i iVar = this.u;
        View b2 = b(!this.z, true);
        return k.b(zVar, iVar, b2, a(!this.z, true), this, this.z);
    }

    /* access modifiers changed from: package-private */
    public boolean A() {
        return (i() == 1073741824 || r() == 1073741824 || !s()) ? false : true;
    }

    public boolean C() {
        return this.D == null && this.v == this.y;
    }

    /* access modifiers changed from: package-private */
    public c D() {
        return new c();
    }

    /* access modifiers changed from: package-private */
    public void E() {
        if (this.t == null) {
            this.t = D();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
     arg types: [int, int, int, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet, int, int):androidx.recyclerview.widget.RecyclerView$o$d
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, androidx.recyclerview.widget.RecyclerView$p):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$z, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View */
    public int F() {
        View a2 = a(0, e(), false, true);
        if (a2 == null) {
            return -1;
        }
        return l(a2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
     arg types: [int, int, int, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet, int, int):androidx.recyclerview.widget.RecyclerView$o$d
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, androidx.recyclerview.widget.RecyclerView$p):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$z, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View */
    public int G() {
        View a2 = a(e() - 1, -1, false, true);
        if (a2 == null) {
            return -1;
        }
        return l(a2);
    }

    public int H() {
        return this.s;
    }

    /* access modifiers changed from: protected */
    public boolean I() {
        return j() == 1;
    }

    public boolean J() {
        return this.z;
    }

    /* access modifiers changed from: package-private */
    public boolean K() {
        return this.u.d() == 0 && this.u.a() == 0;
    }

    public void a(AccessibilityEvent accessibilityEvent) {
        super.a(accessibilityEvent);
        if (e() > 0) {
            accessibilityEvent.setFromIndex(F());
            accessibilityEvent.setToIndex(G());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.v vVar, RecyclerView.z zVar, a aVar, int i2) {
    }

    public void b(RecyclerView recyclerView, RecyclerView.v vVar) {
        super.b(recyclerView, vVar);
        if (this.C) {
            b(vVar);
            vVar.a();
        }
    }

    public RecyclerView.p c() {
        return new RecyclerView.p(-2, -2);
    }

    public int d(RecyclerView.z zVar) {
        return i(zVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int
     arg types: [androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet, int, int):androidx.recyclerview.widget.RecyclerView$o$d
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, androidx.recyclerview.widget.RecyclerView$p):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$z, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int
     arg types: [int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet, int, int):androidx.recyclerview.widget.RecyclerView$o$d
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, androidx.recyclerview.widget.RecyclerView$p):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$z, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int
     arg types: [int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(android.view.View, int, int, androidx.recyclerview.widget.RecyclerView$p):boolean
      androidx.recyclerview.widget.LinearLayoutManager.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int */
    public void e(RecyclerView.v vVar, RecyclerView.z zVar) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        View b2;
        int i8;
        int i9;
        int i10 = -1;
        if (!(this.D == null && this.A == -1) && zVar.a() == 0) {
            b(vVar);
            return;
        }
        SavedState savedState = this.D;
        if (savedState != null && savedState.a()) {
            this.A = this.D.f1701a;
        }
        E();
        this.t.f1713a = false;
        R();
        View g2 = g();
        if (!this.E.f1708e || this.A != -1 || this.D != null) {
            this.E.b();
            a aVar = this.E;
            aVar.f1707d = this.x ^ this.y;
            b(vVar, zVar, aVar);
            this.E.f1708e = true;
        } else if (g2 != null && (this.u.d(g2) >= this.u.b() || this.u.a(g2) <= this.u.f())) {
            this.E.b(g2, l(g2));
        }
        c cVar = this.t;
        cVar.f1718f = cVar.f1723k >= 0 ? 1 : -1;
        int[] iArr = this.H;
        iArr[0] = 0;
        iArr[1] = 0;
        a(zVar, iArr);
        int max = Math.max(0, this.H[0]) + this.u.f();
        int max2 = Math.max(0, this.H[1]) + this.u.c();
        if (!(!zVar.d() || (i7 = this.A) == -1 || this.B == Integer.MIN_VALUE || (b2 = b(i7)) == null)) {
            if (this.x) {
                i8 = this.u.b() - this.u.a(b2);
                i9 = this.B;
            } else {
                i9 = this.u.d(b2) - this.u.f();
                i8 = this.B;
            }
            int i11 = i8 - i9;
            if (i11 > 0) {
                max += i11;
            } else {
                max2 -= i11;
            }
        }
        if (!this.E.f1707d ? !this.x : this.x) {
            i10 = 1;
        }
        a(vVar, zVar, this.E, i10);
        a(vVar);
        this.t.m = K();
        this.t.f1722j = zVar.d();
        this.t.f1721i = 0;
        a aVar2 = this.E;
        if (aVar2.f1707d) {
            b(aVar2);
            c cVar2 = this.t;
            cVar2.f1720h = max;
            a(vVar, cVar2, zVar, false);
            c cVar3 = this.t;
            i3 = cVar3.f1714b;
            int i12 = cVar3.f1716d;
            int i13 = cVar3.f1715c;
            if (i13 > 0) {
                max2 += i13;
            }
            a(this.E);
            c cVar4 = this.t;
            cVar4.f1720h = max2;
            cVar4.f1716d += cVar4.f1717e;
            a(vVar, cVar4, zVar, false);
            c cVar5 = this.t;
            i2 = cVar5.f1714b;
            int i14 = cVar5.f1715c;
            if (i14 > 0) {
                g(i12, i3);
                c cVar6 = this.t;
                cVar6.f1720h = i14;
                a(vVar, cVar6, zVar, false);
                i3 = this.t.f1714b;
            }
        } else {
            a(aVar2);
            c cVar7 = this.t;
            cVar7.f1720h = max2;
            a(vVar, cVar7, zVar, false);
            c cVar8 = this.t;
            i2 = cVar8.f1714b;
            int i15 = cVar8.f1716d;
            int i16 = cVar8.f1715c;
            if (i16 > 0) {
                max += i16;
            }
            b(this.E);
            c cVar9 = this.t;
            cVar9.f1720h = max;
            cVar9.f1716d += cVar9.f1717e;
            a(vVar, cVar9, zVar, false);
            c cVar10 = this.t;
            i3 = cVar10.f1714b;
            int i17 = cVar10.f1715c;
            if (i17 > 0) {
                f(i15, i2);
                c cVar11 = this.t;
                cVar11.f1720h = i17;
                a(vVar, cVar11, zVar, false);
                i2 = this.t.f1714b;
            }
        }
        if (e() > 0) {
            if (this.x ^ this.y) {
                int a2 = a(i2, vVar, zVar, true);
                i5 = i3 + a2;
                i4 = i2 + a2;
                i6 = b(i5, vVar, zVar, false);
            } else {
                int b3 = b(i3, vVar, zVar, true);
                i5 = i3 + b3;
                i4 = i2 + b3;
                i6 = a(i4, vVar, zVar, false);
            }
            i3 = i5 + i6;
            i2 = i4 + i6;
        }
        b(vVar, zVar, i3, i2);
        if (!zVar.d()) {
            this.u.i();
        } else {
            this.E.b();
        }
        this.v = this.y;
    }

    public void g(RecyclerView.z zVar) {
        super.g(zVar);
        this.D = null;
        this.A = -1;
        this.B = Integer.MIN_VALUE;
        this.E.b();
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public int h(RecyclerView.z zVar) {
        if (zVar.c()) {
            return this.u.g();
        }
        return 0;
    }

    public void i(int i2) {
        if (i2 == 0 || i2 == 1) {
            a((String) null);
            if (i2 != this.s || this.u == null) {
                this.u = i.a(this, i2);
                this.E.f1704a = this.u;
                this.s = i2;
                y();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("invalid orientation:" + i2);
    }

    public boolean u() {
        return true;
    }

    public Parcelable x() {
        SavedState savedState = this.D;
        if (savedState != null) {
            return new SavedState(savedState);
        }
        SavedState savedState2 = new SavedState();
        if (e() > 0) {
            E();
            boolean z2 = this.v ^ this.x;
            savedState2.f1703c = z2;
            if (z2) {
                View P = P();
                savedState2.f1702b = this.u.b() - this.u.a(P);
                savedState2.f1701a = l(P);
            } else {
                View Q = Q();
                savedState2.f1701a = l(Q);
                savedState2.f1702b = this.u.d(Q) - this.u.f();
            }
        } else {
            savedState2.b();
        }
        return savedState2;
    }

    public int c(RecyclerView.z zVar) {
        return k(zVar);
    }

    static class a {

        /* renamed from: a  reason: collision with root package name */
        i f1704a;

        /* renamed from: b  reason: collision with root package name */
        int f1705b;

        /* renamed from: c  reason: collision with root package name */
        int f1706c;

        /* renamed from: d  reason: collision with root package name */
        boolean f1707d;

        /* renamed from: e  reason: collision with root package name */
        boolean f1708e;

        a() {
            b();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            int i2;
            if (this.f1707d) {
                i2 = this.f1704a.b();
            } else {
                i2 = this.f1704a.f();
            }
            this.f1706c = i2;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f1705b = -1;
            this.f1706c = Integer.MIN_VALUE;
            this.f1707d = false;
            this.f1708e = false;
        }

        public String toString() {
            return "AnchorInfo{mPosition=" + this.f1705b + ", mCoordinate=" + this.f1706c + ", mLayoutFromEnd=" + this.f1707d + ", mValid=" + this.f1708e + '}';
        }

        /* access modifiers changed from: package-private */
        public boolean a(View view, RecyclerView.z zVar) {
            RecyclerView.p pVar = (RecyclerView.p) view.getLayoutParams();
            return !pVar.c() && pVar.a() >= 0 && pVar.a() < zVar.a();
        }

        public void b(View view, int i2) {
            int h2 = this.f1704a.h();
            if (h2 >= 0) {
                a(view, i2);
                return;
            }
            this.f1705b = i2;
            if (this.f1707d) {
                int b2 = (this.f1704a.b() - h2) - this.f1704a.a(view);
                this.f1706c = this.f1704a.b() - b2;
                if (b2 > 0) {
                    int b3 = this.f1706c - this.f1704a.b(view);
                    int f2 = this.f1704a.f();
                    int min = b3 - (f2 + Math.min(this.f1704a.d(view) - f2, 0));
                    if (min < 0) {
                        this.f1706c += Math.min(b2, -min);
                        return;
                    }
                    return;
                }
                return;
            }
            int d2 = this.f1704a.d(view);
            int f3 = d2 - this.f1704a.f();
            this.f1706c = d2;
            if (f3 > 0) {
                int b4 = (this.f1704a.b() - Math.min(0, (this.f1704a.b() - h2) - this.f1704a.a(view))) - (d2 + this.f1704a.b(view));
                if (b4 < 0) {
                    this.f1706c -= Math.min(f3, -b4);
                }
            }
        }

        public void a(View view, int i2) {
            if (this.f1707d) {
                this.f1706c = this.f1704a.a(view) + this.f1704a.h();
            } else {
                this.f1706c = this.f1704a.d(view);
            }
            this.f1705b = i2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void
     arg types: [int, int, int, androidx.recyclerview.widget.RecyclerView$z]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet, int, int):androidx.recyclerview.widget.RecyclerView$o$d
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, androidx.recyclerview.widget.RecyclerView$p):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$z, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int
     arg types: [androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet, int, int):androidx.recyclerview.widget.RecyclerView$o$d
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, androidx.recyclerview.widget.RecyclerView$p):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$z, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int */
    /* access modifiers changed from: package-private */
    public int c(int i2, RecyclerView.v vVar, RecyclerView.z zVar) {
        if (e() == 0 || i2 == 0) {
            return 0;
        }
        E();
        this.t.f1713a = true;
        int i3 = i2 > 0 ? 1 : -1;
        int abs = Math.abs(i2);
        a(i3, abs, true, zVar);
        c cVar = this.t;
        int a2 = cVar.f1719g + a(vVar, cVar, zVar, false);
        if (a2 < 0) {
            return 0;
        }
        if (abs > a2) {
            i2 = i3 * a2;
        }
        this.u.a(-i2);
        this.t.f1723k = i2;
        return i2;
    }

    /* access modifiers changed from: package-private */
    public int h(int i2) {
        if (i2 == 1) {
            return (this.s != 1 && I()) ? 1 : -1;
        }
        if (i2 == 2) {
            return (this.s != 1 && I()) ? -1 : 1;
        }
        if (i2 != 17) {
            if (i2 != 33) {
                if (i2 != 66) {
                    return (i2 == 130 && this.s == 1) ? 1 : Integer.MIN_VALUE;
                }
                if (this.s == 0) {
                    return 1;
                }
                return Integer.MIN_VALUE;
            } else if (this.s == 1) {
                return -1;
            } else {
                return Integer.MIN_VALUE;
            }
        } else if (this.s == 0) {
            return -1;
        } else {
            return Integer.MIN_VALUE;
        }
    }

    public void a(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.D = (SavedState) parcelable;
            y();
        }
    }

    public boolean b() {
        return this.s == 1;
    }

    private void g(int i2, int i3) {
        this.t.f1715c = i3 - this.u.f();
        c cVar = this.t;
        cVar.f1716d = i2;
        cVar.f1717e = this.x ? 1 : -1;
        c cVar2 = this.t;
        cVar2.f1718f = -1;
        cVar2.f1714b = i3;
        cVar2.f1719g = Integer.MIN_VALUE;
    }

    public void b(boolean z2) {
        a((String) null);
        if (this.y != z2) {
            this.y = z2;
            y();
        }
    }

    public int f(RecyclerView.z zVar) {
        return k(zVar);
    }

    private View f(RecyclerView.v vVar, RecyclerView.z zVar) {
        return a(vVar, zVar, 0, e(), zVar.a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.b(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.b(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.b(int, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(android.view.View, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(android.view.View, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.LinearLayoutManager.b(boolean, boolean):android.view.View */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, int[]):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, android.view.View):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet):androidx.recyclerview.widget.RecyclerView$p
      androidx.recyclerview.widget.RecyclerView.o.a(int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$g, androidx.recyclerview.widget.RecyclerView$g):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, android.os.Bundle):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(boolean, boolean):android.view.View */
    private int i(RecyclerView.z zVar) {
        if (e() == 0) {
            return 0;
        }
        E();
        i iVar = this.u;
        View b2 = b(!this.z, true);
        return k.a(zVar, iVar, b2, a(!this.z, true), this, this.z);
    }

    public boolean a() {
        return this.s == 0;
    }

    public void a(boolean z2) {
        a((String) null);
        if (z2 != this.w) {
            this.w = z2;
            y();
        }
    }

    public View b(int i2) {
        int e2 = e();
        if (e2 == 0) {
            return null;
        }
        int l = i2 - l(c(0));
        if (l >= 0 && l < e2) {
            View c2 = c(l);
            if (l(c2) == i2) {
                return c2;
            }
        }
        return super.b(i2);
    }

    private View h(RecyclerView.v vVar, RecyclerView.z zVar) {
        if (this.x) {
            return f(vVar, zVar);
        }
        return g(vVar, zVar);
    }

    private void c(RecyclerView.v vVar, int i2, int i3) {
        if (i2 >= 0) {
            int i4 = i2 - i3;
            int e2 = e();
            if (this.x) {
                int i5 = e2 - 1;
                for (int i6 = i5; i6 >= 0; i6--) {
                    View c2 = c(i6);
                    if (this.u.a(c2) > i4 || this.u.e(c2) > i4) {
                        a(vVar, i5, i6);
                        return;
                    }
                }
                return;
            }
            for (int i7 = 0; i7 < e2; i7++) {
                View c3 = c(i7);
                if (this.u.a(c3) > i4 || this.u.e(c3) > i4) {
                    a(vVar, 0, i7);
                    return;
                }
            }
        }
    }

    private View g(RecyclerView.v vVar, RecyclerView.z zVar) {
        return a(vVar, zVar, e() - 1, -1, zVar.a());
    }

    /* access modifiers changed from: protected */
    public void a(RecyclerView.z zVar, int[] iArr) {
        int i2;
        int h2 = h(zVar);
        if (this.t.f1718f == -1) {
            i2 = 0;
        } else {
            i2 = h2;
            h2 = 0;
        }
        iArr[0] = h2;
        iArr[1] = i2;
    }

    private View i(RecyclerView.v vVar, RecyclerView.z zVar) {
        if (this.x) {
            return g(vVar, zVar);
        }
        return f(vVar, zVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int
     arg types: [androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet, int, int):androidx.recyclerview.widget.RecyclerView$o$d
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, androidx.recyclerview.widget.RecyclerView$p):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$z, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int */
    private void b(RecyclerView.v vVar, RecyclerView.z zVar, int i2, int i3) {
        RecyclerView.v vVar2 = vVar;
        RecyclerView.z zVar2 = zVar;
        if (zVar.e() && e() != 0 && !zVar.d() && C()) {
            List<RecyclerView.c0> f2 = vVar.f();
            int size = f2.size();
            int l = l(c(0));
            int i4 = 0;
            int i5 = 0;
            for (int i6 = 0; i6 < size; i6++) {
                RecyclerView.c0 c0Var = f2.get(i6);
                if (!c0Var.isRemoved()) {
                    char c2 = 1;
                    if ((c0Var.getLayoutPosition() < l) != this.x) {
                        c2 = 65535;
                    }
                    if (c2 == 65535) {
                        i4 += this.u.b(c0Var.itemView);
                    } else {
                        i5 += this.u.b(c0Var.itemView);
                    }
                }
            }
            this.t.l = f2;
            if (i4 > 0) {
                g(l(Q()), i2);
                c cVar = this.t;
                cVar.f1720h = i4;
                cVar.f1715c = 0;
                cVar.a();
                a(vVar2, this.t, zVar2, false);
            }
            if (i5 > 0) {
                f(l(P()), i3);
                c cVar2 = this.t;
                cVar2.f1720h = i5;
                cVar2.f1715c = 0;
                cVar2.a();
                a(vVar2, this.t, zVar2, false);
            }
            this.t.l = null;
        }
    }

    public LinearLayoutManager(Context context, AttributeSet attributeSet, int i2, int i3) {
        RecyclerView.o.d a2 = RecyclerView.o.a(context, attributeSet, i2, i3);
        i(a2.f1770a);
        a(a2.f1772c);
        b(a2.f1773d);
    }

    private boolean a(RecyclerView.v vVar, RecyclerView.z zVar, a aVar) {
        View view;
        int i2;
        boolean z2 = false;
        if (e() == 0) {
            return false;
        }
        View g2 = g();
        if (g2 != null && aVar.a(g2, zVar)) {
            aVar.b(g2, l(g2));
            return true;
        } else if (this.v != this.y) {
            return false;
        } else {
            if (aVar.f1707d) {
                view = h(vVar, zVar);
            } else {
                view = i(vVar, zVar);
            }
            if (view == null) {
                return false;
            }
            aVar.a(view, l(view));
            if (!zVar.d() && C()) {
                if (this.u.d(view) >= this.u.b() || this.u.a(view) < this.u.f()) {
                    z2 = true;
                }
                if (z2) {
                    if (aVar.f1707d) {
                        i2 = this.u.b();
                    } else {
                        i2 = this.u.f();
                    }
                    aVar.f1706c = i2;
                }
            }
            return true;
        }
    }

    private boolean a(RecyclerView.z zVar, a aVar) {
        int i2;
        int i3;
        boolean z2 = false;
        if (!zVar.d() && (i2 = this.A) != -1) {
            if (i2 < 0 || i2 >= zVar.a()) {
                this.A = -1;
                this.B = Integer.MIN_VALUE;
            } else {
                aVar.f1705b = this.A;
                SavedState savedState = this.D;
                if (savedState != null && savedState.a()) {
                    aVar.f1707d = this.D.f1703c;
                    if (aVar.f1707d) {
                        aVar.f1706c = this.u.b() - this.D.f1702b;
                    } else {
                        aVar.f1706c = this.u.f() + this.D.f1702b;
                    }
                    return true;
                } else if (this.B == Integer.MIN_VALUE) {
                    View b2 = b(this.A);
                    if (b2 == null) {
                        if (e() > 0) {
                            if ((this.A < l(c(0))) == this.x) {
                                z2 = true;
                            }
                            aVar.f1707d = z2;
                        }
                        aVar.a();
                    } else if (this.u.b(b2) > this.u.g()) {
                        aVar.a();
                        return true;
                    } else if (this.u.d(b2) - this.u.f() < 0) {
                        aVar.f1706c = this.u.f();
                        aVar.f1707d = false;
                        return true;
                    } else if (this.u.b() - this.u.a(b2) < 0) {
                        aVar.f1706c = this.u.b();
                        aVar.f1707d = true;
                        return true;
                    } else {
                        if (aVar.f1707d) {
                            i3 = this.u.a(b2) + this.u.h();
                        } else {
                            i3 = this.u.d(b2);
                        }
                        aVar.f1706c = i3;
                    }
                    return true;
                } else {
                    boolean z3 = this.x;
                    aVar.f1707d = z3;
                    if (z3) {
                        aVar.f1706c = this.u.b() - this.B;
                    } else {
                        aVar.f1706c = this.u.f() + this.B;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private void b(RecyclerView.v vVar, RecyclerView.z zVar, a aVar) {
        if (!a(zVar, aVar) && !a(vVar, zVar, aVar)) {
            aVar.a();
            aVar.f1705b = this.y ? zVar.a() - 1 : 0;
        }
    }

    private int b(int i2, RecyclerView.v vVar, RecyclerView.z zVar, boolean z2) {
        int f2;
        int f3 = i2 - this.u.f();
        if (f3 <= 0) {
            return 0;
        }
        int i3 = -c(f3, vVar, zVar);
        int i4 = i2 + i3;
        if (!z2 || (f2 = i4 - this.u.f()) <= 0) {
            return i3;
        }
        this.u.a(-f2);
        return i3 - f2;
    }

    private void b(a aVar) {
        g(aVar.f1705b, aVar.f1706c);
    }

    public int b(int i2, RecyclerView.v vVar, RecyclerView.z zVar) {
        if (this.s == 0) {
            return 0;
        }
        return c(i2, vVar, zVar);
    }

    public int b(RecyclerView.z zVar) {
        return j(zVar);
    }

    private void b(RecyclerView.v vVar, int i2, int i3) {
        int e2 = e();
        if (i2 >= 0) {
            int a2 = (this.u.a() - i2) + i3;
            if (this.x) {
                for (int i4 = 0; i4 < e2; i4++) {
                    View c2 = c(i4);
                    if (this.u.d(c2) < a2 || this.u.f(c2) < a2) {
                        a(vVar, 0, i4);
                        return;
                    }
                }
                return;
            }
            int i5 = e2 - 1;
            for (int i6 = i5; i6 >= 0; i6--) {
                View c3 = c(i6);
                if (this.u.d(c3) < a2 || this.u.f(c3) < a2) {
                    a(vVar, i5, i6);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public View b(boolean z2, boolean z3) {
        if (this.x) {
            return a(e() - 1, -1, z2, z3);
        }
        return a(0, e(), z2, z3);
    }

    private int a(int i2, RecyclerView.v vVar, RecyclerView.z zVar, boolean z2) {
        int b2;
        int b3 = this.u.b() - i2;
        if (b3 <= 0) {
            return 0;
        }
        int i3 = -c(-b3, vVar, zVar);
        int i4 = i2 + i3;
        if (!z2 || (b2 = this.u.b() - i4) <= 0) {
            return i3;
        }
        this.u.a(b2);
        return b2 + i3;
    }

    private void a(a aVar) {
        f(aVar.f1705b, aVar.f1706c);
    }

    public int a(int i2, RecyclerView.v vVar, RecyclerView.z zVar) {
        if (this.s == 1) {
            return 0;
        }
        return c(i2, vVar, zVar);
    }

    public int a(RecyclerView.z zVar) {
        return i(zVar);
    }

    private void a(int i2, int i3, boolean z2, RecyclerView.z zVar) {
        int i4;
        this.t.m = K();
        this.t.f1718f = i2;
        int[] iArr = this.H;
        boolean z3 = false;
        iArr[0] = 0;
        iArr[1] = 0;
        a(zVar, iArr);
        int max = Math.max(0, this.H[0]);
        int max2 = Math.max(0, this.H[1]);
        if (i2 == 1) {
            z3 = true;
        }
        this.t.f1720h = z3 ? max2 : max;
        c cVar = this.t;
        if (!z3) {
            max = max2;
        }
        cVar.f1721i = max;
        int i5 = -1;
        if (z3) {
            this.t.f1720h += this.u.c();
            View P = P();
            c cVar2 = this.t;
            if (!this.x) {
                i5 = 1;
            }
            cVar2.f1717e = i5;
            c cVar3 = this.t;
            int l = l(P);
            c cVar4 = this.t;
            cVar3.f1716d = l + cVar4.f1717e;
            cVar4.f1714b = this.u.a(P);
            i4 = this.u.a(P) - this.u.b();
        } else {
            View Q = Q();
            this.t.f1720h += this.u.f();
            c cVar5 = this.t;
            if (this.x) {
                i5 = 1;
            }
            cVar5.f1717e = i5;
            c cVar6 = this.t;
            int l2 = l(Q);
            c cVar7 = this.t;
            cVar6.f1716d = l2 + cVar7.f1717e;
            cVar7.f1714b = this.u.d(Q);
            i4 = (-this.u.d(Q)) + this.u.f();
        }
        c cVar8 = this.t;
        cVar8.f1715c = i3;
        if (z2) {
            cVar8.f1715c -= i4;
        }
        this.t.f1719g = i4;
    }

    public int e(RecyclerView.z zVar) {
        return j(zVar);
    }

    /* access modifiers changed from: package-private */
    public View e(int i2, int i3) {
        int i4;
        int i5;
        E();
        if ((i3 > i2 ? 1 : i3 < i2 ? (char) 65535 : 0) == 0) {
            return c(i2);
        }
        if (this.u.d(c(i2)) < this.u.f()) {
            i5 = 16644;
            i4 = 16388;
        } else {
            i5 = 4161;
            i4 = 4097;
        }
        if (this.s == 0) {
            return this.f1761e.a(i2, i3, i5, i4);
        }
        return this.f1762f.a(i2, i3, i5, i4);
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.z zVar, c cVar, RecyclerView.o.c cVar2) {
        int i2 = cVar.f1716d;
        if (i2 >= 0 && i2 < zVar.a()) {
            cVar2.a(i2, Math.max(0, cVar.f1719g));
        }
    }

    public void a(int i2, RecyclerView.o.c cVar) {
        boolean z2;
        int i3;
        SavedState savedState = this.D;
        int i4 = -1;
        if (savedState == null || !savedState.a()) {
            R();
            z2 = this.x;
            i3 = this.A;
            if (i3 == -1) {
                i3 = z2 ? i2 - 1 : 0;
            }
        } else {
            SavedState savedState2 = this.D;
            z2 = savedState2.f1703c;
            i3 = savedState2.f1701a;
        }
        if (!z2) {
            i4 = 1;
        }
        int i5 = i3;
        for (int i6 = 0; i6 < this.G && i5 >= 0 && i5 < i2; i6++) {
            cVar.a(i5, 0);
            i5 += i4;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void
     arg types: [int, int, int, androidx.recyclerview.widget.RecyclerView$z]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet, int, int):androidx.recyclerview.widget.RecyclerView$o$d
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, androidx.recyclerview.widget.RecyclerView$p):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$z, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void */
    public void a(int i2, int i3, RecyclerView.z zVar, RecyclerView.o.c cVar) {
        if (this.s != 0) {
            i2 = i3;
        }
        if (e() != 0 && i2 != 0) {
            E();
            a(i2 > 0 ? 1 : -1, Math.abs(i2), true, zVar);
            a(zVar, this.t, cVar);
        }
    }

    public void a(String str) {
        if (this.D == null) {
            super.a(str);
        }
    }

    private void a(RecyclerView.v vVar, int i2, int i3) {
        if (i2 != i3) {
            if (i3 > i2) {
                for (int i4 = i3 - 1; i4 >= i2; i4--) {
                    a(i4, vVar);
                }
                return;
            }
            while (i2 > i3) {
                a(i2, vVar);
                i2--;
            }
        }
    }

    private void a(RecyclerView.v vVar, c cVar) {
        if (cVar.f1713a && !cVar.m) {
            int i2 = cVar.f1719g;
            int i3 = cVar.f1721i;
            if (cVar.f1718f == -1) {
                b(vVar, i2, i3);
            } else {
                c(vVar, i2, i3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int a(RecyclerView.v vVar, c cVar, RecyclerView.z zVar, boolean z2) {
        int i2 = cVar.f1715c;
        int i3 = cVar.f1719g;
        if (i3 != Integer.MIN_VALUE) {
            if (i2 < 0) {
                cVar.f1719g = i3 + i2;
            }
            a(vVar, cVar);
        }
        int i4 = cVar.f1715c + cVar.f1720h;
        b bVar = this.F;
        while (true) {
            if ((!cVar.m && i4 <= 0) || !cVar.a(zVar)) {
                break;
            }
            bVar.a();
            a(vVar, zVar, cVar, bVar);
            if (!bVar.f1710b) {
                cVar.f1714b += bVar.f1709a * cVar.f1718f;
                if (!bVar.f1711c || cVar.l != null || !zVar.d()) {
                    int i5 = cVar.f1715c;
                    int i6 = bVar.f1709a;
                    cVar.f1715c = i5 - i6;
                    i4 -= i6;
                }
                int i7 = cVar.f1719g;
                if (i7 != Integer.MIN_VALUE) {
                    cVar.f1719g = i7 + bVar.f1709a;
                    int i8 = cVar.f1715c;
                    if (i8 < 0) {
                        cVar.f1719g += i8;
                    }
                    a(vVar, cVar);
                }
                if (z2 && bVar.f1712d) {
                    break;
                }
            } else {
                break;
            }
        }
        return i2 - cVar.f1715c;
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.v vVar, RecyclerView.z zVar, c cVar, b bVar) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        View a2 = cVar.a(vVar);
        if (a2 == null) {
            bVar.f1710b = true;
            return;
        }
        RecyclerView.p pVar = (RecyclerView.p) a2.getLayoutParams();
        if (cVar.l == null) {
            if (this.x == (cVar.f1718f == -1)) {
                b(a2);
            } else {
                b(a2, 0);
            }
        } else {
            if (this.x == (cVar.f1718f == -1)) {
                a(a2);
            } else {
                a(a2, 0);
            }
        }
        a(a2, 0, 0);
        bVar.f1709a = this.u.b(a2);
        if (this.s == 1) {
            if (I()) {
                i6 = q() - o();
                i5 = i6 - this.u.c(a2);
            } else {
                i5 = n();
                i6 = this.u.c(a2) + i5;
            }
            if (cVar.f1718f == -1) {
                int i7 = cVar.f1714b;
                i2 = i7;
                i3 = i6;
                i4 = i7 - bVar.f1709a;
            } else {
                int i8 = cVar.f1714b;
                i4 = i8;
                i3 = i6;
                i2 = bVar.f1709a + i8;
            }
        } else {
            int p = p();
            int c2 = this.u.c(a2) + p;
            if (cVar.f1718f == -1) {
                int i9 = cVar.f1714b;
                i3 = i9;
                i4 = p;
                i2 = c2;
                i5 = i9 - bVar.f1709a;
            } else {
                int i10 = cVar.f1714b;
                i4 = p;
                i3 = bVar.f1709a + i10;
                i2 = c2;
                i5 = i10;
            }
        }
        a(a2, i5, i4, i3, i2);
        if (pVar.c() || pVar.b()) {
            bVar.f1711c = true;
        }
        bVar.f1712d = a2.hasFocusable();
    }

    /* access modifiers changed from: package-private */
    public View a(boolean z2, boolean z3) {
        if (this.x) {
            return a(0, e(), z2, z3);
        }
        return a(e() - 1, -1, z2, z3);
    }

    /* access modifiers changed from: package-private */
    public View a(RecyclerView.v vVar, RecyclerView.z zVar, int i2, int i3, int i4) {
        E();
        int f2 = this.u.f();
        int b2 = this.u.b();
        int i5 = i3 > i2 ? 1 : -1;
        View view = null;
        View view2 = null;
        while (i2 != i3) {
            View c2 = c(i2);
            int l = l(c2);
            if (l >= 0 && l < i4) {
                if (((RecyclerView.p) c2.getLayoutParams()).c()) {
                    if (view2 == null) {
                        view2 = c2;
                    }
                } else if (this.u.d(c2) < b2 && this.u.a(c2) >= f2) {
                    return c2;
                } else {
                    if (view == null) {
                        view = c2;
                    }
                }
            }
            i2 += i5;
        }
        return view != null ? view : view2;
    }

    /* access modifiers changed from: package-private */
    public View a(int i2, int i3, boolean z2, boolean z3) {
        E();
        int i4 = 320;
        int i5 = z2 ? 24579 : 320;
        if (!z3) {
            i4 = 0;
        }
        if (this.s == 0) {
            return this.f1761e.a(i2, i3, i5, i4);
        }
        return this.f1762f.a(i2, i3, i5, i4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void
     arg types: [int, int, int, androidx.recyclerview.widget.RecyclerView$z]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet, int, int):androidx.recyclerview.widget.RecyclerView$o$d
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, androidx.recyclerview.widget.RecyclerView$p):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$z, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int
     arg types: [androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet, int, int):androidx.recyclerview.widget.RecyclerView$o$d
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, androidx.recyclerview.widget.RecyclerView$p):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$z, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int */
    public View a(View view, int i2, RecyclerView.v vVar, RecyclerView.z zVar) {
        int h2;
        View view2;
        View view3;
        R();
        if (e() == 0 || (h2 = h(i2)) == Integer.MIN_VALUE) {
            return null;
        }
        E();
        a(h2, (int) (((float) this.u.g()) * 0.33333334f), false, zVar);
        c cVar = this.t;
        cVar.f1719g = Integer.MIN_VALUE;
        cVar.f1713a = false;
        a(vVar, cVar, zVar, true);
        if (h2 == -1) {
            view2 = O();
        } else {
            view2 = N();
        }
        if (h2 == -1) {
            view3 = Q();
        } else {
            view3 = P();
        }
        if (!view3.hasFocusable()) {
            return view2;
        }
        if (view2 == null) {
            return null;
        }
        return view3;
    }
}
