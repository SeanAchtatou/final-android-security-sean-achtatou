package androidx.recyclerview.widget;

import androidx.recyclerview.widget.RecyclerView;
import b.d.d;
import b.d.g;
import b.e.l.e;

/* compiled from: ViewInfoStore */
class n {

    /* renamed from: a  reason: collision with root package name */
    final g<RecyclerView.c0, a> f1953a = new g<>();

    /* renamed from: b  reason: collision with root package name */
    final d<RecyclerView.c0> f1954b = new d<>();

    /* compiled from: ViewInfoStore */
    interface b {
        void a(RecyclerView.c0 c0Var);

        void a(RecyclerView.c0 c0Var, RecyclerView.l.c cVar, RecyclerView.l.c cVar2);

        void b(RecyclerView.c0 c0Var, RecyclerView.l.c cVar, RecyclerView.l.c cVar2);

        void c(RecyclerView.c0 c0Var, RecyclerView.l.c cVar, RecyclerView.l.c cVar2);
    }

    n() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f1953a.clear();
        this.f1954b.clear();
    }

    /* access modifiers changed from: package-private */
    public boolean b(RecyclerView.c0 c0Var) {
        a aVar = this.f1953a.get(c0Var);
        if (aVar == null || (aVar.f1956a & 1) == 0) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void c(RecyclerView.c0 c0Var, RecyclerView.l.c cVar) {
        a aVar = this.f1953a.get(c0Var);
        if (aVar == null) {
            aVar = a.b();
            this.f1953a.put(c0Var, aVar);
        }
        aVar.f1957b = cVar;
        aVar.f1956a |= 4;
    }

    public void d(RecyclerView.c0 c0Var) {
        g(c0Var);
    }

    /* access modifiers changed from: package-private */
    public RecyclerView.l.c e(RecyclerView.c0 c0Var) {
        return a(c0Var, 8);
    }

    /* access modifiers changed from: package-private */
    public RecyclerView.l.c f(RecyclerView.c0 c0Var) {
        return a(c0Var, 4);
    }

    /* access modifiers changed from: package-private */
    public void g(RecyclerView.c0 c0Var) {
        a aVar = this.f1953a.get(c0Var);
        if (aVar != null) {
            aVar.f1956a &= -2;
        }
    }

    /* access modifiers changed from: package-private */
    public void h(RecyclerView.c0 c0Var) {
        int a2 = this.f1954b.a() - 1;
        while (true) {
            if (a2 < 0) {
                break;
            } else if (c0Var == this.f1954b.c(a2)) {
                this.f1954b.b(a2);
                break;
            } else {
                a2--;
            }
        }
        a remove = this.f1953a.remove(c0Var);
        if (remove != null) {
            a.a(remove);
        }
    }

    private RecyclerView.l.c a(RecyclerView.c0 c0Var, int i2) {
        a d2;
        RecyclerView.l.c cVar;
        int a2 = this.f1953a.a(c0Var);
        if (a2 >= 0 && (d2 = this.f1953a.d(a2)) != null) {
            int i3 = d2.f1956a;
            if ((i3 & i2) != 0) {
                d2.f1956a = (i2 ^ -1) & i3;
                if (i2 == 4) {
                    cVar = d2.f1957b;
                } else if (i2 == 8) {
                    cVar = d2.f1958c;
                } else {
                    throw new IllegalArgumentException("Must provide flag PRE or POST");
                }
                if ((d2.f1956a & 12) == 0) {
                    this.f1953a.c(a2);
                    a.a(d2);
                }
                return cVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void b(RecyclerView.c0 c0Var, RecyclerView.l.c cVar) {
        a aVar = this.f1953a.get(c0Var);
        if (aVar == null) {
            aVar = a.b();
            this.f1953a.put(c0Var, aVar);
        }
        aVar.f1958c = cVar;
        aVar.f1956a |= 8;
    }

    /* compiled from: ViewInfoStore */
    static class a {

        /* renamed from: d  reason: collision with root package name */
        static b.e.l.d<a> f1955d = new e(20);

        /* renamed from: a  reason: collision with root package name */
        int f1956a;

        /* renamed from: b  reason: collision with root package name */
        RecyclerView.l.c f1957b;

        /* renamed from: c  reason: collision with root package name */
        RecyclerView.l.c f1958c;

        private a() {
        }

        static void a(a aVar) {
            aVar.f1956a = 0;
            aVar.f1957b = null;
            aVar.f1958c = null;
            f1955d.a(aVar);
        }

        static a b() {
            a a2 = f1955d.a();
            return a2 == null ? new a() : a2;
        }

        static void a() {
            do {
            } while (f1955d.a() != null);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c(RecyclerView.c0 c0Var) {
        a aVar = this.f1953a.get(c0Var);
        return (aVar == null || (aVar.f1956a & 4) == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a.a();
    }

    /* access modifiers changed from: package-private */
    public void a(long j2, RecyclerView.c0 c0Var) {
        this.f1954b.c(j2, c0Var);
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.c0 c0Var, RecyclerView.l.c cVar) {
        a aVar = this.f1953a.get(c0Var);
        if (aVar == null) {
            aVar = a.b();
            this.f1953a.put(c0Var, aVar);
        }
        aVar.f1956a |= 2;
        aVar.f1957b = cVar;
    }

    /* access modifiers changed from: package-private */
    public RecyclerView.c0 a(long j2) {
        return this.f1954b.b(j2);
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.c0 c0Var) {
        a aVar = this.f1953a.get(c0Var);
        if (aVar == null) {
            aVar = a.b();
            this.f1953a.put(c0Var, aVar);
        }
        aVar.f1956a |= 1;
    }

    /* access modifiers changed from: package-private */
    public void a(b bVar) {
        for (int size = this.f1953a.size() - 1; size >= 0; size--) {
            RecyclerView.c0 b2 = this.f1953a.b(size);
            a c2 = this.f1953a.c(size);
            int i2 = c2.f1956a;
            if ((i2 & 3) == 3) {
                bVar.a(b2);
            } else if ((i2 & 1) != 0) {
                RecyclerView.l.c cVar = c2.f1957b;
                if (cVar == null) {
                    bVar.a(b2);
                } else {
                    bVar.b(b2, cVar, c2.f1958c);
                }
            } else if ((i2 & 14) == 14) {
                bVar.a(b2, c2.f1957b, c2.f1958c);
            } else if ((i2 & 12) == 12) {
                bVar.c(b2, c2.f1957b, c2.f1958c);
            } else if ((i2 & 4) != 0) {
                bVar.b(b2, c2.f1957b, null);
            } else if ((i2 & 8) != 0) {
                bVar.a(b2, c2.f1957b, c2.f1958c);
            }
            a.a(c2);
        }
    }
}
