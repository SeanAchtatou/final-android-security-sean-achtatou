package androidx.recyclerview.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.e.m.d0.c;
import com.esotericsoftware.spine.Animation;
import java.util.Arrays;

public class GridLayoutManager extends LinearLayoutManager {
    boolean I = false;
    int J = -1;
    int[] K;
    View[] L;
    final SparseIntArray M = new SparseIntArray();
    final SparseIntArray N = new SparseIntArray();
    c O = new a();
    final Rect P = new Rect();
    private boolean Q;

    public static final class a extends c {
        public int a(int i2) {
            return 1;
        }

        public int d(int i2, int i3) {
            return i2 % i3;
        }
    }

    public static abstract class c {

        /* renamed from: a  reason: collision with root package name */
        final SparseIntArray f1697a = new SparseIntArray();

        /* renamed from: b  reason: collision with root package name */
        final SparseIntArray f1698b = new SparseIntArray();

        /* renamed from: c  reason: collision with root package name */
        private boolean f1699c = false;

        /* renamed from: d  reason: collision with root package name */
        private boolean f1700d = false;

        public abstract int a(int i2);

        public void a() {
            this.f1698b.clear();
        }

        public void b() {
            this.f1697a.clear();
        }

        public int c(int i2, int i3) {
            int i4;
            int i5;
            int i6;
            int a2;
            if (!this.f1700d || (a2 = a(this.f1698b, i2)) == -1) {
                i6 = 0;
                i5 = 0;
                i4 = 0;
            } else {
                i5 = this.f1698b.get(a2);
                i4 = a2 + 1;
                i6 = a(a2) + b(a2, i3);
                if (i6 == i3) {
                    i5++;
                    i6 = 0;
                }
            }
            int a3 = a(i2);
            while (i4 < i2) {
                int a4 = a(i4);
                int i7 = i6 + a4;
                if (i7 == i3) {
                    i5++;
                    i7 = 0;
                } else if (i7 > i3) {
                    i5++;
                    i7 = a4;
                }
                i4++;
            }
            return i6 + a3 > i3 ? i5 + 1 : i5;
        }

        public abstract int d(int i2, int i3);

        /* access modifiers changed from: package-private */
        public int a(int i2, int i3) {
            if (!this.f1700d) {
                return c(i2, i3);
            }
            int i4 = this.f1698b.get(i2, -1);
            if (i4 != -1) {
                return i4;
            }
            int c2 = c(i2, i3);
            this.f1698b.put(i2, c2);
            return c2;
        }

        /* access modifiers changed from: package-private */
        public int b(int i2, int i3) {
            if (!this.f1699c) {
                return d(i2, i3);
            }
            int i4 = this.f1697a.get(i2, -1);
            if (i4 != -1) {
                return i4;
            }
            int d2 = d(i2, i3);
            this.f1697a.put(i2, d2);
            return d2;
        }

        static int a(SparseIntArray sparseIntArray, int i2) {
            int size = sparseIntArray.size() - 1;
            int i3 = 0;
            while (i3 <= size) {
                int i4 = (i3 + size) >>> 1;
                if (sparseIntArray.keyAt(i4) < i2) {
                    i3 = i4 + 1;
                } else {
                    size = i4 - 1;
                }
            }
            int i5 = i3 - 1;
            if (i5 < 0 || i5 >= sparseIntArray.size()) {
                return -1;
            }
            return sparseIntArray.keyAt(i5);
        }
    }

    public GridLayoutManager(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        j(RecyclerView.o.a(context, attributeSet, i2, i3).f1771b);
    }

    private void L() {
        int e2 = e();
        for (int i2 = 0; i2 < e2; i2++) {
            b bVar = (b) c(i2).getLayoutParams();
            int a2 = bVar.a();
            this.M.put(a2, bVar.f());
            this.N.put(a2, bVar.e());
        }
    }

    private void M() {
        this.M.clear();
        this.N.clear();
    }

    private void N() {
        View[] viewArr = this.L;
        if (viewArr == null || viewArr.length != this.J) {
            this.L = new View[this.J];
        }
    }

    private void O() {
        int i2;
        int i3;
        if (H() == 1) {
            i3 = q() - o();
            i2 = n();
        } else {
            i3 = h() - m();
            i2 = p();
        }
        k(i3 - i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.b(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      androidx.recyclerview.widget.GridLayoutManager.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.LinearLayoutManager.b(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.b(int, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(android.view.View, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(android.view.View, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.LinearLayoutManager.b(boolean, boolean):android.view.View */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      androidx.recyclerview.widget.GridLayoutManager.a(float, int):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.GridLayoutManager.a(android.content.Context, android.util.AttributeSet):androidx.recyclerview.widget.RecyclerView$p
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, int[]):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, android.view.View):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet):androidx.recyclerview.widget.RecyclerView$p
      androidx.recyclerview.widget.RecyclerView.o.a(int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$g, androidx.recyclerview.widget.RecyclerView$g):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, android.os.Bundle):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(boolean, boolean):android.view.View */
    private int i(RecyclerView.z zVar) {
        int i2;
        if (!(e() == 0 || zVar.a() == 0)) {
            E();
            boolean J2 = J();
            View b2 = b(!J2, true);
            View a2 = a(!J2, true);
            if (!(b2 == null || a2 == null)) {
                int a3 = this.O.a(l(b2), this.J);
                int a4 = this.O.a(l(a2), this.J);
                int min = Math.min(a3, a4);
                int max = Math.max(a3, a4);
                int a5 = this.O.a(zVar.a() - 1, this.J) + 1;
                if (this.x) {
                    i2 = Math.max(0, (a5 - max) - 1);
                } else {
                    i2 = Math.max(0, min);
                }
                if (!J2) {
                    return i2;
                }
                return Math.round((((float) i2) * (((float) Math.abs(this.u.a(a2) - this.u.d(b2))) / ((float) ((this.O.a(l(a2), this.J) - this.O.a(l(b2), this.J)) + 1)))) + ((float) (this.u.f() - this.u.d(b2))));
            }
        }
        return 0;
    }

    private void k(int i2) {
        this.K = a(this.K, this.J, i2);
    }

    public boolean C() {
        return this.D == null && !this.I;
    }

    public int a(RecyclerView.v vVar, RecyclerView.z zVar) {
        if (this.s == 1) {
            return this.J;
        }
        if (zVar.a() < 1) {
            return 0;
        }
        return a(vVar, zVar, zVar.a() - 1) + 1;
    }

    public void b(boolean z) {
        if (!z) {
            super.b(false);
            return;
        }
        throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
    }

    public RecyclerView.p c() {
        if (this.s == 0) {
            return new b(-2, -1);
        }
        return new b(-1, -2);
    }

    public void d(RecyclerView recyclerView) {
        this.O.b();
        this.O.a();
    }

    public void e(RecyclerView.v vVar, RecyclerView.z zVar) {
        if (zVar.d()) {
            L();
        }
        super.e(vVar, zVar);
        M();
    }

    /* access modifiers changed from: package-private */
    public int f(int i2, int i3) {
        if (this.s != 1 || !I()) {
            int[] iArr = this.K;
            return iArr[i3 + i2] - iArr[i2];
        }
        int[] iArr2 = this.K;
        int i4 = this.J;
        return iArr2[i4 - i2] - iArr2[(i4 - i2) - i3];
    }

    public void g(RecyclerView.z zVar) {
        super.g(zVar);
        this.I = false;
    }

    public void j(int i2) {
        if (i2 != this.J) {
            this.I = true;
            if (i2 >= 1) {
                this.J = i2;
                this.O.b();
                y();
                return;
            }
            throw new IllegalArgumentException("Span count should be at least 1. Provided " + i2);
        }
    }

    public static class b extends RecyclerView.p {

        /* renamed from: e  reason: collision with root package name */
        int f1695e = -1;

        /* renamed from: f  reason: collision with root package name */
        int f1696f = 0;

        public b(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public int e() {
            return this.f1695e;
        }

        public int f() {
            return this.f1696f;
        }

        public b(int i2, int i3) {
            super(i2, i3);
        }

        public b(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public b(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    public int b(RecyclerView.v vVar, RecyclerView.z zVar) {
        if (this.s == 0) {
            return this.J;
        }
        if (zVar.a() < 1) {
            return 0;
        }
        return a(vVar, zVar, zVar.a() - 1) + 1;
    }

    private int c(RecyclerView.v vVar, RecyclerView.z zVar, int i2) {
        if (!zVar.d()) {
            return this.O.a(i2);
        }
        int i3 = this.M.get(i2, -1);
        if (i3 != -1) {
            return i3;
        }
        int a2 = vVar.a(i2);
        if (a2 != -1) {
            return this.O.a(a2);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i2);
        return 1;
    }

    public int f(RecyclerView.z zVar) {
        if (this.Q) {
            return j(zVar);
        }
        return super.f(zVar);
    }

    public void a(RecyclerView.v vVar, RecyclerView.z zVar, View view, b.e.m.d0.c cVar) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof b)) {
            super.a(view, cVar);
            return;
        }
        b bVar = (b) layoutParams;
        int a2 = a(vVar, zVar, bVar.a());
        if (this.s == 0) {
            cVar.b(c.C0053c.a(bVar.e(), bVar.f(), a2, 1, false, false));
            return;
        }
        cVar.b(c.C0053c.a(a2, 1, bVar.e(), bVar.f(), false, false));
    }

    public int e(RecyclerView.z zVar) {
        if (this.Q) {
            return i(zVar);
        }
        return super.e(zVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.b(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      androidx.recyclerview.widget.GridLayoutManager.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.LinearLayoutManager.b(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.b(int, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(android.view.View, int):void
      androidx.recyclerview.widget.RecyclerView.o.b(android.view.View, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.LinearLayoutManager.b(boolean, boolean):android.view.View */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.LinearLayoutManager.a(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      androidx.recyclerview.widget.GridLayoutManager.a(float, int):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.GridLayoutManager.a(android.content.Context, android.util.AttributeSet):androidx.recyclerview.widget.RecyclerView$p
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, int[]):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, android.view.View):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet):androidx.recyclerview.widget.RecyclerView$p
      androidx.recyclerview.widget.RecyclerView.o.a(int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$g, androidx.recyclerview.widget.RecyclerView$g):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$v):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, android.os.Bundle):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(boolean, boolean):android.view.View */
    private int j(RecyclerView.z zVar) {
        if (!(e() == 0 || zVar.a() == 0)) {
            E();
            View b2 = b(!J(), true);
            View a2 = a(!J(), true);
            if (!(b2 == null || a2 == null)) {
                if (!J()) {
                    return this.O.a(zVar.a() - 1, this.J) + 1;
                }
                int a3 = this.u.a(a2) - this.u.d(b2);
                int a4 = this.O.a(l(b2), this.J);
                return (int) ((((float) a3) / ((float) ((this.O.a(l(a2), this.J) - a4) + 1))) * ((float) (this.O.a(zVar.a() - 1, this.J) + 1)));
            }
        }
        return 0;
    }

    public void b(RecyclerView recyclerView, int i2, int i3) {
        this.O.b();
        this.O.a();
    }

    public int b(int i2, RecyclerView.v vVar, RecyclerView.z zVar) {
        O();
        N();
        return super.b(i2, vVar, zVar);
    }

    public int c(RecyclerView.z zVar) {
        if (this.Q) {
            return j(zVar);
        }
        return super.c(zVar);
    }

    private void b(RecyclerView.v vVar, RecyclerView.z zVar, LinearLayoutManager.a aVar, int i2) {
        boolean z = i2 == 1;
        int b2 = b(vVar, zVar, aVar.f1705b);
        if (z) {
            while (b2 > 0) {
                int i3 = aVar.f1705b;
                if (i3 > 0) {
                    aVar.f1705b = i3 - 1;
                    b2 = b(vVar, zVar, aVar.f1705b);
                } else {
                    return;
                }
            }
            return;
        }
        int a2 = zVar.a() - 1;
        int i4 = aVar.f1705b;
        while (i4 < a2) {
            int i5 = i4 + 1;
            int b3 = b(vVar, zVar, i5);
            if (b3 <= b2) {
                break;
            }
            i4 = i5;
            b2 = b3;
        }
        aVar.f1705b = i4;
    }

    public void a(RecyclerView recyclerView, int i2, int i3) {
        this.O.b();
        this.O.a();
    }

    public void a(RecyclerView recyclerView, int i2, int i3, Object obj) {
        this.O.b();
        this.O.a();
    }

    public void a(RecyclerView recyclerView, int i2, int i3, int i4) {
        this.O.b();
        this.O.a();
    }

    private int b(RecyclerView.v vVar, RecyclerView.z zVar, int i2) {
        if (!zVar.d()) {
            return this.O.b(i2, this.J);
        }
        int i3 = this.N.get(i2, -1);
        if (i3 != -1) {
            return i3;
        }
        int a2 = vVar.a(i2);
        if (a2 != -1) {
            return this.O.b(a2, this.J);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i2);
        return 0;
    }

    public RecyclerView.p a(Context context, AttributeSet attributeSet) {
        return new b(context, attributeSet);
    }

    public RecyclerView.p a(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new b((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new b(layoutParams);
    }

    public boolean a(RecyclerView.p pVar) {
        return pVar instanceof b;
    }

    public void a(Rect rect, int i2, int i3) {
        int i4;
        int i5;
        if (this.K == null) {
            super.a(rect, i2, i3);
        }
        int n = n() + o();
        int p = p() + m();
        if (this.s == 1) {
            i5 = RecyclerView.o.a(i3, rect.height() + p, k());
            int[] iArr = this.K;
            i4 = RecyclerView.o.a(i2, iArr[iArr.length - 1] + n, l());
        } else {
            i4 = RecyclerView.o.a(i2, rect.width() + n, l());
            int[] iArr2 = this.K;
            i5 = RecyclerView.o.a(i3, iArr2[iArr2.length - 1] + p, k());
        }
        c(i4, i5);
    }

    public int b(RecyclerView.z zVar) {
        if (this.Q) {
            return i(zVar);
        }
        return super.b(zVar);
    }

    static int[] a(int[] iArr, int i2, int i3) {
        int i4;
        if (!(iArr != null && iArr.length == i2 + 1 && iArr[iArr.length - 1] == i3)) {
            iArr = new int[(i2 + 1)];
        }
        int i5 = 0;
        iArr[0] = 0;
        int i6 = i3 / i2;
        int i7 = i3 % i2;
        int i8 = 0;
        for (int i9 = 1; i9 <= i2; i9++) {
            i5 += i7;
            if (i5 <= 0 || i2 - i5 >= i7) {
                i4 = i6;
            } else {
                i4 = i6 + 1;
                i5 -= i2;
            }
            i8 += i4;
            iArr[i9] = i8;
        }
        return iArr;
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.v vVar, RecyclerView.z zVar, LinearLayoutManager.a aVar, int i2) {
        super.a(vVar, zVar, aVar, i2);
        O();
        if (zVar.a() > 0 && !zVar.d()) {
            b(vVar, zVar, aVar, i2);
        }
        N();
    }

    public int a(int i2, RecyclerView.v vVar, RecyclerView.z zVar) {
        O();
        N();
        return super.a(i2, vVar, zVar);
    }

    /* access modifiers changed from: package-private */
    public View a(RecyclerView.v vVar, RecyclerView.z zVar, int i2, int i3, int i4) {
        E();
        int f2 = this.u.f();
        int b2 = this.u.b();
        int i5 = i3 > i2 ? 1 : -1;
        View view = null;
        View view2 = null;
        while (i2 != i3) {
            View c2 = c(i2);
            int l = l(c2);
            if (l >= 0 && l < i4 && b(vVar, zVar, l) == 0) {
                if (((RecyclerView.p) c2.getLayoutParams()).c()) {
                    if (view2 == null) {
                        view2 = c2;
                    }
                } else if (this.u.d(c2) < b2 && this.u.a(c2) >= f2) {
                    return c2;
                } else {
                    if (view == null) {
                        view = c2;
                    }
                }
            }
            i2 += i5;
        }
        return view != null ? view : view2;
    }

    private int a(RecyclerView.v vVar, RecyclerView.z zVar, int i2) {
        if (!zVar.d()) {
            return this.O.a(i2, this.J);
        }
        int a2 = vVar.a(i2);
        if (a2 != -1) {
            return this.O.a(a2, this.J);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. " + i2);
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.z zVar, LinearLayoutManager.c cVar, RecyclerView.o.c cVar2) {
        int i2 = this.J;
        for (int i3 = 0; i3 < this.J && cVar.a(zVar) && i2 > 0; i3++) {
            int i4 = cVar.f1716d;
            cVar2.a(i4, Math.max(0, cVar.f1719g));
            i2 -= this.O.a(i4);
            cVar.f1716d += cVar.f1717e;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.GridLayoutManager.a(android.view.View, int, boolean):void
     arg types: [android.view.View, int, int]
     candidates:
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int):int
      androidx.recyclerview.widget.GridLayoutManager.a(int[], int, int):int[]
      androidx.recyclerview.widget.GridLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.GridLayoutManager.a(android.graphics.Rect, int, int):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, int, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, int, android.view.View):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$p):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.accessibility.AccessibilityEvent):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.GridLayoutManager.a(android.view.View, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.GridLayoutManager.f(int, int):int
     arg types: [int, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.f(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.f(int, int):void
      androidx.recyclerview.widget.GridLayoutManager.f(int, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.o.a(int, int, int, int, boolean):int
     arg types: [int, int, int, int, int]
     candidates:
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.GridLayoutManager.a(android.view.View, int, int, boolean):void
     arg types: [android.view.View, int, int, int]
     candidates:
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, boolean):void
      androidx.recyclerview.widget.GridLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$z):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$z, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet, int, int):androidx.recyclerview.widget.RecyclerView$o$d
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, androidx.recyclerview.widget.RecyclerView$p):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$z, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      androidx.recyclerview.widget.GridLayoutManager.a(android.view.View, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(RecyclerView.v vVar, RecyclerView.z zVar, LinearLayoutManager.c cVar, LinearLayoutManager.b bVar) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        View a2;
        RecyclerView.v vVar2 = vVar;
        RecyclerView.z zVar2 = zVar;
        LinearLayoutManager.c cVar2 = cVar;
        LinearLayoutManager.b bVar2 = bVar;
        int e2 = this.u.e();
        boolean z = false;
        boolean z2 = e2 != 1073741824;
        int i12 = e() > 0 ? this.K[this.J] : 0;
        if (z2) {
            O();
        }
        boolean z3 = cVar2.f1717e == 1;
        int i13 = this.J;
        if (!z3) {
            i13 = b(vVar2, zVar2, cVar2.f1716d) + c(vVar2, zVar2, cVar2.f1716d);
        }
        int i14 = i13;
        int i15 = 0;
        while (i15 < this.J && cVar2.a(zVar2) && i14 > 0) {
            int i16 = cVar2.f1716d;
            int c2 = c(vVar2, zVar2, i16);
            if (c2 <= this.J) {
                i14 -= c2;
                if (i14 < 0 || (a2 = cVar2.a(vVar2)) == null) {
                    break;
                }
                this.L[i15] = a2;
                i15++;
            } else {
                throw new IllegalArgumentException("Item at position " + i16 + " requires " + c2 + " spans but GridLayoutManager has only " + this.J + " spans.");
            }
        }
        if (i15 == 0) {
            bVar2.f1710b = true;
            return;
        }
        float f2 = Animation.CurveTimeline.LINEAR;
        a(vVar2, zVar2, i15, z3);
        int i17 = 0;
        int i18 = 0;
        while (i17 < i15) {
            View view = this.L[i17];
            if (cVar2.l == null) {
                if (z3) {
                    b(view);
                } else {
                    b(view, z);
                }
            } else if (z3) {
                a(view);
            } else {
                a(view, z ? 1 : 0);
            }
            a(view, this.P);
            a(view, e2, z);
            int b2 = this.u.b(view);
            if (b2 > i18) {
                i18 = b2;
            }
            float c3 = (((float) this.u.c(view)) * 1.0f) / ((float) ((b) view.getLayoutParams()).f1696f);
            if (c3 > f2) {
                f2 = c3;
            }
            i17++;
            z = false;
        }
        if (z2) {
            a(f2, i12);
            i18 = 0;
            for (int i19 = 0; i19 < i15; i19++) {
                View view2 = this.L[i19];
                a(view2, 1073741824, true);
                int b3 = this.u.b(view2);
                if (b3 > i18) {
                    i18 = b3;
                }
            }
        }
        for (int i20 = 0; i20 < i15; i20++) {
            View view3 = this.L[i20];
            if (this.u.b(view3) != i18) {
                b bVar3 = (b) view3.getLayoutParams();
                Rect rect = bVar3.f1775b;
                int i21 = rect.top + rect.bottom + bVar3.topMargin + bVar3.bottomMargin;
                int i22 = rect.left + rect.right + bVar3.leftMargin + bVar3.rightMargin;
                int f3 = f(bVar3.f1695e, bVar3.f1696f);
                if (this.s == 1) {
                    i11 = RecyclerView.o.a(f3, 1073741824, i22, bVar3.width, false);
                    i10 = View.MeasureSpec.makeMeasureSpec(i18 - i21, 1073741824);
                } else {
                    int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i18 - i22, 1073741824);
                    i10 = RecyclerView.o.a(f3, 1073741824, i21, bVar3.height, false);
                    i11 = makeMeasureSpec;
                }
                a(view3, i11, i10, true);
            }
        }
        int i23 = 0;
        bVar2.f1709a = i18;
        if (this.s == 1) {
            if (cVar2.f1718f == -1) {
                int i24 = cVar2.f1714b;
                i4 = i24 - i18;
                i3 = i24;
            } else {
                int i25 = cVar2.f1714b;
                i3 = i25 + i18;
                i4 = i25;
            }
            i5 = 0;
            i2 = 0;
        } else if (cVar2.f1718f == -1) {
            i2 = cVar2.f1714b;
            i5 = i2 - i18;
            i4 = 0;
            i3 = 0;
        } else {
            int i26 = cVar2.f1714b;
            int i27 = i26 + i18;
            i4 = 0;
            i3 = 0;
            int i28 = i26;
            i2 = i27;
            i5 = i28;
        }
        while (i23 < i15) {
            View view4 = this.L[i23];
            b bVar4 = (b) view4.getLayoutParams();
            if (this.s != 1) {
                int p = p() + this.K[bVar4.f1695e];
                i9 = i5;
                i8 = p;
                i6 = this.u.c(view4) + p;
                i7 = i2;
            } else if (I()) {
                int n = n() + this.K[this.J - bVar4.f1695e];
                i7 = n;
                i8 = i4;
                i6 = i3;
                i9 = n - this.u.c(view4);
            } else {
                int n2 = n() + this.K[bVar4.f1695e];
                i9 = n2;
                i8 = i4;
                i6 = i3;
                i7 = this.u.c(view4) + n2;
            }
            a(view4, i9, i8, i7, i6);
            if (bVar4.c() || bVar4.b()) {
                bVar2.f1711c = true;
            }
            bVar2.f1712d |= view4.hasFocusable();
            i23++;
            i5 = i9;
            i4 = i8;
            i2 = i7;
            i3 = i6;
        }
        Arrays.fill(this.L, (Object) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.GridLayoutManager.f(int, int):int
     arg types: [int, int]
     candidates:
      androidx.recyclerview.widget.LinearLayoutManager.f(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.f(int, int):void
      androidx.recyclerview.widget.GridLayoutManager.f(int, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.o.a(int, int, int, int, boolean):int
     arg types: [int, int, int, int, int]
     candidates:
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.View, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int, int, boolean):int */
    private void a(View view, int i2, boolean z) {
        int i3;
        int i4;
        b bVar = (b) view.getLayoutParams();
        Rect rect = bVar.f1775b;
        int i5 = rect.top + rect.bottom + bVar.topMargin + bVar.bottomMargin;
        int i6 = rect.left + rect.right + bVar.leftMargin + bVar.rightMargin;
        int f2 = f(bVar.f1695e, bVar.f1696f);
        if (this.s == 1) {
            i3 = RecyclerView.o.a(f2, i2, i6, bVar.width, false);
            i4 = RecyclerView.o.a(this.u.g(), i(), i5, bVar.height, true);
        } else {
            int a2 = RecyclerView.o.a(f2, i2, i5, bVar.height, false);
            int a3 = RecyclerView.o.a(this.u.g(), r(), i6, bVar.width, true);
            i4 = a2;
            i3 = a3;
        }
        a(view, i3, i4, z);
    }

    private void a(float f2, int i2) {
        k(Math.max(Math.round(f2 * ((float) this.J)), i2));
    }

    private void a(View view, int i2, int i3, boolean z) {
        boolean z2;
        RecyclerView.p pVar = (RecyclerView.p) view.getLayoutParams();
        if (z) {
            z2 = b(view, i2, i3, pVar);
        } else {
            z2 = a(view, i2, i3, pVar);
        }
        if (z2) {
            view.measure(i2, i3);
        }
    }

    private void a(RecyclerView.v vVar, RecyclerView.z zVar, int i2, boolean z) {
        int i3;
        int i4;
        int i5 = 0;
        int i6 = -1;
        if (z) {
            i6 = i2;
            i4 = 0;
            i3 = 1;
        } else {
            i4 = i2 - 1;
            i3 = -1;
        }
        while (i4 != i6) {
            View view = this.L[i4];
            b bVar = (b) view.getLayoutParams();
            bVar.f1696f = c(vVar, zVar, l(view));
            bVar.f1695e = i5;
            i5 += bVar.f1696f;
            i4 += i3;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, boolean):boolean
     arg types: [android.view.View, int, int]
     candidates:
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, int):int
      androidx.recyclerview.widget.GridLayoutManager.a(android.view.View, int, boolean):void
      androidx.recyclerview.widget.GridLayoutManager.a(int[], int, int):int[]
      androidx.recyclerview.widget.GridLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.GridLayoutManager.a(android.graphics.Rect, int, int):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, int, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$a):boolean
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, int, android.view.View):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$p):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, android.view.accessibility.AccessibilityEvent):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z, b.e.m.d0.c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, android.os.Bundle):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.view.View):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, boolean):boolean */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00d7, code lost:
        if (r13 == (r2 > r8)) goto L_0x00cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00f7, code lost:
        if (r13 == r11) goto L_0x00b7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0105  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View a(android.view.View r24, int r25, androidx.recyclerview.widget.RecyclerView.v r26, androidx.recyclerview.widget.RecyclerView.z r27) {
        /*
            r23 = this;
            r0 = r23
            r1 = r26
            r2 = r27
            android.view.View r3 = r23.c(r24)
            r4 = 0
            if (r3 != 0) goto L_0x000e
            return r4
        L_0x000e:
            android.view.ViewGroup$LayoutParams r5 = r3.getLayoutParams()
            androidx.recyclerview.widget.GridLayoutManager$b r5 = (androidx.recyclerview.widget.GridLayoutManager.b) r5
            int r6 = r5.f1695e
            int r5 = r5.f1696f
            int r5 = r5 + r6
            android.view.View r7 = super.a(r24, r25, r26, r27)
            if (r7 != 0) goto L_0x0020
            return r4
        L_0x0020:
            r7 = r25
            int r7 = r0.h(r7)
            r9 = 1
            if (r7 != r9) goto L_0x002b
            r7 = 1
            goto L_0x002c
        L_0x002b:
            r7 = 0
        L_0x002c:
            boolean r10 = r0.x
            if (r7 == r10) goto L_0x0032
            r7 = 1
            goto L_0x0033
        L_0x0032:
            r7 = 0
        L_0x0033:
            r10 = -1
            if (r7 == 0) goto L_0x003e
            int r7 = r23.e()
            int r7 = r7 - r9
            r11 = -1
            r12 = -1
            goto L_0x0045
        L_0x003e:
            int r7 = r23.e()
            r11 = r7
            r7 = 0
            r12 = 1
        L_0x0045:
            int r13 = r0.s
            if (r13 != r9) goto L_0x0051
            boolean r13 = r23.I()
            if (r13 == 0) goto L_0x0051
            r13 = 1
            goto L_0x0052
        L_0x0051:
            r13 = 0
        L_0x0052:
            int r14 = r0.a(r1, r2, r7)
            r10 = r4
            r8 = -1
            r15 = 0
            r16 = 0
            r17 = -1
        L_0x005d:
            if (r7 == r11) goto L_0x0147
            int r9 = r0.a(r1, r2, r7)
            android.view.View r1 = r0.c(r7)
            if (r1 != r3) goto L_0x006b
            goto L_0x0147
        L_0x006b:
            boolean r18 = r1.hasFocusable()
            if (r18 == 0) goto L_0x0085
            if (r9 == r14) goto L_0x0085
            if (r4 == 0) goto L_0x0077
            goto L_0x0147
        L_0x0077:
            r18 = r3
            r19 = r8
            r21 = r10
            r20 = r11
            r8 = r16
            r10 = r17
            goto L_0x0133
        L_0x0085:
            android.view.ViewGroup$LayoutParams r9 = r1.getLayoutParams()
            androidx.recyclerview.widget.GridLayoutManager$b r9 = (androidx.recyclerview.widget.GridLayoutManager.b) r9
            int r2 = r9.f1695e
            r18 = r3
            int r3 = r9.f1696f
            int r3 = r3 + r2
            boolean r19 = r1.hasFocusable()
            if (r19 == 0) goto L_0x009d
            if (r2 != r6) goto L_0x009d
            if (r3 != r5) goto L_0x009d
            return r1
        L_0x009d:
            boolean r19 = r1.hasFocusable()
            if (r19 == 0) goto L_0x00a5
            if (r4 == 0) goto L_0x00ad
        L_0x00a5:
            boolean r19 = r1.hasFocusable()
            if (r19 != 0) goto L_0x00b9
            if (r10 != 0) goto L_0x00b9
        L_0x00ad:
            r19 = r8
            r21 = r10
        L_0x00b1:
            r20 = r11
            r8 = r16
            r10 = r17
        L_0x00b7:
            r11 = 1
            goto L_0x0103
        L_0x00b9:
            int r19 = java.lang.Math.max(r2, r6)
            int r20 = java.lang.Math.min(r3, r5)
            r21 = r10
            int r10 = r20 - r19
            boolean r19 = r1.hasFocusable()
            if (r19 == 0) goto L_0x00da
            if (r10 <= r15) goto L_0x00d0
        L_0x00cd:
            r19 = r8
            goto L_0x00b1
        L_0x00d0:
            if (r10 != r15) goto L_0x00fa
            if (r2 <= r8) goto L_0x00d6
            r10 = 1
            goto L_0x00d7
        L_0x00d6:
            r10 = 0
        L_0x00d7:
            if (r13 != r10) goto L_0x00fa
            goto L_0x00cd
        L_0x00da:
            if (r4 != 0) goto L_0x00fa
            r19 = r8
            r20 = r11
            r8 = 0
            r11 = 1
            boolean r22 = r0.a(r1, r8, r11)
            r8 = r16
            if (r22 == 0) goto L_0x0100
            if (r10 <= r8) goto L_0x00ef
            r10 = r17
            goto L_0x0103
        L_0x00ef:
            if (r10 != r8) goto L_0x0100
            r10 = r17
            if (r2 <= r10) goto L_0x00f6
            goto L_0x00f7
        L_0x00f6:
            r11 = 0
        L_0x00f7:
            if (r13 != r11) goto L_0x0102
            goto L_0x00b7
        L_0x00fa:
            r19 = r8
            r20 = r11
            r8 = r16
        L_0x0100:
            r10 = r17
        L_0x0102:
            r11 = 0
        L_0x0103:
            if (r11 == 0) goto L_0x0133
            boolean r11 = r1.hasFocusable()
            if (r11 == 0) goto L_0x0120
            int r4 = r9.f1695e
            int r3 = java.lang.Math.min(r3, r5)
            int r2 = java.lang.Math.max(r2, r6)
            int r3 = r3 - r2
            r15 = r3
            r16 = r8
            r17 = r10
            r10 = r21
            r8 = r4
            r4 = r1
            goto L_0x013b
        L_0x0120:
            int r8 = r9.f1695e
            int r3 = java.lang.Math.min(r3, r5)
            int r2 = java.lang.Math.max(r2, r6)
            int r3 = r3 - r2
            r10 = r1
            r16 = r3
            r17 = r8
            r8 = r19
            goto L_0x013b
        L_0x0133:
            r16 = r8
            r17 = r10
            r8 = r19
            r10 = r21
        L_0x013b:
            int r7 = r7 + r12
            r1 = r26
            r2 = r27
            r3 = r18
            r11 = r20
            r9 = 1
            goto L_0x005d
        L_0x0147:
            r21 = r10
            if (r4 == 0) goto L_0x014c
            goto L_0x014e
        L_0x014c:
            r4 = r21
        L_0x014e:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.GridLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$z):android.view.View");
    }
}
