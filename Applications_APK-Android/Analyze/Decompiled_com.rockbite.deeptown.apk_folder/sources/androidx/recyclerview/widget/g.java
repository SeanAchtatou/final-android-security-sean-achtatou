package androidx.recyclerview.widget;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: LayoutState */
class g {

    /* renamed from: a  reason: collision with root package name */
    boolean f1928a = true;

    /* renamed from: b  reason: collision with root package name */
    int f1929b;

    /* renamed from: c  reason: collision with root package name */
    int f1930c;

    /* renamed from: d  reason: collision with root package name */
    int f1931d;

    /* renamed from: e  reason: collision with root package name */
    int f1932e;

    /* renamed from: f  reason: collision with root package name */
    int f1933f = 0;

    /* renamed from: g  reason: collision with root package name */
    int f1934g = 0;

    /* renamed from: h  reason: collision with root package name */
    boolean f1935h;

    /* renamed from: i  reason: collision with root package name */
    boolean f1936i;

    g() {
    }

    /* access modifiers changed from: package-private */
    public boolean a(RecyclerView.z zVar) {
        int i2 = this.f1930c;
        return i2 >= 0 && i2 < zVar.a();
    }

    public String toString() {
        return "LayoutState{mAvailable=" + this.f1929b + ", mCurrentPosition=" + this.f1930c + ", mItemDirection=" + this.f1931d + ", mLayoutDirection=" + this.f1932e + ", mStartLine=" + this.f1933f + ", mEndLine=" + this.f1934g + '}';
    }

    /* access modifiers changed from: package-private */
    public View a(RecyclerView.v vVar) {
        View d2 = vVar.d(this.f1930c);
        this.f1930c += this.f1931d;
        return d2;
    }
}
