package androidx.recyclerview.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewPropertyAnimator;
import androidx.recyclerview.widget.RecyclerView;
import b.e.m.u;
import com.esotericsoftware.spine.Animation;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: DefaultItemAnimator */
public class c extends l {
    private static TimeInterpolator s;

    /* renamed from: h  reason: collision with root package name */
    private ArrayList<RecyclerView.c0> f1854h = new ArrayList<>();

    /* renamed from: i  reason: collision with root package name */
    private ArrayList<RecyclerView.c0> f1855i = new ArrayList<>();

    /* renamed from: j  reason: collision with root package name */
    private ArrayList<j> f1856j = new ArrayList<>();

    /* renamed from: k  reason: collision with root package name */
    private ArrayList<i> f1857k = new ArrayList<>();
    ArrayList<ArrayList<RecyclerView.c0>> l = new ArrayList<>();
    ArrayList<ArrayList<j>> m = new ArrayList<>();
    ArrayList<ArrayList<i>> n = new ArrayList<>();
    ArrayList<RecyclerView.c0> o = new ArrayList<>();
    ArrayList<RecyclerView.c0> p = new ArrayList<>();
    ArrayList<RecyclerView.c0> q = new ArrayList<>();
    ArrayList<RecyclerView.c0> r = new ArrayList<>();

    /* compiled from: DefaultItemAnimator */
    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ ArrayList f1858a;

        a(ArrayList arrayList) {
            this.f1858a = arrayList;
        }

        public void run() {
            Iterator it = this.f1858a.iterator();
            while (it.hasNext()) {
                j jVar = (j) it.next();
                c.this.b(jVar.f1892a, jVar.f1893b, jVar.f1894c, jVar.f1895d, jVar.f1896e);
            }
            this.f1858a.clear();
            c.this.m.remove(this.f1858a);
        }
    }

    /* compiled from: DefaultItemAnimator */
    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ ArrayList f1860a;

        b(ArrayList arrayList) {
            this.f1860a = arrayList;
        }

        public void run() {
            Iterator it = this.f1860a.iterator();
            while (it.hasNext()) {
                c.this.a((i) it.next());
            }
            this.f1860a.clear();
            c.this.n.remove(this.f1860a);
        }
    }

    /* renamed from: androidx.recyclerview.widget.c$c  reason: collision with other inner class name */
    /* compiled from: DefaultItemAnimator */
    class C0022c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ ArrayList f1862a;

        C0022c(ArrayList arrayList) {
            this.f1862a = arrayList;
        }

        public void run() {
            Iterator it = this.f1862a.iterator();
            while (it.hasNext()) {
                c.this.t((RecyclerView.c0) it.next());
            }
            this.f1862a.clear();
            c.this.l.remove(this.f1862a);
        }
    }

    /* compiled from: DefaultItemAnimator */
    class d extends AnimatorListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ RecyclerView.c0 f1864a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ ViewPropertyAnimator f1865b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ View f1866c;

        d(RecyclerView.c0 c0Var, ViewPropertyAnimator viewPropertyAnimator, View view) {
            this.f1864a = c0Var;
            this.f1865b = viewPropertyAnimator;
            this.f1866c = view;
        }

        public void onAnimationEnd(Animator animator) {
            this.f1865b.setListener(null);
            this.f1866c.setAlpha(1.0f);
            c.this.l(this.f1864a);
            c.this.q.remove(this.f1864a);
            c.this.j();
        }

        public void onAnimationStart(Animator animator) {
            c.this.m(this.f1864a);
        }
    }

    /* compiled from: DefaultItemAnimator */
    class e extends AnimatorListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ RecyclerView.c0 f1868a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ View f1869b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ ViewPropertyAnimator f1870c;

        e(RecyclerView.c0 c0Var, View view, ViewPropertyAnimator viewPropertyAnimator) {
            this.f1868a = c0Var;
            this.f1869b = view;
            this.f1870c = viewPropertyAnimator;
        }

        public void onAnimationCancel(Animator animator) {
            this.f1869b.setAlpha(1.0f);
        }

        public void onAnimationEnd(Animator animator) {
            this.f1870c.setListener(null);
            c.this.h(this.f1868a);
            c.this.o.remove(this.f1868a);
            c.this.j();
        }

        public void onAnimationStart(Animator animator) {
            c.this.i(this.f1868a);
        }
    }

    /* compiled from: DefaultItemAnimator */
    class f extends AnimatorListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ RecyclerView.c0 f1872a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ int f1873b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ View f1874c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ int f1875d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ ViewPropertyAnimator f1876e;

        f(RecyclerView.c0 c0Var, int i2, View view, int i3, ViewPropertyAnimator viewPropertyAnimator) {
            this.f1872a = c0Var;
            this.f1873b = i2;
            this.f1874c = view;
            this.f1875d = i3;
            this.f1876e = viewPropertyAnimator;
        }

        public void onAnimationCancel(Animator animator) {
            if (this.f1873b != 0) {
                this.f1874c.setTranslationX(Animation.CurveTimeline.LINEAR);
            }
            if (this.f1875d != 0) {
                this.f1874c.setTranslationY(Animation.CurveTimeline.LINEAR);
            }
        }

        public void onAnimationEnd(Animator animator) {
            this.f1876e.setListener(null);
            c.this.j(this.f1872a);
            c.this.p.remove(this.f1872a);
            c.this.j();
        }

        public void onAnimationStart(Animator animator) {
            c.this.k(this.f1872a);
        }
    }

    /* compiled from: DefaultItemAnimator */
    class g extends AnimatorListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ i f1878a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ ViewPropertyAnimator f1879b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ View f1880c;

        g(i iVar, ViewPropertyAnimator viewPropertyAnimator, View view) {
            this.f1878a = iVar;
            this.f1879b = viewPropertyAnimator;
            this.f1880c = view;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.l.a(androidx.recyclerview.widget.RecyclerView$c0, boolean):void
         arg types: [androidx.recyclerview.widget.RecyclerView$c0, int]
         candidates:
          androidx.recyclerview.widget.c.a(java.util.List<androidx.recyclerview.widget.c$i>, androidx.recyclerview.widget.RecyclerView$c0):void
          androidx.recyclerview.widget.c.a(androidx.recyclerview.widget.c$i, androidx.recyclerview.widget.RecyclerView$c0):boolean
          androidx.recyclerview.widget.c.a(androidx.recyclerview.widget.RecyclerView$c0, java.util.List<java.lang.Object>):boolean
          androidx.recyclerview.widget.RecyclerView.l.a(androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$c0):androidx.recyclerview.widget.RecyclerView$l$c
          androidx.recyclerview.widget.RecyclerView.l.a(androidx.recyclerview.widget.RecyclerView$c0, java.util.List<java.lang.Object>):boolean
          androidx.recyclerview.widget.l.a(androidx.recyclerview.widget.RecyclerView$c0, boolean):void */
        public void onAnimationEnd(Animator animator) {
            this.f1879b.setListener(null);
            this.f1880c.setAlpha(1.0f);
            this.f1880c.setTranslationX(Animation.CurveTimeline.LINEAR);
            this.f1880c.setTranslationY(Animation.CurveTimeline.LINEAR);
            c.this.a(this.f1878a.f1886a, true);
            c.this.r.remove(this.f1878a.f1886a);
            c.this.j();
        }

        public void onAnimationStart(Animator animator) {
            c.this.b(this.f1878a.f1886a, true);
        }
    }

    /* compiled from: DefaultItemAnimator */
    class h extends AnimatorListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ i f1882a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ ViewPropertyAnimator f1883b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ View f1884c;

        h(i iVar, ViewPropertyAnimator viewPropertyAnimator, View view) {
            this.f1882a = iVar;
            this.f1883b = viewPropertyAnimator;
            this.f1884c = view;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.recyclerview.widget.l.a(androidx.recyclerview.widget.RecyclerView$c0, boolean):void
         arg types: [androidx.recyclerview.widget.RecyclerView$c0, int]
         candidates:
          androidx.recyclerview.widget.c.a(java.util.List<androidx.recyclerview.widget.c$i>, androidx.recyclerview.widget.RecyclerView$c0):void
          androidx.recyclerview.widget.c.a(androidx.recyclerview.widget.c$i, androidx.recyclerview.widget.RecyclerView$c0):boolean
          androidx.recyclerview.widget.c.a(androidx.recyclerview.widget.RecyclerView$c0, java.util.List<java.lang.Object>):boolean
          androidx.recyclerview.widget.RecyclerView.l.a(androidx.recyclerview.widget.RecyclerView$z, androidx.recyclerview.widget.RecyclerView$c0):androidx.recyclerview.widget.RecyclerView$l$c
          androidx.recyclerview.widget.RecyclerView.l.a(androidx.recyclerview.widget.RecyclerView$c0, java.util.List<java.lang.Object>):boolean
          androidx.recyclerview.widget.l.a(androidx.recyclerview.widget.RecyclerView$c0, boolean):void */
        public void onAnimationEnd(Animator animator) {
            this.f1883b.setListener(null);
            this.f1884c.setAlpha(1.0f);
            this.f1884c.setTranslationX(Animation.CurveTimeline.LINEAR);
            this.f1884c.setTranslationY(Animation.CurveTimeline.LINEAR);
            c.this.a(this.f1882a.f1887b, false);
            c.this.r.remove(this.f1882a.f1887b);
            c.this.j();
        }

        public void onAnimationStart(Animator animator) {
            c.this.b(this.f1882a.f1887b, false);
        }
    }

    /* compiled from: DefaultItemAnimator */
    private static class j {

        /* renamed from: a  reason: collision with root package name */
        public RecyclerView.c0 f1892a;

        /* renamed from: b  reason: collision with root package name */
        public int f1893b;

        /* renamed from: c  reason: collision with root package name */
        public int f1894c;

        /* renamed from: d  reason: collision with root package name */
        public int f1895d;

        /* renamed from: e  reason: collision with root package name */
        public int f1896e;

        j(RecyclerView.c0 c0Var, int i2, int i3, int i4, int i5) {
            this.f1892a = c0Var;
            this.f1893b = i2;
            this.f1894c = i3;
            this.f1895d = i4;
            this.f1896e = i5;
        }
    }

    private void u(RecyclerView.c0 c0Var) {
        View view = c0Var.itemView;
        ViewPropertyAnimator animate = view.animate();
        this.q.add(c0Var);
        animate.setDuration(f()).alpha(Animation.CurveTimeline.LINEAR).setListener(new d(c0Var, animate, view)).start();
    }

    private void v(RecyclerView.c0 c0Var) {
        if (s == null) {
            s = new ValueAnimator().getInterpolator();
        }
        c0Var.itemView.animate().setInterpolator(s);
        c(c0Var);
    }

    public boolean a(RecyclerView.c0 c0Var, int i2, int i3, int i4, int i5) {
        View view = c0Var.itemView;
        int translationX = i2 + ((int) view.getTranslationX());
        int translationY = i3 + ((int) c0Var.itemView.getTranslationY());
        v(c0Var);
        int i6 = i4 - translationX;
        int i7 = i5 - translationY;
        if (i6 == 0 && i7 == 0) {
            j(c0Var);
            return false;
        }
        if (i6 != 0) {
            view.setTranslationX((float) (-i6));
        }
        if (i7 != 0) {
            view.setTranslationY((float) (-i7));
        }
        this.f1856j.add(new j(c0Var, translationX, translationY, i4, i5));
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b(RecyclerView.c0 c0Var, int i2, int i3, int i4, int i5) {
        View view = c0Var.itemView;
        int i6 = i4 - i2;
        int i7 = i5 - i3;
        if (i6 != 0) {
            view.animate().translationX(Animation.CurveTimeline.LINEAR);
        }
        if (i7 != 0) {
            view.animate().translationY(Animation.CurveTimeline.LINEAR);
        }
        ViewPropertyAnimator animate = view.animate();
        this.p.add(c0Var);
        animate.setDuration(e()).setListener(new f(c0Var, i6, view, i7, animate)).start();
    }

    public void c(RecyclerView.c0 c0Var) {
        View view = c0Var.itemView;
        view.animate().cancel();
        int size = this.f1856j.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            } else if (this.f1856j.get(size).f1892a == c0Var) {
                view.setTranslationY(Animation.CurveTimeline.LINEAR);
                view.setTranslationX(Animation.CurveTimeline.LINEAR);
                j(c0Var);
                this.f1856j.remove(size);
            }
        }
        a(this.f1857k, c0Var);
        if (this.f1854h.remove(c0Var)) {
            view.setAlpha(1.0f);
            l(c0Var);
        }
        if (this.f1855i.remove(c0Var)) {
            view.setAlpha(1.0f);
            h(c0Var);
        }
        for (int size2 = this.n.size() - 1; size2 >= 0; size2--) {
            ArrayList arrayList = this.n.get(size2);
            a(arrayList, c0Var);
            if (arrayList.isEmpty()) {
                this.n.remove(size2);
            }
        }
        for (int size3 = this.m.size() - 1; size3 >= 0; size3--) {
            ArrayList arrayList2 = this.m.get(size3);
            int size4 = arrayList2.size() - 1;
            while (true) {
                if (size4 < 0) {
                    break;
                } else if (((j) arrayList2.get(size4)).f1892a == c0Var) {
                    view.setTranslationY(Animation.CurveTimeline.LINEAR);
                    view.setTranslationX(Animation.CurveTimeline.LINEAR);
                    j(c0Var);
                    arrayList2.remove(size4);
                    if (arrayList2.isEmpty()) {
                        this.m.remove(size3);
                    }
                } else {
                    size4--;
                }
            }
        }
        for (int size5 = this.l.size() - 1; size5 >= 0; size5--) {
            ArrayList arrayList3 = this.l.get(size5);
            if (arrayList3.remove(c0Var)) {
                view.setAlpha(1.0f);
                h(c0Var);
                if (arrayList3.isEmpty()) {
                    this.l.remove(size5);
                }
            }
        }
        this.q.remove(c0Var);
        this.o.remove(c0Var);
        this.r.remove(c0Var);
        this.p.remove(c0Var);
        j();
    }

    public boolean f(RecyclerView.c0 c0Var) {
        v(c0Var);
        c0Var.itemView.setAlpha(Animation.CurveTimeline.LINEAR);
        this.f1855i.add(c0Var);
        return true;
    }

    public boolean g(RecyclerView.c0 c0Var) {
        v(c0Var);
        this.f1854h.add(c0Var);
        return true;
    }

    public void i() {
        boolean z = !this.f1854h.isEmpty();
        boolean z2 = !this.f1856j.isEmpty();
        boolean z3 = !this.f1857k.isEmpty();
        boolean z4 = !this.f1855i.isEmpty();
        if (z || z2 || z4 || z3) {
            Iterator<RecyclerView.c0> it = this.f1854h.iterator();
            while (it.hasNext()) {
                u(it.next());
            }
            this.f1854h.clear();
            if (z2) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(this.f1856j);
                this.m.add(arrayList);
                this.f1856j.clear();
                a aVar = new a(arrayList);
                if (z) {
                    u.a(((j) arrayList.get(0)).f1892a.itemView, aVar, f());
                } else {
                    aVar.run();
                }
            }
            if (z3) {
                ArrayList arrayList2 = new ArrayList();
                arrayList2.addAll(this.f1857k);
                this.n.add(arrayList2);
                this.f1857k.clear();
                b bVar = new b(arrayList2);
                if (z) {
                    u.a(((i) arrayList2.get(0)).f1886a.itemView, bVar, f());
                } else {
                    bVar.run();
                }
            }
            if (z4) {
                ArrayList arrayList3 = new ArrayList();
                arrayList3.addAll(this.f1855i);
                this.l.add(arrayList3);
                this.f1855i.clear();
                C0022c cVar = new C0022c(arrayList3);
                if (z || z2 || z3) {
                    long j2 = 0;
                    long f2 = z ? f() : 0;
                    long e2 = z2 ? e() : 0;
                    if (z3) {
                        j2 = d();
                    }
                    u.a(((RecyclerView.c0) arrayList3.get(0)).itemView, cVar, f2 + Math.max(e2, j2));
                    return;
                }
                cVar.run();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void j() {
        if (!g()) {
            a();
        }
    }

    /* access modifiers changed from: package-private */
    public void t(RecyclerView.c0 c0Var) {
        View view = c0Var.itemView;
        ViewPropertyAnimator animate = view.animate();
        this.o.add(c0Var);
        animate.alpha(1.0f).setDuration(c()).setListener(new e(c0Var, view, animate)).start();
    }

    /* compiled from: DefaultItemAnimator */
    private static class i {

        /* renamed from: a  reason: collision with root package name */
        public RecyclerView.c0 f1886a;

        /* renamed from: b  reason: collision with root package name */
        public RecyclerView.c0 f1887b;

        /* renamed from: c  reason: collision with root package name */
        public int f1888c;

        /* renamed from: d  reason: collision with root package name */
        public int f1889d;

        /* renamed from: e  reason: collision with root package name */
        public int f1890e;

        /* renamed from: f  reason: collision with root package name */
        public int f1891f;

        private i(RecyclerView.c0 c0Var, RecyclerView.c0 c0Var2) {
            this.f1886a = c0Var;
            this.f1887b = c0Var2;
        }

        public String toString() {
            return "ChangeInfo{oldHolder=" + this.f1886a + ", newHolder=" + this.f1887b + ", fromX=" + this.f1888c + ", fromY=" + this.f1889d + ", toX=" + this.f1890e + ", toY=" + this.f1891f + '}';
        }

        i(RecyclerView.c0 c0Var, RecyclerView.c0 c0Var2, int i2, int i3, int i4, int i5) {
            this(c0Var, c0Var2);
            this.f1888c = i2;
            this.f1889d = i3;
            this.f1890e = i4;
            this.f1891f = i5;
        }
    }

    public boolean g() {
        return !this.f1855i.isEmpty() || !this.f1857k.isEmpty() || !this.f1856j.isEmpty() || !this.f1854h.isEmpty() || !this.p.isEmpty() || !this.q.isEmpty() || !this.o.isEmpty() || !this.r.isEmpty() || !this.m.isEmpty() || !this.l.isEmpty() || !this.n.isEmpty();
    }

    private void b(i iVar) {
        RecyclerView.c0 c0Var = iVar.f1886a;
        if (c0Var != null) {
            a(iVar, c0Var);
        }
        RecyclerView.c0 c0Var2 = iVar.f1887b;
        if (c0Var2 != null) {
            a(iVar, c0Var2);
        }
    }

    public boolean a(RecyclerView.c0 c0Var, RecyclerView.c0 c0Var2, int i2, int i3, int i4, int i5) {
        if (c0Var == c0Var2) {
            return a(c0Var, i2, i3, i4, i5);
        }
        float translationX = c0Var.itemView.getTranslationX();
        float translationY = c0Var.itemView.getTranslationY();
        float alpha = c0Var.itemView.getAlpha();
        v(c0Var);
        int i6 = (int) (((float) (i4 - i2)) - translationX);
        int i7 = (int) (((float) (i5 - i3)) - translationY);
        c0Var.itemView.setTranslationX(translationX);
        c0Var.itemView.setTranslationY(translationY);
        c0Var.itemView.setAlpha(alpha);
        if (c0Var2 != null) {
            v(c0Var2);
            c0Var2.itemView.setTranslationX((float) (-i6));
            c0Var2.itemView.setTranslationY((float) (-i7));
            c0Var2.itemView.setAlpha(Animation.CurveTimeline.LINEAR);
        }
        this.f1857k.add(new i(c0Var, c0Var2, i2, i3, i4, i5));
        return true;
    }

    public void b() {
        int size = this.f1856j.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            }
            j jVar = this.f1856j.get(size);
            View view = jVar.f1892a.itemView;
            view.setTranslationY(Animation.CurveTimeline.LINEAR);
            view.setTranslationX(Animation.CurveTimeline.LINEAR);
            j(jVar.f1892a);
            this.f1856j.remove(size);
        }
        for (int size2 = this.f1854h.size() - 1; size2 >= 0; size2--) {
            l(this.f1854h.get(size2));
            this.f1854h.remove(size2);
        }
        int size3 = this.f1855i.size();
        while (true) {
            size3--;
            if (size3 < 0) {
                break;
            }
            RecyclerView.c0 c0Var = this.f1855i.get(size3);
            c0Var.itemView.setAlpha(1.0f);
            h(c0Var);
            this.f1855i.remove(size3);
        }
        for (int size4 = this.f1857k.size() - 1; size4 >= 0; size4--) {
            b(this.f1857k.get(size4));
        }
        this.f1857k.clear();
        if (g()) {
            for (int size5 = this.m.size() - 1; size5 >= 0; size5--) {
                ArrayList arrayList = this.m.get(size5);
                for (int size6 = arrayList.size() - 1; size6 >= 0; size6--) {
                    j jVar2 = (j) arrayList.get(size6);
                    View view2 = jVar2.f1892a.itemView;
                    view2.setTranslationY(Animation.CurveTimeline.LINEAR);
                    view2.setTranslationX(Animation.CurveTimeline.LINEAR);
                    j(jVar2.f1892a);
                    arrayList.remove(size6);
                    if (arrayList.isEmpty()) {
                        this.m.remove(arrayList);
                    }
                }
            }
            for (int size7 = this.l.size() - 1; size7 >= 0; size7--) {
                ArrayList arrayList2 = this.l.get(size7);
                for (int size8 = arrayList2.size() - 1; size8 >= 0; size8--) {
                    RecyclerView.c0 c0Var2 = (RecyclerView.c0) arrayList2.get(size8);
                    c0Var2.itemView.setAlpha(1.0f);
                    h(c0Var2);
                    arrayList2.remove(size8);
                    if (arrayList2.isEmpty()) {
                        this.l.remove(arrayList2);
                    }
                }
            }
            for (int size9 = this.n.size() - 1; size9 >= 0; size9--) {
                ArrayList arrayList3 = this.n.get(size9);
                for (int size10 = arrayList3.size() - 1; size10 >= 0; size10--) {
                    b((i) arrayList3.get(size10));
                    if (arrayList3.isEmpty()) {
                        this.n.remove(arrayList3);
                    }
                }
            }
            a(this.q);
            a(this.p);
            a(this.o);
            a(this.r);
            a();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(i iVar) {
        View view;
        RecyclerView.c0 c0Var = iVar.f1886a;
        View view2 = null;
        if (c0Var == null) {
            view = null;
        } else {
            view = c0Var.itemView;
        }
        RecyclerView.c0 c0Var2 = iVar.f1887b;
        if (c0Var2 != null) {
            view2 = c0Var2.itemView;
        }
        if (view != null) {
            ViewPropertyAnimator duration = view.animate().setDuration(d());
            this.r.add(iVar.f1886a);
            duration.translationX((float) (iVar.f1890e - iVar.f1888c));
            duration.translationY((float) (iVar.f1891f - iVar.f1889d));
            duration.alpha(Animation.CurveTimeline.LINEAR).setListener(new g(iVar, duration, view)).start();
        }
        if (view2 != null) {
            ViewPropertyAnimator animate = view2.animate();
            this.r.add(iVar.f1887b);
            animate.translationX(Animation.CurveTimeline.LINEAR).translationY(Animation.CurveTimeline.LINEAR).setDuration(d()).alpha(1.0f).setListener(new h(iVar, animate, view2)).start();
        }
    }

    private void a(List<i> list, RecyclerView.c0 c0Var) {
        for (int size = list.size() - 1; size >= 0; size--) {
            i iVar = list.get(size);
            if (a(iVar, c0Var) && iVar.f1886a == null && iVar.f1887b == null) {
                list.remove(iVar);
            }
        }
    }

    private boolean a(i iVar, RecyclerView.c0 c0Var) {
        boolean z = false;
        if (iVar.f1887b == c0Var) {
            iVar.f1887b = null;
        } else if (iVar.f1886a != c0Var) {
            return false;
        } else {
            iVar.f1886a = null;
            z = true;
        }
        c0Var.itemView.setAlpha(1.0f);
        c0Var.itemView.setTranslationX(Animation.CurveTimeline.LINEAR);
        c0Var.itemView.setTranslationY(Animation.CurveTimeline.LINEAR);
        a(c0Var, z);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(List<RecyclerView.c0> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            list.get(size).itemView.animate().cancel();
        }
    }

    public boolean a(RecyclerView.c0 c0Var, List<Object> list) {
        return !list.isEmpty() || super.a(c0Var, list);
    }
}
