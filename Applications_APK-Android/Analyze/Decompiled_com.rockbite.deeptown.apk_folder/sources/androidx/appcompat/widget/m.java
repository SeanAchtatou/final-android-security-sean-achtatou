package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageButton;
import androidx.core.widget.l;
import b.a.a;
import b.e.m.t;

/* compiled from: AppCompatImageButton */
public class m extends ImageButton implements t, l {

    /* renamed from: a  reason: collision with root package name */
    private final e f1111a;

    /* renamed from: b  reason: collision with root package name */
    private final n f1112b;

    public m(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.imageButtonStyle);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        e eVar = this.f1111a;
        if (eVar != null) {
            eVar.a();
        }
        n nVar = this.f1112b;
        if (nVar != null) {
            nVar.a();
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        e eVar = this.f1111a;
        if (eVar != null) {
            return eVar.b();
        }
        return null;
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        e eVar = this.f1111a;
        if (eVar != null) {
            return eVar.c();
        }
        return null;
    }

    public ColorStateList getSupportImageTintList() {
        n nVar = this.f1112b;
        if (nVar != null) {
            return nVar.b();
        }
        return null;
    }

    public PorterDuff.Mode getSupportImageTintMode() {
        n nVar = this.f1112b;
        if (nVar != null) {
            return nVar.c();
        }
        return null;
    }

    public boolean hasOverlappingRendering() {
        return this.f1112b.d() && super.hasOverlappingRendering();
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        e eVar = this.f1111a;
        if (eVar != null) {
            eVar.a(drawable);
        }
    }

    public void setBackgroundResource(int i2) {
        super.setBackgroundResource(i2);
        e eVar = this.f1111a;
        if (eVar != null) {
            eVar.a(i2);
        }
    }

    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        n nVar = this.f1112b;
        if (nVar != null) {
            nVar.a();
        }
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        n nVar = this.f1112b;
        if (nVar != null) {
            nVar.a();
        }
    }

    public void setImageResource(int i2) {
        this.f1112b.a(i2);
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        n nVar = this.f1112b;
        if (nVar != null) {
            nVar.a();
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        e eVar = this.f1111a;
        if (eVar != null) {
            eVar.b(colorStateList);
        }
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        e eVar = this.f1111a;
        if (eVar != null) {
            eVar.a(mode);
        }
    }

    public void setSupportImageTintList(ColorStateList colorStateList) {
        n nVar = this.f1112b;
        if (nVar != null) {
            nVar.a(colorStateList);
        }
    }

    public void setSupportImageTintMode(PorterDuff.Mode mode) {
        n nVar = this.f1112b;
        if (nVar != null) {
            nVar.a(mode);
        }
    }

    public m(Context context, AttributeSet attributeSet, int i2) {
        super(s0.b(context), attributeSet, i2);
        this.f1111a = new e(this);
        this.f1111a.a(attributeSet, i2);
        this.f1112b = new n(this);
        this.f1112b.a(attributeSet, i2);
    }
}
