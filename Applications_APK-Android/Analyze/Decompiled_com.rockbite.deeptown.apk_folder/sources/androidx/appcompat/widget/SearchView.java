package androidx.appcompat.widget;

import android.app.PendingIntent;
import android.app.SearchableInfo;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.customview.view.AbsSavedState;
import b.e.m.u;
import com.appsflyer.share.Constants;
import com.google.android.gms.drive.DriveFile;
import java.lang.reflect.Method;
import java.util.WeakHashMap;

public class SearchView extends h0 implements b.a.n.c {
    static final k q0 = new k();
    private Rect A;
    private int[] B;
    private int[] C;
    private final ImageView D;
    private final Drawable E;
    private final int F;
    private final int G;
    private final Intent H;
    private final Intent I;
    private final CharSequence J;
    private m K;
    private l L;
    View.OnFocusChangeListener M;
    private n N;
    private View.OnClickListener O;
    private boolean P;
    private boolean Q;
    b.f.a.a R;
    private boolean S;
    private CharSequence T;
    private boolean U;
    private boolean V;
    private int W;
    private boolean a0;
    private CharSequence b0;
    private CharSequence c0;
    private boolean d0;
    private int e0;
    SearchableInfo f0;
    private Bundle g0;
    private final Runnable h0;
    private Runnable i0;
    private final WeakHashMap<String, Drawable.ConstantState> j0;
    private final View.OnClickListener k0;
    View.OnKeyListener l0;
    private final TextView.OnEditorActionListener m0;
    private final AdapterView.OnItemClickListener n0;
    private final AdapterView.OnItemSelectedListener o0;
    final SearchAutoComplete p;
    private TextWatcher p0;
    private final View q;
    private final View r;
    private final View s;
    final ImageView t;
    final ImageView u;
    final ImageView v;
    final ImageView w;
    private final View x;
    private o y;
    private Rect z;

    static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();

        /* renamed from: c  reason: collision with root package name */
        boolean f935c;

        static class a implements Parcelable.ClassLoaderCreator<SavedState> {
            a() {
            }

            public SavedState[] newArray(int i2) {
                return new SavedState[i2];
            }

            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "SearchView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " isIconified=" + this.f935c + "}";
        }

        public void writeToParcel(Parcel parcel, int i2) {
            super.writeToParcel(parcel, i2);
            parcel.writeValue(Boolean.valueOf(this.f935c));
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f935c = ((Boolean) parcel.readValue(null)).booleanValue();
        }
    }

    public static class SearchAutoComplete extends d {

        /* renamed from: d  reason: collision with root package name */
        private int f936d;

        /* renamed from: e  reason: collision with root package name */
        private SearchView f937e;

        /* renamed from: f  reason: collision with root package name */
        private boolean f938f;

        /* renamed from: g  reason: collision with root package name */
        final Runnable f939g;

        class a implements Runnable {
            a() {
            }

            public void run() {
                SearchAutoComplete.this.b();
            }
        }

        public SearchAutoComplete(Context context) {
            this(context, null);
        }

        private int getSearchViewTextMinWidthDp() {
            Configuration configuration = getResources().getConfiguration();
            int i2 = configuration.screenWidthDp;
            int i3 = configuration.screenHeightDp;
            if (i2 >= 960 && i3 >= 720 && configuration.orientation == 2) {
                return 256;
            }
            if (i2 < 600) {
                return (i2 < 640 || i3 < 480) ? 160 : 192;
            }
            return 192;
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return TextUtils.getTrimmedLength(getText()) == 0;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            if (this.f938f) {
                ((InputMethodManager) getContext().getSystemService("input_method")).showSoftInput(this, 0);
                this.f938f = false;
            }
        }

        public boolean enoughToFilter() {
            return this.f936d <= 0 || super.enoughToFilter();
        }

        public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
            InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
            if (this.f938f) {
                removeCallbacks(this.f939g);
                post(this.f939g);
            }
            return onCreateInputConnection;
        }

        /* access modifiers changed from: protected */
        public void onFinishInflate() {
            super.onFinishInflate();
            setMinWidth((int) TypedValue.applyDimension(1, (float) getSearchViewTextMinWidthDp(), getResources().getDisplayMetrics()));
        }

        /* access modifiers changed from: protected */
        public void onFocusChanged(boolean z, int i2, Rect rect) {
            super.onFocusChanged(z, i2, rect);
            this.f937e.i();
        }

        public boolean onKeyPreIme(int i2, KeyEvent keyEvent) {
            if (i2 == 4) {
                if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                    KeyEvent.DispatcherState keyDispatcherState = getKeyDispatcherState();
                    if (keyDispatcherState != null) {
                        keyDispatcherState.startTracking(keyEvent, this);
                    }
                    return true;
                } else if (keyEvent.getAction() == 1) {
                    KeyEvent.DispatcherState keyDispatcherState2 = getKeyDispatcherState();
                    if (keyDispatcherState2 != null) {
                        keyDispatcherState2.handleUpEvent(keyEvent);
                    }
                    if (keyEvent.isTracking() && !keyEvent.isCanceled()) {
                        this.f937e.clearFocus();
                        setImeVisibility(false);
                        return true;
                    }
                }
            }
            return super.onKeyPreIme(i2, keyEvent);
        }

        public void onWindowFocusChanged(boolean z) {
            super.onWindowFocusChanged(z);
            if (z && this.f937e.hasFocus() && getVisibility() == 0) {
                this.f938f = true;
                if (SearchView.a(getContext())) {
                    SearchView.q0.a(this, true);
                }
            }
        }

        public void performCompletion() {
        }

        /* access modifiers changed from: protected */
        public void replaceText(CharSequence charSequence) {
        }

        /* access modifiers changed from: package-private */
        public void setImeVisibility(boolean z) {
            InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
            if (!z) {
                this.f938f = false;
                removeCallbacks(this.f939g);
                inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            } else if (inputMethodManager.isActive(this)) {
                this.f938f = false;
                removeCallbacks(this.f939g);
                inputMethodManager.showSoftInput(this, 0);
            } else {
                this.f938f = true;
            }
        }

        /* access modifiers changed from: package-private */
        public void setSearchView(SearchView searchView) {
            this.f937e = searchView;
        }

        public void setThreshold(int i2) {
            super.setThreshold(i2);
            this.f936d = i2;
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet) {
            this(context, attributeSet, b.a.a.autoCompleteTextViewStyle);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet, int i2) {
            super(context, attributeSet, i2);
            this.f939g = new a();
            this.f936d = getThreshold();
        }
    }

    class a implements TextWatcher {
        a() {
        }

        public void afterTextChanged(Editable editable) {
        }

        public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        }

        public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
            SearchView.this.b(charSequence);
        }
    }

    class b implements Runnable {
        b() {
        }

        public void run() {
            SearchView.this.k();
        }
    }

    class c implements Runnable {
        c() {
        }

        public void run() {
            b.f.a.a aVar = SearchView.this.R;
            if (aVar instanceof p0) {
                aVar.a((Cursor) null);
            }
        }
    }

    class d implements View.OnFocusChangeListener {
        d() {
        }

        public void onFocusChange(View view, boolean z) {
            SearchView searchView = SearchView.this;
            View.OnFocusChangeListener onFocusChangeListener = searchView.M;
            if (onFocusChangeListener != null) {
                onFocusChangeListener.onFocusChange(searchView, z);
            }
        }
    }

    class e implements View.OnLayoutChangeListener {
        e() {
        }

        public void onLayoutChange(View view, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
            SearchView.this.c();
        }
    }

    class f implements View.OnClickListener {
        f() {
        }

        public void onClick(View view) {
            SearchView searchView = SearchView.this;
            if (view == searchView.t) {
                searchView.g();
            } else if (view == searchView.v) {
                searchView.f();
            } else if (view == searchView.u) {
                searchView.h();
            } else if (view == searchView.w) {
                searchView.j();
            } else if (view == searchView.p) {
                searchView.d();
            }
        }
    }

    class g implements View.OnKeyListener {
        g() {
        }

        public boolean onKey(View view, int i2, KeyEvent keyEvent) {
            SearchView searchView = SearchView.this;
            if (searchView.f0 == null) {
                return false;
            }
            if (searchView.p.isPopupShowing() && SearchView.this.p.getListSelection() != -1) {
                return SearchView.this.a(view, i2, keyEvent);
            }
            if (SearchView.this.p.a() || !keyEvent.hasNoModifiers() || keyEvent.getAction() != 1 || i2 != 66) {
                return false;
            }
            view.cancelLongPress();
            SearchView searchView2 = SearchView.this;
            searchView2.a(0, (String) null, searchView2.p.getText().toString());
            return true;
        }
    }

    class h implements TextView.OnEditorActionListener {
        h() {
        }

        public boolean onEditorAction(TextView textView, int i2, KeyEvent keyEvent) {
            SearchView.this.h();
            return true;
        }
    }

    class i implements AdapterView.OnItemClickListener {
        i() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
            SearchView.this.a(i2, 0, (String) null);
        }
    }

    class j implements AdapterView.OnItemSelectedListener {
        j() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int i2, long j2) {
            SearchView.this.d(i2);
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    public interface l {
        boolean onClose();
    }

    public interface m {
        boolean a(String str);

        boolean b(String str);
    }

    public interface n {
        boolean a(int i2);

        boolean b(int i2);
    }

    private static class o extends TouchDelegate {

        /* renamed from: a  reason: collision with root package name */
        private final View f954a;

        /* renamed from: b  reason: collision with root package name */
        private final Rect f955b = new Rect();

        /* renamed from: c  reason: collision with root package name */
        private final Rect f956c = new Rect();

        /* renamed from: d  reason: collision with root package name */
        private final Rect f957d = new Rect();

        /* renamed from: e  reason: collision with root package name */
        private final int f958e;

        /* renamed from: f  reason: collision with root package name */
        private boolean f959f;

        public o(Rect rect, Rect rect2, View view) {
            super(rect, view);
            this.f958e = ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
            a(rect, rect2);
            this.f954a = view;
        }

        public void a(Rect rect, Rect rect2) {
            this.f955b.set(rect);
            this.f957d.set(rect);
            Rect rect3 = this.f957d;
            int i2 = this.f958e;
            rect3.inset(-i2, -i2);
            this.f956c.set(rect2);
        }

        /* JADX WARNING: Removed duplicated region for block: B:17:0x003d  */
        /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTouchEvent(android.view.MotionEvent r8) {
            /*
                r7 = this;
                float r0 = r8.getX()
                int r0 = (int) r0
                float r1 = r8.getY()
                int r1 = (int) r1
                int r2 = r8.getAction()
                r3 = 2
                r4 = 1
                r5 = 0
                if (r2 == 0) goto L_0x002e
                if (r2 == r4) goto L_0x0020
                if (r2 == r3) goto L_0x0020
                r6 = 3
                if (r2 == r6) goto L_0x001b
                goto L_0x003a
            L_0x001b:
                boolean r2 = r7.f959f
                r7.f959f = r5
                goto L_0x003b
            L_0x0020:
                boolean r2 = r7.f959f
                if (r2 == 0) goto L_0x003b
                android.graphics.Rect r6 = r7.f957d
                boolean r6 = r6.contains(r0, r1)
                if (r6 != 0) goto L_0x003b
                r4 = 0
                goto L_0x003b
            L_0x002e:
                android.graphics.Rect r2 = r7.f955b
                boolean r2 = r2.contains(r0, r1)
                if (r2 == 0) goto L_0x003a
                r7.f959f = r4
                r2 = 1
                goto L_0x003b
            L_0x003a:
                r2 = 0
            L_0x003b:
                if (r2 == 0) goto L_0x006e
                if (r4 == 0) goto L_0x005b
                android.graphics.Rect r2 = r7.f956c
                boolean r2 = r2.contains(r0, r1)
                if (r2 != 0) goto L_0x005b
                android.view.View r0 = r7.f954a
                int r0 = r0.getWidth()
                int r0 = r0 / r3
                float r0 = (float) r0
                android.view.View r1 = r7.f954a
                int r1 = r1.getHeight()
                int r1 = r1 / r3
                float r1 = (float) r1
                r8.setLocation(r0, r1)
                goto L_0x0068
            L_0x005b:
                android.graphics.Rect r2 = r7.f956c
                int r3 = r2.left
                int r0 = r0 - r3
                float r0 = (float) r0
                int r2 = r2.top
                int r1 = r1 - r2
                float r1 = (float) r1
                r8.setLocation(r0, r1)
            L_0x0068:
                android.view.View r0 = r7.f954a
                boolean r5 = r0.dispatchTouchEvent(r8)
            L_0x006e:
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.SearchView.o.onTouchEvent(android.view.MotionEvent):boolean");
        }
    }

    public SearchView(Context context) {
        this(context, null);
    }

    private void b(boolean z2) {
        this.Q = z2;
        int i2 = 0;
        int i3 = z2 ? 0 : 8;
        boolean z3 = !TextUtils.isEmpty(this.p.getText());
        this.t.setVisibility(i3);
        a(z3);
        this.q.setVisibility(z2 ? 8 : 0);
        if (this.D.getDrawable() == null || this.P) {
            i2 = 8;
        }
        this.D.setVisibility(i2);
        p();
        c(!z3);
        s();
    }

    private CharSequence c(CharSequence charSequence) {
        if (!this.P || this.E == null) {
            return charSequence;
        }
        double textSize = (double) this.p.getTextSize();
        Double.isNaN(textSize);
        int i2 = (int) (textSize * 1.25d);
        this.E.setBounds(0, 0, i2, i2);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("   ");
        spannableStringBuilder.setSpan(new ImageSpan(this.E), 1, 2, 33);
        spannableStringBuilder.append(charSequence);
        return spannableStringBuilder;
    }

    private int getPreferredHeight() {
        return getContext().getResources().getDimensionPixelSize(b.a.d.abc_search_view_preferred_height);
    }

    private int getPreferredWidth() {
        return getContext().getResources().getDimensionPixelSize(b.a.d.abc_search_view_preferred_width);
    }

    private void l() {
        this.p.dismissDropDown();
    }

    private boolean m() {
        SearchableInfo searchableInfo = this.f0;
        if (searchableInfo == null || !searchableInfo.getVoiceSearchEnabled()) {
            return false;
        }
        Intent intent = null;
        if (this.f0.getVoiceSearchLaunchWebSearch()) {
            intent = this.H;
        } else if (this.f0.getVoiceSearchLaunchRecognizer()) {
            intent = this.I;
        }
        if (intent == null || getContext().getPackageManager().resolveActivity(intent, 65536) == null) {
            return false;
        }
        return true;
    }

    private boolean n() {
        return (this.S || this.a0) && !e();
    }

    private void o() {
        post(this.h0);
    }

    private void p() {
        boolean z2 = true;
        boolean z3 = !TextUtils.isEmpty(this.p.getText());
        int i2 = 0;
        if (!z3 && (!this.P || this.d0)) {
            z2 = false;
        }
        ImageView imageView = this.v;
        if (!z2) {
            i2 = 8;
        }
        imageView.setVisibility(i2);
        Drawable drawable = this.v.getDrawable();
        if (drawable != null) {
            drawable.setState(z3 ? ViewGroup.ENABLED_STATE_SET : ViewGroup.EMPTY_STATE_SET);
        }
    }

    private void q() {
        CharSequence queryHint = getQueryHint();
        SearchAutoComplete searchAutoComplete = this.p;
        if (queryHint == null) {
            queryHint = "";
        }
        searchAutoComplete.setHint(c(queryHint));
    }

    private void r() {
        this.p.setThreshold(this.f0.getSuggestThreshold());
        this.p.setImeOptions(this.f0.getImeOptions());
        int inputType = this.f0.getInputType();
        int i2 = 1;
        if ((inputType & 15) == 1) {
            inputType &= -65537;
            if (this.f0.getSuggestAuthority() != null) {
                inputType = inputType | 65536 | 524288;
            }
        }
        this.p.setInputType(inputType);
        b.f.a.a aVar = this.R;
        if (aVar != null) {
            aVar.a((Cursor) null);
        }
        if (this.f0.getSuggestAuthority() != null) {
            this.R = new p0(getContext(), this, this.f0, this.j0);
            this.p.setAdapter(this.R);
            p0 p0Var = (p0) this.R;
            if (this.U) {
                i2 = 2;
            }
            p0Var.a(i2);
        }
    }

    private void s() {
        this.s.setVisibility((!n() || !(this.u.getVisibility() == 0 || this.w.getVisibility() == 0)) ? 8 : 0);
    }

    private void setQuery(CharSequence charSequence) {
        this.p.setText(charSequence);
        this.p.setSelection(TextUtils.isEmpty(charSequence) ? 0 : charSequence.length());
    }

    public void a(CharSequence charSequence, boolean z2) {
        this.p.setText(charSequence);
        if (charSequence != null) {
            SearchAutoComplete searchAutoComplete = this.p;
            searchAutoComplete.setSelection(searchAutoComplete.length());
            this.c0 = charSequence;
        }
        if (z2 && !TextUtils.isEmpty(charSequence)) {
            h();
        }
    }

    public void clearFocus() {
        this.V = true;
        super.clearFocus();
        this.p.clearFocus();
        this.p.setImeVisibility(false);
        this.V = false;
    }

    /* access modifiers changed from: package-private */
    public boolean d(int i2) {
        n nVar = this.N;
        if (nVar != null && nVar.a(i2)) {
            return false;
        }
        e(i2);
        return true;
    }

    public boolean e() {
        return this.Q;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (!TextUtils.isEmpty(this.p.getText())) {
            this.p.setText("");
            this.p.requestFocus();
            this.p.setImeVisibility(true);
        } else if (this.P) {
            l lVar = this.L;
            if (lVar == null || !lVar.onClose()) {
                clearFocus();
                b(true);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        b(false);
        this.p.requestFocus();
        this.p.setImeVisibility(true);
        View.OnClickListener onClickListener = this.O;
        if (onClickListener != null) {
            onClickListener.onClick(this);
        }
    }

    public int getImeOptions() {
        return this.p.getImeOptions();
    }

    public int getInputType() {
        return this.p.getInputType();
    }

    public int getMaxWidth() {
        return this.W;
    }

    public CharSequence getQuery() {
        return this.p.getText();
    }

    public CharSequence getQueryHint() {
        CharSequence charSequence = this.T;
        if (charSequence != null) {
            return charSequence;
        }
        SearchableInfo searchableInfo = this.f0;
        if (searchableInfo == null || searchableInfo.getHintId() == 0) {
            return this.J;
        }
        return getContext().getText(this.f0.getHintId());
    }

    /* access modifiers changed from: package-private */
    public int getSuggestionCommitIconResId() {
        return this.G;
    }

    /* access modifiers changed from: package-private */
    public int getSuggestionRowLayout() {
        return this.F;
    }

    public b.f.a.a getSuggestionsAdapter() {
        return this.R;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        Editable text = this.p.getText();
        if (text != null && TextUtils.getTrimmedLength(text) > 0) {
            m mVar = this.K;
            if (mVar == null || !mVar.b(text.toString())) {
                if (this.f0 != null) {
                    a(0, (String) null, text.toString());
                }
                this.p.setImeVisibility(false);
                l();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        b(e());
        o();
        if (this.p.hasFocus()) {
            d();
        }
    }

    /* access modifiers changed from: package-private */
    public void j() {
        SearchableInfo searchableInfo = this.f0;
        if (searchableInfo != null) {
            try {
                if (searchableInfo.getVoiceSearchLaunchWebSearch()) {
                    getContext().startActivity(b(this.H, searchableInfo));
                } else if (searchableInfo.getVoiceSearchLaunchRecognizer()) {
                    getContext().startActivity(a(this.I, searchableInfo));
                }
            } catch (ActivityNotFoundException unused) {
                Log.w("SearchView", "Could not find voice search activity");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void k() {
        int[] iArr = this.p.hasFocus() ? ViewGroup.FOCUSED_STATE_SET : ViewGroup.EMPTY_STATE_SET;
        Drawable background = this.r.getBackground();
        if (background != null) {
            background.setState(iArr);
        }
        Drawable background2 = this.s.getBackground();
        if (background2 != null) {
            background2.setState(iArr);
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.h0);
        post(this.i0);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        if (z2) {
            a(this.p, this.z);
            Rect rect = this.A;
            Rect rect2 = this.z;
            rect.set(rect2.left, 0, rect2.right, i5 - i3);
            o oVar = this.y;
            if (oVar == null) {
                this.y = new o(this.A, this.z, this.p);
                setTouchDelegate(this.y);
                return;
            }
            oVar.a(this.A, this.z);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        if (e()) {
            super.onMeasure(i2, i3);
            return;
        }
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == Integer.MIN_VALUE) {
            int i5 = this.W;
            size = i5 > 0 ? Math.min(i5, size) : Math.min(getPreferredWidth(), size);
        } else if (mode == 0) {
            size = this.W;
            if (size <= 0) {
                size = getPreferredWidth();
            }
        } else if (mode == 1073741824 && (i4 = this.W) > 0) {
            size = Math.min(i4, size);
        }
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (mode2 == Integer.MIN_VALUE) {
            size2 = Math.min(getPreferredHeight(), size2);
        } else if (mode2 == 0) {
            size2 = getPreferredHeight();
        }
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), View.MeasureSpec.makeMeasureSpec(size2, 1073741824));
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        b(savedState.f935c);
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f935c = e();
        return savedState;
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        o();
    }

    public boolean requestFocus(int i2, Rect rect) {
        if (this.V || !isFocusable()) {
            return false;
        }
        if (e()) {
            return super.requestFocus(i2, rect);
        }
        boolean requestFocus = this.p.requestFocus(i2, rect);
        if (requestFocus) {
            b(false);
        }
        return requestFocus;
    }

    public void setAppSearchData(Bundle bundle) {
        this.g0 = bundle;
    }

    public void setIconified(boolean z2) {
        if (z2) {
            f();
        } else {
            g();
        }
    }

    public void setIconifiedByDefault(boolean z2) {
        if (this.P != z2) {
            this.P = z2;
            b(z2);
            q();
        }
    }

    public void setImeOptions(int i2) {
        this.p.setImeOptions(i2);
    }

    public void setInputType(int i2) {
        this.p.setInputType(i2);
    }

    public void setMaxWidth(int i2) {
        this.W = i2;
        requestLayout();
    }

    public void setOnCloseListener(l lVar) {
        this.L = lVar;
    }

    public void setOnQueryTextFocusChangeListener(View.OnFocusChangeListener onFocusChangeListener) {
        this.M = onFocusChangeListener;
    }

    public void setOnQueryTextListener(m mVar) {
        this.K = mVar;
    }

    public void setOnSearchClickListener(View.OnClickListener onClickListener) {
        this.O = onClickListener;
    }

    public void setOnSuggestionListener(n nVar) {
        this.N = nVar;
    }

    public void setQueryHint(CharSequence charSequence) {
        this.T = charSequence;
        q();
    }

    public void setQueryRefinementEnabled(boolean z2) {
        this.U = z2;
        b.f.a.a aVar = this.R;
        if (aVar instanceof p0) {
            ((p0) aVar).a(z2 ? 2 : 1);
        }
    }

    public void setSearchableInfo(SearchableInfo searchableInfo) {
        this.f0 = searchableInfo;
        if (this.f0 != null) {
            r();
            q();
        }
        this.a0 = m();
        if (this.a0) {
            this.p.setPrivateImeOptions("nm");
        }
        b(e());
    }

    public void setSubmitButtonEnabled(boolean z2) {
        this.S = z2;
        b(e());
    }

    public void setSuggestionsAdapter(b.f.a.a aVar) {
        this.R = aVar;
        this.p.setAdapter(this.R);
    }

    private static class k {

        /* renamed from: a  reason: collision with root package name */
        private Method f951a;

        /* renamed from: b  reason: collision with root package name */
        private Method f952b;

        /* renamed from: c  reason: collision with root package name */
        private Method f953c;

        k() {
            try {
                this.f951a = AutoCompleteTextView.class.getDeclaredMethod("doBeforeTextChanged", new Class[0]);
                this.f951a.setAccessible(true);
            } catch (NoSuchMethodException unused) {
            }
            try {
                this.f952b = AutoCompleteTextView.class.getDeclaredMethod("doAfterTextChanged", new Class[0]);
                this.f952b.setAccessible(true);
            } catch (NoSuchMethodException unused2) {
            }
            Class<AutoCompleteTextView> cls = AutoCompleteTextView.class;
            try {
                this.f953c = cls.getMethod("ensureImeVisible", Boolean.TYPE);
                this.f953c.setAccessible(true);
            } catch (NoSuchMethodException unused3) {
            }
        }

        /* access modifiers changed from: package-private */
        public void a(AutoCompleteTextView autoCompleteTextView) {
            Method method = this.f952b;
            if (method != null) {
                try {
                    method.invoke(autoCompleteTextView, new Object[0]);
                } catch (Exception unused) {
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void b(AutoCompleteTextView autoCompleteTextView) {
            Method method = this.f951a;
            if (method != null) {
                try {
                    method.invoke(autoCompleteTextView, new Object[0]);
                } catch (Exception unused) {
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(AutoCompleteTextView autoCompleteTextView, boolean z) {
            Method method = this.f953c;
            if (method != null) {
                try {
                    method.invoke(autoCompleteTextView, Boolean.valueOf(z));
                } catch (Exception unused) {
                }
            }
        }
    }

    public SearchView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.a.a.searchViewStyle);
    }

    private void e(int i2) {
        Editable text = this.p.getText();
        Cursor a2 = this.R.a();
        if (a2 != null) {
            if (a2.moveToPosition(i2)) {
                CharSequence b2 = this.R.b(a2);
                if (b2 != null) {
                    setQuery(b2);
                } else {
                    setQuery(text);
                }
            } else {
                setQuery(text);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, androidx.appcompat.widget.SearchView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.v0.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      androidx.appcompat.widget.v0.a(int, float):float
      androidx.appcompat.widget.v0.a(int, int):int
      androidx.appcompat.widget.v0.a(int, boolean):boolean */
    public SearchView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.z = new Rect();
        this.A = new Rect();
        this.B = new int[2];
        this.C = new int[2];
        this.h0 = new b();
        this.i0 = new c();
        this.j0 = new WeakHashMap<>();
        this.k0 = new f();
        this.l0 = new g();
        this.m0 = new h();
        this.n0 = new i();
        this.o0 = new j();
        this.p0 = new a();
        v0 a2 = v0.a(context, attributeSet, b.a.j.SearchView, i2, 0);
        LayoutInflater.from(context).inflate(a2.g(b.a.j.SearchView_layout, b.a.g.abc_search_view), (ViewGroup) this, true);
        this.p = (SearchAutoComplete) findViewById(b.a.f.search_src_text);
        this.p.setSearchView(this);
        this.q = findViewById(b.a.f.search_edit_frame);
        this.r = findViewById(b.a.f.search_plate);
        this.s = findViewById(b.a.f.submit_area);
        this.t = (ImageView) findViewById(b.a.f.search_button);
        this.u = (ImageView) findViewById(b.a.f.search_go_btn);
        this.v = (ImageView) findViewById(b.a.f.search_close_btn);
        this.w = (ImageView) findViewById(b.a.f.search_voice_btn);
        this.D = (ImageView) findViewById(b.a.f.search_mag_icon);
        u.a(this.r, a2.b(b.a.j.SearchView_queryBackground));
        u.a(this.s, a2.b(b.a.j.SearchView_submitBackground));
        this.t.setImageDrawable(a2.b(b.a.j.SearchView_searchIcon));
        this.u.setImageDrawable(a2.b(b.a.j.SearchView_goIcon));
        this.v.setImageDrawable(a2.b(b.a.j.SearchView_closeIcon));
        this.w.setImageDrawable(a2.b(b.a.j.SearchView_voiceIcon));
        this.D.setImageDrawable(a2.b(b.a.j.SearchView_searchIcon));
        this.E = a2.b(b.a.j.SearchView_searchHintIcon);
        x0.a(this.t, getResources().getString(b.a.h.abc_searchview_description_search));
        this.F = a2.g(b.a.j.SearchView_suggestionRowLayout, b.a.g.abc_search_dropdown_item_icons_2line);
        this.G = a2.g(b.a.j.SearchView_commitIcon, 0);
        this.t.setOnClickListener(this.k0);
        this.v.setOnClickListener(this.k0);
        this.u.setOnClickListener(this.k0);
        this.w.setOnClickListener(this.k0);
        this.p.setOnClickListener(this.k0);
        this.p.addTextChangedListener(this.p0);
        this.p.setOnEditorActionListener(this.m0);
        this.p.setOnItemClickListener(this.n0);
        this.p.setOnItemSelectedListener(this.o0);
        this.p.setOnKeyListener(this.l0);
        this.p.setOnFocusChangeListener(new d());
        setIconifiedByDefault(a2.a(b.a.j.SearchView_iconifiedByDefault, true));
        int c2 = a2.c(b.a.j.SearchView_android_maxWidth, -1);
        if (c2 != -1) {
            setMaxWidth(c2);
        }
        this.J = a2.e(b.a.j.SearchView_defaultQueryHint);
        this.T = a2.e(b.a.j.SearchView_queryHint);
        int d2 = a2.d(b.a.j.SearchView_android_imeOptions, -1);
        if (d2 != -1) {
            setImeOptions(d2);
        }
        int d3 = a2.d(b.a.j.SearchView_android_inputType, -1);
        if (d3 != -1) {
            setInputType(d3);
        }
        setFocusable(a2.a(b.a.j.SearchView_android_focusable, true));
        a2.a();
        this.H = new Intent("android.speech.action.WEB_SEARCH");
        this.H.addFlags(DriveFile.MODE_READ_ONLY);
        this.H.putExtra("android.speech.extra.LANGUAGE_MODEL", "web_search");
        this.I = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        this.I.addFlags(DriveFile.MODE_READ_ONLY);
        this.x = findViewById(this.p.getDropDownAnchor());
        View view = this.x;
        if (view != null) {
            view.addOnLayoutChangeListener(new e());
        }
        b(this.P);
        q();
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (Build.VERSION.SDK_INT >= 29) {
            this.p.refreshAutoCompleteResults();
            return;
        }
        q0.b(this.p);
        q0.a(this.p);
    }

    private void a(View view, Rect rect) {
        view.getLocationInWindow(this.B);
        getLocationInWindow(this.C);
        int[] iArr = this.B;
        int i2 = iArr[1];
        int[] iArr2 = this.C;
        int i3 = i2 - iArr2[1];
        int i4 = iArr[0] - iArr2[0];
        rect.set(i4, i3, view.getWidth() + i4, view.getHeight() + i3);
    }

    private void c(boolean z2) {
        int i2;
        if (!this.a0 || e() || !z2) {
            i2 = 8;
        } else {
            i2 = 0;
            this.u.setVisibility(8);
        }
        this.w.setVisibility(i2);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        int i2;
        if (this.x.getWidth() > 1) {
            Resources resources = getContext().getResources();
            int paddingLeft = this.r.getPaddingLeft();
            Rect rect = new Rect();
            boolean a2 = b1.a(this);
            int dimensionPixelSize = this.P ? resources.getDimensionPixelSize(b.a.d.abc_dropdownitem_icon_width) + resources.getDimensionPixelSize(b.a.d.abc_dropdownitem_text_padding_left) : 0;
            this.p.getDropDownBackground().getPadding(rect);
            if (a2) {
                i2 = -rect.left;
            } else {
                i2 = paddingLeft - (rect.left + dimensionPixelSize);
            }
            this.p.setDropDownHorizontalOffset(i2);
            this.p.setDropDownWidth((((this.x.getWidth() + rect.left) + rect.right) + dimensionPixelSize) - paddingLeft);
        }
    }

    private void a(boolean z2) {
        this.u.setVisibility((!this.S || !n() || !hasFocus() || (!z2 && this.a0)) ? 8 : 0);
    }

    /* access modifiers changed from: package-private */
    public void b(CharSequence charSequence) {
        Editable text = this.p.getText();
        this.c0 = text;
        boolean z2 = !TextUtils.isEmpty(text);
        a(z2);
        c(!z2);
        p();
        s();
        if (this.K != null && !TextUtils.equals(charSequence, this.b0)) {
            this.K.a(charSequence.toString());
        }
        this.b0 = charSequence.toString();
    }

    /* access modifiers changed from: package-private */
    public void a(CharSequence charSequence) {
        setQuery(charSequence);
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view, int i2, KeyEvent keyEvent) {
        int i3;
        if (this.f0 != null && this.R != null && keyEvent.getAction() == 0 && keyEvent.hasNoModifiers()) {
            if (i2 == 66 || i2 == 84 || i2 == 61) {
                return a(this.p.getListSelection(), 0, (String) null);
            }
            if (i2 == 21 || i2 == 22) {
                if (i2 == 21) {
                    i3 = 0;
                } else {
                    i3 = this.p.length();
                }
                this.p.setSelection(i3);
                this.p.setListSelection(0);
                this.p.clearListSelection();
                q0.a(this.p, true);
                return true;
            } else if (i2 != 19 || this.p.getListSelection() == 0) {
                return false;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.SearchView.a(java.lang.CharSequence, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      androidx.appcompat.widget.SearchView.a(android.content.Intent, android.app.SearchableInfo):android.content.Intent
      androidx.appcompat.widget.SearchView.a(android.view.View, android.graphics.Rect):void
      androidx.appcompat.widget.h0.a(android.view.View, int):int
      androidx.appcompat.widget.h0.a(int, int):void
      androidx.appcompat.widget.h0.a(android.graphics.Canvas, int):void
      androidx.appcompat.widget.SearchView.a(java.lang.CharSequence, boolean):void */
    public void b() {
        a((CharSequence) "", false);
        clearFocus();
        b(true);
        this.p.setImeOptions(this.e0);
        this.d0 = false;
    }

    private boolean b(int i2, int i3, String str) {
        Cursor a2 = this.R.a();
        if (a2 == null || !a2.moveToPosition(i2)) {
            return false;
        }
        a(a(a2, i3, str));
        return true;
    }

    public void a() {
        if (!this.d0) {
            this.d0 = true;
            this.e0 = this.p.getImeOptions();
            this.p.setImeOptions(this.e0 | 33554432);
            this.p.setText("");
            setIconified(false);
        }
    }

    private Intent b(Intent intent, SearchableInfo searchableInfo) {
        String str;
        Intent intent2 = new Intent(intent);
        ComponentName searchActivity = searchableInfo.getSearchActivity();
        if (searchActivity == null) {
            str = null;
        } else {
            str = searchActivity.flattenToShortString();
        }
        intent2.putExtra("calling_package", str);
        return intent2;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, int i3, String str) {
        n nVar = this.N;
        if (nVar != null && nVar.b(i2)) {
            return false;
        }
        b(i2, 0, null);
        this.p.setImeVisibility(false);
        l();
        return true;
    }

    private void a(Intent intent) {
        if (intent != null) {
            try {
                getContext().startActivity(intent);
            } catch (RuntimeException e2) {
                Log.e("SearchView", "Failed launch activity: " + intent, e2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, String str, String str2) {
        getContext().startActivity(a("android.intent.action.SEARCH", (Uri) null, (String) null, str2, i2, str));
    }

    private Intent a(String str, Uri uri, String str2, String str3, int i2, String str4) {
        Intent intent = new Intent(str);
        intent.addFlags(DriveFile.MODE_READ_ONLY);
        if (uri != null) {
            intent.setData(uri);
        }
        intent.putExtra("user_query", this.c0);
        if (str3 != null) {
            intent.putExtra("query", str3);
        }
        if (str2 != null) {
            intent.putExtra("intent_extra_data_key", str2);
        }
        Bundle bundle = this.g0;
        if (bundle != null) {
            intent.putExtra("app_data", bundle);
        }
        if (i2 != 0) {
            intent.putExtra("action_key", i2);
            intent.putExtra("action_msg", str4);
        }
        intent.setComponent(this.f0.getSearchActivity());
        return intent;
    }

    private Intent a(Intent intent, SearchableInfo searchableInfo) {
        ComponentName searchActivity = searchableInfo.getSearchActivity();
        Intent intent2 = new Intent("android.intent.action.SEARCH");
        intent2.setComponent(searchActivity);
        PendingIntent activity = PendingIntent.getActivity(getContext(), 0, intent2, 1073741824);
        Bundle bundle = new Bundle();
        Bundle bundle2 = this.g0;
        if (bundle2 != null) {
            bundle.putParcelable("app_data", bundle2);
        }
        Intent intent3 = new Intent(intent);
        int i2 = 1;
        Resources resources = getResources();
        String string = searchableInfo.getVoiceLanguageModeId() != 0 ? resources.getString(searchableInfo.getVoiceLanguageModeId()) : "free_form";
        String str = null;
        String string2 = searchableInfo.getVoicePromptTextId() != 0 ? resources.getString(searchableInfo.getVoicePromptTextId()) : null;
        String string3 = searchableInfo.getVoiceLanguageId() != 0 ? resources.getString(searchableInfo.getVoiceLanguageId()) : null;
        if (searchableInfo.getVoiceMaxResults() != 0) {
            i2 = searchableInfo.getVoiceMaxResults();
        }
        intent3.putExtra("android.speech.extra.LANGUAGE_MODEL", string);
        intent3.putExtra("android.speech.extra.PROMPT", string2);
        intent3.putExtra("android.speech.extra.LANGUAGE", string3);
        intent3.putExtra("android.speech.extra.MAX_RESULTS", i2);
        if (searchActivity != null) {
            str = searchActivity.flattenToShortString();
        }
        intent3.putExtra("calling_package", str);
        intent3.putExtra("android.speech.extra.RESULTS_PENDINGINTENT", activity);
        intent3.putExtra("android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE", bundle);
        return intent3;
    }

    private Intent a(Cursor cursor, int i2, String str) {
        int i3;
        Uri uri;
        String a2;
        try {
            String a3 = p0.a(cursor, "suggest_intent_action");
            if (a3 == null) {
                a3 = this.f0.getSuggestIntentAction();
            }
            if (a3 == null) {
                a3 = "android.intent.action.SEARCH";
            }
            String str2 = a3;
            String a4 = p0.a(cursor, "suggest_intent_data");
            if (a4 == null) {
                a4 = this.f0.getSuggestIntentData();
            }
            if (!(a4 == null || (a2 = p0.a(cursor, "suggest_intent_data_id")) == null)) {
                a4 = a4 + Constants.URL_PATH_DELIMITER + Uri.encode(a2);
            }
            if (a4 == null) {
                uri = null;
            } else {
                uri = Uri.parse(a4);
            }
            return a(str2, uri, p0.a(cursor, "suggest_intent_extra_data"), p0.a(cursor, "suggest_intent_query"), i2, str);
        } catch (RuntimeException e2) {
            try {
                i3 = cursor.getPosition();
            } catch (RuntimeException unused) {
                i3 = -1;
            }
            Log.w("SearchView", "Search suggestions cursor at row " + i3 + " returned exception.", e2);
            return null;
        }
    }

    static boolean a(Context context) {
        return context.getResources().getConfiguration().orientation == 2;
    }
}
