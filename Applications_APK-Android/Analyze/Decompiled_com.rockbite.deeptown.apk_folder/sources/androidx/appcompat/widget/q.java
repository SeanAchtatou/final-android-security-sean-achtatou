package androidx.appcompat.widget;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.PopupWindow;
import androidx.core.widget.h;
import b.a.j;

/* compiled from: AppCompatPopupWindow */
class q extends PopupWindow {

    /* renamed from: b  reason: collision with root package name */
    private static final boolean f1155b = (Build.VERSION.SDK_INT < 21);

    /* renamed from: a  reason: collision with root package name */
    private boolean f1156a;

    public q(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        a(context, attributeSet, i2, i3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.v0.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      androidx.appcompat.widget.v0.a(int, float):float
      androidx.appcompat.widget.v0.a(int, int):int
      androidx.appcompat.widget.v0.a(int, boolean):boolean */
    private void a(Context context, AttributeSet attributeSet, int i2, int i3) {
        v0 a2 = v0.a(context, attributeSet, j.PopupWindow, i2, i3);
        if (a2.g(j.PopupWindow_overlapAnchor)) {
            a(a2.a(j.PopupWindow_overlapAnchor, false));
        }
        setBackgroundDrawable(a2.b(j.PopupWindow_android_popupBackground));
        a2.a();
    }

    public void showAsDropDown(View view, int i2, int i3) {
        if (f1155b && this.f1156a) {
            i3 -= view.getHeight();
        }
        super.showAsDropDown(view, i2, i3);
    }

    public void update(View view, int i2, int i3, int i4, int i5) {
        if (f1155b && this.f1156a) {
            i3 -= view.getHeight();
        }
        super.update(view, i2, i3, i4, i5);
    }

    public void showAsDropDown(View view, int i2, int i3, int i4) {
        if (f1155b && this.f1156a) {
            i3 -= view.getHeight();
        }
        super.showAsDropDown(view, i2, i3, i4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.core.widget.h.a(android.widget.PopupWindow, boolean):void
     arg types: [androidx.appcompat.widget.q, boolean]
     candidates:
      androidx.core.widget.h.a(android.widget.PopupWindow, int):void
      androidx.core.widget.h.a(android.widget.PopupWindow, boolean):void */
    private void a(boolean z) {
        if (f1155b) {
            this.f1156a = z;
        } else {
            h.a((PopupWindow) this, z);
        }
    }
}
