package androidx.appcompat.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;
import androidx.core.widget.e;
import b.a.j;
import b.a.k.a.a;

/* compiled from: AppCompatImageHelper */
public class n {

    /* renamed from: a  reason: collision with root package name */
    private final ImageView f1114a;

    /* renamed from: b  reason: collision with root package name */
    private t0 f1115b;

    /* renamed from: c  reason: collision with root package name */
    private t0 f1116c;

    /* renamed from: d  reason: collision with root package name */
    private t0 f1117d;

    public n(ImageView imageView) {
        this.f1114a = imageView;
    }

    private boolean e() {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 <= 21) {
            return i2 == 21;
        }
        if (this.f1115b != null) {
            return true;
        }
        return false;
    }

    public void a(AttributeSet attributeSet, int i2) {
        int g2;
        v0 a2 = v0.a(this.f1114a.getContext(), attributeSet, j.AppCompatImageView, i2, 0);
        try {
            Drawable drawable = this.f1114a.getDrawable();
            if (!(drawable != null || (g2 = a2.g(j.AppCompatImageView_srcCompat, -1)) == -1 || (drawable = a.c(this.f1114a.getContext(), g2)) == null)) {
                this.f1114a.setImageDrawable(drawable);
            }
            if (drawable != null) {
                d0.b(drawable);
            }
            if (a2.g(j.AppCompatImageView_tint)) {
                e.a(this.f1114a, a2.a(j.AppCompatImageView_tint));
            }
            if (a2.g(j.AppCompatImageView_tintMode)) {
                e.a(this.f1114a, d0.a(a2.d(j.AppCompatImageView_tintMode, -1), null));
            }
        } finally {
            a2.a();
        }
    }

    /* access modifiers changed from: package-private */
    public ColorStateList b() {
        t0 t0Var = this.f1116c;
        if (t0Var != null) {
            return t0Var.f1175a;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public PorterDuff.Mode c() {
        t0 t0Var = this.f1116c;
        if (t0Var != null) {
            return t0Var.f1176b;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return Build.VERSION.SDK_INT < 21 || !(this.f1114a.getBackground() instanceof RippleDrawable);
    }

    public void a(int i2) {
        if (i2 != 0) {
            Drawable c2 = a.c(this.f1114a.getContext(), i2);
            if (c2 != null) {
                d0.b(c2);
            }
            this.f1114a.setImageDrawable(c2);
        } else {
            this.f1114a.setImageDrawable(null);
        }
        a();
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        if (this.f1116c == null) {
            this.f1116c = new t0();
        }
        t0 t0Var = this.f1116c;
        t0Var.f1175a = colorStateList;
        t0Var.f1178d = true;
        a();
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        if (this.f1116c == null) {
            this.f1116c = new t0();
        }
        t0 t0Var = this.f1116c;
        t0Var.f1176b = mode;
        t0Var.f1177c = true;
        a();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        Drawable drawable = this.f1114a.getDrawable();
        if (drawable != null) {
            d0.b(drawable);
        }
        if (drawable == null) {
            return;
        }
        if (!e() || !a(drawable)) {
            t0 t0Var = this.f1116c;
            if (t0Var != null) {
                j.a(drawable, t0Var, this.f1114a.getDrawableState());
                return;
            }
            t0 t0Var2 = this.f1115b;
            if (t0Var2 != null) {
                j.a(drawable, t0Var2, this.f1114a.getDrawableState());
            }
        }
    }

    private boolean a(Drawable drawable) {
        if (this.f1117d == null) {
            this.f1117d = new t0();
        }
        t0 t0Var = this.f1117d;
        t0Var.a();
        ColorStateList a2 = e.a(this.f1114a);
        if (a2 != null) {
            t0Var.f1178d = true;
            t0Var.f1175a = a2;
        }
        PorterDuff.Mode b2 = e.b(this.f1114a);
        if (b2 != null) {
            t0Var.f1177c = true;
            t0Var.f1176b = b2;
        }
        if (!t0Var.f1178d && !t0Var.f1177c) {
            return false;
        }
        j.a(drawable, t0Var, this.f1114a.getDrawableState());
        return true;
    }
}
