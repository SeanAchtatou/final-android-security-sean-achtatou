package androidx.appcompat.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.appcompat.view.menu.g;
import androidx.appcompat.view.menu.n;
import androidx.appcompat.widget.Toolbar;
import b.a.e;
import b.a.f;
import b.a.h;
import b.a.j;
import b.e.m.a0;
import b.e.m.u;
import b.e.m.y;
import com.esotericsoftware.spine.Animation;

/* compiled from: ToolbarWidgetWrapper */
public class w0 implements c0 {

    /* renamed from: a  reason: collision with root package name */
    Toolbar f1192a;

    /* renamed from: b  reason: collision with root package name */
    private int f1193b;

    /* renamed from: c  reason: collision with root package name */
    private View f1194c;

    /* renamed from: d  reason: collision with root package name */
    private View f1195d;

    /* renamed from: e  reason: collision with root package name */
    private Drawable f1196e;

    /* renamed from: f  reason: collision with root package name */
    private Drawable f1197f;

    /* renamed from: g  reason: collision with root package name */
    private Drawable f1198g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f1199h;

    /* renamed from: i  reason: collision with root package name */
    CharSequence f1200i;

    /* renamed from: j  reason: collision with root package name */
    private CharSequence f1201j;

    /* renamed from: k  reason: collision with root package name */
    private CharSequence f1202k;
    Window.Callback l;
    boolean m;
    private c n;
    private int o;
    private int p;
    private Drawable q;

    /* compiled from: ToolbarWidgetWrapper */
    class a implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        final androidx.appcompat.view.menu.a f1203a = new androidx.appcompat.view.menu.a(w0.this.f1192a.getContext(), 0, 16908332, 0, 0, w0.this.f1200i);

        a() {
        }

        public void onClick(View view) {
            w0 w0Var = w0.this;
            Window.Callback callback = w0Var.l;
            if (callback != null && w0Var.m) {
                callback.onMenuItemSelected(0, this.f1203a);
            }
        }
    }

    /* compiled from: ToolbarWidgetWrapper */
    class b extends a0 {

        /* renamed from: a  reason: collision with root package name */
        private boolean f1205a = false;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ int f1206b;

        b(int i2) {
            this.f1206b = i2;
        }

        public void a(View view) {
            this.f1205a = true;
        }

        public void b(View view) {
            if (!this.f1205a) {
                w0.this.f1192a.setVisibility(this.f1206b);
            }
        }

        public void c(View view) {
            w0.this.f1192a.setVisibility(0);
        }
    }

    public w0(Toolbar toolbar, boolean z) {
        this(toolbar, z, h.abc_action_bar_up_description, e.abc_ic_ab_back_material);
    }

    private int n() {
        if (this.f1192a.getNavigationIcon() == null) {
            return 11;
        }
        this.q = this.f1192a.getNavigationIcon();
        return 15;
    }

    private void o() {
        if ((this.f1193b & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.f1202k)) {
            this.f1192a.setNavigationContentDescription(this.p);
        } else {
            this.f1192a.setNavigationContentDescription(this.f1202k);
        }
    }

    private void p() {
        if ((this.f1193b & 4) != 0) {
            Toolbar toolbar = this.f1192a;
            Drawable drawable = this.f1198g;
            if (drawable == null) {
                drawable = this.q;
            }
            toolbar.setNavigationIcon(drawable);
            return;
        }
        this.f1192a.setNavigationIcon((Drawable) null);
    }

    private void q() {
        Drawable drawable;
        int i2 = this.f1193b;
        if ((i2 & 2) == 0) {
            drawable = null;
        } else if ((i2 & 1) != 0) {
            drawable = this.f1197f;
            if (drawable == null) {
                drawable = this.f1196e;
            }
        } else {
            drawable = this.f1196e;
        }
        this.f1192a.setLogo(drawable);
    }

    public void a(Drawable drawable) {
        this.f1197f = drawable;
        q();
    }

    public void a(boolean z) {
    }

    public void b(CharSequence charSequence) {
        this.f1201j = charSequence;
        if ((this.f1193b & 8) != 0) {
            this.f1192a.setSubtitle(charSequence);
        }
    }

    public void c(CharSequence charSequence) {
        this.f1199h = true;
        d(charSequence);
    }

    public void collapseActionView() {
        this.f1192a.c();
    }

    public void d(int i2) {
        if (i2 != this.p) {
            this.p = i2;
            if (TextUtils.isEmpty(this.f1192a.getNavigationContentDescription())) {
                e(this.p);
            }
        }
    }

    public boolean e() {
        return this.f1192a.g();
    }

    public boolean f() {
        return this.f1192a.k();
    }

    public void g() {
        this.f1192a.d();
    }

    public Context getContext() {
        return this.f1192a.getContext();
    }

    public CharSequence getTitle() {
        return this.f1192a.getTitle();
    }

    public boolean h() {
        return this.f1192a.f();
    }

    public int i() {
        return this.o;
    }

    public ViewGroup j() {
        return this.f1192a;
    }

    public int k() {
        return this.f1193b;
    }

    public void l() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public void m() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public void setIcon(int i2) {
        setIcon(i2 != 0 ? b.a.k.a.a.c(getContext(), i2) : null);
    }

    public void setWindowCallback(Window.Callback callback) {
        this.l = callback;
    }

    public void setWindowTitle(CharSequence charSequence) {
        if (!this.f1199h) {
            d(charSequence);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, androidx.appcompat.widget.Toolbar, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public w0(Toolbar toolbar, boolean z, int i2, int i3) {
        Drawable drawable;
        this.o = 0;
        this.p = 0;
        this.f1192a = toolbar;
        this.f1200i = toolbar.getTitle();
        this.f1201j = toolbar.getSubtitle();
        this.f1199h = this.f1200i != null;
        this.f1198g = toolbar.getNavigationIcon();
        v0 a2 = v0.a(toolbar.getContext(), null, j.ActionBar, b.a.a.actionBarStyle, 0);
        this.q = a2.b(j.ActionBar_homeAsUpIndicator);
        if (z) {
            CharSequence e2 = a2.e(j.ActionBar_title);
            if (!TextUtils.isEmpty(e2)) {
                c(e2);
            }
            CharSequence e3 = a2.e(j.ActionBar_subtitle);
            if (!TextUtils.isEmpty(e3)) {
                b(e3);
            }
            Drawable b2 = a2.b(j.ActionBar_logo);
            if (b2 != null) {
                a(b2);
            }
            Drawable b3 = a2.b(j.ActionBar_icon);
            if (b3 != null) {
                setIcon(b3);
            }
            if (this.f1198g == null && (drawable = this.q) != null) {
                b(drawable);
            }
            a(a2.d(j.ActionBar_displayOptions, 0));
            int g2 = a2.g(j.ActionBar_customNavigationLayout, 0);
            if (g2 != 0) {
                a(LayoutInflater.from(this.f1192a.getContext()).inflate(g2, (ViewGroup) this.f1192a, false));
                a(this.f1193b | 16);
            }
            int f2 = a2.f(j.ActionBar_height, 0);
            if (f2 > 0) {
                ViewGroup.LayoutParams layoutParams = this.f1192a.getLayoutParams();
                layoutParams.height = f2;
                this.f1192a.setLayoutParams(layoutParams);
            }
            int b4 = a2.b(j.ActionBar_contentInsetStart, -1);
            int b5 = a2.b(j.ActionBar_contentInsetEnd, -1);
            if (b4 >= 0 || b5 >= 0) {
                this.f1192a.a(Math.max(b4, 0), Math.max(b5, 0));
            }
            int g3 = a2.g(j.ActionBar_titleTextStyle, 0);
            if (g3 != 0) {
                Toolbar toolbar2 = this.f1192a;
                toolbar2.b(toolbar2.getContext(), g3);
            }
            int g4 = a2.g(j.ActionBar_subtitleTextStyle, 0);
            if (g4 != 0) {
                Toolbar toolbar3 = this.f1192a;
                toolbar3.a(toolbar3.getContext(), g4);
            }
            int g5 = a2.g(j.ActionBar_popupTheme, 0);
            if (g5 != 0) {
                this.f1192a.setPopupTheme(g5);
            }
        } else {
            this.f1193b = n();
        }
        a2.a();
        d(i2);
        this.f1202k = this.f1192a.getNavigationContentDescription();
        this.f1192a.setNavigationOnClickListener(new a());
    }

    public void e(int i2) {
        a(i2 == 0 ? null : getContext().getString(i2));
    }

    public void setIcon(Drawable drawable) {
        this.f1196e = drawable;
        q();
    }

    public boolean a() {
        return this.f1192a.i();
    }

    public boolean c() {
        return this.f1192a.b();
    }

    public void a(Menu menu, n.a aVar) {
        if (this.n == null) {
            this.n = new c(this.f1192a.getContext());
            this.n.a(f.action_menu_presenter);
        }
        this.n.a(aVar);
        this.f1192a.a((g) menu, this.n);
    }

    public void b(int i2) {
        a(i2 != 0 ? b.a.k.a.a.c(getContext(), i2) : null);
    }

    public void c(int i2) {
        this.f1192a.setVisibility(i2);
    }

    private void d(CharSequence charSequence) {
        this.f1200i = charSequence;
        if ((this.f1193b & 8) != 0) {
            this.f1192a.setTitle(charSequence);
        }
    }

    public void b() {
        this.m = true;
    }

    public void b(boolean z) {
        this.f1192a.setCollapsible(z);
    }

    public void b(Drawable drawable) {
        this.f1198g = drawable;
        p();
    }

    public boolean d() {
        return this.f1192a.h();
    }

    public void a(int i2) {
        View view;
        int i3 = this.f1193b ^ i2;
        this.f1193b = i2;
        if (i3 != 0) {
            if ((i3 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    o();
                }
                p();
            }
            if ((i3 & 3) != 0) {
                q();
            }
            if ((i3 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    this.f1192a.setTitle(this.f1200i);
                    this.f1192a.setSubtitle(this.f1201j);
                } else {
                    this.f1192a.setTitle((CharSequence) null);
                    this.f1192a.setSubtitle((CharSequence) null);
                }
            }
            if ((i3 & 16) != 0 && (view = this.f1195d) != null) {
                if ((i2 & 16) != 0) {
                    this.f1192a.addView(view);
                } else {
                    this.f1192a.removeView(view);
                }
            }
        }
    }

    public void a(o0 o0Var) {
        Toolbar toolbar;
        View view = this.f1194c;
        if (view != null && view.getParent() == (toolbar = this.f1192a)) {
            toolbar.removeView(this.f1194c);
        }
        this.f1194c = o0Var;
        if (o0Var != null && this.o == 2) {
            this.f1192a.addView(this.f1194c, 0);
            Toolbar.e eVar = (Toolbar.e) this.f1194c.getLayoutParams();
            eVar.width = -2;
            eVar.height = -2;
            eVar.f624a = 8388691;
            o0Var.setAllowCollapse(true);
        }
    }

    public void a(View view) {
        View view2 = this.f1195d;
        if (!(view2 == null || (this.f1193b & 16) == 0)) {
            this.f1192a.removeView(view2);
        }
        this.f1195d = view;
        if (view != null && (this.f1193b & 16) != 0) {
            this.f1192a.addView(this.f1195d);
        }
    }

    public y a(int i2, long j2) {
        y a2 = u.a(this.f1192a);
        a2.a(i2 == 0 ? 1.0f : Animation.CurveTimeline.LINEAR);
        a2.a(j2);
        a2.a(new b(i2));
        return a2;
    }

    public void a(CharSequence charSequence) {
        this.f1202k = charSequence;
        o();
    }
}
