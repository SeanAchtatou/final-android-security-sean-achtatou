package androidx.appcompat.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class ActivityChooserView$InnerLayout extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f898a = {16842964};

    public ActivityChooserView$InnerLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        v0 a2 = v0.a(context, attributeSet, f898a);
        setBackgroundDrawable(a2.b(0));
        a2.a();
    }
}
