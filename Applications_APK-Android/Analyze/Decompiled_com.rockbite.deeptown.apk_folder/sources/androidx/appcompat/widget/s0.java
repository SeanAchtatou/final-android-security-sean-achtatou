package androidx.appcompat.widget;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: TintContextWrapper */
public class s0 extends ContextWrapper {

    /* renamed from: c  reason: collision with root package name */
    private static final Object f1170c = new Object();

    /* renamed from: d  reason: collision with root package name */
    private static ArrayList<WeakReference<s0>> f1171d;

    /* renamed from: a  reason: collision with root package name */
    private final Resources f1172a;

    /* renamed from: b  reason: collision with root package name */
    private final Resources.Theme f1173b;

    private s0(Context context) {
        super(context);
        if (a1.b()) {
            this.f1172a = new a1(this, context.getResources());
            this.f1173b = this.f1172a.newTheme();
            this.f1173b.setTo(context.getTheme());
            return;
        }
        this.f1172a = new u0(this, context.getResources());
        this.f1173b = null;
    }

    private static boolean a(Context context) {
        if ((context instanceof s0) || (context.getResources() instanceof u0) || (context.getResources() instanceof a1)) {
            return false;
        }
        if (Build.VERSION.SDK_INT < 21 || a1.b()) {
            return true;
        }
        return false;
    }

    public static Context b(Context context) {
        if (!a(context)) {
            return context;
        }
        synchronized (f1170c) {
            if (f1171d == null) {
                f1171d = new ArrayList<>();
            } else {
                for (int size = f1171d.size() - 1; size >= 0; size--) {
                    WeakReference weakReference = f1171d.get(size);
                    if (weakReference == null || weakReference.get() == null) {
                        f1171d.remove(size);
                    }
                }
                for (int size2 = f1171d.size() - 1; size2 >= 0; size2--) {
                    WeakReference weakReference2 = f1171d.get(size2);
                    s0 s0Var = weakReference2 != null ? (s0) weakReference2.get() : null;
                    if (s0Var != null && s0Var.getBaseContext() == context) {
                        return s0Var;
                    }
                }
            }
            s0 s0Var2 = new s0(context);
            f1171d.add(new WeakReference(s0Var2));
            return s0Var2;
        }
    }

    public AssetManager getAssets() {
        return this.f1172a.getAssets();
    }

    public Resources getResources() {
        return this.f1172a;
    }

    public Resources.Theme getTheme() {
        Resources.Theme theme = this.f1173b;
        return theme == null ? super.getTheme() : theme;
    }

    public void setTheme(int i2) {
        Resources.Theme theme = this.f1173b;
        if (theme == null) {
            super.setTheme(i2);
        } else {
            theme.applyStyle(i2, true);
        }
    }
}
