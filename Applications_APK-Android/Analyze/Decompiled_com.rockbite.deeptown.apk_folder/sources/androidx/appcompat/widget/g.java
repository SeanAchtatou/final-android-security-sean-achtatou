package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.CheckBox;
import androidx.core.widget.j;
import b.a.a;
import b.e.m.t;

/* compiled from: AppCompatCheckBox */
public class g extends CheckBox implements j, t {

    /* renamed from: a  reason: collision with root package name */
    private final i f1036a;

    /* renamed from: b  reason: collision with root package name */
    private final e f1037b;

    /* renamed from: c  reason: collision with root package name */
    private final x f1038c;

    public g(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.checkboxStyle);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        e eVar = this.f1037b;
        if (eVar != null) {
            eVar.a();
        }
        x xVar = this.f1038c;
        if (xVar != null) {
            xVar.a();
        }
    }

    public int getCompoundPaddingLeft() {
        int compoundPaddingLeft = super.getCompoundPaddingLeft();
        i iVar = this.f1036a;
        return iVar != null ? iVar.a(compoundPaddingLeft) : compoundPaddingLeft;
    }

    public ColorStateList getSupportBackgroundTintList() {
        e eVar = this.f1037b;
        if (eVar != null) {
            return eVar.b();
        }
        return null;
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        e eVar = this.f1037b;
        if (eVar != null) {
            return eVar.c();
        }
        return null;
    }

    public ColorStateList getSupportButtonTintList() {
        i iVar = this.f1036a;
        if (iVar != null) {
            return iVar.b();
        }
        return null;
    }

    public PorterDuff.Mode getSupportButtonTintMode() {
        i iVar = this.f1036a;
        if (iVar != null) {
            return iVar.c();
        }
        return null;
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        e eVar = this.f1037b;
        if (eVar != null) {
            eVar.a(drawable);
        }
    }

    public void setBackgroundResource(int i2) {
        super.setBackgroundResource(i2);
        e eVar = this.f1037b;
        if (eVar != null) {
            eVar.a(i2);
        }
    }

    public void setButtonDrawable(Drawable drawable) {
        super.setButtonDrawable(drawable);
        i iVar = this.f1036a;
        if (iVar != null) {
            iVar.d();
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        e eVar = this.f1037b;
        if (eVar != null) {
            eVar.b(colorStateList);
        }
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        e eVar = this.f1037b;
        if (eVar != null) {
            eVar.a(mode);
        }
    }

    public void setSupportButtonTintList(ColorStateList colorStateList) {
        i iVar = this.f1036a;
        if (iVar != null) {
            iVar.a(colorStateList);
        }
    }

    public void setSupportButtonTintMode(PorterDuff.Mode mode) {
        i iVar = this.f1036a;
        if (iVar != null) {
            iVar.a(mode);
        }
    }

    public g(Context context, AttributeSet attributeSet, int i2) {
        super(s0.b(context), attributeSet, i2);
        this.f1036a = new i(this);
        this.f1036a.a(attributeSet, i2);
        this.f1037b = new e(this);
        this.f1037b.a(attributeSet, i2);
        this.f1038c = new x(this);
        this.f1038c.a(attributeSet, i2);
    }

    public void setButtonDrawable(int i2) {
        setButtonDrawable(b.a.k.a.a.c(getContext(), i2));
    }
}
