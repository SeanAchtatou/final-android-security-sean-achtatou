package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.view.menu.g;
import androidx.appcompat.view.menu.j;
import androidx.appcompat.view.menu.m;
import androidx.appcompat.view.menu.n;
import androidx.appcompat.view.menu.o;
import androidx.appcompat.view.menu.q;
import androidx.appcompat.view.menu.s;
import androidx.appcompat.widget.ActionMenuView;
import b.e.m.b;
import java.util.ArrayList;

/* compiled from: ActionMenuPresenter */
class c extends androidx.appcompat.view.menu.b implements b.a {
    final f A = new f();
    int B;

    /* renamed from: i  reason: collision with root package name */
    d f1001i;

    /* renamed from: j  reason: collision with root package name */
    private Drawable f1002j;

    /* renamed from: k  reason: collision with root package name */
    private boolean f1003k;
    private boolean l;
    private boolean m;
    private int n;
    private int o;
    private int p;
    private boolean q;
    private boolean r;
    private boolean s;
    private boolean t;
    private int u;
    private final SparseBooleanArray v = new SparseBooleanArray();
    e w;
    a x;
    C0008c y;
    private b z;

    /* compiled from: ActionMenuPresenter */
    private class a extends m {
        public a(Context context, s sVar, View view) {
            super(context, sVar, view, false, b.a.a.actionOverflowMenuStyle);
            if (!((j) sVar.getItem()).h()) {
                View view2 = c.this.f1001i;
                a(view2 == null ? (View) c.this.f758h : view2);
            }
            a(c.this.A);
        }

        /* access modifiers changed from: protected */
        public void d() {
            c cVar = c.this;
            cVar.x = null;
            cVar.B = 0;
            super.d();
        }
    }

    /* compiled from: ActionMenuPresenter */
    private class b extends ActionMenuItemView.b {
        b() {
        }

        public q a() {
            a aVar = c.this.x;
            if (aVar != null) {
                return aVar.b();
            }
            return null;
        }
    }

    /* renamed from: androidx.appcompat.widget.c$c  reason: collision with other inner class name */
    /* compiled from: ActionMenuPresenter */
    private class C0008c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private e f1005a;

        public C0008c(e eVar) {
            this.f1005a = eVar;
        }

        public void run() {
            if (c.this.f753c != null) {
                c.this.f753c.a();
            }
            View view = (View) c.this.f758h;
            if (!(view == null || view.getWindowToken() == null || !this.f1005a.f())) {
                c.this.w = this.f1005a;
            }
            c.this.y = null;
        }
    }

    /* compiled from: ActionMenuPresenter */
    private class d extends o implements ActionMenuView.a {

        /* compiled from: ActionMenuPresenter */
        class a extends g0 {
            a(View view, c cVar) {
                super(view);
            }

            public q a() {
                e eVar = c.this.w;
                if (eVar == null) {
                    return null;
                }
                return eVar.b();
            }

            public boolean b() {
                c.this.i();
                return true;
            }

            public boolean c() {
                c cVar = c.this;
                if (cVar.y != null) {
                    return false;
                }
                cVar.e();
                return true;
            }
        }

        public d(Context context) {
            super(context, null, b.a.a.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            x0.a(this, getContentDescription());
            setOnTouchListener(new a(this, c.this));
        }

        public boolean a() {
            return false;
        }

        public boolean b() {
            return false;
        }

        public boolean performClick() {
            if (super.performClick()) {
                return true;
            }
            playSoundEffect(0);
            c.this.i();
            return true;
        }

        /* access modifiers changed from: protected */
        public boolean setFrame(int i2, int i3, int i4, int i5) {
            boolean frame = super.setFrame(i2, i3, i4, i5);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                androidx.core.graphics.drawable.a.a(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    /* compiled from: ActionMenuPresenter */
    private class e extends m {
        public e(Context context, g gVar, View view, boolean z) {
            super(context, gVar, view, z, b.a.a.actionOverflowMenuStyle);
            a(8388613);
            a(c.this.A);
        }

        /* access modifiers changed from: protected */
        public void d() {
            if (c.this.f753c != null) {
                c.this.f753c.close();
            }
            c.this.w = null;
            super.d();
        }
    }

    public c(Context context) {
        super(context, b.a.g.abc_action_menu_layout, b.a.g.abc_action_menu_item_layout);
    }

    public boolean g() {
        return this.y != null || h();
    }

    public boolean h() {
        e eVar = this.w;
        return eVar != null && eVar.c();
    }

    public boolean i() {
        g gVar;
        if (!this.l || h() || (gVar = this.f753c) == null || this.f758h == null || this.y != null || gVar.j().isEmpty()) {
            return false;
        }
        this.y = new C0008c(new e(this.f752b, this.f753c, this.f1001i, true));
        ((View) this.f758h).post(this.y);
        super.a((s) null);
        return true;
    }

    public void a(Context context, g gVar) {
        super.a(context, gVar);
        Resources resources = context.getResources();
        b.a.n.a a2 = b.a.n.a.a(context);
        if (!this.m) {
            this.l = a2.g();
        }
        if (!this.s) {
            this.n = a2.b();
        }
        if (!this.q) {
            this.p = a2.c();
        }
        int i2 = this.n;
        if (this.l) {
            if (this.f1001i == null) {
                this.f1001i = new d(this.f751a);
                if (this.f1003k) {
                    this.f1001i.setImageDrawable(this.f1002j);
                    this.f1002j = null;
                    this.f1003k = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.f1001i.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i2 -= this.f1001i.getMeasuredWidth();
        } else {
            this.f1001i = null;
        }
        this.o = i2;
        this.u = (int) (resources.getDisplayMetrics().density * 56.0f);
    }

    public void b(boolean z2) {
        this.t = z2;
    }

    public void c(boolean z2) {
        this.l = z2;
        this.m = true;
    }

    public Drawable d() {
        d dVar = this.f1001i;
        if (dVar != null) {
            return dVar.getDrawable();
        }
        if (this.f1003k) {
            return this.f1002j;
        }
        return null;
    }

    public boolean e() {
        o oVar;
        C0008c cVar = this.y;
        if (cVar == null || (oVar = this.f758h) == null) {
            e eVar = this.w;
            if (eVar == null) {
                return false;
            }
            eVar.a();
            return true;
        }
        ((View) oVar).removeCallbacks(cVar);
        this.y = null;
        return true;
    }

    public boolean f() {
        a aVar = this.x;
        if (aVar == null) {
            return false;
        }
        aVar.a();
        return true;
    }

    /* compiled from: ActionMenuPresenter */
    private class f implements n.a {
        f() {
        }

        public boolean a(g gVar) {
            if (gVar == null) {
                return false;
            }
            c.this.B = ((s) gVar).getItem().getItemId();
            n.a b2 = c.this.b();
            if (b2 != null) {
                return b2.a(gVar);
            }
            return false;
        }

        public void a(g gVar, boolean z) {
            if (gVar instanceof s) {
                gVar.m().a(false);
            }
            n.a b2 = c.this.b();
            if (b2 != null) {
                b2.a(gVar, z);
            }
        }
    }

    public o b(ViewGroup viewGroup) {
        o oVar = this.f758h;
        o b2 = super.b(viewGroup);
        if (oVar != b2) {
            ((ActionMenuView) b2).setPresenter(this);
        }
        return b2;
    }

    public boolean c() {
        return e() | f();
    }

    public void a(Configuration configuration) {
        if (!this.q) {
            this.p = b.a.n.a.a(this.f752b).c();
        }
        g gVar = this.f753c;
        if (gVar != null) {
            gVar.b(true);
        }
    }

    public void a(Drawable drawable) {
        d dVar = this.f1001i;
        if (dVar != null) {
            dVar.setImageDrawable(drawable);
            return;
        }
        this.f1003k = true;
        this.f1002j = drawable;
    }

    public View a(j jVar, View view, ViewGroup viewGroup) {
        View actionView = jVar.getActionView();
        if (actionView == null || jVar.f()) {
            actionView = super.a(jVar, view, viewGroup);
        }
        actionView.setVisibility(jVar.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    public void a(j jVar, o.a aVar) {
        aVar.a(jVar, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) aVar;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.f758h);
        if (this.z == null) {
            this.z = new b();
        }
        actionMenuItemView.setPopupCallback(this.z);
    }

    public boolean a(int i2, j jVar) {
        return jVar.h();
    }

    public void a(boolean z2) {
        o oVar;
        super.a(z2);
        ((View) this.f758h).requestLayout();
        g gVar = this.f753c;
        boolean z3 = false;
        if (gVar != null) {
            ArrayList<j> c2 = gVar.c();
            int size = c2.size();
            for (int i2 = 0; i2 < size; i2++) {
                b.e.m.b a2 = c2.get(i2).a();
                if (a2 != null) {
                    a2.a(this);
                }
            }
        }
        g gVar2 = this.f753c;
        ArrayList<j> j2 = gVar2 != null ? gVar2.j() : null;
        if (this.l && j2 != null) {
            int size2 = j2.size();
            if (size2 == 1) {
                z3 = !((j) j2.get(0)).isActionViewExpanded();
            } else if (size2 > 0) {
                z3 = true;
            }
        }
        if (z3) {
            if (this.f1001i == null) {
                this.f1001i = new d(this.f751a);
            }
            ViewGroup viewGroup = (ViewGroup) this.f1001i.getParent();
            if (viewGroup != this.f758h) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.f1001i);
                }
                ActionMenuView actionMenuView = (ActionMenuView) this.f758h;
                actionMenuView.addView(this.f1001i, actionMenuView.d());
            }
        } else {
            d dVar = this.f1001i;
            if (dVar != null && dVar.getParent() == (oVar = this.f758h)) {
                ((ViewGroup) oVar).removeView(this.f1001i);
            }
        }
        ((ActionMenuView) this.f758h).setOverflowReserved(this.l);
    }

    public boolean a(ViewGroup viewGroup, int i2) {
        if (viewGroup.getChildAt(i2) == this.f1001i) {
            return false;
        }
        return super.a(viewGroup, i2);
    }

    public boolean a(s sVar) {
        boolean z2 = false;
        if (!sVar.hasVisibleItems()) {
            return false;
        }
        s sVar2 = sVar;
        while (sVar2.t() != this.f753c) {
            sVar2 = (s) sVar2.t();
        }
        View a2 = a(sVar2.getItem());
        if (a2 == null) {
            return false;
        }
        sVar.getItem().getItemId();
        int size = sVar.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                break;
            }
            MenuItem item = sVar.getItem(i2);
            if (item.isVisible() && item.getIcon() != null) {
                z2 = true;
                break;
            }
            i2++;
        }
        this.x = new a(this.f752b, sVar, a2);
        this.x.a(z2);
        this.x.e();
        super.a(sVar);
        return true;
    }

    private View a(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.f758h;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = viewGroup.getChildAt(i2);
            if ((childAt instanceof o.a) && ((o.a) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.ActionMenuView.a(android.view.View, int, int, int, int):int
     arg types: [android.view.View, int, int, int, int]
     candidates:
      androidx.appcompat.widget.h0.a(android.view.View, int, int, int, int):void
      androidx.appcompat.widget.ActionMenuView.a(android.view.View, int, int, int, int):int */
    public boolean a() {
        int i2;
        ArrayList<j> arrayList;
        int i3;
        int i4;
        int i5;
        c cVar = this;
        g gVar = cVar.f753c;
        View view = null;
        int i6 = 0;
        if (gVar != null) {
            arrayList = gVar.n();
            i2 = arrayList.size();
        } else {
            arrayList = null;
            i2 = 0;
        }
        int i7 = cVar.p;
        int i8 = cVar.o;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) cVar.f758h;
        int i9 = i7;
        boolean z2 = false;
        int i10 = 0;
        int i11 = 0;
        for (int i12 = 0; i12 < i2; i12++) {
            j jVar = (j) arrayList.get(i12);
            if (jVar.k()) {
                i10++;
            } else if (jVar.j()) {
                i11++;
            } else {
                z2 = true;
            }
            if (cVar.t && jVar.isActionViewExpanded()) {
                i9 = 0;
            }
        }
        if (cVar.l && (z2 || i11 + i10 > i9)) {
            i9--;
        }
        int i13 = i9 - i10;
        SparseBooleanArray sparseBooleanArray = cVar.v;
        sparseBooleanArray.clear();
        if (cVar.r) {
            int i14 = cVar.u;
            i3 = i8 / i14;
            i4 = i14 + ((i8 % i14) / i3);
        } else {
            i4 = 0;
            i3 = 0;
        }
        int i15 = i8;
        int i16 = 0;
        int i17 = 0;
        while (i16 < i2) {
            j jVar2 = (j) arrayList.get(i16);
            if (jVar2.k()) {
                View a2 = cVar.a(jVar2, view, viewGroup);
                if (cVar.r) {
                    i3 -= ActionMenuView.a(a2, i4, i3, makeMeasureSpec, i6);
                } else {
                    a2.measure(makeMeasureSpec, makeMeasureSpec);
                }
                int measuredWidth = a2.getMeasuredWidth();
                i15 -= measuredWidth;
                if (i17 != 0) {
                    measuredWidth = i17;
                }
                int groupId = jVar2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                jVar2.d(true);
                i17 = measuredWidth;
                i5 = i2;
            } else if (jVar2.j()) {
                int groupId2 = jVar2.getGroupId();
                boolean z3 = sparseBooleanArray.get(groupId2);
                boolean z4 = (i13 > 0 || z3) && i15 > 0 && (!cVar.r || i3 > 0);
                boolean z5 = z4;
                i5 = i2;
                if (z4) {
                    View a3 = cVar.a(jVar2, null, viewGroup);
                    if (cVar.r) {
                        int a4 = ActionMenuView.a(a3, i4, i3, makeMeasureSpec, 0);
                        i3 -= a4;
                        z5 = a4 == 0 ? false : z5;
                    } else {
                        a3.measure(makeMeasureSpec, makeMeasureSpec);
                    }
                    int measuredWidth2 = a3.getMeasuredWidth();
                    i15 -= measuredWidth2;
                    if (i17 == 0) {
                        i17 = measuredWidth2;
                    }
                    z4 = z5 & (!cVar.r ? i15 + i17 > 0 : i15 >= 0);
                }
                if (z4 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                } else if (z3) {
                    sparseBooleanArray.put(groupId2, false);
                    for (int i18 = 0; i18 < i16; i18++) {
                        j jVar3 = (j) arrayList.get(i18);
                        if (jVar3.getGroupId() == groupId2) {
                            if (jVar3.h()) {
                                i13++;
                            }
                            jVar3.d(false);
                        }
                    }
                }
                if (z4) {
                    i13--;
                }
                jVar2.d(z4);
            } else {
                i5 = i2;
                jVar2.d(false);
                i16++;
                i2 = i5;
                view = null;
                i6 = 0;
                cVar = this;
            }
            i16++;
            i2 = i5;
            view = null;
            i6 = 0;
            cVar = this;
        }
        return true;
    }

    public void a(g gVar, boolean z2) {
        c();
        super.a(gVar, z2);
    }

    public void a(ActionMenuView actionMenuView) {
        this.f758h = actionMenuView;
        actionMenuView.a(this.f753c);
    }
}
