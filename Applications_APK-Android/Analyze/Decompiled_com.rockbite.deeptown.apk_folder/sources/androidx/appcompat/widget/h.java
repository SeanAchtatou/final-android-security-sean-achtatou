package androidx.appcompat.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.CheckedTextView;
import androidx.core.widget.i;
import b.a.k.a.a;

/* compiled from: AppCompatCheckedTextView */
public class h extends CheckedTextView {

    /* renamed from: b  reason: collision with root package name */
    private static final int[] f1050b = {16843016};

    /* renamed from: a  reason: collision with root package name */
    private final x f1051a;

    public h(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16843720);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        x xVar = this.f1051a;
        if (xVar != null) {
            xVar.a();
        }
    }

    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        l.a(onCreateInputConnection, editorInfo, this);
        return onCreateInputConnection;
    }

    public void setCheckMarkDrawable(int i2) {
        setCheckMarkDrawable(a.c(getContext(), i2));
    }

    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        super.setCustomSelectionActionModeCallback(i.a(this, callback));
    }

    public void setTextAppearance(Context context, int i2) {
        super.setTextAppearance(context, i2);
        x xVar = this.f1051a;
        if (xVar != null) {
            xVar.a(context, i2);
        }
    }

    public h(Context context, AttributeSet attributeSet, int i2) {
        super(s0.b(context), attributeSet, i2);
        this.f1051a = new x(this);
        this.f1051a.a(attributeSet, i2);
        this.f1051a.a();
        v0 a2 = v0.a(getContext(), attributeSet, f1050b, i2, 0);
        setCheckMarkDrawable(a2.b(0));
        a2.a();
    }
}
