package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import b.e.f.a;

/* compiled from: ThemeUtils */
class q0 {

    /* renamed from: a  reason: collision with root package name */
    private static final ThreadLocal<TypedValue> f1157a = new ThreadLocal<>();

    /* renamed from: b  reason: collision with root package name */
    static final int[] f1158b = {-16842910};

    /* renamed from: c  reason: collision with root package name */
    static final int[] f1159c = {16842908};

    /* renamed from: d  reason: collision with root package name */
    static final int[] f1160d = {16842919};

    /* renamed from: e  reason: collision with root package name */
    static final int[] f1161e = {16842912};

    /* renamed from: f  reason: collision with root package name */
    static final int[] f1162f = new int[0];

    /* renamed from: g  reason: collision with root package name */
    private static final int[] f1163g = new int[1];

    static {
        new int[1][0] = 16843518;
        new int[1][0] = 16842913;
    }

    public static int a(Context context, int i2) {
        ColorStateList c2 = c(context, i2);
        if (c2 != null && c2.isStateful()) {
            return c2.getColorForState(f1158b, c2.getDefaultColor());
        }
        TypedValue a2 = a();
        context.getTheme().resolveAttribute(16842803, a2, true);
        return a(context, i2, a2.getFloat());
    }

    public static int b(Context context, int i2) {
        int[] iArr = f1163g;
        iArr[0] = i2;
        v0 a2 = v0.a(context, (AttributeSet) null, iArr);
        try {
            return a2.a(0, 0);
        } finally {
            a2.a();
        }
    }

    public static ColorStateList c(Context context, int i2) {
        int[] iArr = f1163g;
        iArr[0] = i2;
        v0 a2 = v0.a(context, (AttributeSet) null, iArr);
        try {
            return a2.a(0);
        } finally {
            a2.a();
        }
    }

    private static TypedValue a() {
        TypedValue typedValue = f1157a.get();
        if (typedValue != null) {
            return typedValue;
        }
        TypedValue typedValue2 = new TypedValue();
        f1157a.set(typedValue2);
        return typedValue2;
    }

    static int a(Context context, int i2, float f2) {
        int b2 = b(context, i2);
        return a.c(b2, Math.round(((float) Color.alpha(b2)) * f2));
    }
}
