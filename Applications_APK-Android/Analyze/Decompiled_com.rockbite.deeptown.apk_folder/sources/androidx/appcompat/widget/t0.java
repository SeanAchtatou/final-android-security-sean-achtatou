package androidx.appcompat.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* compiled from: TintInfo */
public class t0 {

    /* renamed from: a  reason: collision with root package name */
    public ColorStateList f1175a;

    /* renamed from: b  reason: collision with root package name */
    public PorterDuff.Mode f1176b;

    /* renamed from: c  reason: collision with root package name */
    public boolean f1177c;

    /* renamed from: d  reason: collision with root package name */
    public boolean f1178d;

    /* access modifiers changed from: package-private */
    public void a() {
        this.f1175a = null;
        this.f1178d = false;
        this.f1176b = null;
        this.f1177c = false;
    }
}
