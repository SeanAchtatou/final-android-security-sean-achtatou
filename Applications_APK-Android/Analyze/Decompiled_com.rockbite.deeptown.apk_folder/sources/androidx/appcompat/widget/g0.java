package androidx.appcompat.widget;

import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import androidx.appcompat.view.menu.q;
import com.esotericsoftware.spine.Animation;

/* compiled from: ForwardingListener */
public abstract class g0 implements View.OnTouchListener, View.OnAttachStateChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final float f1039a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1040b;

    /* renamed from: c  reason: collision with root package name */
    private final int f1041c;

    /* renamed from: d  reason: collision with root package name */
    final View f1042d;

    /* renamed from: e  reason: collision with root package name */
    private Runnable f1043e;

    /* renamed from: f  reason: collision with root package name */
    private Runnable f1044f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f1045g;

    /* renamed from: h  reason: collision with root package name */
    private int f1046h;

    /* renamed from: i  reason: collision with root package name */
    private final int[] f1047i = new int[2];

    /* compiled from: ForwardingListener */
    private class a implements Runnable {
        a() {
        }

        public void run() {
            ViewParent parent = g0.this.f1042d.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }
    }

    /* compiled from: ForwardingListener */
    private class b implements Runnable {
        b() {
        }

        public void run() {
            g0.this.d();
        }
    }

    public g0(View view) {
        this.f1042d = view;
        view.setLongClickable(true);
        view.addOnAttachStateChangeListener(this);
        this.f1039a = (float) ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
        this.f1040b = ViewConfiguration.getTapTimeout();
        this.f1041c = (this.f1040b + ViewConfiguration.getLongPressTimeout()) / 2;
    }

    private boolean a(MotionEvent motionEvent) {
        e0 e0Var;
        View view = this.f1042d;
        q a2 = a();
        if (a2 == null || !a2.isShowing() || (e0Var = (e0) a2.d()) == null || !e0Var.isShown()) {
            return false;
        }
        MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent);
        a(view, obtainNoHistory);
        b(e0Var, obtainNoHistory);
        boolean a3 = e0Var.a(obtainNoHistory, this.f1046h);
        obtainNoHistory.recycle();
        int actionMasked = motionEvent.getActionMasked();
        return a3 && (actionMasked != 1 && actionMasked != 3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0017, code lost:
        if (r1 != 3) goto L_0x006d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(android.view.MotionEvent r6) {
        /*
            r5 = this;
            android.view.View r0 = r5.f1042d
            boolean r1 = r0.isEnabled()
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            int r1 = r6.getActionMasked()
            if (r1 == 0) goto L_0x0041
            r3 = 1
            if (r1 == r3) goto L_0x003d
            r4 = 2
            if (r1 == r4) goto L_0x001a
            r6 = 3
            if (r1 == r6) goto L_0x003d
            goto L_0x006d
        L_0x001a:
            int r1 = r5.f1046h
            int r1 = r6.findPointerIndex(r1)
            if (r1 < 0) goto L_0x006d
            float r4 = r6.getX(r1)
            float r6 = r6.getY(r1)
            float r1 = r5.f1039a
            boolean r6 = a(r0, r4, r6, r1)
            if (r6 != 0) goto L_0x006d
            r5.e()
            android.view.ViewParent r6 = r0.getParent()
            r6.requestDisallowInterceptTouchEvent(r3)
            return r3
        L_0x003d:
            r5.e()
            goto L_0x006d
        L_0x0041:
            int r6 = r6.getPointerId(r2)
            r5.f1046h = r6
            java.lang.Runnable r6 = r5.f1043e
            if (r6 != 0) goto L_0x0052
            androidx.appcompat.widget.g0$a r6 = new androidx.appcompat.widget.g0$a
            r6.<init>()
            r5.f1043e = r6
        L_0x0052:
            java.lang.Runnable r6 = r5.f1043e
            int r1 = r5.f1040b
            long r3 = (long) r1
            r0.postDelayed(r6, r3)
            java.lang.Runnable r6 = r5.f1044f
            if (r6 != 0) goto L_0x0065
            androidx.appcompat.widget.g0$b r6 = new androidx.appcompat.widget.g0$b
            r6.<init>()
            r5.f1044f = r6
        L_0x0065:
            java.lang.Runnable r6 = r5.f1044f
            int r1 = r5.f1041c
            long r3 = (long) r1
            r0.postDelayed(r6, r3)
        L_0x006d:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.g0.b(android.view.MotionEvent):boolean");
    }

    private void e() {
        Runnable runnable = this.f1044f;
        if (runnable != null) {
            this.f1042d.removeCallbacks(runnable);
        }
        Runnable runnable2 = this.f1043e;
        if (runnable2 != null) {
            this.f1042d.removeCallbacks(runnable2);
        }
    }

    public abstract q a();

    /* access modifiers changed from: protected */
    public abstract boolean b();

    /* access modifiers changed from: protected */
    public boolean c() {
        q a2 = a();
        if (a2 == null || !a2.isShowing()) {
            return true;
        }
        a2.dismiss();
        return true;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        e();
        View view = this.f1042d;
        if (view.isEnabled() && !view.isLongClickable() && b()) {
            view.getParent().requestDisallowInterceptTouchEvent(true);
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0);
            view.onTouchEvent(obtain);
            obtain.recycle();
            this.f1045g = true;
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z;
        boolean z2 = this.f1045g;
        if (z2) {
            z = a(motionEvent) || !c();
        } else {
            z = b(motionEvent) && b();
            if (z) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0);
                this.f1042d.onTouchEvent(obtain);
                obtain.recycle();
            }
        }
        this.f1045g = z;
        if (z || z2) {
            return true;
        }
        return false;
    }

    public void onViewAttachedToWindow(View view) {
    }

    public void onViewDetachedFromWindow(View view) {
        this.f1045g = false;
        this.f1046h = -1;
        Runnable runnable = this.f1043e;
        if (runnable != null) {
            this.f1042d.removeCallbacks(runnable);
        }
    }

    private static boolean a(View view, float f2, float f3, float f4) {
        float f5 = -f4;
        return f2 >= f5 && f3 >= f5 && f2 < ((float) (view.getRight() - view.getLeft())) + f4 && f3 < ((float) (view.getBottom() - view.getTop())) + f4;
    }

    private boolean a(View view, MotionEvent motionEvent) {
        int[] iArr = this.f1047i;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) iArr[0], (float) iArr[1]);
        return true;
    }

    private boolean b(View view, MotionEvent motionEvent) {
        int[] iArr = this.f1047i;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) (-iArr[0]), (float) (-iArr[1]));
        return true;
    }
}
