package androidx.appcompat.widget;

import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityManager;
import b.e.m.u;
import b.e.m.v;
import com.google.android.gms.common.api.Api;

/* compiled from: TooltipCompatHandler */
class y0 implements View.OnLongClickListener, View.OnHoverListener, View.OnAttachStateChangeListener {

    /* renamed from: j  reason: collision with root package name */
    private static y0 f1228j;

    /* renamed from: k  reason: collision with root package name */
    private static y0 f1229k;

    /* renamed from: a  reason: collision with root package name */
    private final View f1230a;

    /* renamed from: b  reason: collision with root package name */
    private final CharSequence f1231b;

    /* renamed from: c  reason: collision with root package name */
    private final int f1232c;

    /* renamed from: d  reason: collision with root package name */
    private final Runnable f1233d = new a();

    /* renamed from: e  reason: collision with root package name */
    private final Runnable f1234e = new b();

    /* renamed from: f  reason: collision with root package name */
    private int f1235f;

    /* renamed from: g  reason: collision with root package name */
    private int f1236g;

    /* renamed from: h  reason: collision with root package name */
    private z0 f1237h;

    /* renamed from: i  reason: collision with root package name */
    private boolean f1238i;

    /* compiled from: TooltipCompatHandler */
    class a implements Runnable {
        a() {
        }

        public void run() {
            y0.this.a(false);
        }
    }

    /* compiled from: TooltipCompatHandler */
    class b implements Runnable {
        b() {
        }

        public void run() {
            y0.this.a();
        }
    }

    private y0(View view, CharSequence charSequence) {
        this.f1230a = view;
        this.f1231b = charSequence;
        this.f1232c = v.a(ViewConfiguration.get(this.f1230a.getContext()));
        c();
        this.f1230a.setOnLongClickListener(this);
        this.f1230a.setOnHoverListener(this);
    }

    public static void a(View view, CharSequence charSequence) {
        y0 y0Var = f1228j;
        if (y0Var != null && y0Var.f1230a == view) {
            a((y0) null);
        }
        if (TextUtils.isEmpty(charSequence)) {
            y0 y0Var2 = f1229k;
            if (y0Var2 != null && y0Var2.f1230a == view) {
                y0Var2.a();
            }
            view.setOnLongClickListener(null);
            view.setLongClickable(false);
            view.setOnHoverListener(null);
            return;
        }
        new y0(view, charSequence);
    }

    private void b() {
        this.f1230a.removeCallbacks(this.f1233d);
    }

    private void c() {
        this.f1235f = Api.BaseClientBuilder.API_PRIORITY_OTHER;
        this.f1236g = Api.BaseClientBuilder.API_PRIORITY_OTHER;
    }

    private void d() {
        this.f1230a.postDelayed(this.f1233d, (long) ViewConfiguration.getLongPressTimeout());
    }

    public boolean onHover(View view, MotionEvent motionEvent) {
        if (this.f1237h != null && this.f1238i) {
            return false;
        }
        AccessibilityManager accessibilityManager = (AccessibilityManager) this.f1230a.getContext().getSystemService("accessibility");
        if (accessibilityManager.isEnabled() && accessibilityManager.isTouchExplorationEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action != 7) {
            if (action == 10) {
                c();
                a();
            }
        } else if (this.f1230a.isEnabled() && this.f1237h == null && a(motionEvent)) {
            a(this);
        }
        return false;
    }

    public boolean onLongClick(View view) {
        this.f1235f = view.getWidth() / 2;
        this.f1236g = view.getHeight() / 2;
        a(true);
        return true;
    }

    public void onViewAttachedToWindow(View view) {
    }

    public void onViewDetachedFromWindow(View view) {
        a();
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        long j2;
        int i2;
        long j3;
        if (u.s(this.f1230a)) {
            a((y0) null);
            y0 y0Var = f1229k;
            if (y0Var != null) {
                y0Var.a();
            }
            f1229k = this;
            this.f1238i = z;
            this.f1237h = new z0(this.f1230a.getContext());
            this.f1237h.a(this.f1230a, this.f1235f, this.f1236g, this.f1238i, this.f1231b);
            this.f1230a.addOnAttachStateChangeListener(this);
            if (this.f1238i) {
                j2 = 2500;
            } else {
                if ((u.o(this.f1230a) & 1) == 1) {
                    j3 = 3000;
                    i2 = ViewConfiguration.getLongPressTimeout();
                } else {
                    j3 = 15000;
                    i2 = ViewConfiguration.getLongPressTimeout();
                }
                j2 = j3 - ((long) i2);
            }
            this.f1230a.removeCallbacks(this.f1234e);
            this.f1230a.postDelayed(this.f1234e, j2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (f1229k == this) {
            f1229k = null;
            z0 z0Var = this.f1237h;
            if (z0Var != null) {
                z0Var.a();
                this.f1237h = null;
                c();
                this.f1230a.removeOnAttachStateChangeListener(this);
            } else {
                Log.e("TooltipCompatHandler", "sActiveHandler.mPopup == null");
            }
        }
        if (f1228j == this) {
            a((y0) null);
        }
        this.f1230a.removeCallbacks(this.f1234e);
    }

    private static void a(y0 y0Var) {
        y0 y0Var2 = f1228j;
        if (y0Var2 != null) {
            y0Var2.b();
        }
        f1228j = y0Var;
        y0 y0Var3 = f1228j;
        if (y0Var3 != null) {
            y0Var3.d();
        }
    }

    private boolean a(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (Math.abs(x - this.f1235f) <= this.f1232c && Math.abs(y - this.f1236g) <= this.f1232c) {
            return false;
        }
        this.f1235f = x;
        this.f1236g = y;
        return true;
    }
}
