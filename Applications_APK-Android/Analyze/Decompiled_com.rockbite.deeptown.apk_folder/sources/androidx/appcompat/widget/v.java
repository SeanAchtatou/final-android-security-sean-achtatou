package androidx.appcompat.widget;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.SeekBar;
import androidx.core.graphics.drawable.a;
import b.a.j;
import b.e.m.u;
import com.esotericsoftware.spine.Animation;

/* compiled from: AppCompatSeekBarHelper */
class v extends r {

    /* renamed from: d  reason: collision with root package name */
    private final SeekBar f1181d;

    /* renamed from: e  reason: collision with root package name */
    private Drawable f1182e;

    /* renamed from: f  reason: collision with root package name */
    private ColorStateList f1183f = null;

    /* renamed from: g  reason: collision with root package name */
    private PorterDuff.Mode f1184g = null;

    /* renamed from: h  reason: collision with root package name */
    private boolean f1185h = false;

    /* renamed from: i  reason: collision with root package name */
    private boolean f1186i = false;

    v(SeekBar seekBar) {
        super(seekBar);
        this.f1181d = seekBar;
    }

    private void d() {
        if (this.f1182e == null) {
            return;
        }
        if (this.f1185h || this.f1186i) {
            this.f1182e = a.g(this.f1182e.mutate());
            if (this.f1185h) {
                a.a(this.f1182e, this.f1183f);
            }
            if (this.f1186i) {
                a.a(this.f1182e, this.f1184g);
            }
            if (this.f1182e.isStateful()) {
                this.f1182e.setState(this.f1181d.getDrawableState());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i2) {
        super.a(attributeSet, i2);
        v0 a2 = v0.a(this.f1181d.getContext(), attributeSet, j.AppCompatSeekBar, i2, 0);
        Drawable c2 = a2.c(j.AppCompatSeekBar_android_thumb);
        if (c2 != null) {
            this.f1181d.setThumb(c2);
        }
        a(a2.b(j.AppCompatSeekBar_tickMark));
        if (a2.g(j.AppCompatSeekBar_tickMarkTintMode)) {
            this.f1184g = d0.a(a2.d(j.AppCompatSeekBar_tickMarkTintMode, -1), this.f1184g);
            this.f1186i = true;
        }
        if (a2.g(j.AppCompatSeekBar_tickMarkTint)) {
            this.f1183f = a2.a(j.AppCompatSeekBar_tickMarkTint);
            this.f1185h = true;
        }
        a2.a();
        d();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        Drawable drawable = this.f1182e;
        if (drawable != null && drawable.isStateful() && drawable.setState(this.f1181d.getDrawableState())) {
            this.f1181d.invalidateDrawable(drawable);
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        Drawable drawable = this.f1182e;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Drawable drawable) {
        Drawable drawable2 = this.f1182e;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.f1182e = drawable;
        if (drawable != null) {
            drawable.setCallback(this.f1181d);
            a.a(drawable, u.k(this.f1181d));
            if (drawable.isStateful()) {
                drawable.setState(this.f1181d.getDrawableState());
            }
            d();
        }
        this.f1181d.invalidate();
    }

    /* access modifiers changed from: package-private */
    public void a(Canvas canvas) {
        if (this.f1182e != null) {
            int max = this.f1181d.getMax();
            int i2 = 1;
            if (max > 1) {
                int intrinsicWidth = this.f1182e.getIntrinsicWidth();
                int intrinsicHeight = this.f1182e.getIntrinsicHeight();
                int i3 = intrinsicWidth >= 0 ? intrinsicWidth / 2 : 1;
                if (intrinsicHeight >= 0) {
                    i2 = intrinsicHeight / 2;
                }
                this.f1182e.setBounds(-i3, -i2, i3, i2);
                float width = ((float) ((this.f1181d.getWidth() - this.f1181d.getPaddingLeft()) - this.f1181d.getPaddingRight())) / ((float) max);
                int save = canvas.save();
                canvas.translate((float) this.f1181d.getPaddingLeft(), (float) (this.f1181d.getHeight() / 2));
                for (int i4 = 0; i4 <= max; i4++) {
                    this.f1182e.draw(canvas);
                    canvas.translate(width, Animation.CurveTimeline.LINEAR);
                }
                canvas.restoreToCount(save);
            }
        }
    }
}
