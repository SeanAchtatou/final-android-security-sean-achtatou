package androidx.appcompat.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;
import androidx.core.widget.b;
import androidx.core.widget.i;
import b.a.a;
import b.e.m.t;

/* compiled from: AppCompatButton */
public class f extends Button implements t, b {

    /* renamed from: a  reason: collision with root package name */
    private final e f1034a;

    /* renamed from: b  reason: collision with root package name */
    private final x f1035b;

    public f(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.buttonStyle);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        e eVar = this.f1034a;
        if (eVar != null) {
            eVar.a();
        }
        x xVar = this.f1035b;
        if (xVar != null) {
            xVar.a();
        }
    }

    public int getAutoSizeMaxTextSize() {
        if (b.X) {
            return super.getAutoSizeMaxTextSize();
        }
        x xVar = this.f1035b;
        if (xVar != null) {
            return xVar.c();
        }
        return -1;
    }

    public int getAutoSizeMinTextSize() {
        if (b.X) {
            return super.getAutoSizeMinTextSize();
        }
        x xVar = this.f1035b;
        if (xVar != null) {
            return xVar.d();
        }
        return -1;
    }

    public int getAutoSizeStepGranularity() {
        if (b.X) {
            return super.getAutoSizeStepGranularity();
        }
        x xVar = this.f1035b;
        if (xVar != null) {
            return xVar.e();
        }
        return -1;
    }

    public int[] getAutoSizeTextAvailableSizes() {
        if (b.X) {
            return super.getAutoSizeTextAvailableSizes();
        }
        x xVar = this.f1035b;
        return xVar != null ? xVar.f() : new int[0];
    }

    @SuppressLint({"WrongConstant"})
    public int getAutoSizeTextType() {
        if (!b.X) {
            x xVar = this.f1035b;
            if (xVar != null) {
                return xVar.g();
            }
            return 0;
        } else if (super.getAutoSizeTextType() == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        e eVar = this.f1034a;
        if (eVar != null) {
            return eVar.b();
        }
        return null;
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        e eVar = this.f1034a;
        if (eVar != null) {
            return eVar.c();
        }
        return null;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(Button.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(Button.class.getName());
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        x xVar = this.f1035b;
        if (xVar != null) {
            xVar.a(z, i2, i3, i4, i5);
        }
    }

    /* access modifiers changed from: protected */
    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        super.onTextChanged(charSequence, i2, i3, i4);
        x xVar = this.f1035b;
        if (xVar != null && !b.X && xVar.j()) {
            this.f1035b.b();
        }
    }

    public void setAutoSizeTextTypeUniformWithConfiguration(int i2, int i3, int i4, int i5) throws IllegalArgumentException {
        if (b.X) {
            super.setAutoSizeTextTypeUniformWithConfiguration(i2, i3, i4, i5);
            return;
        }
        x xVar = this.f1035b;
        if (xVar != null) {
            xVar.a(i2, i3, i4, i5);
        }
    }

    public void setAutoSizeTextTypeUniformWithPresetSizes(int[] iArr, int i2) throws IllegalArgumentException {
        if (b.X) {
            super.setAutoSizeTextTypeUniformWithPresetSizes(iArr, i2);
            return;
        }
        x xVar = this.f1035b;
        if (xVar != null) {
            xVar.a(iArr, i2);
        }
    }

    public void setAutoSizeTextTypeWithDefaults(int i2) {
        if (b.X) {
            super.setAutoSizeTextTypeWithDefaults(i2);
            return;
        }
        x xVar = this.f1035b;
        if (xVar != null) {
            xVar.a(i2);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        e eVar = this.f1034a;
        if (eVar != null) {
            eVar.a(drawable);
        }
    }

    public void setBackgroundResource(int i2) {
        super.setBackgroundResource(i2);
        e eVar = this.f1034a;
        if (eVar != null) {
            eVar.a(i2);
        }
    }

    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        super.setCustomSelectionActionModeCallback(i.a(this, callback));
    }

    public void setSupportAllCaps(boolean z) {
        x xVar = this.f1035b;
        if (xVar != null) {
            xVar.a(z);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        e eVar = this.f1034a;
        if (eVar != null) {
            eVar.b(colorStateList);
        }
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        e eVar = this.f1034a;
        if (eVar != null) {
            eVar.a(mode);
        }
    }

    public void setTextAppearance(Context context, int i2) {
        super.setTextAppearance(context, i2);
        x xVar = this.f1035b;
        if (xVar != null) {
            xVar.a(context, i2);
        }
    }

    public void setTextSize(int i2, float f2) {
        if (b.X) {
            super.setTextSize(i2, f2);
            return;
        }
        x xVar = this.f1035b;
        if (xVar != null) {
            xVar.a(i2, f2);
        }
    }

    public f(Context context, AttributeSet attributeSet, int i2) {
        super(s0.b(context), attributeSet, i2);
        this.f1034a = new e(this);
        this.f1034a.a(attributeSet, i2);
        this.f1035b = new x(this);
        this.f1035b.a(attributeSet, i2);
        this.f1035b.a();
    }
}
