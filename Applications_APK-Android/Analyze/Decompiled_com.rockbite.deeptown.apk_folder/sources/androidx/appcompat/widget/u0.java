package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;

/* compiled from: TintResources */
class u0 extends m0 {

    /* renamed from: b  reason: collision with root package name */
    private final WeakReference<Context> f1180b;

    public u0(Context context, Resources resources) {
        super(resources);
        this.f1180b = new WeakReference<>(context);
    }

    public Drawable getDrawable(int i2) throws Resources.NotFoundException {
        Drawable drawable = super.getDrawable(i2);
        Context context = this.f1180b.get();
        if (!(drawable == null || context == null)) {
            l0.a().a(context, i2, drawable);
        }
        return drawable;
    }
}
