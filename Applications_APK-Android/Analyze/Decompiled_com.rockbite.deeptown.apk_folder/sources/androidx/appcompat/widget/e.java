package androidx.appcompat.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import b.a.j;
import b.e.m.u;

/* compiled from: AppCompatBackgroundHelper */
class e {

    /* renamed from: a  reason: collision with root package name */
    private final View f1015a;

    /* renamed from: b  reason: collision with root package name */
    private final j f1016b;

    /* renamed from: c  reason: collision with root package name */
    private int f1017c = -1;

    /* renamed from: d  reason: collision with root package name */
    private t0 f1018d;

    /* renamed from: e  reason: collision with root package name */
    private t0 f1019e;

    /* renamed from: f  reason: collision with root package name */
    private t0 f1020f;

    e(View view) {
        this.f1015a = view;
        this.f1016b = j.b();
    }

    private boolean d() {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 <= 21) {
            return i2 == 21;
        }
        if (this.f1018d != null) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i2) {
        v0 a2 = v0.a(this.f1015a.getContext(), attributeSet, j.ViewBackgroundHelper, i2, 0);
        try {
            if (a2.g(j.ViewBackgroundHelper_android_background)) {
                this.f1017c = a2.g(j.ViewBackgroundHelper_android_background, -1);
                ColorStateList b2 = this.f1016b.b(this.f1015a.getContext(), this.f1017c);
                if (b2 != null) {
                    a(b2);
                }
            }
            if (a2.g(j.ViewBackgroundHelper_backgroundTint)) {
                u.a(this.f1015a, a2.a(j.ViewBackgroundHelper_backgroundTint));
            }
            if (a2.g(j.ViewBackgroundHelper_backgroundTintMode)) {
                u.a(this.f1015a, d0.a(a2.d(j.ViewBackgroundHelper_backgroundTintMode, -1), null));
            }
        } finally {
            a2.a();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(ColorStateList colorStateList) {
        if (this.f1019e == null) {
            this.f1019e = new t0();
        }
        t0 t0Var = this.f1019e;
        t0Var.f1175a = colorStateList;
        t0Var.f1178d = true;
        a();
    }

    /* access modifiers changed from: package-private */
    public PorterDuff.Mode c() {
        t0 t0Var = this.f1019e;
        if (t0Var != null) {
            return t0Var.f1176b;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public ColorStateList b() {
        t0 t0Var = this.f1019e;
        if (t0Var != null) {
            return t0Var.f1175a;
        }
        return null;
    }

    private boolean b(Drawable drawable) {
        if (this.f1020f == null) {
            this.f1020f = new t0();
        }
        t0 t0Var = this.f1020f;
        t0Var.a();
        ColorStateList f2 = u.f(this.f1015a);
        if (f2 != null) {
            t0Var.f1178d = true;
            t0Var.f1175a = f2;
        }
        PorterDuff.Mode g2 = u.g(this.f1015a);
        if (g2 != null) {
            t0Var.f1177c = true;
            t0Var.f1176b = g2;
        }
        if (!t0Var.f1178d && !t0Var.f1177c) {
            return false;
        }
        j.a(drawable, t0Var, this.f1015a.getDrawableState());
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.f1017c = i2;
        j jVar = this.f1016b;
        a(jVar != null ? jVar.b(this.f1015a.getContext(), i2) : null);
        a();
    }

    /* access modifiers changed from: package-private */
    public void a(Drawable drawable) {
        this.f1017c = -1;
        a((ColorStateList) null);
        a();
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        if (this.f1019e == null) {
            this.f1019e = new t0();
        }
        t0 t0Var = this.f1019e;
        t0Var.f1176b = mode;
        t0Var.f1177c = true;
        a();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        Drawable background = this.f1015a.getBackground();
        if (background == null) {
            return;
        }
        if (!d() || !b(background)) {
            t0 t0Var = this.f1019e;
            if (t0Var != null) {
                j.a(background, t0Var, this.f1015a.getDrawableState());
                return;
            }
            t0 t0Var2 = this.f1018d;
            if (t0Var2 != null) {
                j.a(background, t0Var2, this.f1015a.getDrawableState());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        if (colorStateList != null) {
            if (this.f1018d == null) {
                this.f1018d = new t0();
            }
            t0 t0Var = this.f1018d;
            t0Var.f1175a = colorStateList;
            t0Var.f1178d = true;
        } else {
            this.f1018d = null;
        }
        a();
    }
}
