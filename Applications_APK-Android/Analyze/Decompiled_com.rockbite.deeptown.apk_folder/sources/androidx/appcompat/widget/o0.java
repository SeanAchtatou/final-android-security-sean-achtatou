package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import androidx.appcompat.app.a;
import androidx.appcompat.widget.h0;

/* compiled from: ScrollingTabContainerView */
public class o0 extends HorizontalScrollView implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    Runnable f1128a;

    /* renamed from: b  reason: collision with root package name */
    private c f1129b;

    /* renamed from: c  reason: collision with root package name */
    h0 f1130c;

    /* renamed from: d  reason: collision with root package name */
    private Spinner f1131d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f1132e;

    /* renamed from: f  reason: collision with root package name */
    int f1133f;

    /* renamed from: g  reason: collision with root package name */
    int f1134g;

    /* renamed from: h  reason: collision with root package name */
    private int f1135h;

    /* renamed from: i  reason: collision with root package name */
    private int f1136i;

    /* compiled from: ScrollingTabContainerView */
    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ View f1137a;

        a(View view) {
            this.f1137a = view;
        }

        public void run() {
            o0.this.smoothScrollTo(this.f1137a.getLeft() - ((o0.this.getWidth() - this.f1137a.getWidth()) / 2), 0);
            o0.this.f1128a = null;
        }
    }

    /* compiled from: ScrollingTabContainerView */
    private class b extends BaseAdapter {
        b() {
        }

        public int getCount() {
            return o0.this.f1130c.getChildCount();
        }

        public Object getItem(int i2) {
            return ((d) o0.this.f1130c.getChildAt(i2)).a();
        }

        public long getItemId(int i2) {
            return (long) i2;
        }

        public View getView(int i2, View view, ViewGroup viewGroup) {
            if (view == null) {
                return o0.this.a((a.c) getItem(i2), true);
            }
            ((d) view).a((a.c) getItem(i2));
            return view;
        }
    }

    /* compiled from: ScrollingTabContainerView */
    private class c implements View.OnClickListener {
        c() {
        }

        public void onClick(View view) {
            ((d) view).a().e();
            int childCount = o0.this.f1130c.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = o0.this.f1130c.getChildAt(i2);
                childAt.setSelected(childAt == view);
            }
        }
    }

    static {
        new DecelerateInterpolator();
    }

    private Spinner a() {
        AppCompatSpinner appCompatSpinner = new AppCompatSpinner(getContext(), null, b.a.a.actionDropDownStyle);
        appCompatSpinner.setLayoutParams(new h0.a(-2, -1));
        appCompatSpinner.setOnItemSelectedListener(this);
        return appCompatSpinner;
    }

    private boolean b() {
        Spinner spinner = this.f1131d;
        return spinner != null && spinner.getParent() == this;
    }

    private void c() {
        if (!b()) {
            if (this.f1131d == null) {
                this.f1131d = a();
            }
            removeView(this.f1130c);
            addView(this.f1131d, new ViewGroup.LayoutParams(-2, -1));
            if (this.f1131d.getAdapter() == null) {
                this.f1131d.setAdapter((SpinnerAdapter) new b());
            }
            Runnable runnable = this.f1128a;
            if (runnable != null) {
                removeCallbacks(runnable);
                this.f1128a = null;
            }
            this.f1131d.setSelection(this.f1136i);
        }
    }

    private boolean d() {
        if (!b()) {
            return false;
        }
        removeView(this.f1131d);
        addView(this.f1130c, new ViewGroup.LayoutParams(-2, -1));
        setTabSelected(this.f1131d.getSelectedItemPosition());
        return false;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Runnable runnable = this.f1128a;
        if (runnable != null) {
            post(runnable);
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        b.a.n.a a2 = b.a.n.a.a(getContext());
        setContentHeight(a2.e());
        this.f1134g = a2.d();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Runnable runnable = this.f1128a;
        if (runnable != null) {
            removeCallbacks(runnable);
        }
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i2, long j2) {
        ((d) view).a().e();
    }

    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        boolean z = true;
        boolean z2 = mode == 1073741824;
        setFillViewport(z2);
        int childCount = this.f1130c.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.f1133f = -1;
        } else {
            if (childCount > 2) {
                this.f1133f = (int) (((float) View.MeasureSpec.getSize(i2)) * 0.4f);
            } else {
                this.f1133f = View.MeasureSpec.getSize(i2) / 2;
            }
            this.f1133f = Math.min(this.f1133f, this.f1134g);
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.f1135h, 1073741824);
        if (z2 || !this.f1132e) {
            z = false;
        }
        if (z) {
            this.f1130c.measure(0, makeMeasureSpec);
            if (this.f1130c.getMeasuredWidth() > View.MeasureSpec.getSize(i2)) {
                c();
            } else {
                d();
            }
        } else {
            d();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i2, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z2 && measuredWidth != measuredWidth2) {
            setTabSelected(this.f1136i);
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public void setAllowCollapse(boolean z) {
        this.f1132e = z;
    }

    public void setContentHeight(int i2) {
        this.f1135h = i2;
        requestLayout();
    }

    public void setTabSelected(int i2) {
        this.f1136i = i2;
        int childCount = this.f1130c.getChildCount();
        int i3 = 0;
        while (i3 < childCount) {
            View childAt = this.f1130c.getChildAt(i3);
            boolean z = i3 == i2;
            childAt.setSelected(z);
            if (z) {
                a(i2);
            }
            i3++;
        }
        Spinner spinner = this.f1131d;
        if (spinner != null && i2 >= 0) {
            spinner.setSelection(i2);
        }
    }

    /* compiled from: ScrollingTabContainerView */
    private class d extends LinearLayout {

        /* renamed from: a  reason: collision with root package name */
        private final int[] f1141a = {16842964};

        /* renamed from: b  reason: collision with root package name */
        private a.c f1142b;

        /* renamed from: c  reason: collision with root package name */
        private TextView f1143c;

        /* renamed from: d  reason: collision with root package name */
        private ImageView f1144d;

        /* renamed from: e  reason: collision with root package name */
        private View f1145e;

        public d(Context context, a.c cVar, boolean z) {
            super(context, null, b.a.a.actionBarTabStyle);
            this.f1142b = cVar;
            v0 a2 = v0.a(context, null, this.f1141a, b.a.a.actionBarTabStyle, 0);
            if (a2.g(0)) {
                setBackgroundDrawable(a2.b(0));
            }
            a2.a();
            if (z) {
                setGravity(8388627);
            }
            b();
        }

        public void a(a.c cVar) {
            this.f1142b = cVar;
            b();
        }

        public void b() {
            a.c cVar = this.f1142b;
            View b2 = cVar.b();
            CharSequence charSequence = null;
            if (b2 != null) {
                ViewParent parent = b2.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(b2);
                    }
                    addView(b2);
                }
                this.f1145e = b2;
                TextView textView = this.f1143c;
                if (textView != null) {
                    textView.setVisibility(8);
                }
                ImageView imageView = this.f1144d;
                if (imageView != null) {
                    imageView.setVisibility(8);
                    this.f1144d.setImageDrawable(null);
                    return;
                }
                return;
            }
            View view = this.f1145e;
            if (view != null) {
                removeView(view);
                this.f1145e = null;
            }
            Drawable c2 = cVar.c();
            CharSequence d2 = cVar.d();
            if (c2 != null) {
                if (this.f1144d == null) {
                    o oVar = new o(getContext());
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams.gravity = 16;
                    oVar.setLayoutParams(layoutParams);
                    addView(oVar, 0);
                    this.f1144d = oVar;
                }
                this.f1144d.setImageDrawable(c2);
                this.f1144d.setVisibility(0);
            } else {
                ImageView imageView2 = this.f1144d;
                if (imageView2 != null) {
                    imageView2.setVisibility(8);
                    this.f1144d.setImageDrawable(null);
                }
            }
            boolean z = !TextUtils.isEmpty(d2);
            if (z) {
                if (this.f1143c == null) {
                    y yVar = new y(getContext(), null, b.a.a.actionBarTabTextStyle);
                    yVar.setEllipsize(TextUtils.TruncateAt.END);
                    LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams2.gravity = 16;
                    yVar.setLayoutParams(layoutParams2);
                    addView(yVar);
                    this.f1143c = yVar;
                }
                this.f1143c.setText(d2);
                this.f1143c.setVisibility(0);
            } else {
                TextView textView2 = this.f1143c;
                if (textView2 != null) {
                    textView2.setVisibility(8);
                    this.f1143c.setText((CharSequence) null);
                }
            }
            ImageView imageView3 = this.f1144d;
            if (imageView3 != null) {
                imageView3.setContentDescription(cVar.a());
            }
            if (!z) {
                charSequence = cVar.a();
            }
            x0.a(this, charSequence);
        }

        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName("androidx.appcompat.app.ActionBar$Tab");
        }

        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName("androidx.appcompat.app.ActionBar$Tab");
        }

        public void onMeasure(int i2, int i3) {
            int i4;
            super.onMeasure(i2, i3);
            if (o0.this.f1133f > 0 && getMeasuredWidth() > (i4 = o0.this.f1133f)) {
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(i4, 1073741824), i3);
            }
        }

        public void setSelected(boolean z) {
            boolean z2 = isSelected() != z;
            super.setSelected(z);
            if (z2 && z) {
                sendAccessibilityEvent(4);
            }
        }

        public a.c a() {
            return this.f1142b;
        }
    }

    public void a(int i2) {
        View childAt = this.f1130c.getChildAt(i2);
        Runnable runnable = this.f1128a;
        if (runnable != null) {
            removeCallbacks(runnable);
        }
        this.f1128a = new a(childAt);
        post(this.f1128a);
    }

    /* access modifiers changed from: package-private */
    public d a(a.c cVar, boolean z) {
        d dVar = new d(getContext(), cVar, z);
        if (z) {
            dVar.setBackgroundDrawable(null);
            dVar.setLayoutParams(new AbsListView.LayoutParams(-1, this.f1135h));
        } else {
            dVar.setFocusable(true);
            if (this.f1129b == null) {
                this.f1129b = new c();
            }
            dVar.setOnClickListener(this.f1129b);
        }
        return dVar;
    }
}
