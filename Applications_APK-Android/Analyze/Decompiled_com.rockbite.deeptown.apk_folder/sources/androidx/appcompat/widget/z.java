package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.os.Build;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;
import b.a.j;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.common.api.Api;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: AppCompatTextViewAutoSizeHelper */
class z {

    /* renamed from: k  reason: collision with root package name */
    private static final RectF f1241k = new RectF();
    private static ConcurrentHashMap<String, Method> l = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Field> m = new ConcurrentHashMap<>();

    /* renamed from: a  reason: collision with root package name */
    private int f1242a = 0;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1243b = false;

    /* renamed from: c  reason: collision with root package name */
    private float f1244c = -1.0f;

    /* renamed from: d  reason: collision with root package name */
    private float f1245d = -1.0f;

    /* renamed from: e  reason: collision with root package name */
    private float f1246e = -1.0f;

    /* renamed from: f  reason: collision with root package name */
    private int[] f1247f = new int[0];

    /* renamed from: g  reason: collision with root package name */
    private boolean f1248g = false;

    /* renamed from: h  reason: collision with root package name */
    private TextPaint f1249h;

    /* renamed from: i  reason: collision with root package name */
    private final TextView f1250i;

    /* renamed from: j  reason: collision with root package name */
    private final Context f1251j;

    z(TextView textView) {
        this.f1250i = textView;
        this.f1251j = this.f1250i.getContext();
    }

    private void h() {
        this.f1242a = 0;
        this.f1245d = -1.0f;
        this.f1246e = -1.0f;
        this.f1244c = -1.0f;
        this.f1247f = new int[0];
        this.f1243b = false;
    }

    private boolean i() {
        if (!k() || this.f1242a != 1) {
            this.f1243b = false;
        } else {
            if (!this.f1248g || this.f1247f.length == 0) {
                int floor = ((int) Math.floor((double) ((this.f1246e - this.f1245d) / this.f1244c))) + 1;
                int[] iArr = new int[floor];
                for (int i2 = 0; i2 < floor; i2++) {
                    iArr[i2] = Math.round(this.f1245d + (((float) i2) * this.f1244c));
                }
                this.f1247f = a(iArr);
            }
            this.f1243b = true;
        }
        return this.f1243b;
    }

    private boolean j() {
        int length = this.f1247f.length;
        this.f1248g = length > 0;
        if (this.f1248g) {
            this.f1242a = 1;
            int[] iArr = this.f1247f;
            this.f1245d = (float) iArr[0];
            this.f1246e = (float) iArr[length - 1];
            this.f1244c = -1.0f;
        }
        return this.f1248g;
    }

    private boolean k() {
        return !(this.f1250i instanceof k);
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i2) {
        int resourceId;
        TypedArray obtainStyledAttributes = this.f1251j.obtainStyledAttributes(attributeSet, j.AppCompatTextView, i2, 0);
        if (obtainStyledAttributes.hasValue(j.AppCompatTextView_autoSizeTextType)) {
            this.f1242a = obtainStyledAttributes.getInt(j.AppCompatTextView_autoSizeTextType, 0);
        }
        float dimension = obtainStyledAttributes.hasValue(j.AppCompatTextView_autoSizeStepGranularity) ? obtainStyledAttributes.getDimension(j.AppCompatTextView_autoSizeStepGranularity, -1.0f) : -1.0f;
        float dimension2 = obtainStyledAttributes.hasValue(j.AppCompatTextView_autoSizeMinTextSize) ? obtainStyledAttributes.getDimension(j.AppCompatTextView_autoSizeMinTextSize, -1.0f) : -1.0f;
        float dimension3 = obtainStyledAttributes.hasValue(j.AppCompatTextView_autoSizeMaxTextSize) ? obtainStyledAttributes.getDimension(j.AppCompatTextView_autoSizeMaxTextSize, -1.0f) : -1.0f;
        if (obtainStyledAttributes.hasValue(j.AppCompatTextView_autoSizePresetSizes) && (resourceId = obtainStyledAttributes.getResourceId(j.AppCompatTextView_autoSizePresetSizes, 0)) > 0) {
            TypedArray obtainTypedArray = obtainStyledAttributes.getResources().obtainTypedArray(resourceId);
            a(obtainTypedArray);
            obtainTypedArray.recycle();
        }
        obtainStyledAttributes.recycle();
        if (!k()) {
            this.f1242a = 0;
        } else if (this.f1242a == 1) {
            if (!this.f1248g) {
                DisplayMetrics displayMetrics = this.f1251j.getResources().getDisplayMetrics();
                if (dimension2 == -1.0f) {
                    dimension2 = TypedValue.applyDimension(2, 12.0f, displayMetrics);
                }
                if (dimension3 == -1.0f) {
                    dimension3 = TypedValue.applyDimension(2, 112.0f, displayMetrics);
                }
                if (dimension == -1.0f) {
                    dimension = 1.0f;
                }
                a(dimension2, dimension3, dimension);
            }
            i();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(int i2) {
        if (!k()) {
            return;
        }
        if (i2 == 0) {
            h();
        } else if (i2 == 1) {
            DisplayMetrics displayMetrics = this.f1251j.getResources().getDisplayMetrics();
            a(TypedValue.applyDimension(2, 12.0f, displayMetrics), TypedValue.applyDimension(2, 112.0f, displayMetrics), 1.0f);
            if (i()) {
                a();
            }
        } else {
            throw new IllegalArgumentException("Unknown auto-size text type: " + i2);
        }
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return Math.round(this.f1245d);
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return Math.round(this.f1244c);
    }

    /* access modifiers changed from: package-private */
    public int[] e() {
        return this.f1247f;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.f1242a;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return k() && this.f1242a != 0;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return Math.round(this.f1246e);
    }

    private StaticLayout b(CharSequence charSequence, Layout.Alignment alignment, int i2, int i3) {
        TextDirectionHeuristic textDirectionHeuristic;
        StaticLayout.Builder obtain = StaticLayout.Builder.obtain(charSequence, 0, charSequence.length(), this.f1249h, i2);
        StaticLayout.Builder hyphenationFrequency = obtain.setAlignment(alignment).setLineSpacing(this.f1250i.getLineSpacingExtra(), this.f1250i.getLineSpacingMultiplier()).setIncludePad(this.f1250i.getIncludeFontPadding()).setBreakStrategy(this.f1250i.getBreakStrategy()).setHyphenationFrequency(this.f1250i.getHyphenationFrequency());
        if (i3 == -1) {
            i3 = Api.BaseClientBuilder.API_PRIORITY_OTHER;
        }
        hyphenationFrequency.setMaxLines(i3);
        try {
            if (Build.VERSION.SDK_INT >= 29) {
                textDirectionHeuristic = this.f1250i.getTextDirectionHeuristic();
            } else {
                textDirectionHeuristic = (TextDirectionHeuristic) b(this.f1250i, "getTextDirectionHeuristic", TextDirectionHeuristics.FIRSTSTRONG_LTR);
            }
            obtain.setTextDirection(textDirectionHeuristic);
        } catch (ClassCastException unused) {
            Log.w("ACTVAutoSizeHelper", "Failed to obtain TextDirectionHeuristic, auto size may be incorrect");
        }
        return obtain.build();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4, int i5) throws IllegalArgumentException {
        if (k()) {
            DisplayMetrics displayMetrics = this.f1251j.getResources().getDisplayMetrics();
            a(TypedValue.applyDimension(i5, (float) i2, displayMetrics), TypedValue.applyDimension(i5, (float) i3, displayMetrics), TypedValue.applyDimension(i5, (float) i4, displayMetrics));
            if (i()) {
                a();
            }
        }
    }

    private StaticLayout b(CharSequence charSequence, Layout.Alignment alignment, int i2) {
        return new StaticLayout(charSequence, this.f1249h, i2, alignment, this.f1250i.getLineSpacingMultiplier(), this.f1250i.getLineSpacingExtra(), this.f1250i.getIncludeFontPadding());
    }

    private static <T> T b(Object obj, String str, T t) {
        try {
            return b(str).invoke(obj, new Object[0]);
        } catch (Exception e2) {
            Log.w("ACTVAutoSizeHelper", "Failed to invoke TextView#" + str + "() method", e2);
            return t;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int[] iArr, int i2) throws IllegalArgumentException {
        if (k()) {
            int length = iArr.length;
            if (length > 0) {
                int[] iArr2 = new int[length];
                if (i2 == 0) {
                    iArr2 = Arrays.copyOf(iArr, length);
                } else {
                    DisplayMetrics displayMetrics = this.f1251j.getResources().getDisplayMetrics();
                    for (int i3 = 0; i3 < length; i3++) {
                        iArr2[i3] = Math.round(TypedValue.applyDimension(i2, (float) iArr[i3], displayMetrics));
                    }
                }
                this.f1247f = a(iArr2);
                if (!j()) {
                    throw new IllegalArgumentException("None of the preset sizes is valid: " + Arrays.toString(iArr));
                }
            } else {
                this.f1248g = false;
            }
            if (i()) {
                a();
            }
        }
    }

    private static Method b(String str) {
        try {
            Method method = l.get(str);
            if (method == null && (method = TextView.class.getDeclaredMethod(str, new Class[0])) != null) {
                method.setAccessible(true);
                l.put(str, method);
            }
            return method;
        } catch (Exception e2) {
            Log.w("ACTVAutoSizeHelper", "Failed to retrieve TextView#" + str + "() method", e2);
            return null;
        }
    }

    private void a(TypedArray typedArray) {
        int length = typedArray.length();
        int[] iArr = new int[length];
        if (length > 0) {
            for (int i2 = 0; i2 < length; i2++) {
                iArr[i2] = typedArray.getDimensionPixelSize(i2, -1);
            }
            this.f1247f = a(iArr);
            j();
        }
    }

    private int[] a(int[] iArr) {
        if (r0 == 0) {
            return iArr;
        }
        Arrays.sort(iArr);
        ArrayList arrayList = new ArrayList();
        for (int i2 : iArr) {
            if (i2 > 0 && Collections.binarySearch(arrayList, Integer.valueOf(i2)) < 0) {
                arrayList.add(Integer.valueOf(i2));
            }
        }
        if (r0 == arrayList.size()) {
            return iArr;
        }
        int size = arrayList.size();
        int[] iArr2 = new int[size];
        for (int i3 = 0; i3 < size; i3++) {
            iArr2[i3] = ((Integer) arrayList.get(i3)).intValue();
        }
        return iArr2;
    }

    private void a(float f2, float f3, float f4) throws IllegalArgumentException {
        if (f2 <= Animation.CurveTimeline.LINEAR) {
            throw new IllegalArgumentException("Minimum auto-size text size (" + f2 + "px) is less or equal to (0px)");
        } else if (f3 <= f2) {
            throw new IllegalArgumentException("Maximum auto-size text size (" + f3 + "px) is less or equal to minimum auto-size text size (" + f2 + "px)");
        } else if (f4 > Animation.CurveTimeline.LINEAR) {
            this.f1242a = 1;
            this.f1245d = f2;
            this.f1246e = f3;
            this.f1244c = f4;
            this.f1248g = false;
        } else {
            throw new IllegalArgumentException("The auto-size step granularity (" + f4 + "px) is less or equal to (0px)");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.z.b(java.lang.Object, java.lang.String, java.lang.Object):T
     arg types: [android.widget.TextView, java.lang.String, boolean]
     candidates:
      androidx.appcompat.widget.z.b(java.lang.CharSequence, android.text.Layout$Alignment, int):android.text.StaticLayout
      androidx.appcompat.widget.z.b(java.lang.Object, java.lang.String, java.lang.Object):T */
    /* access modifiers changed from: package-private */
    public void a() {
        boolean z;
        int i2;
        if (g()) {
            if (this.f1243b) {
                if (this.f1250i.getMeasuredHeight() > 0 && this.f1250i.getMeasuredWidth() > 0) {
                    if (Build.VERSION.SDK_INT >= 29) {
                        z = this.f1250i.isHorizontallyScrollable();
                    } else {
                        z = ((Boolean) b((Object) this.f1250i, "getHorizontallyScrolling", (Object) false)).booleanValue();
                    }
                    if (z) {
                        i2 = 1048576;
                    } else {
                        i2 = (this.f1250i.getMeasuredWidth() - this.f1250i.getTotalPaddingLeft()) - this.f1250i.getTotalPaddingRight();
                    }
                    int height = (this.f1250i.getHeight() - this.f1250i.getCompoundPaddingBottom()) - this.f1250i.getCompoundPaddingTop();
                    if (i2 > 0 && height > 0) {
                        synchronized (f1241k) {
                            f1241k.setEmpty();
                            f1241k.right = (float) i2;
                            f1241k.bottom = (float) height;
                            float a2 = (float) a(f1241k);
                            if (a2 != this.f1250i.getTextSize()) {
                                a(0, a2);
                            }
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
            this.f1243b = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, float f2) {
        Resources resources;
        Context context = this.f1251j;
        if (context == null) {
            resources = Resources.getSystem();
        } else {
            resources = context.getResources();
        }
        a(TypedValue.applyDimension(i2, f2, resources.getDisplayMetrics()));
    }

    private void a(float f2) {
        if (f2 != this.f1250i.getPaint().getTextSize()) {
            this.f1250i.getPaint().setTextSize(f2);
            boolean isInLayout = Build.VERSION.SDK_INT >= 18 ? this.f1250i.isInLayout() : false;
            if (this.f1250i.getLayout() != null) {
                this.f1243b = false;
                try {
                    Method b2 = b("nullLayouts");
                    if (b2 != null) {
                        b2.invoke(this.f1250i, new Object[0]);
                    }
                } catch (Exception e2) {
                    Log.w("ACTVAutoSizeHelper", "Failed to invoke TextView#nullLayouts() method", e2);
                }
                if (!isInLayout) {
                    this.f1250i.requestLayout();
                } else {
                    this.f1250i.forceLayout();
                }
                this.f1250i.invalidate();
            }
        }
    }

    private int a(RectF rectF) {
        int length = this.f1247f.length;
        if (length != 0) {
            int i2 = length - 1;
            int i3 = 1;
            int i4 = 0;
            while (i3 <= i2) {
                int i5 = (i3 + i2) / 2;
                if (a(this.f1247f[i5], rectF)) {
                    int i6 = i5 + 1;
                    i4 = i3;
                    i3 = i6;
                } else {
                    i4 = i5 - 1;
                    i2 = i4;
                }
            }
            return this.f1247f[i4];
        }
        throw new IllegalStateException("No available text sizes to choose from.");
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        TextPaint textPaint = this.f1249h;
        if (textPaint == null) {
            this.f1249h = new TextPaint();
        } else {
            textPaint.reset();
        }
        this.f1249h.set(this.f1250i.getPaint());
        this.f1249h.setTextSize((float) i2);
    }

    /* access modifiers changed from: package-private */
    public StaticLayout a(CharSequence charSequence, Layout.Alignment alignment, int i2, int i3) {
        int i4 = Build.VERSION.SDK_INT;
        if (i4 >= 23) {
            return b(charSequence, alignment, i2, i3);
        }
        if (i4 >= 16) {
            return b(charSequence, alignment, i2);
        }
        return a(charSequence, alignment, i2);
    }

    private boolean a(int i2, RectF rectF) {
        CharSequence transformation;
        CharSequence text = this.f1250i.getText();
        TransformationMethod transformationMethod = this.f1250i.getTransformationMethod();
        if (!(transformationMethod == null || (transformation = transformationMethod.getTransformation(text, this.f1250i)) == null)) {
            text = transformation;
        }
        int maxLines = Build.VERSION.SDK_INT >= 16 ? this.f1250i.getMaxLines() : -1;
        a(i2);
        StaticLayout a2 = a(text, (Layout.Alignment) b(this.f1250i, "getLayoutAlignment", Layout.Alignment.ALIGN_NORMAL), Math.round(rectF.right), maxLines);
        return (maxLines == -1 || (a2.getLineCount() <= maxLines && a2.getLineEnd(a2.getLineCount() - 1) == text.length())) && ((float) a2.getHeight()) <= rectF.bottom;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.z.a(java.lang.Object, java.lang.String, java.lang.Object):T
     arg types: [android.widget.TextView, java.lang.String, boolean]
     candidates:
      androidx.appcompat.widget.z.a(java.lang.CharSequence, android.text.Layout$Alignment, int):android.text.StaticLayout
      androidx.appcompat.widget.z.a(float, float, float):void
      androidx.appcompat.widget.z.a(java.lang.Object, java.lang.String, java.lang.Object):T */
    private StaticLayout a(CharSequence charSequence, Layout.Alignment alignment, int i2) {
        return new StaticLayout(charSequence, this.f1249h, i2, alignment, ((Float) a(this.f1250i, "mSpacingMult", Float.valueOf(1.0f))).floatValue(), ((Float) a(this.f1250i, "mSpacingAdd", Float.valueOf((float) Animation.CurveTimeline.LINEAR))).floatValue(), ((Boolean) a((Object) this.f1250i, "mIncludePad", (Object) true)).booleanValue());
    }

    private static <T> T a(Object obj, String str, T t) {
        try {
            Field a2 = a(str);
            if (a2 == null) {
                return t;
            }
            return a2.get(obj);
        } catch (IllegalAccessException e2) {
            Log.w("ACTVAutoSizeHelper", "Failed to access TextView#" + str + " member", e2);
            return t;
        }
    }

    private static Field a(String str) {
        try {
            Field field = m.get(str);
            if (field == null && (field = TextView.class.getDeclaredField(str)) != null) {
                field.setAccessible(true);
                m.put(str, field);
            }
            return field;
        } catch (NoSuchFieldException e2) {
            Log.w("ACTVAutoSizeHelper", "Failed to access TextView#" + str + " member", e2);
            return null;
        }
    }
}
