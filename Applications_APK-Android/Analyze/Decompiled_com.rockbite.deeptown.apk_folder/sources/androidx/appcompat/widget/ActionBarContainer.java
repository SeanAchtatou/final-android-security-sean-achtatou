package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import b.a.f;
import b.a.j;
import b.e.m.u;
import com.google.android.gms.common.api.Api;

public class ActionBarContainer extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private boolean f863a;

    /* renamed from: b  reason: collision with root package name */
    private View f864b;

    /* renamed from: c  reason: collision with root package name */
    private View f865c;

    /* renamed from: d  reason: collision with root package name */
    private View f866d;

    /* renamed from: e  reason: collision with root package name */
    Drawable f867e;

    /* renamed from: f  reason: collision with root package name */
    Drawable f868f;

    /* renamed from: g  reason: collision with root package name */
    Drawable f869g;

    /* renamed from: h  reason: collision with root package name */
    boolean f870h;

    /* renamed from: i  reason: collision with root package name */
    boolean f871i;

    /* renamed from: j  reason: collision with root package name */
    private int f872j;

    public ActionBarContainer(Context context) {
        this(context, null);
    }

    private int a(View view) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        return view.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
    }

    private boolean b(View view) {
        return view == null || view.getVisibility() == 8 || view.getMeasuredHeight() == 0;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.f867e;
        if (drawable != null && drawable.isStateful()) {
            this.f867e.setState(getDrawableState());
        }
        Drawable drawable2 = this.f868f;
        if (drawable2 != null && drawable2.isStateful()) {
            this.f868f.setState(getDrawableState());
        }
        Drawable drawable3 = this.f869g;
        if (drawable3 != null && drawable3.isStateful()) {
            this.f869g.setState(getDrawableState());
        }
    }

    public View getTabContainer() {
        return this.f864b;
    }

    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.f867e;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
        Drawable drawable2 = this.f868f;
        if (drawable2 != null) {
            drawable2.jumpToCurrentState();
        }
        Drawable drawable3 = this.f869g;
        if (drawable3 != null) {
            drawable3.jumpToCurrentState();
        }
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        this.f865c = findViewById(f.action_bar);
        this.f866d = findViewById(f.action_context_bar);
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        super.onHoverEvent(motionEvent);
        return true;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.f863a || super.onInterceptTouchEvent(motionEvent);
    }

    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        Drawable drawable;
        super.onLayout(z, i2, i3, i4, i5);
        View view = this.f864b;
        boolean z2 = true;
        boolean z3 = false;
        boolean z4 = (view == null || view.getVisibility() == 8) ? false : true;
        if (!(view == null || view.getVisibility() == 8)) {
            int measuredHeight = getMeasuredHeight();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
            view.layout(i2, (measuredHeight - view.getMeasuredHeight()) - layoutParams.bottomMargin, i4, measuredHeight - layoutParams.bottomMargin);
        }
        if (this.f870h) {
            Drawable drawable2 = this.f869g;
            if (drawable2 != null) {
                drawable2.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
            } else {
                z2 = false;
            }
        } else {
            if (this.f867e != null) {
                if (this.f865c.getVisibility() == 0) {
                    this.f867e.setBounds(this.f865c.getLeft(), this.f865c.getTop(), this.f865c.getRight(), this.f865c.getBottom());
                } else {
                    View view2 = this.f866d;
                    if (view2 == null || view2.getVisibility() != 0) {
                        this.f867e.setBounds(0, 0, 0, 0);
                    } else {
                        this.f867e.setBounds(this.f866d.getLeft(), this.f866d.getTop(), this.f866d.getRight(), this.f866d.getBottom());
                    }
                }
                z3 = true;
            }
            this.f871i = z4;
            if (!z4 || (drawable = this.f868f) == null) {
                z2 = z3;
            } else {
                drawable.setBounds(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
        }
        if (z2) {
            invalidate();
        }
    }

    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        if (this.f865c == null && View.MeasureSpec.getMode(i3) == Integer.MIN_VALUE && (i5 = this.f872j) >= 0) {
            i3 = View.MeasureSpec.makeMeasureSpec(Math.min(i5, View.MeasureSpec.getSize(i3)), Integer.MIN_VALUE);
        }
        super.onMeasure(i2, i3);
        if (this.f865c != null) {
            int mode = View.MeasureSpec.getMode(i3);
            View view = this.f864b;
            if (view != null && view.getVisibility() != 8 && mode != 1073741824) {
                if (!b(this.f865c)) {
                    i4 = a(this.f865c);
                } else {
                    i4 = !b(this.f866d) ? a(this.f866d) : 0;
                }
                setMeasuredDimension(getMeasuredWidth(), Math.min(i4 + a(this.f864b), mode == Integer.MIN_VALUE ? View.MeasureSpec.getSize(i3) : Api.BaseClientBuilder.API_PRIORITY_OTHER));
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        return true;
    }

    public void setPrimaryBackground(Drawable drawable) {
        Drawable drawable2 = this.f867e;
        if (drawable2 != null) {
            drawable2.setCallback(null);
            unscheduleDrawable(this.f867e);
        }
        this.f867e = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            View view = this.f865c;
            if (view != null) {
                this.f867e.setBounds(view.getLeft(), this.f865c.getTop(), this.f865c.getRight(), this.f865c.getBottom());
            }
        }
        boolean z = true;
        if (!this.f870h ? !(this.f867e == null && this.f868f == null) : this.f869g != null) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
        if (Build.VERSION.SDK_INT >= 21) {
            invalidateOutline();
        }
    }

    public void setSplitBackground(Drawable drawable) {
        Drawable drawable2;
        Drawable drawable3 = this.f869g;
        if (drawable3 != null) {
            drawable3.setCallback(null);
            unscheduleDrawable(this.f869g);
        }
        this.f869g = drawable;
        boolean z = false;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.f870h && (drawable2 = this.f869g) != null) {
                drawable2.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
            }
        }
        if (!this.f870h ? this.f867e == null && this.f868f == null : this.f869g == null) {
            z = true;
        }
        setWillNotDraw(z);
        invalidate();
        if (Build.VERSION.SDK_INT >= 21) {
            invalidateOutline();
        }
    }

    public void setStackedBackground(Drawable drawable) {
        Drawable drawable2;
        Drawable drawable3 = this.f868f;
        if (drawable3 != null) {
            drawable3.setCallback(null);
            unscheduleDrawable(this.f868f);
        }
        this.f868f = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.f871i && (drawable2 = this.f868f) != null) {
                drawable2.setBounds(this.f864b.getLeft(), this.f864b.getTop(), this.f864b.getRight(), this.f864b.getBottom());
            }
        }
        boolean z = true;
        if (!this.f870h ? !(this.f867e == null && this.f868f == null) : this.f869g != null) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
        if (Build.VERSION.SDK_INT >= 21) {
            invalidateOutline();
        }
    }

    public void setTabContainer(o0 o0Var) {
        View view = this.f864b;
        if (view != null) {
            removeView(view);
        }
        this.f864b = o0Var;
        if (o0Var != null) {
            addView(o0Var);
            ViewGroup.LayoutParams layoutParams = o0Var.getLayoutParams();
            layoutParams.width = -1;
            layoutParams.height = -2;
            o0Var.setAllowCollapse(false);
        }
    }

    public void setTransitioning(boolean z) {
        this.f863a = z;
        setDescendantFocusability(z ? 393216 : 262144);
    }

    public void setVisibility(int i2) {
        super.setVisibility(i2);
        boolean z = i2 == 0;
        Drawable drawable = this.f867e;
        if (drawable != null) {
            drawable.setVisible(z, false);
        }
        Drawable drawable2 = this.f868f;
        if (drawable2 != null) {
            drawable2.setVisible(z, false);
        }
        Drawable drawable3 = this.f869g;
        if (drawable3 != null) {
            drawable3.setVisible(z, false);
        }
    }

    public ActionMode startActionModeForChild(View view, ActionMode.Callback callback) {
        return null;
    }

    public ActionMode startActionModeForChild(View view, ActionMode.Callback callback, int i2) {
        if (i2 != 0) {
            return super.startActionModeForChild(view, callback, i2);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return (drawable == this.f867e && !this.f870h) || (drawable == this.f868f && this.f871i) || ((drawable == this.f869g && this.f870h) || super.verifyDrawable(drawable));
    }

    public ActionBarContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        u.a(this, new b(this));
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.ActionBar);
        this.f867e = obtainStyledAttributes.getDrawable(j.ActionBar_background);
        this.f868f = obtainStyledAttributes.getDrawable(j.ActionBar_backgroundStacked);
        this.f872j = obtainStyledAttributes.getDimensionPixelSize(j.ActionBar_height, -1);
        if (getId() == f.split_action_bar) {
            this.f870h = true;
            this.f869g = obtainStyledAttributes.getDrawable(j.ActionBar_backgroundSplit);
        }
        obtainStyledAttributes.recycle();
        boolean z = false;
        if (!this.f870h ? this.f867e == null && this.f868f == null : this.f869g == null) {
            z = true;
        }
        setWillNotDraw(z);
    }
}
