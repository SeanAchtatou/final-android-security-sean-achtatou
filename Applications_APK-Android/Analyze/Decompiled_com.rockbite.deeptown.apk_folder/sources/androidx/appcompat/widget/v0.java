package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import b.a.k.a.a;
import b.e.e.c.f;

/* compiled from: TintTypedArray */
public class v0 {

    /* renamed from: a  reason: collision with root package name */
    private final Context f1187a;

    /* renamed from: b  reason: collision with root package name */
    private final TypedArray f1188b;

    /* renamed from: c  reason: collision with root package name */
    private TypedValue f1189c;

    private v0(Context context, TypedArray typedArray) {
        this.f1187a = context;
        this.f1188b = typedArray;
    }

    public static v0 a(Context context, AttributeSet attributeSet, int[] iArr) {
        return new v0(context, context.obtainStyledAttributes(attributeSet, iArr));
    }

    public Drawable b(int i2) {
        int resourceId;
        if (!this.f1188b.hasValue(i2) || (resourceId = this.f1188b.getResourceId(i2, 0)) == 0) {
            return this.f1188b.getDrawable(i2);
        }
        return a.c(this.f1187a, resourceId);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.j.a(android.content.Context, int, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, int, int]
     candidates:
      androidx.appcompat.widget.j.a(android.graphics.drawable.Drawable, androidx.appcompat.widget.t0, int[]):void
      androidx.appcompat.widget.j.a(android.content.Context, int, boolean):android.graphics.drawable.Drawable */
    public Drawable c(int i2) {
        int resourceId;
        if (!this.f1188b.hasValue(i2) || (resourceId = this.f1188b.getResourceId(i2, 0)) == 0) {
            return null;
        }
        return j.b().a(this.f1187a, resourceId, true);
    }

    public String d(int i2) {
        return this.f1188b.getString(i2);
    }

    public CharSequence e(int i2) {
        return this.f1188b.getText(i2);
    }

    public int f(int i2, int i3) {
        return this.f1188b.getLayoutDimension(i2, i3);
    }

    public int g(int i2, int i3) {
        return this.f1188b.getResourceId(i2, i3);
    }

    public static v0 a(Context context, AttributeSet attributeSet, int[] iArr, int i2, int i3) {
        return new v0(context, context.obtainStyledAttributes(attributeSet, iArr, i2, i3));
    }

    public int d(int i2, int i3) {
        return this.f1188b.getInt(i2, i3);
    }

    public int e(int i2, int i3) {
        return this.f1188b.getInteger(i2, i3);
    }

    public CharSequence[] f(int i2) {
        return this.f1188b.getTextArray(i2);
    }

    public boolean g(int i2) {
        return this.f1188b.hasValue(i2);
    }

    public static v0 a(Context context, int i2, int[] iArr) {
        return new v0(context, context.obtainStyledAttributes(i2, iArr));
    }

    public int c(int i2, int i3) {
        return this.f1188b.getDimensionPixelSize(i2, i3);
    }

    public Typeface a(int i2, int i3, f.a aVar) {
        int resourceId = this.f1188b.getResourceId(i2, 0);
        if (resourceId == 0) {
            return null;
        }
        if (this.f1189c == null) {
            this.f1189c = new TypedValue();
        }
        return f.a(this.f1187a, resourceId, this.f1189c, i3, aVar);
    }

    public int b(int i2, int i3) {
        return this.f1188b.getDimensionPixelOffset(i2, i3);
    }

    public boolean a(int i2, boolean z) {
        return this.f1188b.getBoolean(i2, z);
    }

    public float a(int i2, float f2) {
        return this.f1188b.getFloat(i2, f2);
    }

    public int a(int i2, int i3) {
        return this.f1188b.getColor(i2, i3);
    }

    public ColorStateList a(int i2) {
        int resourceId;
        ColorStateList b2;
        if (!this.f1188b.hasValue(i2) || (resourceId = this.f1188b.getResourceId(i2, 0)) == 0 || (b2 = a.b(this.f1187a, resourceId)) == null) {
            return this.f1188b.getColorStateList(i2);
        }
        return b2;
    }

    public void a() {
        this.f1188b.recycle();
    }
}
