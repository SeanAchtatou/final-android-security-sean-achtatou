package androidx.appcompat.widget;

/* compiled from: RtlSpacingHelper */
class n0 {

    /* renamed from: a  reason: collision with root package name */
    private int f1118a = 0;

    /* renamed from: b  reason: collision with root package name */
    private int f1119b = 0;

    /* renamed from: c  reason: collision with root package name */
    private int f1120c = Integer.MIN_VALUE;

    /* renamed from: d  reason: collision with root package name */
    private int f1121d = Integer.MIN_VALUE;

    /* renamed from: e  reason: collision with root package name */
    private int f1122e = 0;

    /* renamed from: f  reason: collision with root package name */
    private int f1123f = 0;

    /* renamed from: g  reason: collision with root package name */
    private boolean f1124g = false;

    /* renamed from: h  reason: collision with root package name */
    private boolean f1125h = false;

    n0() {
    }

    public int a() {
        return this.f1124g ? this.f1118a : this.f1119b;
    }

    public int b() {
        return this.f1118a;
    }

    public int c() {
        return this.f1119b;
    }

    public int d() {
        return this.f1124g ? this.f1119b : this.f1118a;
    }

    public void a(int i2, int i3) {
        this.f1125h = false;
        if (i2 != Integer.MIN_VALUE) {
            this.f1122e = i2;
            this.f1118a = i2;
        }
        if (i3 != Integer.MIN_VALUE) {
            this.f1123f = i3;
            this.f1119b = i3;
        }
    }

    public void b(int i2, int i3) {
        this.f1120c = i2;
        this.f1121d = i3;
        this.f1125h = true;
        if (this.f1124g) {
            if (i3 != Integer.MIN_VALUE) {
                this.f1118a = i3;
            }
            if (i2 != Integer.MIN_VALUE) {
                this.f1119b = i2;
                return;
            }
            return;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f1118a = i2;
        }
        if (i3 != Integer.MIN_VALUE) {
            this.f1119b = i3;
        }
    }

    public void a(boolean z) {
        if (z != this.f1124g) {
            this.f1124g = z;
            if (!this.f1125h) {
                this.f1118a = this.f1122e;
                this.f1119b = this.f1123f;
            } else if (z) {
                int i2 = this.f1121d;
                if (i2 == Integer.MIN_VALUE) {
                    i2 = this.f1122e;
                }
                this.f1118a = i2;
                int i3 = this.f1120c;
                if (i3 == Integer.MIN_VALUE) {
                    i3 = this.f1123f;
                }
                this.f1119b = i3;
            } else {
                int i4 = this.f1120c;
                if (i4 == Integer.MIN_VALUE) {
                    i4 = this.f1122e;
                }
                this.f1118a = i4;
                int i5 = this.f1121d;
                if (i5 == Integer.MIN_VALUE) {
                    i5 = this.f1123f;
                }
                this.f1119b = i5;
            }
        }
    }
}
