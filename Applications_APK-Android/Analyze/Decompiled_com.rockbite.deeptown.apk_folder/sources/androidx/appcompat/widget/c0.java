package androidx.appcompat.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;
import androidx.appcompat.view.menu.n;
import b.e.m.y;

/* compiled from: DecorToolbar */
public interface c0 {
    y a(int i2, long j2);

    void a(int i2);

    void a(Menu menu, n.a aVar);

    void a(o0 o0Var);

    void a(boolean z);

    boolean a();

    void b();

    void b(int i2);

    void b(boolean z);

    void c(int i2);

    boolean c();

    void collapseActionView();

    boolean d();

    boolean e();

    boolean f();

    void g();

    Context getContext();

    CharSequence getTitle();

    boolean h();

    int i();

    ViewGroup j();

    int k();

    void l();

    void m();

    void setIcon(int i2);

    void setIcon(Drawable drawable);

    void setWindowCallback(Window.Callback callback);

    void setWindowTitle(CharSequence charSequence);
}
