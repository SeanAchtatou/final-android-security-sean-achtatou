package androidx.appcompat.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.widget.OverScroller;
import androidx.appcompat.view.menu.n;
import b.a.f;
import b.e.m.m;
import b.e.m.n;
import b.e.m.o;
import b.e.m.p;
import b.e.m.u;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.common.api.Api;

public class ActionBarOverlayLayout extends ViewGroup implements b0, o, m, n {
    static final int[] B = {b.a.a.actionBarSize, 16842841};
    private final p A;

    /* renamed from: a  reason: collision with root package name */
    private int f877a;

    /* renamed from: b  reason: collision with root package name */
    private int f878b;

    /* renamed from: c  reason: collision with root package name */
    private ContentFrameLayout f879c;

    /* renamed from: d  reason: collision with root package name */
    ActionBarContainer f880d;

    /* renamed from: e  reason: collision with root package name */
    private c0 f881e;

    /* renamed from: f  reason: collision with root package name */
    private Drawable f882f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f883g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f884h;

    /* renamed from: i  reason: collision with root package name */
    private boolean f885i;

    /* renamed from: j  reason: collision with root package name */
    private boolean f886j;

    /* renamed from: k  reason: collision with root package name */
    boolean f887k;
    private int l;
    private int m;
    private final Rect n;
    private final Rect o;
    private final Rect p;
    private final Rect q;
    private final Rect r;
    private final Rect s;
    private final Rect t;
    private d u;
    private OverScroller v;
    ViewPropertyAnimator w;
    final AnimatorListenerAdapter x;
    private final Runnable y;
    private final Runnable z;

    class a extends AnimatorListenerAdapter {
        a() {
        }

        public void onAnimationCancel(Animator animator) {
            ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
            actionBarOverlayLayout.w = null;
            actionBarOverlayLayout.f887k = false;
        }

        public void onAnimationEnd(Animator animator) {
            ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
            actionBarOverlayLayout.w = null;
            actionBarOverlayLayout.f887k = false;
        }
    }

    class b implements Runnable {
        b() {
        }

        public void run() {
            ActionBarOverlayLayout.this.h();
            ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
            actionBarOverlayLayout.w = actionBarOverlayLayout.f880d.animate().translationY(Animation.CurveTimeline.LINEAR).setListener(ActionBarOverlayLayout.this.x);
        }
    }

    class c implements Runnable {
        c() {
        }

        public void run() {
            ActionBarOverlayLayout.this.h();
            ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
            actionBarOverlayLayout.w = actionBarOverlayLayout.f880d.animate().translationY((float) (-ActionBarOverlayLayout.this.f880d.getHeight())).setListener(ActionBarOverlayLayout.this.x);
        }
    }

    public interface d {
        void a();

        void a(boolean z);

        void b();

        void c();

        void d();

        void onWindowVisibilityChanged(int i2);
    }

    public static class e extends ViewGroup.MarginLayoutParams {
        public e(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public e(int i2, int i3) {
            super(i2, i3);
        }

        public e(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    public ActionBarOverlayLayout(Context context) {
        this(context, null);
    }

    private void a(Context context) {
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(B);
        boolean z2 = false;
        this.f877a = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        this.f882f = obtainStyledAttributes.getDrawable(1);
        setWillNotDraw(this.f882f == null);
        obtainStyledAttributes.recycle();
        if (context.getApplicationInfo().targetSdkVersion < 19) {
            z2 = true;
        }
        this.f883g = z2;
        this.v = new OverScroller(context);
    }

    private void k() {
        h();
        this.z.run();
    }

    private void l() {
        h();
        postDelayed(this.z, 600);
    }

    private void m() {
        h();
        postDelayed(this.y, 600);
    }

    private void n() {
        h();
        this.y.run();
    }

    public void b(View view, View view2, int i2, int i3) {
        if (i3 == 0) {
            onNestedScrollAccepted(view, view2, i2);
        }
    }

    public boolean c() {
        j();
        return this.f881e.c();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof e;
    }

    public boolean d() {
        j();
        return this.f881e.d();
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f882f != null && !this.f883g) {
            int bottom = this.f880d.getVisibility() == 0 ? (int) (((float) this.f880d.getBottom()) + this.f880d.getTranslationY() + 0.5f) : 0;
            this.f882f.setBounds(0, bottom, getWidth(), this.f882f.getIntrinsicHeight() + bottom);
            this.f882f.draw(canvas);
        }
    }

    public boolean e() {
        j();
        return this.f881e.e();
    }

    public boolean f() {
        j();
        return this.f881e.f();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.ActionBarOverlayLayout.a(android.view.View, android.graphics.Rect, boolean, boolean, boolean, boolean):boolean
     arg types: [androidx.appcompat.widget.ActionBarContainer, android.graphics.Rect, int, int, int, int]
     candidates:
      androidx.appcompat.widget.ActionBarOverlayLayout.a(android.view.View, int, int, int, int, int):void
      b.e.m.m.a(android.view.View, int, int, int, int, int):void
      b.e.m.m.a(android.view.View, int, int, int, int, int):void
      androidx.appcompat.widget.ActionBarOverlayLayout.a(android.view.View, android.graphics.Rect, boolean, boolean, boolean, boolean):boolean */
    /* access modifiers changed from: protected */
    public boolean fitSystemWindows(Rect rect) {
        j();
        int o2 = u.o(this) & 256;
        boolean a2 = a((View) this.f880d, rect, true, true, false, true);
        this.q.set(rect);
        b1.a(this, this.q, this.n);
        if (!this.r.equals(this.q)) {
            this.r.set(this.q);
            a2 = true;
        }
        if (!this.o.equals(this.n)) {
            this.o.set(this.n);
            a2 = true;
        }
        if (a2) {
            requestLayout();
        }
        return true;
    }

    public void g() {
        j();
        this.f881e.g();
    }

    public int getActionBarHideOffset() {
        ActionBarContainer actionBarContainer = this.f880d;
        if (actionBarContainer != null) {
            return -((int) actionBarContainer.getTranslationY());
        }
        return 0;
    }

    public int getNestedScrollAxes() {
        return this.A.a();
    }

    public CharSequence getTitle() {
        j();
        return this.f881e.getTitle();
    }

    /* access modifiers changed from: package-private */
    public void h() {
        removeCallbacks(this.y);
        removeCallbacks(this.z);
        ViewPropertyAnimator viewPropertyAnimator = this.w;
        if (viewPropertyAnimator != null) {
            viewPropertyAnimator.cancel();
        }
    }

    public boolean i() {
        return this.f884h;
    }

    /* access modifiers changed from: package-private */
    public void j() {
        if (this.f879c == null) {
            this.f879c = (ContentFrameLayout) findViewById(f.action_bar_activity_content);
            this.f880d = (ActionBarContainer) findViewById(f.action_bar_container);
            this.f881e = a(findViewById(f.action_bar));
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        a(getContext());
        u.w(this);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        h();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        int paddingLeft = getPaddingLeft();
        getPaddingRight();
        int paddingTop = getPaddingTop();
        getPaddingBottom();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 8) {
                e eVar = (e) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                int i7 = eVar.leftMargin + paddingLeft;
                int i8 = eVar.topMargin + paddingTop;
                childAt.layout(i7, i8, measuredWidth + i7, measuredHeight + i8);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.ActionBarOverlayLayout.a(android.view.View, android.graphics.Rect, boolean, boolean, boolean, boolean):boolean
     arg types: [androidx.appcompat.widget.ContentFrameLayout, android.graphics.Rect, int, int, int, int]
     candidates:
      androidx.appcompat.widget.ActionBarOverlayLayout.a(android.view.View, int, int, int, int, int):void
      b.e.m.m.a(android.view.View, int, int, int, int, int):void
      b.e.m.m.a(android.view.View, int, int, int, int, int):void
      androidx.appcompat.widget.ActionBarOverlayLayout.a(android.view.View, android.graphics.Rect, boolean, boolean, boolean, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        j();
        measureChildWithMargins(this.f880d, i2, 0, i3, 0);
        e eVar = (e) this.f880d.getLayoutParams();
        int max = Math.max(0, this.f880d.getMeasuredWidth() + eVar.leftMargin + eVar.rightMargin);
        int max2 = Math.max(0, this.f880d.getMeasuredHeight() + eVar.topMargin + eVar.bottomMargin);
        int combineMeasuredStates = View.combineMeasuredStates(0, this.f880d.getMeasuredState());
        boolean z2 = (u.o(this) & 256) != 0;
        if (z2) {
            i4 = this.f877a;
            if (this.f885i && this.f880d.getTabContainer() != null) {
                i4 += this.f877a;
            }
        } else {
            i4 = this.f880d.getVisibility() != 8 ? this.f880d.getMeasuredHeight() : 0;
        }
        this.p.set(this.n);
        this.s.set(this.q);
        if (this.f884h || z2) {
            Rect rect = this.s;
            rect.top += i4;
            rect.bottom += 0;
        } else {
            Rect rect2 = this.p;
            rect2.top += i4;
            rect2.bottom += 0;
        }
        a((View) this.f879c, this.p, true, true, true, true);
        if (!this.t.equals(this.s)) {
            this.t.set(this.s);
            this.f879c.a(this.s);
        }
        measureChildWithMargins(this.f879c, i2, 0, i3, 0);
        e eVar2 = (e) this.f879c.getLayoutParams();
        int max3 = Math.max(max, this.f879c.getMeasuredWidth() + eVar2.leftMargin + eVar2.rightMargin);
        int max4 = Math.max(max2, this.f879c.getMeasuredHeight() + eVar2.topMargin + eVar2.bottomMargin);
        int combineMeasuredStates2 = View.combineMeasuredStates(combineMeasuredStates, this.f879c.getMeasuredState());
        setMeasuredDimension(View.resolveSizeAndState(Math.max(max3 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i2, combineMeasuredStates2), View.resolveSizeAndState(Math.max(max4 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i3, combineMeasuredStates2 << 16));
    }

    public boolean onNestedFling(View view, float f2, float f3, boolean z2) {
        if (!this.f886j || !z2) {
            return false;
        }
        if (a(f2, f3)) {
            k();
        } else {
            n();
        }
        this.f887k = true;
        return true;
    }

    public boolean onNestedPreFling(View view, float f2, float f3) {
        return false;
    }

    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr) {
    }

    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        this.l += i3;
        setActionBarHideOffset(this.l);
    }

    public void onNestedScrollAccepted(View view, View view2, int i2) {
        this.A.a(view, view2, i2);
        this.l = getActionBarHideOffset();
        h();
        d dVar = this.u;
        if (dVar != null) {
            dVar.d();
        }
    }

    public boolean onStartNestedScroll(View view, View view2, int i2) {
        if ((i2 & 2) == 0 || this.f880d.getVisibility() != 0) {
            return false;
        }
        return this.f886j;
    }

    public void onStopNestedScroll(View view) {
        if (this.f886j && !this.f887k) {
            if (this.l <= this.f880d.getHeight()) {
                m();
            } else {
                l();
            }
        }
        d dVar = this.u;
        if (dVar != null) {
            dVar.b();
        }
    }

    public void onWindowSystemUiVisibilityChanged(int i2) {
        if (Build.VERSION.SDK_INT >= 16) {
            super.onWindowSystemUiVisibilityChanged(i2);
        }
        j();
        int i3 = this.m ^ i2;
        this.m = i2;
        boolean z2 = false;
        boolean z3 = (i2 & 4) == 0;
        if ((i2 & 256) != 0) {
            z2 = true;
        }
        d dVar = this.u;
        if (dVar != null) {
            dVar.a(!z2);
            if (z3 || !z2) {
                this.u.a();
            } else {
                this.u.c();
            }
        }
        if ((i3 & 256) != 0 && this.u != null) {
            u.w(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        this.f878b = i2;
        d dVar = this.u;
        if (dVar != null) {
            dVar.onWindowVisibilityChanged(i2);
        }
    }

    public void setActionBarHideOffset(int i2) {
        h();
        this.f880d.setTranslationY((float) (-Math.max(0, Math.min(i2, this.f880d.getHeight()))));
    }

    public void setActionBarVisibilityCallback(d dVar) {
        this.u = dVar;
        if (getWindowToken() != null) {
            this.u.onWindowVisibilityChanged(this.f878b);
            int i2 = this.m;
            if (i2 != 0) {
                onWindowSystemUiVisibilityChanged(i2);
                u.w(this);
            }
        }
    }

    public void setHasNonEmbeddedTabs(boolean z2) {
        this.f885i = z2;
    }

    public void setHideOnContentScrollEnabled(boolean z2) {
        if (z2 != this.f886j) {
            this.f886j = z2;
            if (!z2) {
                h();
                setActionBarHideOffset(0);
            }
        }
    }

    public void setIcon(int i2) {
        j();
        this.f881e.setIcon(i2);
    }

    public void setLogo(int i2) {
        j();
        this.f881e.b(i2);
    }

    public void setOverlayMode(boolean z2) {
        this.f884h = z2;
        this.f883g = z2 && getContext().getApplicationInfo().targetSdkVersion < 19;
    }

    public void setShowingForActionMode(boolean z2) {
    }

    public void setUiOptions(int i2) {
    }

    public void setWindowCallback(Window.Callback callback) {
        j();
        this.f881e.setWindowCallback(callback);
    }

    public void setWindowTitle(CharSequence charSequence) {
        j();
        this.f881e.setWindowTitle(charSequence);
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public ActionBarOverlayLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f878b = 0;
        this.n = new Rect();
        this.o = new Rect();
        this.p = new Rect();
        this.q = new Rect();
        this.r = new Rect();
        this.s = new Rect();
        this.t = new Rect();
        this.x = new a();
        this.y = new b();
        this.z = new c();
        a(context);
        this.A = new p(this);
    }

    public void b() {
        j();
        this.f881e.b();
    }

    /* access modifiers changed from: protected */
    public e generateDefaultLayoutParams() {
        return new e(-1, -1);
    }

    public e generateLayoutParams(AttributeSet attributeSet) {
        return new e(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new e(layoutParams);
    }

    public void setIcon(Drawable drawable) {
        j();
        this.f881e.setIcon(drawable);
    }

    private boolean a(View view, Rect rect, boolean z2, boolean z3, boolean z4, boolean z5) {
        boolean z6;
        int i2;
        int i3;
        int i4;
        int i5;
        e eVar = (e) view.getLayoutParams();
        if (!z2 || eVar.leftMargin == (i5 = rect.left)) {
            z6 = false;
        } else {
            eVar.leftMargin = i5;
            z6 = true;
        }
        if (z3 && eVar.topMargin != (i4 = rect.top)) {
            eVar.topMargin = i4;
            z6 = true;
        }
        if (z5 && eVar.rightMargin != (i3 = rect.right)) {
            eVar.rightMargin = i3;
            z6 = true;
        }
        if (!z4 || eVar.bottomMargin == (i2 = rect.bottom)) {
            return z6;
        }
        eVar.bottomMargin = i2;
        return true;
    }

    public void a(View view, int i2, int i3, int i4, int i5, int i6, int[] iArr) {
        a(view, i2, i3, i4, i5, i6);
    }

    public boolean a(View view, View view2, int i2, int i3) {
        return i3 == 0 && onStartNestedScroll(view, view2, i2);
    }

    public void a(View view, int i2) {
        if (i2 == 0) {
            onStopNestedScroll(view);
        }
    }

    public void a(View view, int i2, int i3, int i4, int i5, int i6) {
        if (i6 == 0) {
            onNestedScroll(view, i2, i3, i4, i5);
        }
    }

    public void a(View view, int i2, int i3, int[] iArr, int i4) {
        if (i4 == 0) {
            onNestedPreScroll(view, i2, i3, iArr);
        }
    }

    private c0 a(View view) {
        if (view instanceof c0) {
            return (c0) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        throw new IllegalStateException("Can't make a decor toolbar out of " + view.getClass().getSimpleName());
    }

    private boolean a(float f2, float f3) {
        this.v.fling(0, 0, 0, (int) f3, 0, 0, Integer.MIN_VALUE, Api.BaseClientBuilder.API_PRIORITY_OTHER);
        return this.v.getFinalY() > this.f880d.getHeight();
    }

    public void a(int i2) {
        j();
        if (i2 == 2) {
            this.f881e.l();
        } else if (i2 == 5) {
            this.f881e.m();
        } else if (i2 == 109) {
            setOverlayMode(true);
        }
    }

    public boolean a() {
        j();
        return this.f881e.a();
    }

    public void a(Menu menu, n.a aVar) {
        j();
        this.f881e.a(menu, aVar);
    }
}
