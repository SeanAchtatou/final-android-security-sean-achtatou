package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import androidx.appcompat.view.menu.q;
import androidx.core.widget.h;
import b.a.j;
import b.e.m.u;
import com.google.android.gms.common.api.Api;
import java.lang.reflect.Method;

/* compiled from: ListPopupWindow */
public class i0 implements q {
    private static Method F;
    private static Method G;
    private static Method H;
    final Handler A;
    private final Rect B;
    private Rect C;
    private boolean D;
    PopupWindow E;

    /* renamed from: a  reason: collision with root package name */
    private Context f1071a;

    /* renamed from: b  reason: collision with root package name */
    private ListAdapter f1072b;

    /* renamed from: c  reason: collision with root package name */
    e0 f1073c;

    /* renamed from: d  reason: collision with root package name */
    private int f1074d;

    /* renamed from: e  reason: collision with root package name */
    private int f1075e;

    /* renamed from: f  reason: collision with root package name */
    private int f1076f;

    /* renamed from: g  reason: collision with root package name */
    private int f1077g;

    /* renamed from: h  reason: collision with root package name */
    private int f1078h;

    /* renamed from: i  reason: collision with root package name */
    private boolean f1079i;

    /* renamed from: j  reason: collision with root package name */
    private boolean f1080j;

    /* renamed from: k  reason: collision with root package name */
    private boolean f1081k;
    private int l;
    private boolean m;
    private boolean n;
    int o;
    private View p;
    private int q;
    private DataSetObserver r;
    private View s;
    private Drawable t;
    private AdapterView.OnItemClickListener u;
    private AdapterView.OnItemSelectedListener v;
    final g w;
    private final f x;
    private final e y;
    private final c z;

    /* compiled from: ListPopupWindow */
    class a implements Runnable {
        a() {
        }

        public void run() {
            View g2 = i0.this.g();
            if (g2 != null && g2.getWindowToken() != null) {
                i0.this.show();
            }
        }
    }

    /* compiled from: ListPopupWindow */
    class b implements AdapterView.OnItemSelectedListener {
        b() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int i2, long j2) {
            e0 e0Var;
            if (i2 != -1 && (e0Var = i0.this.f1073c) != null) {
                e0Var.setListSelectionHidden(false);
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* compiled from: ListPopupWindow */
    private class c implements Runnable {
        c() {
        }

        public void run() {
            i0.this.f();
        }
    }

    /* compiled from: ListPopupWindow */
    private class d extends DataSetObserver {
        d() {
        }

        public void onChanged() {
            if (i0.this.isShowing()) {
                i0.this.show();
            }
        }

        public void onInvalidated() {
            i0.this.dismiss();
        }
    }

    /* compiled from: ListPopupWindow */
    private class e implements AbsListView.OnScrollListener {
        e() {
        }

        public void onScroll(AbsListView absListView, int i2, int i3, int i4) {
        }

        public void onScrollStateChanged(AbsListView absListView, int i2) {
            if (i2 == 1 && !i0.this.i() && i0.this.E.getContentView() != null) {
                i0 i0Var = i0.this;
                i0Var.A.removeCallbacks(i0Var.w);
                i0.this.w.run();
            }
        }
    }

    /* compiled from: ListPopupWindow */
    private class f implements View.OnTouchListener {
        f() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            PopupWindow popupWindow;
            int action = motionEvent.getAction();
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            if (action == 0 && (popupWindow = i0.this.E) != null && popupWindow.isShowing() && x >= 0 && x < i0.this.E.getWidth() && y >= 0 && y < i0.this.E.getHeight()) {
                i0 i0Var = i0.this;
                i0Var.A.postDelayed(i0Var.w, 250);
                return false;
            } else if (action != 1) {
                return false;
            } else {
                i0 i0Var2 = i0.this;
                i0Var2.A.removeCallbacks(i0Var2.w);
                return false;
            }
        }
    }

    /* compiled from: ListPopupWindow */
    private class g implements Runnable {
        g() {
        }

        public void run() {
            e0 e0Var = i0.this.f1073c;
            if (e0Var != null && u.s(e0Var) && i0.this.f1073c.getCount() > i0.this.f1073c.getChildCount()) {
                int childCount = i0.this.f1073c.getChildCount();
                i0 i0Var = i0.this;
                if (childCount <= i0Var.o) {
                    i0Var.E.setInputMethodMode(2);
                    i0.this.show();
                }
            }
        }
    }

    static {
        if (Build.VERSION.SDK_INT <= 28) {
            Class<PopupWindow> cls = PopupWindow.class;
            try {
                F = cls.getDeclaredMethod("setClipToScreenEnabled", Boolean.TYPE);
            } catch (NoSuchMethodException unused) {
                Log.i("ListPopupWindow", "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well.");
            }
            try {
                H = PopupWindow.class.getDeclaredMethod("setEpicenterBounds", Rect.class);
            } catch (NoSuchMethodException unused2) {
                Log.i("ListPopupWindow", "Could not find method setEpicenterBounds(Rect) on PopupWindow. Oh well.");
            }
        }
        if (Build.VERSION.SDK_INT <= 23) {
            try {
                G = PopupWindow.class.getDeclaredMethod("getMaxAvailableHeight", View.class, Integer.TYPE, Boolean.TYPE);
            } catch (NoSuchMethodException unused3) {
                Log.i("ListPopupWindow", "Could not find method getMaxAvailableHeight(View, int, boolean) on PopupWindow. Oh well.");
            }
        }
    }

    public i0(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v24, resolved type: androidx.appcompat.widget.e0} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v25, resolved type: androidx.appcompat.widget.e0} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: android.widget.LinearLayout} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v30, resolved type: androidx.appcompat.widget.e0} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int k() {
        /*
            r12 = this;
            androidx.appcompat.widget.e0 r0 = r12.f1073c
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = -1
            r3 = 1
            r4 = 0
            if (r0 != 0) goto L_0x00bf
            android.content.Context r0 = r12.f1071a
            androidx.appcompat.widget.i0$a r5 = new androidx.appcompat.widget.i0$a
            r5.<init>()
            boolean r5 = r12.D
            r5 = r5 ^ r3
            androidx.appcompat.widget.e0 r5 = r12.a(r0, r5)
            r12.f1073c = r5
            android.graphics.drawable.Drawable r5 = r12.t
            if (r5 == 0) goto L_0x0022
            androidx.appcompat.widget.e0 r6 = r12.f1073c
            r6.setSelector(r5)
        L_0x0022:
            androidx.appcompat.widget.e0 r5 = r12.f1073c
            android.widget.ListAdapter r6 = r12.f1072b
            r5.setAdapter(r6)
            androidx.appcompat.widget.e0 r5 = r12.f1073c
            android.widget.AdapterView$OnItemClickListener r6 = r12.u
            r5.setOnItemClickListener(r6)
            androidx.appcompat.widget.e0 r5 = r12.f1073c
            r5.setFocusable(r3)
            androidx.appcompat.widget.e0 r5 = r12.f1073c
            r5.setFocusableInTouchMode(r3)
            androidx.appcompat.widget.e0 r5 = r12.f1073c
            androidx.appcompat.widget.i0$b r6 = new androidx.appcompat.widget.i0$b
            r6.<init>()
            r5.setOnItemSelectedListener(r6)
            androidx.appcompat.widget.e0 r5 = r12.f1073c
            androidx.appcompat.widget.i0$e r6 = r12.y
            r5.setOnScrollListener(r6)
            android.widget.AdapterView$OnItemSelectedListener r5 = r12.v
            if (r5 == 0) goto L_0x0054
            androidx.appcompat.widget.e0 r6 = r12.f1073c
            r6.setOnItemSelectedListener(r5)
        L_0x0054:
            androidx.appcompat.widget.e0 r5 = r12.f1073c
            android.view.View r6 = r12.p
            if (r6 == 0) goto L_0x00b8
            android.widget.LinearLayout r7 = new android.widget.LinearLayout
            r7.<init>(r0)
            r7.setOrientation(r3)
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r8 = 1065353216(0x3f800000, float:1.0)
            r0.<init>(r2, r4, r8)
            int r8 = r12.q
            if (r8 == 0) goto L_0x008f
            if (r8 == r3) goto L_0x0088
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r5 = "Invalid hint position "
            r0.append(r5)
            int r5 = r12.q
            r0.append(r5)
            java.lang.String r0 = r0.toString()
            java.lang.String r5 = "ListPopupWindow"
            android.util.Log.e(r5, r0)
            goto L_0x0095
        L_0x0088:
            r7.addView(r5, r0)
            r7.addView(r6)
            goto L_0x0095
        L_0x008f:
            r7.addView(r6)
            r7.addView(r5, r0)
        L_0x0095:
            int r0 = r12.f1075e
            if (r0 < 0) goto L_0x009c
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            goto L_0x009e
        L_0x009c:
            r0 = 0
            r5 = 0
        L_0x009e:
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r5)
            r6.measure(r0, r4)
            android.view.ViewGroup$LayoutParams r0 = r6.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r0 = (android.widget.LinearLayout.LayoutParams) r0
            int r5 = r6.getMeasuredHeight()
            int r6 = r0.topMargin
            int r5 = r5 + r6
            int r0 = r0.bottomMargin
            int r5 = r5 + r0
            r0 = r5
            r5 = r7
            goto L_0x00b9
        L_0x00b8:
            r0 = 0
        L_0x00b9:
            android.widget.PopupWindow r6 = r12.E
            r6.setContentView(r5)
            goto L_0x00dd
        L_0x00bf:
            android.widget.PopupWindow r0 = r12.E
            android.view.View r0 = r0.getContentView()
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            android.view.View r0 = r12.p
            if (r0 == 0) goto L_0x00dc
            android.view.ViewGroup$LayoutParams r5 = r0.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r5 = (android.widget.LinearLayout.LayoutParams) r5
            int r0 = r0.getMeasuredHeight()
            int r6 = r5.topMargin
            int r0 = r0 + r6
            int r5 = r5.bottomMargin
            int r0 = r0 + r5
            goto L_0x00dd
        L_0x00dc:
            r0 = 0
        L_0x00dd:
            android.widget.PopupWindow r5 = r12.E
            android.graphics.drawable.Drawable r5 = r5.getBackground()
            if (r5 == 0) goto L_0x00f9
            android.graphics.Rect r6 = r12.B
            r5.getPadding(r6)
            android.graphics.Rect r5 = r12.B
            int r6 = r5.top
            int r5 = r5.bottom
            int r5 = r5 + r6
            boolean r7 = r12.f1079i
            if (r7 != 0) goto L_0x00ff
            int r6 = -r6
            r12.f1077g = r6
            goto L_0x00ff
        L_0x00f9:
            android.graphics.Rect r5 = r12.B
            r5.setEmpty()
            r5 = 0
        L_0x00ff:
            android.widget.PopupWindow r6 = r12.E
            int r6 = r6.getInputMethodMode()
            r7 = 2
            if (r6 != r7) goto L_0x0109
            goto L_0x010a
        L_0x0109:
            r3 = 0
        L_0x010a:
            android.view.View r4 = r12.g()
            int r6 = r12.f1077g
            int r3 = r12.a(r4, r6, r3)
            boolean r4 = r12.m
            if (r4 != 0) goto L_0x017c
            int r4 = r12.f1074d
            if (r4 != r2) goto L_0x011d
            goto L_0x017c
        L_0x011d:
            int r4 = r12.f1075e
            r6 = -2
            if (r4 == r6) goto L_0x0145
            r1 = 1073741824(0x40000000, float:2.0)
            if (r4 == r2) goto L_0x012c
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r4, r1)
        L_0x012a:
            r7 = r1
            goto L_0x015e
        L_0x012c:
            android.content.Context r2 = r12.f1071a
            android.content.res.Resources r2 = r2.getResources()
            android.util.DisplayMetrics r2 = r2.getDisplayMetrics()
            int r2 = r2.widthPixels
            android.graphics.Rect r4 = r12.B
            int r6 = r4.left
            int r4 = r4.right
            int r6 = r6 + r4
            int r2 = r2 - r6
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r1)
            goto L_0x012a
        L_0x0145:
            android.content.Context r2 = r12.f1071a
            android.content.res.Resources r2 = r2.getResources()
            android.util.DisplayMetrics r2 = r2.getDisplayMetrics()
            int r2 = r2.widthPixels
            android.graphics.Rect r4 = r12.B
            int r6 = r4.left
            int r4 = r4.right
            int r6 = r6 + r4
            int r2 = r2 - r6
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r1)
            goto L_0x012a
        L_0x015e:
            androidx.appcompat.widget.e0 r6 = r12.f1073c
            r8 = 0
            r9 = -1
            int r10 = r3 - r0
            r11 = -1
            int r1 = r6.a(r7, r8, r9, r10, r11)
            if (r1 <= 0) goto L_0x017a
            androidx.appcompat.widget.e0 r2 = r12.f1073c
            int r2 = r2.getPaddingTop()
            androidx.appcompat.widget.e0 r3 = r12.f1073c
            int r3 = r3.getPaddingBottom()
            int r2 = r2 + r3
            int r5 = r5 + r2
            int r0 = r0 + r5
        L_0x017a:
            int r1 = r1 + r0
            return r1
        L_0x017c:
            int r3 = r3 + r5
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.i0.k():int");
    }

    private void l() {
        View view = this.p;
        if (view != null) {
            ViewParent parent = view.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.p);
            }
        }
    }

    public void a(ListAdapter listAdapter) {
        DataSetObserver dataSetObserver = this.r;
        if (dataSetObserver == null) {
            this.r = new d();
        } else {
            ListAdapter listAdapter2 = this.f1072b;
            if (listAdapter2 != null) {
                listAdapter2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.f1072b = listAdapter;
        if (listAdapter != null) {
            listAdapter.registerDataSetObserver(this.r);
        }
        e0 e0Var = this.f1073c;
        if (e0Var != null) {
            e0Var.setAdapter(this.f1072b);
        }
    }

    public void b(int i2) {
        this.f1077g = i2;
        this.f1079i = true;
    }

    public Drawable c() {
        return this.E.getBackground();
    }

    public void d(int i2) {
        this.E.setAnimationStyle(i2);
    }

    public void dismiss() {
        this.E.dismiss();
        l();
        this.E.setContentView(null);
        this.f1073c = null;
        this.A.removeCallbacks(this.w);
    }

    public int e() {
        if (!this.f1079i) {
            return 0;
        }
        return this.f1077g;
    }

    public void f(int i2) {
        this.l = i2;
    }

    public View g() {
        return this.s;
    }

    public void h(int i2) {
        this.q = i2;
    }

    public void i(int i2) {
        e0 e0Var = this.f1073c;
        if (isShowing() && e0Var != null) {
            e0Var.setListSelectionHidden(false);
            e0Var.setSelection(i2);
            if (e0Var.getChoiceMode() != 0) {
                e0Var.setItemChecked(i2, true);
            }
        }
    }

    public boolean isShowing() {
        return this.E.isShowing();
    }

    public boolean j() {
        return this.D;
    }

    public void show() {
        int k2 = k();
        boolean i2 = i();
        h.a(this.E, this.f1078h);
        boolean z2 = true;
        if (!this.E.isShowing()) {
            int i3 = this.f1075e;
            if (i3 == -1) {
                i3 = -1;
            } else if (i3 == -2) {
                i3 = g().getWidth();
            }
            int i4 = this.f1074d;
            if (i4 == -1) {
                k2 = -1;
            } else if (i4 != -2) {
                k2 = i4;
            }
            this.E.setWidth(i3);
            this.E.setHeight(k2);
            c(true);
            this.E.setOutsideTouchable(!this.n && !this.m);
            this.E.setTouchInterceptor(this.x);
            if (this.f1081k) {
                h.a(this.E, this.f1080j);
            }
            if (Build.VERSION.SDK_INT <= 28) {
                Method method = H;
                if (method != null) {
                    try {
                        method.invoke(this.E, this.C);
                    } catch (Exception e2) {
                        Log.e("ListPopupWindow", "Could not invoke setEpicenterBounds on PopupWindow", e2);
                    }
                }
            } else {
                this.E.setEpicenterBounds(this.C);
            }
            h.a(this.E, g(), this.f1076f, this.f1077g, this.l);
            this.f1073c.setSelection(-1);
            if (!this.D || this.f1073c.isInTouchMode()) {
                f();
            }
            if (!this.D) {
                this.A.post(this.z);
            }
        } else if (u.s(g())) {
            int i5 = this.f1075e;
            if (i5 == -1) {
                i5 = -1;
            } else if (i5 == -2) {
                i5 = g().getWidth();
            }
            int i6 = this.f1074d;
            if (i6 == -1) {
                if (!i2) {
                    k2 = -1;
                }
                if (i2) {
                    this.E.setWidth(this.f1075e == -1 ? -1 : 0);
                    this.E.setHeight(0);
                } else {
                    this.E.setWidth(this.f1075e == -1 ? -1 : 0);
                    this.E.setHeight(-1);
                }
            } else if (i6 != -2) {
                k2 = i6;
            }
            PopupWindow popupWindow = this.E;
            if (this.n || this.m) {
                z2 = false;
            }
            popupWindow.setOutsideTouchable(z2);
            this.E.update(g(), this.f1076f, this.f1077g, i5 < 0 ? -1 : i5, k2 < 0 ? -1 : k2);
        }
    }

    public i0(Context context, AttributeSet attributeSet, int i2, int i3) {
        this.f1074d = -2;
        this.f1075e = -2;
        this.f1078h = 1002;
        this.l = 0;
        this.m = false;
        this.n = false;
        this.o = Api.BaseClientBuilder.API_PRIORITY_OTHER;
        this.q = 0;
        this.w = new g();
        this.x = new f();
        this.y = new e();
        this.z = new c();
        this.B = new Rect();
        this.f1071a = context;
        this.A = new Handler(context.getMainLooper());
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.ListPopupWindow, i2, i3);
        this.f1076f = obtainStyledAttributes.getDimensionPixelOffset(j.ListPopupWindow_android_dropDownHorizontalOffset, 0);
        this.f1077g = obtainStyledAttributes.getDimensionPixelOffset(j.ListPopupWindow_android_dropDownVerticalOffset, 0);
        if (this.f1077g != 0) {
            this.f1079i = true;
        }
        obtainStyledAttributes.recycle();
        this.E = new q(context, attributeSet, i2, i3);
        this.E.setInputMethodMode(1);
    }

    private void c(boolean z2) {
        if (Build.VERSION.SDK_INT <= 28) {
            Method method = F;
            if (method != null) {
                try {
                    method.invoke(this.E, Boolean.valueOf(z2));
                } catch (Exception unused) {
                    Log.i("ListPopupWindow", "Could not call setClipToScreenEnabled() on PopupWindow. Oh well.");
                }
            }
        } else {
            this.E.setIsClippedToScreen(z2);
        }
    }

    public ListView d() {
        return this.f1073c;
    }

    public void f() {
        e0 e0Var = this.f1073c;
        if (e0Var != null) {
            e0Var.setListSelectionHidden(true);
            e0Var.requestLayout();
        }
    }

    public void g(int i2) {
        this.E.setInputMethodMode(i2);
    }

    public int h() {
        return this.f1075e;
    }

    public void j(int i2) {
        this.f1075e = i2;
    }

    public void b(boolean z2) {
        this.f1081k = true;
        this.f1080j = z2;
    }

    public void e(int i2) {
        Drawable background = this.E.getBackground();
        if (background != null) {
            background.getPadding(this.B);
            Rect rect = this.B;
            this.f1075e = rect.left + rect.right + i2;
            return;
        }
        j(i2);
    }

    public boolean i() {
        return this.E.getInputMethodMode() == 2;
    }

    public void a(boolean z2) {
        this.D = z2;
        this.E.setFocusable(z2);
    }

    public void a(Drawable drawable) {
        this.E.setBackgroundDrawable(drawable);
    }

    public void a(View view) {
        this.s = view;
    }

    public int a() {
        return this.f1076f;
    }

    public void a(int i2) {
        this.f1076f = i2;
    }

    public void a(Rect rect) {
        this.C = rect != null ? new Rect(rect) : null;
    }

    public void a(AdapterView.OnItemClickListener onItemClickListener) {
        this.u = onItemClickListener;
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.E.setOnDismissListener(onDismissListener);
    }

    /* access modifiers changed from: package-private */
    public e0 a(Context context, boolean z2) {
        return new e0(context, z2);
    }

    private int a(View view, int i2, boolean z2) {
        if (Build.VERSION.SDK_INT > 23) {
            return this.E.getMaxAvailableHeight(view, i2, z2);
        }
        Method method = G;
        if (method != null) {
            try {
                return ((Integer) method.invoke(this.E, view, Integer.valueOf(i2), Boolean.valueOf(z2))).intValue();
            } catch (Exception unused) {
                Log.i("ListPopupWindow", "Could not call getMaxAvailableHeightMethod(View, int, boolean) on PopupWindow. Using the public version.");
            }
        }
        return this.E.getMaxAvailableHeight(view, i2);
    }
}
