package androidx.appcompat.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RatingBar;
import b.a.a;

/* compiled from: AppCompatRatingBar */
public class t extends RatingBar {

    /* renamed from: a  reason: collision with root package name */
    private final r f1174a;

    public t(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.ratingBarStyle);
    }

    /* access modifiers changed from: protected */
    public synchronized void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        Bitmap a2 = this.f1174a.a();
        if (a2 != null) {
            setMeasuredDimension(View.resolveSizeAndState(a2.getWidth() * getNumStars(), i2, 0), getMeasuredHeight());
        }
    }

    public t(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f1174a = new r(this);
        this.f1174a.a(attributeSet, i2);
    }
}
