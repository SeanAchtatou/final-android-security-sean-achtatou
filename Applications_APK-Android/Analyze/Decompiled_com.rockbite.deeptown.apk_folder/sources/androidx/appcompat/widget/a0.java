package androidx.appcompat.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ToggleButton;

/* compiled from: AppCompatToggleButton */
public class a0 extends ToggleButton {

    /* renamed from: a  reason: collision with root package name */
    private final x f996a;

    public a0(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842827);
    }

    public a0(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f996a = new x(this);
        this.f996a.a(attributeSet, i2);
    }
}
