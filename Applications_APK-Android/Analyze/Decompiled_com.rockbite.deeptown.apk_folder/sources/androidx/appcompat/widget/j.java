package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import androidx.appcompat.widget.l0;
import b.a.c;
import b.a.e;

/* compiled from: AppCompatDrawableManager */
public final class j {
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final PorterDuff.Mode f1089b = PorterDuff.Mode.SRC_IN;

    /* renamed from: c  reason: collision with root package name */
    private static j f1090c;

    /* renamed from: a  reason: collision with root package name */
    private l0 f1091a;

    /* compiled from: AppCompatDrawableManager */
    static class a implements l0.e {

        /* renamed from: a  reason: collision with root package name */
        private final int[] f1092a = {e.abc_textfield_search_default_mtrl_alpha, e.abc_textfield_default_mtrl_alpha, e.abc_ab_share_pack_mtrl_alpha};

        /* renamed from: b  reason: collision with root package name */
        private final int[] f1093b = {e.abc_ic_commit_search_api_mtrl_alpha, e.abc_seekbar_tick_mark_material, e.abc_ic_menu_share_mtrl_alpha, e.abc_ic_menu_copy_mtrl_am_alpha, e.abc_ic_menu_cut_mtrl_alpha, e.abc_ic_menu_selectall_mtrl_alpha, e.abc_ic_menu_paste_mtrl_am_alpha};

        /* renamed from: c  reason: collision with root package name */
        private final int[] f1094c = {e.abc_textfield_activated_mtrl_alpha, e.abc_textfield_search_activated_mtrl_alpha, e.abc_cab_background_top_mtrl_alpha, e.abc_text_cursor_material, e.abc_text_select_handle_left_mtrl_dark, e.abc_text_select_handle_middle_mtrl_dark, e.abc_text_select_handle_right_mtrl_dark, e.abc_text_select_handle_left_mtrl_light, e.abc_text_select_handle_middle_mtrl_light, e.abc_text_select_handle_right_mtrl_light};

        /* renamed from: d  reason: collision with root package name */
        private final int[] f1095d = {e.abc_popup_background_mtrl_mult, e.abc_cab_background_internal_bg, e.abc_menu_hardkey_panel_mtrl_mult};

        /* renamed from: e  reason: collision with root package name */
        private final int[] f1096e = {e.abc_tab_indicator_material, e.abc_textfield_search_material};

        /* renamed from: f  reason: collision with root package name */
        private final int[] f1097f = {e.abc_btn_check_material, e.abc_btn_radio_material, e.abc_btn_check_material_anim, e.abc_btn_radio_material_anim};

        a() {
        }

        private ColorStateList a(Context context) {
            return b(context, 0);
        }

        private ColorStateList b(Context context) {
            return b(context, q0.b(context, b.a.a.colorAccent));
        }

        private ColorStateList c(Context context) {
            return b(context, q0.b(context, b.a.a.colorButtonNormal));
        }

        private ColorStateList d(Context context) {
            int[][] iArr = new int[3][];
            int[] iArr2 = new int[3];
            ColorStateList c2 = q0.c(context, b.a.a.colorSwitchThumbNormal);
            if (c2 == null || !c2.isStateful()) {
                iArr[0] = q0.f1158b;
                iArr2[0] = q0.a(context, b.a.a.colorSwitchThumbNormal);
                iArr[1] = q0.f1161e;
                iArr2[1] = q0.b(context, b.a.a.colorControlActivated);
                iArr[2] = q0.f1162f;
                iArr2[2] = q0.b(context, b.a.a.colorSwitchThumbNormal);
            } else {
                iArr[0] = q0.f1158b;
                iArr2[0] = c2.getColorForState(iArr[0], 0);
                iArr[1] = q0.f1161e;
                iArr2[1] = q0.b(context, b.a.a.colorControlActivated);
                iArr[2] = q0.f1162f;
                iArr2[2] = c2.getDefaultColor();
            }
            return new ColorStateList(iArr, iArr2);
        }

        public Drawable a(l0 l0Var, Context context, int i2) {
            if (i2 != e.abc_cab_background_top_material) {
                return null;
            }
            return new LayerDrawable(new Drawable[]{l0Var.a(context, e.abc_cab_background_internal_bg), l0Var.a(context, e.abc_cab_background_top_mtrl_alpha)});
        }

        private ColorStateList b(Context context, int i2) {
            int b2 = q0.b(context, b.a.a.colorControlHighlight);
            int a2 = q0.a(context, b.a.a.colorButtonNormal);
            return new ColorStateList(new int[][]{q0.f1158b, q0.f1160d, q0.f1159c, q0.f1162f}, new int[]{a2, b.e.f.a.b(b2, i2), b.e.f.a.b(b2, i2), i2});
        }

        private void a(Drawable drawable, int i2, PorterDuff.Mode mode) {
            if (d0.a(drawable)) {
                drawable = drawable.mutate();
            }
            if (mode == null) {
                mode = j.f1089b;
            }
            drawable.setColorFilter(j.a(i2, mode));
        }

        private boolean a(int[] iArr, int i2) {
            for (int i3 : iArr) {
                if (i3 == i2) {
                    return true;
                }
            }
            return false;
        }

        public ColorStateList a(Context context, int i2) {
            if (i2 == e.abc_edit_text_material) {
                return b.a.k.a.a.b(context, c.abc_tint_edittext);
            }
            if (i2 == e.abc_switch_track_mtrl_alpha) {
                return b.a.k.a.a.b(context, c.abc_tint_switch_track);
            }
            if (i2 == e.abc_switch_thumb_material) {
                return d(context);
            }
            if (i2 == e.abc_btn_default_mtrl_shape) {
                return c(context);
            }
            if (i2 == e.abc_btn_borderless_material) {
                return a(context);
            }
            if (i2 == e.abc_btn_colored_material) {
                return b(context);
            }
            if (i2 == e.abc_spinner_mtrl_am_alpha || i2 == e.abc_spinner_textfield_background_material) {
                return b.a.k.a.a.b(context, c.abc_tint_spinner);
            }
            if (a(this.f1093b, i2)) {
                return q0.c(context, b.a.a.colorControlNormal);
            }
            if (a(this.f1096e, i2)) {
                return b.a.k.a.a.b(context, c.abc_tint_default);
            }
            if (a(this.f1097f, i2)) {
                return b.a.k.a.a.b(context, c.abc_tint_btn_checkable);
            }
            if (i2 == e.abc_seekbar_thumb_material) {
                return b.a.k.a.a.b(context, c.abc_tint_seek_thumb);
            }
            return null;
        }

        public boolean b(Context context, int i2, Drawable drawable) {
            if (i2 == e.abc_seekbar_track_material) {
                LayerDrawable layerDrawable = (LayerDrawable) drawable;
                a(layerDrawable.findDrawableByLayerId(16908288), q0.b(context, b.a.a.colorControlNormal), j.f1089b);
                a(layerDrawable.findDrawableByLayerId(16908303), q0.b(context, b.a.a.colorControlNormal), j.f1089b);
                a(layerDrawable.findDrawableByLayerId(16908301), q0.b(context, b.a.a.colorControlActivated), j.f1089b);
                return true;
            } else if (i2 != e.abc_ratingbar_material && i2 != e.abc_ratingbar_indicator_material && i2 != e.abc_ratingbar_small_material) {
                return false;
            } else {
                LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
                a(layerDrawable2.findDrawableByLayerId(16908288), q0.a(context, b.a.a.colorControlNormal), j.f1089b);
                a(layerDrawable2.findDrawableByLayerId(16908303), q0.b(context, b.a.a.colorControlActivated), j.f1089b);
                a(layerDrawable2.findDrawableByLayerId(16908301), q0.b(context, b.a.a.colorControlActivated), j.f1089b);
                return true;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:17:0x004b  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0066 A[RETURN] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(android.content.Context r7, int r8, android.graphics.drawable.Drawable r9) {
            /*
                r6 = this;
                android.graphics.PorterDuff$Mode r0 = androidx.appcompat.widget.j.f1089b
                int[] r1 = r6.f1092a
                boolean r1 = r6.a(r1, r8)
                r2 = 16842801(0x1010031, float:2.3693695E-38)
                r3 = -1
                r4 = 0
                r5 = 1
                if (r1 == 0) goto L_0x0018
                int r2 = b.a.a.colorControlNormal
            L_0x0014:
                r1 = r0
                r8 = 1
                r0 = -1
                goto L_0x0049
            L_0x0018:
                int[] r1 = r6.f1094c
                boolean r1 = r6.a(r1, r8)
                if (r1 == 0) goto L_0x0023
                int r2 = b.a.a.colorControlActivated
                goto L_0x0014
            L_0x0023:
                int[] r1 = r6.f1095d
                boolean r1 = r6.a(r1, r8)
                if (r1 == 0) goto L_0x002e
                android.graphics.PorterDuff$Mode r0 = android.graphics.PorterDuff.Mode.MULTIPLY
                goto L_0x0014
            L_0x002e:
                int r1 = b.a.e.abc_list_divider_mtrl_alpha
                if (r8 != r1) goto L_0x0040
                r2 = 16842800(0x1010030, float:2.3693693E-38)
                r8 = 1109603123(0x42233333, float:40.8)
                int r8 = java.lang.Math.round(r8)
                r1 = r0
                r0 = r8
                r8 = 1
                goto L_0x0049
            L_0x0040:
                int r1 = b.a.e.abc_dialog_material_background
                if (r8 != r1) goto L_0x0045
                goto L_0x0014
            L_0x0045:
                r1 = r0
                r8 = 0
                r0 = -1
                r2 = 0
            L_0x0049:
                if (r8 == 0) goto L_0x0066
                boolean r8 = androidx.appcompat.widget.d0.a(r9)
                if (r8 == 0) goto L_0x0055
                android.graphics.drawable.Drawable r9 = r9.mutate()
            L_0x0055:
                int r7 = androidx.appcompat.widget.q0.b(r7, r2)
                android.graphics.PorterDuffColorFilter r7 = androidx.appcompat.widget.j.a(r7, r1)
                r9.setColorFilter(r7)
                if (r0 == r3) goto L_0x0065
                r9.setAlpha(r0)
            L_0x0065:
                return r5
            L_0x0066:
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.j.a.a(android.content.Context, int, android.graphics.drawable.Drawable):boolean");
        }

        public PorterDuff.Mode a(int i2) {
            if (i2 == e.abc_switch_thumb_material) {
                return PorterDuff.Mode.MULTIPLY;
            }
            return null;
        }
    }

    public static synchronized j b() {
        j jVar;
        synchronized (j.class) {
            if (f1090c == null) {
                c();
            }
            jVar = f1090c;
        }
        return jVar;
    }

    public static synchronized void c() {
        synchronized (j.class) {
            if (f1090c == null) {
                f1090c = new j();
                f1090c.f1091a = l0.a();
                f1090c.f1091a.a(new a());
            }
        }
    }

    public synchronized Drawable a(Context context, int i2) {
        return this.f1091a.a(context, i2);
    }

    /* access modifiers changed from: package-private */
    public synchronized Drawable a(Context context, int i2, boolean z) {
        return this.f1091a.a(context, i2, z);
    }

    public synchronized void a(Context context) {
        this.f1091a.a(context);
    }

    /* access modifiers changed from: package-private */
    public synchronized ColorStateList b(Context context, int i2) {
        return this.f1091a.b(context, i2);
    }

    static void a(Drawable drawable, t0 t0Var, int[] iArr) {
        l0.a(drawable, t0Var, iArr);
    }

    public static synchronized PorterDuffColorFilter a(int i2, PorterDuff.Mode mode) {
        PorterDuffColorFilter a2;
        synchronized (j.class) {
            a2 = l0.a(i2, mode);
        }
        return a2;
    }
}
