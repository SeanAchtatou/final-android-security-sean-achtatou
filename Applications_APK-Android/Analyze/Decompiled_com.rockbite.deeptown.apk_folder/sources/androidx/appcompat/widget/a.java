package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import b.a.j;
import b.e.m.u;
import b.e.m.y;
import b.e.m.z;
import com.esotericsoftware.spine.Animation;

/* compiled from: AbsActionBarView */
abstract class a extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    protected final C0007a f985a;

    /* renamed from: b  reason: collision with root package name */
    protected final Context f986b;

    /* renamed from: c  reason: collision with root package name */
    protected ActionMenuView f987c;

    /* renamed from: d  reason: collision with root package name */
    protected c f988d;

    /* renamed from: e  reason: collision with root package name */
    protected int f989e;

    /* renamed from: f  reason: collision with root package name */
    protected y f990f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f991g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f992h;

    a(Context context) {
        this(context, null);
    }

    protected static int a(int i2, int i3, boolean z) {
        return z ? i2 - i3 : i2 + i3;
    }

    public int getAnimatedVisibility() {
        if (this.f990f != null) {
            return this.f985a.f994b;
        }
        return getVisibility();
    }

    public int getContentHeight() {
        return this.f989e;
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, j.ActionBar, b.a.a.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(j.ActionBar_height, 0));
        obtainStyledAttributes.recycle();
        c cVar = this.f988d;
        if (cVar != null) {
            cVar.a(configuration);
        }
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.f992h = false;
        }
        if (!this.f992h) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.f992h = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.f992h = false;
        }
        return true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.f991g = false;
        }
        if (!this.f991g) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.f991g = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.f991g = false;
        }
        return true;
    }

    public abstract void setContentHeight(int i2);

    public void setVisibility(int i2) {
        if (i2 != getVisibility()) {
            y yVar = this.f990f;
            if (yVar != null) {
                yVar.a();
            }
            super.setVisibility(i2);
        }
    }

    /* renamed from: androidx.appcompat.widget.a$a  reason: collision with other inner class name */
    /* compiled from: AbsActionBarView */
    protected class C0007a implements z {

        /* renamed from: a  reason: collision with root package name */
        private boolean f993a = false;

        /* renamed from: b  reason: collision with root package name */
        int f994b;

        protected C0007a() {
        }

        public C0007a a(y yVar, int i2) {
            a.this.f990f = yVar;
            this.f994b = i2;
            return this;
        }

        public void b(View view) {
            if (!this.f993a) {
                a aVar = a.this;
                aVar.f990f = null;
                a.super.setVisibility(this.f994b);
            }
        }

        public void c(View view) {
            a.super.setVisibility(0);
            this.f993a = false;
        }

        public void a(View view) {
            this.f993a = true;
        }
    }

    a(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public y a(int i2, long j2) {
        y yVar = this.f990f;
        if (yVar != null) {
            yVar.a();
        }
        if (i2 == 0) {
            if (getVisibility() != 0) {
                setAlpha(Animation.CurveTimeline.LINEAR);
            }
            y a2 = u.a(this);
            a2.a(1.0f);
            a2.a(j2);
            C0007a aVar = this.f985a;
            aVar.a(a2, i2);
            a2.a(aVar);
            return a2;
        }
        y a3 = u.a(this);
        a3.a((float) Animation.CurveTimeline.LINEAR);
        a3.a(j2);
        C0007a aVar2 = this.f985a;
        aVar2.a(a3, i2);
        a3.a(aVar2);
        return a3;
    }

    a(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        int i3;
        this.f985a = new C0007a();
        TypedValue typedValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(b.a.a.actionBarPopupTheme, typedValue, true) || (i3 = typedValue.resourceId) == 0) {
            this.f986b = context;
        } else {
            this.f986b = new ContextThemeWrapper(context, i3);
        }
    }

    /* access modifiers changed from: protected */
    public int a(View view, int i2, int i3, int i4) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i2, Integer.MIN_VALUE), i3);
        return Math.max(0, (i2 - view.getMeasuredWidth()) - i4);
    }

    /* access modifiers changed from: protected */
    public int a(View view, int i2, int i3, int i4, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i5 = i3 + ((i4 - measuredHeight) / 2);
        if (z) {
            view.layout(i2 - measuredWidth, i5, i2, measuredHeight + i5);
        } else {
            view.layout(i2, i5, i2 + measuredWidth, measuredHeight + i5);
        }
        return z ? -measuredWidth : measuredWidth;
    }
}
