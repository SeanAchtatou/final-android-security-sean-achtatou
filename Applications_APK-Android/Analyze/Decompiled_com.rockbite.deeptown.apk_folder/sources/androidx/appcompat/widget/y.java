package androidx.appcompat.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.textclassifier.TextClassifier;
import android.widget.TextView;
import androidx.core.widget.b;
import androidx.core.widget.i;
import androidx.core.widget.k;
import b.e.f.c;
import b.e.k.a;
import b.e.m.t;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/* compiled from: AppCompatTextView */
public class y extends TextView implements t, k, b {

    /* renamed from: a  reason: collision with root package name */
    private final e f1224a;

    /* renamed from: b  reason: collision with root package name */
    private final x f1225b;

    /* renamed from: c  reason: collision with root package name */
    private final w f1226c;

    /* renamed from: d  reason: collision with root package name */
    private Future<a> f1227d;

    public y(Context context) {
        this(context, null);
    }

    private void d() {
        Future<a> future = this.f1227d;
        if (future != null) {
            try {
                this.f1227d = null;
                i.a(this, future.get());
            } catch (InterruptedException | ExecutionException unused) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        e eVar = this.f1224a;
        if (eVar != null) {
            eVar.a();
        }
        x xVar = this.f1225b;
        if (xVar != null) {
            xVar.a();
        }
    }

    public int getAutoSizeMaxTextSize() {
        if (b.X) {
            return super.getAutoSizeMaxTextSize();
        }
        x xVar = this.f1225b;
        if (xVar != null) {
            return xVar.c();
        }
        return -1;
    }

    public int getAutoSizeMinTextSize() {
        if (b.X) {
            return super.getAutoSizeMinTextSize();
        }
        x xVar = this.f1225b;
        if (xVar != null) {
            return xVar.d();
        }
        return -1;
    }

    public int getAutoSizeStepGranularity() {
        if (b.X) {
            return super.getAutoSizeStepGranularity();
        }
        x xVar = this.f1225b;
        if (xVar != null) {
            return xVar.e();
        }
        return -1;
    }

    public int[] getAutoSizeTextAvailableSizes() {
        if (b.X) {
            return super.getAutoSizeTextAvailableSizes();
        }
        x xVar = this.f1225b;
        return xVar != null ? xVar.f() : new int[0];
    }

    @SuppressLint({"WrongConstant"})
    public int getAutoSizeTextType() {
        if (!b.X) {
            x xVar = this.f1225b;
            if (xVar != null) {
                return xVar.g();
            }
            return 0;
        } else if (super.getAutoSizeTextType() == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    public int getFirstBaselineToTopHeight() {
        return i.a(this);
    }

    public int getLastBaselineToBottomHeight() {
        return i.b(this);
    }

    public ColorStateList getSupportBackgroundTintList() {
        e eVar = this.f1224a;
        if (eVar != null) {
            return eVar.b();
        }
        return null;
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        e eVar = this.f1224a;
        if (eVar != null) {
            return eVar.c();
        }
        return null;
    }

    public ColorStateList getSupportCompoundDrawablesTintList() {
        return this.f1225b.h();
    }

    public PorterDuff.Mode getSupportCompoundDrawablesTintMode() {
        return this.f1225b.i();
    }

    public CharSequence getText() {
        d();
        return super.getText();
    }

    public TextClassifier getTextClassifier() {
        w wVar;
        if (Build.VERSION.SDK_INT >= 28 || (wVar = this.f1226c) == null) {
            return super.getTextClassifier();
        }
        return wVar.a();
    }

    public a.C0049a getTextMetricsParamsCompat() {
        return i.d(this);
    }

    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        l.a(onCreateInputConnection, editorInfo, this);
        return onCreateInputConnection;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        x xVar = this.f1225b;
        if (xVar != null) {
            xVar.a(z, i2, i3, i4, i5);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        d();
        super.onMeasure(i2, i3);
    }

    /* access modifiers changed from: protected */
    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        super.onTextChanged(charSequence, i2, i3, i4);
        x xVar = this.f1225b;
        if (xVar != null && !b.X && xVar.j()) {
            this.f1225b.b();
        }
    }

    public void setAutoSizeTextTypeUniformWithConfiguration(int i2, int i3, int i4, int i5) throws IllegalArgumentException {
        if (b.X) {
            super.setAutoSizeTextTypeUniformWithConfiguration(i2, i3, i4, i5);
            return;
        }
        x xVar = this.f1225b;
        if (xVar != null) {
            xVar.a(i2, i3, i4, i5);
        }
    }

    public void setAutoSizeTextTypeUniformWithPresetSizes(int[] iArr, int i2) throws IllegalArgumentException {
        if (b.X) {
            super.setAutoSizeTextTypeUniformWithPresetSizes(iArr, i2);
            return;
        }
        x xVar = this.f1225b;
        if (xVar != null) {
            xVar.a(iArr, i2);
        }
    }

    public void setAutoSizeTextTypeWithDefaults(int i2) {
        if (b.X) {
            super.setAutoSizeTextTypeWithDefaults(i2);
            return;
        }
        x xVar = this.f1225b;
        if (xVar != null) {
            xVar.a(i2);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        e eVar = this.f1224a;
        if (eVar != null) {
            eVar.a(drawable);
        }
    }

    public void setBackgroundResource(int i2) {
        super.setBackgroundResource(i2);
        e eVar = this.f1224a;
        if (eVar != null) {
            eVar.a(i2);
        }
    }

    public void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        x xVar = this.f1225b;
        if (xVar != null) {
            xVar.k();
        }
    }

    public void setCompoundDrawablesRelative(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        x xVar = this.f1225b;
        if (xVar != null) {
            xVar.k();
        }
    }

    public void setCompoundDrawablesRelativeWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        super.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        x xVar = this.f1225b;
        if (xVar != null) {
            xVar.k();
        }
    }

    public void setCompoundDrawablesWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        super.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        x xVar = this.f1225b;
        if (xVar != null) {
            xVar.k();
        }
    }

    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        super.setCustomSelectionActionModeCallback(i.a(this, callback));
    }

    public void setFirstBaselineToTopHeight(int i2) {
        if (Build.VERSION.SDK_INT >= 28) {
            super.setFirstBaselineToTopHeight(i2);
        } else {
            i.a(this, i2);
        }
    }

    public void setLastBaselineToBottomHeight(int i2) {
        if (Build.VERSION.SDK_INT >= 28) {
            super.setLastBaselineToBottomHeight(i2);
        } else {
            i.b(this, i2);
        }
    }

    public void setLineHeight(int i2) {
        i.c(this, i2);
    }

    public void setPrecomputedText(a aVar) {
        i.a(this, aVar);
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        e eVar = this.f1224a;
        if (eVar != null) {
            eVar.b(colorStateList);
        }
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        e eVar = this.f1224a;
        if (eVar != null) {
            eVar.a(mode);
        }
    }

    public void setSupportCompoundDrawablesTintList(ColorStateList colorStateList) {
        this.f1225b.a(colorStateList);
        this.f1225b.a();
    }

    public void setSupportCompoundDrawablesTintMode(PorterDuff.Mode mode) {
        this.f1225b.a(mode);
        this.f1225b.a();
    }

    public void setTextAppearance(Context context, int i2) {
        super.setTextAppearance(context, i2);
        x xVar = this.f1225b;
        if (xVar != null) {
            xVar.a(context, i2);
        }
    }

    public void setTextClassifier(TextClassifier textClassifier) {
        w wVar;
        if (Build.VERSION.SDK_INT >= 28 || (wVar = this.f1226c) == null) {
            super.setTextClassifier(textClassifier);
        } else {
            wVar.a(textClassifier);
        }
    }

    public void setTextFuture(Future<a> future) {
        this.f1227d = future;
        if (future != null) {
            requestLayout();
        }
    }

    public void setTextMetricsParamsCompat(a.C0049a aVar) {
        i.a(this, aVar);
    }

    public void setTextSize(int i2, float f2) {
        if (b.X) {
            super.setTextSize(i2, f2);
            return;
        }
        x xVar = this.f1225b;
        if (xVar != null) {
            xVar.a(i2, f2);
        }
    }

    public void setTypeface(Typeface typeface, int i2) {
        Typeface a2 = (typeface == null || i2 <= 0) ? null : c.a(getContext(), typeface, i2);
        if (a2 != null) {
            typeface = a2;
        }
        super.setTypeface(typeface, i2);
    }

    public y(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842884);
    }

    public y(Context context, AttributeSet attributeSet, int i2) {
        super(s0.b(context), attributeSet, i2);
        this.f1224a = new e(this);
        this.f1224a.a(attributeSet, i2);
        this.f1225b = new x(this);
        this.f1225b.a(attributeSet, i2);
        this.f1225b.a();
        this.f1226c = new w(this);
    }

    public void setCompoundDrawablesRelativeWithIntrinsicBounds(int i2, int i3, int i4, int i5) {
        Context context = getContext();
        Drawable drawable = null;
        Drawable c2 = i2 != 0 ? b.a.k.a.a.c(context, i2) : null;
        Drawable c3 = i3 != 0 ? b.a.k.a.a.c(context, i3) : null;
        Drawable c4 = i4 != 0 ? b.a.k.a.a.c(context, i4) : null;
        if (i5 != 0) {
            drawable = b.a.k.a.a.c(context, i5);
        }
        setCompoundDrawablesRelativeWithIntrinsicBounds(c2, c3, c4, drawable);
        x xVar = this.f1225b;
        if (xVar != null) {
            xVar.k();
        }
    }

    public void setCompoundDrawablesWithIntrinsicBounds(int i2, int i3, int i4, int i5) {
        Context context = getContext();
        Drawable drawable = null;
        Drawable c2 = i2 != 0 ? b.a.k.a.a.c(context, i2) : null;
        Drawable c3 = i3 != 0 ? b.a.k.a.a.c(context, i3) : null;
        Drawable c4 = i4 != 0 ? b.a.k.a.a.c(context, i4) : null;
        if (i5 != 0) {
            drawable = b.a.k.a.a.c(context, i5);
        }
        setCompoundDrawablesWithIntrinsicBounds(c2, c3, c4, drawable);
        x xVar = this.f1225b;
        if (xVar != null) {
            xVar.k();
        }
    }
}
