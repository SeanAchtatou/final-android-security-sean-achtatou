package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.view.menu.g;
import androidx.appcompat.view.menu.j;
import androidx.appcompat.view.menu.n;
import androidx.appcompat.view.menu.o;
import androidx.appcompat.widget.h0;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.common.api.Api;

public class ActionMenuView extends h0 implements g.b, o {
    e A;
    private g p;
    private Context q;
    private int r;
    private boolean s;
    private c t;
    private n.a u;
    g.a v;
    private boolean w;
    private int x;
    private int y;
    private int z;

    public interface a {
        boolean a();

        boolean b();
    }

    private static class b implements n.a {
        b() {
        }

        public void a(g gVar, boolean z) {
        }

        public boolean a(g gVar) {
            return false;
        }
    }

    public static class c extends h0.a {
        @ViewDebug.ExportedProperty

        /* renamed from: c  reason: collision with root package name */
        public boolean f891c;
        @ViewDebug.ExportedProperty

        /* renamed from: d  reason: collision with root package name */
        public int f892d;
        @ViewDebug.ExportedProperty

        /* renamed from: e  reason: collision with root package name */
        public int f893e;
        @ViewDebug.ExportedProperty

        /* renamed from: f  reason: collision with root package name */
        public boolean f894f;
        @ViewDebug.ExportedProperty

        /* renamed from: g  reason: collision with root package name */
        public boolean f895g;

        /* renamed from: h  reason: collision with root package name */
        boolean f896h;

        public c(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public c(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public c(c cVar) {
            super(cVar);
            this.f891c = cVar.f891c;
        }

        public c(int i2, int i3) {
            super(i2, i3);
            this.f891c = false;
        }
    }

    public interface e {
        boolean onMenuItemClick(MenuItem menuItem);
    }

    public ActionMenuView(Context context) {
        this(context, null);
    }

    static int a(View view, int i2, int i3, int i4, int i5) {
        c cVar = (c) view.getLayoutParams();
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i4) - i5, View.MeasureSpec.getMode(i4));
        ActionMenuItemView actionMenuItemView = view instanceof ActionMenuItemView ? (ActionMenuItemView) view : null;
        boolean z2 = true;
        boolean z3 = actionMenuItemView != null && actionMenuItemView.d();
        int i6 = 2;
        if (i3 <= 0 || (z3 && i3 < 2)) {
            i6 = 0;
        } else {
            view.measure(View.MeasureSpec.makeMeasureSpec(i3 * i2, Integer.MIN_VALUE), makeMeasureSpec);
            int measuredWidth = view.getMeasuredWidth();
            int i7 = measuredWidth / i2;
            if (measuredWidth % i2 != 0) {
                i7++;
            }
            if (!z3 || i7 >= 2) {
                i6 = i7;
            }
        }
        if (cVar.f891c || !z3) {
            z2 = false;
        }
        cVar.f894f = z2;
        cVar.f892d = i6;
        view.measure(View.MeasureSpec.makeMeasureSpec(i2 * i6, 1073741824), makeMeasureSpec);
        return i6;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.ActionMenuView.a(android.view.View, int, int, int, int):int
     arg types: [android.view.View, int, int, int, int]
     candidates:
      androidx.appcompat.widget.h0.a(android.view.View, int, int, int, int):void
      androidx.appcompat.widget.ActionMenuView.a(android.view.View, int, int, int, int):int */
    private void c(int i2, int i3) {
        int i4;
        boolean z2;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        boolean z3;
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i2);
        int size2 = View.MeasureSpec.getSize(i3);
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        int paddingTop = getPaddingTop() + getPaddingBottom();
        int childMeasureSpec = ViewGroup.getChildMeasureSpec(i3, paddingTop, -2);
        int i10 = size - paddingLeft;
        int i11 = this.y;
        int i12 = i10 / i11;
        int i13 = i10 % i11;
        if (i12 == 0) {
            setMeasuredDimension(i10, 0);
            return;
        }
        int i14 = i11 + (i13 / i12);
        int childCount = getChildCount();
        int i15 = i12;
        int i16 = 0;
        int i17 = 0;
        boolean z4 = false;
        int i18 = 0;
        int i19 = 0;
        int i20 = 0;
        long j2 = 0;
        while (i16 < childCount) {
            View childAt = getChildAt(i16);
            int i21 = size2;
            if (childAt.getVisibility() != 8) {
                boolean z5 = childAt instanceof ActionMenuItemView;
                int i22 = i18 + 1;
                if (z5) {
                    int i23 = this.z;
                    i9 = i22;
                    z3 = false;
                    childAt.setPadding(i23, 0, i23, 0);
                } else {
                    i9 = i22;
                    z3 = false;
                }
                c cVar = (c) childAt.getLayoutParams();
                cVar.f896h = z3;
                cVar.f893e = z3 ? 1 : 0;
                cVar.f892d = z3;
                cVar.f894f = z3;
                cVar.leftMargin = z3;
                cVar.rightMargin = z3;
                cVar.f895g = z5 && ((ActionMenuItemView) childAt).d();
                int a2 = a(childAt, i14, cVar.f891c ? 1 : i15, childMeasureSpec, paddingTop);
                int max = Math.max(i19, a2);
                if (cVar.f894f) {
                    i20++;
                }
                if (cVar.f891c) {
                    z4 = true;
                }
                i15 -= a2;
                i17 = Math.max(i17, childAt.getMeasuredHeight());
                if (a2 == 1) {
                    j2 |= (long) (1 << i16);
                    i17 = i17;
                }
                i19 = max;
                i18 = i9;
            }
            i16++;
            size2 = i21;
        }
        int i24 = size2;
        boolean z6 = z4 && i18 == 2;
        boolean z7 = false;
        while (true) {
            if (i20 <= 0 || i15 <= 0) {
                i6 = mode;
                i4 = i10;
                z2 = z7;
                i5 = i17;
            } else {
                int i25 = Api.BaseClientBuilder.API_PRIORITY_OTHER;
                int i26 = 0;
                int i27 = 0;
                long j3 = 0;
                while (i26 < childCount) {
                    boolean z8 = z7;
                    c cVar2 = (c) getChildAt(i26).getLayoutParams();
                    int i28 = i17;
                    if (cVar2.f894f) {
                        int i29 = cVar2.f892d;
                        if (i29 < i25) {
                            i25 = i29;
                            j3 = 1 << i26;
                            i27 = 1;
                        } else if (i29 == i25) {
                            j3 |= 1 << i26;
                            i27++;
                        }
                    }
                    i26++;
                    i17 = i28;
                    z7 = z8;
                }
                z2 = z7;
                i5 = i17;
                j2 |= j3;
                if (i27 > i15) {
                    i6 = mode;
                    i4 = i10;
                    break;
                }
                int i30 = i25 + 1;
                int i31 = 0;
                while (i31 < childCount) {
                    View childAt2 = getChildAt(i31);
                    c cVar3 = (c) childAt2.getLayoutParams();
                    int i32 = i10;
                    int i33 = mode;
                    long j4 = (long) (1 << i31);
                    if ((j3 & j4) == 0) {
                        if (cVar3.f892d == i30) {
                            j2 |= j4;
                        }
                        i8 = i30;
                    } else {
                        if (!z6 || !cVar3.f895g || i15 != 1) {
                            i8 = i30;
                        } else {
                            int i34 = this.z;
                            i8 = i30;
                            childAt2.setPadding(i34 + i14, 0, i34, 0);
                        }
                        cVar3.f892d++;
                        cVar3.f896h = true;
                        i15--;
                    }
                    i31++;
                    mode = i33;
                    i30 = i8;
                    i10 = i32;
                }
                i17 = i5;
                z7 = true;
            }
        }
        boolean z9 = !z4 && i18 == 1;
        if (i15 <= 0 || j2 == 0 || (i15 >= i18 - 1 && !z9 && i19 <= 1)) {
            i7 = 0;
        } else {
            float bitCount = (float) Long.bitCount(j2);
            if (!z9) {
                i7 = 0;
                if ((j2 & 1) != 0 && !((c) getChildAt(0).getLayoutParams()).f895g) {
                    bitCount -= 0.5f;
                }
                int i35 = childCount - 1;
                if ((j2 & ((long) (1 << i35))) != 0 && !((c) getChildAt(i35).getLayoutParams()).f895g) {
                    bitCount -= 0.5f;
                }
            } else {
                i7 = 0;
            }
            int i36 = bitCount > Animation.CurveTimeline.LINEAR ? (int) (((float) (i15 * i14)) / bitCount) : 0;
            for (int i37 = 0; i37 < childCount; i37++) {
                if ((j2 & ((long) (1 << i37))) != 0) {
                    View childAt3 = getChildAt(i37);
                    c cVar4 = (c) childAt3.getLayoutParams();
                    if (childAt3 instanceof ActionMenuItemView) {
                        cVar4.f893e = i36;
                        cVar4.f896h = true;
                        if (i37 == 0 && !cVar4.f895g) {
                            cVar4.leftMargin = (-i36) / 2;
                        }
                    } else if (cVar4.f891c) {
                        cVar4.f893e = i36;
                        cVar4.f896h = true;
                        cVar4.rightMargin = (-i36) / 2;
                    } else {
                        if (i37 != 0) {
                            cVar4.leftMargin = i36 / 2;
                        }
                        if (i37 != childCount - 1) {
                            cVar4.rightMargin = i36 / 2;
                        }
                    }
                    z2 = true;
                }
            }
        }
        if (z2) {
            while (i7 < childCount) {
                View childAt4 = getChildAt(i7);
                c cVar5 = (c) childAt4.getLayoutParams();
                if (cVar5.f896h) {
                    childAt4.measure(View.MeasureSpec.makeMeasureSpec((cVar5.f892d * i14) + cVar5.f893e, 1073741824), childMeasureSpec);
                }
                i7++;
            }
        }
        setMeasuredDimension(i4, i6 != 1073741824 ? i5 : i24);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof c;
    }

    public c d() {
        c generateDefaultLayoutParams = generateDefaultLayoutParams();
        generateDefaultLayoutParams.f891c = true;
        return generateDefaultLayoutParams;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return false;
    }

    public boolean e() {
        c cVar = this.t;
        return cVar != null && cVar.e();
    }

    public boolean f() {
        c cVar = this.t;
        return cVar != null && cVar.g();
    }

    public boolean g() {
        c cVar = this.t;
        return cVar != null && cVar.h();
    }

    public Menu getMenu() {
        if (this.p == null) {
            Context context = getContext();
            this.p = new g(context);
            this.p.a(new d());
            this.t = new c(context);
            this.t.c(true);
            c cVar = this.t;
            n.a aVar = this.u;
            if (aVar == null) {
                aVar = new b();
            }
            cVar.a(aVar);
            this.p.a(this.t, this.q);
            this.t.a(this);
        }
        return this.p;
    }

    public Drawable getOverflowIcon() {
        getMenu();
        return this.t.d();
    }

    public int getPopupTheme() {
        return this.r;
    }

    public int getWindowAnimations() {
        return 0;
    }

    public boolean h() {
        return this.s;
    }

    public g i() {
        return this.p;
    }

    public boolean j() {
        c cVar = this.t;
        return cVar != null && cVar.i();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        c cVar = this.t;
        if (cVar != null) {
            cVar.a(false);
            if (this.t.h()) {
                this.t.e();
                this.t.i();
            }
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        c();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        if (!this.w) {
            super.onLayout(z2, i2, i3, i4, i5);
            return;
        }
        int childCount = getChildCount();
        int i10 = (i5 - i3) / 2;
        int dividerWidth = getDividerWidth();
        int i11 = i4 - i2;
        int paddingRight = (i11 - getPaddingRight()) - getPaddingLeft();
        boolean a2 = b1.a(this);
        int i12 = paddingRight;
        int i13 = 0;
        int i14 = 0;
        for (int i15 = 0; i15 < childCount; i15++) {
            View childAt = getChildAt(i15);
            if (childAt.getVisibility() != 8) {
                c cVar = (c) childAt.getLayoutParams();
                if (cVar.f891c) {
                    int measuredWidth = childAt.getMeasuredWidth();
                    if (d(i15)) {
                        measuredWidth += dividerWidth;
                    }
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (a2) {
                        i8 = getPaddingLeft() + cVar.leftMargin;
                        i9 = i8 + measuredWidth;
                    } else {
                        i9 = (getWidth() - getPaddingRight()) - cVar.rightMargin;
                        i8 = i9 - measuredWidth;
                    }
                    int i16 = i10 - (measuredHeight / 2);
                    childAt.layout(i8, i16, i9, measuredHeight + i16);
                    i12 -= measuredWidth;
                    i13 = 1;
                } else {
                    i12 -= (childAt.getMeasuredWidth() + cVar.leftMargin) + cVar.rightMargin;
                    boolean d2 = d(i15);
                    i14++;
                }
            }
        }
        if (childCount == 1 && i13 == 0) {
            View childAt2 = getChildAt(0);
            int measuredWidth2 = childAt2.getMeasuredWidth();
            int measuredHeight2 = childAt2.getMeasuredHeight();
            int i17 = (i11 / 2) - (measuredWidth2 / 2);
            int i18 = i10 - (measuredHeight2 / 2);
            childAt2.layout(i17, i18, measuredWidth2 + i17, measuredHeight2 + i18);
            return;
        }
        int i19 = i14 - (i13 ^ 1);
        if (i19 > 0) {
            i6 = i12 / i19;
            i7 = 0;
        } else {
            i7 = 0;
            i6 = 0;
        }
        int max = Math.max(i7, i6);
        if (a2) {
            int width = getWidth() - getPaddingRight();
            while (i7 < childCount) {
                View childAt3 = getChildAt(i7);
                c cVar2 = (c) childAt3.getLayoutParams();
                if (childAt3.getVisibility() != 8 && !cVar2.f891c) {
                    int i20 = width - cVar2.rightMargin;
                    int measuredWidth3 = childAt3.getMeasuredWidth();
                    int measuredHeight3 = childAt3.getMeasuredHeight();
                    int i21 = i10 - (measuredHeight3 / 2);
                    childAt3.layout(i20 - measuredWidth3, i21, i20, measuredHeight3 + i21);
                    width = i20 - ((measuredWidth3 + cVar2.leftMargin) + max);
                }
                i7++;
            }
            return;
        }
        int paddingLeft = getPaddingLeft();
        while (i7 < childCount) {
            View childAt4 = getChildAt(i7);
            c cVar3 = (c) childAt4.getLayoutParams();
            if (childAt4.getVisibility() != 8 && !cVar3.f891c) {
                int i22 = paddingLeft + cVar3.leftMargin;
                int measuredWidth4 = childAt4.getMeasuredWidth();
                int measuredHeight4 = childAt4.getMeasuredHeight();
                int i23 = i10 - (measuredHeight4 / 2);
                childAt4.layout(i22, i23, i22 + measuredWidth4, measuredHeight4 + i23);
                paddingLeft = i22 + measuredWidth4 + cVar3.rightMargin + max;
            }
            i7++;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        g gVar;
        boolean z2 = this.w;
        this.w = View.MeasureSpec.getMode(i2) == 1073741824;
        if (z2 != this.w) {
            this.x = 0;
        }
        int size = View.MeasureSpec.getSize(i2);
        if (!(!this.w || (gVar = this.p) == null || size == this.x)) {
            this.x = size;
            gVar.b(true);
        }
        int childCount = getChildCount();
        if (!this.w || childCount <= 0) {
            for (int i4 = 0; i4 < childCount; i4++) {
                c cVar = (c) getChildAt(i4).getLayoutParams();
                cVar.rightMargin = 0;
                cVar.leftMargin = 0;
            }
            super.onMeasure(i2, i3);
            return;
        }
        c(i2, i3);
    }

    public void setExpandedActionViewsExclusive(boolean z2) {
        this.t.b(z2);
    }

    public void setOnMenuItemClickListener(e eVar) {
        this.A = eVar;
    }

    public void setOverflowIcon(Drawable drawable) {
        getMenu();
        this.t.a(drawable);
    }

    public void setOverflowReserved(boolean z2) {
        this.s = z2;
    }

    public void setPopupTheme(int i2) {
        if (this.r != i2) {
            this.r = i2;
            if (i2 == 0) {
                this.q = getContext();
            } else {
                this.q = new ContextThemeWrapper(getContext(), i2);
            }
        }
    }

    public void setPresenter(c cVar) {
        this.t = cVar;
        this.t.a(this);
    }

    private class d implements g.a {
        d() {
        }

        public boolean a(g gVar, MenuItem menuItem) {
            e eVar = ActionMenuView.this.A;
            return eVar != null && eVar.onMenuItemClick(menuItem);
        }

        public void a(g gVar) {
            g.a aVar = ActionMenuView.this.v;
            if (aVar != null) {
                aVar.a(gVar);
            }
        }
    }

    public ActionMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setBaselineAligned(false);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.y = (int) (56.0f * f2);
        this.z = (int) (f2 * 4.0f);
        this.q = context;
        this.r = 0;
    }

    /* access modifiers changed from: protected */
    public boolean d(int i2) {
        boolean z2 = false;
        if (i2 == 0) {
            return false;
        }
        View childAt = getChildAt(i2 - 1);
        View childAt2 = getChildAt(i2);
        if (i2 < getChildCount() && (childAt instanceof a)) {
            z2 = false | ((a) childAt).a();
        }
        return (i2 <= 0 || !(childAt2 instanceof a)) ? z2 : z2 | ((a) childAt2).b();
    }

    /* access modifiers changed from: protected */
    public c generateDefaultLayoutParams() {
        c cVar = new c(-2, -2);
        cVar.f1064b = 16;
        return cVar;
    }

    public c generateLayoutParams(AttributeSet attributeSet) {
        return new c(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public c generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams == null) {
            return generateDefaultLayoutParams();
        }
        c cVar = layoutParams instanceof c ? new c((c) layoutParams) : new c(layoutParams);
        if (cVar.f1064b <= 0) {
            cVar.f1064b = 16;
        }
        return cVar;
    }

    public boolean a(j jVar) {
        return this.p.a(jVar, 0);
    }

    public void a(g gVar) {
        this.p = gVar;
    }

    public void a(n.a aVar, g.a aVar2) {
        this.u = aVar;
        this.v = aVar2;
    }

    public void c() {
        c cVar = this.t;
        if (cVar != null) {
            cVar.c();
        }
    }
}
