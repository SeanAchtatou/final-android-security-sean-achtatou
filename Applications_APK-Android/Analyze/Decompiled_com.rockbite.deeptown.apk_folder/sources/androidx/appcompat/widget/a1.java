package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import java.lang.ref.WeakReference;

/* compiled from: VectorEnabledTintResources */
public class a1 extends Resources {

    /* renamed from: b  reason: collision with root package name */
    private static boolean f997b = false;

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<Context> f998a;

    public a1(Context context, Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.f998a = new WeakReference<>(context);
    }

    public static boolean b() {
        return a() && Build.VERSION.SDK_INT <= 20;
    }

    /* access modifiers changed from: package-private */
    public final Drawable a(int i2) {
        return super.getDrawable(i2);
    }

    public Drawable getDrawable(int i2) throws Resources.NotFoundException {
        Context context = this.f998a.get();
        if (context != null) {
            return l0.a().a(context, this, i2);
        }
        return super.getDrawable(i2);
    }

    public static boolean a() {
        return f997b;
    }
}
