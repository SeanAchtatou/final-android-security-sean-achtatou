package androidx.appcompat.widget;

import android.view.textclassifier.TextClassificationManager;
import android.view.textclassifier.TextClassifier;
import android.widget.TextView;
import b.e.l.f;

/* compiled from: AppCompatTextClassifierHelper */
final class w {

    /* renamed from: a  reason: collision with root package name */
    private TextView f1190a;

    /* renamed from: b  reason: collision with root package name */
    private TextClassifier f1191b;

    w(TextView textView) {
        f.a(textView);
        this.f1190a = textView;
    }

    public void a(TextClassifier textClassifier) {
        this.f1191b = textClassifier;
    }

    public TextClassifier a() {
        TextClassifier textClassifier = this.f1191b;
        if (textClassifier != null) {
            return textClassifier;
        }
        TextClassificationManager textClassificationManager = (TextClassificationManager) this.f1190a.getContext().getSystemService(TextClassificationManager.class);
        if (textClassificationManager != null) {
            return textClassificationManager.getTextClassifier();
        }
        return TextClassifier.NO_OP;
    }
}
