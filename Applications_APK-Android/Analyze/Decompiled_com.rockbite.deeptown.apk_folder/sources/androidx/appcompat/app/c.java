package androidx.appcompat.app;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.appcompat.app.AlertController;

/* compiled from: AlertDialog */
public class c extends h implements DialogInterface {

    /* renamed from: c  reason: collision with root package name */
    final AlertController f625c = new AlertController(getContext(), this, getWindow());

    /* compiled from: AlertDialog */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private final AlertController.f f626a;

        /* renamed from: b  reason: collision with root package name */
        private final int f627b;

        public a(Context context) {
            this(context, c.a(context, 0));
        }

        public a a(CharSequence charSequence) {
            this.f626a.f596f = charSequence;
            return this;
        }

        public Context b() {
            return this.f626a.f591a;
        }

        public a(Context context, int i2) {
            this.f626a = new AlertController.f(new ContextThemeWrapper(context, c.a(context, i2)));
            this.f627b = i2;
        }

        public a a(View view) {
            this.f626a.f597g = view;
            return this;
        }

        public a a(Drawable drawable) {
            this.f626a.f594d = drawable;
            return this;
        }

        public a a(DialogInterface.OnKeyListener onKeyListener) {
            this.f626a.u = onKeyListener;
            return this;
        }

        public a a(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.f626a;
            fVar.w = listAdapter;
            fVar.x = onClickListener;
            return this;
        }

        public a a(ListAdapter listAdapter, int i2, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.f626a;
            fVar.w = listAdapter;
            fVar.x = onClickListener;
            fVar.I = i2;
            fVar.H = true;
            return this;
        }

        public c a() {
            c cVar = new c(this.f626a.f591a, this.f627b);
            this.f626a.a(cVar.f625c);
            cVar.setCancelable(this.f626a.r);
            if (this.f626a.r) {
                cVar.setCanceledOnTouchOutside(true);
            }
            cVar.setOnCancelListener(this.f626a.s);
            cVar.setOnDismissListener(this.f626a.t);
            DialogInterface.OnKeyListener onKeyListener = this.f626a.u;
            if (onKeyListener != null) {
                cVar.setOnKeyListener(onKeyListener);
            }
            return cVar;
        }
    }

    protected c(Context context, int i2) {
        super(context, a(context, i2));
    }

    static int a(Context context, int i2) {
        if (((i2 >>> 24) & 255) >= 1) {
            return i2;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(b.a.a.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    public ListView b() {
        return this.f625c.a();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f625c.b();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.f625c.a(i2, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (this.f625c.b(i2, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i2, keyEvent);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.f625c.b(charSequence);
    }
}
