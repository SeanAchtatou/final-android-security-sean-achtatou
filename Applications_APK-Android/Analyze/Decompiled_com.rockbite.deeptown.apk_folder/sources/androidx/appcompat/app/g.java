package androidx.appcompat.app;

import android.app.Activity;
import android.app.Dialog;
import android.app.UiModeManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.KeyboardShortcutGroup;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.view.menu.g;
import androidx.appcompat.view.menu.n;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ContentFrameLayout;
import androidx.appcompat.widget.a1;
import androidx.appcompat.widget.b0;
import androidx.appcompat.widget.b1;
import androidx.appcompat.widget.f0;
import androidx.appcompat.widget.v0;
import androidx.lifecycle.e;
import b.a.n.b;
import b.a.n.f;
import b.e.m.a0;
import b.e.m.c0;
import b.e.m.d;
import b.e.m.q;
import b.e.m.u;
import b.e.m.y;
import b.e.m.z;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.ads.AdRequest;
import com.google.protobuf.CodedOutputStream;
import java.lang.Thread;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;

/* compiled from: AppCompatDelegateImpl */
class g extends f implements g.a, LayoutInflater.Factory2 {
    private static final Map<Class<?>, Integer> b0 = new b.d.a();
    private static final boolean c0 = (Build.VERSION.SDK_INT < 21);
    private static final int[] d0 = {16842836};
    private static boolean e0 = true;
    private static final boolean f0;
    boolean A;
    boolean B;
    boolean C;
    boolean D;
    boolean E;
    private boolean F;
    private o[] G;
    private o H;
    private boolean I;
    private boolean J;
    private boolean K;
    private boolean L;
    boolean M;
    private int N;
    private int O;
    private boolean P;
    private boolean Q;
    private l R;
    private l S;
    boolean T;
    int U;
    private final Runnable V;
    private boolean W;
    private Rect Y;
    private Rect Z;
    private AppCompatViewInflater a0;

    /* renamed from: d  reason: collision with root package name */
    final Object f633d;

    /* renamed from: e  reason: collision with root package name */
    final Context f634e;

    /* renamed from: f  reason: collision with root package name */
    Window f635f;

    /* renamed from: g  reason: collision with root package name */
    private j f636g;

    /* renamed from: h  reason: collision with root package name */
    final e f637h;

    /* renamed from: i  reason: collision with root package name */
    a f638i;

    /* renamed from: j  reason: collision with root package name */
    MenuInflater f639j;

    /* renamed from: k  reason: collision with root package name */
    private CharSequence f640k;
    private b0 l;
    private h m;
    private p n;
    b.a.n.b o;
    ActionBarContextView p;
    PopupWindow q;
    Runnable r;
    y s;
    private boolean t;
    private boolean u;
    private ViewGroup v;
    private TextView w;
    private View x;
    private boolean y;
    private boolean z;

    /* compiled from: AppCompatDelegateImpl */
    static class a implements Thread.UncaughtExceptionHandler {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Thread.UncaughtExceptionHandler f641a;

        a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.f641a = uncaughtExceptionHandler;
        }

        private boolean a(Throwable th) {
            String message;
            if (!(th instanceof Resources.NotFoundException) || (message = th.getMessage()) == null) {
                return false;
            }
            if (message.contains("drawable") || message.contains("Drawable")) {
                return true;
            }
            return false;
        }

        public void uncaughtException(Thread thread, Throwable th) {
            if (a(th)) {
                Resources.NotFoundException notFoundException = new Resources.NotFoundException(th.getMessage() + ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
                notFoundException.initCause(th.getCause());
                notFoundException.setStackTrace(th.getStackTrace());
                this.f641a.uncaughtException(thread, notFoundException);
                return;
            }
            this.f641a.uncaughtException(thread, th);
        }
    }

    /* compiled from: AppCompatDelegateImpl */
    class b implements Runnable {
        b() {
        }

        public void run() {
            g gVar = g.this;
            if ((gVar.U & 1) != 0) {
                gVar.f(0);
            }
            g gVar2 = g.this;
            if ((gVar2.U & CodedOutputStream.DEFAULT_BUFFER_SIZE) != 0) {
                gVar2.f(108);
            }
            g gVar3 = g.this;
            gVar3.T = false;
            gVar3.U = 0;
        }
    }

    /* compiled from: AppCompatDelegateImpl */
    class c implements q {
        c() {
        }

        public c0 a(View view, c0 c0Var) {
            int d2 = c0Var.d();
            int j2 = g.this.j(d2);
            if (d2 != j2) {
                c0Var = c0Var.a(c0Var.b(), j2, c0Var.c(), c0Var.a());
            }
            return u.b(view, c0Var);
        }
    }

    /* compiled from: AppCompatDelegateImpl */
    class d implements f0.a {
        d() {
        }

        public void a(Rect rect) {
            rect.top = g.this.j(rect.top);
        }
    }

    /* compiled from: AppCompatDelegateImpl */
    class e implements ContentFrameLayout.a {
        e() {
        }

        public void a() {
        }

        public void onDetachedFromWindow() {
            g.this.l();
        }
    }

    /* compiled from: AppCompatDelegateImpl */
    class f implements Runnable {

        /* compiled from: AppCompatDelegateImpl */
        class a extends a0 {
            a() {
            }

            public void b(View view) {
                g.this.p.setAlpha(1.0f);
                g.this.s.a((z) null);
                g.this.s = null;
            }

            public void c(View view) {
                g.this.p.setVisibility(0);
            }
        }

        f() {
        }

        public void run() {
            g gVar = g.this;
            gVar.q.showAtLocation(gVar.p, 55, 0, 0);
            g.this.m();
            if (g.this.u()) {
                g.this.p.setAlpha(Animation.CurveTimeline.LINEAR);
                g gVar2 = g.this;
                y a2 = u.a(gVar2.p);
                a2.a(1.0f);
                gVar2.s = a2;
                g.this.s.a(new a());
                return;
            }
            g.this.p.setAlpha(1.0f);
            g.this.p.setVisibility(0);
        }
    }

    /* renamed from: androidx.appcompat.app.g$g  reason: collision with other inner class name */
    /* compiled from: AppCompatDelegateImpl */
    class C0005g extends a0 {
        C0005g() {
        }

        public void b(View view) {
            g.this.p.setAlpha(1.0f);
            g.this.s.a((z) null);
            g.this.s = null;
        }

        public void c(View view) {
            g.this.p.setVisibility(0);
            g.this.p.sendAccessibilityEvent(32);
            if (g.this.p.getParent() instanceof View) {
                u.w((View) g.this.p.getParent());
            }
        }
    }

    /* compiled from: AppCompatDelegateImpl */
    class i implements b.a {

        /* renamed from: a  reason: collision with root package name */
        private b.a f650a;

        /* compiled from: AppCompatDelegateImpl */
        class a extends a0 {
            a() {
            }

            public void b(View view) {
                g.this.p.setVisibility(8);
                g gVar = g.this;
                PopupWindow popupWindow = gVar.q;
                if (popupWindow != null) {
                    popupWindow.dismiss();
                } else if (gVar.p.getParent() instanceof View) {
                    u.w((View) g.this.p.getParent());
                }
                g.this.p.removeAllViews();
                g.this.s.a((z) null);
                g.this.s = null;
            }
        }

        public i(b.a aVar) {
            this.f650a = aVar;
        }

        public boolean a(b.a.n.b bVar, Menu menu) {
            return this.f650a.a(bVar, menu);
        }

        public boolean b(b.a.n.b bVar, Menu menu) {
            return this.f650a.b(bVar, menu);
        }

        public boolean a(b.a.n.b bVar, MenuItem menuItem) {
            return this.f650a.a(bVar, menuItem);
        }

        public void a(b.a.n.b bVar) {
            this.f650a.a(bVar);
            g gVar = g.this;
            if (gVar.q != null) {
                gVar.f635f.getDecorView().removeCallbacks(g.this.r);
            }
            g gVar2 = g.this;
            if (gVar2.p != null) {
                gVar2.m();
                g gVar3 = g.this;
                y a2 = u.a(gVar3.p);
                a2.a((float) Animation.CurveTimeline.LINEAR);
                gVar3.s = a2;
                g.this.s.a(new a());
            }
            g gVar4 = g.this;
            e eVar = gVar4.f637h;
            if (eVar != null) {
                eVar.b(gVar4.o);
            }
            g.this.o = null;
        }
    }

    /* compiled from: AppCompatDelegateImpl */
    private class k extends l {

        /* renamed from: c  reason: collision with root package name */
        private final PowerManager f654c;

        k(Context context) {
            super();
            this.f654c = (PowerManager) context.getSystemService("power");
        }

        /* access modifiers changed from: package-private */
        public IntentFilter b() {
            if (Build.VERSION.SDK_INT < 21) {
                return null;
            }
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
            return intentFilter;
        }

        public int c() {
            if (Build.VERSION.SDK_INT < 21 || !this.f654c.isPowerSaveMode()) {
                return 1;
            }
            return 2;
        }

        public void d() {
            g.this.k();
        }
    }

    /* compiled from: AppCompatDelegateImpl */
    abstract class l {

        /* renamed from: a  reason: collision with root package name */
        private BroadcastReceiver f656a;

        /* compiled from: AppCompatDelegateImpl */
        class a extends BroadcastReceiver {
            a() {
            }

            public void onReceive(Context context, Intent intent) {
                l.this.d();
            }
        }

        l() {
        }

        /* access modifiers changed from: package-private */
        public void a() {
            BroadcastReceiver broadcastReceiver = this.f656a;
            if (broadcastReceiver != null) {
                try {
                    g.this.f634e.unregisterReceiver(broadcastReceiver);
                } catch (IllegalArgumentException unused) {
                }
                this.f656a = null;
            }
        }

        /* access modifiers changed from: package-private */
        public abstract IntentFilter b();

        /* access modifiers changed from: package-private */
        public abstract int c();

        /* access modifiers changed from: package-private */
        public abstract void d();

        /* access modifiers changed from: package-private */
        public void e() {
            a();
            IntentFilter b2 = b();
            if (b2 != null && b2.countActions() != 0) {
                if (this.f656a == null) {
                    this.f656a = new a();
                }
                g.this.f634e.registerReceiver(this.f656a, b2);
            }
        }
    }

    /* compiled from: AppCompatDelegateImpl */
    private class m extends l {

        /* renamed from: c  reason: collision with root package name */
        private final k f659c;

        m(k kVar) {
            super();
            this.f659c = kVar;
        }

        /* access modifiers changed from: package-private */
        public IntentFilter b() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.TIME_SET");
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            intentFilter.addAction("android.intent.action.TIME_TICK");
            return intentFilter;
        }

        public int c() {
            return this.f659c.a() ? 2 : 1;
        }

        public void d() {
            g.this.k();
        }
    }

    /* compiled from: AppCompatDelegateImpl */
    private class n extends ContentFrameLayout {
        public n(Context context) {
            super(context);
        }

        private boolean a(int i2, int i3) {
            return i2 < -5 || i3 < -5 || i2 > getWidth() + 5 || i3 > getHeight() + 5;
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return g.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !a((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            g.this.e(0);
            return true;
        }

        public void setBackgroundResource(int i2) {
            setBackgroundDrawable(b.a.k.a.a.c(getContext(), i2));
        }
    }

    static {
        boolean z2 = false;
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 21 && i2 <= 25) {
            z2 = true;
        }
        f0 = z2;
        if (c0 && !e0) {
            Thread.setDefaultUncaughtExceptionHandler(new a(Thread.getDefaultUncaughtExceptionHandler()));
        }
    }

    g(Activity activity, e eVar) {
        this(activity, null, eVar, activity);
    }

    private void A() {
        if (this.f635f == null) {
            Object obj = this.f633d;
            if (obj instanceof Activity) {
                a(((Activity) obj).getWindow());
            }
        }
        if (this.f635f == null) {
            throw new IllegalStateException("We have not been given a Window");
        }
    }

    private l B() {
        if (this.S == null) {
            this.S = new k(this.f634e);
        }
        return this.S;
    }

    private void C() {
        z();
        if (this.A && this.f638i == null) {
            Object obj = this.f633d;
            if (obj instanceof Activity) {
                this.f638i = new l((Activity) obj, this.B);
            } else if (obj instanceof Dialog) {
                this.f638i = new l((Dialog) obj);
            }
            a aVar = this.f638i;
            if (aVar != null) {
                aVar.c(this.W);
            }
        }
    }

    private boolean D() {
        if (!this.Q && (this.f633d instanceof Activity)) {
            PackageManager packageManager = this.f634e.getPackageManager();
            if (packageManager == null) {
                return false;
            }
            try {
                ActivityInfo activityInfo = packageManager.getActivityInfo(new ComponentName(this.f634e, this.f633d.getClass()), 0);
                this.P = (activityInfo == null || (activityInfo.configChanges & AdRequest.MAX_CONTENT_URL_LENGTH) == 0) ? false : true;
            } catch (PackageManager.NameNotFoundException e2) {
                Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", e2);
                this.P = false;
            }
        }
        this.Q = true;
        return this.P;
    }

    private void E() {
        if (this.u) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    private d F() {
        Context context = this.f634e;
        while (context != null) {
            if (!(context instanceof d)) {
                if (!(context instanceof ContextWrapper)) {
                    break;
                }
                context = ((ContextWrapper) context).getBaseContext();
            } else {
                return (d) context;
            }
        }
        return null;
    }

    private void k(int i2) {
        this.U = (1 << i2) | this.U;
        if (!this.T) {
            u.a(this.f635f.getDecorView(), this.V);
            this.T = true;
        }
    }

    private int l(int i2) {
        if (i2 == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i2 != 9) {
            return i2;
        } else {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    private void v() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.v.findViewById(16908290);
        View decorView = this.f635f.getDecorView();
        contentFrameLayout.a(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.f634e.obtainStyledAttributes(b.a.j.AppCompatTheme);
        obtainStyledAttributes.getValue(b.a.j.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(b.a.j.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(b.a.j.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(b.a.j.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(b.a.j.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(b.a.j.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(b.a.j.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(b.a.j.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(b.a.j.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(b.a.j.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    private int w() {
        int i2 = this.N;
        return i2 != -100 ? i2 : f.j();
    }

    private void x() {
        l lVar = this.R;
        if (lVar != null) {
            lVar.a();
        }
        l lVar2 = this.S;
        if (lVar2 != null) {
            lVar2.a();
        }
    }

    private ViewGroup y() {
        ViewGroup viewGroup;
        Context context;
        TypedArray obtainStyledAttributes = this.f634e.obtainStyledAttributes(b.a.j.AppCompatTheme);
        if (obtainStyledAttributes.hasValue(b.a.j.AppCompatTheme_windowActionBar)) {
            if (obtainStyledAttributes.getBoolean(b.a.j.AppCompatTheme_windowNoTitle, false)) {
                b(1);
            } else if (obtainStyledAttributes.getBoolean(b.a.j.AppCompatTheme_windowActionBar, false)) {
                b(108);
            }
            if (obtainStyledAttributes.getBoolean(b.a.j.AppCompatTheme_windowActionBarOverlay, false)) {
                b(109);
            }
            if (obtainStyledAttributes.getBoolean(b.a.j.AppCompatTheme_windowActionModeOverlay, false)) {
                b(10);
            }
            this.D = obtainStyledAttributes.getBoolean(b.a.j.AppCompatTheme_android_windowIsFloating, false);
            obtainStyledAttributes.recycle();
            A();
            this.f635f.getDecorView();
            LayoutInflater from = LayoutInflater.from(this.f634e);
            if (this.E) {
                if (this.C) {
                    viewGroup = (ViewGroup) from.inflate(b.a.g.abc_screen_simple_overlay_action_mode, (ViewGroup) null);
                } else {
                    viewGroup = (ViewGroup) from.inflate(b.a.g.abc_screen_simple, (ViewGroup) null);
                }
                if (Build.VERSION.SDK_INT >= 21) {
                    u.a(viewGroup, new c());
                } else {
                    ((f0) viewGroup).setOnFitSystemWindowsListener(new d());
                }
            } else if (this.D) {
                viewGroup = (ViewGroup) from.inflate(b.a.g.abc_dialog_title_material, (ViewGroup) null);
                this.B = false;
                this.A = false;
            } else if (this.A) {
                TypedValue typedValue = new TypedValue();
                this.f634e.getTheme().resolveAttribute(b.a.a.actionBarTheme, typedValue, true);
                int i2 = typedValue.resourceId;
                if (i2 != 0) {
                    context = new b.a.n.d(this.f634e, i2);
                } else {
                    context = this.f634e;
                }
                viewGroup = (ViewGroup) LayoutInflater.from(context).inflate(b.a.g.abc_screen_toolbar, (ViewGroup) null);
                this.l = (b0) viewGroup.findViewById(b.a.f.decor_content_parent);
                this.l.setWindowCallback(q());
                if (this.B) {
                    this.l.a(109);
                }
                if (this.y) {
                    this.l.a(2);
                }
                if (this.z) {
                    this.l.a(5);
                }
            } else {
                viewGroup = null;
            }
            if (viewGroup != null) {
                if (this.l == null) {
                    this.w = (TextView) viewGroup.findViewById(b.a.f.title);
                }
                b1.b(viewGroup);
                ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup.findViewById(b.a.f.action_bar_activity_content);
                ViewGroup viewGroup2 = (ViewGroup) this.f635f.findViewById(16908290);
                if (viewGroup2 != null) {
                    while (viewGroup2.getChildCount() > 0) {
                        View childAt = viewGroup2.getChildAt(0);
                        viewGroup2.removeViewAt(0);
                        contentFrameLayout.addView(childAt);
                    }
                    viewGroup2.setId(-1);
                    contentFrameLayout.setId(16908290);
                    if (viewGroup2 instanceof FrameLayout) {
                        ((FrameLayout) viewGroup2).setForeground(null);
                    }
                }
                this.f635f.setContentView(viewGroup);
                contentFrameLayout.setAttachListener(new e());
                return viewGroup;
            }
            throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.A + ", windowActionBarOverlay: " + this.B + ", android:windowIsFloating: " + this.D + ", windowActionModeOverlay: " + this.C + ", windowNoTitle: " + this.E + " }");
        }
        obtainStyledAttributes.recycle();
        throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
     arg types: [int, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o */
    private void z() {
        if (!this.u) {
            this.v = y();
            CharSequence p2 = p();
            if (!TextUtils.isEmpty(p2)) {
                b0 b0Var = this.l;
                if (b0Var != null) {
                    b0Var.setWindowTitle(p2);
                } else if (t() != null) {
                    t().a(p2);
                } else {
                    TextView textView = this.w;
                    if (textView != null) {
                        textView.setText(p2);
                    }
                }
            }
            v();
            a(this.v);
            this.u = true;
            o a2 = a(0, false);
            if (this.M) {
                return;
            }
            if (a2 == null || a2.f671j == null) {
                k(108);
            }
        }
    }

    public void a(Context context) {
        a(false);
        this.J = true;
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup viewGroup) {
    }

    public void b(Bundle bundle) {
        z();
    }

    public a c() {
        C();
        return this.f638i;
    }

    public void d(int i2) {
        this.O = i2;
    }

    public void e() {
        a c2 = c();
        if (c2 == null || !c2.i()) {
            k(0);
        }
    }

    public void f() {
        f.b(this);
        if (this.T) {
            this.f635f.getDecorView().removeCallbacks(this.V);
        }
        this.L = false;
        this.M = true;
        a aVar = this.f638i;
        if (aVar != null) {
            aVar.j();
        }
        x();
    }

    public void g() {
        a c2 = c();
        if (c2 != null) {
            c2.d(true);
        }
    }

    public void h() {
        this.L = true;
        k();
        f.a(this);
    }

    public void i() {
        this.L = false;
        f.b(this);
        a c2 = c();
        if (c2 != null) {
            c2.d(false);
        }
        if (this.f633d instanceof Dialog) {
            x();
        }
    }

    /* access modifiers changed from: package-private */
    public int j(int i2) {
        boolean z2;
        boolean z3;
        boolean z4;
        ActionBarContextView actionBarContextView = this.p;
        int i3 = 0;
        if (actionBarContextView == null || !(actionBarContextView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z2 = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.p.getLayoutParams();
            z2 = true;
            if (this.p.isShown()) {
                if (this.Y == null) {
                    this.Y = new Rect();
                    this.Z = new Rect();
                }
                Rect rect = this.Y;
                Rect rect2 = this.Z;
                rect.set(0, i2, 0, 0);
                b1.a(this.v, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i2 : 0)) {
                    marginLayoutParams.topMargin = i2;
                    View view = this.x;
                    if (view == null) {
                        this.x = new View(this.f634e);
                        this.x.setBackgroundColor(this.f634e.getResources().getColor(b.a.c.abc_input_method_navigation_guard));
                        this.v.addView(this.x, -1, new ViewGroup.LayoutParams(-1, i2));
                    } else {
                        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                        if (layoutParams.height != i2) {
                            layoutParams.height = i2;
                            this.x.setLayoutParams(layoutParams);
                        }
                    }
                    z3 = true;
                } else {
                    z3 = false;
                }
                if (this.x == null) {
                    z2 = false;
                }
                if (!this.C && z2) {
                    i2 = 0;
                }
            } else {
                if (marginLayoutParams.topMargin != 0) {
                    marginLayoutParams.topMargin = 0;
                    z4 = true;
                } else {
                    z4 = false;
                }
                z2 = false;
            }
            if (z3) {
                this.p.setLayoutParams(marginLayoutParams);
            }
        }
        View view2 = this.x;
        if (view2 != null) {
            if (!z2) {
                i3 = 8;
            }
            view2.setVisibility(i3);
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    public void m() {
        y yVar = this.s;
        if (yVar != null) {
            yVar.a();
        }
    }

    /* access modifiers changed from: package-private */
    public final Context n() {
        a c2 = c();
        Context h2 = c2 != null ? c2.h() : null;
        return h2 == null ? this.f634e : h2;
    }

    /* access modifiers changed from: package-private */
    public final l o() {
        if (this.R == null) {
            this.R = new m(k.a(this.f634e));
        }
        return this.R;
    }

    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return a(view, str, context, attributeSet);
    }

    /* access modifiers changed from: package-private */
    public final CharSequence p() {
        Object obj = this.f633d;
        if (obj instanceof Activity) {
            return ((Activity) obj).getTitle();
        }
        return this.f640k;
    }

    /* access modifiers changed from: package-private */
    public final Window.Callback q() {
        return this.f635f.getCallback();
    }

    public boolean r() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public boolean s() {
        b.a.n.b bVar = this.o;
        if (bVar != null) {
            bVar.a();
            return true;
        }
        a c2 = c();
        if (c2 == null || !c2.f()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final a t() {
        return this.f638i;
    }

    /* access modifiers changed from: package-private */
    public final boolean u() {
        ViewGroup viewGroup;
        return this.u && (viewGroup = this.v) != null && u.t(viewGroup);
    }

    /* compiled from: AppCompatDelegateImpl */
    private final class h implements n.a {
        h() {
        }

        public boolean a(androidx.appcompat.view.menu.g gVar) {
            Window.Callback q = g.this.q();
            if (q == null) {
                return true;
            }
            q.onMenuOpened(108, gVar);
            return true;
        }

        public void a(androidx.appcompat.view.menu.g gVar, boolean z) {
            g.this.b(gVar);
        }
    }

    g(Dialog dialog, e eVar) {
        this(dialog.getContext(), dialog.getWindow(), eVar, dialog);
    }

    public MenuInflater b() {
        if (this.f639j == null) {
            C();
            a aVar = this.f638i;
            this.f639j = new b.a.n.g(aVar != null ? aVar.h() : this.f634e);
        }
        return this.f639j;
    }

    public void d() {
        LayoutInflater from = LayoutInflater.from(this.f634e);
        if (from.getFactory() == null) {
            b.e.m.e.b(from, this);
        } else if (!(from.getFactory2() instanceof g)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }

    /* compiled from: AppCompatDelegateImpl */
    protected static final class o {

        /* renamed from: a  reason: collision with root package name */
        int f662a;

        /* renamed from: b  reason: collision with root package name */
        int f663b;

        /* renamed from: c  reason: collision with root package name */
        int f664c;

        /* renamed from: d  reason: collision with root package name */
        int f665d;

        /* renamed from: e  reason: collision with root package name */
        int f666e;

        /* renamed from: f  reason: collision with root package name */
        int f667f;

        /* renamed from: g  reason: collision with root package name */
        ViewGroup f668g;

        /* renamed from: h  reason: collision with root package name */
        View f669h;

        /* renamed from: i  reason: collision with root package name */
        View f670i;

        /* renamed from: j  reason: collision with root package name */
        androidx.appcompat.view.menu.g f671j;

        /* renamed from: k  reason: collision with root package name */
        androidx.appcompat.view.menu.e f672k;
        Context l;
        boolean m;
        boolean n;
        boolean o;
        public boolean p;
        boolean q = false;
        boolean r;
        Bundle s;

        o(int i2) {
            this.f662a = i2;
        }

        public boolean a() {
            if (this.f669h == null) {
                return false;
            }
            if (this.f670i == null && this.f672k.b().getCount() <= 0) {
                return false;
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public void a(Context context) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(b.a.a.actionBarPopupTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                newTheme.applyStyle(i2, true);
            }
            newTheme.resolveAttribute(b.a.a.panelMenuListTheme, typedValue, true);
            int i3 = typedValue.resourceId;
            if (i3 != 0) {
                newTheme.applyStyle(i3, true);
            } else {
                newTheme.applyStyle(b.a.i.Theme_AppCompat_CompactMenu, true);
            }
            b.a.n.d dVar = new b.a.n.d(context, 0);
            dVar.getTheme().setTo(newTheme);
            this.l = dVar;
            TypedArray obtainStyledAttributes = dVar.obtainStyledAttributes(b.a.j.AppCompatTheme);
            this.f663b = obtainStyledAttributes.getResourceId(b.a.j.AppCompatTheme_panelBackground, 0);
            this.f667f = obtainStyledAttributes.getResourceId(b.a.j.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        /* access modifiers changed from: package-private */
        public void a(androidx.appcompat.view.menu.g gVar) {
            androidx.appcompat.view.menu.e eVar;
            androidx.appcompat.view.menu.g gVar2 = this.f671j;
            if (gVar != gVar2) {
                if (gVar2 != null) {
                    gVar2.b(this.f672k);
                }
                this.f671j = gVar;
                if (gVar != null && (eVar = this.f672k) != null) {
                    gVar.a(eVar);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public androidx.appcompat.view.menu.o a(n.a aVar) {
            if (this.f671j == null) {
                return null;
            }
            if (this.f672k == null) {
                this.f672k = new androidx.appcompat.view.menu.e(this.l, b.a.g.abc_list_menu_item_layout);
                this.f672k.a(aVar);
                this.f671j.a(this.f672k);
            }
            return this.f672k.a(this.f668g);
        }
    }

    private g(Context context, Window window, e eVar, Object obj) {
        Integer num;
        d F2;
        this.s = null;
        this.t = true;
        this.N = -100;
        this.V = new b();
        this.f634e = context;
        this.f637h = eVar;
        this.f633d = obj;
        if (this.N == -100 && (this.f633d instanceof Dialog) && (F2 = F()) != null) {
            this.N = F2.c().a();
        }
        if (this.N == -100 && (num = b0.get(this.f633d.getClass())) != null) {
            this.N = num.intValue();
            b0.remove(this.f633d.getClass());
        }
        if (window != null) {
            a(window);
        }
        androidx.appcompat.widget.j.c();
    }

    public void a(Bundle bundle) {
        this.J = true;
        a(false);
        A();
        Object obj = this.f633d;
        if (obj instanceof Activity) {
            String str = null;
            try {
                str = androidx.core.app.g.b((Activity) obj);
            } catch (IllegalArgumentException unused) {
            }
            if (str != null) {
                a t2 = t();
                if (t2 == null) {
                    this.W = true;
                } else {
                    t2.c(true);
                }
            }
        }
        this.K = true;
    }

    public void c(int i2) {
        z();
        ViewGroup viewGroup = (ViewGroup) this.v.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.f634e).inflate(i2, viewGroup);
        this.f636g.a().onContentChanged();
    }

    /* access modifiers changed from: package-private */
    public int g(int i2) {
        if (i2 == -100) {
            return -1;
        }
        if (i2 == -1) {
            return i2;
        }
        if (i2 != 0) {
            if (i2 == 1 || i2 == 2) {
                return i2;
            }
            if (i2 == 3) {
                return B().c();
            }
            throw new IllegalStateException("Unknown value set for night mode. Please use one of the MODE_NIGHT values from AppCompatDelegate.");
        } else if (Build.VERSION.SDK_INT < 23 || ((UiModeManager) this.f634e.getSystemService(UiModeManager.class)).getNightMode() != 0) {
            return o().c();
        } else {
            return -1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
     arg types: [int, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o */
    /* access modifiers changed from: package-private */
    public void l() {
        androidx.appcompat.view.menu.g gVar;
        b0 b0Var = this.l;
        if (b0Var != null) {
            b0Var.g();
        }
        if (this.q != null) {
            this.f635f.getDecorView().removeCallbacks(this.r);
            if (this.q.isShowing()) {
                try {
                    this.q.dismiss();
                } catch (IllegalArgumentException unused) {
                }
            }
            this.q = null;
        }
        m();
        o a2 = a(0, false);
        if (a2 != null && (gVar = a2.f671j) != null) {
            gVar.close();
        }
    }

    /* compiled from: AppCompatDelegateImpl */
    class j extends b.a.n.i {
        j(Window.Callback callback) {
            super(callback);
        }

        /* access modifiers changed from: package-private */
        public final ActionMode a(ActionMode.Callback callback) {
            f.a aVar = new f.a(g.this.f634e, callback);
            b.a.n.b a2 = g.this.a(aVar);
            if (a2 != null) {
                return aVar.b(a2);
            }
            return null;
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return g.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || g.this.b(keyEvent.getKeyCode(), keyEvent);
        }

        public void onContentChanged() {
        }

        public boolean onCreatePanelMenu(int i2, Menu menu) {
            if (i2 != 0 || (menu instanceof androidx.appcompat.view.menu.g)) {
                return super.onCreatePanelMenu(i2, menu);
            }
            return false;
        }

        public boolean onMenuOpened(int i2, Menu menu) {
            super.onMenuOpened(i2, menu);
            g.this.h(i2);
            return true;
        }

        public void onPanelClosed(int i2, Menu menu) {
            super.onPanelClosed(i2, menu);
            g.this.i(i2);
        }

        public boolean onPreparePanel(int i2, View view, Menu menu) {
            androidx.appcompat.view.menu.g gVar = menu instanceof androidx.appcompat.view.menu.g ? (androidx.appcompat.view.menu.g) menu : null;
            if (i2 == 0 && gVar == null) {
                return false;
            }
            if (gVar != null) {
                gVar.c(true);
            }
            boolean onPreparePanel = super.onPreparePanel(i2, view, menu);
            if (gVar != null) {
                gVar.c(false);
            }
            return onPreparePanel;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
         arg types: [int, int]
         candidates:
          androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
          androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
          androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
          androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
          androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
          androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
          androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
          androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
          androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o */
        public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> list, Menu menu, int i2) {
            androidx.appcompat.view.menu.g gVar;
            o a2 = g.this.a(0, true);
            if (a2 == null || (gVar = a2.f671j) == null) {
                super.onProvideKeyboardShortcuts(list, menu, i2);
            } else {
                super.onProvideKeyboardShortcuts(list, gVar, i2);
            }
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            if (Build.VERSION.SDK_INT >= 23) {
                return null;
            }
            if (g.this.r()) {
                return a(callback);
            }
            return super.onWindowStartingActionMode(callback);
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i2) {
            if (!g.this.r() || i2 != 0) {
                return super.onWindowStartingActionMode(callback, i2);
            }
            return a(callback);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
     arg types: [androidx.appcompat.app.g$o, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
     arg types: [int, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o */
    /* access modifiers changed from: package-private */
    public void e(int i2) {
        a(a(i2, true), true);
    }

    /* access modifiers changed from: package-private */
    public void h(int i2) {
        a c2;
        if (i2 == 108 && (c2 = c()) != null) {
            c2.b(true);
        }
    }

    /* compiled from: AppCompatDelegateImpl */
    private final class p implements n.a {
        p() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
         arg types: [androidx.appcompat.app.g$o, int]
         candidates:
          androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
          androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
          androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
          androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
          androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
          androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
          androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
          androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
          androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void */
        public void a(androidx.appcompat.view.menu.g gVar, boolean z) {
            androidx.appcompat.view.menu.g m = gVar.m();
            boolean z2 = m != gVar;
            g gVar2 = g.this;
            if (z2) {
                gVar = m;
            }
            o a2 = gVar2.a((Menu) gVar);
            if (a2 == null) {
                return;
            }
            if (z2) {
                g.this.a(a2.f662a, a2, m);
                g.this.a(a2, true);
                return;
            }
            g.this.a(a2, z);
        }

        public boolean a(androidx.appcompat.view.menu.g gVar) {
            Window.Callback q;
            if (gVar != null) {
                return true;
            }
            g gVar2 = g.this;
            if (!gVar2.A || (q = gVar2.q()) == null || g.this.M) {
                return true;
            }
            q.onMenuOpened(108, gVar);
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
     arg types: [int, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
     arg types: [androidx.appcompat.app.g$o, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean e(int r4, android.view.KeyEvent r5) {
        /*
            r3 = this;
            b.a.n.b r0 = r3.o
            r1 = 0
            if (r0 == 0) goto L_0x0006
            return r1
        L_0x0006:
            r0 = 1
            androidx.appcompat.app.g$o r2 = r3.a(r4, r0)
            if (r4 != 0) goto L_0x0043
            androidx.appcompat.widget.b0 r4 = r3.l
            if (r4 == 0) goto L_0x0043
            boolean r4 = r4.c()
            if (r4 == 0) goto L_0x0043
            android.content.Context r4 = r3.f634e
            android.view.ViewConfiguration r4 = android.view.ViewConfiguration.get(r4)
            boolean r4 = r4.hasPermanentMenuKey()
            if (r4 != 0) goto L_0x0043
            androidx.appcompat.widget.b0 r4 = r3.l
            boolean r4 = r4.a()
            if (r4 != 0) goto L_0x003c
            boolean r4 = r3.M
            if (r4 != 0) goto L_0x0063
            boolean r4 = r3.b(r2, r5)
            if (r4 == 0) goto L_0x0063
            androidx.appcompat.widget.b0 r4 = r3.l
            boolean r4 = r4.f()
            goto L_0x006a
        L_0x003c:
            androidx.appcompat.widget.b0 r4 = r3.l
            boolean r4 = r4.e()
            goto L_0x006a
        L_0x0043:
            boolean r4 = r2.o
            if (r4 != 0) goto L_0x0065
            boolean r4 = r2.n
            if (r4 == 0) goto L_0x004c
            goto L_0x0065
        L_0x004c:
            boolean r4 = r2.m
            if (r4 == 0) goto L_0x0063
            boolean r4 = r2.r
            if (r4 == 0) goto L_0x005b
            r2.m = r1
            boolean r4 = r3.b(r2, r5)
            goto L_0x005c
        L_0x005b:
            r4 = 1
        L_0x005c:
            if (r4 == 0) goto L_0x0063
            r3.a(r2, r5)
            r4 = 1
            goto L_0x006a
        L_0x0063:
            r4 = 0
            goto L_0x006a
        L_0x0065:
            boolean r4 = r2.o
            r3.a(r2, r0)
        L_0x006a:
            if (r4 == 0) goto L_0x0083
            android.content.Context r5 = r3.f634e
            java.lang.String r0 = "audio"
            java.lang.Object r5 = r5.getSystemService(r0)
            android.media.AudioManager r5 = (android.media.AudioManager) r5
            if (r5 == 0) goto L_0x007c
            r5.playSoundEffect(r1)
            goto L_0x0083
        L_0x007c:
            java.lang.String r5 = "AppCompatDelegate"
            java.lang.String r0 = "Couldn't get audio manager"
            android.util.Log.w(r5, r0)
        L_0x0083:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.app.g.e(int, android.view.KeyEvent):boolean");
    }

    public boolean k() {
        return a(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
     arg types: [int, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o */
    private boolean d(int i2, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() != 0) {
            return false;
        }
        o a2 = a(i2, true);
        if (!a2.o) {
            return b(a2, keyEvent);
        }
        return false;
    }

    public void b(View view, ViewGroup.LayoutParams layoutParams) {
        z();
        ViewGroup viewGroup = (ViewGroup) this.v.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.f636g.a().onContentChanged();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
     arg types: [int, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
     arg types: [androidx.appcompat.app.g$o, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void */
    /* access modifiers changed from: package-private */
    public void i(int i2) {
        if (i2 == 108) {
            a c2 = c();
            if (c2 != null) {
                c2.b(false);
            }
        } else if (i2 == 0) {
            o a2 = a(i2, true);
            if (a2.o) {
                a(a2, false);
            }
        }
    }

    public void c(Bundle bundle) {
        if (this.N != -100) {
            b0.put(this.f633d.getClass(), Integer.valueOf(this.N));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
     arg types: [int, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o */
    /* access modifiers changed from: package-private */
    public void f(int i2) {
        o a2;
        o a3 = a(i2, true);
        if (a3.f671j != null) {
            Bundle bundle = new Bundle();
            a3.f671j.b(bundle);
            if (bundle.size() > 0) {
                a3.s = bundle;
            }
            a3.f671j.s();
            a3.f671j.clear();
        }
        a3.r = true;
        a3.q = true;
        if ((i2 == 108 || i2 == 0) && this.l != null && (a2 = a(0, false)) != null) {
            a2.m = false;
            b(a2, (KeyEvent) null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
     arg types: [int, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
     arg types: [androidx.appcompat.app.g$o, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean c(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            boolean z2 = this.I;
            this.I = false;
            o a2 = a(0, false);
            if (a2 != null && a2.o) {
                if (!z2) {
                    a(a2, true);
                }
                return true;
            } else if (s()) {
                return true;
            }
        } else if (i2 == 82) {
            e(0, keyEvent);
            return true;
        }
        return false;
    }

    public <T extends View> T a(int i2) {
        z();
        return this.f635f.findViewById(i2);
    }

    public boolean b(int i2) {
        int l2 = l(i2);
        if (this.E && l2 == 108) {
            return false;
        }
        if (this.A && l2 == 1) {
            this.A = false;
        }
        if (l2 == 1) {
            E();
            this.E = true;
            return true;
        } else if (l2 == 2) {
            E();
            this.y = true;
            return true;
        } else if (l2 == 5) {
            E();
            this.z = true;
            return true;
        } else if (l2 == 10) {
            E();
            this.C = true;
            return true;
        } else if (l2 == 108) {
            E();
            this.A = true;
            return true;
        } else if (l2 != 109) {
            return this.f635f.requestFeature(l2);
        } else {
            E();
            this.B = true;
            return true;
        }
    }

    public void a(Configuration configuration) {
        a c2;
        if (this.A && this.u && (c2 = c()) != null) {
            c2.a(configuration);
        }
        androidx.appcompat.widget.j.b().a(this.f634e);
        a(false);
    }

    private boolean c(o oVar) {
        Context context = this.f634e;
        int i2 = oVar.f662a;
        if ((i2 == 0 || i2 == 108) && this.l != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = context.getTheme();
            theme.resolveAttribute(b.a.a.actionBarTheme, typedValue, true);
            Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(b.a.a.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(b.a.a.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            if (theme2 != null) {
                b.a.n.d dVar = new b.a.n.d(context, 0);
                dVar.getTheme().setTo(theme2);
                context = dVar;
            }
        }
        androidx.appcompat.view.menu.g gVar = new androidx.appcompat.view.menu.g(context);
        gVar.a(this);
        oVar.a(gVar);
        return true;
    }

    public void a(View view) {
        z();
        ViewGroup viewGroup = (ViewGroup) this.v.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.f636g.a().onContentChanged();
    }

    public void a(View view, ViewGroup.LayoutParams layoutParams) {
        z();
        ((ViewGroup) this.v.findViewById(16908290)).addView(view, layoutParams);
        this.f636g.a().onContentChanged();
    }

    private void a(Window window) {
        if (this.f635f == null) {
            Window.Callback callback = window.getCallback();
            if (!(callback instanceof j)) {
                this.f636g = new j(callback);
                window.setCallback(this.f636g);
                v0 a2 = v0.a(this.f634e, (AttributeSet) null, d0);
                Drawable c2 = a2.c(0);
                if (c2 != null) {
                    window.setBackgroundDrawable(c2);
                }
                a2.a();
                this.f635f = window;
                return;
            }
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        throw new IllegalStateException("AppCompat has already installed itself into the Window");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public b.a.n.b b(b.a.n.b.a r8) {
        /*
            r7 = this;
            r7.m()
            b.a.n.b r0 = r7.o
            if (r0 == 0) goto L_0x000a
            r0.a()
        L_0x000a:
            boolean r0 = r8 instanceof androidx.appcompat.app.g.i
            if (r0 != 0) goto L_0x0014
            androidx.appcompat.app.g$i r0 = new androidx.appcompat.app.g$i
            r0.<init>(r8)
            r8 = r0
        L_0x0014:
            androidx.appcompat.app.e r0 = r7.f637h
            r1 = 0
            if (r0 == 0) goto L_0x0022
            boolean r2 = r7.M
            if (r2 != 0) goto L_0x0022
            b.a.n.b r0 = r0.a(r8)     // Catch:{ AbstractMethodError -> 0x0022 }
            goto L_0x0023
        L_0x0022:
            r0 = r1
        L_0x0023:
            if (r0 == 0) goto L_0x0029
            r7.o = r0
            goto L_0x0165
        L_0x0029:
            androidx.appcompat.widget.ActionBarContextView r0 = r7.p
            r2 = 0
            r3 = 1
            if (r0 != 0) goto L_0x00d6
            boolean r0 = r7.D
            if (r0 == 0) goto L_0x00b7
            android.util.TypedValue r0 = new android.util.TypedValue
            r0.<init>()
            android.content.Context r4 = r7.f634e
            android.content.res.Resources$Theme r4 = r4.getTheme()
            int r5 = b.a.a.actionBarTheme
            r4.resolveAttribute(r5, r0, r3)
            int r5 = r0.resourceId
            if (r5 == 0) goto L_0x0068
            android.content.Context r5 = r7.f634e
            android.content.res.Resources r5 = r5.getResources()
            android.content.res.Resources$Theme r5 = r5.newTheme()
            r5.setTo(r4)
            int r4 = r0.resourceId
            r5.applyStyle(r4, r3)
            b.a.n.d r4 = new b.a.n.d
            android.content.Context r6 = r7.f634e
            r4.<init>(r6, r2)
            android.content.res.Resources$Theme r6 = r4.getTheme()
            r6.setTo(r5)
            goto L_0x006a
        L_0x0068:
            android.content.Context r4 = r7.f634e
        L_0x006a:
            androidx.appcompat.widget.ActionBarContextView r5 = new androidx.appcompat.widget.ActionBarContextView
            r5.<init>(r4)
            r7.p = r5
            android.widget.PopupWindow r5 = new android.widget.PopupWindow
            int r6 = b.a.a.actionModePopupWindowStyle
            r5.<init>(r4, r1, r6)
            r7.q = r5
            android.widget.PopupWindow r5 = r7.q
            r6 = 2
            androidx.core.widget.h.a(r5, r6)
            android.widget.PopupWindow r5 = r7.q
            androidx.appcompat.widget.ActionBarContextView r6 = r7.p
            r5.setContentView(r6)
            android.widget.PopupWindow r5 = r7.q
            r6 = -1
            r5.setWidth(r6)
            android.content.res.Resources$Theme r5 = r4.getTheme()
            int r6 = b.a.a.actionBarSize
            r5.resolveAttribute(r6, r0, r3)
            int r0 = r0.data
            android.content.res.Resources r4 = r4.getResources()
            android.util.DisplayMetrics r4 = r4.getDisplayMetrics()
            int r0 = android.util.TypedValue.complexToDimensionPixelSize(r0, r4)
            androidx.appcompat.widget.ActionBarContextView r4 = r7.p
            r4.setContentHeight(r0)
            android.widget.PopupWindow r0 = r7.q
            r4 = -2
            r0.setHeight(r4)
            androidx.appcompat.app.g$f r0 = new androidx.appcompat.app.g$f
            r0.<init>()
            r7.r = r0
            goto L_0x00d6
        L_0x00b7:
            android.view.ViewGroup r0 = r7.v
            int r4 = b.a.f.action_mode_bar_stub
            android.view.View r0 = r0.findViewById(r4)
            androidx.appcompat.widget.ViewStubCompat r0 = (androidx.appcompat.widget.ViewStubCompat) r0
            if (r0 == 0) goto L_0x00d6
            android.content.Context r4 = r7.n()
            android.view.LayoutInflater r4 = android.view.LayoutInflater.from(r4)
            r0.setLayoutInflater(r4)
            android.view.View r0 = r0.a()
            androidx.appcompat.widget.ActionBarContextView r0 = (androidx.appcompat.widget.ActionBarContextView) r0
            r7.p = r0
        L_0x00d6:
            androidx.appcompat.widget.ActionBarContextView r0 = r7.p
            if (r0 == 0) goto L_0x0165
            r7.m()
            androidx.appcompat.widget.ActionBarContextView r0 = r7.p
            r0.c()
            b.a.n.e r0 = new b.a.n.e
            androidx.appcompat.widget.ActionBarContextView r4 = r7.p
            android.content.Context r4 = r4.getContext()
            androidx.appcompat.widget.ActionBarContextView r5 = r7.p
            android.widget.PopupWindow r6 = r7.q
            if (r6 != 0) goto L_0x00f1
            goto L_0x00f2
        L_0x00f1:
            r3 = 0
        L_0x00f2:
            r0.<init>(r4, r5, r8, r3)
            android.view.Menu r3 = r0.c()
            boolean r8 = r8.a(r0, r3)
            if (r8 == 0) goto L_0x0163
            r0.i()
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            r8.a(r0)
            r7.o = r0
            boolean r8 = r7.u()
            r0 = 1065353216(0x3f800000, float:1.0)
            if (r8 == 0) goto L_0x012d
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            r1 = 0
            r8.setAlpha(r1)
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            b.e.m.y r8 = b.e.m.u.a(r8)
            r8.a(r0)
            r7.s = r8
            b.e.m.y r8 = r7.s
            androidx.appcompat.app.g$g r0 = new androidx.appcompat.app.g$g
            r0.<init>()
            r8.a(r0)
            goto L_0x0153
        L_0x012d:
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            r8.setAlpha(r0)
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            r8.setVisibility(r2)
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            r0 = 32
            r8.sendAccessibilityEvent(r0)
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            android.view.ViewParent r8 = r8.getParent()
            boolean r8 = r8 instanceof android.view.View
            if (r8 == 0) goto L_0x0153
            androidx.appcompat.widget.ActionBarContextView r8 = r7.p
            android.view.ViewParent r8 = r8.getParent()
            android.view.View r8 = (android.view.View) r8
            b.e.m.u.w(r8)
        L_0x0153:
            android.widget.PopupWindow r8 = r7.q
            if (r8 == 0) goto L_0x0165
            android.view.Window r8 = r7.f635f
            android.view.View r8 = r8.getDecorView()
            java.lang.Runnable r0 = r7.r
            r8.post(r0)
            goto L_0x0165
        L_0x0163:
            r7.o = r1
        L_0x0165:
            b.a.n.b r8 = r7.o
            if (r8 == 0) goto L_0x0170
            androidx.appcompat.app.e r0 = r7.f637h
            if (r0 == 0) goto L_0x0170
            r0.a(r8)
        L_0x0170:
            b.a.n.b r8 = r7.o
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.app.g.b(b.a.n.b$a):b.a.n.b");
    }

    private void c(int i2, boolean z2) {
        Resources resources = this.f634e.getResources();
        Configuration configuration = new Configuration(resources.getConfiguration());
        configuration.uiMode = i2 | (resources.getConfiguration().uiMode & -49);
        resources.updateConfiguration(configuration, null);
        if (Build.VERSION.SDK_INT < 26) {
            i.a(resources);
        }
        int i3 = this.O;
        if (i3 != 0) {
            this.f634e.setTheme(i3);
            if (Build.VERSION.SDK_INT >= 23) {
                this.f634e.getTheme().applyStyle(this.O, true);
            }
        }
        if (z2) {
            Object obj = this.f633d;
            if (obj instanceof Activity) {
                Activity activity = (Activity) obj;
                if (activity instanceof androidx.lifecycle.h) {
                    if (((androidx.lifecycle.h) activity).getLifecycle().a().a(e.b.STARTED)) {
                        activity.onConfigurationChanged(configuration);
                    }
                } else if (this.L) {
                    activity.onConfigurationChanged(configuration);
                }
            }
        }
    }

    public final void a(CharSequence charSequence) {
        this.f640k = charSequence;
        b0 b0Var = this.l;
        if (b0Var != null) {
            b0Var.setWindowTitle(charSequence);
        } else if (t() != null) {
            t().a(charSequence);
        } else {
            TextView textView = this.w;
            if (textView != null) {
                textView.setText(charSequence);
            }
        }
    }

    public boolean a(androidx.appcompat.view.menu.g gVar, MenuItem menuItem) {
        o a2;
        Window.Callback q2 = q();
        if (q2 == null || this.M || (a2 = a((Menu) gVar.m())) == null) {
            return false;
        }
        return q2.onMenuItemSelected(a2.f662a, menuItem);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
     arg types: [androidx.appcompat.view.menu.g, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void */
    public void a(androidx.appcompat.view.menu.g gVar) {
        a(gVar, true);
    }

    public b.a.n.b a(b.a aVar) {
        e eVar;
        if (aVar != null) {
            b.a.n.b bVar = this.o;
            if (bVar != null) {
                bVar.a();
            }
            i iVar = new i(aVar);
            a c2 = c();
            if (c2 != null) {
                this.o = c2.a(iVar);
                b.a.n.b bVar2 = this.o;
                if (!(bVar2 == null || (eVar = this.f637h) == null)) {
                    eVar.a(bVar2);
                }
            }
            if (this.o == null) {
                this.o = b(iVar);
            }
            return this.o;
        }
        throw new IllegalArgumentException("ActionMode callback can not be null.");
    }

    /* access modifiers changed from: package-private */
    public boolean a(KeyEvent keyEvent) {
        View decorView;
        Object obj = this.f633d;
        boolean z2 = true;
        if (((obj instanceof d.a) || (obj instanceof h)) && (decorView = this.f635f.getDecorView()) != null && b.e.m.d.a(decorView, keyEvent)) {
            return true;
        }
        if (keyEvent.getKeyCode() == 82 && this.f636g.a().dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            z2 = false;
        }
        return z2 ? a(keyCode, keyEvent) : c(keyCode, keyEvent);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, KeyEvent keyEvent) {
        boolean z2 = true;
        if (i2 == 4) {
            if ((keyEvent.getFlags() & 128) == 0) {
                z2 = false;
            }
            this.I = z2;
        } else if (i2 == 82) {
            d(0, keyEvent);
            return true;
        }
        return false;
    }

    public View a(View view, String str, Context context, AttributeSet attributeSet) {
        boolean z2;
        boolean z3 = false;
        if (this.a0 == null) {
            String string = this.f634e.obtainStyledAttributes(b.a.j.AppCompatTheme).getString(b.a.j.AppCompatTheme_viewInflaterClass);
            if (string == null || AppCompatViewInflater.class.getName().equals(string)) {
                this.a0 = new AppCompatViewInflater();
            } else {
                try {
                    this.a0 = (AppCompatViewInflater) Class.forName(string).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                } catch (Throwable th) {
                    Log.i("AppCompatDelegate", "Failed to instantiate custom view inflater " + string + ". Falling back to default.", th);
                    this.a0 = new AppCompatViewInflater();
                }
            }
        }
        if (c0) {
            if (!(attributeSet instanceof XmlPullParser)) {
                z3 = a((ViewParent) view);
            } else if (((XmlPullParser) attributeSet).getDepth() > 1) {
                z3 = true;
            }
            z2 = z3;
        } else {
            z2 = false;
        }
        return this.a0.a(view, str, context, attributeSet, z2, c0, true, a1.b());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
     arg types: [int, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o */
    /* access modifiers changed from: package-private */
    public boolean b(int i2, KeyEvent keyEvent) {
        a c2 = c();
        if (c2 != null && c2.a(i2, keyEvent)) {
            return true;
        }
        o oVar = this.H;
        if (oVar == null || !a(oVar, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.H == null) {
                o a2 = a(0, true);
                b(a2, keyEvent);
                boolean a3 = a(a2, keyEvent.getKeyCode(), keyEvent, 1);
                a2.m = false;
                if (a3) {
                    return true;
                }
            }
            return false;
        }
        o oVar2 = this.H;
        if (oVar2 != null) {
            oVar2.n = true;
        }
        return true;
    }

    private boolean a(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        View decorView = this.f635f.getDecorView();
        while (viewParent != null) {
            if (viewParent == decorView || !(viewParent instanceof View) || u.s((View) viewParent)) {
                return false;
            }
            viewParent = viewParent.getParent();
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
     arg types: [androidx.appcompat.app.g$o, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void */
    private void a(o oVar, KeyEvent keyEvent) {
        int i2;
        ViewGroup.LayoutParams layoutParams;
        if (!oVar.o && !this.M) {
            if (oVar.f662a == 0) {
                if ((this.f634e.getResources().getConfiguration().screenLayout & 15) == 4) {
                    return;
                }
            }
            Window.Callback q2 = q();
            if (q2 == null || q2.onMenuOpened(oVar.f662a, oVar.f671j)) {
                WindowManager windowManager = (WindowManager) this.f634e.getSystemService("window");
                if (windowManager != null && b(oVar, keyEvent)) {
                    if (oVar.f668g == null || oVar.q) {
                        ViewGroup viewGroup = oVar.f668g;
                        if (viewGroup == null) {
                            if (!b(oVar) || oVar.f668g == null) {
                                return;
                            }
                        } else if (oVar.q && viewGroup.getChildCount() > 0) {
                            oVar.f668g.removeAllViews();
                        }
                        if (a(oVar) && oVar.a()) {
                            ViewGroup.LayoutParams layoutParams2 = oVar.f669h.getLayoutParams();
                            if (layoutParams2 == null) {
                                layoutParams2 = new ViewGroup.LayoutParams(-2, -2);
                            }
                            oVar.f668g.setBackgroundResource(oVar.f663b);
                            ViewParent parent = oVar.f669h.getParent();
                            if (parent instanceof ViewGroup) {
                                ((ViewGroup) parent).removeView(oVar.f669h);
                            }
                            oVar.f668g.addView(oVar.f669h, layoutParams2);
                            if (!oVar.f669h.hasFocus()) {
                                oVar.f669h.requestFocus();
                            }
                        } else {
                            return;
                        }
                    } else {
                        View view = oVar.f670i;
                        if (!(view == null || (layoutParams = view.getLayoutParams()) == null || layoutParams.width != -1)) {
                            i2 = -1;
                            oVar.n = false;
                            WindowManager.LayoutParams layoutParams3 = new WindowManager.LayoutParams(i2, -2, oVar.f665d, oVar.f666e, 1002, 8519680, -3);
                            layoutParams3.gravity = oVar.f664c;
                            layoutParams3.windowAnimations = oVar.f667f;
                            windowManager.addView(oVar.f668g, layoutParams3);
                            oVar.o = true;
                            return;
                        }
                    }
                    i2 = -2;
                    oVar.n = false;
                    WindowManager.LayoutParams layoutParams32 = new WindowManager.LayoutParams(i2, -2, oVar.f665d, oVar.f666e, 1002, 8519680, -3);
                    layoutParams32.gravity = oVar.f664c;
                    layoutParams32.windowAnimations = oVar.f667f;
                    windowManager.addView(oVar.f668g, layoutParams32);
                    oVar.o = true;
                    return;
                }
                return;
            }
            a(oVar, true);
        }
    }

    private boolean b(o oVar) {
        oVar.a(n());
        oVar.f668g = new n(oVar.l);
        oVar.f664c = 81;
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
     arg types: [androidx.appcompat.app.g$o, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void */
    private boolean b(o oVar, KeyEvent keyEvent) {
        b0 b0Var;
        b0 b0Var2;
        b0 b0Var3;
        if (this.M) {
            return false;
        }
        if (oVar.m) {
            return true;
        }
        o oVar2 = this.H;
        if (!(oVar2 == null || oVar2 == oVar)) {
            a(oVar2, false);
        }
        Window.Callback q2 = q();
        if (q2 != null) {
            oVar.f670i = q2.onCreatePanelView(oVar.f662a);
        }
        int i2 = oVar.f662a;
        boolean z2 = i2 == 0 || i2 == 108;
        if (z2 && (b0Var3 = this.l) != null) {
            b0Var3.b();
        }
        if (oVar.f670i == null) {
            if (z2) {
                t();
            }
            if (oVar.f671j == null || oVar.r) {
                if (oVar.f671j == null && (!c(oVar) || oVar.f671j == null)) {
                    return false;
                }
                if (z2 && this.l != null) {
                    if (this.m == null) {
                        this.m = new h();
                    }
                    this.l.a(oVar.f671j, this.m);
                }
                oVar.f671j.s();
                if (!q2.onCreatePanelMenu(oVar.f662a, oVar.f671j)) {
                    oVar.a((androidx.appcompat.view.menu.g) null);
                    if (z2 && (b0Var2 = this.l) != null) {
                        b0Var2.a(null, this.m);
                    }
                    return false;
                }
                oVar.r = false;
            }
            oVar.f671j.s();
            Bundle bundle = oVar.s;
            if (bundle != null) {
                oVar.f671j.a(bundle);
                oVar.s = null;
            }
            if (!q2.onPreparePanel(0, oVar.f670i, oVar.f671j)) {
                if (z2 && (b0Var = this.l) != null) {
                    b0Var.a(null, this.m);
                }
                oVar.f671j.r();
                return false;
            }
            oVar.p = KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1;
            oVar.f671j.setQwertyMode(oVar.p);
            oVar.f671j.r();
        }
        oVar.m = true;
        oVar.n = false;
        this.H = oVar;
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
     arg types: [int, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
     arg types: [androidx.appcompat.app.g$o, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void */
    private void a(androidx.appcompat.view.menu.g gVar, boolean z2) {
        b0 b0Var = this.l;
        if (b0Var == null || !b0Var.c() || (ViewConfiguration.get(this.f634e).hasPermanentMenuKey() && !this.l.d())) {
            o a2 = a(0, true);
            a2.q = true;
            a(a2, false);
            a(a2, (KeyEvent) null);
            return;
        }
        Window.Callback q2 = q();
        if (this.l.a() && z2) {
            this.l.e();
            if (!this.M) {
                q2.onPanelClosed(108, a(0, true).f671j);
            }
        } else if (q2 != null && !this.M) {
            if (this.T && (this.U & 1) != 0) {
                this.f635f.getDecorView().removeCallbacks(this.V);
                this.V.run();
            }
            o a3 = a(0, true);
            androidx.appcompat.view.menu.g gVar2 = a3.f671j;
            if (gVar2 != null && !a3.r && q2.onPreparePanel(0, a3.f670i, gVar2)) {
                q2.onMenuOpened(108, a3.f671j);
                this.l.f();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(androidx.appcompat.view.menu.g gVar) {
        if (!this.F) {
            this.F = true;
            this.l.g();
            Window.Callback q2 = q();
            if (q2 != null && !this.M) {
                q2.onPanelClosed(108, gVar);
            }
            this.F = false;
        }
    }

    private boolean b(int i2, boolean z2) {
        int i3 = this.f634e.getApplicationContext().getResources().getConfiguration().uiMode & 48;
        boolean z3 = true;
        int i4 = i2 != 1 ? i2 != 2 ? i3 : 32 : 16;
        boolean D2 = D();
        boolean z4 = false;
        if ((f0 || i4 != i3) && !D2 && Build.VERSION.SDK_INT >= 17 && !this.J && (this.f633d instanceof ContextThemeWrapper)) {
            Configuration configuration = new Configuration();
            configuration.uiMode = (configuration.uiMode & -49) | i4;
            try {
                ((ContextThemeWrapper) this.f633d).applyOverrideConfiguration(configuration);
                z4 = true;
            } catch (IllegalStateException e2) {
                Log.e("AppCompatDelegate", "updateForNightMode. Calling applyOverrideConfiguration() failed with an exception. Will fall back to using Resources.updateConfiguration()", e2);
            }
        }
        int i5 = this.f634e.getResources().getConfiguration().uiMode & 48;
        if (!z4 && i5 != i4 && z2 && !D2 && this.J && (Build.VERSION.SDK_INT >= 17 || this.K)) {
            Object obj = this.f633d;
            if (obj instanceof Activity) {
                androidx.core.app.a.e((Activity) obj);
                z4 = true;
            }
        }
        if (z4 || i5 == i4) {
            z3 = z4;
        } else {
            c(i4, D2);
        }
        if (z3) {
            Object obj2 = this.f633d;
            if (obj2 instanceof d) {
                ((d) obj2).a(i2);
            }
        }
        return z3;
    }

    private boolean a(o oVar) {
        View view = oVar.f670i;
        if (view != null) {
            oVar.f669h = view;
            return true;
        } else if (oVar.f671j == null) {
            return false;
        } else {
            if (this.n == null) {
                this.n = new p();
            }
            oVar.f669h = (View) oVar.a(this.n);
            if (oVar.f669h != null) {
                return true;
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(o oVar, boolean z2) {
        ViewGroup viewGroup;
        b0 b0Var;
        if (!z2 || oVar.f662a != 0 || (b0Var = this.l) == null || !b0Var.a()) {
            WindowManager windowManager = (WindowManager) this.f634e.getSystemService("window");
            if (!(windowManager == null || !oVar.o || (viewGroup = oVar.f668g) == null)) {
                windowManager.removeView(viewGroup);
                if (z2) {
                    a(oVar.f662a, oVar, null);
                }
            }
            oVar.m = false;
            oVar.n = false;
            oVar.o = false;
            oVar.f669h = null;
            oVar.q = true;
            if (this.H == oVar) {
                this.H = null;
                return;
            }
            return;
        }
        b(oVar.f671j);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, o oVar, Menu menu) {
        if (menu == null) {
            if (oVar == null && i2 >= 0) {
                o[] oVarArr = this.G;
                if (i2 < oVarArr.length) {
                    oVar = oVarArr[i2];
                }
            }
            if (oVar != null) {
                menu = oVar.f671j;
            }
        }
        if ((oVar == null || oVar.o) && !this.M) {
            this.f636g.a().onPanelClosed(i2, menu);
        }
    }

    /* access modifiers changed from: package-private */
    public o a(Menu menu) {
        o[] oVarArr = this.G;
        int length = oVarArr != null ? oVarArr.length : 0;
        for (int i2 = 0; i2 < length; i2++) {
            o oVar = oVarArr[i2];
            if (oVar != null && oVar.f671j == menu) {
                return oVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public o a(int i2, boolean z2) {
        o[] oVarArr = this.G;
        if (oVarArr == null || oVarArr.length <= i2) {
            o[] oVarArr2 = new o[(i2 + 1)];
            if (oVarArr != null) {
                System.arraycopy(oVarArr, 0, oVarArr2, 0, oVarArr.length);
            }
            this.G = oVarArr2;
            oVarArr = oVarArr2;
        }
        o oVar = oVarArr[i2];
        if (oVar != null) {
            return oVar;
        }
        o oVar2 = new o(i2);
        oVarArr[i2] = oVar2;
        return oVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void
     arg types: [androidx.appcompat.app.g$o, int]
     candidates:
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, android.view.KeyEvent):void
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, boolean):void
      androidx.appcompat.app.g.a(int, boolean):androidx.appcompat.app.g$o
      androidx.appcompat.app.g.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.app.g.a(int, android.view.KeyEvent):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.f.a(android.app.Activity, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.app.Dialog, androidx.appcompat.app.e):androidx.appcompat.app.f
      androidx.appcompat.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      androidx.appcompat.view.menu.g.a.a(androidx.appcompat.view.menu.g, android.view.MenuItem):boolean
      androidx.appcompat.app.g.a(androidx.appcompat.app.g$o, boolean):void */
    private boolean a(o oVar, int i2, KeyEvent keyEvent, int i3) {
        androidx.appcompat.view.menu.g gVar;
        boolean z2 = false;
        if (keyEvent.isSystem()) {
            return false;
        }
        if ((oVar.m || b(oVar, keyEvent)) && (gVar = oVar.f671j) != null) {
            z2 = gVar.performShortcut(i2, keyEvent, i3);
        }
        if (z2 && (i3 & 1) == 0 && this.l == null) {
            a(oVar, true);
        }
        return z2;
    }

    private boolean a(boolean z2) {
        if (this.M) {
            return false;
        }
        int w2 = w();
        boolean b2 = b(g(w2), z2);
        if (w2 == 0) {
            o().e();
        } else {
            l lVar = this.R;
            if (lVar != null) {
                lVar.a();
            }
        }
        if (w2 == 3) {
            B().e();
        } else {
            l lVar2 = this.S;
            if (lVar2 != null) {
                lVar2.a();
            }
        }
        return b2;
    }

    public int a() {
        return this.N;
    }
}
