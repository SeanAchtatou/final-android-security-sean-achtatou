package androidx.appcompat.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import androidx.appcompat.app.a;
import androidx.appcompat.view.menu.g;
import androidx.appcompat.widget.ActionBarContainer;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ActionBarOverlayLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.c0;
import androidx.appcompat.widget.o0;
import b.a.f;
import b.a.j;
import b.a.n.b;
import b.a.n.h;
import b.e.m.a0;
import b.e.m.b0;
import b.e.m.u;
import b.e.m.y;
import b.e.m.z;
import com.esotericsoftware.spine.Animation;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: WindowDecorActionBar */
public class l extends a implements ActionBarOverlayLayout.d {
    private static final Interpolator B = new AccelerateInterpolator();
    private static final Interpolator C = new DecelerateInterpolator();
    final b0 A = new c();

    /* renamed from: a  reason: collision with root package name */
    Context f699a;

    /* renamed from: b  reason: collision with root package name */
    private Context f700b;

    /* renamed from: c  reason: collision with root package name */
    ActionBarOverlayLayout f701c;

    /* renamed from: d  reason: collision with root package name */
    ActionBarContainer f702d;

    /* renamed from: e  reason: collision with root package name */
    c0 f703e;

    /* renamed from: f  reason: collision with root package name */
    ActionBarContextView f704f;

    /* renamed from: g  reason: collision with root package name */
    View f705g;

    /* renamed from: h  reason: collision with root package name */
    o0 f706h;

    /* renamed from: i  reason: collision with root package name */
    private boolean f707i;

    /* renamed from: j  reason: collision with root package name */
    d f708j;

    /* renamed from: k  reason: collision with root package name */
    b.a.n.b f709k;
    b.a l;
    private boolean m;
    private ArrayList<a.b> n = new ArrayList<>();
    private boolean o;
    private int p = 0;
    boolean q = true;
    boolean r;
    boolean s;
    private boolean t;
    private boolean u = true;
    h v;
    private boolean w;
    boolean x;
    final z y = new a();
    final z z = new b();

    /* compiled from: WindowDecorActionBar */
    class a extends a0 {
        a() {
        }

        public void b(View view) {
            View view2;
            l lVar = l.this;
            if (lVar.q && (view2 = lVar.f705g) != null) {
                view2.setTranslationY(Animation.CurveTimeline.LINEAR);
                l.this.f702d.setTranslationY(Animation.CurveTimeline.LINEAR);
            }
            l.this.f702d.setVisibility(8);
            l.this.f702d.setTransitioning(false);
            l lVar2 = l.this;
            lVar2.v = null;
            lVar2.l();
            ActionBarOverlayLayout actionBarOverlayLayout = l.this.f701c;
            if (actionBarOverlayLayout != null) {
                u.w(actionBarOverlayLayout);
            }
        }
    }

    /* compiled from: WindowDecorActionBar */
    class b extends a0 {
        b() {
        }

        public void b(View view) {
            l lVar = l.this;
            lVar.v = null;
            lVar.f702d.requestLayout();
        }
    }

    /* compiled from: WindowDecorActionBar */
    class c implements b0 {
        c() {
        }

        public void a(View view) {
            ((View) l.this.f702d.getParent()).invalidate();
        }
    }

    /* compiled from: WindowDecorActionBar */
    public class d extends b.a.n.b implements g.a {

        /* renamed from: c  reason: collision with root package name */
        private final Context f713c;

        /* renamed from: d  reason: collision with root package name */
        private final g f714d;

        /* renamed from: e  reason: collision with root package name */
        private b.a f715e;

        /* renamed from: f  reason: collision with root package name */
        private WeakReference<View> f716f;

        public d(Context context, b.a aVar) {
            this.f713c = context;
            this.f715e = aVar;
            g gVar = new g(context);
            gVar.c(1);
            this.f714d = gVar;
            this.f714d.a(this);
        }

        public void a() {
            l lVar = l.this;
            if (lVar.f708j == this) {
                if (!l.a(lVar.r, lVar.s, false)) {
                    l lVar2 = l.this;
                    lVar2.f709k = this;
                    lVar2.l = this.f715e;
                } else {
                    this.f715e.a(this);
                }
                this.f715e = null;
                l.this.e(false);
                l.this.f704f.a();
                l.this.f703e.j().sendAccessibilityEvent(32);
                l lVar3 = l.this;
                lVar3.f701c.setHideOnContentScrollEnabled(lVar3.x);
                l.this.f708j = null;
            }
        }

        public void b(CharSequence charSequence) {
            l.this.f704f.setTitle(charSequence);
        }

        public Menu c() {
            return this.f714d;
        }

        public MenuInflater d() {
            return new b.a.n.g(this.f713c);
        }

        public CharSequence e() {
            return l.this.f704f.getSubtitle();
        }

        public CharSequence g() {
            return l.this.f704f.getTitle();
        }

        public void i() {
            if (l.this.f708j == this) {
                this.f714d.s();
                try {
                    this.f715e.b(this, this.f714d);
                } finally {
                    this.f714d.r();
                }
            }
        }

        public boolean j() {
            return l.this.f704f.b();
        }

        public boolean k() {
            this.f714d.s();
            try {
                return this.f715e.a(this, this.f714d);
            } finally {
                this.f714d.r();
            }
        }

        public void b(int i2) {
            b(l.this.f699a.getResources().getString(i2));
        }

        public View b() {
            WeakReference<View> weakReference = this.f716f;
            if (weakReference != null) {
                return weakReference.get();
            }
            return null;
        }

        public void a(View view) {
            l.this.f704f.setCustomView(view);
            this.f716f = new WeakReference<>(view);
        }

        public void a(CharSequence charSequence) {
            l.this.f704f.setSubtitle(charSequence);
        }

        public void a(int i2) {
            a((CharSequence) l.this.f699a.getResources().getString(i2));
        }

        public void a(boolean z) {
            super.a(z);
            l.this.f704f.setTitleOptional(z);
        }

        public boolean a(g gVar, MenuItem menuItem) {
            b.a aVar = this.f715e;
            if (aVar != null) {
                return aVar.a(this, menuItem);
            }
            return false;
        }

        public void a(g gVar) {
            if (this.f715e != null) {
                i();
                l.this.f704f.d();
            }
        }
    }

    public l(Activity activity, boolean z2) {
        new ArrayList();
        View decorView = activity.getWindow().getDecorView();
        b(decorView);
        if (!z2) {
            this.f705g = decorView.findViewById(16908290);
        }
    }

    private c0 a(View view) {
        if (view instanceof c0) {
            return (c0) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Can't make a decor toolbar out of ");
        sb.append(view != null ? view.getClass().getSimpleName() : "null");
        throw new IllegalStateException(sb.toString());
    }

    static boolean a(boolean z2, boolean z3, boolean z4) {
        if (z4) {
            return true;
        }
        return !z2 && !z3;
    }

    private void b(View view) {
        this.f701c = (ActionBarOverlayLayout) view.findViewById(f.decor_content_parent);
        ActionBarOverlayLayout actionBarOverlayLayout = this.f701c;
        if (actionBarOverlayLayout != null) {
            actionBarOverlayLayout.setActionBarVisibilityCallback(this);
        }
        this.f703e = a(view.findViewById(f.action_bar));
        this.f704f = (ActionBarContextView) view.findViewById(f.action_context_bar);
        this.f702d = (ActionBarContainer) view.findViewById(f.action_bar_container);
        c0 c0Var = this.f703e;
        if (c0Var == null || this.f704f == null || this.f702d == null) {
            throw new IllegalStateException(l.class.getSimpleName() + " can only be used with a compatible window decor layout");
        }
        this.f699a = c0Var.getContext();
        boolean z2 = (this.f703e.k() & 4) != 0;
        if (z2) {
            this.f707i = true;
        }
        b.a.n.a a2 = b.a.n.a.a(this.f699a);
        j(a2.a() || z2);
        k(a2.f());
        TypedArray obtainStyledAttributes = this.f699a.obtainStyledAttributes(null, j.ActionBar, b.a.a.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(j.ActionBar_hideOnContentScroll, false)) {
            i(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(j.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            a((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    private void k(boolean z2) {
        this.o = z2;
        if (!this.o) {
            this.f703e.a((o0) null);
            this.f702d.setTabContainer(this.f706h);
        } else {
            this.f702d.setTabContainer(null);
            this.f703e.a(this.f706h);
        }
        boolean z3 = true;
        boolean z4 = m() == 2;
        o0 o0Var = this.f706h;
        if (o0Var != null) {
            if (z4) {
                o0Var.setVisibility(0);
                ActionBarOverlayLayout actionBarOverlayLayout = this.f701c;
                if (actionBarOverlayLayout != null) {
                    u.w(actionBarOverlayLayout);
                }
            } else {
                o0Var.setVisibility(8);
            }
        }
        this.f703e.b(!this.o && z4);
        ActionBarOverlayLayout actionBarOverlayLayout2 = this.f701c;
        if (this.o || !z4) {
            z3 = false;
        }
        actionBarOverlayLayout2.setHasNonEmbeddedTabs(z3);
    }

    private void n() {
        if (this.t) {
            this.t = false;
            ActionBarOverlayLayout actionBarOverlayLayout = this.f701c;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(false);
            }
            l(false);
        }
    }

    private boolean o() {
        return u.t(this.f702d);
    }

    private void p() {
        if (!this.t) {
            this.t = true;
            ActionBarOverlayLayout actionBarOverlayLayout = this.f701c;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(true);
            }
            l(false);
        }
    }

    public void b() {
    }

    public void c() {
        if (!this.s) {
            this.s = true;
            l(true);
        }
    }

    public void d(boolean z2) {
        h hVar;
        this.w = z2;
        if (!z2 && (hVar = this.v) != null) {
            hVar.a();
        }
    }

    public void e(boolean z2) {
        y yVar;
        y yVar2;
        if (z2) {
            p();
        } else {
            n();
        }
        if (o()) {
            if (z2) {
                yVar = this.f703e.a(4, 100);
                yVar2 = this.f704f.a(0, 200);
            } else {
                yVar2 = this.f703e.a(0, 200);
                yVar = this.f704f.a(8, 100);
            }
            h hVar = new h();
            hVar.a(yVar, yVar2);
            hVar.c();
        } else if (z2) {
            this.f703e.c(4);
            this.f704f.setVisibility(0);
        } else {
            this.f703e.c(0);
            this.f704f.setVisibility(8);
        }
    }

    public void f(boolean z2) {
        View view;
        h hVar = this.v;
        if (hVar != null) {
            hVar.a();
        }
        if (this.p != 0 || (!this.w && !z2)) {
            this.y.b(null);
            return;
        }
        this.f702d.setAlpha(1.0f);
        this.f702d.setTransitioning(true);
        h hVar2 = new h();
        float f2 = (float) (-this.f702d.getHeight());
        if (z2) {
            int[] iArr = {0, 0};
            this.f702d.getLocationInWindow(iArr);
            f2 -= (float) iArr[1];
        }
        y a2 = u.a(this.f702d);
        a2.b(f2);
        a2.a(this.A);
        hVar2.a(a2);
        if (this.q && (view = this.f705g) != null) {
            y a3 = u.a(view);
            a3.b(f2);
            hVar2.a(a3);
        }
        hVar2.a(B);
        hVar2.a(250);
        hVar2.a(this.y);
        this.v = hVar2;
        hVar2.c();
    }

    public int g() {
        return this.f703e.k();
    }

    public void h(boolean z2) {
        a(z2 ? 4 : 0, 4);
    }

    public void i(boolean z2) {
        if (!z2 || this.f701c.i()) {
            this.x = z2;
            this.f701c.setHideOnContentScrollEnabled(z2);
            return;
        }
        throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    public void j(boolean z2) {
        this.f703e.a(z2);
    }

    /* access modifiers changed from: package-private */
    public void l() {
        b.a aVar = this.l;
        if (aVar != null) {
            aVar.a(this.f709k);
            this.f709k = null;
            this.l = null;
        }
    }

    public int m() {
        return this.f703e.i();
    }

    public void onWindowVisibilityChanged(int i2) {
        this.p = i2;
    }

    public void g(boolean z2) {
        View view;
        View view2;
        h hVar = this.v;
        if (hVar != null) {
            hVar.a();
        }
        this.f702d.setVisibility(0);
        if (this.p != 0 || (!this.w && !z2)) {
            this.f702d.setAlpha(1.0f);
            this.f702d.setTranslationY(Animation.CurveTimeline.LINEAR);
            if (this.q && (view = this.f705g) != null) {
                view.setTranslationY(Animation.CurveTimeline.LINEAR);
            }
            this.z.b(null);
        } else {
            this.f702d.setTranslationY(Animation.CurveTimeline.LINEAR);
            float f2 = (float) (-this.f702d.getHeight());
            if (z2) {
                int[] iArr = {0, 0};
                this.f702d.getLocationInWindow(iArr);
                f2 -= (float) iArr[1];
            }
            this.f702d.setTranslationY(f2);
            h hVar2 = new h();
            y a2 = u.a(this.f702d);
            a2.b((float) Animation.CurveTimeline.LINEAR);
            a2.a(this.A);
            hVar2.a(a2);
            if (this.q && (view2 = this.f705g) != null) {
                view2.setTranslationY(f2);
                y a3 = u.a(this.f705g);
                a3.b((float) Animation.CurveTimeline.LINEAR);
                hVar2.a(a3);
            }
            hVar2.a(C);
            hVar2.a(250);
            hVar2.a(this.z);
            this.v = hVar2;
            hVar2.c();
        }
        ActionBarOverlayLayout actionBarOverlayLayout = this.f701c;
        if (actionBarOverlayLayout != null) {
            u.w(actionBarOverlayLayout);
        }
    }

    public Context h() {
        if (this.f700b == null) {
            TypedValue typedValue = new TypedValue();
            this.f699a.getTheme().resolveAttribute(b.a.a.actionBarWidgetTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.f700b = new ContextThemeWrapper(this.f699a, i2);
            } else {
                this.f700b = this.f699a;
            }
        }
        return this.f700b;
    }

    public void c(boolean z2) {
        if (!this.f707i) {
            h(z2);
        }
    }

    public void d() {
        h hVar = this.v;
        if (hVar != null) {
            hVar.a();
            this.v = null;
        }
    }

    private void l(boolean z2) {
        if (a(this.r, this.s, this.t)) {
            if (!this.u) {
                this.u = true;
                g(z2);
            }
        } else if (this.u) {
            this.u = false;
            f(z2);
        }
    }

    public void a(float f2) {
        u.a(this.f702d, f2);
    }

    public void a(Configuration configuration) {
        k(b.a.n.a.a(this.f699a).f());
    }

    public void a(CharSequence charSequence) {
        this.f703e.setWindowTitle(charSequence);
    }

    public void a(int i2, int i3) {
        int k2 = this.f703e.k();
        if ((i3 & 4) != 0) {
            this.f707i = true;
        }
        this.f703e.a((i2 & i3) | ((i3 ^ -1) & k2));
    }

    public b.a.n.b a(b.a aVar) {
        d dVar = this.f708j;
        if (dVar != null) {
            dVar.a();
        }
        this.f701c.setHideOnContentScrollEnabled(false);
        this.f704f.c();
        d dVar2 = new d(this.f704f.getContext(), aVar);
        if (!dVar2.k()) {
            return null;
        }
        this.f708j = dVar2;
        dVar2.i();
        this.f704f.a(dVar2);
        e(true);
        this.f704f.sendAccessibilityEvent(32);
        return dVar2;
    }

    public l(Dialog dialog) {
        new ArrayList();
        b(dialog.getWindow().getDecorView());
    }

    public void b(boolean z2) {
        if (z2 != this.m) {
            this.m = z2;
            int size = this.n.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.n.get(i2).a(z2);
            }
        }
    }

    public boolean f() {
        c0 c0Var = this.f703e;
        if (c0Var == null || !c0Var.h()) {
            return false;
        }
        this.f703e.collapseActionView();
        return true;
    }

    public void a(boolean z2) {
        this.q = z2;
    }

    public void a() {
        if (this.s) {
            this.s = false;
            l(true);
        }
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        Menu c2;
        d dVar = this.f708j;
        if (dVar == null || (c2 = dVar.c()) == null) {
            return false;
        }
        boolean z2 = true;
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z2 = false;
        }
        c2.setQwertyMode(z2);
        return c2.performShortcut(i2, keyEvent, 0);
    }
}
