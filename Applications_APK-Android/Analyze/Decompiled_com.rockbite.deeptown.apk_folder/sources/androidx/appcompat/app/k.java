package androidx.appcompat.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import b.e.e.b;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.Calendar;

/* compiled from: TwilightManager */
class k {

    /* renamed from: d  reason: collision with root package name */
    private static k f689d;

    /* renamed from: a  reason: collision with root package name */
    private final Context f690a;

    /* renamed from: b  reason: collision with root package name */
    private final LocationManager f691b;

    /* renamed from: c  reason: collision with root package name */
    private final a f692c = new a();

    /* compiled from: TwilightManager */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        boolean f693a;

        /* renamed from: b  reason: collision with root package name */
        long f694b;

        /* renamed from: c  reason: collision with root package name */
        long f695c;

        /* renamed from: d  reason: collision with root package name */
        long f696d;

        /* renamed from: e  reason: collision with root package name */
        long f697e;

        /* renamed from: f  reason: collision with root package name */
        long f698f;

        a() {
        }
    }

    k(Context context, LocationManager locationManager) {
        this.f690a = context;
        this.f691b = locationManager;
    }

    static k a(Context context) {
        if (f689d == null) {
            Context applicationContext = context.getApplicationContext();
            f689d = new k(applicationContext, (LocationManager) applicationContext.getSystemService(FirebaseAnalytics.Param.LOCATION));
        }
        return f689d;
    }

    @SuppressLint({"MissingPermission"})
    private Location b() {
        Location location = null;
        Location a2 = b.b(this.f690a, "android.permission.ACCESS_COARSE_LOCATION") == 0 ? a("network") : null;
        if (b.b(this.f690a, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            location = a("gps");
        }
        return (location == null || a2 == null) ? location != null ? location : a2 : location.getTime() > a2.getTime() ? location : a2;
    }

    private boolean c() {
        return this.f692c.f698f > System.currentTimeMillis();
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        a aVar = this.f692c;
        if (c()) {
            return aVar.f693a;
        }
        Location b2 = b();
        if (b2 != null) {
            a(b2);
            return aVar.f693a;
        }
        Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        int i2 = Calendar.getInstance().get(11);
        return i2 < 6 || i2 >= 22;
    }

    private Location a(String str) {
        try {
            if (this.f691b.isProviderEnabled(str)) {
                return this.f691b.getLastKnownLocation(str);
            }
            return null;
        } catch (Exception e2) {
            Log.d("TwilightManager", "Failed to get last known location", e2);
            return null;
        }
    }

    private void a(Location location) {
        long j2;
        a aVar = this.f692c;
        long currentTimeMillis = System.currentTimeMillis();
        j a2 = j.a();
        j jVar = a2;
        jVar.a(currentTimeMillis - 86400000, location.getLatitude(), location.getLongitude());
        long j3 = a2.f686a;
        jVar.a(currentTimeMillis, location.getLatitude(), location.getLongitude());
        boolean z = a2.f688c == 1;
        long j4 = a2.f687b;
        long j5 = j3;
        long j6 = a2.f686a;
        long j7 = j4;
        boolean z2 = z;
        a2.a(86400000 + currentTimeMillis, location.getLatitude(), location.getLongitude());
        long j8 = a2.f687b;
        if (j7 == -1 || j6 == -1) {
            j2 = 43200000 + currentTimeMillis;
        } else {
            j2 = (currentTimeMillis > j6 ? 0 + j8 : currentTimeMillis > j7 ? 0 + j6 : 0 + j7) + 60000;
        }
        aVar.f693a = z2;
        aVar.f694b = j5;
        aVar.f695c = j7;
        aVar.f696d = j6;
        aVar.f697e = j8;
        aVar.f698f = j2;
    }
}
