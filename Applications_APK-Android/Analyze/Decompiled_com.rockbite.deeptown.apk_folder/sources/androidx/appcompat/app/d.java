package androidx.appcompat.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.appcompat.widget.a1;
import androidx.core.app.a;
import androidx.core.app.g;
import androidx.core.app.o;
import androidx.fragment.app.c;
import b.a.n.b;

/* compiled from: AppCompatActivity */
public class d extends c implements e, o.a, b {

    /* renamed from: a  reason: collision with root package name */
    private f f628a;

    /* renamed from: b  reason: collision with root package name */
    private Resources f629b;

    public b a(b.a aVar) {
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(int i2) {
    }

    public void a(o oVar) {
        oVar.a((Activity) this);
    }

    public void a(b bVar) {
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        c().a(view, layoutParams);
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        c().a(context);
    }

    public Intent b() {
        return g.a(this);
    }

    public void b(o oVar) {
    }

    public void b(b bVar) {
    }

    public f c() {
        if (this.f628a == null) {
            this.f628a = f.a(this, this);
        }
        return this.f628a;
    }

    public void closeOptionsMenu() {
        a d2 = d();
        if (!getWindow().hasFeature(0)) {
            return;
        }
        if (d2 == null || !d2.e()) {
            super.closeOptionsMenu();
        }
    }

    public a d() {
        return c().c();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        a d2 = d();
        if (keyCode != 82 || d2 == null || !d2.a(keyEvent)) {
            return super.dispatchKeyEvent(keyEvent);
        }
        return true;
    }

    @Deprecated
    public void e() {
    }

    public boolean f() {
        Intent b2 = b();
        if (b2 == null) {
            return false;
        }
        if (b(b2)) {
            o a2 = o.a((Context) this);
            a(a2);
            b(a2);
            a2.a();
            try {
                a.a((Activity) this);
                return true;
            } catch (IllegalStateException unused) {
                finish();
                return true;
            }
        } else {
            a(b2);
            return true;
        }
    }

    public <T extends View> T findViewById(int i2) {
        return c().a(i2);
    }

    public MenuInflater getMenuInflater() {
        return c().b();
    }

    public Resources getResources() {
        if (this.f629b == null && a1.b()) {
            this.f629b = new a1(this, super.getResources());
        }
        Resources resources = this.f629b;
        return resources == null ? super.getResources() : resources;
    }

    public void invalidateOptionsMenu() {
        c().e();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.f629b != null) {
            this.f629b.updateConfiguration(configuration, super.getResources().getDisplayMetrics());
        }
        c().a(configuration);
    }

    public void onContentChanged() {
        e();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        f c2 = c();
        c2.d();
        c2.a(bundle);
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        c().f();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (a(i2, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public final boolean onMenuItemSelected(int i2, MenuItem menuItem) {
        if (super.onMenuItemSelected(i2, menuItem)) {
            return true;
        }
        a d2 = d();
        if (menuItem.getItemId() != 16908332 || d2 == null || (d2.g() & 4) == 0) {
            return false;
        }
        return f();
    }

    public boolean onMenuOpened(int i2, Menu menu) {
        return super.onMenuOpened(i2, menu);
    }

    public void onPanelClosed(int i2, Menu menu) {
        super.onPanelClosed(i2, menu);
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        c().b(bundle);
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        c().g();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        c().c(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        c().h();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        c().i();
    }

    /* access modifiers changed from: protected */
    public void onTitleChanged(CharSequence charSequence, int i2) {
        super.onTitleChanged(charSequence, i2);
        c().a(charSequence);
    }

    public void openOptionsMenu() {
        a d2 = d();
        if (!getWindow().hasFeature(0)) {
            return;
        }
        if (d2 == null || !d2.k()) {
            super.openOptionsMenu();
        }
    }

    public void setContentView(int i2) {
        c().c(i2);
    }

    public void setTheme(int i2) {
        super.setTheme(i2);
        c().d(i2);
    }

    public void supportInvalidateOptionsMenu() {
        c().e();
    }

    public void a(Intent intent) {
        g.a(this, intent);
    }

    public boolean b(Intent intent) {
        return g.b(this, intent);
    }

    public void setContentView(View view) {
        c().a(view);
    }

    private boolean a(int i2, KeyEvent keyEvent) {
        Window window;
        return Build.VERSION.SDK_INT < 26 && !keyEvent.isCtrlPressed() && !KeyEvent.metaStateHasNoModifiers(keyEvent.getMetaState()) && keyEvent.getRepeatCount() == 0 && !KeyEvent.isModifierKey(keyEvent.getKeyCode()) && (window = getWindow()) != null && window.getDecorView() != null && window.getDecorView().dispatchKeyShortcutEvent(keyEvent);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        c().b(view, layoutParams);
    }
}
