package androidx.appcompat.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import b.d.b;
import java.lang.ref.WeakReference;
import java.util.Iterator;

/* compiled from: AppCompatDelegate */
public abstract class f {

    /* renamed from: a  reason: collision with root package name */
    private static int f630a = -100;

    /* renamed from: b  reason: collision with root package name */
    private static final b<WeakReference<f>> f631b = new b<>();

    /* renamed from: c  reason: collision with root package name */
    private static final Object f632c = new Object();

    f() {
    }

    public static f a(Activity activity, e eVar) {
        return new g(activity, eVar);
    }

    static void b(f fVar) {
        synchronized (f632c) {
            c(fVar);
        }
    }

    private static void c(f fVar) {
        synchronized (f632c) {
            Iterator<WeakReference<f>> it = f631b.iterator();
            while (it.hasNext()) {
                f fVar2 = (f) it.next().get();
                if (fVar2 == fVar || fVar2 == null) {
                    it.remove();
                }
            }
        }
    }

    public static int j() {
        return f630a;
    }

    public int a() {
        return -100;
    }

    public abstract <T extends View> T a(int i2);

    public void a(Context context) {
    }

    public abstract void a(Configuration configuration);

    public abstract void a(Bundle bundle);

    public abstract void a(View view);

    public abstract void a(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void a(CharSequence charSequence);

    public abstract MenuInflater b();

    public abstract void b(Bundle bundle);

    public abstract void b(View view, ViewGroup.LayoutParams layoutParams);

    public abstract boolean b(int i2);

    public abstract a c();

    public abstract void c(int i2);

    public abstract void c(Bundle bundle);

    public abstract void d();

    public void d(int i2) {
    }

    public abstract void e();

    public abstract void f();

    public abstract void g();

    public abstract void h();

    public abstract void i();

    public static f a(Dialog dialog, e eVar) {
        return new g(dialog, eVar);
    }

    static void a(f fVar) {
        synchronized (f632c) {
            c(fVar);
            f631b.add(new WeakReference(fVar));
        }
    }
}
