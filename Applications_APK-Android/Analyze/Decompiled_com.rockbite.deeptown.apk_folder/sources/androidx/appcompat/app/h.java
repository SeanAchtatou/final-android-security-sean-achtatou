package androidx.appcompat.app;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import b.a.n.b;
import b.e.m.d;

/* compiled from: AppCompatDialog */
public class h extends Dialog implements e {

    /* renamed from: a  reason: collision with root package name */
    private f f674a;

    /* renamed from: b  reason: collision with root package name */
    private final d.a f675b = new a();

    /* compiled from: AppCompatDialog */
    class a implements d.a {
        a() {
        }

        public boolean superDispatchKeyEvent(KeyEvent keyEvent) {
            return h.this.a(keyEvent);
        }
    }

    public h(Context context, int i2) {
        super(context, a(context, i2));
        f a2 = a();
        a2.d(a(context, i2));
        a2.a((Bundle) null);
    }

    public b a(b.a aVar) {
        return null;
    }

    public void a(b bVar) {
    }

    public boolean a(int i2) {
        return a().b(i2);
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().a(view, layoutParams);
    }

    public void b(b bVar) {
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return d.a(this.f675b, getWindow().getDecorView(), this, keyEvent);
    }

    public <T extends View> T findViewById(int i2) {
        return a().a(i2);
    }

    public void invalidateOptionsMenu() {
        a().e();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        a().d();
        super.onCreate(bundle);
        a().a(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        a().i();
    }

    public void setContentView(int i2) {
        a().c(i2);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        a().a(charSequence);
    }

    public f a() {
        if (this.f674a == null) {
            this.f674a = f.a(this, this);
        }
        return this.f674a;
    }

    public void setContentView(View view) {
        a().a(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().b(view, layoutParams);
    }

    public void setTitle(int i2) {
        super.setTitle(i2);
        a().a(getContext().getString(i2));
    }

    private static int a(Context context, int i2) {
        if (i2 != 0) {
            return i2;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(b.a.a.dialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    /* access modifiers changed from: package-private */
    public boolean a(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }
}
