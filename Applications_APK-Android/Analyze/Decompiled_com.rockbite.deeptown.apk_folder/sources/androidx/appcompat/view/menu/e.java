package androidx.appcompat.view.menu;

import android.content.Context;
import android.os.IBinder;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import androidx.appcompat.view.menu.n;
import androidx.appcompat.view.menu.o;
import b.a.g;
import java.util.ArrayList;

/* compiled from: ListMenuPresenter */
public class e implements n, AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    Context f782a;

    /* renamed from: b  reason: collision with root package name */
    LayoutInflater f783b;

    /* renamed from: c  reason: collision with root package name */
    g f784c;

    /* renamed from: d  reason: collision with root package name */
    ExpandedMenuView f785d;

    /* renamed from: e  reason: collision with root package name */
    int f786e;

    /* renamed from: f  reason: collision with root package name */
    int f787f;

    /* renamed from: g  reason: collision with root package name */
    int f788g;

    /* renamed from: h  reason: collision with root package name */
    private n.a f789h;

    /* renamed from: i  reason: collision with root package name */
    a f790i;

    /* compiled from: ListMenuPresenter */
    private class a extends BaseAdapter {

        /* renamed from: a  reason: collision with root package name */
        private int f791a = -1;

        public a() {
            a();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            j f2 = e.this.f784c.f();
            if (f2 != null) {
                ArrayList<j> j2 = e.this.f784c.j();
                int size = j2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    if (j2.get(i2) == f2) {
                        this.f791a = i2;
                        return;
                    }
                }
            }
            this.f791a = -1;
        }

        public int getCount() {
            int size = e.this.f784c.j().size() - e.this.f786e;
            return this.f791a < 0 ? size : size - 1;
        }

        public long getItemId(int i2) {
            return (long) i2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i2, View view, ViewGroup viewGroup) {
            if (view == null) {
                e eVar = e.this;
                view = eVar.f783b.inflate(eVar.f788g, viewGroup, false);
            }
            ((o.a) view).a(getItem(i2), 0);
            return view;
        }

        public void notifyDataSetChanged() {
            a();
            super.notifyDataSetChanged();
        }

        public j getItem(int i2) {
            ArrayList<j> j2 = e.this.f784c.j();
            int i3 = i2 + e.this.f786e;
            int i4 = this.f791a;
            if (i4 >= 0 && i3 >= i4) {
                i3++;
            }
            return j2.get(i3);
        }
    }

    public e(Context context, int i2) {
        this(i2, 0);
        this.f782a = context;
        this.f783b = LayoutInflater.from(this.f782a);
    }

    public void a(Context context, g gVar) {
        int i2 = this.f787f;
        if (i2 != 0) {
            this.f782a = new ContextThemeWrapper(context, i2);
            this.f783b = LayoutInflater.from(this.f782a);
        } else if (this.f782a != null) {
            this.f782a = context;
            if (this.f783b == null) {
                this.f783b = LayoutInflater.from(this.f782a);
            }
        }
        this.f784c = gVar;
        a aVar = this.f790i;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    public boolean a() {
        return false;
    }

    public boolean a(g gVar, j jVar) {
        return false;
    }

    public ListAdapter b() {
        if (this.f790i == null) {
            this.f790i = new a();
        }
        return this.f790i;
    }

    public boolean b(g gVar, j jVar) {
        return false;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        this.f784c.a(this.f790i.getItem(i2), this, 0);
    }

    public e(int i2, int i3) {
        this.f788g = i2;
        this.f787f = i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public o a(ViewGroup viewGroup) {
        if (this.f785d == null) {
            this.f785d = (ExpandedMenuView) this.f783b.inflate(g.abc_expanded_menu_layout, viewGroup, false);
            if (this.f790i == null) {
                this.f790i = new a();
            }
            this.f785d.setAdapter((ListAdapter) this.f790i);
            this.f785d.setOnItemClickListener(this);
        }
        return this.f785d;
    }

    public void a(boolean z) {
        a aVar = this.f790i;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    public void a(n.a aVar) {
        this.f789h = aVar;
    }

    public boolean a(s sVar) {
        if (!sVar.hasVisibleItems()) {
            return false;
        }
        new h(sVar).a((IBinder) null);
        n.a aVar = this.f789h;
        if (aVar == null) {
            return true;
        }
        aVar.a(sVar);
        return true;
    }

    public void a(g gVar, boolean z) {
        n.a aVar = this.f789h;
        if (aVar != null) {
            aVar.a(gVar, z);
        }
    }
}
