package androidx.appcompat.view.menu;

import android.content.Context;
import android.view.MenuItem;
import android.view.SubMenu;
import b.d.a;
import b.e.g.a.b;
import java.util.Iterator;
import java.util.Map;

/* compiled from: BaseMenuWrapper */
abstract class c {

    /* renamed from: a  reason: collision with root package name */
    final Context f759a;

    /* renamed from: b  reason: collision with root package name */
    private Map<b, MenuItem> f760b;

    /* renamed from: c  reason: collision with root package name */
    private Map<b.e.g.a.c, SubMenu> f761c;

    c(Context context) {
        this.f759a = context;
    }

    /* access modifiers changed from: package-private */
    public final MenuItem a(MenuItem menuItem) {
        if (!(menuItem instanceof b)) {
            return menuItem;
        }
        b bVar = (b) menuItem;
        if (this.f760b == null) {
            this.f760b = new a();
        }
        MenuItem menuItem2 = this.f760b.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        k kVar = new k(this.f759a, bVar);
        this.f760b.put(bVar, kVar);
        return kVar;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        Map<b, MenuItem> map = this.f760b;
        if (map != null) {
            map.clear();
        }
        Map<b.e.g.a.c, SubMenu> map2 = this.f761c;
        if (map2 != null) {
            map2.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(int i2) {
        Map<b, MenuItem> map = this.f760b;
        if (map != null) {
            Iterator<b> it = map.keySet().iterator();
            while (it.hasNext()) {
                if (i2 == it.next().getItemId()) {
                    it.remove();
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final SubMenu a(SubMenu subMenu) {
        if (!(subMenu instanceof b.e.g.a.c)) {
            return subMenu;
        }
        b.e.g.a.c cVar = (b.e.g.a.c) subMenu;
        if (this.f761c == null) {
            this.f761c = new a();
        }
        SubMenu subMenu2 = this.f761c.get(cVar);
        if (subMenu2 != null) {
            return subMenu2;
        }
        t tVar = new t(this.f759a, cVar);
        this.f761c.put(cVar, tVar);
        return tVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2) {
        Map<b, MenuItem> map = this.f760b;
        if (map != null) {
            Iterator<b> it = map.keySet().iterator();
            while (it.hasNext()) {
                if (i2 == it.next().getGroupId()) {
                    it.remove();
                }
            }
        }
    }
}
