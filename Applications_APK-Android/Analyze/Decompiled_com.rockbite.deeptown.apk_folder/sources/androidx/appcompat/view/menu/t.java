package androidx.appcompat.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import b.e.g.a.c;

/* compiled from: SubMenuWrapperICS */
class t extends p implements SubMenu {

    /* renamed from: e  reason: collision with root package name */
    private final c f862e;

    t(Context context, c cVar) {
        super(context, cVar);
        this.f862e = cVar;
    }

    public void clearHeader() {
        this.f862e.clearHeader();
    }

    public MenuItem getItem() {
        return a(this.f862e.getItem());
    }

    public SubMenu setHeaderIcon(int i2) {
        this.f862e.setHeaderIcon(i2);
        return this;
    }

    public SubMenu setHeaderTitle(int i2) {
        this.f862e.setHeaderTitle(i2);
        return this;
    }

    public SubMenu setHeaderView(View view) {
        this.f862e.setHeaderView(view);
        return this;
    }

    public SubMenu setIcon(int i2) {
        this.f862e.setIcon(i2);
        return this;
    }

    public SubMenu setHeaderIcon(Drawable drawable) {
        this.f862e.setHeaderIcon(drawable);
        return this;
    }

    public SubMenu setHeaderTitle(CharSequence charSequence) {
        this.f862e.setHeaderTitle(charSequence);
        return this;
    }

    public SubMenu setIcon(Drawable drawable) {
        this.f862e.setIcon(drawable);
        return this;
    }
}
