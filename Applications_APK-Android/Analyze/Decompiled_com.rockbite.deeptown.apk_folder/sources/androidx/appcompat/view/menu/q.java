package androidx.appcompat.view.menu;

import android.widget.ListView;

/* compiled from: ShowableListMenu */
public interface q {
    ListView d();

    void dismiss();

    boolean isShowing();

    void show();
}
