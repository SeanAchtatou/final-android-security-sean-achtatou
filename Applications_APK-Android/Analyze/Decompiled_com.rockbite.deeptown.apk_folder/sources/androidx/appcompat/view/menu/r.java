package androidx.appcompat.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.view.menu.n;
import androidx.appcompat.widget.k0;
import b.a.d;
import b.a.g;
import b.e.m.u;

/* compiled from: StandardMenuPopup */
final class r extends l implements PopupWindow.OnDismissListener, AdapterView.OnItemClickListener, n, View.OnKeyListener {
    private static final int v = g.abc_popup_menu_item_layout;

    /* renamed from: b  reason: collision with root package name */
    private final Context f850b;

    /* renamed from: c  reason: collision with root package name */
    private final g f851c;

    /* renamed from: d  reason: collision with root package name */
    private final f f852d;

    /* renamed from: e  reason: collision with root package name */
    private final boolean f853e;

    /* renamed from: f  reason: collision with root package name */
    private final int f854f;

    /* renamed from: g  reason: collision with root package name */
    private final int f855g;

    /* renamed from: h  reason: collision with root package name */
    private final int f856h;

    /* renamed from: i  reason: collision with root package name */
    final k0 f857i;

    /* renamed from: j  reason: collision with root package name */
    final ViewTreeObserver.OnGlobalLayoutListener f858j = new a();

    /* renamed from: k  reason: collision with root package name */
    private final View.OnAttachStateChangeListener f859k = new b();
    private PopupWindow.OnDismissListener l;
    private View m;
    View n;
    private n.a o;
    ViewTreeObserver p;
    private boolean q;
    private boolean r;
    private int s;
    private int t = 0;
    private boolean u;

    /* compiled from: StandardMenuPopup */
    class a implements ViewTreeObserver.OnGlobalLayoutListener {
        a() {
        }

        public void onGlobalLayout() {
            if (r.this.isShowing() && !r.this.f857i.j()) {
                View view = r.this.n;
                if (view == null || !view.isShown()) {
                    r.this.dismiss();
                } else {
                    r.this.f857i.show();
                }
            }
        }
    }

    /* compiled from: StandardMenuPopup */
    class b implements View.OnAttachStateChangeListener {
        b() {
        }

        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = r.this.p;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    r.this.p = view.getViewTreeObserver();
                }
                r rVar = r.this;
                rVar.p.removeGlobalOnLayoutListener(rVar.f858j);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    public r(Context context, g gVar, View view, int i2, int i3, boolean z) {
        this.f850b = context;
        this.f851c = gVar;
        this.f853e = z;
        this.f852d = new f(gVar, LayoutInflater.from(context), this.f853e, v);
        this.f855g = i2;
        this.f856h = i3;
        Resources resources = context.getResources();
        this.f854f = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(d.abc_config_prefDialogWidth));
        this.m = view;
        this.f857i = new k0(this.f850b, null, this.f855g, this.f856h);
        gVar.a(this, context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private boolean e() {
        View view;
        if (isShowing()) {
            return true;
        }
        if (this.q || (view = this.m) == null) {
            return false;
        }
        this.n = view;
        this.f857i.a((PopupWindow.OnDismissListener) this);
        this.f857i.a((AdapterView.OnItemClickListener) this);
        this.f857i.a(true);
        View view2 = this.n;
        boolean z = this.p == null;
        this.p = view2.getViewTreeObserver();
        if (z) {
            this.p.addOnGlobalLayoutListener(this.f858j);
        }
        view2.addOnAttachStateChangeListener(this.f859k);
        this.f857i.a(view2);
        this.f857i.f(this.t);
        if (!this.r) {
            this.s = l.a(this.f852d, null, this.f850b, this.f854f);
            this.r = true;
        }
        this.f857i.e(this.s);
        this.f857i.g(2);
        this.f857i.a(c());
        this.f857i.show();
        ListView d2 = this.f857i.d();
        d2.setOnKeyListener(this);
        if (this.u && this.f851c.h() != null) {
            FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.f850b).inflate(g.abc_popup_menu_header_item_layout, (ViewGroup) d2, false);
            TextView textView = (TextView) frameLayout.findViewById(16908310);
            if (textView != null) {
                textView.setText(this.f851c.h());
            }
            frameLayout.setEnabled(false);
            d2.addHeaderView(frameLayout, null, false);
        }
        this.f857i.a((ListAdapter) this.f852d);
        this.f857i.show();
        return true;
    }

    public void a(int i2) {
        this.t = i2;
    }

    public void a(g gVar) {
    }

    public boolean a() {
        return false;
    }

    public void b(boolean z) {
        this.f852d.a(z);
    }

    public void c(int i2) {
        this.f857i.b(i2);
    }

    public ListView d() {
        return this.f857i.d();
    }

    public void dismiss() {
        if (isShowing()) {
            this.f857i.dismiss();
        }
    }

    public boolean isShowing() {
        return !this.q && this.f857i.isShowing();
    }

    public void onDismiss() {
        this.q = true;
        this.f851c.close();
        ViewTreeObserver viewTreeObserver = this.p;
        if (viewTreeObserver != null) {
            if (!viewTreeObserver.isAlive()) {
                this.p = this.n.getViewTreeObserver();
            }
            this.p.removeGlobalOnLayoutListener(this.f858j);
            this.p = null;
        }
        this.n.removeOnAttachStateChangeListener(this.f859k);
        PopupWindow.OnDismissListener onDismissListener = this.l;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    public void show() {
        if (!e()) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    public void a(boolean z) {
        this.r = false;
        f fVar = this.f852d;
        if (fVar != null) {
            fVar.notifyDataSetChanged();
        }
    }

    public void b(int i2) {
        this.f857i.a(i2);
    }

    public void c(boolean z) {
        this.u = z;
    }

    public void a(n.a aVar) {
        this.o = aVar;
    }

    public boolean a(s sVar) {
        if (sVar.hasVisibleItems()) {
            m mVar = new m(this.f850b, sVar, this.n, this.f853e, this.f855g, this.f856h);
            mVar.a(this.o);
            mVar.a(l.b(sVar));
            mVar.a(this.l);
            this.l = null;
            this.f851c.a(false);
            int a2 = this.f857i.a();
            int e2 = this.f857i.e();
            if ((Gravity.getAbsoluteGravity(this.t, u.k(this.m)) & 7) == 5) {
                a2 += this.m.getWidth();
            }
            if (mVar.a(a2, e2)) {
                n.a aVar = this.o;
                if (aVar == null) {
                    return true;
                }
                aVar.a(sVar);
                return true;
            }
        }
        return false;
    }

    public void a(g gVar, boolean z) {
        if (gVar == this.f851c) {
            dismiss();
            n.a aVar = this.o;
            if (aVar != null) {
                aVar.a(gVar, z);
            }
        }
    }

    public void a(View view) {
        this.m = view;
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.l = onDismissListener;
    }
}
