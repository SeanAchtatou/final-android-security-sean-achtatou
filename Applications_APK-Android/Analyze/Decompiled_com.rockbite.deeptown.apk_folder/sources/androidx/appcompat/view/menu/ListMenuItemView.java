package androidx.appcompat.view.menu;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.appcompat.view.menu.o;
import androidx.appcompat.widget.v0;
import b.a.a;
import b.a.f;
import b.a.g;
import b.a.j;
import b.e.m.u;

public class ListMenuItemView extends LinearLayout implements o.a, AbsListView.SelectionBoundsAdjuster {

    /* renamed from: a  reason: collision with root package name */
    private j f729a;

    /* renamed from: b  reason: collision with root package name */
    private ImageView f730b;

    /* renamed from: c  reason: collision with root package name */
    private RadioButton f731c;

    /* renamed from: d  reason: collision with root package name */
    private TextView f732d;

    /* renamed from: e  reason: collision with root package name */
    private CheckBox f733e;

    /* renamed from: f  reason: collision with root package name */
    private TextView f734f;

    /* renamed from: g  reason: collision with root package name */
    private ImageView f735g;

    /* renamed from: h  reason: collision with root package name */
    private ImageView f736h;

    /* renamed from: i  reason: collision with root package name */
    private LinearLayout f737i;

    /* renamed from: j  reason: collision with root package name */
    private Drawable f738j;

    /* renamed from: k  reason: collision with root package name */
    private int f739k;
    private Context l;
    private boolean m;
    private Drawable n;
    private boolean o;
    private LayoutInflater p;
    private boolean q;

    public ListMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.listMenuViewStyle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, androidx.appcompat.view.menu.ListMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void b() {
        this.f730b = (ImageView) getInflater().inflate(g.abc_list_menu_item_icon, (ViewGroup) this, false);
        a(this.f730b, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, androidx.appcompat.view.menu.ListMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void d() {
        this.f731c = (RadioButton) getInflater().inflate(g.abc_list_menu_item_radio, (ViewGroup) this, false);
        a(this.f731c);
    }

    private LayoutInflater getInflater() {
        if (this.p == null) {
            this.p = LayoutInflater.from(getContext());
        }
        return this.p;
    }

    private void setSubMenuArrowVisible(boolean z) {
        ImageView imageView = this.f735g;
        if (imageView != null) {
            imageView.setVisibility(z ? 0 : 8);
        }
    }

    public void a(j jVar, int i2) {
        this.f729a = jVar;
        setVisibility(jVar.isVisible() ? 0 : 8);
        setTitle(jVar.a(this));
        setCheckable(jVar.isCheckable());
        a(jVar.m(), jVar.d());
        setIcon(jVar.getIcon());
        setEnabled(jVar.isEnabled());
        setSubMenuArrowVisible(jVar.hasSubMenu());
        setContentDescription(jVar.getContentDescription());
    }

    public void adjustListItemSelectionBounds(Rect rect) {
        ImageView imageView = this.f736h;
        if (imageView != null && imageView.getVisibility() == 0) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.f736h.getLayoutParams();
            rect.top += this.f736h.getHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
        }
    }

    public boolean c() {
        return false;
    }

    public j getItemData() {
        return this.f729a;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        u.a(this, this.f738j);
        this.f732d = (TextView) findViewById(f.title);
        int i2 = this.f739k;
        if (i2 != -1) {
            this.f732d.setTextAppearance(this.l, i2);
        }
        this.f734f = (TextView) findViewById(f.shortcut);
        this.f735g = (ImageView) findViewById(f.submenuarrow);
        ImageView imageView = this.f735g;
        if (imageView != null) {
            imageView.setImageDrawable(this.n);
        }
        this.f736h = (ImageView) findViewById(f.group_divider);
        this.f737i = (LinearLayout) findViewById(f.content);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (this.f730b != null && this.m) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) this.f730b.getLayoutParams();
            if (layoutParams.height > 0 && layoutParams2.width <= 0) {
                layoutParams2.width = layoutParams.height;
            }
        }
        super.onMeasure(i2, i3);
    }

    public void setCheckable(boolean z) {
        CompoundButton compoundButton;
        CompoundButton compoundButton2;
        if (z || this.f731c != null || this.f733e != null) {
            if (this.f729a.i()) {
                if (this.f731c == null) {
                    d();
                }
                compoundButton2 = this.f731c;
                compoundButton = this.f733e;
            } else {
                if (this.f733e == null) {
                    a();
                }
                compoundButton2 = this.f733e;
                compoundButton = this.f731c;
            }
            if (z) {
                compoundButton2.setChecked(this.f729a.isChecked());
                if (compoundButton2.getVisibility() != 0) {
                    compoundButton2.setVisibility(0);
                }
                if (compoundButton != null && compoundButton.getVisibility() != 8) {
                    compoundButton.setVisibility(8);
                    return;
                }
                return;
            }
            CheckBox checkBox = this.f733e;
            if (checkBox != null) {
                checkBox.setVisibility(8);
            }
            RadioButton radioButton = this.f731c;
            if (radioButton != null) {
                radioButton.setVisibility(8);
            }
        }
    }

    public void setChecked(boolean z) {
        CompoundButton compoundButton;
        if (this.f729a.i()) {
            if (this.f731c == null) {
                d();
            }
            compoundButton = this.f731c;
        } else {
            if (this.f733e == null) {
                a();
            }
            compoundButton = this.f733e;
        }
        compoundButton.setChecked(z);
    }

    public void setForceShowIcon(boolean z) {
        this.q = z;
        this.m = z;
    }

    public void setGroupDividerEnabled(boolean z) {
        ImageView imageView = this.f736h;
        if (imageView != null) {
            imageView.setVisibility((this.o || !z) ? 8 : 0);
        }
    }

    public void setIcon(Drawable drawable) {
        boolean z = this.f729a.l() || this.q;
        if (!z && !this.m) {
            return;
        }
        if (this.f730b != null || drawable != null || this.m) {
            if (this.f730b == null) {
                b();
            }
            if (drawable != null || this.m) {
                ImageView imageView = this.f730b;
                if (!z) {
                    drawable = null;
                }
                imageView.setImageDrawable(drawable);
                if (this.f730b.getVisibility() != 0) {
                    this.f730b.setVisibility(0);
                    return;
                }
                return;
            }
            this.f730b.setVisibility(8);
        }
    }

    public void setTitle(CharSequence charSequence) {
        if (charSequence != null) {
            this.f732d.setText(charSequence);
            if (this.f732d.getVisibility() != 0) {
                this.f732d.setVisibility(0);
            }
        } else if (this.f732d.getVisibility() != 8) {
            this.f732d.setVisibility(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.v0.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      androidx.appcompat.widget.v0.a(int, float):float
      androidx.appcompat.widget.v0.a(int, int):int
      androidx.appcompat.widget.v0.a(int, boolean):boolean */
    public ListMenuItemView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        v0 a2 = v0.a(getContext(), attributeSet, j.MenuView, i2, 0);
        this.f738j = a2.b(j.MenuView_android_itemBackground);
        this.f739k = a2.g(j.MenuView_android_itemTextAppearance, -1);
        this.m = a2.a(j.MenuView_preserveIconSpacing, false);
        this.l = context;
        this.n = a2.b(j.MenuView_subMenuArrow);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(null, new int[]{16843049}, a.dropDownListViewStyle, 0);
        this.o = obtainStyledAttributes.hasValue(0);
        a2.a();
        obtainStyledAttributes.recycle();
    }

    private void a(View view) {
        a(view, -1);
    }

    private void a(View view, int i2) {
        LinearLayout linearLayout = this.f737i;
        if (linearLayout != null) {
            linearLayout.addView(view, i2);
        } else {
            addView(view, i2);
        }
    }

    public void a(boolean z, char c2) {
        int i2 = (!z || !this.f729a.m()) ? 8 : 0;
        if (i2 == 0) {
            this.f734f.setText(this.f729a.e());
        }
        if (this.f734f.getVisibility() != i2) {
            this.f734f.setVisibility(i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, androidx.appcompat.view.menu.ListMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a() {
        this.f733e = (CheckBox) getInflater().inflate(g.abc_list_menu_item_checkbox, (ViewGroup) this, false);
        a(this.f733e);
    }
}
