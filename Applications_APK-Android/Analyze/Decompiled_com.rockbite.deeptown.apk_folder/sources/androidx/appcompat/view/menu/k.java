package androidx.appcompat.view.menu;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.ActionProvider;
import android.view.CollapsibleActionView;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import b.e.m.b;
import java.lang.reflect.Method;

/* compiled from: MenuItemWrapperICS */
public class k extends c implements MenuItem {

    /* renamed from: d  reason: collision with root package name */
    private final b.e.g.a.b f826d;

    /* renamed from: e  reason: collision with root package name */
    private Method f827e;

    /* compiled from: MenuItemWrapperICS */
    private class a extends b.e.m.b {

        /* renamed from: b  reason: collision with root package name */
        final ActionProvider f828b;

        a(Context context, ActionProvider actionProvider) {
            super(context);
            this.f828b = actionProvider;
        }

        public boolean a() {
            return this.f828b.hasSubMenu();
        }

        public View c() {
            return this.f828b.onCreateActionView();
        }

        public boolean d() {
            return this.f828b.onPerformDefaultAction();
        }

        public void a(SubMenu subMenu) {
            this.f828b.onPrepareSubMenu(k.this.a(subMenu));
        }
    }

    /* compiled from: MenuItemWrapperICS */
    private class b extends a implements ActionProvider.VisibilityListener {

        /* renamed from: d  reason: collision with root package name */
        private b.C0052b f830d;

        b(k kVar, Context context, ActionProvider actionProvider) {
            super(context, actionProvider);
        }

        public View a(MenuItem menuItem) {
            return this.f828b.onCreateActionView(menuItem);
        }

        public boolean b() {
            return this.f828b.isVisible();
        }

        public boolean e() {
            return this.f828b.overridesItemVisibility();
        }

        public void onActionProviderVisibilityChanged(boolean z) {
            b.C0052b bVar = this.f830d;
            if (bVar != null) {
                bVar.onActionProviderVisibilityChanged(z);
            }
        }

        public void a(b.C0052b bVar) {
            this.f830d = bVar;
            this.f828b.setVisibilityListener(bVar != null ? this : null);
        }
    }

    /* compiled from: MenuItemWrapperICS */
    static class c extends FrameLayout implements b.a.n.c {

        /* renamed from: a  reason: collision with root package name */
        final CollapsibleActionView f831a;

        c(View view) {
            super(view.getContext());
            this.f831a = (CollapsibleActionView) view;
            addView(view);
        }

        public void a() {
            this.f831a.onActionViewExpanded();
        }

        public void b() {
            this.f831a.onActionViewCollapsed();
        }

        /* access modifiers changed from: package-private */
        public View c() {
            return (View) this.f831a;
        }
    }

    /* compiled from: MenuItemWrapperICS */
    private class d implements MenuItem.OnActionExpandListener {

        /* renamed from: a  reason: collision with root package name */
        private final MenuItem.OnActionExpandListener f832a;

        d(MenuItem.OnActionExpandListener onActionExpandListener) {
            this.f832a = onActionExpandListener;
        }

        public boolean onMenuItemActionCollapse(MenuItem menuItem) {
            return this.f832a.onMenuItemActionCollapse(k.this.a(menuItem));
        }

        public boolean onMenuItemActionExpand(MenuItem menuItem) {
            return this.f832a.onMenuItemActionExpand(k.this.a(menuItem));
        }
    }

    /* compiled from: MenuItemWrapperICS */
    private class e implements MenuItem.OnMenuItemClickListener {

        /* renamed from: a  reason: collision with root package name */
        private final MenuItem.OnMenuItemClickListener f834a;

        e(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
            this.f834a = onMenuItemClickListener;
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            return this.f834a.onMenuItemClick(k.this.a(menuItem));
        }
    }

    public k(Context context, b.e.g.a.b bVar) {
        super(context);
        if (bVar != null) {
            this.f826d = bVar;
            return;
        }
        throw new IllegalArgumentException("Wrapped Object can not be null.");
    }

    public void a(boolean z) {
        try {
            if (this.f827e == null) {
                this.f827e = this.f826d.getClass().getDeclaredMethod("setExclusiveCheckable", Boolean.TYPE);
            }
            this.f827e.invoke(this.f826d, Boolean.valueOf(z));
        } catch (Exception e2) {
            Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e2);
        }
    }

    public boolean collapseActionView() {
        return this.f826d.collapseActionView();
    }

    public boolean expandActionView() {
        return this.f826d.expandActionView();
    }

    public ActionProvider getActionProvider() {
        b.e.m.b a2 = this.f826d.a();
        if (a2 instanceof a) {
            return ((a) a2).f828b;
        }
        return null;
    }

    public View getActionView() {
        View actionView = this.f826d.getActionView();
        return actionView instanceof c ? ((c) actionView).c() : actionView;
    }

    public int getAlphabeticModifiers() {
        return this.f826d.getAlphabeticModifiers();
    }

    public char getAlphabeticShortcut() {
        return this.f826d.getAlphabeticShortcut();
    }

    public CharSequence getContentDescription() {
        return this.f826d.getContentDescription();
    }

    public int getGroupId() {
        return this.f826d.getGroupId();
    }

    public Drawable getIcon() {
        return this.f826d.getIcon();
    }

    public ColorStateList getIconTintList() {
        return this.f826d.getIconTintList();
    }

    public PorterDuff.Mode getIconTintMode() {
        return this.f826d.getIconTintMode();
    }

    public Intent getIntent() {
        return this.f826d.getIntent();
    }

    public int getItemId() {
        return this.f826d.getItemId();
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.f826d.getMenuInfo();
    }

    public int getNumericModifiers() {
        return this.f826d.getNumericModifiers();
    }

    public char getNumericShortcut() {
        return this.f826d.getNumericShortcut();
    }

    public int getOrder() {
        return this.f826d.getOrder();
    }

    public SubMenu getSubMenu() {
        return a(this.f826d.getSubMenu());
    }

    public CharSequence getTitle() {
        return this.f826d.getTitle();
    }

    public CharSequence getTitleCondensed() {
        return this.f826d.getTitleCondensed();
    }

    public CharSequence getTooltipText() {
        return this.f826d.getTooltipText();
    }

    public boolean hasSubMenu() {
        return this.f826d.hasSubMenu();
    }

    public boolean isActionViewExpanded() {
        return this.f826d.isActionViewExpanded();
    }

    public boolean isCheckable() {
        return this.f826d.isCheckable();
    }

    public boolean isChecked() {
        return this.f826d.isChecked();
    }

    public boolean isEnabled() {
        return this.f826d.isEnabled();
    }

    public boolean isVisible() {
        return this.f826d.isVisible();
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        b.e.m.b bVar;
        if (Build.VERSION.SDK_INT >= 16) {
            bVar = new b(this, this.f759a, actionProvider);
        } else {
            bVar = new a(this.f759a, actionProvider);
        }
        b.e.g.a.b bVar2 = this.f826d;
        if (actionProvider == null) {
            bVar = null;
        }
        bVar2.a(bVar);
        return this;
    }

    public MenuItem setActionView(View view) {
        if (view instanceof CollapsibleActionView) {
            view = new c(view);
        }
        this.f826d.setActionView(view);
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        this.f826d.setAlphabeticShortcut(c2);
        return this;
    }

    public MenuItem setCheckable(boolean z) {
        this.f826d.setCheckable(z);
        return this;
    }

    public MenuItem setChecked(boolean z) {
        this.f826d.setChecked(z);
        return this;
    }

    public MenuItem setContentDescription(CharSequence charSequence) {
        this.f826d.setContentDescription(charSequence);
        return this;
    }

    public MenuItem setEnabled(boolean z) {
        this.f826d.setEnabled(z);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.f826d.setIcon(drawable);
        return this;
    }

    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.f826d.setIconTintList(colorStateList);
        return this;
    }

    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.f826d.setIconTintMode(mode);
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        this.f826d.setIntent(intent);
        return this;
    }

    public MenuItem setNumericShortcut(char c2) {
        this.f826d.setNumericShortcut(c2);
        return this;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        this.f826d.setOnActionExpandListener(onActionExpandListener != null ? new d(onActionExpandListener) : null);
        return this;
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.f826d.setOnMenuItemClickListener(onMenuItemClickListener != null ? new e(onMenuItemClickListener) : null);
        return this;
    }

    public MenuItem setShortcut(char c2, char c3) {
        this.f826d.setShortcut(c2, c3);
        return this;
    }

    public void setShowAsAction(int i2) {
        this.f826d.setShowAsAction(i2);
    }

    public MenuItem setShowAsActionFlags(int i2) {
        this.f826d.setShowAsActionFlags(i2);
        return this;
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.f826d.setTitle(charSequence);
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f826d.setTitleCondensed(charSequence);
        return this;
    }

    public MenuItem setTooltipText(CharSequence charSequence) {
        this.f826d.setTooltipText(charSequence);
        return this;
    }

    public MenuItem setVisible(boolean z) {
        return this.f826d.setVisible(z);
    }

    public MenuItem setAlphabeticShortcut(char c2, int i2) {
        this.f826d.setAlphabeticShortcut(c2, i2);
        return this;
    }

    public MenuItem setIcon(int i2) {
        this.f826d.setIcon(i2);
        return this;
    }

    public MenuItem setNumericShortcut(char c2, int i2) {
        this.f826d.setNumericShortcut(c2, i2);
        return this;
    }

    public MenuItem setShortcut(char c2, char c3, int i2, int i3) {
        this.f826d.setShortcut(c2, c3, i2, i3);
        return this;
    }

    public MenuItem setTitle(int i2) {
        this.f826d.setTitle(i2);
        return this;
    }

    public MenuItem setActionView(int i2) {
        this.f826d.setActionView(i2);
        View actionView = this.f826d.getActionView();
        if (actionView instanceof CollapsibleActionView) {
            this.f826d.setActionView(new c(actionView));
        }
        return this;
    }
}
