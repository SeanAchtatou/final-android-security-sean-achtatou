package androidx.appcompat.view.menu;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import androidx.appcompat.view.menu.o;
import java.util.ArrayList;

/* compiled from: MenuAdapter */
public class f extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    g f793a;

    /* renamed from: b  reason: collision with root package name */
    private int f794b = -1;

    /* renamed from: c  reason: collision with root package name */
    private boolean f795c;

    /* renamed from: d  reason: collision with root package name */
    private final boolean f796d;

    /* renamed from: e  reason: collision with root package name */
    private final LayoutInflater f797e;

    /* renamed from: f  reason: collision with root package name */
    private final int f798f;

    public f(g gVar, LayoutInflater layoutInflater, boolean z, int i2) {
        this.f796d = z;
        this.f797e = layoutInflater;
        this.f793a = gVar;
        this.f798f = i2;
        a();
    }

    public void a(boolean z) {
        this.f795c = z;
    }

    public g b() {
        return this.f793a;
    }

    public int getCount() {
        ArrayList<j> j2 = this.f796d ? this.f793a.j() : this.f793a.n();
        if (this.f794b < 0) {
            return j2.size();
        }
        return j2.size() - 1;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.f797e.inflate(this.f798f, viewGroup, false);
        }
        int groupId = getItem(i2).getGroupId();
        int i3 = i2 - 1;
        ListMenuItemView listMenuItemView = (ListMenuItemView) view;
        listMenuItemView.setGroupDividerEnabled(this.f793a.o() && groupId != (i3 >= 0 ? getItem(i3).getGroupId() : groupId));
        o.a aVar = (o.a) view;
        if (this.f795c) {
            listMenuItemView.setForceShowIcon(true);
        }
        aVar.a(getItem(i2), 0);
        return view;
    }

    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        j f2 = this.f793a.f();
        if (f2 != null) {
            ArrayList<j> j2 = this.f793a.j();
            int size = j2.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (j2.get(i2) == f2) {
                    this.f794b = i2;
                    return;
                }
            }
        }
        this.f794b = -1;
    }

    public j getItem(int i2) {
        ArrayList<j> j2 = this.f796d ? this.f793a.j() : this.f793a.n();
        int i3 = this.f794b;
        if (i3 >= 0 && i2 >= i3) {
            i2++;
        }
        return (j) j2.get(i2);
    }
}
