package androidx.appcompat.view.menu;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import b.e.g.a.b;
import com.google.protobuf.CodedOutputStream;

/* compiled from: ActionMenuItem */
public class a implements b {

    /* renamed from: a  reason: collision with root package name */
    private final int f740a;

    /* renamed from: b  reason: collision with root package name */
    private final int f741b;

    /* renamed from: c  reason: collision with root package name */
    private final int f742c;

    /* renamed from: d  reason: collision with root package name */
    private CharSequence f743d;

    /* renamed from: e  reason: collision with root package name */
    private CharSequence f744e;

    /* renamed from: f  reason: collision with root package name */
    private Intent f745f;

    /* renamed from: g  reason: collision with root package name */
    private char f746g;

    /* renamed from: h  reason: collision with root package name */
    private int f747h = CodedOutputStream.DEFAULT_BUFFER_SIZE;

    /* renamed from: i  reason: collision with root package name */
    private char f748i;

    /* renamed from: j  reason: collision with root package name */
    private int f749j = CodedOutputStream.DEFAULT_BUFFER_SIZE;

    /* renamed from: k  reason: collision with root package name */
    private Drawable f750k;
    private Context l;
    private CharSequence m;
    private CharSequence n;
    private ColorStateList o = null;
    private PorterDuff.Mode p = null;
    private boolean q = false;
    private boolean r = false;
    private int s = 16;

    public a(Context context, int i2, int i3, int i4, int i5, CharSequence charSequence) {
        this.l = context;
        this.f740a = i3;
        this.f741b = i2;
        this.f742c = i5;
        this.f743d = charSequence;
    }

    private void b() {
        if (this.f750k == null) {
            return;
        }
        if (this.q || this.r) {
            this.f750k = androidx.core.graphics.drawable.a.g(this.f750k);
            this.f750k = this.f750k.mutate();
            if (this.q) {
                androidx.core.graphics.drawable.a.a(this.f750k, this.o);
            }
            if (this.r) {
                androidx.core.graphics.drawable.a.a(this.f750k, this.p);
            }
        }
    }

    public b a(b.e.m.b bVar) {
        throw new UnsupportedOperationException();
    }

    public b.e.m.b a() {
        return null;
    }

    public boolean collapseActionView() {
        return false;
    }

    public boolean expandActionView() {
        return false;
    }

    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException();
    }

    public View getActionView() {
        return null;
    }

    public int getAlphabeticModifiers() {
        return this.f749j;
    }

    public char getAlphabeticShortcut() {
        return this.f748i;
    }

    public CharSequence getContentDescription() {
        return this.m;
    }

    public int getGroupId() {
        return this.f741b;
    }

    public Drawable getIcon() {
        return this.f750k;
    }

    public ColorStateList getIconTintList() {
        return this.o;
    }

    public PorterDuff.Mode getIconTintMode() {
        return this.p;
    }

    public Intent getIntent() {
        return this.f745f;
    }

    public int getItemId() {
        return this.f740a;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return null;
    }

    public int getNumericModifiers() {
        return this.f747h;
    }

    public char getNumericShortcut() {
        return this.f746g;
    }

    public int getOrder() {
        return this.f742c;
    }

    public SubMenu getSubMenu() {
        return null;
    }

    public CharSequence getTitle() {
        return this.f743d;
    }

    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.f744e;
        return charSequence != null ? charSequence : this.f743d;
    }

    public CharSequence getTooltipText() {
        return this.n;
    }

    public boolean hasSubMenu() {
        return false;
    }

    public boolean isActionViewExpanded() {
        return false;
    }

    public boolean isCheckable() {
        return (this.s & 1) != 0;
    }

    public boolean isChecked() {
        return (this.s & 2) != 0;
    }

    public boolean isEnabled() {
        return (this.s & 16) != 0;
    }

    public boolean isVisible() {
        return (this.s & 8) == 0;
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException();
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        this.f748i = Character.toLowerCase(c2);
        return this;
    }

    public MenuItem setCheckable(boolean z) {
        this.s = z | (this.s & true) ? 1 : 0;
        return this;
    }

    public MenuItem setChecked(boolean z) {
        this.s = (z ? 2 : 0) | (this.s & -3);
        return this;
    }

    public MenuItem setEnabled(boolean z) {
        this.s = (z ? 16 : 0) | (this.s & -17);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.f750k = drawable;
        b();
        return this;
    }

    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.o = colorStateList;
        this.q = true;
        b();
        return this;
    }

    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.p = mode;
        this.r = true;
        b();
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        this.f745f = intent;
        return this;
    }

    public MenuItem setNumericShortcut(char c2) {
        this.f746g = c2;
        return this;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException();
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        return this;
    }

    public MenuItem setShortcut(char c2, char c3) {
        this.f746g = c2;
        this.f748i = Character.toLowerCase(c3);
        return this;
    }

    public void setShowAsAction(int i2) {
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.f743d = charSequence;
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f744e = charSequence;
        return this;
    }

    public MenuItem setVisible(boolean z) {
        int i2 = 8;
        int i3 = this.s & 8;
        if (z) {
            i2 = 0;
        }
        this.s = i3 | i2;
        return this;
    }

    public MenuItem setAlphabeticShortcut(char c2, int i2) {
        this.f748i = Character.toLowerCase(c2);
        this.f749j = KeyEvent.normalizeMetaState(i2);
        return this;
    }

    public b setContentDescription(CharSequence charSequence) {
        this.m = charSequence;
        return this;
    }

    public MenuItem setNumericShortcut(char c2, int i2) {
        this.f746g = c2;
        this.f747h = KeyEvent.normalizeMetaState(i2);
        return this;
    }

    public b setShowAsActionFlags(int i2) {
        setShowAsAction(i2);
        return this;
    }

    public MenuItem setTitle(int i2) {
        this.f743d = this.l.getResources().getString(i2);
        return this;
    }

    public b setTooltipText(CharSequence charSequence) {
        this.n = charSequence;
        return this;
    }

    public b setActionView(View view) {
        throw new UnsupportedOperationException();
    }

    public MenuItem setIcon(int i2) {
        this.f750k = b.e.e.a.c(this.l, i2);
        b();
        return this;
    }

    public MenuItem setShortcut(char c2, char c3, int i2, int i3) {
        this.f746g = c2;
        this.f747h = KeyEvent.normalizeMetaState(i2);
        this.f748i = Character.toLowerCase(c3);
        this.f749j = KeyEvent.normalizeMetaState(i3);
        return this;
    }

    public b setActionView(int i2) {
        throw new UnsupportedOperationException();
    }
}
