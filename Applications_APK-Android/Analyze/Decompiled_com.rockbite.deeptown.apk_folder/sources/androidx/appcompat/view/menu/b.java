package androidx.appcompat.view.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.view.menu.n;
import androidx.appcompat.view.menu.o;
import java.util.ArrayList;

/* compiled from: BaseMenuPresenter */
public abstract class b implements n {

    /* renamed from: a  reason: collision with root package name */
    protected Context f751a;

    /* renamed from: b  reason: collision with root package name */
    protected Context f752b;

    /* renamed from: c  reason: collision with root package name */
    protected g f753c;

    /* renamed from: d  reason: collision with root package name */
    protected LayoutInflater f754d;

    /* renamed from: e  reason: collision with root package name */
    private n.a f755e;

    /* renamed from: f  reason: collision with root package name */
    private int f756f;

    /* renamed from: g  reason: collision with root package name */
    private int f757g;

    /* renamed from: h  reason: collision with root package name */
    protected o f758h;

    public b(Context context, int i2, int i3) {
        this.f751a = context;
        this.f754d = LayoutInflater.from(context);
        this.f756f = i2;
        this.f757g = i3;
    }

    public void a(int i2) {
    }

    public void a(Context context, g gVar) {
        this.f752b = context;
        LayoutInflater.from(this.f752b);
        this.f753c = gVar;
    }

    public abstract void a(j jVar, o.a aVar);

    public abstract boolean a(int i2, j jVar);

    public boolean a(g gVar, j jVar) {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public o b(ViewGroup viewGroup) {
        if (this.f758h == null) {
            this.f758h = (o) this.f754d.inflate(this.f756f, viewGroup, false);
            this.f758h.a(this.f753c);
            a(true);
        }
        return this.f758h;
    }

    public boolean b(g gVar, j jVar) {
        return false;
    }

    public void a(boolean z) {
        ViewGroup viewGroup = (ViewGroup) this.f758h;
        if (viewGroup != null) {
            g gVar = this.f753c;
            int i2 = 0;
            if (gVar != null) {
                gVar.b();
                ArrayList<j> n = this.f753c.n();
                int size = n.size();
                int i3 = 0;
                for (int i4 = 0; i4 < size; i4++) {
                    j jVar = n.get(i4);
                    if (a(i3, jVar)) {
                        View childAt = viewGroup.getChildAt(i3);
                        j itemData = childAt instanceof o.a ? ((o.a) childAt).getItemData() : null;
                        View a2 = a(jVar, childAt, viewGroup);
                        if (jVar != itemData) {
                            a2.setPressed(false);
                            a2.jumpDrawablesToCurrentState();
                        }
                        if (a2 != childAt) {
                            a(a2, i3);
                        }
                        i3++;
                    }
                }
                i2 = i3;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!a(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    public n.a b() {
        return this.f755e;
    }

    /* access modifiers changed from: protected */
    public void a(View view, int i2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.f758h).addView(view, i2);
    }

    /* access modifiers changed from: protected */
    public boolean a(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    public void a(n.a aVar) {
        this.f755e = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public o.a a(ViewGroup viewGroup) {
        return (o.a) this.f754d.inflate(this.f757g, viewGroup, false);
    }

    public View a(j jVar, View view, ViewGroup viewGroup) {
        o.a aVar;
        if (view instanceof o.a) {
            aVar = (o.a) view;
        } else {
            aVar = a(viewGroup);
        }
        a(jVar, aVar);
        return (View) aVar;
    }

    public void a(g gVar, boolean z) {
        n.a aVar = this.f755e;
        if (aVar != null) {
            aVar.a(gVar, z);
        }
    }

    public boolean a(s sVar) {
        n.a aVar = this.f755e;
        if (aVar != null) {
            return aVar.a(sVar);
        }
        return false;
    }
}
