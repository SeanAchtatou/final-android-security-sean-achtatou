package androidx.appcompat.view.menu;

import android.content.DialogInterface;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import androidx.appcompat.app.c;
import androidx.appcompat.view.menu.n;
import b.a.g;
import com.google.android.gms.drive.MetadataChangeSet;

/* compiled from: MenuDialogHelper */
class h implements DialogInterface.OnKeyListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener, n.a {

    /* renamed from: a  reason: collision with root package name */
    private g f810a;

    /* renamed from: b  reason: collision with root package name */
    private c f811b;

    /* renamed from: c  reason: collision with root package name */
    e f812c;

    /* renamed from: d  reason: collision with root package name */
    private n.a f813d;

    public h(g gVar) {
        this.f810a = gVar;
    }

    public void a(IBinder iBinder) {
        g gVar = this.f810a;
        c.a aVar = new c.a(gVar.e());
        this.f812c = new e(aVar.b(), g.abc_list_menu_item_layout);
        this.f812c.a(this);
        this.f810a.a(this.f812c);
        aVar.a(this.f812c.b(), this);
        View i2 = gVar.i();
        if (i2 != null) {
            aVar.a(i2);
        } else {
            aVar.a(gVar.g());
            aVar.a(gVar.h());
        }
        aVar.a(this);
        this.f811b = aVar.a();
        this.f811b.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.f811b.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= MetadataChangeSet.INDEXABLE_TEXT_SIZE_LIMIT_BYTES;
        this.f811b.show();
    }

    public void onClick(DialogInterface dialogInterface, int i2) {
        this.f810a.a((j) this.f812c.b().getItem(i2), 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.view.menu.e.a(androidx.appcompat.view.menu.g, boolean):void
     arg types: [androidx.appcompat.view.menu.g, int]
     candidates:
      androidx.appcompat.view.menu.e.a(android.content.Context, androidx.appcompat.view.menu.g):void
      androidx.appcompat.view.menu.e.a(androidx.appcompat.view.menu.g, androidx.appcompat.view.menu.j):boolean
      androidx.appcompat.view.menu.n.a(android.content.Context, androidx.appcompat.view.menu.g):void
      androidx.appcompat.view.menu.n.a(androidx.appcompat.view.menu.g, androidx.appcompat.view.menu.j):boolean
      androidx.appcompat.view.menu.e.a(androidx.appcompat.view.menu.g, boolean):void */
    public void onDismiss(DialogInterface dialogInterface) {
        this.f812c.a(this.f810a, true);
    }

    public boolean onKey(DialogInterface dialogInterface, int i2, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i2 == 82 || i2 == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.f811b.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.f811b.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.f810a.a(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.f810a.performShortcut(i2, keyEvent, 0);
    }

    public void a() {
        c cVar = this.f811b;
        if (cVar != null) {
            cVar.dismiss();
        }
    }

    public void a(g gVar, boolean z) {
        if (z || gVar == this.f810a) {
            a();
        }
        n.a aVar = this.f813d;
        if (aVar != null) {
            aVar.a(gVar, z);
        }
    }

    public boolean a(g gVar) {
        n.a aVar = this.f813d;
        if (aVar != null) {
            return aVar.a(gVar);
        }
        return false;
    }
}
