package androidx.appcompat.view.menu;

import android.content.Context;

/* compiled from: MenuPresenter */
public interface n {

    /* compiled from: MenuPresenter */
    public interface a {
        void a(g gVar, boolean z);

        boolean a(g gVar);
    }

    void a(Context context, g gVar);

    void a(g gVar, boolean z);

    void a(a aVar);

    void a(boolean z);

    boolean a();

    boolean a(g gVar, j jVar);

    boolean a(s sVar);

    boolean b(g gVar, j jVar);
}
