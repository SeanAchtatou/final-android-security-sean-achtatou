package androidx.appcompat.view.menu;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import android.widget.PopupWindow;
import androidx.appcompat.view.menu.n;
import b.e.m.c;
import b.e.m.u;

/* compiled from: MenuPopupHelper */
public class m implements i {

    /* renamed from: a  reason: collision with root package name */
    private final Context f837a;

    /* renamed from: b  reason: collision with root package name */
    private final g f838b;

    /* renamed from: c  reason: collision with root package name */
    private final boolean f839c;

    /* renamed from: d  reason: collision with root package name */
    private final int f840d;

    /* renamed from: e  reason: collision with root package name */
    private final int f841e;

    /* renamed from: f  reason: collision with root package name */
    private View f842f;

    /* renamed from: g  reason: collision with root package name */
    private int f843g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f844h;

    /* renamed from: i  reason: collision with root package name */
    private n.a f845i;

    /* renamed from: j  reason: collision with root package name */
    private l f846j;

    /* renamed from: k  reason: collision with root package name */
    private PopupWindow.OnDismissListener f847k;
    private final PopupWindow.OnDismissListener l;

    /* compiled from: MenuPopupHelper */
    class a implements PopupWindow.OnDismissListener {
        a() {
        }

        public void onDismiss() {
            m.this.d();
        }
    }

    public m(Context context, g gVar, View view, boolean z, int i2) {
        this(context, gVar, view, z, i2, 0);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v7, types: [androidx.appcompat.view.menu.l, androidx.appcompat.view.menu.n] */
    /* JADX WARN: Type inference failed for: r7v1, types: [androidx.appcompat.view.menu.r] */
    /* JADX WARN: Type inference failed for: r1v13, types: [androidx.appcompat.view.menu.d] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    private androidx.appcompat.view.menu.l g() {
        /*
            r14 = this;
            android.content.Context r0 = r14.f837a
            java.lang.String r1 = "window"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.view.WindowManager r0 = (android.view.WindowManager) r0
            android.view.Display r0 = r0.getDefaultDisplay()
            android.graphics.Point r1 = new android.graphics.Point
            r1.<init>()
            int r2 = android.os.Build.VERSION.SDK_INT
            r3 = 17
            if (r2 < r3) goto L_0x001d
            r0.getRealSize(r1)
            goto L_0x0020
        L_0x001d:
            r0.getSize(r1)
        L_0x0020:
            int r0 = r1.x
            int r1 = r1.y
            int r0 = java.lang.Math.min(r0, r1)
            android.content.Context r1 = r14.f837a
            android.content.res.Resources r1 = r1.getResources()
            int r2 = b.a.d.abc_cascading_menus_min_smallest_width
            int r1 = r1.getDimensionPixelSize(r2)
            if (r0 < r1) goto L_0x0038
            r0 = 1
            goto L_0x0039
        L_0x0038:
            r0 = 0
        L_0x0039:
            if (r0 == 0) goto L_0x004c
            androidx.appcompat.view.menu.d r0 = new androidx.appcompat.view.menu.d
            android.content.Context r2 = r14.f837a
            android.view.View r3 = r14.f842f
            int r4 = r14.f840d
            int r5 = r14.f841e
            boolean r6 = r14.f839c
            r1 = r0
            r1.<init>(r2, r3, r4, r5, r6)
            goto L_0x005e
        L_0x004c:
            androidx.appcompat.view.menu.r r0 = new androidx.appcompat.view.menu.r
            android.content.Context r8 = r14.f837a
            androidx.appcompat.view.menu.g r9 = r14.f838b
            android.view.View r10 = r14.f842f
            int r11 = r14.f840d
            int r12 = r14.f841e
            boolean r13 = r14.f839c
            r7 = r0
            r7.<init>(r8, r9, r10, r11, r12, r13)
        L_0x005e:
            androidx.appcompat.view.menu.g r1 = r14.f838b
            r0.a(r1)
            android.widget.PopupWindow$OnDismissListener r1 = r14.l
            r0.a(r1)
            android.view.View r1 = r14.f842f
            r0.a(r1)
            androidx.appcompat.view.menu.n$a r1 = r14.f845i
            r0.a(r1)
            boolean r1 = r14.f844h
            r0.b(r1)
            int r1 = r14.f843g
            r0.a(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.view.menu.m.g():androidx.appcompat.view.menu.l");
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.f847k = onDismissListener;
    }

    public l b() {
        if (this.f846j == null) {
            this.f846j = g();
        }
        return this.f846j;
    }

    public boolean c() {
        l lVar = this.f846j;
        return lVar != null && lVar.isShowing();
    }

    /* access modifiers changed from: protected */
    public void d() {
        this.f846j = null;
        PopupWindow.OnDismissListener onDismissListener = this.f847k;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    public void e() {
        if (!f()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    public boolean f() {
        if (c()) {
            return true;
        }
        if (this.f842f == null) {
            return false;
        }
        a(0, 0, false, false);
        return true;
    }

    public m(Context context, g gVar, View view, boolean z, int i2, int i3) {
        this.f843g = 8388611;
        this.l = new a();
        this.f837a = context;
        this.f838b = gVar;
        this.f842f = view;
        this.f839c = z;
        this.f840d = i2;
        this.f841e = i3;
    }

    public void a(View view) {
        this.f842f = view;
    }

    public void a(boolean z) {
        this.f844h = z;
        l lVar = this.f846j;
        if (lVar != null) {
            lVar.b(z);
        }
    }

    public void a(int i2) {
        this.f843g = i2;
    }

    public boolean a(int i2, int i3) {
        if (c()) {
            return true;
        }
        if (this.f842f == null) {
            return false;
        }
        a(i2, i3, true, true);
        return true;
    }

    private void a(int i2, int i3, boolean z, boolean z2) {
        l b2 = b();
        b2.c(z2);
        if (z) {
            if ((c.a(this.f843g, u.k(this.f842f)) & 7) == 5) {
                i2 -= this.f842f.getWidth();
            }
            b2.b(i2);
            b2.c(i3);
            int i4 = (int) ((this.f837a.getResources().getDisplayMetrics().density * 48.0f) / 2.0f);
            b2.a(new Rect(i2 - i4, i3 - i4, i2 + i4, i3 + i4));
        }
        b2.show();
    }

    public void a() {
        if (c()) {
            this.f846j.dismiss();
        }
    }

    public void a(n.a aVar) {
        this.f845i = aVar;
        l lVar = this.f846j;
        if (lVar != null) {
            lVar.a(aVar);
        }
    }
}
