package androidx.appcompat.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.view.menu.n;
import androidx.appcompat.widget.j0;
import androidx.appcompat.widget.k0;
import b.a.g;
import b.e.m.u;
import java.util.ArrayList;
import java.util.List;

/* compiled from: CascadingMenuPopup */
final class d extends l implements n, View.OnKeyListener, PopupWindow.OnDismissListener {
    private static final int B = g.abc_cascading_menu_item_layout;
    boolean A;

    /* renamed from: b  reason: collision with root package name */
    private final Context f762b;

    /* renamed from: c  reason: collision with root package name */
    private final int f763c;

    /* renamed from: d  reason: collision with root package name */
    private final int f764d;

    /* renamed from: e  reason: collision with root package name */
    private final int f765e;

    /* renamed from: f  reason: collision with root package name */
    private final boolean f766f;

    /* renamed from: g  reason: collision with root package name */
    final Handler f767g;

    /* renamed from: h  reason: collision with root package name */
    private final List<g> f768h = new ArrayList();

    /* renamed from: i  reason: collision with root package name */
    final List<C0006d> f769i = new ArrayList();

    /* renamed from: j  reason: collision with root package name */
    final ViewTreeObserver.OnGlobalLayoutListener f770j = new a();

    /* renamed from: k  reason: collision with root package name */
    private final View.OnAttachStateChangeListener f771k = new b();
    private final j0 l = new c();
    private int m = 0;
    private int n = 0;
    private View o;
    View p;
    private int q;
    private boolean r;
    private boolean s;
    private int t;
    private int u;
    private boolean v;
    private boolean w;
    private n.a x;
    ViewTreeObserver y;
    private PopupWindow.OnDismissListener z;

    /* compiled from: CascadingMenuPopup */
    class a implements ViewTreeObserver.OnGlobalLayoutListener {
        a() {
        }

        public void onGlobalLayout() {
            if (d.this.isShowing() && d.this.f769i.size() > 0 && !d.this.f769i.get(0).f779a.j()) {
                View view = d.this.p;
                if (view == null || !view.isShown()) {
                    d.this.dismiss();
                    return;
                }
                for (C0006d dVar : d.this.f769i) {
                    dVar.f779a.show();
                }
            }
        }
    }

    /* compiled from: CascadingMenuPopup */
    class b implements View.OnAttachStateChangeListener {
        b() {
        }

        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = d.this.y;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    d.this.y = view.getViewTreeObserver();
                }
                d dVar = d.this;
                dVar.y.removeGlobalOnLayoutListener(dVar.f770j);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    /* compiled from: CascadingMenuPopup */
    class c implements j0 {

        /* compiled from: CascadingMenuPopup */
        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ C0006d f775a;

            /* renamed from: b  reason: collision with root package name */
            final /* synthetic */ MenuItem f776b;

            /* renamed from: c  reason: collision with root package name */
            final /* synthetic */ g f777c;

            a(C0006d dVar, MenuItem menuItem, g gVar) {
                this.f775a = dVar;
                this.f776b = menuItem;
                this.f777c = gVar;
            }

            public void run() {
                C0006d dVar = this.f775a;
                if (dVar != null) {
                    d.this.A = true;
                    dVar.f780b.a(false);
                    d.this.A = false;
                }
                if (this.f776b.isEnabled() && this.f776b.hasSubMenu()) {
                    this.f777c.a(this.f776b, 4);
                }
            }
        }

        c() {
        }

        public void a(g gVar, MenuItem menuItem) {
            C0006d dVar = null;
            d.this.f767g.removeCallbacksAndMessages(null);
            int size = d.this.f769i.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    i2 = -1;
                    break;
                } else if (gVar == d.this.f769i.get(i2).f780b) {
                    break;
                } else {
                    i2++;
                }
            }
            if (i2 != -1) {
                int i3 = i2 + 1;
                if (i3 < d.this.f769i.size()) {
                    dVar = d.this.f769i.get(i3);
                }
                d.this.f767g.postAtTime(new a(dVar, menuItem, gVar), gVar, SystemClock.uptimeMillis() + 200);
            }
        }

        public void b(g gVar, MenuItem menuItem) {
            d.this.f767g.removeCallbacksAndMessages(gVar);
        }
    }

    /* renamed from: androidx.appcompat.view.menu.d$d  reason: collision with other inner class name */
    /* compiled from: CascadingMenuPopup */
    private static class C0006d {

        /* renamed from: a  reason: collision with root package name */
        public final k0 f779a;

        /* renamed from: b  reason: collision with root package name */
        public final g f780b;

        /* renamed from: c  reason: collision with root package name */
        public final int f781c;

        public C0006d(k0 k0Var, g gVar, int i2) {
            this.f779a = k0Var;
            this.f780b = gVar;
            this.f781c = i2;
        }

        public ListView a() {
            return this.f779a.d();
        }
    }

    public d(Context context, View view, int i2, int i3, boolean z2) {
        this.f762b = context;
        this.o = view;
        this.f764d = i2;
        this.f765e = i3;
        this.f766f = z2;
        this.v = false;
        this.q = f();
        Resources resources = context.getResources();
        this.f763c = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(b.a.d.abc_config_prefDialogWidth));
        this.f767g = new Handler();
    }

    private int c(g gVar) {
        int size = this.f769i.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (gVar == this.f769i.get(i2).f780b) {
                return i2;
            }
        }
        return -1;
    }

    private int d(int i2) {
        List<C0006d> list = this.f769i;
        ListView a2 = list.get(list.size() - 1).a();
        int[] iArr = new int[2];
        a2.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        this.p.getWindowVisibleDisplayFrame(rect);
        if (this.q == 1) {
            if (iArr[0] + a2.getWidth() + i2 > rect.right) {
                return 0;
            }
            return 1;
        } else if (iArr[0] - i2 < 0) {
            return 1;
        } else {
            return 0;
        }
    }

    private k0 e() {
        k0 k0Var = new k0(this.f762b, null, this.f764d, this.f765e);
        k0Var.a(this.l);
        k0Var.a((AdapterView.OnItemClickListener) this);
        k0Var.a((PopupWindow.OnDismissListener) this);
        k0Var.a(this.o);
        k0Var.f(this.n);
        k0Var.a(true);
        k0Var.g(2);
        return k0Var;
    }

    private int f() {
        return u.k(this.o) == 1 ? 0 : 1;
    }

    public void a(g gVar) {
        gVar.a(this, this.f762b);
        if (isShowing()) {
            d(gVar);
        } else {
            this.f768h.add(gVar);
        }
    }

    public boolean a() {
        return false;
    }

    public void b(boolean z2) {
        this.v = z2;
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return false;
    }

    public void dismiss() {
        int size = this.f769i.size();
        if (size > 0) {
            C0006d[] dVarArr = (C0006d[]) this.f769i.toArray(new C0006d[size]);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                C0006d dVar = dVarArr[i2];
                if (dVar.f779a.isShowing()) {
                    dVar.f779a.dismiss();
                }
            }
        }
    }

    public boolean isShowing() {
        return this.f769i.size() > 0 && this.f769i.get(0).f779a.isShowing();
    }

    public void onDismiss() {
        C0006d dVar;
        int size = this.f769i.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                dVar = null;
                break;
            }
            dVar = this.f769i.get(i2);
            if (!dVar.f779a.isShowing()) {
                break;
            }
            i2++;
        }
        if (dVar != null) {
            dVar.f780b.a(false);
        }
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    public void show() {
        if (!isShowing()) {
            for (g d2 : this.f768h) {
                d(d2);
            }
            this.f768h.clear();
            this.p = this.o;
            if (this.p != null) {
                boolean z2 = this.y == null;
                this.y = this.p.getViewTreeObserver();
                if (z2) {
                    this.y.addOnGlobalLayoutListener(this.f770j);
                }
                this.p.addOnAttachStateChangeListener(this.f771k);
            }
        }
    }

    public void b(int i2) {
        this.r = true;
        this.t = i2;
    }

    public void c(int i2) {
        this.s = true;
        this.u = i2;
    }

    private MenuItem a(g gVar, g gVar2) {
        int size = gVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItem item = gVar.getItem(i2);
            if (item.hasSubMenu() && gVar2 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    public void c(boolean z2) {
        this.w = z2;
    }

    private View a(C0006d dVar, g gVar) {
        int i2;
        f fVar;
        int firstVisiblePosition;
        MenuItem a2 = a(dVar.f780b, gVar);
        if (a2 == null) {
            return null;
        }
        ListView a3 = dVar.a();
        ListAdapter adapter = a3.getAdapter();
        int i3 = 0;
        if (adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
            i2 = headerViewListAdapter.getHeadersCount();
            fVar = (f) headerViewListAdapter.getWrappedAdapter();
        } else {
            fVar = (f) adapter;
            i2 = 0;
        }
        int count = fVar.getCount();
        while (true) {
            if (i3 >= count) {
                i3 = -1;
                break;
            } else if (a2 == fVar.getItem(i3)) {
                break;
            } else {
                i3++;
            }
        }
        if (i3 != -1 && (firstVisiblePosition = (i3 + i2) - a3.getFirstVisiblePosition()) >= 0 && firstVisiblePosition < a3.getChildCount()) {
            return a3.getChildAt(firstVisiblePosition);
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void d(g gVar) {
        View view;
        C0006d dVar;
        int i2;
        int i3;
        int i4;
        LayoutInflater from = LayoutInflater.from(this.f762b);
        f fVar = new f(gVar, from, this.f766f, B);
        if (!isShowing() && this.v) {
            fVar.a(true);
        } else if (isShowing()) {
            fVar.a(l.b(gVar));
        }
        int a2 = l.a(fVar, null, this.f762b, this.f763c);
        k0 e2 = e();
        e2.a((ListAdapter) fVar);
        e2.e(a2);
        e2.f(this.n);
        if (this.f769i.size() > 0) {
            List<C0006d> list = this.f769i;
            dVar = list.get(list.size() - 1);
            view = a(dVar, gVar);
        } else {
            dVar = null;
            view = null;
        }
        if (view != null) {
            e2.c(false);
            e2.a((Object) null);
            int d2 = d(a2);
            boolean z2 = d2 == 1;
            this.q = d2;
            if (Build.VERSION.SDK_INT >= 26) {
                e2.a(view);
                i3 = 0;
                i2 = 0;
            } else {
                int[] iArr = new int[2];
                this.o.getLocationOnScreen(iArr);
                int[] iArr2 = new int[2];
                view.getLocationOnScreen(iArr2);
                if ((this.n & 7) == 5) {
                    iArr[0] = iArr[0] + this.o.getWidth();
                    iArr2[0] = iArr2[0] + view.getWidth();
                }
                i2 = iArr2[0] - iArr[0];
                i3 = iArr2[1] - iArr[1];
            }
            if ((this.n & 5) != 5) {
                if (z2) {
                    a2 = view.getWidth();
                }
                i4 = i2 - a2;
                e2.a(i4);
                e2.b(true);
                e2.b(i3);
            } else if (!z2) {
                a2 = view.getWidth();
                i4 = i2 - a2;
                e2.a(i4);
                e2.b(true);
                e2.b(i3);
            }
            i4 = i2 + a2;
            e2.a(i4);
            e2.b(true);
            e2.b(i3);
        } else {
            if (this.r) {
                e2.a(this.t);
            }
            if (this.s) {
                e2.b(this.u);
            }
            e2.a(c());
        }
        this.f769i.add(new C0006d(e2, gVar, this.q));
        e2.show();
        ListView d3 = e2.d();
        d3.setOnKeyListener(this);
        if (dVar == null && this.w && gVar.h() != null) {
            FrameLayout frameLayout = (FrameLayout) from.inflate(g.abc_popup_menu_header_item_layout, (ViewGroup) d3, false);
            frameLayout.setEnabled(false);
            ((TextView) frameLayout.findViewById(16908310)).setText(gVar.h());
            d3.addHeaderView(frameLayout, null, false);
            e2.show();
        }
    }

    public void a(boolean z2) {
        for (C0006d a2 : this.f769i) {
            l.a(a2.a().getAdapter()).notifyDataSetChanged();
        }
    }

    public void a(n.a aVar) {
        this.x = aVar;
    }

    public boolean a(s sVar) {
        for (C0006d next : this.f769i) {
            if (sVar == next.f780b) {
                next.a().requestFocus();
                return true;
            }
        }
        if (!sVar.hasVisibleItems()) {
            return false;
        }
        a((g) sVar);
        n.a aVar = this.x;
        if (aVar != null) {
            aVar.a(sVar);
        }
        return true;
    }

    public void a(g gVar, boolean z2) {
        int c2 = c(gVar);
        if (c2 >= 0) {
            int i2 = c2 + 1;
            if (i2 < this.f769i.size()) {
                this.f769i.get(i2).f780b.a(false);
            }
            C0006d remove = this.f769i.remove(c2);
            remove.f780b.b(this);
            if (this.A) {
                remove.f779a.b((Object) null);
                remove.f779a.d(0);
            }
            remove.f779a.dismiss();
            int size = this.f769i.size();
            if (size > 0) {
                this.q = this.f769i.get(size - 1).f781c;
            } else {
                this.q = f();
            }
            if (size == 0) {
                dismiss();
                n.a aVar = this.x;
                if (aVar != null) {
                    aVar.a(gVar, true);
                }
                ViewTreeObserver viewTreeObserver = this.y;
                if (viewTreeObserver != null) {
                    if (viewTreeObserver.isAlive()) {
                        this.y.removeGlobalOnLayoutListener(this.f770j);
                    }
                    this.y = null;
                }
                this.p.removeOnAttachStateChangeListener(this.f771k);
                this.z.onDismiss();
            } else if (z2) {
                this.f769i.get(0).f780b.a(false);
            }
        }
    }

    public void a(int i2) {
        if (this.m != i2) {
            this.m = i2;
            this.n = b.e.m.c.a(i2, u.k(this.o));
        }
    }

    public void a(View view) {
        if (this.o != view) {
            this.o = view;
            this.n = b.e.m.c.a(this.m, u.k(this.o));
        }
    }

    public ListView d() {
        if (this.f769i.isEmpty()) {
            return null;
        }
        List<C0006d> list = this.f769i;
        return list.get(list.size() - 1).a();
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.z = onDismissListener;
    }
}
