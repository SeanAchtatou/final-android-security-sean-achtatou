package androidx.versionedparcelable;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.SparseIntArray;
import b.d.a;
import java.lang.reflect.Method;

/* compiled from: VersionedParcelParcel */
class b extends a {

    /* renamed from: d  reason: collision with root package name */
    private final SparseIntArray f2120d;

    /* renamed from: e  reason: collision with root package name */
    private final Parcel f2121e;

    /* renamed from: f  reason: collision with root package name */
    private final int f2122f;

    /* renamed from: g  reason: collision with root package name */
    private final int f2123g;

    /* renamed from: h  reason: collision with root package name */
    private final String f2124h;

    /* renamed from: i  reason: collision with root package name */
    private int f2125i;

    /* renamed from: j  reason: collision with root package name */
    private int f2126j;

    /* renamed from: k  reason: collision with root package name */
    private int f2127k;

    b(Parcel parcel) {
        this(parcel, parcel.dataPosition(), parcel.dataSize(), "", new a(), new a(), new a());
    }

    public boolean a(int i2) {
        while (this.f2126j < this.f2123g) {
            int i3 = this.f2127k;
            if (i3 == i2) {
                return true;
            }
            if (String.valueOf(i3).compareTo(String.valueOf(i2)) > 0) {
                return false;
            }
            this.f2121e.setDataPosition(this.f2126j);
            int readInt = this.f2121e.readInt();
            this.f2127k = this.f2121e.readInt();
            this.f2126j += readInt;
        }
        if (this.f2127k == i2) {
            return true;
        }
        return false;
    }

    public void b(int i2) {
        a();
        this.f2125i = i2;
        this.f2120d.put(i2, this.f2121e.dataPosition());
        c(0);
        c(i2);
    }

    public void c(int i2) {
        this.f2121e.writeInt(i2);
    }

    public boolean d() {
        return this.f2121e.readInt() != 0;
    }

    public byte[] e() {
        int readInt = this.f2121e.readInt();
        if (readInt < 0) {
            return null;
        }
        byte[] bArr = new byte[readInt];
        this.f2121e.readByteArray(bArr);
        return bArr;
    }

    /* access modifiers changed from: protected */
    public CharSequence f() {
        return (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(this.f2121e);
    }

    public int g() {
        return this.f2121e.readInt();
    }

    public <T extends Parcelable> T h() {
        return this.f2121e.readParcelable(b.class.getClassLoader());
    }

    public String i() {
        return this.f2121e.readString();
    }

    private b(Parcel parcel, int i2, int i3, String str, a<String, Method> aVar, a<String, Method> aVar2, a<String, Class> aVar3) {
        super(aVar, aVar2, aVar3);
        this.f2120d = new SparseIntArray();
        this.f2125i = -1;
        this.f2126j = 0;
        this.f2127k = -1;
        this.f2121e = parcel;
        this.f2122f = i2;
        this.f2123g = i3;
        this.f2126j = this.f2122f;
        this.f2124h = str;
    }

    /* access modifiers changed from: protected */
    public a b() {
        Parcel parcel = this.f2121e;
        int dataPosition = parcel.dataPosition();
        int i2 = this.f2126j;
        if (i2 == this.f2122f) {
            i2 = this.f2123g;
        }
        int i3 = i2;
        return new b(parcel, dataPosition, i3, this.f2124h + "  ", this.f2117a, this.f2118b, this.f2119c);
    }

    public void a() {
        int i2 = this.f2125i;
        if (i2 >= 0) {
            int i3 = this.f2120d.get(i2);
            int dataPosition = this.f2121e.dataPosition();
            this.f2121e.setDataPosition(i3);
            this.f2121e.writeInt(dataPosition - i3);
            this.f2121e.setDataPosition(dataPosition);
        }
    }

    public void a(byte[] bArr) {
        if (bArr != null) {
            this.f2121e.writeInt(bArr.length);
            this.f2121e.writeByteArray(bArr);
            return;
        }
        this.f2121e.writeInt(-1);
    }

    public void a(String str) {
        this.f2121e.writeString(str);
    }

    public void a(Parcelable parcelable) {
        this.f2121e.writeParcelable(parcelable, 0);
    }

    public void a(boolean z) {
        this.f2121e.writeInt(z ? 1 : 0);
    }

    /* access modifiers changed from: protected */
    public void a(CharSequence charSequence) {
        TextUtils.writeToParcel(charSequence, this.f2121e, 0);
    }
}
