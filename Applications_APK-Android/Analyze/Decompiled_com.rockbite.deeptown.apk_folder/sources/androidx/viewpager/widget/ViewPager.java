package androidx.viewpager.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.Scroller;
import androidx.customview.view.AbsSavedState;
import b.e.m.c0;
import b.e.m.q;
import b.e.m.u;
import com.esotericsoftware.spine.Animation;
import com.facebook.internal.Utility;
import com.google.protobuf.CodedOutputStream;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ViewPager extends ViewGroup {
    static final int[] f0 = {16842931};
    private static final Comparator<f> g0 = new a();
    private static final Interpolator h0 = new b();
    private static final m i0 = new m();
    private int A;
    private int B;
    private float C;
    private float D;
    private float E;
    private float F;
    private int G = -1;
    private VelocityTracker H;
    private int I;
    private int J;
    private int K;
    private int L;
    private boolean M;
    private EdgeEffect N;
    private EdgeEffect O;
    private boolean P = true;
    private boolean Q;
    private int R;
    private List<j> S;
    private j T;
    private j U;
    private List<i> V;
    private k W;

    /* renamed from: a  reason: collision with root package name */
    private int f2128a;
    private int a0;

    /* renamed from: b  reason: collision with root package name */
    private final ArrayList<f> f2129b = new ArrayList<>();
    private int b0;

    /* renamed from: c  reason: collision with root package name */
    private final f f2130c = new f();
    private ArrayList<View> c0;

    /* renamed from: d  reason: collision with root package name */
    private final Rect f2131d = new Rect();
    private final Runnable d0 = new c();

    /* renamed from: e  reason: collision with root package name */
    a f2132e;
    private int e0 = 0;

    /* renamed from: f  reason: collision with root package name */
    int f2133f;

    /* renamed from: g  reason: collision with root package name */
    private int f2134g = -1;

    /* renamed from: h  reason: collision with root package name */
    private Parcelable f2135h = null;

    /* renamed from: i  reason: collision with root package name */
    private ClassLoader f2136i = null;

    /* renamed from: j  reason: collision with root package name */
    private Scroller f2137j;

    /* renamed from: k  reason: collision with root package name */
    private boolean f2138k;
    private l l;
    private int m;
    private Drawable n;
    private int o;
    private int p;
    private float q = -3.4028235E38f;
    private float r = Float.MAX_VALUE;
    private int s;
    private boolean t;
    private boolean u;
    private boolean v;
    private int w = 1;
    private boolean x;
    private boolean y;
    private int z;

    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();

        /* renamed from: c  reason: collision with root package name */
        int f2139c;

        /* renamed from: d  reason: collision with root package name */
        Parcelable f2140d;

        /* renamed from: e  reason: collision with root package name */
        ClassLoader f2141e;

        static class a implements Parcelable.ClassLoaderCreator<SavedState> {
            a() {
            }

            public SavedState[] newArray(int i2) {
                return new SavedState[i2];
            }

            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.f2139c + "}";
        }

        public void writeToParcel(Parcel parcel, int i2) {
            super.writeToParcel(parcel, i2);
            parcel.writeInt(this.f2139c);
            parcel.writeParcelable(this.f2140d, i2);
        }

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            classLoader = classLoader == null ? SavedState.class.getClassLoader() : classLoader;
            this.f2139c = parcel.readInt();
            this.f2140d = parcel.readParcelable(classLoader);
            this.f2141e = classLoader;
        }
    }

    static class a implements Comparator<f> {
        a() {
        }

        /* renamed from: a */
        public int compare(f fVar, f fVar2) {
            return fVar.f2146b - fVar2.f2146b;
        }
    }

    static class b implements Interpolator {
        b() {
        }

        public float getInterpolation(float f2) {
            float f3 = f2 - 1.0f;
            return (f3 * f3 * f3 * f3 * f3) + 1.0f;
        }
    }

    class c implements Runnable {
        c() {
        }

        public void run() {
            ViewPager.this.setScrollState(0);
            ViewPager.this.e();
        }
    }

    class d implements q {

        /* renamed from: a  reason: collision with root package name */
        private final Rect f2143a = new Rect();

        d() {
        }

        public c0 a(View view, c0 c0Var) {
            c0 b2 = u.b(view, c0Var);
            if (b2.e()) {
                return b2;
            }
            Rect rect = this.f2143a;
            rect.left = b2.b();
            rect.top = b2.d();
            rect.right = b2.c();
            rect.bottom = b2.a();
            int childCount = ViewPager.this.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                c0 a2 = u.a(ViewPager.this.getChildAt(i2), b2);
                rect.left = Math.min(a2.b(), rect.left);
                rect.top = Math.min(a2.d(), rect.top);
                rect.right = Math.min(a2.c(), rect.right);
                rect.bottom = Math.min(a2.a(), rect.bottom);
            }
            return b2.a(rect.left, rect.top, rect.right, rect.bottom);
        }
    }

    @Inherited
    @Target({ElementType.TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface e {
    }

    static class f {

        /* renamed from: a  reason: collision with root package name */
        Object f2145a;

        /* renamed from: b  reason: collision with root package name */
        int f2146b;

        /* renamed from: c  reason: collision with root package name */
        boolean f2147c;

        /* renamed from: d  reason: collision with root package name */
        float f2148d;

        /* renamed from: e  reason: collision with root package name */
        float f2149e;

        f() {
        }
    }

    public interface i {
        void a(ViewPager viewPager, a aVar, a aVar2);
    }

    public interface j {
        void onPageScrollStateChanged(int i2);

        void onPageScrolled(int i2, float f2, int i3);

        void onPageSelected(int i2);
    }

    public interface k {
        void a(View view, float f2);
    }

    private class l extends DataSetObserver {
        l() {
        }

        public void onChanged() {
            ViewPager.this.a();
        }

        public void onInvalidated() {
            ViewPager.this.a();
        }
    }

    static class m implements Comparator<View> {
        m() {
        }

        /* renamed from: a */
        public int compare(View view, View view2) {
            g gVar = (g) view.getLayoutParams();
            g gVar2 = (g) view2.getLayoutParams();
            boolean z = gVar.f2150a;
            if (z != gVar2.f2150a) {
                return z ? 1 : -1;
            }
            return gVar.f2154e - gVar2.f2154e;
        }
    }

    public ViewPager(Context context) {
        super(context);
        b();
    }

    private void d(int i2) {
        j jVar = this.T;
        if (jVar != null) {
            jVar.onPageSelected(i2);
        }
        List<j> list = this.S;
        if (list != null) {
            int size = list.size();
            for (int i3 = 0; i3 < size; i3++) {
                j jVar2 = this.S.get(i3);
                if (jVar2 != null) {
                    jVar2.onPageSelected(i2);
                }
            }
        }
        j jVar3 = this.U;
        if (jVar3 != null) {
            jVar3.onPageSelected(i2);
        }
    }

    private boolean f(int i2) {
        if (this.f2129b.size() != 0) {
            f g2 = g();
            int clientWidth = getClientWidth();
            int i3 = this.m;
            int i4 = clientWidth + i3;
            float f2 = (float) clientWidth;
            int i5 = g2.f2146b;
            float f3 = ((((float) i2) / f2) - g2.f2149e) / (g2.f2148d + (((float) i3) / f2));
            this.Q = false;
            a(i5, f3, (int) (((float) i4) * f3));
            if (this.Q) {
                return true;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        } else if (this.P) {
            return false;
        } else {
            this.Q = false;
            a(0, (float) Animation.CurveTimeline.LINEAR, 0);
            if (this.Q) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
    }

    private f g() {
        int i2;
        int clientWidth = getClientWidth();
        float scrollX = clientWidth > 0 ? ((float) getScrollX()) / ((float) clientWidth) : Animation.CurveTimeline.LINEAR;
        float f2 = clientWidth > 0 ? ((float) this.m) / ((float) clientWidth) : Animation.CurveTimeline.LINEAR;
        f fVar = null;
        int i3 = 0;
        boolean z2 = true;
        int i4 = -1;
        float f3 = Animation.CurveTimeline.LINEAR;
        float f4 = Animation.CurveTimeline.LINEAR;
        while (i3 < this.f2129b.size()) {
            f fVar2 = this.f2129b.get(i3);
            if (!z2 && fVar2.f2146b != (i2 = i4 + 1)) {
                fVar2 = this.f2130c;
                fVar2.f2149e = f3 + f4 + f2;
                fVar2.f2146b = i2;
                fVar2.f2148d = this.f2132e.getPageWidth(fVar2.f2146b);
                i3--;
            }
            f3 = fVar2.f2149e;
            float f5 = fVar2.f2148d + f3 + f2;
            if (!z2 && scrollX < f3) {
                return fVar;
            }
            if (scrollX < f5 || i3 == this.f2129b.size() - 1) {
                return fVar2;
            }
            i4 = fVar2.f2146b;
            f4 = fVar2.f2148d;
            i3++;
            fVar = fVar2;
            z2 = false;
        }
        return fVar;
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    private void h() {
        int i2 = 0;
        while (i2 < getChildCount()) {
            if (!((g) getChildAt(i2).getLayoutParams()).f2150a) {
                removeViewAt(i2);
                i2--;
            }
            i2++;
        }
    }

    private boolean i() {
        this.G = -1;
        f();
        this.N.onRelease();
        this.O.onRelease();
        return this.N.isFinished() || this.O.isFinished();
    }

    private void j() {
        if (this.b0 != 0) {
            ArrayList<View> arrayList = this.c0;
            if (arrayList == null) {
                this.c0 = new ArrayList<>();
            } else {
                arrayList.clear();
            }
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                this.c0.add(getChildAt(i2));
            }
            Collections.sort(this.c0, i0);
        }
    }

    private void setScrollingCacheEnabled(boolean z2) {
        if (this.u != z2) {
            this.u = z2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(androidx.viewpager.widget.ViewPager$f, int, androidx.viewpager.widget.ViewPager$f):void
      androidx.viewpager.widget.ViewPager.a(int, float, int):void
      androidx.viewpager.widget.ViewPager.a(int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean):void */
    public void a(int i2, boolean z2) {
        this.v = false;
        a(i2, z2, false);
    }

    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        f b2;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                View childAt = getChildAt(i4);
                if (childAt.getVisibility() == 0 && (b2 = b(childAt)) != null && b2.f2146b == this.f2133f) {
                    childAt.addFocusables(arrayList, i2, i3);
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if (((i3 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) && arrayList != null) {
            arrayList.add(this);
        }
    }

    public void addTouchables(ArrayList<View> arrayList) {
        f b2;
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (b2 = b(childAt)) != null && b2.f2146b == this.f2133f) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        if (!checkLayoutParams(layoutParams)) {
            layoutParams = generateLayoutParams(layoutParams);
        }
        g gVar = (g) layoutParams;
        gVar.f2150a |= c(view);
        if (!this.t) {
            super.addView(view, i2, layoutParams);
        } else if (gVar == null || !gVar.f2150a) {
            gVar.f2153d = true;
            addViewInLayout(view, i2, layoutParams);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        setWillNotDraw(false);
        setDescendantFocusability(262144);
        setFocusable(true);
        Context context = getContext();
        this.f2137j = new Scroller(context, h0);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.B = viewConfiguration.getScaledPagingTouchSlop();
        this.I = (int) (400.0f * f2);
        this.J = viewConfiguration.getScaledMaximumFlingVelocity();
        this.N = new EdgeEffect(context);
        this.O = new EdgeEffect(context);
        this.K = (int) (25.0f * f2);
        this.L = (int) (2.0f * f2);
        this.z = (int) (f2 * 16.0f);
        u.a(this, new h());
        if (u.i(this) == 0) {
            u.b(this, 1);
        }
        u.a(this, new d());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.a.destroyItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [androidx.viewpager.widget.ViewPager, int, java.lang.Object]
     candidates:
      androidx.viewpager.widget.a.destroyItem(android.view.View, int, java.lang.Object):void
      androidx.viewpager.widget.a.destroyItem(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.a.setPrimaryItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [androidx.viewpager.widget.ViewPager, int, java.lang.Object]
     candidates:
      androidx.viewpager.widget.a.setPrimaryItem(android.view.View, int, java.lang.Object):void
      androidx.viewpager.widget.a.setPrimaryItem(android.view.ViewGroup, int, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0060, code lost:
        if (r9 == r10) goto L_0x0067;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0066, code lost:
        r8 = null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(int r18) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            int r2 = r0.f2133f
            if (r2 == r1) goto L_0x000f
            androidx.viewpager.widget.ViewPager$f r2 = r0.b(r2)
            r0.f2133f = r1
            goto L_0x0010
        L_0x000f:
            r2 = 0
        L_0x0010:
            androidx.viewpager.widget.a r1 = r0.f2132e
            if (r1 != 0) goto L_0x0018
            r17.j()
            return
        L_0x0018:
            boolean r1 = r0.v
            if (r1 == 0) goto L_0x0020
            r17.j()
            return
        L_0x0020:
            android.os.IBinder r1 = r17.getWindowToken()
            if (r1 != 0) goto L_0x0027
            return
        L_0x0027:
            androidx.viewpager.widget.a r1 = r0.f2132e
            r1.startUpdate(r0)
            int r1 = r0.w
            int r4 = r0.f2133f
            int r4 = r4 - r1
            r5 = 0
            int r4 = java.lang.Math.max(r5, r4)
            androidx.viewpager.widget.a r6 = r0.f2132e
            int r6 = r6.getCount()
            int r7 = r6 + -1
            int r8 = r0.f2133f
            int r8 = r8 + r1
            int r1 = java.lang.Math.min(r7, r8)
            int r7 = r0.f2128a
            if (r6 != r7) goto L_0x0211
            r7 = 0
        L_0x004a:
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r8 = r0.f2129b
            int r8 = r8.size()
            if (r7 >= r8) goto L_0x0066
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r8 = r0.f2129b
            java.lang.Object r8 = r8.get(r7)
            androidx.viewpager.widget.ViewPager$f r8 = (androidx.viewpager.widget.ViewPager.f) r8
            int r9 = r8.f2146b
            int r10 = r0.f2133f
            if (r9 < r10) goto L_0x0063
            if (r9 != r10) goto L_0x0066
            goto L_0x0067
        L_0x0063:
            int r7 = r7 + 1
            goto L_0x004a
        L_0x0066:
            r8 = 0
        L_0x0067:
            if (r8 != 0) goto L_0x0071
            if (r6 <= 0) goto L_0x0071
            int r8 = r0.f2133f
            androidx.viewpager.widget.ViewPager$f r8 = r0.a(r8, r7)
        L_0x0071:
            r9 = 0
            if (r8 == 0) goto L_0x019e
            int r10 = r7 + -1
            if (r10 < 0) goto L_0x0081
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r11 = r0.f2129b
            java.lang.Object r11 = r11.get(r10)
            androidx.viewpager.widget.ViewPager$f r11 = (androidx.viewpager.widget.ViewPager.f) r11
            goto L_0x0082
        L_0x0081:
            r11 = 0
        L_0x0082:
            int r12 = r17.getClientWidth()
            r13 = 1073741824(0x40000000, float:2.0)
            if (r12 > 0) goto L_0x008c
            r3 = 0
            goto L_0x0099
        L_0x008c:
            float r14 = r8.f2148d
            float r14 = r13 - r14
            int r15 = r17.getPaddingLeft()
            float r15 = (float) r15
            float r3 = (float) r12
            float r15 = r15 / r3
            float r3 = r14 + r15
        L_0x0099:
            int r14 = r0.f2133f
            int r14 = r14 + -1
            r15 = r10
            r10 = r7
            r7 = 0
        L_0x00a0:
            if (r14 < 0) goto L_0x0100
            int r16 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r16 < 0) goto L_0x00ce
            if (r14 >= r4) goto L_0x00ce
            if (r11 != 0) goto L_0x00ab
            goto L_0x0100
        L_0x00ab:
            int r5 = r11.f2146b
            if (r14 != r5) goto L_0x00fc
            boolean r5 = r11.f2147c
            if (r5 != 0) goto L_0x00fc
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r0.f2129b
            r5.remove(r15)
            androidx.viewpager.widget.a r5 = r0.f2132e
            java.lang.Object r11 = r11.f2145a
            r5.destroyItem(r0, r14, r11)
            int r15 = r15 + -1
            int r10 = r10 + -1
            if (r15 < 0) goto L_0x00fa
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r0.f2129b
            java.lang.Object r5 = r5.get(r15)
            androidx.viewpager.widget.ViewPager$f r5 = (androidx.viewpager.widget.ViewPager.f) r5
            goto L_0x00fb
        L_0x00ce:
            if (r11 == 0) goto L_0x00e4
            int r5 = r11.f2146b
            if (r14 != r5) goto L_0x00e4
            float r5 = r11.f2148d
            float r7 = r7 + r5
            int r15 = r15 + -1
            if (r15 < 0) goto L_0x00fa
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r0.f2129b
            java.lang.Object r5 = r5.get(r15)
            androidx.viewpager.widget.ViewPager$f r5 = (androidx.viewpager.widget.ViewPager.f) r5
            goto L_0x00fb
        L_0x00e4:
            int r5 = r15 + 1
            androidx.viewpager.widget.ViewPager$f r5 = r0.a(r14, r5)
            float r5 = r5.f2148d
            float r7 = r7 + r5
            int r10 = r10 + 1
            if (r15 < 0) goto L_0x00fa
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r0.f2129b
            java.lang.Object r5 = r5.get(r15)
            androidx.viewpager.widget.ViewPager$f r5 = (androidx.viewpager.widget.ViewPager.f) r5
            goto L_0x00fb
        L_0x00fa:
            r5 = 0
        L_0x00fb:
            r11 = r5
        L_0x00fc:
            int r14 = r14 + -1
            r5 = 0
            goto L_0x00a0
        L_0x0100:
            float r3 = r8.f2148d
            int r4 = r10 + 1
            int r5 = (r3 > r13 ? 1 : (r3 == r13 ? 0 : -1))
            if (r5 >= 0) goto L_0x0192
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r0.f2129b
            int r5 = r5.size()
            if (r4 >= r5) goto L_0x0119
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r0.f2129b
            java.lang.Object r5 = r5.get(r4)
            androidx.viewpager.widget.ViewPager$f r5 = (androidx.viewpager.widget.ViewPager.f) r5
            goto L_0x011a
        L_0x0119:
            r5 = 0
        L_0x011a:
            if (r12 > 0) goto L_0x011e
            r7 = 0
            goto L_0x0126
        L_0x011e:
            int r7 = r17.getPaddingRight()
            float r7 = (float) r7
            float r11 = (float) r12
            float r7 = r7 / r11
            float r7 = r7 + r13
        L_0x0126:
            int r11 = r0.f2133f
        L_0x0128:
            int r11 = r11 + 1
            if (r11 >= r6) goto L_0x0192
            int r12 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r12 < 0) goto L_0x015c
            if (r11 <= r1) goto L_0x015c
            if (r5 != 0) goto L_0x0135
            goto L_0x0192
        L_0x0135:
            int r12 = r5.f2146b
            if (r11 != r12) goto L_0x0191
            boolean r12 = r5.f2147c
            if (r12 != 0) goto L_0x0191
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r12 = r0.f2129b
            r12.remove(r4)
            androidx.viewpager.widget.a r12 = r0.f2132e
            java.lang.Object r5 = r5.f2145a
            r12.destroyItem(r0, r11, r5)
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r0.f2129b
            int r5 = r5.size()
            if (r4 >= r5) goto L_0x015a
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r0.f2129b
            java.lang.Object r5 = r5.get(r4)
            androidx.viewpager.widget.ViewPager$f r5 = (androidx.viewpager.widget.ViewPager.f) r5
            goto L_0x0191
        L_0x015a:
            r5 = 0
            goto L_0x0191
        L_0x015c:
            if (r5 == 0) goto L_0x0178
            int r12 = r5.f2146b
            if (r11 != r12) goto L_0x0178
            float r5 = r5.f2148d
            float r3 = r3 + r5
            int r4 = r4 + 1
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r0.f2129b
            int r5 = r5.size()
            if (r4 >= r5) goto L_0x015a
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r0.f2129b
            java.lang.Object r5 = r5.get(r4)
            androidx.viewpager.widget.ViewPager$f r5 = (androidx.viewpager.widget.ViewPager.f) r5
            goto L_0x0191
        L_0x0178:
            androidx.viewpager.widget.ViewPager$f r5 = r0.a(r11, r4)
            int r4 = r4 + 1
            float r5 = r5.f2148d
            float r3 = r3 + r5
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r0.f2129b
            int r5 = r5.size()
            if (r4 >= r5) goto L_0x015a
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r0.f2129b
            java.lang.Object r5 = r5.get(r4)
            androidx.viewpager.widget.ViewPager$f r5 = (androidx.viewpager.widget.ViewPager.f) r5
        L_0x0191:
            goto L_0x0128
        L_0x0192:
            r0.a(r8, r10, r2)
            androidx.viewpager.widget.a r1 = r0.f2132e
            int r2 = r0.f2133f
            java.lang.Object r3 = r8.f2145a
            r1.setPrimaryItem(r0, r2, r3)
        L_0x019e:
            androidx.viewpager.widget.a r1 = r0.f2132e
            r1.finishUpdate(r0)
            int r1 = r17.getChildCount()
            r2 = 0
        L_0x01a8:
            if (r2 >= r1) goto L_0x01d1
            android.view.View r3 = r0.getChildAt(r2)
            android.view.ViewGroup$LayoutParams r4 = r3.getLayoutParams()
            androidx.viewpager.widget.ViewPager$g r4 = (androidx.viewpager.widget.ViewPager.g) r4
            r4.f2155f = r2
            boolean r5 = r4.f2150a
            if (r5 != 0) goto L_0x01ce
            float r5 = r4.f2152c
            int r5 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r5 != 0) goto L_0x01ce
            androidx.viewpager.widget.ViewPager$f r3 = r0.b(r3)
            if (r3 == 0) goto L_0x01ce
            float r5 = r3.f2148d
            r4.f2152c = r5
            int r3 = r3.f2146b
            r4.f2154e = r3
        L_0x01ce:
            int r2 = r2 + 1
            goto L_0x01a8
        L_0x01d1:
            r17.j()
            boolean r1 = r17.hasFocus()
            if (r1 == 0) goto L_0x0210
            android.view.View r1 = r17.findFocus()
            if (r1 == 0) goto L_0x01e5
            androidx.viewpager.widget.ViewPager$f r3 = r0.a(r1)
            goto L_0x01e6
        L_0x01e5:
            r3 = 0
        L_0x01e6:
            if (r3 == 0) goto L_0x01ee
            int r1 = r3.f2146b
            int r2 = r0.f2133f
            if (r1 == r2) goto L_0x0210
        L_0x01ee:
            r1 = 0
        L_0x01ef:
            int r2 = r17.getChildCount()
            if (r1 >= r2) goto L_0x0210
            android.view.View r2 = r0.getChildAt(r1)
            androidx.viewpager.widget.ViewPager$f r3 = r0.b(r2)
            if (r3 == 0) goto L_0x020d
            int r3 = r3.f2146b
            int r4 = r0.f2133f
            if (r3 != r4) goto L_0x020d
            r3 = 2
            boolean r2 = r2.requestFocus(r3)
            if (r2 == 0) goto L_0x020d
            goto L_0x0210
        L_0x020d:
            int r1 = r1 + 1
            goto L_0x01ef
        L_0x0210:
            return
        L_0x0211:
            android.content.res.Resources r1 = r17.getResources()     // Catch:{ NotFoundException -> 0x021e }
            int r2 = r17.getId()     // Catch:{ NotFoundException -> 0x021e }
            java.lang.String r1 = r1.getResourceName(r2)     // Catch:{ NotFoundException -> 0x021e }
            goto L_0x0226
        L_0x021e:
            int r1 = r17.getId()
            java.lang.String r1 = java.lang.Integer.toHexString(r1)
        L_0x0226:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: "
            r3.append(r4)
            int r4 = r0.f2128a
            r3.append(r4)
            java.lang.String r4 = ", found: "
            r3.append(r4)
            r3.append(r6)
            java.lang.String r4 = " Pager id: "
            r3.append(r4)
            r3.append(r1)
            java.lang.String r1 = " Pager class: "
            r3.append(r1)
            java.lang.Class<androidx.viewpager.widget.ViewPager> r1 = androidx.viewpager.widget.ViewPager.class
            r3.append(r1)
            java.lang.String r1 = " Problematic adapter: "
            r3.append(r1)
            androidx.viewpager.widget.a r1 = r0.f2132e
            java.lang.Class r1 = r1.getClass()
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r2.<init>(r1)
            goto L_0x0268
        L_0x0267:
            throw r2
        L_0x0268:
            goto L_0x0267
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.c(int):void");
    }

    public boolean canScrollHorizontally(int i2) {
        if (this.f2132e == null) {
            return false;
        }
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        if (i2 < 0) {
            if (scrollX > ((int) (((float) clientWidth) * this.q))) {
                return true;
            }
            return false;
        } else if (i2 <= 0 || scrollX >= ((int) (((float) clientWidth) * this.r))) {
            return false;
        } else {
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof g) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        this.f2138k = true;
        if (this.f2137j.isFinished() || !this.f2137j.computeScrollOffset()) {
            a(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.f2137j.getCurrX();
        int currY = this.f2137j.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!f(currX)) {
                this.f2137j.abortAnimation();
                scrollTo(0, currY);
            }
        }
        u.v(this);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || a(keyEvent);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        f b2;
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (b2 = b(childAt)) != null && b2.f2146b == this.f2133f && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    public void draw(Canvas canvas) {
        a aVar;
        super.draw(canvas);
        int overScrollMode = getOverScrollMode();
        boolean z2 = false;
        if (overScrollMode == 0 || (overScrollMode == 1 && (aVar = this.f2132e) != null && aVar.getCount() > 1)) {
            if (!this.N.isFinished()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) ((-height) + getPaddingTop()), this.q * ((float) width));
                this.N.setSize(height, width);
                z2 = false | this.N.draw(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.O.isFinished()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.r + 1.0f)) * ((float) width2));
                this.O.setSize(height2, width2);
                z2 |= this.O.draw(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.N.finish();
            this.O.finish();
        }
        if (z2) {
            u.v(this);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.n;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        c(this.f2133f);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new g();
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    public a getAdapter() {
        return this.f2132e;
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        if (this.b0 == 2) {
            i3 = (i2 - 1) - i3;
        }
        return ((g) this.c0.get(i3).getLayoutParams()).f2155f;
    }

    public int getCurrentItem() {
        return this.f2133f;
    }

    public int getOffscreenPageLimit() {
        return this.w;
    }

    public int getPageMargin() {
        return this.m;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.P = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.d0);
        Scroller scroller = this.f2137j;
        if (scroller != null && !scroller.isFinished()) {
            this.f2137j.abortAnimation();
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f2;
        float f3;
        super.onDraw(canvas);
        if (this.m > 0 && this.n != null && this.f2129b.size() > 0 && this.f2132e != null) {
            int scrollX = getScrollX();
            int width = getWidth();
            float f4 = (float) width;
            float f5 = ((float) this.m) / f4;
            int i2 = 0;
            f fVar = this.f2129b.get(0);
            float f6 = fVar.f2149e;
            int size = this.f2129b.size();
            int i3 = fVar.f2146b;
            int i4 = this.f2129b.get(size - 1).f2146b;
            while (i3 < i4) {
                while (i3 > fVar.f2146b && i2 < size) {
                    i2++;
                    fVar = this.f2129b.get(i2);
                }
                if (i3 == fVar.f2146b) {
                    float f7 = fVar.f2149e;
                    float f8 = fVar.f2148d;
                    f2 = (f7 + f8) * f4;
                    f6 = f7 + f8 + f5;
                } else {
                    float pageWidth = this.f2132e.getPageWidth(i3);
                    f2 = (f6 + pageWidth) * f4;
                    f6 += pageWidth + f5;
                }
                if (((float) this.m) + f2 > ((float) scrollX)) {
                    f3 = f5;
                    this.n.setBounds(Math.round(f2), this.o, Math.round(((float) this.m) + f2), this.p);
                    this.n.draw(canvas);
                } else {
                    f3 = f5;
                }
                if (f2 <= ((float) (scrollX + width))) {
                    i3++;
                    f5 = f3;
                } else {
                    return;
                }
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int action = motionEvent.getAction() & 255;
        if (action == 3 || action == 1) {
            i();
            return false;
        }
        if (action != 0) {
            if (this.x) {
                return true;
            }
            if (this.y) {
                return false;
            }
        }
        if (action == 0) {
            float x2 = motionEvent.getX();
            this.E = x2;
            this.C = x2;
            float y2 = motionEvent.getY();
            this.F = y2;
            this.D = y2;
            this.G = motionEvent2.getPointerId(0);
            this.y = false;
            this.f2138k = true;
            this.f2137j.computeScrollOffset();
            if (this.e0 != 2 || Math.abs(this.f2137j.getFinalX() - this.f2137j.getCurrX()) <= this.L) {
                a(false);
                this.x = false;
            } else {
                this.f2137j.abortAnimation();
                this.v = false;
                e();
                this.x = true;
                c(true);
                setScrollState(1);
            }
        } else if (action == 2) {
            int i2 = this.G;
            if (i2 != -1) {
                int findPointerIndex = motionEvent2.findPointerIndex(i2);
                float x3 = motionEvent2.getX(findPointerIndex);
                float f2 = x3 - this.C;
                float abs = Math.abs(f2);
                float y3 = motionEvent2.getY(findPointerIndex);
                float abs2 = Math.abs(y3 - this.F);
                if (f2 == Animation.CurveTimeline.LINEAR || a(this.C, f2) || !a(this, false, (int) f2, (int) x3, (int) y3)) {
                    if (abs > ((float) this.B) && abs * 0.5f > abs2) {
                        this.x = true;
                        c(true);
                        setScrollState(1);
                        this.C = f2 > Animation.CurveTimeline.LINEAR ? this.E + ((float) this.B) : this.E - ((float) this.B);
                        this.D = y3;
                        setScrollingCacheEnabled(true);
                    } else if (abs2 > ((float) this.B)) {
                        this.y = true;
                    }
                    if (this.x && b(x3)) {
                        u.v(this);
                    }
                } else {
                    this.C = x3;
                    this.D = y3;
                    this.y = true;
                    return false;
                }
            }
        } else if (action == 6) {
            a(motionEvent);
        }
        if (this.H == null) {
            this.H = VelocityTracker.obtain();
        }
        this.H.addMovement(motionEvent2);
        return this.x;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(int, float, int, int):int
      androidx.viewpager.widget.ViewPager.a(int, int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void */
    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        boolean z3;
        f b2;
        int i6;
        int i7;
        int childCount = getChildCount();
        int i8 = i4 - i2;
        int i9 = i5 - i3;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int i10 = paddingBottom;
        int i11 = 0;
        int i12 = paddingTop;
        int i13 = paddingLeft;
        for (int i14 = 0; i14 < childCount; i14++) {
            View childAt = getChildAt(i14);
            if (childAt.getVisibility() != 8) {
                g gVar = (g) childAt.getLayoutParams();
                if (gVar.f2150a) {
                    int i15 = gVar.f2151b;
                    int i16 = i15 & 7;
                    int i17 = i15 & 112;
                    if (i16 == 1) {
                        i6 = Math.max((i8 - childAt.getMeasuredWidth()) / 2, i13);
                    } else if (i16 == 3) {
                        i6 = i13;
                        i13 = childAt.getMeasuredWidth() + i13;
                    } else if (i16 != 5) {
                        i6 = i13;
                    } else {
                        i6 = (i8 - paddingRight) - childAt.getMeasuredWidth();
                        paddingRight += childAt.getMeasuredWidth();
                    }
                    if (i17 == 16) {
                        i7 = Math.max((i9 - childAt.getMeasuredHeight()) / 2, i12);
                    } else if (i17 == 48) {
                        i7 = i12;
                        i12 = childAt.getMeasuredHeight() + i12;
                    } else if (i17 != 80) {
                        i7 = i12;
                    } else {
                        i7 = (i9 - i10) - childAt.getMeasuredHeight();
                        i10 += childAt.getMeasuredHeight();
                    }
                    int i18 = i6 + scrollX;
                    childAt.layout(i18, i7, childAt.getMeasuredWidth() + i18, i7 + childAt.getMeasuredHeight());
                    i11++;
                }
            }
        }
        int i19 = (i8 - i13) - paddingRight;
        for (int i20 = 0; i20 < childCount; i20++) {
            View childAt2 = getChildAt(i20);
            if (childAt2.getVisibility() != 8) {
                g gVar2 = (g) childAt2.getLayoutParams();
                if (!gVar2.f2150a && (b2 = b(childAt2)) != null) {
                    float f2 = (float) i19;
                    int i21 = ((int) (b2.f2149e * f2)) + i13;
                    if (gVar2.f2153d) {
                        gVar2.f2153d = false;
                        childAt2.measure(View.MeasureSpec.makeMeasureSpec((int) (f2 * gVar2.f2152c), 1073741824), View.MeasureSpec.makeMeasureSpec((i9 - i12) - i10, 1073741824));
                    }
                    childAt2.layout(i21, i12, childAt2.getMeasuredWidth() + i21, childAt2.getMeasuredHeight() + i12);
                }
            }
        }
        this.o = i12;
        this.p = i9 - i10;
        this.R = i11;
        if (this.P) {
            z3 = false;
            a(this.f2133f, false, 0, false);
        } else {
            z3 = false;
        }
        this.P = z3;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00aa  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r0 = 0
            int r14 = android.view.ViewGroup.getDefaultSize(r0, r14)
            int r15 = android.view.ViewGroup.getDefaultSize(r0, r15)
            r13.setMeasuredDimension(r14, r15)
            int r14 = r13.getMeasuredWidth()
            int r15 = r14 / 10
            int r1 = r13.z
            int r15 = java.lang.Math.min(r15, r1)
            r13.A = r15
            int r15 = r13.getPaddingLeft()
            int r14 = r14 - r15
            int r15 = r13.getPaddingRight()
            int r14 = r14 - r15
            int r15 = r13.getMeasuredHeight()
            int r1 = r13.getPaddingTop()
            int r15 = r15 - r1
            int r1 = r13.getPaddingBottom()
            int r15 = r15 - r1
            int r1 = r13.getChildCount()
            r2 = r15
            r15 = r14
            r14 = 0
        L_0x0039:
            r3 = 8
            r4 = 1
            r5 = 1073741824(0x40000000, float:2.0)
            if (r14 >= r1) goto L_0x00b4
            android.view.View r6 = r13.getChildAt(r14)
            int r7 = r6.getVisibility()
            if (r7 == r3) goto L_0x00b1
            android.view.ViewGroup$LayoutParams r3 = r6.getLayoutParams()
            androidx.viewpager.widget.ViewPager$g r3 = (androidx.viewpager.widget.ViewPager.g) r3
            if (r3 == 0) goto L_0x00b1
            boolean r7 = r3.f2150a
            if (r7 == 0) goto L_0x00b1
            int r7 = r3.f2151b
            r8 = r7 & 7
            r7 = r7 & 112(0x70, float:1.57E-43)
            r9 = 48
            if (r7 == r9) goto L_0x0067
            r9 = 80
            if (r7 != r9) goto L_0x0065
            goto L_0x0067
        L_0x0065:
            r7 = 0
            goto L_0x0068
        L_0x0067:
            r7 = 1
        L_0x0068:
            r9 = 3
            if (r8 == r9) goto L_0x0070
            r9 = 5
            if (r8 != r9) goto L_0x006f
            goto L_0x0070
        L_0x006f:
            r4 = 0
        L_0x0070:
            r8 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r7 == 0) goto L_0x0077
            r8 = 1073741824(0x40000000, float:2.0)
            goto L_0x007c
        L_0x0077:
            if (r4 == 0) goto L_0x007c
            r9 = 1073741824(0x40000000, float:2.0)
            goto L_0x007e
        L_0x007c:
            r9 = -2147483648(0xffffffff80000000, float:-0.0)
        L_0x007e:
            int r10 = r3.width
            r11 = -1
            r12 = -2
            if (r10 == r12) goto L_0x008b
            if (r10 == r11) goto L_0x0087
            goto L_0x0088
        L_0x0087:
            r10 = r15
        L_0x0088:
            r8 = 1073741824(0x40000000, float:2.0)
            goto L_0x008c
        L_0x008b:
            r10 = r15
        L_0x008c:
            int r3 = r3.height
            if (r3 == r12) goto L_0x0095
            if (r3 == r11) goto L_0x0093
            goto L_0x0097
        L_0x0093:
            r3 = r2
            goto L_0x0097
        L_0x0095:
            r3 = r2
            r5 = r9
        L_0x0097:
            int r8 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r8)
            int r3 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r5)
            r6.measure(r8, r3)
            if (r7 == 0) goto L_0x00aa
            int r3 = r6.getMeasuredHeight()
            int r2 = r2 - r3
            goto L_0x00b1
        L_0x00aa:
            if (r4 == 0) goto L_0x00b1
            int r3 = r6.getMeasuredWidth()
            int r15 = r15 - r3
        L_0x00b1:
            int r14 = r14 + 1
            goto L_0x0039
        L_0x00b4:
            android.view.View.MeasureSpec.makeMeasureSpec(r15, r5)
            int r14 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r5)
            r13.s = r14
            r13.t = r4
            r13.e()
            r13.t = r0
            int r14 = r13.getChildCount()
        L_0x00c8:
            if (r0 >= r14) goto L_0x00f2
            android.view.View r1 = r13.getChildAt(r0)
            int r2 = r1.getVisibility()
            if (r2 == r3) goto L_0x00ef
            android.view.ViewGroup$LayoutParams r2 = r1.getLayoutParams()
            androidx.viewpager.widget.ViewPager$g r2 = (androidx.viewpager.widget.ViewPager.g) r2
            if (r2 == 0) goto L_0x00e0
            boolean r4 = r2.f2150a
            if (r4 != 0) goto L_0x00ef
        L_0x00e0:
            float r4 = (float) r15
            float r2 = r2.f2152c
            float r4 = r4 * r2
            int r2 = (int) r4
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r5)
            int r4 = r13.s
            r1.measure(r2, r4)
        L_0x00ef:
            int r0 = r0 + 1
            goto L_0x00c8
        L_0x00f2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        int i3;
        int i4;
        f b2;
        int childCount = getChildCount();
        int i5 = -1;
        if ((i2 & 2) != 0) {
            i5 = childCount;
            i4 = 0;
            i3 = 1;
        } else {
            i4 = childCount - 1;
            i3 = -1;
        }
        while (i4 != i5) {
            View childAt = getChildAt(i4);
            if (childAt.getVisibility() == 0 && (b2 = b(childAt)) != null && b2.f2146b == this.f2133f && childAt.requestFocus(i2, rect)) {
                return true;
            }
            i4 += i3;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(androidx.viewpager.widget.ViewPager$f, int, androidx.viewpager.widget.ViewPager$f):void
      androidx.viewpager.widget.ViewPager.a(int, float, int):void
      androidx.viewpager.widget.ViewPager.a(int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean):void */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        a aVar = this.f2132e;
        if (aVar != null) {
            aVar.restoreState(savedState.f2140d, savedState.f2141e);
            a(savedState.f2139c, false, true);
            return;
        }
        this.f2134g = savedState.f2139c;
        this.f2135h = savedState.f2140d;
        this.f2136i = savedState.f2141e;
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f2139c = this.f2133f;
        a aVar = this.f2132e;
        if (aVar != null) {
            savedState.f2140d = aVar.saveState();
        }
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            int i6 = this.m;
            a(i2, i4, i6, i6);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(int, float, int, int):int
      androidx.viewpager.widget.ViewPager.a(int, int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(int, float, int, int):int
      androidx.viewpager.widget.ViewPager.a(int, int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        a aVar;
        if (this.M) {
            return true;
        }
        boolean z2 = false;
        if ((motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) || (aVar = this.f2132e) == null || aVar.getCount() == 0) {
            return false;
        }
        if (this.H == null) {
            this.H = VelocityTracker.obtain();
        }
        this.H.addMovement(motionEvent);
        int action = motionEvent.getAction() & 255;
        if (action == 0) {
            this.f2137j.abortAnimation();
            this.v = false;
            e();
            float x2 = motionEvent.getX();
            this.E = x2;
            this.C = x2;
            float y2 = motionEvent.getY();
            this.F = y2;
            this.D = y2;
            this.G = motionEvent.getPointerId(0);
        } else if (action != 1) {
            if (action == 2) {
                if (!this.x) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.G);
                    if (findPointerIndex == -1) {
                        z2 = i();
                    } else {
                        float x3 = motionEvent.getX(findPointerIndex);
                        float abs = Math.abs(x3 - this.C);
                        float y3 = motionEvent.getY(findPointerIndex);
                        float abs2 = Math.abs(y3 - this.D);
                        if (abs > ((float) this.B) && abs > abs2) {
                            this.x = true;
                            c(true);
                            float f2 = this.E;
                            this.C = x3 - f2 > Animation.CurveTimeline.LINEAR ? f2 + ((float) this.B) : f2 - ((float) this.B);
                            this.D = y3;
                            setScrollState(1);
                            setScrollingCacheEnabled(true);
                            ViewParent parent = getParent();
                            if (parent != null) {
                                parent.requestDisallowInterceptTouchEvent(true);
                            }
                        }
                    }
                }
                if (this.x) {
                    z2 = false | b(motionEvent.getX(motionEvent.findPointerIndex(this.G)));
                }
            } else if (action != 3) {
                if (action == 5) {
                    int actionIndex = motionEvent.getActionIndex();
                    this.C = motionEvent.getX(actionIndex);
                    this.G = motionEvent.getPointerId(actionIndex);
                } else if (action == 6) {
                    a(motionEvent);
                    this.C = motionEvent.getX(motionEvent.findPointerIndex(this.G));
                }
            } else if (this.x) {
                a(this.f2133f, true, 0, false);
                z2 = i();
            }
        } else if (this.x) {
            VelocityTracker velocityTracker = this.H;
            velocityTracker.computeCurrentVelocity(1000, (float) this.J);
            int xVelocity = (int) velocityTracker.getXVelocity(this.G);
            this.v = true;
            int clientWidth = getClientWidth();
            int scrollX = getScrollX();
            f g2 = g();
            float f3 = (float) clientWidth;
            a(a(g2.f2146b, ((((float) scrollX) / f3) - g2.f2149e) / (g2.f2148d + (((float) this.m) / f3)), xVelocity, (int) (motionEvent.getX(motionEvent.findPointerIndex(this.G)) - this.E)), true, true, xVelocity);
            z2 = i();
        }
        if (z2) {
            u.v(this);
        }
        return true;
    }

    public void removeView(View view) {
        if (this.t) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.a.destroyItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [androidx.viewpager.widget.ViewPager, int, java.lang.Object]
     candidates:
      androidx.viewpager.widget.a.destroyItem(android.view.View, int, java.lang.Object):void
      androidx.viewpager.widget.a.destroyItem(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(androidx.viewpager.widget.ViewPager$f, int, androidx.viewpager.widget.ViewPager$f):void
      androidx.viewpager.widget.ViewPager.a(int, float, int):void
      androidx.viewpager.widget.ViewPager.a(int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean):void */
    public void setAdapter(a aVar) {
        a aVar2 = this.f2132e;
        if (aVar2 != null) {
            aVar2.setViewPagerObserver(null);
            this.f2132e.startUpdate((ViewGroup) this);
            for (int i2 = 0; i2 < this.f2129b.size(); i2++) {
                f fVar = this.f2129b.get(i2);
                this.f2132e.destroyItem((ViewGroup) this, fVar.f2146b, fVar.f2145a);
            }
            this.f2132e.finishUpdate((ViewGroup) this);
            this.f2129b.clear();
            h();
            this.f2133f = 0;
            scrollTo(0, 0);
        }
        a aVar3 = this.f2132e;
        this.f2132e = aVar;
        this.f2128a = 0;
        if (this.f2132e != null) {
            if (this.l == null) {
                this.l = new l();
            }
            this.f2132e.setViewPagerObserver(this.l);
            this.v = false;
            boolean z2 = this.P;
            this.P = true;
            this.f2128a = this.f2132e.getCount();
            if (this.f2134g >= 0) {
                this.f2132e.restoreState(this.f2135h, this.f2136i);
                a(this.f2134g, false, true);
                this.f2134g = -1;
                this.f2135h = null;
                this.f2136i = null;
            } else if (!z2) {
                e();
            } else {
                requestLayout();
            }
        }
        List<i> list = this.V;
        if (list != null && !list.isEmpty()) {
            int size = this.V.size();
            for (int i3 = 0; i3 < size; i3++) {
                this.V.get(i3).a(this, aVar3, aVar);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(androidx.viewpager.widget.ViewPager$f, int, androidx.viewpager.widget.ViewPager$f):void
      androidx.viewpager.widget.ViewPager.a(int, float, int):void
      androidx.viewpager.widget.ViewPager.a(int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean):void */
    public void setCurrentItem(int i2) {
        this.v = false;
        a(i2, !this.P, false);
    }

    public void setOffscreenPageLimit(int i2) {
        if (i2 < 1) {
            Log.w("ViewPager", "Requested offscreen page limit " + i2 + " too small; defaulting to " + 1);
            i2 = 1;
        }
        if (i2 != this.w) {
            this.w = i2;
            e();
        }
    }

    @Deprecated
    public void setOnPageChangeListener(j jVar) {
        this.T = jVar;
    }

    public void setPageMargin(int i2) {
        int i3 = this.m;
        this.m = i2;
        int width = getWidth();
        a(width, width, i2, i3);
        requestLayout();
    }

    public void setPageMarginDrawable(Drawable drawable) {
        this.n = drawable;
        if (drawable != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable == null);
        invalidate();
    }

    /* access modifiers changed from: package-private */
    public void setScrollState(int i2) {
        if (this.e0 != i2) {
            this.e0 = i2;
            if (this.W != null) {
                b(i2 != 0);
            }
            e(i2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.n;
    }

    public static class g extends ViewGroup.LayoutParams {

        /* renamed from: a  reason: collision with root package name */
        public boolean f2150a;

        /* renamed from: b  reason: collision with root package name */
        public int f2151b;

        /* renamed from: c  reason: collision with root package name */
        float f2152c = Animation.CurveTimeline.LINEAR;

        /* renamed from: d  reason: collision with root package name */
        boolean f2153d;

        /* renamed from: e  reason: collision with root package name */
        int f2154e;

        /* renamed from: f  reason: collision with root package name */
        int f2155f;

        public g() {
            super(-1, -1);
        }

        public g(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, ViewPager.f0);
            this.f2151b = obtainStyledAttributes.getInteger(0, 48);
            obtainStyledAttributes.recycle();
        }
    }

    private void e(int i2) {
        j jVar = this.T;
        if (jVar != null) {
            jVar.onPageScrollStateChanged(i2);
        }
        List<j> list = this.S;
        if (list != null) {
            int size = list.size();
            for (int i3 = 0; i3 < size; i3++) {
                j jVar2 = this.S.get(i3);
                if (jVar2 != null) {
                    jVar2.onPageScrollStateChanged(i2);
                }
            }
        }
        j jVar3 = this.U;
        if (jVar3 != null) {
            jVar3.onPageScrollStateChanged(i2);
        }
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new g(getContext(), attributeSet);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2, boolean z3) {
        a(i2, z2, z3, 0);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2, boolean z3, int i3) {
        a aVar = this.f2132e;
        if (aVar == null || aVar.getCount() <= 0) {
            setScrollingCacheEnabled(false);
        } else if (z3 || this.f2133f != i2 || this.f2129b.size() == 0) {
            boolean z4 = true;
            if (i2 < 0) {
                i2 = 0;
            } else if (i2 >= this.f2132e.getCount()) {
                i2 = this.f2132e.getCount() - 1;
            }
            int i4 = this.w;
            int i5 = this.f2133f;
            if (i2 > i5 + i4 || i2 < i5 - i4) {
                for (int i6 = 0; i6 < this.f2129b.size(); i6++) {
                    this.f2129b.get(i6).f2147c = true;
                }
            }
            if (this.f2133f == i2) {
                z4 = false;
            }
            if (this.P) {
                this.f2133f = i2;
                if (z4) {
                    d(i2);
                }
                requestLayout();
                return;
            }
            c(i2);
            a(i2, z2, i3, z4);
        } else {
            setScrollingCacheEnabled(false);
        }
    }

    public void setPageMarginDrawable(int i2) {
        setPageMarginDrawable(b.e.e.a.c(getContext(), i2));
    }

    class h extends b.e.m.a {
        h() {
        }

        public void a(View view, b.e.m.d0.c cVar) {
            super.a(view, cVar);
            cVar.a((CharSequence) ViewPager.class.getName());
            cVar.c(b());
            if (ViewPager.this.canScrollHorizontally(1)) {
                cVar.a((int) CodedOutputStream.DEFAULT_BUFFER_SIZE);
            }
            if (ViewPager.this.canScrollHorizontally(-1)) {
                cVar.a((int) Utility.DEFAULT_STREAM_BUFFER_SIZE);
            }
        }

        public void b(View view, AccessibilityEvent accessibilityEvent) {
            a aVar;
            super.b(view, accessibilityEvent);
            accessibilityEvent.setClassName(ViewPager.class.getName());
            accessibilityEvent.setScrollable(b());
            if (accessibilityEvent.getEventType() == 4096 && (aVar = ViewPager.this.f2132e) != null) {
                accessibilityEvent.setItemCount(aVar.getCount());
                accessibilityEvent.setFromIndex(ViewPager.this.f2133f);
                accessibilityEvent.setToIndex(ViewPager.this.f2133f);
            }
        }

        private boolean b() {
            a aVar = ViewPager.this.f2132e;
            return aVar != null && aVar.getCount() > 1;
        }

        public boolean a(View view, int i2, Bundle bundle) {
            if (super.a(view, i2, bundle)) {
                return true;
            }
            if (i2 != 4096) {
                if (i2 != 8192 || !ViewPager.this.canScrollHorizontally(-1)) {
                    return false;
                }
                ViewPager viewPager = ViewPager.this;
                viewPager.setCurrentItem(viewPager.f2133f - 1);
                return true;
            } else if (!ViewPager.this.canScrollHorizontally(1)) {
                return false;
            } else {
                ViewPager viewPager2 = ViewPager.this;
                viewPager2.setCurrentItem(viewPager2.f2133f + 1);
                return true;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean):void
     arg types: [int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      androidx.viewpager.widget.ViewPager.a(float, float):boolean
      androidx.viewpager.widget.ViewPager.a(int, int):androidx.viewpager.widget.ViewPager$f
      androidx.viewpager.widget.ViewPager.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean d() {
        a aVar = this.f2132e;
        if (aVar == null || this.f2133f >= aVar.getCount() - 1) {
            return false;
        }
        a(this.f2133f + 1, true);
        return true;
    }

    private void f() {
        this.x = false;
        this.y = false;
        VelocityTracker velocityTracker = this.H;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.H = null;
        }
    }

    /* access modifiers changed from: package-private */
    public f b(View view) {
        for (int i2 = 0; i2 < this.f2129b.size(); i2++) {
            f fVar = this.f2129b.get(i2);
            if (this.f2132e.isViewFromObject(view, fVar.f2145a)) {
                return fVar;
            }
        }
        return null;
    }

    private void a(int i2, boolean z2, int i3, boolean z3) {
        f b2 = b(i2);
        int clientWidth = b2 != null ? (int) (((float) getClientWidth()) * Math.max(this.q, Math.min(b2.f2149e, this.r))) : 0;
        if (z2) {
            a(clientWidth, 0, i3);
            if (z3) {
                d(i2);
                return;
            }
            return;
        }
        if (z3) {
            d(i2);
        }
        a(false);
        scrollTo(clientWidth, 0);
        f(clientWidth);
    }

    /* access modifiers changed from: package-private */
    public f b(int i2) {
        for (int i3 = 0; i3 < this.f2129b.size(); i3++) {
            f fVar = this.f2129b.get(i3);
            if (fVar.f2146b == i2) {
                return fVar;
            }
        }
        return null;
    }

    private void b(int i2, float f2, int i3) {
        j jVar = this.T;
        if (jVar != null) {
            jVar.onPageScrolled(i2, f2, i3);
        }
        List<j> list = this.S;
        if (list != null) {
            int size = list.size();
            for (int i4 = 0; i4 < size; i4++) {
                j jVar2 = this.S.get(i4);
                if (jVar2 != null) {
                    jVar2.onPageScrolled(i2, f2, i3);
                }
            }
        }
        j jVar3 = this.U;
        if (jVar3 != null) {
            jVar3.onPageScrolled(i2, f2, i3);
        }
    }

    public void a(j jVar) {
        if (this.S == null) {
            this.S = new ArrayList();
        }
        this.S.add(jVar);
    }

    private void b(boolean z2) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            getChildAt(i2).setLayerType(z2 ? this.a0 : 0, null);
        }
    }

    /* access modifiers changed from: package-private */
    public float a(float f2) {
        return (float) Math.sin((double) ((f2 - 0.5f) * 0.47123894f));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4) {
        int i5;
        int i6;
        if (getChildCount() == 0) {
            setScrollingCacheEnabled(false);
            return;
        }
        Scroller scroller = this.f2137j;
        if (scroller != null && !scroller.isFinished()) {
            i5 = this.f2138k ? this.f2137j.getCurrX() : this.f2137j.getStartX();
            this.f2137j.abortAnimation();
            setScrollingCacheEnabled(false);
        } else {
            i5 = getScrollX();
        }
        int i7 = i5;
        int scrollY = getScrollY();
        int i8 = i2 - i7;
        int i9 = i3 - scrollY;
        if (i8 == 0 && i9 == 0) {
            a(false);
            e();
            setScrollState(0);
            return;
        }
        setScrollingCacheEnabled(true);
        setScrollState(2);
        int clientWidth = getClientWidth();
        int i10 = clientWidth / 2;
        float f2 = (float) clientWidth;
        float f3 = (float) i10;
        float a2 = f3 + (a(Math.min(1.0f, (((float) Math.abs(i8)) * 1.0f) / f2)) * f3);
        int abs = Math.abs(i4);
        if (abs > 0) {
            i6 = Math.round(Math.abs(a2 / ((float) abs)) * 1000.0f) * 4;
        } else {
            i6 = (int) (((((float) Math.abs(i8)) / ((f2 * this.f2132e.getPageWidth(this.f2133f)) + ((float) this.m))) + 1.0f) * 100.0f);
        }
        int min = Math.min(i6, 600);
        this.f2138k = false;
        this.f2137j.startScroll(i7, scrollY, i8, i9, min);
        u.v(this);
    }

    private boolean b(float f2) {
        boolean z2;
        boolean z3;
        float f3 = this.C - f2;
        this.C = f2;
        float scrollX = ((float) getScrollX()) + f3;
        float clientWidth = (float) getClientWidth();
        float f4 = this.q * clientWidth;
        float f5 = this.r * clientWidth;
        boolean z4 = false;
        f fVar = this.f2129b.get(0);
        ArrayList<f> arrayList = this.f2129b;
        f fVar2 = arrayList.get(arrayList.size() - 1);
        if (fVar.f2146b != 0) {
            f4 = fVar.f2149e * clientWidth;
            z2 = false;
        } else {
            z2 = true;
        }
        if (fVar2.f2146b != this.f2132e.getCount() - 1) {
            f5 = fVar2.f2149e * clientWidth;
            z3 = false;
        } else {
            z3 = true;
        }
        if (scrollX < f4) {
            if (z2) {
                this.N.onPull(Math.abs(f4 - scrollX) / clientWidth);
                z4 = true;
            }
            scrollX = f4;
        } else if (scrollX > f5) {
            if (z3) {
                this.O.onPull(Math.abs(scrollX - f5) / clientWidth);
                z4 = true;
            }
            scrollX = f5;
        }
        int i2 = (int) scrollX;
        this.C += scrollX - ((float) i2);
        scrollTo(i2, getScrollY());
        f(i2);
        return z4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.a.instantiateItem(android.view.ViewGroup, int):java.lang.Object
     arg types: [androidx.viewpager.widget.ViewPager, int]
     candidates:
      androidx.viewpager.widget.a.instantiateItem(android.view.View, int):java.lang.Object
      androidx.viewpager.widget.a.instantiateItem(android.view.ViewGroup, int):java.lang.Object */
    /* access modifiers changed from: package-private */
    public f a(int i2, int i3) {
        f fVar = new f();
        fVar.f2146b = i2;
        fVar.f2145a = this.f2132e.instantiateItem((ViewGroup) this, i2);
        fVar.f2148d = this.f2132e.getPageWidth(i2);
        if (i3 < 0 || i3 >= this.f2129b.size()) {
            this.f2129b.add(fVar);
        } else {
            this.f2129b.add(i3, fVar);
        }
        return fVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.a.destroyItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [androidx.viewpager.widget.ViewPager, int, java.lang.Object]
     candidates:
      androidx.viewpager.widget.a.destroyItem(android.view.View, int, java.lang.Object):void
      androidx.viewpager.widget.a.destroyItem(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(androidx.viewpager.widget.ViewPager$f, int, androidx.viewpager.widget.ViewPager$f):void
      androidx.viewpager.widget.ViewPager.a(int, float, int):void
      androidx.viewpager.widget.ViewPager.a(int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean):void */
    /* access modifiers changed from: package-private */
    public void a() {
        int count = this.f2132e.getCount();
        this.f2128a = count;
        boolean z2 = this.f2129b.size() < (this.w * 2) + 1 && this.f2129b.size() < count;
        int i2 = this.f2133f;
        int i3 = 0;
        boolean z3 = false;
        while (i3 < this.f2129b.size()) {
            f fVar = this.f2129b.get(i3);
            int itemPosition = this.f2132e.getItemPosition(fVar.f2145a);
            if (itemPosition != -1) {
                if (itemPosition == -2) {
                    this.f2129b.remove(i3);
                    i3--;
                    if (!z3) {
                        this.f2132e.startUpdate((ViewGroup) this);
                        z3 = true;
                    }
                    this.f2132e.destroyItem((ViewGroup) this, fVar.f2146b, fVar.f2145a);
                    int i4 = this.f2133f;
                    if (i4 == fVar.f2146b) {
                        i2 = Math.max(0, Math.min(i4, count - 1));
                    }
                } else {
                    int i5 = fVar.f2146b;
                    if (i5 != itemPosition) {
                        if (i5 == this.f2133f) {
                            i2 = itemPosition;
                        }
                        fVar.f2146b = itemPosition;
                    }
                }
                z2 = true;
            }
            i3++;
        }
        if (z3) {
            this.f2132e.finishUpdate((ViewGroup) this);
        }
        Collections.sort(this.f2129b, g0);
        if (z2) {
            int childCount = getChildCount();
            for (int i6 = 0; i6 < childCount; i6++) {
                g gVar = (g) getChildAt(i6).getLayoutParams();
                if (!gVar.f2150a) {
                    gVar.f2152c = Animation.CurveTimeline.LINEAR;
                }
            }
            a(i2, false, true);
            requestLayout();
        }
    }

    private static boolean c(View view) {
        return view.getClass().getAnnotation(e.class) != null;
    }

    private void c(boolean z2) {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(z2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean):void
     arg types: [int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      androidx.viewpager.widget.ViewPager.a(float, float):boolean
      androidx.viewpager.widget.ViewPager.a(int, int):androidx.viewpager.widget.ViewPager$f
      androidx.viewpager.widget.ViewPager.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean c() {
        int i2 = this.f2133f;
        if (i2 <= 0) {
            return false;
        }
        a(i2 - 1, true);
        return true;
    }

    private void a(f fVar, int i2, f fVar2) {
        int i3;
        int i4;
        f fVar3;
        f fVar4;
        int count = this.f2132e.getCount();
        int clientWidth = getClientWidth();
        float f2 = clientWidth > 0 ? ((float) this.m) / ((float) clientWidth) : Animation.CurveTimeline.LINEAR;
        if (fVar2 != null) {
            int i5 = fVar2.f2146b;
            int i6 = fVar.f2146b;
            if (i5 < i6) {
                int i7 = 0;
                float f3 = fVar2.f2149e + fVar2.f2148d + f2;
                while (true) {
                    i5++;
                    if (i5 > fVar.f2146b || i7 >= this.f2129b.size()) {
                        break;
                    }
                    f fVar5 = this.f2129b.get(i7);
                    while (true) {
                        fVar4 = fVar5;
                        if (i5 > fVar4.f2146b && i7 < this.f2129b.size() - 1) {
                            i7++;
                            fVar5 = this.f2129b.get(i7);
                        }
                    }
                    while (i5 < fVar4.f2146b) {
                        f3 += this.f2132e.getPageWidth(i5) + f2;
                        i5++;
                    }
                    fVar4.f2149e = f3;
                    f3 += fVar4.f2148d + f2;
                }
            } else if (i5 > i6) {
                int size = this.f2129b.size() - 1;
                float f4 = fVar2.f2149e;
                while (true) {
                    int i8 = i5 - 1;
                    if (i8 < fVar.f2146b || size < 0) {
                        break;
                    }
                    f fVar6 = this.f2129b.get(size);
                    while (true) {
                        fVar3 = fVar6;
                        if (i8 < fVar3.f2146b && size > 0) {
                            size--;
                            fVar6 = this.f2129b.get(size);
                        }
                    }
                    while (i5 > fVar3.f2146b) {
                        f4 -= this.f2132e.getPageWidth(i5) + f2;
                        i8 = i5 - 1;
                    }
                    f4 -= fVar3.f2148d + f2;
                    fVar3.f2149e = f4;
                }
            }
        }
        int size2 = this.f2129b.size();
        float f5 = fVar.f2149e;
        int i9 = fVar.f2146b;
        int i10 = i9 - 1;
        this.q = i9 == 0 ? f5 : -3.4028235E38f;
        int i11 = count - 1;
        this.r = fVar.f2146b == i11 ? (fVar.f2149e + fVar.f2148d) - 1.0f : Float.MAX_VALUE;
        int i12 = i2 - 1;
        while (i12 >= 0) {
            f fVar7 = this.f2129b.get(i12);
            while (true) {
                i4 = fVar7.f2146b;
                if (i10 <= i4) {
                    break;
                }
                f5 -= this.f2132e.getPageWidth(i10) + f2;
                i10--;
            }
            f5 -= fVar7.f2148d + f2;
            fVar7.f2149e = f5;
            if (i4 == 0) {
                this.q = f5;
            }
            i12--;
            i10--;
        }
        float f6 = fVar.f2149e + fVar.f2148d + f2;
        int i13 = fVar.f2146b + 1;
        int i14 = i2 + 1;
        while (i14 < size2) {
            f fVar8 = this.f2129b.get(i14);
            while (true) {
                i3 = fVar8.f2146b;
                if (i13 >= i3) {
                    break;
                }
                f6 += this.f2132e.getPageWidth(i13) + f2;
                i13++;
            }
            if (i3 == i11) {
                this.r = (fVar8.f2148d + f6) - 1.0f;
            }
            fVar8.f2149e = f6;
            f6 += fVar8.f2148d + f2;
            i14++;
            i13++;
        }
    }

    /* access modifiers changed from: package-private */
    public f a(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent == this) {
                return b(view);
            }
            if (parent == null || !(parent instanceof View)) {
                return null;
            }
            view = (View) parent;
        }
    }

    private void a(int i2, int i3, int i4, int i5) {
        if (i3 <= 0 || this.f2129b.isEmpty()) {
            f b2 = b(this.f2133f);
            int min = (int) ((b2 != null ? Math.min(b2.f2149e, this.r) : Animation.CurveTimeline.LINEAR) * ((float) ((i2 - getPaddingLeft()) - getPaddingRight())));
            if (min != getScrollX()) {
                a(false);
                scrollTo(min, getScrollY());
            }
        } else if (!this.f2137j.isFinished()) {
            this.f2137j.setFinalX(getCurrentItem() * getClientWidth());
        } else {
            scrollTo((int) ((((float) getScrollX()) / ((float) (((i3 - getPaddingLeft()) - getPaddingRight()) + i5))) * ((float) (((i2 - getPaddingLeft()) - getPaddingRight()) + i4))), getScrollY());
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0066  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r13, float r14, int r15) {
        /*
            r12 = this;
            int r0 = r12.R
            r1 = 0
            r2 = 1
            if (r0 <= 0) goto L_0x006d
            int r0 = r12.getScrollX()
            int r3 = r12.getPaddingLeft()
            int r4 = r12.getPaddingRight()
            int r5 = r12.getWidth()
            int r6 = r12.getChildCount()
            r7 = r4
            r4 = r3
            r3 = 0
        L_0x001d:
            if (r3 >= r6) goto L_0x006d
            android.view.View r8 = r12.getChildAt(r3)
            android.view.ViewGroup$LayoutParams r9 = r8.getLayoutParams()
            androidx.viewpager.widget.ViewPager$g r9 = (androidx.viewpager.widget.ViewPager.g) r9
            boolean r10 = r9.f2150a
            if (r10 != 0) goto L_0x002e
            goto L_0x006a
        L_0x002e:
            int r9 = r9.f2151b
            r9 = r9 & 7
            if (r9 == r2) goto L_0x004f
            r10 = 3
            if (r9 == r10) goto L_0x0049
            r10 = 5
            if (r9 == r10) goto L_0x003c
            r9 = r4
            goto L_0x005e
        L_0x003c:
            int r9 = r5 - r7
            int r10 = r8.getMeasuredWidth()
            int r9 = r9 - r10
            int r10 = r8.getMeasuredWidth()
            int r7 = r7 + r10
            goto L_0x005b
        L_0x0049:
            int r9 = r8.getWidth()
            int r9 = r9 + r4
            goto L_0x005e
        L_0x004f:
            int r9 = r8.getMeasuredWidth()
            int r9 = r5 - r9
            int r9 = r9 / 2
            int r9 = java.lang.Math.max(r9, r4)
        L_0x005b:
            r11 = r9
            r9 = r4
            r4 = r11
        L_0x005e:
            int r4 = r4 + r0
            int r10 = r8.getLeft()
            int r4 = r4 - r10
            if (r4 == 0) goto L_0x0069
            r8.offsetLeftAndRight(r4)
        L_0x0069:
            r4 = r9
        L_0x006a:
            int r3 = r3 + 1
            goto L_0x001d
        L_0x006d:
            r12.b(r13, r14, r15)
            androidx.viewpager.widget.ViewPager$k r13 = r12.W
            if (r13 == 0) goto L_0x00a1
            int r13 = r12.getScrollX()
            int r14 = r12.getChildCount()
        L_0x007c:
            if (r1 >= r14) goto L_0x00a1
            android.view.View r15 = r12.getChildAt(r1)
            android.view.ViewGroup$LayoutParams r0 = r15.getLayoutParams()
            androidx.viewpager.widget.ViewPager$g r0 = (androidx.viewpager.widget.ViewPager.g) r0
            boolean r0 = r0.f2150a
            if (r0 == 0) goto L_0x008d
            goto L_0x009e
        L_0x008d:
            int r0 = r15.getLeft()
            int r0 = r0 - r13
            float r0 = (float) r0
            int r3 = r12.getClientWidth()
            float r3 = (float) r3
            float r0 = r0 / r3
            androidx.viewpager.widget.ViewPager$k r3 = r12.W
            r3.a(r15, r0)
        L_0x009e:
            int r1 = r1 + 1
            goto L_0x007c
        L_0x00a1:
            r12.Q = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.a(int, float, int):void");
    }

    private void a(boolean z2) {
        boolean z3 = this.e0 == 2;
        if (z3) {
            setScrollingCacheEnabled(false);
            if (!this.f2137j.isFinished()) {
                this.f2137j.abortAnimation();
                int scrollX = getScrollX();
                int scrollY = getScrollY();
                int currX = this.f2137j.getCurrX();
                int currY = this.f2137j.getCurrY();
                if (!(scrollX == currX && scrollY == currY)) {
                    scrollTo(currX, currY);
                    if (currX != scrollX) {
                        f(currX);
                    }
                }
            }
        }
        this.v = false;
        boolean z4 = z3;
        for (int i2 = 0; i2 < this.f2129b.size(); i2++) {
            f fVar = this.f2129b.get(i2);
            if (fVar.f2147c) {
                fVar.f2147c = false;
                z4 = true;
            }
        }
        if (!z4) {
            return;
        }
        if (z2) {
            u.a(this, this.d0);
        } else {
            this.d0.run();
        }
    }

    private boolean a(float f2, float f3) {
        return (f2 < ((float) this.A) && f3 > Animation.CurveTimeline.LINEAR) || (f2 > ((float) (getWidth() - this.A)) && f3 < Animation.CurveTimeline.LINEAR);
    }

    private int a(int i2, float f2, int i3, int i4) {
        if (Math.abs(i4) <= this.K || Math.abs(i3) <= this.I) {
            i2 += (int) (f2 + (i2 >= this.f2133f ? 0.4f : 0.6f));
        } else if (i3 <= 0) {
            i2++;
        }
        if (this.f2129b.size() <= 0) {
            return i2;
        }
        ArrayList<f> arrayList = this.f2129b;
        return Math.max(this.f2129b.get(0).f2146b, Math.min(i2, arrayList.get(arrayList.size() - 1).f2146b));
    }

    private void a(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.G) {
            int i2 = actionIndex == 0 ? 1 : 0;
            this.C = motionEvent.getX(i2);
            this.G = motionEvent.getPointerId(i2);
            VelocityTracker velocityTracker = this.H;
            if (velocityTracker != null) {
                velocityTracker.clear();
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(View view, boolean z2, int i2, int i3, int i4) {
        int i5;
        View view2 = view;
        if (view2 instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view2;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                int i6 = i3 + scrollX;
                if (i6 >= childAt.getLeft() && i6 < childAt.getRight() && (i5 = i4 + scrollY) >= childAt.getTop() && i5 < childAt.getBottom()) {
                    if (a(childAt, true, i2, i6 - childAt.getLeft(), i5 - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        if (!z2 || !view.canScrollHorizontally(-i2)) {
            return false;
        }
        return true;
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            int keyCode = keyEvent.getKeyCode();
            if (keyCode != 21) {
                if (keyCode != 22) {
                    if (keyCode == 61) {
                        if (keyEvent.hasNoModifiers()) {
                            return a(2);
                        }
                        if (keyEvent.hasModifiers(1)) {
                            return a(1);
                        }
                    }
                } else if (keyEvent.hasModifiers(2)) {
                    return d();
                } else {
                    return a(66);
                }
            } else if (keyEvent.hasModifiers(2)) {
                return c();
            } else {
                return a(17);
            }
        }
        return false;
    }

    public boolean a(int i2) {
        boolean requestFocus;
        boolean z2;
        View findFocus = findFocus();
        boolean z3 = false;
        View view = null;
        if (findFocus != this) {
            if (findFocus != null) {
                ViewParent parent = findFocus.getParent();
                while (true) {
                    if (!(parent instanceof ViewGroup)) {
                        z2 = false;
                        break;
                    } else if (parent == this) {
                        z2 = true;
                        break;
                    } else {
                        parent = parent.getParent();
                    }
                }
                if (!z2) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(findFocus.getClass().getSimpleName());
                    for (ViewParent parent2 = findFocus.getParent(); parent2 instanceof ViewGroup; parent2 = parent2.getParent()) {
                        sb.append(" => ");
                        sb.append(parent2.getClass().getSimpleName());
                    }
                    Log.e("ViewPager", "arrowScroll tried to find focus based on non-child current focused view " + sb.toString());
                }
            }
            view = findFocus;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, view, i2);
        if (findNextFocus != null && findNextFocus != view) {
            if (i2 == 17) {
                int i3 = a(this.f2131d, findNextFocus).left;
                int i4 = a(this.f2131d, view).left;
                if (view == null || i3 < i4) {
                    requestFocus = findNextFocus.requestFocus();
                } else {
                    requestFocus = c();
                }
            } else if (i2 == 66) {
                int i5 = a(this.f2131d, findNextFocus).left;
                int i6 = a(this.f2131d, view).left;
                if (view == null || i5 > i6) {
                    requestFocus = findNextFocus.requestFocus();
                } else {
                    requestFocus = d();
                }
            }
            z3 = requestFocus;
        } else if (i2 == 17 || i2 == 1) {
            z3 = c();
        } else if (i2 == 66 || i2 == 2) {
            z3 = d();
        }
        if (z3) {
            playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i2));
        }
        return z3;
    }

    private Rect a(Rect rect, View view) {
        if (rect == null) {
            rect = new Rect();
        }
        if (view == null) {
            rect.set(0, 0, 0, 0);
            return rect;
        }
        rect.left = view.getLeft();
        rect.right = view.getRight();
        rect.top = view.getTop();
        rect.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect.left += viewGroup.getLeft();
            rect.right += viewGroup.getRight();
            rect.top += viewGroup.getTop();
            rect.bottom += viewGroup.getBottom();
            parent = viewGroup.getParent();
        }
        return rect;
    }
}
