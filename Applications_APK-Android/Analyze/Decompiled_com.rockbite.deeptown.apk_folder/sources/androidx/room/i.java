package androidx.room;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Looper;
import android.util.Log;
import b.m.a.c;
import b.m.a.e;
import b.m.a.f;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* compiled from: RoomDatabase */
public abstract class i {
    @Deprecated

    /* renamed from: a  reason: collision with root package name */
    protected volatile b.m.a.b f2019a;

    /* renamed from: b  reason: collision with root package name */
    private Executor f2020b;

    /* renamed from: c  reason: collision with root package name */
    private b.m.a.c f2021c;

    /* renamed from: d  reason: collision with root package name */
    private final f f2022d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f2023e;

    /* renamed from: f  reason: collision with root package name */
    boolean f2024f;
    @Deprecated

    /* renamed from: g  reason: collision with root package name */
    protected List<b> f2025g;

    /* renamed from: h  reason: collision with root package name */
    private final ReentrantReadWriteLock f2026h = new ReentrantReadWriteLock();

    /* renamed from: i  reason: collision with root package name */
    private final ThreadLocal<Integer> f2027i = new ThreadLocal<>();

    /* compiled from: RoomDatabase */
    public static class a<T extends i> {

        /* renamed from: a  reason: collision with root package name */
        private final Class<T> f2028a;

        /* renamed from: b  reason: collision with root package name */
        private final String f2029b;

        /* renamed from: c  reason: collision with root package name */
        private final Context f2030c;

        /* renamed from: d  reason: collision with root package name */
        private ArrayList<b> f2031d;

        /* renamed from: e  reason: collision with root package name */
        private Executor f2032e;

        /* renamed from: f  reason: collision with root package name */
        private Executor f2033f;

        /* renamed from: g  reason: collision with root package name */
        private c.C0063c f2034g;

        /* renamed from: h  reason: collision with root package name */
        private boolean f2035h;

        /* renamed from: i  reason: collision with root package name */
        private c f2036i = c.AUTOMATIC;

        /* renamed from: j  reason: collision with root package name */
        private boolean f2037j;

        /* renamed from: k  reason: collision with root package name */
        private boolean f2038k = true;
        private boolean l;
        private final d m = new d();
        private Set<Integer> n;
        private Set<Integer> o;
        private String p;
        private File q;

        a(Context context, Class<T> cls, String str) {
            this.f2030c = context;
            this.f2028a = cls;
            this.f2029b = str;
        }

        public a<T> a(c.C0063c cVar) {
            this.f2034g = cVar;
            return this;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        @android.annotation.SuppressLint({"RestrictedApi"})
        public T b() {
            /*
                r21 = this;
                r0 = r21
                android.content.Context r1 = r0.f2030c
                if (r1 == 0) goto L_0x00ed
                java.lang.Class<T> r1 = r0.f2028a
                if (r1 == 0) goto L_0x00e5
                java.util.concurrent.Executor r1 = r0.f2032e
                if (r1 != 0) goto L_0x001b
                java.util.concurrent.Executor r1 = r0.f2033f
                if (r1 != 0) goto L_0x001b
                java.util.concurrent.Executor r1 = b.b.a.a.a.b()
                r0.f2033f = r1
                r0.f2032e = r1
                goto L_0x0030
            L_0x001b:
                java.util.concurrent.Executor r1 = r0.f2032e
                if (r1 == 0) goto L_0x0026
                java.util.concurrent.Executor r2 = r0.f2033f
                if (r2 != 0) goto L_0x0026
                r0.f2033f = r1
                goto L_0x0030
            L_0x0026:
                java.util.concurrent.Executor r1 = r0.f2032e
                if (r1 != 0) goto L_0x0030
                java.util.concurrent.Executor r1 = r0.f2033f
                if (r1 == 0) goto L_0x0030
                r0.f2032e = r1
            L_0x0030:
                java.util.Set<java.lang.Integer> r1 = r0.o
                if (r1 == 0) goto L_0x0068
                java.util.Set<java.lang.Integer> r2 = r0.n
                if (r2 == 0) goto L_0x0068
                java.util.Iterator r1 = r1.iterator()
            L_0x003c:
                boolean r2 = r1.hasNext()
                if (r2 == 0) goto L_0x0068
                java.lang.Object r2 = r1.next()
                java.lang.Integer r2 = (java.lang.Integer) r2
                java.util.Set<java.lang.Integer> r3 = r0.n
                boolean r3 = r3.contains(r2)
                if (r3 != 0) goto L_0x0051
                goto L_0x003c
            L_0x0051:
                java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "Inconsistency detected. A Migration was supplied to addMigration(Migration... migrations) that has a start or end version equal to a start version supplied to fallbackToDestructiveMigrationFrom(int... startVersions). Start version: "
                r3.append(r4)
                r3.append(r2)
                java.lang.String r2 = r3.toString()
                r1.<init>(r2)
                throw r1
            L_0x0068:
                b.m.a.c$c r1 = r0.f2034g
                if (r1 != 0) goto L_0x0073
                b.m.a.g.c r1 = new b.m.a.g.c
                r1.<init>()
                r0.f2034g = r1
            L_0x0073:
                java.lang.String r1 = r0.p
                if (r1 != 0) goto L_0x007b
                java.io.File r1 = r0.q
                if (r1 == 0) goto L_0x009d
            L_0x007b:
                java.lang.String r1 = r0.f2029b
                if (r1 == 0) goto L_0x00dd
                java.lang.String r1 = r0.p
                if (r1 == 0) goto L_0x0090
                java.io.File r1 = r0.q
                if (r1 != 0) goto L_0x0088
                goto L_0x0090
            L_0x0088:
                java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
                java.lang.String r2 = "Both createFromAsset() and createFromFile() was called on this Builder but the database can only be created using one of the two configurations."
                r1.<init>(r2)
                throw r1
            L_0x0090:
                androidx.room.n r1 = new androidx.room.n
                java.lang.String r2 = r0.p
                java.io.File r3 = r0.q
                b.m.a.c$c r4 = r0.f2034g
                r1.<init>(r2, r3, r4)
                r0.f2034g = r1
            L_0x009d:
                androidx.room.a r1 = new androidx.room.a
                android.content.Context r6 = r0.f2030c
                java.lang.String r7 = r0.f2029b
                b.m.a.c$c r8 = r0.f2034g
                androidx.room.i$d r9 = r0.m
                java.util.ArrayList<androidx.room.i$b> r10 = r0.f2031d
                boolean r11 = r0.f2035h
                androidx.room.i$c r2 = r0.f2036i
                androidx.room.i$c r12 = r2.a(r6)
                java.util.concurrent.Executor r13 = r0.f2032e
                java.util.concurrent.Executor r14 = r0.f2033f
                boolean r15 = r0.f2037j
                boolean r2 = r0.f2038k
                boolean r3 = r0.l
                java.util.Set<java.lang.Integer> r4 = r0.n
                java.lang.String r5 = r0.p
                r18 = r4
                java.io.File r4 = r0.q
                r19 = r5
                r5 = r1
                r16 = r2
                r17 = r3
                r20 = r4
                r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
                java.lang.Class<T> r2 = r0.f2028a
                java.lang.String r3 = "_Impl"
                java.lang.Object r2 = androidx.room.h.a(r2, r3)
                androidx.room.i r2 = (androidx.room.i) r2
                r2.b(r1)
                return r2
            L_0x00dd:
                java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
                java.lang.String r2 = "Cannot create from asset or file for an in-memory database."
                r1.<init>(r2)
                throw r1
            L_0x00e5:
                java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
                java.lang.String r2 = "Must provide an abstract class that extends RoomDatabase"
                r1.<init>(r2)
                throw r1
            L_0x00ed:
                java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
                java.lang.String r2 = "Cannot provide null context for the database."
                r1.<init>(r2)
                goto L_0x00f6
            L_0x00f5:
                throw r1
            L_0x00f6:
                goto L_0x00f5
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.room.i.a.b():androidx.room.i");
        }

        public a<T> c() {
            this.f2038k = false;
            this.l = true;
            return this;
        }

        public a<T> a(androidx.room.q.a... aVarArr) {
            if (this.o == null) {
                this.o = new HashSet();
            }
            for (androidx.room.q.a aVar : aVarArr) {
                this.o.add(Integer.valueOf(aVar.f2078a));
                this.o.add(Integer.valueOf(aVar.f2079b));
            }
            this.m.a(aVarArr);
            return this;
        }

        public a<T> a() {
            this.f2035h = true;
            return this;
        }

        public a<T> a(Executor executor) {
            this.f2032e = executor;
            return this;
        }

        public a<T> a(b bVar) {
            if (this.f2031d == null) {
                this.f2031d = new ArrayList<>();
            }
            this.f2031d.add(bVar);
            return this;
        }
    }

    /* compiled from: RoomDatabase */
    public static abstract class b {
        public void a(b.m.a.b bVar) {
        }

        public void b(b.m.a.b bVar) {
        }

        public void c(b.m.a.b bVar) {
        }
    }

    public i() {
        new ConcurrentHashMap();
        this.f2022d = d();
    }

    private static boolean l() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    /* access modifiers changed from: protected */
    public abstract b.m.a.c a(a aVar);

    public void a() {
        if (!this.f2023e && l()) {
            throw new IllegalStateException("Cannot access database on the main thread since it may potentially lock the UI for a long period of time.");
        }
    }

    public void b(a aVar) {
        this.f2021c = a(aVar);
        b.m.a.c cVar = this.f2021c;
        if (cVar instanceof m) {
            ((m) cVar).a(aVar);
        }
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 16) {
            if (aVar.f1971g == c.WRITE_AHEAD_LOGGING) {
                z = true;
            }
            this.f2021c.b(z);
        }
        this.f2025g = aVar.f1969e;
        this.f2020b = aVar.f1972h;
        new p(aVar.f1973i);
        this.f2023e = aVar.f1970f;
        this.f2024f = z;
        if (aVar.f1974j) {
            this.f2022d.a(aVar.f1966b, aVar.f1967c);
        }
    }

    @Deprecated
    public void c() {
        a();
        b.m.a.b H = this.f2021c.H();
        this.f2022d.b(H);
        H.J();
    }

    /* access modifiers changed from: protected */
    public abstract f d();

    @Deprecated
    public void e() {
        this.f2021c.H().M();
        if (!i()) {
            this.f2022d.b();
        }
    }

    /* access modifiers changed from: package-private */
    public Lock f() {
        return this.f2026h.readLock();
    }

    public b.m.a.c g() {
        return this.f2021c;
    }

    public Executor h() {
        return this.f2020b;
    }

    public boolean i() {
        return this.f2021c.H().O();
    }

    public boolean j() {
        b.m.a.b bVar = this.f2019a;
        return bVar != null && bVar.isOpen();
    }

    @Deprecated
    public void k() {
        this.f2021c.H().L();
    }

    /* compiled from: RoomDatabase */
    public static class d {

        /* renamed from: a  reason: collision with root package name */
        private HashMap<Integer, TreeMap<Integer, androidx.room.q.a>> f2043a = new HashMap<>();

        public void a(androidx.room.q.a... aVarArr) {
            for (androidx.room.q.a a2 : aVarArr) {
                a(a2);
            }
        }

        private void a(androidx.room.q.a aVar) {
            int i2 = aVar.f2078a;
            int i3 = aVar.f2079b;
            TreeMap treeMap = this.f2043a.get(Integer.valueOf(i2));
            if (treeMap == null) {
                treeMap = new TreeMap();
                this.f2043a.put(Integer.valueOf(i2), treeMap);
            }
            androidx.room.q.a aVar2 = (androidx.room.q.a) treeMap.get(Integer.valueOf(i3));
            if (aVar2 != null) {
                Log.w("ROOM", "Overriding migration " + aVar2 + " with " + aVar);
            }
            treeMap.put(Integer.valueOf(i3), aVar);
        }

        public List<androidx.room.q.a> a(int i2, int i3) {
            if (i2 == i3) {
                return Collections.emptyList();
            }
            return a(new ArrayList(), i3 > i2, i2, i3);
        }

        private List<androidx.room.q.a> a(List<androidx.room.q.a> list, boolean z, int i2, int i3) {
            Set set;
            boolean z2;
            do {
                if (z) {
                    if (i2 >= i3) {
                        return list;
                    }
                } else if (i2 <= i3) {
                    return list;
                }
                TreeMap treeMap = this.f2043a.get(Integer.valueOf(i2));
                if (treeMap != null) {
                    if (z) {
                        set = treeMap.descendingKeySet();
                    } else {
                        set = treeMap.keySet();
                    }
                    Iterator it = set.iterator();
                    while (true) {
                        z2 = true;
                        boolean z3 = false;
                        if (!it.hasNext()) {
                            z2 = false;
                            continue;
                            break;
                        }
                        int intValue = ((Integer) it.next()).intValue();
                        if (!z ? !(intValue < i3 || intValue >= i2) : !(intValue > i3 || intValue <= i2)) {
                            z3 = true;
                            continue;
                        }
                        if (z3) {
                            list.add(treeMap.get(Integer.valueOf(intValue)));
                            i2 = intValue;
                            continue;
                            break;
                        }
                    }
                } else {
                    return null;
                }
            } while (z2);
            return null;
        }
    }

    public Cursor a(e eVar) {
        return a(eVar, null);
    }

    public Cursor a(e eVar, CancellationSignal cancellationSignal) {
        a();
        b();
        if (cancellationSignal == null || Build.VERSION.SDK_INT < 16) {
            return this.f2021c.H().a(eVar);
        }
        return this.f2021c.H().a(eVar, cancellationSignal);
    }

    /* compiled from: RoomDatabase */
    public enum c {
        AUTOMATIC,
        TRUNCATE,
        WRITE_AHEAD_LOGGING;

        /* access modifiers changed from: package-private */
        @SuppressLint({"NewApi"})
        public c a(Context context) {
            ActivityManager activityManager;
            if (this != AUTOMATIC) {
                return this;
            }
            if (Build.VERSION.SDK_INT < 16 || (activityManager = (ActivityManager) context.getSystemService("activity")) == null || a(activityManager)) {
                return TRUNCATE;
            }
            return WRITE_AHEAD_LOGGING;
        }

        private static boolean a(ActivityManager activityManager) {
            if (Build.VERSION.SDK_INT >= 19) {
                return activityManager.isLowRamDevice();
            }
            return false;
        }
    }

    public f a(String str) {
        a();
        b();
        return this.f2021c.H().f(str);
    }

    /* access modifiers changed from: protected */
    public void a(b.m.a.b bVar) {
        this.f2022d.a(bVar);
    }

    public void b() {
        if (!i() && this.f2027i.get() != null) {
            throw new IllegalStateException("Cannot access database on a different coroutine context inherited from a suspending transaction.");
        }
    }
}
