package androidx.room.r;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: CopyLock */
public class a {

    /* renamed from: e  reason: collision with root package name */
    private static final Map<String, Lock> f2080e = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    private final File f2081a;

    /* renamed from: b  reason: collision with root package name */
    private final Lock f2082b = a(this.f2081a.getAbsolutePath());

    /* renamed from: c  reason: collision with root package name */
    private final boolean f2083c;

    /* renamed from: d  reason: collision with root package name */
    private FileChannel f2084d;

    public a(String str, File file, boolean z) {
        this.f2081a = new File(file, str + ".lck");
        this.f2083c = z;
    }

    public void a() {
        this.f2082b.lock();
        if (this.f2083c) {
            try {
                this.f2084d = new FileOutputStream(this.f2081a).getChannel();
                this.f2084d.lock();
            } catch (IOException e2) {
                throw new IllegalStateException("Unable to grab copy lock.", e2);
            }
        }
    }

    public void b() {
        FileChannel fileChannel = this.f2084d;
        if (fileChannel != null) {
            try {
                fileChannel.close();
            } catch (IOException unused) {
            }
        }
        this.f2082b.unlock();
    }

    private static Lock a(String str) {
        Lock lock;
        synchronized (f2080e) {
            lock = f2080e.get(str);
            if (lock == null) {
                lock = new ReentrantLock();
                f2080e.put(str, lock);
            }
        }
        return lock;
    }
}
