package androidx.room.r;

import android.database.Cursor;
import android.os.Build;
import com.appsflyer.share.Constants;
import com.tapjoy.TJAdUnitConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/* compiled from: TableInfo */
public class f {

    /* renamed from: a  reason: collision with root package name */
    public final String f2085a;

    /* renamed from: b  reason: collision with root package name */
    public final Map<String, a> f2086b;

    /* renamed from: c  reason: collision with root package name */
    public final Set<b> f2087c;

    /* renamed from: d  reason: collision with root package name */
    public final Set<d> f2088d;

    /* compiled from: TableInfo */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public final String f2096a;

        /* renamed from: b  reason: collision with root package name */
        public final String f2097b;

        /* renamed from: c  reason: collision with root package name */
        public final String f2098c;

        /* renamed from: d  reason: collision with root package name */
        public final List<String> f2099d;

        /* renamed from: e  reason: collision with root package name */
        public final List<String> f2100e;

        public b(String str, String str2, String str3, List<String> list, List<String> list2) {
            this.f2096a = str;
            this.f2097b = str2;
            this.f2098c = str3;
            this.f2099d = Collections.unmodifiableList(list);
            this.f2100e = Collections.unmodifiableList(list2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.f2096a.equals(bVar.f2096a) && this.f2097b.equals(bVar.f2097b) && this.f2098c.equals(bVar.f2098c) && this.f2099d.equals(bVar.f2099d)) {
                return this.f2100e.equals(bVar.f2100e);
            }
            return false;
        }

        public int hashCode() {
            return (((((((this.f2096a.hashCode() * 31) + this.f2097b.hashCode()) * 31) + this.f2098c.hashCode()) * 31) + this.f2099d.hashCode()) * 31) + this.f2100e.hashCode();
        }

        public String toString() {
            return "ForeignKey{referenceTable='" + this.f2096a + '\'' + ", onDelete='" + this.f2097b + '\'' + ", onUpdate='" + this.f2098c + '\'' + ", columnNames=" + this.f2099d + ", referenceColumnNames=" + this.f2100e + '}';
        }
    }

    /* compiled from: TableInfo */
    static class c implements Comparable<c> {

        /* renamed from: a  reason: collision with root package name */
        final int f2101a;

        /* renamed from: b  reason: collision with root package name */
        final int f2102b;

        /* renamed from: c  reason: collision with root package name */
        final String f2103c;

        /* renamed from: d  reason: collision with root package name */
        final String f2104d;

        c(int i2, int i3, String str, String str2) {
            this.f2101a = i2;
            this.f2102b = i3;
            this.f2103c = str;
            this.f2104d = str2;
        }

        /* renamed from: a */
        public int compareTo(c cVar) {
            int i2 = this.f2101a - cVar.f2101a;
            return i2 == 0 ? this.f2102b - cVar.f2102b : i2;
        }
    }

    /* compiled from: TableInfo */
    public static class d {

        /* renamed from: a  reason: collision with root package name */
        public final String f2105a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f2106b;

        /* renamed from: c  reason: collision with root package name */
        public final List<String> f2107c;

        public d(String str, boolean z, List<String> list) {
            this.f2105a = str;
            this.f2106b = z;
            this.f2107c = list;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || d.class != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            if (this.f2106b != dVar.f2106b || !this.f2107c.equals(dVar.f2107c)) {
                return false;
            }
            if (this.f2105a.startsWith("index_")) {
                return dVar.f2105a.startsWith("index_");
            }
            return this.f2105a.equals(dVar.f2105a);
        }

        public int hashCode() {
            int i2;
            if (this.f2105a.startsWith("index_")) {
                i2 = "index_".hashCode();
            } else {
                i2 = this.f2105a.hashCode();
            }
            return (((i2 * 31) + (this.f2106b ? 1 : 0)) * 31) + this.f2107c.hashCode();
        }

        public String toString() {
            return "Index{name='" + this.f2105a + '\'' + ", unique=" + this.f2106b + ", columns=" + this.f2107c + '}';
        }
    }

    public f(String str, Map<String, a> map, Set<b> set, Set<d> set2) {
        Set<d> set3;
        this.f2085a = str;
        this.f2086b = Collections.unmodifiableMap(map);
        this.f2087c = Collections.unmodifiableSet(set);
        if (set2 == null) {
            set3 = null;
        } else {
            set3 = Collections.unmodifiableSet(set2);
        }
        this.f2088d = set3;
    }

    public static f a(b.m.a.b bVar, String str) {
        return new f(str, b(bVar, str), c(bVar, str), d(bVar, str));
    }

    private static Map<String, a> b(b.m.a.b bVar, String str) {
        Cursor g2 = bVar.g("PRAGMA table_info(`" + str + "`)");
        HashMap hashMap = new HashMap();
        try {
            if (g2.getColumnCount() > 0) {
                int columnIndex = g2.getColumnIndex("name");
                int columnIndex2 = g2.getColumnIndex("type");
                int columnIndex3 = g2.getColumnIndex("notnull");
                int columnIndex4 = g2.getColumnIndex("pk");
                int columnIndex5 = g2.getColumnIndex("dflt_value");
                while (g2.moveToNext()) {
                    String string = g2.getString(columnIndex);
                    hashMap.put(string, new a(string, g2.getString(columnIndex2), g2.getInt(columnIndex3) != 0, g2.getInt(columnIndex4), g2.getString(columnIndex5), 2));
                }
            }
            return hashMap;
        } finally {
            g2.close();
        }
    }

    private static Set<b> c(b.m.a.b bVar, String str) {
        HashSet hashSet = new HashSet();
        Cursor g2 = bVar.g("PRAGMA foreign_key_list(`" + str + "`)");
        try {
            int columnIndex = g2.getColumnIndex("id");
            int columnIndex2 = g2.getColumnIndex("seq");
            int columnIndex3 = g2.getColumnIndex("table");
            int columnIndex4 = g2.getColumnIndex("on_delete");
            int columnIndex5 = g2.getColumnIndex("on_update");
            List<c> a2 = a(g2);
            int count = g2.getCount();
            for (int i2 = 0; i2 < count; i2++) {
                g2.moveToPosition(i2);
                if (g2.getInt(columnIndex2) == 0) {
                    int i3 = g2.getInt(columnIndex);
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    for (c next : a2) {
                        if (next.f2101a == i3) {
                            arrayList.add(next.f2103c);
                            arrayList2.add(next.f2104d);
                        }
                    }
                    hashSet.add(new b(g2.getString(columnIndex3), g2.getString(columnIndex4), g2.getString(columnIndex5), arrayList, arrayList2));
                }
            }
            return hashSet;
        } finally {
            g2.close();
        }
    }

    private static Set<d> d(b.m.a.b bVar, String str) {
        Cursor g2 = bVar.g("PRAGMA index_list(`" + str + "`)");
        try {
            int columnIndex = g2.getColumnIndex("name");
            int columnIndex2 = g2.getColumnIndex("origin");
            int columnIndex3 = g2.getColumnIndex("unique");
            if (!(columnIndex == -1 || columnIndex2 == -1)) {
                if (columnIndex3 != -1) {
                    HashSet hashSet = new HashSet();
                    while (g2.moveToNext()) {
                        if (Constants.URL_CAMPAIGN.equals(g2.getString(columnIndex2))) {
                            String string = g2.getString(columnIndex);
                            boolean z = true;
                            if (g2.getInt(columnIndex3) != 1) {
                                z = false;
                            }
                            d a2 = a(bVar, string, z);
                            if (a2 == null) {
                                g2.close();
                                return null;
                            }
                            hashSet.add(a2);
                        }
                    }
                    g2.close();
                    return hashSet;
                }
            }
            return null;
        } finally {
            g2.close();
        }
    }

    public boolean equals(Object obj) {
        Set<d> set;
        if (this == obj) {
            return true;
        }
        if (obj == null || f.class != obj.getClass()) {
            return false;
        }
        f fVar = (f) obj;
        String str = this.f2085a;
        if (str == null ? fVar.f2085a != null : !str.equals(fVar.f2085a)) {
            return false;
        }
        Map<String, a> map = this.f2086b;
        if (map == null ? fVar.f2086b != null : !map.equals(fVar.f2086b)) {
            return false;
        }
        Set<b> set2 = this.f2087c;
        if (set2 == null ? fVar.f2087c != null : !set2.equals(fVar.f2087c)) {
            return false;
        }
        Set<d> set3 = this.f2088d;
        if (set3 == null || (set = fVar.f2088d) == null) {
            return true;
        }
        return set3.equals(set);
    }

    public int hashCode() {
        String str = this.f2085a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Map<String, a> map = this.f2086b;
        int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
        Set<b> set = this.f2087c;
        if (set != null) {
            i2 = set.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        return "TableInfo{name='" + this.f2085a + '\'' + ", columns=" + this.f2086b + ", foreignKeys=" + this.f2087c + ", indices=" + this.f2088d + '}';
    }

    private static List<c> a(Cursor cursor) {
        int columnIndex = cursor.getColumnIndex("id");
        int columnIndex2 = cursor.getColumnIndex("seq");
        int columnIndex3 = cursor.getColumnIndex("from");
        int columnIndex4 = cursor.getColumnIndex(TJAdUnitConstants.String.SPLIT_VIEW_TRIGGER_TO);
        int count = cursor.getCount();
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < count; i2++) {
            cursor.moveToPosition(i2);
            arrayList.add(new c(cursor.getInt(columnIndex), cursor.getInt(columnIndex2), cursor.getString(columnIndex3), cursor.getString(columnIndex4)));
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    /* compiled from: TableInfo */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public final String f2089a;

        /* renamed from: b  reason: collision with root package name */
        public final String f2090b;

        /* renamed from: c  reason: collision with root package name */
        public final int f2091c;

        /* renamed from: d  reason: collision with root package name */
        public final boolean f2092d;

        /* renamed from: e  reason: collision with root package name */
        public final int f2093e;

        /* renamed from: f  reason: collision with root package name */
        public final String f2094f;

        /* renamed from: g  reason: collision with root package name */
        private final int f2095g;

        public a(String str, String str2, boolean z, int i2, String str3, int i3) {
            this.f2089a = str;
            this.f2090b = str2;
            this.f2092d = z;
            this.f2093e = i2;
            this.f2091c = a(str2);
            this.f2094f = str3;
            this.f2095g = i3;
        }

        private static int a(String str) {
            if (str == null) {
                return 5;
            }
            String upperCase = str.toUpperCase(Locale.US);
            if (upperCase.contains("INT")) {
                return 3;
            }
            if (upperCase.contains("CHAR") || upperCase.contains("CLOB") || upperCase.contains("TEXT")) {
                return 2;
            }
            if (upperCase.contains("BLOB")) {
                return 5;
            }
            return (upperCase.contains("REAL") || upperCase.contains("FLOA") || upperCase.contains("DOUB")) ? 4 : 1;
        }

        public boolean equals(Object obj) {
            String str;
            String str2;
            String str3;
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (Build.VERSION.SDK_INT >= 20) {
                if (this.f2093e != aVar.f2093e) {
                    return false;
                }
            } else if (a() != aVar.a()) {
                return false;
            }
            if (!this.f2089a.equals(aVar.f2089a) || this.f2092d != aVar.f2092d) {
                return false;
            }
            if (this.f2095g == 1 && aVar.f2095g == 2 && (str3 = this.f2094f) != null && !str3.equals(aVar.f2094f)) {
                return false;
            }
            if (this.f2095g == 2 && aVar.f2095g == 1 && (str2 = aVar.f2094f) != null && !str2.equals(this.f2094f)) {
                return false;
            }
            int i2 = this.f2095g;
            if ((i2 == 0 || i2 != aVar.f2095g || ((str = this.f2094f) == null ? aVar.f2094f == null : str.equals(aVar.f2094f))) && this.f2091c == aVar.f2091c) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (((((this.f2089a.hashCode() * 31) + this.f2091c) * 31) + (this.f2092d ? 1231 : 1237)) * 31) + this.f2093e;
        }

        public String toString() {
            return "Column{name='" + this.f2089a + '\'' + ", type='" + this.f2090b + '\'' + ", affinity='" + this.f2091c + '\'' + ", notNull=" + this.f2092d + ", primaryKeyPosition=" + this.f2093e + ", defaultValue='" + this.f2094f + '\'' + '}';
        }

        public boolean a() {
            return this.f2093e > 0;
        }
    }

    private static d a(b.m.a.b bVar, String str, boolean z) {
        Cursor g2 = bVar.g("PRAGMA index_xinfo(`" + str + "`)");
        try {
            int columnIndex = g2.getColumnIndex("seqno");
            int columnIndex2 = g2.getColumnIndex("cid");
            int columnIndex3 = g2.getColumnIndex("name");
            if (!(columnIndex == -1 || columnIndex2 == -1)) {
                if (columnIndex3 != -1) {
                    TreeMap treeMap = new TreeMap();
                    while (g2.moveToNext()) {
                        if (g2.getInt(columnIndex2) >= 0) {
                            int i2 = g2.getInt(columnIndex);
                            treeMap.put(Integer.valueOf(i2), g2.getString(columnIndex3));
                        }
                    }
                    ArrayList arrayList = new ArrayList(treeMap.size());
                    arrayList.addAll(treeMap.values());
                    d dVar = new d(str, z, arrayList);
                    g2.close();
                    return dVar;
                }
            }
            return null;
        } finally {
            g2.close();
        }
    }
}
