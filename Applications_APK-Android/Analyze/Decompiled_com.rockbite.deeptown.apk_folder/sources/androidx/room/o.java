package androidx.room;

import b.m.a.f;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: SharedSQLiteStatement */
public abstract class o {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicBoolean f2070a = new AtomicBoolean(false);

    /* renamed from: b  reason: collision with root package name */
    private final i f2071b;

    /* renamed from: c  reason: collision with root package name */
    private volatile f f2072c;

    public o(i iVar) {
        this.f2071b = iVar;
    }

    private f a(boolean z) {
        if (!z) {
            return d();
        }
        if (this.f2072c == null) {
            this.f2072c = d();
        }
        return this.f2072c;
    }

    private f d() {
        return this.f2071b.a(c());
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.f2071b.a();
    }

    /* access modifiers changed from: protected */
    public abstract String c();

    public f a() {
        b();
        return a(this.f2070a.compareAndSet(false, true));
    }

    public void a(f fVar) {
        if (fVar == this.f2072c) {
            this.f2070a.set(false);
        }
    }
}
