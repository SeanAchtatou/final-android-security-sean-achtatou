package androidx.room;

import b.m.a.f;

/* compiled from: EntityInsertionAdapter */
public abstract class b<T> extends o {
    public b(i iVar) {
        super(iVar);
    }

    /* access modifiers changed from: protected */
    public abstract void a(f fVar, T t);

    public final void a(T t) {
        f a2 = a();
        try {
            a(a2, t);
            a2.G();
        } finally {
            a(a2);
        }
    }
}
