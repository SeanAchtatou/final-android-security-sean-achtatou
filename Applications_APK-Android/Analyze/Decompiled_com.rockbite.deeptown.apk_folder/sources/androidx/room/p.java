package androidx.room;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;

/* compiled from: TransactionExecutor */
class p implements Executor {

    /* renamed from: a  reason: collision with root package name */
    private final Executor f2073a;

    /* renamed from: b  reason: collision with root package name */
    private final ArrayDeque<Runnable> f2074b = new ArrayDeque<>();

    /* renamed from: c  reason: collision with root package name */
    private Runnable f2075c;

    /* compiled from: TransactionExecutor */
    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Runnable f2076a;

        a(Runnable runnable) {
            this.f2076a = runnable;
        }

        public void run() {
            try {
                this.f2076a.run();
            } finally {
                p.this.a();
            }
        }
    }

    p(Executor executor) {
        this.f2073a = executor;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a() {
        Runnable poll = this.f2074b.poll();
        this.f2075c = poll;
        if (poll != null) {
            this.f2073a.execute(this.f2075c);
        }
    }

    public synchronized void execute(Runnable runnable) {
        this.f2074b.offer(new a(runnable));
        if (this.f2075c == null) {
            a();
        }
    }
}
