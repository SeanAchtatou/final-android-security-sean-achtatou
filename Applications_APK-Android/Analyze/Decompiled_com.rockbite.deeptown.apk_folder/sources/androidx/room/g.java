package androidx.room;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import androidx.room.c;
import androidx.room.d;
import androidx.room.f;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: MultiInstanceInvalidationClient */
class g {

    /* renamed from: a  reason: collision with root package name */
    final Context f2000a;

    /* renamed from: b  reason: collision with root package name */
    final String f2001b;

    /* renamed from: c  reason: collision with root package name */
    int f2002c;

    /* renamed from: d  reason: collision with root package name */
    final f f2003d;

    /* renamed from: e  reason: collision with root package name */
    final f.c f2004e;

    /* renamed from: f  reason: collision with root package name */
    d f2005f;

    /* renamed from: g  reason: collision with root package name */
    final Executor f2006g;

    /* renamed from: h  reason: collision with root package name */
    final c f2007h = new a();

    /* renamed from: i  reason: collision with root package name */
    final AtomicBoolean f2008i = new AtomicBoolean(false);

    /* renamed from: j  reason: collision with root package name */
    final ServiceConnection f2009j = new b();

    /* renamed from: k  reason: collision with root package name */
    final Runnable f2010k = new c();
    final Runnable l = new d();

    /* compiled from: MultiInstanceInvalidationClient */
    class a extends c.a {

        /* renamed from: androidx.room.g$a$a  reason: collision with other inner class name */
        /* compiled from: MultiInstanceInvalidationClient */
        class C0026a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ String[] f2012a;

            C0026a(String[] strArr) {
                this.f2012a = strArr;
            }

            public void run() {
                g.this.f2003d.a(this.f2012a);
            }
        }

        a() {
        }

        public void a(String[] strArr) {
            g.this.f2006g.execute(new C0026a(strArr));
        }
    }

    /* compiled from: MultiInstanceInvalidationClient */
    class b implements ServiceConnection {
        b() {
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            g.this.f2005f = d.a.a(iBinder);
            g gVar = g.this;
            gVar.f2006g.execute(gVar.f2010k);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            g gVar = g.this;
            gVar.f2006g.execute(gVar.l);
            g.this.f2005f = null;
        }
    }

    /* compiled from: MultiInstanceInvalidationClient */
    class c implements Runnable {
        c() {
        }

        public void run() {
            try {
                d dVar = g.this.f2005f;
                if (dVar != null) {
                    g.this.f2002c = dVar.a(g.this.f2007h, g.this.f2001b);
                    g.this.f2003d.a(g.this.f2004e);
                }
            } catch (RemoteException e2) {
                Log.w("ROOM", "Cannot register multi-instance invalidation callback", e2);
            }
        }
    }

    /* compiled from: MultiInstanceInvalidationClient */
    class d implements Runnable {
        d() {
        }

        public void run() {
            g gVar = g.this;
            gVar.f2003d.b(gVar.f2004e);
        }
    }

    /* compiled from: MultiInstanceInvalidationClient */
    class e implements Runnable {
        e() {
        }

        public void run() {
            g gVar = g.this;
            gVar.f2003d.b(gVar.f2004e);
            try {
                d dVar = g.this.f2005f;
                if (dVar != null) {
                    dVar.a(g.this.f2007h, g.this.f2002c);
                }
            } catch (RemoteException e2) {
                Log.w("ROOM", "Cannot unregister multi-instance invalidation callback", e2);
            }
            g gVar2 = g.this;
            gVar2.f2000a.unbindService(gVar2.f2009j);
        }
    }

    /* compiled from: MultiInstanceInvalidationClient */
    class f extends f.c {
        f(String[] strArr) {
            super(strArr);
        }

        public void a(Set<String> set) {
            if (!g.this.f2008i.get()) {
                try {
                    d dVar = g.this.f2005f;
                    if (dVar != null) {
                        dVar.a(g.this.f2002c, (String[]) set.toArray(new String[0]));
                    }
                } catch (RemoteException e2) {
                    Log.w("ROOM", "Cannot broadcast invalidation", e2);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return true;
        }
    }

    g(Context context, String str, f fVar, Executor executor) {
        new e();
        this.f2000a = context.getApplicationContext();
        this.f2001b = str;
        this.f2003d = fVar;
        this.f2006g = executor;
        this.f2004e = new f(fVar.f1980b);
        this.f2000a.bindService(new Intent(this.f2000a, MultiInstanceInvalidationService.class), this.f2009j, 1);
    }
}
