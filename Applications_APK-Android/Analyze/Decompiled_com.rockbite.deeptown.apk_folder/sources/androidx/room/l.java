package androidx.room;

import b.m.a.d;
import b.m.a.e;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/* compiled from: RoomSQLiteQuery */
public class l implements e, d {

    /* renamed from: i  reason: collision with root package name */
    static final TreeMap<Integer, l> f2051i = new TreeMap<>();

    /* renamed from: a  reason: collision with root package name */
    private volatile String f2052a;

    /* renamed from: b  reason: collision with root package name */
    final long[] f2053b;

    /* renamed from: c  reason: collision with root package name */
    final double[] f2054c;

    /* renamed from: d  reason: collision with root package name */
    final String[] f2055d;

    /* renamed from: e  reason: collision with root package name */
    final byte[][] f2056e;

    /* renamed from: f  reason: collision with root package name */
    private final int[] f2057f;

    /* renamed from: g  reason: collision with root package name */
    final int f2058g;

    /* renamed from: h  reason: collision with root package name */
    int f2059h;

    private l(int i2) {
        this.f2058g = i2;
        int i3 = i2 + 1;
        this.f2057f = new int[i3];
        this.f2053b = new long[i3];
        this.f2054c = new double[i3];
        this.f2055d = new String[i3];
        this.f2056e = new byte[i3][];
    }

    public static l b(String str, int i2) {
        synchronized (f2051i) {
            Map.Entry<Integer, l> ceilingEntry = f2051i.ceilingEntry(Integer.valueOf(i2));
            if (ceilingEntry != null) {
                f2051i.remove(ceilingEntry.getKey());
                l value = ceilingEntry.getValue();
                value.a(str, i2);
                return value;
            }
            l lVar = new l(i2);
            lVar.a(str, i2);
            return lVar;
        }
    }

    private static void c() {
        if (f2051i.size() > 15) {
            int size = f2051i.size() - 10;
            Iterator<Integer> it = f2051i.descendingKeySet().iterator();
            while (true) {
                int i2 = size - 1;
                if (size > 0) {
                    it.next();
                    it.remove();
                    size = i2;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, int i2) {
        this.f2052a = str;
        this.f2059h = i2;
    }

    public void close() {
    }

    public String a() {
        return this.f2052a;
    }

    public void a(d dVar) {
        for (int i2 = 1; i2 <= this.f2059h; i2++) {
            int i3 = this.f2057f[i2];
            if (i3 == 1) {
                dVar.c(i2);
            } else if (i3 == 2) {
                dVar.a(i2, this.f2053b[i2]);
            } else if (i3 == 3) {
                dVar.a(i2, this.f2054c[i2]);
            } else if (i3 == 4) {
                dVar.a(i2, this.f2055d[i2]);
            } else if (i3 == 5) {
                dVar.a(i2, this.f2056e[i2]);
            }
        }
    }

    public void c(int i2) {
        this.f2057f[i2] = 1;
    }

    public void a(int i2, long j2) {
        this.f2057f[i2] = 2;
        this.f2053b[i2] = j2;
    }

    public void b() {
        synchronized (f2051i) {
            f2051i.put(Integer.valueOf(this.f2058g), this);
            c();
        }
    }

    public void a(int i2, double d2) {
        this.f2057f[i2] = 3;
        this.f2054c[i2] = d2;
    }

    public void a(int i2, String str) {
        this.f2057f[i2] = 4;
        this.f2055d[i2] = str;
    }

    public void a(int i2, byte[] bArr) {
        this.f2057f[i2] = 5;
        this.f2056e[i2] = bArr;
    }
}
