package androidx.room;

import android.content.Context;
import androidx.room.i;
import b.m.a.c;
import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;

/* compiled from: DatabaseConfiguration */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public final c.C0063c f1965a;

    /* renamed from: b  reason: collision with root package name */
    public final Context f1966b;

    /* renamed from: c  reason: collision with root package name */
    public final String f1967c;

    /* renamed from: d  reason: collision with root package name */
    public final i.d f1968d;

    /* renamed from: e  reason: collision with root package name */
    public final List<i.b> f1969e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f1970f;

    /* renamed from: g  reason: collision with root package name */
    public final i.c f1971g;

    /* renamed from: h  reason: collision with root package name */
    public final Executor f1972h;

    /* renamed from: i  reason: collision with root package name */
    public final Executor f1973i;

    /* renamed from: j  reason: collision with root package name */
    public final boolean f1974j;

    /* renamed from: k  reason: collision with root package name */
    public final boolean f1975k;
    public final boolean l;
    private final Set<Integer> m;

    public a(Context context, String str, c.C0063c cVar, i.d dVar, List<i.b> list, boolean z, i.c cVar2, Executor executor, Executor executor2, boolean z2, boolean z3, boolean z4, Set<Integer> set, String str2, File file) {
        this.f1965a = cVar;
        this.f1966b = context;
        this.f1967c = str;
        this.f1968d = dVar;
        this.f1969e = list;
        this.f1970f = z;
        this.f1971g = cVar2;
        this.f1972h = executor;
        this.f1973i = executor2;
        this.f1974j = z2;
        this.f1975k = z3;
        this.l = z4;
        this.m = set;
    }

    public boolean a(int i2, int i3) {
        Set<Integer> set;
        if ((!(i2 > i3) || !this.l) && this.f1975k && ((set = this.m) == null || !set.contains(Integer.valueOf(i2)))) {
            return true;
        }
        return false;
    }
}
