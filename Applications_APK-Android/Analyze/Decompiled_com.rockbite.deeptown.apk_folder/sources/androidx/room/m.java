package androidx.room;

import android.content.Context;
import android.util.Log;
import androidx.room.r.a;
import androidx.room.r.d;
import b.m.a.b;
import b.m.a.c;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/* compiled from: SQLiteCopyOpenHelper */
class m implements c {

    /* renamed from: a  reason: collision with root package name */
    private final Context f2060a;

    /* renamed from: b  reason: collision with root package name */
    private final String f2061b;

    /* renamed from: c  reason: collision with root package name */
    private final File f2062c;

    /* renamed from: d  reason: collision with root package name */
    private final int f2063d;

    /* renamed from: e  reason: collision with root package name */
    private final c f2064e;

    /* renamed from: f  reason: collision with root package name */
    private a f2065f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f2066g;

    m(Context context, String str, File file, int i2, c cVar) {
        this.f2060a = context;
        this.f2061b = str;
        this.f2062c = file;
        this.f2063d = i2;
        this.f2064e = cVar;
    }

    public synchronized b H() {
        if (!this.f2066g) {
            a();
            this.f2066g = true;
        }
        return this.f2064e.H();
    }

    public String I() {
        return this.f2064e.I();
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        this.f2065f = aVar;
    }

    public void b(boolean z) {
        this.f2064e.b(z);
    }

    public synchronized void close() {
        this.f2064e.close();
        this.f2066g = false;
    }

    private void a() {
        String I = I();
        File databasePath = this.f2060a.getDatabasePath(I);
        a aVar = this.f2065f;
        a aVar2 = new a(I, this.f2060a.getFilesDir(), aVar == null || aVar.f1974j);
        try {
            aVar2.a();
            if (!databasePath.exists()) {
                a(databasePath);
                aVar2.b();
            } else if (this.f2065f == null) {
                aVar2.b();
            } else {
                try {
                    int a2 = androidx.room.r.c.a(databasePath);
                    if (a2 == this.f2063d) {
                        aVar2.b();
                    } else if (this.f2065f.a(a2, this.f2063d)) {
                        aVar2.b();
                    } else {
                        if (this.f2060a.deleteDatabase(I)) {
                            try {
                                a(databasePath);
                            } catch (IOException e2) {
                                Log.w("ROOM", "Unable to copy database file.", e2);
                            }
                        } else {
                            Log.w("ROOM", "Failed to delete database file (" + I + ") for a copy destructive migration.");
                        }
                        aVar2.b();
                    }
                } catch (IOException e3) {
                    Log.w("ROOM", "Unable to read database version.", e3);
                    aVar2.b();
                }
            }
        } catch (IOException e4) {
            throw new RuntimeException("Unable to copy database file.", e4);
        } catch (Throwable th) {
            aVar2.b();
            throw th;
        }
    }

    private void a(File file) throws IOException {
        ReadableByteChannel readableByteChannel;
        if (this.f2061b != null) {
            readableByteChannel = Channels.newChannel(this.f2060a.getAssets().open(this.f2061b));
        } else {
            File file2 = this.f2062c;
            if (file2 != null) {
                readableByteChannel = new FileInputStream(file2).getChannel();
            } else {
                throw new IllegalStateException("copyFromAssetPath and copyFromFile == null!");
            }
        }
        File createTempFile = File.createTempFile("room-copy-helper", ".tmp", this.f2060a.getCacheDir());
        createTempFile.deleteOnExit();
        d.a(readableByteChannel, new FileOutputStream(createTempFile).getChannel());
        File parentFile = file.getParentFile();
        if (parentFile != null && !parentFile.exists() && !parentFile.mkdirs()) {
            throw new IOException("Failed to create directories for " + file.getAbsolutePath());
        } else if (!createTempFile.renameTo(file)) {
            throw new IOException("Failed to move intermediate file (" + createTempFile.getAbsolutePath() + ") to destination (" + file.getAbsolutePath() + ").");
        }
    }
}
