package androidx.room;

import b.m.a.c;
import java.io.File;

/* compiled from: SQLiteCopyOpenHelperFactory */
class n implements c.C0063c {

    /* renamed from: a  reason: collision with root package name */
    private final String f2067a;

    /* renamed from: b  reason: collision with root package name */
    private final File f2068b;

    /* renamed from: c  reason: collision with root package name */
    private final c.C0063c f2069c;

    n(String str, File file, c.C0063c cVar) {
        this.f2067a = str;
        this.f2068b = file;
        this.f2069c = cVar;
    }

    public c a(c.b bVar) {
        return new m(bVar.f3042a, this.f2067a, this.f2068b, bVar.f3044c.f3041a, this.f2069c.a(bVar));
    }
}
