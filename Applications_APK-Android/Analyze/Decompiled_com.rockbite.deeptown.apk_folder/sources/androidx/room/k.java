package androidx.room;

import android.database.Cursor;
import b.m.a.c;
import java.util.List;

/* compiled from: RoomOpenHelper */
public class k extends c.a {

    /* renamed from: b  reason: collision with root package name */
    private a f2044b;

    /* renamed from: c  reason: collision with root package name */
    private final a f2045c;

    /* renamed from: d  reason: collision with root package name */
    private final String f2046d;

    /* renamed from: e  reason: collision with root package name */
    private final String f2047e;

    /* compiled from: RoomOpenHelper */
    public static abstract class a {

        /* renamed from: a  reason: collision with root package name */
        public final int f2048a;

        public a(int i2) {
            this.f2048a = i2;
        }

        /* access modifiers changed from: protected */
        public abstract void a(b.m.a.b bVar);

        /* access modifiers changed from: protected */
        public abstract void b(b.m.a.b bVar);

        /* access modifiers changed from: protected */
        public abstract void c(b.m.a.b bVar);

        /* access modifiers changed from: protected */
        public abstract void d(b.m.a.b bVar);

        /* access modifiers changed from: protected */
        public abstract void e(b.m.a.b bVar);

        /* access modifiers changed from: protected */
        public abstract void f(b.m.a.b bVar);

        /* access modifiers changed from: protected */
        public abstract b g(b.m.a.b bVar);
    }

    /* compiled from: RoomOpenHelper */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public final boolean f2049a;

        /* renamed from: b  reason: collision with root package name */
        public final String f2050b;

        public b(boolean z, String str) {
            this.f2049a = z;
            this.f2050b = str;
        }
    }

    public k(a aVar, a aVar2, String str, String str2) {
        super(aVar2.f2048a);
        this.f2044b = aVar;
        this.f2045c = aVar2;
        this.f2046d = str;
        this.f2047e = str2;
    }

    /* JADX INFO: finally extract failed */
    private void e(b.m.a.b bVar) {
        if (h(bVar)) {
            String str = null;
            Cursor a2 = bVar.a(new b.m.a.a("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
            try {
                if (a2.moveToFirst()) {
                    str = a2.getString(0);
                }
                a2.close();
                if (!this.f2046d.equals(str) && !this.f2047e.equals(str)) {
                    throw new IllegalStateException("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.");
                }
            } catch (Throwable th) {
                a2.close();
                throw th;
            }
        } else {
            b g2 = this.f2045c.g(bVar);
            if (g2.f2049a) {
                this.f2045c.e(bVar);
                i(bVar);
                return;
            }
            throw new IllegalStateException("Pre-packaged database has an invalid schema: " + g2.f2050b);
        }
    }

    private void f(b.m.a.b bVar) {
        bVar.e("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    }

    private static boolean g(b.m.a.b bVar) {
        Cursor g2 = bVar.g("SELECT count(*) FROM sqlite_master WHERE name != 'android_metadata'");
        try {
            boolean z = false;
            if (g2.moveToFirst() && g2.getInt(0) == 0) {
                z = true;
            }
            return z;
        } finally {
            g2.close();
        }
    }

    private static boolean h(b.m.a.b bVar) {
        Cursor g2 = bVar.g("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
        try {
            boolean z = false;
            if (g2.moveToFirst() && g2.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            g2.close();
        }
    }

    private void i(b.m.a.b bVar) {
        f(bVar);
        bVar.e(j.a(this.f2046d));
    }

    public void a(b.m.a.b bVar) {
        super.a(bVar);
    }

    public void b(b.m.a.b bVar, int i2, int i3) {
        boolean z;
        List<androidx.room.q.a> a2;
        a aVar = this.f2044b;
        if (aVar == null || (a2 = aVar.f1968d.a(i2, i3)) == null) {
            z = false;
        } else {
            this.f2045c.f(bVar);
            for (androidx.room.q.a a3 : a2) {
                a3.a(bVar);
            }
            b g2 = this.f2045c.g(bVar);
            if (g2.f2049a) {
                this.f2045c.e(bVar);
                i(bVar);
                z = true;
            } else {
                throw new IllegalStateException("Migration didn't properly handle: " + g2.f2050b);
            }
        }
        if (!z) {
            a aVar2 = this.f2044b;
            if (aVar2 == null || aVar2.a(i2, i3)) {
                throw new IllegalStateException("A migration from " + i2 + " to " + i3 + " was required but not found. Please provide the necessary Migration path via RoomDatabase.Builder.addMigration(Migration ...) or allow for destructive migrations via one of the RoomDatabase.Builder.fallbackToDestructiveMigration* methods.");
            }
            this.f2045c.b(bVar);
            this.f2045c.a(bVar);
        }
    }

    public void c(b.m.a.b bVar) {
        boolean g2 = g(bVar);
        this.f2045c.a(bVar);
        if (!g2) {
            b g3 = this.f2045c.g(bVar);
            if (!g3.f2049a) {
                throw new IllegalStateException("Pre-packaged database has an invalid schema: " + g3.f2050b);
            }
        }
        i(bVar);
        this.f2045c.c(bVar);
    }

    public void d(b.m.a.b bVar) {
        super.d(bVar);
        e(bVar);
        this.f2045c.d(bVar);
        this.f2044b = null;
    }

    public void a(b.m.a.b bVar, int i2, int i3) {
        b(bVar, i2, i3);
    }
}
