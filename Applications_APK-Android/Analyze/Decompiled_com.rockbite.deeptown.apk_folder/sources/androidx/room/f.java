package androidx.room;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import com.google.firebase.perf.FirebasePerformance;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;

/* compiled from: InvalidationTracker */
public class f {

    /* renamed from: k  reason: collision with root package name */
    private static final String[] f1978k = {"UPDATE", FirebasePerformance.HttpMethod.DELETE, "INSERT"};

    /* renamed from: a  reason: collision with root package name */
    final HashMap<String, Integer> f1979a;

    /* renamed from: b  reason: collision with root package name */
    final String[] f1980b;

    /* renamed from: c  reason: collision with root package name */
    private Map<String, Set<String>> f1981c;

    /* renamed from: d  reason: collision with root package name */
    final i f1982d;

    /* renamed from: e  reason: collision with root package name */
    AtomicBoolean f1983e = new AtomicBoolean(false);

    /* renamed from: f  reason: collision with root package name */
    private volatile boolean f1984f = false;

    /* renamed from: g  reason: collision with root package name */
    volatile b.m.a.f f1985g;

    /* renamed from: h  reason: collision with root package name */
    private b f1986h;
    @SuppressLint({"RestrictedApi"})

    /* renamed from: i  reason: collision with root package name */
    final b.b.a.b.b<c, d> f1987i = new b.b.a.b.b<>();

    /* renamed from: j  reason: collision with root package name */
    Runnable f1988j = new a();

    /* compiled from: InvalidationTracker */
    class a implements Runnable {
        a() {
        }

        /* JADX INFO: finally extract failed */
        private Set<Integer> a() {
            HashSet hashSet = new HashSet();
            Cursor a2 = f.this.f1982d.a(new b.m.a.a("SELECT * FROM room_table_modification_log WHERE invalidated = 1;"));
            while (a2.moveToNext()) {
                try {
                    hashSet.add(Integer.valueOf(a2.getInt(0)));
                } catch (Throwable th) {
                    a2.close();
                    throw th;
                }
            }
            a2.close();
            if (!hashSet.isEmpty()) {
                f.this.f1985g.F();
            }
            return hashSet;
        }

        public void run() {
            b.m.a.b H;
            Lock f2 = f.this.f1982d.f();
            Set<Integer> set = null;
            try {
                f2.lock();
                if (!f.this.a()) {
                    f2.unlock();
                } else if (!f.this.f1983e.compareAndSet(true, false)) {
                    f2.unlock();
                } else if (f.this.f1982d.i()) {
                    f2.unlock();
                } else {
                    if (f.this.f1982d.f2024f) {
                        H = f.this.f1982d.g().H();
                        H.J();
                        set = a();
                        H.L();
                        H.M();
                    } else {
                        set = a();
                    }
                    f2.unlock();
                    if (set != null && !set.isEmpty()) {
                        synchronized (f.this.f1987i) {
                            Iterator<Map.Entry<c, d>> it = f.this.f1987i.iterator();
                            while (it.hasNext()) {
                                ((d) it.next().getValue()).a(set);
                            }
                        }
                    }
                }
            } catch (SQLiteException | IllegalStateException e2) {
                try {
                    Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", e2);
                } catch (Throwable th) {
                    f2.unlock();
                    throw th;
                }
            } catch (Throwable th2) {
                H.M();
                throw th2;
            }
        }
    }

    /* compiled from: InvalidationTracker */
    public static abstract class c {

        /* renamed from: a  reason: collision with root package name */
        final String[] f1995a;

        public c(String[] strArr) {
            this.f1995a = (String[]) Arrays.copyOf(strArr, strArr.length);
        }

        public abstract void a(Set<String> set);

        /* access modifiers changed from: package-private */
        public boolean a() {
            return false;
        }
    }

    public f(i iVar, Map<String, String> map, Map<String, Set<String>> map2, String... strArr) {
        this.f1982d = iVar;
        this.f1986h = new b(strArr.length);
        this.f1979a = new HashMap<>();
        this.f1981c = map2;
        new e(this.f1982d);
        int length = strArr.length;
        this.f1980b = new String[length];
        for (int i2 = 0; i2 < length; i2++) {
            String lowerCase = strArr[i2].toLowerCase(Locale.US);
            this.f1979a.put(lowerCase, Integer.valueOf(i2));
            String str = map.get(strArr[i2]);
            if (str != null) {
                this.f1980b[i2] = str.toLowerCase(Locale.US);
            } else {
                this.f1980b[i2] = lowerCase;
            }
        }
        for (Map.Entry next : map.entrySet()) {
            String lowerCase2 = ((String) next.getValue()).toLowerCase(Locale.US);
            if (this.f1979a.containsKey(lowerCase2)) {
                String lowerCase3 = ((String) next.getKey()).toLowerCase(Locale.US);
                HashMap<String, Integer> hashMap = this.f1979a;
                hashMap.put(lowerCase3, hashMap.get(lowerCase2));
            }
        }
    }

    private void b(b.m.a.b bVar, int i2) {
        String str = this.f1980b[i2];
        StringBuilder sb = new StringBuilder();
        for (String a2 : f1978k) {
            sb.setLength(0);
            sb.append("DROP TRIGGER IF EXISTS ");
            a(sb, str, a2);
            bVar.e(sb.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(b.m.a.b bVar) {
        synchronized (this) {
            if (this.f1984f) {
                Log.e("ROOM", "Invalidation tracker is initialized twice :/.");
                return;
            }
            bVar.e("PRAGMA temp_store = MEMORY;");
            bVar.e("PRAGMA recursive_triggers='ON';");
            bVar.e("CREATE TEMP TABLE room_table_modification_log(table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)");
            b(bVar);
            this.f1985g = bVar.f("UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1 ");
            this.f1984f = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.f1982d.j()) {
            b(this.f1982d.g().H());
        }
    }

    /* compiled from: InvalidationTracker */
    static class b {

        /* renamed from: a  reason: collision with root package name */
        final long[] f1990a;

        /* renamed from: b  reason: collision with root package name */
        final boolean[] f1991b;

        /* renamed from: c  reason: collision with root package name */
        final int[] f1992c;

        /* renamed from: d  reason: collision with root package name */
        boolean f1993d;

        /* renamed from: e  reason: collision with root package name */
        boolean f1994e;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.Arrays.fill(long[], long):void}
         arg types: [long[], int]
         candidates:
          ClspMth{java.util.Arrays.fill(double[], double):void}
          ClspMth{java.util.Arrays.fill(byte[], byte):void}
          ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
          ClspMth{java.util.Arrays.fill(char[], char):void}
          ClspMth{java.util.Arrays.fill(short[], short):void}
          ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
          ClspMth{java.util.Arrays.fill(int[], int):void}
          ClspMth{java.util.Arrays.fill(float[], float):void}
          ClspMth{java.util.Arrays.fill(long[], long):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
         arg types: [boolean[], int]
         candidates:
          ClspMth{java.util.Arrays.fill(double[], double):void}
          ClspMth{java.util.Arrays.fill(byte[], byte):void}
          ClspMth{java.util.Arrays.fill(long[], long):void}
          ClspMth{java.util.Arrays.fill(char[], char):void}
          ClspMth{java.util.Arrays.fill(short[], short):void}
          ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
          ClspMth{java.util.Arrays.fill(int[], int):void}
          ClspMth{java.util.Arrays.fill(float[], float):void}
          ClspMth{java.util.Arrays.fill(boolean[], boolean):void} */
        b(int i2) {
            this.f1990a = new long[i2];
            this.f1991b = new boolean[i2];
            this.f1992c = new int[i2];
            Arrays.fill(this.f1990a, 0L);
            Arrays.fill(this.f1991b, false);
        }

        /* access modifiers changed from: package-private */
        public boolean a(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i2 : iArr) {
                    long j2 = this.f1990a[i2];
                    this.f1990a[i2] = 1 + j2;
                    if (j2 == 0) {
                        this.f1993d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        /* access modifiers changed from: package-private */
        public boolean b(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i2 : iArr) {
                    long j2 = this.f1990a[i2];
                    this.f1990a[i2] = j2 - 1;
                    if (j2 == 1) {
                        this.f1993d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        /* access modifiers changed from: package-private */
        public int[] a() {
            synchronized (this) {
                if (this.f1993d) {
                    if (!this.f1994e) {
                        int length = this.f1990a.length;
                        int i2 = 0;
                        while (true) {
                            int i3 = 1;
                            if (i2 < length) {
                                boolean z = this.f1990a[i2] > 0;
                                if (z != this.f1991b[i2]) {
                                    int[] iArr = this.f1992c;
                                    if (!z) {
                                        i3 = 2;
                                    }
                                    iArr[i2] = i3;
                                } else {
                                    this.f1992c[i2] = 0;
                                }
                                this.f1991b[i2] = z;
                                i2++;
                            } else {
                                this.f1994e = true;
                                this.f1993d = false;
                                int[] iArr2 = this.f1992c;
                                return iArr2;
                            }
                        }
                    }
                }
                return null;
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            synchronized (this) {
                this.f1994e = false;
            }
        }
    }

    /* compiled from: InvalidationTracker */
    static class d {

        /* renamed from: a  reason: collision with root package name */
        final int[] f1996a;

        /* renamed from: b  reason: collision with root package name */
        private final String[] f1997b;

        /* renamed from: c  reason: collision with root package name */
        final c f1998c;

        /* renamed from: d  reason: collision with root package name */
        private final Set<String> f1999d;

        d(c cVar, int[] iArr, String[] strArr) {
            this.f1998c = cVar;
            this.f1996a = iArr;
            this.f1997b = strArr;
            if (iArr.length == 1) {
                HashSet hashSet = new HashSet();
                hashSet.add(this.f1997b[0]);
                this.f1999d = Collections.unmodifiableSet(hashSet);
                return;
            }
            this.f1999d = null;
        }

        /* access modifiers changed from: package-private */
        public void a(Set<Integer> set) {
            int length = this.f1996a.length;
            Set set2 = null;
            for (int i2 = 0; i2 < length; i2++) {
                if (set.contains(Integer.valueOf(this.f1996a[i2]))) {
                    if (length == 1) {
                        set2 = this.f1999d;
                    } else {
                        if (set2 == null) {
                            set2 = new HashSet(length);
                        }
                        set2.add(this.f1997b[i2]);
                    }
                }
            }
            if (set2 != null) {
                this.f1998c.a(set2);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(String[] strArr) {
            Set<String> set = null;
            if (this.f1997b.length == 1) {
                int length = strArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        break;
                    } else if (strArr[i2].equalsIgnoreCase(this.f1997b[0])) {
                        set = this.f1999d;
                        break;
                    } else {
                        i2++;
                    }
                }
            } else {
                HashSet hashSet = new HashSet();
                for (String str : strArr) {
                    String[] strArr2 = this.f1997b;
                    int length2 = strArr2.length;
                    int i3 = 0;
                    while (true) {
                        if (i3 >= length2) {
                            break;
                        }
                        String str2 = strArr2[i3];
                        if (str2.equalsIgnoreCase(str)) {
                            hashSet.add(str2);
                            break;
                        }
                        i3++;
                    }
                }
                if (hashSet.size() > 0) {
                    set = hashSet;
                }
            }
            if (set != null) {
                this.f1998c.a(set);
            }
        }
    }

    private String[] b(String[] strArr) {
        HashSet hashSet = new HashSet();
        for (String str : strArr) {
            String lowerCase = str.toLowerCase(Locale.US);
            if (this.f1981c.containsKey(lowerCase)) {
                hashSet.addAll(this.f1981c.get(lowerCase));
            } else {
                hashSet.add(str);
            }
        }
        return (String[]) hashSet.toArray(new String[hashSet.size()]);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str) {
        new g(context, str, this, this.f1982d.h());
    }

    private static void a(StringBuilder sb, String str, String str2) {
        sb.append("`");
        sb.append("room_table_modification_trigger_");
        sb.append(str);
        sb.append("_");
        sb.append(str2);
        sb.append("`");
    }

    @SuppressLint({"RestrictedApi"})
    public void b(c cVar) {
        d remove;
        synchronized (this.f1987i) {
            remove = this.f1987i.remove(cVar);
        }
        if (remove != null && this.f1986h.b(remove.f1996a)) {
            c();
        }
    }

    private void a(b.m.a.b bVar, int i2) {
        bVar.e("INSERT OR IGNORE INTO room_table_modification_log VALUES(" + i2 + ", 0)");
        String str = this.f1980b[i2];
        StringBuilder sb = new StringBuilder();
        for (String str2 : f1978k) {
            sb.setLength(0);
            sb.append("CREATE TEMP TRIGGER IF NOT EXISTS ");
            a(sb, str, str2);
            sb.append(" AFTER ");
            sb.append(str2);
            sb.append(" ON `");
            sb.append(str);
            sb.append("` BEGIN UPDATE ");
            sb.append("room_table_modification_log");
            sb.append(" SET ");
            sb.append("invalidated");
            sb.append(" = 1");
            sb.append(" WHERE ");
            sb.append("table_id");
            sb.append(" = ");
            sb.append(i2);
            sb.append(" AND ");
            sb.append("invalidated");
            sb.append(" = 0");
            sb.append("; END");
            bVar.e(sb.toString());
        }
    }

    public void b() {
        if (this.f1983e.compareAndSet(false, true)) {
            this.f1982d.h().execute(this.f1988j);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(b.m.a.b bVar) {
        if (!bVar.O()) {
            while (true) {
                try {
                    Lock f2 = this.f1982d.f();
                    f2.lock();
                    try {
                        int[] a2 = this.f1986h.a();
                        if (a2 == null) {
                            f2.unlock();
                            return;
                        }
                        int length = a2.length;
                        bVar.J();
                        for (int i2 = 0; i2 < length; i2++) {
                            int i3 = a2[i2];
                            if (i3 == 1) {
                                a(bVar, i2);
                            } else if (i3 == 2) {
                                b(bVar, i2);
                            }
                        }
                        bVar.L();
                        bVar.M();
                        this.f1986h.b();
                        f2.unlock();
                    } catch (Throwable th) {
                        f2.unlock();
                        throw th;
                    }
                } catch (SQLiteException | IllegalStateException e2) {
                    Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", e2);
                    return;
                }
            }
        }
    }

    @SuppressLint({"RestrictedApi"})
    public void a(c cVar) {
        d b2;
        String[] b3 = b(cVar.f1995a);
        int[] iArr = new int[b3.length];
        int length = b3.length;
        int i2 = 0;
        while (i2 < length) {
            Integer num = this.f1979a.get(b3[i2].toLowerCase(Locale.US));
            if (num != null) {
                iArr[i2] = num.intValue();
                i2++;
            } else {
                throw new IllegalArgumentException("There is no table with name " + b3[i2]);
            }
        }
        d dVar = new d(cVar, iArr, b3);
        synchronized (this.f1987i) {
            b2 = this.f1987i.b(cVar, dVar);
        }
        if (b2 == null && this.f1986h.a(iArr)) {
            c();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        if (!this.f1982d.j()) {
            return false;
        }
        if (!this.f1984f) {
            this.f1982d.g().H();
        }
        if (this.f1984f) {
            return true;
        }
        Log.e("ROOM", "database is not initialized even though it is open");
        return false;
    }

    public void a(String... strArr) {
        synchronized (this.f1987i) {
            Iterator<Map.Entry<c, d>> it = this.f1987i.iterator();
            while (it.hasNext()) {
                Map.Entry next = it.next();
                if (!((c) next.getKey()).a()) {
                    ((d) next.getValue()).a(strArr);
                }
            }
        }
    }
}
