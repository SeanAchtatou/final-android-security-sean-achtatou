package androidx.savedstate;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.lifecycle.d;
import androidx.lifecycle.e;
import androidx.lifecycle.h;
import java.util.Map;

@SuppressLint({"RestrictedApi"})
public final class SavedStateRegistry {

    /* renamed from: a  reason: collision with root package name */
    private b.b.a.b.b<String, b> f2109a = new b.b.a.b.b<>();

    /* renamed from: b  reason: collision with root package name */
    private Bundle f2110b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f2111c;

    /* renamed from: d  reason: collision with root package name */
    boolean f2112d;

    public interface a {
        void a(b bVar);
    }

    public interface b {
        Bundle a();
    }

    SavedStateRegistry() {
    }

    public Bundle a(String str) {
        if (this.f2111c) {
            Bundle bundle = this.f2110b;
            if (bundle == null) {
                return null;
            }
            Bundle bundle2 = bundle.getBundle(str);
            this.f2110b.remove(str);
            if (this.f2110b.isEmpty()) {
                this.f2110b = null;
            }
            return bundle2;
        }
        throw new IllegalStateException("You can consumeRestoredStateForKey only after super.onCreate of corresponding component");
    }

    /* access modifiers changed from: package-private */
    public void a(e eVar, Bundle bundle) {
        if (!this.f2111c) {
            if (bundle != null) {
                this.f2110b = bundle.getBundle("androidx.lifecycle.BundlableSavedStateRegistry.key");
            }
            eVar.a(new d() {
                public void a(h hVar, e.a aVar) {
                    if (aVar == e.a.ON_START) {
                        SavedStateRegistry.this.f2112d = true;
                    } else if (aVar == e.a.ON_STOP) {
                        SavedStateRegistry.this.f2112d = false;
                    }
                }
            });
            this.f2111c = true;
            return;
        }
        throw new IllegalStateException("SavedStateRegistry was already restored.");
    }

    /* access modifiers changed from: package-private */
    public void a(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        Bundle bundle3 = this.f2110b;
        if (bundle3 != null) {
            bundle2.putAll(bundle3);
        }
        b.b.a.b.b<K, V>.d b2 = this.f2109a.b();
        while (b2.hasNext()) {
            Map.Entry entry = (Map.Entry) b2.next();
            bundle2.putBundle((String) entry.getKey(), ((b) entry.getValue()).a());
        }
        bundle.putBundle("androidx.lifecycle.BundlableSavedStateRegistry.key", bundle2);
    }
}
