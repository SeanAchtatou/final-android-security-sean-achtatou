package androidx.savedstate;

import android.os.Bundle;
import androidx.lifecycle.e;

/* compiled from: SavedStateRegistryController */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final b f2114a;

    /* renamed from: b  reason: collision with root package name */
    private final SavedStateRegistry f2115b = new SavedStateRegistry();

    private a(b bVar) {
        this.f2114a = bVar;
    }

    public SavedStateRegistry a() {
        return this.f2115b;
    }

    public void b(Bundle bundle) {
        this.f2115b.a(bundle);
    }

    public void a(Bundle bundle) {
        e lifecycle = this.f2114a.getLifecycle();
        if (lifecycle.a() == e.b.INITIALIZED) {
            lifecycle.a(new Recreator(this.f2114a));
            this.f2115b.a(lifecycle, bundle);
            return;
        }
        throw new IllegalStateException("Restarter must be created only during owner's initialization stage");
    }

    public static a a(b bVar) {
        return new a(bVar);
    }
}
