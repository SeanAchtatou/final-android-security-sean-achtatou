package androidx.customview.view;

import android.os.Parcel;
import android.os.Parcelable;

public abstract class AbsSavedState implements Parcelable {
    public static final Parcelable.Creator<AbsSavedState> CREATOR = new a();

    /* renamed from: b  reason: collision with root package name */
    public static final AbsSavedState f1437b = new AbsSavedState() {
    };

    /* renamed from: a  reason: collision with root package name */
    private final Parcelable f1438a;

    static class a implements Parcelable.ClassLoaderCreator<AbsSavedState> {
        a() {
        }

        public AbsSavedState[] newArray(int i2) {
            return new AbsSavedState[i2];
        }

        public AbsSavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
            if (parcel.readParcelable(classLoader) == null) {
                return AbsSavedState.f1437b;
            }
            throw new IllegalStateException("superState must be null");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.customview.view.AbsSavedState.a.createFromParcel(android.os.Parcel, java.lang.ClassLoader):androidx.customview.view.AbsSavedState
         arg types: [android.os.Parcel, ?[OBJECT, ARRAY]]
         candidates:
          androidx.customview.view.AbsSavedState.a.createFromParcel(android.os.Parcel, java.lang.ClassLoader):java.lang.Object
          ClspMth{android.os.Parcelable.ClassLoaderCreator.createFromParcel(android.os.Parcel, java.lang.ClassLoader):T}
          androidx.customview.view.AbsSavedState.a.createFromParcel(android.os.Parcel, java.lang.ClassLoader):androidx.customview.view.AbsSavedState */
        public AbsSavedState createFromParcel(Parcel parcel) {
            return createFromParcel(parcel, (ClassLoader) null);
        }
    }

    public final Parcelable a() {
        return this.f1438a;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeParcelable(this.f1438a, i2);
    }

    private AbsSavedState() {
        this.f1438a = null;
    }

    protected AbsSavedState(Parcelable parcelable) {
        if (parcelable != null) {
            this.f1438a = parcelable == f1437b ? null : parcelable;
            return;
        }
        throw new IllegalArgumentException("superState must not be null");
    }

    protected AbsSavedState(Parcel parcel, ClassLoader classLoader) {
        Parcelable readParcelable = parcel.readParcelable(classLoader);
        this.f1438a = readParcelable == null ? f1437b : readParcelable;
    }
}
