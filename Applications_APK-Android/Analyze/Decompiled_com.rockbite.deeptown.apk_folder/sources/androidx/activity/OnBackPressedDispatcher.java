package androidx.activity;

import androidx.lifecycle.e;
import androidx.lifecycle.f;
import androidx.lifecycle.h;
import java.util.ArrayDeque;
import java.util.Iterator;

public final class OnBackPressedDispatcher {

    /* renamed from: a  reason: collision with root package name */
    private final Runnable f557a;

    /* renamed from: b  reason: collision with root package name */
    final ArrayDeque<b> f558b = new ArrayDeque<>();

    private class LifecycleOnBackPressedCancellable implements f, a {

        /* renamed from: a  reason: collision with root package name */
        private final e f559a;

        /* renamed from: b  reason: collision with root package name */
        private final b f560b;

        /* renamed from: c  reason: collision with root package name */
        private a f561c;

        LifecycleOnBackPressedCancellable(e eVar, b bVar) {
            this.f559a = eVar;
            this.f560b = bVar;
            eVar.a(this);
        }

        public void a(h hVar, e.a aVar) {
            if (aVar == e.a.ON_START) {
                this.f561c = OnBackPressedDispatcher.this.a(this.f560b);
            } else if (aVar == e.a.ON_STOP) {
                a aVar2 = this.f561c;
                if (aVar2 != null) {
                    aVar2.cancel();
                }
            } else if (aVar == e.a.ON_DESTROY) {
                cancel();
            }
        }

        public void cancel() {
            this.f559a.b(this);
            this.f560b.b(this);
            a aVar = this.f561c;
            if (aVar != null) {
                aVar.cancel();
                this.f561c = null;
            }
        }
    }

    private class a implements a {

        /* renamed from: a  reason: collision with root package name */
        private final b f563a;

        a(b bVar) {
            this.f563a = bVar;
        }

        public void cancel() {
            OnBackPressedDispatcher.this.f558b.remove(this.f563a);
            this.f563a.b(this);
        }
    }

    public OnBackPressedDispatcher(Runnable runnable) {
        this.f557a = runnable;
    }

    /* access modifiers changed from: package-private */
    public a a(b bVar) {
        this.f558b.add(bVar);
        a aVar = new a(bVar);
        bVar.a(aVar);
        return aVar;
    }

    public void a(h hVar, b bVar) {
        e lifecycle = hVar.getLifecycle();
        if (lifecycle.a() != e.b.DESTROYED) {
            bVar.a(new LifecycleOnBackPressedCancellable(lifecycle, bVar));
        }
    }

    public void a() {
        Iterator<b> descendingIterator = this.f558b.descendingIterator();
        while (descendingIterator.hasNext()) {
            b next = descendingIterator.next();
            if (next.b()) {
                next.a();
                return;
            }
        }
        Runnable runnable = this.f557a;
        if (runnable != null) {
            runnable.run();
        }
    }
}
