package androidx.activity;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: OnBackPressedCallback */
public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    private boolean f565a;

    /* renamed from: b  reason: collision with root package name */
    private CopyOnWriteArrayList<a> f566b = new CopyOnWriteArrayList<>();

    public b(boolean z) {
        this.f565a = z;
    }

    public abstract void a();

    public final void a(boolean z) {
        this.f565a = z;
    }

    public final boolean b() {
        return this.f565a;
    }

    public final void c() {
        Iterator<a> it = this.f566b.iterator();
        while (it.hasNext()) {
            it.next().cancel();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        this.f566b.add(aVar);
    }

    /* access modifiers changed from: package-private */
    public void b(a aVar) {
        this.f566b.remove(aVar);
    }
}
