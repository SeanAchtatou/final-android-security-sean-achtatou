package androidx.core.app;

import android.app.Notification;
import android.app.RemoteInput;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.SparseArray;
import android.widget.RemoteViews;
import androidx.core.app.i;
import com.google.android.gms.ads.AdRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: NotificationCompatBuilder */
class j implements h {

    /* renamed from: a  reason: collision with root package name */
    private final Notification.Builder f1328a;

    /* renamed from: b  reason: collision with root package name */
    private final i.d f1329b;

    /* renamed from: c  reason: collision with root package name */
    private RemoteViews f1330c;

    /* renamed from: d  reason: collision with root package name */
    private RemoteViews f1331d;

    /* renamed from: e  reason: collision with root package name */
    private final List<Bundle> f1332e = new ArrayList();

    /* renamed from: f  reason: collision with root package name */
    private final Bundle f1333f = new Bundle();

    /* renamed from: g  reason: collision with root package name */
    private int f1334g;

    /* renamed from: h  reason: collision with root package name */
    private RemoteViews f1335h;

    j(i.d dVar) {
        ArrayList<String> arrayList;
        this.f1329b = dVar;
        if (Build.VERSION.SDK_INT >= 26) {
            this.f1328a = new Notification.Builder(dVar.f1313a, dVar.I);
        } else {
            this.f1328a = new Notification.Builder(dVar.f1313a);
        }
        Notification notification = dVar.N;
        this.f1328a.setWhen(notification.when).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, dVar.f1320h).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(dVar.f1316d).setContentText(dVar.f1317e).setContentInfo(dVar.f1322j).setContentIntent(dVar.f1318f).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(dVar.f1319g, (notification.flags & 128) != 0).setLargeIcon(dVar.f1321i).setNumber(dVar.f1323k).setProgress(dVar.r, dVar.s, dVar.t);
        if (Build.VERSION.SDK_INT < 21) {
            this.f1328a.setSound(notification.sound, notification.audioStreamType);
        }
        if (Build.VERSION.SDK_INT >= 16) {
            this.f1328a.setSubText(dVar.p).setUsesChronometer(dVar.n).setPriority(dVar.l);
            Iterator<i.a> it = dVar.f1314b.iterator();
            while (it.hasNext()) {
                a(it.next());
            }
            Bundle bundle = dVar.B;
            if (bundle != null) {
                this.f1333f.putAll(bundle);
            }
            if (Build.VERSION.SDK_INT < 20) {
                if (dVar.x) {
                    this.f1333f.putBoolean("android.support.localOnly", true);
                }
                String str = dVar.u;
                if (str != null) {
                    this.f1333f.putString("android.support.groupKey", str);
                    if (dVar.v) {
                        this.f1333f.putBoolean("android.support.isGroupSummary", true);
                    } else {
                        this.f1333f.putBoolean("android.support.useSideChannel", true);
                    }
                }
                String str2 = dVar.w;
                if (str2 != null) {
                    this.f1333f.putString("android.support.sortKey", str2);
                }
            }
            this.f1330c = dVar.F;
            this.f1331d = dVar.G;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            this.f1328a.setShowWhen(dVar.m);
            if (Build.VERSION.SDK_INT < 21 && (arrayList = dVar.O) != null && !arrayList.isEmpty()) {
                Bundle bundle2 = this.f1333f;
                ArrayList<String> arrayList2 = dVar.O;
                bundle2.putStringArray("android.people", (String[]) arrayList2.toArray(new String[arrayList2.size()]));
            }
        }
        if (Build.VERSION.SDK_INT >= 20) {
            this.f1328a.setLocalOnly(dVar.x).setGroup(dVar.u).setGroupSummary(dVar.v).setSortKey(dVar.w);
            this.f1334g = dVar.M;
        }
        if (Build.VERSION.SDK_INT >= 21) {
            this.f1328a.setCategory(dVar.A).setColor(dVar.C).setVisibility(dVar.D).setPublicVersion(dVar.E).setSound(notification.sound, notification.audioAttributes);
            Iterator<String> it2 = dVar.O.iterator();
            while (it2.hasNext()) {
                this.f1328a.addPerson(it2.next());
            }
            this.f1335h = dVar.H;
            if (dVar.f1315c.size() > 0) {
                Bundle bundle3 = dVar.b().getBundle("android.car.EXTENSIONS");
                bundle3 = bundle3 == null ? new Bundle() : bundle3;
                Bundle bundle4 = new Bundle();
                for (int i2 = 0; i2 < dVar.f1315c.size(); i2++) {
                    bundle4.putBundle(Integer.toString(i2), k.a(dVar.f1315c.get(i2)));
                }
                bundle3.putBundle("invisible_actions", bundle4);
                dVar.b().putBundle("android.car.EXTENSIONS", bundle3);
                this.f1333f.putBundle("android.car.EXTENSIONS", bundle3);
            }
        }
        if (Build.VERSION.SDK_INT >= 24) {
            this.f1328a.setExtras(dVar.B).setRemoteInputHistory(dVar.q);
            RemoteViews remoteViews = dVar.F;
            if (remoteViews != null) {
                this.f1328a.setCustomContentView(remoteViews);
            }
            RemoteViews remoteViews2 = dVar.G;
            if (remoteViews2 != null) {
                this.f1328a.setCustomBigContentView(remoteViews2);
            }
            RemoteViews remoteViews3 = dVar.H;
            if (remoteViews3 != null) {
                this.f1328a.setCustomHeadsUpContentView(remoteViews3);
            }
        }
        if (Build.VERSION.SDK_INT >= 26) {
            this.f1328a.setBadgeIconType(dVar.J).setShortcutId(dVar.K).setTimeoutAfter(dVar.L).setGroupAlertBehavior(dVar.M);
            if (dVar.z) {
                this.f1328a.setColorized(dVar.y);
            }
            if (!TextUtils.isEmpty(dVar.I)) {
                this.f1328a.setSound(null).setDefaults(0).setLights(0, 0, 0).setVibrate(null);
            }
        }
    }

    public Notification.Builder a() {
        return this.f1328a;
    }

    public Notification b() {
        Bundle a2;
        RemoteViews d2;
        RemoteViews b2;
        i.e eVar = this.f1329b.o;
        if (eVar != null) {
            eVar.a(this);
        }
        RemoteViews c2 = eVar != null ? eVar.c(this) : null;
        Notification c3 = c();
        if (c2 != null) {
            c3.contentView = c2;
        } else {
            RemoteViews remoteViews = this.f1329b.F;
            if (remoteViews != null) {
                c3.contentView = remoteViews;
            }
        }
        if (!(Build.VERSION.SDK_INT < 16 || eVar == null || (b2 = eVar.b(this)) == null)) {
            c3.bigContentView = b2;
        }
        if (!(Build.VERSION.SDK_INT < 21 || eVar == null || (d2 = this.f1329b.o.d(this)) == null)) {
            c3.headsUpContentView = d2;
        }
        if (!(Build.VERSION.SDK_INT < 16 || eVar == null || (a2 = i.a(c3)) == null)) {
            eVar.a(a2);
        }
        return c3;
    }

    /* access modifiers changed from: protected */
    public Notification c() {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 26) {
            return this.f1328a.build();
        }
        if (i2 >= 24) {
            Notification build = this.f1328a.build();
            if (this.f1334g != 0) {
                if (!(build.getGroup() == null || (build.flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 || this.f1334g != 2)) {
                    a(build);
                }
                if (build.getGroup() != null && (build.flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 && this.f1334g == 1) {
                    a(build);
                }
            }
            return build;
        } else if (i2 >= 21) {
            this.f1328a.setExtras(this.f1333f);
            Notification build2 = this.f1328a.build();
            RemoteViews remoteViews = this.f1330c;
            if (remoteViews != null) {
                build2.contentView = remoteViews;
            }
            RemoteViews remoteViews2 = this.f1331d;
            if (remoteViews2 != null) {
                build2.bigContentView = remoteViews2;
            }
            RemoteViews remoteViews3 = this.f1335h;
            if (remoteViews3 != null) {
                build2.headsUpContentView = remoteViews3;
            }
            if (this.f1334g != 0) {
                if (!(build2.getGroup() == null || (build2.flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 || this.f1334g != 2)) {
                    a(build2);
                }
                if (build2.getGroup() != null && (build2.flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 && this.f1334g == 1) {
                    a(build2);
                }
            }
            return build2;
        } else if (i2 >= 20) {
            this.f1328a.setExtras(this.f1333f);
            Notification build3 = this.f1328a.build();
            RemoteViews remoteViews4 = this.f1330c;
            if (remoteViews4 != null) {
                build3.contentView = remoteViews4;
            }
            RemoteViews remoteViews5 = this.f1331d;
            if (remoteViews5 != null) {
                build3.bigContentView = remoteViews5;
            }
            if (this.f1334g != 0) {
                if (!(build3.getGroup() == null || (build3.flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 || this.f1334g != 2)) {
                    a(build3);
                }
                if (build3.getGroup() != null && (build3.flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 && this.f1334g == 1) {
                    a(build3);
                }
            }
            return build3;
        } else if (i2 >= 19) {
            SparseArray<Bundle> a2 = k.a(this.f1332e);
            if (a2 != null) {
                this.f1333f.putSparseParcelableArray("android.support.actionExtras", a2);
            }
            this.f1328a.setExtras(this.f1333f);
            Notification build4 = this.f1328a.build();
            RemoteViews remoteViews6 = this.f1330c;
            if (remoteViews6 != null) {
                build4.contentView = remoteViews6;
            }
            RemoteViews remoteViews7 = this.f1331d;
            if (remoteViews7 != null) {
                build4.bigContentView = remoteViews7;
            }
            return build4;
        } else if (i2 < 16) {
            return this.f1328a.getNotification();
        } else {
            Notification build5 = this.f1328a.build();
            Bundle a3 = i.a(build5);
            Bundle bundle = new Bundle(this.f1333f);
            for (String next : this.f1333f.keySet()) {
                if (a3.containsKey(next)) {
                    bundle.remove(next);
                }
            }
            a3.putAll(bundle);
            SparseArray<Bundle> a4 = k.a(this.f1332e);
            if (a4 != null) {
                i.a(build5).putSparseParcelableArray("android.support.actionExtras", a4);
            }
            RemoteViews remoteViews8 = this.f1330c;
            if (remoteViews8 != null) {
                build5.contentView = remoteViews8;
            }
            RemoteViews remoteViews9 = this.f1331d;
            if (remoteViews9 != null) {
                build5.bigContentView = remoteViews9;
            }
            return build5;
        }
    }

    private void a(i.a aVar) {
        Bundle bundle;
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 20) {
            Notification.Action.Builder builder = new Notification.Action.Builder(aVar.e(), aVar.i(), aVar.a());
            if (aVar.f() != null) {
                for (RemoteInput addRemoteInput : m.a(aVar.f())) {
                    builder.addRemoteInput(addRemoteInput);
                }
            }
            if (aVar.d() != null) {
                bundle = new Bundle(aVar.d());
            } else {
                bundle = new Bundle();
            }
            bundle.putBoolean("android.support.allowGeneratedReplies", aVar.b());
            if (Build.VERSION.SDK_INT >= 24) {
                builder.setAllowGeneratedReplies(aVar.b());
            }
            bundle.putInt("android.support.action.semanticAction", aVar.g());
            if (Build.VERSION.SDK_INT >= 28) {
                builder.setSemanticAction(aVar.g());
            }
            bundle.putBoolean("android.support.action.showsUserInterface", aVar.h());
            builder.addExtras(bundle);
            this.f1328a.addAction(builder.build());
        } else if (i2 >= 16) {
            this.f1332e.add(k.a(this.f1328a, aVar));
        }
    }

    private void a(Notification notification) {
        notification.sound = null;
        notification.vibrate = null;
        notification.defaults &= -2;
        notification.defaults &= -3;
    }
}
