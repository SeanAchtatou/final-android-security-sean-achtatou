package androidx.core.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.v4.app.a;
import android.util.Log;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: NotificationManagerCompat */
public final class l {

    /* renamed from: c  reason: collision with root package name */
    private static final Object f1339c = new Object();

    /* renamed from: d  reason: collision with root package name */
    private static String f1340d;

    /* renamed from: e  reason: collision with root package name */
    private static Set<String> f1341e = new HashSet();

    /* renamed from: f  reason: collision with root package name */
    private static final Object f1342f = new Object();

    /* renamed from: g  reason: collision with root package name */
    private static c f1343g;

    /* renamed from: a  reason: collision with root package name */
    private final Context f1344a;

    /* renamed from: b  reason: collision with root package name */
    private final NotificationManager f1345b = ((NotificationManager) this.f1344a.getSystemService("notification"));

    /* compiled from: NotificationManagerCompat */
    private static class a implements d {

        /* renamed from: a  reason: collision with root package name */
        final String f1346a;

        /* renamed from: b  reason: collision with root package name */
        final int f1347b;

        /* renamed from: c  reason: collision with root package name */
        final String f1348c;

        /* renamed from: d  reason: collision with root package name */
        final Notification f1349d;

        a(String str, int i2, String str2, Notification notification) {
            this.f1346a = str;
            this.f1347b = i2;
            this.f1348c = str2;
            this.f1349d = notification;
        }

        public void a(android.support.v4.app.a aVar) throws RemoteException {
            aVar.a(this.f1346a, this.f1347b, this.f1348c, this.f1349d);
        }

        public String toString() {
            return "NotifyTask[" + "packageName:" + this.f1346a + ", id:" + this.f1347b + ", tag:" + this.f1348c + "]";
        }
    }

    /* compiled from: NotificationManagerCompat */
    private static class b {

        /* renamed from: a  reason: collision with root package name */
        final ComponentName f1350a;

        /* renamed from: b  reason: collision with root package name */
        final IBinder f1351b;

        b(ComponentName componentName, IBinder iBinder) {
            this.f1350a = componentName;
            this.f1351b = iBinder;
        }
    }

    /* compiled from: NotificationManagerCompat */
    private static class c implements Handler.Callback, ServiceConnection {

        /* renamed from: a  reason: collision with root package name */
        private final Context f1352a;

        /* renamed from: b  reason: collision with root package name */
        private final HandlerThread f1353b;

        /* renamed from: c  reason: collision with root package name */
        private final Handler f1354c;

        /* renamed from: d  reason: collision with root package name */
        private final Map<ComponentName, a> f1355d = new HashMap();

        /* renamed from: e  reason: collision with root package name */
        private Set<String> f1356e = new HashSet();

        /* compiled from: NotificationManagerCompat */
        private static class a {

            /* renamed from: a  reason: collision with root package name */
            final ComponentName f1357a;

            /* renamed from: b  reason: collision with root package name */
            boolean f1358b = false;

            /* renamed from: c  reason: collision with root package name */
            android.support.v4.app.a f1359c;

            /* renamed from: d  reason: collision with root package name */
            ArrayDeque<d> f1360d = new ArrayDeque<>();

            /* renamed from: e  reason: collision with root package name */
            int f1361e = 0;

            a(ComponentName componentName) {
                this.f1357a = componentName;
            }
        }

        c(Context context) {
            this.f1352a = context;
            this.f1353b = new HandlerThread("NotificationManagerCompat");
            this.f1353b.start();
            this.f1354c = new Handler(this.f1353b.getLooper(), this);
        }

        private void b(d dVar) {
            a();
            for (a next : this.f1355d.values()) {
                next.f1360d.add(dVar);
                c(next);
            }
        }

        private void c(a aVar) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                Log.d("NotifManCompat", "Processing component " + aVar.f1357a + ", " + aVar.f1360d.size() + " queued tasks");
            }
            if (!aVar.f1360d.isEmpty()) {
                if (!a(aVar) || aVar.f1359c == null) {
                    d(aVar);
                    return;
                }
                while (true) {
                    d peek = aVar.f1360d.peek();
                    if (peek == null) {
                        break;
                    }
                    try {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Sending task " + peek);
                        }
                        peek.a(aVar.f1359c);
                        aVar.f1360d.remove();
                    } catch (DeadObjectException unused) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Remote service has died: " + aVar.f1357a);
                        }
                    } catch (RemoteException e2) {
                        Log.w("NotifManCompat", "RemoteException communicating with " + aVar.f1357a, e2);
                    }
                }
                if (!aVar.f1360d.isEmpty()) {
                    d(aVar);
                }
            }
        }

        private void d(a aVar) {
            if (!this.f1354c.hasMessages(3, aVar.f1357a)) {
                aVar.f1361e++;
                int i2 = aVar.f1361e;
                if (i2 > 6) {
                    Log.w("NotifManCompat", "Giving up on delivering " + aVar.f1360d.size() + " tasks to " + aVar.f1357a + " after " + aVar.f1361e + " retries");
                    aVar.f1360d.clear();
                    return;
                }
                int i3 = (1 << (i2 - 1)) * 1000;
                if (Log.isLoggable("NotifManCompat", 3)) {
                    Log.d("NotifManCompat", "Scheduling retry for " + i3 + " ms");
                }
                this.f1354c.sendMessageDelayed(this.f1354c.obtainMessage(3, aVar.f1357a), (long) i3);
            }
        }

        public void a(d dVar) {
            this.f1354c.obtainMessage(0, dVar).sendToTarget();
        }

        public boolean handleMessage(Message message) {
            int i2 = message.what;
            if (i2 == 0) {
                b((d) message.obj);
                return true;
            } else if (i2 == 1) {
                b bVar = (b) message.obj;
                a(bVar.f1350a, bVar.f1351b);
                return true;
            } else if (i2 == 2) {
                b((ComponentName) message.obj);
                return true;
            } else if (i2 != 3) {
                return false;
            } else {
                a((ComponentName) message.obj);
                return true;
            }
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                Log.d("NotifManCompat", "Connected to service " + componentName);
            }
            this.f1354c.obtainMessage(1, new b(componentName, iBinder)).sendToTarget();
        }

        public void onServiceDisconnected(ComponentName componentName) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                Log.d("NotifManCompat", "Disconnected from service " + componentName);
            }
            this.f1354c.obtainMessage(2, componentName).sendToTarget();
        }

        private void a(ComponentName componentName, IBinder iBinder) {
            a aVar = this.f1355d.get(componentName);
            if (aVar != null) {
                aVar.f1359c = a.C0002a.a(iBinder);
                aVar.f1361e = 0;
                c(aVar);
            }
        }

        private void b(ComponentName componentName) {
            a aVar = this.f1355d.get(componentName);
            if (aVar != null) {
                b(aVar);
            }
        }

        private void a(ComponentName componentName) {
            a aVar = this.f1355d.get(componentName);
            if (aVar != null) {
                c(aVar);
            }
        }

        private void b(a aVar) {
            if (aVar.f1358b) {
                this.f1352a.unbindService(this);
                aVar.f1358b = false;
            }
            aVar.f1359c = null;
        }

        private void a() {
            Set<String> b2 = l.b(this.f1352a);
            if (!b2.equals(this.f1356e)) {
                this.f1356e = b2;
                List<ResolveInfo> queryIntentServices = this.f1352a.getPackageManager().queryIntentServices(new Intent().setAction("android.support.BIND_NOTIFICATION_SIDE_CHANNEL"), 0);
                HashSet<ComponentName> hashSet = new HashSet<>();
                for (ResolveInfo next : queryIntentServices) {
                    if (b2.contains(next.serviceInfo.packageName)) {
                        ComponentName componentName = new ComponentName(next.serviceInfo.packageName, next.serviceInfo.name);
                        if (next.serviceInfo.permission != null) {
                            Log.w("NotifManCompat", "Permission present on component " + componentName + ", not adding listener record.");
                        } else {
                            hashSet.add(componentName);
                        }
                    }
                }
                for (ComponentName componentName2 : hashSet) {
                    if (!this.f1355d.containsKey(componentName2)) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Adding listener record for " + componentName2);
                        }
                        this.f1355d.put(componentName2, new a(componentName2));
                    }
                }
                Iterator<Map.Entry<ComponentName, a>> it = this.f1355d.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry next2 = it.next();
                    if (!hashSet.contains(next2.getKey())) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Removing listener record for " + next2.getKey());
                        }
                        b((a) next2.getValue());
                        it.remove();
                    }
                }
            }
        }

        private boolean a(a aVar) {
            if (aVar.f1358b) {
                return true;
            }
            aVar.f1358b = this.f1352a.bindService(new Intent("android.support.BIND_NOTIFICATION_SIDE_CHANNEL").setComponent(aVar.f1357a), this, 33);
            if (aVar.f1358b) {
                aVar.f1361e = 0;
            } else {
                Log.w("NotifManCompat", "Unable to bind to listener " + aVar.f1357a);
                this.f1352a.unbindService(this);
            }
            return aVar.f1358b;
        }
    }

    /* compiled from: NotificationManagerCompat */
    private interface d {
        void a(android.support.v4.app.a aVar) throws RemoteException;
    }

    private l(Context context) {
        this.f1344a = context;
    }

    public static l a(Context context) {
        return new l(context);
    }

    public static Set<String> b(Context context) {
        Set<String> set;
        String string = Settings.Secure.getString(context.getContentResolver(), "enabled_notification_listeners");
        synchronized (f1339c) {
            if (string != null) {
                if (!string.equals(f1340d)) {
                    String[] split = string.split(":", -1);
                    HashSet hashSet = new HashSet(split.length);
                    for (String unflattenFromString : split) {
                        ComponentName unflattenFromString2 = ComponentName.unflattenFromString(unflattenFromString);
                        if (unflattenFromString2 != null) {
                            hashSet.add(unflattenFromString2.getPackageName());
                        }
                    }
                    f1341e = hashSet;
                    f1340d = string;
                }
            }
            set = f1341e;
        }
        return set;
    }

    public void a(int i2, Notification notification) {
        a(null, i2, notification);
    }

    public void a(String str, int i2, Notification notification) {
        if (a(notification)) {
            a(new a(this.f1344a.getPackageName(), i2, str, notification));
            this.f1345b.cancel(str, i2);
            return;
        }
        this.f1345b.notify(str, i2, notification);
    }

    private static boolean a(Notification notification) {
        Bundle a2 = i.a(notification);
        return a2 != null && a2.getBoolean("android.support.useSideChannel");
    }

    private void a(d dVar) {
        synchronized (f1342f) {
            if (f1343g == null) {
                f1343g = new c(this.f1344a.getApplicationContext());
            }
            f1343g.a(dVar);
        }
    }
}
