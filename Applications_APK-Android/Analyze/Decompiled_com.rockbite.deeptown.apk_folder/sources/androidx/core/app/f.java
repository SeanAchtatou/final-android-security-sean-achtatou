package androidx.core.app;

import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.SparseIntArray;
import android.view.FrameMetrics;
import android.view.Window;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: FrameMetricsAggregator */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private b f1292a;

    /* compiled from: FrameMetricsAggregator */
    private static class b {
        b() {
        }

        public void a(Activity activity) {
        }

        public SparseIntArray[] b(Activity activity) {
            return null;
        }
    }

    public f() {
        this(1);
    }

    public void a(Activity activity) {
        this.f1292a.a(activity);
    }

    public SparseIntArray[] b(Activity activity) {
        return this.f1292a.b(activity);
    }

    public f(int i2) {
        if (Build.VERSION.SDK_INT >= 24) {
            this.f1292a = new a(i2);
        } else {
            this.f1292a = new b();
        }
    }

    /* compiled from: FrameMetricsAggregator */
    private static class a extends b {

        /* renamed from: e  reason: collision with root package name */
        private static HandlerThread f1293e;

        /* renamed from: f  reason: collision with root package name */
        private static Handler f1294f;

        /* renamed from: a  reason: collision with root package name */
        int f1295a;

        /* renamed from: b  reason: collision with root package name */
        SparseIntArray[] f1296b = new SparseIntArray[9];

        /* renamed from: c  reason: collision with root package name */
        private ArrayList<WeakReference<Activity>> f1297c = new ArrayList<>();

        /* renamed from: d  reason: collision with root package name */
        Window.OnFrameMetricsAvailableListener f1298d = new C0013a();

        /* renamed from: androidx.core.app.f$a$a  reason: collision with other inner class name */
        /* compiled from: FrameMetricsAggregator */
        class C0013a implements Window.OnFrameMetricsAvailableListener {
            C0013a() {
            }

            public void onFrameMetricsAvailable(Window window, FrameMetrics frameMetrics, int i2) {
                a aVar = a.this;
                if ((aVar.f1295a & 1) != 0) {
                    aVar.a(aVar.f1296b[0], frameMetrics.getMetric(8));
                }
                a aVar2 = a.this;
                if ((aVar2.f1295a & 2) != 0) {
                    aVar2.a(aVar2.f1296b[1], frameMetrics.getMetric(1));
                }
                a aVar3 = a.this;
                if ((aVar3.f1295a & 4) != 0) {
                    aVar3.a(aVar3.f1296b[2], frameMetrics.getMetric(3));
                }
                a aVar4 = a.this;
                if ((aVar4.f1295a & 8) != 0) {
                    aVar4.a(aVar4.f1296b[3], frameMetrics.getMetric(4));
                }
                a aVar5 = a.this;
                if ((aVar5.f1295a & 16) != 0) {
                    aVar5.a(aVar5.f1296b[4], frameMetrics.getMetric(5));
                }
                a aVar6 = a.this;
                if ((aVar6.f1295a & 64) != 0) {
                    aVar6.a(aVar6.f1296b[6], frameMetrics.getMetric(7));
                }
                a aVar7 = a.this;
                if ((aVar7.f1295a & 32) != 0) {
                    aVar7.a(aVar7.f1296b[5], frameMetrics.getMetric(6));
                }
                a aVar8 = a.this;
                if ((aVar8.f1295a & 128) != 0) {
                    aVar8.a(aVar8.f1296b[7], frameMetrics.getMetric(0));
                }
                a aVar9 = a.this;
                if ((aVar9.f1295a & 256) != 0) {
                    aVar9.a(aVar9.f1296b[8], frameMetrics.getMetric(2));
                }
            }
        }

        a(int i2) {
            this.f1295a = i2;
        }

        /* access modifiers changed from: package-private */
        public void a(SparseIntArray sparseIntArray, long j2) {
            if (sparseIntArray != null) {
                int i2 = (int) ((500000 + j2) / 1000000);
                if (j2 >= 0) {
                    sparseIntArray.put(i2, sparseIntArray.get(i2) + 1);
                }
            }
        }

        public SparseIntArray[] b(Activity activity) {
            Iterator<WeakReference<Activity>> it = this.f1297c.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                WeakReference next = it.next();
                if (next.get() == activity) {
                    this.f1297c.remove(next);
                    break;
                }
            }
            activity.getWindow().removeOnFrameMetricsAvailableListener(this.f1298d);
            return this.f1296b;
        }

        public void a(Activity activity) {
            if (f1293e == null) {
                f1293e = new HandlerThread("FrameMetricsAggregator");
                f1293e.start();
                f1294f = new Handler(f1293e.getLooper());
            }
            for (int i2 = 0; i2 <= 8; i2++) {
                SparseIntArray[] sparseIntArrayArr = this.f1296b;
                if (sparseIntArrayArr[i2] == null && (this.f1295a & (1 << i2)) != 0) {
                    sparseIntArrayArr[i2] = new SparseIntArray();
                }
            }
            activity.getWindow().addOnFrameMetricsAvailableListener(this.f1298d, f1294f);
            this.f1297c.add(new WeakReference(activity));
        }
    }
}
