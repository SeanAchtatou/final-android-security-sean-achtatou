package androidx.core.app;

import android.app.Activity;
import android.app.Application;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

/* compiled from: ActivityRecreator */
final class b {

    /* renamed from: a  reason: collision with root package name */
    protected static final Class<?> f1272a = a();

    /* renamed from: b  reason: collision with root package name */
    protected static final Field f1273b = b();

    /* renamed from: c  reason: collision with root package name */
    protected static final Field f1274c = c();

    /* renamed from: d  reason: collision with root package name */
    protected static final Method f1275d = b(f1272a);

    /* renamed from: e  reason: collision with root package name */
    protected static final Method f1276e = a(f1272a);

    /* renamed from: f  reason: collision with root package name */
    protected static final Method f1277f = c(f1272a);

    /* renamed from: g  reason: collision with root package name */
    private static final Handler f1278g = new Handler(Looper.getMainLooper());

    /* compiled from: ActivityRecreator */
    static class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ d f1279a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Object f1280b;

        a(d dVar, Object obj) {
            this.f1279a = dVar;
            this.f1280b = obj;
        }

        public void run() {
            this.f1279a.f1285a = this.f1280b;
        }
    }

    /* renamed from: androidx.core.app.b$b  reason: collision with other inner class name */
    /* compiled from: ActivityRecreator */
    static class C0012b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Application f1281a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ d f1282b;

        C0012b(Application application, d dVar) {
            this.f1281a = application;
            this.f1282b = dVar;
        }

        public void run() {
            this.f1281a.unregisterActivityLifecycleCallbacks(this.f1282b);
        }
    }

    /* compiled from: ActivityRecreator */
    static class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Object f1283a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Object f1284b;

        c(Object obj, Object obj2) {
            this.f1283a = obj;
            this.f1284b = obj2;
        }

        public void run() {
            try {
                if (b.f1275d != null) {
                    b.f1275d.invoke(this.f1283a, this.f1284b, false, "AppCompat recreation");
                    return;
                }
                b.f1276e.invoke(this.f1283a, this.f1284b, false);
            } catch (RuntimeException e2) {
                if (e2.getClass() == RuntimeException.class && e2.getMessage() != null && e2.getMessage().startsWith("Unable to stop")) {
                    throw e2;
                }
            } catch (Throwable th) {
                Log.e("ActivityRecreator", "Exception while invoking performStopActivity", th);
            }
        }
    }

    /* compiled from: ActivityRecreator */
    private static final class d implements Application.ActivityLifecycleCallbacks {

        /* renamed from: a  reason: collision with root package name */
        Object f1285a;

        /* renamed from: b  reason: collision with root package name */
        private Activity f1286b;

        /* renamed from: c  reason: collision with root package name */
        private boolean f1287c = false;

        /* renamed from: d  reason: collision with root package name */
        private boolean f1288d = false;

        /* renamed from: e  reason: collision with root package name */
        private boolean f1289e = false;

        d(Activity activity) {
            this.f1286b = activity;
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        public void onActivityDestroyed(Activity activity) {
            if (this.f1286b == activity) {
                this.f1286b = null;
                this.f1288d = true;
            }
        }

        public void onActivityPaused(Activity activity) {
            if (this.f1288d && !this.f1289e && !this.f1287c && b.a(this.f1285a, activity)) {
                this.f1289e = true;
                this.f1285a = null;
            }
        }

        public void onActivityResumed(Activity activity) {
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
            if (this.f1286b == activity) {
                this.f1287c = true;
            }
        }

        public void onActivityStopped(Activity activity) {
        }
    }

    static boolean a(Activity activity) {
        Object obj;
        Application application;
        d dVar;
        if (Build.VERSION.SDK_INT >= 28) {
            activity.recreate();
            return true;
        } else if (d() && f1277f == null) {
            return false;
        } else {
            if (f1276e == null && f1275d == null) {
                return false;
            }
            try {
                Object obj2 = f1274c.get(activity);
                if (obj2 == null || (obj = f1273b.get(activity)) == null) {
                    return false;
                }
                application = activity.getApplication();
                dVar = new d(activity);
                application.registerActivityLifecycleCallbacks(dVar);
                f1278g.post(new a(dVar, obj2));
                if (d()) {
                    f1277f.invoke(obj, obj2, null, null, 0, false, null, null, false, false);
                } else {
                    activity.recreate();
                }
                f1278g.post(new C0012b(application, dVar));
                return true;
            } catch (Throwable unused) {
                return false;
            }
        }
    }

    private static Method b(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        try {
            Method declaredMethod = cls.getDeclaredMethod("performStopActivity", IBinder.class, Boolean.TYPE, String.class);
            declaredMethod.setAccessible(true);
            return declaredMethod;
        } catch (Throwable unused) {
            return null;
        }
    }

    private static Method c(Class<?> cls) {
        if (d() && cls != null) {
            try {
                Method declaredMethod = cls.getDeclaredMethod("requestRelaunchActivity", IBinder.class, List.class, List.class, Integer.TYPE, Boolean.TYPE, Configuration.class, Configuration.class, Boolean.TYPE, Boolean.TYPE);
                declaredMethod.setAccessible(true);
                return declaredMethod;
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    private static boolean d() {
        int i2 = Build.VERSION.SDK_INT;
        return i2 == 26 || i2 == 27;
    }

    private static Field b() {
        try {
            Field declaredField = Activity.class.getDeclaredField("mMainThread");
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Throwable unused) {
            return null;
        }
    }

    private static Field c() {
        try {
            Field declaredField = Activity.class.getDeclaredField("mToken");
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Throwable unused) {
            return null;
        }
    }

    protected static boolean a(Object obj, Activity activity) {
        try {
            Object obj2 = f1274c.get(activity);
            if (obj2 != obj) {
                return false;
            }
            f1278g.postAtFrontOfQueue(new c(f1273b.get(activity), obj2));
            return true;
        } catch (Throwable th) {
            Log.e("ActivityRecreator", "Exception while fetching field values", th);
            return false;
        }
    }

    private static Method a(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        try {
            Method declaredMethod = cls.getDeclaredMethod("performStopActivity", IBinder.class, Boolean.TYPE);
            declaredMethod.setAccessible(true);
            return declaredMethod;
        } catch (Throwable unused) {
            return null;
        }
    }

    private static Class<?> a() {
        try {
            return Class.forName("android.app.ActivityThread");
        } catch (Throwable unused) {
            return null;
        }
    }
}
