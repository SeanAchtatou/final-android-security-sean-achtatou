package androidx.core.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.drive.DriveFile;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: TaskStackBuilder */
public final class o implements Iterable<Intent> {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList<Intent> f1362a = new ArrayList<>();

    /* renamed from: b  reason: collision with root package name */
    private final Context f1363b;

    /* compiled from: TaskStackBuilder */
    public interface a {
        Intent b();
    }

    private o(Context context) {
        this.f1363b = context;
    }

    public static o a(Context context) {
        return new o(context);
    }

    @Deprecated
    public Iterator<Intent> iterator() {
        return this.f1362a.iterator();
    }

    public o a(Intent intent) {
        this.f1362a.add(intent);
        return this;
    }

    public o a(Activity activity) {
        Intent b2 = activity instanceof a ? ((a) activity).b() : null;
        if (b2 == null) {
            b2 = g.a(activity);
        }
        if (b2 != null) {
            ComponentName component = b2.getComponent();
            if (component == null) {
                component = b2.resolveActivity(this.f1363b.getPackageManager());
            }
            a(component);
            a(b2);
        }
        return this;
    }

    public o a(ComponentName componentName) {
        int size = this.f1362a.size();
        try {
            Intent a2 = g.a(this.f1363b, componentName);
            while (a2 != null) {
                this.f1362a.add(size, a2);
                a2 = g.a(this.f1363b, a2.getComponent());
            }
            return this;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
            throw new IllegalArgumentException(e2);
        }
    }

    public void a() {
        a((Bundle) null);
    }

    public void a(Bundle bundle) {
        if (!this.f1362a.isEmpty()) {
            ArrayList<Intent> arrayList = this.f1362a;
            Intent[] intentArr = (Intent[]) arrayList.toArray(new Intent[arrayList.size()]);
            intentArr[0] = new Intent(intentArr[0]).addFlags(268484608);
            if (!b.e.e.a.a(this.f1363b, intentArr, bundle)) {
                Intent intent = new Intent(intentArr[intentArr.length - 1]);
                intent.addFlags(DriveFile.MODE_READ_ONLY);
                this.f1363b.startActivity(intent);
                return;
            }
            return;
        }
        throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
    }
}
