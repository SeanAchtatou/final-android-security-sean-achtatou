package androidx.core.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.RemoteViews;
import java.util.ArrayList;

/* compiled from: NotificationCompat */
public class i {

    /* compiled from: NotificationCompat */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        final Bundle f1300a;

        /* renamed from: b  reason: collision with root package name */
        private final m[] f1301b;

        /* renamed from: c  reason: collision with root package name */
        private final m[] f1302c;

        /* renamed from: d  reason: collision with root package name */
        private boolean f1303d;

        /* renamed from: e  reason: collision with root package name */
        boolean f1304e;

        /* renamed from: f  reason: collision with root package name */
        private final int f1305f;

        /* renamed from: g  reason: collision with root package name */
        public int f1306g;

        /* renamed from: h  reason: collision with root package name */
        public CharSequence f1307h;

        /* renamed from: i  reason: collision with root package name */
        public PendingIntent f1308i;

        public a(int i2, CharSequence charSequence, PendingIntent pendingIntent) {
            this(i2, charSequence, pendingIntent, new Bundle(), null, null, true, 0, true);
        }

        public PendingIntent a() {
            return this.f1308i;
        }

        public boolean b() {
            return this.f1303d;
        }

        public m[] c() {
            return this.f1302c;
        }

        public Bundle d() {
            return this.f1300a;
        }

        public int e() {
            return this.f1306g;
        }

        public m[] f() {
            return this.f1301b;
        }

        public int g() {
            return this.f1305f;
        }

        public boolean h() {
            return this.f1304e;
        }

        public CharSequence i() {
            return this.f1307h;
        }

        a(int i2, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle, m[] mVarArr, m[] mVarArr2, boolean z, int i3, boolean z2) {
            this.f1304e = true;
            this.f1306g = i2;
            this.f1307h = d.d(charSequence);
            this.f1308i = pendingIntent;
            this.f1300a = bundle == null ? new Bundle() : bundle;
            this.f1301b = mVarArr;
            this.f1302c = mVarArr2;
            this.f1303d = z;
            this.f1305f = i3;
            this.f1304e = z2;
        }
    }

    /* compiled from: NotificationCompat */
    public static class c extends e {

        /* renamed from: e  reason: collision with root package name */
        private CharSequence f1312e;

        public c a(CharSequence charSequence) {
            this.f1312e = d.d(charSequence);
            return this;
        }

        public void a(h hVar) {
            if (Build.VERSION.SDK_INT >= 16) {
                Notification.BigTextStyle bigText = new Notification.BigTextStyle(hVar.a()).setBigContentTitle(this.f1325b).bigText(this.f1312e);
                if (this.f1327d) {
                    bigText.setSummaryText(this.f1326c);
                }
            }
        }
    }

    /* compiled from: NotificationCompat */
    public static class d {
        String A;
        Bundle B;
        int C;
        int D;
        Notification E;
        RemoteViews F;
        RemoteViews G;
        RemoteViews H;
        String I;
        int J;
        String K;
        long L;
        int M;
        Notification N;
        @Deprecated
        public ArrayList<String> O;

        /* renamed from: a  reason: collision with root package name */
        public Context f1313a;

        /* renamed from: b  reason: collision with root package name */
        public ArrayList<a> f1314b;

        /* renamed from: c  reason: collision with root package name */
        ArrayList<a> f1315c;

        /* renamed from: d  reason: collision with root package name */
        CharSequence f1316d;

        /* renamed from: e  reason: collision with root package name */
        CharSequence f1317e;

        /* renamed from: f  reason: collision with root package name */
        PendingIntent f1318f;

        /* renamed from: g  reason: collision with root package name */
        PendingIntent f1319g;

        /* renamed from: h  reason: collision with root package name */
        RemoteViews f1320h;

        /* renamed from: i  reason: collision with root package name */
        Bitmap f1321i;

        /* renamed from: j  reason: collision with root package name */
        CharSequence f1322j;

        /* renamed from: k  reason: collision with root package name */
        int f1323k;
        int l;
        boolean m;
        boolean n;
        e o;
        CharSequence p;
        CharSequence[] q;
        int r;
        int s;
        boolean t;
        String u;
        boolean v;
        String w;
        boolean x;
        boolean y;
        boolean z;

        public d(Context context, String str) {
            this.f1314b = new ArrayList<>();
            this.f1315c = new ArrayList<>();
            this.m = true;
            this.x = false;
            this.C = 0;
            this.D = 0;
            this.J = 0;
            this.M = 0;
            this.N = new Notification();
            this.f1313a = context;
            this.I = str;
            this.N.when = System.currentTimeMillis();
            this.N.audioStreamType = -1;
            this.l = 0;
            this.O = new ArrayList<>();
        }

        public d a(long j2) {
            this.N.when = j2;
            return this;
        }

        public d b(CharSequence charSequence) {
            this.f1316d = d(charSequence);
            return this;
        }

        public d c(int i2) {
            this.f1323k = i2;
            return this;
        }

        public d d(boolean z2) {
            this.m = z2;
            return this;
        }

        public d e(int i2) {
            this.N.icon = i2;
            return this;
        }

        public d f(int i2) {
            this.D = i2;
            return this;
        }

        public d a(CharSequence charSequence) {
            this.f1317e = d(charSequence);
            return this;
        }

        public d b(PendingIntent pendingIntent) {
            this.N.deleteIntent = pendingIntent;
            return this;
        }

        public d c(CharSequence charSequence) {
            this.N.tickerText = d(charSequence);
            return this;
        }

        public d d(int i2) {
            this.l = i2;
            return this;
        }

        private Bitmap b(Bitmap bitmap) {
            if (bitmap == null || Build.VERSION.SDK_INT >= 27) {
                return bitmap;
            }
            Resources resources = this.f1313a.getResources();
            int dimensionPixelSize = resources.getDimensionPixelSize(b.e.b.compat_notification_large_icon_max_width);
            int dimensionPixelSize2 = resources.getDimensionPixelSize(b.e.b.compat_notification_large_icon_max_height);
            if (bitmap.getWidth() <= dimensionPixelSize && bitmap.getHeight() <= dimensionPixelSize2) {
                return bitmap;
            }
            double d2 = (double) dimensionPixelSize;
            double max = (double) Math.max(1, bitmap.getWidth());
            Double.isNaN(d2);
            Double.isNaN(max);
            double d3 = d2 / max;
            double d4 = (double) dimensionPixelSize2;
            double max2 = (double) Math.max(1, bitmap.getHeight());
            Double.isNaN(d4);
            Double.isNaN(max2);
            double min = Math.min(d3, d4 / max2);
            double width = (double) bitmap.getWidth();
            Double.isNaN(width);
            double height = (double) bitmap.getHeight();
            Double.isNaN(height);
            return Bitmap.createScaledBitmap(bitmap, (int) Math.ceil(width * min), (int) Math.ceil(height * min), true);
        }

        protected static CharSequence d(CharSequence charSequence) {
            return (charSequence != null && charSequence.length() > 5120) ? charSequence.subSequence(0, 5120) : charSequence;
        }

        public d a(PendingIntent pendingIntent) {
            this.f1318f = pendingIntent;
            return this;
        }

        public d c(boolean z2) {
            a(8, z2);
            return this;
        }

        public d a(Bitmap bitmap) {
            this.f1321i = b(bitmap);
            return this;
        }

        public d a(Uri uri) {
            Notification notification = this.N;
            notification.sound = uri;
            notification.audioStreamType = -1;
            if (Build.VERSION.SDK_INT >= 21) {
                notification.audioAttributes = new AudioAttributes.Builder().setContentType(4).setUsage(5).build();
            }
            return this;
        }

        public d a(long[] jArr) {
            this.N.vibrate = jArr;
            return this;
        }

        public d a(int i2, int i3, int i4) {
            Notification notification = this.N;
            notification.ledARGB = i2;
            notification.ledOnMS = i3;
            notification.ledOffMS = i4;
            int i5 = (notification.ledOnMS == 0 || notification.ledOffMS == 0) ? 0 : 1;
            Notification notification2 = this.N;
            notification2.flags = i5 | (notification2.flags & -2);
            return this;
        }

        public d b(boolean z2) {
            this.x = z2;
            return this;
        }

        @Deprecated
        public d(Context context) {
            this(context, null);
        }

        public d b(int i2) {
            Notification notification = this.N;
            notification.defaults = i2;
            if ((i2 & 4) != 0) {
                notification.flags |= 1;
            }
            return this;
        }

        public d a(boolean z2) {
            a(16, z2);
            return this;
        }

        private void a(int i2, boolean z2) {
            if (z2) {
                Notification notification = this.N;
                notification.flags = i2 | notification.flags;
                return;
            }
            Notification notification2 = this.N;
            notification2.flags = (i2 ^ -1) & notification2.flags;
        }

        public Bundle b() {
            if (this.B == null) {
                this.B = new Bundle();
            }
            return this.B;
        }

        public d a(int i2, CharSequence charSequence, PendingIntent pendingIntent) {
            this.f1314b.add(new a(i2, charSequence, pendingIntent));
            return this;
        }

        public d a(e eVar) {
            if (this.o != eVar) {
                this.o = eVar;
                e eVar2 = this.o;
                if (eVar2 != null) {
                    eVar2.a(this);
                }
            }
            return this;
        }

        public d a(int i2) {
            this.C = i2;
            return this;
        }

        public d a(String str) {
            this.I = str;
            return this;
        }

        public Notification a() {
            return new j(this).b();
        }
    }

    /* compiled from: NotificationCompat */
    public static abstract class e {

        /* renamed from: a  reason: collision with root package name */
        protected d f1324a;

        /* renamed from: b  reason: collision with root package name */
        CharSequence f1325b;

        /* renamed from: c  reason: collision with root package name */
        CharSequence f1326c;

        /* renamed from: d  reason: collision with root package name */
        boolean f1327d = false;

        public void a(Bundle bundle) {
        }

        public abstract void a(h hVar);

        public void a(d dVar) {
            if (this.f1324a != dVar) {
                this.f1324a = dVar;
                d dVar2 = this.f1324a;
                if (dVar2 != null) {
                    dVar2.a(this);
                }
            }
        }

        public RemoteViews b(h hVar) {
            return null;
        }

        public RemoteViews c(h hVar) {
            return null;
        }

        public RemoteViews d(h hVar) {
            return null;
        }
    }

    public static Bundle a(Notification notification) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 19) {
            return notification.extras;
        }
        if (i2 >= 16) {
            return k.a(notification);
        }
        return null;
    }

    /* compiled from: NotificationCompat */
    public static class b extends e {

        /* renamed from: e  reason: collision with root package name */
        private Bitmap f1309e;

        /* renamed from: f  reason: collision with root package name */
        private Bitmap f1310f;

        /* renamed from: g  reason: collision with root package name */
        private boolean f1311g;

        public b a(Bitmap bitmap) {
            this.f1310f = bitmap;
            this.f1311g = true;
            return this;
        }

        public b b(Bitmap bitmap) {
            this.f1309e = bitmap;
            return this;
        }

        public void a(h hVar) {
            if (Build.VERSION.SDK_INT >= 16) {
                Notification.BigPictureStyle bigPicture = new Notification.BigPictureStyle(hVar.a()).setBigContentTitle(this.f1325b).bigPicture(this.f1309e);
                if (this.f1311g) {
                    bigPicture.bigLargeIcon(this.f1310f);
                }
                if (this.f1327d) {
                    bigPicture.setSummaryText(this.f1326c);
                }
            }
        }
    }
}
