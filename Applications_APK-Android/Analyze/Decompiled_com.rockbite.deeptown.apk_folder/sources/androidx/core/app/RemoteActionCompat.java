package androidx.core.app;

import android.app.PendingIntent;
import androidx.core.graphics.drawable.IconCompat;
import androidx.versionedparcelable.c;

public final class RemoteActionCompat implements c {

    /* renamed from: a  reason: collision with root package name */
    public IconCompat f1261a;

    /* renamed from: b  reason: collision with root package name */
    public CharSequence f1262b;

    /* renamed from: c  reason: collision with root package name */
    public CharSequence f1263c;

    /* renamed from: d  reason: collision with root package name */
    public PendingIntent f1264d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f1265e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1266f;
}
