package androidx.core.app;

import android.app.Activity;
import android.app.SharedElementCallback;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.view.View;
import androidx.core.app.n;
import java.util.List;
import java.util.Map;

/* compiled from: ActivityCompat */
public class a extends b.e.e.a {

    /* renamed from: c  reason: collision with root package name */
    private static c f1267c;

    /* renamed from: androidx.core.app.a$a  reason: collision with other inner class name */
    /* compiled from: ActivityCompat */
    static class C0010a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String[] f1268a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Activity f1269b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ int f1270c;

        C0010a(String[] strArr, Activity activity, int i2) {
            this.f1268a = strArr;
            this.f1269b = activity;
            this.f1270c = i2;
        }

        public void run() {
            int[] iArr = new int[this.f1268a.length];
            PackageManager packageManager = this.f1269b.getPackageManager();
            String packageName = this.f1269b.getPackageName();
            int length = this.f1268a.length;
            for (int i2 = 0; i2 < length; i2++) {
                iArr[i2] = packageManager.checkPermission(this.f1268a[i2], packageName);
            }
            ((b) this.f1269b).onRequestPermissionsResult(this.f1270c, this.f1268a, iArr);
        }
    }

    /* compiled from: ActivityCompat */
    public interface b {
        void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr);
    }

    /* compiled from: ActivityCompat */
    public interface c {
        boolean a(Activity activity, String[] strArr, int i2);

        boolean onActivityResult(Activity activity, int i2, int i3, Intent intent);
    }

    /* compiled from: ActivityCompat */
    public interface d {
        void validateRequestPermissionsRequestCode(int i2);
    }

    /* compiled from: ActivityCompat */
    private static class e extends SharedElementCallback {

        /* renamed from: a  reason: collision with root package name */
        private final n f1271a;

        /* renamed from: androidx.core.app.a$e$a  reason: collision with other inner class name */
        /* compiled from: ActivityCompat */
        class C0011a implements n.a {
            C0011a(e eVar, SharedElementCallback.OnSharedElementsReadyListener onSharedElementsReadyListener) {
            }
        }

        e(n nVar) {
            this.f1271a = nVar;
        }

        public Parcelable onCaptureSharedElementSnapshot(View view, Matrix matrix, RectF rectF) {
            return this.f1271a.a(view, matrix, rectF);
        }

        public View onCreateSnapshotView(Context context, Parcelable parcelable) {
            return this.f1271a.a(context, parcelable);
        }

        public void onMapSharedElements(List<String> list, Map<String, View> map) {
            this.f1271a.a(list, map);
        }

        public void onRejectSharedElements(List<View> list) {
            this.f1271a.a(list);
        }

        public void onSharedElementEnd(List<String> list, List<View> list2, List<View> list3) {
            this.f1271a.a(list, list2, list3);
        }

        public void onSharedElementStart(List<String> list, List<View> list2, List<View> list3) {
            this.f1271a.b(list, list2, list3);
        }

        public void onSharedElementsArrived(List<String> list, List<View> list2, SharedElementCallback.OnSharedElementsReadyListener onSharedElementsReadyListener) {
            this.f1271a.a(list, list2, new C0011a(this, onSharedElementsReadyListener));
        }
    }

    public static c a() {
        return f1267c;
    }

    public static void b(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.finishAfterTransition();
        } else {
            activity.finish();
        }
    }

    public static Uri c(Activity activity) {
        if (Build.VERSION.SDK_INT >= 22) {
            return activity.getReferrer();
        }
        Intent intent = activity.getIntent();
        Uri uri = (Uri) intent.getParcelableExtra("android.intent.extra.REFERRER");
        if (uri != null) {
            return uri;
        }
        String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        if (stringExtra != null) {
            return Uri.parse(stringExtra);
        }
        return null;
    }

    public static void d(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.postponeEnterTransition();
        }
    }

    public static void e(Activity activity) {
        if (Build.VERSION.SDK_INT >= 28) {
            activity.recreate();
        } else if (!b.a(activity)) {
            activity.recreate();
        }
    }

    public static void f(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.startPostponedEnterTransition();
        }
    }

    public static void a(Activity activity, Intent intent, int i2, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.startActivityForResult(intent, i2, bundle);
        } else {
            activity.startActivityForResult(intent, i2);
        }
    }

    public static void b(Activity activity, n nVar) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.setExitSharedElementCallback(nVar != null ? new e(nVar) : null);
        }
    }

    public static void a(Activity activity, IntentSender intentSender, int i2, Intent intent, int i3, int i4, int i5, Bundle bundle) throws IntentSender.SendIntentException {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.startIntentSenderForResult(intentSender, i2, intent, i3, i4, i5, bundle);
        } else {
            activity.startIntentSenderForResult(intentSender, i2, intent, i3, i4, i5);
        }
    }

    public static void a(Activity activity) {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.finishAffinity();
        } else {
            activity.finish();
        }
    }

    public static void a(Activity activity, n nVar) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.setEnterSharedElementCallback(nVar != null ? new e(nVar) : null);
        }
    }

    public static void a(Activity activity, String[] strArr, int i2) {
        c cVar = f1267c;
        if (cVar != null && cVar.a(activity, strArr, i2)) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity instanceof d) {
                ((d) activity).validateRequestPermissionsRequestCode(i2);
            }
            activity.requestPermissions(strArr, i2);
        } else if (activity instanceof b) {
            new Handler(Looper.getMainLooper()).post(new C0010a(strArr, activity, i2));
        }
    }

    public static boolean a(Activity activity, String str) {
        if (Build.VERSION.SDK_INT >= 23) {
            return activity.shouldShowRequestPermissionRationale(str);
        }
        return false;
    }
}
