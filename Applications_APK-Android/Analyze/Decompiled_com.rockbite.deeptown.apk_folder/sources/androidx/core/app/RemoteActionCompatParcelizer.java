package androidx.core.app;

import android.app.PendingIntent;
import androidx.core.graphics.drawable.IconCompat;
import androidx.versionedparcelable.a;

public class RemoteActionCompatParcelizer {
    public static RemoteActionCompat read(a aVar) {
        RemoteActionCompat remoteActionCompat = new RemoteActionCompat();
        remoteActionCompat.f1261a = (IconCompat) aVar.a(remoteActionCompat.f1261a, 1);
        remoteActionCompat.f1262b = aVar.a(remoteActionCompat.f1262b, 2);
        remoteActionCompat.f1263c = aVar.a(remoteActionCompat.f1263c, 3);
        remoteActionCompat.f1264d = (PendingIntent) aVar.a(remoteActionCompat.f1264d, 4);
        remoteActionCompat.f1265e = aVar.a(remoteActionCompat.f1265e, 5);
        remoteActionCompat.f1266f = aVar.a(remoteActionCompat.f1266f, 6);
        return remoteActionCompat;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.versionedparcelable.a.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      androidx.versionedparcelable.a.a(int, int):int
      androidx.versionedparcelable.a.a(android.os.Parcelable, int):T
      androidx.versionedparcelable.a.a(androidx.versionedparcelable.c, int):T
      androidx.versionedparcelable.a.a(java.lang.String, androidx.versionedparcelable.a):T
      androidx.versionedparcelable.a.a(java.lang.CharSequence, int):java.lang.CharSequence
      androidx.versionedparcelable.a.a(java.lang.String, int):java.lang.String
      androidx.versionedparcelable.a.a(androidx.versionedparcelable.c, androidx.versionedparcelable.a):void
      androidx.versionedparcelable.a.a(boolean, int):boolean
      androidx.versionedparcelable.a.a(byte[], int):byte[]
      androidx.versionedparcelable.a.a(boolean, boolean):void */
    public static void write(RemoteActionCompat remoteActionCompat, a aVar) {
        aVar.a(false, false);
        aVar.b(remoteActionCompat.f1261a, 1);
        aVar.b(remoteActionCompat.f1262b, 2);
        aVar.b(remoteActionCompat.f1263c, 3);
        aVar.b(remoteActionCompat.f1264d, 4);
        aVar.b(remoteActionCompat.f1265e, 5);
        aVar.b(remoteActionCompat.f1266f, 6);
    }
}
