package androidx.core.graphics.drawable;

import android.content.res.ColorStateList;
import android.os.Parcelable;
import androidx.versionedparcelable.a;

public class IconCompatParcelizer {
    public static IconCompat read(a aVar) {
        IconCompat iconCompat = new IconCompat();
        iconCompat.f1365a = aVar.a(iconCompat.f1365a, 1);
        iconCompat.f1367c = aVar.a(iconCompat.f1367c, 2);
        iconCompat.f1368d = aVar.a(iconCompat.f1368d, 3);
        iconCompat.f1369e = aVar.a(iconCompat.f1369e, 4);
        iconCompat.f1370f = aVar.a(iconCompat.f1370f, 5);
        iconCompat.f1371g = (ColorStateList) aVar.a(iconCompat.f1371g, 6);
        iconCompat.f1373i = aVar.a(iconCompat.f1373i, 7);
        iconCompat.c();
        return iconCompat;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.versionedparcelable.a.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      androidx.versionedparcelable.a.a(int, int):int
      androidx.versionedparcelable.a.a(android.os.Parcelable, int):T
      androidx.versionedparcelable.a.a(androidx.versionedparcelable.c, int):T
      androidx.versionedparcelable.a.a(java.lang.String, androidx.versionedparcelable.a):T
      androidx.versionedparcelable.a.a(java.lang.CharSequence, int):java.lang.CharSequence
      androidx.versionedparcelable.a.a(java.lang.String, int):java.lang.String
      androidx.versionedparcelable.a.a(androidx.versionedparcelable.c, androidx.versionedparcelable.a):void
      androidx.versionedparcelable.a.a(boolean, int):boolean
      androidx.versionedparcelable.a.a(byte[], int):byte[]
      androidx.versionedparcelable.a.a(boolean, boolean):void */
    public static void write(IconCompat iconCompat, a aVar) {
        aVar.a(true, true);
        iconCompat.a(aVar.c());
        int i2 = iconCompat.f1365a;
        if (-1 != i2) {
            aVar.b(i2, 1);
        }
        byte[] bArr = iconCompat.f1367c;
        if (bArr != null) {
            aVar.b(bArr, 2);
        }
        Parcelable parcelable = iconCompat.f1368d;
        if (parcelable != null) {
            aVar.b(parcelable, 3);
        }
        int i3 = iconCompat.f1369e;
        if (i3 != 0) {
            aVar.b(i3, 4);
        }
        int i4 = iconCompat.f1370f;
        if (i4 != 0) {
            aVar.b(i4, 5);
        }
        ColorStateList colorStateList = iconCompat.f1371g;
        if (colorStateList != null) {
            aVar.b(colorStateList, 6);
        }
        String str = iconCompat.f1373i;
        if (str != null) {
            aVar.b(str, 7);
        }
    }
}
