package androidx.core.graphics.drawable;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;

/* compiled from: WrappedDrawableApi14 */
class d extends Drawable implements Drawable.Callback, c, b {

    /* renamed from: g  reason: collision with root package name */
    static final PorterDuff.Mode f1378g = PorterDuff.Mode.SRC_IN;

    /* renamed from: a  reason: collision with root package name */
    private int f1379a;

    /* renamed from: b  reason: collision with root package name */
    private PorterDuff.Mode f1380b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f1381c;

    /* renamed from: d  reason: collision with root package name */
    f f1382d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f1383e;

    /* renamed from: f  reason: collision with root package name */
    Drawable f1384f;

    d(f fVar, Resources resources) {
        this.f1382d = fVar;
        a(resources);
    }

    private void a(Resources resources) {
        Drawable.ConstantState constantState;
        f fVar = this.f1382d;
        if (fVar != null && (constantState = fVar.f1387b) != null) {
            a(constantState.newDrawable(resources));
        }
    }

    private f c() {
        return new f(this.f1382d);
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return true;
    }

    public void draw(Canvas canvas) {
        this.f1384f.draw(canvas);
    }

    public int getChangingConfigurations() {
        int changingConfigurations = super.getChangingConfigurations();
        f fVar = this.f1382d;
        return changingConfigurations | (fVar != null ? fVar.getChangingConfigurations() : 0) | this.f1384f.getChangingConfigurations();
    }

    public Drawable.ConstantState getConstantState() {
        f fVar = this.f1382d;
        if (fVar == null || !fVar.a()) {
            return null;
        }
        this.f1382d.f1386a = getChangingConfigurations();
        return this.f1382d;
    }

    public Drawable getCurrent() {
        return this.f1384f.getCurrent();
    }

    public int getIntrinsicHeight() {
        return this.f1384f.getIntrinsicHeight();
    }

    public int getIntrinsicWidth() {
        return this.f1384f.getIntrinsicWidth();
    }

    public int getMinimumHeight() {
        return this.f1384f.getMinimumHeight();
    }

    public int getMinimumWidth() {
        return this.f1384f.getMinimumWidth();
    }

    public int getOpacity() {
        return this.f1384f.getOpacity();
    }

    public boolean getPadding(Rect rect) {
        return this.f1384f.getPadding(rect);
    }

    public int[] getState() {
        return this.f1384f.getState();
    }

    public Region getTransparentRegion() {
        return this.f1384f.getTransparentRegion();
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public boolean isAutoMirrored() {
        return this.f1384f.isAutoMirrored();
    }

    public boolean isStateful() {
        f fVar;
        ColorStateList colorStateList = (!b() || (fVar = this.f1382d) == null) ? null : fVar.f1388c;
        return (colorStateList != null && colorStateList.isStateful()) || this.f1384f.isStateful();
    }

    public void jumpToCurrentState() {
        this.f1384f.jumpToCurrentState();
    }

    public Drawable mutate() {
        if (!this.f1383e && super.mutate() == this) {
            this.f1382d = c();
            Drawable drawable = this.f1384f;
            if (drawable != null) {
                drawable.mutate();
            }
            f fVar = this.f1382d;
            if (fVar != null) {
                Drawable drawable2 = this.f1384f;
                fVar.f1387b = drawable2 != null ? drawable2.getConstantState() : null;
            }
            this.f1383e = true;
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.f1384f;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i2) {
        return this.f1384f.setLevel(i2);
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
        scheduleSelf(runnable, j2);
    }

    public void setAlpha(int i2) {
        this.f1384f.setAlpha(i2);
    }

    public void setAutoMirrored(boolean z) {
        this.f1384f.setAutoMirrored(z);
    }

    public void setChangingConfigurations(int i2) {
        this.f1384f.setChangingConfigurations(i2);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f1384f.setColorFilter(colorFilter);
    }

    public void setDither(boolean z) {
        this.f1384f.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f1384f.setFilterBitmap(z);
    }

    public boolean setState(int[] iArr) {
        return a(iArr) || this.f1384f.setState(iArr);
    }

    public void setTint(int i2) {
        setTintList(ColorStateList.valueOf(i2));
    }

    public void setTintList(ColorStateList colorStateList) {
        this.f1382d.f1388c = colorStateList;
        a(getState());
    }

    public void setTintMode(PorterDuff.Mode mode) {
        this.f1382d.f1389d = mode;
        a(getState());
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f1384f.setVisible(z, z2);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    private boolean a(int[] iArr) {
        if (!b()) {
            return false;
        }
        f fVar = this.f1382d;
        ColorStateList colorStateList = fVar.f1388c;
        PorterDuff.Mode mode = fVar.f1389d;
        if (colorStateList == null || mode == null) {
            this.f1381c = false;
            clearColorFilter();
        } else {
            int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
            if (!(this.f1381c && colorForState == this.f1379a && mode == this.f1380b)) {
                setColorFilter(colorForState, mode);
                this.f1379a = colorForState;
                this.f1380b = mode;
                this.f1381c = true;
                return true;
            }
        }
        return false;
    }

    d(Drawable drawable) {
        this.f1382d = c();
        a(drawable);
    }

    public final Drawable a() {
        return this.f1384f;
    }

    public final void a(Drawable drawable) {
        Drawable drawable2 = this.f1384f;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.f1384f = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            setVisible(drawable.isVisible(), true);
            setState(drawable.getState());
            setLevel(drawable.getLevel());
            setBounds(drawable.getBounds());
            f fVar = this.f1382d;
            if (fVar != null) {
                fVar.f1387b = drawable.getConstantState();
            }
        }
        invalidateSelf();
    }
}
