package androidx.core.graphics.drawable;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;

/* compiled from: WrappedDrawableState */
final class f extends Drawable.ConstantState {

    /* renamed from: a  reason: collision with root package name */
    int f1386a;

    /* renamed from: b  reason: collision with root package name */
    Drawable.ConstantState f1387b;

    /* renamed from: c  reason: collision with root package name */
    ColorStateList f1388c = null;

    /* renamed from: d  reason: collision with root package name */
    PorterDuff.Mode f1389d = d.f1378g;

    f(f fVar) {
        if (fVar != null) {
            this.f1386a = fVar.f1386a;
            this.f1387b = fVar.f1387b;
            this.f1388c = fVar.f1388c;
            this.f1389d = fVar.f1389d;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.f1387b != null;
    }

    public int getChangingConfigurations() {
        int i2 = this.f1386a;
        Drawable.ConstantState constantState = this.f1387b;
        return i2 | (constantState != null ? constantState.getChangingConfigurations() : 0);
    }

    public Drawable newDrawable() {
        return newDrawable(null);
    }

    public Drawable newDrawable(Resources resources) {
        if (Build.VERSION.SDK_INT >= 21) {
            return new e(this, resources);
        }
        return new d(this, resources);
    }
}
