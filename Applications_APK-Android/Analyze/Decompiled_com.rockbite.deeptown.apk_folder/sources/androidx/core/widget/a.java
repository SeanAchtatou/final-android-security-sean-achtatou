package androidx.core.widget;

import android.content.res.Resources;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import b.e.m.u;
import com.esotericsoftware.spine.Animation;
import com.tapjoy.TJAdUnitConstants;

/* compiled from: AutoScrollHelper */
public abstract class a implements View.OnTouchListener {
    private static final int r = ViewConfiguration.getTapTimeout();

    /* renamed from: a  reason: collision with root package name */
    final C0014a f1402a = new C0014a();

    /* renamed from: b  reason: collision with root package name */
    private final Interpolator f1403b = new AccelerateInterpolator();

    /* renamed from: c  reason: collision with root package name */
    final View f1404c;

    /* renamed from: d  reason: collision with root package name */
    private Runnable f1405d;

    /* renamed from: e  reason: collision with root package name */
    private float[] f1406e = {Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR};

    /* renamed from: f  reason: collision with root package name */
    private float[] f1407f = {Float.MAX_VALUE, Float.MAX_VALUE};

    /* renamed from: g  reason: collision with root package name */
    private int f1408g;

    /* renamed from: h  reason: collision with root package name */
    private int f1409h;

    /* renamed from: i  reason: collision with root package name */
    private float[] f1410i = {Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR};

    /* renamed from: j  reason: collision with root package name */
    private float[] f1411j = {Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR};

    /* renamed from: k  reason: collision with root package name */
    private float[] f1412k = {Float.MAX_VALUE, Float.MAX_VALUE};
    private boolean l;
    boolean m;
    boolean n;
    boolean o;
    private boolean p;
    private boolean q;

    /* renamed from: androidx.core.widget.a$a  reason: collision with other inner class name */
    /* compiled from: AutoScrollHelper */
    private static class C0014a {

        /* renamed from: a  reason: collision with root package name */
        private int f1413a;

        /* renamed from: b  reason: collision with root package name */
        private int f1414b;

        /* renamed from: c  reason: collision with root package name */
        private float f1415c;

        /* renamed from: d  reason: collision with root package name */
        private float f1416d;

        /* renamed from: e  reason: collision with root package name */
        private long f1417e = Long.MIN_VALUE;

        /* renamed from: f  reason: collision with root package name */
        private long f1418f = 0;

        /* renamed from: g  reason: collision with root package name */
        private int f1419g = 0;

        /* renamed from: h  reason: collision with root package name */
        private int f1420h = 0;

        /* renamed from: i  reason: collision with root package name */
        private long f1421i = -1;

        /* renamed from: j  reason: collision with root package name */
        private float f1422j;

        /* renamed from: k  reason: collision with root package name */
        private int f1423k;

        C0014a() {
        }

        private float a(float f2) {
            return (-4.0f * f2 * f2) + (f2 * 4.0f);
        }

        public void a(int i2) {
            this.f1414b = i2;
        }

        public void b(int i2) {
            this.f1413a = i2;
        }

        public int c() {
            return this.f1420h;
        }

        public int d() {
            float f2 = this.f1415c;
            return (int) (f2 / Math.abs(f2));
        }

        public int e() {
            float f2 = this.f1416d;
            return (int) (f2 / Math.abs(f2));
        }

        public boolean f() {
            return this.f1421i > 0 && AnimationUtils.currentAnimationTimeMillis() > this.f1421i + ((long) this.f1423k);
        }

        public void g() {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.f1423k = a.a((int) (currentAnimationTimeMillis - this.f1417e), 0, this.f1414b);
            this.f1422j = a(currentAnimationTimeMillis);
            this.f1421i = currentAnimationTimeMillis;
        }

        public void h() {
            this.f1417e = AnimationUtils.currentAnimationTimeMillis();
            this.f1421i = -1;
            this.f1418f = this.f1417e;
            this.f1422j = 0.5f;
            this.f1419g = 0;
            this.f1420h = 0;
        }

        private float a(long j2) {
            if (j2 < this.f1417e) {
                return Animation.CurveTimeline.LINEAR;
            }
            long j3 = this.f1421i;
            if (j3 < 0 || j2 < j3) {
                return a.a(((float) (j2 - this.f1417e)) / ((float) this.f1413a), (float) Animation.CurveTimeline.LINEAR, 1.0f) * 0.5f;
            }
            long j4 = j2 - j3;
            float f2 = this.f1422j;
            return (1.0f - f2) + (f2 * a.a(((float) j4) / ((float) this.f1423k), (float) Animation.CurveTimeline.LINEAR, 1.0f));
        }

        public int b() {
            return this.f1419g;
        }

        public void a() {
            if (this.f1418f != 0) {
                long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
                float a2 = a(a(currentAnimationTimeMillis));
                this.f1418f = currentAnimationTimeMillis;
                float f2 = ((float) (currentAnimationTimeMillis - this.f1418f)) * a2;
                this.f1419g = (int) (this.f1415c * f2);
                this.f1420h = (int) (f2 * this.f1416d);
                return;
            }
            throw new RuntimeException("Cannot compute scroll delta before calling start()");
        }

        public void a(float f2, float f3) {
            this.f1415c = f2;
            this.f1416d = f3;
        }
    }

    /* compiled from: AutoScrollHelper */
    private class b implements Runnable {
        b() {
        }

        public void run() {
            a aVar = a.this;
            if (aVar.o) {
                if (aVar.m) {
                    aVar.m = false;
                    aVar.f1402a.h();
                }
                C0014a aVar2 = a.this.f1402a;
                if (aVar2.f() || !a.this.b()) {
                    a.this.o = false;
                    return;
                }
                a aVar3 = a.this;
                if (aVar3.n) {
                    aVar3.n = false;
                    aVar3.a();
                }
                aVar2.a();
                a.this.a(aVar2.b(), aVar2.c());
                u.a(a.this.f1404c, this);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.core.widget.a.a(float, float):androidx.core.widget.a
     arg types: [int, int]
     candidates:
      androidx.core.widget.a.a(int, int):void
      androidx.core.widget.a.a(float, float):androidx.core.widget.a */
    public a(View view) {
        this.f1404c = view;
        float f2 = Resources.getSystem().getDisplayMetrics().density;
        float f3 = (float) ((int) ((1575.0f * f2) + 0.5f));
        b(f3, f3);
        float f4 = (float) ((int) ((f2 * 315.0f) + 0.5f));
        c(f4, f4);
        d(1);
        a(Float.MAX_VALUE, Float.MAX_VALUE);
        d(0.2f, 0.2f);
        e(1.0f, 1.0f);
        c(r);
        f(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
        e(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
    }

    static float a(float f2, float f3, float f4) {
        return f2 > f4 ? f4 : f2 < f3 ? f3 : f2;
    }

    static int a(int i2, int i3, int i4) {
        return i2 > i4 ? i4 : i2 < i3 ? i3 : i2;
    }

    public a a(boolean z) {
        if (this.p && !z) {
            c();
        }
        this.p = z;
        return this;
    }

    public abstract void a(int i2, int i3);

    public abstract boolean a(int i2);

    public a b(float f2, float f3) {
        float[] fArr = this.f1412k;
        fArr[0] = f2 / 1000.0f;
        fArr[1] = f3 / 1000.0f;
        return this;
    }

    public abstract boolean b(int i2);

    public a c(float f2, float f3) {
        float[] fArr = this.f1411j;
        fArr[0] = f2 / 1000.0f;
        fArr[1] = f3 / 1000.0f;
        return this;
    }

    public a d(int i2) {
        this.f1408g = i2;
        return this;
    }

    public a e(float f2, float f3) {
        float[] fArr = this.f1410i;
        fArr[0] = f2 / 1000.0f;
        fArr[1] = f3 / 1000.0f;
        return this;
    }

    public a f(int i2) {
        this.f1402a.b(i2);
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0013, code lost:
        if (r0 != 3) goto L_0x0058;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r6, android.view.MotionEvent r7) {
        /*
            r5 = this;
            boolean r0 = r5.p
            r1 = 0
            if (r0 != 0) goto L_0x0006
            return r1
        L_0x0006:
            int r0 = r7.getActionMasked()
            r2 = 1
            if (r0 == 0) goto L_0x001a
            if (r0 == r2) goto L_0x0016
            r3 = 2
            if (r0 == r3) goto L_0x001e
            r6 = 3
            if (r0 == r6) goto L_0x0016
            goto L_0x0058
        L_0x0016:
            r5.c()
            goto L_0x0058
        L_0x001a:
            r5.n = r2
            r5.l = r1
        L_0x001e:
            float r0 = r7.getX()
            int r3 = r6.getWidth()
            float r3 = (float) r3
            android.view.View r4 = r5.f1404c
            int r4 = r4.getWidth()
            float r4 = (float) r4
            float r0 = r5.a(r1, r0, r3, r4)
            float r7 = r7.getY()
            int r6 = r6.getHeight()
            float r6 = (float) r6
            android.view.View r3 = r5.f1404c
            int r3 = r3.getHeight()
            float r3 = (float) r3
            float r6 = r5.a(r2, r7, r6, r3)
            androidx.core.widget.a$a r7 = r5.f1402a
            r7.a(r0, r6)
            boolean r6 = r5.o
            if (r6 != 0) goto L_0x0058
            boolean r6 = r5.b()
            if (r6 == 0) goto L_0x0058
            r5.d()
        L_0x0058:
            boolean r6 = r5.q
            if (r6 == 0) goto L_0x0061
            boolean r6 = r5.o
            if (r6 == 0) goto L_0x0061
            r1 = 1
        L_0x0061:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.core.widget.a.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }

    private float f(float f2, float f3) {
        if (f3 == Animation.CurveTimeline.LINEAR) {
            return Animation.CurveTimeline.LINEAR;
        }
        int i2 = this.f1408g;
        if (i2 == 0 || i2 == 1) {
            if (f2 < f3) {
                if (f2 >= Animation.CurveTimeline.LINEAR) {
                    return 1.0f - (f2 / f3);
                }
                if (!this.o || this.f1408g != 1) {
                    return Animation.CurveTimeline.LINEAR;
                }
                return 1.0f;
            }
        } else if (i2 == 2 && f2 < Animation.CurveTimeline.LINEAR) {
            return f2 / (-f3);
        }
        return Animation.CurveTimeline.LINEAR;
    }

    public a d(float f2, float f3) {
        float[] fArr = this.f1406e;
        fArr[0] = f2;
        fArr[1] = f3;
        return this;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        C0014a aVar = this.f1402a;
        int e2 = aVar.e();
        int d2 = aVar.d();
        return (e2 != 0 && b(e2)) || (d2 != 0 && a(d2));
    }

    public a c(int i2) {
        this.f1409h = i2;
        return this;
    }

    public a e(int i2) {
        this.f1402a.a(i2);
        return this;
    }

    private void c() {
        if (this.m) {
            this.o = false;
        } else {
            this.f1402a.g();
        }
    }

    private void d() {
        int i2;
        if (this.f1405d == null) {
            this.f1405d = new b();
        }
        this.o = true;
        this.m = true;
        if (this.l || (i2 = this.f1409h) <= 0) {
            this.f1405d.run();
        } else {
            u.a(this.f1404c, this.f1405d, (long) i2);
        }
        this.l = true;
    }

    public a a(float f2, float f3) {
        float[] fArr = this.f1407f;
        fArr[0] = f2;
        fArr[1] = f3;
        return this;
    }

    private float a(int i2, float f2, float f3, float f4) {
        float a2 = a(this.f1406e[i2], f3, this.f1407f[i2], f2);
        if (a2 == Animation.CurveTimeline.LINEAR) {
            return Animation.CurveTimeline.LINEAR;
        }
        float f5 = this.f1410i[i2];
        float f6 = this.f1411j[i2];
        float f7 = this.f1412k[i2];
        float f8 = f5 * f4;
        if (a2 > Animation.CurveTimeline.LINEAR) {
            return a(a2 * f8, f6, f7);
        }
        return -a((-a2) * f8, f6, f7);
    }

    private float a(float f2, float f3, float f4, float f5) {
        float f6;
        float a2 = a(f2 * f3, (float) Animation.CurveTimeline.LINEAR, f4);
        float f7 = f(f3 - f5, a2) - f(f5, a2);
        if (f7 < Animation.CurveTimeline.LINEAR) {
            f6 = -this.f1403b.getInterpolation(-f7);
        } else if (f7 <= Animation.CurveTimeline.LINEAR) {
            return Animation.CurveTimeline.LINEAR;
        } else {
            f6 = this.f1403b.getInterpolation(f7);
        }
        return a(f6, -1.0f, 1.0f);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0);
        this.f1404c.onTouchEvent(obtain);
        obtain.recycle();
    }
}
