package androidx.core.widget;

import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.PopupWindow;
import b.e.m.c;
import b.e.m.u;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/* compiled from: PopupWindowCompat */
public final class h {

    /* renamed from: a  reason: collision with root package name */
    private static Method f1427a;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f1428b;

    /* renamed from: c  reason: collision with root package name */
    private static Field f1429c;

    /* renamed from: d  reason: collision with root package name */
    private static boolean f1430d;

    public static void a(PopupWindow popupWindow, View view, int i2, int i3, int i4) {
        if (Build.VERSION.SDK_INT >= 19) {
            popupWindow.showAsDropDown(view, i2, i3, i4);
            return;
        }
        if ((c.a(i4, u.k(view)) & 7) == 5) {
            i2 -= popupWindow.getWidth() - view.getWidth();
        }
        popupWindow.showAsDropDown(view, i2, i3);
    }

    public static void a(PopupWindow popupWindow, boolean z) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 23) {
            popupWindow.setOverlapAnchor(z);
        } else if (i2 >= 21) {
            if (!f1430d) {
                try {
                    f1429c = PopupWindow.class.getDeclaredField("mOverlapAnchor");
                    f1429c.setAccessible(true);
                } catch (NoSuchFieldException e2) {
                    Log.i("PopupWindowCompatApi21", "Could not fetch mOverlapAnchor field from PopupWindow", e2);
                }
                f1430d = true;
            }
            Field field = f1429c;
            if (field != null) {
                try {
                    field.set(popupWindow, Boolean.valueOf(z));
                } catch (IllegalAccessException e3) {
                    Log.i("PopupWindowCompatApi21", "Could not set overlap anchor field in PopupWindow", e3);
                }
            }
        }
    }

    public static void a(PopupWindow popupWindow, int i2) {
        if (Build.VERSION.SDK_INT >= 23) {
            popupWindow.setWindowLayoutType(i2);
            return;
        }
        if (!f1428b) {
            try {
                f1427a = PopupWindow.class.getDeclaredMethod("setWindowLayoutType", Integer.TYPE);
                f1427a.setAccessible(true);
            } catch (Exception unused) {
            }
            f1428b = true;
        }
        Method method = f1427a;
        if (method != null) {
            try {
                method.invoke(popupWindow, Integer.valueOf(i2));
            } catch (Exception unused2) {
            }
        }
    }
}
