package androidx.lifecycle;

import android.os.Handler;
import androidx.lifecycle.e;

/* compiled from: ServiceLifecycleDispatcher */
public class r {

    /* renamed from: a  reason: collision with root package name */
    private final i f1669a;

    /* renamed from: b  reason: collision with root package name */
    private final Handler f1670b = new Handler();

    /* renamed from: c  reason: collision with root package name */
    private a f1671c;

    /* compiled from: ServiceLifecycleDispatcher */
    static class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final i f1672a;

        /* renamed from: b  reason: collision with root package name */
        final e.a f1673b;

        /* renamed from: c  reason: collision with root package name */
        private boolean f1674c = false;

        a(i iVar, e.a aVar) {
            this.f1672a = iVar;
            this.f1673b = aVar;
        }

        public void run() {
            if (!this.f1674c) {
                this.f1672a.a(this.f1673b);
                this.f1674c = true;
            }
        }
    }

    public r(h hVar) {
        this.f1669a = new i(hVar);
    }

    private void a(e.a aVar) {
        a aVar2 = this.f1671c;
        if (aVar2 != null) {
            aVar2.run();
        }
        this.f1671c = new a(this.f1669a, aVar);
        this.f1670b.postAtFrontOfQueue(this.f1671c);
    }

    public void b() {
        a(e.a.ON_START);
    }

    public void c() {
        a(e.a.ON_CREATE);
    }

    public void d() {
        a(e.a.ON_STOP);
        a(e.a.ON_DESTROY);
    }

    public void e() {
        a(e.a.ON_START);
    }

    public e a() {
        return this.f1669a;
    }
}
