package androidx.lifecycle;

import androidx.lifecycle.e;

class CompositeGeneratedAdaptersObserver implements f {

    /* renamed from: a  reason: collision with root package name */
    private final c[] f1616a;

    CompositeGeneratedAdaptersObserver(c[] cVarArr) {
        this.f1616a = cVarArr;
    }

    public void a(h hVar, e.a aVar) {
        m mVar = new m();
        for (c a2 : this.f1616a) {
            a2.a(hVar, aVar, false, mVar);
        }
        for (c a3 : this.f1616a) {
            a3.a(hVar, aVar, true, mVar);
        }
    }
}
