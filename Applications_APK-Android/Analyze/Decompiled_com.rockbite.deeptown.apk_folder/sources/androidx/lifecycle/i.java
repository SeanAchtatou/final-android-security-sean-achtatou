package androidx.lifecycle;

import androidx.lifecycle.e;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/* compiled from: LifecycleRegistry */
public class i extends e {

    /* renamed from: a  reason: collision with root package name */
    private b.b.a.b.a<g, b> f1654a = new b.b.a.b.a<>();

    /* renamed from: b  reason: collision with root package name */
    private e.b f1655b;

    /* renamed from: c  reason: collision with root package name */
    private final WeakReference<h> f1656c;

    /* renamed from: d  reason: collision with root package name */
    private int f1657d = 0;

    /* renamed from: e  reason: collision with root package name */
    private boolean f1658e = false;

    /* renamed from: f  reason: collision with root package name */
    private boolean f1659f = false;

    /* renamed from: g  reason: collision with root package name */
    private ArrayList<e.b> f1660g = new ArrayList<>();

    /* compiled from: LifecycleRegistry */
    static /* synthetic */ class a {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f1661a = new int[e.a.values().length];

        /* renamed from: b  reason: collision with root package name */
        static final /* synthetic */ int[] f1662b = new int[e.b.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|(2:17|18)|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(27:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|(2:17|18)|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|(2:17|18)|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(30:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0053 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x005d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0067 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x007b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0086 */
        static {
            /*
                androidx.lifecycle.e$b[] r0 = androidx.lifecycle.e.b.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                androidx.lifecycle.i.a.f1662b = r0
                r0 = 1
                int[] r1 = androidx.lifecycle.i.a.f1662b     // Catch:{ NoSuchFieldError -> 0x0014 }
                androidx.lifecycle.e$b r2 = androidx.lifecycle.e.b.INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = androidx.lifecycle.i.a.f1662b     // Catch:{ NoSuchFieldError -> 0x001f }
                androidx.lifecycle.e$b r3 = androidx.lifecycle.e.b.CREATED     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = androidx.lifecycle.i.a.f1662b     // Catch:{ NoSuchFieldError -> 0x002a }
                androidx.lifecycle.e$b r4 = androidx.lifecycle.e.b.STARTED     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                r3 = 4
                int[] r4 = androidx.lifecycle.i.a.f1662b     // Catch:{ NoSuchFieldError -> 0x0035 }
                androidx.lifecycle.e$b r5 = androidx.lifecycle.e.b.RESUMED     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                r4 = 5
                int[] r5 = androidx.lifecycle.i.a.f1662b     // Catch:{ NoSuchFieldError -> 0x0040 }
                androidx.lifecycle.e$b r6 = androidx.lifecycle.e.b.DESTROYED     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r5[r6] = r4     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                androidx.lifecycle.e$a[] r5 = androidx.lifecycle.e.a.values()
                int r5 = r5.length
                int[] r5 = new int[r5]
                androidx.lifecycle.i.a.f1661a = r5
                int[] r5 = androidx.lifecycle.i.a.f1661a     // Catch:{ NoSuchFieldError -> 0x0053 }
                androidx.lifecycle.e$a r6 = androidx.lifecycle.e.a.ON_CREATE     // Catch:{ NoSuchFieldError -> 0x0053 }
                int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x0053 }
                r5[r6] = r0     // Catch:{ NoSuchFieldError -> 0x0053 }
            L_0x0053:
                int[] r0 = androidx.lifecycle.i.a.f1661a     // Catch:{ NoSuchFieldError -> 0x005d }
                androidx.lifecycle.e$a r5 = androidx.lifecycle.e.a.ON_STOP     // Catch:{ NoSuchFieldError -> 0x005d }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x005d }
                r0[r5] = r1     // Catch:{ NoSuchFieldError -> 0x005d }
            L_0x005d:
                int[] r0 = androidx.lifecycle.i.a.f1661a     // Catch:{ NoSuchFieldError -> 0x0067 }
                androidx.lifecycle.e$a r1 = androidx.lifecycle.e.a.ON_START     // Catch:{ NoSuchFieldError -> 0x0067 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0067 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0067 }
            L_0x0067:
                int[] r0 = androidx.lifecycle.i.a.f1661a     // Catch:{ NoSuchFieldError -> 0x0071 }
                androidx.lifecycle.e$a r1 = androidx.lifecycle.e.a.ON_PAUSE     // Catch:{ NoSuchFieldError -> 0x0071 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0071 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0071 }
            L_0x0071:
                int[] r0 = androidx.lifecycle.i.a.f1661a     // Catch:{ NoSuchFieldError -> 0x007b }
                androidx.lifecycle.e$a r1 = androidx.lifecycle.e.a.ON_RESUME     // Catch:{ NoSuchFieldError -> 0x007b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007b }
                r0[r1] = r4     // Catch:{ NoSuchFieldError -> 0x007b }
            L_0x007b:
                int[] r0 = androidx.lifecycle.i.a.f1661a     // Catch:{ NoSuchFieldError -> 0x0086 }
                androidx.lifecycle.e$a r1 = androidx.lifecycle.e.a.ON_DESTROY     // Catch:{ NoSuchFieldError -> 0x0086 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0086 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0086 }
            L_0x0086:
                int[] r0 = androidx.lifecycle.i.a.f1661a     // Catch:{ NoSuchFieldError -> 0x0091 }
                androidx.lifecycle.e$a r1 = androidx.lifecycle.e.a.ON_ANY     // Catch:{ NoSuchFieldError -> 0x0091 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0091 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0091 }
            L_0x0091:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.lifecycle.i.a.<clinit>():void");
        }
    }

    /* compiled from: LifecycleRegistry */
    static class b {

        /* renamed from: a  reason: collision with root package name */
        e.b f1663a;

        /* renamed from: b  reason: collision with root package name */
        f f1664b;

        b(g gVar, e.b bVar) {
            this.f1664b = l.a(gVar);
            this.f1663a = bVar;
        }

        /* access modifiers changed from: package-private */
        public void a(h hVar, e.a aVar) {
            e.b b2 = i.b(aVar);
            this.f1663a = i.a(this.f1663a, b2);
            this.f1664b.a(hVar, aVar);
            this.f1663a = b2;
        }
    }

    public i(h hVar) {
        this.f1656c = new WeakReference<>(hVar);
        this.f1655b = e.b.INITIALIZED;
    }

    private e.b c(g gVar) {
        Map.Entry<g, b> b2 = this.f1654a.b(gVar);
        e.b bVar = null;
        e.b bVar2 = b2 != null ? b2.getValue().f1663a : null;
        if (!this.f1660g.isEmpty()) {
            ArrayList<e.b> arrayList = this.f1660g;
            bVar = arrayList.get(arrayList.size() - 1);
        }
        return a(a(this.f1655b, bVar2), bVar);
    }

    private void d(e.b bVar) {
        if (this.f1655b != bVar) {
            this.f1655b = bVar;
            if (this.f1658e || this.f1657d != 0) {
                this.f1659f = true;
                return;
            }
            this.f1658e = true;
            d();
            this.f1658e = false;
        }
    }

    private void e(e.b bVar) {
        this.f1660g.add(bVar);
    }

    private static e.a f(e.b bVar) {
        int i2 = a.f1662b[bVar.ordinal()];
        if (i2 != 1) {
            if (i2 == 2) {
                return e.a.ON_START;
            }
            if (i2 == 3) {
                return e.a.ON_RESUME;
            }
            if (i2 == 4) {
                throw new IllegalArgumentException();
            } else if (i2 != 5) {
                throw new IllegalArgumentException("Unexpected state value " + bVar);
            }
        }
        return e.a.ON_CREATE;
    }

    @Deprecated
    public void a(e.b bVar) {
        b(bVar);
    }

    public void b(e.b bVar) {
        d(bVar);
    }

    private boolean b() {
        if (this.f1654a.size() == 0) {
            return true;
        }
        e.b bVar = this.f1654a.a().getValue().f1663a;
        e.b bVar2 = this.f1654a.c().getValue().f1663a;
        if (bVar == bVar2 && this.f1655b == bVar2) {
            return true;
        }
        return false;
    }

    public void a(e.a aVar) {
        d(b(aVar));
    }

    public void a(g gVar) {
        h hVar;
        e.b bVar = this.f1655b;
        e.b bVar2 = e.b.DESTROYED;
        if (bVar != bVar2) {
            bVar2 = e.b.INITIALIZED;
        }
        b bVar3 = new b(gVar, bVar2);
        if (this.f1654a.b(gVar, bVar3) == null && (hVar = this.f1656c.get()) != null) {
            boolean z = this.f1657d != 0 || this.f1658e;
            e.b c2 = c(gVar);
            this.f1657d++;
            while (bVar3.f1663a.compareTo((Enum) c2) < 0 && this.f1654a.contains(gVar)) {
                e(bVar3.f1663a);
                bVar3.a(hVar, f(bVar3.f1663a));
                c();
                c2 = c(gVar);
            }
            if (!z) {
                d();
            }
            this.f1657d--;
        }
    }

    private void c() {
        ArrayList<e.b> arrayList = this.f1660g;
        arrayList.remove(arrayList.size() - 1);
    }

    private static e.a c(e.b bVar) {
        int i2 = a.f1662b[bVar.ordinal()];
        if (i2 == 1) {
            throw new IllegalArgumentException();
        } else if (i2 == 2) {
            return e.a.ON_DESTROY;
        } else {
            if (i2 == 3) {
                return e.a.ON_STOP;
            }
            if (i2 == 4) {
                return e.a.ON_PAUSE;
            }
            if (i2 != 5) {
                throw new IllegalArgumentException("Unexpected state value " + bVar);
            }
            throw new IllegalArgumentException();
        }
    }

    public void b(g gVar) {
        this.f1654a.remove(gVar);
    }

    static e.b b(e.a aVar) {
        switch (a.f1661a[aVar.ordinal()]) {
            case 1:
            case 2:
                return e.b.CREATED;
            case 3:
            case 4:
                return e.b.STARTED;
            case 5:
                return e.b.RESUMED;
            case 6:
                return e.b.DESTROYED;
            default:
                throw new IllegalArgumentException("Unexpected event value " + aVar);
        }
    }

    private void d() {
        h hVar = this.f1656c.get();
        if (hVar != null) {
            while (!b()) {
                this.f1659f = false;
                if (this.f1655b.compareTo((Enum) this.f1654a.a().getValue().f1663a) < 0) {
                    a(hVar);
                }
                Map.Entry<g, b> c2 = this.f1654a.c();
                if (!this.f1659f && c2 != null && this.f1655b.compareTo((Enum) c2.getValue().f1663a) > 0) {
                    b(hVar);
                }
            }
            this.f1659f = false;
            return;
        }
        throw new IllegalStateException("LifecycleOwner of this LifecycleRegistry is alreadygarbage collected. It is too late to change lifecycle state.");
    }

    private void b(h hVar) {
        b.b.a.b.b<K, V>.d b2 = this.f1654a.b();
        while (b2.hasNext() && !this.f1659f) {
            Map.Entry entry = (Map.Entry) b2.next();
            b bVar = (b) entry.getValue();
            while (bVar.f1663a.compareTo((Enum) this.f1655b) < 0 && !this.f1659f && this.f1654a.contains(entry.getKey())) {
                e(bVar.f1663a);
                bVar.a(hVar, f(bVar.f1663a));
                c();
            }
        }
    }

    public e.b a() {
        return this.f1655b;
    }

    private void a(h hVar) {
        Iterator<Map.Entry<g, b>> descendingIterator = this.f1654a.descendingIterator();
        while (descendingIterator.hasNext() && !this.f1659f) {
            Map.Entry next = descendingIterator.next();
            b bVar = (b) next.getValue();
            while (bVar.f1663a.compareTo((Enum) this.f1655b) > 0 && !this.f1659f && this.f1654a.contains(next.getKey())) {
                e.a c2 = c(bVar.f1663a);
                e(b(c2));
                bVar.a(hVar, c2);
                c();
            }
        }
    }

    static e.b a(e.b bVar, e.b bVar2) {
        return (bVar2 == null || bVar2.compareTo(bVar) >= 0) ? bVar : bVar2;
    }
}
