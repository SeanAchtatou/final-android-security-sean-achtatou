package androidx.lifecycle;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/* compiled from: LifecycleService */
public class k extends Service implements h {

    /* renamed from: a  reason: collision with root package name */
    private final r f1665a = new r(this);

    public e getLifecycle() {
        return this.f1665a.a();
    }

    public IBinder onBind(Intent intent) {
        this.f1665a.b();
        return null;
    }

    public void onCreate() {
        this.f1665a.c();
        super.onCreate();
    }

    public void onDestroy() {
        this.f1665a.d();
        super.onDestroy();
    }

    public void onStart(Intent intent, int i2) {
        this.f1665a.e();
        super.onStart(intent, i2);
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        return super.onStartCommand(intent, i2, i3);
    }
}
