package androidx.lifecycle;

import androidx.lifecycle.e;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ClassesInfoCache */
class a {

    /* renamed from: c  reason: collision with root package name */
    static a f1641c = new a();

    /* renamed from: a  reason: collision with root package name */
    private final Map<Class, C0016a> f1642a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    private final Map<Class, Boolean> f1643b = new HashMap();

    /* compiled from: ClassesInfoCache */
    static class b {

        /* renamed from: a  reason: collision with root package name */
        final int f1646a;

        /* renamed from: b  reason: collision with root package name */
        final Method f1647b;

        b(int i2, Method method) {
            this.f1646a = i2;
            this.f1647b = method;
            this.f1647b.setAccessible(true);
        }

        /* access modifiers changed from: package-private */
        public void a(h hVar, e.a aVar, Object obj) {
            try {
                int i2 = this.f1646a;
                if (i2 == 0) {
                    this.f1647b.invoke(obj, new Object[0]);
                } else if (i2 == 1) {
                    this.f1647b.invoke(obj, hVar);
                } else if (i2 == 2) {
                    this.f1647b.invoke(obj, hVar, aVar);
                }
            } catch (InvocationTargetException e2) {
                throw new RuntimeException("Failed to call observer method", e2.getCause());
            } catch (IllegalAccessException e3) {
                throw new RuntimeException(e3);
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.f1646a != bVar.f1646a || !this.f1647b.getName().equals(bVar.f1647b.getName())) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (this.f1646a * 31) + this.f1647b.getName().hashCode();
        }
    }

    a() {
    }

    private Method[] c(Class cls) {
        try {
            return cls.getDeclaredMethods();
        } catch (NoClassDefFoundError e2) {
            throw new IllegalArgumentException("The observer class has some methods that use newer APIs which are not available in the current OS version. Lifecycles cannot access even other methods so you should make sure that your observer classes only access framework classes that are available in your min API level OR use lifecycle:compiler annotation processor.", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public C0016a a(Class cls) {
        C0016a aVar = this.f1642a.get(cls);
        if (aVar != null) {
            return aVar;
        }
        return a(cls, null);
    }

    /* access modifiers changed from: package-private */
    public boolean b(Class cls) {
        Boolean bool = this.f1643b.get(cls);
        if (bool != null) {
            return bool.booleanValue();
        }
        Method[] c2 = c(cls);
        for (Method annotation : c2) {
            if (((p) annotation.getAnnotation(p.class)) != null) {
                a(cls, c2);
                return true;
            }
        }
        this.f1643b.put(cls, false);
        return false;
    }

    /* renamed from: androidx.lifecycle.a$a  reason: collision with other inner class name */
    /* compiled from: ClassesInfoCache */
    static class C0016a {

        /* renamed from: a  reason: collision with root package name */
        final Map<e.a, List<b>> f1644a = new HashMap();

        /* renamed from: b  reason: collision with root package name */
        final Map<b, e.a> f1645b;

        C0016a(Map<b, e.a> map) {
            this.f1645b = map;
            for (Map.Entry next : map.entrySet()) {
                e.a aVar = (e.a) next.getValue();
                Object obj = this.f1644a.get(aVar);
                if (obj == null) {
                    obj = new ArrayList();
                    this.f1644a.put(aVar, obj);
                }
                obj.add(next.getKey());
            }
        }

        /* access modifiers changed from: package-private */
        public void a(h hVar, e.a aVar, Object obj) {
            a(this.f1644a.get(aVar), hVar, aVar, obj);
            a(this.f1644a.get(e.a.ON_ANY), hVar, aVar, obj);
        }

        private static void a(List<b> list, h hVar, e.a aVar, Object obj) {
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    list.get(size).a(hVar, aVar, obj);
                }
            }
        }
    }

    private void a(Map<b, e.a> map, b bVar, e.a aVar, Class cls) {
        e.a aVar2 = map.get(bVar);
        if (aVar2 != null && aVar != aVar2) {
            Method method = bVar.f1647b;
            throw new IllegalArgumentException("Method " + method.getName() + " in " + cls.getName() + " already declared with different @OnLifecycleEvent value: previous value " + aVar2 + ", new value " + aVar);
        } else if (aVar2 == null) {
            map.put(bVar, aVar);
        }
    }

    private C0016a a(Class cls, Method[] methodArr) {
        int i2;
        C0016a a2;
        Class superclass = cls.getSuperclass();
        HashMap hashMap = new HashMap();
        if (!(superclass == null || (a2 = a(superclass)) == null)) {
            hashMap.putAll(a2.f1645b);
        }
        for (Class<?> a3 : cls.getInterfaces()) {
            for (Map.Entry next : a(a3).f1645b.entrySet()) {
                a(hashMap, (b) next.getKey(), (e.a) next.getValue(), cls);
            }
        }
        if (methodArr == null) {
            methodArr = c(cls);
        }
        boolean z = false;
        for (Method method : methodArr) {
            p pVar = (p) method.getAnnotation(p.class);
            if (pVar != null) {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length <= 0) {
                    i2 = 0;
                } else if (parameterTypes[0].isAssignableFrom(h.class)) {
                    i2 = 1;
                } else {
                    throw new IllegalArgumentException("invalid parameter type. Must be one and instanceof LifecycleOwner");
                }
                e.a value = pVar.value();
                if (parameterTypes.length > 1) {
                    if (!parameterTypes[1].isAssignableFrom(e.a.class)) {
                        throw new IllegalArgumentException("invalid parameter type. second arg must be an event");
                    } else if (value == e.a.ON_ANY) {
                        i2 = 2;
                    } else {
                        throw new IllegalArgumentException("Second arg is supported only for ON_ANY value");
                    }
                }
                if (parameterTypes.length <= 2) {
                    a(hashMap, new b(i2, method), value, cls);
                    z = true;
                } else {
                    throw new IllegalArgumentException("cannot have more than 2 params");
                }
            }
        }
        C0016a aVar = new C0016a(hashMap);
        this.f1642a.put(cls, aVar);
        this.f1643b.put(cls, Boolean.valueOf(z));
        return aVar;
    }
}
