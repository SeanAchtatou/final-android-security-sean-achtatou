package androidx.lifecycle;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ViewModel */
public abstract class s {

    /* renamed from: a  reason: collision with root package name */
    private final Map<String, Object> f1675a = new HashMap();

    /* access modifiers changed from: package-private */
    public final void a() {
        Map<String, Object> map = this.f1675a;
        if (map != null) {
            synchronized (map) {
                for (Object a2 : this.f1675a.values()) {
                    a(a2);
                }
            }
        }
        b();
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    private static void a(Object obj) {
        if (obj instanceof Closeable) {
            try {
                ((Closeable) obj).close();
            } catch (IOException e2) {
                throw new RuntimeException(e2);
            }
        }
    }
}
