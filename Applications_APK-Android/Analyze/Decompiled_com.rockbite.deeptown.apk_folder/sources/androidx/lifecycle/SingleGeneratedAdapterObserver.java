package androidx.lifecycle;

import androidx.lifecycle.e;

class SingleGeneratedAdapterObserver implements f {

    /* renamed from: a  reason: collision with root package name */
    private final c f1640a;

    SingleGeneratedAdapterObserver(c cVar) {
        this.f1640a = cVar;
    }

    public void a(h hVar, e.a aVar) {
        this.f1640a.a(hVar, aVar, false, null);
        this.f1640a.a(hVar, aVar, true, null);
    }
}
