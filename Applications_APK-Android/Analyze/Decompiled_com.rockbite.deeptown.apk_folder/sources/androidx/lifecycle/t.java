package androidx.lifecycle;

/* compiled from: ViewModelProvider */
public class t {

    /* renamed from: a  reason: collision with root package name */
    private final a f1676a;

    /* renamed from: b  reason: collision with root package name */
    private final u f1677b;

    /* compiled from: ViewModelProvider */
    public interface a {
        <T extends s> T a(Class<T> cls);
    }

    /* compiled from: ViewModelProvider */
    static abstract class b implements a {
        b() {
        }

        public <T extends s> T a(Class<T> cls) {
            throw new UnsupportedOperationException("create(String, Class<?>) must be called on implementaions of KeyedFactory");
        }

        public abstract <T extends s> T a(String str, Class<T> cls);
    }

    public t(u uVar, a aVar) {
        this.f1676a = aVar;
        this.f1677b = uVar;
    }

    public <T extends s> T a(Class<T> cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return a("androidx.lifecycle.ViewModelProvider.DefaultKey:" + canonicalName, cls);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }

    public <T extends s> T a(String str, Class<T> cls) {
        T t;
        T a2 = this.f1677b.a(str);
        if (cls.isInstance(a2)) {
            return a2;
        }
        a aVar = this.f1676a;
        if (aVar instanceof b) {
            t = ((b) aVar).a(str, cls);
        } else {
            t = aVar.a(cls);
        }
        this.f1677b.a(str, t);
        return t;
    }
}
