package androidx.lifecycle;

import androidx.lifecycle.e;
import java.util.Map;

public abstract class LiveData<T> {

    /* renamed from: j  reason: collision with root package name */
    static final Object f1621j = new Object();

    /* renamed from: a  reason: collision with root package name */
    final Object f1622a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private b.b.a.b.b<o<? super T>, LiveData<T>.b> f1623b = new b.b.a.b.b<>();

    /* renamed from: c  reason: collision with root package name */
    int f1624c = 0;

    /* renamed from: d  reason: collision with root package name */
    private volatile Object f1625d = f1621j;

    /* renamed from: e  reason: collision with root package name */
    volatile Object f1626e = f1621j;

    /* renamed from: f  reason: collision with root package name */
    private int f1627f = -1;

    /* renamed from: g  reason: collision with root package name */
    private boolean f1628g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f1629h;

    /* renamed from: i  reason: collision with root package name */
    private final Runnable f1630i = new a();

    class a implements Runnable {
        a() {
        }

        public void run() {
            Object obj;
            synchronized (LiveData.this.f1622a) {
                obj = LiveData.this.f1626e;
                LiveData.this.f1626e = LiveData.f1621j;
            }
            LiveData.this.b(obj);
        }
    }

    private abstract class b {

        /* renamed from: a  reason: collision with root package name */
        final o<? super T> f1634a;

        /* renamed from: b  reason: collision with root package name */
        boolean f1635b;

        /* renamed from: c  reason: collision with root package name */
        int f1636c = -1;

        b(o<? super T> oVar) {
            this.f1634a = oVar;
        }

        /* access modifiers changed from: package-private */
        public void a() {
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z) {
            if (z != this.f1635b) {
                this.f1635b = z;
                int i2 = 1;
                boolean z2 = LiveData.this.f1624c == 0;
                LiveData liveData = LiveData.this;
                int i3 = liveData.f1624c;
                if (!this.f1635b) {
                    i2 = -1;
                }
                liveData.f1624c = i3 + i2;
                if (z2 && this.f1635b) {
                    LiveData.this.c();
                }
                LiveData liveData2 = LiveData.this;
                if (liveData2.f1624c == 0 && !this.f1635b) {
                    liveData2.d();
                }
                if (this.f1635b) {
                    LiveData.this.a((LiveData<T>.b) this);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(h hVar) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public abstract boolean b();
    }

    private void b(LiveData<T>.b bVar) {
        if (bVar.f1635b) {
            if (!bVar.b()) {
                bVar.a(false);
                return;
            }
            int i2 = bVar.f1636c;
            int i3 = this.f1627f;
            if (i2 < i3) {
                bVar.f1636c = i3;
                bVar.f1634a.a(this.f1625d);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(LiveData<T>.b bVar) {
        if (this.f1628g) {
            this.f1629h = true;
            return;
        }
        this.f1628g = true;
        do {
            this.f1629h = false;
            if (bVar == null) {
                b.b.a.b.b<K, V>.d b2 = this.f1623b.b();
                while (b2.hasNext()) {
                    b((LiveData<T>.b) ((b) ((Map.Entry) b2.next()).getValue()));
                    if (this.f1629h) {
                        break;
                    }
                }
            } else {
                b(bVar);
                bVar = null;
            }
        } while (this.f1629h);
        this.f1628g = false;
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* access modifiers changed from: protected */
    public void d() {
    }

    class LifecycleBoundObserver extends LiveData<T>.b implements f {

        /* renamed from: e  reason: collision with root package name */
        final h f1631e;

        LifecycleBoundObserver(h hVar, o<? super T> oVar) {
            super(oVar);
            this.f1631e = hVar;
        }

        public void a(h hVar, e.a aVar) {
            if (this.f1631e.getLifecycle().a() == e.b.DESTROYED) {
                LiveData.this.a((o) this.f1634a);
            } else {
                a(b());
            }
        }

        /* access modifiers changed from: package-private */
        public boolean b() {
            return this.f1631e.getLifecycle().a().a(e.b.STARTED);
        }

        /* access modifiers changed from: package-private */
        public boolean a(h hVar) {
            return this.f1631e == hVar;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f1631e.getLifecycle().b(this);
        }
    }

    /* access modifiers changed from: protected */
    public void b(T t) {
        a("setValue");
        this.f1627f++;
        this.f1625d = t;
        a((LiveData<T>.b) null);
    }

    public boolean b() {
        return this.f1624c > 0;
    }

    public void a(h hVar, o<? super T> oVar) {
        a("observe");
        if (hVar.getLifecycle().a() != e.b.DESTROYED) {
            LifecycleBoundObserver lifecycleBoundObserver = new LifecycleBoundObserver(hVar, oVar);
            b b2 = this.f1623b.b(oVar, lifecycleBoundObserver);
            if (b2 != null && !b2.a(hVar)) {
                throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
            } else if (b2 == null) {
                hVar.getLifecycle().a(lifecycleBoundObserver);
            }
        }
    }

    public void a(o<? super T> oVar) {
        a("removeObserver");
        b remove = this.f1623b.remove(oVar);
        if (remove != null) {
            remove.a();
            remove.a(false);
        }
    }

    /* access modifiers changed from: protected */
    public void a(T t) {
        boolean z;
        synchronized (this.f1622a) {
            z = this.f1626e == f1621j;
            this.f1626e = t;
        }
        if (z) {
            b.b.a.a.a.c().b(this.f1630i);
        }
    }

    public T a() {
        T t = this.f1625d;
        if (t != f1621j) {
            return t;
        }
        return null;
    }

    static void a(String str) {
        if (!b.b.a.a.a.c().a()) {
            throw new IllegalStateException("Cannot invoke " + str + " on a background thread");
        }
    }
}
