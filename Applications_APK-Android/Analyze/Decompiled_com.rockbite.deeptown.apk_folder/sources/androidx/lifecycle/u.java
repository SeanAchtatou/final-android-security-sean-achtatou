package androidx.lifecycle;

import java.util.HashMap;

/* compiled from: ViewModelStore */
public class u {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap<String, s> f1678a = new HashMap<>();

    /* access modifiers changed from: package-private */
    public final void a(String str, s sVar) {
        s put = this.f1678a.put(str, sVar);
        if (put != null) {
            put.b();
        }
    }

    /* access modifiers changed from: package-private */
    public final s a(String str) {
        return this.f1678a.get(str);
    }

    public final void a() {
        for (s a2 : this.f1678a.values()) {
            a2.a();
        }
        this.f1678a.clear();
    }
}
