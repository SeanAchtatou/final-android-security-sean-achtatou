package androidx.lifecycle;

import androidx.lifecycle.e;

class FullLifecycleObserverAdapter implements f {

    /* renamed from: a  reason: collision with root package name */
    private final b f1617a;

    /* renamed from: b  reason: collision with root package name */
    private final f f1618b;

    static /* synthetic */ class a {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f1619a = new int[e.a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(14:0|1|2|3|4|5|6|7|8|9|10|11|12|(3:13|14|16)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|16) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                androidx.lifecycle.e$a[] r0 = androidx.lifecycle.e.a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                androidx.lifecycle.FullLifecycleObserverAdapter.a.f1619a = r0
                int[] r0 = androidx.lifecycle.FullLifecycleObserverAdapter.a.f1619a     // Catch:{ NoSuchFieldError -> 0x0014 }
                androidx.lifecycle.e$a r1 = androidx.lifecycle.e.a.ON_CREATE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = androidx.lifecycle.FullLifecycleObserverAdapter.a.f1619a     // Catch:{ NoSuchFieldError -> 0x001f }
                androidx.lifecycle.e$a r1 = androidx.lifecycle.e.a.ON_START     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = androidx.lifecycle.FullLifecycleObserverAdapter.a.f1619a     // Catch:{ NoSuchFieldError -> 0x002a }
                androidx.lifecycle.e$a r1 = androidx.lifecycle.e.a.ON_RESUME     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = androidx.lifecycle.FullLifecycleObserverAdapter.a.f1619a     // Catch:{ NoSuchFieldError -> 0x0035 }
                androidx.lifecycle.e$a r1 = androidx.lifecycle.e.a.ON_PAUSE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = androidx.lifecycle.FullLifecycleObserverAdapter.a.f1619a     // Catch:{ NoSuchFieldError -> 0x0040 }
                androidx.lifecycle.e$a r1 = androidx.lifecycle.e.a.ON_STOP     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = androidx.lifecycle.FullLifecycleObserverAdapter.a.f1619a     // Catch:{ NoSuchFieldError -> 0x004b }
                androidx.lifecycle.e$a r1 = androidx.lifecycle.e.a.ON_DESTROY     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = androidx.lifecycle.FullLifecycleObserverAdapter.a.f1619a     // Catch:{ NoSuchFieldError -> 0x0056 }
                androidx.lifecycle.e$a r1 = androidx.lifecycle.e.a.ON_ANY     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.lifecycle.FullLifecycleObserverAdapter.a.<clinit>():void");
        }
    }

    FullLifecycleObserverAdapter(b bVar, f fVar) {
        this.f1617a = bVar;
        this.f1618b = fVar;
    }

    public void a(h hVar, e.a aVar) {
        switch (a.f1619a[aVar.ordinal()]) {
            case 1:
                this.f1617a.b(hVar);
                break;
            case 2:
                this.f1617a.f(hVar);
                break;
            case 3:
                this.f1617a.a(hVar);
                break;
            case 4:
                this.f1617a.c(hVar);
                break;
            case 5:
                this.f1617a.d(hVar);
                break;
            case 6:
                this.f1617a.e(hVar);
                break;
            case 7:
                throw new IllegalArgumentException("ON_ANY must not been send by anybody");
        }
        f fVar = this.f1618b;
        if (fVar != null) {
            fVar.a(hVar, aVar);
        }
    }
}
