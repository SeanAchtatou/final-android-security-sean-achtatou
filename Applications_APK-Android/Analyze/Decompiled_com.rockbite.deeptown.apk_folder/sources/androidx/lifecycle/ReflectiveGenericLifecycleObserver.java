package androidx.lifecycle;

import androidx.lifecycle.a;
import androidx.lifecycle.e;

class ReflectiveGenericLifecycleObserver implements f {

    /* renamed from: a  reason: collision with root package name */
    private final Object f1638a;

    /* renamed from: b  reason: collision with root package name */
    private final a.C0016a f1639b = a.f1641c.a(this.f1638a.getClass());

    ReflectiveGenericLifecycleObserver(Object obj) {
        this.f1638a = obj;
    }

    public void a(h hVar, e.a aVar) {
        this.f1639b.a(hVar, aVar, this.f1638a);
    }
}
