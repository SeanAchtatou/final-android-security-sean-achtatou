package b.n.a.a;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.util.AttributeSet;
import b.e.f.b;
import com.esotericsoftware.spine.Animation;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: VectorDrawableCompat */
public class i extends h {

    /* renamed from: j  reason: collision with root package name */
    static final PorterDuff.Mode f3095j = PorterDuff.Mode.SRC_IN;

    /* renamed from: b  reason: collision with root package name */
    private h f3096b;

    /* renamed from: c  reason: collision with root package name */
    private PorterDuffColorFilter f3097c;

    /* renamed from: d  reason: collision with root package name */
    private ColorFilter f3098d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f3099e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f3100f;

    /* renamed from: g  reason: collision with root package name */
    private final float[] f3101g;

    /* renamed from: h  reason: collision with root package name */
    private final Matrix f3102h;

    /* renamed from: i  reason: collision with root package name */
    private final Rect f3103i;

    /* compiled from: VectorDrawableCompat */
    private static class b extends f {
        b() {
        }

        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            if (b.e.e.c.g.a(xmlPullParser, "pathData")) {
                TypedArray a2 = b.e.e.c.g.a(resources, theme, attributeSet, a.f3071d);
                a(a2, xmlPullParser);
                a2.recycle();
            }
        }

        public boolean b() {
            return true;
        }

        b(b bVar) {
            super(bVar);
        }

        private void a(TypedArray typedArray, XmlPullParser xmlPullParser) {
            String string = typedArray.getString(0);
            if (string != null) {
                this.f3123b = string;
            }
            String string2 = typedArray.getString(1);
            if (string2 != null) {
                this.f3122a = b.e.f.b.a(string2);
            }
            this.f3124c = b.e.e.c.g.b(typedArray, xmlPullParser, "fillType", 2, 0);
        }
    }

    /* compiled from: VectorDrawableCompat */
    private static abstract class e {
        private e() {
        }

        public boolean a() {
            return false;
        }

        public boolean a(int[] iArr) {
            return false;
        }
    }

    /* compiled from: VectorDrawableCompat */
    private static class h extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        int f3137a;

        /* renamed from: b  reason: collision with root package name */
        g f3138b;

        /* renamed from: c  reason: collision with root package name */
        ColorStateList f3139c;

        /* renamed from: d  reason: collision with root package name */
        PorterDuff.Mode f3140d;

        /* renamed from: e  reason: collision with root package name */
        boolean f3141e;

        /* renamed from: f  reason: collision with root package name */
        Bitmap f3142f;

        /* renamed from: g  reason: collision with root package name */
        ColorStateList f3143g;

        /* renamed from: h  reason: collision with root package name */
        PorterDuff.Mode f3144h;

        /* renamed from: i  reason: collision with root package name */
        int f3145i;

        /* renamed from: j  reason: collision with root package name */
        boolean f3146j;

        /* renamed from: k  reason: collision with root package name */
        boolean f3147k;
        Paint l;

        public h(h hVar) {
            this.f3139c = null;
            this.f3140d = i.f3095j;
            if (hVar != null) {
                this.f3137a = hVar.f3137a;
                this.f3138b = new g(hVar.f3138b);
                Paint paint = hVar.f3138b.f3130e;
                if (paint != null) {
                    this.f3138b.f3130e = new Paint(paint);
                }
                Paint paint2 = hVar.f3138b.f3129d;
                if (paint2 != null) {
                    this.f3138b.f3129d = new Paint(paint2);
                }
                this.f3139c = hVar.f3139c;
                this.f3140d = hVar.f3140d;
                this.f3141e = hVar.f3141e;
            }
        }

        public void a(Canvas canvas, ColorFilter colorFilter, Rect rect) {
            canvas.drawBitmap(this.f3142f, (Rect) null, rect, a(colorFilter));
        }

        public boolean b() {
            return this.f3138b.getRootAlpha() < 255;
        }

        public void c(int i2, int i3) {
            this.f3142f.eraseColor(0);
            this.f3138b.a(new Canvas(this.f3142f), i2, i3, (ColorFilter) null);
        }

        public void d() {
            this.f3143g = this.f3139c;
            this.f3144h = this.f3140d;
            this.f3145i = this.f3138b.getRootAlpha();
            this.f3146j = this.f3141e;
            this.f3147k = false;
        }

        public int getChangingConfigurations() {
            return this.f3137a;
        }

        public Drawable newDrawable() {
            return new i(this);
        }

        public void b(int i2, int i3) {
            if (this.f3142f == null || !a(i2, i3)) {
                this.f3142f = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
                this.f3147k = true;
            }
        }

        public Drawable newDrawable(Resources resources) {
            return new i(this);
        }

        public Paint a(ColorFilter colorFilter) {
            if (!b() && colorFilter == null) {
                return null;
            }
            if (this.l == null) {
                this.l = new Paint();
                this.l.setFilterBitmap(true);
            }
            this.l.setAlpha(this.f3138b.getRootAlpha());
            this.l.setColorFilter(colorFilter);
            return this.l;
        }

        public boolean c() {
            return this.f3138b.a();
        }

        public boolean a(int i2, int i3) {
            return i2 == this.f3142f.getWidth() && i3 == this.f3142f.getHeight();
        }

        public boolean a() {
            return !this.f3147k && this.f3143g == this.f3139c && this.f3144h == this.f3140d && this.f3146j == this.f3141e && this.f3145i == this.f3138b.getRootAlpha();
        }

        public h() {
            this.f3139c = null;
            this.f3140d = i.f3095j;
            this.f3138b = new g();
        }

        public boolean a(int[] iArr) {
            boolean a2 = this.f3138b.a(iArr);
            this.f3147k |= a2;
            return a2;
        }
    }

    i() {
        this.f3100f = true;
        this.f3101g = new float[9];
        this.f3102h = new Matrix();
        this.f3103i = new Rect();
        this.f3096b = new h();
    }

    public static i createFromXmlInner(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        i iVar = new i();
        iVar.inflate(resources, xmlPullParser, attributeSet, theme);
        return iVar;
    }

    /* access modifiers changed from: package-private */
    public Object a(String str) {
        return this.f3096b.f3138b.p.get(str);
    }

    public boolean canApplyTheme() {
        Drawable drawable = this.f3094a;
        if (drawable == null) {
            return false;
        }
        androidx.core.graphics.drawable.a.a(drawable);
        return false;
    }

    public void draw(Canvas canvas) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        copyBounds(this.f3103i);
        if (this.f3103i.width() > 0 && this.f3103i.height() > 0) {
            ColorFilter colorFilter = this.f3098d;
            if (colorFilter == null) {
                colorFilter = this.f3097c;
            }
            canvas.getMatrix(this.f3102h);
            this.f3102h.getValues(this.f3101g);
            float abs = Math.abs(this.f3101g[0]);
            float abs2 = Math.abs(this.f3101g[4]);
            float abs3 = Math.abs(this.f3101g[1]);
            float abs4 = Math.abs(this.f3101g[3]);
            if (!(abs3 == Animation.CurveTimeline.LINEAR && abs4 == Animation.CurveTimeline.LINEAR)) {
                abs = 1.0f;
                abs2 = 1.0f;
            }
            int min = Math.min(2048, (int) (((float) this.f3103i.width()) * abs));
            int min2 = Math.min(2048, (int) (((float) this.f3103i.height()) * abs2));
            if (min > 0 && min2 > 0) {
                int save = canvas.save();
                Rect rect = this.f3103i;
                canvas.translate((float) rect.left, (float) rect.top);
                if (a()) {
                    canvas.translate((float) this.f3103i.width(), Animation.CurveTimeline.LINEAR);
                    canvas.scale(-1.0f, 1.0f);
                }
                this.f3103i.offsetTo(0, 0);
                this.f3096b.b(min, min2);
                if (!this.f3100f) {
                    this.f3096b.c(min, min2);
                } else if (!this.f3096b.a()) {
                    this.f3096b.c(min, min2);
                    this.f3096b.d();
                }
                this.f3096b.a(canvas, colorFilter, this.f3103i);
                canvas.restoreToCount(save);
            }
        }
    }

    public int getAlpha() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return androidx.core.graphics.drawable.a.b(drawable);
        }
        return this.f3096b.f3138b.getRootAlpha();
    }

    public int getChangingConfigurations() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.f3096b.getChangingConfigurations();
    }

    public ColorFilter getColorFilter() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return androidx.core.graphics.drawable.a.c(drawable);
        }
        return this.f3098d;
    }

    public Drawable.ConstantState getConstantState() {
        Drawable drawable = this.f3094a;
        if (drawable != null && Build.VERSION.SDK_INT >= 24) {
            return new C0067i(drawable.getConstantState());
        }
        this.f3096b.f3137a = getChangingConfigurations();
        return this.f3096b;
    }

    public int getIntrinsicHeight() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return (int) this.f3096b.f3138b.f3135j;
    }

    public int getIntrinsicWidth() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return (int) this.f3096b.f3138b.f3134i;
    }

    public int getOpacity() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return -3;
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.inflate(resources, xmlPullParser, attributeSet);
        } else {
            inflate(resources, xmlPullParser, attributeSet, null);
        }
    }

    public void invalidateSelf() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.invalidateSelf();
        } else {
            super.invalidateSelf();
        }
    }

    public boolean isAutoMirrored() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return androidx.core.graphics.drawable.a.e(drawable);
        }
        return this.f3096b.f3141e;
    }

    public boolean isStateful() {
        h hVar;
        ColorStateList colorStateList;
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.isStateful();
        }
        return super.isStateful() || ((hVar = this.f3096b) != null && (hVar.c() || ((colorStateList = this.f3096b.f3139c) != null && colorStateList.isStateful())));
    }

    public Drawable mutate() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.mutate();
            return this;
        }
        if (!this.f3099e && super.mutate() == this) {
            this.f3096b = new h(this.f3096b);
            this.f3099e = true;
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        PorterDuff.Mode mode;
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        boolean z = false;
        h hVar = this.f3096b;
        ColorStateList colorStateList = hVar.f3139c;
        if (!(colorStateList == null || (mode = hVar.f3140d) == null)) {
            this.f3097c = a(this.f3097c, colorStateList, mode);
            invalidateSelf();
            z = true;
        }
        if (!hVar.c() || !hVar.a(iArr)) {
            return z;
        }
        invalidateSelf();
        return true;
    }

    public void scheduleSelf(Runnable runnable, long j2) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.scheduleSelf(runnable, j2);
        } else {
            super.scheduleSelf(runnable, j2);
        }
    }

    public void setAlpha(int i2) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.setAlpha(i2);
        } else if (this.f3096b.f3138b.getRootAlpha() != i2) {
            this.f3096b.f3138b.setRootAlpha(i2);
            invalidateSelf();
        }
    }

    public void setAutoMirrored(boolean z) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.a(drawable, z);
        } else {
            this.f3096b.f3141e = z;
        }
    }

    public void setTint(int i2) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.b(drawable, i2);
        } else {
            setTintList(ColorStateList.valueOf(i2));
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.a(drawable, colorStateList);
            return;
        }
        h hVar = this.f3096b;
        if (hVar.f3139c != colorStateList) {
            hVar.f3139c = colorStateList;
            this.f3097c = a(this.f3097c, colorStateList, hVar.f3140d);
            invalidateSelf();
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.a(drawable, mode);
            return;
        }
        h hVar = this.f3096b;
        if (hVar.f3140d != mode) {
            hVar.f3140d = mode;
            this.f3097c = a(this.f3097c, hVar.f3139c, mode);
            invalidateSelf();
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        return super.setVisible(z, z2);
    }

    public void unscheduleSelf(Runnable runnable) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.unscheduleSelf(runnable);
        } else {
            super.unscheduleSelf(runnable);
        }
    }

    /* renamed from: b.n.a.a.i$i  reason: collision with other inner class name */
    /* compiled from: VectorDrawableCompat */
    private static class C0067i extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        private final Drawable.ConstantState f3148a;

        public C0067i(Drawable.ConstantState constantState) {
            this.f3148a = constantState;
        }

        public boolean canApplyTheme() {
            return this.f3148a.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.f3148a.getChangingConfigurations();
        }

        public Drawable newDrawable() {
            i iVar = new i();
            iVar.f3094a = (VectorDrawable) this.f3148a.newDrawable();
            return iVar;
        }

        public Drawable newDrawable(Resources resources) {
            i iVar = new i();
            iVar.f3094a = (VectorDrawable) this.f3148a.newDrawable(resources);
            return iVar;
        }

        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            i iVar = new i();
            iVar.f3094a = (VectorDrawable) this.f3148a.newDrawable(resources, theme);
            return iVar;
        }
    }

    /* access modifiers changed from: package-private */
    public PorterDuffColorFilter a(PorterDuffColorFilter porterDuffColorFilter, ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
            return;
        }
        this.f3098d = colorFilter;
        invalidateSelf();
    }

    /* compiled from: VectorDrawableCompat */
    private static class c extends f {

        /* renamed from: e  reason: collision with root package name */
        private int[] f3104e;

        /* renamed from: f  reason: collision with root package name */
        b.e.e.c.b f3105f;

        /* renamed from: g  reason: collision with root package name */
        float f3106g = Animation.CurveTimeline.LINEAR;

        /* renamed from: h  reason: collision with root package name */
        b.e.e.c.b f3107h;

        /* renamed from: i  reason: collision with root package name */
        float f3108i = 1.0f;

        /* renamed from: j  reason: collision with root package name */
        float f3109j = 1.0f;

        /* renamed from: k  reason: collision with root package name */
        float f3110k = Animation.CurveTimeline.LINEAR;
        float l = 1.0f;
        float m = Animation.CurveTimeline.LINEAR;
        Paint.Cap n = Paint.Cap.BUTT;
        Paint.Join o = Paint.Join.MITER;
        float p = 4.0f;

        c() {
        }

        private Paint.Cap a(int i2, Paint.Cap cap) {
            if (i2 == 0) {
                return Paint.Cap.BUTT;
            }
            if (i2 != 1) {
                return i2 != 2 ? cap : Paint.Cap.SQUARE;
            }
            return Paint.Cap.ROUND;
        }

        /* access modifiers changed from: package-private */
        public float getFillAlpha() {
            return this.f3109j;
        }

        /* access modifiers changed from: package-private */
        public int getFillColor() {
            return this.f3107h.a();
        }

        /* access modifiers changed from: package-private */
        public float getStrokeAlpha() {
            return this.f3108i;
        }

        /* access modifiers changed from: package-private */
        public int getStrokeColor() {
            return this.f3105f.a();
        }

        /* access modifiers changed from: package-private */
        public float getStrokeWidth() {
            return this.f3106g;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathEnd() {
            return this.l;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathOffset() {
            return this.m;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathStart() {
            return this.f3110k;
        }

        /* access modifiers changed from: package-private */
        public void setFillAlpha(float f2) {
            this.f3109j = f2;
        }

        /* access modifiers changed from: package-private */
        public void setFillColor(int i2) {
            this.f3107h.a(i2);
        }

        /* access modifiers changed from: package-private */
        public void setStrokeAlpha(float f2) {
            this.f3108i = f2;
        }

        /* access modifiers changed from: package-private */
        public void setStrokeColor(int i2) {
            this.f3105f.a(i2);
        }

        /* access modifiers changed from: package-private */
        public void setStrokeWidth(float f2) {
            this.f3106g = f2;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathEnd(float f2) {
            this.l = f2;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathOffset(float f2) {
            this.m = f2;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathStart(float f2) {
            this.f3110k = f2;
        }

        private Paint.Join a(int i2, Paint.Join join) {
            if (i2 == 0) {
                return Paint.Join.MITER;
            }
            if (i2 != 1) {
                return i2 != 2 ? join : Paint.Join.BEVEL;
            }
            return Paint.Join.ROUND;
        }

        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray a2 = b.e.e.c.g.a(resources, theme, attributeSet, a.f3070c);
            a(a2, xmlPullParser, theme);
            a2.recycle();
        }

        private void a(TypedArray typedArray, XmlPullParser xmlPullParser, Resources.Theme theme) {
            this.f3104e = null;
            if (b.e.e.c.g.a(xmlPullParser, "pathData")) {
                String string = typedArray.getString(0);
                if (string != null) {
                    this.f3123b = string;
                }
                String string2 = typedArray.getString(2);
                if (string2 != null) {
                    this.f3122a = b.e.f.b.a(string2);
                }
                Resources.Theme theme2 = theme;
                this.f3107h = b.e.e.c.g.a(typedArray, xmlPullParser, theme2, "fillColor", 1, 0);
                this.f3109j = b.e.e.c.g.a(typedArray, xmlPullParser, "fillAlpha", 12, this.f3109j);
                this.n = a(b.e.e.c.g.b(typedArray, xmlPullParser, "strokeLineCap", 8, -1), this.n);
                this.o = a(b.e.e.c.g.b(typedArray, xmlPullParser, "strokeLineJoin", 9, -1), this.o);
                this.p = b.e.e.c.g.a(typedArray, xmlPullParser, "strokeMiterLimit", 10, this.p);
                this.f3105f = b.e.e.c.g.a(typedArray, xmlPullParser, theme2, "strokeColor", 3, 0);
                this.f3108i = b.e.e.c.g.a(typedArray, xmlPullParser, "strokeAlpha", 11, this.f3108i);
                this.f3106g = b.e.e.c.g.a(typedArray, xmlPullParser, "strokeWidth", 4, this.f3106g);
                this.l = b.e.e.c.g.a(typedArray, xmlPullParser, "trimPathEnd", 6, this.l);
                this.m = b.e.e.c.g.a(typedArray, xmlPullParser, "trimPathOffset", 7, this.m);
                this.f3110k = b.e.e.c.g.a(typedArray, xmlPullParser, "trimPathStart", 5, this.f3110k);
                this.f3124c = b.e.e.c.g.b(typedArray, xmlPullParser, "fillType", 13, this.f3124c);
            }
        }

        c(c cVar) {
            super(cVar);
            this.f3104e = cVar.f3104e;
            this.f3105f = cVar.f3105f;
            this.f3106g = cVar.f3106g;
            this.f3108i = cVar.f3108i;
            this.f3107h = cVar.f3107h;
            this.f3124c = cVar.f3124c;
            this.f3109j = cVar.f3109j;
            this.f3110k = cVar.f3110k;
            this.l = cVar.l;
            this.m = cVar.m;
            this.n = cVar.n;
            this.o = cVar.o;
            this.p = cVar.p;
        }

        public boolean a() {
            return this.f3107h.d() || this.f3105f.d();
        }

        public boolean a(int[] iArr) {
            return this.f3105f.a(iArr) | this.f3107h.a(iArr);
        }
    }

    /* compiled from: VectorDrawableCompat */
    private static class d extends e {

        /* renamed from: a  reason: collision with root package name */
        final Matrix f3111a = new Matrix();

        /* renamed from: b  reason: collision with root package name */
        final ArrayList<e> f3112b = new ArrayList<>();

        /* renamed from: c  reason: collision with root package name */
        float f3113c = Animation.CurveTimeline.LINEAR;

        /* renamed from: d  reason: collision with root package name */
        private float f3114d = Animation.CurveTimeline.LINEAR;

        /* renamed from: e  reason: collision with root package name */
        private float f3115e = Animation.CurveTimeline.LINEAR;

        /* renamed from: f  reason: collision with root package name */
        private float f3116f = 1.0f;

        /* renamed from: g  reason: collision with root package name */
        private float f3117g = 1.0f;

        /* renamed from: h  reason: collision with root package name */
        private float f3118h = Animation.CurveTimeline.LINEAR;

        /* renamed from: i  reason: collision with root package name */
        private float f3119i = Animation.CurveTimeline.LINEAR;

        /* renamed from: j  reason: collision with root package name */
        final Matrix f3120j = new Matrix();

        /* renamed from: k  reason: collision with root package name */
        int f3121k;
        private int[] l;
        private String m = null;

        public d(d dVar, b.d.a<String, Object> aVar) {
            super();
            f fVar;
            this.f3113c = dVar.f3113c;
            this.f3114d = dVar.f3114d;
            this.f3115e = dVar.f3115e;
            this.f3116f = dVar.f3116f;
            this.f3117g = dVar.f3117g;
            this.f3118h = dVar.f3118h;
            this.f3119i = dVar.f3119i;
            this.l = dVar.l;
            this.m = dVar.m;
            this.f3121k = dVar.f3121k;
            String str = this.m;
            if (str != null) {
                aVar.put(str, this);
            }
            this.f3120j.set(dVar.f3120j);
            ArrayList<e> arrayList = dVar.f3112b;
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                e eVar = arrayList.get(i2);
                if (eVar instanceof d) {
                    this.f3112b.add(new d((d) eVar, aVar));
                } else {
                    if (eVar instanceof c) {
                        fVar = new c((c) eVar);
                    } else if (eVar instanceof b) {
                        fVar = new b((b) eVar);
                    } else {
                        throw new IllegalStateException("Unknown object in the tree!");
                    }
                    this.f3112b.add(fVar);
                    String str2 = fVar.f3123b;
                    if (str2 != null) {
                        aVar.put(str2, fVar);
                    }
                }
            }
        }

        private void b() {
            this.f3120j.reset();
            this.f3120j.postTranslate(-this.f3114d, -this.f3115e);
            this.f3120j.postScale(this.f3116f, this.f3117g);
            this.f3120j.postRotate(this.f3113c, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
            this.f3120j.postTranslate(this.f3118h + this.f3114d, this.f3119i + this.f3115e);
        }

        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray a2 = b.e.e.c.g.a(resources, theme, attributeSet, a.f3069b);
            a(a2, xmlPullParser);
            a2.recycle();
        }

        public String getGroupName() {
            return this.m;
        }

        public Matrix getLocalMatrix() {
            return this.f3120j;
        }

        public float getPivotX() {
            return this.f3114d;
        }

        public float getPivotY() {
            return this.f3115e;
        }

        public float getRotation() {
            return this.f3113c;
        }

        public float getScaleX() {
            return this.f3116f;
        }

        public float getScaleY() {
            return this.f3117g;
        }

        public float getTranslateX() {
            return this.f3118h;
        }

        public float getTranslateY() {
            return this.f3119i;
        }

        public void setPivotX(float f2) {
            if (f2 != this.f3114d) {
                this.f3114d = f2;
                b();
            }
        }

        public void setPivotY(float f2) {
            if (f2 != this.f3115e) {
                this.f3115e = f2;
                b();
            }
        }

        public void setRotation(float f2) {
            if (f2 != this.f3113c) {
                this.f3113c = f2;
                b();
            }
        }

        public void setScaleX(float f2) {
            if (f2 != this.f3116f) {
                this.f3116f = f2;
                b();
            }
        }

        public void setScaleY(float f2) {
            if (f2 != this.f3117g) {
                this.f3117g = f2;
                b();
            }
        }

        public void setTranslateX(float f2) {
            if (f2 != this.f3118h) {
                this.f3118h = f2;
                b();
            }
        }

        public void setTranslateY(float f2) {
            if (f2 != this.f3119i) {
                this.f3119i = f2;
                b();
            }
        }

        private void a(TypedArray typedArray, XmlPullParser xmlPullParser) {
            this.l = null;
            this.f3113c = b.e.e.c.g.a(typedArray, xmlPullParser, "rotation", 5, this.f3113c);
            this.f3114d = typedArray.getFloat(1, this.f3114d);
            this.f3115e = typedArray.getFloat(2, this.f3115e);
            this.f3116f = b.e.e.c.g.a(typedArray, xmlPullParser, "scaleX", 3, this.f3116f);
            this.f3117g = b.e.e.c.g.a(typedArray, xmlPullParser, "scaleY", 4, this.f3117g);
            this.f3118h = b.e.e.c.g.a(typedArray, xmlPullParser, "translateX", 6, this.f3118h);
            this.f3119i = b.e.e.c.g.a(typedArray, xmlPullParser, "translateY", 7, this.f3119i);
            String string = typedArray.getString(0);
            if (string != null) {
                this.m = string;
            }
            b();
        }

        public boolean a() {
            for (int i2 = 0; i2 < this.f3112b.size(); i2++) {
                if (this.f3112b.get(i2).a()) {
                    return true;
                }
            }
            return false;
        }

        public boolean a(int[] iArr) {
            boolean z = false;
            for (int i2 = 0; i2 < this.f3112b.size(); i2++) {
                z |= this.f3112b.get(i2).a(iArr);
            }
            return z;
        }

        public d() {
            super();
        }
    }

    /* compiled from: VectorDrawableCompat */
    private static abstract class f extends e {

        /* renamed from: a  reason: collision with root package name */
        protected b.C0046b[] f3122a = null;

        /* renamed from: b  reason: collision with root package name */
        String f3123b;

        /* renamed from: c  reason: collision with root package name */
        int f3124c = 0;

        /* renamed from: d  reason: collision with root package name */
        int f3125d;

        public f() {
            super();
        }

        public void a(Path path) {
            path.reset();
            b.C0046b[] bVarArr = this.f3122a;
            if (bVarArr != null) {
                b.C0046b.a(bVarArr, path);
            }
        }

        public boolean b() {
            return false;
        }

        public b.C0046b[] getPathData() {
            return this.f3122a;
        }

        public String getPathName() {
            return this.f3123b;
        }

        public void setPathData(b.C0046b[] bVarArr) {
            if (!b.e.f.b.a(this.f3122a, bVarArr)) {
                this.f3122a = b.e.f.b.a(bVarArr);
            } else {
                b.e.f.b.b(this.f3122a, bVarArr);
            }
        }

        public f(f fVar) {
            super();
            this.f3123b = fVar.f3123b;
            this.f3125d = fVar.f3125d;
            this.f3122a = b.e.f.b.a(fVar.f3122a);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.n.a.a.i.createFromXmlInner(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):b.n.a.a.i
     arg types: [android.content.res.Resources, android.content.res.XmlResourceParser, android.util.AttributeSet, android.content.res.Resources$Theme]
     candidates:
      ClspMth{android.graphics.drawable.Drawable.createFromXmlInner(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.graphics.drawable.Drawable throws java.io.IOException, org.xmlpull.v1.XmlPullParserException}
      b.n.a.a.i.createFromXmlInner(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):b.n.a.a.i */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036 A[Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003b A[Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static b.n.a.a.i a(android.content.res.Resources r6, int r7, android.content.res.Resources.Theme r8) {
        /*
            java.lang.String r0 = "parser error"
            java.lang.String r1 = "VectorDrawableCompat"
            int r2 = android.os.Build.VERSION.SDK_INT
            r3 = 24
            if (r2 < r3) goto L_0x0021
            b.n.a.a.i r0 = new b.n.a.a.i
            r0.<init>()
            android.graphics.drawable.Drawable r6 = b.e.e.c.f.a(r6, r7, r8)
            r0.f3094a = r6
            b.n.a.a.i$i r6 = new b.n.a.a.i$i
            android.graphics.drawable.Drawable r7 = r0.f3094a
            android.graphics.drawable.Drawable$ConstantState r7 = r7.getConstantState()
            r6.<init>(r7)
            return r0
        L_0x0021:
            android.content.res.XmlResourceParser r7 = r6.getXml(r7)     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            android.util.AttributeSet r2 = android.util.Xml.asAttributeSet(r7)     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
        L_0x0029:
            int r3 = r7.next()     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            r4 = 2
            if (r3 == r4) goto L_0x0034
            r5 = 1
            if (r3 == r5) goto L_0x0034
            goto L_0x0029
        L_0x0034:
            if (r3 != r4) goto L_0x003b
            b.n.a.a.i r6 = createFromXmlInner(r6, r7, r2, r8)     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            return r6
        L_0x003b:
            org.xmlpull.v1.XmlPullParserException r6 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            java.lang.String r7 = "No start tag found"
            r6.<init>(r7)     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            throw r6     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
        L_0x0043:
            r6 = move-exception
            android.util.Log.e(r1, r0, r6)
            goto L_0x004c
        L_0x0048:
            r6 = move-exception
            android.util.Log.e(r1, r0, r6)
        L_0x004c:
            r6 = 0
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: b.n.a.a.i.a(android.content.res.Resources, int, android.content.res.Resources$Theme):b.n.a.a.i");
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.a(drawable, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        h hVar = this.f3096b;
        hVar.f3138b = new g();
        TypedArray a2 = b.e.e.c.g.a(resources, theme, attributeSet, a.f3068a);
        a(a2, xmlPullParser, theme);
        a2.recycle();
        hVar.f3137a = getChangingConfigurations();
        hVar.f3147k = true;
        a(resources, xmlPullParser, attributeSet, theme);
        this.f3097c = a(this.f3097c, hVar.f3139c, hVar.f3140d);
    }

    i(h hVar) {
        this.f3100f = true;
        this.f3101g = new float[9];
        this.f3102h = new Matrix();
        this.f3103i = new Rect();
        this.f3096b = hVar;
        this.f3097c = a(this.f3097c, hVar.f3139c, hVar.f3140d);
    }

    /* compiled from: VectorDrawableCompat */
    private static class g {
        private static final Matrix q = new Matrix();

        /* renamed from: a  reason: collision with root package name */
        private final Path f3126a;

        /* renamed from: b  reason: collision with root package name */
        private final Path f3127b;

        /* renamed from: c  reason: collision with root package name */
        private final Matrix f3128c;

        /* renamed from: d  reason: collision with root package name */
        Paint f3129d;

        /* renamed from: e  reason: collision with root package name */
        Paint f3130e;

        /* renamed from: f  reason: collision with root package name */
        private PathMeasure f3131f;

        /* renamed from: g  reason: collision with root package name */
        private int f3132g;

        /* renamed from: h  reason: collision with root package name */
        final d f3133h;

        /* renamed from: i  reason: collision with root package name */
        float f3134i;

        /* renamed from: j  reason: collision with root package name */
        float f3135j;

        /* renamed from: k  reason: collision with root package name */
        float f3136k;
        float l;
        int m;
        String n;
        Boolean o;
        final b.d.a<String, Object> p;

        public g() {
            this.f3128c = new Matrix();
            this.f3134i = Animation.CurveTimeline.LINEAR;
            this.f3135j = Animation.CurveTimeline.LINEAR;
            this.f3136k = Animation.CurveTimeline.LINEAR;
            this.l = Animation.CurveTimeline.LINEAR;
            this.m = 255;
            this.n = null;
            this.o = null;
            this.p = new b.d.a<>();
            this.f3133h = new d();
            this.f3126a = new Path();
            this.f3127b = new Path();
        }

        private static float a(float f2, float f3, float f4, float f5) {
            return (f2 * f5) - (f3 * f4);
        }

        private void a(d dVar, Matrix matrix, Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            dVar.f3111a.set(matrix);
            dVar.f3111a.preConcat(dVar.f3120j);
            canvas.save();
            for (int i4 = 0; i4 < dVar.f3112b.size(); i4++) {
                e eVar = dVar.f3112b.get(i4);
                if (eVar instanceof d) {
                    a((d) eVar, dVar.f3111a, canvas, i2, i3, colorFilter);
                } else if (eVar instanceof f) {
                    a(dVar, (f) eVar, canvas, i2, i3, colorFilter);
                }
            }
            canvas.restore();
        }

        public float getAlpha() {
            return ((float) getRootAlpha()) / 255.0f;
        }

        public int getRootAlpha() {
            return this.m;
        }

        public void setAlpha(float f2) {
            setRootAlpha((int) (f2 * 255.0f));
        }

        public void setRootAlpha(int i2) {
            this.m = i2;
        }

        public void a(Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            a(this.f3133h, q, canvas, i2, i3, colorFilter);
        }

        public g(g gVar) {
            this.f3128c = new Matrix();
            this.f3134i = Animation.CurveTimeline.LINEAR;
            this.f3135j = Animation.CurveTimeline.LINEAR;
            this.f3136k = Animation.CurveTimeline.LINEAR;
            this.l = Animation.CurveTimeline.LINEAR;
            this.m = 255;
            this.n = null;
            this.o = null;
            this.p = new b.d.a<>();
            this.f3133h = new d(gVar.f3133h, this.p);
            this.f3126a = new Path(gVar.f3126a);
            this.f3127b = new Path(gVar.f3127b);
            this.f3134i = gVar.f3134i;
            this.f3135j = gVar.f3135j;
            this.f3136k = gVar.f3136k;
            this.l = gVar.l;
            this.f3132g = gVar.f3132g;
            this.m = gVar.m;
            this.n = gVar.n;
            String str = gVar.n;
            if (str != null) {
                this.p.put(str, this);
            }
            this.o = gVar.o;
        }

        private void a(d dVar, f fVar, Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            float f2 = ((float) i2) / this.f3136k;
            float f3 = ((float) i3) / this.l;
            float min = Math.min(f2, f3);
            Matrix matrix = dVar.f3111a;
            this.f3128c.set(matrix);
            this.f3128c.postScale(f2, f3);
            float a2 = a(matrix);
            if (a2 != Animation.CurveTimeline.LINEAR) {
                fVar.a(this.f3126a);
                Path path = this.f3126a;
                this.f3127b.reset();
                if (fVar.b()) {
                    this.f3127b.setFillType(fVar.f3124c == 0 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD);
                    this.f3127b.addPath(path, this.f3128c);
                    canvas.clipPath(this.f3127b);
                    return;
                }
                c cVar = (c) fVar;
                if (!(cVar.f3110k == Animation.CurveTimeline.LINEAR && cVar.l == 1.0f)) {
                    float f4 = cVar.f3110k;
                    float f5 = cVar.m;
                    float f6 = (f4 + f5) % 1.0f;
                    float f7 = (cVar.l + f5) % 1.0f;
                    if (this.f3131f == null) {
                        this.f3131f = new PathMeasure();
                    }
                    this.f3131f.setPath(this.f3126a, false);
                    float length = this.f3131f.getLength();
                    float f8 = f6 * length;
                    float f9 = f7 * length;
                    path.reset();
                    if (f8 > f9) {
                        this.f3131f.getSegment(f8, length, path, true);
                        this.f3131f.getSegment(Animation.CurveTimeline.LINEAR, f9, path, true);
                    } else {
                        this.f3131f.getSegment(f8, f9, path, true);
                    }
                    path.rLineTo(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                }
                this.f3127b.addPath(path, this.f3128c);
                if (cVar.f3107h.e()) {
                    b.e.e.c.b bVar = cVar.f3107h;
                    if (this.f3130e == null) {
                        this.f3130e = new Paint(1);
                        this.f3130e.setStyle(Paint.Style.FILL);
                    }
                    Paint paint = this.f3130e;
                    if (bVar.c()) {
                        Shader b2 = bVar.b();
                        b2.setLocalMatrix(this.f3128c);
                        paint.setShader(b2);
                        paint.setAlpha(Math.round(cVar.f3109j * 255.0f));
                    } else {
                        paint.setShader(null);
                        paint.setAlpha(255);
                        paint.setColor(i.a(bVar.a(), cVar.f3109j));
                    }
                    paint.setColorFilter(colorFilter);
                    this.f3127b.setFillType(cVar.f3124c == 0 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD);
                    canvas.drawPath(this.f3127b, paint);
                }
                if (cVar.f3105f.e()) {
                    b.e.e.c.b bVar2 = cVar.f3105f;
                    if (this.f3129d == null) {
                        this.f3129d = new Paint(1);
                        this.f3129d.setStyle(Paint.Style.STROKE);
                    }
                    Paint paint2 = this.f3129d;
                    Paint.Join join = cVar.o;
                    if (join != null) {
                        paint2.setStrokeJoin(join);
                    }
                    Paint.Cap cap = cVar.n;
                    if (cap != null) {
                        paint2.setStrokeCap(cap);
                    }
                    paint2.setStrokeMiter(cVar.p);
                    if (bVar2.c()) {
                        Shader b3 = bVar2.b();
                        b3.setLocalMatrix(this.f3128c);
                        paint2.setShader(b3);
                        paint2.setAlpha(Math.round(cVar.f3108i * 255.0f));
                    } else {
                        paint2.setShader(null);
                        paint2.setAlpha(255);
                        paint2.setColor(i.a(bVar2.a(), cVar.f3108i));
                    }
                    paint2.setColorFilter(colorFilter);
                    paint2.setStrokeWidth(cVar.f3106g * min * a2);
                    canvas.drawPath(this.f3127b, paint2);
                }
            }
        }

        private float a(Matrix matrix) {
            float[] fArr = {Animation.CurveTimeline.LINEAR, 1.0f, 1.0f, Animation.CurveTimeline.LINEAR};
            matrix.mapVectors(fArr);
            float a2 = a(fArr[0], fArr[1], fArr[2], fArr[3]);
            float max = Math.max((float) Math.hypot((double) fArr[0], (double) fArr[1]), (float) Math.hypot((double) fArr[2], (double) fArr[3]));
            if (max > Animation.CurveTimeline.LINEAR) {
                return Math.abs(a2) / max;
            }
            return Animation.CurveTimeline.LINEAR;
        }

        public boolean a() {
            if (this.o == null) {
                this.o = Boolean.valueOf(this.f3133h.a());
            }
            return this.o.booleanValue();
        }

        public boolean a(int[] iArr) {
            return this.f3133h.a(iArr);
        }
    }

    static int a(int i2, float f2) {
        return (i2 & 16777215) | (((int) (((float) Color.alpha(i2)) * f2)) << 24);
    }

    private static PorterDuff.Mode a(int i2, PorterDuff.Mode mode) {
        if (i2 == 3) {
            return PorterDuff.Mode.SRC_OVER;
        }
        if (i2 == 5) {
            return PorterDuff.Mode.SRC_IN;
        }
        if (i2 == 9) {
            return PorterDuff.Mode.SRC_ATOP;
        }
        switch (i2) {
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                return PorterDuff.Mode.ADD;
            default:
                return mode;
        }
    }

    private void a(TypedArray typedArray, XmlPullParser xmlPullParser, Resources.Theme theme) throws XmlPullParserException {
        h hVar = this.f3096b;
        g gVar = hVar.f3138b;
        hVar.f3140d = a(b.e.e.c.g.b(typedArray, xmlPullParser, "tintMode", 6, -1), PorterDuff.Mode.SRC_IN);
        ColorStateList a2 = b.e.e.c.g.a(typedArray, xmlPullParser, theme, "tint", 1);
        if (a2 != null) {
            hVar.f3139c = a2;
        }
        hVar.f3141e = b.e.e.c.g.a(typedArray, xmlPullParser, "autoMirrored", 5, hVar.f3141e);
        gVar.f3136k = b.e.e.c.g.a(typedArray, xmlPullParser, "viewportWidth", 7, gVar.f3136k);
        gVar.l = b.e.e.c.g.a(typedArray, xmlPullParser, "viewportHeight", 8, gVar.l);
        if (gVar.f3136k <= Animation.CurveTimeline.LINEAR) {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportWidth > 0");
        } else if (gVar.l > Animation.CurveTimeline.LINEAR) {
            gVar.f3134i = typedArray.getDimension(3, gVar.f3134i);
            gVar.f3135j = typedArray.getDimension(2, gVar.f3135j);
            if (gVar.f3134i <= Animation.CurveTimeline.LINEAR) {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires width > 0");
            } else if (gVar.f3135j > Animation.CurveTimeline.LINEAR) {
                gVar.setAlpha(b.e.e.c.g.a(typedArray, xmlPullParser, "alpha", 4, gVar.getAlpha()));
                String string = typedArray.getString(0);
                if (string != null) {
                    gVar.n = string;
                    gVar.p.put(string, gVar);
                }
            } else {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires height > 0");
            }
        } else {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportHeight > 0");
        }
    }

    private void a(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        h hVar = this.f3096b;
        g gVar = hVar.f3138b;
        ArrayDeque arrayDeque = new ArrayDeque();
        arrayDeque.push(gVar.f3133h);
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        boolean z = true;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                d dVar = (d) arrayDeque.peek();
                if ("path".equals(name)) {
                    c cVar = new c();
                    cVar.a(resources, attributeSet, theme, xmlPullParser);
                    dVar.f3112b.add(cVar);
                    if (cVar.getPathName() != null) {
                        gVar.p.put(cVar.getPathName(), cVar);
                    }
                    z = false;
                    hVar.f3137a = cVar.f3125d | hVar.f3137a;
                } else if ("clip-path".equals(name)) {
                    b bVar = new b();
                    bVar.a(resources, attributeSet, theme, xmlPullParser);
                    dVar.f3112b.add(bVar);
                    if (bVar.getPathName() != null) {
                        gVar.p.put(bVar.getPathName(), bVar);
                    }
                    hVar.f3137a = bVar.f3125d | hVar.f3137a;
                } else if ("group".equals(name)) {
                    d dVar2 = new d();
                    dVar2.a(resources, attributeSet, theme, xmlPullParser);
                    dVar.f3112b.add(dVar2);
                    arrayDeque.push(dVar2);
                    if (dVar2.getGroupName() != null) {
                        gVar.p.put(dVar2.getGroupName(), dVar2);
                    }
                    hVar.f3137a = dVar2.f3121k | hVar.f3137a;
                }
            } else if (eventType == 3 && "group".equals(xmlPullParser.getName())) {
                arrayDeque.pop();
            }
            eventType = xmlPullParser.next();
        }
        if (z) {
            throw new XmlPullParserException("no path defined");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.f3100f = z;
    }

    private boolean a() {
        if (Build.VERSION.SDK_INT < 17 || !isAutoMirrored() || androidx.core.graphics.drawable.a.d(this) != 1) {
            return false;
        }
        return true;
    }
}
