package b.n.a.a;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import b.e.e.c.g;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: AnimatedVectorDrawableCompat */
public class c extends h implements b {

    /* renamed from: b  reason: collision with root package name */
    private b f3079b;

    /* renamed from: c  reason: collision with root package name */
    private Context f3080c;

    /* renamed from: d  reason: collision with root package name */
    private ArgbEvaluator f3081d;

    /* renamed from: e  reason: collision with root package name */
    final Drawable.Callback f3082e;

    /* compiled from: AnimatedVectorDrawableCompat */
    class a implements Drawable.Callback {
        a() {
        }

        public void invalidateDrawable(Drawable drawable) {
            c.this.invalidateSelf();
        }

        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
            c.this.scheduleSelf(runnable, j2);
        }

        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            c.this.unscheduleSelf(runnable);
        }
    }

    /* compiled from: AnimatedVectorDrawableCompat */
    private static class b extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        int f3084a;

        /* renamed from: b  reason: collision with root package name */
        i f3085b;

        /* renamed from: c  reason: collision with root package name */
        AnimatorSet f3086c;

        /* renamed from: d  reason: collision with root package name */
        ArrayList<Animator> f3087d;

        /* renamed from: e  reason: collision with root package name */
        b.d.a<Animator, String> f3088e;

        public b(Context context, b bVar, Drawable.Callback callback, Resources resources) {
            if (bVar != null) {
                this.f3084a = bVar.f3084a;
                i iVar = bVar.f3085b;
                if (iVar != null) {
                    Drawable.ConstantState constantState = iVar.getConstantState();
                    if (resources != null) {
                        this.f3085b = (i) constantState.newDrawable(resources);
                    } else {
                        this.f3085b = (i) constantState.newDrawable();
                    }
                    i iVar2 = this.f3085b;
                    iVar2.mutate();
                    this.f3085b = iVar2;
                    this.f3085b.setCallback(callback);
                    this.f3085b.setBounds(bVar.f3085b.getBounds());
                    this.f3085b.a(false);
                }
                ArrayList<Animator> arrayList = bVar.f3087d;
                if (arrayList != null) {
                    int size = arrayList.size();
                    this.f3087d = new ArrayList<>(size);
                    this.f3088e = new b.d.a<>(size);
                    for (int i2 = 0; i2 < size; i2++) {
                        Animator animator = bVar.f3087d.get(i2);
                        Animator clone = animator.clone();
                        String str = bVar.f3088e.get(animator);
                        clone.setTarget(this.f3085b.a(str));
                        this.f3087d.add(clone);
                        this.f3088e.put(clone, str);
                    }
                    a();
                }
            }
        }

        public void a() {
            if (this.f3086c == null) {
                this.f3086c = new AnimatorSet();
            }
            this.f3086c.playTogether(this.f3087d);
        }

        public int getChangingConfigurations() {
            return this.f3084a;
        }

        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        public Drawable newDrawable(Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }
    }

    c() {
        this(null, null, null);
    }

    public static c a(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        c cVar = new c(context);
        cVar.inflate(resources, xmlPullParser, attributeSet, theme);
        return cVar;
    }

    public void applyTheme(Resources.Theme theme) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.a(drawable, theme);
        }
    }

    public boolean canApplyTheme() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return androidx.core.graphics.drawable.a.a(drawable);
        }
        return false;
    }

    public void draw(Canvas canvas) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        this.f3079b.f3085b.draw(canvas);
        if (this.f3079b.f3086c.isStarted()) {
            invalidateSelf();
        }
    }

    public int getAlpha() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return androidx.core.graphics.drawable.a.b(drawable);
        }
        return this.f3079b.f3085b.getAlpha();
    }

    public int getChangingConfigurations() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.f3079b.f3084a;
    }

    public ColorFilter getColorFilter() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return androidx.core.graphics.drawable.a.c(drawable);
        }
        return this.f3079b.f3085b.getColorFilter();
    }

    public Drawable.ConstantState getConstantState() {
        Drawable drawable = this.f3094a;
        if (drawable == null || Build.VERSION.SDK_INT < 24) {
            return null;
        }
        return new C0066c(drawable.getConstantState());
    }

    public int getIntrinsicHeight() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return this.f3079b.f3085b.getIntrinsicHeight();
    }

    public int getIntrinsicWidth() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return this.f3079b.f3085b.getIntrinsicWidth();
    }

    public int getOpacity() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return this.f3079b.f3085b.getOpacity();
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.a(drawable, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                if ("animated-vector".equals(name)) {
                    TypedArray a2 = g.a(resources, theme, attributeSet, a.f3072e);
                    int resourceId = a2.getResourceId(0, 0);
                    if (resourceId != 0) {
                        i a3 = i.a(resources, resourceId, theme);
                        a3.a(false);
                        a3.setCallback(this.f3082e);
                        i iVar = this.f3079b.f3085b;
                        if (iVar != null) {
                            iVar.setCallback(null);
                        }
                        this.f3079b.f3085b = a3;
                    }
                    a2.recycle();
                } else if ("target".equals(name)) {
                    TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, a.f3073f);
                    String string = obtainAttributes.getString(0);
                    int resourceId2 = obtainAttributes.getResourceId(1, 0);
                    if (resourceId2 != 0) {
                        Context context = this.f3080c;
                        if (context != null) {
                            a(string, e.a(context, resourceId2));
                        } else {
                            obtainAttributes.recycle();
                            throw new IllegalStateException("Context can't be null when inflating animators");
                        }
                    }
                    obtainAttributes.recycle();
                } else {
                    continue;
                }
            }
            eventType = xmlPullParser.next();
        }
        this.f3079b.a();
    }

    public boolean isAutoMirrored() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return androidx.core.graphics.drawable.a.e(drawable);
        }
        return this.f3079b.f3085b.isAutoMirrored();
    }

    public boolean isRunning() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return ((AnimatedVectorDrawable) drawable).isRunning();
        }
        return this.f3079b.f3086c.isRunning();
    }

    public boolean isStateful() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.isStateful();
        }
        return this.f3079b.f3085b.isStateful();
    }

    public Drawable mutate() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.mutate();
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.setBounds(rect);
        } else {
            this.f3079b.f3085b.setBounds(rect);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i2) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.setLevel(i2);
        }
        return this.f3079b.f3085b.setLevel(i2);
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        return this.f3079b.f3085b.setState(iArr);
    }

    public void setAlpha(int i2) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.setAlpha(i2);
        } else {
            this.f3079b.f3085b.setAlpha(i2);
        }
    }

    public void setAutoMirrored(boolean z) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.a(drawable, z);
        } else {
            this.f3079b.f3085b.setAutoMirrored(z);
        }
    }

    public void setTint(int i2) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.b(drawable, i2);
        } else {
            this.f3079b.f3085b.setTint(i2);
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.a(drawable, colorStateList);
        } else {
            this.f3079b.f3085b.setTintList(colorStateList);
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.a(drawable, mode);
        } else {
            this.f3079b.f3085b.setTintMode(mode);
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        this.f3079b.f3085b.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    public void start() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).start();
        } else if (!this.f3079b.f3086c.isStarted()) {
            this.f3079b.f3086c.start();
            invalidateSelf();
        }
    }

    public void stop() {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).stop();
        } else {
            this.f3079b.f3086c.end();
        }
    }

    private c(Context context) {
        this(context, null, null);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = this.f3094a;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
        } else {
            this.f3079b.f3085b.setColorFilter(colorFilter);
        }
    }

    /* renamed from: b.n.a.a.c$c  reason: collision with other inner class name */
    /* compiled from: AnimatedVectorDrawableCompat */
    private static class C0066c extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        private final Drawable.ConstantState f3089a;

        public C0066c(Drawable.ConstantState constantState) {
            this.f3089a = constantState;
        }

        public boolean canApplyTheme() {
            return this.f3089a.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.f3089a.getChangingConfigurations();
        }

        public Drawable newDrawable() {
            c cVar = new c();
            cVar.f3094a = this.f3089a.newDrawable();
            cVar.f3094a.setCallback(cVar.f3082e);
            return cVar;
        }

        public Drawable newDrawable(Resources resources) {
            c cVar = new c();
            cVar.f3094a = this.f3089a.newDrawable(resources);
            cVar.f3094a.setCallback(cVar.f3082e);
            return cVar;
        }

        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            c cVar = new c();
            cVar.f3094a = this.f3089a.newDrawable(resources, theme);
            cVar.f3094a.setCallback(cVar.f3082e);
            return cVar;
        }
    }

    private c(Context context, b bVar, Resources resources) {
        this.f3081d = null;
        this.f3082e = new a();
        this.f3080c = context;
        if (bVar != null) {
            this.f3079b = bVar;
        } else {
            this.f3079b = new b(context, bVar, this.f3082e, resources);
        }
    }

    private void a(Animator animator) {
        ArrayList<Animator> childAnimations;
        if ((animator instanceof AnimatorSet) && (childAnimations = ((AnimatorSet) animator).getChildAnimations()) != null) {
            for (int i2 = 0; i2 < childAnimations.size(); i2++) {
                a(childAnimations.get(i2));
            }
        }
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                if (this.f3081d == null) {
                    this.f3081d = new ArgbEvaluator();
                }
                objectAnimator.setEvaluator(this.f3081d);
            }
        }
    }

    private void a(String str, Animator animator) {
        animator.setTarget(this.f3079b.f3085b.a(str));
        if (Build.VERSION.SDK_INT < 21) {
            a(animator);
        }
        b bVar = this.f3079b;
        if (bVar.f3087d == null) {
            bVar.f3087d = new ArrayList<>();
            this.f3079b.f3088e = new b.d.a<>();
        }
        this.f3079b.f3087d.add(animator);
        this.f3079b.f3088e.put(animator, str);
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        inflate(resources, xmlPullParser, attributeSet, null);
    }
}
