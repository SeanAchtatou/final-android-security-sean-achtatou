package b.j.a;

import android.os.Bundle;
import androidx.lifecycle.h;
import androidx.lifecycle.v;
import b.j.b.b;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: LoaderManager */
public abstract class a {

    /* renamed from: b.j.a.a$a  reason: collision with other inner class name */
    /* compiled from: LoaderManager */
    public interface C0057a<D> {
        b<D> onCreateLoader(int i2, Bundle bundle);

        void onLoadFinished(b<D> bVar, D d2);

        void onLoaderReset(b<D> bVar);
    }

    public static <T extends h & v> a a(T t) {
        return new b(t, ((v) t).getViewModelStore());
    }

    public abstract <D> b<D> a(int i2, Bundle bundle, C0057a<D> aVar);

    public abstract void a();

    @Deprecated
    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);
}
