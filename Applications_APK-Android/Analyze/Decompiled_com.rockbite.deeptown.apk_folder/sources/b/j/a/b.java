package b.j.a;

import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import androidx.lifecycle.h;
import androidx.lifecycle.n;
import androidx.lifecycle.o;
import androidx.lifecycle.s;
import androidx.lifecycle.t;
import androidx.lifecycle.u;
import androidx.work.n;
import b.j.a.a;
import b.j.b.b;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

/* compiled from: LoaderManagerImpl */
class b extends a {

    /* renamed from: c  reason: collision with root package name */
    static boolean f2993c = false;

    /* renamed from: a  reason: collision with root package name */
    private final h f2994a;

    /* renamed from: b  reason: collision with root package name */
    private final c f2995b;

    /* compiled from: LoaderManagerImpl */
    static class c extends s {

        /* renamed from: d  reason: collision with root package name */
        private static final t.a f3000d = new a();

        /* renamed from: b  reason: collision with root package name */
        private b.d.h<a> f3001b = new b.d.h<>();

        /* renamed from: c  reason: collision with root package name */
        private boolean f3002c = false;

        /* compiled from: LoaderManagerImpl */
        static class a implements t.a {
            a() {
            }

            public <T extends s> T a(Class<T> cls) {
                return new c();
            }
        }

        c() {
        }

        static c a(u uVar) {
            return (c) new t(uVar, f3000d).a(c.class);
        }

        /* access modifiers changed from: protected */
        public void b() {
            super.b();
            int a2 = this.f3001b.a();
            for (int i2 = 0; i2 < a2; i2++) {
                this.f3001b.e(i2).a(true);
            }
            this.f3001b.clear();
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.f3002c = false;
        }

        /* access modifiers changed from: package-private */
        public boolean d() {
            return this.f3002c;
        }

        /* access modifiers changed from: package-private */
        public void e() {
            int a2 = this.f3001b.a();
            for (int i2 = 0; i2 < a2; i2++) {
                this.f3001b.e(i2).f();
            }
        }

        /* access modifiers changed from: package-private */
        public void f() {
            this.f3002c = true;
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, a aVar) {
            this.f3001b.c(i2, aVar);
        }

        /* access modifiers changed from: package-private */
        public <D> a<D> a(int i2) {
            return this.f3001b.a(i2);
        }

        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            if (this.f3001b.a() > 0) {
                printWriter.print(str);
                printWriter.println("Loaders:");
                String str2 = str + "    ";
                for (int i2 = 0; i2 < this.f3001b.a(); i2++) {
                    a e2 = this.f3001b.e(i2);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(this.f3001b.c(i2));
                    printWriter.print(": ");
                    printWriter.println(e2.toString());
                    e2.a(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
    }

    b(h hVar, u uVar) {
        this.f2994a = hVar;
        this.f2995b = c.a(uVar);
    }

    /* JADX INFO: finally extract failed */
    private <D> b.j.b.b<D> a(int i2, Bundle bundle, a.C0057a<D> aVar, b.j.b.b<D> bVar) {
        try {
            this.f2995b.f();
            b.j.b.b<D> onCreateLoader = aVar.onCreateLoader(i2, bundle);
            if (onCreateLoader != null) {
                if (onCreateLoader.getClass().isMemberClass()) {
                    if (!Modifier.isStatic(onCreateLoader.getClass().getModifiers())) {
                        throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + onCreateLoader);
                    }
                }
                a aVar2 = new a(i2, bundle, onCreateLoader, bVar);
                if (f2993c) {
                    Log.v("LoaderManager", "  Created new loader " + aVar2);
                }
                this.f2995b.a(i2, aVar2);
                this.f2995b.c();
                return aVar2.a(this.f2994a, aVar);
            }
            throw new IllegalArgumentException("Object returned from onCreateLoader must not be null");
        } catch (Throwable th) {
            this.f2995b.c();
            throw th;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        b.e.l.a.a(this.f2994a, sb);
        sb.append("}}");
        return sb.toString();
    }

    /* renamed from: b.j.a.b$b  reason: collision with other inner class name */
    /* compiled from: LoaderManagerImpl */
    static class C0058b<D> implements o<D> {

        /* renamed from: a  reason: collision with root package name */
        private final b.j.b.b<D> f2997a;

        /* renamed from: b  reason: collision with root package name */
        private final a.C0057a<D> f2998b;

        /* renamed from: c  reason: collision with root package name */
        private boolean f2999c = false;

        C0058b(b.j.b.b<D> bVar, a.C0057a<D> aVar) {
            this.f2997a = bVar;
            this.f2998b = aVar;
        }

        public void a(D d2) {
            if (b.f2993c) {
                Log.v("LoaderManager", "  onLoadFinished in " + this.f2997a + ": " + this.f2997a.dataToString(d2));
            }
            this.f2998b.onLoadFinished(this.f2997a, d2);
            this.f2999c = true;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            if (this.f2999c) {
                if (b.f2993c) {
                    Log.v("LoaderManager", "  Resetting: " + this.f2997a);
                }
                this.f2998b.onLoaderReset(this.f2997a);
            }
        }

        public String toString() {
            return this.f2998b.toString();
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return this.f2999c;
        }

        public void a(String str, PrintWriter printWriter) {
            printWriter.print(str);
            printWriter.print("mDeliveredData=");
            printWriter.println(this.f2999c);
        }
    }

    /* compiled from: LoaderManagerImpl */
    public static class a<D> extends n<D> implements b.C0060b<D> {

        /* renamed from: k  reason: collision with root package name */
        private final int f2996k;
        private final Bundle l;
        private final b.j.b.b<D> m;
        private h n;
        private C0058b<D> o;
        private b.j.b.b<D> p;

        a(int i2, Bundle bundle, b.j.b.b<D> bVar, b.j.b.b<D> bVar2) {
            this.f2996k = i2;
            this.l = bundle;
            this.m = bVar;
            this.p = bVar2;
            this.m.registerListener(i2, this);
        }

        /* access modifiers changed from: package-private */
        public b.j.b.b<D> a(h hVar, a.C0057a<D> aVar) {
            C0058b<D> bVar = new C0058b<>(this.m, aVar);
            a(hVar, bVar);
            C0058b<D> bVar2 = this.o;
            if (bVar2 != null) {
                a((o) bVar2);
            }
            this.n = hVar;
            this.o = bVar;
            return this.m;
        }

        public void b(D d2) {
            super.b((h) d2);
            b.j.b.b<D> bVar = this.p;
            if (bVar != null) {
                bVar.reset();
                this.p = null;
            }
        }

        /* access modifiers changed from: protected */
        public void c() {
            if (b.f2993c) {
                Log.v("LoaderManager", "  Starting: " + this);
            }
            this.m.startLoading();
        }

        /* access modifiers changed from: protected */
        public void d() {
            if (b.f2993c) {
                Log.v("LoaderManager", "  Stopping: " + this);
            }
            this.m.stopLoading();
        }

        /* access modifiers changed from: package-private */
        public b.j.b.b<D> e() {
            return this.m;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            h hVar = this.n;
            C0058b<D> bVar = this.o;
            if (hVar != null && bVar != null) {
                super.a((o) bVar);
                a(hVar, bVar);
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.f2996k);
            sb.append(" : ");
            b.e.l.a.a(this.m, sb);
            sb.append("}}");
            return sb.toString();
        }

        public void a(o<? super D> oVar) {
            super.a((o) oVar);
            this.n = null;
            this.o = null;
        }

        /* access modifiers changed from: package-private */
        public b.j.b.b<D> a(boolean z) {
            if (b.f2993c) {
                Log.v("LoaderManager", "  Destroying: " + this);
            }
            this.m.cancelLoad();
            this.m.abandon();
            C0058b<D> bVar = this.o;
            if (bVar != null) {
                a((o) bVar);
                if (z) {
                    bVar.b();
                }
            }
            this.m.unregisterListener(this);
            if ((bVar == null || bVar.a()) && !z) {
                return this.m;
            }
            this.m.reset();
            return this.p;
        }

        public void a(b.j.b.b<D> bVar, D d2) {
            if (b.f2993c) {
                Log.v("LoaderManager", "onLoadComplete: " + this);
            }
            if (Looper.myLooper() == Looper.getMainLooper()) {
                b((Object) d2);
                return;
            }
            if (b.f2993c) {
                Log.w("LoaderManager", "onLoadComplete was incorrectly called on a background thread");
            }
            a((n.b) d2);
        }

        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.f2996k);
            printWriter.print(" mArgs=");
            printWriter.println(this.l);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.m);
            b.j.b.b<D> bVar = this.m;
            bVar.dump(str + "  ", fileDescriptor, printWriter, strArr);
            if (this.o != null) {
                printWriter.print(str);
                printWriter.print("mCallbacks=");
                printWriter.println(this.o);
                C0058b<D> bVar2 = this.o;
                bVar2.a(str + "  ", printWriter);
            }
            printWriter.print(str);
            printWriter.print("mData=");
            printWriter.println(e().dataToString(a()));
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.println(b());
        }
    }

    public <D> b.j.b.b<D> a(int i2, Bundle bundle, a.C0057a<D> aVar) {
        if (this.f2995b.d()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            a a2 = this.f2995b.a(i2);
            if (f2993c) {
                Log.v("LoaderManager", "initLoader in " + this + ": args=" + bundle);
            }
            if (a2 == null) {
                return a(i2, bundle, aVar, (b.j.b.b) null);
            }
            if (f2993c) {
                Log.v("LoaderManager", "  Re-using existing loader " + a2);
            }
            return a2.a(this.f2994a, aVar);
        } else {
            throw new IllegalStateException("initLoader must be called on the main thread");
        }
    }

    public void a() {
        this.f2995b.e();
    }

    @Deprecated
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        this.f2995b.a(str, fileDescriptor, printWriter, strArr);
    }
}
