package b.j.b;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import b.e.i.b;
import b.e.l.g;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

/* compiled from: AsyncTaskLoader */
public abstract class a<D> extends b<D> {
    static final boolean DEBUG = false;
    static final String TAG = "AsyncTaskLoader";
    volatile a<D>.a mCancellingTask;
    private final Executor mExecutor;
    Handler mHandler;
    long mLastLoadCompleteTime;
    volatile a<D>.a mTask;
    long mUpdateThrottle;

    /* renamed from: b.j.b.a$a  reason: collision with other inner class name */
    /* compiled from: AsyncTaskLoader */
    final class C0059a extends c<Void, Void, D> implements Runnable {

        /* renamed from: j  reason: collision with root package name */
        private final CountDownLatch f3003j = new CountDownLatch(1);

        /* renamed from: k  reason: collision with root package name */
        boolean f3004k;

        C0059a() {
        }

        /* access modifiers changed from: protected */
        public void b(D d2) {
            try {
                a.this.dispatchOnCancelled(this, d2);
            } finally {
                this.f3003j.countDown();
            }
        }

        /* access modifiers changed from: protected */
        public void c(D d2) {
            try {
                a.this.dispatchOnLoadComplete(this, d2);
            } finally {
                this.f3003j.countDown();
            }
        }

        public void d() {
            try {
                this.f3003j.await();
            } catch (InterruptedException unused) {
            }
        }

        public void run() {
            this.f3004k = false;
            a.this.executePendingTask();
        }

        /* access modifiers changed from: protected */
        public D a(Void... voidArr) {
            try {
                return a.this.onLoadInBackground();
            } catch (b e2) {
                if (a()) {
                    return null;
                }
                throw e2;
            }
        }
    }

    public a(Context context) {
        this(context, c.f3007h);
    }

    public void cancelLoadInBackground() {
    }

    /* access modifiers changed from: package-private */
    public void dispatchOnCancelled(a<D>.a aVar, D d2) {
        onCanceled(d2);
        if (this.mCancellingTask == aVar) {
            rollbackContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mCancellingTask = null;
            deliverCancellation();
            executePendingTask();
        }
    }

    /* access modifiers changed from: package-private */
    public void dispatchOnLoadComplete(a<D>.a aVar, D d2) {
        if (this.mTask != aVar) {
            dispatchOnCancelled(aVar, d2);
        } else if (isAbandoned()) {
            onCanceled(d2);
        } else {
            commitContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mTask = null;
            deliverResult(d2);
        }
    }

    @Deprecated
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        if (this.mTask != null) {
            printWriter.print(str);
            printWriter.print("mTask=");
            printWriter.print(this.mTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mTask.f3004k);
        }
        if (this.mCancellingTask != null) {
            printWriter.print(str);
            printWriter.print("mCancellingTask=");
            printWriter.print(this.mCancellingTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mCancellingTask.f3004k);
        }
        if (this.mUpdateThrottle != 0) {
            printWriter.print(str);
            printWriter.print("mUpdateThrottle=");
            g.a(this.mUpdateThrottle, printWriter);
            printWriter.print(" mLastLoadCompleteTime=");
            g.a(this.mLastLoadCompleteTime, SystemClock.uptimeMillis(), printWriter);
            printWriter.println();
        }
    }

    /* access modifiers changed from: package-private */
    public void executePendingTask() {
        if (this.mCancellingTask == null && this.mTask != null) {
            if (this.mTask.f3004k) {
                this.mTask.f3004k = false;
                this.mHandler.removeCallbacks(this.mTask);
            }
            if (this.mUpdateThrottle <= 0 || SystemClock.uptimeMillis() >= this.mLastLoadCompleteTime + this.mUpdateThrottle) {
                this.mTask.a(this.mExecutor, null);
                return;
            }
            this.mTask.f3004k = true;
            this.mHandler.postAtTime(this.mTask, this.mLastLoadCompleteTime + this.mUpdateThrottle);
        }
    }

    public boolean isLoadInBackgroundCanceled() {
        return this.mCancellingTask != null;
    }

    public abstract D loadInBackground();

    /* access modifiers changed from: protected */
    public boolean onCancelLoad() {
        if (this.mTask == null) {
            return false;
        }
        if (!this.mStarted) {
            this.mContentChanged = true;
        }
        if (this.mCancellingTask != null) {
            if (this.mTask.f3004k) {
                this.mTask.f3004k = false;
                this.mHandler.removeCallbacks(this.mTask);
            }
            this.mTask = null;
            return false;
        } else if (this.mTask.f3004k) {
            this.mTask.f3004k = false;
            this.mHandler.removeCallbacks(this.mTask);
            this.mTask = null;
            return false;
        } else {
            boolean a2 = this.mTask.a(false);
            if (a2) {
                this.mCancellingTask = this.mTask;
                cancelLoadInBackground();
            }
            this.mTask = null;
            return a2;
        }
    }

    public void onCanceled(D d2) {
    }

    /* access modifiers changed from: protected */
    public void onForceLoad() {
        super.onForceLoad();
        cancelLoad();
        this.mTask = new C0059a();
        executePendingTask();
    }

    /* access modifiers changed from: protected */
    public D onLoadInBackground() {
        return loadInBackground();
    }

    public void setUpdateThrottle(long j2) {
        this.mUpdateThrottle = j2;
        if (j2 != 0) {
            this.mHandler = new Handler();
        }
    }

    public void waitForLoader() {
        a<D>.a aVar = this.mTask;
        if (aVar != null) {
            aVar.d();
        }
    }

    private a(Context context, Executor executor) {
        super(context);
        this.mLastLoadCompleteTime = -10000;
        this.mExecutor = executor;
    }
}
