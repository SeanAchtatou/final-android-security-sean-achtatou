package b.j.b;

import android.os.Binder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ModernAsyncTask */
abstract class c<Params, Progress, Result> {

    /* renamed from: f  reason: collision with root package name */
    private static final ThreadFactory f3005f = new a();

    /* renamed from: g  reason: collision with root package name */
    private static final BlockingQueue<Runnable> f3006g = new LinkedBlockingQueue(10);

    /* renamed from: h  reason: collision with root package name */
    public static final Executor f3007h = new ThreadPoolExecutor(5, 128, 1, TimeUnit.SECONDS, f3006g, f3005f);

    /* renamed from: i  reason: collision with root package name */
    private static f f3008i;

    /* renamed from: a  reason: collision with root package name */
    private final h<Params, Result> f3009a = new b();

    /* renamed from: b  reason: collision with root package name */
    private final FutureTask<Result> f3010b = new C0061c(this.f3009a);

    /* renamed from: c  reason: collision with root package name */
    private volatile g f3011c = g.PENDING;

    /* renamed from: d  reason: collision with root package name */
    final AtomicBoolean f3012d = new AtomicBoolean();

    /* renamed from: e  reason: collision with root package name */
    final AtomicBoolean f3013e = new AtomicBoolean();

    /* compiled from: ModernAsyncTask */
    static class a implements ThreadFactory {

        /* renamed from: a  reason: collision with root package name */
        private final AtomicInteger f3014a = new AtomicInteger(1);

        a() {
        }

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "ModernAsyncTask #" + this.f3014a.getAndIncrement());
        }
    }

    /* compiled from: ModernAsyncTask */
    class b extends h<Params, Result> {
        b() {
        }

        public Result call() throws Exception {
            c.this.f3013e.set(true);
            Result result = null;
            try {
                Process.setThreadPriority(10);
                result = c.this.a((Object[]) this.f3024a);
                Binder.flushPendingCommands();
                c.this.d(result);
                return result;
            } catch (Throwable th) {
                c.this.d(result);
                throw th;
            }
        }
    }

    /* renamed from: b.j.b.c$c  reason: collision with other inner class name */
    /* compiled from: ModernAsyncTask */
    class C0061c extends FutureTask<Result> {
        C0061c(Callable callable) {
            super(callable);
        }

        /* access modifiers changed from: protected */
        public void done() {
            try {
                c.this.e(get());
            } catch (InterruptedException e2) {
                Log.w("AsyncTask", e2);
            } catch (ExecutionException e3) {
                throw new RuntimeException("An error occurred while executing doInBackground()", e3.getCause());
            } catch (CancellationException unused) {
                c.this.e(null);
            } catch (Throwable th) {
                throw new RuntimeException("An error occurred while executing doInBackground()", th);
            }
        }
    }

    /* compiled from: ModernAsyncTask */
    static /* synthetic */ class d {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f3017a = new int[g.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                b.j.b.c$g[] r0 = b.j.b.c.g.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                b.j.b.c.d.f3017a = r0
                int[] r0 = b.j.b.c.d.f3017a     // Catch:{ NoSuchFieldError -> 0x0014 }
                b.j.b.c$g r1 = b.j.b.c.g.RUNNING     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = b.j.b.c.d.f3017a     // Catch:{ NoSuchFieldError -> 0x001f }
                b.j.b.c$g r1 = b.j.b.c.g.FINISHED     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.j.b.c.d.<clinit>():void");
        }
    }

    /* compiled from: ModernAsyncTask */
    private static class e<Data> {

        /* renamed from: a  reason: collision with root package name */
        final c f3018a;

        /* renamed from: b  reason: collision with root package name */
        final Data[] f3019b;

        e(c cVar, Data... dataArr) {
            this.f3018a = cVar;
            this.f3019b = dataArr;
        }
    }

    /* compiled from: ModernAsyncTask */
    private static class f extends Handler {
        f() {
            super(Looper.getMainLooper());
        }

        public void handleMessage(Message message) {
            e eVar = (e) message.obj;
            int i2 = message.what;
            if (i2 == 1) {
                eVar.f3018a.a(eVar.f3019b[0]);
            } else if (i2 == 2) {
                eVar.f3018a.b((Object[]) eVar.f3019b);
            }
        }
    }

    /* compiled from: ModernAsyncTask */
    public enum g {
        PENDING,
        RUNNING,
        FINISHED
    }

    /* compiled from: ModernAsyncTask */
    private static abstract class h<Params, Result> implements Callable<Result> {

        /* renamed from: a  reason: collision with root package name */
        Params[] f3024a;

        h() {
        }
    }

    c() {
    }

    private static Handler d() {
        f fVar;
        synchronized (c.class) {
            if (f3008i == null) {
                f3008i = new f();
            }
            fVar = f3008i;
        }
        return fVar;
    }

    /* access modifiers changed from: protected */
    public abstract Result a(Object... objArr);

    public final boolean a() {
        return this.f3012d.get();
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    /* access modifiers changed from: protected */
    public void b(Result result) {
        b();
    }

    /* access modifiers changed from: protected */
    public void b(Progress... progressArr) {
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* access modifiers changed from: protected */
    public void c(Result result) {
    }

    /* access modifiers changed from: package-private */
    public void e(Result result) {
        if (!this.f3013e.get()) {
            d(result);
        }
    }

    public final boolean a(boolean z) {
        this.f3012d.set(true);
        return this.f3010b.cancel(z);
    }

    public final c<Params, Progress, Result> a(Executor executor, Params... paramsArr) {
        if (this.f3011c != g.PENDING) {
            int i2 = d.f3017a[this.f3011c.ordinal()];
            if (i2 == 1) {
                throw new IllegalStateException("Cannot execute task: the task is already running.");
            } else if (i2 != 2) {
                throw new IllegalStateException("We should never reach this state");
            } else {
                throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        } else {
            this.f3011c = g.RUNNING;
            c();
            this.f3009a.f3024a = paramsArr;
            executor.execute(this.f3010b);
            return this;
        }
    }

    /* access modifiers changed from: package-private */
    public Result d(Result result) {
        d().obtainMessage(1, new e(this, result)).sendToTarget();
        return result;
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj) {
        if (a()) {
            b(obj);
        } else {
            c(obj);
        }
        this.f3011c = g.FINISHED;
    }
}
