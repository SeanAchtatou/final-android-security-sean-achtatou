package b.e.l;

/* compiled from: Pools */
public class e<T> implements d<T> {

    /* renamed from: a  reason: collision with root package name */
    private final Object[] f2881a;

    /* renamed from: b  reason: collision with root package name */
    private int f2882b;

    public e(int i2) {
        if (i2 > 0) {
            this.f2881a = new Object[i2];
            return;
        }
        throw new IllegalArgumentException("The max pool size must be > 0");
    }

    private boolean b(T t) {
        for (int i2 = 0; i2 < this.f2882b; i2++) {
            if (this.f2881a[i2] == t) {
                return true;
            }
        }
        return false;
    }

    public T a() {
        int i2 = this.f2882b;
        if (i2 <= 0) {
            return null;
        }
        int i3 = i2 - 1;
        T[] tArr = this.f2881a;
        T t = tArr[i3];
        tArr[i3] = null;
        this.f2882b = i2 - 1;
        return t;
    }

    public boolean a(T t) {
        if (!b(t)) {
            int i2 = this.f2882b;
            Object[] objArr = this.f2881a;
            if (i2 >= objArr.length) {
                return false;
            }
            objArr[i2] = t;
            this.f2882b = i2 + 1;
            return true;
        }
        throw new IllegalStateException("Already in the pool!");
    }
}
