package b.e.l;

import android.util.Log;
import java.io.Writer;

/* compiled from: LogWriter */
public class b extends Writer {

    /* renamed from: a  reason: collision with root package name */
    private final String f2879a;

    /* renamed from: b  reason: collision with root package name */
    private StringBuilder f2880b = new StringBuilder(128);

    public b(String str) {
        this.f2879a = str;
    }

    private void d() {
        if (this.f2880b.length() > 0) {
            Log.d(this.f2879a, this.f2880b.toString());
            StringBuilder sb = this.f2880b;
            sb.delete(0, sb.length());
        }
    }

    public void close() {
        d();
    }

    public void flush() {
        d();
    }

    public void write(char[] cArr, int i2, int i3) {
        for (int i4 = 0; i4 < i3; i4++) {
            char c2 = cArr[i2 + i4];
            if (c2 == 10) {
                d();
            } else {
                this.f2880b.append(c2);
            }
        }
    }
}
