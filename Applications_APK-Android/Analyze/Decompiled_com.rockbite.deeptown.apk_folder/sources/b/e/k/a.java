package b.e.k;

import android.os.Build;
import android.text.PrecomputedText;
import android.text.Spannable;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.MetricAffectingSpan;
import b.e.l.c;

/* compiled from: PrecomputedTextCompat */
public class a implements Spannable {

    /* renamed from: a  reason: collision with root package name */
    private final Spannable f2868a;

    /* renamed from: b  reason: collision with root package name */
    private final C0049a f2869b;

    /* renamed from: b.e.k.a$a  reason: collision with other inner class name */
    /* compiled from: PrecomputedTextCompat */
    public static final class C0049a {

        /* renamed from: a  reason: collision with root package name */
        private final TextPaint f2870a;

        /* renamed from: b  reason: collision with root package name */
        private final TextDirectionHeuristic f2871b;

        /* renamed from: c  reason: collision with root package name */
        private final int f2872c;

        /* renamed from: d  reason: collision with root package name */
        private final int f2873d;

        /* renamed from: e  reason: collision with root package name */
        final PrecomputedText.Params f2874e = null;

        /* renamed from: b.e.k.a$a$a  reason: collision with other inner class name */
        /* compiled from: PrecomputedTextCompat */
        public static class C0050a {

            /* renamed from: a  reason: collision with root package name */
            private final TextPaint f2875a;

            /* renamed from: b  reason: collision with root package name */
            private TextDirectionHeuristic f2876b;

            /* renamed from: c  reason: collision with root package name */
            private int f2877c;

            /* renamed from: d  reason: collision with root package name */
            private int f2878d;

            public C0050a(TextPaint textPaint) {
                this.f2875a = textPaint;
                if (Build.VERSION.SDK_INT >= 23) {
                    this.f2877c = 1;
                    this.f2878d = 1;
                } else {
                    this.f2878d = 0;
                    this.f2877c = 0;
                }
                if (Build.VERSION.SDK_INT >= 18) {
                    this.f2876b = TextDirectionHeuristics.FIRSTSTRONG_LTR;
                } else {
                    this.f2876b = null;
                }
            }

            public C0050a a(int i2) {
                this.f2877c = i2;
                return this;
            }

            public C0050a b(int i2) {
                this.f2878d = i2;
                return this;
            }

            public C0050a a(TextDirectionHeuristic textDirectionHeuristic) {
                this.f2876b = textDirectionHeuristic;
                return this;
            }

            public C0049a a() {
                return new C0049a(this.f2875a, this.f2876b, this.f2877c, this.f2878d);
            }
        }

        C0049a(TextPaint textPaint, TextDirectionHeuristic textDirectionHeuristic, int i2, int i3) {
            this.f2870a = textPaint;
            this.f2871b = textDirectionHeuristic;
            this.f2872c = i2;
            this.f2873d = i3;
        }

        public int a() {
            return this.f2872c;
        }

        public int b() {
            return this.f2873d;
        }

        public TextDirectionHeuristic c() {
            return this.f2871b;
        }

        public TextPaint d() {
            return this.f2870a;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof C0049a)) {
                return false;
            }
            C0049a aVar = (C0049a) obj;
            if (!a(aVar)) {
                return false;
            }
            return Build.VERSION.SDK_INT < 18 || this.f2871b == aVar.c();
        }

        public int hashCode() {
            int i2 = Build.VERSION.SDK_INT;
            if (i2 >= 24) {
                return c.a(Float.valueOf(this.f2870a.getTextSize()), Float.valueOf(this.f2870a.getTextScaleX()), Float.valueOf(this.f2870a.getTextSkewX()), Float.valueOf(this.f2870a.getLetterSpacing()), Integer.valueOf(this.f2870a.getFlags()), this.f2870a.getTextLocales(), this.f2870a.getTypeface(), Boolean.valueOf(this.f2870a.isElegantTextHeight()), this.f2871b, Integer.valueOf(this.f2872c), Integer.valueOf(this.f2873d));
            } else if (i2 >= 21) {
                return c.a(Float.valueOf(this.f2870a.getTextSize()), Float.valueOf(this.f2870a.getTextScaleX()), Float.valueOf(this.f2870a.getTextSkewX()), Float.valueOf(this.f2870a.getLetterSpacing()), Integer.valueOf(this.f2870a.getFlags()), this.f2870a.getTextLocale(), this.f2870a.getTypeface(), Boolean.valueOf(this.f2870a.isElegantTextHeight()), this.f2871b, Integer.valueOf(this.f2872c), Integer.valueOf(this.f2873d));
            } else if (i2 >= 18) {
                return c.a(Float.valueOf(this.f2870a.getTextSize()), Float.valueOf(this.f2870a.getTextScaleX()), Float.valueOf(this.f2870a.getTextSkewX()), Integer.valueOf(this.f2870a.getFlags()), this.f2870a.getTextLocale(), this.f2870a.getTypeface(), this.f2871b, Integer.valueOf(this.f2872c), Integer.valueOf(this.f2873d));
            } else if (i2 >= 17) {
                return c.a(Float.valueOf(this.f2870a.getTextSize()), Float.valueOf(this.f2870a.getTextScaleX()), Float.valueOf(this.f2870a.getTextSkewX()), Integer.valueOf(this.f2870a.getFlags()), this.f2870a.getTextLocale(), this.f2870a.getTypeface(), this.f2871b, Integer.valueOf(this.f2872c), Integer.valueOf(this.f2873d));
            } else {
                return c.a(Float.valueOf(this.f2870a.getTextSize()), Float.valueOf(this.f2870a.getTextScaleX()), Float.valueOf(this.f2870a.getTextSkewX()), Integer.valueOf(this.f2870a.getFlags()), this.f2870a.getTypeface(), this.f2871b, Integer.valueOf(this.f2872c), Integer.valueOf(this.f2873d));
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("{");
            sb.append("textSize=" + this.f2870a.getTextSize());
            sb.append(", textScaleX=" + this.f2870a.getTextScaleX());
            sb.append(", textSkewX=" + this.f2870a.getTextSkewX());
            if (Build.VERSION.SDK_INT >= 21) {
                sb.append(", letterSpacing=" + this.f2870a.getLetterSpacing());
                sb.append(", elegantTextHeight=" + this.f2870a.isElegantTextHeight());
            }
            int i2 = Build.VERSION.SDK_INT;
            if (i2 >= 24) {
                sb.append(", textLocale=" + this.f2870a.getTextLocales());
            } else if (i2 >= 17) {
                sb.append(", textLocale=" + this.f2870a.getTextLocale());
            }
            sb.append(", typeface=" + this.f2870a.getTypeface());
            if (Build.VERSION.SDK_INT >= 26) {
                sb.append(", variationSettings=" + this.f2870a.getFontVariationSettings());
            }
            sb.append(", textDir=" + this.f2871b);
            sb.append(", breakStrategy=" + this.f2872c);
            sb.append(", hyphenationFrequency=" + this.f2873d);
            sb.append("}");
            return sb.toString();
        }

        public boolean a(C0049a aVar) {
            PrecomputedText.Params params = this.f2874e;
            if (params != null) {
                return params.equals(aVar.f2874e);
            }
            if ((Build.VERSION.SDK_INT >= 23 && (this.f2872c != aVar.a() || this.f2873d != aVar.b())) || this.f2870a.getTextSize() != aVar.d().getTextSize() || this.f2870a.getTextScaleX() != aVar.d().getTextScaleX() || this.f2870a.getTextSkewX() != aVar.d().getTextSkewX()) {
                return false;
            }
            if ((Build.VERSION.SDK_INT >= 21 && (this.f2870a.getLetterSpacing() != aVar.d().getLetterSpacing() || !TextUtils.equals(this.f2870a.getFontFeatureSettings(), aVar.d().getFontFeatureSettings()))) || this.f2870a.getFlags() != aVar.d().getFlags()) {
                return false;
            }
            int i2 = Build.VERSION.SDK_INT;
            if (i2 >= 24) {
                if (!this.f2870a.getTextLocales().equals(aVar.d().getTextLocales())) {
                    return false;
                }
            } else if (i2 >= 17 && !this.f2870a.getTextLocale().equals(aVar.d().getTextLocale())) {
                return false;
            }
            if (this.f2870a.getTypeface() == null) {
                if (aVar.d().getTypeface() != null) {
                    return false;
                }
                return true;
            } else if (!this.f2870a.getTypeface().equals(aVar.d().getTypeface())) {
                return false;
            } else {
                return true;
            }
        }

        public C0049a(PrecomputedText.Params params) {
            this.f2870a = params.getTextPaint();
            this.f2871b = params.getTextDirection();
            this.f2872c = params.getBreakStrategy();
            this.f2873d = params.getHyphenationFrequency();
        }
    }

    public C0049a a() {
        return this.f2869b;
    }

    public char charAt(int i2) {
        return this.f2868a.charAt(i2);
    }

    public int getSpanEnd(Object obj) {
        return this.f2868a.getSpanEnd(obj);
    }

    public int getSpanFlags(Object obj) {
        return this.f2868a.getSpanFlags(obj);
    }

    public int getSpanStart(Object obj) {
        return this.f2868a.getSpanStart(obj);
    }

    public <T> T[] getSpans(int i2, int i3, Class<T> cls) {
        return this.f2868a.getSpans(i2, i3, cls);
    }

    public int length() {
        return this.f2868a.length();
    }

    public int nextSpanTransition(int i2, int i3, Class cls) {
        return this.f2868a.nextSpanTransition(i2, i3, cls);
    }

    public void removeSpan(Object obj) {
        if (!(obj instanceof MetricAffectingSpan)) {
            this.f2868a.removeSpan(obj);
            return;
        }
        throw new IllegalArgumentException("MetricAffectingSpan can not be removed from PrecomputedText.");
    }

    public void setSpan(Object obj, int i2, int i3, int i4) {
        if (!(obj instanceof MetricAffectingSpan)) {
            this.f2868a.setSpan(obj, i2, i3, i4);
            return;
        }
        throw new IllegalArgumentException("MetricAffectingSpan can not be set to PrecomputedText.");
    }

    public CharSequence subSequence(int i2, int i3) {
        return this.f2868a.subSequence(i2, i3);
    }

    public String toString() {
        return this.f2868a.toString();
    }
}
