package b.e.e.c;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.LinearGradient;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import com.esotericsoftware.spine.Animation;
import java.io.IOException;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: GradientColorInflaterCompat */
final class d {
    static Shader a(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws IOException, XmlPullParserException {
        XmlPullParser xmlPullParser2 = xmlPullParser;
        String name = xmlPullParser.getName();
        if (name.equals("gradient")) {
            Resources.Theme theme2 = theme;
            TypedArray a2 = g.a(resources, theme2, attributeSet, b.e.d.GradientColor);
            float a3 = g.a(a2, xmlPullParser2, "startX", b.e.d.GradientColor_android_startX, (float) Animation.CurveTimeline.LINEAR);
            float a4 = g.a(a2, xmlPullParser2, "startY", b.e.d.GradientColor_android_startY, (float) Animation.CurveTimeline.LINEAR);
            float a5 = g.a(a2, xmlPullParser2, "endX", b.e.d.GradientColor_android_endX, (float) Animation.CurveTimeline.LINEAR);
            float a6 = g.a(a2, xmlPullParser2, "endY", b.e.d.GradientColor_android_endY, (float) Animation.CurveTimeline.LINEAR);
            float a7 = g.a(a2, xmlPullParser2, "centerX", b.e.d.GradientColor_android_centerX, (float) Animation.CurveTimeline.LINEAR);
            float a8 = g.a(a2, xmlPullParser2, "centerY", b.e.d.GradientColor_android_centerY, (float) Animation.CurveTimeline.LINEAR);
            int b2 = g.b(a2, xmlPullParser2, "type", b.e.d.GradientColor_android_type, 0);
            int a9 = g.a(a2, xmlPullParser2, "startColor", b.e.d.GradientColor_android_startColor, 0);
            boolean a10 = g.a(xmlPullParser2, "centerColor");
            int a11 = g.a(a2, xmlPullParser2, "centerColor", b.e.d.GradientColor_android_centerColor, 0);
            int a12 = g.a(a2, xmlPullParser2, "endColor", b.e.d.GradientColor_android_endColor, 0);
            int b3 = g.b(a2, xmlPullParser2, "tileMode", b.e.d.GradientColor_android_tileMode, 0);
            float f2 = a7;
            float a13 = g.a(a2, xmlPullParser2, "gradientRadius", b.e.d.GradientColor_android_gradientRadius, (float) Animation.CurveTimeline.LINEAR);
            a2.recycle();
            a a14 = a(b(resources, xmlPullParser, attributeSet, theme), a9, a12, a10, a11);
            if (b2 == 1) {
                float f3 = f2;
                if (a13 > Animation.CurveTimeline.LINEAR) {
                    int[] iArr = a14.f2795a;
                    return new RadialGradient(f3, a8, a13, iArr, a14.f2796b, a(b3));
                }
                throw new XmlPullParserException("<gradient> tag requires 'gradientRadius' attribute with radial type");
            } else if (b2 != 2) {
                return new LinearGradient(a3, a4, a5, a6, a14.f2795a, a14.f2796b, a(b3));
            } else {
                return new SweepGradient(f2, a8, a14.f2795a, a14.f2796b);
            }
        } else {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid gradient color tag " + name);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0084, code lost:
        throw new org.xmlpull.v1.XmlPullParserException(r9.getPositionDescription() + ": <item> tag requires a 'color' attribute and a 'offset' attribute!");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static b.e.e.c.d.a b(android.content.res.Resources r8, org.xmlpull.v1.XmlPullParser r9, android.util.AttributeSet r10, android.content.res.Resources.Theme r11) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            int r0 = r9.getDepth()
            r1 = 1
            int r0 = r0 + r1
            java.util.ArrayList r2 = new java.util.ArrayList
            r3 = 20
            r2.<init>(r3)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>(r3)
        L_0x0012:
            int r3 = r9.next()
            if (r3 == r1) goto L_0x0085
            int r5 = r9.getDepth()
            if (r5 >= r0) goto L_0x0021
            r6 = 3
            if (r3 == r6) goto L_0x0085
        L_0x0021:
            r6 = 2
            if (r3 == r6) goto L_0x0025
            goto L_0x0012
        L_0x0025:
            if (r5 > r0) goto L_0x0012
            java.lang.String r3 = r9.getName()
            java.lang.String r5 = "item"
            boolean r3 = r3.equals(r5)
            if (r3 != 0) goto L_0x0034
            goto L_0x0012
        L_0x0034:
            int[] r3 = b.e.d.GradientColorItem
            android.content.res.TypedArray r3 = b.e.e.c.g.a(r8, r11, r10, r3)
            int r5 = b.e.d.GradientColorItem_android_color
            boolean r5 = r3.hasValue(r5)
            int r6 = b.e.d.GradientColorItem_android_offset
            boolean r6 = r3.hasValue(r6)
            if (r5 == 0) goto L_0x006a
            if (r6 == 0) goto L_0x006a
            int r5 = b.e.d.GradientColorItem_android_color
            r6 = 0
            int r5 = r3.getColor(r5, r6)
            int r6 = b.e.d.GradientColorItem_android_offset
            r7 = 0
            float r6 = r3.getFloat(r6, r7)
            r3.recycle()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r5)
            r4.add(r3)
            java.lang.Float r3 = java.lang.Float.valueOf(r6)
            r2.add(r3)
            goto L_0x0012
        L_0x006a:
            org.xmlpull.v1.XmlPullParserException r8 = new org.xmlpull.v1.XmlPullParserException
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r9 = r9.getPositionDescription()
            r10.append(r9)
            java.lang.String r9 = ": <item> tag requires a 'color' attribute and a 'offset' attribute!"
            r10.append(r9)
            java.lang.String r9 = r10.toString()
            r8.<init>(r9)
            throw r8
        L_0x0085:
            int r8 = r4.size()
            if (r8 <= 0) goto L_0x0091
            b.e.e.c.d$a r8 = new b.e.e.c.d$a
            r8.<init>(r4, r2)
            return r8
        L_0x0091:
            r8 = 0
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: b.e.e.c.d.b(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):b.e.e.c.d$a");
    }

    /* compiled from: GradientColorInflaterCompat */
    static final class a {

        /* renamed from: a  reason: collision with root package name */
        final int[] f2795a;

        /* renamed from: b  reason: collision with root package name */
        final float[] f2796b;

        a(List<Integer> list, List<Float> list2) {
            int size = list.size();
            this.f2795a = new int[size];
            this.f2796b = new float[size];
            for (int i2 = 0; i2 < size; i2++) {
                this.f2795a[i2] = list.get(i2).intValue();
                this.f2796b[i2] = list2.get(i2).floatValue();
            }
        }

        a(int i2, int i3) {
            this.f2795a = new int[]{i2, i3};
            this.f2796b = new float[]{Animation.CurveTimeline.LINEAR, 1.0f};
        }

        a(int i2, int i3, int i4) {
            this.f2795a = new int[]{i2, i3, i4};
            this.f2796b = new float[]{Animation.CurveTimeline.LINEAR, 0.5f, 1.0f};
        }
    }

    private static a a(a aVar, int i2, int i3, boolean z, int i4) {
        if (aVar != null) {
            return aVar;
        }
        if (z) {
            return new a(i2, i4, i3);
        }
        return new a(i2, i3);
    }

    private static Shader.TileMode a(int i2) {
        if (i2 == 1) {
            return Shader.TileMode.REPEAT;
        }
        if (i2 != 2) {
            return Shader.TileMode.CLAMP;
        }
        return Shader.TileMode.MIRROR;
    }
}
