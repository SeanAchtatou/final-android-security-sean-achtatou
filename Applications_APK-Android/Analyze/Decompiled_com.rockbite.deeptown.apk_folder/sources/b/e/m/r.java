package b.e.m;

import android.view.View;
import android.view.ViewTreeObserver;

/* compiled from: OneShotPreDrawListener */
public final class r implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final View f2917a;

    /* renamed from: b  reason: collision with root package name */
    private ViewTreeObserver f2918b;

    /* renamed from: c  reason: collision with root package name */
    private final Runnable f2919c;

    private r(View view, Runnable runnable) {
        this.f2917a = view;
        this.f2918b = view.getViewTreeObserver();
        this.f2919c = runnable;
    }

    public static r a(View view, Runnable runnable) {
        if (view == null) {
            throw new NullPointerException("view == null");
        } else if (runnable != null) {
            r rVar = new r(view, runnable);
            view.getViewTreeObserver().addOnPreDrawListener(rVar);
            view.addOnAttachStateChangeListener(rVar);
            return rVar;
        } else {
            throw new NullPointerException("runnable == null");
        }
    }

    public boolean onPreDraw() {
        a();
        this.f2919c.run();
        return true;
    }

    public void onViewAttachedToWindow(View view) {
        this.f2918b = view.getViewTreeObserver();
    }

    public void onViewDetachedFromWindow(View view) {
        a();
    }

    public void a() {
        if (this.f2918b.isAlive()) {
            this.f2918b.removeOnPreDrawListener(this);
        } else {
            this.f2917a.getViewTreeObserver().removeOnPreDrawListener(this);
        }
        this.f2917a.removeOnAttachStateChangeListener(this);
    }
}
