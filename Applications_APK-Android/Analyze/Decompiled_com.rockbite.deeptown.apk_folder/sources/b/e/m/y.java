package b.e.m;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.os.Build;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;

/* compiled from: ViewPropertyAnimatorCompat */
public final class y {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference<View> f2938a;

    /* renamed from: b  reason: collision with root package name */
    Runnable f2939b = null;

    /* renamed from: c  reason: collision with root package name */
    Runnable f2940c = null;

    /* renamed from: d  reason: collision with root package name */
    int f2941d = -1;

    /* compiled from: ViewPropertyAnimatorCompat */
    class a extends AnimatorListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ z f2942a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ View f2943b;

        a(y yVar, z zVar, View view) {
            this.f2942a = zVar;
            this.f2943b = view;
        }

        public void onAnimationCancel(Animator animator) {
            this.f2942a.a(this.f2943b);
        }

        public void onAnimationEnd(Animator animator) {
            this.f2942a.b(this.f2943b);
        }

        public void onAnimationStart(Animator animator) {
            this.f2942a.c(this.f2943b);
        }
    }

    /* compiled from: ViewPropertyAnimatorCompat */
    class b implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ b0 f2944a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ View f2945b;

        b(y yVar, b0 b0Var, View view) {
            this.f2944a = b0Var;
            this.f2945b = view;
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.f2944a.a(this.f2945b);
        }
    }

    /* compiled from: ViewPropertyAnimatorCompat */
    static class c implements z {

        /* renamed from: a  reason: collision with root package name */
        y f2946a;

        /* renamed from: b  reason: collision with root package name */
        boolean f2947b;

        c(y yVar) {
            this.f2946a = yVar;
        }

        public void a(View view) {
            Object tag = view.getTag(2113929216);
            z zVar = tag instanceof z ? (z) tag : null;
            if (zVar != null) {
                zVar.a(view);
            }
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: b.e.m.z} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void b(android.view.View r4) {
            /*
                r3 = this;
                b.e.m.y r0 = r3.f2946a
                int r0 = r0.f2941d
                r1 = -1
                r2 = 0
                if (r0 <= r1) goto L_0x000f
                r4.setLayerType(r0, r2)
                b.e.m.y r0 = r3.f2946a
                r0.f2941d = r1
            L_0x000f:
                int r0 = android.os.Build.VERSION.SDK_INT
                r1 = 16
                if (r0 >= r1) goto L_0x0019
                boolean r0 = r3.f2947b
                if (r0 != 0) goto L_0x0039
            L_0x0019:
                b.e.m.y r0 = r3.f2946a
                java.lang.Runnable r1 = r0.f2940c
                if (r1 == 0) goto L_0x0024
                r0.f2940c = r2
                r1.run()
            L_0x0024:
                r0 = 2113929216(0x7e000000, float:4.2535296E37)
                java.lang.Object r0 = r4.getTag(r0)
                boolean r1 = r0 instanceof b.e.m.z
                if (r1 == 0) goto L_0x0031
                r2 = r0
                b.e.m.z r2 = (b.e.m.z) r2
            L_0x0031:
                if (r2 == 0) goto L_0x0036
                r2.b(r4)
            L_0x0036:
                r4 = 1
                r3.f2947b = r4
            L_0x0039:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.e.m.y.c.b(android.view.View):void");
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: b.e.m.z} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void c(android.view.View r4) {
            /*
                r3 = this;
                r0 = 0
                r3.f2947b = r0
                b.e.m.y r0 = r3.f2946a
                int r0 = r0.f2941d
                r1 = 0
                r2 = -1
                if (r0 <= r2) goto L_0x000f
                r0 = 2
                r4.setLayerType(r0, r1)
            L_0x000f:
                b.e.m.y r0 = r3.f2946a
                java.lang.Runnable r2 = r0.f2939b
                if (r2 == 0) goto L_0x001a
                r0.f2939b = r1
                r2.run()
            L_0x001a:
                r0 = 2113929216(0x7e000000, float:4.2535296E37)
                java.lang.Object r0 = r4.getTag(r0)
                boolean r2 = r0 instanceof b.e.m.z
                if (r2 == 0) goto L_0x0027
                r1 = r0
                b.e.m.z r1 = (b.e.m.z) r1
            L_0x0027:
                if (r1 == 0) goto L_0x002c
                r1.c(r4)
            L_0x002c:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.e.m.y.c.c(android.view.View):void");
        }
    }

    y(View view) {
        this.f2938a = new WeakReference<>(view);
    }

    public y a(long j2) {
        View view = this.f2938a.get();
        if (view != null) {
            view.animate().setDuration(j2);
        }
        return this;
    }

    public y b(float f2) {
        View view = this.f2938a.get();
        if (view != null) {
            view.animate().translationY(f2);
        }
        return this;
    }

    public void c() {
        View view = this.f2938a.get();
        if (view != null) {
            view.animate().start();
        }
    }

    public y a(float f2) {
        View view = this.f2938a.get();
        if (view != null) {
            view.animate().alpha(f2);
        }
        return this;
    }

    public long b() {
        View view = this.f2938a.get();
        if (view != null) {
            return view.animate().getDuration();
        }
        return 0;
    }

    public y a(Interpolator interpolator) {
        View view = this.f2938a.get();
        if (view != null) {
            view.animate().setInterpolator(interpolator);
        }
        return this;
    }

    public y b(long j2) {
        View view = this.f2938a.get();
        if (view != null) {
            view.animate().setStartDelay(j2);
        }
        return this;
    }

    public void a() {
        View view = this.f2938a.get();
        if (view != null) {
            view.animate().cancel();
        }
    }

    public y a(z zVar) {
        View view = this.f2938a.get();
        if (view != null) {
            if (Build.VERSION.SDK_INT >= 16) {
                a(view, zVar);
            } else {
                view.setTag(2113929216, zVar);
                a(view, new c(this));
            }
        }
        return this;
    }

    private void a(View view, z zVar) {
        if (zVar != null) {
            view.animate().setListener(new a(this, zVar, view));
        } else {
            view.animate().setListener(null);
        }
    }

    public y a(b0 b0Var) {
        View view = this.f2938a.get();
        if (view != null && Build.VERSION.SDK_INT >= 19) {
            b bVar = null;
            if (b0Var != null) {
                bVar = new b(this, b0Var, view);
            }
            view.animate().setUpdateListener(bVar);
        }
        return this;
    }
}
