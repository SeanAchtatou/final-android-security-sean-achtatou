package b.e.m;

import android.os.Build;
import android.view.Gravity;

/* compiled from: GravityCompat */
public final class c {
    public static int a(int i2, int i3) {
        return Build.VERSION.SDK_INT >= 17 ? Gravity.getAbsoluteGravity(i2, i3) : i2 & -8388609;
    }
}
