package b.e.m;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import b.e.m.a;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ViewCompat */
public class u {

    /* renamed from: a  reason: collision with root package name */
    private static Field f2920a;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f2921b;

    /* renamed from: c  reason: collision with root package name */
    private static Field f2922c;

    /* renamed from: d  reason: collision with root package name */
    private static boolean f2923d;

    /* renamed from: e  reason: collision with root package name */
    private static WeakHashMap<View, String> f2924e;

    /* renamed from: f  reason: collision with root package name */
    private static WeakHashMap<View, y> f2925f = null;

    /* renamed from: g  reason: collision with root package name */
    private static Field f2926g;

    /* renamed from: h  reason: collision with root package name */
    private static boolean f2927h = false;

    /* compiled from: ViewCompat */
    static class a implements View.OnApplyWindowInsetsListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ q f2928a;

        a(q qVar) {
            this.f2928a = qVar;
        }

        public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
            return (WindowInsets) c0.a(this.f2928a.a(view, c0.a(windowInsets)));
        }
    }

    /* compiled from: ViewCompat */
    static class b extends f<Boolean> {
        b(int i2, Class cls, int i3) {
            super(i2, cls, i3);
        }

        /* access modifiers changed from: package-private */
        public Boolean a(View view) {
            return Boolean.valueOf(view.isScreenReaderFocusable());
        }
    }

    /* compiled from: ViewCompat */
    static class c extends f<CharSequence> {
        c(int i2, Class cls, int i3, int i4) {
            super(i2, cls, i3, i4);
        }

        /* access modifiers changed from: package-private */
        public CharSequence a(View view) {
            return view.getAccessibilityPaneTitle();
        }
    }

    /* compiled from: ViewCompat */
    static class d extends f<Boolean> {
        d(int i2, Class cls, int i3) {
            super(i2, cls, i3);
        }

        /* access modifiers changed from: package-private */
        public Boolean a(View view) {
            return Boolean.valueOf(view.isAccessibilityHeading());
        }
    }

    /* compiled from: ViewCompat */
    static abstract class f<T> {

        /* renamed from: a  reason: collision with root package name */
        private final int f2930a;

        /* renamed from: b  reason: collision with root package name */
        private final Class<T> f2931b;

        /* renamed from: c  reason: collision with root package name */
        private final int f2932c;

        f(int i2, Class<T> cls, int i3) {
            this(i2, cls, 0, i3);
        }

        private boolean a() {
            return Build.VERSION.SDK_INT >= 19;
        }

        /* access modifiers changed from: package-private */
        public abstract T a(View view);

        /* access modifiers changed from: package-private */
        public T b(View view) {
            if (b()) {
                return a(view);
            }
            if (!a()) {
                return null;
            }
            T tag = view.getTag(this.f2930a);
            if (this.f2931b.isInstance(tag)) {
                return tag;
            }
            return null;
        }

        f(int i2, Class<T> cls, int i3, int i4) {
            this.f2930a = i2;
            this.f2931b = cls;
            this.f2932c = i4;
        }

        private boolean b() {
            return Build.VERSION.SDK_INT >= this.f2932c;
        }
    }

    /* compiled from: ViewCompat */
    public interface g {
        boolean a(View view, KeyEvent keyEvent);
    }

    static {
        new AtomicInteger(1);
        int[] iArr = {b.e.c.accessibility_custom_action_0, b.e.c.accessibility_custom_action_1, b.e.c.accessibility_custom_action_2, b.e.c.accessibility_custom_action_3, b.e.c.accessibility_custom_action_4, b.e.c.accessibility_custom_action_5, b.e.c.accessibility_custom_action_6, b.e.c.accessibility_custom_action_7, b.e.c.accessibility_custom_action_8, b.e.c.accessibility_custom_action_9, b.e.c.accessibility_custom_action_10, b.e.c.accessibility_custom_action_11, b.e.c.accessibility_custom_action_12, b.e.c.accessibility_custom_action_13, b.e.c.accessibility_custom_action_14, b.e.c.accessibility_custom_action_15, b.e.c.accessibility_custom_action_16, b.e.c.accessibility_custom_action_17, b.e.c.accessibility_custom_action_18, b.e.c.accessibility_custom_action_19, b.e.c.accessibility_custom_action_20, b.e.c.accessibility_custom_action_21, b.e.c.accessibility_custom_action_22, b.e.c.accessibility_custom_action_23, b.e.c.accessibility_custom_action_24, b.e.c.accessibility_custom_action_25, b.e.c.accessibility_custom_action_26, b.e.c.accessibility_custom_action_27, b.e.c.accessibility_custom_action_28, b.e.c.accessibility_custom_action_29, b.e.c.accessibility_custom_action_30, b.e.c.accessibility_custom_action_31};
        new e();
    }

    public static void a(View view, a aVar) {
        View.AccessibilityDelegate accessibilityDelegate;
        if (aVar == null && (c(view) instanceof a.C0051a)) {
            aVar = new a();
        }
        if (aVar == null) {
            accessibilityDelegate = null;
        } else {
            accessibilityDelegate = aVar.a();
        }
        view.setAccessibilityDelegate(accessibilityDelegate);
    }

    public static a b(View view) {
        View.AccessibilityDelegate c2 = c(view);
        if (c2 == null) {
            return null;
        }
        if (c2 instanceof a.C0051a) {
            return ((a.C0051a) c2).f2888a;
        }
        return new a(c2);
    }

    public static void c(View view, int i2) {
        if (Build.VERSION.SDK_INT >= 26) {
            view.setImportantForAutofill(i2);
        }
    }

    public static int d(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return view.getAccessibilityLiveRegion();
        }
        return 0;
    }

    public static CharSequence e(View view) {
        return b().b(view);
    }

    public static ColorStateList f(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getBackgroundTintList();
        }
        if (view instanceof t) {
            return ((t) view).getSupportBackgroundTintList();
        }
        return null;
    }

    public static PorterDuff.Mode g(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getBackgroundTintMode();
        }
        if (view instanceof t) {
            return ((t) view).getSupportBackgroundTintMode();
        }
        return null;
    }

    public static Display h(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return view.getDisplay();
        }
        if (s(view)) {
            return ((WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay();
        }
        return null;
    }

    public static int i(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getImportantForAccessibility();
        }
        return 0;
    }

    @SuppressLint({"InlinedApi"})
    public static int j(View view) {
        if (Build.VERSION.SDK_INT >= 26) {
            return view.getImportantForAutofill();
        }
        return 0;
    }

    public static int k(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return view.getLayoutDirection();
        }
        return 0;
    }

    public static int l(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getMinimumHeight();
        }
        if (!f2923d) {
            try {
                f2922c = View.class.getDeclaredField("mMinHeight");
                f2922c.setAccessible(true);
            } catch (NoSuchFieldException unused) {
            }
            f2923d = true;
        }
        Field field = f2922c;
        if (field == null) {
            return 0;
        }
        try {
            return ((Integer) field.get(view)).intValue();
        } catch (Exception unused2) {
            return 0;
        }
    }

    public static int m(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getMinimumWidth();
        }
        if (!f2921b) {
            try {
                f2920a = View.class.getDeclaredField("mMinWidth");
                f2920a.setAccessible(true);
            } catch (NoSuchFieldException unused) {
            }
            f2921b = true;
        }
        Field field = f2920a;
        if (field == null) {
            return 0;
        }
        try {
            return ((Integer) field.get(view)).intValue();
        } catch (Exception unused2) {
            return 0;
        }
    }

    public static String n(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getTransitionName();
        }
        WeakHashMap<View, String> weakHashMap = f2924e;
        if (weakHashMap == null) {
            return null;
        }
        return weakHashMap.get(view);
    }

    public static int o(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getWindowSystemUiVisibility();
        }
        return 0;
    }

    public static boolean p(View view) {
        if (Build.VERSION.SDK_INT >= 15) {
            return view.hasOnClickListeners();
        }
        return false;
    }

    public static boolean q(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.hasTransientState();
        }
        return false;
    }

    public static boolean r(View view) {
        Boolean b2 = a().b(view);
        if (b2 == null) {
            return false;
        }
        return b2.booleanValue();
    }

    public static boolean s(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return view.isAttachedToWindow();
        }
        return view.getWindowToken() != null;
    }

    public static boolean t(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return view.isLaidOut();
        }
        return view.getWidth() > 0 && view.getHeight() > 0;
    }

    public static boolean u(View view) {
        Boolean b2 = c().b(view);
        if (b2 == null) {
            return false;
        }
        return b2.booleanValue();
    }

    public static void v(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postInvalidateOnAnimation();
        } else {
            view.postInvalidate();
        }
    }

    public static void w(View view) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 20) {
            view.requestApplyInsets();
        } else if (i2 >= 16) {
            view.requestFitSystemWindows();
        }
    }

    public static void x(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.stopNestedScroll();
        } else if (view instanceof k) {
            ((k) view).stopNestedScroll();
        }
    }

    /* compiled from: ViewCompat */
    static class e implements ViewTreeObserver.OnGlobalLayoutListener, View.OnAttachStateChangeListener {

        /* renamed from: a  reason: collision with root package name */
        private WeakHashMap<View, Boolean> f2929a = new WeakHashMap<>();

        e() {
        }

        private void a(View view, boolean z) {
            boolean z2 = view.getVisibility() == 0;
            if (z != z2) {
                if (z2) {
                    u.a(view, 16);
                }
                this.f2929a.put(view, Boolean.valueOf(z2));
            }
        }

        public void onGlobalLayout() {
            for (Map.Entry next : this.f2929a.entrySet()) {
                a((View) next.getKey(), ((Boolean) next.getValue()).booleanValue());
            }
        }

        public void onViewAttachedToWindow(View view) {
            a(view);
        }

        public void onViewDetachedFromWindow(View view) {
        }

        private void a(View view) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(this);
        }
    }

    /* compiled from: ViewCompat */
    static class h {

        /* renamed from: d  reason: collision with root package name */
        private static final ArrayList<WeakReference<View>> f2933d = new ArrayList<>();

        /* renamed from: a  reason: collision with root package name */
        private WeakHashMap<View, Boolean> f2934a = null;

        /* renamed from: b  reason: collision with root package name */
        private SparseArray<WeakReference<View>> f2935b = null;

        /* renamed from: c  reason: collision with root package name */
        private WeakReference<KeyEvent> f2936c = null;

        h() {
        }

        private SparseArray<WeakReference<View>> a() {
            if (this.f2935b == null) {
                this.f2935b = new SparseArray<>();
            }
            return this.f2935b;
        }

        private View b(View view, KeyEvent keyEvent) {
            WeakHashMap<View, Boolean> weakHashMap = this.f2934a;
            if (weakHashMap != null && weakHashMap.containsKey(view)) {
                if (view instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) view;
                    for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                        View b2 = b(viewGroup.getChildAt(childCount), keyEvent);
                        if (b2 != null) {
                            return b2;
                        }
                    }
                }
                if (c(view, keyEvent)) {
                    return view;
                }
            }
            return null;
        }

        private boolean c(View view, KeyEvent keyEvent) {
            ArrayList arrayList = (ArrayList) view.getTag(b.e.c.tag_unhandled_key_listeners);
            if (arrayList == null) {
                return false;
            }
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                if (((g) arrayList.get(size)).a(view, keyEvent)) {
                    return true;
                }
            }
            return false;
        }

        static h a(View view) {
            h hVar = (h) view.getTag(b.e.c.tag_unhandled_key_event_manager);
            if (hVar != null) {
                return hVar;
            }
            h hVar2 = new h();
            view.setTag(b.e.c.tag_unhandled_key_event_manager, hVar2);
            return hVar2;
        }

        private void b() {
            WeakHashMap<View, Boolean> weakHashMap = this.f2934a;
            if (weakHashMap != null) {
                weakHashMap.clear();
            }
            if (!f2933d.isEmpty()) {
                synchronized (f2933d) {
                    if (this.f2934a == null) {
                        this.f2934a = new WeakHashMap<>();
                    }
                    for (int size = f2933d.size() - 1; size >= 0; size--) {
                        View view = (View) f2933d.get(size).get();
                        if (view == null) {
                            f2933d.remove(size);
                        } else {
                            this.f2934a.put(view, Boolean.TRUE);
                            for (ViewParent parent = view.getParent(); parent instanceof View; parent = parent.getParent()) {
                                this.f2934a.put((View) parent, Boolean.TRUE);
                            }
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(View view, KeyEvent keyEvent) {
            if (keyEvent.getAction() == 0) {
                b();
            }
            View b2 = b(view, keyEvent);
            if (keyEvent.getAction() == 0) {
                int keyCode = keyEvent.getKeyCode();
                if (b2 != null && !KeyEvent.isModifierKey(keyCode)) {
                    a().put(keyCode, new WeakReference(b2));
                }
            }
            return b2 != null;
        }

        /* access modifiers changed from: package-private */
        public boolean a(KeyEvent keyEvent) {
            int indexOfKey;
            WeakReference<KeyEvent> weakReference = this.f2936c;
            if (weakReference != null && weakReference.get() == keyEvent) {
                return false;
            }
            this.f2936c = new WeakReference<>(keyEvent);
            WeakReference weakReference2 = null;
            SparseArray<WeakReference<View>> a2 = a();
            if (keyEvent.getAction() == 1 && (indexOfKey = a2.indexOfKey(keyEvent.getKeyCode())) >= 0) {
                weakReference2 = a2.valueAt(indexOfKey);
                a2.removeAt(indexOfKey);
            }
            if (weakReference2 == null) {
                weakReference2 = a2.get(keyEvent.getKeyCode());
            }
            if (weakReference2 == null) {
                return false;
            }
            View view = (View) weakReference2.get();
            if (view != null && u.s(view)) {
                c(view, keyEvent);
            }
            return true;
        }
    }

    private static View.AccessibilityDelegate c(View view) {
        if (f2927h) {
            return null;
        }
        if (f2926g == null) {
            try {
                f2926g = View.class.getDeclaredField("mAccessibilityDelegate");
                f2926g.setAccessible(true);
            } catch (Throwable unused) {
                f2927h = true;
                return null;
            }
        }
        try {
            Object obj = f2926g.get(view);
            if (obj instanceof View.AccessibilityDelegate) {
                return (View.AccessibilityDelegate) obj;
            }
            return null;
        } catch (Throwable unused2) {
            f2927h = true;
            return null;
        }
    }

    public static void a(View view, Runnable runnable) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postOnAnimation(runnable);
        } else {
            view.postDelayed(runnable, ValueAnimator.getFrameDelay());
        }
    }

    public static void b(View view, int i2) {
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 19) {
            view.setImportantForAccessibility(i2);
        } else if (i3 >= 16) {
            if (i2 == 4) {
                i2 = 2;
            }
            view.setImportantForAccessibility(i2);
        }
    }

    public static void a(View view, Runnable runnable, long j2) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postOnAnimationDelayed(runnable, j2);
        } else {
            view.postDelayed(runnable, ValueAnimator.getFrameDelay() + j2);
        }
    }

    public static c0 b(View view, c0 c0Var) {
        if (Build.VERSION.SDK_INT < 21) {
            return c0Var;
        }
        WindowInsets windowInsets = (WindowInsets) c0.a(c0Var);
        WindowInsets onApplyWindowInsets = view.onApplyWindowInsets(windowInsets);
        if (!onApplyWindowInsets.equals(windowInsets)) {
            windowInsets = new WindowInsets(onApplyWindowInsets);
        }
        return c0.a(windowInsets);
    }

    public static y a(View view) {
        if (f2925f == null) {
            f2925f = new WeakHashMap<>();
        }
        y yVar = f2925f.get(view);
        if (yVar != null) {
            return yVar;
        }
        y yVar2 = new y(view);
        f2925f.put(view, yVar2);
        return yVar2;
    }

    private static f<Boolean> c() {
        return new b(b.e.c.tag_screen_reader_focusable, Boolean.class, 28);
    }

    static boolean b(View view, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 28) {
            return false;
        }
        return h.a(view).a(keyEvent);
    }

    public static void a(View view, float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setElevation(f2);
        }
    }

    private static f<CharSequence> b() {
        return new c(b.e.c.tag_accessibility_pane_title, CharSequence.class, 8, 28);
    }

    public static void a(View view, String str) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setTransitionName(str);
            return;
        }
        if (f2924e == null) {
            f2924e = new WeakHashMap<>();
        }
        f2924e.put(view, str);
    }

    public static void a(View view, q qVar) {
        if (Build.VERSION.SDK_INT < 21) {
            return;
        }
        if (qVar == null) {
            view.setOnApplyWindowInsetsListener(null);
        } else {
            view.setOnApplyWindowInsetsListener(new a(qVar));
        }
    }

    public static c0 a(View view, c0 c0Var) {
        if (Build.VERSION.SDK_INT < 21) {
            return c0Var;
        }
        WindowInsets windowInsets = (WindowInsets) c0.a(c0Var);
        WindowInsets dispatchApplyWindowInsets = view.dispatchApplyWindowInsets(windowInsets);
        if (!dispatchApplyWindowInsets.equals(windowInsets)) {
            windowInsets = new WindowInsets(dispatchApplyWindowInsets);
        }
        return c0.a(windowInsets);
    }

    public static void a(View view, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }

    public static void a(View view, ColorStateList colorStateList) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setBackgroundTintList(colorStateList);
            if (Build.VERSION.SDK_INT == 21) {
                Drawable background = view.getBackground();
                boolean z = (view.getBackgroundTintList() == null && view.getBackgroundTintMode() == null) ? false : true;
                if (background != null && z) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        } else if (view instanceof t) {
            ((t) view).setSupportBackgroundTintList(colorStateList);
        }
    }

    public static void a(View view, PorterDuff.Mode mode) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setBackgroundTintMode(mode);
            if (Build.VERSION.SDK_INT == 21) {
                Drawable background = view.getBackground();
                boolean z = (view.getBackgroundTintList() == null && view.getBackgroundTintMode() == null) ? false : true;
                if (background != null && z) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        } else if (view instanceof t) {
            ((t) view).setSupportBackgroundTintMode(mode);
        }
    }

    public static void a(View view, int i2, int i3) {
        if (Build.VERSION.SDK_INT >= 23) {
            view.setScrollIndicators(i2, i3);
        }
    }

    static boolean a(View view, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 28) {
            return false;
        }
        return h.a(view).a(view, keyEvent);
    }

    private static f<Boolean> a() {
        return new d(b.e.c.tag_accessibility_heading, Boolean.class, 28);
    }

    static void a(View view, int i2) {
        if (((AccessibilityManager) view.getContext().getSystemService("accessibility")).isEnabled()) {
            boolean z = e(view) != null;
            if (d(view) != 0 || (z && view.getVisibility() == 0)) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain();
                obtain.setEventType(z ? 32 : 2048);
                obtain.setContentChangeTypes(i2);
                view.sendAccessibilityEventUnchecked(obtain);
            } else if (view.getParent() != null) {
                try {
                    view.getParent().notifySubtreeAccessibilityStateChanged(view, view, i2);
                } catch (AbstractMethodError e2) {
                    Log.e("ViewCompat", view.getParent().getClass().getSimpleName() + " does not fully implement ViewParent", e2);
                }
            }
        }
    }
}
