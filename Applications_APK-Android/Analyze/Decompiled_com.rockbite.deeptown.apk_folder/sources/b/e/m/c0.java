package b.e.m;

import android.os.Build;
import android.view.WindowInsets;

/* compiled from: WindowInsetsCompat */
public class c0 {

    /* renamed from: a  reason: collision with root package name */
    private final Object f2890a;

    private c0(Object obj) {
        this.f2890a = obj;
    }

    public int a() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.f2890a).getSystemWindowInsetBottom();
        }
        return 0;
    }

    public int b() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.f2890a).getSystemWindowInsetLeft();
        }
        return 0;
    }

    public int c() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.f2890a).getSystemWindowInsetRight();
        }
        return 0;
    }

    public int d() {
        if (Build.VERSION.SDK_INT >= 20) {
            return ((WindowInsets) this.f2890a).getSystemWindowInsetTop();
        }
        return 0;
    }

    public boolean e() {
        if (Build.VERSION.SDK_INT >= 21) {
            return ((WindowInsets) this.f2890a).isConsumed();
        }
        return false;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || c0.class != obj.getClass()) {
            return false;
        }
        Object obj2 = this.f2890a;
        Object obj3 = ((c0) obj).f2890a;
        if (obj2 != null) {
            return obj2.equals(obj3);
        }
        if (obj3 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        Object obj = this.f2890a;
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    public c0 a(int i2, int i3, int i4, int i5) {
        if (Build.VERSION.SDK_INT >= 20) {
            return new c0(((WindowInsets) this.f2890a).replaceSystemWindowInsets(i2, i3, i4, i5));
        }
        return null;
    }

    static c0 a(Object obj) {
        if (obj == null) {
            return null;
        }
        return new c0(obj);
    }

    static Object a(c0 c0Var) {
        if (c0Var == null) {
            return null;
        }
        return c0Var.f2890a;
    }
}
