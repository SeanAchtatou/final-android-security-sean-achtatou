package b.e.m.d0;

import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.view.View;

/* compiled from: AccessibilityClickableSpanCompat */
public final class a extends ClickableSpan {

    /* renamed from: a  reason: collision with root package name */
    private final int f2895a;

    /* renamed from: b  reason: collision with root package name */
    private final c f2896b;

    /* renamed from: c  reason: collision with root package name */
    private final int f2897c;

    public a(int i2, c cVar, int i3) {
        this.f2895a = i2;
        this.f2896b = cVar;
        this.f2897c = i3;
    }

    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", this.f2895a);
        this.f2896b.a(this.f2897c, bundle);
    }
}
