package b.e.m.d0;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.SparseArray;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import b.e.m.d0.f;
import com.facebook.internal.Utility;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.protobuf.CodedOutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AccessibilityNodeInfoCompat */
public class c {

    /* renamed from: d  reason: collision with root package name */
    private static int f2898d;

    /* renamed from: a  reason: collision with root package name */
    private final AccessibilityNodeInfo f2899a;

    /* renamed from: b  reason: collision with root package name */
    public int f2900b = -1;

    /* renamed from: c  reason: collision with root package name */
    private int f2901c = -1;

    /* compiled from: AccessibilityNodeInfoCompat */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        final Object f2902a;

        /* renamed from: b  reason: collision with root package name */
        private final Class<? extends f.a> f2903b;

        /* renamed from: c  reason: collision with root package name */
        protected final f f2904c;

        static {
            Class<f.c> cls = f.c.class;
            Class<f.b> cls2 = f.b.class;
            AccessibilityNodeInfo.AccessibilityAction accessibilityAction = null;
            new a(1, null);
            new a(2, null);
            new a(4, null);
            new a(8, null);
            new a(16, null);
            new a(32, null);
            new a(64, null);
            new a(128, null);
            new a(256, null, cls2);
            new a(AdRequest.MAX_CONTENT_URL_LENGTH, null, cls2);
            new a(1024, null, cls);
            new a(2048, null, cls);
            new a(CodedOutputStream.DEFAULT_BUFFER_SIZE, null);
            new a(Utility.DEFAULT_STREAM_BUFFER_SIZE, null);
            new a(16384, null);
            new a(32768, null);
            new a(65536, null);
            new a(MetadataChangeSet.INDEXABLE_TEXT_SIZE_LIMIT_BYTES, null, f.g.class);
            new a(262144, null);
            new a(524288, null);
            new a(1048576, null);
            new a(2097152, null, f.h.class);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_ON_SCREEN : null, 16908342, null, null, null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_TO_POSITION : null, 16908343, null, null, f.e.class);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP : null, 16908344, null, null, null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_LEFT : null, 16908345, null, null, null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN : null, 16908346, null, null, null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_RIGHT : null, 16908347, null, null, null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_CONTEXT_CLICK : null, 16908348, null, null, null);
            new a(Build.VERSION.SDK_INT >= 24 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SET_PROGRESS : null, 16908349, null, null, f.C0054f.class);
            new a(Build.VERSION.SDK_INT >= 26 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_MOVE_WINDOW : null, 16908354, null, null, f.d.class);
            new a(Build.VERSION.SDK_INT >= 28 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_TOOLTIP : null, 16908356, null, null, null);
            if (Build.VERSION.SDK_INT >= 28) {
                accessibilityAction = AccessibilityNodeInfo.AccessibilityAction.ACTION_HIDE_TOOLTIP;
            }
            new a(accessibilityAction, 16908357, null, null, null);
        }

        public a(int i2, CharSequence charSequence) {
            this(null, i2, charSequence, null, null);
        }

        public int a() {
            if (Build.VERSION.SDK_INT >= 21) {
                return ((AccessibilityNodeInfo.AccessibilityAction) this.f2902a).getId();
            }
            return 0;
        }

        private a(int i2, CharSequence charSequence, Class<? extends f.a> cls) {
            this(null, i2, charSequence, null, cls);
        }

        a(Object obj, int i2, CharSequence charSequence, f fVar, Class<? extends f.a> cls) {
            this.f2904c = fVar;
            if (Build.VERSION.SDK_INT < 21 || obj != null) {
                this.f2902a = obj;
            } else {
                this.f2902a = new AccessibilityNodeInfo.AccessibilityAction(i2, charSequence);
            }
            this.f2903b = cls;
        }

        /* JADX WARNING: Removed duplicated region for block: B:14:0x0025  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0028  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(android.view.View r5, android.os.Bundle r6) {
            /*
                r4 = this;
                b.e.m.d0.f r0 = r4.f2904c
                r1 = 0
                if (r0 == 0) goto L_0x0049
                r0 = 0
                java.lang.Class<? extends b.e.m.d0.f$a> r2 = r4.f2903b
                if (r2 == 0) goto L_0x0042
                java.lang.Class[] r3 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x0020 }
                java.lang.reflect.Constructor r2 = r2.getDeclaredConstructor(r3)     // Catch:{ Exception -> 0x0020 }
                java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0020 }
                java.lang.Object r1 = r2.newInstance(r1)     // Catch:{ Exception -> 0x0020 }
                b.e.m.d0.f$a r1 = (b.e.m.d0.f.a) r1     // Catch:{ Exception -> 0x0020 }
                r1.a(r6)     // Catch:{ Exception -> 0x001d }
                r0 = r1
                goto L_0x0042
            L_0x001d:
                r6 = move-exception
                r0 = r1
                goto L_0x0021
            L_0x0020:
                r6 = move-exception
            L_0x0021:
                java.lang.Class<? extends b.e.m.d0.f$a> r1 = r4.f2903b
                if (r1 != 0) goto L_0x0028
                java.lang.String r1 = "null"
                goto L_0x002c
            L_0x0028:
                java.lang.String r1 = r1.getName()
            L_0x002c:
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "Failed to execute command with argument class ViewCommandArgument: "
                r2.append(r3)
                r2.append(r1)
                java.lang.String r1 = r2.toString()
                java.lang.String r2 = "A11yActionCompat"
                android.util.Log.e(r2, r1, r6)
            L_0x0042:
                b.e.m.d0.f r6 = r4.f2904c
                boolean r5 = r6.a(r5, r0)
                return r5
            L_0x0049:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: b.e.m.d0.c.a.a(android.view.View, android.os.Bundle):boolean");
        }
    }

    /* compiled from: AccessibilityNodeInfoCompat */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        final Object f2905a;

        b(Object obj) {
            this.f2905a = obj;
        }

        public static b a(int i2, int i3, boolean z, int i4) {
            int i5 = Build.VERSION.SDK_INT;
            if (i5 >= 21) {
                return new b(AccessibilityNodeInfo.CollectionInfo.obtain(i2, i3, z, i4));
            }
            if (i5 >= 19) {
                return new b(AccessibilityNodeInfo.CollectionInfo.obtain(i2, i3, z));
            }
            return new b(null);
        }
    }

    /* renamed from: b.e.m.d0.c$c  reason: collision with other inner class name */
    /* compiled from: AccessibilityNodeInfoCompat */
    public static class C0053c {

        /* renamed from: a  reason: collision with root package name */
        final Object f2906a;

        C0053c(Object obj) {
            this.f2906a = obj;
        }

        public static C0053c a(int i2, int i3, int i4, int i5, boolean z, boolean z2) {
            int i6 = Build.VERSION.SDK_INT;
            if (i6 >= 21) {
                return new C0053c(AccessibilityNodeInfo.CollectionItemInfo.obtain(i2, i3, i4, i5, z, z2));
            }
            if (i6 >= 19) {
                return new C0053c(AccessibilityNodeInfo.CollectionItemInfo.obtain(i2, i3, i4, i5, z));
            }
            return new C0053c(null);
        }
    }

    private c(AccessibilityNodeInfo accessibilityNodeInfo) {
        this.f2899a = accessibilityNodeInfo;
    }

    public static c a(AccessibilityNodeInfo accessibilityNodeInfo) {
        return new c(accessibilityNodeInfo);
    }

    private static String b(int i2) {
        if (i2 == 1) {
            return "ACTION_FOCUS";
        }
        if (i2 == 2) {
            return "ACTION_CLEAR_FOCUS";
        }
        switch (i2) {
            case 4:
                return "ACTION_SELECT";
            case 8:
                return "ACTION_CLEAR_SELECTION";
            case 16:
                return "ACTION_CLICK";
            case 32:
                return "ACTION_LONG_CLICK";
            case 64:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case 128:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case 256:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case AdRequest.MAX_CONTENT_URL_LENGTH /*512*/:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case 1024:
                return "ACTION_NEXT_HTML_ELEMENT";
            case 2048:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case CodedOutputStream.DEFAULT_BUFFER_SIZE /*4096*/:
                return "ACTION_SCROLL_FORWARD";
            case Utility.DEFAULT_STREAM_BUFFER_SIZE /*8192*/:
                return "ACTION_SCROLL_BACKWARD";
            case 16384:
                return "ACTION_COPY";
            case 32768:
                return "ACTION_PASTE";
            case 65536:
                return "ACTION_CUT";
            case MetadataChangeSet.INDEXABLE_TEXT_SIZE_LIMIT_BYTES /*131072*/:
                return "ACTION_SET_SELECTION";
            default:
                return "ACTION_UNKNOWN";
        }
    }

    private void s() {
        if (Build.VERSION.SDK_INT >= 19) {
            this.f2899a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
            this.f2899a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
            this.f2899a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
            this.f2899a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
        }
    }

    private boolean t() {
        return !a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").isEmpty();
    }

    public void b(Rect rect) {
        this.f2899a.getBoundsInScreen(rect);
    }

    public void c(boolean z) {
        this.f2899a.setScrollable(z);
    }

    public Bundle d() {
        if (Build.VERSION.SDK_INT >= 19) {
            return this.f2899a.getExtras();
        }
        return new Bundle();
    }

    public CharSequence e() {
        return this.f2899a.getPackageName();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || c.class != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        AccessibilityNodeInfo accessibilityNodeInfo = this.f2899a;
        if (accessibilityNodeInfo == null) {
            if (cVar.f2899a != null) {
                return false;
            }
        } else if (!accessibilityNodeInfo.equals(cVar.f2899a)) {
            return false;
        }
        return this.f2901c == cVar.f2901c && this.f2900b == cVar.f2900b;
    }

    public CharSequence f() {
        if (!t()) {
            return this.f2899a.getText();
        }
        List<Integer> a2 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
        List<Integer> a3 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
        List<Integer> a4 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
        List<Integer> a5 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
        SpannableString spannableString = new SpannableString(TextUtils.substring(this.f2899a.getText(), 0, this.f2899a.getText().length()));
        for (int i2 = 0; i2 < a2.size(); i2++) {
            spannableString.setSpan(new a(a5.get(i2).intValue(), this, d().getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY")), a2.get(i2).intValue(), a3.get(i2).intValue(), a4.get(i2).intValue());
        }
        return spannableString;
    }

    public String g() {
        if (Build.VERSION.SDK_INT >= 18) {
            return this.f2899a.getViewIdResourceName();
        }
        return null;
    }

    public boolean h() {
        return this.f2899a.isCheckable();
    }

    public int hashCode() {
        AccessibilityNodeInfo accessibilityNodeInfo = this.f2899a;
        if (accessibilityNodeInfo == null) {
            return 0;
        }
        return accessibilityNodeInfo.hashCode();
    }

    public boolean i() {
        return this.f2899a.isChecked();
    }

    public boolean j() {
        return this.f2899a.isClickable();
    }

    public boolean k() {
        return this.f2899a.isEnabled();
    }

    public boolean l() {
        return this.f2899a.isFocusable();
    }

    public boolean m() {
        return this.f2899a.isFocused();
    }

    public boolean n() {
        return this.f2899a.isLongClickable();
    }

    public boolean o() {
        return this.f2899a.isPassword();
    }

    public boolean p() {
        return this.f2899a.isScrollable();
    }

    public boolean q() {
        return this.f2899a.isSelected();
    }

    public AccessibilityNodeInfo r() {
        return this.f2899a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        Rect rect = new Rect();
        a(rect);
        sb.append("; boundsInParent: " + rect);
        b(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ");
        sb.append(e());
        sb.append("; className: ");
        sb.append(b());
        sb.append("; text: ");
        sb.append(f());
        sb.append("; contentDescription: ");
        sb.append(c());
        sb.append("; viewId: ");
        sb.append(g());
        sb.append("; checkable: ");
        sb.append(h());
        sb.append("; checked: ");
        sb.append(i());
        sb.append("; focusable: ");
        sb.append(l());
        sb.append("; focused: ");
        sb.append(m());
        sb.append("; selected: ");
        sb.append(q());
        sb.append("; clickable: ");
        sb.append(j());
        sb.append("; longClickable: ");
        sb.append(n());
        sb.append("; enabled: ");
        sb.append(k());
        sb.append("; password: ");
        sb.append(o());
        sb.append("; scrollable: " + p());
        sb.append("; [");
        int a2 = a();
        while (a2 != 0) {
            int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(a2);
            a2 &= numberOfTrailingZeros ^ -1;
            sb.append(b(numberOfTrailingZeros));
            if (a2 != 0) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public static ClickableSpan[] c(CharSequence charSequence) {
        if (charSequence instanceof Spanned) {
            return (ClickableSpan[]) ((Spanned) charSequence).getSpans(0, charSequence.length(), ClickableSpan.class);
        }
        return null;
    }

    public int a() {
        return this.f2899a.getActions();
    }

    public CharSequence b() {
        return this.f2899a.getClassName();
    }

    private SparseArray<WeakReference<ClickableSpan>> b(View view) {
        return (SparseArray) view.getTag(b.e.c.tag_accessibility_clickable_spans);
    }

    public void a(int i2) {
        this.f2899a.addAction(i2);
    }

    private List<Integer> a(String str) {
        if (Build.VERSION.SDK_INT < 19) {
            return new ArrayList();
        }
        ArrayList<Integer> integerArrayList = this.f2899a.getExtras().getIntegerArrayList(str);
        if (integerArrayList != null) {
            return integerArrayList;
        }
        ArrayList arrayList = new ArrayList();
        this.f2899a.getExtras().putIntegerArrayList(str, arrayList);
        return arrayList;
    }

    public void b(Object obj) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.f2899a.setCollectionItemInfo(obj == null ? null : (AccessibilityNodeInfo.CollectionItemInfo) ((C0053c) obj).f2906a);
        }
    }

    private void c(View view) {
        SparseArray<WeakReference<ClickableSpan>> b2 = b(view);
        if (b2 != null) {
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < b2.size(); i2++) {
                if (b2.valueAt(i2).get() == null) {
                    arrayList.add(Integer.valueOf(i2));
                }
            }
            for (int i3 = 0; i3 < arrayList.size(); i3++) {
                b2.remove(((Integer) arrayList.get(i3)).intValue());
            }
        }
    }

    public void b(CharSequence charSequence) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 28) {
            this.f2899a.setPaneTitle(charSequence);
        } else if (i2 >= 19) {
            this.f2899a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.PANE_TITLE_KEY", charSequence);
        }
    }

    public void b(boolean z) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.f2899a.setScreenReaderFocusable(z);
        } else {
            a(1, z);
        }
    }

    public void a(a aVar) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.f2899a.addAction((AccessibilityNodeInfo.AccessibilityAction) aVar.f2902a);
        }
    }

    public boolean a(int i2, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.f2899a.performAction(i2, bundle);
        }
        return false;
    }

    public CharSequence c() {
        return this.f2899a.getContentDescription();
    }

    public void a(Rect rect) {
        this.f2899a.getBoundsInParent(rect);
    }

    public void a(CharSequence charSequence) {
        this.f2899a.setClassName(charSequence);
    }

    public void a(CharSequence charSequence, View view) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 19 && i2 < 26) {
            s();
            c(view);
            ClickableSpan[] c2 = c(charSequence);
            if (c2 != null && c2.length > 0) {
                d().putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY", b.e.c.accessibility_action_clickable_span);
                SparseArray<WeakReference<ClickableSpan>> a2 = a(view);
                int i3 = 0;
                while (c2 != null && i3 < c2.length) {
                    int a3 = a(c2[i3], a2);
                    a2.put(a3, new WeakReference(c2[i3]));
                    a(c2[i3], (Spanned) charSequence, a3);
                    i3++;
                }
            }
        }
    }

    private SparseArray<WeakReference<ClickableSpan>> a(View view) {
        SparseArray<WeakReference<ClickableSpan>> b2 = b(view);
        if (b2 != null) {
            return b2;
        }
        SparseArray<WeakReference<ClickableSpan>> sparseArray = new SparseArray<>();
        view.setTag(b.e.c.tag_accessibility_clickable_spans, sparseArray);
        return sparseArray;
    }

    private int a(ClickableSpan clickableSpan, SparseArray<WeakReference<ClickableSpan>> sparseArray) {
        if (sparseArray != null) {
            for (int i2 = 0; i2 < sparseArray.size(); i2++) {
                if (clickableSpan.equals((ClickableSpan) sparseArray.valueAt(i2).get())) {
                    return sparseArray.keyAt(i2);
                }
            }
        }
        int i3 = f2898d;
        f2898d = i3 + 1;
        return i3;
    }

    private void a(ClickableSpan clickableSpan, Spanned spanned, int i2) {
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").add(Integer.valueOf(spanned.getSpanStart(clickableSpan)));
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY").add(Integer.valueOf(spanned.getSpanEnd(clickableSpan)));
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY").add(Integer.valueOf(spanned.getSpanFlags(clickableSpan)));
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY").add(Integer.valueOf(i2));
    }

    public void a(Object obj) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.f2899a.setCollectionInfo(obj == null ? null : (AccessibilityNodeInfo.CollectionInfo) ((b) obj).f2905a);
        }
    }

    public void a(boolean z) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.f2899a.setHeading(z);
        } else {
            a(2, z);
        }
    }

    private void a(int i2, boolean z) {
        Bundle d2 = d();
        if (d2 != null) {
            int i3 = d2.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & (i2 ^ -1);
            if (!z) {
                i2 = 0;
            }
            d2.putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", i2 | i3);
        }
    }
}
