package b.e.m;

import android.view.View;
import android.view.ViewGroup;

/* compiled from: NestedScrollingParentHelper */
public class p {

    /* renamed from: a  reason: collision with root package name */
    private int f2915a;

    /* renamed from: b  reason: collision with root package name */
    private int f2916b;

    public p(ViewGroup viewGroup) {
    }

    public void a(View view, View view2, int i2) {
        a(view, view2, i2, 0);
    }

    public void a(View view, View view2, int i2, int i3) {
        if (i3 == 1) {
            this.f2916b = i2;
        } else {
            this.f2915a = i2;
        }
    }

    public int a() {
        return this.f2915a | this.f2916b;
    }

    public void a(View view, int i2) {
        if (i2 == 1) {
            this.f2916b = 0;
        } else {
            this.f2915a = 0;
        }
    }
}
