package b.e.j;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: SelfDestructiveThread */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private final Object f2849a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private HandlerThread f2850b;

    /* renamed from: c  reason: collision with root package name */
    private Handler f2851c;

    /* renamed from: d  reason: collision with root package name */
    private int f2852d;

    /* renamed from: e  reason: collision with root package name */
    private Handler.Callback f2853e = new a();

    /* renamed from: f  reason: collision with root package name */
    private final int f2854f;

    /* renamed from: g  reason: collision with root package name */
    private final int f2855g;

    /* renamed from: h  reason: collision with root package name */
    private final String f2856h;

    /* compiled from: SelfDestructiveThread */
    class a implements Handler.Callback {
        a() {
        }

        public boolean handleMessage(Message message) {
            int i2 = message.what;
            if (i2 == 0) {
                c.this.a();
                return true;
            } else if (i2 != 1) {
                return true;
            } else {
                c.this.a((Runnable) message.obj);
                return true;
            }
        }
    }

    /* compiled from: SelfDestructiveThread */
    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Callable f2858a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Handler f2859b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ d f2860c;

        /* compiled from: SelfDestructiveThread */
        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ Object f2861a;

            a(Object obj) {
                this.f2861a = obj;
            }

            public void run() {
                b.this.f2860c.a(this.f2861a);
            }
        }

        b(c cVar, Callable callable, Handler handler, d dVar) {
            this.f2858a = callable;
            this.f2859b = handler;
            this.f2860c = dVar;
        }

        public void run() {
            Object obj;
            try {
                obj = this.f2858a.call();
            } catch (Exception unused) {
                obj = null;
            }
            this.f2859b.post(new a(obj));
        }
    }

    /* renamed from: b.e.j.c$c  reason: collision with other inner class name */
    /* compiled from: SelfDestructiveThread */
    class C0048c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AtomicReference f2863a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Callable f2864b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ ReentrantLock f2865c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ AtomicBoolean f2866d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ Condition f2867e;

        C0048c(c cVar, AtomicReference atomicReference, Callable callable, ReentrantLock reentrantLock, AtomicBoolean atomicBoolean, Condition condition) {
            this.f2863a = atomicReference;
            this.f2864b = callable;
            this.f2865c = reentrantLock;
            this.f2866d = atomicBoolean;
            this.f2867e = condition;
        }

        public void run() {
            try {
                this.f2863a.set(this.f2864b.call());
            } catch (Exception unused) {
            }
            this.f2865c.lock();
            try {
                this.f2866d.set(false);
                this.f2867e.signal();
            } finally {
                this.f2865c.unlock();
            }
        }
    }

    /* compiled from: SelfDestructiveThread */
    public interface d<T> {
        void a(Object obj);
    }

    public c(String str, int i2, int i3) {
        this.f2856h = str;
        this.f2855g = i2;
        this.f2854f = i3;
        this.f2852d = 0;
    }

    private void b(Runnable runnable) {
        synchronized (this.f2849a) {
            if (this.f2850b == null) {
                this.f2850b = new HandlerThread(this.f2856h, this.f2855g);
                this.f2850b.start();
                this.f2851c = new Handler(this.f2850b.getLooper(), this.f2853e);
                this.f2852d++;
            }
            this.f2851c.removeMessages(0);
            this.f2851c.sendMessage(this.f2851c.obtainMessage(1, runnable));
        }
    }

    public <T> void a(Callable callable, d dVar) {
        b(new b(this, callable, new Handler(), dVar));
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:9|10|11|12|(4:25|14|15|16)(1:17)) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003f */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> T a(java.util.concurrent.Callable r13, int r14) throws java.lang.InterruptedException {
        /*
            r12 = this;
            java.util.concurrent.locks.ReentrantLock r7 = new java.util.concurrent.locks.ReentrantLock
            r7.<init>()
            java.util.concurrent.locks.Condition r8 = r7.newCondition()
            java.util.concurrent.atomic.AtomicReference r9 = new java.util.concurrent.atomic.AtomicReference
            r9.<init>()
            java.util.concurrent.atomic.AtomicBoolean r10 = new java.util.concurrent.atomic.AtomicBoolean
            r0 = 1
            r10.<init>(r0)
            b.e.j.c$c r11 = new b.e.j.c$c
            r0 = r11
            r1 = r12
            r2 = r9
            r3 = r13
            r4 = r7
            r5 = r10
            r6 = r8
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r12.b(r11)
            r7.lock()
            boolean r13 = r10.get()     // Catch:{ all -> 0x005c }
            if (r13 != 0) goto L_0x0034
            java.lang.Object r13 = r9.get()     // Catch:{ all -> 0x005c }
            r7.unlock()
            return r13
        L_0x0034:
            java.util.concurrent.TimeUnit r13 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x005c }
            long r0 = (long) r14     // Catch:{ all -> 0x005c }
            long r13 = r13.toNanos(r0)     // Catch:{ all -> 0x005c }
        L_0x003b:
            long r13 = r8.awaitNanos(r13)     // Catch:{ InterruptedException -> 0x003f }
        L_0x003f:
            boolean r0 = r10.get()     // Catch:{ all -> 0x005c }
            if (r0 != 0) goto L_0x004d
            java.lang.Object r13 = r9.get()     // Catch:{ all -> 0x005c }
            r7.unlock()
            return r13
        L_0x004d:
            r0 = 0
            int r2 = (r13 > r0 ? 1 : (r13 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x0054
            goto L_0x003b
        L_0x0054:
            java.lang.InterruptedException r13 = new java.lang.InterruptedException     // Catch:{ all -> 0x005c }
            java.lang.String r14 = "timeout"
            r13.<init>(r14)     // Catch:{ all -> 0x005c }
            throw r13     // Catch:{ all -> 0x005c }
        L_0x005c:
            r13 = move-exception
            r7.unlock()
            goto L_0x0062
        L_0x0061:
            throw r13
        L_0x0062:
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: b.e.j.c.a(java.util.concurrent.Callable, int):java.lang.Object");
    }

    /* access modifiers changed from: package-private */
    public void a(Runnable runnable) {
        runnable.run();
        synchronized (this.f2849a) {
            this.f2851c.removeMessages(0);
            this.f2851c.sendMessageDelayed(this.f2851c.obtainMessage(0), (long) this.f2854f);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        synchronized (this.f2849a) {
            if (!this.f2851c.hasMessages(1)) {
                this.f2850b.quit();
                this.f2850b = null;
                this.f2851c = null;
            }
        }
    }
}
