package b.e.j;

import android.util.Base64;
import b.e.l.f;
import java.util.List;

/* compiled from: FontRequest */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final String f2822a;

    /* renamed from: b  reason: collision with root package name */
    private final String f2823b;

    /* renamed from: c  reason: collision with root package name */
    private final String f2824c;

    /* renamed from: d  reason: collision with root package name */
    private final List<List<byte[]>> f2825d;

    /* renamed from: e  reason: collision with root package name */
    private final int f2826e = 0;

    /* renamed from: f  reason: collision with root package name */
    private final String f2827f = (this.f2822a + "-" + this.f2823b + "-" + this.f2824c);

    public a(String str, String str2, String str3, List<List<byte[]>> list) {
        f.a(str);
        this.f2822a = str;
        f.a(str2);
        this.f2823b = str2;
        f.a(str3);
        this.f2824c = str3;
        f.a(list);
        this.f2825d = list;
    }

    public List<List<byte[]>> a() {
        return this.f2825d;
    }

    public int b() {
        return this.f2826e;
    }

    public String c() {
        return this.f2827f;
    }

    public String d() {
        return this.f2822a;
    }

    public String e() {
        return this.f2823b;
    }

    public String f() {
        return this.f2824c;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("FontRequest {mProviderAuthority: " + this.f2822a + ", mProviderPackage: " + this.f2823b + ", mQuery: " + this.f2824c + ", mCertificates:");
        for (int i2 = 0; i2 < this.f2825d.size(); i2++) {
            sb.append(" [");
            List list = this.f2825d.get(i2);
            for (int i3 = 0; i3 < list.size(); i3++) {
                sb.append(" \"");
                sb.append(Base64.encodeToString((byte[]) list.get(i3), 0));
                sb.append("\"");
            }
            sb.append(" ]");
        }
        sb.append("}");
        sb.append("mCertificatesArray: " + this.f2826e);
        return sb.toString();
    }
}
