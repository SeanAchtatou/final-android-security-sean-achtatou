package b.e.i;

import android.os.Build;

/* compiled from: BuildCompat */
public class a {
    public static boolean a() {
        if (Build.VERSION.CODENAME.length() != 1 || Build.VERSION.CODENAME.charAt(0) < 'Q' || Build.VERSION.CODENAME.charAt(0) > 'Z') {
            return false;
        }
        return true;
    }
}
