package b.e.i;

import android.os.Build;
import android.os.Trace;

/* compiled from: TraceCompat */
public final class c {
    public static void a(String str) {
        if (Build.VERSION.SDK_INT >= 18) {
            Trace.beginSection(str);
        }
    }

    public static void a() {
        if (Build.VERSION.SDK_INT >= 18) {
            Trace.endSection();
        }
    }
}
