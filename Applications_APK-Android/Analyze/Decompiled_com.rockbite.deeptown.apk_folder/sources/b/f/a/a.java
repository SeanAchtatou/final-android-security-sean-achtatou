package b.f.a;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import b.f.a.b;

/* compiled from: CursorAdapter */
public abstract class a extends BaseAdapter implements Filterable, b.a {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f2948a;

    /* renamed from: b  reason: collision with root package name */
    protected boolean f2949b;

    /* renamed from: c  reason: collision with root package name */
    protected Cursor f2950c;

    /* renamed from: d  reason: collision with root package name */
    protected Context f2951d;

    /* renamed from: e  reason: collision with root package name */
    protected int f2952e;

    /* renamed from: f  reason: collision with root package name */
    protected C0055a f2953f;

    /* renamed from: g  reason: collision with root package name */
    protected DataSetObserver f2954g;

    /* renamed from: h  reason: collision with root package name */
    protected b f2955h;

    /* renamed from: b.f.a.a$a  reason: collision with other inner class name */
    /* compiled from: CursorAdapter */
    private class C0055a extends ContentObserver {
        C0055a() {
            super(new Handler());
        }

        public boolean deliverSelfNotifications() {
            return true;
        }

        public void onChange(boolean z) {
            a.this.b();
        }
    }

    /* compiled from: CursorAdapter */
    private class b extends DataSetObserver {
        b() {
        }

        public void onChanged() {
            a aVar = a.this;
            aVar.f2948a = true;
            aVar.notifyDataSetChanged();
        }

        public void onInvalidated() {
            a aVar = a.this;
            aVar.f2948a = false;
            aVar.notifyDataSetInvalidated();
        }
    }

    public a(Context context, Cursor cursor, boolean z) {
        a(context, cursor, z ? 1 : 2);
    }

    public abstract View a(Context context, Cursor cursor, ViewGroup viewGroup);

    /* access modifiers changed from: package-private */
    public void a(Context context, Cursor cursor, int i2) {
        boolean z = false;
        if ((i2 & 1) == 1) {
            i2 |= 2;
            this.f2949b = true;
        } else {
            this.f2949b = false;
        }
        if (cursor != null) {
            z = true;
        }
        this.f2950c = cursor;
        this.f2948a = z;
        this.f2951d = context;
        this.f2952e = z ? cursor.getColumnIndexOrThrow("_id") : -1;
        if ((i2 & 2) == 2) {
            this.f2953f = new C0055a();
            this.f2954g = new b();
        } else {
            this.f2953f = null;
            this.f2954g = null;
        }
        if (z) {
            C0055a aVar = this.f2953f;
            if (aVar != null) {
                cursor.registerContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.f2954g;
            if (dataSetObserver != null) {
                cursor.registerDataSetObserver(dataSetObserver);
            }
        }
    }

    public abstract void a(View view, Context context, Cursor cursor);

    public abstract View b(Context context, Cursor cursor, ViewGroup viewGroup);

    public abstract CharSequence b(Cursor cursor);

    /* access modifiers changed from: protected */
    public void b() {
        Cursor cursor;
        if (this.f2949b && (cursor = this.f2950c) != null && !cursor.isClosed()) {
            this.f2948a = this.f2950c.requery();
        }
    }

    public Cursor c(Cursor cursor) {
        Cursor cursor2 = this.f2950c;
        if (cursor == cursor2) {
            return null;
        }
        if (cursor2 != null) {
            C0055a aVar = this.f2953f;
            if (aVar != null) {
                cursor2.unregisterContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.f2954g;
            if (dataSetObserver != null) {
                cursor2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.f2950c = cursor;
        if (cursor != null) {
            C0055a aVar2 = this.f2953f;
            if (aVar2 != null) {
                cursor.registerContentObserver(aVar2);
            }
            DataSetObserver dataSetObserver2 = this.f2954g;
            if (dataSetObserver2 != null) {
                cursor.registerDataSetObserver(dataSetObserver2);
            }
            this.f2952e = cursor.getColumnIndexOrThrow("_id");
            this.f2948a = true;
            notifyDataSetChanged();
        } else {
            this.f2952e = -1;
            this.f2948a = false;
            notifyDataSetInvalidated();
        }
        return cursor2;
    }

    public int getCount() {
        Cursor cursor;
        if (!this.f2948a || (cursor = this.f2950c) == null) {
            return 0;
        }
        return cursor.getCount();
    }

    public View getDropDownView(int i2, View view, ViewGroup viewGroup) {
        if (!this.f2948a) {
            return null;
        }
        this.f2950c.moveToPosition(i2);
        if (view == null) {
            view = a(this.f2951d, this.f2950c, viewGroup);
        }
        a(view, this.f2951d, this.f2950c);
        return view;
    }

    public Filter getFilter() {
        if (this.f2955h == null) {
            this.f2955h = new b(this);
        }
        return this.f2955h;
    }

    public Object getItem(int i2) {
        Cursor cursor;
        if (!this.f2948a || (cursor = this.f2950c) == null) {
            return null;
        }
        cursor.moveToPosition(i2);
        return this.f2950c;
    }

    public long getItemId(int i2) {
        Cursor cursor;
        if (!this.f2948a || (cursor = this.f2950c) == null || !cursor.moveToPosition(i2)) {
            return 0;
        }
        return this.f2950c.getLong(this.f2952e);
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (!this.f2948a) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        } else if (this.f2950c.moveToPosition(i2)) {
            if (view == null) {
                view = b(this.f2951d, this.f2950c, viewGroup);
            }
            a(view, this.f2951d, this.f2950c);
            return view;
        } else {
            throw new IllegalStateException("couldn't move cursor to position " + i2);
        }
    }

    public Cursor a() {
        return this.f2950c;
    }

    public void a(Cursor cursor) {
        Cursor c2 = c(cursor);
        if (c2 != null) {
            c2.close();
        }
    }
}
