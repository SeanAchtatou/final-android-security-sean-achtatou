package b.a.n;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.view.menu.g;
import androidx.appcompat.widget.ActionBarContextView;
import b.a.n.b;
import java.lang.ref.WeakReference;

/* compiled from: StandaloneActionMode */
public class e extends b implements g.a {

    /* renamed from: c  reason: collision with root package name */
    private Context f2639c;

    /* renamed from: d  reason: collision with root package name */
    private ActionBarContextView f2640d;

    /* renamed from: e  reason: collision with root package name */
    private b.a f2641e;

    /* renamed from: f  reason: collision with root package name */
    private WeakReference<View> f2642f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f2643g;

    /* renamed from: h  reason: collision with root package name */
    private g f2644h;

    public e(Context context, ActionBarContextView actionBarContextView, b.a aVar, boolean z) {
        this.f2639c = context;
        this.f2640d = actionBarContextView;
        this.f2641e = aVar;
        g gVar = new g(actionBarContextView.getContext());
        gVar.c(1);
        this.f2644h = gVar;
        this.f2644h.a(this);
    }

    public void a(CharSequence charSequence) {
        this.f2640d.setSubtitle(charSequence);
    }

    public void b(CharSequence charSequence) {
        this.f2640d.setTitle(charSequence);
    }

    public Menu c() {
        return this.f2644h;
    }

    public MenuInflater d() {
        return new g(this.f2640d.getContext());
    }

    public CharSequence e() {
        return this.f2640d.getSubtitle();
    }

    public CharSequence g() {
        return this.f2640d.getTitle();
    }

    public void i() {
        this.f2641e.b(this, this.f2644h);
    }

    public boolean j() {
        return this.f2640d.b();
    }

    public void a(int i2) {
        a((CharSequence) this.f2639c.getString(i2));
    }

    public void b(int i2) {
        b(this.f2639c.getString(i2));
    }

    public void a(boolean z) {
        super.a(z);
        this.f2640d.setTitleOptional(z);
    }

    public View b() {
        WeakReference<View> weakReference = this.f2642f;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    public void a(View view) {
        this.f2640d.setCustomView(view);
        this.f2642f = view != null ? new WeakReference<>(view) : null;
    }

    public void a() {
        if (!this.f2643g) {
            this.f2643g = true;
            this.f2640d.sendAccessibilityEvent(32);
            this.f2641e.a(this);
        }
    }

    public boolean a(g gVar, MenuItem menuItem) {
        return this.f2641e.a(this, menuItem);
    }

    public void a(g gVar) {
        i();
        this.f2640d.d();
    }
}
