package b.a.n;

import android.content.Context;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.view.menu.k;
import androidx.appcompat.view.menu.p;
import b.a.n.b;
import b.d.g;
import java.util.ArrayList;

/* compiled from: SupportActionModeWrapper */
public class f extends ActionMode {

    /* renamed from: a  reason: collision with root package name */
    final Context f2645a;

    /* renamed from: b  reason: collision with root package name */
    final b f2646b;

    public f(Context context, b bVar) {
        this.f2645a = context;
        this.f2646b = bVar;
    }

    public void finish() {
        this.f2646b.a();
    }

    public View getCustomView() {
        return this.f2646b.b();
    }

    public Menu getMenu() {
        return new p(this.f2645a, (b.e.g.a.a) this.f2646b.c());
    }

    public MenuInflater getMenuInflater() {
        return this.f2646b.d();
    }

    public CharSequence getSubtitle() {
        return this.f2646b.e();
    }

    public Object getTag() {
        return this.f2646b.f();
    }

    public CharSequence getTitle() {
        return this.f2646b.g();
    }

    public boolean getTitleOptionalHint() {
        return this.f2646b.h();
    }

    public void invalidate() {
        this.f2646b.i();
    }

    public boolean isTitleOptional() {
        return this.f2646b.j();
    }

    public void setCustomView(View view) {
        this.f2646b.a(view);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.f2646b.a(charSequence);
    }

    public void setTag(Object obj) {
        this.f2646b.a(obj);
    }

    public void setTitle(CharSequence charSequence) {
        this.f2646b.b(charSequence);
    }

    public void setTitleOptionalHint(boolean z) {
        this.f2646b.a(z);
    }

    public void setSubtitle(int i2) {
        this.f2646b.a(i2);
    }

    public void setTitle(int i2) {
        this.f2646b.b(i2);
    }

    /* compiled from: SupportActionModeWrapper */
    public static class a implements b.a {

        /* renamed from: a  reason: collision with root package name */
        final ActionMode.Callback f2647a;

        /* renamed from: b  reason: collision with root package name */
        final Context f2648b;

        /* renamed from: c  reason: collision with root package name */
        final ArrayList<f> f2649c = new ArrayList<>();

        /* renamed from: d  reason: collision with root package name */
        final g<Menu, Menu> f2650d = new g<>();

        public a(Context context, ActionMode.Callback callback) {
            this.f2648b = context;
            this.f2647a = callback;
        }

        public boolean a(b bVar, Menu menu) {
            return this.f2647a.onCreateActionMode(b(bVar), a(menu));
        }

        public boolean b(b bVar, Menu menu) {
            return this.f2647a.onPrepareActionMode(b(bVar), a(menu));
        }

        public boolean a(b bVar, MenuItem menuItem) {
            return this.f2647a.onActionItemClicked(b(bVar), new k(this.f2648b, (b.e.g.a.b) menuItem));
        }

        public ActionMode b(b bVar) {
            int size = this.f2649c.size();
            for (int i2 = 0; i2 < size; i2++) {
                f fVar = this.f2649c.get(i2);
                if (fVar != null && fVar.f2646b == bVar) {
                    return fVar;
                }
            }
            f fVar2 = new f(this.f2648b, bVar);
            this.f2649c.add(fVar2);
            return fVar2;
        }

        public void a(b bVar) {
            this.f2647a.onDestroyActionMode(b(bVar));
        }

        private Menu a(Menu menu) {
            Menu menu2 = this.f2650d.get(menu);
            if (menu2 != null) {
                return menu2;
            }
            p pVar = new p(this.f2648b, (b.e.g.a.a) menu);
            this.f2650d.put(menu, pVar);
            return pVar;
        }
    }
}
