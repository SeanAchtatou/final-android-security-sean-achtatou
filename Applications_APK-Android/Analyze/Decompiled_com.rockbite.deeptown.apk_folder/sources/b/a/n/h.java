package b.a.n;

import android.view.View;
import android.view.animation.Interpolator;
import b.e.m.a0;
import b.e.m.y;
import b.e.m.z;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ViewPropertyAnimatorCompatSet */
public class h {

    /* renamed from: a  reason: collision with root package name */
    final ArrayList<y> f2671a = new ArrayList<>();

    /* renamed from: b  reason: collision with root package name */
    private long f2672b = -1;

    /* renamed from: c  reason: collision with root package name */
    private Interpolator f2673c;

    /* renamed from: d  reason: collision with root package name */
    z f2674d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f2675e;

    /* renamed from: f  reason: collision with root package name */
    private final a0 f2676f = new a();

    /* compiled from: ViewPropertyAnimatorCompatSet */
    class a extends a0 {

        /* renamed from: a  reason: collision with root package name */
        private boolean f2677a = false;

        /* renamed from: b  reason: collision with root package name */
        private int f2678b = 0;

        a() {
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f2678b = 0;
            this.f2677a = false;
            h.this.b();
        }

        public void b(View view) {
            int i2 = this.f2678b + 1;
            this.f2678b = i2;
            if (i2 == h.this.f2671a.size()) {
                z zVar = h.this.f2674d;
                if (zVar != null) {
                    zVar.b(null);
                }
                a();
            }
        }

        public void c(View view) {
            if (!this.f2677a) {
                this.f2677a = true;
                z zVar = h.this.f2674d;
                if (zVar != null) {
                    zVar.c(null);
                }
            }
        }
    }

    public h a(y yVar) {
        if (!this.f2675e) {
            this.f2671a.add(yVar);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.f2675e = false;
    }

    public void c() {
        if (!this.f2675e) {
            Iterator<y> it = this.f2671a.iterator();
            while (it.hasNext()) {
                y next = it.next();
                long j2 = this.f2672b;
                if (j2 >= 0) {
                    next.a(j2);
                }
                Interpolator interpolator = this.f2673c;
                if (interpolator != null) {
                    next.a(interpolator);
                }
                if (this.f2674d != null) {
                    next.a(this.f2676f);
                }
                next.c();
            }
            this.f2675e = true;
        }
    }

    public h a(y yVar, y yVar2) {
        this.f2671a.add(yVar);
        yVar2.b(yVar.b());
        this.f2671a.add(yVar2);
        return this;
    }

    public void a() {
        if (this.f2675e) {
            Iterator<y> it = this.f2671a.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
            this.f2675e = false;
        }
    }

    public h a(long j2) {
        if (!this.f2675e) {
            this.f2672b = j2;
        }
        return this;
    }

    public h a(Interpolator interpolator) {
        if (!this.f2675e) {
            this.f2673c = interpolator;
        }
        return this;
    }

    public h a(z zVar) {
        if (!this.f2675e) {
            this.f2674d = zVar;
        }
        return this;
    }
}
