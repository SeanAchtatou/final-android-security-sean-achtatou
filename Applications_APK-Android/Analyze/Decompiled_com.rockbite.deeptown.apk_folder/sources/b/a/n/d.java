package b.a.n;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.view.LayoutInflater;
import b.a.i;

/* compiled from: ContextThemeWrapper */
public class d extends ContextWrapper {

    /* renamed from: a  reason: collision with root package name */
    private int f2634a;

    /* renamed from: b  reason: collision with root package name */
    private Resources.Theme f2635b;

    /* renamed from: c  reason: collision with root package name */
    private LayoutInflater f2636c;

    /* renamed from: d  reason: collision with root package name */
    private Configuration f2637d;

    /* renamed from: e  reason: collision with root package name */
    private Resources f2638e;

    public d() {
        super(null);
    }

    private Resources b() {
        if (this.f2638e == null) {
            Configuration configuration = this.f2637d;
            if (configuration == null) {
                this.f2638e = super.getResources();
            } else if (Build.VERSION.SDK_INT >= 17) {
                this.f2638e = createConfigurationContext(configuration).getResources();
            }
        }
        return this.f2638e;
    }

    private void c() {
        boolean z = this.f2635b == null;
        if (z) {
            this.f2635b = getResources().newTheme();
            Resources.Theme theme = getBaseContext().getTheme();
            if (theme != null) {
                this.f2635b.setTo(theme);
            }
        }
        a(this.f2635b, this.f2634a, z);
    }

    public int a() {
        return this.f2634a;
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }

    public AssetManager getAssets() {
        return getResources().getAssets();
    }

    public Resources getResources() {
        return b();
    }

    public Object getSystemService(String str) {
        if (!"layout_inflater".equals(str)) {
            return getBaseContext().getSystemService(str);
        }
        if (this.f2636c == null) {
            this.f2636c = LayoutInflater.from(getBaseContext()).cloneInContext(this);
        }
        return this.f2636c;
    }

    public Resources.Theme getTheme() {
        Resources.Theme theme = this.f2635b;
        if (theme != null) {
            return theme;
        }
        if (this.f2634a == 0) {
            this.f2634a = i.Theme_AppCompat_Light;
        }
        c();
        return this.f2635b;
    }

    public void setTheme(int i2) {
        if (this.f2634a != i2) {
            this.f2634a = i2;
            c();
        }
    }

    public d(Context context, int i2) {
        super(context);
        this.f2634a = i2;
    }

    /* access modifiers changed from: protected */
    public void a(Resources.Theme theme, int i2, boolean z) {
        theme.applyStyle(i2, true);
    }

    public d(Context context, Resources.Theme theme) {
        super(context);
        this.f2635b = theme;
    }
}
