package b.a.n;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import androidx.appcompat.view.menu.k;
import androidx.appcompat.widget.d0;
import androidx.appcompat.widget.v0;
import b.a.j;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: SupportMenuInflater */
public class g extends MenuInflater {

    /* renamed from: e  reason: collision with root package name */
    static final Class<?>[] f2651e = {Context.class};

    /* renamed from: f  reason: collision with root package name */
    static final Class<?>[] f2652f = f2651e;

    /* renamed from: a  reason: collision with root package name */
    final Object[] f2653a;

    /* renamed from: b  reason: collision with root package name */
    final Object[] f2654b = this.f2653a;

    /* renamed from: c  reason: collision with root package name */
    Context f2655c;

    /* renamed from: d  reason: collision with root package name */
    private Object f2656d;

    /* compiled from: SupportMenuInflater */
    private static class a implements MenuItem.OnMenuItemClickListener {

        /* renamed from: c  reason: collision with root package name */
        private static final Class<?>[] f2657c = {MenuItem.class};

        /* renamed from: a  reason: collision with root package name */
        private Object f2658a;

        /* renamed from: b  reason: collision with root package name */
        private Method f2659b;

        public a(Object obj, String str) {
            this.f2658a = obj;
            Class<?> cls = obj.getClass();
            try {
                this.f2659b = cls.getMethod(str, f2657c);
            } catch (Exception e2) {
                InflateException inflateException = new InflateException("Couldn't resolve menu item onClick handler " + str + " in class " + cls.getName());
                inflateException.initCause(e2);
                throw inflateException;
            }
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            try {
                if (this.f2659b.getReturnType() == Boolean.TYPE) {
                    return ((Boolean) this.f2659b.invoke(this.f2658a, menuItem)).booleanValue();
                }
                this.f2659b.invoke(this.f2658a, menuItem);
                return true;
            } catch (Exception e2) {
                throw new RuntimeException(e2);
            }
        }
    }

    public g(Context context) {
        super(context);
        this.f2655c = context;
        this.f2653a = new Object[]{context};
    }

    private void a(XmlPullParser xmlPullParser, AttributeSet attributeSet, Menu menu) throws XmlPullParserException, IOException {
        b bVar = new b(menu);
        int eventType = xmlPullParser.getEventType();
        while (true) {
            if (eventType != 2) {
                eventType = xmlPullParser.next();
                if (eventType == 1) {
                    break;
                }
            } else {
                String name = xmlPullParser.getName();
                if (name.equals("menu")) {
                    eventType = xmlPullParser.next();
                } else {
                    throw new RuntimeException("Expecting menu, got " + name);
                }
            }
        }
        int i2 = eventType;
        String str = null;
        boolean z = false;
        boolean z2 = false;
        while (!z) {
            if (i2 != 1) {
                if (i2 != 2) {
                    if (i2 == 3) {
                        String name2 = xmlPullParser.getName();
                        if (z2 && name2.equals(str)) {
                            str = null;
                            z2 = false;
                        } else if (name2.equals("group")) {
                            bVar.d();
                        } else if (name2.equals("item")) {
                            if (!bVar.c()) {
                                b.e.m.b bVar2 = bVar.A;
                                if (bVar2 == null || !bVar2.a()) {
                                    bVar.a();
                                } else {
                                    bVar.b();
                                }
                            }
                        } else if (name2.equals("menu")) {
                            z = true;
                        }
                    }
                } else if (!z2) {
                    String name3 = xmlPullParser.getName();
                    if (name3.equals("group")) {
                        bVar.a(attributeSet);
                    } else if (name3.equals("item")) {
                        bVar.b(attributeSet);
                    } else if (name3.equals("menu")) {
                        a(xmlPullParser, attributeSet, bVar.b());
                    } else {
                        str = name3;
                        z2 = true;
                    }
                }
                i2 = xmlPullParser.next();
            } else {
                throw new RuntimeException("Unexpected end of document");
            }
        }
    }

    public void inflate(int i2, Menu menu) {
        if (!(menu instanceof b.e.g.a.a)) {
            super.inflate(i2, menu);
            return;
        }
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = this.f2655c.getResources().getLayout(i2);
            a(xmlResourceParser, Xml.asAttributeSet(xmlResourceParser), menu);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        } catch (XmlPullParserException e2) {
            throw new InflateException("Error inflating menu XML", e2);
        } catch (IOException e3) {
            throw new InflateException("Error inflating menu XML", e3);
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    /* compiled from: SupportMenuInflater */
    private class b {
        b.e.m.b A;
        private CharSequence B;
        private CharSequence C;
        private ColorStateList D = null;
        private PorterDuff.Mode E = null;

        /* renamed from: a  reason: collision with root package name */
        private Menu f2660a;

        /* renamed from: b  reason: collision with root package name */
        private int f2661b;

        /* renamed from: c  reason: collision with root package name */
        private int f2662c;

        /* renamed from: d  reason: collision with root package name */
        private int f2663d;

        /* renamed from: e  reason: collision with root package name */
        private int f2664e;

        /* renamed from: f  reason: collision with root package name */
        private boolean f2665f;

        /* renamed from: g  reason: collision with root package name */
        private boolean f2666g;

        /* renamed from: h  reason: collision with root package name */
        private boolean f2667h;

        /* renamed from: i  reason: collision with root package name */
        private int f2668i;

        /* renamed from: j  reason: collision with root package name */
        private int f2669j;

        /* renamed from: k  reason: collision with root package name */
        private CharSequence f2670k;
        private CharSequence l;
        private int m;
        private char n;
        private int o;
        private char p;
        private int q;
        private int r;
        private boolean s;
        private boolean t;
        private boolean u;
        private int v;
        private int w;
        private String x;
        private String y;
        private String z;

        public b(Menu menu) {
            this.f2660a = menu;
            d();
        }

        public void a(AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = g.this.f2655c.obtainStyledAttributes(attributeSet, j.MenuGroup);
            this.f2661b = obtainStyledAttributes.getResourceId(j.MenuGroup_android_id, 0);
            this.f2662c = obtainStyledAttributes.getInt(j.MenuGroup_android_menuCategory, 0);
            this.f2663d = obtainStyledAttributes.getInt(j.MenuGroup_android_orderInCategory, 0);
            this.f2664e = obtainStyledAttributes.getInt(j.MenuGroup_android_checkableBehavior, 0);
            this.f2665f = obtainStyledAttributes.getBoolean(j.MenuGroup_android_visible, true);
            this.f2666g = obtainStyledAttributes.getBoolean(j.MenuGroup_android_enabled, true);
            obtainStyledAttributes.recycle();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: androidx.appcompat.widget.v0.a(int, boolean):boolean
         arg types: [int, int]
         candidates:
          androidx.appcompat.widget.v0.a(int, float):float
          androidx.appcompat.widget.v0.a(int, int):int
          androidx.appcompat.widget.v0.a(int, boolean):boolean */
        public void b(AttributeSet attributeSet) {
            v0 a2 = v0.a(g.this.f2655c, attributeSet, j.MenuItem);
            this.f2668i = a2.g(j.MenuItem_android_id, 0);
            this.f2669j = (a2.d(j.MenuItem_android_menuCategory, this.f2662c) & -65536) | (a2.d(j.MenuItem_android_orderInCategory, this.f2663d) & 65535);
            this.f2670k = a2.e(j.MenuItem_android_title);
            this.l = a2.e(j.MenuItem_android_titleCondensed);
            this.m = a2.g(j.MenuItem_android_icon, 0);
            this.n = a(a2.d(j.MenuItem_android_alphabeticShortcut));
            this.o = a2.d(j.MenuItem_alphabeticModifiers, CodedOutputStream.DEFAULT_BUFFER_SIZE);
            this.p = a(a2.d(j.MenuItem_android_numericShortcut));
            this.q = a2.d(j.MenuItem_numericModifiers, CodedOutputStream.DEFAULT_BUFFER_SIZE);
            if (a2.g(j.MenuItem_android_checkable)) {
                this.r = a2.a(j.MenuItem_android_checkable, false) ? 1 : 0;
            } else {
                this.r = this.f2664e;
            }
            this.s = a2.a(j.MenuItem_android_checked, false);
            this.t = a2.a(j.MenuItem_android_visible, this.f2665f);
            this.u = a2.a(j.MenuItem_android_enabled, this.f2666g);
            this.v = a2.d(j.MenuItem_showAsAction, -1);
            this.z = a2.d(j.MenuItem_android_onClick);
            this.w = a2.g(j.MenuItem_actionLayout, 0);
            this.x = a2.d(j.MenuItem_actionViewClass);
            this.y = a2.d(j.MenuItem_actionProviderClass);
            boolean z2 = this.y != null;
            if (z2 && this.w == 0 && this.x == null) {
                this.A = (b.e.m.b) a(this.y, g.f2652f, g.this.f2654b);
            } else {
                if (z2) {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
                }
                this.A = null;
            }
            this.B = a2.e(j.MenuItem_contentDescription);
            this.C = a2.e(j.MenuItem_tooltipText);
            if (a2.g(j.MenuItem_iconTintMode)) {
                this.E = d0.a(a2.d(j.MenuItem_iconTintMode, -1), this.E);
            } else {
                this.E = null;
            }
            if (a2.g(j.MenuItem_iconTint)) {
                this.D = a2.a(j.MenuItem_iconTint);
            } else {
                this.D = null;
            }
            a2.a();
            this.f2667h = false;
        }

        public boolean c() {
            return this.f2667h;
        }

        public void d() {
            this.f2661b = 0;
            this.f2662c = 0;
            this.f2663d = 0;
            this.f2664e = 0;
            this.f2665f = true;
            this.f2666g = true;
        }

        private char a(String str) {
            if (str == null) {
                return 0;
            }
            return str.charAt(0);
        }

        private void a(MenuItem menuItem) {
            boolean z2 = false;
            menuItem.setChecked(this.s).setVisible(this.t).setEnabled(this.u).setCheckable(this.r >= 1).setTitleCondensed(this.l).setIcon(this.m);
            int i2 = this.v;
            if (i2 >= 0) {
                menuItem.setShowAsAction(i2);
            }
            if (this.z != null) {
                if (!g.this.f2655c.isRestricted()) {
                    menuItem.setOnMenuItemClickListener(new a(g.this.a(), this.z));
                } else {
                    throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
                }
            }
            boolean z3 = menuItem instanceof androidx.appcompat.view.menu.j;
            if (z3) {
                androidx.appcompat.view.menu.j jVar = (androidx.appcompat.view.menu.j) menuItem;
            }
            if (this.r >= 2) {
                if (z3) {
                    ((androidx.appcompat.view.menu.j) menuItem).c(true);
                } else if (menuItem instanceof k) {
                    ((k) menuItem).a(true);
                }
            }
            String str = this.x;
            if (str != null) {
                menuItem.setActionView((View) a(str, g.f2651e, g.this.f2653a));
                z2 = true;
            }
            int i3 = this.w;
            if (i3 > 0) {
                if (!z2) {
                    menuItem.setActionView(i3);
                } else {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
                }
            }
            b.e.m.b bVar = this.A;
            if (bVar != null) {
                b.e.m.g.a(menuItem, bVar);
            }
            b.e.m.g.a(menuItem, this.B);
            b.e.m.g.b(menuItem, this.C);
            b.e.m.g.a(menuItem, this.n, this.o);
            b.e.m.g.b(menuItem, this.p, this.q);
            PorterDuff.Mode mode = this.E;
            if (mode != null) {
                b.e.m.g.a(menuItem, mode);
            }
            ColorStateList colorStateList = this.D;
            if (colorStateList != null) {
                b.e.m.g.a(menuItem, colorStateList);
            }
        }

        public SubMenu b() {
            this.f2667h = true;
            SubMenu addSubMenu = this.f2660a.addSubMenu(this.f2661b, this.f2668i, this.f2669j, this.f2670k);
            a(addSubMenu.getItem());
            return addSubMenu;
        }

        public void a() {
            this.f2667h = true;
            a(this.f2660a.add(this.f2661b, this.f2668i, this.f2669j, this.f2670k));
        }

        private <T> T a(String str, Class<?>[] clsArr, Object[] objArr) {
            try {
                Constructor<?> constructor = Class.forName(str, false, g.this.f2655c.getClassLoader()).getConstructor(clsArr);
                constructor.setAccessible(true);
                return constructor.newInstance(objArr);
            } catch (Exception e2) {
                Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e2);
                return null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Object a() {
        if (this.f2656d == null) {
            this.f2656d = a(this.f2655c);
        }
        return this.f2656d;
    }

    private Object a(Object obj) {
        return (!(obj instanceof Activity) && (obj instanceof ContextWrapper)) ? a(((ContextWrapper) obj).getBaseContext()) : obj;
    }
}
