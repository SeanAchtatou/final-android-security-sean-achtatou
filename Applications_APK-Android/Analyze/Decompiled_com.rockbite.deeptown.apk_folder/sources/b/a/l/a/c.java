package b.a.l.a;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import androidx.core.graphics.drawable.a;

/* compiled from: DrawableWrapper */
public class c extends Drawable implements Drawable.Callback {

    /* renamed from: a  reason: collision with root package name */
    private Drawable f2630a;

    public c(Drawable drawable) {
        a(drawable);
    }

    public Drawable a() {
        return this.f2630a;
    }

    public void draw(Canvas canvas) {
        this.f2630a.draw(canvas);
    }

    public int getChangingConfigurations() {
        return this.f2630a.getChangingConfigurations();
    }

    public Drawable getCurrent() {
        return this.f2630a.getCurrent();
    }

    public int getIntrinsicHeight() {
        return this.f2630a.getIntrinsicHeight();
    }

    public int getIntrinsicWidth() {
        return this.f2630a.getIntrinsicWidth();
    }

    public int getMinimumHeight() {
        return this.f2630a.getMinimumHeight();
    }

    public int getMinimumWidth() {
        return this.f2630a.getMinimumWidth();
    }

    public int getOpacity() {
        return this.f2630a.getOpacity();
    }

    public boolean getPadding(Rect rect) {
        return this.f2630a.getPadding(rect);
    }

    public int[] getState() {
        return this.f2630a.getState();
    }

    public Region getTransparentRegion() {
        return this.f2630a.getTransparentRegion();
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public boolean isAutoMirrored() {
        return a.e(this.f2630a);
    }

    public boolean isStateful() {
        return this.f2630a.isStateful();
    }

    public void jumpToCurrentState() {
        a.f(this.f2630a);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.f2630a.setBounds(rect);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i2) {
        return this.f2630a.setLevel(i2);
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
        scheduleSelf(runnable, j2);
    }

    public void setAlpha(int i2) {
        this.f2630a.setAlpha(i2);
    }

    public void setAutoMirrored(boolean z) {
        a.a(this.f2630a, z);
    }

    public void setChangingConfigurations(int i2) {
        this.f2630a.setChangingConfigurations(i2);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f2630a.setColorFilter(colorFilter);
    }

    public void setDither(boolean z) {
        this.f2630a.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f2630a.setFilterBitmap(z);
    }

    public void setHotspot(float f2, float f3) {
        a.a(this.f2630a, f2, f3);
    }

    public void setHotspotBounds(int i2, int i3, int i4, int i5) {
        a.a(this.f2630a, i2, i3, i4, i5);
    }

    public boolean setState(int[] iArr) {
        return this.f2630a.setState(iArr);
    }

    public void setTint(int i2) {
        a.b(this.f2630a, i2);
    }

    public void setTintList(ColorStateList colorStateList) {
        a.a(this.f2630a, colorStateList);
    }

    public void setTintMode(PorterDuff.Mode mode) {
        a.a(this.f2630a, mode);
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f2630a.setVisible(z, z2);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    public void a(Drawable drawable) {
        Drawable drawable2 = this.f2630a;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.f2630a = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }
}
