package b.a.l.a;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.SparseArray;

/* compiled from: DrawableContainer */
class b extends Drawable implements Drawable.Callback {

    /* renamed from: a  reason: collision with root package name */
    private c f2606a;

    /* renamed from: b  reason: collision with root package name */
    private Rect f2607b;

    /* renamed from: c  reason: collision with root package name */
    private Drawable f2608c;

    /* renamed from: d  reason: collision with root package name */
    private Drawable f2609d;

    /* renamed from: e  reason: collision with root package name */
    private int f2610e = 255;

    /* renamed from: f  reason: collision with root package name */
    private boolean f2611f;

    /* renamed from: g  reason: collision with root package name */
    private int f2612g = -1;

    /* renamed from: h  reason: collision with root package name */
    private boolean f2613h;

    /* renamed from: i  reason: collision with root package name */
    private Runnable f2614i;

    /* renamed from: j  reason: collision with root package name */
    private long f2615j;

    /* renamed from: k  reason: collision with root package name */
    private long f2616k;
    private C0038b l;

    /* compiled from: DrawableContainer */
    class a implements Runnable {
        a() {
        }

        public void run() {
            b.this.a(true);
            b.this.invalidateSelf();
        }
    }

    /* renamed from: b.a.l.a.b$b  reason: collision with other inner class name */
    /* compiled from: DrawableContainer */
    static class C0038b implements Drawable.Callback {

        /* renamed from: a  reason: collision with root package name */
        private Drawable.Callback f2618a;

        C0038b() {
        }

        public C0038b a(Drawable.Callback callback) {
            this.f2618a = callback;
            return this;
        }

        public void invalidateDrawable(Drawable drawable) {
        }

        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
            Drawable.Callback callback = this.f2618a;
            if (callback != null) {
                callback.scheduleDrawable(drawable, runnable, j2);
            }
        }

        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            Drawable.Callback callback = this.f2618a;
            if (callback != null) {
                callback.unscheduleDrawable(drawable, runnable);
            }
        }

        public Drawable.Callback a() {
            Drawable.Callback callback = this.f2618a;
            this.f2618a = null;
            return callback;
        }
    }

    /* compiled from: DrawableContainer */
    static abstract class c extends Drawable.ConstantState {
        int A;
        int B;
        boolean C;
        ColorFilter D;
        boolean E;
        ColorStateList F;
        PorterDuff.Mode G;
        boolean H;
        boolean I;

        /* renamed from: a  reason: collision with root package name */
        final b f2619a;

        /* renamed from: b  reason: collision with root package name */
        Resources f2620b;

        /* renamed from: c  reason: collision with root package name */
        int f2621c = 160;

        /* renamed from: d  reason: collision with root package name */
        int f2622d;

        /* renamed from: e  reason: collision with root package name */
        int f2623e;

        /* renamed from: f  reason: collision with root package name */
        SparseArray<Drawable.ConstantState> f2624f;

        /* renamed from: g  reason: collision with root package name */
        Drawable[] f2625g;

        /* renamed from: h  reason: collision with root package name */
        int f2626h;

        /* renamed from: i  reason: collision with root package name */
        boolean f2627i;

        /* renamed from: j  reason: collision with root package name */
        boolean f2628j;

        /* renamed from: k  reason: collision with root package name */
        Rect f2629k;
        boolean l;
        boolean m;
        int n;
        int o;
        int p;
        int q;
        boolean r;
        int s;
        boolean t;
        boolean u;
        boolean v;
        boolean w;
        boolean x;
        boolean y;
        int z;

        c(c cVar, b bVar, Resources resources) {
            Resources resources2;
            this.f2627i = false;
            this.l = false;
            this.x = true;
            this.A = 0;
            this.B = 0;
            this.f2619a = bVar;
            if (resources != null) {
                resources2 = resources;
            } else {
                resources2 = cVar != null ? cVar.f2620b : null;
            }
            this.f2620b = resources2;
            this.f2621c = b.a(resources, cVar != null ? cVar.f2621c : 0);
            if (cVar != null) {
                this.f2622d = cVar.f2622d;
                this.f2623e = cVar.f2623e;
                this.v = true;
                this.w = true;
                this.f2627i = cVar.f2627i;
                this.l = cVar.l;
                this.x = cVar.x;
                this.y = cVar.y;
                this.z = cVar.z;
                this.A = cVar.A;
                this.B = cVar.B;
                this.C = cVar.C;
                this.D = cVar.D;
                this.E = cVar.E;
                this.F = cVar.F;
                this.G = cVar.G;
                this.H = cVar.H;
                this.I = cVar.I;
                if (cVar.f2621c == this.f2621c) {
                    if (cVar.f2628j) {
                        this.f2629k = new Rect(cVar.f2629k);
                        this.f2628j = true;
                    }
                    if (cVar.m) {
                        this.n = cVar.n;
                        this.o = cVar.o;
                        this.p = cVar.p;
                        this.q = cVar.q;
                        this.m = true;
                    }
                }
                if (cVar.r) {
                    this.s = cVar.s;
                    this.r = true;
                }
                if (cVar.t) {
                    this.u = cVar.u;
                    this.t = true;
                }
                Drawable[] drawableArr = cVar.f2625g;
                this.f2625g = new Drawable[drawableArr.length];
                this.f2626h = cVar.f2626h;
                SparseArray<Drawable.ConstantState> sparseArray = cVar.f2624f;
                if (sparseArray != null) {
                    this.f2624f = sparseArray.clone();
                } else {
                    this.f2624f = new SparseArray<>(this.f2626h);
                }
                int i2 = this.f2626h;
                for (int i3 = 0; i3 < i2; i3++) {
                    if (drawableArr[i3] != null) {
                        Drawable.ConstantState constantState = drawableArr[i3].getConstantState();
                        if (constantState != null) {
                            this.f2624f.put(i3, constantState);
                        } else {
                            this.f2625g[i3] = drawableArr[i3];
                        }
                    }
                }
                return;
            }
            this.f2625g = new Drawable[10];
            this.f2626h = 0;
        }

        private Drawable b(Drawable drawable) {
            if (Build.VERSION.SDK_INT >= 23) {
                drawable.setLayoutDirection(this.z);
            }
            Drawable mutate = drawable.mutate();
            mutate.setCallback(this.f2619a);
            return mutate;
        }

        private void n() {
            SparseArray<Drawable.ConstantState> sparseArray = this.f2624f;
            if (sparseArray != null) {
                int size = sparseArray.size();
                for (int i2 = 0; i2 < size; i2++) {
                    this.f2625g[this.f2624f.keyAt(i2)] = b(this.f2624f.valueAt(i2).newDrawable(this.f2620b));
                }
                this.f2624f = null;
            }
        }

        public final int a(Drawable drawable) {
            int i2 = this.f2626h;
            if (i2 >= this.f2625g.length) {
                a(i2, i2 + 10);
            }
            drawable.mutate();
            drawable.setVisible(false, true);
            drawable.setCallback(this.f2619a);
            this.f2625g[i2] = drawable;
            this.f2626h++;
            this.f2623e = drawable.getChangingConfigurations() | this.f2623e;
            k();
            this.f2629k = null;
            this.f2628j = false;
            this.m = false;
            this.v = false;
            return i2;
        }

        /* access modifiers changed from: package-private */
        public final int c() {
            return this.f2625g.length;
        }

        public boolean canApplyTheme() {
            int i2 = this.f2626h;
            Drawable[] drawableArr = this.f2625g;
            for (int i3 = 0; i3 < i2; i3++) {
                Drawable drawable = drawableArr[i3];
                if (drawable == null) {
                    Drawable.ConstantState constantState = this.f2624f.get(i3);
                    if (constantState != null && constantState.canApplyTheme()) {
                        return true;
                    }
                } else if (drawable.canApplyTheme()) {
                    return true;
                }
            }
            return false;
        }

        public final int d() {
            return this.f2626h;
        }

        public final int e() {
            if (!this.m) {
                b();
            }
            return this.o;
        }

        public final int f() {
            if (!this.m) {
                b();
            }
            return this.q;
        }

        public final int g() {
            if (!this.m) {
                b();
            }
            return this.p;
        }

        public int getChangingConfigurations() {
            return this.f2622d | this.f2623e;
        }

        public final Rect h() {
            if (this.f2627i) {
                return null;
            }
            if (this.f2629k != null || this.f2628j) {
                return this.f2629k;
            }
            n();
            Rect rect = new Rect();
            int i2 = this.f2626h;
            Drawable[] drawableArr = this.f2625g;
            Rect rect2 = null;
            for (int i3 = 0; i3 < i2; i3++) {
                if (drawableArr[i3].getPadding(rect)) {
                    if (rect2 == null) {
                        rect2 = new Rect(0, 0, 0, 0);
                    }
                    int i4 = rect.left;
                    if (i4 > rect2.left) {
                        rect2.left = i4;
                    }
                    int i5 = rect.top;
                    if (i5 > rect2.top) {
                        rect2.top = i5;
                    }
                    int i6 = rect.right;
                    if (i6 > rect2.right) {
                        rect2.right = i6;
                    }
                    int i7 = rect.bottom;
                    if (i7 > rect2.bottom) {
                        rect2.bottom = i7;
                    }
                }
            }
            this.f2628j = true;
            this.f2629k = rect2;
            return rect2;
        }

        public final int i() {
            if (!this.m) {
                b();
            }
            return this.n;
        }

        public final int j() {
            if (this.r) {
                return this.s;
            }
            n();
            int i2 = this.f2626h;
            Drawable[] drawableArr = this.f2625g;
            int opacity = i2 > 0 ? drawableArr[0].getOpacity() : -2;
            for (int i3 = 1; i3 < i2; i3++) {
                opacity = Drawable.resolveOpacity(opacity, drawableArr[i3].getOpacity());
            }
            this.s = opacity;
            this.r = true;
            return opacity;
        }

        /* access modifiers changed from: package-private */
        public void k() {
            this.r = false;
            this.t = false;
        }

        public final boolean l() {
            return this.l;
        }

        /* access modifiers changed from: package-private */
        public abstract void m();

        public final void c(int i2) {
            this.B = i2;
        }

        /* access modifiers changed from: package-private */
        public final boolean b(int i2, int i3) {
            int i4 = this.f2626h;
            Drawable[] drawableArr = this.f2625g;
            boolean z2 = false;
            for (int i5 = 0; i5 < i4; i5++) {
                if (drawableArr[i5] != null) {
                    boolean layoutDirection = Build.VERSION.SDK_INT >= 23 ? drawableArr[i5].setLayoutDirection(i2) : false;
                    if (i5 == i3) {
                        z2 = layoutDirection;
                    }
                }
            }
            this.z = i2;
            return z2;
        }

        public final void b(boolean z2) {
            this.f2627i = z2;
        }

        /* access modifiers changed from: protected */
        public void b() {
            this.m = true;
            n();
            int i2 = this.f2626h;
            Drawable[] drawableArr = this.f2625g;
            this.o = -1;
            this.n = -1;
            this.q = 0;
            this.p = 0;
            for (int i3 = 0; i3 < i2; i3++) {
                Drawable drawable = drawableArr[i3];
                int intrinsicWidth = drawable.getIntrinsicWidth();
                if (intrinsicWidth > this.n) {
                    this.n = intrinsicWidth;
                }
                int intrinsicHeight = drawable.getIntrinsicHeight();
                if (intrinsicHeight > this.o) {
                    this.o = intrinsicHeight;
                }
                int minimumWidth = drawable.getMinimumWidth();
                if (minimumWidth > this.p) {
                    this.p = minimumWidth;
                }
                int minimumHeight = drawable.getMinimumHeight();
                if (minimumHeight > this.q) {
                    this.q = minimumHeight;
                }
            }
        }

        public final Drawable a(int i2) {
            int indexOfKey;
            Drawable drawable = this.f2625g[i2];
            if (drawable != null) {
                return drawable;
            }
            SparseArray<Drawable.ConstantState> sparseArray = this.f2624f;
            if (sparseArray == null || (indexOfKey = sparseArray.indexOfKey(i2)) < 0) {
                return null;
            }
            Drawable b2 = b(this.f2624f.valueAt(indexOfKey).newDrawable(this.f2620b));
            this.f2625g[i2] = b2;
            this.f2624f.removeAt(indexOfKey);
            if (this.f2624f.size() == 0) {
                this.f2624f = null;
            }
            return b2;
        }

        /* access modifiers changed from: package-private */
        public final void a(Resources resources) {
            if (resources != null) {
                this.f2620b = resources;
                int a2 = b.a(resources, this.f2621c);
                int i2 = this.f2621c;
                this.f2621c = a2;
                if (i2 != a2) {
                    this.m = false;
                    this.f2628j = false;
                }
            }
        }

        public final void b(int i2) {
            this.A = i2;
        }

        /* access modifiers changed from: package-private */
        public final void a(Resources.Theme theme) {
            if (theme != null) {
                n();
                int i2 = this.f2626h;
                Drawable[] drawableArr = this.f2625g;
                for (int i3 = 0; i3 < i2; i3++) {
                    if (drawableArr[i3] != null && drawableArr[i3].canApplyTheme()) {
                        drawableArr[i3].applyTheme(theme);
                        this.f2623e |= drawableArr[i3].getChangingConfigurations();
                    }
                }
                a(theme.getResources());
            }
        }

        public final void a(boolean z2) {
            this.l = z2;
        }

        public void a(int i2, int i3) {
            Drawable[] drawableArr = new Drawable[i3];
            System.arraycopy(this.f2625g, 0, drawableArr, 0, i2);
            this.f2625g = drawableArr;
        }

        public synchronized boolean a() {
            if (this.v) {
                return this.w;
            }
            n();
            this.v = true;
            int i2 = this.f2626h;
            Drawable[] drawableArr = this.f2625g;
            for (int i3 = 0; i3 < i2; i3++) {
                if (drawableArr[i3].getConstantState() == null) {
                    this.w = false;
                    return false;
                }
            }
            this.w = true;
            return true;
        }
    }

    b() {
    }

    private boolean c() {
        if (!isAutoMirrored() || androidx.core.graphics.drawable.a.d(this) != 1) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public c a() {
        throw null;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0073  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(int r10) {
        /*
            r9 = this;
            int r0 = r9.f2612g
            r1 = 0
            if (r10 != r0) goto L_0x0006
            return r1
        L_0x0006:
            long r2 = android.os.SystemClock.uptimeMillis()
            b.a.l.a.b$c r0 = r9.f2606a
            int r0 = r0.B
            r4 = 0
            r5 = 0
            if (r0 <= 0) goto L_0x002e
            android.graphics.drawable.Drawable r0 = r9.f2609d
            if (r0 == 0) goto L_0x001a
            r0.setVisible(r1, r1)
        L_0x001a:
            android.graphics.drawable.Drawable r0 = r9.f2608c
            if (r0 == 0) goto L_0x0029
            r9.f2609d = r0
            b.a.l.a.b$c r0 = r9.f2606a
            int r0 = r0.B
            long r0 = (long) r0
            long r0 = r0 + r2
            r9.f2616k = r0
            goto L_0x0035
        L_0x0029:
            r9.f2609d = r4
            r9.f2616k = r5
            goto L_0x0035
        L_0x002e:
            android.graphics.drawable.Drawable r0 = r9.f2608c
            if (r0 == 0) goto L_0x0035
            r0.setVisible(r1, r1)
        L_0x0035:
            if (r10 < 0) goto L_0x0055
            b.a.l.a.b$c r0 = r9.f2606a
            int r1 = r0.f2626h
            if (r10 >= r1) goto L_0x0055
            android.graphics.drawable.Drawable r0 = r0.a(r10)
            r9.f2608c = r0
            r9.f2612g = r10
            if (r0 == 0) goto L_0x005a
            b.a.l.a.b$c r10 = r9.f2606a
            int r10 = r10.A
            if (r10 <= 0) goto L_0x0051
            long r7 = (long) r10
            long r2 = r2 + r7
            r9.f2615j = r2
        L_0x0051:
            r9.a(r0)
            goto L_0x005a
        L_0x0055:
            r9.f2608c = r4
            r10 = -1
            r9.f2612g = r10
        L_0x005a:
            long r0 = r9.f2615j
            r10 = 1
            int r2 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r2 != 0) goto L_0x0067
            long r0 = r9.f2616k
            int r2 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r2 == 0) goto L_0x0079
        L_0x0067:
            java.lang.Runnable r0 = r9.f2614i
            if (r0 != 0) goto L_0x0073
            b.a.l.a.b$a r0 = new b.a.l.a.b$a
            r0.<init>()
            r9.f2614i = r0
            goto L_0x0076
        L_0x0073:
            r9.unscheduleSelf(r0)
        L_0x0076:
            r9.a(r10)
        L_0x0079:
            r9.invalidateSelf()
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.l.a.b.a(int):boolean");
    }

    public void applyTheme(Resources.Theme theme) {
        this.f2606a.a(theme);
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.f2612g;
    }

    public boolean canApplyTheme() {
        return this.f2606a.canApplyTheme();
    }

    public void draw(Canvas canvas) {
        Drawable drawable = this.f2608c;
        if (drawable != null) {
            drawable.draw(canvas);
        }
        Drawable drawable2 = this.f2609d;
        if (drawable2 != null) {
            drawable2.draw(canvas);
        }
    }

    public int getAlpha() {
        return this.f2610e;
    }

    public int getChangingConfigurations() {
        return super.getChangingConfigurations() | this.f2606a.getChangingConfigurations();
    }

    public final Drawable.ConstantState getConstantState() {
        if (!this.f2606a.a()) {
            return null;
        }
        this.f2606a.f2622d = getChangingConfigurations();
        return this.f2606a;
    }

    public Drawable getCurrent() {
        return this.f2608c;
    }

    public void getHotspotBounds(Rect rect) {
        Rect rect2 = this.f2607b;
        if (rect2 != null) {
            rect.set(rect2);
        } else {
            super.getHotspotBounds(rect);
        }
    }

    public int getIntrinsicHeight() {
        if (this.f2606a.l()) {
            return this.f2606a.e();
        }
        Drawable drawable = this.f2608c;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return -1;
    }

    public int getIntrinsicWidth() {
        if (this.f2606a.l()) {
            return this.f2606a.i();
        }
        Drawable drawable = this.f2608c;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return -1;
    }

    public int getMinimumHeight() {
        if (this.f2606a.l()) {
            return this.f2606a.f();
        }
        Drawable drawable = this.f2608c;
        if (drawable != null) {
            return drawable.getMinimumHeight();
        }
        return 0;
    }

    public int getMinimumWidth() {
        if (this.f2606a.l()) {
            return this.f2606a.g();
        }
        Drawable drawable = this.f2608c;
        if (drawable != null) {
            return drawable.getMinimumWidth();
        }
        return 0;
    }

    public int getOpacity() {
        Drawable drawable = this.f2608c;
        if (drawable == null || !drawable.isVisible()) {
            return -2;
        }
        return this.f2606a.j();
    }

    public void getOutline(Outline outline) {
        Drawable drawable = this.f2608c;
        if (drawable != null) {
            drawable.getOutline(outline);
        }
    }

    public boolean getPadding(Rect rect) {
        boolean z;
        Rect h2 = this.f2606a.h();
        if (h2 != null) {
            rect.set(h2);
            z = (h2.right | ((h2.left | h2.top) | h2.bottom)) != 0;
        } else {
            Drawable drawable = this.f2608c;
            if (drawable != null) {
                z = drawable.getPadding(rect);
            } else {
                z = super.getPadding(rect);
            }
        }
        if (c()) {
            int i2 = rect.left;
            rect.left = rect.right;
            rect.right = i2;
        }
        return z;
    }

    public void invalidateDrawable(Drawable drawable) {
        c cVar = this.f2606a;
        if (cVar != null) {
            cVar.k();
        }
        if (drawable == this.f2608c && getCallback() != null) {
            getCallback().invalidateDrawable(this);
        }
    }

    public boolean isAutoMirrored() {
        return this.f2606a.C;
    }

    public void jumpToCurrentState() {
        boolean z;
        Drawable drawable = this.f2609d;
        if (drawable != null) {
            drawable.jumpToCurrentState();
            this.f2609d = null;
            z = true;
        } else {
            z = false;
        }
        Drawable drawable2 = this.f2608c;
        if (drawable2 != null) {
            drawable2.jumpToCurrentState();
            if (this.f2611f) {
                this.f2608c.setAlpha(this.f2610e);
            }
        }
        if (this.f2616k != 0) {
            this.f2616k = 0;
            z = true;
        }
        if (this.f2615j != 0) {
            this.f2615j = 0;
            z = true;
        }
        if (z) {
            invalidateSelf();
        }
    }

    public Drawable mutate() {
        if (!this.f2613h && super.mutate() == this) {
            c a2 = a();
            a2.m();
            a(a2);
            this.f2613h = true;
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.f2609d;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
        Drawable drawable2 = this.f2608c;
        if (drawable2 != null) {
            drawable2.setBounds(rect);
        }
    }

    public boolean onLayoutDirectionChanged(int i2) {
        return this.f2606a.b(i2, b());
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i2) {
        Drawable drawable = this.f2609d;
        if (drawable != null) {
            return drawable.setLevel(i2);
        }
        Drawable drawable2 = this.f2608c;
        if (drawable2 != null) {
            return drawable2.setLevel(i2);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        Drawable drawable = this.f2609d;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        Drawable drawable2 = this.f2608c;
        if (drawable2 != null) {
            return drawable2.setState(iArr);
        }
        return false;
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
        if (drawable == this.f2608c && getCallback() != null) {
            getCallback().scheduleDrawable(this, runnable, j2);
        }
    }

    public void setAlpha(int i2) {
        if (!this.f2611f || this.f2610e != i2) {
            this.f2611f = true;
            this.f2610e = i2;
            Drawable drawable = this.f2608c;
            if (drawable == null) {
                return;
            }
            if (this.f2615j == 0) {
                drawable.setAlpha(i2);
            } else {
                a(false);
            }
        }
    }

    public void setAutoMirrored(boolean z) {
        c cVar = this.f2606a;
        if (cVar.C != z) {
            cVar.C = z;
            Drawable drawable = this.f2608c;
            if (drawable != null) {
                androidx.core.graphics.drawable.a.a(drawable, cVar.C);
            }
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        c cVar = this.f2606a;
        cVar.E = true;
        if (cVar.D != colorFilter) {
            cVar.D = colorFilter;
            Drawable drawable = this.f2608c;
            if (drawable != null) {
                drawable.setColorFilter(colorFilter);
            }
        }
    }

    public void setDither(boolean z) {
        c cVar = this.f2606a;
        if (cVar.x != z) {
            cVar.x = z;
            Drawable drawable = this.f2608c;
            if (drawable != null) {
                drawable.setDither(cVar.x);
            }
        }
    }

    public void setHotspot(float f2, float f3) {
        Drawable drawable = this.f2608c;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.a(drawable, f2, f3);
        }
    }

    public void setHotspotBounds(int i2, int i3, int i4, int i5) {
        Rect rect = this.f2607b;
        if (rect == null) {
            this.f2607b = new Rect(i2, i3, i4, i5);
        } else {
            rect.set(i2, i3, i4, i5);
        }
        Drawable drawable = this.f2608c;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.a(drawable, i2, i3, i4, i5);
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        c cVar = this.f2606a;
        cVar.H = true;
        if (cVar.F != colorStateList) {
            cVar.F = colorStateList;
            androidx.core.graphics.drawable.a.a(this.f2608c, colorStateList);
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        c cVar = this.f2606a;
        cVar.I = true;
        if (cVar.G != mode) {
            cVar.G = mode;
            androidx.core.graphics.drawable.a.a(this.f2608c, mode);
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        Drawable drawable = this.f2609d;
        if (drawable != null) {
            drawable.setVisible(z, z2);
        }
        Drawable drawable2 = this.f2608c;
        if (drawable2 != null) {
            drawable2.setVisible(z, z2);
        }
        return visible;
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        if (drawable == this.f2608c && getCallback() != null) {
            getCallback().unscheduleDrawable(this, runnable);
        }
    }

    private void a(Drawable drawable) {
        if (this.l == null) {
            this.l = new C0038b();
        }
        C0038b bVar = this.l;
        bVar.a(drawable.getCallback());
        drawable.setCallback(bVar);
        try {
            if (this.f2606a.A <= 0 && this.f2611f) {
                drawable.setAlpha(this.f2610e);
            }
            if (this.f2606a.E) {
                drawable.setColorFilter(this.f2606a.D);
            } else {
                if (this.f2606a.H) {
                    androidx.core.graphics.drawable.a.a(drawable, this.f2606a.F);
                }
                if (this.f2606a.I) {
                    androidx.core.graphics.drawable.a.a(drawable, this.f2606a.G);
                }
            }
            drawable.setVisible(isVisible(), true);
            drawable.setDither(this.f2606a.x);
            drawable.setState(getState());
            drawable.setLevel(getLevel());
            drawable.setBounds(getBounds());
            if (Build.VERSION.SDK_INT >= 23) {
                drawable.setLayoutDirection(getLayoutDirection());
            }
            if (Build.VERSION.SDK_INT >= 19) {
                drawable.setAutoMirrored(this.f2606a.C);
            }
            Rect rect = this.f2607b;
            if (Build.VERSION.SDK_INT >= 21 && rect != null) {
                drawable.setHotspotBounds(rect.left, rect.top, rect.right, rect.bottom);
            }
        } finally {
            drawable.setCallback(this.l.a());
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006a A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(boolean r14) {
        /*
            r13 = this;
            r0 = 1
            r13.f2611f = r0
            long r1 = android.os.SystemClock.uptimeMillis()
            android.graphics.drawable.Drawable r3 = r13.f2608c
            r4 = 255(0xff, double:1.26E-321)
            r6 = 0
            r7 = 0
            if (r3 == 0) goto L_0x0038
            long r9 = r13.f2615j
            int r11 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r11 == 0) goto L_0x003a
            int r11 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r11 > 0) goto L_0x0022
            int r9 = r13.f2610e
            r3.setAlpha(r9)
            r13.f2615j = r7
            goto L_0x003a
        L_0x0022:
            long r9 = r9 - r1
            long r9 = r9 * r4
            int r10 = (int) r9
            b.a.l.a.b$c r9 = r13.f2606a
            int r9 = r9.A
            int r10 = r10 / r9
            int r9 = 255 - r10
            int r10 = r13.f2610e
            int r9 = r9 * r10
            int r9 = r9 / 255
            r3.setAlpha(r9)
            r3 = 1
            goto L_0x003b
        L_0x0038:
            r13.f2615j = r7
        L_0x003a:
            r3 = 0
        L_0x003b:
            android.graphics.drawable.Drawable r9 = r13.f2609d
            if (r9 == 0) goto L_0x0065
            long r10 = r13.f2616k
            int r12 = (r10 > r7 ? 1 : (r10 == r7 ? 0 : -1))
            if (r12 == 0) goto L_0x0067
            int r12 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r12 > 0) goto L_0x0052
            r9.setVisible(r6, r6)
            r0 = 0
            r13.f2609d = r0
            r13.f2616k = r7
            goto L_0x0067
        L_0x0052:
            long r10 = r10 - r1
            long r10 = r10 * r4
            int r3 = (int) r10
            b.a.l.a.b$c r4 = r13.f2606a
            int r4 = r4.B
            int r3 = r3 / r4
            int r4 = r13.f2610e
            int r3 = r3 * r4
            int r3 = r3 / 255
            r9.setAlpha(r3)
            goto L_0x0068
        L_0x0065:
            r13.f2616k = r7
        L_0x0067:
            r0 = r3
        L_0x0068:
            if (r14 == 0) goto L_0x0074
            if (r0 == 0) goto L_0x0074
            java.lang.Runnable r14 = r13.f2614i
            r3 = 16
            long r1 = r1 + r3
            r13.scheduleSelf(r14, r1)
        L_0x0074:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.l.a.b.a(boolean):void");
    }

    /* access modifiers changed from: package-private */
    public final void a(Resources resources) {
        this.f2606a.a(resources);
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar) {
        this.f2606a = cVar;
        int i2 = this.f2612g;
        if (i2 >= 0) {
            this.f2608c = cVar.a(i2);
            Drawable drawable = this.f2608c;
            if (drawable != null) {
                a(drawable);
            }
        }
        this.f2609d = null;
    }

    static int a(Resources resources, int i2) {
        if (resources != null) {
            i2 = resources.getDisplayMetrics().densityDpi;
        }
        if (i2 == 0) {
            return 160;
        }
        return i2;
    }
}
