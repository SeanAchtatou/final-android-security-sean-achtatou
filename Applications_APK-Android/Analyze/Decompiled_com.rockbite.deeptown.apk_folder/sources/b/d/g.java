package b.d;

import java.util.ConcurrentModificationException;
import java.util.Map;

/* compiled from: SimpleArrayMap */
public class g<K, V> {

    /* renamed from: d  reason: collision with root package name */
    static Object[] f2768d;

    /* renamed from: e  reason: collision with root package name */
    static int f2769e;

    /* renamed from: f  reason: collision with root package name */
    static Object[] f2770f;

    /* renamed from: g  reason: collision with root package name */
    static int f2771g;

    /* renamed from: a  reason: collision with root package name */
    int[] f2772a;

    /* renamed from: b  reason: collision with root package name */
    Object[] f2773b;

    /* renamed from: c  reason: collision with root package name */
    int f2774c;

    public g() {
        this.f2772a = c.f2745a;
        this.f2773b = c.f2747c;
        this.f2774c = 0;
    }

    private static int a(int[] iArr, int i2, int i3) {
        try {
            return c.a(iArr, i2, i3);
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new ConcurrentModificationException();
        }
    }

    private void e(int i2) {
        Class<g> cls = g.class;
        if (i2 == 8) {
            synchronized (cls) {
                if (f2770f != null) {
                    Object[] objArr = f2770f;
                    this.f2773b = objArr;
                    f2770f = (Object[]) objArr[0];
                    this.f2772a = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    f2771g--;
                    return;
                }
            }
        } else if (i2 == 4) {
            synchronized (cls) {
                if (f2768d != null) {
                    Object[] objArr2 = f2768d;
                    this.f2773b = objArr2;
                    f2768d = (Object[]) objArr2[0];
                    this.f2772a = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    f2769e--;
                    return;
                }
            }
        }
        this.f2772a = new int[i2];
        this.f2773b = new Object[(i2 << 1)];
    }

    /* access modifiers changed from: package-private */
    public int b(Object obj) {
        int i2 = this.f2774c * 2;
        Object[] objArr = this.f2773b;
        if (obj == null) {
            for (int i3 = 1; i3 < i2; i3 += 2) {
                if (objArr[i3] == null) {
                    return i3 >> 1;
                }
            }
            return -1;
        }
        for (int i4 = 1; i4 < i2; i4 += 2) {
            if (obj.equals(objArr[i4])) {
                return i4 >> 1;
            }
        }
        return -1;
    }

    public V c(int i2) {
        int i3;
        V[] vArr = this.f2773b;
        int i4 = i2 << 1;
        V v = vArr[i4 + 1];
        int i5 = this.f2774c;
        if (i5 <= 1) {
            a(this.f2772a, vArr, i5);
            this.f2772a = c.f2745a;
            this.f2773b = c.f2747c;
            i3 = 0;
        } else {
            i3 = i5 - 1;
            int[] iArr = this.f2772a;
            int i6 = 8;
            if (iArr.length <= 8 || i5 >= iArr.length / 3) {
                if (i2 < i3) {
                    int[] iArr2 = this.f2772a;
                    int i7 = i2 + 1;
                    int i8 = i3 - i2;
                    System.arraycopy(iArr2, i7, iArr2, i2, i8);
                    Object[] objArr = this.f2773b;
                    System.arraycopy(objArr, i7 << 1, objArr, i4, i8 << 1);
                }
                Object[] objArr2 = this.f2773b;
                int i9 = i3 << 1;
                objArr2[i9] = null;
                objArr2[i9 + 1] = null;
            } else {
                if (i5 > 8) {
                    i6 = i5 + (i5 >> 1);
                }
                int[] iArr3 = this.f2772a;
                Object[] objArr3 = this.f2773b;
                e(i6);
                if (i5 == this.f2774c) {
                    if (i2 > 0) {
                        System.arraycopy(iArr3, 0, this.f2772a, 0, i2);
                        System.arraycopy(objArr3, 0, this.f2773b, 0, i4);
                    }
                    if (i2 < i3) {
                        int i10 = i2 + 1;
                        int i11 = i3 - i2;
                        System.arraycopy(iArr3, i10, this.f2772a, i2, i11);
                        System.arraycopy(objArr3, i10 << 1, this.f2773b, i4, i11 << 1);
                    }
                } else {
                    throw new ConcurrentModificationException();
                }
            }
        }
        if (i5 == this.f2774c) {
            this.f2774c = i3;
            return v;
        }
        throw new ConcurrentModificationException();
    }

    public void clear() {
        int i2 = this.f2774c;
        if (i2 > 0) {
            int[] iArr = this.f2772a;
            Object[] objArr = this.f2773b;
            this.f2772a = c.f2745a;
            this.f2773b = c.f2747c;
            this.f2774c = 0;
            a(iArr, objArr, i2);
        }
        if (this.f2774c > 0) {
            throw new ConcurrentModificationException();
        }
    }

    public boolean containsKey(Object obj) {
        return a(obj) >= 0;
    }

    public boolean containsValue(Object obj) {
        return b(obj) >= 0;
    }

    public V d(int i2) {
        return this.f2773b[(i2 << 1) + 1];
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof g) {
            g gVar = (g) obj;
            if (size() != gVar.size()) {
                return false;
            }
            int i2 = 0;
            while (i2 < this.f2774c) {
                try {
                    Object b2 = b(i2);
                    Object d2 = d(i2);
                    Object obj2 = gVar.get(b2);
                    if (d2 == null) {
                        if (obj2 != null || !gVar.containsKey(b2)) {
                            return false;
                        }
                    } else if (!d2.equals(obj2)) {
                        return false;
                    }
                    i2++;
                } catch (ClassCastException | NullPointerException unused) {
                    return false;
                }
            }
            return true;
        }
        if (obj instanceof Map) {
            Map map = (Map) obj;
            if (size() != map.size()) {
                return false;
            }
            int i3 = 0;
            while (i3 < this.f2774c) {
                try {
                    Object b3 = b(i3);
                    Object d3 = d(i3);
                    Object obj3 = map.get(b3);
                    if (d3 == null) {
                        if (obj3 != null || !map.containsKey(b3)) {
                            return false;
                        }
                    } else if (!d3.equals(obj3)) {
                        return false;
                    }
                    i3++;
                } catch (ClassCastException | NullPointerException unused2) {
                }
            }
            return true;
        }
        return false;
    }

    public V get(Object obj) {
        return getOrDefault(obj, null);
    }

    public V getOrDefault(Object obj, V v) {
        int a2 = a(obj);
        return a2 >= 0 ? this.f2773b[(a2 << 1) + 1] : v;
    }

    public int hashCode() {
        int[] iArr = this.f2772a;
        Object[] objArr = this.f2773b;
        int i2 = this.f2774c;
        int i3 = 0;
        int i4 = 0;
        int i5 = 1;
        while (i3 < i2) {
            Object obj = objArr[i5];
            i4 += (obj == null ? 0 : obj.hashCode()) ^ iArr[i3];
            i3++;
            i5 += 2;
        }
        return i4;
    }

    public boolean isEmpty() {
        return this.f2774c <= 0;
    }

    public V put(K k2, V v) {
        int i2;
        int i3;
        int i4 = this.f2774c;
        if (k2 == null) {
            i3 = a();
            i2 = 0;
        } else {
            int hashCode = k2.hashCode();
            i2 = hashCode;
            i3 = a(k2, hashCode);
        }
        if (i3 >= 0) {
            int i5 = (i3 << 1) + 1;
            V[] vArr = this.f2773b;
            V v2 = vArr[i5];
            vArr[i5] = v;
            return v2;
        }
        int i6 = i3 ^ -1;
        if (i4 >= this.f2772a.length) {
            int i7 = 4;
            if (i4 >= 8) {
                i7 = (i4 >> 1) + i4;
            } else if (i4 >= 4) {
                i7 = 8;
            }
            int[] iArr = this.f2772a;
            Object[] objArr = this.f2773b;
            e(i7);
            if (i4 == this.f2774c) {
                int[] iArr2 = this.f2772a;
                if (iArr2.length > 0) {
                    System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                    System.arraycopy(objArr, 0, this.f2773b, 0, objArr.length);
                }
                a(iArr, objArr, i4);
            } else {
                throw new ConcurrentModificationException();
            }
        }
        if (i6 < i4) {
            int[] iArr3 = this.f2772a;
            int i8 = i6 + 1;
            System.arraycopy(iArr3, i6, iArr3, i8, i4 - i6);
            Object[] objArr2 = this.f2773b;
            System.arraycopy(objArr2, i6 << 1, objArr2, i8 << 1, (this.f2774c - i6) << 1);
        }
        int i9 = this.f2774c;
        if (i4 == i9) {
            int[] iArr4 = this.f2772a;
            if (i6 < iArr4.length) {
                iArr4[i6] = i2;
                Object[] objArr3 = this.f2773b;
                int i10 = i6 << 1;
                objArr3[i10] = k2;
                objArr3[i10 + 1] = v;
                this.f2774c = i9 + 1;
                return null;
            }
        }
        throw new ConcurrentModificationException();
    }

    public V putIfAbsent(K k2, V v) {
        V v2 = get(k2);
        return v2 == null ? put(k2, v) : v2;
    }

    public V remove(Object obj) {
        int a2 = a(obj);
        if (a2 >= 0) {
            return c(a2);
        }
        return null;
    }

    public V replace(K k2, V v) {
        int a2 = a((Object) k2);
        if (a2 >= 0) {
            return a(a2, v);
        }
        return null;
    }

    public int size() {
        return this.f2774c;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f2774c * 28);
        sb.append('{');
        for (int i2 = 0; i2 < this.f2774c; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            Object b2 = b(i2);
            if (b2 != this) {
                sb.append(b2);
            } else {
                sb.append("(this Map)");
            }
            sb.append('=');
            Object d2 = d(i2);
            if (d2 != this) {
                sb.append(d2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public int a(Object obj, int i2) {
        int i3 = this.f2774c;
        if (i3 == 0) {
            return -1;
        }
        int a2 = a(this.f2772a, i3, i2);
        if (a2 < 0 || obj.equals(this.f2773b[a2 << 1])) {
            return a2;
        }
        int i4 = a2 + 1;
        while (i4 < i3 && this.f2772a[i4] == i2) {
            if (obj.equals(this.f2773b[i4 << 1])) {
                return i4;
            }
            i4++;
        }
        int i5 = a2 - 1;
        while (i5 >= 0 && this.f2772a[i5] == i2) {
            if (obj.equals(this.f2773b[i5 << 1])) {
                return i5;
            }
            i5--;
        }
        return i4 ^ -1;
    }

    public boolean remove(Object obj, Object obj2) {
        int a2 = a(obj);
        if (a2 < 0) {
            return false;
        }
        Object d2 = d(a2);
        if (obj2 != d2 && (obj2 == null || !obj2.equals(d2))) {
            return false;
        }
        c(a2);
        return true;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean replace(K r2, V r3, V r4) {
        /*
            r1 = this;
            int r2 = r1.a(r2)
            if (r2 < 0) goto L_0x0019
            java.lang.Object r0 = r1.d(r2)
            if (r0 == r3) goto L_0x0014
            if (r3 == 0) goto L_0x0019
            boolean r3 = r3.equals(r0)
            if (r3 == 0) goto L_0x0019
        L_0x0014:
            r1.a(r2, r4)
            r2 = 1
            return r2
        L_0x0019:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: b.d.g.replace(java.lang.Object, java.lang.Object, java.lang.Object):boolean");
    }

    public g(int i2) {
        if (i2 == 0) {
            this.f2772a = c.f2745a;
            this.f2773b = c.f2747c;
        } else {
            e(i2);
        }
        this.f2774c = 0;
    }

    public K b(int i2) {
        return this.f2773b[i2 << 1];
    }

    public g(g gVar) {
        this();
        if (gVar != null) {
            a(gVar);
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        int i2 = this.f2774c;
        if (i2 == 0) {
            return -1;
        }
        int a2 = a(this.f2772a, i2, 0);
        if (a2 < 0 || this.f2773b[a2 << 1] == null) {
            return a2;
        }
        int i3 = a2 + 1;
        while (i3 < i2 && this.f2772a[i3] == 0) {
            if (this.f2773b[i3 << 1] == null) {
                return i3;
            }
            i3++;
        }
        int i4 = a2 - 1;
        while (i4 >= 0 && this.f2772a[i4] == 0) {
            if (this.f2773b[i4 << 1] == null) {
                return i4;
            }
            i4--;
        }
        return i3 ^ -1;
    }

    private static void a(int[] iArr, Object[] objArr, int i2) {
        Class<g> cls = g.class;
        if (iArr.length == 8) {
            synchronized (cls) {
                if (f2771g < 10) {
                    objArr[0] = f2770f;
                    objArr[1] = iArr;
                    for (int i3 = (i2 << 1) - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    f2770f = objArr;
                    f2771g++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (cls) {
                if (f2769e < 10) {
                    objArr[0] = f2768d;
                    objArr[1] = iArr;
                    for (int i4 = (i2 << 1) - 1; i4 >= 2; i4--) {
                        objArr[i4] = null;
                    }
                    f2768d = objArr;
                    f2769e++;
                }
            }
        }
    }

    public void a(int i2) {
        int i3 = this.f2774c;
        int[] iArr = this.f2772a;
        if (iArr.length < i2) {
            Object[] objArr = this.f2773b;
            e(i2);
            if (this.f2774c > 0) {
                System.arraycopy(iArr, 0, this.f2772a, 0, i3);
                System.arraycopy(objArr, 0, this.f2773b, 0, i3 << 1);
            }
            a(iArr, objArr, i3);
        }
        if (this.f2774c != i3) {
            throw new ConcurrentModificationException();
        }
    }

    public int a(Object obj) {
        return obj == null ? a() : a(obj, obj.hashCode());
    }

    public V a(int i2, Object obj) {
        int i3 = (i2 << 1) + 1;
        Object[] objArr = this.f2773b;
        Object obj2 = objArr[i3];
        objArr[i3] = obj;
        return obj2;
    }

    public void a(g gVar) {
        int i2 = gVar.f2774c;
        a(this.f2774c + i2);
        if (this.f2774c != 0) {
            for (int i3 = 0; i3 < i2; i3++) {
                put(gVar.b(i3), gVar.d(i3));
            }
        } else if (i2 > 0) {
            System.arraycopy(gVar.f2772a, 0, this.f2772a, 0, i2);
            System.arraycopy(gVar.f2773b, 0, this.f2773b, 0, i2 << 1);
            this.f2774c = i2;
        }
    }
}
