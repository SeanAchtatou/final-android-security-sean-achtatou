package b.d;

/* compiled from: LongSparseArray */
public class d<E> implements Cloneable {

    /* renamed from: e  reason: collision with root package name */
    private static final Object f2748e = new Object();

    /* renamed from: a  reason: collision with root package name */
    private boolean f2749a;

    /* renamed from: b  reason: collision with root package name */
    private long[] f2750b;

    /* renamed from: c  reason: collision with root package name */
    private Object[] f2751c;

    /* renamed from: d  reason: collision with root package name */
    private int f2752d;

    public d() {
        this(10);
    }

    @Deprecated
    public void a(long j2) {
        c(j2);
    }

    public E b(long j2) {
        return b(j2, null);
    }

    public void c(long j2) {
        Object[] objArr;
        Object obj;
        int a2 = c.a(this.f2750b, this.f2752d, j2);
        if (a2 >= 0 && (objArr = this.f2751c)[a2] != (obj = f2748e)) {
            objArr[a2] = obj;
            this.f2749a = true;
        }
    }

    public void clear() {
        int i2 = this.f2752d;
        Object[] objArr = this.f2751c;
        for (int i3 = 0; i3 < i2; i3++) {
            objArr[i3] = null;
        }
        this.f2752d = 0;
        this.f2749a = false;
    }

    public String toString() {
        if (a() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f2752d * 28);
        sb.append('{');
        for (int i2 = 0; i2 < this.f2752d; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            sb.append(a(i2));
            sb.append('=');
            Object c2 = c(i2);
            if (c2 != this) {
                sb.append(c2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public d(int i2) {
        this.f2749a = false;
        if (i2 == 0) {
            this.f2750b = c.f2746b;
            this.f2751c = c.f2747c;
            return;
        }
        int c2 = c.c(i2);
        this.f2750b = new long[c2];
        this.f2751c = new Object[c2];
    }

    public int a() {
        if (this.f2749a) {
            b();
        }
        return this.f2752d;
    }

    public E b(long j2, E e2) {
        int a2 = c.a(this.f2750b, this.f2752d, j2);
        if (a2 >= 0) {
            E[] eArr = this.f2751c;
            if (eArr[a2] != f2748e) {
                return eArr[a2];
            }
        }
        return e2;
    }

    public d<E> clone() {
        try {
            d<E> dVar = (d) super.clone();
            dVar.f2750b = (long[]) this.f2750b.clone();
            dVar.f2751c = (Object[]) this.f2751c.clone();
            return dVar;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    public long a(int i2) {
        if (this.f2749a) {
            b();
        }
        return this.f2750b[i2];
    }

    public void b(int i2) {
        Object[] objArr = this.f2751c;
        Object obj = objArr[i2];
        Object obj2 = f2748e;
        if (obj != obj2) {
            objArr[i2] = obj2;
            this.f2749a = true;
        }
    }

    public void c(long j2, E e2) {
        int a2 = c.a(this.f2750b, this.f2752d, j2);
        if (a2 >= 0) {
            this.f2751c[a2] = e2;
            return;
        }
        int i2 = a2 ^ -1;
        if (i2 < this.f2752d) {
            Object[] objArr = this.f2751c;
            if (objArr[i2] == f2748e) {
                this.f2750b[i2] = j2;
                objArr[i2] = e2;
                return;
            }
        }
        if (this.f2749a && this.f2752d >= this.f2750b.length) {
            b();
            i2 = c.a(this.f2750b, this.f2752d, j2) ^ -1;
        }
        int i3 = this.f2752d;
        if (i3 >= this.f2750b.length) {
            int c2 = c.c(i3 + 1);
            long[] jArr = new long[c2];
            Object[] objArr2 = new Object[c2];
            long[] jArr2 = this.f2750b;
            System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
            Object[] objArr3 = this.f2751c;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.f2750b = jArr;
            this.f2751c = objArr2;
        }
        int i4 = this.f2752d;
        if (i4 - i2 != 0) {
            long[] jArr3 = this.f2750b;
            int i5 = i2 + 1;
            System.arraycopy(jArr3, i2, jArr3, i5, i4 - i2);
            Object[] objArr4 = this.f2751c;
            System.arraycopy(objArr4, i2, objArr4, i5, this.f2752d - i2);
        }
        this.f2750b[i2] = j2;
        this.f2751c[i2] = e2;
        this.f2752d++;
    }

    private void b() {
        int i2 = this.f2752d;
        long[] jArr = this.f2750b;
        Object[] objArr = this.f2751c;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            Object obj = objArr[i4];
            if (obj != f2748e) {
                if (i4 != i3) {
                    jArr[i3] = jArr[i4];
                    objArr[i3] = obj;
                    objArr[i4] = null;
                }
                i3++;
            }
        }
        this.f2749a = false;
        this.f2752d = i3;
    }

    public void a(long j2, E e2) {
        int i2 = this.f2752d;
        if (i2 == 0 || j2 > this.f2750b[i2 - 1]) {
            if (this.f2749a && this.f2752d >= this.f2750b.length) {
                b();
            }
            int i3 = this.f2752d;
            if (i3 >= this.f2750b.length) {
                int c2 = c.c(i3 + 1);
                long[] jArr = new long[c2];
                Object[] objArr = new Object[c2];
                long[] jArr2 = this.f2750b;
                System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
                Object[] objArr2 = this.f2751c;
                System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.f2750b = jArr;
                this.f2751c = objArr;
            }
            this.f2750b[i3] = j2;
            this.f2751c[i3] = e2;
            this.f2752d = i3 + 1;
            return;
        }
        c(j2, e2);
    }

    public E c(int i2) {
        if (this.f2749a) {
            b();
        }
        return this.f2751c[i2];
    }
}
