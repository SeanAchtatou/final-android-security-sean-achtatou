package b.d;

/* compiled from: SparseArrayCompat */
public class h<E> implements Cloneable {

    /* renamed from: e  reason: collision with root package name */
    private static final Object f2775e = new Object();

    /* renamed from: a  reason: collision with root package name */
    private boolean f2776a;

    /* renamed from: b  reason: collision with root package name */
    private int[] f2777b;

    /* renamed from: c  reason: collision with root package name */
    private Object[] f2778c;

    /* renamed from: d  reason: collision with root package name */
    private int f2779d;

    public h() {
        this(10);
    }

    public E a(int i2) {
        return b(i2, null);
    }

    public E b(int i2, E e2) {
        int a2 = c.a(this.f2777b, this.f2779d, i2);
        if (a2 >= 0) {
            E[] eArr = this.f2778c;
            if (eArr[a2] != f2775e) {
                return eArr[a2];
            }
        }
        return e2;
    }

    public void c(int i2, E e2) {
        int a2 = c.a(this.f2777b, this.f2779d, i2);
        if (a2 >= 0) {
            this.f2778c[a2] = e2;
            return;
        }
        int i3 = a2 ^ -1;
        if (i3 < this.f2779d) {
            Object[] objArr = this.f2778c;
            if (objArr[i3] == f2775e) {
                this.f2777b[i3] = i2;
                objArr[i3] = e2;
                return;
            }
        }
        if (this.f2776a && this.f2779d >= this.f2777b.length) {
            b();
            i3 = c.a(this.f2777b, this.f2779d, i2) ^ -1;
        }
        int i4 = this.f2779d;
        if (i4 >= this.f2777b.length) {
            int b2 = c.b(i4 + 1);
            int[] iArr = new int[b2];
            Object[] objArr2 = new Object[b2];
            int[] iArr2 = this.f2777b;
            System.arraycopy(iArr2, 0, iArr, 0, iArr2.length);
            Object[] objArr3 = this.f2778c;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.f2777b = iArr;
            this.f2778c = objArr2;
        }
        int i5 = this.f2779d;
        if (i5 - i3 != 0) {
            int[] iArr3 = this.f2777b;
            int i6 = i3 + 1;
            System.arraycopy(iArr3, i3, iArr3, i6, i5 - i3);
            Object[] objArr4 = this.f2778c;
            System.arraycopy(objArr4, i3, objArr4, i6, this.f2779d - i3);
        }
        this.f2777b[i3] = i2;
        this.f2778c[i3] = e2;
        this.f2779d++;
    }

    public void clear() {
        int i2 = this.f2779d;
        Object[] objArr = this.f2778c;
        for (int i3 = 0; i3 < i2; i3++) {
            objArr[i3] = null;
        }
        this.f2779d = 0;
        this.f2776a = false;
    }

    public void d(int i2) {
        Object[] objArr;
        Object obj;
        int a2 = c.a(this.f2777b, this.f2779d, i2);
        if (a2 >= 0 && (objArr = this.f2778c)[a2] != (obj = f2775e)) {
            objArr[a2] = obj;
            this.f2776a = true;
        }
    }

    public E e(int i2) {
        if (this.f2776a) {
            b();
        }
        return this.f2778c[i2];
    }

    public String toString() {
        if (a() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f2779d * 28);
        sb.append('{');
        for (int i2 = 0; i2 < this.f2779d; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            sb.append(c(i2));
            sb.append('=');
            Object e2 = e(i2);
            if (e2 != this) {
                sb.append(e2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public h(int i2) {
        this.f2776a = false;
        if (i2 == 0) {
            this.f2777b = c.f2745a;
            this.f2778c = c.f2747c;
            return;
        }
        int b2 = c.b(i2);
        this.f2777b = new int[b2];
        this.f2778c = new Object[b2];
    }

    public int a() {
        if (this.f2776a) {
            b();
        }
        return this.f2779d;
    }

    public h<E> clone() {
        try {
            h<E> hVar = (h) super.clone();
            hVar.f2777b = (int[]) this.f2777b.clone();
            hVar.f2778c = (Object[]) this.f2778c.clone();
            return hVar;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    private void b() {
        int i2 = this.f2779d;
        int[] iArr = this.f2777b;
        Object[] objArr = this.f2778c;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            Object obj = objArr[i4];
            if (obj != f2775e) {
                if (i4 != i3) {
                    iArr[i3] = iArr[i4];
                    objArr[i3] = obj;
                    objArr[i4] = null;
                }
                i3++;
            }
        }
        this.f2776a = false;
        this.f2779d = i3;
    }

    public void a(int i2, E e2) {
        int i3 = this.f2779d;
        if (i3 == 0 || i2 > this.f2777b[i3 - 1]) {
            if (this.f2776a && this.f2779d >= this.f2777b.length) {
                b();
            }
            int i4 = this.f2779d;
            if (i4 >= this.f2777b.length) {
                int b2 = c.b(i4 + 1);
                int[] iArr = new int[b2];
                Object[] objArr = new Object[b2];
                int[] iArr2 = this.f2777b;
                System.arraycopy(iArr2, 0, iArr, 0, iArr2.length);
                Object[] objArr2 = this.f2778c;
                System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.f2777b = iArr;
                this.f2778c = objArr;
            }
            this.f2777b[i4] = i2;
            this.f2778c[i4] = e2;
            this.f2779d = i4 + 1;
            return;
        }
        c(i2, e2);
    }

    public int b(int i2) {
        if (this.f2776a) {
            b();
        }
        return c.a(this.f2777b, this.f2779d, i2);
    }

    public int c(int i2) {
        if (this.f2776a) {
            b();
        }
        return this.f2777b[i2];
    }
}
