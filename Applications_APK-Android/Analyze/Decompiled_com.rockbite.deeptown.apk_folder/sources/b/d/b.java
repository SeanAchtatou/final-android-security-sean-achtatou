package b.d;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* compiled from: ArraySet */
public final class b<E> implements Collection<E>, Set<E> {

    /* renamed from: e  reason: collision with root package name */
    private static final int[] f2734e = new int[0];

    /* renamed from: f  reason: collision with root package name */
    private static final Object[] f2735f = new Object[0];

    /* renamed from: g  reason: collision with root package name */
    private static Object[] f2736g;

    /* renamed from: h  reason: collision with root package name */
    private static int f2737h;

    /* renamed from: i  reason: collision with root package name */
    private static Object[] f2738i;

    /* renamed from: j  reason: collision with root package name */
    private static int f2739j;

    /* renamed from: a  reason: collision with root package name */
    private int[] f2740a;

    /* renamed from: b  reason: collision with root package name */
    Object[] f2741b;

    /* renamed from: c  reason: collision with root package name */
    int f2742c;

    /* renamed from: d  reason: collision with root package name */
    private f<E, E> f2743d;

    /* compiled from: ArraySet */
    class a extends f<E, E> {
        a() {
        }

        /* access modifiers changed from: protected */
        public Object a(int i2, int i3) {
            return b.this.f2741b[i2];
        }

        /* access modifiers changed from: protected */
        public int b(Object obj) {
            return b.this.indexOf(obj);
        }

        /* access modifiers changed from: protected */
        public int c() {
            return b.this.f2742c;
        }

        /* access modifiers changed from: protected */
        public int a(Object obj) {
            return b.this.indexOf(obj);
        }

        /* access modifiers changed from: protected */
        public Map<E, E> b() {
            throw new UnsupportedOperationException("not a map");
        }

        /* access modifiers changed from: protected */
        public void a(E e2, E e3) {
            b.this.add(e2);
        }

        /* access modifiers changed from: protected */
        public E a(int i2, E e2) {
            throw new UnsupportedOperationException("not a map");
        }

        /* access modifiers changed from: protected */
        public void a(int i2) {
            b.this.d(i2);
        }

        /* access modifiers changed from: protected */
        public void a() {
            b.this.clear();
        }
    }

    public b() {
        this(0);
    }

    private int a(Object obj, int i2) {
        int i3 = this.f2742c;
        if (i3 == 0) {
            return -1;
        }
        int a2 = c.a(this.f2740a, i3, i2);
        if (a2 < 0 || obj.equals(this.f2741b[a2])) {
            return a2;
        }
        int i4 = a2 + 1;
        while (i4 < i3 && this.f2740a[i4] == i2) {
            if (obj.equals(this.f2741b[i4])) {
                return i4;
            }
            i4++;
        }
        int i5 = a2 - 1;
        while (i5 >= 0 && this.f2740a[i5] == i2) {
            if (obj.equals(this.f2741b[i5])) {
                return i5;
            }
            i5--;
        }
        return i4 ^ -1;
    }

    private int b() {
        int i2 = this.f2742c;
        if (i2 == 0) {
            return -1;
        }
        int a2 = c.a(this.f2740a, i2, 0);
        if (a2 < 0 || this.f2741b[a2] == null) {
            return a2;
        }
        int i3 = a2 + 1;
        while (i3 < i2 && this.f2740a[i3] == 0) {
            if (this.f2741b[i3] == null) {
                return i3;
            }
            i3++;
        }
        int i4 = a2 - 1;
        while (i4 >= 0 && this.f2740a[i4] == 0) {
            if (this.f2741b[i4] == null) {
                return i4;
            }
            i4--;
        }
        return i3 ^ -1;
    }

    private void f(int i2) {
        if (i2 == 8) {
            synchronized (b.class) {
                if (f2738i != null) {
                    Object[] objArr = f2738i;
                    this.f2741b = objArr;
                    f2738i = (Object[]) objArr[0];
                    this.f2740a = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    f2739j--;
                    return;
                }
            }
        } else if (i2 == 4) {
            synchronized (b.class) {
                if (f2736g != null) {
                    Object[] objArr2 = f2736g;
                    this.f2741b = objArr2;
                    f2736g = (Object[]) objArr2[0];
                    this.f2740a = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    f2737h--;
                    return;
                }
            }
        }
        this.f2740a = new int[i2];
        this.f2741b = new Object[i2];
    }

    public boolean add(E e2) {
        int i2;
        int i3;
        if (e2 == null) {
            i3 = b();
            i2 = 0;
        } else {
            int hashCode = e2.hashCode();
            i2 = hashCode;
            i3 = a(e2, hashCode);
        }
        if (i3 >= 0) {
            return false;
        }
        int i4 = i3 ^ -1;
        int i5 = this.f2742c;
        if (i5 >= this.f2740a.length) {
            int i6 = 4;
            if (i5 >= 8) {
                i6 = (i5 >> 1) + i5;
            } else if (i5 >= 4) {
                i6 = 8;
            }
            int[] iArr = this.f2740a;
            Object[] objArr = this.f2741b;
            f(i6);
            int[] iArr2 = this.f2740a;
            if (iArr2.length > 0) {
                System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                System.arraycopy(objArr, 0, this.f2741b, 0, objArr.length);
            }
            a(iArr, objArr, this.f2742c);
        }
        int i7 = this.f2742c;
        if (i4 < i7) {
            int[] iArr3 = this.f2740a;
            int i8 = i4 + 1;
            System.arraycopy(iArr3, i4, iArr3, i8, i7 - i4);
            Object[] objArr2 = this.f2741b;
            System.arraycopy(objArr2, i4, objArr2, i8, this.f2742c - i4);
        }
        this.f2740a[i4] = i2;
        this.f2741b[i4] = e2;
        this.f2742c++;
        return true;
    }

    public boolean addAll(Collection<? extends E> collection) {
        c(this.f2742c + collection.size());
        boolean z = false;
        for (Object add : collection) {
            z |= add(add);
        }
        return z;
    }

    public void c(int i2) {
        int[] iArr = this.f2740a;
        if (iArr.length < i2) {
            Object[] objArr = this.f2741b;
            f(i2);
            int i3 = this.f2742c;
            if (i3 > 0) {
                System.arraycopy(iArr, 0, this.f2740a, 0, i3);
                System.arraycopy(objArr, 0, this.f2741b, 0, this.f2742c);
            }
            a(iArr, objArr, this.f2742c);
        }
    }

    public void clear() {
        int i2 = this.f2742c;
        if (i2 != 0) {
            a(this.f2740a, this.f2741b, i2);
            this.f2740a = f2734e;
            this.f2741b = f2735f;
            this.f2742c = 0;
        }
    }

    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    public boolean containsAll(Collection<?> collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public E d(int i2) {
        E[] eArr = this.f2741b;
        E e2 = eArr[i2];
        int i3 = this.f2742c;
        if (i3 <= 1) {
            a(this.f2740a, eArr, i3);
            this.f2740a = f2734e;
            this.f2741b = f2735f;
            this.f2742c = 0;
        } else {
            int[] iArr = this.f2740a;
            int i4 = 8;
            if (iArr.length <= 8 || i3 >= iArr.length / 3) {
                this.f2742c--;
                int i5 = this.f2742c;
                if (i2 < i5) {
                    int[] iArr2 = this.f2740a;
                    int i6 = i2 + 1;
                    System.arraycopy(iArr2, i6, iArr2, i2, i5 - i2);
                    Object[] objArr = this.f2741b;
                    System.arraycopy(objArr, i6, objArr, i2, this.f2742c - i2);
                }
                this.f2741b[this.f2742c] = null;
            } else {
                if (i3 > 8) {
                    i4 = i3 + (i3 >> 1);
                }
                int[] iArr3 = this.f2740a;
                Object[] objArr2 = this.f2741b;
                f(i4);
                this.f2742c--;
                if (i2 > 0) {
                    System.arraycopy(iArr3, 0, this.f2740a, 0, i2);
                    System.arraycopy(objArr2, 0, this.f2741b, 0, i2);
                }
                int i7 = this.f2742c;
                if (i2 < i7) {
                    int i8 = i2 + 1;
                    System.arraycopy(iArr3, i8, this.f2740a, i2, i7 - i2);
                    System.arraycopy(objArr2, i8, this.f2741b, i2, this.f2742c - i2);
                }
            }
        }
        return e2;
    }

    public E e(int i2) {
        return this.f2741b[i2];
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set = (Set) obj;
            if (size() != set.size()) {
                return false;
            }
            int i2 = 0;
            while (i2 < this.f2742c) {
                try {
                    if (!set.contains(e(i2))) {
                        return false;
                    }
                    i2++;
                } catch (ClassCastException | NullPointerException unused) {
                }
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        int[] iArr = this.f2740a;
        int i2 = this.f2742c;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            i3 += iArr[i4];
        }
        return i3;
    }

    public int indexOf(Object obj) {
        return obj == null ? b() : a(obj, obj.hashCode());
    }

    public boolean isEmpty() {
        return this.f2742c <= 0;
    }

    public Iterator<E> iterator() {
        return a().e().iterator();
    }

    public boolean remove(Object obj) {
        int indexOf = indexOf(obj);
        if (indexOf < 0) {
            return false;
        }
        d(indexOf);
        return true;
    }

    public boolean removeAll(Collection<?> collection) {
        boolean z = false;
        for (Object remove : collection) {
            z |= remove(remove);
        }
        return z;
    }

    public boolean retainAll(Collection<?> collection) {
        boolean z = false;
        for (int i2 = this.f2742c - 1; i2 >= 0; i2--) {
            if (!collection.contains(this.f2741b[i2])) {
                d(i2);
                z = true;
            }
        }
        return z;
    }

    public int size() {
        return this.f2742c;
    }

    public Object[] toArray() {
        int i2 = this.f2742c;
        Object[] objArr = new Object[i2];
        System.arraycopy(this.f2741b, 0, objArr, 0, i2);
        return objArr;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f2742c * 14);
        sb.append('{');
        for (int i2 = 0; i2 < this.f2742c; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            Object e2 = e(i2);
            if (e2 != this) {
                sb.append(e2);
            } else {
                sb.append("(this Set)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public b(int i2) {
        if (i2 == 0) {
            this.f2740a = f2734e;
            this.f2741b = f2735f;
        } else {
            f(i2);
        }
        this.f2742c = 0;
    }

    public <T> T[] toArray(T[] tArr) {
        if (tArr.length < this.f2742c) {
            tArr = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), this.f2742c);
        }
        System.arraycopy(this.f2741b, 0, tArr, 0, this.f2742c);
        int length = tArr.length;
        int i2 = this.f2742c;
        if (length > i2) {
            tArr[i2] = null;
        }
        return tArr;
    }

    private static void a(int[] iArr, Object[] objArr, int i2) {
        if (iArr.length == 8) {
            synchronized (b.class) {
                if (f2739j < 10) {
                    objArr[0] = f2738i;
                    objArr[1] = iArr;
                    for (int i3 = i2 - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    f2738i = objArr;
                    f2739j++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (b.class) {
                if (f2737h < 10) {
                    objArr[0] = f2736g;
                    objArr[1] = iArr;
                    for (int i4 = i2 - 1; i4 >= 2; i4--) {
                        objArr[i4] = null;
                    }
                    f2736g = objArr;
                    f2737h++;
                }
            }
        }
    }

    private f<E, E> a() {
        if (this.f2743d == null) {
            this.f2743d = new a();
        }
        return this.f2743d;
    }
}
