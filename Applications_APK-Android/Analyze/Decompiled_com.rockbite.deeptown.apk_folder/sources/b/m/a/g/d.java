package b.m.a.g;

import android.database.sqlite.SQLiteProgram;

/* compiled from: FrameworkSQLiteProgram */
class d implements b.m.a.d {

    /* renamed from: a  reason: collision with root package name */
    private final SQLiteProgram f3066a;

    d(SQLiteProgram sQLiteProgram) {
        this.f3066a = sQLiteProgram;
    }

    public void a(int i2, long j2) {
        this.f3066a.bindLong(i2, j2);
    }

    public void c(int i2) {
        this.f3066a.bindNull(i2);
    }

    public void close() {
        this.f3066a.close();
    }

    public void a(int i2, double d2) {
        this.f3066a.bindDouble(i2, d2);
    }

    public void a(int i2, String str) {
        this.f3066a.bindString(i2, str);
    }

    public void a(int i2, byte[] bArr) {
        this.f3066a.bindBlob(i2, bArr);
    }
}
