package b.m.a.g;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import b.m.a.c;
import java.io.File;

/* compiled from: FrameworkSQLiteOpenHelper */
class b implements c {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3054a;

    /* renamed from: b  reason: collision with root package name */
    private final String f3055b;

    /* renamed from: c  reason: collision with root package name */
    private final c.a f3056c;

    /* renamed from: d  reason: collision with root package name */
    private final boolean f3057d;

    /* renamed from: e  reason: collision with root package name */
    private final Object f3058e = new Object();

    /* renamed from: f  reason: collision with root package name */
    private a f3059f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f3060g;

    b(Context context, String str, c.a aVar, boolean z) {
        this.f3054a = context;
        this.f3055b = str;
        this.f3056c = aVar;
        this.f3057d = z;
    }

    private a a() {
        a aVar;
        synchronized (this.f3058e) {
            if (this.f3059f == null) {
                a[] aVarArr = new a[1];
                if (Build.VERSION.SDK_INT < 23 || this.f3055b == null || !this.f3057d) {
                    this.f3059f = new a(this.f3054a, this.f3055b, aVarArr, this.f3056c);
                } else {
                    this.f3059f = new a(this.f3054a, new File(this.f3054a.getNoBackupFilesDir(), this.f3055b).getAbsolutePath(), aVarArr, this.f3056c);
                }
                if (Build.VERSION.SDK_INT >= 16) {
                    this.f3059f.setWriteAheadLoggingEnabled(this.f3060g);
                }
            }
            aVar = this.f3059f;
        }
        return aVar;
    }

    public b.m.a.b H() {
        return a().a();
    }

    public String I() {
        return this.f3055b;
    }

    public void b(boolean z) {
        synchronized (this.f3058e) {
            if (this.f3059f != null) {
                this.f3059f.setWriteAheadLoggingEnabled(z);
            }
            this.f3060g = z;
        }
    }

    public void close() {
        a().close();
    }

    /* compiled from: FrameworkSQLiteOpenHelper */
    static class a extends SQLiteOpenHelper {

        /* renamed from: a  reason: collision with root package name */
        final a[] f3061a;

        /* renamed from: b  reason: collision with root package name */
        final c.a f3062b;

        /* renamed from: c  reason: collision with root package name */
        private boolean f3063c;

        /* renamed from: b.m.a.g.b$a$a  reason: collision with other inner class name */
        /* compiled from: FrameworkSQLiteOpenHelper */
        class C0065a implements DatabaseErrorHandler {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ c.a f3064a;

            /* renamed from: b  reason: collision with root package name */
            final /* synthetic */ a[] f3065b;

            C0065a(c.a aVar, a[] aVarArr) {
                this.f3064a = aVar;
                this.f3065b = aVarArr;
            }

            public void onCorruption(SQLiteDatabase sQLiteDatabase) {
                this.f3064a.b(a.a(this.f3065b, sQLiteDatabase));
            }
        }

        a(Context context, String str, a[] aVarArr, c.a aVar) {
            super(context, str, null, aVar.f3041a, new C0065a(aVar, aVarArr));
            this.f3062b = aVar;
            this.f3061a = aVarArr;
        }

        /* access modifiers changed from: package-private */
        public synchronized b.m.a.b a() {
            this.f3063c = false;
            SQLiteDatabase writableDatabase = super.getWritableDatabase();
            if (this.f3063c) {
                close();
                return a();
            }
            return a(writableDatabase);
        }

        public synchronized void close() {
            super.close();
            this.f3061a[0] = null;
        }

        public void onConfigure(SQLiteDatabase sQLiteDatabase) {
            this.f3062b.a(a(sQLiteDatabase));
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            this.f3062b.c(a(sQLiteDatabase));
        }

        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i2, int i3) {
            this.f3063c = true;
            this.f3062b.a(a(sQLiteDatabase), i2, i3);
        }

        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (!this.f3063c) {
                this.f3062b.d(a(sQLiteDatabase));
            }
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i2, int i3) {
            this.f3063c = true;
            this.f3062b.b(a(sQLiteDatabase), i2, i3);
        }

        /* access modifiers changed from: package-private */
        public a a(SQLiteDatabase sQLiteDatabase) {
            return a(this.f3061a, sQLiteDatabase);
        }

        static a a(a[] aVarArr, SQLiteDatabase sQLiteDatabase) {
            a aVar = aVarArr[0];
            if (aVar == null || !aVar.a(sQLiteDatabase)) {
                aVarArr[0] = new a(sQLiteDatabase);
            }
            return aVarArr[0];
        }
    }
}
