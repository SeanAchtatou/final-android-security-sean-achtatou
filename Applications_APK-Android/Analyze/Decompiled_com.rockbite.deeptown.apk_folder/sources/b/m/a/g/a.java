package b.m.a.g;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.os.CancellationSignal;
import android.util.Pair;
import b.m.a.e;
import b.m.a.f;
import java.io.IOException;
import java.util.List;

/* compiled from: FrameworkSQLiteDatabase */
class a implements b.m.a.b {

    /* renamed from: b  reason: collision with root package name */
    private static final String[] f3050b = new String[0];

    /* renamed from: a  reason: collision with root package name */
    private final SQLiteDatabase f3051a;

    /* renamed from: b.m.a.g.a$a  reason: collision with other inner class name */
    /* compiled from: FrameworkSQLiteDatabase */
    class C0064a implements SQLiteDatabase.CursorFactory {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ e f3052a;

        C0064a(a aVar, e eVar) {
            this.f3052a = eVar;
        }

        public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
            this.f3052a.a(new d(sQLiteQuery));
            return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
        }
    }

    /* compiled from: FrameworkSQLiteDatabase */
    class b implements SQLiteDatabase.CursorFactory {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ e f3053a;

        b(a aVar, e eVar) {
            this.f3053a = eVar;
        }

        public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
            this.f3053a.a(new d(sQLiteQuery));
            return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
        }
    }

    static {
        String[] strArr = {"", " OR ROLLBACK ", " OR ABORT ", " OR FAIL ", " OR IGNORE ", " OR REPLACE "};
    }

    a(SQLiteDatabase sQLiteDatabase) {
        this.f3051a = sQLiteDatabase;
    }

    public void J() {
        this.f3051a.beginTransaction();
    }

    public List<Pair<String, String>> K() {
        return this.f3051a.getAttachedDbs();
    }

    public void L() {
        this.f3051a.setTransactionSuccessful();
    }

    public void M() {
        this.f3051a.endTransaction();
    }

    public String N() {
        return this.f3051a.getPath();
    }

    public boolean O() {
        return this.f3051a.inTransaction();
    }

    public Cursor a(e eVar) {
        return this.f3051a.rawQueryWithFactory(new C0064a(this, eVar), eVar.a(), f3050b, null);
    }

    public void close() throws IOException {
        this.f3051a.close();
    }

    public void e(String str) throws SQLException {
        this.f3051a.execSQL(str);
    }

    public f f(String str) {
        return new e(this.f3051a.compileStatement(str));
    }

    public Cursor g(String str) {
        return a(new b.m.a.a(str));
    }

    public boolean isOpen() {
        return this.f3051a.isOpen();
    }

    public Cursor a(e eVar, CancellationSignal cancellationSignal) {
        return this.f3051a.rawQueryWithFactory(new b(this, eVar), eVar.a(), f3050b, null, cancellationSignal);
    }

    public void a(String str, Object[] objArr) throws SQLException {
        this.f3051a.execSQL(str, objArr);
    }

    /* access modifiers changed from: package-private */
    public boolean a(SQLiteDatabase sQLiteDatabase) {
        return this.f3051a == sQLiteDatabase;
    }
}
