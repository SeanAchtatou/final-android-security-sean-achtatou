package b.m.a.g;

import android.database.sqlite.SQLiteStatement;
import b.m.a.f;

/* compiled from: FrameworkSQLiteStatement */
class e extends d implements f {

    /* renamed from: b  reason: collision with root package name */
    private final SQLiteStatement f3067b;

    e(SQLiteStatement sQLiteStatement) {
        super(sQLiteStatement);
        this.f3067b = sQLiteStatement;
    }

    public int F() {
        return this.f3067b.executeUpdateDelete();
    }

    public long G() {
        return this.f3067b.executeInsert();
    }
}
