package b.m.a;

import android.database.Cursor;
import android.database.SQLException;
import android.os.CancellationSignal;
import android.util.Pair;
import java.io.Closeable;
import java.util.List;

/* compiled from: SupportSQLiteDatabase */
public interface b extends Closeable {
    void J();

    List<Pair<String, String>> K();

    void L();

    void M();

    String N();

    boolean O();

    Cursor a(e eVar);

    Cursor a(e eVar, CancellationSignal cancellationSignal);

    void a(String str, Object[] objArr) throws SQLException;

    void e(String str) throws SQLException;

    f f(String str);

    Cursor g(String str);

    boolean isOpen();
}
