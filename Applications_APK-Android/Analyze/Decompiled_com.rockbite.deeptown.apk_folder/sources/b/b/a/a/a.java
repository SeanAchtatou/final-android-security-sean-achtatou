package b.b.a.a;

import java.util.concurrent.Executor;

/* compiled from: ArchTaskExecutor */
public class a extends c {

    /* renamed from: c  reason: collision with root package name */
    private static volatile a f2681c;

    /* renamed from: d  reason: collision with root package name */
    private static final Executor f2682d = new b();

    /* renamed from: a  reason: collision with root package name */
    private c f2683a = this.f2684b;

    /* renamed from: b  reason: collision with root package name */
    private c f2684b = new b();

    /* renamed from: b.b.a.a.a$a  reason: collision with other inner class name */
    /* compiled from: ArchTaskExecutor */
    static class C0039a implements Executor {
        C0039a() {
        }

        public void execute(Runnable runnable) {
            a.c().b(runnable);
        }
    }

    /* compiled from: ArchTaskExecutor */
    static class b implements Executor {
        b() {
        }

        public void execute(Runnable runnable) {
            a.c().a(runnable);
        }
    }

    static {
        new C0039a();
    }

    private a() {
    }

    public static a c() {
        if (f2681c != null) {
            return f2681c;
        }
        synchronized (a.class) {
            if (f2681c == null) {
                f2681c = new a();
            }
        }
        return f2681c;
    }

    public void a(Runnable runnable) {
        this.f2683a.a(runnable);
    }

    public void b(Runnable runnable) {
        this.f2683a.b(runnable);
    }

    public static Executor b() {
        return f2682d;
    }

    public boolean a() {
        return this.f2683a.a();
    }
}
