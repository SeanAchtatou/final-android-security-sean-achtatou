package b.b.a.b;

import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

/* compiled from: SafeIterableMap */
public class b<K, V> implements Iterable<Map.Entry<K, V>> {

    /* renamed from: a  reason: collision with root package name */
    c<K, V> f2690a;

    /* renamed from: b  reason: collision with root package name */
    private c<K, V> f2691b;

    /* renamed from: c  reason: collision with root package name */
    private WeakHashMap<f<K, V>, Boolean> f2692c = new WeakHashMap<>();

    /* renamed from: d  reason: collision with root package name */
    private int f2693d = 0;

    /* compiled from: SafeIterableMap */
    static class a<K, V> extends e<K, V> {
        a(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        /* access modifiers changed from: package-private */
        public c<K, V> b(c<K, V> cVar) {
            return cVar.f2697d;
        }

        /* access modifiers changed from: package-private */
        public c<K, V> c(c<K, V> cVar) {
            return cVar.f2696c;
        }
    }

    /* renamed from: b.b.a.b.b$b  reason: collision with other inner class name */
    /* compiled from: SafeIterableMap */
    private static class C0040b<K, V> extends e<K, V> {
        C0040b(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        /* access modifiers changed from: package-private */
        public c<K, V> b(c<K, V> cVar) {
            return cVar.f2696c;
        }

        /* access modifiers changed from: package-private */
        public c<K, V> c(c<K, V> cVar) {
            return cVar.f2697d;
        }
    }

    /* compiled from: SafeIterableMap */
    static class c<K, V> implements Map.Entry<K, V> {

        /* renamed from: a  reason: collision with root package name */
        final K f2694a;

        /* renamed from: b  reason: collision with root package name */
        final V f2695b;

        /* renamed from: c  reason: collision with root package name */
        c<K, V> f2696c;

        /* renamed from: d  reason: collision with root package name */
        c<K, V> f2697d;

        c(K k2, V v) {
            this.f2694a = k2;
            this.f2695b = v;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean equals(java.lang.Object r5) {
            /*
                r4 = this;
                r0 = 1
                if (r5 != r4) goto L_0x0004
                return r0
            L_0x0004:
                boolean r1 = r5 instanceof b.b.a.b.b.c
                r2 = 0
                if (r1 != 0) goto L_0x000a
                return r2
            L_0x000a:
                b.b.a.b.b$c r5 = (b.b.a.b.b.c) r5
                K r1 = r4.f2694a
                K r3 = r5.f2694a
                boolean r1 = r1.equals(r3)
                if (r1 == 0) goto L_0x0021
                V r1 = r4.f2695b
                V r5 = r5.f2695b
                boolean r5 = r1.equals(r5)
                if (r5 == 0) goto L_0x0021
                goto L_0x0022
            L_0x0021:
                r0 = 0
            L_0x0022:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.b.b.c.equals(java.lang.Object):boolean");
        }

        public K getKey() {
            return this.f2694a;
        }

        public V getValue() {
            return this.f2695b;
        }

        public int hashCode() {
            return this.f2694a.hashCode() ^ this.f2695b.hashCode();
        }

        public V setValue(V v) {
            throw new UnsupportedOperationException("An entry modification is not supported");
        }

        public String toString() {
            return ((Object) this.f2694a) + "=" + ((Object) this.f2695b);
        }
    }

    /* compiled from: SafeIterableMap */
    private class d implements Iterator<Map.Entry<K, V>>, f<K, V> {

        /* renamed from: a  reason: collision with root package name */
        private c<K, V> f2698a;

        /* renamed from: b  reason: collision with root package name */
        private boolean f2699b = true;

        d() {
        }

        public void a(c<K, V> cVar) {
            c<K, V> cVar2 = this.f2698a;
            if (cVar == cVar2) {
                this.f2698a = cVar2.f2697d;
                this.f2699b = this.f2698a == null;
            }
        }

        public boolean hasNext() {
            if (!this.f2699b) {
                c<K, V> cVar = this.f2698a;
                if (cVar == null || cVar.f2696c == null) {
                    return false;
                }
                return true;
            } else if (b.this.f2690a != null) {
                return true;
            } else {
                return false;
            }
        }

        public Map.Entry<K, V> next() {
            if (this.f2699b) {
                this.f2699b = false;
                this.f2698a = b.this.f2690a;
            } else {
                c<K, V> cVar = this.f2698a;
                this.f2698a = cVar != null ? cVar.f2696c : null;
            }
            return this.f2698a;
        }
    }

    /* compiled from: SafeIterableMap */
    private static abstract class e<K, V> implements Iterator<Map.Entry<K, V>>, f<K, V> {

        /* renamed from: a  reason: collision with root package name */
        c<K, V> f2701a;

        /* renamed from: b  reason: collision with root package name */
        c<K, V> f2702b;

        e(c<K, V> cVar, c<K, V> cVar2) {
            this.f2701a = cVar2;
            this.f2702b = cVar;
        }

        public void a(c<K, V> cVar) {
            if (this.f2701a == cVar && cVar == this.f2702b) {
                this.f2702b = null;
                this.f2701a = null;
            }
            c<K, V> cVar2 = this.f2701a;
            if (cVar2 == cVar) {
                this.f2701a = b(cVar2);
            }
            if (this.f2702b == cVar) {
                this.f2702b = a();
            }
        }

        /* access modifiers changed from: package-private */
        public abstract c<K, V> b(c<K, V> cVar);

        /* access modifiers changed from: package-private */
        public abstract c<K, V> c(c<K, V> cVar);

        public boolean hasNext() {
            return this.f2702b != null;
        }

        public Map.Entry<K, V> next() {
            c<K, V> cVar = this.f2702b;
            this.f2702b = a();
            return cVar;
        }

        private c<K, V> a() {
            c<K, V> cVar = this.f2702b;
            c<K, V> cVar2 = this.f2701a;
            if (cVar == cVar2 || cVar2 == null) {
                return null;
            }
            return c(cVar);
        }
    }

    /* compiled from: SafeIterableMap */
    interface f<K, V> {
        void a(c<K, V> cVar);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected b.b.a.b.b.c<K, V> a(K r3) {
        /*
            r2 = this;
            b.b.a.b.b$c<K, V> r0 = r2.f2690a
        L_0x0002:
            if (r0 == 0) goto L_0x0010
            K r1 = r0.f2694a
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x000d
            goto L_0x0010
        L_0x000d:
            b.b.a.b.b$c<K, V> r0 = r0.f2696c
            goto L_0x0002
        L_0x0010:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.b.b.a(java.lang.Object):b.b.a.b.b$c");
    }

    public V b(K k2, V v) {
        c a2 = a(k2);
        if (a2 != null) {
            return a2.f2695b;
        }
        a(k2, v);
        return null;
    }

    public Map.Entry<K, V> c() {
        return this.f2691b;
    }

    public Iterator<Map.Entry<K, V>> descendingIterator() {
        C0040b bVar = new C0040b(this.f2691b, this.f2690a);
        this.f2692c.put(bVar, false);
        return bVar;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (size() != bVar.size()) {
            return false;
        }
        Iterator it = iterator();
        Iterator it2 = bVar.iterator();
        while (it.hasNext() && it2.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Object next = it2.next();
            if ((entry == null && next != null) || (entry != null && !entry.equals(next))) {
                return false;
            }
        }
        if (it.hasNext() || it2.hasNext()) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        Iterator it = iterator();
        int i2 = 0;
        while (it.hasNext()) {
            i2 += ((Map.Entry) it.next()).hashCode();
        }
        return i2;
    }

    public Iterator<Map.Entry<K, V>> iterator() {
        a aVar = new a(this.f2690a, this.f2691b);
        this.f2692c.put(aVar, false);
        return aVar;
    }

    public V remove(K k2) {
        c a2 = a(k2);
        if (a2 == null) {
            return null;
        }
        this.f2693d--;
        if (!this.f2692c.isEmpty()) {
            for (f<K, V> a3 : this.f2692c.keySet()) {
                a3.a(a2);
            }
        }
        c<K, V> cVar = a2.f2697d;
        if (cVar != null) {
            cVar.f2696c = a2.f2696c;
        } else {
            this.f2690a = a2.f2696c;
        }
        c<K, V> cVar2 = a2.f2696c;
        if (cVar2 != null) {
            cVar2.f2697d = a2.f2697d;
        } else {
            this.f2691b = a2.f2697d;
        }
        a2.f2696c = null;
        a2.f2697d = null;
        return a2.f2695b;
    }

    public int size() {
        return this.f2693d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator it = iterator();
        while (it.hasNext()) {
            sb.append(((Map.Entry) it.next()).toString());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public c<K, V> a(K k2, V v) {
        c<K, V> cVar = new c<>(k2, v);
        this.f2693d++;
        c<K, V> cVar2 = this.f2691b;
        if (cVar2 == null) {
            this.f2690a = cVar;
            this.f2691b = this.f2690a;
            return cVar;
        }
        cVar2.f2696c = cVar;
        cVar.f2697d = cVar2;
        this.f2691b = cVar;
        return cVar;
    }

    public b<K, V>.d b() {
        b<K, V>.d dVar = new d();
        this.f2692c.put(dVar, false);
        return dVar;
    }

    public Map.Entry<K, V> a() {
        return this.f2690a;
    }
}
