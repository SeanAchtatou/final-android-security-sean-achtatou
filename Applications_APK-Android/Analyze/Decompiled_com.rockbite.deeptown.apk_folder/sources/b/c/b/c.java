package b.c.b;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import androidx.core.app.d;
import java.util.ArrayList;

/* compiled from: CustomTabsIntent */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    public final Intent f2723a;

    /* renamed from: b  reason: collision with root package name */
    public final Bundle f2724b;

    /* compiled from: CustomTabsIntent */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        private final Intent f2725a;

        /* renamed from: b  reason: collision with root package name */
        private ArrayList<Bundle> f2726b;

        /* renamed from: c  reason: collision with root package name */
        private Bundle f2727c;

        /* renamed from: d  reason: collision with root package name */
        private ArrayList<Bundle> f2728d;

        /* renamed from: e  reason: collision with root package name */
        private boolean f2729e;

        public a() {
            this(null);
        }

        public c a() {
            ArrayList<Bundle> arrayList = this.f2726b;
            if (arrayList != null) {
                this.f2725a.putParcelableArrayListExtra("android.support.customtabs.extra.MENU_ITEMS", arrayList);
            }
            ArrayList<Bundle> arrayList2 = this.f2728d;
            if (arrayList2 != null) {
                this.f2725a.putParcelableArrayListExtra("android.support.customtabs.extra.TOOLBAR_ITEMS", arrayList2);
            }
            this.f2725a.putExtra("android.support.customtabs.extra.EXTRA_ENABLE_INSTANT_APPS", this.f2729e);
            return new c(this.f2725a, this.f2727c);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public a b() {
            this.f2725a.putExtra("android.support.customtabs.extra.ENABLE_URLBAR_HIDING", true);
            return this;
        }

        public a(e eVar) {
            this.f2725a = new Intent("android.intent.action.VIEW");
            IBinder iBinder = null;
            this.f2726b = null;
            this.f2727c = null;
            this.f2728d = null;
            this.f2729e = true;
            if (eVar != null) {
                this.f2725a.setPackage(eVar.b().getPackageName());
            }
            Bundle bundle = new Bundle();
            d.a(bundle, "android.support.customtabs.extra.SESSION", eVar != null ? eVar.a() : iBinder);
            this.f2725a.putExtras(bundle);
        }
    }

    c(Intent intent, Bundle bundle) {
        this.f2723a = intent;
        this.f2724b = bundle;
    }

    public void a(Context context, Uri uri) {
        this.f2723a.setData(uri);
        b.e.e.a.a(context, this.f2723a, this.f2724b);
    }
}
