package b.c.b;

import a.a.a.a;
import a.a.a.b;
import android.content.ComponentName;
import android.os.IBinder;

/* compiled from: CustomTabsSession */
public final class e {

    /* renamed from: a  reason: collision with root package name */
    private final a f2730a;

    /* renamed from: b  reason: collision with root package name */
    private final ComponentName f2731b;

    e(b bVar, a aVar, ComponentName componentName) {
        this.f2730a = aVar;
        this.f2731b = componentName;
    }

    /* access modifiers changed from: package-private */
    public IBinder a() {
        return this.f2730a.asBinder();
    }

    /* access modifiers changed from: package-private */
    public ComponentName b() {
        return this.f2731b;
    }
}
