package b.c.b;

import a.a.a.a;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;

/* compiled from: CustomTabsClient */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private final a.a.a.b f2703a;

    /* renamed from: b  reason: collision with root package name */
    private final ComponentName f2704b;

    b(a.a.a.b bVar, ComponentName componentName) {
        this.f2703a = bVar;
        this.f2704b = componentName;
    }

    public static boolean a(Context context, String str, d dVar) {
        Intent intent = new Intent("android.support.customtabs.action.CustomTabsService");
        if (!TextUtils.isEmpty(str)) {
            intent.setPackage(str);
        }
        return context.bindService(intent, dVar, 33);
    }

    /* compiled from: CustomTabsClient */
    class a extends a.C0000a {

        /* renamed from: a  reason: collision with root package name */
        private Handler f2705a = new Handler(Looper.getMainLooper());

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ a f2706b;

        /* renamed from: b.c.b.b$a$a  reason: collision with other inner class name */
        /* compiled from: CustomTabsClient */
        class C0041a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ int f2707a;

            /* renamed from: b  reason: collision with root package name */
            final /* synthetic */ Bundle f2708b;

            C0041a(int i2, Bundle bundle) {
                this.f2707a = i2;
                this.f2708b = bundle;
            }

            public void run() {
                a.this.f2706b.onNavigationEvent(this.f2707a, this.f2708b);
            }
        }

        /* renamed from: b.c.b.b$a$b  reason: collision with other inner class name */
        /* compiled from: CustomTabsClient */
        class C0042b implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ String f2710a;

            /* renamed from: b  reason: collision with root package name */
            final /* synthetic */ Bundle f2711b;

            C0042b(String str, Bundle bundle) {
                this.f2710a = str;
                this.f2711b = bundle;
            }

            public void run() {
                a.this.f2706b.extraCallback(this.f2710a, this.f2711b);
            }
        }

        /* compiled from: CustomTabsClient */
        class c implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ Bundle f2713a;

            c(Bundle bundle) {
                this.f2713a = bundle;
            }

            public void run() {
                a.this.f2706b.onMessageChannelReady(this.f2713a);
            }
        }

        /* compiled from: CustomTabsClient */
        class d implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ String f2715a;

            /* renamed from: b  reason: collision with root package name */
            final /* synthetic */ Bundle f2716b;

            d(String str, Bundle bundle) {
                this.f2715a = str;
                this.f2716b = bundle;
            }

            public void run() {
                a.this.f2706b.onPostMessage(this.f2715a, this.f2716b);
            }
        }

        /* compiled from: CustomTabsClient */
        class e implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ int f2718a;

            /* renamed from: b  reason: collision with root package name */
            final /* synthetic */ Uri f2719b;

            /* renamed from: c  reason: collision with root package name */
            final /* synthetic */ boolean f2720c;

            /* renamed from: d  reason: collision with root package name */
            final /* synthetic */ Bundle f2721d;

            e(int i2, Uri uri, boolean z, Bundle bundle) {
                this.f2718a = i2;
                this.f2719b = uri;
                this.f2720c = z;
                this.f2721d = bundle;
            }

            public void run() {
                a.this.f2706b.onRelationshipValidationResult(this.f2718a, this.f2719b, this.f2720c, this.f2721d);
            }
        }

        a(b bVar, a aVar) {
            this.f2706b = aVar;
        }

        public void a(int i2, Bundle bundle) {
            if (this.f2706b != null) {
                this.f2705a.post(new C0041a(i2, bundle));
            }
        }

        public void b(String str, Bundle bundle) throws RemoteException {
            if (this.f2706b != null) {
                this.f2705a.post(new d(str, bundle));
            }
        }

        public void a(String str, Bundle bundle) throws RemoteException {
            if (this.f2706b != null) {
                this.f2705a.post(new C0042b(str, bundle));
            }
        }

        public void a(Bundle bundle) throws RemoteException {
            if (this.f2706b != null) {
                this.f2705a.post(new c(bundle));
            }
        }

        public void a(int i2, Uri uri, boolean z, Bundle bundle) throws RemoteException {
            if (this.f2706b != null) {
                this.f2705a.post(new e(i2, uri, z, bundle));
            }
        }
    }

    public e a(a aVar) {
        a aVar2 = new a(this, aVar);
        try {
            if (!this.f2703a.a(aVar2)) {
                return null;
            }
            return new e(this.f2703a, aVar2, this.f2704b);
        } catch (RemoteException unused) {
            return null;
        }
    }
}
