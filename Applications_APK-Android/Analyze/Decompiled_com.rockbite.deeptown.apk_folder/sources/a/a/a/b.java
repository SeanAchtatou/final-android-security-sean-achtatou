package a.a.a;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

/* compiled from: ICustomTabsService */
public interface b extends IInterface {

    /* compiled from: ICustomTabsService */
    public static abstract class a extends Binder implements b {

        /* renamed from: a.a.a.b$a$a  reason: collision with other inner class name */
        /* compiled from: ICustomTabsService */
        private static class C0001a implements b {

            /* renamed from: a  reason: collision with root package name */
            private IBinder f545a;

            C0001a(IBinder iBinder) {
                this.f545a = iBinder;
            }

            public boolean a(a aVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeStrongBinder(aVar != null ? aVar.asBinder() : null);
                    boolean z = false;
                    this.f545a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f545a;
            }
        }

        public static b a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("android.support.customtabs.ICustomTabsService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof b)) {
                return new C0001a(iBinder);
            }
            return (b) queryLocalInterface;
        }
    }

    boolean a(a aVar) throws RemoteException;
}
