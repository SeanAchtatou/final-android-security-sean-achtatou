package com.anjlab.android.iab.v3;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/* compiled from: BillingBase */
class a {

    /* renamed from: a  reason: collision with root package name */
    private Context f3188a;

    a(Context context) {
        this.f3188a = context;
    }

    private SharedPreferences c() {
        return PreferenceManager.getDefaultSharedPreferences(a());
    }

    /* access modifiers changed from: package-private */
    public Context a() {
        return this.f3188a;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return a().getPackageName() + "_preferences";
    }

    /* access modifiers changed from: package-private */
    public String a(String str, String str2) {
        SharedPreferences c2 = c();
        return c2 != null ? c2.getString(str, str2) : str2;
    }

    /* access modifiers changed from: package-private */
    public boolean b(String str, String str2) {
        SharedPreferences c2 = c();
        if (c2 == null) {
            return false;
        }
        SharedPreferences.Editor edit = c2.edit();
        edit.putString(str, str2);
        edit.commit();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, Boolean bool) {
        SharedPreferences c2 = c();
        if (c2 == null) {
            return false;
        }
        SharedPreferences.Editor edit = c2.edit();
        edit.putBoolean(str, bool.booleanValue());
        edit.commit();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, boolean z) {
        SharedPreferences c2 = c();
        return c2 != null ? c2.getBoolean(str, z) : z;
    }
}
