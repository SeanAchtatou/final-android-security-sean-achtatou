package com.anjlab.android.iab.v3;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Date;
import java.util.Locale;

public class TransactionDetails implements Parcelable {
    public static final Parcelable.Creator<TransactionDetails> CREATOR = new a();
    @Deprecated

    /* renamed from: a  reason: collision with root package name */
    public final String f3183a;
    @Deprecated

    /* renamed from: b  reason: collision with root package name */
    public final String f3184b;
    @Deprecated

    /* renamed from: c  reason: collision with root package name */
    public final String f3185c;
    @Deprecated

    /* renamed from: d  reason: collision with root package name */
    public final Date f3186d;

    /* renamed from: e  reason: collision with root package name */
    public final PurchaseInfo f3187e;

    class a implements Parcelable.Creator<TransactionDetails> {
        a() {
        }

        public TransactionDetails createFromParcel(Parcel parcel) {
            return new TransactionDetails(parcel);
        }

        public TransactionDetails[] newArray(int i2) {
            return new TransactionDetails[i2];
        }
    }

    public TransactionDetails(PurchaseInfo purchaseInfo) {
        this.f3187e = purchaseInfo;
        PurchaseData purchaseData = this.f3187e.f3171c;
        this.f3183a = purchaseData.f3163c;
        this.f3184b = purchaseData.f3161a;
        this.f3185c = purchaseData.f3167g;
        this.f3186d = purchaseData.f3164d;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || TransactionDetails.class != obj.getClass()) {
            return false;
        }
        String str = this.f3184b;
        String str2 = ((TransactionDetails) obj).f3184b;
        if (str != null) {
            if (!str.equals(str2)) {
                return false;
            }
            return true;
        } else if (str2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.f3184b;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        return String.format(Locale.US, "%s purchased at %s(%s). Token: %s, Signature: %s", this.f3183a, this.f3186d, this.f3184b, this.f3185c, this.f3187e.f3170b);
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeParcelable(this.f3187e, i2);
    }

    protected TransactionDetails(Parcel parcel) {
        this.f3187e = (PurchaseInfo) parcel.readParcelable(PurchaseInfo.class.getClassLoader());
        PurchaseData purchaseData = this.f3187e.f3171c;
        this.f3183a = purchaseData.f3163c;
        this.f3184b = purchaseData.f3161a;
        this.f3185c = purchaseData.f3167g;
        this.f3186d = purchaseData.f3164d;
    }
}
