package com.anjlab.android.iab.v3;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.ads.mediation.inmobi.InMobiNetworkValues;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public class SkuDetails implements Parcelable {
    public static final Parcelable.Creator<SkuDetails> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public final String f3172a;

    /* renamed from: b  reason: collision with root package name */
    public final String f3173b;

    /* renamed from: c  reason: collision with root package name */
    public final String f3174c;

    /* renamed from: d  reason: collision with root package name */
    public final boolean f3175d;

    /* renamed from: e  reason: collision with root package name */
    public final String f3176e;

    /* renamed from: f  reason: collision with root package name */
    public final Double f3177f;

    /* renamed from: g  reason: collision with root package name */
    public final String f3178g;

    /* renamed from: h  reason: collision with root package name */
    public final String f3179h;

    /* renamed from: i  reason: collision with root package name */
    public final boolean f3180i;

    /* renamed from: j  reason: collision with root package name */
    public final double f3181j;

    /* renamed from: k  reason: collision with root package name */
    public final String f3182k;
    public final boolean l;
    public final int m;
    public final long n;
    public final String o;
    public final long p;
    public final String q;

    class a implements Parcelable.Creator<SkuDetails> {
        a() {
        }

        public SkuDetails createFromParcel(Parcel parcel) {
            return new SkuDetails(parcel);
        }

        public SkuDetails[] newArray(int i2) {
            return new SkuDetails[i2];
        }
    }

    public SkuDetails(JSONObject jSONObject) throws JSONException {
        String optString = jSONObject.optString("type");
        optString = optString == null ? "inapp" : optString;
        this.f3172a = jSONObject.optString("productId");
        this.f3173b = jSONObject.optString("title");
        this.f3174c = jSONObject.optString(InMobiNetworkValues.DESCRIPTION);
        this.f3175d = optString.equalsIgnoreCase("subs");
        this.f3176e = jSONObject.optString("price_currency_code");
        this.n = jSONObject.optLong("price_amount_micros");
        double d2 = (double) this.n;
        Double.isNaN(d2);
        this.f3177f = Double.valueOf(d2 / 1000000.0d);
        this.o = jSONObject.optString("price");
        this.f3178g = jSONObject.optString("subscriptionPeriod");
        this.f3179h = jSONObject.optString("freeTrialPeriod");
        this.f3180i = !TextUtils.isEmpty(this.f3179h);
        this.p = jSONObject.optLong("introductoryPriceAmountMicros");
        double d3 = (double) this.p;
        Double.isNaN(d3);
        this.f3181j = d3 / 1000000.0d;
        this.q = jSONObject.optString("introductoryPrice");
        this.f3182k = jSONObject.optString("introductoryPricePeriod");
        this.l = !TextUtils.isEmpty(this.f3182k);
        this.m = jSONObject.optInt("introductoryPriceCycles");
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || SkuDetails.class != obj.getClass()) {
            return false;
        }
        SkuDetails skuDetails = (SkuDetails) obj;
        if (this.f3175d != skuDetails.f3175d) {
            return false;
        }
        String str = this.f3172a;
        String str2 = skuDetails.f3172a;
        if (str != null) {
            if (!str.equals(str2)) {
                return false;
            }
            return true;
        } else if (str2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.f3172a;
        return ((str != null ? str.hashCode() : 0) * 31) + (this.f3175d ? 1 : 0);
    }

    public String toString() {
        return String.format(Locale.US, "%s: %s(%s) %f in %s (%s)", this.f3172a, this.f3173b, this.f3174c, this.f3177f, this.f3176e, this.o);
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.f3172a);
        parcel.writeString(this.f3173b);
        parcel.writeString(this.f3174c);
        parcel.writeByte(this.f3175d ? (byte) 1 : 0);
        parcel.writeString(this.f3176e);
        parcel.writeDouble(this.f3177f.doubleValue());
        parcel.writeLong(this.n);
        parcel.writeString(this.o);
        parcel.writeString(this.f3178g);
        parcel.writeString(this.f3179h);
        parcel.writeByte(this.f3180i ? (byte) 1 : 0);
        parcel.writeDouble(this.f3181j);
        parcel.writeLong(this.p);
        parcel.writeString(this.q);
        parcel.writeString(this.f3182k);
        parcel.writeByte(this.l ? (byte) 1 : 0);
        parcel.writeInt(this.m);
    }

    protected SkuDetails(Parcel parcel) {
        this.f3172a = parcel.readString();
        this.f3173b = parcel.readString();
        this.f3174c = parcel.readString();
        boolean z = true;
        this.f3175d = parcel.readByte() != 0;
        this.f3176e = parcel.readString();
        this.f3177f = Double.valueOf(parcel.readDouble());
        this.n = parcel.readLong();
        this.o = parcel.readString();
        this.f3178g = parcel.readString();
        this.f3179h = parcel.readString();
        this.f3180i = parcel.readByte() != 0;
        this.f3181j = parcel.readDouble();
        this.p = parcel.readLong();
        this.q = parcel.readString();
        this.f3182k = parcel.readString();
        this.l = parcel.readByte() == 0 ? false : z;
        this.m = parcel.readInt();
    }
}
