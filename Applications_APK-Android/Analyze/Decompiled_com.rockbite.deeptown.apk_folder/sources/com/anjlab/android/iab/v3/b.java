package com.anjlab.android.iab.v3;

import android.content.Context;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

/* compiled from: BillingCache */
class b extends a {

    /* renamed from: b  reason: collision with root package name */
    private HashMap<String, PurchaseInfo> f3189b = new HashMap<>();

    /* renamed from: c  reason: collision with root package name */
    private String f3190c;

    /* renamed from: d  reason: collision with root package name */
    private String f3191d;

    b(Context context, String str) {
        super(context);
        this.f3190c = str;
        h();
    }

    private void d() {
        ArrayList arrayList = new ArrayList();
        for (String next : this.f3189b.keySet()) {
            PurchaseInfo purchaseInfo = this.f3189b.get(next);
            arrayList.add(next + ">>>>>" + purchaseInfo.f3169a + ">>>>>" + purchaseInfo.f3170b);
        }
        b(f(), TextUtils.join("#####", arrayList));
        this.f3191d = Long.toString(new Date().getTime());
        b(g(), this.f3191d);
    }

    private String e() {
        return a(g(), AppEventsConstants.EVENT_PARAM_VALUE_NO);
    }

    private String f() {
        return b() + this.f3190c;
    }

    private String g() {
        return f() + ".version";
    }

    private void h() {
        for (String str : a(f(), "").split(Pattern.quote("#####"))) {
            if (!TextUtils.isEmpty(str)) {
                String[] split = str.split(Pattern.quote(">>>>>"));
                if (split.length > 2) {
                    this.f3189b.put(split[0], new PurchaseInfo(split[1], split[2]));
                } else if (split.length > 1) {
                    this.f3189b.put(split[0], new PurchaseInfo(split[1], null));
                }
            }
        }
        this.f3191d = e();
    }

    private void i() {
        if (!this.f3191d.equalsIgnoreCase(e())) {
            this.f3189b.clear();
            h();
        }
    }

    /* access modifiers changed from: package-private */
    public PurchaseInfo a(String str) {
        i();
        if (this.f3189b.containsKey(str)) {
            return this.f3189b.get(str);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean b(String str) {
        i();
        return this.f3189b.containsKey(str);
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        i();
        if (this.f3189b.containsKey(str)) {
            this.f3189b.remove(str);
            d();
        }
    }

    public String toString() {
        return TextUtils.join(", ", this.f3189b.keySet());
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, String str3) {
        i();
        if (!this.f3189b.containsKey(str)) {
            this.f3189b.put(str, new PurchaseInfo(str2, str3));
            d();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        i();
        this.f3189b.clear();
        d();
    }
}
