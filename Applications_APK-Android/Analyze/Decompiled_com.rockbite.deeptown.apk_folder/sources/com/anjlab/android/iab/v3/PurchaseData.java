package com.anjlab.android.iab.v3;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Date;

public class PurchaseData implements Parcelable {
    public static final Parcelable.Creator<PurchaseData> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public String f3161a;

    /* renamed from: b  reason: collision with root package name */
    public String f3162b;

    /* renamed from: c  reason: collision with root package name */
    public String f3163c;

    /* renamed from: d  reason: collision with root package name */
    public Date f3164d;

    /* renamed from: e  reason: collision with root package name */
    public d f3165e;

    /* renamed from: f  reason: collision with root package name */
    public String f3166f;

    /* renamed from: g  reason: collision with root package name */
    public String f3167g;

    /* renamed from: h  reason: collision with root package name */
    public boolean f3168h;

    class a implements Parcelable.Creator<PurchaseData> {
        a() {
        }

        public PurchaseData createFromParcel(Parcel parcel) {
            return new PurchaseData(parcel);
        }

        public PurchaseData[] newArray(int i2) {
            return new PurchaseData[i2];
        }
    }

    public PurchaseData() {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.f3161a);
        parcel.writeString(this.f3162b);
        parcel.writeString(this.f3163c);
        Date date = this.f3164d;
        parcel.writeLong(date != null ? date.getTime() : -1);
        d dVar = this.f3165e;
        parcel.writeInt(dVar == null ? -1 : dVar.ordinal());
        parcel.writeString(this.f3166f);
        parcel.writeString(this.f3167g);
        parcel.writeByte(this.f3168h ? (byte) 1 : 0);
    }

    protected PurchaseData(Parcel parcel) {
        Date date;
        this.f3161a = parcel.readString();
        this.f3162b = parcel.readString();
        this.f3163c = parcel.readString();
        long readLong = parcel.readLong();
        d dVar = null;
        if (readLong == -1) {
            date = null;
        } else {
            date = new Date(readLong);
        }
        this.f3164d = date;
        int readInt = parcel.readInt();
        this.f3165e = readInt != -1 ? d.values()[readInt] : dVar;
        this.f3166f = parcel.readString();
        this.f3167g = parcel.readString();
        this.f3168h = parcel.readByte() != 0;
    }
}
