package com.anjlab.android.iab.v3;

/* compiled from: PurchaseState */
public enum d {
    PurchasedSuccessfully,
    Canceled,
    Refunded,
    SubscriptionExpired
}
