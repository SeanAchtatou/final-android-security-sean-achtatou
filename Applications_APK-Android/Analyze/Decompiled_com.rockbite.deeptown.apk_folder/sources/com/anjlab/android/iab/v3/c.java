package com.anjlab.android.iab.v3;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import com.android.vending.billing.IInAppBillingService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.json.JSONObject;

/* compiled from: BillingProcessor */
public class c extends a {

    /* renamed from: j  reason: collision with root package name */
    private static final Date f3192j;

    /* renamed from: k  reason: collision with root package name */
    private static final Date f3193k;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public IInAppBillingService f3194b;

    /* renamed from: c  reason: collision with root package name */
    private String f3195c;

    /* renamed from: d  reason: collision with root package name */
    private String f3196d;

    /* renamed from: e  reason: collision with root package name */
    private b f3197e;

    /* renamed from: f  reason: collision with root package name */
    private b f3198f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public C0069c f3199g;

    /* renamed from: h  reason: collision with root package name */
    private String f3200h;

    /* renamed from: i  reason: collision with root package name */
    private ServiceConnection f3201i;

    /* compiled from: BillingProcessor */
    class a implements ServiceConnection {
        a() {
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            IInAppBillingService unused = c.this.f3194b = IInAppBillingService.Stub.asInterface(iBinder);
            new b(c.this, null).execute(new Void[0]);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            IInAppBillingService unused = c.this.f3194b = (IInAppBillingService) null;
        }
    }

    /* compiled from: BillingProcessor */
    private class b extends AsyncTask<Void, Void, Boolean> {
        private b() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Boolean doInBackground(Void... voidArr) {
            if (c.this.i()) {
                return false;
            }
            c.this.d();
            return true;
        }

        /* synthetic */ b(c cVar, a aVar) {
            this();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            if (bool.booleanValue()) {
                c.this.j();
                if (c.this.f3199g != null) {
                    c.this.f3199g.a();
                }
            }
            if (c.this.f3199g != null) {
                c.this.f3199g.b();
            }
        }
    }

    /* renamed from: com.anjlab.android.iab.v3.c$c  reason: collision with other inner class name */
    /* compiled from: BillingProcessor */
    public interface C0069c {
        void a();

        void a(int i2, Throwable th);

        void a(String str, TransactionDetails transactionDetails);

        void b();
    }

    static {
        Calendar instance = Calendar.getInstance();
        instance.set(2012, 11, 5);
        f3192j = instance.getTime();
        instance.set(2015, 6, 21);
        f3193k = instance.getTime();
    }

    public c(Context context, String str, C0069c cVar) {
        this(context, str, null, cVar);
    }

    private void f() {
        try {
            a().bindService(g(), this.f3201i, 1);
        } catch (Exception e2) {
            Log.e("iabv3", "error in bindPlayServices", e2);
            a(113, e2);
        }
    }

    private static Intent g() {
        Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        intent.setPackage("com.android.vending");
        return intent;
    }

    private String h() {
        return a(b() + ".purchase.last.v2_6", (String) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.anjlab.android.iab.v3.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.anjlab.android.iab.v3.c.a(com.anjlab.android.iab.v3.c, com.android.vending.billing.IInAppBillingService):com.android.vending.billing.IInAppBillingService
      com.anjlab.android.iab.v3.c.a(java.lang.String, com.anjlab.android.iab.v3.b):com.anjlab.android.iab.v3.TransactionDetails
      com.anjlab.android.iab.v3.c.a(java.util.ArrayList<java.lang.String>, java.lang.String):java.util.List<com.anjlab.android.iab.v3.SkuDetails>
      com.anjlab.android.iab.v3.c.a(int, java.lang.Throwable):void
      com.anjlab.android.iab.v3.c.a(android.app.Activity, java.lang.String):boolean
      com.anjlab.android.iab.v3.a.a(java.lang.String, java.lang.String):java.lang.String
      com.anjlab.android.iab.v3.a.a(java.lang.String, java.lang.Boolean):boolean
      com.anjlab.android.iab.v3.a.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    public boolean i() {
        return a(b() + ".products.restored.v2_6", false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.anjlab.android.iab.v3.a.a(java.lang.String, java.lang.Boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.anjlab.android.iab.v3.c.a(com.anjlab.android.iab.v3.c, com.android.vending.billing.IInAppBillingService):com.android.vending.billing.IInAppBillingService
      com.anjlab.android.iab.v3.c.a(java.lang.String, com.anjlab.android.iab.v3.b):com.anjlab.android.iab.v3.TransactionDetails
      com.anjlab.android.iab.v3.c.a(java.util.ArrayList<java.lang.String>, java.lang.String):java.util.List<com.anjlab.android.iab.v3.SkuDetails>
      com.anjlab.android.iab.v3.c.a(int, java.lang.Throwable):void
      com.anjlab.android.iab.v3.c.a(android.app.Activity, java.lang.String):boolean
      com.anjlab.android.iab.v3.a.a(java.lang.String, java.lang.String):java.lang.String
      com.anjlab.android.iab.v3.a.a(java.lang.String, boolean):boolean
      com.anjlab.android.iab.v3.a.a(java.lang.String, java.lang.Boolean):boolean */
    /* access modifiers changed from: private */
    public void j() {
        a(b() + ".products.restored.v2_6", (Boolean) true);
    }

    public boolean d() {
        return b("inapp", this.f3197e) && b("subs", this.f3198f);
    }

    public void e() {
        if (c() && this.f3201i != null) {
            try {
                a().unbindService(this.f3201i);
            } catch (Exception e2) {
                Log.e("iabv3", "Error in release", e2);
            }
            this.f3194b = null;
        }
    }

    public c(Context context, String str, String str2, C0069c cVar) {
        this(context, str, str2, cVar, true);
    }

    private boolean b(String str, b bVar) {
        if (!c()) {
            return false;
        }
        try {
            Bundle purchases = this.f3194b.getPurchases(3, this.f3195c, str, null);
            if (purchases.getInt("RESPONSE_CODE") == 0) {
                bVar.c();
                ArrayList<String> stringArrayList = purchases.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                ArrayList<String> stringArrayList2 = purchases.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                if (stringArrayList == null) {
                    return true;
                }
                int i2 = 0;
                while (i2 < stringArrayList.size()) {
                    String str2 = stringArrayList.get(i2);
                    if (!TextUtils.isEmpty(str2)) {
                        bVar.a(new JSONObject(str2).getString("productId"), str2, (stringArrayList2 == null || stringArrayList2.size() <= i2) ? null : stringArrayList2.get(i2));
                    }
                    i2++;
                }
                return true;
            }
        } catch (Exception e2) {
            a(100, e2);
            Log.e("iabv3", "Error in loadPurchasesByType", e2);
        }
        return false;
    }

    public boolean c() {
        return this.f3194b != null;
    }

    private c(Context context, String str, String str2, C0069c cVar, boolean z) {
        super(context.getApplicationContext());
        this.f3201i = new a();
        this.f3196d = str;
        this.f3199g = cVar;
        this.f3195c = a().getPackageName();
        this.f3197e = new b(a(), ".products.cache.v2_6");
        this.f3198f = new b(a(), ".subscriptions.cache.v2_6");
        this.f3200h = str2;
        if (z) {
            f();
        }
    }

    private SkuDetails c(String str, String str2) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(str);
        List<SkuDetails> a2 = a(arrayList, str2);
        if (a2 == null || a2.size() <= 0) {
            return null;
        }
        return a2.get(0);
    }

    private void g(String str) {
        b(b() + ".purchase.last.v2_6", str);
    }

    public boolean a(Activity activity, String str) {
        return a(activity, null, str, "inapp", null);
    }

    public TransactionDetails d(String str) {
        return a(str, this.f3198f);
    }

    private boolean a(Activity activity, List<String> list, String str, String str2, String str3) {
        return a(activity, list, str, str2, str3, null);
    }

    public boolean f(String str) {
        return this.f3198f.b(str);
    }

    private boolean a(Activity activity, List<String> list, String str, String str2, String str3, Bundle bundle) {
        String str4;
        Bundle bundle2;
        List<String> list2 = list;
        String str5 = str;
        String str6 = str2;
        String str7 = str3;
        Bundle bundle3 = bundle;
        if (c() && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            try {
                String str8 = str6 + ":" + str5;
                if (!str6.equals("subs")) {
                    str8 = str8 + ":" + UUID.randomUUID().toString();
                }
                if (str7 != null) {
                    str4 = str8 + ":" + str7;
                } else {
                    str4 = str8;
                }
                g(str4);
                if (list2 == null || !str6.equals("subs")) {
                    if (bundle3 == null) {
                        bundle2 = this.f3194b.getBuyIntent(3, this.f3195c, str, str2, str4);
                    } else {
                        bundle2 = this.f3194b.getBuyIntentExtraParams(7, this.f3195c, str, str2, str4, bundle);
                    }
                } else if (bundle3 == null) {
                    bundle2 = this.f3194b.getBuyIntentToReplaceSkus(5, this.f3195c, list, str, str2, str4);
                } else {
                    if (!bundle3.containsKey("skusToReplace")) {
                        bundle3.putStringArrayList("skusToReplace", new ArrayList(list2));
                    }
                    bundle2 = this.f3194b.getBuyIntentExtraParams(7, this.f3195c, str, str2, str4, bundle);
                }
                if (bundle2 == null) {
                    return true;
                }
                int i2 = bundle2.getInt("RESPONSE_CODE");
                if (i2 == 0) {
                    PendingIntent pendingIntent = (PendingIntent) bundle2.getParcelable("BUY_INTENT");
                    if (activity == null || pendingIntent == null) {
                        a(103, (Throwable) null);
                        return true;
                    }
                    activity.startIntentSenderForResult(pendingIntent.getIntentSender(), 32459, new Intent(), 0, 0, 0);
                    return true;
                } else if (i2 == 7) {
                    if (!e(str5) && !f(str5)) {
                        d();
                    }
                    TransactionDetails c2 = c(str5);
                    if (!a(c2)) {
                        Log.i("iabv3", "Invalid or tampered merchant id!");
                        a(104, (Throwable) null);
                        return false;
                    } else if (this.f3199g == null) {
                        return true;
                    } else {
                        if (c2 == null) {
                            c2 = d(str5);
                        }
                        this.f3199g.a(str5, c2);
                        return true;
                    }
                } else {
                    a(101, (Throwable) null);
                    return true;
                }
            } catch (Exception e2) {
                Log.e("iabv3", "Error in purchase", e2);
                a(110, e2);
            }
        }
        return false;
    }

    public boolean e(String str) {
        return this.f3197e.b(str);
    }

    public TransactionDetails c(String str) {
        return a(str, this.f3197e);
    }

    public SkuDetails b(String str) {
        return c(str, "inapp");
    }

    private boolean a(TransactionDetails transactionDetails) {
        int indexOf;
        if (this.f3200h == null || transactionDetails.f3187e.f3171c.f3164d.before(f3192j) || transactionDetails.f3187e.f3171c.f3164d.after(f3193k)) {
            return true;
        }
        String str = transactionDetails.f3187e.f3171c.f3161a;
        if (str == null || str.trim().length() == 0 || (indexOf = transactionDetails.f3187e.f3171c.f3161a.indexOf(46)) <= 0 || transactionDetails.f3187e.f3171c.f3161a.substring(0, indexOf).compareTo(this.f3200h) != 0) {
            return false;
        }
        return true;
    }

    private TransactionDetails a(String str, b bVar) {
        PurchaseInfo a2 = bVar.a(str);
        if (a2 == null || TextUtils.isEmpty(a2.f3169a)) {
            return null;
        }
        return new TransactionDetails(a2);
    }

    public boolean a(String str) {
        if (!c()) {
            return false;
        }
        try {
            TransactionDetails a2 = a(str, this.f3197e);
            if (a2 != null && !TextUtils.isEmpty(a2.f3187e.f3171c.f3167g)) {
                int consumePurchase = this.f3194b.consumePurchase(3, this.f3195c, a2.f3187e.f3171c.f3167g);
                if (consumePurchase == 0) {
                    this.f3197e.c(str);
                    Log.d("iabv3", "Successfully consumed " + str + " purchase.");
                    return true;
                }
                a(consumePurchase, (Throwable) null);
                Log.e("iabv3", String.format("Failed to consume %s: %d", str, Integer.valueOf(consumePurchase)));
            }
        } catch (Exception e2) {
            Log.e("iabv3", "Error in consumePurchase", e2);
            a(111, e2);
        }
        return false;
    }

    private List<SkuDetails> a(ArrayList<String> arrayList, String str) {
        if (!(this.f3194b == null || arrayList == null || arrayList.size() <= 0)) {
            try {
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("ITEM_ID_LIST", arrayList);
                Bundle skuDetails = this.f3194b.getSkuDetails(3, this.f3195c, str, bundle);
                int i2 = skuDetails.getInt("RESPONSE_CODE");
                if (i2 == 0) {
                    ArrayList arrayList2 = new ArrayList();
                    ArrayList<String> stringArrayList = skuDetails.getStringArrayList("DETAILS_LIST");
                    if (stringArrayList != null) {
                        for (String jSONObject : stringArrayList) {
                            arrayList2.add(new SkuDetails(new JSONObject(jSONObject)));
                        }
                    }
                    return arrayList2;
                }
                a(i2, (Throwable) null);
                Log.e("iabv3", String.format("Failed to retrieve info for %d products, %d", Integer.valueOf(arrayList.size()), Integer.valueOf(i2)));
            } catch (Exception e2) {
                Log.e("iabv3", "Failed to call getSkuDetails", e2);
                a(112, e2);
            }
        }
        return null;
    }

    private String a(JSONObject jSONObject) {
        String h2 = h();
        if (!TextUtils.isEmpty(h2) && h2.startsWith("subs")) {
            return "subs";
        }
        if (jSONObject == null || !jSONObject.has("autoRenewing")) {
            return "inapp";
        }
        return "subs";
    }

    public boolean a(int i2, int i3, Intent intent) {
        if (i2 != 32459) {
            return false;
        }
        if (intent == null) {
            Log.e("iabv3", "handleActivityResult: data is null!");
            return false;
        }
        int intExtra = intent.getIntExtra("RESPONSE_CODE", 0);
        Log.d("iabv3", String.format("resultCode = %d, responseCode = %d", Integer.valueOf(i3), Integer.valueOf(intExtra)));
        if (i3 == -1 && intExtra == 0) {
            String stringExtra = intent.getStringExtra("INAPP_PURCHASE_DATA");
            String stringExtra2 = intent.getStringExtra("INAPP_DATA_SIGNATURE");
            try {
                JSONObject jSONObject = new JSONObject(stringExtra);
                String string = jSONObject.getString("productId");
                if (a(string, stringExtra, stringExtra2)) {
                    (a(jSONObject).equals("subs") ? this.f3198f : this.f3197e).a(string, stringExtra, stringExtra2);
                    if (this.f3199g != null) {
                        this.f3199g.a(string, new TransactionDetails(new PurchaseInfo(stringExtra, stringExtra2)));
                    }
                } else {
                    Log.e("iabv3", "Public key signature doesn't match!");
                    a(102, (Throwable) null);
                }
            } catch (Exception e2) {
                Log.e("iabv3", "Error in handleActivityResult", e2);
                a(110, e2);
            }
            g(null);
        } else {
            a(intExtra, (Throwable) null);
        }
        return true;
    }

    private boolean a(String str, String str2, String str3) {
        try {
            if (TextUtils.isEmpty(this.f3196d) || e.a(str, this.f3196d, str2, str3)) {
                return true;
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    private void a(int i2, Throwable th) {
        C0069c cVar = this.f3199g;
        if (cVar != null) {
            cVar.a(i2, th);
        }
    }
}
