package com.anjlab.android.iab.v3;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.firebase.remoteconfig.RemoteConfigConstants;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public class PurchaseInfo implements Parcelable {
    public static final Parcelable.Creator<PurchaseInfo> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public final String f3169a;

    /* renamed from: b  reason: collision with root package name */
    public final String f3170b;

    /* renamed from: c  reason: collision with root package name */
    public final PurchaseData f3171c = a();

    class a implements Parcelable.Creator<PurchaseInfo> {
        a() {
        }

        public PurchaseInfo createFromParcel(Parcel parcel) {
            return new PurchaseInfo(parcel);
        }

        public PurchaseInfo[] newArray(int i2) {
            return new PurchaseInfo[i2];
        }
    }

    public PurchaseInfo(String str, String str2) {
        this.f3169a = str;
        this.f3170b = str2;
    }

    /* access modifiers changed from: package-private */
    public PurchaseData a() {
        try {
            JSONObject jSONObject = new JSONObject(this.f3169a);
            PurchaseData purchaseData = new PurchaseData();
            purchaseData.f3161a = jSONObject.optString("orderId");
            purchaseData.f3162b = jSONObject.optString(RemoteConfigConstants.RequestFieldKey.PACKAGE_NAME);
            purchaseData.f3163c = jSONObject.optString("productId");
            long optLong = jSONObject.optLong("purchaseTime", 0);
            purchaseData.f3164d = optLong != 0 ? new Date(optLong) : null;
            purchaseData.f3165e = d.values()[jSONObject.optInt("purchaseState", 1)];
            purchaseData.f3166f = jSONObject.optString("developerPayload");
            purchaseData.f3167g = jSONObject.getString("purchaseToken");
            purchaseData.f3168h = jSONObject.optBoolean("autoRenewing");
            return purchaseData;
        } catch (JSONException e2) {
            Log.e("iabv3.purchaseInfo", "Failed to parse response data", e2);
            return null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof PurchaseInfo)) {
            return false;
        }
        PurchaseInfo purchaseInfo = (PurchaseInfo) obj;
        if (!this.f3169a.equals(purchaseInfo.f3169a) || !this.f3170b.equals(purchaseInfo.f3170b) || !this.f3171c.f3167g.equals(purchaseInfo.f3171c.f3167g) || !this.f3171c.f3164d.equals(purchaseInfo.f3171c.f3164d)) {
            return false;
        }
        return true;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.f3169a);
        parcel.writeString(this.f3170b);
    }

    protected PurchaseInfo(Parcel parcel) {
        this.f3169a = parcel.readString();
        this.f3170b = parcel.readString();
    }
}
