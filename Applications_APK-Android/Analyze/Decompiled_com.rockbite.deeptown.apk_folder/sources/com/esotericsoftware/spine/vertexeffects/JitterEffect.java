package com.esotericsoftware.spine.vertexeffects;

import com.badlogic.gdx.math.h;
import com.badlogic.gdx.math.p;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonRenderer;
import e.d.b.t.b;

public class JitterEffect implements SkeletonRenderer.VertexEffect {
    private float x;
    private float y;

    public JitterEffect(float f2, float f3) {
        this.x = f2;
        this.y = f3;
    }

    public void begin(Skeleton skeleton) {
    }

    public void end() {
    }

    public void setJitter(float f2, float f3) {
        this.x = f2;
        this.y = f3;
    }

    public void setJitterX(float f2) {
        this.x = f2;
    }

    public void setJitterY(float f2) {
        this.y = f2;
    }

    public void transform(p pVar, p pVar2, b bVar, b bVar2) {
        pVar.f5294a += h.c(-this.x, this.y);
        pVar.f5295b += h.c(-this.x, this.y);
    }
}
