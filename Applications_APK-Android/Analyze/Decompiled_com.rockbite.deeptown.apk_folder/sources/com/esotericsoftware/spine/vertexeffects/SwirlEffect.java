package com.esotericsoftware.spine.vertexeffects;

import com.badlogic.gdx.math.f;
import com.badlogic.gdx.math.p;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonRenderer;
import com.esotericsoftware.spine.utils.SpineUtils;
import e.d.b.t.b;

public class SwirlEffect implements SkeletonRenderer.VertexEffect {
    private float angle;
    private float centerX;
    private float centerY;
    private f interpolation = f.f5261f;
    private float radius;
    private float worldX;
    private float worldY;

    public SwirlEffect(float f2) {
        this.radius = f2;
    }

    public void begin(Skeleton skeleton) {
        this.worldX = skeleton.getX() + this.centerX;
        this.worldY = skeleton.getY() + this.centerY;
    }

    public void end() {
    }

    public f getInterpolation() {
        return this.interpolation;
    }

    public void setAngle(float f2) {
        this.angle = f2 * 0.017453292f;
    }

    public void setCenter(float f2, float f3) {
        this.centerX = f2;
        this.centerY = f3;
    }

    public void setCenterX(float f2) {
        this.centerX = f2;
    }

    public void setCenterY(float f2) {
        this.centerY = f2;
    }

    public void setInterpolation(f fVar) {
        this.interpolation = fVar;
    }

    public void setRadius(float f2) {
        this.radius = f2;
    }

    public void transform(p pVar, p pVar2, b bVar, b bVar2) {
        float f2 = pVar.f5294a - this.worldX;
        float f3 = pVar.f5295b - this.worldY;
        float sqrt = (float) Math.sqrt((double) ((f2 * f2) + (f3 * f3)));
        float f4 = this.radius;
        if (sqrt < f4) {
            float a2 = this.interpolation.a(Animation.CurveTimeline.LINEAR, this.angle, (f4 - sqrt) / f4);
            float cos = SpineUtils.cos(a2);
            float sin = SpineUtils.sin(a2);
            pVar.f5294a = ((cos * f2) - (sin * f3)) + this.worldX;
            pVar.f5295b = (sin * f2) + (cos * f3) + this.worldY;
        }
    }
}
