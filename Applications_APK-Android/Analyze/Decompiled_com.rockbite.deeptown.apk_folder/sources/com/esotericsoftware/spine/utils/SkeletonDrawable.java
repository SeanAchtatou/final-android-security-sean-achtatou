package com.esotericsoftware.spine.utils;

import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonRenderer;
import e.d.b.w.a.l.b;

public class SkeletonDrawable extends b {
    private SkeletonRenderer renderer;
    private boolean resetBlendFunction = true;
    private Skeleton skeleton;
    AnimationState state;

    public SkeletonDrawable() {
    }

    public void draw(com.badlogic.gdx.graphics.g2d.b bVar, float f2, float f3, float f4, float f5) {
        int blendSrcFunc = bVar.getBlendSrcFunc();
        int blendDstFunc = bVar.getBlendDstFunc();
        int blendSrcFuncAlpha = bVar.getBlendSrcFuncAlpha();
        int blendDstFuncAlpha = bVar.getBlendDstFuncAlpha();
        this.skeleton.setPosition(f2, f3);
        this.skeleton.updateWorldTransform();
        this.renderer.draw(bVar, this.skeleton);
        if (this.resetBlendFunction) {
            bVar.setBlendFunctionSeparate(blendSrcFunc, blendDstFunc, blendSrcFuncAlpha, blendDstFuncAlpha);
        }
    }

    public AnimationState getAnimationState() {
        return this.state;
    }

    public SkeletonRenderer getRenderer() {
        return this.renderer;
    }

    public boolean getResetBlendFunction() {
        return this.resetBlendFunction;
    }

    public Skeleton getSkeleton() {
        return this.skeleton;
    }

    public void setAnimationState(AnimationState animationState) {
        this.state = animationState;
    }

    public void setRenderer(SkeletonRenderer skeletonRenderer) {
        this.renderer = skeletonRenderer;
    }

    public void setResetBlendFunction(boolean z) {
        this.resetBlendFunction = z;
    }

    public void setSkeleton(Skeleton skeleton2) {
        this.skeleton = skeleton2;
    }

    public void update(float f2) {
        this.state.update(f2);
        this.state.apply(this.skeleton);
    }

    public SkeletonDrawable(SkeletonRenderer skeletonRenderer, Skeleton skeleton2, AnimationState animationState) {
        this.renderer = skeletonRenderer;
        this.skeleton = skeleton2;
        this.state = animationState;
    }
}
