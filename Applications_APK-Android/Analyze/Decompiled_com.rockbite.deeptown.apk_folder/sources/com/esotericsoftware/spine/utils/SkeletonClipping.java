package com.esotericsoftware.spine.utils;

import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.m;
import com.badlogic.gdx.utils.n0;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.Slot;
import com.esotericsoftware.spine.attachments.ClippingAttachment;
import java.util.Iterator;

public class SkeletonClipping {
    private ClippingAttachment clipAttachment;
    private final m clipOutput = new m(128);
    private final n0 clippedTriangles = new n0(128);
    private final m clippedVertices = new m(128);
    private final m clippingPolygon = new m();
    private a<m> clippingPolygons;
    private final m scratch = new m();
    private final Triangulator triangulator = new Triangulator();

    static void makeClockwise(m mVar) {
        float[] fArr = mVar.f5527a;
        int i2 = mVar.f5528b;
        int i3 = i2 - 2;
        int i4 = i2 - 3;
        float f2 = (fArr[i3] * fArr[1]) - (fArr[0] * fArr[i2 - 1]);
        int i5 = 0;
        while (i5 < i4) {
            int i6 = i5 + 2;
            f2 += (fArr[i5] * fArr[i5 + 3]) - (fArr[i6] * fArr[i5 + 1]);
            i5 = i6;
        }
        if (f2 >= Animation.CurveTimeline.LINEAR) {
            int i7 = i2 >> 1;
            for (int i8 = 0; i8 < i7; i8 += 2) {
                float f3 = fArr[i8];
                int i9 = i8 + 1;
                float f4 = fArr[i9];
                int i10 = i3 - i8;
                fArr[i8] = fArr[i10];
                int i11 = i10 + 1;
                fArr[i9] = fArr[i11];
                fArr[i10] = f3;
                fArr[i11] = f4;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean clip(float f2, float f3, float f4, float f5, float f6, float f7, m mVar, m mVar2) {
        m mVar3;
        m mVar4;
        float[] fArr;
        float f8 = f2;
        float f9 = f3;
        m mVar5 = mVar;
        m mVar6 = mVar2;
        int i2 = 2;
        if (mVar5.f5528b % 4 >= 2) {
            mVar3 = this.scratch;
            mVar4 = mVar6;
        } else {
            mVar4 = this.scratch;
            mVar3 = mVar6;
        }
        mVar4.a();
        mVar4.a(f8);
        mVar4.a(f9);
        mVar4.a(f4);
        mVar4.a(f5);
        mVar4.a(f6);
        mVar4.a(f7);
        mVar4.a(f8);
        mVar4.a(f9);
        mVar3.a();
        float[] fArr2 = mVar5.f5527a;
        int i3 = mVar5.f5528b - 4;
        m mVar7 = mVar3;
        boolean z = false;
        m mVar8 = mVar4;
        int i4 = 0;
        while (true) {
            float f10 = fArr2[i4];
            float f11 = fArr2[i4 + 1];
            int i5 = i4 + 2;
            float f12 = fArr2[i5];
            float f13 = fArr2[i4 + 3];
            float f14 = f10 - f12;
            float f15 = f11 - f13;
            float[] fArr3 = mVar8.f5527a;
            int i6 = mVar8.f5528b - i2;
            int i7 = mVar7.f5528b;
            boolean z2 = z;
            int i8 = 0;
            while (i8 < i6) {
                float f16 = fArr3[i8];
                float f17 = fArr3[i8 + 1];
                int i9 = i8 + 2;
                int i10 = i6;
                float f18 = fArr3[i9];
                float f19 = fArr3[i8 + 3];
                boolean z3 = ((f19 - f13) * f14) - ((f18 - f12) * f15) > Animation.CurveTimeline.LINEAR;
                if (((f17 - f13) * f14) - ((f16 - f12) * f15) > Animation.CurveTimeline.LINEAR) {
                    if (z3) {
                        mVar7.a(f18);
                        mVar7.a(f19);
                        fArr = fArr2;
                        i6 = i10;
                        fArr2 = fArr;
                        i8 = i9;
                    } else {
                        float f20 = f19 - f17;
                        float f21 = f18 - f16;
                        float f22 = f12 - f10;
                        float f23 = f13 - f11;
                        float f24 = (f20 * f22) - (f21 * f23);
                        if (Math.abs(f24) > 1.0E-6f) {
                            float f25 = ((f21 * (f11 - f17)) - (f20 * (f10 - f16))) / f24;
                            mVar7.a(f10 + (f22 * f25));
                            mVar7.a(f11 + (f23 * f25));
                        } else {
                            mVar7.a(f10);
                            mVar7.a(f11);
                        }
                    }
                } else if (z3) {
                    float f26 = f19 - f17;
                    float f27 = f18 - f16;
                    float f28 = f12 - f10;
                    float f29 = f13 - f11;
                    float f30 = (f26 * f28) - (f27 * f29);
                    if (Math.abs(f30) > 1.0E-6f) {
                        float f31 = ((f27 * (f11 - f17)) - (f26 * (f10 - f16))) / f30;
                        fArr = fArr2;
                        mVar7.a(f10 + (f28 * f31));
                        mVar7.a(f11 + (f29 * f31));
                    } else {
                        fArr = fArr2;
                        mVar7.a(f10);
                        mVar7.a(f11);
                    }
                    mVar7.a(f18);
                    mVar7.a(f19);
                    z2 = true;
                    i6 = i10;
                    fArr2 = fArr;
                    i8 = i9;
                }
                fArr = fArr2;
                z2 = true;
                i6 = i10;
                fArr2 = fArr;
                i8 = i9;
            }
            float[] fArr4 = fArr2;
            if (i7 == mVar7.f5528b) {
                mVar2.a();
                return true;
            }
            mVar7.a(mVar7.f5527a[0]);
            mVar7.a(mVar7.f5527a[1]);
            if (i4 == i3) {
                if (mVar6 != mVar7) {
                    mVar2.a();
                    mVar6.a(mVar7.f5527a, 0, mVar7.f5528b - 2);
                } else {
                    mVar6.d(mVar6.f5528b - 2);
                }
                return z2;
            }
            mVar8.a();
            i2 = 2;
            i4 = i5;
            z = z2;
            fArr2 = fArr4;
            m mVar9 = mVar7;
            mVar7 = mVar8;
            mVar8 = mVar9;
        }
    }

    public void clipEnd(Slot slot) {
        ClippingAttachment clippingAttachment = this.clipAttachment;
        if (clippingAttachment != null && clippingAttachment.getEndSlot() == slot.getData()) {
            clipEnd();
        }
    }

    public int clipStart(Slot slot, ClippingAttachment clippingAttachment) {
        int worldVerticesLength;
        if (this.clipAttachment != null || (worldVerticesLength = clippingAttachment.getWorldVerticesLength()) < 6) {
            return 0;
        }
        this.clipAttachment = clippingAttachment;
        clippingAttachment.computeWorldVertices(slot, 0, worldVerticesLength, this.clippingPolygon.d(worldVerticesLength), 0, 2);
        makeClockwise(this.clippingPolygon);
        this.clippingPolygons = this.triangulator.decompose(this.clippingPolygon, this.triangulator.triangulate(this.clippingPolygon));
        Iterator<m> it = this.clippingPolygons.iterator();
        while (it.hasNext()) {
            m next = it.next();
            makeClockwise(next);
            next.a(next.f5527a[0]);
            next.a(next.f5527a[1]);
        }
        return this.clippingPolygons.f5374b;
    }

    public void clipTriangles(float[] fArr, int i2, short[] sArr, int i3, float[] fArr2, float f2, float f3, boolean z) {
        int i4;
        int i5;
        m mVar = this.clipOutput;
        m mVar2 = this.clippedVertices;
        n0 n0Var = this.clippedTriangles;
        a<m> aVar = this.clippingPolygons;
        T[] tArr = aVar.f5373a;
        int i6 = aVar.f5374b;
        int i7 = z ? 6 : 5;
        mVar2.a();
        n0Var.a();
        int i8 = i3;
        short s = 0;
        int i9 = 0;
        while (i9 < i8) {
            int i10 = sArr[i9] << 1;
            float f4 = fArr[i10];
            int i11 = i10 + 1;
            float f5 = fArr[i11];
            float f6 = fArr2[i10];
            float f7 = fArr2[i11];
            int i12 = sArr[i9 + 1] << 1;
            float f8 = fArr[i12];
            int i13 = i12 + 1;
            float f9 = fArr[i13];
            float f10 = fArr2[i12];
            float f11 = fArr2[i13];
            int i14 = sArr[i9 + 2] << 1;
            float f12 = fArr[i14];
            int i15 = i14 + 1;
            float f13 = fArr[i15];
            float f14 = fArr2[i14];
            float f15 = fArr2[i15];
            short s2 = s;
            int i16 = 0;
            while (true) {
                if (i16 >= i6) {
                    i4 = i9;
                    s = s2;
                    break;
                }
                int i17 = mVar2.f5528b;
                int i18 = i16;
                i4 = i9;
                if (clip(f4, f5, f8, f9, f12, f13, (m) tArr[i16], mVar)) {
                    int i19 = mVar.f5528b;
                    if (i19 != 0) {
                        float f16 = f9 - f13;
                        float f17 = f12 - f8;
                        float f18 = f4 - f12;
                        float f19 = f13 - f5;
                        float f20 = 1.0f / ((f16 * f18) + ((f5 - f13) * f17));
                        int i20 = i19 >> 1;
                        float[] fArr3 = mVar.f5527a;
                        float[] d2 = mVar2.d(i17 + (i20 * i7));
                        for (int i21 = 0; i21 < i19; i21 += 2) {
                            float f21 = fArr3[i21];
                            float f22 = fArr3[i21 + 1];
                            d2[i17] = f21;
                            d2[i17 + 1] = f22;
                            d2[i17 + 2] = f2;
                            if (z) {
                                d2[i17 + 3] = f3;
                                i5 = i17 + 4;
                            } else {
                                i5 = i17 + 3;
                            }
                            float f23 = f21 - f12;
                            float f24 = f22 - f13;
                            float f25 = ((f16 * f23) + (f17 * f24)) * f20;
                            float f26 = ((f23 * f19) + (f24 * f18)) * f20;
                            float f27 = (1.0f - f25) - f26;
                            d2[i5] = (f6 * f25) + (f10 * f26) + (f14 * f27);
                            d2[i5 + 1] = (f25 * f7) + (f26 * f11) + (f27 * f15);
                            i17 = i5 + 2;
                        }
                        int i22 = n0Var.f5543b;
                        short[] f28 = n0Var.f(((i20 - 2) * 3) + i22);
                        int i23 = i20 - 1;
                        int i24 = i22;
                        for (int i25 = 1; i25 < i23; i25++) {
                            f28[i24] = s2;
                            int i26 = s2 + i25;
                            f28[i24 + 1] = (short) i26;
                            f28[i24 + 2] = (short) (i26 + 1);
                            i24 += 3;
                        }
                        s2 = (short) (s2 + i23 + 1);
                    }
                    i16 = i18 + 1;
                    i9 = i4;
                } else {
                    float[] d3 = mVar2.d(i17 + (i7 * 3));
                    d3[i17] = f4;
                    d3[i17 + 1] = f5;
                    d3[i17 + 2] = f2;
                    if (!z) {
                        d3[i17 + 3] = f6;
                        d3[i17 + 4] = f7;
                        d3[i17 + 5] = f8;
                        d3[i17 + 6] = f9;
                        d3[i17 + 7] = f2;
                        d3[i17 + 8] = f10;
                        d3[i17 + 9] = f11;
                        d3[i17 + 10] = f12;
                        d3[i17 + 11] = f13;
                        d3[i17 + 12] = f2;
                        d3[i17 + 13] = f14;
                        d3[i17 + 14] = f15;
                    } else {
                        d3[i17 + 3] = f3;
                        d3[i17 + 4] = f6;
                        d3[i17 + 5] = f7;
                        d3[i17 + 6] = f8;
                        d3[i17 + 7] = f9;
                        d3[i17 + 8] = f2;
                        d3[i17 + 9] = f3;
                        d3[i17 + 10] = f10;
                        d3[i17 + 11] = f11;
                        d3[i17 + 12] = f12;
                        d3[i17 + 13] = f13;
                        d3[i17 + 14] = f2;
                        d3[i17 + 15] = f3;
                        d3[i17 + 16] = f14;
                        d3[i17 + 17] = f15;
                    }
                    int i27 = n0Var.f5543b;
                    short[] f29 = n0Var.f(i27 + 3);
                    f29[i27] = s2;
                    f29[i27 + 1] = (short) (s2 + 1);
                    f29[i27 + 2] = (short) (s2 + 2);
                    s = (short) (s2 + 3);
                }
            }
            i9 = i4 + 3;
            i8 = i3;
        }
    }

    public n0 getClippedTriangles() {
        return this.clippedTriangles;
    }

    public m getClippedVertices() {
        return this.clippedVertices;
    }

    public boolean isClipping() {
        return this.clipAttachment != null;
    }

    public void clipEnd() {
        if (this.clipAttachment != null) {
            this.clipAttachment = null;
            this.clippingPolygons = null;
            this.clippedVertices.a();
            this.clippedTriangles.a();
            this.clippingPolygon.a();
        }
    }
}
