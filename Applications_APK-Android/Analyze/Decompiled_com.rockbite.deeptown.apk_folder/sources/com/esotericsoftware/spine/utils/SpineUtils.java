package com.esotericsoftware.spine.utils;

public class SpineUtils {
    public static final float PI = 3.1415927f;
    public static final float PI2 = 6.2831855f;
    public static final float degRad = 0.017453292f;
    public static final float degreesToRadians = 0.017453292f;
    public static final float radDeg = 57.295776f;
    public static final float radiansToDegrees = 57.295776f;

    public static float atan2(float f2, float f3) {
        return (float) Math.atan2((double) f2, (double) f3);
    }

    public static float cos(float f2) {
        return (float) Math.cos((double) f2);
    }

    public static float cosDeg(float f2) {
        return (float) Math.cos((double) (f2 * 0.017453292f));
    }

    public static float sin(float f2) {
        return (float) Math.sin((double) f2);
    }

    public static float sinDeg(float f2) {
        return (float) Math.sin((double) (f2 * 0.017453292f));
    }
}
