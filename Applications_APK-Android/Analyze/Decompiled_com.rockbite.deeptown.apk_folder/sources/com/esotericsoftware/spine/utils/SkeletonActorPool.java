package com.esotericsoftware.spine.utils;

import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.f0;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.SkeletonRenderer;
import com.esotericsoftware.spine.Skin;
import com.google.android.gms.common.api.Api;
import e.d.b.t.b;

public class SkeletonActorPool extends f0<SkeletonActor> {
    private final a<SkeletonActor> obtained;
    private SkeletonRenderer renderer;
    SkeletonData skeletonData;
    private final f0<Skeleton> skeletonPool;
    AnimationStateData stateData;
    private final f0<AnimationState> statePool;

    public SkeletonActorPool(SkeletonRenderer skeletonRenderer, SkeletonData skeletonData2, AnimationStateData animationStateData) {
        this(skeletonRenderer, skeletonData2, animationStateData, 16, Api.BaseClientBuilder.API_PRIORITY_OTHER);
    }

    public void freeComplete() {
        a<SkeletonActor> aVar = this.obtained;
        for (int i2 = aVar.f5374b - 1; i2 >= 0; i2--) {
            SkeletonActor skeletonActor = aVar.get(i2);
            a<AnimationState.TrackEntry> tracks = skeletonActor.state.getTracks();
            int i3 = 0;
            int i4 = tracks.f5374b;
            while (true) {
                if (i3 >= i4) {
                    free(skeletonActor);
                    break;
                } else if (tracks.get(i3) != null) {
                    break;
                } else {
                    i3++;
                }
            }
        }
    }

    public a<SkeletonActor> getObtained() {
        return this.obtained;
    }

    public SkeletonActorPool(SkeletonRenderer skeletonRenderer, SkeletonData skeletonData2, AnimationStateData animationStateData, int i2, int i3) {
        super(i2, i3);
        this.renderer = skeletonRenderer;
        this.skeletonData = skeletonData2;
        this.stateData = animationStateData;
        this.obtained = new a<>(false, i2);
        this.skeletonPool = new f0<Skeleton>(i2, i3) {
            /* access modifiers changed from: protected */
            public Skeleton newObject() {
                return new Skeleton(SkeletonActorPool.this.skeletonData);
            }

            /* access modifiers changed from: protected */
            public void reset(Skeleton skeleton) {
                skeleton.setColor(b.f6927e);
                skeleton.setScale(1.0f, 1.0f);
                skeleton.setSkin((Skin) null);
                skeleton.setSkin(SkeletonActorPool.this.skeletonData.getDefaultSkin());
                skeleton.setToSetupPose();
            }
        };
        this.statePool = new f0<AnimationState>(i2, i3) {
            /* access modifiers changed from: protected */
            public AnimationState newObject() {
                return new AnimationState(SkeletonActorPool.this.stateData);
            }

            /* access modifiers changed from: protected */
            public void reset(AnimationState animationState) {
                animationState.clearTracks();
                animationState.clearListeners();
            }
        };
    }

    /* access modifiers changed from: protected */
    public SkeletonActor newObject() {
        SkeletonActor skeletonActor = new SkeletonActor();
        skeletonActor.setRenderer(this.renderer);
        return skeletonActor;
    }

    public SkeletonActor obtain() {
        SkeletonActor skeletonActor = (SkeletonActor) super.obtain();
        skeletonActor.setSkeleton(this.skeletonPool.obtain());
        skeletonActor.setAnimationState(this.statePool.obtain());
        this.obtained.add(skeletonActor);
        return skeletonActor;
    }

    /* access modifiers changed from: protected */
    public void reset(SkeletonActor skeletonActor) {
        skeletonActor.remove();
        this.obtained.d(skeletonActor, true);
        this.skeletonPool.free(skeletonActor.getSkeleton());
        this.statePool.free(skeletonActor.getAnimationState());
    }
}
