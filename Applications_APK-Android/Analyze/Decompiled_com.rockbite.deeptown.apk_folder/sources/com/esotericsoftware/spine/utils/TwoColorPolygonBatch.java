package com.esotericsoftware.spine.utils;

import com.badlogic.gdx.graphics.g2d.j;
import com.badlogic.gdx.graphics.g2d.k;
import com.badlogic.gdx.graphics.glutils.s;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.a;
import com.badlogic.gdx.math.h;
import com.badlogic.gdx.utils.z;
import com.esotericsoftware.spine.Animation;
import e.d.b.g;
import e.d.b.t.b;
import e.d.b.t.j;
import e.d.b.t.n;
import e.d.b.t.r;

public class TwoColorPolygonBatch implements j {
    static final int SPRITE_SIZE = 24;
    static final int VERTEX_SIZE = 6;
    private int blendDstFunc;
    private int blendDstFuncAlpha;
    private int blendSrcFunc;
    private int blendSrcFuncAlpha;
    private boolean blendingDisabled;
    private final Matrix4 combinedMatrix;
    private final b dark;
    private float darkPacked;
    private final s defaultShader;
    private boolean drawing;
    private float invTexHeight;
    private float invTexWidth;
    private n lastTexture;
    private final b light;
    private float lightPacked;
    private final e.d.b.t.j mesh;
    private boolean premultipliedAlpha;
    private final Matrix4 projectionMatrix;
    private s shader;
    public int totalRenderCalls;
    private final Matrix4 transformMatrix;
    private int triangleIndex;
    private final short[] triangles;
    private int vertexIndex;
    private final float[] vertices;

    public TwoColorPolygonBatch() {
        this(2000);
    }

    private s createDefaultShader() {
        s sVar = new s("attribute vec4 a_position;\nattribute vec4 a_light;\nattribute vec4 a_dark;\nattribute vec2 a_texCoord0;\nuniform mat4 u_projTrans;\nvarying vec4 v_light;\nvarying vec4 v_dark;\nvarying vec2 v_texCoords;\n\nvoid main()\n{\n   v_light = a_light;\n   v_light.a = v_light.a * (255.0/254.0);\n   v_dark = a_dark;\n   v_texCoords = a_texCoord0;\n   gl_Position =  u_projTrans * a_position;\n}\n", "#ifdef GL_ES\n#define LOWP lowp\nprecision mediump float;\n#else\n#define LOWP \n#endif\nvarying LOWP vec4 v_light;\nvarying LOWP vec4 v_dark;\nuniform float u_pma;\nvarying vec2 v_texCoords;\nuniform sampler2D u_texture;\nvoid main()\n{\n  vec4 texColor = texture2D(u_texture, v_texCoords);\n  gl_FragColor.a = texColor.a * v_light.a;\n  gl_FragColor.rgb = ((texColor.a - 1.0) * u_pma + 1.0 - texColor.rgb) * v_dark.rgb + texColor.rgb * v_light.rgb;\n}");
        if (sVar.l()) {
            return sVar;
        }
        throw new IllegalArgumentException("Error compiling shader: " + sVar.k());
    }

    private void setupMatrices() {
        Matrix4 matrix4 = this.combinedMatrix;
        matrix4.b(this.projectionMatrix);
        matrix4.a(this.transformMatrix);
        this.shader.a("u_pma", this.premultipliedAlpha ? 1.0f : Animation.CurveTimeline.LINEAR);
        this.shader.a("u_projTrans", this.combinedMatrix);
        this.shader.a("u_texture", 0);
    }

    private void switchTexture(n nVar) {
        flush();
        this.lastTexture = nVar;
        this.invTexWidth = 1.0f / ((float) nVar.r());
        this.invTexHeight = 1.0f / ((float) nVar.p());
    }

    public void begin() {
        if (!this.drawing) {
            g.f6806g.a(false);
            this.shader.begin();
            setupMatrices();
            this.drawing = true;
            return;
        }
        throw new IllegalStateException("end must be called before begin.");
    }

    public void disableBlending() {
        flush();
        this.blendingDisabled = true;
    }

    public void dispose() {
        this.mesh.dispose();
        this.shader.dispose();
    }

    public void draw(k kVar, float f2, float f3) {
        if (this.drawing) {
            short[] sArr = this.triangles;
            short[] c2 = kVar.c();
            int length = c2.length;
            float[] d2 = kVar.d();
            int length2 = d2.length;
            n e2 = kVar.a().e();
            if (e2 != this.lastTexture) {
                switchTexture(e2);
            } else if (this.triangleIndex + length > sArr.length || this.vertexIndex + ((length2 * 6) / 2) > this.vertices.length) {
                flush();
            }
            int i2 = this.triangleIndex;
            int i3 = this.vertexIndex;
            int i4 = i3 / 6;
            int i5 = i2;
            int i6 = 0;
            while (i6 < length) {
                sArr[i5] = (short) (c2[i6] + i4);
                i6++;
                i5++;
            }
            this.triangleIndex = i5;
            float[] fArr = this.vertices;
            float f4 = this.lightPacked;
            float f5 = this.darkPacked;
            float[] b2 = kVar.b();
            for (int i7 = 0; i7 < length2; i7 += 2) {
                int i8 = i3 + 1;
                fArr[i3] = d2[i7] + f2;
                int i9 = i8 + 1;
                int i10 = i7 + 1;
                fArr[i8] = d2[i10] + f3;
                int i11 = i9 + 1;
                fArr[i9] = f4;
                int i12 = i11 + 1;
                fArr[i11] = f5;
                int i13 = i12 + 1;
                fArr[i12] = b2[i7];
                i3 = i13 + 1;
                fArr[i13] = b2[i10];
            }
            this.vertexIndex = i3;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void drawTwoColor(n nVar, float[] fArr, int i2, int i3, short[] sArr, int i4, int i5) {
        if (this.drawing) {
            short[] sArr2 = this.triangles;
            float[] fArr2 = this.vertices;
            if (nVar != this.lastTexture) {
                switchTexture(nVar);
            } else if (this.triangleIndex + i5 > sArr2.length || this.vertexIndex + i3 > fArr2.length) {
                flush();
            }
            int i6 = this.triangleIndex;
            int i7 = this.vertexIndex;
            int i8 = i7 / 6;
            int i9 = i5 + i4;
            while (i4 < i9) {
                sArr2[i6] = (short) (sArr[i4] + i8);
                i4++;
                i6++;
            }
            this.triangleIndex = i6;
            System.arraycopy(fArr, i2, fArr2, i7, i3);
            this.vertexIndex += i3;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void enableBlending() {
        flush();
        this.blendingDisabled = false;
    }

    public void end() {
        if (this.drawing) {
            if (this.vertexIndex > 0) {
                flush();
            }
            this.shader.end();
            g.f6806g.a(true);
            if (isBlendingEnabled()) {
                g.f6806g.k(3042);
            }
            this.lastTexture = null;
            this.drawing = false;
            return;
        }
        throw new IllegalStateException("begin must be called before end.");
    }

    public void flush() {
        if (this.vertexIndex != 0) {
            this.totalRenderCalls++;
            this.lastTexture.f();
            e.d.b.t.j jVar = this.mesh;
            jVar.a(this.vertices, 0, this.vertexIndex);
            jVar.a(this.triangles, 0, this.triangleIndex);
            g.f6806g.a(3042);
            int i2 = this.blendSrcFunc;
            if (i2 != -1) {
                g.f6806g.e(i2, this.blendDstFunc, this.blendSrcFuncAlpha, this.blendDstFuncAlpha);
            }
            jVar.a(this.shader, 4, 0, this.triangleIndex);
            this.vertexIndex = 0;
            this.triangleIndex = 0;
        }
    }

    public int getBlendDstFunc() {
        return this.blendDstFunc;
    }

    public int getBlendDstFuncAlpha() {
        return this.blendDstFuncAlpha;
    }

    public int getBlendSrcFunc() {
        return this.blendSrcFunc;
    }

    public int getBlendSrcFuncAlpha() {
        return this.blendSrcFuncAlpha;
    }

    public b getColor() {
        return this.light;
    }

    public b getDarkColor() {
        return this.dark;
    }

    public float getPackedColor() {
        return this.lightPacked;
    }

    public float getPackedDarkColor() {
        return this.darkPacked;
    }

    public Matrix4 getProjectionMatrix() {
        return this.projectionMatrix;
    }

    public s getShader() {
        return this.shader;
    }

    public Matrix4 getTransformMatrix() {
        return this.transformMatrix;
    }

    public boolean isBlendingEnabled() {
        return !this.blendingDisabled;
    }

    public boolean isDrawing() {
        return this.drawing;
    }

    public void setBlendFunction(int i2, int i3) {
        setBlendFunctionSeparate(i2, i3, i2, i3);
    }

    public void setBlendFunctionSeparate(int i2, int i3, int i4, int i5) {
        if (this.blendSrcFunc != i2 || this.blendDstFunc != i3 || this.blendSrcFuncAlpha != i4 || this.blendDstFuncAlpha != i5) {
            flush();
            this.blendSrcFunc = i2;
            this.blendDstFunc = i3;
            this.blendSrcFuncAlpha = i4;
            this.blendDstFuncAlpha = i5;
        }
    }

    public void setColor(b bVar) {
        this.light.b(bVar);
        this.lightPacked = bVar.b();
    }

    public void setDarkColor(b bVar) {
        this.dark.b(bVar);
        this.darkPacked = bVar.b();
    }

    public void setPackedColor(float f2) {
        b.b(this.light, z.b(f2));
        this.lightPacked = f2;
    }

    public void setPackedDarkColor(float f2) {
        b.b(this.dark, z.b(f2));
        this.darkPacked = f2;
    }

    public void setPremultipliedAlpha(boolean z) {
        if (this.premultipliedAlpha != z) {
            if (this.drawing) {
                flush();
            }
            this.premultipliedAlpha = z;
            if (this.drawing) {
                setupMatrices();
            }
        }
    }

    public void setProjectionMatrix(Matrix4 matrix4) {
        if (this.drawing) {
            flush();
        }
        this.projectionMatrix.b(matrix4);
        if (this.drawing) {
            setupMatrices();
        }
    }

    public void setShader(s sVar) {
        if (this.shader != sVar) {
            if (this.drawing) {
                flush();
                this.shader.end();
            }
            if (sVar == null) {
                sVar = this.defaultShader;
            }
            this.shader = sVar;
            if (this.drawing) {
                this.shader.begin();
                setupMatrices();
            }
        }
    }

    public void setTransformMatrix(Matrix4 matrix4) {
        if (this.drawing) {
            flush();
        }
        this.transformMatrix.b(matrix4);
        if (this.drawing) {
            setupMatrices();
        }
    }

    public TwoColorPolygonBatch(int i2) {
        this(i2, i2 * 2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.t.j.<init>(e.d.b.t.j$b, boolean, int, int, e.d.b.t.r[]):void
     arg types: [e.d.b.t.j$b, int, int, int, e.d.b.t.r[]]
     candidates:
      e.d.b.t.j.<init>(e.d.b.t.j$b, boolean, int, int, e.d.b.t.s):void
      e.d.b.t.j.<init>(e.d.b.t.j$b, boolean, int, int, e.d.b.t.r[]):void */
    public TwoColorPolygonBatch(int i2, int i3) {
        this.transformMatrix = new Matrix4();
        this.projectionMatrix = new Matrix4();
        this.combinedMatrix = new Matrix4();
        this.invTexWidth = Animation.CurveTimeline.LINEAR;
        this.invTexHeight = Animation.CurveTimeline.LINEAR;
        this.blendSrcFunc = 770;
        this.blendDstFunc = 771;
        this.blendSrcFuncAlpha = 770;
        this.blendDstFuncAlpha = 771;
        this.light = new b(1.0f, 1.0f, 1.0f, 1.0f);
        this.dark = new b(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f);
        this.lightPacked = b.f6927e.b();
        this.darkPacked = b.f6931i.b();
        this.totalRenderCalls = 0;
        if (i2 <= 32767) {
            int i4 = i3 * 3;
            this.mesh = new e.d.b.t.j(g.f6808i != null ? j.b.VertexBufferObjectWithVAO : j.b.VertexArray, false, i2, i4, new r(1, 2, "a_position"), new r(4, 4, "a_light"), new r(4, 4, "a_dark"), new r(16, 2, "a_texCoord0"));
            this.vertices = new float[(i2 * 6)];
            this.triangles = new short[i4];
            this.defaultShader = createDefaultShader();
            this.shader = this.defaultShader;
            this.projectionMatrix.a(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) g.f6801b.getWidth(), (float) g.f6801b.getHeight());
            return;
        }
        throw new IllegalArgumentException("Can't have more than 32767 vertices per batch: " + i3);
    }

    public void setColor(float f2, float f3, float f4, float f5) {
        this.light.b(f2, f3, f4, f5);
        this.lightPacked = this.light.b();
    }

    public void setDarkColor(float f2, float f3, float f4, float f5) {
        this.dark.b(f2, f3, f4, f5);
        this.darkPacked = this.dark.b();
    }

    public void drawTwoColor(n nVar, float[] fArr, int i2, int i3) {
        if (this.drawing) {
            short[] sArr = this.triangles;
            float[] fArr2 = this.vertices;
            int i4 = (i3 / 24) * 6;
            if (nVar != this.lastTexture) {
                switchTexture(nVar);
            } else if (this.triangleIndex + i4 > sArr.length || this.vertexIndex + i3 > fArr2.length) {
                flush();
            }
            int i5 = this.vertexIndex;
            int i6 = this.triangleIndex;
            short s = (short) (i5 / 6);
            int i7 = i4 + i6;
            while (i6 < i7) {
                sArr[i6] = s;
                sArr[i6 + 1] = (short) (s + 1);
                short s2 = (short) (s + 2);
                sArr[i6 + 2] = s2;
                sArr[i6 + 3] = s2;
                sArr[i6 + 4] = (short) (s + 3);
                sArr[i6 + 5] = s;
                i6 += 6;
                s = (short) (s + 4);
            }
            this.triangleIndex = i6;
            System.arraycopy(fArr, i2, fArr2, i5, i3);
            this.vertexIndex += i3;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void draw(k kVar, float f2, float f3, float f4, float f5) {
        if (this.drawing) {
            short[] sArr = this.triangles;
            short[] c2 = kVar.c();
            int length = c2.length;
            float[] d2 = kVar.d();
            int length2 = d2.length;
            com.badlogic.gdx.graphics.g2d.r a2 = kVar.a();
            n e2 = a2.e();
            if (e2 != this.lastTexture) {
                switchTexture(e2);
            } else if (this.triangleIndex + length > sArr.length || this.vertexIndex + ((length2 * 6) / 2) > this.vertices.length) {
                flush();
            }
            int i2 = this.triangleIndex;
            int i3 = this.vertexIndex;
            int i4 = i3 / 6;
            int length3 = c2.length;
            int i5 = i2;
            int i6 = 0;
            while (i6 < length3) {
                sArr[i5] = (short) (c2[i6] + i4);
                i6++;
                i5++;
            }
            this.triangleIndex = i5;
            float[] fArr = this.vertices;
            float f6 = this.lightPacked;
            float f7 = this.darkPacked;
            float[] b2 = kVar.b();
            float b3 = f4 / ((float) a2.b());
            float a3 = f5 / ((float) a2.a());
            for (int i7 = 0; i7 < length2; i7 += 2) {
                int i8 = i3 + 1;
                fArr[i3] = (d2[i7] * b3) + f2;
                int i9 = i8 + 1;
                int i10 = i7 + 1;
                fArr[i8] = (d2[i10] * a3) + f3;
                int i11 = i9 + 1;
                fArr[i9] = f6;
                int i12 = i11 + 1;
                fArr[i11] = f7;
                int i13 = i12 + 1;
                fArr[i12] = b2[i7];
                i3 = i13 + 1;
                fArr[i13] = b2[i10];
            }
            this.vertexIndex = i3;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void draw(k kVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10) {
        if (this.drawing) {
            short[] sArr = this.triangles;
            short[] c2 = kVar.c();
            int length = c2.length;
            float[] d2 = kVar.d();
            int length2 = d2.length;
            com.badlogic.gdx.graphics.g2d.r a2 = kVar.a();
            n e2 = a2.e();
            if (e2 != this.lastTexture) {
                switchTexture(e2);
            } else if (this.triangleIndex + length > sArr.length || this.vertexIndex + ((length2 * 6) / 2) > this.vertices.length) {
                flush();
            }
            int i2 = this.triangleIndex;
            int i3 = this.vertexIndex;
            int i4 = i3 / 6;
            int i5 = i2;
            int i6 = 0;
            while (i6 < length) {
                sArr[i5] = (short) (c2[i6] + i4);
                i6++;
                i5++;
            }
            this.triangleIndex = i5;
            float[] fArr = this.vertices;
            float f11 = this.lightPacked;
            float f12 = this.darkPacked;
            float[] b2 = kVar.b();
            float f13 = f2 + f4;
            float f14 = f3 + f5;
            float b3 = f6 / ((float) a2.b());
            float a3 = f7 / ((float) a2.a());
            float b4 = h.b(f10);
            float h2 = h.h(f10);
            for (int i7 = 0; i7 < length2; i7 += 2) {
                float f15 = ((d2[i7] * b3) - f4) * f8;
                int i8 = i7 + 1;
                float f16 = ((d2[i8] * a3) - f5) * f9;
                int i9 = i3 + 1;
                fArr[i3] = ((b4 * f15) - (h2 * f16)) + f13;
                int i10 = i9 + 1;
                fArr[i9] = (f15 * h2) + (f16 * b4) + f14;
                int i11 = i10 + 1;
                fArr[i10] = f11;
                int i12 = i11 + 1;
                fArr[i11] = f12;
                int i13 = i12 + 1;
                fArr[i12] = b2[i7];
                i3 = i13 + 1;
                fArr[i13] = b2[i8];
            }
            this.vertexIndex = i3;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void draw(n nVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, int i2, int i3, int i4, int i5, boolean z, boolean z2) {
        float f11;
        float f12;
        float f13;
        float f14;
        float f15 = f4;
        float f16 = f5;
        int i6 = i2;
        int i7 = i3;
        if (this.drawing) {
            short[] sArr = this.triangles;
            float[] fArr = this.vertices;
            if (nVar != this.lastTexture) {
                switchTexture(nVar);
            } else if (this.triangleIndex + 6 > sArr.length || this.vertexIndex + 24 > fArr.length) {
                flush();
            }
            int i8 = this.triangleIndex;
            int i9 = this.vertexIndex / 6;
            int i10 = i8 + 1;
            short s = (short) i9;
            sArr[i8] = s;
            int i11 = i10 + 1;
            sArr[i10] = (short) (i9 + 1);
            int i12 = i11 + 1;
            short s2 = (short) (i9 + 2);
            sArr[i11] = s2;
            int i13 = i12 + 1;
            sArr[i12] = s2;
            int i14 = i13 + 1;
            sArr[i13] = (short) (i9 + 3);
            sArr[i14] = s;
            this.triangleIndex = i14 + 1;
            float f17 = f2 + f15;
            float f18 = f3 + f16;
            float f19 = -f15;
            float f20 = -f16;
            float f21 = f6 - f15;
            float f22 = f7 - f16;
            if (!(f8 == 1.0f && f9 == 1.0f)) {
                f19 *= f8;
                f20 *= f9;
                f21 *= f8;
                f22 *= f9;
            }
            if (f10 != Animation.CurveTimeline.LINEAR) {
                float b2 = h.b(f10);
                float h2 = h.h(f10);
                float f23 = b2 * f19;
                float f24 = f23 - (h2 * f20);
                float f25 = f19 * h2;
                f20 = (f20 * b2) + f25;
                float f26 = h2 * f22;
                f12 = f23 - f26;
                float f27 = f22 * b2;
                float f28 = f25 + f27;
                float f29 = (b2 * f21) - f26;
                float f30 = f27 + (h2 * f21);
                float f31 = f30 - (f28 - f20);
                f13 = (f29 - f12) + f24;
                f21 = f29;
                f14 = f30;
                f22 = f28;
                f19 = f24;
                f11 = f31;
            } else {
                f13 = f21;
                f14 = f22;
                f12 = f19;
                f11 = f20;
            }
            float f32 = f19 + f17;
            float f33 = f20 + f18;
            float f34 = f12 + f17;
            float f35 = f22 + f18;
            float f36 = f21 + f17;
            float f37 = f14 + f18;
            float f38 = f13 + f17;
            float f39 = f11 + f18;
            float f40 = this.invTexWidth;
            float f41 = ((float) i6) * f40;
            float f42 = this.invTexHeight;
            float f43 = ((float) (i7 + i5)) * f42;
            float f44 = ((float) (i6 + i4)) * f40;
            float f45 = ((float) i7) * f42;
            if (!z) {
                float f46 = f41;
                f41 = f44;
                f44 = f46;
            }
            if (!z2) {
                float f47 = f43;
                f43 = f45;
                f45 = f47;
            }
            float f48 = this.lightPacked;
            float f49 = this.darkPacked;
            float f50 = f39;
            int i15 = this.vertexIndex;
            int i16 = i15 + 1;
            fArr[i15] = f32;
            int i17 = i16 + 1;
            fArr[i16] = f33;
            int i18 = i17 + 1;
            fArr[i17] = f48;
            int i19 = i18 + 1;
            fArr[i18] = f49;
            int i20 = i19 + 1;
            fArr[i19] = f44;
            int i21 = i20 + 1;
            fArr[i20] = f45;
            int i22 = i21 + 1;
            fArr[i21] = f34;
            int i23 = i22 + 1;
            fArr[i22] = f35;
            int i24 = i23 + 1;
            fArr[i23] = f48;
            int i25 = i24 + 1;
            fArr[i24] = f49;
            int i26 = i25 + 1;
            fArr[i25] = f44;
            int i27 = i26 + 1;
            fArr[i26] = f43;
            int i28 = i27 + 1;
            fArr[i27] = f36;
            int i29 = i28 + 1;
            fArr[i28] = f37;
            int i30 = i29 + 1;
            fArr[i29] = f48;
            int i31 = i30 + 1;
            fArr[i30] = f49;
            int i32 = i31 + 1;
            fArr[i31] = f41;
            int i33 = i32 + 1;
            fArr[i32] = f43;
            int i34 = i33 + 1;
            fArr[i33] = f38;
            int i35 = i34 + 1;
            fArr[i34] = f50;
            int i36 = i35 + 1;
            fArr[i35] = f48;
            int i37 = i36 + 1;
            fArr[i36] = f49;
            int i38 = i37 + 1;
            fArr[i37] = f41;
            fArr[i38] = f45;
            this.vertexIndex = i38 + 1;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void draw(n nVar, float f2, float f3, float f4, float f5, int i2, int i3, int i4, int i5, boolean z, boolean z2) {
        int i6 = i2;
        int i7 = i3;
        if (this.drawing) {
            short[] sArr = this.triangles;
            float[] fArr = this.vertices;
            if (nVar != this.lastTexture) {
                switchTexture(nVar);
            } else if (this.triangleIndex + 6 > sArr.length || this.vertexIndex + 24 > fArr.length) {
                flush();
            }
            int i8 = this.triangleIndex;
            int i9 = this.vertexIndex / 6;
            int i10 = i8 + 1;
            short s = (short) i9;
            sArr[i8] = s;
            int i11 = i10 + 1;
            sArr[i10] = (short) (i9 + 1);
            int i12 = i11 + 1;
            short s2 = (short) (i9 + 2);
            sArr[i11] = s2;
            int i13 = i12 + 1;
            sArr[i12] = s2;
            int i14 = i13 + 1;
            sArr[i13] = (short) (i9 + 3);
            sArr[i14] = s;
            this.triangleIndex = i14 + 1;
            float f6 = this.invTexWidth;
            float f7 = ((float) i6) * f6;
            float f8 = this.invTexHeight;
            float f9 = ((float) (i7 + i5)) * f8;
            float f10 = ((float) (i6 + i4)) * f6;
            float f11 = ((float) i7) * f8;
            float f12 = f2 + f4;
            float f13 = f3 + f5;
            if (!z) {
                float f14 = f7;
                f7 = f10;
                f10 = f14;
            }
            if (z2) {
                float f15 = f9;
                f9 = f11;
                f11 = f15;
            }
            float f16 = this.lightPacked;
            float f17 = this.darkPacked;
            int i15 = this.vertexIndex;
            int i16 = i15 + 1;
            fArr[i15] = f2;
            int i17 = i16 + 1;
            fArr[i16] = f3;
            int i18 = i17 + 1;
            fArr[i17] = f16;
            int i19 = i18 + 1;
            fArr[i18] = f17;
            int i20 = i19 + 1;
            fArr[i19] = f10;
            int i21 = i20 + 1;
            fArr[i20] = f9;
            int i22 = i21 + 1;
            fArr[i21] = f2;
            int i23 = i22 + 1;
            fArr[i22] = f13;
            int i24 = i23 + 1;
            fArr[i23] = f16;
            int i25 = i24 + 1;
            fArr[i24] = f17;
            int i26 = i25 + 1;
            fArr[i25] = f10;
            int i27 = i26 + 1;
            fArr[i26] = f11;
            int i28 = i27 + 1;
            fArr[i27] = f12;
            int i29 = i28 + 1;
            fArr[i28] = f13;
            int i30 = i29 + 1;
            fArr[i29] = f16;
            int i31 = i30 + 1;
            fArr[i30] = f17;
            int i32 = i31 + 1;
            fArr[i31] = f7;
            int i33 = i32 + 1;
            fArr[i32] = f11;
            int i34 = i33 + 1;
            fArr[i33] = f12;
            int i35 = i34 + 1;
            fArr[i34] = f3;
            int i36 = i35 + 1;
            fArr[i35] = f16;
            int i37 = i36 + 1;
            fArr[i36] = f17;
            int i38 = i37 + 1;
            fArr[i37] = f7;
            fArr[i38] = f9;
            this.vertexIndex = i38 + 1;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void draw(n nVar, float f2, float f3, int i2, int i3, int i4, int i5) {
        if (this.drawing) {
            short[] sArr = this.triangles;
            float[] fArr = this.vertices;
            if (nVar != this.lastTexture) {
                switchTexture(nVar);
            } else if (this.triangleIndex + 6 > sArr.length || this.vertexIndex + 24 > fArr.length) {
                flush();
            }
            int i6 = this.triangleIndex;
            int i7 = this.vertexIndex;
            int i8 = i7 / 6;
            int i9 = i6 + 1;
            short s = (short) i8;
            sArr[i6] = s;
            int i10 = i9 + 1;
            sArr[i9] = (short) (i8 + 1);
            int i11 = i10 + 1;
            short s2 = (short) (i8 + 2);
            sArr[i10] = s2;
            int i12 = i11 + 1;
            sArr[i11] = s2;
            int i13 = i12 + 1;
            sArr[i12] = (short) (i8 + 3);
            sArr[i13] = s;
            this.triangleIndex = i13 + 1;
            float f4 = this.invTexWidth;
            float f5 = ((float) i2) * f4;
            float f6 = this.invTexHeight;
            float f7 = ((float) (i3 + i5)) * f6;
            float f8 = ((float) (i2 + i4)) * f4;
            float f9 = ((float) i3) * f6;
            float f10 = ((float) i4) + f2;
            float f11 = ((float) i5) + f3;
            float f12 = this.lightPacked;
            float f13 = this.darkPacked;
            int i14 = i7 + 1;
            fArr[i7] = f2;
            int i15 = i14 + 1;
            fArr[i14] = f3;
            int i16 = i15 + 1;
            fArr[i15] = f12;
            int i17 = i16 + 1;
            fArr[i16] = f13;
            int i18 = i17 + 1;
            fArr[i17] = f5;
            int i19 = i18 + 1;
            fArr[i18] = f7;
            int i20 = i19 + 1;
            fArr[i19] = f2;
            int i21 = i20 + 1;
            fArr[i20] = f11;
            int i22 = i21 + 1;
            fArr[i21] = f12;
            int i23 = i22 + 1;
            fArr[i22] = f13;
            int i24 = i23 + 1;
            fArr[i23] = f5;
            int i25 = i24 + 1;
            fArr[i24] = f9;
            int i26 = i25 + 1;
            fArr[i25] = f10;
            int i27 = i26 + 1;
            fArr[i26] = f11;
            int i28 = i27 + 1;
            fArr[i27] = f12;
            int i29 = i28 + 1;
            fArr[i28] = f13;
            int i30 = i29 + 1;
            fArr[i29] = f8;
            int i31 = i30 + 1;
            fArr[i30] = f9;
            int i32 = i31 + 1;
            fArr[i31] = f10;
            int i33 = i32 + 1;
            fArr[i32] = f3;
            int i34 = i33 + 1;
            fArr[i33] = f12;
            int i35 = i34 + 1;
            fArr[i34] = f13;
            int i36 = i35 + 1;
            fArr[i35] = f8;
            fArr[i36] = f7;
            this.vertexIndex = i36 + 1;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void draw(n nVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
        if (this.drawing) {
            short[] sArr = this.triangles;
            float[] fArr = this.vertices;
            if (nVar != this.lastTexture) {
                switchTexture(nVar);
            } else if (this.triangleIndex + 6 > sArr.length || this.vertexIndex + 24 > fArr.length) {
                flush();
            }
            int i2 = this.triangleIndex;
            int i3 = this.vertexIndex;
            int i4 = i3 / 6;
            int i5 = i2 + 1;
            short s = (short) i4;
            sArr[i2] = s;
            int i6 = i5 + 1;
            sArr[i5] = (short) (i4 + 1);
            int i7 = i6 + 1;
            short s2 = (short) (i4 + 2);
            sArr[i6] = s2;
            int i8 = i7 + 1;
            sArr[i7] = s2;
            int i9 = i8 + 1;
            sArr[i8] = (short) (i4 + 3);
            sArr[i9] = s;
            this.triangleIndex = i9 + 1;
            float f10 = f2 + f4;
            float f11 = f3 + f5;
            float f12 = this.lightPacked;
            float f13 = this.darkPacked;
            int i10 = i3 + 1;
            fArr[i3] = f2;
            int i11 = i10 + 1;
            fArr[i10] = f3;
            int i12 = i11 + 1;
            fArr[i11] = f12;
            int i13 = i12 + 1;
            fArr[i12] = f13;
            int i14 = i13 + 1;
            fArr[i13] = f6;
            int i15 = i14 + 1;
            fArr[i14] = f7;
            int i16 = i15 + 1;
            fArr[i15] = f2;
            int i17 = i16 + 1;
            fArr[i16] = f11;
            int i18 = i17 + 1;
            fArr[i17] = f12;
            int i19 = i18 + 1;
            fArr[i18] = f13;
            int i20 = i19 + 1;
            fArr[i19] = f6;
            int i21 = i20 + 1;
            fArr[i20] = f9;
            int i22 = i21 + 1;
            fArr[i21] = f10;
            int i23 = i22 + 1;
            fArr[i22] = f11;
            int i24 = i23 + 1;
            fArr[i23] = f12;
            int i25 = i24 + 1;
            fArr[i24] = f13;
            int i26 = i25 + 1;
            fArr[i25] = f8;
            int i27 = i26 + 1;
            fArr[i26] = f9;
            int i28 = i27 + 1;
            fArr[i27] = f10;
            int i29 = i28 + 1;
            fArr[i28] = f3;
            int i30 = i29 + 1;
            fArr[i29] = f12;
            int i31 = i30 + 1;
            fArr[i30] = f13;
            int i32 = i31 + 1;
            fArr[i31] = f8;
            fArr[i32] = f7;
            this.vertexIndex = i32 + 1;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void draw(n nVar, float f2, float f3) {
        draw(nVar, f2, f3, (float) nVar.r(), (float) nVar.p());
    }

    public void draw(n nVar, float f2, float f3, float f4, float f5) {
        if (this.drawing) {
            short[] sArr = this.triangles;
            float[] fArr = this.vertices;
            if (nVar != this.lastTexture) {
                switchTexture(nVar);
            } else if (this.triangleIndex + 6 > sArr.length || this.vertexIndex + 24 > fArr.length) {
                flush();
            }
            int i2 = this.triangleIndex;
            int i3 = this.vertexIndex;
            int i4 = i3 / 6;
            int i5 = i2 + 1;
            short s = (short) i4;
            sArr[i2] = s;
            int i6 = i5 + 1;
            sArr[i5] = (short) (i4 + 1);
            int i7 = i6 + 1;
            short s2 = (short) (i4 + 2);
            sArr[i6] = s2;
            int i8 = i7 + 1;
            sArr[i7] = s2;
            int i9 = i8 + 1;
            sArr[i8] = (short) (i4 + 3);
            sArr[i9] = s;
            this.triangleIndex = i9 + 1;
            float f6 = f4 + f2;
            float f7 = f5 + f3;
            float f8 = this.lightPacked;
            float f9 = this.darkPacked;
            int i10 = i3 + 1;
            fArr[i3] = f2;
            int i11 = i10 + 1;
            fArr[i10] = f3;
            int i12 = i11 + 1;
            fArr[i11] = f8;
            int i13 = i12 + 1;
            fArr[i12] = f9;
            int i14 = i13 + 1;
            fArr[i13] = 0.0f;
            int i15 = i14 + 1;
            fArr[i14] = 1.0f;
            int i16 = i15 + 1;
            fArr[i15] = f2;
            int i17 = i16 + 1;
            fArr[i16] = f7;
            int i18 = i17 + 1;
            fArr[i17] = f8;
            int i19 = i18 + 1;
            fArr[i18] = f9;
            int i20 = i19 + 1;
            fArr[i19] = 0.0f;
            int i21 = i20 + 1;
            fArr[i20] = 0.0f;
            int i22 = i21 + 1;
            fArr[i21] = f6;
            int i23 = i22 + 1;
            fArr[i22] = f7;
            int i24 = i23 + 1;
            fArr[i23] = f8;
            int i25 = i24 + 1;
            fArr[i24] = f9;
            int i26 = i25 + 1;
            fArr[i25] = 1.0f;
            int i27 = i26 + 1;
            fArr[i26] = 0.0f;
            int i28 = i27 + 1;
            fArr[i27] = f6;
            int i29 = i28 + 1;
            fArr[i28] = f3;
            int i30 = i29 + 1;
            fArr[i29] = f8;
            int i31 = i30 + 1;
            fArr[i30] = f9;
            int i32 = i31 + 1;
            fArr[i31] = 1.0f;
            fArr[i32] = 1.0f;
            this.vertexIndex = i32 + 1;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void draw(n nVar, float[] fArr, int i2, int i3, short[] sArr, int i4, int i5) {
        if (this.drawing) {
            short[] sArr2 = this.triangles;
            float[] fArr2 = this.vertices;
            if (nVar != this.lastTexture) {
                switchTexture(nVar);
            } else if (this.triangleIndex + i5 > sArr2.length || this.vertexIndex + ((i3 / 5) * 6) > fArr2.length) {
                flush();
            }
            int i6 = this.triangleIndex;
            int i7 = this.vertexIndex / 6;
            int i8 = i5 + i4;
            while (i4 < i8) {
                sArr2[i6] = (short) (sArr[i4] + i7);
                i4++;
                i6++;
            }
            this.triangleIndex = i6;
            int i9 = this.vertexIndex;
            int i10 = i3 + i2;
            while (i2 < i10) {
                int i11 = i9 + 1;
                fArr2[i9] = fArr[i2];
                int i12 = i11 + 1;
                fArr2[i11] = fArr[i2 + 1];
                int i13 = i12 + 1;
                fArr2[i12] = fArr[i2 + 2];
                int i14 = i13 + 1;
                fArr2[i13] = 0.0f;
                int i15 = i14 + 1;
                fArr2[i14] = fArr[i2 + 3];
                i9 = i15 + 1;
                fArr2[i15] = fArr[i2 + 4];
                i2 += 5;
            }
            this.vertexIndex = i9;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void draw(n nVar, float[] fArr, int i2, int i3) {
        if (this.drawing) {
            short[] sArr = this.triangles;
            float[] fArr2 = this.vertices;
            int i4 = (i3 / 20) * 6;
            if (nVar != this.lastTexture) {
                switchTexture(nVar);
            } else if (this.triangleIndex + i4 > sArr.length || this.vertexIndex + ((i3 / 5) * 6) > fArr2.length) {
                flush();
            }
            int i5 = this.vertexIndex;
            int i6 = this.triangleIndex;
            short s = (short) (i5 / 6);
            int i7 = i4 + i6;
            while (i6 < i7) {
                sArr[i6] = s;
                sArr[i6 + 1] = (short) (s + 1);
                short s2 = (short) (s + 2);
                sArr[i6 + 2] = s2;
                sArr[i6 + 3] = s2;
                sArr[i6 + 4] = (short) (s + 3);
                sArr[i6 + 5] = s;
                i6 += 6;
                s = (short) (s + 4);
            }
            this.triangleIndex = i6;
            int i8 = this.vertexIndex;
            int i9 = i3 + i2;
            while (i2 < i9) {
                int i10 = i8 + 1;
                fArr2[i8] = fArr[i2];
                int i11 = i10 + 1;
                fArr2[i10] = fArr[i2 + 1];
                int i12 = i11 + 1;
                fArr2[i11] = fArr[i2 + 2];
                int i13 = i12 + 1;
                fArr2[i12] = 0.0f;
                int i14 = i13 + 1;
                fArr2[i13] = fArr[i2 + 3];
                i8 = i14 + 1;
                fArr2[i14] = fArr[i2 + 4];
                i2 += 5;
            }
            this.vertexIndex = i8;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void draw(com.badlogic.gdx.graphics.g2d.r rVar, float f2, float f3) {
        draw(rVar, f2, f3, (float) rVar.b(), (float) rVar.a());
    }

    public void draw(com.badlogic.gdx.graphics.g2d.r rVar, float f2, float f3, float f4, float f5) {
        if (this.drawing) {
            short[] sArr = this.triangles;
            float[] fArr = this.vertices;
            n e2 = rVar.e();
            if (e2 != this.lastTexture) {
                switchTexture(e2);
            } else if (this.triangleIndex + 6 > sArr.length || this.vertexIndex + 24 > fArr.length) {
                flush();
            }
            int i2 = this.triangleIndex;
            int i3 = this.vertexIndex / 6;
            int i4 = i2 + 1;
            short s = (short) i3;
            sArr[i2] = s;
            int i5 = i4 + 1;
            sArr[i4] = (short) (i3 + 1);
            int i6 = i5 + 1;
            short s2 = (short) (i3 + 2);
            sArr[i5] = s2;
            int i7 = i6 + 1;
            sArr[i6] = s2;
            int i8 = i7 + 1;
            sArr[i7] = (short) (i3 + 3);
            sArr[i8] = s;
            this.triangleIndex = i8 + 1;
            float f6 = f4 + f2;
            float f7 = f5 + f3;
            float f8 = rVar.f();
            float i9 = rVar.i();
            float g2 = rVar.g();
            float h2 = rVar.h();
            float f9 = this.lightPacked;
            float f10 = this.darkPacked;
            int i10 = this.vertexIndex;
            int i11 = i10 + 1;
            fArr[i10] = f2;
            int i12 = i11 + 1;
            fArr[i11] = f3;
            int i13 = i12 + 1;
            fArr[i12] = f9;
            int i14 = i13 + 1;
            fArr[i13] = f10;
            int i15 = i14 + 1;
            fArr[i14] = f8;
            int i16 = i15 + 1;
            fArr[i15] = i9;
            int i17 = i16 + 1;
            fArr[i16] = f2;
            int i18 = i17 + 1;
            fArr[i17] = f7;
            int i19 = i18 + 1;
            fArr[i18] = f9;
            int i20 = i19 + 1;
            fArr[i19] = f10;
            int i21 = i20 + 1;
            fArr[i20] = f8;
            int i22 = i21 + 1;
            fArr[i21] = h2;
            int i23 = i22 + 1;
            fArr[i22] = f6;
            int i24 = i23 + 1;
            fArr[i23] = f7;
            int i25 = i24 + 1;
            fArr[i24] = f9;
            int i26 = i25 + 1;
            fArr[i25] = f10;
            int i27 = i26 + 1;
            fArr[i26] = g2;
            int i28 = i27 + 1;
            fArr[i27] = h2;
            int i29 = i28 + 1;
            fArr[i28] = f6;
            int i30 = i29 + 1;
            fArr[i29] = f3;
            int i31 = i30 + 1;
            fArr[i30] = f9;
            int i32 = i31 + 1;
            fArr[i31] = f10;
            int i33 = i32 + 1;
            fArr[i32] = g2;
            fArr[i33] = i9;
            this.vertexIndex = i33 + 1;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void draw(com.badlogic.gdx.graphics.g2d.r rVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10) {
        float f11;
        float f12;
        float f13;
        float f14;
        float f15;
        float f16 = f4;
        float f17 = f5;
        if (this.drawing) {
            short[] sArr = this.triangles;
            float[] fArr = this.vertices;
            n e2 = rVar.e();
            if (e2 != this.lastTexture) {
                switchTexture(e2);
            } else if (this.triangleIndex + 6 > sArr.length || this.vertexIndex + 24 > fArr.length) {
                flush();
            }
            int i2 = this.triangleIndex;
            int i3 = this.vertexIndex / 6;
            int i4 = i2 + 1;
            short s = (short) i3;
            sArr[i2] = s;
            int i5 = i4 + 1;
            sArr[i4] = (short) (i3 + 1);
            int i6 = i5 + 1;
            short s2 = (short) (i3 + 2);
            sArr[i5] = s2;
            int i7 = i6 + 1;
            sArr[i6] = s2;
            int i8 = i7 + 1;
            sArr[i7] = (short) (i3 + 3);
            sArr[i8] = s;
            this.triangleIndex = i8 + 1;
            float f18 = f2 + f16;
            float f19 = f3 + f17;
            float f20 = -f16;
            float f21 = -f17;
            float f22 = f6 - f16;
            float f23 = f7 - f17;
            if (!(f8 == 1.0f && f9 == 1.0f)) {
                f20 *= f8;
                f21 *= f9;
                f22 *= f8;
                f23 *= f9;
            }
            if (f10 != Animation.CurveTimeline.LINEAR) {
                float b2 = h.b(f10);
                float h2 = h.h(f10);
                float f24 = b2 * f20;
                float f25 = f24 - (h2 * f21);
                float f26 = f20 * h2;
                f21 = (f21 * b2) + f26;
                float f27 = h2 * f23;
                f12 = f24 - f27;
                float f28 = f23 * b2;
                float f29 = f26 + f28;
                f14 = (b2 * f22) - f27;
                float f30 = f28 + (h2 * f22);
                float f31 = f30 - (f29 - f21);
                f13 = (f14 - f12) + f25;
                f15 = f30;
                f23 = f29;
                f20 = f25;
                f11 = f31;
            } else {
                f14 = f22;
                f13 = f14;
                f15 = f23;
                f12 = f20;
                f11 = f21;
            }
            float f32 = f20 + f18;
            float f33 = f21 + f19;
            float f34 = f12 + f18;
            float f35 = f23 + f19;
            float f36 = f14 + f18;
            float f37 = f15 + f19;
            float f38 = f13 + f18;
            float f39 = f11 + f19;
            float f40 = rVar.f();
            float i9 = rVar.i();
            float g2 = rVar.g();
            float h3 = rVar.h();
            float f41 = this.lightPacked;
            float f42 = this.darkPacked;
            float f43 = f39;
            int i10 = this.vertexIndex;
            int i11 = i10 + 1;
            fArr[i10] = f32;
            int i12 = i11 + 1;
            fArr[i11] = f33;
            int i13 = i12 + 1;
            fArr[i12] = f41;
            int i14 = i13 + 1;
            fArr[i13] = f42;
            int i15 = i14 + 1;
            fArr[i14] = f40;
            int i16 = i15 + 1;
            fArr[i15] = i9;
            int i17 = i16 + 1;
            fArr[i16] = f34;
            int i18 = i17 + 1;
            fArr[i17] = f35;
            int i19 = i18 + 1;
            fArr[i18] = f41;
            int i20 = i19 + 1;
            fArr[i19] = f42;
            int i21 = i20 + 1;
            fArr[i20] = f40;
            int i22 = i21 + 1;
            fArr[i21] = h3;
            int i23 = i22 + 1;
            fArr[i22] = f36;
            int i24 = i23 + 1;
            fArr[i23] = f37;
            int i25 = i24 + 1;
            fArr[i24] = f41;
            int i26 = i25 + 1;
            fArr[i25] = f42;
            int i27 = i26 + 1;
            fArr[i26] = g2;
            int i28 = i27 + 1;
            fArr[i27] = h3;
            int i29 = i28 + 1;
            fArr[i28] = f38;
            int i30 = i29 + 1;
            fArr[i29] = f43;
            int i31 = i30 + 1;
            fArr[i30] = f41;
            int i32 = i31 + 1;
            fArr[i31] = f42;
            int i33 = i32 + 1;
            fArr[i32] = g2;
            fArr[i33] = i9;
            this.vertexIndex = i33 + 1;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void draw(com.badlogic.gdx.graphics.g2d.r rVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, boolean z) {
        float f11;
        float f12;
        float f13;
        float f14;
        float f15;
        float f16;
        float f17;
        float f18;
        float f19;
        float f20;
        float f21;
        float f22;
        float f23;
        float f24 = f4;
        float f25 = f5;
        if (this.drawing) {
            short[] sArr = this.triangles;
            float[] fArr = this.vertices;
            n e2 = rVar.e();
            if (e2 != this.lastTexture) {
                switchTexture(e2);
            } else if (this.triangleIndex + 6 > sArr.length || this.vertexIndex + 24 > fArr.length) {
                flush();
            }
            int i2 = this.triangleIndex;
            int i3 = this.vertexIndex / 6;
            int i4 = i2 + 1;
            short s = (short) i3;
            sArr[i2] = s;
            int i5 = i4 + 1;
            sArr[i4] = (short) (i3 + 1);
            int i6 = i5 + 1;
            short s2 = (short) (i3 + 2);
            sArr[i5] = s2;
            int i7 = i6 + 1;
            sArr[i6] = s2;
            int i8 = i7 + 1;
            sArr[i7] = (short) (i3 + 3);
            sArr[i8] = s;
            this.triangleIndex = i8 + 1;
            float f26 = f2 + f24;
            float f27 = f3 + f25;
            float f28 = -f24;
            float f29 = -f25;
            float f30 = f6 - f24;
            float f31 = f7 - f25;
            if (!(f8 == 1.0f && f9 == 1.0f)) {
                f28 *= f8;
                f29 *= f9;
                f30 *= f8;
                f31 *= f9;
            }
            if (f10 != Animation.CurveTimeline.LINEAR) {
                float b2 = h.b(f10);
                float h2 = h.h(f10);
                float f32 = b2 * f28;
                float f33 = f32 - (h2 * f29);
                float f34 = f28 * h2;
                f29 = (f29 * b2) + f34;
                float f35 = h2 * f31;
                f12 = f32 - f35;
                float f36 = f31 * b2;
                float f37 = f34 + f36;
                f14 = (b2 * f30) - f35;
                float f38 = f36 + (h2 * f30);
                float f39 = f38 - (f37 - f29);
                f13 = (f14 - f12) + f33;
                f15 = f38;
                f31 = f37;
                f28 = f33;
                f11 = f39;
            } else {
                f14 = f30;
                f13 = f14;
                f15 = f31;
                f12 = f28;
                f11 = f29;
            }
            float f40 = f28 + f26;
            float f41 = f29 + f27;
            float f42 = f12 + f26;
            float f43 = f31 + f27;
            float f44 = f14 + f26;
            float f45 = f15 + f27;
            float f46 = f13 + f26;
            float f47 = f11 + f27;
            if (z) {
                f23 = rVar.g();
                f22 = rVar.i();
                f21 = rVar.f();
                f20 = rVar.i();
                f19 = rVar.f();
                f18 = rVar.h();
                f17 = rVar.g();
                f16 = rVar.h();
            } else {
                f23 = rVar.f();
                f22 = rVar.h();
                f21 = rVar.g();
                f20 = rVar.h();
                f19 = rVar.g();
                f18 = rVar.i();
                f17 = rVar.f();
                f16 = rVar.i();
            }
            float f48 = f47;
            float f49 = this.lightPacked;
            float f50 = f46;
            float f51 = this.darkPacked;
            float f52 = f18;
            int i9 = this.vertexIndex;
            int i10 = i9 + 1;
            fArr[i9] = f40;
            int i11 = i10 + 1;
            fArr[i10] = f41;
            int i12 = i11 + 1;
            fArr[i11] = f49;
            int i13 = i12 + 1;
            fArr[i12] = f51;
            int i14 = i13 + 1;
            fArr[i13] = f23;
            int i15 = i14 + 1;
            fArr[i14] = f22;
            int i16 = i15 + 1;
            fArr[i15] = f42;
            int i17 = i16 + 1;
            fArr[i16] = f43;
            int i18 = i17 + 1;
            fArr[i17] = f49;
            int i19 = i18 + 1;
            fArr[i18] = f51;
            int i20 = i19 + 1;
            fArr[i19] = f21;
            int i21 = i20 + 1;
            fArr[i20] = f20;
            int i22 = i21 + 1;
            fArr[i21] = f44;
            int i23 = i22 + 1;
            fArr[i22] = f45;
            int i24 = i23 + 1;
            fArr[i23] = f49;
            int i25 = i24 + 1;
            fArr[i24] = f51;
            int i26 = i25 + 1;
            fArr[i25] = f19;
            int i27 = i26 + 1;
            fArr[i26] = f52;
            int i28 = i27 + 1;
            fArr[i27] = f50;
            int i29 = i28 + 1;
            fArr[i28] = f48;
            int i30 = i29 + 1;
            fArr[i29] = f49;
            int i31 = i30 + 1;
            fArr[i30] = f51;
            int i32 = i31 + 1;
            fArr[i31] = f17;
            fArr[i32] = f16;
            this.vertexIndex = i32 + 1;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }

    public void draw(com.badlogic.gdx.graphics.g2d.r rVar, float f2, float f3, a aVar) {
        a aVar2 = aVar;
        if (this.drawing) {
            short[] sArr = this.triangles;
            float[] fArr = this.vertices;
            n e2 = rVar.e();
            if (e2 != this.lastTexture) {
                switchTexture(e2);
            } else if (this.triangleIndex + 6 > sArr.length || this.vertexIndex + 24 > fArr.length) {
                flush();
            }
            int i2 = this.triangleIndex;
            int i3 = this.vertexIndex / 6;
            int i4 = i2 + 1;
            short s = (short) i3;
            sArr[i2] = s;
            int i5 = i4 + 1;
            sArr[i4] = (short) (i3 + 1);
            int i6 = i5 + 1;
            short s2 = (short) (i3 + 2);
            sArr[i5] = s2;
            int i7 = i6 + 1;
            sArr[i6] = s2;
            int i8 = i7 + 1;
            sArr[i7] = (short) (i3 + 3);
            sArr[i8] = s;
            this.triangleIndex = i8 + 1;
            float f4 = aVar2.f5241c;
            float f5 = aVar2.f5244f;
            float f6 = aVar2.f5240b;
            float f7 = (f6 * f3) + f4;
            float f8 = aVar2.f5243e;
            float f9 = aVar2.f5239a;
            float f10 = (f9 * f2) + (f6 * f3) + f4;
            float f11 = aVar2.f5242d;
            float f12 = (f9 * f2) + f4;
            float f13 = rVar.f();
            float i9 = rVar.i();
            float g2 = rVar.g();
            float h2 = rVar.h();
            float f14 = this.lightPacked;
            float f15 = this.darkPacked;
            float f16 = (f11 * f2) + f5;
            int i10 = this.vertexIndex;
            int i11 = i10 + 1;
            fArr[i10] = f4;
            int i12 = i11 + 1;
            fArr[i11] = f5;
            int i13 = i12 + 1;
            fArr[i12] = f14;
            int i14 = i13 + 1;
            fArr[i13] = f15;
            int i15 = i14 + 1;
            fArr[i14] = f13;
            int i16 = i15 + 1;
            fArr[i15] = i9;
            int i17 = i16 + 1;
            fArr[i16] = f7;
            int i18 = i17 + 1;
            fArr[i17] = (f8 * f3) + f5;
            int i19 = i18 + 1;
            fArr[i18] = f14;
            int i20 = i19 + 1;
            fArr[i19] = f15;
            int i21 = i20 + 1;
            fArr[i20] = f13;
            int i22 = i21 + 1;
            fArr[i21] = h2;
            int i23 = i22 + 1;
            fArr[i22] = f10;
            int i24 = i23 + 1;
            fArr[i23] = (f11 * f2) + (f8 * f3) + f5;
            int i25 = i24 + 1;
            fArr[i24] = f14;
            int i26 = i25 + 1;
            fArr[i25] = f15;
            int i27 = i26 + 1;
            fArr[i26] = g2;
            int i28 = i27 + 1;
            fArr[i27] = h2;
            int i29 = i28 + 1;
            fArr[i28] = f12;
            int i30 = i29 + 1;
            fArr[i29] = f16;
            int i31 = i30 + 1;
            fArr[i30] = f14;
            int i32 = i31 + 1;
            fArr[i31] = f15;
            int i33 = i32 + 1;
            fArr[i32] = g2;
            fArr[i33] = i9;
            this.vertexIndex = i33 + 1;
            return;
        }
        throw new IllegalStateException("begin must be called before draw.");
    }
}
