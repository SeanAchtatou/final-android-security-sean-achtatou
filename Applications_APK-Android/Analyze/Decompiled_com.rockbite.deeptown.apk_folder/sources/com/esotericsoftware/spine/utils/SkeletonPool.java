package com.esotericsoftware.spine.utils;

import com.badlogic.gdx.utils.f0;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;

public class SkeletonPool extends f0<Skeleton> {
    private SkeletonData skeletonData;

    public SkeletonPool(SkeletonData skeletonData2) {
        this.skeletonData = skeletonData2;
    }

    /* access modifiers changed from: protected */
    public Skeleton newObject() {
        return new Skeleton(this.skeletonData);
    }

    public SkeletonPool(SkeletonData skeletonData2, int i2) {
        super(i2);
        this.skeletonData = skeletonData2;
    }

    public SkeletonPool(SkeletonData skeletonData2, int i2, int i3) {
        super(i2, i3);
        this.skeletonData = skeletonData2;
    }
}
