package com.esotericsoftware.spine.utils;

import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.f;
import com.badlogic.gdx.utils.f0;
import com.badlogic.gdx.utils.m;
import com.badlogic.gdx.utils.n0;
import com.esotericsoftware.spine.Animation;

class Triangulator {
    private final a<m> convexPolygons = new a<>();
    private final a<n0> convexPolygonsIndices = new a<>();
    private final n0 indicesArray = new n0();
    private final f isConcaveArray = new f();
    private final f0<n0> polygonIndicesPool = new f0() {
        /* access modifiers changed from: protected */
        public n0 newObject() {
            return new n0(16);
        }
    };
    private final f0<m> polygonPool = new f0() {
        /* access modifiers changed from: protected */
        public m newObject() {
            return new m(16);
        }
    };
    private final n0 triangles = new n0();

    Triangulator() {
    }

    private static boolean isConcave(int i2, int i3, float[] fArr, short[] sArr) {
        int i4 = sArr[((i3 + i2) - 1) % i3] << 1;
        int i5 = sArr[i2] << 1;
        int i6 = sArr[(i2 + 1) % i3] << 1;
        return !positiveArea(fArr[i4], fArr[i4 + 1], fArr[i5], fArr[i5 + 1], fArr[i6], fArr[i6 + 1]);
    }

    private static boolean positiveArea(float f2, float f3, float f4, float f5, float f6, float f7) {
        return ((f2 * (f7 - f5)) + (f4 * (f3 - f7))) + (f6 * (f5 - f3)) >= Animation.CurveTimeline.LINEAR;
    }

    private static int winding(float f2, float f3, float f4, float f5, float f6, float f7) {
        float f8 = f4 - f2;
        float f9 = f5 - f3;
        return (((f6 * f9) - (f7 * f8)) + (f8 * f3)) - (f2 * f9) >= Animation.CurveTimeline.LINEAR ? 1 : -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0115  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.badlogic.gdx.utils.a<com.badlogic.gdx.utils.m> decompose(com.badlogic.gdx.utils.m r35, com.badlogic.gdx.utils.n0 r36) {
        /*
            r34 = this;
            r0 = r34
            r1 = r35
            r2 = r36
            float[] r1 = r1.f5527a
            com.badlogic.gdx.utils.a<com.badlogic.gdx.utils.m> r3 = r0.convexPolygons
            com.badlogic.gdx.utils.f0<com.badlogic.gdx.utils.m> r4 = r0.polygonPool
            r4.freeAll(r3)
            r3.clear()
            com.badlogic.gdx.utils.a<com.badlogic.gdx.utils.n0> r4 = r0.convexPolygonsIndices
            com.badlogic.gdx.utils.f0<com.badlogic.gdx.utils.n0> r5 = r0.polygonIndicesPool
            r5.freeAll(r4)
            r4.clear()
            com.badlogic.gdx.utils.f0<com.badlogic.gdx.utils.n0> r5 = r0.polygonIndicesPool
            java.lang.Object r5 = r5.obtain()
            com.badlogic.gdx.utils.n0 r5 = (com.badlogic.gdx.utils.n0) r5
            r5.a()
            com.badlogic.gdx.utils.f0<com.badlogic.gdx.utils.m> r6 = r0.polygonPool
            java.lang.Object r6 = r6.obtain()
            com.badlogic.gdx.utils.m r6 = (com.badlogic.gdx.utils.m) r6
            r6.a()
            short[] r7 = r2.f5542a
            int r2 = r2.f5543b
            r8 = -1
            r10 = r5
            r5 = 0
            r11 = 0
        L_0x003a:
            r14 = 1
            if (r5 >= r2) goto L_0x0121
            short r15 = r7[r5]
            int r15 = r15 << r14
            int r16 = r5 + 1
            short r16 = r7[r16]
            int r13 = r16 << 1
            int r16 = r5 + 2
            short r16 = r7[r16]
            int r9 = r16 << 1
            r14 = r1[r15]
            int r17 = r15 + 1
            r12 = r1[r17]
            r22 = r2
            r2 = r1[r13]
            int r17 = r13 + 1
            r23 = r7
            r7 = r1[r17]
            r30 = r5
            r5 = r1[r9]
            int r17 = r9 + 1
            r19 = r13
            r13 = r1[r17]
            r31 = r1
            if (r8 != r15) goto L_0x00b2
            int r1 = r6.f5528b
            int r1 = r1 + -4
            r17 = r8
            float[] r8 = r6.f5527a
            r24 = r8[r1]
            int r20 = r1 + 1
            r25 = r8[r20]
            int r20 = r1 + 2
            r26 = r8[r20]
            r18 = 3
            int r1 = r1 + 3
            r27 = r8[r1]
            r28 = r5
            r29 = r13
            int r1 = winding(r24, r25, r26, r27, r28, r29)
            r20 = 0
            r26 = r8[r20]
            r16 = 1
            r27 = r8[r16]
            r20 = 2
            r28 = r8[r20]
            r18 = 3
            r29 = r8[r18]
            r24 = r5
            r25 = r13
            int r8 = winding(r24, r25, r26, r27, r28, r29)
            if (r1 != r11) goto L_0x00b4
            if (r8 != r11) goto L_0x00b4
            r6.a(r5)
            r6.a(r13)
            r10.a(r9)
            r16 = 1
            goto L_0x00b6
        L_0x00b2:
            r17 = r8
        L_0x00b4:
            r16 = 0
        L_0x00b6:
            if (r16 != 0) goto L_0x0115
            int r1 = r6.f5528b
            if (r1 <= 0) goto L_0x00c3
            r3.add(r6)
            r4.add(r10)
            goto L_0x00cd
        L_0x00c3:
            com.badlogic.gdx.utils.f0<com.badlogic.gdx.utils.m> r1 = r0.polygonPool
            r1.free(r6)
            com.badlogic.gdx.utils.f0<com.badlogic.gdx.utils.n0> r1 = r0.polygonIndicesPool
            r1.free(r10)
        L_0x00cd:
            com.badlogic.gdx.utils.f0<com.badlogic.gdx.utils.m> r1 = r0.polygonPool
            java.lang.Object r1 = r1.obtain()
            com.badlogic.gdx.utils.m r1 = (com.badlogic.gdx.utils.m) r1
            r1.a()
            r1.a(r14)
            r1.a(r12)
            r1.a(r2)
            r1.a(r7)
            r1.a(r5)
            r1.a(r13)
            com.badlogic.gdx.utils.f0<com.badlogic.gdx.utils.n0> r6 = r0.polygonIndicesPool
            java.lang.Object r6 = r6.obtain()
            com.badlogic.gdx.utils.n0 r6 = (com.badlogic.gdx.utils.n0) r6
            r6.a()
            r6.a(r15)
            r8 = r19
            r6.a(r8)
            r6.a(r9)
            r16 = r14
            r17 = r12
            r18 = r2
            r19 = r7
            r20 = r5
            r21 = r13
            int r2 = winding(r16, r17, r18, r19, r20, r21)
            r11 = r2
            r10 = r6
            r8 = r15
            r6 = r1
            goto L_0x0117
        L_0x0115:
            r8 = r17
        L_0x0117:
            int r5 = r30 + 3
            r2 = r22
            r7 = r23
            r1 = r31
            goto L_0x003a
        L_0x0121:
            int r1 = r6.f5528b
            if (r1 <= 0) goto L_0x012b
            r3.add(r6)
            r4.add(r10)
        L_0x012b:
            int r1 = r3.f5374b
            r2 = 0
        L_0x012e:
            if (r2 >= r1) goto L_0x0240
            java.lang.Object r5 = r4.get(r2)
            com.badlogic.gdx.utils.n0 r5 = (com.badlogic.gdx.utils.n0) r5
            int r6 = r5.f5543b
            if (r6 != 0) goto L_0x0145
        L_0x013a:
            r28 = r1
            r31 = r2
            r33 = r3
            r29 = r4
            r1 = 1
            goto L_0x0234
        L_0x0145:
            r6 = 0
            short r7 = r5.c(r6)
            int r6 = r5.f5543b
            r8 = 1
            int r6 = r6 - r8
            short r6 = r5.c(r6)
            java.lang.Object r8 = r3.get(r2)
            com.badlogic.gdx.utils.m r8 = (com.badlogic.gdx.utils.m) r8
            int r9 = r8.f5528b
            int r9 = r9 + -4
            float[] r10 = r8.f5527a
            r11 = r10[r9]
            int r12 = r9 + 1
            r12 = r10[r12]
            int r13 = r9 + 2
            r13 = r10[r13]
            r14 = 3
            int r9 = r9 + r14
            r9 = r10[r9]
            r15 = 0
            r25 = r10[r15]
            r15 = 1
            r26 = r10[r15]
            r15 = 2
            r27 = r10[r15]
            r10 = r10[r14]
            r19 = r11
            r20 = r12
            r21 = r13
            r22 = r9
            r23 = r25
            r24 = r26
            int r14 = winding(r19, r20, r21, r22, r23, r24)
            r15 = r9
            r9 = 0
        L_0x0189:
            if (r9 >= r1) goto L_0x013a
            if (r9 != r2) goto L_0x019a
            r28 = r1
            r31 = r2
            r33 = r3
            r29 = r4
        L_0x0195:
            r0 = r5
            r32 = r9
            goto L_0x0224
        L_0x019a:
            java.lang.Object r17 = r4.get(r9)
            r28 = r1
            r1 = r17
            com.badlogic.gdx.utils.n0 r1 = (com.badlogic.gdx.utils.n0) r1
            r29 = r4
            int r4 = r1.f5543b
            r0 = 3
            if (r4 == r0) goto L_0x01b0
            r31 = r2
            r33 = r3
            goto L_0x0195
        L_0x01b0:
            r4 = 0
            short r0 = r1.c(r4)
            r31 = r2
            r4 = 1
            short r2 = r1.c(r4)
            r35 = r5
            r4 = 2
            short r5 = r1.c(r4)
            java.lang.Object r17 = r3.get(r9)
            r32 = r9
            r9 = r17
            com.badlogic.gdx.utils.m r9 = (com.badlogic.gdx.utils.m) r9
            r33 = r3
            int r3 = r9.f5528b
            int r3 = r3 - r4
            float r3 = r9.b(r3)
            int r4 = r9.f5528b
            r16 = 1
            int r4 = r4 + -1
            float r4 = r9.b(r4)
            if (r0 != r7) goto L_0x0222
            if (r2 == r6) goto L_0x01e5
            goto L_0x0222
        L_0x01e5:
            r19 = r11
            r20 = r12
            r21 = r13
            r22 = r15
            r23 = r3
            r24 = r4
            int r0 = winding(r19, r20, r21, r22, r23, r24)
            r17 = r3
            r18 = r4
            r19 = r25
            r20 = r26
            r21 = r27
            r22 = r10
            int r2 = winding(r17, r18, r19, r20, r21, r22)
            if (r0 != r14) goto L_0x0222
            if (r2 != r14) goto L_0x0222
            r9.a()
            r1.a()
            r8.a(r3)
            r8.a(r4)
            r0 = r35
            r0.a(r5)
            r11 = r13
            r12 = r15
            r1 = 1
            r32 = 0
            r13 = r3
            r15 = r4
            goto L_0x0225
        L_0x0222:
            r0 = r35
        L_0x0224:
            r1 = 1
        L_0x0225:
            int r9 = r32 + 1
            r5 = r0
            r1 = r28
            r4 = r29
            r2 = r31
            r3 = r33
            r0 = r34
            goto L_0x0189
        L_0x0234:
            int r2 = r31 + 1
            r0 = r34
            r1 = r28
            r4 = r29
            r3 = r33
            goto L_0x012e
        L_0x0240:
            r0 = r3
            r29 = r4
            r1 = 1
            int r2 = r0.f5374b
            int r2 = r2 - r1
        L_0x0247:
            if (r2 < 0) goto L_0x0274
            java.lang.Object r1 = r0.get(r2)
            com.badlogic.gdx.utils.m r1 = (com.badlogic.gdx.utils.m) r1
            int r3 = r1.f5528b
            if (r3 != 0) goto L_0x026b
            r0.d(r2)
            r3 = r34
            com.badlogic.gdx.utils.f0<com.badlogic.gdx.utils.m> r4 = r3.polygonPool
            r4.free(r1)
            r1 = r29
            java.lang.Object r4 = r1.d(r2)
            com.badlogic.gdx.utils.n0 r4 = (com.badlogic.gdx.utils.n0) r4
            com.badlogic.gdx.utils.f0<com.badlogic.gdx.utils.n0> r5 = r3.polygonIndicesPool
            r5.free(r4)
            goto L_0x026f
        L_0x026b:
            r3 = r34
            r1 = r29
        L_0x026f:
            int r2 = r2 + -1
            r29 = r1
            goto L_0x0247
        L_0x0274:
            r3 = r34
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.utils.Triangulator.decompose(com.badlogic.gdx.utils.m, com.badlogic.gdx.utils.n0):com.badlogic.gdx.utils.a");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bf  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.badlogic.gdx.utils.n0 triangulate(com.badlogic.gdx.utils.m r29) {
        /*
            r28 = this;
            r0 = r28
            r1 = r29
            float[] r2 = r1.f5527a
            int r1 = r1.f5528b
            r3 = 1
            int r1 = r1 >> r3
            com.badlogic.gdx.utils.n0 r4 = r0.indicesArray
            r4.a()
            short[] r5 = r4.f(r1)
            r6 = 0
            r7 = 0
        L_0x0015:
            if (r7 >= r1) goto L_0x001d
            r5[r7] = r7
            int r7 = r7 + 1
            short r7 = (short) r7
            goto L_0x0015
        L_0x001d:
            com.badlogic.gdx.utils.f r7 = r0.isConcaveArray
            boolean[] r8 = r7.c(r1)
            r9 = 0
        L_0x0024:
            if (r9 >= r1) goto L_0x002f
            boolean r10 = isConcave(r9, r1, r2, r5)
            r8[r9] = r10
            int r9 = r9 + 1
            goto L_0x0024
        L_0x002f:
            com.badlogic.gdx.utils.n0 r9 = r0.triangles
            r9.a()
            int r10 = r1 + -2
            int r10 = java.lang.Math.max(r6, r10)
            r11 = 2
            int r10 = r10 << r11
            r9.b(r10)
        L_0x003f:
            r10 = 3
            if (r1 <= r10) goto L_0x0101
            int r10 = r1 + -1
            r12 = r10
            r10 = 0
            r13 = 1
        L_0x0047:
            boolean r14 = r8[r10]
            if (r14 != 0) goto L_0x00b7
            short r14 = r5[r12]
            int r14 = r14 << r3
            short r15 = r5[r10]
            int r15 = r15 << r3
            short r16 = r5[r13]
            int r16 = r16 << 1
            r23 = r2[r14]
            int r14 = r14 + r3
            r14 = r2[r14]
            r24 = r2[r15]
            int r15 = r15 + r3
            r15 = r2[r15]
            r25 = r2[r16]
            int r16 = r16 + 1
            r16 = r2[r16]
            int r17 = r13 + 1
            int r17 = r17 % r1
            r6 = r17
        L_0x006b:
            if (r6 == r12) goto L_0x00bd
            boolean r17 = r8[r6]
            if (r17 != 0) goto L_0x0072
            goto L_0x00b3
        L_0x0072:
            short r17 = r5[r6]
            int r17 = r17 << 1
            r26 = r2[r17]
            int r17 = r17 + 1
            r27 = r2[r17]
            r17 = r25
            r18 = r16
            r19 = r23
            r20 = r14
            r21 = r26
            r22 = r27
            boolean r17 = positiveArea(r17, r18, r19, r20, r21, r22)
            if (r17 == 0) goto L_0x00b3
            r17 = r23
            r18 = r14
            r19 = r24
            r20 = r15
            r21 = r26
            r22 = r27
            boolean r17 = positiveArea(r17, r18, r19, r20, r21, r22)
            if (r17 == 0) goto L_0x00b3
            r17 = r24
            r18 = r15
            r19 = r25
            r20 = r16
            r21 = r26
            r22 = r27
            boolean r17 = positiveArea(r17, r18, r19, r20, r21, r22)
            if (r17 == 0) goto L_0x00b3
            goto L_0x00b7
        L_0x00b3:
            int r6 = r6 + 1
            int r6 = r6 % r1
            goto L_0x006b
        L_0x00b7:
            if (r13 != 0) goto L_0x00f8
        L_0x00b9:
            boolean r6 = r8[r10]
            if (r6 != 0) goto L_0x00bf
        L_0x00bd:
            r6 = r10
            goto L_0x00c4
        L_0x00bf:
            int r10 = r10 + -1
            if (r10 > 0) goto L_0x00b9
            goto L_0x00bd
        L_0x00c4:
            int r10 = r1 + r6
            int r10 = r10 - r3
            int r10 = r10 % r1
            short r10 = r5[r10]
            r9.a(r10)
            short r10 = r5[r6]
            r9.a(r10)
            int r10 = r6 + 1
            int r10 = r10 % r1
            short r10 = r5[r10]
            r9.a(r10)
            r4.d(r6)
            r7.a(r6)
            int r1 = r1 + -1
            int r10 = r1 + r6
            int r10 = r10 - r3
            int r10 = r10 % r1
            if (r6 != r1) goto L_0x00e9
            r6 = 0
        L_0x00e9:
            boolean r12 = isConcave(r10, r1, r2, r5)
            r8[r10] = r12
            boolean r10 = isConcave(r6, r1, r2, r5)
            r8[r6] = r10
            r6 = 0
            goto L_0x003f
        L_0x00f8:
            int r6 = r13 + 1
            int r6 = r6 % r1
            r12 = r10
            r10 = r13
            r13 = r6
            r6 = 0
            goto L_0x0047
        L_0x0101:
            if (r1 != r10) goto L_0x0113
            short r1 = r5[r11]
            r9.a(r1)
            r1 = 0
            short r1 = r5[r1]
            r9.a(r1)
            short r1 = r5[r3]
            r9.a(r1)
        L_0x0113:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.utils.Triangulator.triangulate(com.badlogic.gdx.utils.m):com.badlogic.gdx.utils.n0");
    }
}
