package com.esotericsoftware.spine.utils;

import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonRenderer;
import e.d.b.w.a.b;

public class SkeletonActor extends b {
    private SkeletonRenderer renderer;
    private boolean resetBlendFunction = true;
    private Skeleton skeleton;
    AnimationState state;

    public SkeletonActor() {
    }

    public void act(float f2) {
        this.state.update(f2);
        this.state.apply(this.skeleton);
        super.act(f2);
    }

    public void draw(com.badlogic.gdx.graphics.g2d.b bVar, float f2) {
        int blendSrcFunc = bVar.getBlendSrcFunc();
        int blendDstFunc = bVar.getBlendDstFunc();
        int blendSrcFuncAlpha = bVar.getBlendSrcFuncAlpha();
        int blendDstFuncAlpha = bVar.getBlendDstFuncAlpha();
        e.d.b.t.b color = this.skeleton.getColor();
        float f3 = color.f6937d;
        this.skeleton.getColor().f6937d *= f2;
        this.skeleton.setPosition(getX(), getY());
        this.skeleton.updateWorldTransform();
        this.renderer.draw(bVar, this.skeleton);
        if (this.resetBlendFunction) {
            bVar.setBlendFunctionSeparate(blendSrcFunc, blendDstFunc, blendSrcFuncAlpha, blendDstFuncAlpha);
        }
        color.f6937d = f3;
    }

    public AnimationState getAnimationState() {
        return this.state;
    }

    public SkeletonRenderer getRenderer() {
        return this.renderer;
    }

    public boolean getResetBlendFunction() {
        return this.resetBlendFunction;
    }

    public Skeleton getSkeleton() {
        return this.skeleton;
    }

    public void setAnimationState(AnimationState animationState) {
        this.state = animationState;
    }

    public void setRenderer(SkeletonRenderer skeletonRenderer) {
        this.renderer = skeletonRenderer;
    }

    public void setResetBlendFunction(boolean z) {
        this.resetBlendFunction = z;
    }

    public void setSkeleton(Skeleton skeleton2) {
        this.skeleton = skeleton2;
    }

    public SkeletonActor(SkeletonRenderer skeletonRenderer, Skeleton skeleton2, AnimationState animationState) {
        this.renderer = skeletonRenderer;
        this.skeleton = skeleton2;
        this.state = animationState;
    }
}
