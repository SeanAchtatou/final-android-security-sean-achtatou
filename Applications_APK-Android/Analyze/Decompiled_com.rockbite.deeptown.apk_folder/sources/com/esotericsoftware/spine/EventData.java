package com.esotericsoftware.spine;

public class EventData {
    String audioPath;
    float balance;
    float floatValue;
    int intValue;
    final String name;
    String stringValue;
    float volume;

    public EventData(String str) {
        if (str != null) {
            this.name = str;
            return;
        }
        throw new IllegalArgumentException("name cannot be null.");
    }

    public String getAudioPath() {
        return this.audioPath;
    }

    public float getBalance() {
        return this.balance;
    }

    public float getFloat() {
        return this.floatValue;
    }

    public int getInt() {
        return this.intValue;
    }

    public String getName() {
        return this.name;
    }

    public String getString() {
        return this.stringValue;
    }

    public float getVolume() {
        return this.volume;
    }

    public void setAudioPath(String str) {
        this.audioPath = str;
    }

    public void setBalance(float f2) {
        this.balance = f2;
    }

    public void setFloat(float f2) {
        this.floatValue = f2;
    }

    public void setInt(int i2) {
        this.intValue = i2;
    }

    public void setString(String str) {
        this.stringValue = str;
    }

    public void setVolume(float f2) {
        this.volume = f2;
    }

    public String toString() {
        return this.name;
    }
}
