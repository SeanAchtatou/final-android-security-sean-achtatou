package com.esotericsoftware.spine;

import com.badlogic.gdx.utils.a0;

public class AnimationStateData {
    final a0<Key> animationToMixTime = new a0<>();
    float defaultMix;
    final SkeletonData skeletonData;
    final Key tempKey = new Key();

    static class Key {
        Animation a1;
        Animation a2;

        Key() {
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            Key key = (Key) obj;
            Animation animation = this.a1;
            if (animation == null) {
                if (key.a1 != null) {
                    return false;
                }
            } else if (!animation.equals(key.a1)) {
                return false;
            }
            Animation animation2 = this.a2;
            if (animation2 == null) {
                if (key.a2 != null) {
                    return false;
                }
            } else if (!animation2.equals(key.a2)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return ((this.a1.hashCode() + 31) * 31) + this.a2.hashCode();
        }

        public String toString() {
            return this.a1.name + "->" + this.a2.name;
        }
    }

    public AnimationStateData(SkeletonData skeletonData2) {
        if (skeletonData2 != null) {
            this.skeletonData = skeletonData2;
            return;
        }
        throw new IllegalArgumentException("skeletonData cannot be null.");
    }

    public float getDefaultMix() {
        return this.defaultMix;
    }

    public float getMix(Animation animation, Animation animation2) {
        if (animation == null) {
            throw new IllegalArgumentException("from cannot be null.");
        } else if (animation2 != null) {
            Key key = this.tempKey;
            key.a1 = animation;
            key.a2 = animation2;
            return this.animationToMixTime.a(key, this.defaultMix);
        } else {
            throw new IllegalArgumentException("to cannot be null.");
        }
    }

    public SkeletonData getSkeletonData() {
        return this.skeletonData;
    }

    public void setDefaultMix(float f2) {
        this.defaultMix = f2;
    }

    public void setMix(String str, String str2, float f2) {
        Animation findAnimation = this.skeletonData.findAnimation(str);
        if (findAnimation != null) {
            Animation findAnimation2 = this.skeletonData.findAnimation(str2);
            if (findAnimation2 != null) {
                setMix(findAnimation, findAnimation2, f2);
                return;
            }
            throw new IllegalArgumentException("Animation not found: " + str2);
        }
        throw new IllegalArgumentException("Animation not found: " + str);
    }

    public void setMix(Animation animation, Animation animation2, float f2) {
        if (animation == null) {
            throw new IllegalArgumentException("from cannot be null.");
        } else if (animation2 != null) {
            Key key = new Key();
            key.a1 = animation;
            key.a2 = animation2;
            this.animationToMixTime.b(key, f2);
        } else {
            throw new IllegalArgumentException("to cannot be null.");
        }
    }
}
