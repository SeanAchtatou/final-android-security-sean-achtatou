package com.esotericsoftware.spine;

import com.badlogic.gdx.graphics.g2d.q;
import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.j;
import com.badlogic.gdx.utils.l0;
import com.badlogic.gdx.utils.m;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.BoneData;
import com.esotericsoftware.spine.PathConstraintData;
import com.esotericsoftware.spine.SkeletonJson;
import com.esotericsoftware.spine.attachments.AtlasAttachmentLoader;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.AttachmentLoader;
import com.esotericsoftware.spine.attachments.AttachmentType;
import com.esotericsoftware.spine.attachments.BoundingBoxAttachment;
import com.esotericsoftware.spine.attachments.ClippingAttachment;
import com.esotericsoftware.spine.attachments.MeshAttachment;
import com.esotericsoftware.spine.attachments.PathAttachment;
import com.esotericsoftware.spine.attachments.PointAttachment;
import com.esotericsoftware.spine.attachments.RegionAttachment;
import com.google.android.gms.ads.AdRequest;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import e.d.b.t.b;
import java.io.EOFException;
import java.io.IOException;

public class SkeletonBinary {
    public static final int BONE_ROTATE = 0;
    public static final int BONE_SCALE = 2;
    public static final int BONE_SHEAR = 3;
    public static final int BONE_TRANSLATE = 1;
    public static final int CURVE_BEZIER = 2;
    public static final int CURVE_LINEAR = 0;
    public static final int CURVE_STEPPED = 1;
    public static final int PATH_MIX = 2;
    public static final int PATH_POSITION = 0;
    public static final int PATH_SPACING = 1;
    public static final int SLOT_ATTACHMENT = 0;
    public static final int SLOT_COLOR = 1;
    public static final int SLOT_TWO_COLOR = 2;
    private static final b tempColor1 = new b();
    private static final b tempColor2 = new b();
    private final AttachmentLoader attachmentLoader;
    private a<SkeletonJson.LinkedMesh> linkedMeshes = new a<>();
    private float scale = 1.0f;

    /* renamed from: com.esotericsoftware.spine.SkeletonBinary$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType = new int[AttachmentType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(14:0|1|2|3|4|5|6|7|8|9|10|11|12|(3:13|14|16)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|16) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.esotericsoftware.spine.attachments.AttachmentType[] r0 = com.esotericsoftware.spine.attachments.AttachmentType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.esotericsoftware.spine.SkeletonBinary.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType = r0
                int[] r0 = com.esotericsoftware.spine.SkeletonBinary.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.region     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.esotericsoftware.spine.SkeletonBinary.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.boundingbox     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.esotericsoftware.spine.SkeletonBinary.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x002a }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.mesh     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.esotericsoftware.spine.SkeletonBinary.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.linkedmesh     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.esotericsoftware.spine.SkeletonBinary.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.path     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = com.esotericsoftware.spine.SkeletonBinary.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x004b }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.point     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = com.esotericsoftware.spine.SkeletonBinary.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x0056 }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.clipping     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.SkeletonBinary.AnonymousClass2.<clinit>():void");
        }
    }

    static class Vertices {
        int[] bones;
        float[] vertices;

        Vertices() {
        }
    }

    public SkeletonBinary(q qVar) {
        this.attachmentLoader = new AtlasAttachmentLoader(qVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0351, code lost:
        if (r8.positionMode == com.esotericsoftware.spine.PathConstraintData.PositionMode.fixed) goto L_0x0353;
     */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x035d A[Catch:{ IOException -> 0x05bf }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01b6 A[Catch:{ IOException -> 0x05bf }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void readAnimation(com.badlogic.gdx.utils.j r32, java.lang.String r33, com.esotericsoftware.spine.SkeletonData r34) {
        /*
            r31 = this;
            r1 = r31
            r0 = r32
            r2 = r34
            com.badlogic.gdx.utils.a r3 = new com.badlogic.gdx.utils.a
            r3.<init>()
            float r4 = r1.scale
            r5 = 1
            int r6 = r0.readInt(r5)     // Catch:{ IOException -> 0x05bf }
            r8 = 0
            r8 = 0
            r9 = 0
        L_0x0015:
            r10 = 2
            if (r8 >= r6) goto L_0x016d
            int r11 = r0.readInt(r5)     // Catch:{ IOException -> 0x05bf }
            int r12 = r0.readInt(r5)     // Catch:{ IOException -> 0x05bf }
            r13 = r9
            r9 = 0
        L_0x0022:
            if (r9 >= r12) goto L_0x015f
            byte r14 = r32.readByte()     // Catch:{ IOException -> 0x05bf }
            int r15 = r0.readInt(r5)     // Catch:{ IOException -> 0x05bf }
            if (r14 == 0) goto L_0x0120
            if (r14 == r5) goto L_0x00c6
            if (r14 == r10) goto L_0x003c
            r29 = r4
            r27 = r6
            r30 = r8
            r28 = r12
            goto L_0x014f
        L_0x003c:
            com.esotericsoftware.spine.Animation$TwoColorTimeline r14 = new com.esotericsoftware.spine.Animation$TwoColorTimeline     // Catch:{ IOException -> 0x05bf }
            r14.<init>(r15)     // Catch:{ IOException -> 0x05bf }
            r14.slotIndex = r11     // Catch:{ IOException -> 0x05bf }
            r7 = 0
        L_0x0044:
            if (r7 >= r15) goto L_0x00ab
            float r18 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            e.d.b.t.b r10 = com.esotericsoftware.spine.SkeletonBinary.tempColor1     // Catch:{ IOException -> 0x05bf }
            int r5 = r32.readInt()     // Catch:{ IOException -> 0x05bf }
            e.d.b.t.b.b(r10, r5)     // Catch:{ IOException -> 0x05bf }
            e.d.b.t.b r5 = com.esotericsoftware.spine.SkeletonBinary.tempColor2     // Catch:{ IOException -> 0x05bf }
            int r10 = r32.readInt()     // Catch:{ IOException -> 0x05bf }
            e.d.b.t.b.a(r5, r10)     // Catch:{ IOException -> 0x05bf }
            e.d.b.t.b r5 = com.esotericsoftware.spine.SkeletonBinary.tempColor1     // Catch:{ IOException -> 0x05bf }
            float r5 = r5.f6934a     // Catch:{ IOException -> 0x05bf }
            e.d.b.t.b r10 = com.esotericsoftware.spine.SkeletonBinary.tempColor1     // Catch:{ IOException -> 0x05bf }
            float r10 = r10.f6935b     // Catch:{ IOException -> 0x05bf }
            r27 = r6
            e.d.b.t.b r6 = com.esotericsoftware.spine.SkeletonBinary.tempColor1     // Catch:{ IOException -> 0x05bf }
            float r6 = r6.f6936c     // Catch:{ IOException -> 0x05bf }
            r28 = r12
            e.d.b.t.b r12 = com.esotericsoftware.spine.SkeletonBinary.tempColor1     // Catch:{ IOException -> 0x05bf }
            float r12 = r12.f6937d     // Catch:{ IOException -> 0x05bf }
            r29 = r4
            e.d.b.t.b r4 = com.esotericsoftware.spine.SkeletonBinary.tempColor2     // Catch:{ IOException -> 0x05bf }
            float r4 = r4.f6934a     // Catch:{ IOException -> 0x05bf }
            e.d.b.t.b r2 = com.esotericsoftware.spine.SkeletonBinary.tempColor2     // Catch:{ IOException -> 0x05bf }
            float r2 = r2.f6935b     // Catch:{ IOException -> 0x05bf }
            r30 = r8
            e.d.b.t.b r8 = com.esotericsoftware.spine.SkeletonBinary.tempColor2     // Catch:{ IOException -> 0x05bf }
            float r8 = r8.f6936c     // Catch:{ IOException -> 0x05bf }
            r16 = r14
            r17 = r7
            r19 = r5
            r20 = r10
            r21 = r6
            r22 = r12
            r23 = r4
            r24 = r2
            r25 = r8
            r16.setFrame(r17, r18, r19, r20, r21, r22, r23, r24, r25)     // Catch:{ IOException -> 0x05bf }
            int r2 = r15 + -1
            if (r7 >= r2) goto L_0x009c
            r1.readCurve(r0, r7, r14)     // Catch:{ IOException -> 0x05bf }
        L_0x009c:
            int r7 = r7 + 1
            r2 = r34
            r6 = r27
            r12 = r28
            r4 = r29
            r8 = r30
            r5 = 1
            r10 = 2
            goto L_0x0044
        L_0x00ab:
            r29 = r4
            r27 = r6
            r30 = r8
            r28 = r12
            r3.add(r14)     // Catch:{ IOException -> 0x05bf }
            float[] r2 = r14.getFrames()     // Catch:{ IOException -> 0x05bf }
            int r15 = r15 + -1
            int r15 = r15 * 8
            r2 = r2[r15]     // Catch:{ IOException -> 0x05bf }
            float r13 = java.lang.Math.max(r13, r2)     // Catch:{ IOException -> 0x05bf }
            goto L_0x014f
        L_0x00c6:
            r29 = r4
            r27 = r6
            r30 = r8
            r28 = r12
            com.esotericsoftware.spine.Animation$ColorTimeline r2 = new com.esotericsoftware.spine.Animation$ColorTimeline     // Catch:{ IOException -> 0x05bf }
            r2.<init>(r15)     // Catch:{ IOException -> 0x05bf }
            r2.slotIndex = r11     // Catch:{ IOException -> 0x05bf }
            r4 = 0
        L_0x00d6:
            if (r4 >= r15) goto L_0x010e
            float r18 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            e.d.b.t.b r5 = com.esotericsoftware.spine.SkeletonBinary.tempColor1     // Catch:{ IOException -> 0x05bf }
            int r6 = r32.readInt()     // Catch:{ IOException -> 0x05bf }
            e.d.b.t.b.b(r5, r6)     // Catch:{ IOException -> 0x05bf }
            e.d.b.t.b r5 = com.esotericsoftware.spine.SkeletonBinary.tempColor1     // Catch:{ IOException -> 0x05bf }
            float r5 = r5.f6934a     // Catch:{ IOException -> 0x05bf }
            e.d.b.t.b r6 = com.esotericsoftware.spine.SkeletonBinary.tempColor1     // Catch:{ IOException -> 0x05bf }
            float r6 = r6.f6935b     // Catch:{ IOException -> 0x05bf }
            e.d.b.t.b r7 = com.esotericsoftware.spine.SkeletonBinary.tempColor1     // Catch:{ IOException -> 0x05bf }
            float r7 = r7.f6936c     // Catch:{ IOException -> 0x05bf }
            e.d.b.t.b r8 = com.esotericsoftware.spine.SkeletonBinary.tempColor1     // Catch:{ IOException -> 0x05bf }
            float r8 = r8.f6937d     // Catch:{ IOException -> 0x05bf }
            r16 = r2
            r17 = r4
            r19 = r5
            r20 = r6
            r21 = r7
            r22 = r8
            r16.setFrame(r17, r18, r19, r20, r21, r22)     // Catch:{ IOException -> 0x05bf }
            int r5 = r15 + -1
            if (r4 >= r5) goto L_0x010b
            r1.readCurve(r0, r4, r2)     // Catch:{ IOException -> 0x05bf }
        L_0x010b:
            int r4 = r4 + 1
            goto L_0x00d6
        L_0x010e:
            r3.add(r2)     // Catch:{ IOException -> 0x05bf }
            float[] r2 = r2.getFrames()     // Catch:{ IOException -> 0x05bf }
            int r15 = r15 + -1
            int r15 = r15 * 5
            r2 = r2[r15]     // Catch:{ IOException -> 0x05bf }
            float r13 = java.lang.Math.max(r13, r2)     // Catch:{ IOException -> 0x05bf }
            goto L_0x014f
        L_0x0120:
            r29 = r4
            r27 = r6
            r30 = r8
            r28 = r12
            com.esotericsoftware.spine.Animation$AttachmentTimeline r2 = new com.esotericsoftware.spine.Animation$AttachmentTimeline     // Catch:{ IOException -> 0x05bf }
            r2.<init>(r15)     // Catch:{ IOException -> 0x05bf }
            r2.slotIndex = r11     // Catch:{ IOException -> 0x05bf }
            r4 = 0
        L_0x0130:
            if (r4 >= r15) goto L_0x0140
            float r5 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            java.lang.String r6 = r32.readString()     // Catch:{ IOException -> 0x05bf }
            r2.setFrame(r4, r5, r6)     // Catch:{ IOException -> 0x05bf }
            int r4 = r4 + 1
            goto L_0x0130
        L_0x0140:
            r3.add(r2)     // Catch:{ IOException -> 0x05bf }
            float[] r2 = r2.getFrames()     // Catch:{ IOException -> 0x05bf }
            int r15 = r15 + -1
            r2 = r2[r15]     // Catch:{ IOException -> 0x05bf }
            float r13 = java.lang.Math.max(r13, r2)     // Catch:{ IOException -> 0x05bf }
        L_0x014f:
            int r9 = r9 + 1
            r2 = r34
            r6 = r27
            r12 = r28
            r4 = r29
            r8 = r30
            r5 = 1
            r10 = 2
            goto L_0x0022
        L_0x015f:
            r29 = r4
            r27 = r6
            r30 = r8
            int r8 = r30 + 1
            r2 = r34
            r9 = r13
            r5 = 1
            goto L_0x0015
        L_0x016d:
            r29 = r4
            r2 = 1
            int r4 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            r5 = 0
        L_0x0175:
            r7 = 3
            if (r5 >= r4) goto L_0x0224
            int r8 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            int r10 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            r11 = r9
            r9 = 0
        L_0x0182:
            if (r9 >= r10) goto L_0x021e
            byte r12 = r32.readByte()     // Catch:{ IOException -> 0x05bf }
            int r13 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            if (r12 == 0) goto L_0x01e7
            if (r12 == r2) goto L_0x0197
            r2 = 2
            if (r12 == r2) goto L_0x0198
            if (r12 == r7) goto L_0x0198
            goto L_0x0218
        L_0x0197:
            r2 = 2
        L_0x0198:
            if (r12 != r2) goto L_0x01a2
            com.esotericsoftware.spine.Animation$ScaleTimeline r2 = new com.esotericsoftware.spine.Animation$ScaleTimeline     // Catch:{ IOException -> 0x05bf }
            r2.<init>(r13)     // Catch:{ IOException -> 0x05bf }
        L_0x019f:
            r12 = 1065353216(0x3f800000, float:1.0)
            goto L_0x01b1
        L_0x01a2:
            if (r12 != r7) goto L_0x01aa
            com.esotericsoftware.spine.Animation$ShearTimeline r2 = new com.esotericsoftware.spine.Animation$ShearTimeline     // Catch:{ IOException -> 0x05bf }
            r2.<init>(r13)     // Catch:{ IOException -> 0x05bf }
            goto L_0x019f
        L_0x01aa:
            com.esotericsoftware.spine.Animation$TranslateTimeline r2 = new com.esotericsoftware.spine.Animation$TranslateTimeline     // Catch:{ IOException -> 0x05bf }
            r2.<init>(r13)     // Catch:{ IOException -> 0x05bf }
            r12 = r29
        L_0x01b1:
            r2.boneIndex = r8     // Catch:{ IOException -> 0x05bf }
            r14 = 0
        L_0x01b4:
            if (r14 >= r13) goto L_0x01d4
            float r15 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            float r16 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            float r6 = r16 * r12
            float r16 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            float r7 = r16 * r12
            r2.setFrame(r14, r15, r6, r7)     // Catch:{ IOException -> 0x05bf }
            int r6 = r13 + -1
            if (r14 >= r6) goto L_0x01d0
            r1.readCurve(r0, r14, r2)     // Catch:{ IOException -> 0x05bf }
        L_0x01d0:
            int r14 = r14 + 1
            r7 = 3
            goto L_0x01b4
        L_0x01d4:
            r3.add(r2)     // Catch:{ IOException -> 0x05bf }
            float[] r2 = r2.getFrames()     // Catch:{ IOException -> 0x05bf }
            int r13 = r13 + -1
            r6 = 3
            int r13 = r13 * 3
            r2 = r2[r13]     // Catch:{ IOException -> 0x05bf }
            float r11 = java.lang.Math.max(r11, r2)     // Catch:{ IOException -> 0x05bf }
            goto L_0x0218
        L_0x01e7:
            com.esotericsoftware.spine.Animation$RotateTimeline r2 = new com.esotericsoftware.spine.Animation$RotateTimeline     // Catch:{ IOException -> 0x05bf }
            r2.<init>(r13)     // Catch:{ IOException -> 0x05bf }
            r2.boneIndex = r8     // Catch:{ IOException -> 0x05bf }
            r6 = 0
        L_0x01ef:
            if (r6 >= r13) goto L_0x0206
            float r7 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            float r12 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            r2.setFrame(r6, r7, r12)     // Catch:{ IOException -> 0x05bf }
            int r7 = r13 + -1
            if (r6 >= r7) goto L_0x0203
            r1.readCurve(r0, r6, r2)     // Catch:{ IOException -> 0x05bf }
        L_0x0203:
            int r6 = r6 + 1
            goto L_0x01ef
        L_0x0206:
            r3.add(r2)     // Catch:{ IOException -> 0x05bf }
            float[] r2 = r2.getFrames()     // Catch:{ IOException -> 0x05bf }
            int r13 = r13 + -1
            r6 = 2
            int r13 = r13 * 2
            r2 = r2[r13]     // Catch:{ IOException -> 0x05bf }
            float r11 = java.lang.Math.max(r11, r2)     // Catch:{ IOException -> 0x05bf }
        L_0x0218:
            int r9 = r9 + 1
            r2 = 1
            r7 = 3
            goto L_0x0182
        L_0x021e:
            int r5 = r5 + 1
            r9 = r11
            r2 = 1
            goto L_0x0175
        L_0x0224:
            int r4 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            r5 = 0
        L_0x0229:
            if (r5 >= r4) goto L_0x0275
            int r6 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            int r7 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.Animation$IkConstraintTimeline r2 = new com.esotericsoftware.spine.Animation$IkConstraintTimeline     // Catch:{ IOException -> 0x05bf }
            r2.<init>(r7)     // Catch:{ IOException -> 0x05bf }
            r2.ikConstraintIndex = r6     // Catch:{ IOException -> 0x05bf }
            r6 = 0
        L_0x023b:
            if (r6 >= r7) goto L_0x0260
            float r12 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            float r13 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            byte r14 = r32.readByte()     // Catch:{ IOException -> 0x05bf }
            boolean r15 = r32.readBoolean()     // Catch:{ IOException -> 0x05bf }
            boolean r16 = r32.readBoolean()     // Catch:{ IOException -> 0x05bf }
            r10 = r2
            r11 = r6
            r10.setFrame(r11, r12, r13, r14, r15, r16)     // Catch:{ IOException -> 0x05bf }
            int r8 = r7 + -1
            if (r6 >= r8) goto L_0x025d
            r1.readCurve(r0, r6, r2)     // Catch:{ IOException -> 0x05bf }
        L_0x025d:
            int r6 = r6 + 1
            goto L_0x023b
        L_0x0260:
            r3.add(r2)     // Catch:{ IOException -> 0x05bf }
            float[] r2 = r2.getFrames()     // Catch:{ IOException -> 0x05bf }
            int r7 = r7 + -1
            int r7 = r7 * 5
            r2 = r2[r7]     // Catch:{ IOException -> 0x05bf }
            float r9 = java.lang.Math.max(r9, r2)     // Catch:{ IOException -> 0x05bf }
            int r5 = r5 + 1
            r2 = 1
            goto L_0x0229
        L_0x0275:
            int r4 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            r5 = 0
        L_0x027a:
            if (r5 >= r4) goto L_0x02c6
            int r6 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            int r7 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.Animation$TransformConstraintTimeline r2 = new com.esotericsoftware.spine.Animation$TransformConstraintTimeline     // Catch:{ IOException -> 0x05bf }
            r2.<init>(r7)     // Catch:{ IOException -> 0x05bf }
            r2.transformConstraintIndex = r6     // Catch:{ IOException -> 0x05bf }
            r6 = 0
        L_0x028c:
            if (r6 >= r7) goto L_0x02b1
            float r12 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            float r13 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            float r14 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            float r15 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            float r16 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            r10 = r2
            r11 = r6
            r10.setFrame(r11, r12, r13, r14, r15, r16)     // Catch:{ IOException -> 0x05bf }
            int r8 = r7 + -1
            if (r6 >= r8) goto L_0x02ae
            r1.readCurve(r0, r6, r2)     // Catch:{ IOException -> 0x05bf }
        L_0x02ae:
            int r6 = r6 + 1
            goto L_0x028c
        L_0x02b1:
            r3.add(r2)     // Catch:{ IOException -> 0x05bf }
            float[] r2 = r2.getFrames()     // Catch:{ IOException -> 0x05bf }
            int r7 = r7 + -1
            int r7 = r7 * 5
            r2 = r2[r7]     // Catch:{ IOException -> 0x05bf }
            float r9 = java.lang.Math.max(r9, r2)     // Catch:{ IOException -> 0x05bf }
            int r5 = r5 + 1
            r2 = 1
            goto L_0x027a
        L_0x02c6:
            int r4 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            r5 = 0
        L_0x02cb:
            if (r5 >= r4) goto L_0x0395
            int r6 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            r7 = r34
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.PathConstraintData> r8 = r7.pathConstraints     // Catch:{ IOException -> 0x05bf }
            java.lang.Object r8 = r8.get(r6)     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.PathConstraintData r8 = (com.esotericsoftware.spine.PathConstraintData) r8     // Catch:{ IOException -> 0x05bf }
            int r10 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            r11 = r9
            r9 = 0
        L_0x02e1:
            if (r9 >= r10) goto L_0x038d
            byte r12 = r32.readByte()     // Catch:{ IOException -> 0x05bf }
            int r13 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            if (r12 == 0) goto L_0x0332
            if (r12 == r2) goto L_0x0332
            r2 = 2
            if (r12 == r2) goto L_0x02f6
            r16 = r4
            goto L_0x0386
        L_0x02f6:
            com.esotericsoftware.spine.Animation$PathConstraintMixTimeline r2 = new com.esotericsoftware.spine.Animation$PathConstraintMixTimeline     // Catch:{ IOException -> 0x05bf }
            r2.<init>(r13)     // Catch:{ IOException -> 0x05bf }
            r2.pathConstraintIndex = r6     // Catch:{ IOException -> 0x05bf }
            r12 = 0
        L_0x02fe:
            if (r12 >= r13) goto L_0x031d
            float r14 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            float r15 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            r16 = r4
            float r4 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            r2.setFrame(r12, r14, r15, r4)     // Catch:{ IOException -> 0x05bf }
            int r4 = r13 + -1
            if (r12 >= r4) goto L_0x0318
            r1.readCurve(r0, r12, r2)     // Catch:{ IOException -> 0x05bf }
        L_0x0318:
            int r12 = r12 + 1
            r4 = r16
            goto L_0x02fe
        L_0x031d:
            r16 = r4
            r3.add(r2)     // Catch:{ IOException -> 0x05bf }
            float[] r2 = r2.getFrames()     // Catch:{ IOException -> 0x05bf }
            int r13 = r13 + -1
            r4 = 3
            int r13 = r13 * 3
            r2 = r2[r13]     // Catch:{ IOException -> 0x05bf }
            float r11 = java.lang.Math.max(r11, r2)     // Catch:{ IOException -> 0x05bf }
            goto L_0x0386
        L_0x0332:
            r16 = r4
            if (r12 != r2) goto L_0x0348
            com.esotericsoftware.spine.Animation$PathConstraintSpacingTimeline r2 = new com.esotericsoftware.spine.Animation$PathConstraintSpacingTimeline     // Catch:{ IOException -> 0x05bf }
            r2.<init>(r13)     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r4 = r8.spacingMode     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r12 = com.esotericsoftware.spine.PathConstraintData.SpacingMode.length     // Catch:{ IOException -> 0x05bf }
            if (r4 == r12) goto L_0x0353
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r4 = r8.spacingMode     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r12 = com.esotericsoftware.spine.PathConstraintData.SpacingMode.fixed     // Catch:{ IOException -> 0x05bf }
            if (r4 != r12) goto L_0x0356
            goto L_0x0353
        L_0x0348:
            com.esotericsoftware.spine.Animation$PathConstraintPositionTimeline r2 = new com.esotericsoftware.spine.Animation$PathConstraintPositionTimeline     // Catch:{ IOException -> 0x05bf }
            r2.<init>(r13)     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.PathConstraintData$PositionMode r4 = r8.positionMode     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.PathConstraintData$PositionMode r12 = com.esotericsoftware.spine.PathConstraintData.PositionMode.fixed     // Catch:{ IOException -> 0x05bf }
            if (r4 != r12) goto L_0x0356
        L_0x0353:
            r4 = r29
            goto L_0x0358
        L_0x0356:
            r4 = 1065353216(0x3f800000, float:1.0)
        L_0x0358:
            r2.pathConstraintIndex = r6     // Catch:{ IOException -> 0x05bf }
            r12 = 0
        L_0x035b:
            if (r12 >= r13) goto L_0x0374
            float r14 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            float r15 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            float r15 = r15 * r4
            r2.setFrame(r12, r14, r15)     // Catch:{ IOException -> 0x05bf }
            int r14 = r13 + -1
            if (r12 >= r14) goto L_0x0371
            r1.readCurve(r0, r12, r2)     // Catch:{ IOException -> 0x05bf }
        L_0x0371:
            int r12 = r12 + 1
            goto L_0x035b
        L_0x0374:
            r3.add(r2)     // Catch:{ IOException -> 0x05bf }
            float[] r2 = r2.getFrames()     // Catch:{ IOException -> 0x05bf }
            int r13 = r13 + -1
            r4 = 2
            int r13 = r13 * 2
            r2 = r2[r13]     // Catch:{ IOException -> 0x05bf }
            float r11 = java.lang.Math.max(r11, r2)     // Catch:{ IOException -> 0x05bf }
        L_0x0386:
            int r9 = r9 + 1
            r4 = r16
            r2 = 1
            goto L_0x02e1
        L_0x038d:
            r16 = r4
            int r5 = r5 + 1
            r9 = r11
            r2 = 1
            goto L_0x02cb
        L_0x0395:
            r7 = r34
            int r4 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            r5 = 0
        L_0x039c:
            if (r5 >= r4) goto L_0x04b1
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Skin> r6 = r7.skins     // Catch:{ IOException -> 0x05bf }
            int r8 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            java.lang.Object r6 = r6.get(r8)     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.Skin r6 = (com.esotericsoftware.spine.Skin) r6     // Catch:{ IOException -> 0x05bf }
            int r8 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            r10 = r9
            r9 = 0
        L_0x03b0:
            if (r9 >= r8) goto L_0x04a1
            int r11 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            int r12 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            r2 = 0
        L_0x03bb:
            if (r2 >= r12) goto L_0x048e
            java.lang.String r13 = r32.readString()     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.attachments.Attachment r13 = r6.getAttachment(r11, r13)     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.attachments.VertexAttachment r13 = (com.esotericsoftware.spine.attachments.VertexAttachment) r13     // Catch:{ IOException -> 0x05bf }
            int[] r14 = r13.getBones()     // Catch:{ IOException -> 0x05bf }
            if (r14 == 0) goto L_0x03cf
            r14 = 1
            goto L_0x03d0
        L_0x03cf:
            r14 = 0
        L_0x03d0:
            float[] r15 = r13.getVertices()     // Catch:{ IOException -> 0x05bf }
            r16 = r4
            if (r14 == 0) goto L_0x03e2
            int r4 = r15.length     // Catch:{ IOException -> 0x05bf }
            r18 = 3
            int r4 = r4 / 3
            r19 = 2
            int r4 = r4 * 2
            goto L_0x03e7
        L_0x03e2:
            r18 = 3
            r19 = 2
            int r4 = r15.length     // Catch:{ IOException -> 0x05bf }
        L_0x03e7:
            r20 = r6
            r21 = r8
            r6 = 1
            int r8 = r0.readInt(r6)     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.Animation$DeformTimeline r6 = new com.esotericsoftware.spine.Animation$DeformTimeline     // Catch:{ IOException -> 0x05bf }
            r6.<init>(r8)     // Catch:{ IOException -> 0x05bf }
            r6.slotIndex = r11     // Catch:{ IOException -> 0x05bf }
            r6.attachment = r13     // Catch:{ IOException -> 0x05bf }
            r13 = 0
        L_0x03fa:
            if (r13 >= r8) goto L_0x0469
            r22 = r11
            float r11 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            r23 = r12
            r12 = 1
            int r24 = r0.readInt(r12)     // Catch:{ IOException -> 0x05bf }
            if (r24 != 0) goto L_0x0416
            if (r14 == 0) goto L_0x0410
            float[] r12 = new float[r4]     // Catch:{ IOException -> 0x05bf }
            goto L_0x0411
        L_0x0410:
            r12 = r15
        L_0x0411:
            r25 = r4
            r17 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0454
        L_0x0416:
            float[] r12 = new float[r4]     // Catch:{ IOException -> 0x05bf }
            r25 = r4
            r4 = 1
            int r26 = r0.readInt(r4)     // Catch:{ IOException -> 0x05bf }
            int r4 = r24 + r26
            r17 = 1065353216(0x3f800000, float:1.0)
            int r24 = (r29 > r17 ? 1 : (r29 == r17 ? 0 : -1))
            if (r24 != 0) goto L_0x0434
            r7 = r26
        L_0x0429:
            if (r7 >= r4) goto L_0x0443
            float r24 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            r12[r7] = r24     // Catch:{ IOException -> 0x05bf }
            int r7 = r7 + 1
            goto L_0x0429
        L_0x0434:
            r7 = r26
        L_0x0436:
            if (r7 >= r4) goto L_0x0443
            float r24 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            float r24 = r24 * r29
            r12[r7] = r24     // Catch:{ IOException -> 0x05bf }
            int r7 = r7 + 1
            goto L_0x0436
        L_0x0443:
            if (r14 != 0) goto L_0x0454
            int r4 = r12.length     // Catch:{ IOException -> 0x05bf }
            r7 = 0
        L_0x0447:
            if (r7 >= r4) goto L_0x0454
            r24 = r12[r7]     // Catch:{ IOException -> 0x05bf }
            r26 = r15[r7]     // Catch:{ IOException -> 0x05bf }
            float r24 = r24 + r26
            r12[r7] = r24     // Catch:{ IOException -> 0x05bf }
            int r7 = r7 + 1
            goto L_0x0447
        L_0x0454:
            r6.setFrame(r13, r11, r12)     // Catch:{ IOException -> 0x05bf }
            int r4 = r8 + -1
            if (r13 >= r4) goto L_0x045e
            r1.readCurve(r0, r13, r6)     // Catch:{ IOException -> 0x05bf }
        L_0x045e:
            int r13 = r13 + 1
            r7 = r34
            r11 = r22
            r12 = r23
            r4 = r25
            goto L_0x03fa
        L_0x0469:
            r22 = r11
            r23 = r12
            r17 = 1065353216(0x3f800000, float:1.0)
            r3.add(r6)     // Catch:{ IOException -> 0x05bf }
            float[] r4 = r6.getFrames()     // Catch:{ IOException -> 0x05bf }
            int r8 = r8 + -1
            r4 = r4[r8]     // Catch:{ IOException -> 0x05bf }
            float r10 = java.lang.Math.max(r10, r4)     // Catch:{ IOException -> 0x05bf }
            int r2 = r2 + 1
            r7 = r34
            r4 = r16
            r6 = r20
            r8 = r21
            r11 = r22
            r12 = r23
            goto L_0x03bb
        L_0x048e:
            r16 = r4
            r20 = r6
            r21 = r8
            r17 = 1065353216(0x3f800000, float:1.0)
            r18 = 3
            r19 = 2
            int r9 = r9 + 1
            r7 = r34
            r2 = 1
            goto L_0x03b0
        L_0x04a1:
            r16 = r4
            r17 = 1065353216(0x3f800000, float:1.0)
            r18 = 3
            r19 = 2
            int r5 = r5 + 1
            r7 = r34
            r9 = r10
            r2 = 1
            goto L_0x039c
        L_0x04b1:
            int r4 = r0.readInt(r2)     // Catch:{ IOException -> 0x05bf }
            if (r4 <= 0) goto L_0x0543
            com.esotericsoftware.spine.Animation$DrawOrderTimeline r2 = new com.esotericsoftware.spine.Animation$DrawOrderTimeline     // Catch:{ IOException -> 0x05bf }
            r2.<init>(r4)     // Catch:{ IOException -> 0x05bf }
            r5 = r34
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.SlotData> r6 = r5.slots     // Catch:{ IOException -> 0x05bf }
            int r6 = r6.f5374b     // Catch:{ IOException -> 0x05bf }
            r7 = 0
        L_0x04c3:
            if (r7 >= r4) goto L_0x0533
            float r8 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            r10 = 1
            int r11 = r0.readInt(r10)     // Catch:{ IOException -> 0x05bf }
            int[] r10 = new int[r6]     // Catch:{ IOException -> 0x05bf }
            int r12 = r6 + -1
            r13 = r12
        L_0x04d3:
            r14 = -1
            if (r13 < 0) goto L_0x04db
            r10[r13] = r14     // Catch:{ IOException -> 0x05bf }
            int r13 = r13 + -1
            goto L_0x04d3
        L_0x04db:
            int r13 = r6 - r11
            int[] r13 = new int[r13]     // Catch:{ IOException -> 0x05bf }
            r15 = 0
            r16 = 0
            r17 = 0
        L_0x04e4:
            if (r15 >= r11) goto L_0x050d
            r14 = 1
            int r1 = r0.readInt(r14)     // Catch:{ IOException -> 0x05bf }
            r14 = r16
        L_0x04ed:
            if (r14 == r1) goto L_0x04fa
            int r16 = r17 + 1
            int r19 = r14 + 1
            r13[r17] = r14     // Catch:{ IOException -> 0x05bf }
            r17 = r16
            r14 = r19
            goto L_0x04ed
        L_0x04fa:
            r1 = 1
            int r16 = r0.readInt(r1)     // Catch:{ IOException -> 0x05bf }
            int r16 = r14 + r16
            int r1 = r14 + 1
            r10[r16] = r14     // Catch:{ IOException -> 0x05bf }
            int r15 = r15 + 1
            r14 = -1
            r16 = r1
            r1 = r31
            goto L_0x04e4
        L_0x050d:
            r1 = r16
        L_0x050f:
            if (r1 >= r6) goto L_0x051b
            int r11 = r17 + 1
            int r14 = r1 + 1
            r13[r17] = r1     // Catch:{ IOException -> 0x05bf }
            r17 = r11
            r1 = r14
            goto L_0x050f
        L_0x051b:
            if (r12 < 0) goto L_0x052b
            r1 = r10[r12]     // Catch:{ IOException -> 0x05bf }
            r11 = -1
            if (r1 != r11) goto L_0x0528
            int r17 = r17 + -1
            r1 = r13[r17]     // Catch:{ IOException -> 0x05bf }
            r10[r12] = r1     // Catch:{ IOException -> 0x05bf }
        L_0x0528:
            int r12 = r12 + -1
            goto L_0x051b
        L_0x052b:
            r2.setFrame(r7, r8, r10)     // Catch:{ IOException -> 0x05bf }
            int r7 = r7 + 1
            r1 = r31
            goto L_0x04c3
        L_0x0533:
            r3.add(r2)     // Catch:{ IOException -> 0x05bf }
            float[] r1 = r2.getFrames()     // Catch:{ IOException -> 0x05bf }
            r2 = 1
            int r4 = r4 - r2
            r1 = r1[r4]     // Catch:{ IOException -> 0x05bf }
            float r9 = java.lang.Math.max(r9, r1)     // Catch:{ IOException -> 0x05bf }
            goto L_0x0545
        L_0x0543:
            r5 = r34
        L_0x0545:
            r1 = 1
            int r2 = r0.readInt(r1)     // Catch:{ IOException -> 0x05bf }
            if (r2 <= 0) goto L_0x05af
            com.esotericsoftware.spine.Animation$EventTimeline r1 = new com.esotericsoftware.spine.Animation$EventTimeline     // Catch:{ IOException -> 0x05bf }
            r1.<init>(r2)     // Catch:{ IOException -> 0x05bf }
            r4 = 0
        L_0x0552:
            if (r4 >= r2) goto L_0x05a0
            float r6 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.EventData> r7 = r5.events     // Catch:{ IOException -> 0x05bf }
            r8 = 1
            int r10 = r0.readInt(r8)     // Catch:{ IOException -> 0x05bf }
            java.lang.Object r7 = r7.get(r10)     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.EventData r7 = (com.esotericsoftware.spine.EventData) r7     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.Event r8 = new com.esotericsoftware.spine.Event     // Catch:{ IOException -> 0x05bf }
            r8.<init>(r6, r7)     // Catch:{ IOException -> 0x05bf }
            r6 = 0
            int r10 = r0.readInt(r6)     // Catch:{ IOException -> 0x05bf }
            r8.intValue = r10     // Catch:{ IOException -> 0x05bf }
            float r10 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            r8.floatValue = r10     // Catch:{ IOException -> 0x05bf }
            boolean r10 = r32.readBoolean()     // Catch:{ IOException -> 0x05bf }
            if (r10 == 0) goto L_0x0582
            java.lang.String r7 = r32.readString()     // Catch:{ IOException -> 0x05bf }
            goto L_0x0584
        L_0x0582:
            java.lang.String r7 = r7.stringValue     // Catch:{ IOException -> 0x05bf }
        L_0x0584:
            r8.stringValue = r7     // Catch:{ IOException -> 0x05bf }
            com.esotericsoftware.spine.EventData r7 = r8.getData()     // Catch:{ IOException -> 0x05bf }
            java.lang.String r7 = r7.audioPath     // Catch:{ IOException -> 0x05bf }
            if (r7 == 0) goto L_0x059a
            float r7 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            r8.volume = r7     // Catch:{ IOException -> 0x05bf }
            float r7 = r32.readFloat()     // Catch:{ IOException -> 0x05bf }
            r8.balance = r7     // Catch:{ IOException -> 0x05bf }
        L_0x059a:
            r1.setFrame(r4, r8)     // Catch:{ IOException -> 0x05bf }
            int r4 = r4 + 1
            goto L_0x0552
        L_0x05a0:
            r3.add(r1)     // Catch:{ IOException -> 0x05bf }
            float[] r0 = r1.getFrames()     // Catch:{ IOException -> 0x05bf }
            r1 = 1
            int r2 = r2 - r1
            r0 = r0[r2]     // Catch:{ IOException -> 0x05bf }
            float r9 = java.lang.Math.max(r9, r0)     // Catch:{ IOException -> 0x05bf }
        L_0x05af:
            r3.c()
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Animation> r0 = r5.animations
            com.esotericsoftware.spine.Animation r1 = new com.esotericsoftware.spine.Animation
            r2 = r33
            r1.<init>(r2, r3, r9)
            r0.add(r1)
            return
        L_0x05bf:
            r0 = move-exception
            com.badlogic.gdx.utils.l0 r1 = new com.badlogic.gdx.utils.l0
            java.lang.String r2 = "Error reading skeleton file."
            r1.<init>(r2, r0)
            goto L_0x05c9
        L_0x05c8:
            throw r1
        L_0x05c9:
            goto L_0x05c8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.SkeletonBinary.readAnimation(com.badlogic.gdx.utils.j, java.lang.String, com.esotericsoftware.spine.SkeletonData):void");
    }

    private Attachment readAttachment(j jVar, SkeletonData skeletonData, Skin skin, int i2, String str, boolean z) throws IOException {
        float f2;
        short[] sArr;
        float f3;
        float f4;
        j jVar2 = jVar;
        Skin skin2 = skin;
        float f5 = this.scale;
        String readString = jVar.readString();
        if (readString == null) {
            readString = str;
        }
        int i3 = AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType[AttachmentType.values[jVar.readByte()].ordinal()];
        float f6 = Animation.CurveTimeline.LINEAR;
        int i4 = 0;
        switch (i3) {
            case 1:
                String readString2 = jVar.readString();
                float readFloat = jVar.readFloat();
                float readFloat2 = jVar.readFloat();
                float readFloat3 = jVar.readFloat();
                float readFloat4 = jVar.readFloat();
                float readFloat5 = jVar.readFloat();
                float readFloat6 = jVar.readFloat();
                float readFloat7 = jVar.readFloat();
                int readInt = jVar.readInt();
                if (readString2 == null) {
                    readString2 = readString;
                }
                RegionAttachment newRegionAttachment = this.attachmentLoader.newRegionAttachment(skin2, readString, readString2);
                if (newRegionAttachment == null) {
                    return null;
                }
                newRegionAttachment.setPath(readString2);
                newRegionAttachment.setX(readFloat2 * f5);
                newRegionAttachment.setY(readFloat3 * f5);
                newRegionAttachment.setScaleX(readFloat4);
                newRegionAttachment.setScaleY(readFloat5);
                newRegionAttachment.setRotation(readFloat);
                newRegionAttachment.setWidth(readFloat6 * f5);
                newRegionAttachment.setHeight(readFloat7 * f5);
                b.b(newRegionAttachment.getColor(), readInt);
                newRegionAttachment.updateOffset();
                return newRegionAttachment;
            case 2:
                int readInt2 = jVar2.readInt(true);
                Vertices readVertices = readVertices(jVar2, readInt2);
                if (z) {
                    i4 = jVar.readInt();
                }
                BoundingBoxAttachment newBoundingBoxAttachment = this.attachmentLoader.newBoundingBoxAttachment(skin2, readString);
                if (newBoundingBoxAttachment == null) {
                    return null;
                }
                newBoundingBoxAttachment.setWorldVerticesLength(readInt2 << 1);
                newBoundingBoxAttachment.setVertices(readVertices.vertices);
                newBoundingBoxAttachment.setBones(readVertices.bones);
                if (z) {
                    b.b(newBoundingBoxAttachment.getColor(), i4);
                }
                return newBoundingBoxAttachment;
            case 3:
                String readString3 = jVar.readString();
                int readInt3 = jVar.readInt();
                int readInt4 = jVar2.readInt(true);
                int i5 = readInt4 << 1;
                float[] readFloatArray = readFloatArray(jVar2, i5, 1.0f);
                short[] readShortArray = readShortArray(jVar);
                Vertices readVertices2 = readVertices(jVar2, readInt4);
                int readInt5 = jVar2.readInt(true);
                if (z) {
                    sArr = readShortArray(jVar);
                    f2 = jVar.readFloat();
                    f3 = jVar.readFloat();
                } else {
                    sArr = null;
                    f3 = Animation.CurveTimeline.LINEAR;
                    f2 = Animation.CurveTimeline.LINEAR;
                }
                if (readString3 == null) {
                    readString3 = readString;
                }
                MeshAttachment newMeshAttachment = this.attachmentLoader.newMeshAttachment(skin2, readString, readString3);
                if (newMeshAttachment == null) {
                    return null;
                }
                newMeshAttachment.setPath(readString3);
                b.b(newMeshAttachment.getColor(), readInt3);
                newMeshAttachment.setBones(readVertices2.bones);
                newMeshAttachment.setVertices(readVertices2.vertices);
                newMeshAttachment.setWorldVerticesLength(i5);
                newMeshAttachment.setTriangles(readShortArray);
                newMeshAttachment.setRegionUVs(readFloatArray);
                newMeshAttachment.updateUVs();
                newMeshAttachment.setHullLength(readInt5 << 1);
                if (z) {
                    newMeshAttachment.setEdges(sArr);
                    newMeshAttachment.setWidth(f2 * f5);
                    newMeshAttachment.setHeight(f3 * f5);
                }
                return newMeshAttachment;
            case 4:
                String readString4 = jVar.readString();
                int readInt6 = jVar.readInt();
                String readString5 = jVar.readString();
                String readString6 = jVar.readString();
                boolean readBoolean = jVar.readBoolean();
                if (z) {
                    f6 = jVar.readFloat();
                    f4 = jVar.readFloat();
                } else {
                    f4 = Animation.CurveTimeline.LINEAR;
                }
                if (readString4 == null) {
                    readString4 = readString;
                }
                MeshAttachment newMeshAttachment2 = this.attachmentLoader.newMeshAttachment(skin2, readString, readString4);
                if (newMeshAttachment2 == null) {
                    return null;
                }
                newMeshAttachment2.setPath(readString4);
                b.b(newMeshAttachment2.getColor(), readInt6);
                newMeshAttachment2.setInheritDeform(readBoolean);
                if (z) {
                    newMeshAttachment2.setWidth(f6 * f5);
                    newMeshAttachment2.setHeight(f4 * f5);
                }
                this.linkedMeshes.add(new SkeletonJson.LinkedMesh(newMeshAttachment2, readString5, i2, readString6));
                return newMeshAttachment2;
            case 5:
                boolean readBoolean2 = jVar.readBoolean();
                boolean readBoolean3 = jVar.readBoolean();
                int readInt7 = jVar2.readInt(true);
                Vertices readVertices3 = readVertices(jVar2, readInt7);
                float[] fArr = new float[(readInt7 / 3)];
                int length = fArr.length;
                for (int i6 = 0; i6 < length; i6++) {
                    fArr[i6] = jVar.readFloat() * f5;
                }
                if (z) {
                    i4 = jVar.readInt();
                }
                PathAttachment newPathAttachment = this.attachmentLoader.newPathAttachment(skin2, readString);
                if (newPathAttachment == null) {
                    return null;
                }
                newPathAttachment.setClosed(readBoolean2);
                newPathAttachment.setConstantSpeed(readBoolean3);
                newPathAttachment.setWorldVerticesLength(readInt7 << 1);
                newPathAttachment.setVertices(readVertices3.vertices);
                newPathAttachment.setBones(readVertices3.bones);
                newPathAttachment.setLengths(fArr);
                if (z) {
                    b.b(newPathAttachment.getColor(), i4);
                }
                return newPathAttachment;
            case 6:
                float readFloat8 = jVar.readFloat();
                float readFloat9 = jVar.readFloat();
                float readFloat10 = jVar.readFloat();
                if (z) {
                    i4 = jVar.readInt();
                }
                PointAttachment newPointAttachment = this.attachmentLoader.newPointAttachment(skin2, readString);
                if (newPointAttachment == null) {
                    return null;
                }
                newPointAttachment.setX(readFloat9 * f5);
                newPointAttachment.setY(readFloat10 * f5);
                newPointAttachment.setRotation(readFloat8);
                if (z) {
                    b.b(newPointAttachment.getColor(), i4);
                }
                return newPointAttachment;
            case 7:
                int readInt8 = jVar2.readInt(true);
                int readInt9 = jVar2.readInt(true);
                Vertices readVertices4 = readVertices(jVar2, readInt9);
                if (z) {
                    i4 = jVar.readInt();
                }
                ClippingAttachment newClippingAttachment = this.attachmentLoader.newClippingAttachment(skin2, readString);
                if (newClippingAttachment == null) {
                    return null;
                }
                newClippingAttachment.setEndSlot(skeletonData.slots.get(readInt8));
                newClippingAttachment.setWorldVerticesLength(readInt9 << 1);
                newClippingAttachment.setVertices(readVertices4.vertices);
                newClippingAttachment.setBones(readVertices4.bones);
                if (z) {
                    b.b(newClippingAttachment.getColor(), i4);
                }
                return newClippingAttachment;
            default:
                return null;
        }
    }

    private void readCurve(j jVar, int i2, Animation.CurveTimeline curveTimeline) throws IOException {
        byte readByte = jVar.readByte();
        if (readByte == 1) {
            curveTimeline.setStepped(i2);
        } else if (readByte == 2) {
            setCurve(curveTimeline, i2, jVar.readFloat(), jVar.readFloat(), jVar.readFloat(), jVar.readFloat());
        }
    }

    private float[] readFloatArray(j jVar, int i2, float f2) throws IOException {
        float[] fArr = new float[i2];
        int i3 = 0;
        if (f2 == 1.0f) {
            while (i3 < i2) {
                fArr[i3] = jVar.readFloat();
                i3++;
            }
        } else {
            while (i3 < i2) {
                fArr[i3] = jVar.readFloat() * f2;
                i3++;
            }
        }
        return fArr;
    }

    private short[] readShortArray(j jVar) throws IOException {
        int readInt = jVar.readInt(true);
        short[] sArr = new short[readInt];
        for (int i2 = 0; i2 < readInt; i2++) {
            sArr[i2] = jVar.readShort();
        }
        return sArr;
    }

    private Skin readSkin(j jVar, SkeletonData skeletonData, String str, boolean z) throws IOException {
        j jVar2 = jVar;
        boolean z2 = true;
        int readInt = jVar2.readInt(true);
        if (readInt == 0) {
            return null;
        }
        Skin skin = new Skin(str);
        int i2 = 0;
        while (i2 < readInt) {
            int readInt2 = jVar2.readInt(z2);
            int readInt3 = jVar2.readInt(z2);
            for (int i3 = 0; i3 < readInt3; i3++) {
                String readString = jVar.readString();
                String str2 = readString;
                Attachment readAttachment = readAttachment(jVar, skeletonData, skin, readInt2, readString, z);
                if (readAttachment != null) {
                    skin.addAttachment(readInt2, str2, readAttachment);
                }
            }
            i2++;
            z2 = true;
        }
        return skin;
    }

    private Vertices readVertices(j jVar, int i2) throws IOException {
        int i3 = i2 << 1;
        Vertices vertices = new Vertices();
        if (!jVar.readBoolean()) {
            vertices.vertices = readFloatArray(jVar, i3, this.scale);
            return vertices;
        }
        int i4 = i3 * 3;
        m mVar = new m(i4 * 3);
        com.badlogic.gdx.utils.q qVar = new com.badlogic.gdx.utils.q(i4);
        for (int i5 = 0; i5 < i2; i5++) {
            int readInt = jVar.readInt(true);
            qVar.a(readInt);
            for (int i6 = 0; i6 < readInt; i6++) {
                qVar.a(jVar.readInt(true));
                mVar.a(jVar.readFloat() * this.scale);
                mVar.a(jVar.readFloat() * this.scale);
                mVar.a(jVar.readFloat());
            }
        }
        vertices.vertices = mVar.d();
        vertices.bones = qVar.c();
        return vertices;
    }

    public float getScale() {
        return this.scale;
    }

    public SkeletonData readSkeletonData(e.d.b.s.a aVar) {
        BoneData boneData;
        if (aVar != null) {
            float f2 = this.scale;
            SkeletonData skeletonData = new SkeletonData();
            skeletonData.name = aVar.h();
            AnonymousClass1 r2 = new j(aVar.a((int) AdRequest.MAX_CONTENT_URL_LENGTH)) {
                private char[] chars = new char[32];

                public String readString() throws IOException {
                    int i2;
                    int readInt = readInt(true);
                    if (readInt == 0) {
                        return null;
                    }
                    if (readInt == 1) {
                        return "";
                    }
                    int i3 = readInt - 1;
                    if (this.chars.length < i3) {
                        this.chars = new char[i3];
                    }
                    char[] cArr = this.chars;
                    int i4 = 0;
                    int i5 = 0;
                    while (i4 < i3) {
                        int read = read();
                        int i6 = read >> 4;
                        if (i6 != -1) {
                            switch (i6) {
                                case 12:
                                case 13:
                                    i2 = i5 + 1;
                                    cArr[i5] = (char) (((read & 31) << 6) | (read() & 63));
                                    i4 += 2;
                                    break;
                                case 14:
                                    i2 = i5 + 1;
                                    cArr[i5] = (char) (((read & 15) << 12) | ((read() & 63) << 6) | (read() & 63));
                                    i4 += 3;
                                    break;
                                default:
                                    i2 = i5 + 1;
                                    cArr[i5] = (char) read;
                                    i4++;
                                    break;
                            }
                            i5 = i2;
                        } else {
                            throw new EOFException();
                        }
                    }
                    return new String(cArr, 0, i5);
                }
            };
            try {
                skeletonData.hash = r2.readString();
                if (skeletonData.hash.isEmpty()) {
                    skeletonData.hash = null;
                }
                skeletonData.version = r2.readString();
                if (skeletonData.version.isEmpty()) {
                    skeletonData.version = null;
                }
                skeletonData.width = r2.readFloat();
                skeletonData.height = r2.readFloat();
                boolean readBoolean = r2.readBoolean();
                if (readBoolean) {
                    skeletonData.fps = r2.readFloat();
                    skeletonData.imagesPath = r2.readString();
                    if (skeletonData.imagesPath.isEmpty()) {
                        skeletonData.imagesPath = null;
                    }
                    skeletonData.audioPath = r2.readString();
                    if (skeletonData.audioPath.isEmpty()) {
                        skeletonData.audioPath = null;
                    }
                }
                int readInt = r2.readInt(true);
                for (int i2 = 0; i2 < readInt; i2++) {
                    String readString = r2.readString();
                    if (i2 == 0) {
                        boneData = null;
                    } else {
                        boneData = skeletonData.bones.get(r2.readInt(true));
                    }
                    BoneData boneData2 = new BoneData(i2, readString, boneData);
                    boneData2.rotation = r2.readFloat();
                    boneData2.x = r2.readFloat() * f2;
                    boneData2.y = r2.readFloat() * f2;
                    boneData2.scaleX = r2.readFloat();
                    boneData2.scaleY = r2.readFloat();
                    boneData2.shearX = r2.readFloat();
                    boneData2.shearY = r2.readFloat();
                    boneData2.length = r2.readFloat() * f2;
                    boneData2.transformMode = BoneData.TransformMode.values[r2.readInt(true)];
                    if (readBoolean) {
                        b.b(boneData2.color, r2.readInt());
                    }
                    skeletonData.bones.add(boneData2);
                }
                int readInt2 = r2.readInt(true);
                for (int i3 = 0; i3 < readInt2; i3++) {
                    SlotData slotData = new SlotData(i3, r2.readString(), skeletonData.bones.get(r2.readInt(true)));
                    b.b(slotData.color, r2.readInt());
                    int readInt3 = r2.readInt();
                    if (readInt3 != -1) {
                        b bVar = new b();
                        slotData.darkColor = bVar;
                        b.a(bVar, readInt3);
                    }
                    slotData.attachmentName = r2.readString();
                    slotData.blendMode = BlendMode.values[r2.readInt(true)];
                    skeletonData.slots.add(slotData);
                }
                int readInt4 = r2.readInt(true);
                for (int i4 = 0; i4 < readInt4; i4++) {
                    IkConstraintData ikConstraintData = new IkConstraintData(r2.readString());
                    ikConstraintData.order = r2.readInt(true);
                    int readInt5 = r2.readInt(true);
                    for (int i5 = 0; i5 < readInt5; i5++) {
                        ikConstraintData.bones.add(skeletonData.bones.get(r2.readInt(true)));
                    }
                    ikConstraintData.target = skeletonData.bones.get(r2.readInt(true));
                    ikConstraintData.mix = r2.readFloat();
                    ikConstraintData.bendDirection = r2.readByte();
                    ikConstraintData.compress = r2.readBoolean();
                    ikConstraintData.stretch = r2.readBoolean();
                    ikConstraintData.uniform = r2.readBoolean();
                    skeletonData.ikConstraints.add(ikConstraintData);
                }
                int readInt6 = r2.readInt(true);
                for (int i6 = 0; i6 < readInt6; i6++) {
                    TransformConstraintData transformConstraintData = new TransformConstraintData(r2.readString());
                    transformConstraintData.order = r2.readInt(true);
                    int readInt7 = r2.readInt(true);
                    for (int i7 = 0; i7 < readInt7; i7++) {
                        transformConstraintData.bones.add(skeletonData.bones.get(r2.readInt(true)));
                    }
                    transformConstraintData.target = skeletonData.bones.get(r2.readInt(true));
                    transformConstraintData.local = r2.readBoolean();
                    transformConstraintData.relative = r2.readBoolean();
                    transformConstraintData.offsetRotation = r2.readFloat();
                    transformConstraintData.offsetX = r2.readFloat() * f2;
                    transformConstraintData.offsetY = r2.readFloat() * f2;
                    transformConstraintData.offsetScaleX = r2.readFloat();
                    transformConstraintData.offsetScaleY = r2.readFloat();
                    transformConstraintData.offsetShearY = r2.readFloat();
                    transformConstraintData.rotateMix = r2.readFloat();
                    transformConstraintData.translateMix = r2.readFloat();
                    transformConstraintData.scaleMix = r2.readFloat();
                    transformConstraintData.shearMix = r2.readFloat();
                    skeletonData.transformConstraints.add(transformConstraintData);
                }
                int readInt8 = r2.readInt(true);
                for (int i8 = 0; i8 < readInt8; i8++) {
                    PathConstraintData pathConstraintData = new PathConstraintData(r2.readString());
                    pathConstraintData.order = r2.readInt(true);
                    int readInt9 = r2.readInt(true);
                    for (int i9 = 0; i9 < readInt9; i9++) {
                        pathConstraintData.bones.add(skeletonData.bones.get(r2.readInt(true)));
                    }
                    pathConstraintData.target = skeletonData.slots.get(r2.readInt(true));
                    pathConstraintData.positionMode = PathConstraintData.PositionMode.values[r2.readInt(true)];
                    pathConstraintData.spacingMode = PathConstraintData.SpacingMode.values[r2.readInt(true)];
                    pathConstraintData.rotateMode = PathConstraintData.RotateMode.values[r2.readInt(true)];
                    pathConstraintData.offsetRotation = r2.readFloat();
                    pathConstraintData.position = r2.readFloat();
                    if (pathConstraintData.positionMode == PathConstraintData.PositionMode.fixed) {
                        pathConstraintData.position *= f2;
                    }
                    pathConstraintData.spacing = r2.readFloat();
                    if (pathConstraintData.spacingMode == PathConstraintData.SpacingMode.length || pathConstraintData.spacingMode == PathConstraintData.SpacingMode.fixed) {
                        pathConstraintData.spacing *= f2;
                    }
                    pathConstraintData.rotateMix = r2.readFloat();
                    pathConstraintData.translateMix = r2.readFloat();
                    skeletonData.pathConstraints.add(pathConstraintData);
                }
                Skin readSkin = readSkin(r2, skeletonData, RemoteConfigConst.PREDICTION_METHOD_DEFAULT, readBoolean);
                if (readSkin != null) {
                    skeletonData.defaultSkin = readSkin;
                    skeletonData.skins.add(readSkin);
                }
                int readInt10 = r2.readInt(true);
                for (int i10 = 0; i10 < readInt10; i10++) {
                    skeletonData.skins.add(readSkin(r2, skeletonData, r2.readString(), readBoolean));
                }
                int i11 = this.linkedMeshes.f5374b;
                int i12 = 0;
                while (i12 < i11) {
                    SkeletonJson.LinkedMesh linkedMesh = this.linkedMeshes.get(i12);
                    Skin defaultSkin = linkedMesh.skin == null ? skeletonData.getDefaultSkin() : skeletonData.findSkin(linkedMesh.skin);
                    if (defaultSkin != null) {
                        Attachment attachment = defaultSkin.getAttachment(linkedMesh.slotIndex, linkedMesh.parent);
                        if (attachment != null) {
                            linkedMesh.mesh.setParentMesh((MeshAttachment) attachment);
                            linkedMesh.mesh.updateUVs();
                            i12++;
                        } else {
                            throw new l0("Parent mesh not found: " + linkedMesh.parent);
                        }
                    } else {
                        throw new l0("Skin not found: " + linkedMesh.skin);
                    }
                }
                this.linkedMeshes.clear();
                int readInt11 = r2.readInt(true);
                for (int i13 = 0; i13 < readInt11; i13++) {
                    EventData eventData = new EventData(r2.readString());
                    eventData.intValue = r2.readInt(false);
                    eventData.floatValue = r2.readFloat();
                    eventData.stringValue = r2.readString();
                    eventData.audioPath = r2.readString();
                    if (eventData.audioPath != null) {
                        eventData.volume = r2.readFloat();
                        eventData.balance = r2.readFloat();
                    }
                    skeletonData.events.add(eventData);
                }
                int readInt12 = r2.readInt(true);
                for (int i14 = 0; i14 < readInt12; i14++) {
                    readAnimation(r2, r2.readString(), skeletonData);
                }
                try {
                    r2.close();
                } catch (IOException unused) {
                }
                skeletonData.bones.c();
                skeletonData.slots.c();
                skeletonData.skins.c();
                skeletonData.events.c();
                skeletonData.animations.c();
                skeletonData.ikConstraints.c();
                return skeletonData;
            } catch (IOException e2) {
                throw new l0("Error reading skeleton file.", e2);
            } catch (Throwable th) {
                try {
                    r2.close();
                } catch (IOException unused2) {
                }
                throw th;
            }
        } else {
            throw new IllegalArgumentException("file cannot be null.");
        }
    }

    /* access modifiers changed from: package-private */
    public void setCurve(Animation.CurveTimeline curveTimeline, int i2, float f2, float f3, float f4, float f5) {
        curveTimeline.setCurve(i2, f2, f3, f4, f5);
    }

    public void setScale(float f2) {
        this.scale = f2;
    }

    public SkeletonBinary(AttachmentLoader attachmentLoader2) {
        if (attachmentLoader2 != null) {
            this.attachmentLoader = attachmentLoader2;
            return;
        }
        throw new IllegalArgumentException("attachmentLoader cannot be null.");
    }
}
