package com.esotericsoftware.spine;

import com.badlogic.gdx.math.i;
import com.badlogic.gdx.math.p;
import com.badlogic.gdx.utils.a;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.BoneData;
import com.esotericsoftware.spine.utils.SpineUtils;

public class Bone implements Updatable {

    /* renamed from: a  reason: collision with root package name */
    float f6593a;
    boolean appliedValid;
    float arotation;
    float ascaleX;
    float ascaleY;
    float ashearX;
    float ashearY;
    float ax;
    float ay;

    /* renamed from: b  reason: collision with root package name */
    float f6594b;

    /* renamed from: c  reason: collision with root package name */
    float f6595c;
    final a<Bone> children = new a<>();

    /* renamed from: d  reason: collision with root package name */
    float f6596d;
    final BoneData data;
    final Bone parent;
    float rotation;
    float scaleX;
    float scaleY;
    float shearX;
    float shearY;
    final Skeleton skeleton;
    boolean sorted;
    float worldX;
    float worldY;
    float x;
    float y;

    /* renamed from: com.esotericsoftware.spine.Bone$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$esotericsoftware$spine$BoneData$TransformMode = new int[BoneData.TransformMode.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.esotericsoftware.spine.BoneData$TransformMode[] r0 = com.esotericsoftware.spine.BoneData.TransformMode.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.esotericsoftware.spine.Bone.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$BoneData$TransformMode = r0
                int[] r0 = com.esotericsoftware.spine.Bone.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$BoneData$TransformMode     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.esotericsoftware.spine.BoneData$TransformMode r1 = com.esotericsoftware.spine.BoneData.TransformMode.normal     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.esotericsoftware.spine.Bone.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$BoneData$TransformMode     // Catch:{ NoSuchFieldError -> 0x001f }
                com.esotericsoftware.spine.BoneData$TransformMode r1 = com.esotericsoftware.spine.BoneData.TransformMode.onlyTranslation     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.esotericsoftware.spine.Bone.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$BoneData$TransformMode     // Catch:{ NoSuchFieldError -> 0x002a }
                com.esotericsoftware.spine.BoneData$TransformMode r1 = com.esotericsoftware.spine.BoneData.TransformMode.noRotationOrReflection     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.esotericsoftware.spine.Bone.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$BoneData$TransformMode     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.esotericsoftware.spine.BoneData$TransformMode r1 = com.esotericsoftware.spine.BoneData.TransformMode.noScale     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.esotericsoftware.spine.Bone.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$BoneData$TransformMode     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.esotericsoftware.spine.BoneData$TransformMode r1 = com.esotericsoftware.spine.BoneData.TransformMode.noScaleOrReflection     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.Bone.AnonymousClass1.<clinit>():void");
        }
    }

    public Bone(BoneData boneData, Skeleton skeleton2, Bone bone) {
        if (boneData == null) {
            throw new IllegalArgumentException("data cannot be null.");
        } else if (skeleton2 != null) {
            this.data = boneData;
            this.skeleton = skeleton2;
            this.parent = bone;
            setToSetupPose();
        } else {
            throw new IllegalArgumentException("skeleton cannot be null.");
        }
    }

    public float getA() {
        return this.f6593a;
    }

    public float getARotation() {
        return this.arotation;
    }

    public float getAScaleX() {
        return this.ascaleX;
    }

    public float getAScaleY() {
        return this.ascaleY;
    }

    public float getAShearX() {
        return this.ashearX;
    }

    public float getAShearY() {
        return this.ashearY;
    }

    public float getAX() {
        return this.ax;
    }

    public float getAY() {
        return this.ay;
    }

    public float getB() {
        return this.f6594b;
    }

    public float getC() {
        return this.f6595c;
    }

    public a<Bone> getChildren() {
        return this.children;
    }

    public float getD() {
        return this.f6596d;
    }

    public BoneData getData() {
        return this.data;
    }

    public Bone getParent() {
        return this.parent;
    }

    public float getRotation() {
        return this.rotation;
    }

    public float getScaleX() {
        return this.scaleX;
    }

    public float getScaleY() {
        return this.scaleY;
    }

    public float getShearX() {
        return this.shearX;
    }

    public float getShearY() {
        return this.shearY;
    }

    public Skeleton getSkeleton() {
        return this.skeleton;
    }

    public float getWorldRotationX() {
        return SpineUtils.atan2(this.f6595c, this.f6593a) * 57.295776f;
    }

    public float getWorldRotationY() {
        return SpineUtils.atan2(this.f6596d, this.f6594b) * 57.295776f;
    }

    public float getWorldScaleX() {
        float f2 = this.f6593a;
        float f3 = this.f6595c;
        return (float) Math.sqrt((double) ((f2 * f2) + (f3 * f3)));
    }

    public float getWorldScaleY() {
        float f2 = this.f6594b;
        float f3 = this.f6596d;
        return (float) Math.sqrt((double) ((f2 * f2) + (f3 * f3)));
    }

    public i getWorldTransform(i iVar) {
        if (iVar != null) {
            float[] fArr = iVar.f5269a;
            fArr[0] = this.f6593a;
            fArr[3] = this.f6594b;
            fArr[1] = this.f6595c;
            fArr[4] = this.f6596d;
            fArr[6] = this.worldX;
            fArr[7] = this.worldY;
            fArr[2] = 0.0f;
            fArr[5] = 0.0f;
            fArr[8] = 1.0f;
            return iVar;
        }
        throw new IllegalArgumentException("worldTransform cannot be null.");
    }

    public float getWorldX() {
        return this.worldX;
    }

    public float getWorldY() {
        return this.worldY;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public boolean isAppliedValid() {
        return this.appliedValid;
    }

    public p localToWorld(p pVar) {
        float f2 = pVar.f5294a;
        float f3 = pVar.f5295b;
        pVar.f5294a = (this.f6593a * f2) + (this.f6594b * f3) + this.worldX;
        pVar.f5295b = (f2 * this.f6595c) + (f3 * this.f6596d) + this.worldY;
        return pVar;
    }

    public float localToWorldRotation(float f2) {
        float f3 = f2 - (this.rotation - this.shearX);
        float sinDeg = SpineUtils.sinDeg(f3);
        float cosDeg = SpineUtils.cosDeg(f3);
        return SpineUtils.atan2((this.f6595c * cosDeg) + (this.f6596d * sinDeg), (cosDeg * this.f6593a) + (sinDeg * this.f6594b)) * 57.295776f;
    }

    public void rotateWorld(float f2) {
        float cosDeg = SpineUtils.cosDeg(f2);
        float sinDeg = SpineUtils.sinDeg(f2);
        float f3 = this.f6595c;
        this.f6593a = (this.f6593a * cosDeg) - (sinDeg * f3);
        float f4 = this.f6596d;
        this.f6594b = (this.f6594b * cosDeg) - (sinDeg * f4);
        this.f6595c = (this.f6593a * sinDeg) + (f3 * cosDeg);
        this.f6596d = (sinDeg * this.f6594b) + (cosDeg * f4);
        this.appliedValid = false;
    }

    public void setA(float f2) {
        this.f6593a = f2;
    }

    public void setARotation(float f2) {
        this.arotation = f2;
    }

    public void setAScaleX(float f2) {
        this.ascaleX = f2;
    }

    public void setAScaleY(float f2) {
        this.ascaleY = f2;
    }

    public void setAShearX(float f2) {
        this.ashearX = f2;
    }

    public void setAShearY(float f2) {
        this.ashearY = f2;
    }

    public void setAX(float f2) {
        this.ax = f2;
    }

    public void setAY(float f2) {
        this.ay = f2;
    }

    public void setAppliedValid(boolean z) {
        this.appliedValid = z;
    }

    public void setB(float f2) {
        this.f6594b = f2;
    }

    public void setC(float f2) {
        this.f6595c = f2;
    }

    public void setD(float f2) {
        this.f6596d = f2;
    }

    public void setPosition(float f2, float f3) {
        this.x = f2;
        this.y = f3;
    }

    public void setRotation(float f2) {
        this.rotation = f2;
    }

    public void setScale(float f2, float f3) {
        this.scaleX = f2;
        this.scaleY = f3;
    }

    public void setScaleX(float f2) {
        this.scaleX = f2;
    }

    public void setScaleY(float f2) {
        this.scaleY = f2;
    }

    public void setShearX(float f2) {
        this.shearX = f2;
    }

    public void setShearY(float f2) {
        this.shearY = f2;
    }

    public void setToSetupPose() {
        BoneData boneData = this.data;
        this.x = boneData.x;
        this.y = boneData.y;
        this.rotation = boneData.rotation;
        this.scaleX = boneData.scaleX;
        this.scaleY = boneData.scaleY;
        this.shearX = boneData.shearX;
        this.shearY = boneData.shearY;
    }

    public void setWorldX(float f2) {
        this.worldX = f2;
    }

    public void setWorldY(float f2) {
        this.worldY = f2;
    }

    public void setX(float f2) {
        this.x = f2;
    }

    public void setY(float f2) {
        this.y = f2;
    }

    public String toString() {
        return this.data.name;
    }

    public void update() {
        updateWorldTransform(this.x, this.y, this.rotation, this.scaleX, this.scaleY, this.shearX, this.shearY);
    }

    public void updateAppliedTransform() {
        this.appliedValid = true;
        Bone bone = this.parent;
        if (bone == null) {
            this.ax = this.worldX;
            this.ay = this.worldY;
            this.arotation = SpineUtils.atan2(this.f6595c, this.f6593a) * 57.295776f;
            float f2 = this.f6593a;
            float f3 = this.f6595c;
            this.ascaleX = (float) Math.sqrt((double) ((f2 * f2) + (f3 * f3)));
            float f4 = this.f6594b;
            float f5 = this.f6596d;
            this.ascaleY = (float) Math.sqrt((double) ((f4 * f4) + (f5 * f5)));
            this.ashearX = Animation.CurveTimeline.LINEAR;
            float f6 = this.f6593a;
            float f7 = this.f6594b;
            float f8 = this.f6595c;
            float f9 = this.f6596d;
            this.ashearY = SpineUtils.atan2((f6 * f7) + (f8 * f9), (f6 * f9) - (f7 * f8)) * 57.295776f;
            return;
        }
        float f10 = bone.f6593a;
        float f11 = bone.f6594b;
        float f12 = bone.f6595c;
        float f13 = bone.f6596d;
        float f14 = 1.0f / ((f10 * f13) - (f11 * f12));
        float f15 = this.worldX - bone.worldX;
        float f16 = this.worldY - bone.worldY;
        this.ax = ((f15 * f13) * f14) - ((f16 * f11) * f14);
        this.ay = ((f16 * f10) * f14) - ((f15 * f12) * f14);
        float f17 = f13 * f14;
        float f18 = f10 * f14;
        float f19 = f11 * f14;
        float f20 = f14 * f12;
        float f21 = this.f6593a;
        float f22 = this.f6595c;
        float f23 = (f17 * f21) - (f19 * f22);
        float f24 = this.f6594b;
        float f25 = this.f6596d;
        float f26 = (f17 * f24) - (f19 * f25);
        float f27 = (f22 * f18) - (f21 * f20);
        float f28 = (f18 * f25) - (f20 * f24);
        this.ashearX = Animation.CurveTimeline.LINEAR;
        this.ascaleX = (float) Math.sqrt((double) ((f23 * f23) + (f27 * f27)));
        float f29 = this.ascaleX;
        if (f29 > 1.0E-4f) {
            float f30 = (f23 * f28) - (f26 * f27);
            this.ascaleY = f30 / f29;
            this.ashearY = SpineUtils.atan2((f26 * f23) + (f28 * f27), f30) * 57.295776f;
            this.arotation = SpineUtils.atan2(f27, f23) * 57.295776f;
            return;
        }
        this.ascaleX = Animation.CurveTimeline.LINEAR;
        this.ascaleY = (float) Math.sqrt((double) ((f26 * f26) + (f28 * f28)));
        this.ashearY = Animation.CurveTimeline.LINEAR;
        this.arotation = 90.0f - (SpineUtils.atan2(f28, f26) * 57.295776f);
    }

    public void updateWorldTransform() {
        updateWorldTransform(this.x, this.y, this.rotation, this.scaleX, this.scaleY, this.shearX, this.shearY);
    }

    public p worldToLocal(p pVar) {
        float f2 = this.f6593a;
        float f3 = this.f6596d;
        float f4 = this.f6594b;
        float f5 = this.f6595c;
        float f6 = 1.0f / ((f2 * f3) - (f4 * f5));
        float f7 = pVar.f5294a - this.worldX;
        float f8 = pVar.f5295b - this.worldY;
        pVar.f5294a = ((f3 * f7) * f6) - ((f4 * f8) * f6);
        pVar.f5295b = ((f8 * f2) * f6) - ((f7 * f5) * f6);
        return pVar;
    }

    public float worldToLocalRotation(float f2) {
        float sinDeg = SpineUtils.sinDeg(f2);
        float cosDeg = SpineUtils.cosDeg(f2);
        return ((SpineUtils.atan2((this.f6593a * sinDeg) - (this.f6595c * cosDeg), (this.f6596d * cosDeg) - (this.f6594b * sinDeg)) * 57.295776f) + this.rotation) - this.shearX;
    }

    public void updateWorldTransform(float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        float f9;
        float f10;
        float f11 = f2;
        float f12 = f3;
        float f13 = f4;
        float f14 = f5;
        float f15 = f6;
        float f16 = f7;
        float f17 = f8;
        this.ax = f11;
        this.ay = f12;
        this.arotation = f13;
        this.ascaleX = f14;
        this.ascaleY = f15;
        this.ashearX = f16;
        this.ashearY = f17;
        this.appliedValid = true;
        Bone bone = this.parent;
        if (bone == null) {
            Skeleton skeleton2 = this.skeleton;
            float f18 = f13 + 90.0f + f17;
            float f19 = skeleton2.scaleX;
            float f20 = skeleton2.scaleY;
            float f21 = f13 + f16;
            this.f6593a = SpineUtils.cosDeg(f21) * f14 * f19;
            this.f6594b = SpineUtils.cosDeg(f18) * f15 * f20;
            this.f6595c = SpineUtils.sinDeg(f21) * f14 * f19;
            this.f6596d = SpineUtils.sinDeg(f18) * f15 * f20;
            this.worldX = (f11 * f19) + skeleton2.x;
            this.worldY = (f12 * f20) + skeleton2.y;
            return;
        }
        float f22 = bone.f6593a;
        float f23 = bone.f6594b;
        float f24 = bone.f6595c;
        float f25 = bone.f6596d;
        this.worldX = (f22 * f11) + (f23 * f12) + bone.worldX;
        this.worldY = (f11 * f24) + (f12 * f25) + bone.worldY;
        int i2 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$BoneData$TransformMode[this.data.transformMode.ordinal()];
        if (i2 != 1) {
            if (i2 == 2) {
                float f26 = 90.0f + f13 + f17;
                float f27 = f13 + f16;
                this.f6593a = SpineUtils.cosDeg(f27) * f14;
                this.f6594b = SpineUtils.cosDeg(f26) * f15;
                this.f6595c = SpineUtils.sinDeg(f27) * f14;
                this.f6596d = SpineUtils.sinDeg(f26) * f15;
            } else if (i2 == 3) {
                float f28 = Animation.CurveTimeline.LINEAR;
                float f29 = (f22 * f22) + (f24 * f24);
                if (f29 > 1.0E-4f) {
                    float abs = Math.abs((f25 * f22) - (f23 * f24)) / f29;
                    f23 = f24 * abs;
                    f25 = f22 * abs;
                    f10 = SpineUtils.atan2(f24, f22) * 57.295776f;
                    f28 = f22;
                    f9 = 90.0f;
                } else {
                    f9 = 90.0f;
                    f10 = 90.0f - (SpineUtils.atan2(f25, f23) * 57.295776f);
                    f24 = Animation.CurveTimeline.LINEAR;
                }
                float f30 = (f16 + f13) - f10;
                float f31 = ((f13 + f17) - f10) + f9;
                float cosDeg = SpineUtils.cosDeg(f30) * f14;
                float cosDeg2 = SpineUtils.cosDeg(f31) * f15;
                float sinDeg = SpineUtils.sinDeg(f30) * f14;
                float sinDeg2 = SpineUtils.sinDeg(f31) * f15;
                this.f6593a = (f28 * cosDeg) - (f23 * sinDeg);
                this.f6594b = (f28 * cosDeg2) - (f23 * sinDeg2);
                this.f6595c = (cosDeg * f24) + (sinDeg * f25);
                this.f6596d = (f24 * cosDeg2) + (f25 * sinDeg2);
            } else if (i2 == 4 || i2 == 5) {
                float cosDeg3 = SpineUtils.cosDeg(f4);
                float sinDeg3 = SpineUtils.sinDeg(f4);
                Skeleton skeleton3 = this.skeleton;
                float f32 = ((f22 * cosDeg3) + (f23 * sinDeg3)) / skeleton3.scaleX;
                float f33 = ((cosDeg3 * f24) + (sinDeg3 * f25)) / skeleton3.scaleY;
                float sqrt = (float) Math.sqrt((double) ((f32 * f32) + (f33 * f33)));
                if (sqrt > 1.0E-5f) {
                    sqrt = 1.0f / sqrt;
                }
                float f34 = f32 * sqrt;
                float f35 = f33 * sqrt;
                float sqrt2 = (float) Math.sqrt((double) ((f34 * f34) + (f35 * f35)));
                if (this.data.transformMode == BoneData.TransformMode.noScale) {
                    boolean z = false;
                    boolean z2 = (f22 * f25) - (f23 * f24) < Animation.CurveTimeline.LINEAR;
                    if ((this.skeleton.scaleX < Animation.CurveTimeline.LINEAR) != (this.skeleton.scaleY < Animation.CurveTimeline.LINEAR)) {
                        z = true;
                    }
                    if (z2 != z) {
                        sqrt2 = -sqrt2;
                    }
                }
                float atan2 = SpineUtils.atan2(f35, f34) + 1.5707964f;
                float cos = SpineUtils.cos(atan2) * sqrt2;
                float sin = SpineUtils.sin(atan2) * sqrt2;
                float cosDeg4 = SpineUtils.cosDeg(f7) * f14;
                float f36 = f17 + 90.0f;
                float cosDeg5 = SpineUtils.cosDeg(f36) * f15;
                float sinDeg4 = SpineUtils.sinDeg(f7) * f14;
                float sinDeg5 = SpineUtils.sinDeg(f36) * f15;
                this.f6593a = (f34 * cosDeg4) + (cos * sinDeg4);
                this.f6594b = (f34 * cosDeg5) + (cos * sinDeg5);
                this.f6595c = (cosDeg4 * f35) + (sinDeg4 * sin);
                this.f6596d = (f35 * cosDeg5) + (sin * sinDeg5);
            }
            float f37 = this.f6593a;
            Skeleton skeleton4 = this.skeleton;
            float f38 = skeleton4.scaleX;
            this.f6593a = f37 * f38;
            this.f6594b *= f38;
            float f39 = this.f6595c;
            float f40 = skeleton4.scaleY;
            this.f6595c = f39 * f40;
            this.f6596d *= f40;
            return;
        }
        float f41 = 90.0f + f13 + f17;
        float f42 = f13 + f16;
        float cosDeg6 = SpineUtils.cosDeg(f42) * f14;
        float cosDeg7 = SpineUtils.cosDeg(f41) * f15;
        float sinDeg6 = SpineUtils.sinDeg(f42) * f14;
        float sinDeg7 = SpineUtils.sinDeg(f41) * f15;
        this.f6593a = (f22 * cosDeg6) + (f23 * sinDeg6);
        this.f6594b = (f22 * cosDeg7) + (f23 * sinDeg7);
        this.f6595c = (cosDeg6 * f24) + (sinDeg6 * f25);
        this.f6596d = (f24 * cosDeg7) + (f25 * sinDeg7);
    }

    public void setScale(float f2) {
        this.scaleX = f2;
        this.scaleY = f2;
    }

    public Bone(Bone bone, Skeleton skeleton2, Bone bone2) {
        if (bone == null) {
            throw new IllegalArgumentException("bone cannot be null.");
        } else if (skeleton2 != null) {
            this.skeleton = skeleton2;
            this.parent = bone2;
            this.data = bone.data;
            this.x = bone.x;
            this.y = bone.y;
            this.rotation = bone.rotation;
            this.scaleX = bone.scaleX;
            this.scaleY = bone.scaleY;
            this.shearX = bone.shearX;
            this.shearY = bone.shearY;
        } else {
            throw new IllegalArgumentException("skeleton cannot be null.");
        }
    }
}
