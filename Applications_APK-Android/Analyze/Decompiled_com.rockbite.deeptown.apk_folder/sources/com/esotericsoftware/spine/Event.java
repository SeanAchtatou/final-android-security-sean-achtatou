package com.esotericsoftware.spine;

public class Event {
    float balance;
    private final EventData data;
    float floatValue;
    int intValue;
    String stringValue;
    final float time;
    float volume;

    public Event(float f2, EventData eventData) {
        if (eventData != null) {
            this.time = f2;
            this.data = eventData;
            return;
        }
        throw new IllegalArgumentException("data cannot be null.");
    }

    public float getBalance() {
        return this.balance;
    }

    public EventData getData() {
        return this.data;
    }

    public float getFloat() {
        return this.floatValue;
    }

    public int getInt() {
        return this.intValue;
    }

    public String getString() {
        return this.stringValue;
    }

    public float getTime() {
        return this.time;
    }

    public float getVolume() {
        return this.volume;
    }

    public void setBalance(float f2) {
        this.balance = f2;
    }

    public void setFloat(float f2) {
        this.floatValue = f2;
    }

    public void setInt(int i2) {
        this.intValue = i2;
    }

    public void setString(String str) {
        this.stringValue = str;
    }

    public void setVolume(float f2) {
        this.volume = f2;
    }

    public String toString() {
        return this.data.name;
    }
}
