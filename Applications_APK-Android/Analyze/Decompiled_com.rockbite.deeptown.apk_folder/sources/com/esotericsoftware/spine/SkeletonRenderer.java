package com.esotericsoftware.spine;

import com.badlogic.gdx.graphics.g2d.n;
import com.badlogic.gdx.math.p;
import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.m;
import com.badlogic.gdx.utils.n0;
import com.badlogic.gdx.utils.z;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.ClippingAttachment;
import com.esotericsoftware.spine.attachments.MeshAttachment;
import com.esotericsoftware.spine.attachments.RegionAttachment;
import com.esotericsoftware.spine.attachments.SkeletonAttachment;
import com.esotericsoftware.spine.utils.SkeletonClipping;
import com.esotericsoftware.spine.utils.TwoColorPolygonBatch;
import e.d.b.t.b;

public class SkeletonRenderer {
    private static final short[] quadTriangles = {0, 1, 2, 2, 3, 0};
    private final SkeletonClipping clipper = new SkeletonClipping();
    private boolean premultipliedAlpha;
    private final p temp = new p();
    private final p temp2 = new p();
    private final b temp3 = new b();
    private final b temp4 = new b();
    private final b temp5 = new b();
    private final b temp6 = new b();
    private VertexEffect vertexEffect;
    private final m vertices = new m(32);

    public interface VertexEffect {
        void begin(Skeleton skeleton);

        void end();

        void transform(p pVar, p pVar2, b bVar, b bVar2);
    }

    private void applyVertexEffect(float[] fArr, int i2, int i3, float f2, float f3) {
        p pVar = this.temp;
        p pVar2 = this.temp2;
        b bVar = this.temp3;
        b bVar2 = this.temp4;
        b bVar3 = this.temp5;
        b bVar4 = this.temp6;
        VertexEffect vertexEffect2 = this.vertexEffect;
        bVar3.a(z.b(f2));
        bVar4.a(z.b(f3));
        int i4 = 0;
        if (i3 == 5) {
            while (i4 < i2) {
                pVar.f5294a = fArr[i4];
                int i5 = i4 + 1;
                pVar.f5295b = fArr[i5];
                int i6 = i4 + 3;
                pVar2.f5294a = fArr[i6];
                int i7 = i4 + 4;
                pVar2.f5295b = fArr[i7];
                bVar.b(bVar3);
                bVar2.b(bVar4);
                vertexEffect2.transform(pVar, pVar2, bVar, bVar2);
                fArr[i4] = pVar.f5294a;
                fArr[i5] = pVar.f5295b;
                fArr[i4 + 2] = bVar.b();
                fArr[i6] = pVar2.f5294a;
                fArr[i7] = pVar2.f5295b;
                i4 += i3;
            }
            return;
        }
        while (i4 < i2) {
            pVar.f5294a = fArr[i4];
            int i8 = i4 + 1;
            pVar.f5295b = fArr[i8];
            int i9 = i4 + 4;
            pVar2.f5294a = fArr[i9];
            int i10 = i4 + 5;
            pVar2.f5295b = fArr[i10];
            bVar.b(bVar3);
            bVar2.b(bVar4);
            vertexEffect2.transform(pVar, pVar2, bVar, bVar2);
            fArr[i4] = pVar.f5294a;
            fArr[i8] = pVar.f5295b;
            fArr[i4 + 2] = bVar.b();
            fArr[i4 + 3] = bVar2.b();
            fArr[i9] = pVar2.f5294a;
            fArr[i10] = pVar2.f5295b;
            i4 += i3;
        }
    }

    public void draw(com.badlogic.gdx.graphics.g2d.b bVar, Skeleton skeleton) {
        float f2;
        int i2;
        int i3;
        Slot slot;
        Skeleton skeleton2;
        float f3;
        Slot slot2;
        BlendMode blendMode;
        float f4;
        Slot slot3;
        RegionAttachment regionAttachment;
        com.badlogic.gdx.graphics.g2d.b bVar2 = bVar;
        Skeleton skeleton3 = skeleton;
        if (bVar2 instanceof TwoColorPolygonBatch) {
            draw((TwoColorPolygonBatch) bVar2, skeleton3);
        } else if (bVar2 instanceof n) {
            draw((n) bVar2, skeleton3);
        } else {
            VertexEffect vertexEffect2 = this.vertexEffect;
            if (vertexEffect2 != null) {
                vertexEffect2.begin(skeleton3);
            }
            boolean z = this.premultipliedAlpha;
            BlendMode blendMode2 = null;
            float[] fArr = this.vertices.f5527a;
            b bVar3 = skeleton3.color;
            float f5 = bVar3.f6934a;
            float f6 = bVar3.f6935b;
            float f7 = bVar3.f6936c;
            float f8 = bVar3.f6937d;
            a<Slot> aVar = skeleton3.drawOrder;
            int i4 = aVar.f5374b;
            int i5 = 0;
            while (i5 < i4) {
                Slot slot4 = aVar.get(i5);
                Attachment attachment = slot4.attachment;
                if (attachment instanceof RegionAttachment) {
                    RegionAttachment regionAttachment2 = (RegionAttachment) attachment;
                    i3 = i5;
                    i2 = i4;
                    regionAttachment2.computeWorldVertices(slot4.getBone(), fArr, 0, 5);
                    b color = regionAttachment2.getColor();
                    b color2 = slot4.getColor();
                    f2 = f8;
                    float f9 = color2.f6937d * f8 * color.f6937d;
                    float f10 = 255.0f;
                    float f11 = f9 * 255.0f;
                    if (z) {
                        f10 = f11;
                        f3 = f10;
                    } else {
                        f3 = f11;
                    }
                    BlendMode blendMode3 = slot4.data.getBlendMode();
                    if (blendMode3 != blendMode2) {
                        if (blendMode3 == BlendMode.additive && z) {
                            blendMode3 = BlendMode.normal;
                            f3 = Animation.CurveTimeline.LINEAR;
                        }
                        slot2 = slot4;
                        bVar2.setBlendFunction(blendMode3.getSource(z), blendMode3.getDest());
                        f4 = f3;
                        blendMode = blendMode3;
                    } else {
                        slot2 = slot4;
                        float f12 = f3;
                        blendMode = blendMode2;
                        f4 = f12;
                    }
                    float a2 = z.a(((int) (color2.f6934a * f5 * color.f6934a * f10)) | (((int) f4) << 24) | (((int) (((color2.f6936c * f7) * color.f6936c) * f10)) << 16) | (((int) (((color2.f6935b * f6) * color.f6935b) * f10)) << 8));
                    float[] uVs = regionAttachment2.getUVs();
                    int i6 = 0;
                    int i7 = 2;
                    while (i6 < 8) {
                        fArr[i7] = a2;
                        fArr[i7 + 1] = uVs[i6];
                        fArr[i7 + 2] = uVs[i6 + 1];
                        i6 += 2;
                        i7 += 5;
                    }
                    if (vertexEffect2 != null) {
                        slot3 = slot2;
                        regionAttachment = regionAttachment2;
                        applyVertexEffect(fArr, 20, 5, a2, Animation.CurveTimeline.LINEAR);
                    } else {
                        regionAttachment = regionAttachment2;
                        slot3 = slot2;
                    }
                    bVar2.draw(regionAttachment.getRegion().e(), fArr, 0, 20);
                    blendMode2 = blendMode;
                    slot = slot3;
                } else {
                    Slot slot5 = slot4;
                    i3 = i5;
                    i2 = i4;
                    f2 = f8;
                    if (attachment instanceof ClippingAttachment) {
                        this.clipper.clipStart(slot5, (ClippingAttachment) attachment);
                        i5 = i3 + 1;
                        i4 = i2;
                        f8 = f2;
                    } else {
                        slot = slot5;
                        if (attachment instanceof MeshAttachment) {
                            throw new RuntimeException(bVar.getClass().getSimpleName() + " cannot render meshes, PolygonSpriteBatch or TwoColorPolygonBatch is required.");
                        } else if ((attachment instanceof SkeletonAttachment) && (skeleton2 = ((SkeletonAttachment) attachment).getSkeleton()) != null) {
                            draw(bVar2, skeleton2);
                        }
                    }
                }
                this.clipper.clipEnd(slot);
                i5 = i3 + 1;
                i4 = i2;
                f8 = f2;
            }
            this.clipper.clipEnd();
            if (vertexEffect2 != null) {
                vertexEffect2.end();
            }
        }
    }

    public boolean getPremultipliedAlpha() {
        return this.premultipliedAlpha;
    }

    public VertexEffect getVertexEffect() {
        return this.vertexEffect;
    }

    public void setPremultipliedAlpha(boolean z) {
        this.premultipliedAlpha = z;
    }

    public void setVertexEffect(VertexEffect vertexEffect2) {
        this.vertexEffect = vertexEffect2;
    }

    public void draw(n nVar, Skeleton skeleton) {
        int i2;
        int i3;
        b bVar;
        int i4;
        short[] sArr;
        float f2;
        float f3;
        float f4;
        a<Slot> aVar;
        int i5;
        float f5;
        int i6;
        VertexEffect vertexEffect2;
        float[] fArr;
        boolean z;
        p pVar;
        p pVar2;
        b bVar2;
        b bVar3;
        b bVar4;
        p pVar3;
        b bVar5;
        float[] fArr2;
        float[] fArr3;
        e.d.b.t.n nVar2;
        b bVar6;
        short[] sArr2;
        int i7;
        Slot slot;
        b bVar7;
        float f6;
        boolean z2;
        float f7;
        b bVar8;
        int i8;
        short[] sArr3;
        int i9;
        float f8;
        BlendMode blendMode;
        Skeleton skeleton2;
        n nVar3 = nVar;
        Skeleton skeleton3 = skeleton;
        p pVar4 = this.temp;
        p pVar5 = this.temp2;
        b bVar9 = this.temp3;
        b bVar10 = this.temp4;
        b bVar11 = this.temp5;
        b bVar12 = this.temp6;
        VertexEffect vertexEffect3 = this.vertexEffect;
        if (vertexEffect3 != null) {
            vertexEffect3.begin(skeleton3);
        }
        boolean z3 = this.premultipliedAlpha;
        b bVar13 = skeleton3.color;
        float f9 = bVar13.f6934a;
        float f10 = bVar13.f6935b;
        float f11 = bVar13.f6936c;
        float f12 = bVar13.f6937d;
        a<Slot> aVar2 = skeleton3.drawOrder;
        int i10 = aVar2.f5374b;
        p pVar6 = pVar5;
        b bVar14 = bVar11;
        b bVar15 = null;
        BlendMode blendMode2 = null;
        float[] fArr4 = null;
        float[] fArr5 = null;
        short[] sArr4 = null;
        int i11 = 0;
        int i12 = 0;
        while (i11 < i10) {
            int i13 = i11;
            Slot slot2 = aVar2.get(i11);
            if (this.clipper.isClipping()) {
                i2 = i10;
                i3 = 2;
            } else {
                i3 = 5;
                i2 = i10;
            }
            Attachment attachment = slot2.attachment;
            a<Slot> aVar3 = aVar2;
            if (attachment instanceof RegionAttachment) {
                RegionAttachment regionAttachment = (RegionAttachment) attachment;
                int i14 = i3 << 2;
                float[] fArr6 = this.vertices.f5527a;
                bVar5 = bVar12;
                pVar3 = pVar4;
                regionAttachment.computeWorldVertices(slot2.getBone(), fArr6, 0, i3);
                short[] sArr5 = quadTriangles;
                nVar2 = regionAttachment.getRegion().e();
                fArr2 = regionAttachment.getUVs();
                short[] sArr6 = sArr5;
                bVar6 = regionAttachment.getColor();
                i7 = i14;
                fArr3 = fArr6;
                sArr2 = sArr6;
            } else {
                bVar5 = bVar12;
                pVar3 = pVar4;
                if (attachment instanceof MeshAttachment) {
                    MeshAttachment meshAttachment = (MeshAttachment) attachment;
                    int worldVerticesLength = meshAttachment.getWorldVerticesLength();
                    int i15 = (worldVerticesLength >> 1) * i3;
                    float[] d2 = this.vertices.d(i15);
                    meshAttachment.computeWorldVertices(slot2, 0, worldVerticesLength, d2, 0, i3);
                    short[] triangles = meshAttachment.getTriangles();
                    e.d.b.t.n e2 = meshAttachment.getRegion().e();
                    fArr2 = meshAttachment.getUVs();
                    float[] fArr7 = d2;
                    bVar6 = meshAttachment.getColor();
                    i7 = i15;
                    sArr2 = triangles;
                    nVar2 = e2;
                    fArr3 = fArr7;
                } else if (attachment instanceof ClippingAttachment) {
                    this.clipper.clipStart(slot2, (ClippingAttachment) attachment);
                    f4 = f12;
                    f3 = f11;
                    f2 = f9;
                    z = z3;
                    bVar3 = bVar10;
                    bVar2 = bVar9;
                    pVar2 = pVar6;
                    bVar4 = bVar14;
                    fArr = fArr4;
                    sArr = sArr4;
                    i4 = i12;
                    i5 = i2;
                    aVar = aVar3;
                    bVar = bVar5;
                    i6 = i13;
                    f5 = f10;
                    vertexEffect2 = vertexEffect3;
                    pVar = pVar3;
                    i11 = i6 + 1;
                    bVar14 = bVar4;
                    bVar10 = bVar3;
                    bVar9 = bVar2;
                    pVar4 = pVar;
                    vertexEffect3 = vertexEffect2;
                    f10 = f5;
                    i10 = i5;
                    aVar2 = aVar;
                    f12 = f4;
                    sArr4 = sArr;
                    i12 = i4;
                    bVar12 = bVar;
                    pVar6 = pVar2;
                    fArr4 = fArr;
                    f11 = f3;
                    z3 = z;
                    f9 = f2;
                } else {
                    if ((attachment instanceof SkeletonAttachment) && (skeleton2 = ((SkeletonAttachment) attachment).getSkeleton()) != null) {
                        draw(nVar3, skeleton2);
                    }
                    nVar2 = null;
                    bVar6 = bVar15;
                    fArr3 = fArr4;
                    fArr2 = fArr5;
                    sArr2 = sArr4;
                    i7 = i12;
                }
            }
            if (nVar2 != null) {
                b color = slot2.getColor();
                b bVar16 = bVar10;
                float f13 = f12;
                float f14 = color.f6937d * f12 * bVar6.f6937d;
                float f15 = 255.0f;
                float f16 = f14 * 255.0f;
                if (z3) {
                    f15 = f16;
                    f6 = f15;
                } else {
                    f6 = f16;
                }
                BlendMode blendMode3 = slot2.data.getBlendMode();
                Slot slot3 = slot2;
                BlendMode blendMode4 = blendMode2;
                if (blendMode3 != blendMode4) {
                    if (blendMode3 != BlendMode.additive || !z3) {
                        blendMode = blendMode3;
                        f8 = f6;
                    } else {
                        blendMode = BlendMode.normal;
                        f8 = Animation.CurveTimeline.LINEAR;
                    }
                    z2 = z3;
                    nVar3.setBlendFunction(blendMode.getSource(z3), blendMode.getDest());
                    f7 = f8;
                    blendMode2 = blendMode;
                } else {
                    z2 = z3;
                    blendMode2 = blendMode4;
                    f7 = f6;
                }
                float a2 = z.a(((int) (color.f6934a * f9 * bVar6.f6934a * f15)) | (((int) f7) << 24) | (((int) (((color.f6936c * f11) * bVar6.f6936c) * f15)) << 16) | (((int) (((color.f6935b * f10) * bVar6.f6935b) * f15)) << 8));
                if (this.clipper.isClipping()) {
                    this.clipper.clipTriangles(fArr3, i7, sArr2, sArr2.length, fArr2, a2, Animation.CurveTimeline.LINEAR, false);
                    m clippedVertices = this.clipper.getClippedVertices();
                    n0 clippedTriangles = this.clipper.getClippedTriangles();
                    if (vertexEffect3 != null) {
                        i9 = i7;
                        i5 = i2;
                        sArr3 = sArr2;
                        aVar = aVar3;
                        f4 = f13;
                        f3 = f11;
                        f5 = f10;
                        f2 = f9;
                        applyVertexEffect(clippedVertices.f5527a, clippedVertices.f5528b, 5, a2, Animation.CurveTimeline.LINEAR);
                    } else {
                        i9 = i7;
                        sArr3 = sArr2;
                        f3 = f11;
                        f5 = f10;
                        f2 = f9;
                        i5 = i2;
                        aVar = aVar3;
                        f4 = f13;
                    }
                    float[] fArr8 = clippedVertices.f5527a;
                    int i16 = clippedVertices.f5528b;
                    short[] sArr7 = clippedTriangles.f5542a;
                    int i17 = clippedTriangles.f5543b;
                    z = z2;
                    bVar7 = bVar6;
                    float[] fArr9 = fArr8;
                    bVar4 = bVar14;
                    slot = slot3;
                    i6 = i13;
                    bVar3 = bVar16;
                    int i18 = i16;
                    bVar2 = bVar9;
                    short[] sArr8 = sArr7;
                    pVar2 = pVar6;
                    pVar = pVar3;
                    nVar.a(nVar2, fArr9, 0, i18, sArr8, 0, i17);
                    i4 = i9;
                    sArr = sArr3;
                    vertexEffect2 = vertexEffect3;
                    bVar = bVar5;
                } else {
                    int i19 = i7;
                    short[] sArr9 = sArr2;
                    f3 = f11;
                    f5 = f10;
                    f2 = f9;
                    VertexEffect vertexEffect4 = vertexEffect3;
                    bVar7 = bVar6;
                    bVar2 = bVar9;
                    pVar2 = pVar6;
                    bVar4 = bVar14;
                    bVar3 = bVar16;
                    i5 = i2;
                    aVar = aVar3;
                    b bVar17 = bVar5;
                    pVar = pVar3;
                    f4 = f13;
                    slot = slot3;
                    z = z2;
                    i6 = i13;
                    if (vertexEffect4 != null) {
                        bVar4.a(z.b(a2));
                        bVar8 = bVar17;
                        bVar8.a(0);
                        i8 = i19;
                        int i20 = 0;
                        int i21 = 0;
                        while (i20 < i8) {
                            pVar.f5294a = fArr3[i20];
                            int i22 = i20 + 1;
                            pVar.f5295b = fArr3[i22];
                            bVar2.b(bVar4);
                            bVar3.b(bVar8);
                            pVar2.f5294a = fArr2[i21];
                            pVar2.f5295b = fArr2[i21 + 1];
                            vertexEffect4.transform(pVar, pVar2, bVar2, bVar3);
                            fArr3[i20] = pVar.f5294a;
                            fArr3[i22] = pVar.f5295b;
                            fArr3[i20 + 2] = bVar2.b();
                            fArr3[i20 + 3] = pVar2.f5294a;
                            fArr3[i20 + 4] = pVar2.f5295b;
                            i20 += 5;
                            i21 += 2;
                        }
                    } else {
                        i8 = i19;
                        bVar8 = bVar17;
                        int i23 = 2;
                        int i24 = 0;
                        while (i23 < i8) {
                            fArr3[i23] = a2;
                            fArr3[i23 + 1] = fArr2[i24];
                            fArr3[i23 + 2] = fArr2[i24 + 1];
                            i23 += 5;
                            i24 += 2;
                        }
                    }
                    short[] sArr10 = sArr9;
                    sArr = sArr10;
                    i4 = i8;
                    bVar = bVar8;
                    vertexEffect2 = vertexEffect4;
                    nVar.a(nVar2, fArr3, 0, i4, sArr, 0, sArr10.length);
                }
            } else {
                i4 = i7;
                sArr = sArr2;
                f4 = f12;
                f3 = f11;
                f5 = f10;
                f2 = f9;
                z = z3;
                bVar7 = bVar6;
                slot = slot2;
                bVar3 = bVar10;
                bVar2 = bVar9;
                pVar2 = pVar6;
                bVar4 = bVar14;
                i5 = i2;
                aVar = aVar3;
                bVar = bVar5;
                pVar = pVar3;
                i6 = i13;
                vertexEffect2 = vertexEffect3;
            }
            this.clipper.clipEnd(slot);
            fArr = fArr3;
            bVar15 = bVar7;
            fArr5 = fArr2;
            i11 = i6 + 1;
            bVar14 = bVar4;
            bVar10 = bVar3;
            bVar9 = bVar2;
            pVar4 = pVar;
            vertexEffect3 = vertexEffect2;
            f10 = f5;
            i10 = i5;
            aVar2 = aVar;
            f12 = f4;
            sArr4 = sArr;
            i12 = i4;
            bVar12 = bVar;
            pVar6 = pVar2;
            fArr4 = fArr;
            f11 = f3;
            z3 = z;
            f9 = f2;
        }
        VertexEffect vertexEffect5 = vertexEffect3;
        this.clipper.clipEnd();
        if (vertexEffect5 != null) {
            vertexEffect5.end();
        }
    }

    public void draw(TwoColorPolygonBatch twoColorPolygonBatch, Skeleton skeleton) {
        int i2;
        int i3;
        b bVar;
        short[] sArr;
        float[] fArr;
        int i4;
        p pVar;
        float f2;
        float f3;
        float f4;
        a<Slot> aVar;
        int i5;
        float f5;
        int i6;
        float[] fArr2;
        b bVar2;
        VertexEffect vertexEffect2;
        boolean z;
        p pVar2;
        b bVar3;
        b bVar4;
        p pVar3;
        b bVar5;
        float[] fArr3;
        e.d.b.t.n nVar;
        b bVar6;
        int i7;
        short[] sArr2;
        Slot slot;
        BlendMode blendMode;
        b bVar7;
        float f6;
        boolean z2;
        float f7;
        int i8;
        b bVar8;
        int i9;
        short[] sArr3;
        Skeleton skeleton2;
        TwoColorPolygonBatch twoColorPolygonBatch2 = twoColorPolygonBatch;
        Skeleton skeleton3 = skeleton;
        p pVar4 = this.temp;
        p pVar5 = this.temp2;
        b bVar9 = this.temp3;
        b bVar10 = this.temp4;
        b bVar11 = this.temp5;
        b bVar12 = this.temp6;
        VertexEffect vertexEffect3 = this.vertexEffect;
        if (vertexEffect3 != null) {
            vertexEffect3.begin(skeleton3);
        }
        boolean z3 = this.premultipliedAlpha;
        twoColorPolygonBatch2.setPremultipliedAlpha(z3);
        b bVar13 = skeleton3.color;
        float f8 = bVar13.f6934a;
        float f9 = bVar13.f6935b;
        float f10 = bVar13.f6936c;
        float f11 = bVar13.f6937d;
        a<Slot> aVar2 = skeleton3.drawOrder;
        int i10 = aVar2.f5374b;
        p pVar6 = pVar5;
        b bVar14 = bVar11;
        b bVar15 = null;
        BlendMode blendMode2 = null;
        float[] fArr4 = null;
        short[] sArr4 = null;
        float[] fArr5 = null;
        int i11 = 0;
        int i12 = 0;
        while (i11 < i10) {
            int i13 = i11;
            Slot slot2 = aVar2.get(i11);
            if (this.clipper.isClipping()) {
                i2 = i10;
                i3 = 2;
            } else {
                i3 = 6;
                i2 = i10;
            }
            Attachment attachment = slot2.attachment;
            a<Slot> aVar3 = aVar2;
            if (attachment instanceof RegionAttachment) {
                RegionAttachment regionAttachment = (RegionAttachment) attachment;
                int i14 = i3 << 2;
                float[] fArr6 = this.vertices.f5527a;
                bVar5 = bVar12;
                pVar3 = pVar4;
                regionAttachment.computeWorldVertices(slot2.getBone(), fArr6, 0, i3);
                short[] sArr5 = quadTriangles;
                nVar = regionAttachment.getRegion().e();
                fArr = regionAttachment.getUVs();
                short[] sArr6 = sArr5;
                bVar6 = regionAttachment.getColor();
                sArr2 = sArr6;
                int i15 = i14;
                fArr3 = fArr6;
                i7 = i15;
            } else {
                bVar5 = bVar12;
                pVar3 = pVar4;
                if (attachment instanceof MeshAttachment) {
                    MeshAttachment meshAttachment = (MeshAttachment) attachment;
                    int worldVerticesLength = meshAttachment.getWorldVerticesLength();
                    i7 = (worldVerticesLength >> 1) * i3;
                    float[] d2 = this.vertices.d(i7);
                    meshAttachment.computeWorldVertices(slot2, 0, worldVerticesLength, d2, 0, i3);
                    short[] triangles = meshAttachment.getTriangles();
                    e.d.b.t.n e2 = meshAttachment.getRegion().e();
                    fArr = meshAttachment.getUVs();
                    float[] fArr7 = d2;
                    bVar6 = meshAttachment.getColor();
                    sArr2 = triangles;
                    nVar = e2;
                    fArr3 = fArr7;
                } else if (attachment instanceof ClippingAttachment) {
                    this.clipper.clipStart(slot2, (ClippingAttachment) attachment);
                    f3 = f10;
                    f2 = f8;
                    z = z3;
                    bVar3 = bVar10;
                    bVar = bVar9;
                    pVar = pVar6;
                    bVar4 = bVar14;
                    fArr2 = fArr4;
                    i4 = i12;
                    sArr = sArr4;
                    fArr = fArr5;
                    i5 = i2;
                    aVar = aVar3;
                    bVar2 = bVar5;
                    i6 = i13;
                    f4 = f11;
                    f5 = f9;
                    vertexEffect2 = vertexEffect3;
                    pVar2 = pVar3;
                    i11 = i6 + 1;
                    bVar14 = bVar4;
                    bVar10 = bVar3;
                    pVar4 = pVar2;
                    vertexEffect3 = vertexEffect2;
                    bVar12 = bVar2;
                    f9 = f5;
                    i10 = i5;
                    aVar2 = aVar;
                    f11 = f4;
                    f8 = f2;
                    pVar6 = pVar;
                    i12 = i4;
                    fArr5 = fArr;
                    sArr4 = sArr;
                    bVar9 = bVar;
                    fArr4 = fArr2;
                    z3 = z;
                    f10 = f3;
                } else {
                    if ((attachment instanceof SkeletonAttachment) && (skeleton2 = ((SkeletonAttachment) attachment).getSkeleton()) != null) {
                        draw(twoColorPolygonBatch2, skeleton2);
                    }
                    nVar = null;
                    bVar6 = bVar15;
                    fArr3 = fArr4;
                    i7 = i12;
                    sArr2 = sArr4;
                    fArr = fArr5;
                }
            }
            if (nVar != null) {
                b color = slot2.getColor();
                b bVar16 = bVar10;
                float f12 = f11;
                float f13 = color.f6937d * f11 * bVar6.f6937d;
                float f14 = 255.0f;
                float f15 = f13 * 255.0f;
                if (z3) {
                    f14 = f15;
                    f6 = f14;
                } else {
                    f6 = f15;
                }
                BlendMode blendMode3 = slot2.data.getBlendMode();
                b bVar17 = bVar9;
                BlendMode blendMode4 = blendMode2;
                if (blendMode3 != blendMode4) {
                    if (blendMode3 == BlendMode.additive && z3) {
                        blendMode3 = BlendMode.normal;
                        f6 = Animation.CurveTimeline.LINEAR;
                    }
                    z2 = z3;
                    twoColorPolygonBatch2.setBlendFunction(blendMode3.getSource(z3), blendMode3.getDest());
                    blendMode = blendMode3;
                } else {
                    z2 = z3;
                    blendMode = blendMode4;
                }
                float f16 = bVar6.f6934a * f8 * f14;
                float f17 = bVar6.f6935b * f9 * f14;
                float f18 = f9;
                float f19 = bVar6.f6936c * f10 * f14;
                float a2 = z.a((((int) f6) << 24) | (((int) (color.f6936c * f19)) << 16) | (((int) (color.f6935b * f17)) << 8) | ((int) (color.f6934a * f16)));
                b darkColor = slot2.getDarkColor();
                if (darkColor == null) {
                    f7 = Animation.CurveTimeline.LINEAR;
                } else {
                    f7 = z.a(((int) (f16 * darkColor.f6934a)) | (((int) (f19 * darkColor.f6936c)) << 16) | (((int) (f17 * darkColor.f6935b)) << 8));
                }
                if (this.clipper.isClipping()) {
                    this.clipper.clipTriangles(fArr3, i7, sArr2, sArr2.length, fArr, a2, f7, true);
                    m clippedVertices = this.clipper.getClippedVertices();
                    n0 clippedTriangles = this.clipper.getClippedTriangles();
                    if (vertexEffect3 != null) {
                        sArr3 = sArr2;
                        i5 = i2;
                        i9 = i7;
                        aVar = aVar3;
                        f4 = f12;
                        f3 = f10;
                        f5 = f18;
                        f2 = f8;
                        applyVertexEffect(clippedVertices.f5527a, clippedVertices.f5528b, 6, a2, f7);
                    } else {
                        sArr3 = sArr2;
                        i9 = i7;
                        f3 = f10;
                        f2 = f8;
                        i5 = i2;
                        aVar = aVar3;
                        f4 = f12;
                        f5 = f18;
                    }
                    z = z2;
                    vertexEffect2 = vertexEffect3;
                    bVar7 = bVar6;
                    bVar2 = bVar5;
                    bVar4 = bVar14;
                    i6 = i13;
                    bVar3 = bVar16;
                    slot = slot2;
                    pVar2 = pVar3;
                    twoColorPolygonBatch.drawTwoColor(nVar, clippedVertices.f5527a, 0, clippedVertices.f5528b, clippedTriangles.f5542a, 0, clippedTriangles.f5543b);
                    sArr = sArr3;
                    i4 = i9;
                    bVar = bVar17;
                    pVar = pVar6;
                } else {
                    short[] sArr7 = sArr2;
                    int i16 = i7;
                    f3 = f10;
                    f2 = f8;
                    vertexEffect2 = vertexEffect3;
                    slot = slot2;
                    p pVar7 = pVar6;
                    bVar4 = bVar14;
                    z = z2;
                    bVar3 = bVar16;
                    i5 = i2;
                    aVar = aVar3;
                    bVar2 = bVar5;
                    pVar2 = pVar3;
                    f4 = f12;
                    b bVar18 = bVar17;
                    f5 = f18;
                    i6 = i13;
                    bVar7 = bVar6;
                    if (vertexEffect2 != null) {
                        bVar4.a(z.b(a2));
                        bVar2.a(z.b(f7));
                        i8 = i16;
                        int i17 = 0;
                        int i18 = 0;
                        while (i17 < i8) {
                            pVar2.f5294a = fArr3[i17];
                            int i19 = i17 + 1;
                            pVar2.f5295b = fArr3[i19];
                            b bVar19 = bVar18;
                            bVar19.b(bVar4);
                            bVar3.b(bVar2);
                            p pVar8 = pVar7;
                            pVar8.f5294a = fArr[i18];
                            pVar8.f5295b = fArr[i18 + 1];
                            vertexEffect2.transform(pVar2, pVar8, bVar19, bVar3);
                            fArr3[i17] = pVar2.f5294a;
                            fArr3[i19] = pVar2.f5295b;
                            fArr3[i17 + 2] = bVar19.b();
                            fArr3[i17 + 3] = bVar3.b();
                            fArr3[i17 + 4] = pVar8.f5294a;
                            fArr3[i17 + 5] = pVar8.f5295b;
                            i17 += 6;
                            i18 += 2;
                        }
                        bVar8 = bVar18;
                        pVar = pVar7;
                    } else {
                        i8 = i16;
                        bVar8 = bVar18;
                        pVar = pVar7;
                        int i20 = 2;
                        int i21 = 0;
                        while (i20 < i8) {
                            fArr3[i20] = a2;
                            fArr3[i20 + 1] = f7;
                            fArr3[i20 + 2] = fArr[i21];
                            fArr3[i20 + 3] = fArr[i21 + 1];
                            i20 += 6;
                            i21 += 2;
                        }
                    }
                    short[] sArr8 = sArr7;
                    sArr = sArr8;
                    bVar = bVar8;
                    i4 = i8;
                    twoColorPolygonBatch.drawTwoColor(nVar, fArr3, 0, i8, sArr, 0, sArr8.length);
                }
            } else {
                sArr = sArr2;
                i4 = i7;
                f4 = f11;
                f3 = f10;
                f5 = f9;
                f2 = f8;
                z = z3;
                vertexEffect2 = vertexEffect3;
                slot = slot2;
                bVar3 = bVar10;
                bVar = bVar9;
                pVar = pVar6;
                bVar4 = bVar14;
                BlendMode blendMode5 = blendMode2;
                i5 = i2;
                aVar = aVar3;
                bVar2 = bVar5;
                pVar2 = pVar3;
                i6 = i13;
                bVar7 = bVar6;
                blendMode = blendMode5;
            }
            this.clipper.clipEnd(slot);
            fArr2 = fArr3;
            bVar15 = bVar7;
            blendMode2 = blendMode;
            i11 = i6 + 1;
            bVar14 = bVar4;
            bVar10 = bVar3;
            pVar4 = pVar2;
            vertexEffect3 = vertexEffect2;
            bVar12 = bVar2;
            f9 = f5;
            i10 = i5;
            aVar2 = aVar;
            f11 = f4;
            f8 = f2;
            pVar6 = pVar;
            i12 = i4;
            fArr5 = fArr;
            sArr4 = sArr;
            bVar9 = bVar;
            fArr4 = fArr2;
            z3 = z;
            f10 = f3;
        }
        VertexEffect vertexEffect4 = vertexEffect3;
        this.clipper.clipEnd();
        if (vertexEffect4 != null) {
            vertexEffect4.end();
        }
    }
}
