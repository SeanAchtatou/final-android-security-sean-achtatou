package com.esotericsoftware.spine;

import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.f0;
import com.badlogic.gdx.utils.m;
import com.badlogic.gdx.utils.q;
import com.badlogic.gdx.utils.s;
import com.esotericsoftware.spine.Animation;

public class AnimationState {
    private static final int FIRST = 1;
    private static final int HOLD = 2;
    private static final int HOLD_MIX = 3;
    private static final int SUBSEQUENT = 0;
    private static final Animation emptyAnimation = new Animation("<empty>", new a(0), Animation.CurveTimeline.LINEAR);
    boolean animationsChanged;
    private AnimationStateData data;
    private final a<Event> events = new a<>();
    final a<AnimationStateListener> listeners = new a<>();
    private final s propertyIDs = new s();
    private final EventQueue queue = new EventQueue();
    private float timeScale = 1.0f;
    final f0<TrackEntry> trackEntryPool = new f0() {
        /* access modifiers changed from: protected */
        public Object newObject() {
            return new TrackEntry();
        }
    };
    final a<TrackEntry> tracks = new a<>();

    /* renamed from: com.esotericsoftware.spine.AnimationState$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$com$esotericsoftware$spine$AnimationState$EventType = new int[EventType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.esotericsoftware.spine.AnimationState$EventType[] r0 = com.esotericsoftware.spine.AnimationState.EventType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.esotericsoftware.spine.AnimationState.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$AnimationState$EventType = r0
                int[] r0 = com.esotericsoftware.spine.AnimationState.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$AnimationState$EventType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.esotericsoftware.spine.AnimationState$EventType r1 = com.esotericsoftware.spine.AnimationState.EventType.start     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.esotericsoftware.spine.AnimationState.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$AnimationState$EventType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.esotericsoftware.spine.AnimationState$EventType r1 = com.esotericsoftware.spine.AnimationState.EventType.interrupt     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.esotericsoftware.spine.AnimationState.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$AnimationState$EventType     // Catch:{ NoSuchFieldError -> 0x002a }
                com.esotericsoftware.spine.AnimationState$EventType r1 = com.esotericsoftware.spine.AnimationState.EventType.end     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.esotericsoftware.spine.AnimationState.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$AnimationState$EventType     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.esotericsoftware.spine.AnimationState$EventType r1 = com.esotericsoftware.spine.AnimationState.EventType.dispose     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.esotericsoftware.spine.AnimationState.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$AnimationState$EventType     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.esotericsoftware.spine.AnimationState$EventType r1 = com.esotericsoftware.spine.AnimationState.EventType.complete     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = com.esotericsoftware.spine.AnimationState.AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$AnimationState$EventType     // Catch:{ NoSuchFieldError -> 0x004b }
                com.esotericsoftware.spine.AnimationState$EventType r1 = com.esotericsoftware.spine.AnimationState.EventType.event     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.AnimationState.AnonymousClass2.<clinit>():void");
        }
    }

    public static abstract class AnimationStateAdapter implements AnimationStateListener {
        public void complete(TrackEntry trackEntry) {
        }

        public void dispose(TrackEntry trackEntry) {
        }

        public void end(TrackEntry trackEntry) {
        }

        public void event(TrackEntry trackEntry, Event event) {
        }

        public void interrupt(TrackEntry trackEntry) {
        }

        public void start(TrackEntry trackEntry) {
        }
    }

    public interface AnimationStateListener {
        void complete(TrackEntry trackEntry);

        void dispose(TrackEntry trackEntry);

        void end(TrackEntry trackEntry);

        void event(TrackEntry trackEntry, Event event);

        void interrupt(TrackEntry trackEntry);

        void start(TrackEntry trackEntry);
    }

    class EventQueue {
        boolean drainDisabled;
        private final a objects = new a();

        EventQueue() {
        }

        public void clear() {
            this.objects.clear();
        }

        public void complete(TrackEntry trackEntry) {
            this.objects.add(EventType.complete);
            this.objects.add(trackEntry);
        }

        public void dispose(TrackEntry trackEntry) {
            this.objects.add(EventType.dispose);
            this.objects.add(trackEntry);
        }

        public void drain() {
            if (!this.drainDisabled) {
                this.drainDisabled = true;
                a aVar = this.objects;
                a<AnimationStateListener> aVar2 = AnimationState.this.listeners;
                int i2 = 0;
                while (i2 < aVar.f5374b) {
                    int i3 = i2 + 1;
                    TrackEntry trackEntry = (TrackEntry) aVar.get(i3);
                    switch (AnonymousClass2.$SwitchMap$com$esotericsoftware$spine$AnimationState$EventType[((EventType) aVar.get(i2)).ordinal()]) {
                        case 1:
                            AnimationStateListener animationStateListener = trackEntry.listener;
                            if (animationStateListener != null) {
                                animationStateListener.start(trackEntry);
                            }
                            for (int i4 = 0; i4 < aVar2.f5374b; i4++) {
                                aVar2.get(i4).start(trackEntry);
                            }
                            continue;
                            i2 += 2;
                        case 2:
                            AnimationStateListener animationStateListener2 = trackEntry.listener;
                            if (animationStateListener2 != null) {
                                animationStateListener2.interrupt(trackEntry);
                            }
                            for (int i5 = 0; i5 < aVar2.f5374b; i5++) {
                                aVar2.get(i5).interrupt(trackEntry);
                            }
                            continue;
                            i2 += 2;
                        case 3:
                            AnimationStateListener animationStateListener3 = trackEntry.listener;
                            if (animationStateListener3 != null) {
                                animationStateListener3.end(trackEntry);
                            }
                            for (int i6 = 0; i6 < aVar2.f5374b; i6++) {
                                aVar2.get(i6).end(trackEntry);
                            }
                            break;
                        case 4:
                            break;
                        case 5:
                            AnimationStateListener animationStateListener4 = trackEntry.listener;
                            if (animationStateListener4 != null) {
                                animationStateListener4.complete(trackEntry);
                            }
                            for (int i7 = 0; i7 < aVar2.f5374b; i7++) {
                                aVar2.get(i7).complete(trackEntry);
                            }
                            continue;
                            i2 += 2;
                        case 6:
                            Event event = (Event) aVar.get(i2 + 2);
                            AnimationStateListener animationStateListener5 = trackEntry.listener;
                            if (animationStateListener5 != null) {
                                animationStateListener5.event(trackEntry, event);
                            }
                            for (int i8 = 0; i8 < aVar2.f5374b; i8++) {
                                aVar2.get(i8).event(trackEntry, event);
                            }
                            i2 = i3;
                            continue;
                            i2 += 2;
                        default:
                            i2 += 2;
                    }
                    AnimationStateListener animationStateListener6 = trackEntry.listener;
                    if (animationStateListener6 != null) {
                        animationStateListener6.dispose(trackEntry);
                    }
                    for (int i9 = 0; i9 < aVar2.f5374b; i9++) {
                        aVar2.get(i9).dispose(trackEntry);
                    }
                    AnimationState.this.trackEntryPool.free(trackEntry);
                    i2 += 2;
                }
                clear();
                this.drainDisabled = false;
            }
        }

        public void end(TrackEntry trackEntry) {
            this.objects.add(EventType.end);
            this.objects.add(trackEntry);
            AnimationState.this.animationsChanged = true;
        }

        public void event(TrackEntry trackEntry, Event event) {
            this.objects.add(EventType.event);
            this.objects.add(trackEntry);
            this.objects.add(event);
        }

        public void interrupt(TrackEntry trackEntry) {
            this.objects.add(EventType.interrupt);
            this.objects.add(trackEntry);
        }

        public void start(TrackEntry trackEntry) {
            this.objects.add(EventType.start);
            this.objects.add(trackEntry);
            AnimationState.this.animationsChanged = true;
        }
    }

    private enum EventType {
        start,
        interrupt,
        end,
        dispose,
        complete,
        event
    }

    public static class TrackEntry implements f0.a {
        float alpha;
        Animation animation;
        float animationEnd;
        float animationLast;
        float animationStart;
        float attachmentThreshold;
        float delay;
        float drawOrderThreshold;
        float eventThreshold;
        boolean holdPrevious;
        float interruptAlpha;
        AnimationStateListener listener;
        boolean loop;
        Animation.MixBlend mixBlend = Animation.MixBlend.replace;
        float mixDuration;
        float mixTime;
        TrackEntry mixingFrom;
        TrackEntry mixingTo;
        TrackEntry next;
        float nextAnimationLast;
        float nextTrackLast;
        float timeScale;
        final a<TrackEntry> timelineHoldMix = new a<>();
        final q timelineMode = new q();
        final m timelinesRotation = new m();
        float totalAlpha;
        float trackEnd;
        int trackIndex;
        float trackLast;
        float trackTime;

        public float getAlpha() {
            return this.alpha;
        }

        public Animation getAnimation() {
            return this.animation;
        }

        public float getAnimationEnd() {
            return this.animationEnd;
        }

        public float getAnimationLast() {
            return this.animationLast;
        }

        public float getAnimationStart() {
            return this.animationStart;
        }

        public float getAnimationTime() {
            if (!this.loop) {
                return Math.min(this.trackTime + this.animationStart, this.animationEnd);
            }
            float f2 = this.animationEnd;
            float f3 = this.animationStart;
            float f4 = f2 - f3;
            if (f4 == Animation.CurveTimeline.LINEAR) {
                return f3;
            }
            return (this.trackTime % f4) + f3;
        }

        public float getAttachmentThreshold() {
            return this.attachmentThreshold;
        }

        public float getDelay() {
            return this.delay;
        }

        public float getDrawOrderThreshold() {
            return this.drawOrderThreshold;
        }

        public float getEventThreshold() {
            return this.eventThreshold;
        }

        public boolean getHoldPrevious() {
            return this.holdPrevious;
        }

        public AnimationStateListener getListener() {
            return this.listener;
        }

        public boolean getLoop() {
            return this.loop;
        }

        public Animation.MixBlend getMixBlend() {
            return this.mixBlend;
        }

        public float getMixDuration() {
            return this.mixDuration;
        }

        public float getMixTime() {
            return this.mixTime;
        }

        public TrackEntry getMixingFrom() {
            return this.mixingFrom;
        }

        public TrackEntry getMixingTo() {
            return this.mixingTo;
        }

        public TrackEntry getNext() {
            return this.next;
        }

        public float getTimeScale() {
            return this.timeScale;
        }

        public float getTrackEnd() {
            return this.trackEnd;
        }

        public int getTrackIndex() {
            return this.trackIndex;
        }

        public float getTrackTime() {
            return this.trackTime;
        }

        public boolean isComplete() {
            return this.trackTime >= this.animationEnd - this.animationStart;
        }

        public void reset() {
            this.next = null;
            this.mixingFrom = null;
            this.mixingTo = null;
            this.animation = null;
            this.listener = null;
            this.timelineMode.a();
            this.timelineHoldMix.clear();
            this.timelinesRotation.a();
        }

        public void resetRotationDirections() {
            this.timelinesRotation.a();
        }

        public void setAlpha(float f2) {
            this.alpha = f2;
        }

        public void setAnimation(Animation animation2) {
            this.animation = animation2;
        }

        public void setAnimationEnd(float f2) {
            this.animationEnd = f2;
        }

        public void setAnimationLast(float f2) {
            this.animationLast = f2;
            this.nextAnimationLast = f2;
        }

        public void setAnimationStart(float f2) {
            this.animationStart = f2;
        }

        public void setAttachmentThreshold(float f2) {
            this.attachmentThreshold = f2;
        }

        public void setDelay(float f2) {
            this.delay = f2;
        }

        public void setDrawOrderThreshold(float f2) {
            this.drawOrderThreshold = f2;
        }

        public void setEventThreshold(float f2) {
            this.eventThreshold = f2;
        }

        public void setHoldPrevious(boolean z) {
            this.holdPrevious = z;
        }

        public void setListener(AnimationStateListener animationStateListener) {
            this.listener = animationStateListener;
        }

        public void setLoop(boolean z) {
            this.loop = z;
        }

        public void setMixBlend(Animation.MixBlend mixBlend2) {
            this.mixBlend = mixBlend2;
        }

        public void setMixDuration(float f2) {
            this.mixDuration = f2;
        }

        public void setMixTime(float f2) {
            this.mixTime = f2;
        }

        public void setTimeScale(float f2) {
            this.timeScale = f2;
        }

        public void setTrackEnd(float f2) {
            this.trackEnd = f2;
        }

        public void setTrackTime(float f2) {
            this.trackTime = f2;
        }

        public String toString() {
            Animation animation2 = this.animation;
            return animation2 == null ? "<none>" : animation2.name;
        }
    }

    public AnimationState() {
    }

    private void animationsChanged() {
        this.animationsChanged = false;
        this.propertyIDs.b(2048);
        int i2 = this.tracks.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            TrackEntry trackEntry = this.tracks.get(i3);
            if (trackEntry != null) {
                while (true) {
                    TrackEntry trackEntry2 = trackEntry.mixingFrom;
                    if (trackEntry2 == null) {
                        break;
                    }
                    trackEntry = trackEntry2;
                }
                do {
                    if (trackEntry.mixingTo == null || trackEntry.mixBlend != Animation.MixBlend.add) {
                        setTimelineModes(trackEntry);
                    }
                    trackEntry = trackEntry.mixingTo;
                } while (trackEntry != null);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:61:0x0123  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x014a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private float applyMixingFrom(com.esotericsoftware.spine.AnimationState.TrackEntry r36, com.esotericsoftware.spine.Skeleton r37, com.esotericsoftware.spine.Animation.MixBlend r38) {
        /*
            r35 = this;
            r9 = r35
            r10 = r36
            r0 = r38
            com.esotericsoftware.spine.AnimationState$TrackEntry r11 = r10.mixingFrom
            com.esotericsoftware.spine.AnimationState$TrackEntry r1 = r11.mixingFrom
            r8 = r37
            if (r1 == 0) goto L_0x0011
            r9.applyMixingFrom(r11, r8, r0)
        L_0x0011:
            float r1 = r10.mixDuration
            r7 = 0
            r20 = 1065353216(0x3f800000, float:1.0)
            int r2 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r2 != 0) goto L_0x0024
            com.esotericsoftware.spine.Animation$MixBlend r1 = com.esotericsoftware.spine.Animation.MixBlend.first
            if (r0 != r1) goto L_0x0020
            com.esotericsoftware.spine.Animation$MixBlend r0 = com.esotericsoftware.spine.Animation.MixBlend.setup
        L_0x0020:
            r6 = r0
            r21 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0036
        L_0x0024:
            float r2 = r10.mixTime
            float r2 = r2 / r1
            int r1 = (r2 > r20 ? 1 : (r2 == r20 ? 0 : -1))
            if (r1 <= 0) goto L_0x002d
            r2 = 1065353216(0x3f800000, float:1.0)
        L_0x002d:
            com.esotericsoftware.spine.Animation$MixBlend r1 = com.esotericsoftware.spine.Animation.MixBlend.first
            if (r0 == r1) goto L_0x0033
            com.esotericsoftware.spine.Animation$MixBlend r0 = r11.mixBlend
        L_0x0033:
            r6 = r0
            r21 = r2
        L_0x0036:
            float r0 = r11.eventThreshold
            int r0 = (r21 > r0 ? 1 : (r21 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x003f
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Event> r0 = r9.events
            goto L_0x0040
        L_0x003f:
            r0 = 0
        L_0x0040:
            r22 = r0
            float r0 = r11.attachmentThreshold
            r1 = 0
            r15 = 1
            int r0 = (r21 > r0 ? 1 : (r21 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x004d
            r23 = 1
            goto L_0x004f
        L_0x004d:
            r23 = 0
        L_0x004f:
            float r0 = r11.drawOrderThreshold
            int r0 = (r21 > r0 ? 1 : (r21 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x0058
            r24 = 1
            goto L_0x005a
        L_0x0058:
            r24 = 0
        L_0x005a:
            float r5 = r11.animationLast
            float r4 = r11.getAnimationTime()
            com.esotericsoftware.spine.Animation r0 = r11.animation
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Animation$Timeline> r0 = r0.timelines
            int r3 = r0.f5374b
            T[] r2 = r0.f5373a
            float r0 = r11.alpha
            float r12 = r10.interruptAlpha
            float r25 = r0 * r12
            float r0 = r20 - r21
            float r26 = r25 * r0
            com.esotericsoftware.spine.Animation$MixBlend r0 = com.esotericsoftware.spine.Animation.MixBlend.add
            if (r6 != r0) goto L_0x0095
        L_0x0076:
            if (r1 >= r3) goto L_0x008f
            r0 = r2[r1]
            r12 = r0
            com.esotericsoftware.spine.Animation$Timeline r12 = (com.esotericsoftware.spine.Animation.Timeline) r12
            com.esotericsoftware.spine.Animation$MixDirection r19 = com.esotericsoftware.spine.Animation.MixDirection.out
            r13 = r37
            r14 = r5
            r15 = r4
            r16 = r22
            r17 = r26
            r18 = r6
            r12.apply(r13, r14, r15, r16, r17, r18, r19)
            int r1 = r1 + 1
            goto L_0x0076
        L_0x008f:
            r31 = r4
            r34 = 0
            goto L_0x019c
        L_0x0095:
            com.badlogic.gdx.utils.q r0 = r11.timelineMode
            int[] r14 = r0.f5558a
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.AnimationState$TrackEntry> r0 = r11.timelineHoldMix
            T[] r13 = r0.f5373a
            com.badlogic.gdx.utils.m r0 = r11.timelinesRotation
            int r0 = r0.f5528b
            if (r0 != 0) goto L_0x00a6
            r27 = 1
            goto L_0x00a8
        L_0x00a6:
            r27 = 0
        L_0x00a8:
            if (r27 == 0) goto L_0x00b1
            com.badlogic.gdx.utils.m r0 = r11.timelinesRotation
            int r12 = r3 << 1
            r0.d(r12)
        L_0x00b1:
            com.badlogic.gdx.utils.m r0 = r11.timelinesRotation
            float[] r12 = r0.f5527a
            r11.totalAlpha = r7
        L_0x00b7:
            if (r1 >= r3) goto L_0x008f
            r0 = r2[r1]
            com.esotericsoftware.spine.Animation$Timeline r0 = (com.esotericsoftware.spine.Animation.Timeline) r0
            com.esotericsoftware.spine.Animation$MixDirection r16 = com.esotericsoftware.spine.Animation.MixDirection.out
            r7 = r14[r1]
            if (r7 == 0) goto L_0x00f3
            if (r7 == r15) goto L_0x00ea
            r15 = 2
            if (r7 == r15) goto L_0x00e1
            com.esotericsoftware.spine.Animation$MixBlend r7 = com.esotericsoftware.spine.Animation.MixBlend.setup
            r15 = r13[r1]
            com.esotericsoftware.spine.AnimationState$TrackEntry r15 = (com.esotericsoftware.spine.AnimationState.TrackEntry) r15
            r18 = r2
            float r2 = r15.mixTime
            float r15 = r15.mixDuration
            float r2 = r2 / r15
            float r2 = r20 - r2
            r15 = 0
            float r2 = java.lang.Math.max(r15, r2)
            float r2 = r2 * r25
            r17 = r2
            goto L_0x0119
        L_0x00e1:
            r18 = r2
            r15 = 0
            com.esotericsoftware.spine.Animation$MixBlend r2 = com.esotericsoftware.spine.Animation.MixBlend.setup
            r7 = r2
            r17 = r25
            goto L_0x0119
        L_0x00ea:
            r18 = r2
            r15 = 0
            com.esotericsoftware.spine.Animation$MixBlend r2 = com.esotericsoftware.spine.Animation.MixBlend.setup
            r7 = r2
        L_0x00f0:
            r17 = r26
            goto L_0x0119
        L_0x00f3:
            r18 = r2
            r15 = 0
            if (r23 != 0) goto L_0x0110
            boolean r2 = r0 instanceof com.esotericsoftware.spine.Animation.AttachmentTimeline
            if (r2 == 0) goto L_0x0110
        L_0x00fc:
            r28 = r1
            r30 = r3
            r31 = r4
            r32 = r5
            r33 = r6
            r0 = r12
            r1 = r13
            r2 = r14
            r29 = r18
            r3 = 1
            r34 = 0
            goto L_0x0186
        L_0x0110:
            if (r24 != 0) goto L_0x0117
            boolean r2 = r0 instanceof com.esotericsoftware.spine.Animation.DrawOrderTimeline
            if (r2 == 0) goto L_0x0117
            goto L_0x00fc
        L_0x0117:
            r7 = r6
            goto L_0x00f0
        L_0x0119:
            float r2 = r11.totalAlpha
            float r2 = r2 + r17
            r11.totalAlpha = r2
            boolean r2 = r0 instanceof com.esotericsoftware.spine.Animation.RotateTimeline
            if (r2 == 0) goto L_0x014a
            int r16 = r1 << 1
            r2 = r0
            r0 = r35
            r28 = r1
            r1 = r2
            r29 = r18
            r2 = r37
            r30 = r3
            r3 = r4
            r31 = r4
            r4 = r17
            r32 = r5
            r5 = r7
            r33 = r6
            r6 = r12
            r34 = 0
            r7 = r16
            r8 = r27
            r0.applyRotateTimeline(r1, r2, r3, r4, r5, r6, r7, r8)
            r0 = r12
            r1 = r13
            r2 = r14
            r3 = 1
            goto L_0x0186
        L_0x014a:
            r2 = r0
            r28 = r1
            r30 = r3
            r31 = r4
            r32 = r5
            r33 = r6
            r29 = r18
            r34 = 0
            com.esotericsoftware.spine.Animation$MixBlend r0 = com.esotericsoftware.spine.Animation.MixBlend.setup
            if (r7 != r0) goto L_0x0172
            boolean r0 = r2 instanceof com.esotericsoftware.spine.Animation.AttachmentTimeline
            if (r0 == 0) goto L_0x0169
            if (r23 == 0) goto L_0x0172
            com.esotericsoftware.spine.Animation$MixDirection r0 = com.esotericsoftware.spine.Animation.MixDirection.in
        L_0x0165:
            r19 = r0
            r0 = r12
            goto L_0x0175
        L_0x0169:
            boolean r0 = r2 instanceof com.esotericsoftware.spine.Animation.DrawOrderTimeline
            if (r0 == 0) goto L_0x0172
            if (r24 == 0) goto L_0x0172
            com.esotericsoftware.spine.Animation$MixDirection r0 = com.esotericsoftware.spine.Animation.MixDirection.in
            goto L_0x0165
        L_0x0172:
            r0 = r12
            r19 = r16
        L_0x0175:
            r12 = r2
            r1 = r13
            r13 = r37
            r2 = r14
            r14 = r32
            r3 = 1
            r15 = r31
            r16 = r22
            r18 = r7
            r12.apply(r13, r14, r15, r16, r17, r18, r19)
        L_0x0186:
            int r4 = r28 + 1
            r8 = r37
            r12 = r0
            r13 = r1
            r14 = r2
            r1 = r4
            r2 = r29
            r3 = r30
            r4 = r31
            r5 = r32
            r6 = r33
            r7 = 0
            r15 = 1
            goto L_0x00b7
        L_0x019c:
            float r0 = r10.mixDuration
            int r0 = (r0 > r34 ? 1 : (r0 == r34 ? 0 : -1))
            if (r0 <= 0) goto L_0x01a8
            r0 = r31
            r9.queueEvents(r11, r0)
            goto L_0x01aa
        L_0x01a8:
            r0 = r31
        L_0x01aa:
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Event> r1 = r9.events
            r1.clear()
            r11.nextAnimationLast = r0
            float r0 = r11.trackTime
            r11.nextTrackLast = r0
            return r21
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.AnimationState.applyMixingFrom(com.esotericsoftware.spine.AnimationState$TrackEntry, com.esotericsoftware.spine.Skeleton, com.esotericsoftware.spine.Animation$MixBlend):float");
    }

    private void applyRotateTimeline(Animation.Timeline timeline, Skeleton skeleton, float f2, float f3, Animation.MixBlend mixBlend, float[] fArr, int i2, boolean z) {
        float f4;
        float f5;
        float f6;
        float f7;
        float f8 = f2;
        Animation.MixBlend mixBlend2 = mixBlend;
        if (z) {
            fArr[i2] = 0.0f;
        }
        if (f3 == 1.0f) {
            timeline.apply(skeleton, Animation.CurveTimeline.LINEAR, f2, null, 1.0f, mixBlend, Animation.MixDirection.in);
            return;
        }
        Animation.RotateTimeline rotateTimeline = (Animation.RotateTimeline) timeline;
        Bone bone = skeleton.bones.get(rotateTimeline.boneIndex);
        float[] fArr2 = rotateTimeline.frames;
        boolean z2 = false;
        if (f8 >= fArr2[0]) {
            if (f8 >= fArr2[fArr2.length - 2]) {
                f4 = bone.data.rotation + fArr2[fArr2.length - 1];
            } else {
                int binarySearch = Animation.binarySearch(fArr2, f8, 2);
                float f9 = fArr2[binarySearch - 1];
                float f10 = fArr2[binarySearch];
                float curvePercent = rotateTimeline.getCurvePercent((binarySearch >> 1) - 1, 1.0f - ((f8 - f10) / (fArr2[binarySearch - 2] - f10)));
                float f11 = fArr2[binarySearch + 1] - f9;
                double d2 = (double) (f11 / 360.0f);
                Double.isNaN(d2);
                float f12 = f9 + ((f11 - ((float) ((16384 - ((int) (16384.499999999996d - d2))) * 360))) * curvePercent) + bone.data.rotation;
                double d3 = (double) (f12 / 360.0f);
                Double.isNaN(d3);
                f4 = f12 - ((float) ((16384 - ((int) (16384.499999999996d - d3))) * 360));
            }
            float f13 = mixBlend2 == Animation.MixBlend.setup ? bone.data.rotation : bone.rotation;
            float f14 = f4 - f13;
            double d4 = (double) (f14 / 360.0f);
            Double.isNaN(d4);
            float f15 = f14 - ((float) ((16384 - ((int) (16384.499999999996d - d4))) * 360));
            if (f15 == Animation.CurveTimeline.LINEAR) {
                f5 = fArr[i2];
            } else {
                if (z) {
                    f6 = f15;
                    f7 = Animation.CurveTimeline.LINEAR;
                } else {
                    f7 = fArr[i2];
                    f6 = fArr[i2 + 1];
                }
                boolean z3 = f15 > Animation.CurveTimeline.LINEAR;
                if (f7 >= Animation.CurveTimeline.LINEAR) {
                    z2 = true;
                }
                if (Math.signum(f6) != Math.signum(f15) && Math.abs(f6) <= 90.0f) {
                    if (Math.abs(f7) > 180.0f) {
                        f7 += Math.signum(f7) * 360.0f;
                    }
                    z2 = z3;
                }
                f5 = (f15 + f7) - (f7 % 360.0f);
                if (z2 != z3) {
                    f5 += Math.signum(f7) * 360.0f;
                }
                fArr[i2] = f5;
            }
            fArr[i2 + 1] = f15;
            float f16 = f13 + (f5 * f3);
            double d5 = (double) (f16 / 360.0f);
            Double.isNaN(d5);
            bone.rotation = f16 - ((float) ((16384 - ((int) (16384.499999999996d - d5))) * 360));
        } else if (mixBlend2 == Animation.MixBlend.setup) {
            bone.rotation = bone.data.rotation;
        }
    }

    private void disposeNext(TrackEntry trackEntry) {
        for (TrackEntry trackEntry2 = trackEntry.next; trackEntry2 != null; trackEntry2 = trackEntry2.next) {
            this.queue.dispose(trackEntry2);
        }
        trackEntry.next = null;
    }

    private TrackEntry expandToIndex(int i2) {
        a<TrackEntry> aVar = this.tracks;
        int i3 = aVar.f5374b;
        if (i2 < i3) {
            return aVar.get(i2);
        }
        aVar.c((i2 - i3) + 1);
        this.tracks.f5374b = i2 + 1;
        return null;
    }

    private boolean hasTimeline(TrackEntry trackEntry, int i2) {
        a<Animation.Timeline> aVar = trackEntry.animation.timelines;
        T[] tArr = aVar.f5373a;
        int i3 = aVar.f5374b;
        for (int i4 = 0; i4 < i3; i4++) {
            if (((Animation.Timeline) tArr[i4]).getPropertyId() == i2) {
                return true;
            }
        }
        return false;
    }

    private void queueEvents(TrackEntry trackEntry, float f2) {
        float f3 = trackEntry.animationStart;
        float f4 = trackEntry.animationEnd;
        float f5 = f4 - f3;
        float f6 = trackEntry.trackLast % f5;
        a<Event> aVar = this.events;
        int i2 = aVar.f5374b;
        boolean z = false;
        int i3 = 0;
        while (i3 < i2) {
            Event event = aVar.get(i3);
            float f7 = event.time;
            if (f7 < f6) {
                break;
            }
            if (f7 <= f4) {
                this.queue.event(trackEntry, event);
            }
            i3++;
        }
        if (!trackEntry.loop ? !(f2 < f4 || trackEntry.animationLast >= f4) : !(f5 != Animation.CurveTimeline.LINEAR && f6 <= trackEntry.trackTime % f5)) {
            z = true;
        }
        if (z) {
            this.queue.complete(trackEntry);
        }
        while (i3 < i2) {
            if (aVar.get(i3).time >= f3) {
                this.queue.event(trackEntry, aVar.get(i3));
            }
            i3++;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private void setCurrent(int i2, TrackEntry trackEntry, boolean z) {
        TrackEntry expandToIndex = expandToIndex(i2);
        this.tracks.set(i2, trackEntry);
        if (expandToIndex != null) {
            if (z) {
                this.queue.interrupt(expandToIndex);
            }
            trackEntry.mixingFrom = expandToIndex;
            expandToIndex.mixingTo = trackEntry;
            trackEntry.mixTime = Animation.CurveTimeline.LINEAR;
            if (expandToIndex.mixingFrom != null) {
                float f2 = expandToIndex.mixDuration;
                if (f2 > Animation.CurveTimeline.LINEAR) {
                    trackEntry.interruptAlpha *= Math.min(1.0f, expandToIndex.mixTime / f2);
                }
            }
            expandToIndex.timelinesRotation.a();
        }
        this.queue.start(trackEntry);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006f, code lost:
        r3[r7] = 2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void setTimelineModes(com.esotericsoftware.spine.AnimationState.TrackEntry r12) {
        /*
            r11 = this;
            com.esotericsoftware.spine.AnimationState$TrackEntry r0 = r12.mixingTo
            com.esotericsoftware.spine.Animation r1 = r12.animation
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Animation$Timeline> r1 = r1.timelines
            T[] r2 = r1.f5373a
            int r1 = r1.f5374b
            com.badlogic.gdx.utils.q r3 = r12.timelineMode
            int[] r3 = r3.f(r1)
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.AnimationState$TrackEntry> r4 = r12.timelineHoldMix
            r4.clear()
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.AnimationState$TrackEntry> r12 = r12.timelineHoldMix
            java.lang.Object[] r12 = r12.f(r1)
            com.badlogic.gdx.utils.s r4 = r11.propertyIDs
            r5 = 2
            r6 = 0
            if (r0 == 0) goto L_0x0038
            boolean r7 = r0.holdPrevious
            if (r7 == 0) goto L_0x0038
        L_0x0025:
            if (r6 >= r1) goto L_0x0037
            r12 = r2[r6]
            com.esotericsoftware.spine.Animation$Timeline r12 = (com.esotericsoftware.spine.Animation.Timeline) r12
            int r12 = r12.getPropertyId()
            r4.a(r12)
            r3[r6] = r5
            int r6 = r6 + 1
            goto L_0x0025
        L_0x0037:
            return
        L_0x0038:
            r7 = 0
        L_0x0039:
            if (r7 >= r1) goto L_0x0078
            r8 = r2[r7]
            com.esotericsoftware.spine.Animation$Timeline r8 = (com.esotericsoftware.spine.Animation.Timeline) r8
            int r8 = r8.getPropertyId()
            boolean r9 = r4.a(r8)
            if (r9 != 0) goto L_0x004c
            r3[r7] = r6
            goto L_0x0075
        L_0x004c:
            if (r0 == 0) goto L_0x0072
            boolean r9 = r11.hasTimeline(r0, r8)
            if (r9 != 0) goto L_0x0055
            goto L_0x0072
        L_0x0055:
            com.esotericsoftware.spine.AnimationState$TrackEntry r9 = r0.mixingTo
        L_0x0057:
            if (r9 == 0) goto L_0x006f
            boolean r10 = r11.hasTimeline(r9, r8)
            if (r10 == 0) goto L_0x0062
            com.esotericsoftware.spine.AnimationState$TrackEntry r9 = r9.mixingTo
            goto L_0x0057
        L_0x0062:
            float r8 = r9.mixDuration
            r10 = 0
            int r8 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r8 <= 0) goto L_0x006f
            r8 = 3
            r3[r7] = r8
            r12[r7] = r9
            goto L_0x0075
        L_0x006f:
            r3[r7] = r5
            goto L_0x0075
        L_0x0072:
            r8 = 1
            r3[r7] = r8
        L_0x0075:
            int r7 = r7 + 1
            goto L_0x0039
        L_0x0078:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.AnimationState.setTimelineModes(com.esotericsoftware.spine.AnimationState$TrackEntry):void");
    }

    private TrackEntry trackEntry(int i2, Animation animation, boolean z, TrackEntry trackEntry) {
        TrackEntry obtain = this.trackEntryPool.obtain();
        obtain.trackIndex = i2;
        obtain.animation = animation;
        obtain.loop = z;
        obtain.holdPrevious = false;
        float f2 = Animation.CurveTimeline.LINEAR;
        obtain.eventThreshold = Animation.CurveTimeline.LINEAR;
        obtain.attachmentThreshold = Animation.CurveTimeline.LINEAR;
        obtain.drawOrderThreshold = Animation.CurveTimeline.LINEAR;
        obtain.animationStart = Animation.CurveTimeline.LINEAR;
        obtain.animationEnd = animation.getDuration();
        obtain.animationLast = -1.0f;
        obtain.nextAnimationLast = -1.0f;
        obtain.delay = Animation.CurveTimeline.LINEAR;
        obtain.trackTime = Animation.CurveTimeline.LINEAR;
        obtain.trackLast = -1.0f;
        obtain.nextTrackLast = -1.0f;
        obtain.trackEnd = Float.MAX_VALUE;
        obtain.timeScale = 1.0f;
        obtain.alpha = 1.0f;
        obtain.interruptAlpha = 1.0f;
        obtain.mixTime = Animation.CurveTimeline.LINEAR;
        if (trackEntry != null) {
            f2 = this.data.getMix(trackEntry.animation, animation);
        }
        obtain.mixDuration = f2;
        return obtain;
    }

    private boolean updateMixingFrom(TrackEntry trackEntry, float f2) {
        TrackEntry trackEntry2 = trackEntry.mixingFrom;
        if (trackEntry2 == null) {
            return true;
        }
        boolean updateMixingFrom = updateMixingFrom(trackEntry2, f2);
        trackEntry2.animationLast = trackEntry2.nextAnimationLast;
        trackEntry2.trackLast = trackEntry2.nextTrackLast;
        float f3 = trackEntry.mixTime;
        if (f3 > Animation.CurveTimeline.LINEAR) {
            float f4 = trackEntry.mixDuration;
            if (f3 >= f4) {
                if (trackEntry2.totalAlpha == Animation.CurveTimeline.LINEAR || f4 == Animation.CurveTimeline.LINEAR) {
                    trackEntry.mixingFrom = trackEntry2.mixingFrom;
                    TrackEntry trackEntry3 = trackEntry2.mixingFrom;
                    if (trackEntry3 != null) {
                        trackEntry3.mixingTo = trackEntry;
                    }
                    trackEntry.interruptAlpha = trackEntry2.interruptAlpha;
                    this.queue.end(trackEntry2);
                }
                return updateMixingFrom;
            }
        }
        trackEntry2.trackTime += trackEntry2.timeScale * f2;
        trackEntry.mixTime += f2;
        return false;
    }

    public TrackEntry addAnimation(int i2, String str, boolean z, float f2) {
        Animation findAnimation = this.data.skeletonData.findAnimation(str);
        if (findAnimation != null) {
            return addAnimation(i2, findAnimation, z, f2);
        }
        throw new IllegalArgumentException("Animation not found: " + str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.esotericsoftware.spine.AnimationState.addAnimation(int, com.esotericsoftware.spine.Animation, boolean, float):com.esotericsoftware.spine.AnimationState$TrackEntry
     arg types: [int, com.esotericsoftware.spine.Animation, int, float]
     candidates:
      com.esotericsoftware.spine.AnimationState.addAnimation(int, java.lang.String, boolean, float):com.esotericsoftware.spine.AnimationState$TrackEntry
      com.esotericsoftware.spine.AnimationState.addAnimation(int, com.esotericsoftware.spine.Animation, boolean, float):com.esotericsoftware.spine.AnimationState$TrackEntry */
    public TrackEntry addEmptyAnimation(int i2, float f2, float f3) {
        if (f3 <= Animation.CurveTimeline.LINEAR) {
            f3 -= f2;
        }
        TrackEntry addAnimation = addAnimation(i2, emptyAnimation, false, f3);
        addAnimation.mixDuration = f2;
        addAnimation.trackEnd = f2;
        return addAnimation;
    }

    public void addListener(AnimationStateListener animationStateListener) {
        if (animationStateListener != null) {
            this.listeners.add(animationStateListener);
            return;
        }
        throw new IllegalArgumentException("listener cannot be null.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00bf  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean apply(com.esotericsoftware.spine.Skeleton r28) {
        /*
            r27 = this;
            r9 = r27
            r10 = r28
            if (r10 == 0) goto L_0x012a
            boolean r0 = r9.animationsChanged
            if (r0 == 0) goto L_0x000d
            r27.animationsChanged()
        L_0x000d:
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Event> r11 = r9.events
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.AnimationState$TrackEntry> r0 = r9.tracks
            int r12 = r0.f5374b
            r0 = 0
            r14 = 0
        L_0x0015:
            if (r14 >= r12) goto L_0x0124
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.AnimationState$TrackEntry> r1 = r9.tracks
            java.lang.Object r1 = r1.get(r14)
            r15 = r1
            com.esotericsoftware.spine.AnimationState$TrackEntry r15 = (com.esotericsoftware.spine.AnimationState.TrackEntry) r15
            r16 = 1
            if (r15 == 0) goto L_0x0120
            float r1 = r15.delay
            r2 = 0
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x002d
            goto L_0x0120
        L_0x002d:
            if (r14 != 0) goto L_0x0032
            com.esotericsoftware.spine.Animation$MixBlend r0 = com.esotericsoftware.spine.Animation.MixBlend.first
            goto L_0x0034
        L_0x0032:
            com.esotericsoftware.spine.Animation$MixBlend r0 = r15.mixBlend
        L_0x0034:
            r8 = r0
            float r0 = r15.alpha
            com.esotericsoftware.spine.AnimationState$TrackEntry r1 = r15.mixingFrom
            if (r1 == 0) goto L_0x0042
            float r1 = r9.applyMixingFrom(r15, r10, r8)
            float r0 = r0 * r1
            goto L_0x0051
        L_0x0042:
            float r1 = r15.trackTime
            float r3 = r15.trackEnd
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 < 0) goto L_0x0051
            com.esotericsoftware.spine.AnimationState$TrackEntry r1 = r15.next
            if (r1 != 0) goto L_0x0051
            r17 = 0
            goto L_0x0053
        L_0x0051:
            r17 = r0
        L_0x0053:
            float r7 = r15.animationLast
            float r6 = r15.getAnimationTime()
            com.esotericsoftware.spine.Animation r0 = r15.animation
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Animation$Timeline> r0 = r0.timelines
            int r5 = r0.f5374b
            T[] r4 = r0.f5373a
            if (r14 != 0) goto L_0x0099
            r0 = 1065353216(0x3f800000, float:1.0)
            int r0 = (r17 > r0 ? 1 : (r17 == r0 ? 0 : -1))
            if (r0 == 0) goto L_0x006d
            com.esotericsoftware.spine.Animation$MixBlend r0 = com.esotericsoftware.spine.Animation.MixBlend.add
            if (r8 != r0) goto L_0x0099
        L_0x006d:
            r3 = 0
        L_0x006e:
            if (r3 >= r5) goto L_0x0096
            r0 = r4[r3]
            com.esotericsoftware.spine.Animation$Timeline r0 = (com.esotericsoftware.spine.Animation.Timeline) r0
            com.esotericsoftware.spine.Animation$MixDirection r18 = com.esotericsoftware.spine.Animation.MixDirection.in
            r1 = r28
            r2 = r7
            r19 = r3
            r3 = r6
            r20 = r4
            r4 = r11
            r13 = r5
            r5 = r17
            r21 = r6
            r6 = r8
            r22 = r7
            r7 = r18
            r0.apply(r1, r2, r3, r4, r5, r6, r7)
            int r3 = r19 + 1
            r5 = r13
            r4 = r20
            r6 = r21
            r7 = r22
            goto L_0x006e
        L_0x0096:
            r0 = r6
            goto L_0x0113
        L_0x0099:
            r20 = r4
            r13 = r5
            r21 = r6
            r22 = r7
            com.badlogic.gdx.utils.q r0 = r15.timelineMode
            int[] r7 = r0.f5558a
            com.badlogic.gdx.utils.m r0 = r15.timelinesRotation
            int r0 = r0.f5528b
            if (r0 != 0) goto L_0x00ad
            r18 = 1
            goto L_0x00af
        L_0x00ad:
            r18 = 0
        L_0x00af:
            if (r18 == 0) goto L_0x00b8
            com.badlogic.gdx.utils.m r0 = r15.timelinesRotation
            int r1 = r13 << 1
            r0.d(r1)
        L_0x00b8:
            com.badlogic.gdx.utils.m r0 = r15.timelinesRotation
            float[] r6 = r0.f5527a
            r5 = 0
        L_0x00bd:
            if (r5 >= r13) goto L_0x0111
            r0 = r20[r5]
            r1 = r0
            com.esotericsoftware.spine.Animation$Timeline r1 = (com.esotericsoftware.spine.Animation.Timeline) r1
            r0 = r7[r5]
            if (r0 != 0) goto L_0x00cb
            r19 = r8
            goto L_0x00cf
        L_0x00cb:
            com.esotericsoftware.spine.Animation$MixBlend r0 = com.esotericsoftware.spine.Animation.MixBlend.setup
            r19 = r0
        L_0x00cf:
            boolean r0 = r1 instanceof com.esotericsoftware.spine.Animation.RotateTimeline
            if (r0 == 0) goto L_0x00ef
            int r23 = r5 << 1
            r0 = r27
            r2 = r28
            r3 = r21
            r4 = r17
            r24 = r5
            r5 = r19
            r25 = r6
            r26 = r7
            r7 = r23
            r23 = r8
            r8 = r18
            r0.applyRotateTimeline(r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x0108
        L_0x00ef:
            r24 = r5
            r25 = r6
            r26 = r7
            r23 = r8
            com.esotericsoftware.spine.Animation$MixDirection r7 = com.esotericsoftware.spine.Animation.MixDirection.in
            r0 = r1
            r1 = r28
            r2 = r22
            r3 = r21
            r4 = r11
            r5 = r17
            r6 = r19
            r0.apply(r1, r2, r3, r4, r5, r6, r7)
        L_0x0108:
            int r5 = r24 + 1
            r8 = r23
            r6 = r25
            r7 = r26
            goto L_0x00bd
        L_0x0111:
            r0 = r21
        L_0x0113:
            r9.queueEvents(r15, r0)
            r11.clear()
            r15.nextAnimationLast = r0
            float r0 = r15.trackTime
            r15.nextTrackLast = r0
            r0 = 1
        L_0x0120:
            int r14 = r14 + 1
            goto L_0x0015
        L_0x0124:
            com.esotericsoftware.spine.AnimationState$EventQueue r1 = r9.queue
            r1.drain()
            return r0
        L_0x012a:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "skeleton cannot be null."
            r0.<init>(r1)
            goto L_0x0133
        L_0x0132:
            throw r0
        L_0x0133:
            goto L_0x0132
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.AnimationState.apply(com.esotericsoftware.spine.Skeleton):boolean");
    }

    public void clearListenerNotifications() {
        this.queue.clear();
    }

    public void clearListeners() {
        this.listeners.clear();
    }

    public void clearTrack(int i2) {
        TrackEntry trackEntry;
        a<TrackEntry> aVar = this.tracks;
        if (i2 < aVar.f5374b && (trackEntry = aVar.get(i2)) != null) {
            this.queue.end(trackEntry);
            disposeNext(trackEntry);
            TrackEntry trackEntry2 = trackEntry;
            while (true) {
                TrackEntry trackEntry3 = trackEntry2.mixingFrom;
                if (trackEntry3 == null) {
                    this.tracks.set(trackEntry.trackIndex, null);
                    this.queue.drain();
                    return;
                }
                this.queue.end(trackEntry3);
                trackEntry2.mixingFrom = null;
                trackEntry2.mixingTo = null;
                trackEntry2 = trackEntry3;
            }
        }
    }

    public void clearTracks() {
        EventQueue eventQueue = this.queue;
        boolean z = eventQueue.drainDisabled;
        eventQueue.drainDisabled = true;
        int i2 = this.tracks.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            clearTrack(i3);
        }
        this.tracks.clear();
        EventQueue eventQueue2 = this.queue;
        eventQueue2.drainDisabled = z;
        eventQueue2.drain();
    }

    public TrackEntry getCurrent(int i2) {
        a<TrackEntry> aVar = this.tracks;
        if (i2 >= aVar.f5374b) {
            return null;
        }
        return aVar.get(i2);
    }

    public AnimationStateData getData() {
        return this.data;
    }

    public float getTimeScale() {
        return this.timeScale;
    }

    public a<TrackEntry> getTracks() {
        return this.tracks;
    }

    public void removeListener(AnimationStateListener animationStateListener) {
        this.listeners.d(animationStateListener, true);
    }

    public TrackEntry setAnimation(int i2, String str, boolean z) {
        Animation findAnimation = this.data.skeletonData.findAnimation(str);
        if (findAnimation != null) {
            return setAnimation(i2, findAnimation, z);
        }
        throw new IllegalArgumentException("Animation not found: " + str);
    }

    public void setData(AnimationStateData animationStateData) {
        if (animationStateData != null) {
            this.data = animationStateData;
            return;
        }
        throw new IllegalArgumentException("data cannot be null.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.esotericsoftware.spine.AnimationState.setAnimation(int, com.esotericsoftware.spine.Animation, boolean):com.esotericsoftware.spine.AnimationState$TrackEntry
     arg types: [int, com.esotericsoftware.spine.Animation, int]
     candidates:
      com.esotericsoftware.spine.AnimationState.setAnimation(int, java.lang.String, boolean):com.esotericsoftware.spine.AnimationState$TrackEntry
      com.esotericsoftware.spine.AnimationState.setAnimation(int, com.esotericsoftware.spine.Animation, boolean):com.esotericsoftware.spine.AnimationState$TrackEntry */
    public TrackEntry setEmptyAnimation(int i2, float f2) {
        TrackEntry animation = setAnimation(i2, emptyAnimation, false);
        animation.mixDuration = f2;
        animation.trackEnd = f2;
        return animation;
    }

    public void setEmptyAnimations(float f2) {
        EventQueue eventQueue = this.queue;
        boolean z = eventQueue.drainDisabled;
        eventQueue.drainDisabled = true;
        int i2 = this.tracks.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            TrackEntry trackEntry = this.tracks.get(i3);
            if (trackEntry != null) {
                setEmptyAnimation(trackEntry.trackIndex, f2);
            }
        }
        EventQueue eventQueue2 = this.queue;
        eventQueue2.drainDisabled = z;
        eventQueue2.drain();
    }

    public void setTimeScale(float f2) {
        this.timeScale = f2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        int i2 = this.tracks.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            TrackEntry trackEntry = this.tracks.get(i3);
            if (trackEntry != null) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(trackEntry.toString());
            }
        }
        if (sb.length() == 0) {
            return "<none>";
        }
        return sb.toString();
    }

    public void update(float f2) {
        float f3 = f2 * this.timeScale;
        int i2 = this.tracks.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            TrackEntry trackEntry = this.tracks.get(i3);
            if (trackEntry != null) {
                trackEntry.animationLast = trackEntry.nextAnimationLast;
                trackEntry.trackLast = trackEntry.nextTrackLast;
                float f4 = trackEntry.timeScale * f3;
                float f5 = trackEntry.delay;
                if (f5 > Animation.CurveTimeline.LINEAR) {
                    trackEntry.delay = f5 - f4;
                    float f6 = trackEntry.delay;
                    if (f6 <= Animation.CurveTimeline.LINEAR) {
                        f4 = -f6;
                        trackEntry.delay = Animation.CurveTimeline.LINEAR;
                    }
                }
                TrackEntry trackEntry2 = trackEntry.next;
                if (trackEntry2 != null) {
                    float f7 = trackEntry.trackLast - trackEntry2.delay;
                    if (f7 >= Animation.CurveTimeline.LINEAR) {
                        trackEntry2.delay = Animation.CurveTimeline.LINEAR;
                        trackEntry2.trackTime = ((f7 / trackEntry.timeScale) + f3) * trackEntry2.timeScale;
                        trackEntry.trackTime += f4;
                        setCurrent(i3, trackEntry2, true);
                        while (true) {
                            TrackEntry trackEntry3 = trackEntry2.mixingFrom;
                            if (trackEntry3 == null) {
                                break;
                            }
                            trackEntry2.mixTime += f3;
                            trackEntry2 = trackEntry3;
                        }
                    }
                } else if (trackEntry.trackLast >= trackEntry.trackEnd && trackEntry.mixingFrom == null) {
                    this.tracks.set(i3, null);
                    this.queue.end(trackEntry);
                    disposeNext(trackEntry);
                }
                if (trackEntry.mixingFrom != null && updateMixingFrom(trackEntry, f3)) {
                    TrackEntry trackEntry4 = trackEntry.mixingFrom;
                    trackEntry.mixingFrom = null;
                    if (trackEntry4 != null) {
                        trackEntry4.mixingTo = null;
                    }
                    while (trackEntry4 != null) {
                        this.queue.end(trackEntry4);
                        trackEntry4 = trackEntry4.mixingFrom;
                    }
                }
                trackEntry.trackTime += f4;
            }
        }
        this.queue.drain();
    }

    public TrackEntry addAnimation(int i2, Animation animation, boolean z, float f2) {
        float f3;
        if (animation != null) {
            TrackEntry expandToIndex = expandToIndex(i2);
            if (expandToIndex != null) {
                while (true) {
                    TrackEntry trackEntry = expandToIndex.next;
                    if (trackEntry == null) {
                        break;
                    }
                    expandToIndex = trackEntry;
                }
            }
            TrackEntry trackEntry2 = trackEntry(i2, animation, z, expandToIndex);
            if (expandToIndex == null) {
                setCurrent(i2, trackEntry2, true);
                this.queue.drain();
            } else {
                expandToIndex.next = trackEntry2;
                if (f2 <= Animation.CurveTimeline.LINEAR) {
                    float f4 = expandToIndex.animationEnd - expandToIndex.animationStart;
                    if (f4 != Animation.CurveTimeline.LINEAR) {
                        if (expandToIndex.loop) {
                            f3 = f2 + (f4 * ((float) (((int) (expandToIndex.trackTime / f4)) + 1)));
                        } else {
                            f3 = f2 + Math.max(f4, expandToIndex.trackTime);
                        }
                        f2 = f3 - this.data.getMix(expandToIndex.animation, animation);
                    } else {
                        f2 = expandToIndex.trackTime;
                    }
                }
            }
            trackEntry2.delay = f2;
            return trackEntry2;
        }
        throw new IllegalArgumentException("animation cannot be null.");
    }

    public TrackEntry setAnimation(int i2, Animation animation, boolean z) {
        if (animation != null) {
            boolean z2 = true;
            TrackEntry expandToIndex = expandToIndex(i2);
            if (expandToIndex != null) {
                if (expandToIndex.nextTrackLast == -1.0f) {
                    this.tracks.set(i2, expandToIndex.mixingFrom);
                    this.queue.interrupt(expandToIndex);
                    this.queue.end(expandToIndex);
                    disposeNext(expandToIndex);
                    expandToIndex = expandToIndex.mixingFrom;
                    z2 = false;
                } else {
                    disposeNext(expandToIndex);
                }
            }
            TrackEntry trackEntry = trackEntry(i2, animation, z, expandToIndex);
            setCurrent(i2, trackEntry, z2);
            this.queue.drain();
            return trackEntry;
        }
        throw new IllegalArgumentException("animation cannot be null.");
    }

    public AnimationState(AnimationStateData animationStateData) {
        if (animationStateData != null) {
            this.data = animationStateData;
            return;
        }
        throw new IllegalArgumentException("data cannot be null.");
    }
}
