package com.esotericsoftware.spine;

import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.c0;
import com.badlogic.gdx.utils.f0;
import com.esotericsoftware.spine.attachments.Attachment;

public class Skin {
    final c0<Key, Attachment> attachments = new c0<>();
    final f0<Key> keyPool = new f0(64) {
        /* access modifiers changed from: protected */
        public Object newObject() {
            return new Key();
        }
    };
    private final Key lookup = new Key();
    final String name;

    static class Key {
        int hashCode;
        String name;
        int slotIndex;

        Key() {
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            Key key = (Key) obj;
            if (this.slotIndex == key.slotIndex && this.name.equals(key.name)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return this.hashCode;
        }

        public void set(int i2, String str) {
            if (str != null) {
                this.slotIndex = i2;
                this.name = str;
                this.hashCode = str.hashCode() + (i2 * 37);
                return;
            }
            throw new IllegalArgumentException("name cannot be null.");
        }

        public String toString() {
            return this.slotIndex + ":" + this.name;
        }
    }

    public Skin(String str) {
        if (str != null) {
            this.name = str;
            return;
        }
        throw new IllegalArgumentException("name cannot be null.");
    }

    public void addAttachment(int i2, String str, Attachment attachment) {
        if (attachment == null) {
            throw new IllegalArgumentException("attachment cannot be null.");
        } else if (i2 >= 0) {
            Key obtain = this.keyPool.obtain();
            obtain.set(i2, str);
            this.attachments.b(obtain, attachment);
        } else {
            throw new IllegalArgumentException("slotIndex must be >= 0.");
        }
    }

    public void addAttachments(Skin skin) {
        c0.a<Key, Attachment> a2 = skin.attachments.a();
        a2.iterator();
        while (a2.hasNext()) {
            c0.b bVar = (c0.b) a2.next();
            K k2 = bVar.f5459a;
            addAttachment(((Key) k2).slotIndex, ((Key) k2).name, (Attachment) bVar.f5460b);
        }
    }

    /* access modifiers changed from: package-private */
    public void attachAll(Skeleton skeleton, Skin skin) {
        Attachment attachment;
        c0.a<Key, Attachment> a2 = skin.attachments.a();
        a2.iterator();
        while (a2.hasNext()) {
            c0.b bVar = (c0.b) a2.next();
            int i2 = ((Key) bVar.f5459a).slotIndex;
            Slot slot = skeleton.slots.get(i2);
            if (slot.attachment == bVar.f5460b && (attachment = getAttachment(i2, ((Key) bVar.f5459a).name)) != null) {
                slot.setAttachment(attachment);
            }
        }
    }

    public void clear() {
        c0.c<Key> b2 = this.attachments.b();
        b2.iterator();
        while (b2.hasNext()) {
            this.keyPool.free((Key) b2.next());
        }
        this.attachments.c(1024);
    }

    public void findAttachmentsForSlot(int i2, a<Attachment> aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("attachments cannot be null.");
        } else if (i2 >= 0) {
            c0.a<Key, Attachment> a2 = this.attachments.a();
            a2.iterator();
            while (a2.hasNext()) {
                c0.b bVar = (c0.b) a2.next();
                if (((Key) bVar.f5459a).slotIndex == i2) {
                    aVar.add(bVar.f5460b);
                }
            }
        } else {
            throw new IllegalArgumentException("slotIndex must be >= 0.");
        }
    }

    public void findNamesForSlot(int i2, a<String> aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("names cannot be null.");
        } else if (i2 >= 0) {
            c0.c<Key> b2 = this.attachments.b();
            b2.iterator();
            while (b2.hasNext()) {
                Key key = (Key) b2.next();
                if (key.slotIndex == i2) {
                    aVar.add(key.name);
                }
            }
        } else {
            throw new IllegalArgumentException("slotIndex must be >= 0.");
        }
    }

    public Attachment getAttachment(int i2, String str) {
        if (i2 >= 0) {
            this.lookup.set(i2, str);
            return this.attachments.b(this.lookup);
        }
        throw new IllegalArgumentException("slotIndex must be >= 0.");
    }

    public String getName() {
        return this.name;
    }

    public void removeAttachment(int i2, String str) {
        if (i2 >= 0) {
            Key obtain = this.keyPool.obtain();
            obtain.set(i2, str);
            this.attachments.remove(obtain);
            this.keyPool.free(obtain);
            return;
        }
        throw new IllegalArgumentException("slotIndex must be >= 0.");
    }

    public int size() {
        return this.attachments.f5447a;
    }

    public String toString() {
        return this.name;
    }
}
