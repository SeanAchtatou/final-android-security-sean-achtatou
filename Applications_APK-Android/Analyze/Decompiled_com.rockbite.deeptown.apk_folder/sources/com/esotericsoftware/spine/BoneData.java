package com.esotericsoftware.spine;

import e.d.b.t.b;

public class BoneData {
    final b color = new b(0.61f, 0.61f, 0.61f, 1.0f);
    final int index;
    float length;
    final String name;
    final BoneData parent;
    float rotation;
    float scaleX = 1.0f;
    float scaleY = 1.0f;
    float shearX;
    float shearY;
    TransformMode transformMode = TransformMode.normal;
    float x;
    float y;

    public enum TransformMode {
        normal,
        onlyTranslation,
        noRotationOrReflection,
        noScale,
        noScaleOrReflection;
        
        public static final TransformMode[] values = values();
    }

    public BoneData(int i2, String str, BoneData boneData) {
        if (i2 < 0) {
            throw new IllegalArgumentException("index must be >= 0.");
        } else if (str != null) {
            this.index = i2;
            this.name = str;
            this.parent = boneData;
        } else {
            throw new IllegalArgumentException("name cannot be null.");
        }
    }

    public b getColor() {
        return this.color;
    }

    public int getIndex() {
        return this.index;
    }

    public float getLength() {
        return this.length;
    }

    public String getName() {
        return this.name;
    }

    public BoneData getParent() {
        return this.parent;
    }

    public float getRotation() {
        return this.rotation;
    }

    public float getScaleX() {
        return this.scaleX;
    }

    public float getScaleY() {
        return this.scaleY;
    }

    public float getShearX() {
        return this.shearX;
    }

    public float getShearY() {
        return this.shearY;
    }

    public TransformMode getTransformMode() {
        return this.transformMode;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public void setLength(float f2) {
        this.length = f2;
    }

    public void setPosition(float f2, float f3) {
        this.x = f2;
        this.y = f3;
    }

    public void setRotation(float f2) {
        this.rotation = f2;
    }

    public void setScale(float f2, float f3) {
        this.scaleX = f2;
        this.scaleY = f3;
    }

    public void setScaleX(float f2) {
        this.scaleX = f2;
    }

    public void setScaleY(float f2) {
        this.scaleY = f2;
    }

    public void setShearX(float f2) {
        this.shearX = f2;
    }

    public void setShearY(float f2) {
        this.shearY = f2;
    }

    public void setTransformMode(TransformMode transformMode2) {
        this.transformMode = transformMode2;
    }

    public void setX(float f2) {
        this.x = f2;
    }

    public void setY(float f2) {
        this.y = f2;
    }

    public String toString() {
        return this.name;
    }

    public BoneData(BoneData boneData, BoneData boneData2) {
        if (boneData != null) {
            this.index = boneData.index;
            this.name = boneData.name;
            this.parent = boneData2;
            this.length = boneData.length;
            this.x = boneData.x;
            this.y = boneData.y;
            this.rotation = boneData.rotation;
            this.scaleX = boneData.scaleX;
            this.scaleY = boneData.scaleY;
            this.shearX = boneData.shearX;
            this.shearY = boneData.shearY;
            return;
        }
        throw new IllegalArgumentException("bone cannot be null.");
    }
}
