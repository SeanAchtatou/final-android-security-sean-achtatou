package com.esotericsoftware.spine;

import com.badlogic.gdx.graphics.g2d.q;
import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.l0;
import com.badlogic.gdx.utils.m;
import com.badlogic.gdx.utils.u;
import com.badlogic.gdx.utils.v;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.attachments.AtlasAttachmentLoader;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.AttachmentLoader;
import com.esotericsoftware.spine.attachments.AttachmentType;
import com.esotericsoftware.spine.attachments.BoundingBoxAttachment;
import com.esotericsoftware.spine.attachments.ClippingAttachment;
import com.esotericsoftware.spine.attachments.MeshAttachment;
import com.esotericsoftware.spine.attachments.PathAttachment;
import com.esotericsoftware.spine.attachments.PointAttachment;
import com.esotericsoftware.spine.attachments.RegionAttachment;
import com.esotericsoftware.spine.attachments.VertexAttachment;
import e.d.b.t.b;

public class SkeletonJson {
    private final AttachmentLoader attachmentLoader;
    private a<LinkedMesh> linkedMeshes = new a<>();
    private float scale = 1.0f;

    /* renamed from: com.esotericsoftware.spine.SkeletonJson$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType = new int[AttachmentType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(14:0|1|2|3|4|5|6|7|8|9|10|11|12|(3:13|14|16)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|16) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.esotericsoftware.spine.attachments.AttachmentType[] r0 = com.esotericsoftware.spine.attachments.AttachmentType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.esotericsoftware.spine.SkeletonJson.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType = r0
                int[] r0 = com.esotericsoftware.spine.SkeletonJson.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.region     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.esotericsoftware.spine.SkeletonJson.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.boundingbox     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.esotericsoftware.spine.SkeletonJson.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x002a }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.mesh     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.esotericsoftware.spine.SkeletonJson.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.linkedmesh     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.esotericsoftware.spine.SkeletonJson.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.path     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = com.esotericsoftware.spine.SkeletonJson.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x004b }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.point     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = com.esotericsoftware.spine.SkeletonJson.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType     // Catch:{ NoSuchFieldError -> 0x0056 }
                com.esotericsoftware.spine.attachments.AttachmentType r1 = com.esotericsoftware.spine.attachments.AttachmentType.clipping     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.SkeletonJson.AnonymousClass1.<clinit>():void");
        }
    }

    static class LinkedMesh {
        MeshAttachment mesh;
        String parent;
        String skin;
        int slotIndex;

        public LinkedMesh(MeshAttachment meshAttachment, String str, int i2, String str2) {
            this.mesh = meshAttachment;
            this.skin = str;
            this.slotIndex = i2;
            this.parent = str2;
        }
    }

    public SkeletonJson(q qVar) {
        this.attachmentLoader = new AtlasAttachmentLoader(qVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.a.b(java.lang.Object, boolean):int
     arg types: [com.esotericsoftware.spine.IkConstraintData, int]
     candidates:
      com.badlogic.gdx.utils.a.b(int, int):void
      com.badlogic.gdx.utils.a.b(java.lang.Object, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.v.a(java.lang.String, float):float
     arg types: [java.lang.String, int]
     candidates:
      com.badlogic.gdx.utils.v.a(int, com.badlogic.gdx.utils.r0):void
      com.badlogic.gdx.utils.v.a(java.lang.String, int):int
      com.badlogic.gdx.utils.v.a(com.badlogic.gdx.utils.w$c, int):java.lang.String
      com.badlogic.gdx.utils.v.a(java.lang.String, java.lang.String):java.lang.String
      com.badlogic.gdx.utils.v.a(double, java.lang.String):void
      com.badlogic.gdx.utils.v.a(long, java.lang.String):void
      com.badlogic.gdx.utils.v.a(java.lang.String, com.badlogic.gdx.utils.v):void
      com.badlogic.gdx.utils.v.a(java.lang.String, boolean):boolean
      com.badlogic.gdx.utils.v.a(java.lang.String, float):float */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.v.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.badlogic.gdx.utils.v.a(int, com.badlogic.gdx.utils.r0):void
      com.badlogic.gdx.utils.v.a(java.lang.String, float):float
      com.badlogic.gdx.utils.v.a(java.lang.String, int):int
      com.badlogic.gdx.utils.v.a(com.badlogic.gdx.utils.w$c, int):java.lang.String
      com.badlogic.gdx.utils.v.a(java.lang.String, java.lang.String):java.lang.String
      com.badlogic.gdx.utils.v.a(double, java.lang.String):void
      com.badlogic.gdx.utils.v.a(long, java.lang.String):void
      com.badlogic.gdx.utils.v.a(java.lang.String, com.badlogic.gdx.utils.v):void
      com.badlogic.gdx.utils.v.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.a.b(java.lang.Object, boolean):int
     arg types: [com.esotericsoftware.spine.TransformConstraintData, int]
     candidates:
      com.badlogic.gdx.utils.a.b(int, int):void
      com.badlogic.gdx.utils.a.b(java.lang.Object, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.a.b(java.lang.Object, boolean):int
     arg types: [com.esotericsoftware.spine.PathConstraintData, int]
     candidates:
      com.badlogic.gdx.utils.a.b(int, int):void
      com.badlogic.gdx.utils.a.b(java.lang.Object, boolean):int */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x049c, code lost:
        if (r8 != com.esotericsoftware.spine.PathConstraintData.SpacingMode.fixed) goto L_0x04af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x04aa, code lost:
        if (r5.positionMode == com.esotericsoftware.spine.PathConstraintData.PositionMode.fixed) goto L_0x04ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x04af, code lost:
        r8 = 1.0f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x02af A[LOOP:8: B:61:0x02ad->B:62:0x02af, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void readAnimation(com.badlogic.gdx.utils.v r39, java.lang.String r40, com.esotericsoftware.spine.SkeletonData r41) {
        /*
            r38 = this;
            r0 = r38
            r1 = r39
            r2 = r41
            float r3 = r0.scale
            com.badlogic.gdx.utils.a r4 = new com.badlogic.gdx.utils.a
            r4.<init>()
            java.lang.String r5 = "slots"
            com.badlogic.gdx.utils.v r5 = r1.c(r5)
            r7 = 0
        L_0x0014:
            java.lang.String r8 = ")"
            java.lang.String r9 = " ("
            java.lang.String r10 = "name"
            java.lang.String r11 = "Slot not found: "
            java.lang.String r12 = "time"
            if (r5 == 0) goto L_0x01c6
            java.lang.String r15 = r5.f5626e
            com.esotericsoftware.spine.SlotData r15 = r2.findSlot(r15)
            if (r15 == 0) goto L_0x01af
            com.badlogic.gdx.utils.v r11 = r5.f5627f
        L_0x002a:
            if (r11 == 0) goto L_0x01a5
            java.lang.String r13 = r11.f5626e
            java.lang.String r6 = "attachment"
            boolean r6 = r13.equals(r6)
            if (r6 == 0) goto L_0x007e
            com.esotericsoftware.spine.Animation$AttachmentTimeline r6 = new com.esotericsoftware.spine.Animation$AttachmentTimeline
            int r13 = r11.f5631j
            r6.<init>(r13)
            int r13 = r15.index
            r6.slotIndex = r13
            com.badlogic.gdx.utils.v r13 = r11.f5627f
            r14 = 0
        L_0x0044:
            if (r13 == 0) goto L_0x005e
            int r18 = r14 + 1
            r19 = r3
            float r3 = r13.e(r12)
            java.lang.String r2 = r13.h(r10)
            r6.setFrame(r14, r3, r2)
            com.badlogic.gdx.utils.v r13 = r13.f5628g
            r2 = r41
            r14 = r18
            r3 = r19
            goto L_0x0044
        L_0x005e:
            r19 = r3
            r4.add(r6)
            float[] r2 = r6.getFrames()
            int r3 = r6.getFrameCount()
            r6 = 1
            int r3 = r3 - r6
            r2 = r2[r3]
            float r2 = java.lang.Math.max(r7, r2)
            r7 = r2
            r21 = r8
            r27 = r10
        L_0x0078:
            r20 = r12
            r18 = r15
            goto L_0x016d
        L_0x007e:
            r19 = r3
            java.lang.String r2 = "color"
            boolean r3 = r13.equals(r2)
            if (r3 == 0) goto L_0x00e8
            com.esotericsoftware.spine.Animation$ColorTimeline r3 = new com.esotericsoftware.spine.Animation$ColorTimeline
            int r6 = r11.f5631j
            r3.<init>(r6)
            int r6 = r15.index
            r3.slotIndex = r6
            com.badlogic.gdx.utils.v r6 = r11.f5627f
            r13 = 0
        L_0x0096:
            if (r6 == 0) goto L_0x00cd
            java.lang.String r14 = r6.h(r2)
            e.d.b.t.b r14 = e.d.b.t.b.a(r14)
            float r22 = r6.e(r12)
            r18 = r2
            float r2 = r14.f6934a
            r27 = r10
            float r10 = r14.f6935b
            float r1 = r14.f6936c
            float r14 = r14.f6937d
            r20 = r3
            r21 = r13
            r23 = r2
            r24 = r10
            r25 = r1
            r26 = r14
            r20.setFrame(r21, r22, r23, r24, r25, r26)
            r0.readCurve(r6, r3, r13)
            r1 = 1
            int r13 = r13 + r1
            com.badlogic.gdx.utils.v r6 = r6.f5628g
            r1 = r39
            r2 = r18
            r10 = r27
            goto L_0x0096
        L_0x00cd:
            r27 = r10
            r1 = 1
            r4.add(r3)
            float[] r2 = r3.getFrames()
            int r3 = r3.getFrameCount()
            int r3 = r3 - r1
            int r3 = r3 * 5
            r1 = r2[r3]
            float r1 = java.lang.Math.max(r7, r1)
            r7 = r1
            r21 = r8
            goto L_0x0078
        L_0x00e8:
            r27 = r10
            java.lang.String r1 = "twoColor"
            boolean r1 = r13.equals(r1)
            if (r1 == 0) goto L_0x017f
            com.esotericsoftware.spine.Animation$TwoColorTimeline r1 = new com.esotericsoftware.spine.Animation$TwoColorTimeline
            int r2 = r11.f5631j
            r1.<init>(r2)
            int r2 = r15.index
            r1.slotIndex = r2
            com.badlogic.gdx.utils.v r2 = r11.f5627f
            r3 = 0
        L_0x0100:
            if (r2 == 0) goto L_0x0151
            java.lang.String r6 = "light"
            java.lang.String r6 = r2.h(r6)
            e.d.b.t.b r6 = e.d.b.t.b.a(r6)
            java.lang.String r10 = "dark"
            java.lang.String r10 = r2.h(r10)
            e.d.b.t.b r10 = e.d.b.t.b.a(r10)
            float r30 = r2.e(r12)
            float r13 = r6.f6934a
            float r14 = r6.f6935b
            r18 = r15
            float r15 = r6.f6936c
            float r6 = r6.f6937d
            r20 = r12
            float r12 = r10.f6934a
            r21 = r8
            float r8 = r10.f6935b
            float r10 = r10.f6936c
            r28 = r1
            r29 = r3
            r31 = r13
            r32 = r14
            r33 = r15
            r34 = r6
            r35 = r12
            r36 = r8
            r37 = r10
            r28.setFrame(r29, r30, r31, r32, r33, r34, r35, r36, r37)
            r0.readCurve(r2, r1, r3)
            r6 = 1
            int r3 = r3 + r6
            com.badlogic.gdx.utils.v r2 = r2.f5628g
            r15 = r18
            r12 = r20
            r8 = r21
            goto L_0x0100
        L_0x0151:
            r21 = r8
            r20 = r12
            r18 = r15
            r6 = 1
            r4.add(r1)
            float[] r2 = r1.getFrames()
            int r1 = r1.getFrameCount()
            int r1 = r1 - r6
            int r1 = r1 * 8
            r1 = r2[r1]
            float r1 = java.lang.Math.max(r7, r1)
            r7 = r1
        L_0x016d:
            com.badlogic.gdx.utils.v r11 = r11.f5628g
            r1 = r39
            r2 = r41
            r15 = r18
            r3 = r19
            r12 = r20
            r8 = r21
            r10 = r27
            goto L_0x002a
        L_0x017f:
            r21 = r8
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid timeline type for a slot: "
            r2.append(r3)
            r2.append(r13)
            r2.append(r9)
            java.lang.String r3 = r5.f5626e
            r2.append(r3)
            r3 = r21
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x01a5:
            r19 = r3
            com.badlogic.gdx.utils.v r5 = r5.f5628g
            r1 = r39
            r2 = r41
            goto L_0x0014
        L_0x01af:
            com.badlogic.gdx.utils.l0 r1 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r11)
            java.lang.String r3 = r5.f5626e
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x01c6:
            r19 = r3
            r3 = r8
            r27 = r10
            r20 = r12
            java.lang.String r1 = "bones"
            r2 = r39
            com.badlogic.gdx.utils.v r1 = r2.c(r1)
        L_0x01d5:
            if (r1 == 0) goto L_0x0322
            java.lang.String r6 = r1.f5626e
            r8 = r41
            com.esotericsoftware.spine.BoneData r6 = r8.findBone(r6)
            if (r6 == 0) goto L_0x0309
            com.badlogic.gdx.utils.v r10 = r1.f5627f
        L_0x01e3:
            if (r10 == 0) goto L_0x02fd
            java.lang.String r12 = r10.f5626e
            java.lang.String r13 = "rotate"
            boolean r13 = r12.equals(r13)
            if (r13 == 0) goto L_0x023c
            com.esotericsoftware.spine.Animation$RotateTimeline r12 = new com.esotericsoftware.spine.Animation$RotateTimeline
            int r13 = r10.f5631j
            r12.<init>(r13)
            int r13 = r6.index
            r12.boneIndex = r13
            com.badlogic.gdx.utils.v r13 = r10.f5627f
            r14 = 0
        L_0x01fd:
            if (r13 == 0) goto L_0x021c
            r15 = r20
            float r5 = r13.e(r15)
            r20 = r11
            java.lang.String r11 = "angle"
            float r11 = r13.e(r11)
            r12.setFrame(r14, r5, r11)
            r0.readCurve(r13, r12, r14)
            r5 = 1
            int r14 = r14 + r5
            com.badlogic.gdx.utils.v r13 = r13.f5628g
            r11 = r20
            r20 = r15
            goto L_0x01fd
        L_0x021c:
            r15 = r20
            r5 = 1
            r20 = r11
            r4.add(r12)
            float[] r11 = r12.getFrames()
            int r12 = r12.getFrameCount()
            int r12 = r12 - r5
            int r12 = r12 * 2
            r5 = r11[r12]
            float r5 = java.lang.Math.max(r7, r5)
            r21 = r3
            r7 = r5
            r22 = r6
            goto L_0x02f1
        L_0x023c:
            r15 = r20
            r20 = r11
            java.lang.String r5 = "translate"
            boolean r5 = r12.equals(r5)
            if (r5 != 0) goto L_0x027b
            java.lang.String r5 = "scale"
            boolean r5 = r12.equals(r5)
            if (r5 != 0) goto L_0x027b
            java.lang.String r5 = "shear"
            boolean r5 = r12.equals(r5)
            if (r5 == 0) goto L_0x0259
            goto L_0x027b
        L_0x0259:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Invalid timeline type for a bone: "
            r4.append(r5)
            r4.append(r12)
            r4.append(r9)
            java.lang.String r1 = r1.f5626e
            r4.append(r1)
            r4.append(r3)
            java.lang.String r1 = r4.toString()
            r2.<init>(r1)
            throw r2
        L_0x027b:
            java.lang.String r5 = "scale"
            boolean r5 = r12.equals(r5)
            if (r5 == 0) goto L_0x028d
            com.esotericsoftware.spine.Animation$ScaleTimeline r5 = new com.esotericsoftware.spine.Animation$ScaleTimeline
            int r11 = r10.f5631j
            r5.<init>(r11)
        L_0x028a:
            r11 = 1065353216(0x3f800000, float:1.0)
            goto L_0x02a6
        L_0x028d:
            java.lang.String r5 = "shear"
            boolean r5 = r12.equals(r5)
            if (r5 == 0) goto L_0x029d
            com.esotericsoftware.spine.Animation$ShearTimeline r5 = new com.esotericsoftware.spine.Animation$ShearTimeline
            int r11 = r10.f5631j
            r5.<init>(r11)
            goto L_0x028a
        L_0x029d:
            com.esotericsoftware.spine.Animation$TranslateTimeline r5 = new com.esotericsoftware.spine.Animation$TranslateTimeline
            int r11 = r10.f5631j
            r5.<init>(r11)
            r11 = r19
        L_0x02a6:
            int r12 = r6.index
            r5.boneIndex = r12
            com.badlogic.gdx.utils.v r12 = r10.f5627f
            r13 = 0
        L_0x02ad:
            if (r12 == 0) goto L_0x02d7
            java.lang.String r14 = "x"
            r21 = r3
            r3 = 0
            float r14 = r12.a(r14, r3)
            r22 = r6
            java.lang.String r6 = "y"
            float r6 = r12.a(r6, r3)
            float r3 = r12.e(r15)
            float r14 = r14 * r11
            float r6 = r6 * r11
            r5.setFrame(r13, r3, r14, r6)
            r0.readCurve(r12, r5, r13)
            r3 = 1
            int r13 = r13 + r3
            com.badlogic.gdx.utils.v r12 = r12.f5628g
            r3 = r21
            r6 = r22
            goto L_0x02ad
        L_0x02d7:
            r21 = r3
            r22 = r6
            r3 = 1
            r4.add(r5)
            float[] r6 = r5.getFrames()
            int r5 = r5.getFrameCount()
            int r5 = r5 - r3
            int r5 = r5 * 3
            r3 = r6[r5]
            float r3 = java.lang.Math.max(r7, r3)
            r7 = r3
        L_0x02f1:
            com.badlogic.gdx.utils.v r10 = r10.f5628g
            r11 = r20
            r3 = r21
            r6 = r22
            r20 = r15
            goto L_0x01e3
        L_0x02fd:
            r21 = r3
            r15 = r20
            r20 = r11
            com.badlogic.gdx.utils.v r1 = r1.f5628g
            r20 = r15
            goto L_0x01d5
        L_0x0309:
            com.badlogic.gdx.utils.l0 r2 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Bone not found: "
            r3.append(r4)
            java.lang.String r1 = r1.f5626e
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r2.<init>(r1)
            throw r2
        L_0x0322:
            r8 = r41
            r15 = r20
            r20 = r11
            java.lang.String r1 = "ik"
            com.badlogic.gdx.utils.v r1 = r2.c(r1)
        L_0x032e:
            java.lang.String r3 = "mix"
            if (r1 == 0) goto L_0x0399
            java.lang.String r5 = r1.f5626e
            com.esotericsoftware.spine.IkConstraintData r5 = r8.findIkConstraint(r5)
            com.esotericsoftware.spine.Animation$IkConstraintTimeline r6 = new com.esotericsoftware.spine.Animation$IkConstraintTimeline
            int r9 = r1.f5631j
            r6.<init>(r9)
            com.badlogic.gdx.utils.a r9 = r41.getIkConstraints()
            r10 = 1
            int r5 = r9.b(r5, r10)
            r6.ikConstraintIndex = r5
            com.badlogic.gdx.utils.v r5 = r1.f5627f
            r9 = 0
        L_0x034d:
            if (r5 == 0) goto L_0x0382
            float r30 = r5.e(r15)
            r11 = 1065353216(0x3f800000, float:1.0)
            float r31 = r5.a(r3, r11)
            java.lang.String r11 = "bendPositive"
            boolean r11 = r5.a(r11, r10)
            if (r11 == 0) goto L_0x0364
            r32 = 1
            goto L_0x0366
        L_0x0364:
            r32 = -1
        L_0x0366:
            java.lang.String r10 = "compress"
            r11 = 0
            boolean r33 = r5.a(r10, r11)
            java.lang.String r10 = "stretch"
            boolean r34 = r5.a(r10, r11)
            r28 = r6
            r29 = r9
            r28.setFrame(r29, r30, r31, r32, r33, r34)
            r0.readCurve(r5, r6, r9)
            r10 = 1
            int r9 = r9 + r10
            com.badlogic.gdx.utils.v r5 = r5.f5628g
            goto L_0x034d
        L_0x0382:
            r4.add(r6)
            float[] r3 = r6.getFrames()
            int r5 = r6.getFrameCount()
            int r5 = r5 - r10
            int r5 = r5 * 5
            r3 = r3[r5]
            float r7 = java.lang.Math.max(r7, r3)
            com.badlogic.gdx.utils.v r1 = r1.f5628g
            goto L_0x032e
        L_0x0399:
            java.lang.String r1 = "transform"
            com.badlogic.gdx.utils.v r1 = r2.c(r1)
        L_0x039f:
            if (r1 == 0) goto L_0x0403
            java.lang.String r5 = r1.f5626e
            com.esotericsoftware.spine.TransformConstraintData r5 = r8.findTransformConstraint(r5)
            com.esotericsoftware.spine.Animation$TransformConstraintTimeline r6 = new com.esotericsoftware.spine.Animation$TransformConstraintTimeline
            int r9 = r1.f5631j
            r6.<init>(r9)
            com.badlogic.gdx.utils.a r9 = r41.getTransformConstraints()
            r10 = 1
            int r5 = r9.b(r5, r10)
            r6.transformConstraintIndex = r5
            com.badlogic.gdx.utils.v r5 = r1.f5627f
            r9 = 0
        L_0x03bc:
            if (r5 == 0) goto L_0x03eb
            float r30 = r5.e(r15)
            java.lang.String r10 = "rotateMix"
            r11 = 1065353216(0x3f800000, float:1.0)
            float r31 = r5.a(r10, r11)
            java.lang.String r10 = "translateMix"
            float r32 = r5.a(r10, r11)
            java.lang.String r10 = "scaleMix"
            float r33 = r5.a(r10, r11)
            java.lang.String r10 = "shearMix"
            float r34 = r5.a(r10, r11)
            r28 = r6
            r29 = r9
            r28.setFrame(r29, r30, r31, r32, r33, r34)
            r0.readCurve(r5, r6, r9)
            r10 = 1
            int r9 = r9 + r10
            com.badlogic.gdx.utils.v r5 = r5.f5628g
            goto L_0x03bc
        L_0x03eb:
            r10 = 1
            r4.add(r6)
            float[] r5 = r6.getFrames()
            int r6 = r6.getFrameCount()
            int r6 = r6 - r10
            int r6 = r6 * 5
            r5 = r5[r6]
            float r7 = java.lang.Math.max(r7, r5)
            com.badlogic.gdx.utils.v r1 = r1.f5628g
            goto L_0x039f
        L_0x0403:
            java.lang.String r1 = "paths"
            com.badlogic.gdx.utils.v r1 = r2.c(r1)
        L_0x0409:
            if (r1 == 0) goto L_0x050c
            java.lang.String r5 = r1.f5626e
            com.esotericsoftware.spine.PathConstraintData r5 = r8.findPathConstraint(r5)
            if (r5 == 0) goto L_0x04f3
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.PathConstraintData> r6 = r8.pathConstraints
            r9 = 1
            int r6 = r6.b(r5, r9)
            com.badlogic.gdx.utils.v r9 = r1.f5627f
        L_0x041c:
            if (r9 == 0) goto L_0x04eb
            java.lang.String r10 = r9.f5626e
            java.lang.String r11 = "position"
            boolean r11 = r10.equals(r11)
            if (r11 != 0) goto L_0x0483
            java.lang.String r11 = "spacing"
            boolean r11 = r10.equals(r11)
            if (r11 == 0) goto L_0x0431
            goto L_0x0483
        L_0x0431:
            boolean r10 = r10.equals(r3)
            if (r10 == 0) goto L_0x0480
            com.esotericsoftware.spine.Animation$PathConstraintMixTimeline r10 = new com.esotericsoftware.spine.Animation$PathConstraintMixTimeline
            int r11 = r9.f5631j
            r10.<init>(r11)
            r10.pathConstraintIndex = r6
            com.badlogic.gdx.utils.v r11 = r9.f5627f
            r12 = 0
        L_0x0443:
            if (r11 == 0) goto L_0x0468
            float r13 = r11.e(r15)
            java.lang.String r14 = "rotateMix"
            r22 = r3
            r3 = 1065353216(0x3f800000, float:1.0)
            float r14 = r11.a(r14, r3)
            java.lang.String r8 = "translateMix"
            float r8 = r11.a(r8, r3)
            r10.setFrame(r12, r13, r14, r8)
            r0.readCurve(r11, r10, r12)
            r3 = 1
            int r12 = r12 + r3
            com.badlogic.gdx.utils.v r11 = r11.f5628g
            r8 = r41
            r3 = r22
            goto L_0x0443
        L_0x0468:
            r22 = r3
            r3 = 1
            r4.add(r10)
            float[] r8 = r10.getFrames()
            int r10 = r10.getFrameCount()
            int r10 = r10 - r3
            int r10 = r10 * 3
            r3 = r8[r10]
            float r7 = java.lang.Math.max(r7, r3)
            goto L_0x04e3
        L_0x0480:
            r22 = r3
            goto L_0x04e3
        L_0x0483:
            r22 = r3
            java.lang.String r3 = "spacing"
            boolean r3 = r10.equals(r3)
            if (r3 == 0) goto L_0x049f
            com.esotericsoftware.spine.Animation$PathConstraintSpacingTimeline r3 = new com.esotericsoftware.spine.Animation$PathConstraintSpacingTimeline
            int r8 = r9.f5631j
            r3.<init>(r8)
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r8 = r5.spacingMode
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r11 = com.esotericsoftware.spine.PathConstraintData.SpacingMode.length
            if (r8 == r11) goto L_0x04ac
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r11 = com.esotericsoftware.spine.PathConstraintData.SpacingMode.fixed
            if (r8 != r11) goto L_0x04af
            goto L_0x04ac
        L_0x049f:
            com.esotericsoftware.spine.Animation$PathConstraintPositionTimeline r3 = new com.esotericsoftware.spine.Animation$PathConstraintPositionTimeline
            int r8 = r9.f5631j
            r3.<init>(r8)
            com.esotericsoftware.spine.PathConstraintData$PositionMode r8 = r5.positionMode
            com.esotericsoftware.spine.PathConstraintData$PositionMode r11 = com.esotericsoftware.spine.PathConstraintData.PositionMode.fixed
            if (r8 != r11) goto L_0x04af
        L_0x04ac:
            r8 = r19
            goto L_0x04b1
        L_0x04af:
            r8 = 1065353216(0x3f800000, float:1.0)
        L_0x04b1:
            r3.pathConstraintIndex = r6
            com.badlogic.gdx.utils.v r11 = r9.f5627f
            r12 = 0
        L_0x04b6:
            if (r11 == 0) goto L_0x04ce
            float r13 = r11.e(r15)
            r14 = 0
            float r17 = r11.a(r10, r14)
            float r14 = r17 * r8
            r3.setFrame(r12, r13, r14)
            r0.readCurve(r11, r3, r12)
            r13 = 1
            int r12 = r12 + r13
            com.badlogic.gdx.utils.v r11 = r11.f5628g
            goto L_0x04b6
        L_0x04ce:
            r13 = 1
            r4.add(r3)
            float[] r8 = r3.getFrames()
            int r3 = r3.getFrameCount()
            int r3 = r3 - r13
            int r3 = r3 * 2
            r3 = r8[r3]
            float r7 = java.lang.Math.max(r7, r3)
        L_0x04e3:
            com.badlogic.gdx.utils.v r9 = r9.f5628g
            r8 = r41
            r3 = r22
            goto L_0x041c
        L_0x04eb:
            r22 = r3
            com.badlogic.gdx.utils.v r1 = r1.f5628g
            r8 = r41
            goto L_0x0409
        L_0x04f3:
            com.badlogic.gdx.utils.l0 r2 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Path constraint not found: "
            r3.append(r4)
            java.lang.String r1 = r1.f5626e
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r2.<init>(r1)
            throw r2
        L_0x050c:
            java.lang.String r1 = "deform"
            com.badlogic.gdx.utils.v r1 = r2.c(r1)
        L_0x0512:
            if (r1 == 0) goto L_0x066c
            java.lang.String r3 = r1.f5626e
            r5 = r41
            com.esotericsoftware.spine.Skin r3 = r5.findSkin(r3)
            if (r3 == 0) goto L_0x0652
            com.badlogic.gdx.utils.v r6 = r1.f5627f
        L_0x0520:
            if (r6 == 0) goto L_0x0645
            java.lang.String r8 = r6.f5626e
            com.esotericsoftware.spine.SlotData r8 = r5.findSlot(r8)
            if (r8 == 0) goto L_0x062b
            com.badlogic.gdx.utils.v r9 = r6.f5627f
        L_0x052c:
            if (r9 == 0) goto L_0x0618
            int r10 = r8.index
            java.lang.String r11 = r9.f5626e
            com.esotericsoftware.spine.attachments.Attachment r10 = r3.getAttachment(r10, r11)
            com.esotericsoftware.spine.attachments.VertexAttachment r10 = (com.esotericsoftware.spine.attachments.VertexAttachment) r10
            if (r10 == 0) goto L_0x05ff
            int[] r11 = r10.getBones()
            if (r11 == 0) goto L_0x0542
            r11 = 1
            goto L_0x0543
        L_0x0542:
            r11 = 0
        L_0x0543:
            float[] r12 = r10.getVertices()
            int r13 = r12.length
            if (r11 == 0) goto L_0x054e
            int r13 = r13 / 3
            int r13 = r13 * 2
        L_0x054e:
            com.esotericsoftware.spine.Animation$DeformTimeline r14 = new com.esotericsoftware.spine.Animation$DeformTimeline
            r17 = r3
            int r3 = r9.f5631j
            r14.<init>(r3)
            int r3 = r8.index
            r14.slotIndex = r3
            r14.attachment = r10
            com.badlogic.gdx.utils.v r3 = r9.f5627f
            r10 = 0
        L_0x0560:
            if (r3 == 0) goto L_0x05d3
            r22 = r8
            java.lang.String r8 = "vertices"
            com.badlogic.gdx.utils.v r8 = r3.a(r8)
            if (r8 != 0) goto L_0x057d
            if (r11 == 0) goto L_0x0571
            float[] r8 = new float[r13]
            goto L_0x0572
        L_0x0571:
            r8 = r12
        L_0x0572:
            r23 = r1
            r16 = r6
            r5 = r8
            r24 = r9
            r1 = 0
            r6 = 1065353216(0x3f800000, float:1.0)
            goto L_0x05b8
        L_0x057d:
            float[] r5 = new float[r13]
            java.lang.String r2 = "offset"
            r23 = r1
            r1 = 0
            int r2 = r3.a(r2, r1)
            r16 = r6
            float[] r6 = r8.e()
            r24 = r9
            int r9 = r8.f5631j
            java.lang.System.arraycopy(r6, r1, r5, r2, r9)
            r6 = 1065353216(0x3f800000, float:1.0)
            int r9 = (r19 > r6 ? 1 : (r19 == r6 ? 0 : -1))
            if (r9 == 0) goto L_0x05a9
            int r8 = r8.f5631j
            int r8 = r8 + r2
        L_0x059e:
            if (r2 >= r8) goto L_0x05a9
            r9 = r5[r2]
            float r9 = r9 * r19
            r5[r2] = r9
            int r2 = r2 + 1
            goto L_0x059e
        L_0x05a9:
            if (r11 != 0) goto L_0x05b8
            r2 = 0
        L_0x05ac:
            if (r2 >= r13) goto L_0x05b8
            r8 = r5[r2]
            r9 = r12[r2]
            float r8 = r8 + r9
            r5[r2] = r8
            int r2 = r2 + 1
            goto L_0x05ac
        L_0x05b8:
            float r2 = r3.e(r15)
            r14.setFrame(r10, r2, r5)
            r0.readCurve(r3, r14, r10)
            r2 = 1
            int r10 = r10 + r2
            com.badlogic.gdx.utils.v r3 = r3.f5628g
            r2 = r39
            r5 = r41
            r6 = r16
            r8 = r22
            r1 = r23
            r9 = r24
            goto L_0x0560
        L_0x05d3:
            r23 = r1
            r16 = r6
            r22 = r8
            r24 = r9
            r1 = 0
            r2 = 1
            r6 = 1065353216(0x3f800000, float:1.0)
            r4.add(r14)
            float[] r3 = r14.getFrames()
            int r5 = r14.getFrameCount()
            int r5 = r5 - r2
            r2 = r3[r5]
            float r7 = java.lang.Math.max(r7, r2)
            com.badlogic.gdx.utils.v r9 = r9.f5628g
            r2 = r39
            r5 = r41
            r6 = r16
            r3 = r17
            r1 = r23
            goto L_0x052c
        L_0x05ff:
            com.badlogic.gdx.utils.l0 r1 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Deform attachment not found: "
            r2.append(r3)
            java.lang.String r3 = r9.f5626e
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x0618:
            r23 = r1
            r17 = r3
            r2 = r6
            r1 = 0
            r6 = 1065353216(0x3f800000, float:1.0)
            com.badlogic.gdx.utils.v r2 = r2.f5628g
            r5 = r41
            r6 = r2
            r1 = r23
            r2 = r39
            goto L_0x0520
        L_0x062b:
            r2 = r6
            com.badlogic.gdx.utils.l0 r1 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r5 = r20
            r3.append(r5)
            java.lang.String r2 = r2.f5626e
            r3.append(r2)
            java.lang.String r2 = r3.toString()
            r1.<init>(r2)
            throw r1
        L_0x0645:
            r2 = r1
            r5 = r20
            r1 = 0
            r6 = 1065353216(0x3f800000, float:1.0)
            com.badlogic.gdx.utils.v r2 = r2.f5628g
            r1 = r2
            r2 = r39
            goto L_0x0512
        L_0x0652:
            r2 = r1
            com.badlogic.gdx.utils.l0 r1 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Skin not found: "
            r3.append(r4)
            java.lang.String r2 = r2.f5626e
            r3.append(r2)
            java.lang.String r2 = r3.toString()
            r1.<init>(r2)
            throw r1
        L_0x066c:
            r5 = r20
            r1 = 0
            java.lang.String r2 = "drawOrder"
            r3 = r39
            com.badlogic.gdx.utils.v r2 = r3.a(r2)
            if (r2 != 0) goto L_0x067f
            java.lang.String r2 = "draworder"
            com.badlogic.gdx.utils.v r2 = r3.a(r2)
        L_0x067f:
            if (r2 == 0) goto L_0x0749
            com.esotericsoftware.spine.Animation$DrawOrderTimeline r6 = new com.esotericsoftware.spine.Animation$DrawOrderTimeline
            int r8 = r2.f5631j
            r6.<init>(r8)
            r8 = r41
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.SlotData> r9 = r8.slots
            int r9 = r9.f5374b
            com.badlogic.gdx.utils.v r2 = r2.f5627f
            r10 = 0
        L_0x0691:
            if (r2 == 0) goto L_0x0735
            r11 = 0
            java.lang.String r12 = "offsets"
            com.badlogic.gdx.utils.v r12 = r2.a(r12)
            if (r12 == 0) goto L_0x0723
            int[] r11 = new int[r9]
            int r13 = r9 + -1
            r14 = r13
        L_0x06a1:
            if (r14 < 0) goto L_0x06aa
            r16 = -1
            r11[r14] = r16
            int r14 = r14 + -1
            goto L_0x06a1
        L_0x06aa:
            int r14 = r12.f5631j
            int r14 = r9 - r14
            int[] r14 = new int[r14]
            com.badlogic.gdx.utils.v r12 = r12.f5627f
            r16 = 0
            r17 = 0
        L_0x06b6:
            if (r12 == 0) goto L_0x0703
            java.lang.String r1 = "slot"
            java.lang.String r1 = r12.h(r1)
            com.esotericsoftware.spine.SlotData r1 = r8.findSlot(r1)
            if (r1 == 0) goto L_0x06e8
            r19 = r13
        L_0x06c6:
            r0 = r16
            int r13 = r1.index
            if (r0 == r13) goto L_0x06d5
            int r13 = r17 + 1
            int r16 = r0 + 1
            r14[r17] = r0
            r17 = r13
            goto L_0x06c6
        L_0x06d5:
            java.lang.String r1 = "offset"
            int r1 = r12.f(r1)
            int r1 = r1 + r0
            int r16 = r0 + 1
            r11[r1] = r0
            com.badlogic.gdx.utils.v r12 = r12.f5628g
            r1 = 0
            r0 = r38
            r13 = r19
            goto L_0x06b6
        L_0x06e8:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r5)
            java.lang.String r2 = "slot"
            java.lang.String r2 = r12.h(r2)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0703:
            r19 = r13
            r0 = r16
        L_0x0707:
            if (r0 >= r9) goto L_0x0713
            int r1 = r17 + 1
            int r12 = r0 + 1
            r14[r17] = r0
            r17 = r1
            r0 = r12
            goto L_0x0707
        L_0x0713:
            if (r19 < 0) goto L_0x0723
            r0 = r11[r19]
            r1 = -1
            if (r0 != r1) goto L_0x0720
            int r17 = r17 + -1
            r0 = r14[r17]
            r11[r19] = r0
        L_0x0720:
            int r19 = r19 + -1
            goto L_0x0713
        L_0x0723:
            r1 = -1
            int r0 = r10 + 1
            float r12 = r2.e(r15)
            r6.setFrame(r10, r12, r11)
            com.badlogic.gdx.utils.v r2 = r2.f5628g
            r1 = 0
            r10 = r0
            r0 = r38
            goto L_0x0691
        L_0x0735:
            r4.add(r6)
            float[] r0 = r6.getFrames()
            int r1 = r6.getFrameCount()
            r2 = 1
            int r1 = r1 - r2
            r0 = r0[r1]
            float r7 = java.lang.Math.max(r7, r0)
            goto L_0x074b
        L_0x0749:
            r8 = r41
        L_0x074b:
            java.lang.String r0 = "events"
            com.badlogic.gdx.utils.v r0 = r3.a(r0)
            if (r0 == 0) goto L_0x07e7
            com.esotericsoftware.spine.Animation$EventTimeline r1 = new com.esotericsoftware.spine.Animation$EventTimeline
            int r2 = r0.f5631j
            r1.<init>(r2)
            com.badlogic.gdx.utils.v r0 = r0.f5627f
            r2 = 0
        L_0x075d:
            if (r0 == 0) goto L_0x07d4
            r3 = r27
            java.lang.String r5 = r0.h(r3)
            com.esotericsoftware.spine.EventData r5 = r8.findEvent(r5)
            if (r5 == 0) goto L_0x07b9
            com.esotericsoftware.spine.Event r6 = new com.esotericsoftware.spine.Event
            float r9 = r0.e(r15)
            r6.<init>(r9, r5)
            int r9 = r5.intValue
            java.lang.String r10 = "int"
            int r9 = r0.a(r10, r9)
            r6.intValue = r9
            float r9 = r5.floatValue
            java.lang.String r10 = "float"
            float r9 = r0.a(r10, r9)
            r6.floatValue = r9
            java.lang.String r9 = r5.stringValue
            java.lang.String r10 = "string"
            java.lang.String r9 = r0.a(r10, r9)
            r6.stringValue = r9
            com.esotericsoftware.spine.EventData r9 = r6.getData()
            java.lang.String r9 = r9.audioPath
            if (r9 == 0) goto L_0x07ae
            float r9 = r5.volume
            java.lang.String r10 = "volume"
            float r9 = r0.a(r10, r9)
            r6.volume = r9
            float r5 = r5.balance
            java.lang.String r9 = "balance"
            float r5 = r0.a(r9, r5)
            r6.balance = r5
        L_0x07ae:
            int r5 = r2 + 1
            r1.setFrame(r2, r6)
            com.badlogic.gdx.utils.v r0 = r0.f5628g
            r27 = r3
            r2 = r5
            goto L_0x075d
        L_0x07b9:
            com.badlogic.gdx.utils.l0 r1 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "Event not found: "
            r2.append(r4)
            java.lang.String r0 = r0.h(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            throw r1
        L_0x07d4:
            r4.add(r1)
            float[] r0 = r1.getFrames()
            int r1 = r1.getFrameCount()
            r2 = 1
            int r1 = r1 - r2
            r0 = r0[r1]
            float r7 = java.lang.Math.max(r7, r0)
        L_0x07e7:
            r4.c()
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Animation> r0 = r8.animations
            com.esotericsoftware.spine.Animation r1 = new com.esotericsoftware.spine.Animation
            r2 = r40
            r1.<init>(r2, r4, r7)
            r0.add(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.SkeletonJson.readAnimation(com.badlogic.gdx.utils.v, java.lang.String, com.esotericsoftware.spine.SkeletonData):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.v.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.badlogic.gdx.utils.v.a(int, com.badlogic.gdx.utils.r0):void
      com.badlogic.gdx.utils.v.a(java.lang.String, float):float
      com.badlogic.gdx.utils.v.a(java.lang.String, int):int
      com.badlogic.gdx.utils.v.a(com.badlogic.gdx.utils.w$c, int):java.lang.String
      com.badlogic.gdx.utils.v.a(java.lang.String, java.lang.String):java.lang.String
      com.badlogic.gdx.utils.v.a(double, java.lang.String):void
      com.badlogic.gdx.utils.v.a(long, java.lang.String):void
      com.badlogic.gdx.utils.v.a(java.lang.String, com.badlogic.gdx.utils.v):void
      com.badlogic.gdx.utils.v.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.v.a(java.lang.String, float):float
     arg types: [java.lang.String, int]
     candidates:
      com.badlogic.gdx.utils.v.a(int, com.badlogic.gdx.utils.r0):void
      com.badlogic.gdx.utils.v.a(java.lang.String, int):int
      com.badlogic.gdx.utils.v.a(com.badlogic.gdx.utils.w$c, int):java.lang.String
      com.badlogic.gdx.utils.v.a(java.lang.String, java.lang.String):java.lang.String
      com.badlogic.gdx.utils.v.a(double, java.lang.String):void
      com.badlogic.gdx.utils.v.a(long, java.lang.String):void
      com.badlogic.gdx.utils.v.a(java.lang.String, com.badlogic.gdx.utils.v):void
      com.badlogic.gdx.utils.v.a(java.lang.String, boolean):boolean
      com.badlogic.gdx.utils.v.a(java.lang.String, float):float */
    private Attachment readAttachment(v vVar, Skin skin, int i2, String str, SkeletonData skeletonData) {
        v vVar2 = vVar;
        Skin skin2 = skin;
        float f2 = this.scale;
        String a2 = vVar2.a("name", str);
        switch (AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$attachments$AttachmentType[AttachmentType.valueOf(vVar2.a("type", AttachmentType.region.name())).ordinal()]) {
            case 1:
                String a3 = vVar2.a("path", a2);
                RegionAttachment newRegionAttachment = this.attachmentLoader.newRegionAttachment(skin2, a2, a3);
                if (newRegionAttachment == null) {
                    return null;
                }
                newRegionAttachment.setPath(a3);
                newRegionAttachment.setX(vVar2.a("x", (float) Animation.CurveTimeline.LINEAR) * f2);
                newRegionAttachment.setY(vVar2.a("y", (float) Animation.CurveTimeline.LINEAR) * f2);
                newRegionAttachment.setScaleX(vVar2.a("scaleX", 1.0f));
                newRegionAttachment.setScaleY(vVar2.a("scaleY", 1.0f));
                newRegionAttachment.setRotation(vVar2.a("rotation", (float) Animation.CurveTimeline.LINEAR));
                newRegionAttachment.setWidth(vVar2.e("width") * f2);
                newRegionAttachment.setHeight(vVar2.e("height") * f2);
                String a4 = vVar2.a("color", (String) null);
                if (a4 != null) {
                    newRegionAttachment.getColor().b(b.a(a4));
                }
                newRegionAttachment.updateOffset();
                return newRegionAttachment;
            case 2:
                BoundingBoxAttachment newBoundingBoxAttachment = this.attachmentLoader.newBoundingBoxAttachment(skin2, a2);
                if (newBoundingBoxAttachment == null) {
                    return null;
                }
                readVertices(vVar2, newBoundingBoxAttachment, vVar2.f("vertexCount") << 1);
                String a5 = vVar2.a("color", (String) null);
                if (a5 != null) {
                    newBoundingBoxAttachment.getColor().b(b.a(a5));
                }
                return newBoundingBoxAttachment;
            case 3:
            case 4:
                String a6 = vVar2.a("path", a2);
                MeshAttachment newMeshAttachment = this.attachmentLoader.newMeshAttachment(skin2, a2, a6);
                if (newMeshAttachment == null) {
                    return null;
                }
                newMeshAttachment.setPath(a6);
                String a7 = vVar2.a("color", (String) null);
                if (a7 != null) {
                    newMeshAttachment.getColor().b(b.a(a7));
                }
                newMeshAttachment.setWidth(vVar2.a("width", (float) Animation.CurveTimeline.LINEAR) * f2);
                newMeshAttachment.setHeight(vVar2.a("height", (float) Animation.CurveTimeline.LINEAR) * f2);
                String a8 = vVar2.a("parent", (String) null);
                if (a8 != null) {
                    newMeshAttachment.setInheritDeform(vVar2.a("deform", true));
                    this.linkedMeshes.add(new LinkedMesh(newMeshAttachment, vVar2.a("skin", (String) null), i2, a8));
                    return newMeshAttachment;
                }
                float[] e2 = vVar2.k("uvs").e();
                readVertices(vVar2, newMeshAttachment, e2.length);
                newMeshAttachment.setTriangles(vVar2.k("triangles").j());
                newMeshAttachment.setRegionUVs(e2);
                newMeshAttachment.updateUVs();
                if (vVar2.i("hull")) {
                    newMeshAttachment.setHullLength(vVar2.k("hull").f() * 2);
                }
                if (vVar2.i("edges")) {
                    newMeshAttachment.setEdges(vVar2.k("edges").j());
                }
                return newMeshAttachment;
            case 5:
                PathAttachment newPathAttachment = this.attachmentLoader.newPathAttachment(skin2, a2);
                if (newPathAttachment == null) {
                    return null;
                }
                int i3 = 0;
                newPathAttachment.setClosed(vVar2.a("closed", false));
                newPathAttachment.setConstantSpeed(vVar2.a("constantSpeed", true));
                int f3 = vVar2.f("vertexCount");
                readVertices(vVar2, newPathAttachment, f3 << 1);
                float[] fArr = new float[(f3 / 3)];
                v vVar3 = vVar2.k("lengths").f5627f;
                while (vVar3 != null) {
                    fArr[i3] = vVar3.d() * f2;
                    vVar3 = vVar3.f5628g;
                    i3++;
                }
                newPathAttachment.setLengths(fArr);
                String a9 = vVar2.a("color", (String) null);
                if (a9 != null) {
                    newPathAttachment.getColor().b(b.a(a9));
                }
                return newPathAttachment;
            case 6:
                PointAttachment newPointAttachment = this.attachmentLoader.newPointAttachment(skin2, a2);
                if (newPointAttachment == null) {
                    return null;
                }
                newPointAttachment.setX(vVar2.a("x", (float) Animation.CurveTimeline.LINEAR) * f2);
                newPointAttachment.setY(vVar2.a("y", (float) Animation.CurveTimeline.LINEAR) * f2);
                newPointAttachment.setRotation(vVar2.a("rotation", (float) Animation.CurveTimeline.LINEAR));
                String a10 = vVar2.a("color", (String) null);
                if (a10 != null) {
                    newPointAttachment.getColor().b(b.a(a10));
                }
                return newPointAttachment;
            case 7:
                ClippingAttachment newClippingAttachment = this.attachmentLoader.newClippingAttachment(skin2, a2);
                if (newClippingAttachment == null) {
                    return null;
                }
                String a11 = vVar2.a("end", (String) null);
                if (a11 != null) {
                    SlotData findSlot = skeletonData.findSlot(a11);
                    if (findSlot != null) {
                        newClippingAttachment.setEndSlot(findSlot);
                    } else {
                        throw new l0("Clipping end slot not found: " + a11);
                    }
                }
                readVertices(vVar2, newClippingAttachment, vVar2.f("vertexCount") << 1);
                String a12 = vVar2.a("color", (String) null);
                if (a12 != null) {
                    newClippingAttachment.getColor().b(b.a(a12));
                }
                return newClippingAttachment;
            default:
                return null;
        }
    }

    private void readVertices(v vVar, VertexAttachment vertexAttachment, int i2) {
        vertexAttachment.setWorldVerticesLength(i2);
        float[] e2 = vVar.k("vertices").e();
        int i3 = 0;
        if (i2 == e2.length) {
            if (this.scale != 1.0f) {
                int length = e2.length;
                while (i3 < length) {
                    e2[i3] = e2[i3] * this.scale;
                    i3++;
                }
            }
            vertexAttachment.setVertices(e2);
            return;
        }
        int i4 = i2 * 3;
        m mVar = new m(i4 * 3);
        com.badlogic.gdx.utils.q qVar = new com.badlogic.gdx.utils.q(i4);
        int length2 = e2.length;
        while (i3 < length2) {
            int i5 = i3 + 1;
            int i6 = (int) e2[i3];
            qVar.a(i6);
            int i7 = (i6 * 4) + i5;
            while (i5 < i7) {
                qVar.a((int) e2[i5]);
                mVar.a(e2[i5 + 1] * this.scale);
                mVar.a(e2[i5 + 2] * this.scale);
                mVar.a(e2[i5 + 3]);
                i5 += 4;
            }
            i3 = i5;
        }
        vertexAttachment.setBones(qVar.c());
        vertexAttachment.setVertices(mVar.d());
    }

    public float getScale() {
        return this.scale;
    }

    /* access modifiers changed from: protected */
    public v parse(e.d.b.s.a aVar) {
        return new u().a(aVar);
    }

    /* access modifiers changed from: package-private */
    public void readCurve(v vVar, Animation.CurveTimeline curveTimeline, int i2) {
        v a2 = vVar.a("curve");
        if (a2 != null) {
            if (a2.u() && a2.k().equals("stepped")) {
                curveTimeline.setStepped(i2);
            } else if (a2.n()) {
                curveTimeline.setCurve(i2, a2.getFloat(0), a2.getFloat(1), a2.getFloat(2), a2.getFloat(3));
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v8, resolved type: com.esotericsoftware.spine.BoneData} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v10, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v9, resolved type: com.esotericsoftware.spine.BoneData} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v10, resolved type: com.esotericsoftware.spine.BoneData} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.v.a(java.lang.String, float):float
     arg types: [java.lang.String, int]
     candidates:
      com.badlogic.gdx.utils.v.a(int, com.badlogic.gdx.utils.r0):void
      com.badlogic.gdx.utils.v.a(java.lang.String, int):int
      com.badlogic.gdx.utils.v.a(com.badlogic.gdx.utils.w$c, int):java.lang.String
      com.badlogic.gdx.utils.v.a(java.lang.String, java.lang.String):java.lang.String
      com.badlogic.gdx.utils.v.a(double, java.lang.String):void
      com.badlogic.gdx.utils.v.a(long, java.lang.String):void
      com.badlogic.gdx.utils.v.a(java.lang.String, com.badlogic.gdx.utils.v):void
      com.badlogic.gdx.utils.v.a(java.lang.String, boolean):boolean
      com.badlogic.gdx.utils.v.a(java.lang.String, float):float */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.v.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.badlogic.gdx.utils.v.a(int, com.badlogic.gdx.utils.r0):void
      com.badlogic.gdx.utils.v.a(java.lang.String, float):float
      com.badlogic.gdx.utils.v.a(java.lang.String, int):int
      com.badlogic.gdx.utils.v.a(com.badlogic.gdx.utils.w$c, int):java.lang.String
      com.badlogic.gdx.utils.v.a(java.lang.String, java.lang.String):java.lang.String
      com.badlogic.gdx.utils.v.a(double, java.lang.String):void
      com.badlogic.gdx.utils.v.a(long, java.lang.String):void
      com.badlogic.gdx.utils.v.a(java.lang.String, com.badlogic.gdx.utils.v):void
      com.badlogic.gdx.utils.v.a(java.lang.String, boolean):boolean */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.esotericsoftware.spine.SkeletonData readSkeletonData(e.d.b.s.a r20) {
        /*
            r19 = this;
            r7 = r19
            if (r20 == 0) goto L_0x05a7
            float r0 = r7.scale
            com.esotericsoftware.spine.SkeletonData r8 = new com.esotericsoftware.spine.SkeletonData
            r8.<init>()
            java.lang.String r1 = r20.h()
            r8.name = r1
            com.badlogic.gdx.utils.v r9 = r19.parse(r20)
            java.lang.String r1 = "skeleton"
            com.badlogic.gdx.utils.v r1 = r9.a(r1)
            java.lang.String r10 = "audio"
            r11 = 0
            r12 = 0
            if (r1 == 0) goto L_0x0059
            java.lang.String r2 = "hash"
            java.lang.String r2 = r1.a(r2, r11)
            r8.hash = r2
            java.lang.String r2 = "spine"
            java.lang.String r2 = r1.a(r2, r11)
            r8.version = r2
            java.lang.String r2 = "width"
            float r2 = r1.a(r2, r12)
            r8.width = r2
            java.lang.String r2 = "height"
            float r2 = r1.a(r2, r12)
            r8.height = r2
            r2 = 1106247680(0x41f00000, float:30.0)
            java.lang.String r3 = "fps"
            float r2 = r1.a(r3, r2)
            r8.fps = r2
            java.lang.String r2 = "images"
            java.lang.String r2 = r1.a(r2, r11)
            r8.imagesPath = r2
            java.lang.String r1 = r1.a(r10, r11)
            r8.audioPath = r1
        L_0x0059:
            java.lang.String r1 = "bones"
            com.badlogic.gdx.utils.v r2 = r9.c(r1)
        L_0x005f:
            java.lang.String r3 = "scaleX"
            java.lang.String r4 = "y"
            java.lang.String r5 = "x"
            java.lang.String r6 = "length"
            java.lang.String r13 = "rotation"
            java.lang.String r14 = "name"
            if (r2 == 0) goto L_0x0113
            java.lang.String r15 = "parent"
            java.lang.String r15 = r2.a(r15, r11)
            if (r15 == 0) goto L_0x0095
            com.esotericsoftware.spine.BoneData r16 = r8.findBone(r15)
            if (r16 == 0) goto L_0x007e
            r15 = r16
            goto L_0x0096
        L_0x007e:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Parent bone not found: "
            r1.append(r2)
            r1.append(r15)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0095:
            r15 = r11
        L_0x0096:
            com.esotericsoftware.spine.BoneData r11 = new com.esotericsoftware.spine.BoneData
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.BoneData> r12 = r8.bones
            int r12 = r12.f5374b
            java.lang.String r14 = r2.h(r14)
            r11.<init>(r12, r14, r15)
            r12 = 0
            float r6 = r2.a(r6, r12)
            float r6 = r6 * r0
            r11.length = r6
            float r5 = r2.a(r5, r12)
            float r5 = r5 * r0
            r11.x = r5
            float r4 = r2.a(r4, r12)
            float r4 = r4 * r0
            r11.y = r4
            float r4 = r2.a(r13, r12)
            r11.rotation = r4
            r4 = 1065353216(0x3f800000, float:1.0)
            float r3 = r2.a(r3, r4)
            r11.scaleX = r3
            java.lang.String r3 = "scaleY"
            float r3 = r2.a(r3, r4)
            r11.scaleY = r3
            java.lang.String r3 = "shearX"
            float r3 = r2.a(r3, r12)
            r11.shearX = r3
            java.lang.String r3 = "shearY"
            float r3 = r2.a(r3, r12)
            r11.shearY = r3
            com.esotericsoftware.spine.BoneData$TransformMode r3 = com.esotericsoftware.spine.BoneData.TransformMode.normal
            java.lang.String r3 = r3.name()
            java.lang.String r4 = "transform"
            java.lang.String r3 = r2.a(r4, r3)
            com.esotericsoftware.spine.BoneData$TransformMode r3 = com.esotericsoftware.spine.BoneData.TransformMode.valueOf(r3)
            r11.transformMode = r3
            java.lang.String r3 = "color"
            r4 = 0
            java.lang.String r3 = r2.a(r3, r4)
            if (r3 == 0) goto L_0x0108
            e.d.b.t.b r4 = r11.getColor()
            e.d.b.t.b r3 = e.d.b.t.b.a(r3)
            r4.b(r3)
        L_0x0108:
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.BoneData> r3 = r8.bones
            r3.add(r11)
            com.badlogic.gdx.utils.v r2 = r2.f5628g
            r11 = 0
            r12 = 0
            goto L_0x005f
        L_0x0113:
            java.lang.String r2 = "slots"
            com.badlogic.gdx.utils.v r2 = r9.c(r2)
        L_0x0119:
            if (r2 == 0) goto L_0x0194
            java.lang.String r11 = r2.h(r14)
            java.lang.String r12 = "bone"
            java.lang.String r12 = r2.h(r12)
            com.esotericsoftware.spine.BoneData r15 = r8.findBone(r12)
            if (r15 == 0) goto L_0x017d
            com.esotericsoftware.spine.SlotData r12 = new com.esotericsoftware.spine.SlotData
            r17 = r10
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.SlotData> r10 = r8.slots
            int r10 = r10.f5374b
            r12.<init>(r10, r11, r15)
            java.lang.String r10 = "color"
            r11 = 0
            java.lang.String r10 = r2.a(r10, r11)
            if (r10 == 0) goto L_0x014a
            e.d.b.t.b r15 = r12.getColor()
            e.d.b.t.b r10 = e.d.b.t.b.a(r10)
            r15.b(r10)
        L_0x014a:
            java.lang.String r10 = "dark"
            java.lang.String r10 = r2.a(r10, r11)
            if (r10 == 0) goto L_0x0159
            e.d.b.t.b r10 = e.d.b.t.b.a(r10)
            r12.setDarkColor(r10)
        L_0x0159:
            java.lang.String r10 = "attachment"
            java.lang.String r10 = r2.a(r10, r11)
            r12.attachmentName = r10
            com.esotericsoftware.spine.BlendMode r10 = com.esotericsoftware.spine.BlendMode.normal
            java.lang.String r10 = r10.name()
            java.lang.String r11 = "blend"
            java.lang.String r10 = r2.a(r11, r10)
            com.esotericsoftware.spine.BlendMode r10 = com.esotericsoftware.spine.BlendMode.valueOf(r10)
            r12.blendMode = r10
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.SlotData> r10 = r8.slots
            r10.add(r12)
            com.badlogic.gdx.utils.v r2 = r2.f5628g
            r10 = r17
            goto L_0x0119
        L_0x017d:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Slot bone not found: "
            r1.append(r2)
            r1.append(r12)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0194:
            r17 = r10
            java.lang.String r2 = "ik"
            com.badlogic.gdx.utils.v r2 = r9.c(r2)
        L_0x019c:
            java.lang.String r10 = "target"
            java.lang.String r11 = "order"
            r12 = 0
            if (r2 == 0) goto L_0x0242
            com.esotericsoftware.spine.IkConstraintData r15 = new com.esotericsoftware.spine.IkConstraintData
            java.lang.String r7 = r2.h(r14)
            r15.<init>(r7)
            int r7 = r2.a(r11, r12)
            r15.order = r7
            com.badlogic.gdx.utils.v r7 = r2.c(r1)
        L_0x01b6:
            if (r7 == 0) goto L_0x01e2
            java.lang.String r11 = r7.k()
            com.esotericsoftware.spine.BoneData r12 = r8.findBone(r11)
            if (r12 == 0) goto L_0x01cb
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.BoneData> r11 = r15.bones
            r11.add(r12)
            com.badlogic.gdx.utils.v r7 = r7.f5628g
            r12 = 0
            goto L_0x01b6
        L_0x01cb:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "IK bone not found: "
            r1.append(r2)
            r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x01e2:
            java.lang.String r7 = r2.h(r10)
            com.esotericsoftware.spine.BoneData r10 = r8.findBone(r7)
            r15.target = r10
            com.esotericsoftware.spine.BoneData r10 = r15.target
            if (r10 == 0) goto L_0x022b
            java.lang.String r7 = "mix"
            r10 = 1065353216(0x3f800000, float:1.0)
            float r7 = r2.a(r7, r10)
            r15.mix = r7
            r7 = 1
            java.lang.String r10 = "bendPositive"
            boolean r10 = r2.a(r10, r7)
            if (r10 == 0) goto L_0x0204
            goto L_0x0205
        L_0x0204:
            r7 = -1
        L_0x0205:
            r15.bendDirection = r7
            java.lang.String r7 = "compress"
            r10 = 0
            boolean r7 = r2.a(r7, r10)
            r15.compress = r7
            java.lang.String r7 = "stretch"
            boolean r7 = r2.a(r7, r10)
            r15.stretch = r7
            java.lang.String r7 = "uniform"
            boolean r7 = r2.a(r7, r10)
            r15.uniform = r7
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.IkConstraintData> r7 = r8.ikConstraints
            r7.add(r15)
            com.badlogic.gdx.utils.v r2 = r2.f5628g
            r7 = r19
            goto L_0x019c
        L_0x022b:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "IK target bone not found: "
            r1.append(r2)
            r1.append(r7)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0242:
            java.lang.String r2 = "transform"
            com.badlogic.gdx.utils.v r2 = r9.c(r2)
        L_0x0248:
            if (r2 == 0) goto L_0x031f
            com.esotericsoftware.spine.TransformConstraintData r7 = new com.esotericsoftware.spine.TransformConstraintData
            java.lang.String r12 = r2.h(r14)
            r7.<init>(r12)
            r12 = 0
            int r15 = r2.a(r11, r12)
            r7.order = r15
            com.badlogic.gdx.utils.v r12 = r2.c(r1)
        L_0x025e:
            if (r12 == 0) goto L_0x028d
            java.lang.String r15 = r12.k()
            r18 = r6
            com.esotericsoftware.spine.BoneData r6 = r8.findBone(r15)
            if (r6 == 0) goto L_0x0276
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.BoneData> r15 = r7.bones
            r15.add(r6)
            com.badlogic.gdx.utils.v r12 = r12.f5628g
            r6 = r18
            goto L_0x025e
        L_0x0276:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Transform constraint bone not found: "
            r1.append(r2)
            r1.append(r15)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x028d:
            r18 = r6
            java.lang.String r6 = r2.h(r10)
            com.esotericsoftware.spine.BoneData r12 = r8.findBone(r6)
            r7.target = r12
            com.esotericsoftware.spine.BoneData r12 = r7.target
            if (r12 == 0) goto L_0x0308
            java.lang.String r6 = "local"
            r12 = 0
            boolean r6 = r2.a(r6, r12)
            r7.local = r6
            java.lang.String r6 = "relative"
            boolean r6 = r2.a(r6, r12)
            r7.relative = r6
            r6 = 0
            float r12 = r2.a(r13, r6)
            r7.offsetRotation = r12
            float r12 = r2.a(r5, r6)
            float r12 = r12 * r0
            r7.offsetX = r12
            float r12 = r2.a(r4, r6)
            float r12 = r12 * r0
            r7.offsetY = r12
            float r12 = r2.a(r3, r6)
            r7.offsetScaleX = r12
            java.lang.String r12 = "scaleY"
            float r12 = r2.a(r12, r6)
            r7.offsetScaleY = r12
            java.lang.String r12 = "shearY"
            float r12 = r2.a(r12, r6)
            r7.offsetShearY = r12
            java.lang.String r6 = "rotateMix"
            r12 = 1065353216(0x3f800000, float:1.0)
            float r6 = r2.a(r6, r12)
            r7.rotateMix = r6
            java.lang.String r6 = "translateMix"
            float r6 = r2.a(r6, r12)
            r7.translateMix = r6
            java.lang.String r6 = "scaleMix"
            float r6 = r2.a(r6, r12)
            r7.scaleMix = r6
            java.lang.String r6 = "shearMix"
            float r6 = r2.a(r6, r12)
            r7.shearMix = r6
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.TransformConstraintData> r6 = r8.transformConstraints
            r6.add(r7)
            com.badlogic.gdx.utils.v r2 = r2.f5628g
            r6 = r18
            goto L_0x0248
        L_0x0308:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Transform constraint target bone not found: "
            r1.append(r2)
            r1.append(r6)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x031f:
            r18 = r6
            java.lang.String r2 = "path"
            com.badlogic.gdx.utils.v r2 = r9.c(r2)
        L_0x0327:
            if (r2 == 0) goto L_0x0407
            com.esotericsoftware.spine.PathConstraintData r3 = new com.esotericsoftware.spine.PathConstraintData
            java.lang.String r4 = r2.h(r14)
            r3.<init>(r4)
            r4 = 0
            int r5 = r2.a(r11, r4)
            r3.order = r5
            com.badlogic.gdx.utils.v r4 = r2.c(r1)
        L_0x033d:
            if (r4 == 0) goto L_0x0368
            java.lang.String r5 = r4.k()
            com.esotericsoftware.spine.BoneData r6 = r8.findBone(r5)
            if (r6 == 0) goto L_0x0351
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.BoneData> r5 = r3.bones
            r5.add(r6)
            com.badlogic.gdx.utils.v r4 = r4.f5628g
            goto L_0x033d
        L_0x0351:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Path bone not found: "
            r1.append(r2)
            r1.append(r5)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0368:
            java.lang.String r4 = r2.h(r10)
            com.esotericsoftware.spine.SlotData r5 = r8.findSlot(r4)
            r3.target = r5
            com.esotericsoftware.spine.SlotData r5 = r3.target
            if (r5 == 0) goto L_0x03f0
            java.lang.String r4 = "positionMode"
            java.lang.String r5 = "percent"
            java.lang.String r4 = r2.a(r4, r5)
            com.esotericsoftware.spine.PathConstraintData$PositionMode r4 = com.esotericsoftware.spine.PathConstraintData.PositionMode.valueOf(r4)
            r3.positionMode = r4
            java.lang.String r4 = "spacingMode"
            r5 = r18
            java.lang.String r4 = r2.a(r4, r5)
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r4 = com.esotericsoftware.spine.PathConstraintData.SpacingMode.valueOf(r4)
            r3.spacingMode = r4
            java.lang.String r4 = "rotateMode"
            java.lang.String r6 = "tangent"
            java.lang.String r4 = r2.a(r4, r6)
            com.esotericsoftware.spine.PathConstraintData$RotateMode r4 = com.esotericsoftware.spine.PathConstraintData.RotateMode.valueOf(r4)
            r3.rotateMode = r4
            r4 = 0
            float r6 = r2.a(r13, r4)
            r3.offsetRotation = r6
            java.lang.String r6 = "position"
            float r6 = r2.a(r6, r4)
            r3.position = r6
            com.esotericsoftware.spine.PathConstraintData$PositionMode r6 = r3.positionMode
            com.esotericsoftware.spine.PathConstraintData$PositionMode r7 = com.esotericsoftware.spine.PathConstraintData.PositionMode.fixed
            if (r6 != r7) goto L_0x03bb
            float r6 = r3.position
            float r6 = r6 * r0
            r3.position = r6
        L_0x03bb:
            java.lang.String r6 = "spacing"
            float r6 = r2.a(r6, r4)
            r3.spacing = r6
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r4 = r3.spacingMode
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r6 = com.esotericsoftware.spine.PathConstraintData.SpacingMode.length
            if (r4 == r6) goto L_0x03cd
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r6 = com.esotericsoftware.spine.PathConstraintData.SpacingMode.fixed
            if (r4 != r6) goto L_0x03d3
        L_0x03cd:
            float r4 = r3.spacing
            float r4 = r4 * r0
            r3.spacing = r4
        L_0x03d3:
            java.lang.String r4 = "rotateMix"
            r6 = 1065353216(0x3f800000, float:1.0)
            float r4 = r2.a(r4, r6)
            r3.rotateMix = r4
            java.lang.String r4 = "translateMix"
            float r4 = r2.a(r4, r6)
            r3.translateMix = r4
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.PathConstraintData> r4 = r8.pathConstraints
            r4.add(r3)
            com.badlogic.gdx.utils.v r2 = r2.f5628g
            r18 = r5
            goto L_0x0327
        L_0x03f0:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Path target slot not found: "
            r1.append(r2)
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0407:
            java.lang.String r0 = "skins"
            com.badlogic.gdx.utils.v r0 = r9.c(r0)
        L_0x040d:
            if (r0 == 0) goto L_0x0494
            com.esotericsoftware.spine.Skin r7 = new com.esotericsoftware.spine.Skin
            java.lang.String r1 = r0.f5626e
            r7.<init>(r1)
            com.badlogic.gdx.utils.v r1 = r0.f5627f
            r10 = r1
        L_0x0419:
            if (r10 == 0) goto L_0x047f
            java.lang.String r1 = r10.f5626e
            com.esotericsoftware.spine.SlotData r11 = r8.findSlot(r1)
            if (r11 == 0) goto L_0x0466
            com.badlogic.gdx.utils.v r1 = r10.f5627f
            r12 = r1
        L_0x0426:
            if (r12 == 0) goto L_0x0463
            int r4 = r11.index     // Catch:{ all -> 0x0441 }
            java.lang.String r5 = r12.f5626e     // Catch:{ all -> 0x0441 }
            r1 = r19
            r2 = r12
            r3 = r7
            r6 = r8
            com.esotericsoftware.spine.attachments.Attachment r1 = r1.readAttachment(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0441 }
            if (r1 == 0) goto L_0x043e
            int r2 = r11.index     // Catch:{ all -> 0x0441 }
            java.lang.String r3 = r12.f5626e     // Catch:{ all -> 0x0441 }
            r7.addAttachment(r2, r3, r1)     // Catch:{ all -> 0x0441 }
        L_0x043e:
            com.badlogic.gdx.utils.v r12 = r12.f5628g
            goto L_0x0426
        L_0x0441:
            r0 = move-exception
            com.badlogic.gdx.utils.l0 r1 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Error reading attachment: "
            r2.append(r3)
            java.lang.String r3 = r12.f5626e
            r2.append(r3)
            java.lang.String r3 = ", skin: "
            r2.append(r3)
            r2.append(r7)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x0463:
            com.badlogic.gdx.utils.v r10 = r10.f5628g
            goto L_0x0419
        L_0x0466:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Slot not found: "
            r1.append(r2)
            java.lang.String r2 = r10.f5626e
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x047f:
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Skin> r1 = r8.skins
            r1.add(r7)
            java.lang.String r1 = r7.name
            java.lang.String r2 = "default"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0490
            r8.defaultSkin = r7
        L_0x0490:
            com.badlogic.gdx.utils.v r0 = r0.f5628g
            goto L_0x040d
        L_0x0494:
            r1 = r19
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.SkeletonJson$LinkedMesh> r0 = r1.linkedMeshes
            int r0 = r0.f5374b
            r2 = 0
        L_0x049b:
            if (r2 >= r0) goto L_0x04ff
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.SkeletonJson$LinkedMesh> r3 = r1.linkedMeshes
            java.lang.Object r3 = r3.get(r2)
            com.esotericsoftware.spine.SkeletonJson$LinkedMesh r3 = (com.esotericsoftware.spine.SkeletonJson.LinkedMesh) r3
            java.lang.String r4 = r3.skin
            if (r4 != 0) goto L_0x04ae
            com.esotericsoftware.spine.Skin r4 = r8.getDefaultSkin()
            goto L_0x04b2
        L_0x04ae:
            com.esotericsoftware.spine.Skin r4 = r8.findSkin(r4)
        L_0x04b2:
            if (r4 == 0) goto L_0x04e6
            int r5 = r3.slotIndex
            java.lang.String r6 = r3.parent
            com.esotericsoftware.spine.attachments.Attachment r4 = r4.getAttachment(r5, r6)
            if (r4 == 0) goto L_0x04cd
            com.esotericsoftware.spine.attachments.MeshAttachment r5 = r3.mesh
            com.esotericsoftware.spine.attachments.MeshAttachment r4 = (com.esotericsoftware.spine.attachments.MeshAttachment) r4
            r5.setParentMesh(r4)
            com.esotericsoftware.spine.attachments.MeshAttachment r3 = r3.mesh
            r3.updateUVs()
            int r2 = r2 + 1
            goto L_0x049b
        L_0x04cd:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "Parent mesh not found: "
            r2.append(r4)
            java.lang.String r3 = r3.parent
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.<init>(r2)
            throw r0
        L_0x04e6:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "Skin not found: "
            r2.append(r4)
            java.lang.String r3 = r3.skin
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.<init>(r2)
            throw r0
        L_0x04ff:
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.SkeletonJson$LinkedMesh> r0 = r1.linkedMeshes
            r0.clear()
            java.lang.String r0 = "events"
            com.badlogic.gdx.utils.v r0 = r9.c(r0)
        L_0x050a:
            if (r0 == 0) goto L_0x055d
            com.esotericsoftware.spine.EventData r2 = new com.esotericsoftware.spine.EventData
            java.lang.String r3 = r0.f5626e
            r2.<init>(r3)
            java.lang.String r3 = "int"
            r4 = 0
            int r3 = r0.a(r3, r4)
            r2.intValue = r3
            java.lang.String r3 = "float"
            r5 = 0
            float r3 = r0.a(r3, r5)
            r2.floatValue = r3
            java.lang.String r3 = "string"
            java.lang.String r5 = ""
            java.lang.String r3 = r0.a(r3, r5)
            r2.stringValue = r3
            r3 = r17
            r5 = 0
            java.lang.String r6 = r0.a(r3, r5)
            r2.audioPath = r6
            java.lang.String r6 = r2.audioPath
            if (r6 == 0) goto L_0x0550
            java.lang.String r6 = "volume"
            r7 = 1065353216(0x3f800000, float:1.0)
            float r6 = r0.a(r6, r7)
            r2.volume = r6
            java.lang.String r6 = "balance"
            r10 = 0
            float r6 = r0.a(r6, r10)
            r2.balance = r6
            goto L_0x0553
        L_0x0550:
            r7 = 1065353216(0x3f800000, float:1.0)
            r10 = 0
        L_0x0553:
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.EventData> r6 = r8.events
            r6.add(r2)
            com.badlogic.gdx.utils.v r0 = r0.f5628g
            r17 = r3
            goto L_0x050a
        L_0x055d:
            java.lang.String r0 = "animations"
            com.badlogic.gdx.utils.v r0 = r9.c(r0)
            r2 = r0
        L_0x0564:
            if (r2 == 0) goto L_0x0588
            java.lang.String r0 = r2.f5626e     // Catch:{ all -> 0x056e }
            r1.readAnimation(r2, r0, r8)     // Catch:{ all -> 0x056e }
            com.badlogic.gdx.utils.v r2 = r2.f5628g
            goto L_0x0564
        L_0x056e:
            r0 = move-exception
            com.badlogic.gdx.utils.l0 r3 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Error reading animation: "
            r4.append(r5)
            java.lang.String r2 = r2.f5626e
            r4.append(r2)
            java.lang.String r2 = r4.toString()
            r3.<init>(r2, r0)
            throw r3
        L_0x0588:
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.BoneData> r0 = r8.bones
            r0.c()
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.SlotData> r0 = r8.slots
            r0.c()
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Skin> r0 = r8.skins
            r0.c()
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.EventData> r0 = r8.events
            r0.c()
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Animation> r0 = r8.animations
            r0.c()
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.IkConstraintData> r0 = r8.ikConstraints
            r0.c()
            return r8
        L_0x05a7:
            r1 = r7
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "file cannot be null."
            r0.<init>(r2)
            goto L_0x05b1
        L_0x05b0:
            throw r0
        L_0x05b1:
            goto L_0x05b0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.SkeletonJson.readSkeletonData(e.d.b.s.a):com.esotericsoftware.spine.SkeletonData");
    }

    public void setScale(float f2) {
        this.scale = f2;
    }

    public SkeletonJson(AttachmentLoader attachmentLoader2) {
        if (attachmentLoader2 != null) {
            this.attachmentLoader = attachmentLoader2;
            return;
        }
        throw new IllegalArgumentException("attachmentLoader cannot be null.");
    }
}
