package com.esotericsoftware.spine;

import com.badlogic.gdx.utils.a;

public class PathConstraintData {
    final a<BoneData> bones = new a<>();
    final String name;
    float offsetRotation;
    int order;
    float position;
    PositionMode positionMode;
    float rotateMix;
    RotateMode rotateMode;
    float spacing;
    SpacingMode spacingMode;
    SlotData target;
    float translateMix;

    public enum PositionMode {
        fixed,
        percent;
        
        public static final PositionMode[] values = values();
    }

    public enum RotateMode {
        tangent,
        chain,
        chainScale;
        
        public static final RotateMode[] values = values();
    }

    public enum SpacingMode {
        length,
        fixed,
        percent;
        
        public static final SpacingMode[] values = values();
    }

    public PathConstraintData(String str) {
        if (str != null) {
            this.name = str;
            return;
        }
        throw new IllegalArgumentException("name cannot be null.");
    }

    public a<BoneData> getBones() {
        return this.bones;
    }

    public String getName() {
        return this.name;
    }

    public float getOffsetRotation() {
        return this.offsetRotation;
    }

    public int getOrder() {
        return this.order;
    }

    public float getPosition() {
        return this.position;
    }

    public PositionMode getPositionMode() {
        return this.positionMode;
    }

    public float getRotateMix() {
        return this.rotateMix;
    }

    public RotateMode getRotateMode() {
        return this.rotateMode;
    }

    public float getSpacing() {
        return this.spacing;
    }

    public SpacingMode getSpacingMode() {
        return this.spacingMode;
    }

    public SlotData getTarget() {
        return this.target;
    }

    public float getTranslateMix() {
        return this.translateMix;
    }

    public void setOffsetRotation(float f2) {
        this.offsetRotation = f2;
    }

    public void setOrder(int i2) {
        this.order = i2;
    }

    public void setPosition(float f2) {
        this.position = f2;
    }

    public void setPositionMode(PositionMode positionMode2) {
        this.positionMode = positionMode2;
    }

    public void setRotateMix(float f2) {
        this.rotateMix = f2;
    }

    public void setRotateMode(RotateMode rotateMode2) {
        this.rotateMode = rotateMode2;
    }

    public void setSpacing(float f2) {
        this.spacing = f2;
    }

    public void setSpacingMode(SpacingMode spacingMode2) {
        this.spacingMode = spacingMode2;
    }

    public void setTarget(SlotData slotData) {
        this.target = slotData;
    }

    public void setTranslateMix(float f2) {
        this.translateMix = f2;
    }

    public String toString() {
        return this.name;
    }
}
