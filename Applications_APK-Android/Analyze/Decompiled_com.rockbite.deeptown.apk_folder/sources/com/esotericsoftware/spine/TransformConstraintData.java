package com.esotericsoftware.spine;

import com.badlogic.gdx.utils.a;

public class TransformConstraintData {
    final a<BoneData> bones = new a<>();
    boolean local;
    final String name;
    float offsetRotation;
    float offsetScaleX;
    float offsetScaleY;
    float offsetShearY;
    float offsetX;
    float offsetY;
    int order;
    boolean relative;
    float rotateMix;
    float scaleMix;
    float shearMix;
    BoneData target;
    float translateMix;

    public TransformConstraintData(String str) {
        if (str != null) {
            this.name = str;
            return;
        }
        throw new IllegalArgumentException("name cannot be null.");
    }

    public a<BoneData> getBones() {
        return this.bones;
    }

    public boolean getLocal() {
        return this.local;
    }

    public String getName() {
        return this.name;
    }

    public float getOffsetRotation() {
        return this.offsetRotation;
    }

    public float getOffsetScaleX() {
        return this.offsetScaleX;
    }

    public float getOffsetScaleY() {
        return this.offsetScaleY;
    }

    public float getOffsetShearY() {
        return this.offsetShearY;
    }

    public float getOffsetX() {
        return this.offsetX;
    }

    public float getOffsetY() {
        return this.offsetY;
    }

    public int getOrder() {
        return this.order;
    }

    public boolean getRelative() {
        return this.relative;
    }

    public float getRotateMix() {
        return this.rotateMix;
    }

    public float getScaleMix() {
        return this.scaleMix;
    }

    public float getShearMix() {
        return this.shearMix;
    }

    public BoneData getTarget() {
        return this.target;
    }

    public float getTranslateMix() {
        return this.translateMix;
    }

    public void setLocal(boolean z) {
        this.local = z;
    }

    public void setOffsetRotation(float f2) {
        this.offsetRotation = f2;
    }

    public void setOffsetScaleX(float f2) {
        this.offsetScaleX = f2;
    }

    public void setOffsetScaleY(float f2) {
        this.offsetScaleY = f2;
    }

    public void setOffsetShearY(float f2) {
        this.offsetShearY = f2;
    }

    public void setOffsetX(float f2) {
        this.offsetX = f2;
    }

    public void setOffsetY(float f2) {
        this.offsetY = f2;
    }

    public void setOrder(int i2) {
        this.order = i2;
    }

    public void setRelative(boolean z) {
        this.relative = z;
    }

    public void setRotateMix(float f2) {
        this.rotateMix = f2;
    }

    public void setScaleMix(float f2) {
        this.scaleMix = f2;
    }

    public void setShearMix(float f2) {
        this.shearMix = f2;
    }

    public void setTarget(BoneData boneData) {
        if (boneData != null) {
            this.target = boneData;
            return;
        }
        throw new IllegalArgumentException("target cannot be null.");
    }

    public void setTranslateMix(float f2) {
        this.translateMix = f2;
    }

    public String toString() {
        return this.name;
    }
}
