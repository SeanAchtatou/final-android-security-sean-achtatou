package com.esotericsoftware.spine;

import com.badlogic.gdx.math.h;
import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.m;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.VertexAttachment;
import e.d.b.t.b;

public class Animation {
    float duration;
    final String name;
    final a<Timeline> timelines;

    /* renamed from: com.esotericsoftware.spine.Animation$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$esotericsoftware$spine$Animation$MixBlend = new int[MixBlend.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.esotericsoftware.spine.Animation$MixBlend[] r0 = com.esotericsoftware.spine.Animation.MixBlend.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.esotericsoftware.spine.Animation.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend = r0
                int[] r0 = com.esotericsoftware.spine.Animation.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.esotericsoftware.spine.Animation$MixBlend r1 = com.esotericsoftware.spine.Animation.MixBlend.setup     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.esotericsoftware.spine.Animation.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend     // Catch:{ NoSuchFieldError -> 0x001f }
                com.esotericsoftware.spine.Animation$MixBlend r1 = com.esotericsoftware.spine.Animation.MixBlend.first     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.esotericsoftware.spine.Animation.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend     // Catch:{ NoSuchFieldError -> 0x002a }
                com.esotericsoftware.spine.Animation$MixBlend r1 = com.esotericsoftware.spine.Animation.MixBlend.replace     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.esotericsoftware.spine.Animation.AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.esotericsoftware.spine.Animation$MixBlend r1 = com.esotericsoftware.spine.Animation.MixBlend.add     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.Animation.AnonymousClass1.<clinit>():void");
        }
    }

    public static class AttachmentTimeline implements SlotTimeline {
        final String[] attachmentNames;
        final float[] frames;
        int slotIndex;

        public AttachmentTimeline(int i2) {
            this.frames = new float[i2];
            this.attachmentNames = new String[i2];
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            int i2;
            Slot slot = skeleton.slots.get(this.slotIndex);
            Attachment attachment = null;
            if (mixDirection == MixDirection.out && mixBlend == MixBlend.setup) {
                String str = slot.data.attachmentName;
                if (str != null) {
                    attachment = skeleton.getAttachment(this.slotIndex, str);
                }
                slot.setAttachment(attachment);
                return;
            }
            float[] fArr = this.frames;
            if (f3 >= fArr[0]) {
                if (f3 >= fArr[fArr.length - 1]) {
                    i2 = fArr.length;
                } else {
                    i2 = Animation.binarySearch(fArr, f3);
                }
                String str2 = this.attachmentNames[i2 - 1];
                if (str2 != null) {
                    attachment = skeleton.getAttachment(this.slotIndex, str2);
                }
                slot.setAttachment(attachment);
            } else if (mixBlend == MixBlend.setup || mixBlend == MixBlend.first) {
                String str3 = slot.data.attachmentName;
                if (str3 != null) {
                    attachment = skeleton.getAttachment(this.slotIndex, str3);
                }
                slot.setAttachment(attachment);
            }
        }

        public String[] getAttachmentNames() {
            return this.attachmentNames;
        }

        public int getFrameCount() {
            return this.frames.length;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public int getPropertyId() {
            return (TimelineType.attachment.ordinal() << 24) + this.slotIndex;
        }

        public int getSlotIndex() {
            return this.slotIndex;
        }

        public void setFrame(int i2, float f2, String str) {
            this.frames[i2] = f2;
            this.attachmentNames[i2] = str;
        }

        public void setSlotIndex(int i2) {
            if (i2 >= 0) {
                this.slotIndex = i2;
                return;
            }
            throw new IllegalArgumentException("index must be >= 0.");
        }
    }

    public interface BoneTimeline extends Timeline {
        int getBoneIndex();

        void setBoneIndex(int i2);
    }

    public static class ColorTimeline extends CurveTimeline implements SlotTimeline {
        private static final int A = 4;
        private static final int B = 3;
        public static final int ENTRIES = 5;
        private static final int G = 2;
        private static final int PREV_A = -1;
        private static final int PREV_B = -2;
        private static final int PREV_G = -3;
        private static final int PREV_R = -4;
        private static final int PREV_TIME = -5;
        private static final int R = 1;
        private final float[] frames;
        int slotIndex;

        public ColorTimeline(int i2) {
            super(i2);
            this.frames = new float[(i2 * 5)];
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            float f5;
            float f6;
            float f7;
            float f8;
            Slot slot = skeleton.slots.get(this.slotIndex);
            float[] fArr = this.frames;
            if (f3 < fArr[0]) {
                int i2 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i2 == 1) {
                    slot.color.b(slot.data.color);
                } else if (i2 == 2) {
                    b bVar = slot.color;
                    b bVar2 = slot.data.color;
                    bVar.a((bVar2.f6934a - bVar.f6934a) * f4, (bVar2.f6935b - bVar.f6935b) * f4, (bVar2.f6936c - bVar.f6936c) * f4, (bVar2.f6937d - bVar.f6937d) * f4);
                }
            } else {
                if (f3 >= fArr[fArr.length - 5]) {
                    int length = fArr.length;
                    f6 = fArr[length + PREV_R];
                    f5 = fArr[length + PREV_G];
                    f8 = fArr[length - 2];
                    f7 = fArr[length - 1];
                } else {
                    int binarySearch = Animation.binarySearch(fArr, f3, 5);
                    float f9 = fArr[binarySearch + PREV_R];
                    float f10 = fArr[binarySearch + PREV_G];
                    float f11 = fArr[binarySearch - 2];
                    float f12 = fArr[binarySearch - 1];
                    float f13 = fArr[binarySearch];
                    float curvePercent = getCurvePercent((binarySearch / 5) - 1, 1.0f - ((f3 - f13) / (fArr[binarySearch + PREV_TIME] - f13)));
                    float f14 = ((fArr[binarySearch + 1] - f9) * curvePercent) + f9;
                    float f15 = ((fArr[binarySearch + 2] - f10) * curvePercent) + f10;
                    f7 = ((fArr[binarySearch + 4] - f12) * curvePercent) + f12;
                    f6 = f14;
                    f5 = f15;
                    f8 = ((fArr[binarySearch + 3] - f11) * curvePercent) + f11;
                }
                if (f4 == 1.0f) {
                    slot.color.b(f6, f5, f8, f7);
                    return;
                }
                b bVar3 = slot.color;
                if (mixBlend == MixBlend.setup) {
                    bVar3.b(slot.data.color);
                }
                bVar3.a((f6 - bVar3.f6934a) * f4, (f5 - bVar3.f6935b) * f4, (f8 - bVar3.f6936c) * f4, (f7 - bVar3.f6937d) * f4);
            }
        }

        public float[] getFrames() {
            return this.frames;
        }

        public int getPropertyId() {
            return (TimelineType.color.ordinal() << 24) + this.slotIndex;
        }

        public int getSlotIndex() {
            return this.slotIndex;
        }

        public void setFrame(int i2, float f2, float f3, float f4, float f5, float f6) {
            int i3 = i2 * 5;
            float[] fArr = this.frames;
            fArr[i3] = f2;
            fArr[i3 + 1] = f3;
            fArr[i3 + 2] = f4;
            fArr[i3 + 3] = f5;
            fArr[i3 + 4] = f6;
        }

        public void setSlotIndex(int i2) {
            if (i2 >= 0) {
                this.slotIndex = i2;
                return;
            }
            throw new IllegalArgumentException("index must be >= 0.");
        }
    }

    public static abstract class CurveTimeline implements Timeline {
        public static final float BEZIER = 2.0f;
        private static final int BEZIER_SIZE = 19;
        public static final float LINEAR = 0.0f;
        public static final float STEPPED = 1.0f;
        private final float[] curves;

        public CurveTimeline(int i2) {
            if (i2 > 0) {
                this.curves = new float[((i2 - 1) * 19)];
                return;
            }
            throw new IllegalArgumentException("frameCount must be > 0: " + i2);
        }

        public float getCurvePercent(int i2, float f2) {
            float a2 = h.a(f2, (float) LINEAR, 1.0f);
            float[] fArr = this.curves;
            int i3 = i2 * 19;
            float f3 = fArr[i3];
            if (f3 == LINEAR) {
                return a2;
            }
            if (f3 == 1.0f) {
                return LINEAR;
            }
            int i4 = i3 + 1;
            int i5 = (i4 + 19) - 1;
            int i6 = i4;
            float f4 = LINEAR;
            while (i6 < i5) {
                f4 = fArr[i6];
                if (f4 < a2) {
                    i6 += 2;
                } else if (i6 == i4) {
                    return (fArr[i6 + 1] * a2) / f4;
                } else {
                    float f5 = fArr[i6 - 2];
                    float f6 = fArr[i6 - 1];
                    return f6 + (((fArr[i6 + 1] - f6) * (a2 - f5)) / (f4 - f5));
                }
            }
            float f7 = fArr[i6 - 1];
            return f7 + (((1.0f - f7) * (a2 - f4)) / (1.0f - f4));
        }

        public float getCurveType(int i2) {
            int i3 = i2 * 19;
            float[] fArr = this.curves;
            if (i3 == fArr.length) {
                return LINEAR;
            }
            float f2 = fArr[i3];
            if (f2 == LINEAR) {
                return LINEAR;
            }
            return f2 == 1.0f ? 1.0f : 2.0f;
        }

        public int getFrameCount() {
            return (this.curves.length / 19) + 1;
        }

        public void setCurve(int i2, float f2, float f3, float f4, float f5) {
            float f6 = (((-f2) * 2.0f) + f4) * 0.03f;
            float f7 = (((-f3) * 2.0f) + f5) * 0.03f;
            float f8 = (((f2 - f4) * 3.0f) + 1.0f) * 0.006f;
            float f9 = (((f3 - f5) * 3.0f) + 1.0f) * 0.006f;
            float f10 = (f6 * 2.0f) + f8;
            float f11 = (f2 * 0.3f) + f6 + (f8 * 0.16666667f);
            float f12 = (f3 * 0.3f) + f7 + (0.16666667f * f9);
            int i3 = i2 * 19;
            float[] fArr = this.curves;
            int i4 = i3 + 1;
            fArr[i3] = 2.0f;
            int i5 = (i4 + 19) - 1;
            float f13 = f11;
            float f14 = (f7 * 2.0f) + f9;
            float f15 = f12;
            while (i4 < i5) {
                fArr[i4] = f11;
                fArr[i4 + 1] = f12;
                f13 += f10;
                f15 += f14;
                f10 += f8;
                f14 += f9;
                f11 += f13;
                f12 += f15;
                i4 += 2;
            }
        }

        public void setLinear(int i2) {
            this.curves[i2 * 19] = 0.0f;
        }

        public void setStepped(int i2) {
            this.curves[i2 * 19] = 1.0f;
        }
    }

    public static class DeformTimeline extends CurveTimeline implements SlotTimeline {
        VertexAttachment attachment;
        private final float[][] frameVertices;
        private final float[] frames;
        int slotIndex;

        public DeformTimeline(int i2) {
            super(i2);
            this.frames = new float[i2];
            this.frameVertices = new float[i2][];
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            float f5 = f3;
            Slot slot = skeleton.slots.get(this.slotIndex);
            Attachment attachment2 = slot.attachment;
            if (attachment2 instanceof VertexAttachment) {
                VertexAttachment vertexAttachment = (VertexAttachment) attachment2;
                if (vertexAttachment.applyDeform(this.attachment)) {
                    m attachmentVertices = slot.getAttachmentVertices();
                    MixBlend mixBlend2 = attachmentVertices.f5528b == 0 ? MixBlend.setup : mixBlend;
                    float[][] fArr = this.frameVertices;
                    int i2 = 0;
                    int length = fArr[0].length;
                    float[] fArr2 = this.frames;
                    if (f5 < fArr2[0]) {
                        int i3 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend2.ordinal()];
                        if (i3 == 1) {
                            attachmentVertices.a();
                        } else if (i3 == 2) {
                            if (f4 == 1.0f) {
                                attachmentVertices.a();
                                return;
                            }
                            float[] d2 = attachmentVertices.d(length);
                            if (vertexAttachment.getBones() == null) {
                                float[] vertices = vertexAttachment.getVertices();
                                while (i2 < length) {
                                    d2[i2] = d2[i2] + ((vertices[i2] - d2[i2]) * f4);
                                    i2++;
                                }
                                return;
                            }
                            float f6 = 1.0f - f4;
                            while (i2 < length) {
                                d2[i2] = d2[i2] * f6;
                                i2++;
                            }
                        }
                    } else {
                        float[] d3 = attachmentVertices.d(length);
                        if (f5 >= fArr2[fArr2.length - 1]) {
                            float[] fArr3 = fArr[fArr2.length - 1];
                            if (f4 != 1.0f) {
                                int i4 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend2.ordinal()];
                                if (i4 != 1) {
                                    if (i4 == 2 || i4 == 3) {
                                        while (i2 < length) {
                                            d3[i2] = d3[i2] + ((fArr3[i2] - d3[i2]) * f4);
                                            i2++;
                                        }
                                    } else if (i4 == 4) {
                                        if (vertexAttachment.getBones() == null) {
                                            float[] vertices2 = vertexAttachment.getVertices();
                                            while (i2 < length) {
                                                d3[i2] = d3[i2] + ((fArr3[i2] - vertices2[i2]) * f4);
                                                i2++;
                                            }
                                            return;
                                        }
                                        while (i2 < length) {
                                            d3[i2] = d3[i2] + (fArr3[i2] * f4);
                                            i2++;
                                        }
                                    }
                                } else if (vertexAttachment.getBones() == null) {
                                    float[] vertices3 = vertexAttachment.getVertices();
                                    while (i2 < length) {
                                        float f7 = vertices3[i2];
                                        d3[i2] = f7 + ((fArr3[i2] - f7) * f4);
                                        i2++;
                                    }
                                } else {
                                    while (i2 < length) {
                                        d3[i2] = fArr3[i2] * f4;
                                        i2++;
                                    }
                                }
                            } else if (mixBlend2 != MixBlend.add) {
                                System.arraycopy(fArr3, 0, d3, 0, length);
                            } else if (vertexAttachment.getBones() == null) {
                                float[] vertices4 = vertexAttachment.getVertices();
                                while (i2 < length) {
                                    d3[i2] = d3[i2] + (fArr3[i2] - vertices4[i2]);
                                    i2++;
                                }
                            } else {
                                while (i2 < length) {
                                    d3[i2] = d3[i2] + fArr3[i2];
                                    i2++;
                                }
                            }
                        } else {
                            int binarySearch = Animation.binarySearch(fArr2, f5);
                            int i5 = binarySearch - 1;
                            float[] fArr4 = fArr[i5];
                            float[] fArr5 = fArr[binarySearch];
                            float f8 = fArr2[binarySearch];
                            float curvePercent = getCurvePercent(i5, 1.0f - ((f5 - f8) / (fArr2[i5] - f8)));
                            if (f4 != 1.0f) {
                                int i6 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend2.ordinal()];
                                if (i6 != 1) {
                                    if (i6 == 2 || i6 == 3) {
                                        while (i2 < length) {
                                            float f9 = fArr4[i2];
                                            d3[i2] = d3[i2] + (((f9 + ((fArr5[i2] - f9) * curvePercent)) - d3[i2]) * f4);
                                            i2++;
                                        }
                                    } else if (i6 == 4) {
                                        if (vertexAttachment.getBones() == null) {
                                            float[] vertices5 = vertexAttachment.getVertices();
                                            while (i2 < length) {
                                                float f10 = fArr4[i2];
                                                d3[i2] = d3[i2] + (((f10 + ((fArr5[i2] - f10) * curvePercent)) - vertices5[i2]) * f4);
                                                i2++;
                                            }
                                            return;
                                        }
                                        while (i2 < length) {
                                            float f11 = fArr4[i2];
                                            d3[i2] = d3[i2] + ((f11 + ((fArr5[i2] - f11) * curvePercent)) * f4);
                                            i2++;
                                        }
                                    }
                                } else if (vertexAttachment.getBones() == null) {
                                    float[] vertices6 = vertexAttachment.getVertices();
                                    while (i2 < length) {
                                        float f12 = fArr4[i2];
                                        float f13 = vertices6[i2];
                                        d3[i2] = f13 + (((f12 + ((fArr5[i2] - f12) * curvePercent)) - f13) * f4);
                                        i2++;
                                    }
                                } else {
                                    while (i2 < length) {
                                        float f14 = fArr4[i2];
                                        d3[i2] = (f14 + ((fArr5[i2] - f14) * curvePercent)) * f4;
                                        i2++;
                                    }
                                }
                            } else if (mixBlend2 != MixBlend.add) {
                                while (i2 < length) {
                                    float f15 = fArr4[i2];
                                    d3[i2] = f15 + ((fArr5[i2] - f15) * curvePercent);
                                    i2++;
                                }
                            } else if (vertexAttachment.getBones() == null) {
                                float[] vertices7 = vertexAttachment.getVertices();
                                while (i2 < length) {
                                    float f16 = fArr4[i2];
                                    d3[i2] = d3[i2] + ((f16 + ((fArr5[i2] - f16) * curvePercent)) - vertices7[i2]);
                                    i2++;
                                }
                            } else {
                                while (i2 < length) {
                                    float f17 = fArr4[i2];
                                    d3[i2] = d3[i2] + f17 + ((fArr5[i2] - f17) * curvePercent);
                                    i2++;
                                }
                            }
                        }
                    }
                }
            }
        }

        public VertexAttachment getAttachment() {
            return this.attachment;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public int getPropertyId() {
            return (TimelineType.deform.ordinal() << 27) + this.attachment.getId() + this.slotIndex;
        }

        public int getSlotIndex() {
            return this.slotIndex;
        }

        public float[][] getVertices() {
            return this.frameVertices;
        }

        public void setAttachment(VertexAttachment vertexAttachment) {
            this.attachment = vertexAttachment;
        }

        public void setFrame(int i2, float f2, float[] fArr) {
            this.frames[i2] = f2;
            this.frameVertices[i2] = fArr;
        }

        public void setSlotIndex(int i2) {
            if (i2 >= 0) {
                this.slotIndex = i2;
                return;
            }
            throw new IllegalArgumentException("index must be >= 0.");
        }
    }

    public static class DrawOrderTimeline implements Timeline {
        private final int[][] drawOrders;
        private final float[] frames;

        public DrawOrderTimeline(int i2) {
            this.frames = new float[i2];
            this.drawOrders = new int[i2][];
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            int i2;
            a<Slot> aVar2 = skeleton.drawOrder;
            a<Slot> aVar3 = skeleton.slots;
            if (mixDirection == MixDirection.out && mixBlend == MixBlend.setup) {
                System.arraycopy(aVar3.f5373a, 0, aVar2.f5373a, 0, aVar3.f5374b);
                return;
            }
            float[] fArr = this.frames;
            if (f3 >= fArr[0]) {
                if (f3 >= fArr[fArr.length - 1]) {
                    i2 = fArr.length;
                } else {
                    i2 = Animation.binarySearch(fArr, f3);
                }
                int[] iArr = this.drawOrders[i2 - 1];
                if (iArr == null) {
                    System.arraycopy(aVar3.f5373a, 0, aVar2.f5373a, 0, aVar3.f5374b);
                    return;
                }
                int length = iArr.length;
                for (int i3 = 0; i3 < length; i3++) {
                    aVar2.set(i3, aVar3.get(iArr[i3]));
                }
            } else if (mixBlend == MixBlend.setup || mixBlend == MixBlend.first) {
                System.arraycopy(aVar3.f5373a, 0, aVar2.f5373a, 0, aVar3.f5374b);
            }
        }

        public int[][] getDrawOrders() {
            return this.drawOrders;
        }

        public int getFrameCount() {
            return this.frames.length;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public int getPropertyId() {
            return TimelineType.drawOrder.ordinal() << 24;
        }

        public void setFrame(int i2, float f2, int[] iArr) {
            this.frames[i2] = f2;
            this.drawOrders[i2] = iArr;
        }
    }

    public static class EventTimeline implements Timeline {
        private final Event[] events;
        private final float[] frames;

        public EventTimeline(int i2) {
            this.frames = new float[i2];
            this.events = new Event[i2];
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            float f5;
            int i2;
            a<Event> aVar2 = aVar;
            if (aVar2 != null) {
                float[] fArr = this.frames;
                int length = fArr.length;
                if (f2 > f3) {
                    apply(skeleton, f2, 2.14748365E9f, aVar, f4, mixBlend, mixDirection);
                    f5 = -1.0f;
                } else if (f2 < fArr[length - 1]) {
                    f5 = f2;
                } else {
                    return;
                }
                if (f3 >= fArr[0]) {
                    if (f5 < fArr[0]) {
                        i2 = 0;
                    } else {
                        i2 = Animation.binarySearch(fArr, f5);
                        float f6 = fArr[i2];
                        while (i2 > 0 && fArr[i2 - 1] == f6) {
                            i2--;
                        }
                    }
                    while (i2 < length && f3 >= fArr[i2]) {
                        aVar2.add(this.events[i2]);
                        i2++;
                    }
                }
            }
        }

        public Event[] getEvents() {
            return this.events;
        }

        public int getFrameCount() {
            return this.frames.length;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public int getPropertyId() {
            return TimelineType.event.ordinal() << 24;
        }

        public void setFrame(int i2, Event event) {
            this.frames[i2] = event.time;
            this.events[i2] = event;
        }
    }

    public static class IkConstraintTimeline extends CurveTimeline {
        private static final int BEND_DIRECTION = 2;
        private static final int COMPRESS = 3;
        public static final int ENTRIES = 5;
        private static final int MIX = 1;
        private static final int PREV_BEND_DIRECTION = -3;
        private static final int PREV_COMPRESS = -2;
        private static final int PREV_MIX = -4;
        private static final int PREV_STRETCH = -1;
        private static final int PREV_TIME = -5;
        private static final int STRETCH = 4;
        private final float[] frames;
        int ikConstraintIndex;

        public IkConstraintTimeline(int i2) {
            super(i2);
            this.frames = new float[(i2 * 5)];
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            IkConstraint ikConstraint = skeleton.ikConstraints.get(this.ikConstraintIndex);
            float[] fArr = this.frames;
            boolean z = false;
            if (f3 < fArr[0]) {
                int i2 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i2 == 1) {
                    IkConstraintData ikConstraintData = ikConstraint.data;
                    ikConstraint.mix = ikConstraintData.mix;
                    ikConstraint.bendDirection = ikConstraintData.bendDirection;
                    ikConstraint.compress = ikConstraintData.compress;
                    ikConstraint.stretch = ikConstraintData.stretch;
                } else if (i2 == 2) {
                    float f5 = ikConstraint.mix;
                    IkConstraintData ikConstraintData2 = ikConstraint.data;
                    ikConstraint.mix = f5 + ((ikConstraintData2.mix - f5) * f4);
                    ikConstraint.bendDirection = ikConstraintData2.bendDirection;
                    ikConstraint.compress = ikConstraintData2.compress;
                    ikConstraint.stretch = ikConstraintData2.stretch;
                }
            } else if (f3 < fArr[fArr.length - 5]) {
                int binarySearch = Animation.binarySearch(fArr, f3, 5);
                float f6 = fArr[binarySearch + PREV_MIX];
                float f7 = fArr[binarySearch];
                float curvePercent = getCurvePercent((binarySearch / 5) - 1, 1.0f - ((f3 - f7) / (fArr[binarySearch + PREV_TIME] - f7)));
                if (mixBlend == MixBlend.setup) {
                    IkConstraintData ikConstraintData3 = ikConstraint.data;
                    float f8 = ikConstraintData3.mix;
                    ikConstraint.mix = f8 + (((f6 + ((fArr[binarySearch + 1] - f6) * curvePercent)) - f8) * f4);
                    if (mixDirection == MixDirection.out) {
                        ikConstraint.bendDirection = ikConstraintData3.bendDirection;
                        ikConstraint.compress = ikConstraintData3.compress;
                        ikConstraint.stretch = ikConstraintData3.stretch;
                        return;
                    }
                    ikConstraint.bendDirection = (int) fArr[binarySearch + PREV_BEND_DIRECTION];
                    ikConstraint.compress = fArr[binarySearch + -2] != CurveTimeline.LINEAR;
                    if (fArr[binarySearch - 1] != CurveTimeline.LINEAR) {
                        z = true;
                    }
                    ikConstraint.stretch = z;
                    return;
                }
                float f9 = ikConstraint.mix;
                ikConstraint.mix = f9 + (((f6 + ((fArr[binarySearch + 1] - f6) * curvePercent)) - f9) * f4);
                if (mixDirection == MixDirection.in) {
                    ikConstraint.bendDirection = (int) fArr[binarySearch + PREV_BEND_DIRECTION];
                    ikConstraint.compress = fArr[binarySearch + -2] != CurveTimeline.LINEAR;
                    if (fArr[binarySearch - 1] != CurveTimeline.LINEAR) {
                        z = true;
                    }
                    ikConstraint.stretch = z;
                }
            } else if (mixBlend == MixBlend.setup) {
                IkConstraintData ikConstraintData4 = ikConstraint.data;
                float f10 = ikConstraintData4.mix;
                ikConstraint.mix = f10 + ((fArr[fArr.length + PREV_MIX] - f10) * f4);
                if (mixDirection == MixDirection.out) {
                    ikConstraint.bendDirection = ikConstraintData4.bendDirection;
                    ikConstraint.compress = ikConstraintData4.compress;
                    ikConstraint.stretch = ikConstraintData4.stretch;
                    return;
                }
                ikConstraint.bendDirection = (int) fArr[fArr.length + PREV_BEND_DIRECTION];
                ikConstraint.compress = fArr[fArr.length + -2] != CurveTimeline.LINEAR;
                if (fArr[fArr.length - 1] != CurveTimeline.LINEAR) {
                    z = true;
                }
                ikConstraint.stretch = z;
            } else {
                float f11 = ikConstraint.mix;
                ikConstraint.mix = f11 + ((fArr[fArr.length + PREV_MIX] - f11) * f4);
                if (mixDirection == MixDirection.in) {
                    ikConstraint.bendDirection = (int) fArr[fArr.length + PREV_BEND_DIRECTION];
                    ikConstraint.compress = fArr[fArr.length + -2] != CurveTimeline.LINEAR;
                    if (fArr[fArr.length - 1] != CurveTimeline.LINEAR) {
                        z = true;
                    }
                    ikConstraint.stretch = z;
                }
            }
        }

        public float[] getFrames() {
            return this.frames;
        }

        public int getIkConstraintIndex() {
            return this.ikConstraintIndex;
        }

        public int getPropertyId() {
            return (TimelineType.ikConstraint.ordinal() << 24) + this.ikConstraintIndex;
        }

        public void setFrame(int i2, float f2, float f3, int i3, boolean z, boolean z2) {
            int i4 = i2 * 5;
            float[] fArr = this.frames;
            fArr[i4] = f2;
            fArr[i4 + 1] = f3;
            fArr[i4 + 2] = (float) i3;
            float f4 = 1.0f;
            fArr[i4 + 3] = z ? 1.0f : CurveTimeline.LINEAR;
            float[] fArr2 = this.frames;
            int i5 = i4 + 4;
            if (!z2) {
                f4 = CurveTimeline.LINEAR;
            }
            fArr2[i5] = f4;
        }

        public void setIkConstraintIndex(int i2) {
            if (i2 >= 0) {
                this.ikConstraintIndex = i2;
                return;
            }
            throw new IllegalArgumentException("index must be >= 0.");
        }
    }

    public enum MixBlend {
        setup,
        first,
        replace,
        add
    }

    public enum MixDirection {
        in,
        out
    }

    public static class PathConstraintMixTimeline extends CurveTimeline {
        public static final int ENTRIES = 3;
        private static final int PREV_ROTATE = -2;
        private static final int PREV_TIME = -3;
        private static final int PREV_TRANSLATE = -1;
        private static final int ROTATE = 1;
        private static final int TRANSLATE = 2;
        private final float[] frames;
        int pathConstraintIndex;

        public PathConstraintMixTimeline(int i2) {
            super(i2);
            this.frames = new float[(i2 * 3)];
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            float f5;
            float f6;
            PathConstraint pathConstraint = skeleton.pathConstraints.get(this.pathConstraintIndex);
            float[] fArr = this.frames;
            if (f3 < fArr[0]) {
                int i2 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i2 == 1) {
                    PathConstraintData pathConstraintData = pathConstraint.data;
                    pathConstraint.rotateMix = pathConstraintData.rotateMix;
                    pathConstraint.translateMix = pathConstraintData.translateMix;
                } else if (i2 == 2) {
                    float f7 = pathConstraint.rotateMix;
                    PathConstraintData pathConstraintData2 = pathConstraint.data;
                    pathConstraint.rotateMix = f7 + ((pathConstraintData2.rotateMix - f7) * f4);
                    float f8 = pathConstraint.translateMix;
                    pathConstraint.translateMix = f8 + ((pathConstraintData2.translateMix - f8) * f4);
                }
            } else {
                if (f3 >= fArr[fArr.length - 3]) {
                    f5 = fArr[fArr.length - 2];
                    f6 = fArr[fArr.length - 1];
                } else {
                    int binarySearch = Animation.binarySearch(fArr, f3, 3);
                    float f9 = fArr[binarySearch - 2];
                    float f10 = fArr[binarySearch - 1];
                    float f11 = fArr[binarySearch];
                    float curvePercent = getCurvePercent((binarySearch / 3) - 1, 1.0f - ((f3 - f11) / (fArr[binarySearch + PREV_TIME] - f11)));
                    f6 = ((fArr[binarySearch + 2] - f10) * curvePercent) + f10;
                    f5 = ((fArr[binarySearch + 1] - f9) * curvePercent) + f9;
                }
                if (mixBlend == MixBlend.setup) {
                    PathConstraintData pathConstraintData3 = pathConstraint.data;
                    float f12 = pathConstraintData3.rotateMix;
                    pathConstraint.rotateMix = f12 + ((f5 - f12) * f4);
                    float f13 = pathConstraintData3.translateMix;
                    pathConstraint.translateMix = f13 + ((f6 - f13) * f4);
                    return;
                }
                float f14 = pathConstraint.rotateMix;
                pathConstraint.rotateMix = f14 + ((f5 - f14) * f4);
                float f15 = pathConstraint.translateMix;
                pathConstraint.translateMix = f15 + ((f6 - f15) * f4);
            }
        }

        public float[] getFrames() {
            return this.frames;
        }

        public int getPathConstraintIndex() {
            return this.pathConstraintIndex;
        }

        public int getPropertyId() {
            return (TimelineType.pathConstraintMix.ordinal() << 24) + this.pathConstraintIndex;
        }

        public void setFrame(int i2, float f2, float f3, float f4) {
            int i3 = i2 * 3;
            float[] fArr = this.frames;
            fArr[i3] = f2;
            fArr[i3 + 1] = f3;
            fArr[i3 + 2] = f4;
        }

        public void setPathConstraintIndex(int i2) {
            if (i2 >= 0) {
                this.pathConstraintIndex = i2;
                return;
            }
            throw new IllegalArgumentException("index must be >= 0.");
        }
    }

    public static class PathConstraintPositionTimeline extends CurveTimeline {
        public static final int ENTRIES = 2;
        static final int PREV_TIME = -2;
        static final int PREV_VALUE = -1;
        static final int VALUE = 1;
        final float[] frames;
        int pathConstraintIndex;

        public PathConstraintPositionTimeline(int i2) {
            super(i2);
            this.frames = new float[(i2 * 2)];
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            float f5;
            PathConstraint pathConstraint = skeleton.pathConstraints.get(this.pathConstraintIndex);
            float[] fArr = this.frames;
            if (f3 < fArr[0]) {
                int i2 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i2 == 1) {
                    pathConstraint.position = pathConstraint.data.position;
                } else if (i2 == 2) {
                    float f6 = pathConstraint.position;
                    pathConstraint.position = f6 + ((pathConstraint.data.position - f6) * f4);
                }
            } else {
                if (f3 >= fArr[fArr.length - 2]) {
                    f5 = fArr[fArr.length - 1];
                } else {
                    int binarySearch = Animation.binarySearch(fArr, f3, 2);
                    float f7 = fArr[binarySearch - 1];
                    float f8 = fArr[binarySearch];
                    f5 = ((fArr[binarySearch + 1] - f7) * getCurvePercent((binarySearch / 2) - 1, 1.0f - ((f3 - f8) / (fArr[binarySearch - 2] - f8)))) + f7;
                }
                if (mixBlend == MixBlend.setup) {
                    float f9 = pathConstraint.data.position;
                    pathConstraint.position = f9 + ((f5 - f9) * f4);
                    return;
                }
                float f10 = pathConstraint.position;
                pathConstraint.position = f10 + ((f5 - f10) * f4);
            }
        }

        public float[] getFrames() {
            return this.frames;
        }

        public int getPathConstraintIndex() {
            return this.pathConstraintIndex;
        }

        public int getPropertyId() {
            return (TimelineType.pathConstraintPosition.ordinal() << 24) + this.pathConstraintIndex;
        }

        public void setFrame(int i2, float f2, float f3) {
            int i3 = i2 * 2;
            float[] fArr = this.frames;
            fArr[i3] = f2;
            fArr[i3 + 1] = f3;
        }

        public void setPathConstraintIndex(int i2) {
            if (i2 >= 0) {
                this.pathConstraintIndex = i2;
                return;
            }
            throw new IllegalArgumentException("index must be >= 0.");
        }
    }

    public static class PathConstraintSpacingTimeline extends PathConstraintPositionTimeline {
        public PathConstraintSpacingTimeline(int i2) {
            super(i2);
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            float f5;
            PathConstraint pathConstraint = skeleton.pathConstraints.get(this.pathConstraintIndex);
            float[] fArr = this.frames;
            if (f3 < fArr[0]) {
                int i2 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i2 == 1) {
                    pathConstraint.spacing = pathConstraint.data.spacing;
                } else if (i2 == 2) {
                    float f6 = pathConstraint.spacing;
                    pathConstraint.spacing = f6 + ((pathConstraint.data.spacing - f6) * f4);
                }
            } else {
                if (f3 >= fArr[fArr.length - 2]) {
                    f5 = fArr[fArr.length - 1];
                } else {
                    int binarySearch = Animation.binarySearch(fArr, f3, 2);
                    float f7 = fArr[binarySearch - 1];
                    float f8 = fArr[binarySearch];
                    f5 = ((fArr[binarySearch + 1] - f7) * getCurvePercent((binarySearch / 2) - 1, 1.0f - ((f3 - f8) / (fArr[binarySearch - 2] - f8)))) + f7;
                }
                if (mixBlend == MixBlend.setup) {
                    float f9 = pathConstraint.data.spacing;
                    pathConstraint.spacing = f9 + ((f5 - f9) * f4);
                    return;
                }
                float f10 = pathConstraint.spacing;
                pathConstraint.spacing = f10 + ((f5 - f10) * f4);
            }
        }

        public int getPropertyId() {
            return (TimelineType.pathConstraintSpacing.ordinal() << 24) + this.pathConstraintIndex;
        }
    }

    public static class RotateTimeline extends CurveTimeline implements BoneTimeline {
        public static final int ENTRIES = 2;
        static final int PREV_ROTATION = -1;
        static final int PREV_TIME = -2;
        static final int ROTATION = 1;
        int boneIndex;
        final float[] frames;

        public RotateTimeline(int i2) {
            super(i2);
            this.frames = new float[(i2 << 1)];
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            float f5 = f3;
            Bone bone = skeleton.bones.get(this.boneIndex);
            float[] fArr = this.frames;
            if (f5 < fArr[0]) {
                int i2 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i2 == 1) {
                    bone.rotation = bone.data.rotation;
                } else if (i2 == 2) {
                    float f6 = bone.data.rotation;
                    float f7 = bone.rotation;
                    float f8 = f6 - f7;
                    double d2 = (double) (f8 / 360.0f);
                    Double.isNaN(d2);
                    bone.rotation = f7 + ((f8 - ((float) ((16384 - ((int) (16384.499999999996d - d2))) * 360))) * f4);
                }
            } else if (f5 >= fArr[fArr.length - 2]) {
                float f9 = fArr[fArr.length - 1];
                int i3 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i3 != 1) {
                    if (i3 == 2 || i3 == 3) {
                        float f10 = f9 + (bone.data.rotation - bone.rotation);
                        double d3 = (double) (f10 / 360.0f);
                        Double.isNaN(d3);
                        f9 = f10 - ((float) ((16384 - ((int) (16384.499999999996d - d3))) * 360));
                    } else if (i3 != 4) {
                        return;
                    }
                    bone.rotation += f9 * f4;
                    return;
                }
                bone.rotation = bone.data.rotation + (f9 * f4);
            } else {
                int binarySearch = Animation.binarySearch(fArr, f5, 2);
                float f11 = fArr[binarySearch - 1];
                float f12 = fArr[binarySearch];
                float curvePercent = getCurvePercent((binarySearch >> 1) - 1, 1.0f - ((f5 - f12) / (fArr[binarySearch - 2] - f12)));
                float f13 = fArr[binarySearch + 1] - f11;
                double d4 = (double) (f13 / 360.0f);
                Double.isNaN(d4);
                float f14 = f11 + ((f13 - ((float) ((16384 - ((int) (16384.499999999996d - d4))) * 360))) * curvePercent);
                int i4 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i4 != 1) {
                    if (i4 == 2 || i4 == 3) {
                        f14 += bone.data.rotation - bone.rotation;
                    } else if (i4 != 4) {
                        return;
                    }
                    float f15 = bone.rotation;
                    double d5 = (double) (f14 / 360.0f);
                    Double.isNaN(d5);
                    bone.rotation = f15 + ((f14 - ((float) ((16384 - ((int) (16384.499999999996d - d5))) * 360))) * f4);
                    return;
                }
                float f16 = bone.data.rotation;
                double d6 = (double) (f14 / 360.0f);
                Double.isNaN(d6);
                bone.rotation = f16 + ((f14 - ((float) ((16384 - ((int) (16384.499999999996d - d6))) * 360))) * f4);
            }
        }

        public int getBoneIndex() {
            return this.boneIndex;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public int getPropertyId() {
            return (TimelineType.rotate.ordinal() << 24) + this.boneIndex;
        }

        public void setBoneIndex(int i2) {
            if (i2 >= 0) {
                this.boneIndex = i2;
                return;
            }
            throw new IllegalArgumentException("index must be >= 0.");
        }

        public void setFrame(int i2, float f2, float f3) {
            int i3 = i2 << 1;
            float[] fArr = this.frames;
            fArr[i3] = f2;
            fArr[i3 + 1] = f3;
        }
    }

    public static class ScaleTimeline extends TranslateTimeline {
        public ScaleTimeline(int i2) {
            super(i2);
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            float f5;
            float f6;
            float f7 = f3;
            Bone bone = skeleton.bones.get(this.boneIndex);
            float[] fArr = this.frames;
            if (f7 < fArr[0]) {
                int i2 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i2 == 1) {
                    BoneData boneData = bone.data;
                    bone.scaleX = boneData.scaleX;
                    bone.scaleY = boneData.scaleY;
                } else if (i2 == 2) {
                    float f8 = bone.scaleX;
                    BoneData boneData2 = bone.data;
                    bone.scaleX = f8 + ((boneData2.scaleX - f8) * f4);
                    float f9 = bone.scaleY;
                    bone.scaleY = f9 + ((boneData2.scaleY - f9) * f4);
                }
            } else {
                if (f7 >= fArr[fArr.length - 3]) {
                    float f10 = fArr[fArr.length - 2];
                    BoneData boneData3 = bone.data;
                    float f11 = f10 * boneData3.scaleX;
                    f6 = fArr[fArr.length - 1] * boneData3.scaleY;
                    f5 = f11;
                } else {
                    int binarySearch = Animation.binarySearch(fArr, f7, 3);
                    float f12 = fArr[binarySearch - 2];
                    float f13 = fArr[binarySearch - 1];
                    float f14 = fArr[binarySearch];
                    float curvePercent = getCurvePercent((binarySearch / 3) - 1, 1.0f - ((f7 - f14) / (fArr[binarySearch - 3] - f14)));
                    float f15 = f12 + ((fArr[binarySearch + 1] - f12) * curvePercent);
                    BoneData boneData4 = bone.data;
                    f5 = f15 * boneData4.scaleX;
                    f6 = (f13 + ((fArr[binarySearch + 2] - f13) * curvePercent)) * boneData4.scaleY;
                }
                if (f4 == 1.0f) {
                    if (mixBlend == MixBlend.add) {
                        float f16 = bone.scaleX;
                        BoneData boneData5 = bone.data;
                        bone.scaleX = f16 + (f5 - boneData5.scaleX);
                        bone.scaleY += f6 - boneData5.scaleY;
                        return;
                    }
                    bone.scaleX = f5;
                    bone.scaleY = f6;
                } else if (mixDirection == MixDirection.out) {
                    int i3 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                    if (i3 == 1) {
                        BoneData boneData6 = bone.data;
                        float f17 = boneData6.scaleX;
                        float f18 = boneData6.scaleY;
                        bone.scaleX = f17 + (((Math.abs(f5) * Math.signum(f17)) - f17) * f4);
                        bone.scaleY = f18 + (((Math.abs(f6) * Math.signum(f18)) - f18) * f4);
                    } else if (i3 == 2 || i3 == 3) {
                        float f19 = bone.scaleX;
                        float f20 = bone.scaleY;
                        bone.scaleX = f19 + (((Math.abs(f5) * Math.signum(f19)) - f19) * f4);
                        bone.scaleY = f20 + (((Math.abs(f6) * Math.signum(f20)) - f20) * f4);
                    } else if (i3 == 4) {
                        float f21 = bone.scaleX;
                        float f22 = bone.scaleY;
                        bone.scaleX = f21 + (((Math.abs(f5) * Math.signum(f21)) - bone.data.scaleX) * f4);
                        bone.scaleY = f22 + (((Math.abs(f6) * Math.signum(f22)) - bone.data.scaleY) * f4);
                    }
                } else {
                    int i4 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                    if (i4 == 1) {
                        float abs = Math.abs(bone.data.scaleX) * Math.signum(f5);
                        float abs2 = Math.abs(bone.data.scaleY) * Math.signum(f6);
                        bone.scaleX = abs + ((f5 - abs) * f4);
                        bone.scaleY = abs2 + ((f6 - abs2) * f4);
                    } else if (i4 == 2 || i4 == 3) {
                        float abs3 = Math.abs(bone.scaleX) * Math.signum(f5);
                        float abs4 = Math.abs(bone.scaleY) * Math.signum(f6);
                        bone.scaleX = abs3 + ((f5 - abs3) * f4);
                        bone.scaleY = abs4 + ((f6 - abs4) * f4);
                    } else if (i4 == 4) {
                        float signum = Math.signum(f5);
                        float signum2 = Math.signum(f6);
                        bone.scaleX = (Math.abs(bone.scaleX) * signum) + ((f5 - (Math.abs(bone.data.scaleX) * signum)) * f4);
                        bone.scaleY = (Math.abs(bone.scaleY) * signum2) + ((f6 - (Math.abs(bone.data.scaleY) * signum2)) * f4);
                    }
                }
            }
        }

        public int getPropertyId() {
            return (TimelineType.scale.ordinal() << 24) + this.boneIndex;
        }
    }

    public static class ShearTimeline extends TranslateTimeline {
        public ShearTimeline(int i2) {
            super(i2);
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            float f5;
            float f6;
            Bone bone = skeleton.bones.get(this.boneIndex);
            float[] fArr = this.frames;
            if (f3 < fArr[0]) {
                int i2 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i2 == 1) {
                    BoneData boneData = bone.data;
                    bone.shearX = boneData.shearX;
                    bone.shearY = boneData.shearY;
                } else if (i2 == 2) {
                    float f7 = bone.shearX;
                    BoneData boneData2 = bone.data;
                    bone.shearX = f7 + ((boneData2.shearX - f7) * f4);
                    float f8 = bone.shearY;
                    bone.shearY = f8 + ((boneData2.shearY - f8) * f4);
                }
            } else {
                if (f3 >= fArr[fArr.length - 3]) {
                    f5 = fArr[fArr.length - 2];
                    f6 = fArr[fArr.length - 1];
                } else {
                    int binarySearch = Animation.binarySearch(fArr, f3, 3);
                    float f9 = fArr[binarySearch - 2];
                    float f10 = fArr[binarySearch - 1];
                    float f11 = fArr[binarySearch];
                    float curvePercent = getCurvePercent((binarySearch / 3) - 1, 1.0f - ((f3 - f11) / (fArr[binarySearch - 3] - f11)));
                    f6 = ((fArr[binarySearch + 2] - f10) * curvePercent) + f10;
                    f5 = f9 + ((fArr[binarySearch + 1] - f9) * curvePercent);
                }
                int i3 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i3 == 1) {
                    BoneData boneData3 = bone.data;
                    bone.shearX = boneData3.shearX + (f5 * f4);
                    bone.shearY = boneData3.shearY + (f6 * f4);
                } else if (i3 == 2 || i3 == 3) {
                    float f12 = bone.shearX;
                    BoneData boneData4 = bone.data;
                    bone.shearX = f12 + (((boneData4.shearX + f5) - f12) * f4);
                    float f13 = bone.shearY;
                    bone.shearY = f13 + (((boneData4.shearY + f6) - f13) * f4);
                } else if (i3 == 4) {
                    bone.shearX += f5 * f4;
                    bone.shearY += f6 * f4;
                }
            }
        }

        public int getPropertyId() {
            return (TimelineType.shear.ordinal() << 24) + this.boneIndex;
        }
    }

    public interface SlotTimeline extends Timeline {
        int getSlotIndex();

        void setSlotIndex(int i2);
    }

    public interface Timeline {
        void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection);

        int getPropertyId();
    }

    private enum TimelineType {
        rotate,
        translate,
        scale,
        shear,
        attachment,
        color,
        deform,
        event,
        drawOrder,
        ikConstraint,
        transformConstraint,
        pathConstraintPosition,
        pathConstraintSpacing,
        pathConstraintMix,
        twoColor
    }

    public static class TransformConstraintTimeline extends CurveTimeline {
        public static final int ENTRIES = 5;
        private static final int PREV_ROTATE = -4;
        private static final int PREV_SCALE = -2;
        private static final int PREV_SHEAR = -1;
        private static final int PREV_TIME = -5;
        private static final int PREV_TRANSLATE = -3;
        private static final int ROTATE = 1;
        private static final int SCALE = 3;
        private static final int SHEAR = 4;
        private static final int TRANSLATE = 2;
        private final float[] frames;
        int transformConstraintIndex;

        public TransformConstraintTimeline(int i2) {
            super(i2);
            this.frames = new float[(i2 * 5)];
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            float f5;
            float f6;
            float f7;
            float f8;
            TransformConstraint transformConstraint = skeleton.transformConstraints.get(this.transformConstraintIndex);
            float[] fArr = this.frames;
            if (f3 < fArr[0]) {
                TransformConstraintData transformConstraintData = transformConstraint.data;
                int i2 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i2 == 1) {
                    transformConstraint.rotateMix = transformConstraintData.rotateMix;
                    transformConstraint.translateMix = transformConstraintData.translateMix;
                    transformConstraint.scaleMix = transformConstraintData.scaleMix;
                    transformConstraint.shearMix = transformConstraintData.shearMix;
                } else if (i2 == 2) {
                    float f9 = transformConstraint.rotateMix;
                    transformConstraint.rotateMix = f9 + ((transformConstraintData.rotateMix - f9) * f4);
                    float f10 = transformConstraint.translateMix;
                    transformConstraint.translateMix = f10 + ((transformConstraintData.translateMix - f10) * f4);
                    float f11 = transformConstraint.scaleMix;
                    transformConstraint.scaleMix = f11 + ((transformConstraintData.scaleMix - f11) * f4);
                    float f12 = transformConstraint.shearMix;
                    transformConstraint.shearMix = f12 + ((transformConstraintData.shearMix - f12) * f4);
                }
            } else {
                if (f3 >= fArr[fArr.length - 5]) {
                    int length = fArr.length;
                    f6 = fArr[length + PREV_ROTATE];
                    f5 = fArr[length + PREV_TRANSLATE];
                    f8 = fArr[length - 2];
                    f7 = fArr[length - 1];
                } else {
                    int binarySearch = Animation.binarySearch(fArr, f3, 5);
                    float f13 = fArr[binarySearch + PREV_ROTATE];
                    float f14 = fArr[binarySearch + PREV_TRANSLATE];
                    float f15 = fArr[binarySearch - 2];
                    float f16 = fArr[binarySearch - 1];
                    float f17 = fArr[binarySearch];
                    float curvePercent = getCurvePercent((binarySearch / 5) - 1, 1.0f - ((f3 - f17) / (fArr[binarySearch + PREV_TIME] - f17)));
                    float f18 = ((fArr[binarySearch + 1] - f13) * curvePercent) + f13;
                    float f19 = ((fArr[binarySearch + 2] - f14) * curvePercent) + f14;
                    f7 = ((fArr[binarySearch + 4] - f16) * curvePercent) + f16;
                    f6 = f18;
                    f5 = f19;
                    f8 = ((fArr[binarySearch + 3] - f15) * curvePercent) + f15;
                }
                if (mixBlend == MixBlend.setup) {
                    TransformConstraintData transformConstraintData2 = transformConstraint.data;
                    float f20 = transformConstraintData2.rotateMix;
                    transformConstraint.rotateMix = f20 + ((f6 - f20) * f4);
                    float f21 = transformConstraintData2.translateMix;
                    transformConstraint.translateMix = f21 + ((f5 - f21) * f4);
                    float f22 = transformConstraintData2.scaleMix;
                    transformConstraint.scaleMix = f22 + ((f8 - f22) * f4);
                    float f23 = transformConstraintData2.shearMix;
                    transformConstraint.shearMix = f23 + ((f7 - f23) * f4);
                    return;
                }
                float f24 = transformConstraint.rotateMix;
                transformConstraint.rotateMix = f24 + ((f6 - f24) * f4);
                float f25 = transformConstraint.translateMix;
                transformConstraint.translateMix = f25 + ((f5 - f25) * f4);
                float f26 = transformConstraint.scaleMix;
                transformConstraint.scaleMix = f26 + ((f8 - f26) * f4);
                float f27 = transformConstraint.shearMix;
                transformConstraint.shearMix = f27 + ((f7 - f27) * f4);
            }
        }

        public float[] getFrames() {
            return this.frames;
        }

        public int getPropertyId() {
            return (TimelineType.transformConstraint.ordinal() << 24) + this.transformConstraintIndex;
        }

        public int getTransformConstraintIndex() {
            return this.transformConstraintIndex;
        }

        public void setFrame(int i2, float f2, float f3, float f4, float f5, float f6) {
            int i3 = i2 * 5;
            float[] fArr = this.frames;
            fArr[i3] = f2;
            fArr[i3 + 1] = f3;
            fArr[i3 + 2] = f4;
            fArr[i3 + 3] = f5;
            fArr[i3 + 4] = f6;
        }

        public void setTransformConstraintIndex(int i2) {
            if (i2 >= 0) {
                this.transformConstraintIndex = i2;
                return;
            }
            throw new IllegalArgumentException("index must be >= 0.");
        }
    }

    public static class TranslateTimeline extends CurveTimeline implements BoneTimeline {
        public static final int ENTRIES = 3;
        static final int PREV_TIME = -3;
        static final int PREV_X = -2;
        static final int PREV_Y = -1;
        static final int X = 1;
        static final int Y = 2;
        int boneIndex;
        final float[] frames;

        public TranslateTimeline(int i2) {
            super(i2);
            this.frames = new float[(i2 * 3)];
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            float f5;
            float f6;
            Bone bone = skeleton.bones.get(this.boneIndex);
            float[] fArr = this.frames;
            if (f3 < fArr[0]) {
                int i2 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i2 == 1) {
                    BoneData boneData = bone.data;
                    bone.x = boneData.x;
                    bone.y = boneData.y;
                } else if (i2 == 2) {
                    float f7 = bone.x;
                    BoneData boneData2 = bone.data;
                    bone.x = f7 + ((boneData2.x - f7) * f4);
                    float f8 = bone.y;
                    bone.y = f8 + ((boneData2.y - f8) * f4);
                }
            } else {
                if (f3 >= fArr[fArr.length - 3]) {
                    f5 = fArr[fArr.length - 2];
                    f6 = fArr[fArr.length - 1];
                } else {
                    int binarySearch = Animation.binarySearch(fArr, f3, 3);
                    float f9 = fArr[binarySearch - 2];
                    float f10 = fArr[binarySearch - 1];
                    float f11 = fArr[binarySearch];
                    float curvePercent = getCurvePercent((binarySearch / 3) - 1, 1.0f - ((f3 - f11) / (fArr[binarySearch + PREV_TIME] - f11)));
                    f6 = ((fArr[binarySearch + 2] - f10) * curvePercent) + f10;
                    f5 = f9 + ((fArr[binarySearch + 1] - f9) * curvePercent);
                }
                int i3 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i3 == 1) {
                    BoneData boneData3 = bone.data;
                    bone.x = boneData3.x + (f5 * f4);
                    bone.y = boneData3.y + (f6 * f4);
                } else if (i3 == 2 || i3 == 3) {
                    float f12 = bone.x;
                    BoneData boneData4 = bone.data;
                    bone.x = f12 + (((boneData4.x + f5) - f12) * f4);
                    float f13 = bone.y;
                    bone.y = f13 + (((boneData4.y + f6) - f13) * f4);
                } else if (i3 == 4) {
                    bone.x += f5 * f4;
                    bone.y += f6 * f4;
                }
            }
        }

        public int getBoneIndex() {
            return this.boneIndex;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public int getPropertyId() {
            return (TimelineType.translate.ordinal() << 24) + this.boneIndex;
        }

        public void setBoneIndex(int i2) {
            if (i2 >= 0) {
                this.boneIndex = i2;
                return;
            }
            throw new IllegalArgumentException("index must be >= 0.");
        }

        public void setFrame(int i2, float f2, float f3, float f4) {
            int i3 = i2 * 3;
            float[] fArr = this.frames;
            fArr[i3] = f2;
            fArr[i3 + 1] = f3;
            fArr[i3 + 2] = f4;
        }
    }

    public static class TwoColorTimeline extends CurveTimeline implements SlotTimeline {
        private static final int A = 4;
        private static final int B = 3;
        private static final int B2 = 7;
        public static final int ENTRIES = 8;
        private static final int G = 2;
        private static final int G2 = 6;
        private static final int PREV_A = -4;
        private static final int PREV_B = -5;
        private static final int PREV_B2 = -1;
        private static final int PREV_G = -6;
        private static final int PREV_G2 = -2;
        private static final int PREV_R = -7;
        private static final int PREV_R2 = -3;
        private static final int PREV_TIME = -8;
        private static final int R = 1;
        private static final int R2 = 5;
        private final float[] frames;
        int slotIndex;

        public TwoColorTimeline(int i2) {
            super(i2);
            this.frames = new float[(i2 * 8)];
        }

        public void apply(Skeleton skeleton, float f2, float f3, a<Event> aVar, float f4, MixBlend mixBlend, MixDirection mixDirection) {
            float f5;
            float f6;
            float f7;
            float f8;
            float f9;
            float f10;
            float f11;
            float f12 = f3;
            Slot slot = skeleton.slots.get(this.slotIndex);
            float[] fArr = this.frames;
            if (f12 < fArr[0]) {
                int i2 = AnonymousClass1.$SwitchMap$com$esotericsoftware$spine$Animation$MixBlend[mixBlend.ordinal()];
                if (i2 == 1) {
                    slot.color.b(slot.data.color);
                    slot.darkColor.b(slot.data.darkColor);
                } else if (i2 == 2) {
                    b bVar = slot.color;
                    b bVar2 = slot.darkColor;
                    SlotData slotData = slot.data;
                    b bVar3 = slotData.color;
                    b bVar4 = slotData.darkColor;
                    bVar.a((bVar3.f6934a - bVar.f6934a) * f4, (bVar3.f6935b - bVar.f6935b) * f4, (bVar3.f6936c - bVar.f6936c) * f4, (bVar3.f6937d - bVar.f6937d) * f4);
                    bVar2.a((bVar4.f6934a - bVar2.f6934a) * f4, (bVar4.f6935b - bVar2.f6935b) * f4, (bVar4.f6936c - bVar2.f6936c) * f4, CurveTimeline.LINEAR);
                }
            } else {
                if (f12 >= fArr[fArr.length - 8]) {
                    int length = fArr.length;
                    f10 = fArr[length - 7];
                    f9 = fArr[length - 6];
                    float f13 = fArr[length + PREV_B];
                    float f14 = fArr[length + PREV_A];
                    float f15 = fArr[length + PREV_R2];
                    float f16 = fArr[length - 2];
                    f11 = fArr[length - 1];
                    f5 = f16;
                    f6 = f15;
                    f7 = f14;
                    f8 = f13;
                } else {
                    int binarySearch = Animation.binarySearch(fArr, f12, 8);
                    float f17 = fArr[binarySearch - 7];
                    float f18 = fArr[binarySearch - 6];
                    float f19 = fArr[binarySearch + PREV_B];
                    float f20 = fArr[binarySearch + PREV_A];
                    float f21 = fArr[binarySearch + PREV_R2];
                    float f22 = fArr[binarySearch - 2];
                    float f23 = fArr[binarySearch - 1];
                    float f24 = fArr[binarySearch];
                    float curvePercent = getCurvePercent((binarySearch / 8) - 1, 1.0f - ((f12 - f24) / (fArr[binarySearch - 8] - f24)));
                    float f25 = ((fArr[binarySearch + 1] - f17) * curvePercent) + f17;
                    float f26 = ((fArr[binarySearch + 2] - f18) * curvePercent) + f18;
                    f8 = ((fArr[binarySearch + 3] - f19) * curvePercent) + f19;
                    f7 = ((fArr[binarySearch + 4] - f20) * curvePercent) + f20;
                    f6 = ((fArr[binarySearch + 5] - f21) * curvePercent) + f21;
                    f5 = ((fArr[binarySearch + 6] - f22) * curvePercent) + f22;
                    f11 = f23 + ((fArr[binarySearch + 7] - f23) * curvePercent);
                    f10 = f25;
                    f9 = f26;
                }
                if (f4 == 1.0f) {
                    slot.color.b(f10, f9, f8, f7);
                    slot.darkColor.b(f6, f5, f11, 1.0f);
                    return;
                }
                b bVar5 = slot.color;
                b bVar6 = slot.darkColor;
                if (mixBlend == MixBlend.setup) {
                    bVar5.b(slot.data.color);
                    bVar6.b(slot.data.darkColor);
                }
                bVar5.a((f10 - bVar5.f6934a) * f4, (f9 - bVar5.f6935b) * f4, (f8 - bVar5.f6936c) * f4, (f7 - bVar5.f6937d) * f4);
                bVar6.a((f6 - bVar6.f6934a) * f4, (f5 - bVar6.f6935b) * f4, (f11 - bVar6.f6936c) * f4, CurveTimeline.LINEAR);
            }
        }

        public float[] getFrames() {
            return this.frames;
        }

        public int getPropertyId() {
            return (TimelineType.twoColor.ordinal() << 24) + this.slotIndex;
        }

        public int getSlotIndex() {
            return this.slotIndex;
        }

        public void setFrame(int i2, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
            int i3 = i2 * 8;
            float[] fArr = this.frames;
            fArr[i3] = f2;
            fArr[i3 + 1] = f3;
            fArr[i3 + 2] = f4;
            fArr[i3 + 3] = f5;
            fArr[i3 + 4] = f6;
            fArr[i3 + 5] = f7;
            fArr[i3 + 6] = f8;
            fArr[i3 + 7] = f9;
        }

        public void setSlotIndex(int i2) {
            if (i2 >= 0) {
                this.slotIndex = i2;
                return;
            }
            throw new IllegalArgumentException("index must be >= 0.");
        }
    }

    public Animation(String str, a<Timeline> aVar, float f2) {
        if (str == null) {
            throw new IllegalArgumentException("name cannot be null.");
        } else if (aVar != null) {
            this.name = str;
            this.timelines = aVar;
            this.duration = f2;
        } else {
            throw new IllegalArgumentException("timelines cannot be null.");
        }
    }

    static int binarySearch(float[] fArr, float f2, int i2) {
        int length = (fArr.length / i2) - 2;
        if (length == 0) {
            return i2;
        }
        int i3 = length >>> 1;
        int i4 = 0;
        while (true) {
            int i5 = i3 + 1;
            if (fArr[i5 * i2] <= f2) {
                i4 = i5;
            } else {
                length = i3;
            }
            if (i4 == length) {
                return (i4 + 1) * i2;
            }
            i3 = (i4 + length) >>> 1;
        }
    }

    static int linearSearch(float[] fArr, float f2, int i2) {
        int length = fArr.length - i2;
        int i3 = 0;
        while (i3 <= length) {
            if (fArr[i3] > f2) {
                return i3;
            }
            i3 += i2;
        }
        return -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0026 A[LOOP:0: B:12:0x0024->B:13:0x0026, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void apply(com.esotericsoftware.spine.Skeleton r15, float r16, float r17, boolean r18, com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Event> r19, float r20, com.esotericsoftware.spine.Animation.MixBlend r21, com.esotericsoftware.spine.Animation.MixDirection r22) {
        /*
            r14 = this;
            r0 = r14
            if (r15 == 0) goto L_0x003e
            if (r18 == 0) goto L_0x001a
            float r1 = r0.duration
            r2 = 0
            int r3 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r3 == 0) goto L_0x001a
            float r3 = r17 % r1
            int r2 = (r16 > r2 ? 1 : (r16 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x0016
            float r1 = r16 % r1
            r9 = r1
            goto L_0x0018
        L_0x0016:
            r9 = r16
        L_0x0018:
            r10 = r3
            goto L_0x001e
        L_0x001a:
            r9 = r16
            r10 = r17
        L_0x001e:
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Animation$Timeline> r11 = r0.timelines
            r1 = 0
            int r12 = r11.f5374b
            r13 = 0
        L_0x0024:
            if (r13 >= r12) goto L_0x003d
            java.lang.Object r1 = r11.get(r13)
            com.esotericsoftware.spine.Animation$Timeline r1 = (com.esotericsoftware.spine.Animation.Timeline) r1
            r2 = r15
            r3 = r9
            r4 = r10
            r5 = r19
            r6 = r20
            r7 = r21
            r8 = r22
            r1.apply(r2, r3, r4, r5, r6, r7, r8)
            int r13 = r13 + 1
            goto L_0x0024
        L_0x003d:
            return
        L_0x003e:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "skeleton cannot be null."
            r1.<init>(r2)
            goto L_0x0047
        L_0x0046:
            throw r1
        L_0x0047:
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.Animation.apply(com.esotericsoftware.spine.Skeleton, float, float, boolean, com.badlogic.gdx.utils.a, float, com.esotericsoftware.spine.Animation$MixBlend, com.esotericsoftware.spine.Animation$MixDirection):void");
    }

    public float getDuration() {
        return this.duration;
    }

    public String getName() {
        return this.name;
    }

    public a<Timeline> getTimelines() {
        return this.timelines;
    }

    public void setDuration(float f2) {
        this.duration = f2;
    }

    public String toString() {
        return this.name;
    }

    static int binarySearch(float[] fArr, float f2) {
        int length = fArr.length - 2;
        if (length == 0) {
            return 1;
        }
        int i2 = length >>> 1;
        int i3 = 0;
        while (true) {
            int i4 = i2 + 1;
            if (fArr[i4] <= f2) {
                i3 = i4;
            } else {
                length = i2;
            }
            if (i3 == length) {
                return i3 + 1;
            }
            i2 = (i3 + length) >>> 1;
        }
    }
}
