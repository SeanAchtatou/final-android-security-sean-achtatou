package com.esotericsoftware.spine;

import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.m;
import java.util.Iterator;

public class PathConstraint implements Constraint {
    private static final int AFTER = -3;
    private static final int BEFORE = -2;
    private static final int NONE = -1;
    private static final float epsilon = 1.0E-5f;
    final a<Bone> bones;
    private final m curves = new m();
    final PathConstraintData data;
    private final m lengths = new m();
    float position;
    private final m positions = new m();
    float rotateMix;
    private final float[] segments = new float[10];
    private final m spaces = new m();
    float spacing;
    Slot target;
    float translateMix;
    private final m world = new m();

    public PathConstraint(PathConstraintData pathConstraintData, Skeleton skeleton) {
        if (pathConstraintData == null) {
            throw new IllegalArgumentException("data cannot be null.");
        } else if (skeleton != null) {
            this.data = pathConstraintData;
            this.bones = new a<>(pathConstraintData.bones.f5374b);
            Iterator<BoneData> it = pathConstraintData.bones.iterator();
            while (it.hasNext()) {
                this.bones.add(skeleton.findBone(it.next().name));
            }
            this.target = skeleton.findSlot(pathConstraintData.target.name);
            this.position = pathConstraintData.position;
            this.spacing = pathConstraintData.spacing;
            this.rotateMix = pathConstraintData.rotateMix;
            this.translateMix = pathConstraintData.translateMix;
        } else {
            throw new IllegalArgumentException("skeleton cannot be null.");
        }
    }

    private void addAfterPosition(float f2, float[] fArr, int i2, float[] fArr2, int i3) {
        float f3 = fArr[i2 + 2];
        float f4 = fArr[i2 + 3];
        float atan2 = (float) Math.atan2((double) (f4 - fArr[i2 + 1]), (double) (f3 - fArr[i2]));
        double d2 = (double) atan2;
        fArr2[i3] = f3 + (((float) Math.cos(d2)) * f2);
        fArr2[i3 + 1] = f4 + (f2 * ((float) Math.sin(d2)));
        fArr2[i3 + 2] = atan2;
    }

    private void addBeforePosition(float f2, float[] fArr, int i2, float[] fArr2, int i3) {
        float f3 = fArr[i2];
        float f4 = fArr[i2 + 1];
        float atan2 = (float) Math.atan2((double) (fArr[i2 + 3] - f4), (double) (fArr[i2 + 2] - f3));
        double d2 = (double) atan2;
        fArr2[i3] = f3 + (((float) Math.cos(d2)) * f2);
        fArr2[i3 + 1] = f4 + (f2 * ((float) Math.sin(d2)));
        fArr2[i3 + 2] = atan2;
    }

    private void addCurvePosition(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float[] fArr, int i2, boolean z) {
        if (f2 < epsilon || Float.isNaN(f2)) {
            fArr[i2] = f3;
            fArr[i2 + 1] = f4;
            fArr[i2 + 2] = (float) Math.atan2((double) (f6 - f4), (double) (f5 - f3));
            return;
        }
        float f11 = f2 * f2;
        float f12 = f11 * f2;
        float f13 = 1.0f - f2;
        float f14 = f13 * f13;
        float f15 = f14 * f13;
        float f16 = f13 * f2;
        float f17 = 3.0f * f16;
        float f18 = f13 * f17;
        float f19 = f17 * f2;
        float f20 = (f3 * f15) + (f5 * f18) + (f7 * f19) + (f9 * f12);
        float f21 = (f15 * f4) + (f18 * f6) + (f8 * f19) + (f12 * f10);
        fArr[i2] = f20;
        fArr[i2 + 1] = f21;
        if (!z) {
            return;
        }
        if (f2 < 0.001f) {
            fArr[i2 + 2] = (float) Math.atan2((double) (f6 - f4), (double) (f5 - f3));
        } else {
            fArr[i2 + 2] = (float) Math.atan2((double) (f21 - (((f4 * f14) + ((f6 * f16) * 2.0f)) + (f8 * f11))), (double) (f20 - (((f3 * f14) + ((f5 * f16) * 2.0f)) + (f11 * f7))));
        }
    }

    public void apply() {
        update();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x03e5  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0403 A[LOOP:8: B:103:0x03fd->B:105:0x0403, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0408  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x040a  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x00c9 A[EDGE_INSN: B:123:0x00c9->B:38:0x00c9 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x02ed A[EDGE_INSN: B:130:0x02ed->B:92:0x02ed ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0406 A[EDGE_INSN: B:133:0x0406->B:106:0x0406 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c6 A[LOOP:2: B:35:0x00c0->B:37:0x00c6, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x02ea A[LOOP:6: B:89:0x02e4->B:91:0x02ea, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x02f0  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x02f9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public float[] computeWorldPositions(com.esotericsoftware.spine.attachments.PathAttachment r48, int r49, boolean r50, boolean r51, boolean r52) {
        /*
            r47 = this;
            r13 = r47
            r14 = r49
            com.esotericsoftware.spine.Slot r15 = r13.target
            float r7 = r13.position
            com.badlogic.gdx.utils.m r0 = r13.spaces
            float[] r12 = r0.f5527a
            com.badlogic.gdx.utils.m r0 = r13.positions
            int r1 = r14 * 3
            r16 = 2
            int r1 = r1 + 2
            float[] r17 = r0.d(r1)
            boolean r18 = r48.getClosed()
            int r19 = r48.getWorldVerticesLength()
            int r8 = r19 / 6
            boolean r0 = r48.getConstantSpeed()
            r11 = 8
            r20 = 4
            r21 = 6
            r22 = 0
            r23 = 0
            r24 = 1
            if (r0 != 0) goto L_0x0150
            float[] r25 = r48.getLengths()
            if (r18 == 0) goto L_0x003c
            r0 = 1
            goto L_0x003d
        L_0x003c:
            r0 = 2
        L_0x003d:
            int r10 = r8 - r0
            r26 = r25[r10]
            if (r51 == 0) goto L_0x0045
            float r7 = r7 * r26
        L_0x0045:
            if (r52 == 0) goto L_0x0053
            r0 = 1
        L_0x0048:
            if (r0 >= r14) goto L_0x0053
            r1 = r12[r0]
            float r1 = r1 * r26
            r12[r0] = r1
            int r0 = r0 + 1
            goto L_0x0048
        L_0x0053:
            com.badlogic.gdx.utils.m r0 = r13.world
            float[] r27 = r0.d(r11)
            r8 = 0
            r9 = -1
            r11 = 0
            r28 = 0
        L_0x005e:
            if (r11 >= r14) goto L_0x014f
            r29 = r12[r11]
            float r30 = r7 + r29
            if (r18 == 0) goto L_0x0070
            float r0 = r30 % r26
            int r1 = (r0 > r22 ? 1 : (r0 == r22 ? 0 : -1))
            if (r1 >= 0) goto L_0x006e
            float r0 = r0 + r26
        L_0x006e:
            r8 = 0
            goto L_0x00c0
        L_0x0070:
            int r0 = (r30 > r22 ? 1 : (r30 == r22 ? 0 : -1))
            if (r0 >= 0) goto L_0x009a
            r0 = -2
            if (r9 == r0) goto L_0x0084
            r9 = -2
            r2 = 2
            r3 = 4
            r5 = 0
            r6 = 2
            r0 = r48
            r1 = r15
            r4 = r27
            r0.computeWorldVertices(r1, r2, r3, r4, r5, r6)
        L_0x0084:
            r3 = 0
            r0 = r47
            r1 = r30
            r2 = r27
            r4 = r17
            r5 = r28
            r0.addBeforePosition(r1, r2, r3, r4, r5)
        L_0x0092:
            r32 = r10
            r33 = r11
            r35 = r12
            goto L_0x0143
        L_0x009a:
            int r0 = (r30 > r26 ? 1 : (r30 == r26 ? 0 : -1))
            if (r0 <= 0) goto L_0x00be
            r0 = -3
            if (r9 == r0) goto L_0x00af
            r9 = -3
            int r2 = r19 + -6
            r3 = 4
            r5 = 0
            r6 = 2
            r0 = r48
            r1 = r15
            r4 = r27
            r0.computeWorldVertices(r1, r2, r3, r4, r5, r6)
        L_0x00af:
            float r1 = r30 - r26
            r3 = 0
            r0 = r47
            r2 = r27
            r4 = r17
            r5 = r28
            r0.addAfterPosition(r1, r2, r3, r4, r5)
            goto L_0x0092
        L_0x00be:
            r0 = r30
        L_0x00c0:
            r1 = r25[r8]
            int r2 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r2 <= 0) goto L_0x00c9
            int r8 = r8 + 1
            goto L_0x00c0
        L_0x00c9:
            if (r8 != 0) goto L_0x00cc
            goto L_0x00d2
        L_0x00cc:
            int r2 = r8 + -1
            r2 = r25[r2]
            float r0 = r0 - r2
            float r1 = r1 - r2
        L_0x00d2:
            float r0 = r0 / r1
            r7 = r0
            if (r8 == r9) goto L_0x0100
            if (r18 == 0) goto L_0x00ed
            if (r8 != r10) goto L_0x00ed
            int r2 = r19 + -4
            r3 = 4
            r5 = 0
            r6 = 2
            r0 = r48
            r1 = r15
            r4 = r27
            r0.computeWorldVertices(r1, r2, r3, r4, r5, r6)
            r2 = 0
            r5 = 4
            r0.computeWorldVertices(r1, r2, r3, r4, r5, r6)
            goto L_0x00fd
        L_0x00ed:
            int r0 = r8 * 6
            int r2 = r0 + 2
            r3 = 8
            r5 = 0
            r6 = 2
            r0 = r48
            r1 = r15
            r4 = r27
            r0.computeWorldVertices(r1, r2, r3, r4, r5, r6)
        L_0x00fd:
            r31 = r8
            goto L_0x0102
        L_0x0100:
            r31 = r9
        L_0x0102:
            r2 = r27[r23]
            r3 = r27[r24]
            r4 = r27[r16]
            r0 = 3
            r5 = r27[r0]
            r6 = r27[r20]
            r0 = 5
            r9 = r27[r0]
            r32 = r27[r21]
            r0 = 7
            r33 = r27[r0]
            if (r50 != 0) goto L_0x0124
            if (r11 <= 0) goto L_0x0121
            r0 = 925353388(0x3727c5ac, float:1.0E-5)
            int r0 = (r29 > r0 ? 1 : (r29 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x0121
            goto L_0x0124
        L_0x0121:
            r29 = 0
            goto L_0x0126
        L_0x0124:
            r29 = 1
        L_0x0126:
            r0 = r47
            r1 = r7
            r7 = r9
            r34 = r8
            r8 = r32
            r9 = r33
            r32 = r10
            r10 = r17
            r33 = r11
            r11 = r28
            r35 = r12
            r12 = r29
            r0.addCurvePosition(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            r9 = r31
            r8 = r34
        L_0x0143:
            int r11 = r33 + 1
            int r28 = r28 + 3
            r7 = r30
            r10 = r32
            r12 = r35
            goto L_0x005e
        L_0x014f:
            return r17
        L_0x0150:
            r35 = r12
            if (r18 == 0) goto L_0x017e
            int r10 = r19 + 2
            com.badlogic.gdx.utils.m r0 = r13.world
            float[] r12 = r0.d(r10)
            r2 = 2
            int r19 = r10 + -4
            r5 = 0
            r6 = 2
            r0 = r48
            r1 = r15
            r3 = r19
            r4 = r12
            r0.computeWorldVertices(r1, r2, r3, r4, r5, r6)
            r2 = 0
            r3 = 2
            r5 = r19
            r0.computeWorldVertices(r1, r2, r3, r4, r5, r6)
            int r0 = r10 + -2
            r1 = r12[r23]
            r12[r0] = r1
            int r0 = r10 + -1
            r1 = r12[r24]
            r12[r0] = r1
            goto L_0x0193
        L_0x017e:
            int r8 = r8 + -1
            int r10 = r19 + -4
            com.badlogic.gdx.utils.m r0 = r13.world
            float[] r12 = r0.d(r10)
            r2 = 2
            r5 = 0
            r6 = 2
            r0 = r48
            r1 = r15
            r3 = r10
            r4 = r12
            r0.computeWorldVertices(r1, r2, r3, r4, r5, r6)
        L_0x0193:
            r19 = r10
            r15 = r12
            com.badlogic.gdx.utils.m r0 = r13.curves
            float[] r25 = r0.d(r8)
            r0 = r15[r23]
            r1 = r15[r24]
            r2 = r1
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r10 = 0
            r12 = 0
            r26 = 0
            r1 = r0
            r0 = 0
        L_0x01ab:
            r27 = 1042983595(0x3e2aaaab, float:0.16666667)
            r28 = 1077936128(0x40400000, float:3.0)
            r29 = 1073741824(0x40000000, float:2.0)
            if (r0 >= r8) goto L_0x026b
            r3 = r15[r16]
            int r4 = r16 + 1
            r4 = r15[r4]
            int r5 = r16 + 2
            r5 = r15[r5]
            int r6 = r16 + 3
            r6 = r15[r6]
            int r10 = r16 + 4
            r10 = r15[r10]
            int r12 = r16 + 5
            r12 = r15[r12]
            float r30 = r3 * r29
            float r30 = r1 - r30
            float r30 = r30 + r5
            r31 = 1044381696(0x3e400000, float:0.1875)
            float r30 = r30 * r31
            float r32 = r4 * r29
            float r32 = r2 - r32
            float r32 = r32 + r6
            float r32 = r32 * r31
            float r31 = r3 - r5
            float r31 = r31 * r28
            float r31 = r31 - r1
            float r31 = r31 + r10
            r33 = 1035993088(0x3dc00000, float:0.09375)
            float r31 = r31 * r33
            float r34 = r4 - r6
            float r34 = r34 * r28
            float r34 = r34 - r2
            float r34 = r34 + r12
            float r34 = r34 * r33
            float r28 = r30 * r29
            float r28 = r28 + r31
            float r29 = r29 * r32
            float r29 = r29 + r34
            float r1 = r3 - r1
            r33 = 1061158912(0x3f400000, float:0.75)
            float r1 = r1 * r33
            float r1 = r1 + r30
            float r30 = r31 * r27
            float r1 = r1 + r30
            float r2 = r4 - r2
            r30 = 1061158912(0x3f400000, float:0.75)
            float r2 = r2 * r30
            float r2 = r2 + r32
            float r27 = r27 * r34
            float r2 = r2 + r27
            float r27 = r1 * r1
            float r30 = r2 * r2
            float r9 = r27 + r30
            r33 = r12
            double r11 = (double) r9
            double r11 = java.lang.Math.sqrt(r11)
            float r9 = (float) r11
            float r26 = r26 + r9
            float r1 = r1 + r28
            float r2 = r2 + r29
            float r28 = r28 + r31
            float r29 = r29 + r34
            float r9 = r1 * r1
            float r11 = r2 * r2
            float r9 = r9 + r11
            double r11 = (double) r9
            double r11 = java.lang.Math.sqrt(r11)
            float r9 = (float) r11
            float r26 = r26 + r9
            float r1 = r1 + r28
            float r2 = r2 + r29
            float r9 = r1 * r1
            float r11 = r2 * r2
            float r9 = r9 + r11
            double r11 = (double) r9
            double r11 = java.lang.Math.sqrt(r11)
            float r9 = (float) r11
            float r26 = r26 + r9
            float r28 = r28 + r31
            float r1 = r1 + r28
            float r29 = r29 + r34
            float r2 = r2 + r29
            float r1 = r1 * r1
            float r2 = r2 * r2
            float r1 = r1 + r2
            double r1 = (double) r1
            double r1 = java.lang.Math.sqrt(r1)
            float r1 = (float) r1
            float r26 = r26 + r1
            r25[r0] = r26
            int r0 = r0 + 1
            int r16 = r16 + 6
            r1 = r10
            r2 = r33
            r12 = r2
            r11 = 8
            goto L_0x01ab
        L_0x026b:
            if (r51 == 0) goto L_0x0270
            float r7 = r7 * r26
            goto L_0x027c
        L_0x0270:
            float[] r0 = r48.getLengths()
            int r8 = r8 + -1
            r0 = r0[r8]
            float r0 = r26 / r0
            float r7 = r7 * r0
        L_0x027c:
            if (r52 == 0) goto L_0x028a
            r0 = 1
        L_0x027f:
            if (r0 >= r14) goto L_0x028a
            r8 = r35[r0]
            float r8 = r8 * r26
            r35[r0] = r8
            int r0 = r0 + 1
            goto L_0x027f
        L_0x028a:
            float[] r11 = r13.segments
            r21 = r1
            r31 = r2
            r32 = r3
            r33 = r4
            r34 = r5
            r36 = r6
            r37 = r10
            r38 = r12
            r6 = 0
            r8 = -1
            r9 = 0
            r10 = 0
            r12 = 0
            r16 = 0
        L_0x02a3:
            if (r12 >= r14) goto L_0x0470
            r0 = r35[r12]
            float r39 = r7 + r0
            if (r18 == 0) goto L_0x02b5
            float r1 = r39 % r26
            int r2 = (r1 > r22 ? 1 : (r1 == r22 ? 0 : -1))
            if (r2 >= 0) goto L_0x02b3
            float r1 = r1 + r26
        L_0x02b3:
            r7 = 0
            goto L_0x02e4
        L_0x02b5:
            int r1 = (r39 > r22 ? 1 : (r39 == r22 ? 0 : -1))
            if (r1 >= 0) goto L_0x02c7
            r3 = 0
            r0 = r47
            r1 = r39
            r2 = r15
            r4 = r17
            r5 = r16
            r0.addBeforePosition(r1, r2, r3, r4, r5)
            goto L_0x02d9
        L_0x02c7:
            int r1 = (r39 > r26 ? 1 : (r39 == r26 ? 0 : -1))
            if (r1 <= 0) goto L_0x02e1
            float r1 = r39 - r26
            int r3 = r19 + -4
            r0 = r47
            r2 = r15
            r4 = r17
            r5 = r16
            r0.addAfterPosition(r1, r2, r3, r4, r5)
        L_0x02d9:
            r30 = r11
            r46 = r12
            r45 = 8
            goto L_0x0466
        L_0x02e1:
            r7 = r6
            r1 = r39
        L_0x02e4:
            r2 = r25[r7]
            int r3 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r3 <= 0) goto L_0x02ed
            int r7 = r7 + 1
            goto L_0x02e4
        L_0x02ed:
            if (r7 != 0) goto L_0x02f0
            goto L_0x02f6
        L_0x02f0:
            int r3 = r7 + -1
            r3 = r25[r3]
            float r1 = r1 - r3
            float r2 = r2 - r3
        L_0x02f6:
            float r1 = r1 / r2
            if (r7 == r8) goto L_0x03e5
            int r2 = r7 * 6
            r3 = r15[r2]
            int r4 = r2 + 1
            r4 = r15[r4]
            int r5 = r2 + 2
            r5 = r15[r5]
            int r6 = r2 + 3
            r6 = r15[r6]
            int r8 = r2 + 4
            r8 = r15[r8]
            int r9 = r2 + 5
            r9 = r15[r9]
            int r10 = r2 + 6
            r10 = r15[r10]
            int r2 = r2 + 7
            r2 = r15[r2]
            float r21 = r5 * r29
            float r21 = r3 - r21
            float r21 = r21 + r8
            r31 = 1022739087(0x3cf5c28f, float:0.03)
            float r21 = r21 * r31
            float r31 = r6 * r29
            float r31 = r4 - r31
            float r31 = r31 + r9
            r32 = 1022739087(0x3cf5c28f, float:0.03)
            float r31 = r31 * r32
            float r32 = r5 - r8
            float r32 = r32 * r28
            float r32 = r32 - r3
            float r32 = r32 + r10
            r33 = 1002740646(0x3bc49ba6, float:0.006)
            float r32 = r32 * r33
            float r33 = r6 - r9
            float r33 = r33 * r28
            float r33 = r33 - r4
            float r33 = r33 + r2
            r34 = 1002740646(0x3bc49ba6, float:0.006)
            float r33 = r33 * r34
            float r34 = r21 * r29
            float r34 = r34 + r32
            float r36 = r31 * r29
            float r36 = r36 + r33
            float r37 = r5 - r3
            r38 = 1050253722(0x3e99999a, float:0.3)
            float r37 = r37 * r38
            float r37 = r37 + r21
            float r21 = r32 * r27
            float r37 = r37 + r21
            float r21 = r6 - r4
            float r21 = r21 * r38
            float r21 = r21 + r31
            float r31 = r33 * r27
            float r21 = r21 + r31
            float r31 = r37 * r37
            float r38 = r21 * r21
            r48 = r2
            float r2 = r31 + r38
            r51 = r3
            double r2 = (double) r2
            double r2 = java.lang.Math.sqrt(r2)
            float r2 = (float) r2
            r11[r23] = r2
            r3 = r2
            r52 = r4
            r2 = 1
        L_0x037e:
            r4 = 8
            if (r2 >= r4) goto L_0x03a0
            float r37 = r37 + r34
            float r21 = r21 + r36
            float r34 = r34 + r32
            float r36 = r36 + r33
            float r4 = r37 * r37
            float r31 = r21 * r21
            float r4 = r4 + r31
            r31 = r5
            double r4 = (double) r4
            double r4 = java.lang.Math.sqrt(r4)
            float r4 = (float) r4
            float r3 = r3 + r4
            r11[r2] = r3
            int r2 = r2 + 1
            r5 = r31
            goto L_0x037e
        L_0x03a0:
            r31 = r5
            float r37 = r37 + r34
            float r21 = r21 + r36
            float r2 = r37 * r37
            float r4 = r21 * r21
            float r2 = r2 + r4
            double r4 = (double) r2
            double r4 = java.lang.Math.sqrt(r4)
            float r2 = (float) r4
            float r3 = r3 + r2
            r30 = 8
            r11[r30] = r3
            float r34 = r34 + r32
            float r37 = r37 + r34
            float r36 = r36 + r33
            float r21 = r21 + r36
            float r37 = r37 * r37
            float r21 = r21 * r21
            float r2 = r37 + r21
            double r4 = (double) r2
            double r4 = java.lang.Math.sqrt(r4)
            float r2 = (float) r4
            float r2 = r2 + r3
            r3 = 9
            r11[r3] = r2
            r40 = r48
            r32 = r52
            r21 = r2
            r34 = r6
            r41 = r7
            r36 = r8
            r37 = r9
            r38 = r10
            r33 = r31
            r10 = 0
            r31 = r51
            goto L_0x03fb
        L_0x03e5:
            r30 = 8
            r41 = r8
            r40 = r38
            r38 = r37
            r37 = r36
            r36 = r34
            r34 = r33
            r33 = r32
            r32 = r31
            r31 = r21
            r21 = r9
        L_0x03fb:
            float r1 = r1 * r21
        L_0x03fd:
            r2 = r11[r10]
            int r3 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r3 <= 0) goto L_0x0406
            int r10 = r10 + 1
            goto L_0x03fd
        L_0x0406:
            if (r10 != 0) goto L_0x040a
            float r1 = r1 / r2
            goto L_0x0413
        L_0x040a:
            int r3 = r10 + -1
            r3 = r11[r3]
            float r4 = (float) r10
            float r1 = r1 - r3
            float r2 = r2 - r3
            float r1 = r1 / r2
            float r1 = r1 + r4
        L_0x0413:
            r2 = 1036831949(0x3dcccccd, float:0.1)
            float r1 = r1 * r2
            if (r50 != 0) goto L_0x0427
            if (r12 <= 0) goto L_0x0424
            r2 = 925353388(0x3727c5ac, float:1.0E-5)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x0424
            goto L_0x0427
        L_0x0424:
            r42 = 0
            goto L_0x0429
        L_0x0427:
            r42 = 1
        L_0x0429:
            r0 = r47
            r2 = r31
            r3 = r32
            r4 = r33
            r5 = r34
            r6 = r36
            r43 = r7
            r7 = r37
            r8 = r38
            r9 = r40
            r44 = r10
            r10 = r17
            r30 = r11
            r45 = 8
            r11 = r16
            r46 = r12
            r12 = r42
            r0.addCurvePosition(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            r9 = r21
            r21 = r31
            r31 = r32
            r32 = r33
            r33 = r34
            r34 = r36
            r36 = r37
            r37 = r38
            r38 = r40
            r8 = r41
            r6 = r43
            r10 = r44
        L_0x0466:
            int r12 = r46 + 1
            int r16 = r16 + 3
            r11 = r30
            r7 = r39
            goto L_0x02a3
        L_0x0470:
            return r17
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.PathConstraint.computeWorldPositions(com.esotericsoftware.spine.attachments.PathAttachment, int, boolean, boolean, boolean):float[]");
    }

    public a<Bone> getBones() {
        return this.bones;
    }

    public PathConstraintData getData() {
        return this.data;
    }

    public int getOrder() {
        return this.data.order;
    }

    public float getPosition() {
        return this.position;
    }

    public float getRotateMix() {
        return this.rotateMix;
    }

    public float getSpacing() {
        return this.spacing;
    }

    public Slot getTarget() {
        return this.target;
    }

    public float getTranslateMix() {
        return this.translateMix;
    }

    public void setPosition(float f2) {
        this.position = f2;
    }

    public void setRotateMix(float f2) {
        this.rotateMix = f2;
    }

    public void setSpacing(float f2) {
        this.spacing = f2;
    }

    public void setTarget(Slot slot) {
        this.target = slot;
    }

    public void setTranslateMix(float f2) {
        this.translateMix = f2;
    }

    public String toString() {
        return this.data.name;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0129, code lost:
        if (r14 == com.esotericsoftware.spine.PathConstraintData.RotateMode.chain) goto L_0x014c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x01ea  */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0215  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0220  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0222  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0254  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01a6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void update() {
        /*
            r30 = this;
            r6 = r30
            com.esotericsoftware.spine.Slot r0 = r6.target
            com.esotericsoftware.spine.attachments.Attachment r0 = r0.attachment
            boolean r1 = r0 instanceof com.esotericsoftware.spine.attachments.PathAttachment
            if (r1 != 0) goto L_0x000b
            return
        L_0x000b:
            float r7 = r6.rotateMix
            float r8 = r6.translateMix
            r9 = 0
            int r1 = (r8 > r9 ? 1 : (r8 == r9 ? 0 : -1))
            if (r1 <= 0) goto L_0x0016
            r1 = 1
            goto L_0x0017
        L_0x0016:
            r1 = 0
        L_0x0017:
            int r2 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r2 <= 0) goto L_0x001d
            r12 = 1
            goto L_0x001e
        L_0x001d:
            r12 = 0
        L_0x001e:
            if (r1 != 0) goto L_0x0023
            if (r12 != 0) goto L_0x0023
            return
        L_0x0023:
            com.esotericsoftware.spine.PathConstraintData r13 = r6.data
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r1 = r13.spacingMode
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r2 = com.esotericsoftware.spine.PathConstraintData.SpacingMode.percent
            if (r1 != r2) goto L_0x002d
            r5 = 1
            goto L_0x002e
        L_0x002d:
            r5 = 0
        L_0x002e:
            com.esotericsoftware.spine.PathConstraintData$RotateMode r14 = r13.rotateMode
            com.esotericsoftware.spine.PathConstraintData$RotateMode r1 = com.esotericsoftware.spine.PathConstraintData.RotateMode.tangent
            if (r14 != r1) goto L_0x0036
            r15 = 1
            goto L_0x0037
        L_0x0036:
            r15 = 0
        L_0x0037:
            com.esotericsoftware.spine.PathConstraintData$RotateMode r1 = com.esotericsoftware.spine.PathConstraintData.RotateMode.chainScale
            if (r14 != r1) goto L_0x003e
            r16 = 1
            goto L_0x0040
        L_0x003e:
            r16 = 0
        L_0x0040:
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Bone> r1 = r6.bones
            int r4 = r1.f5374b
            if (r15 == 0) goto L_0x0048
            r2 = r4
            goto L_0x004b
        L_0x0048:
            int r1 = r4 + 1
            r2 = r1
        L_0x004b:
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Bone> r1 = r6.bones
            T[] r3 = r1.f5373a
            com.badlogic.gdx.utils.m r1 = r6.spaces
            float[] r17 = r1.d(r2)
            float r1 = r6.spacing
            r19 = 925353388(0x3727c5ac, float:1.0E-5)
            if (r16 != 0) goto L_0x0071
            if (r5 != 0) goto L_0x005f
            goto L_0x0071
        L_0x005f:
            r11 = 1
        L_0x0060:
            if (r11 >= r2) goto L_0x0067
            r17[r11] = r1
            int r11 = r11 + 1
            goto L_0x0060
        L_0x0067:
            r23 = r3
            r22 = r4
            r25 = r12
            r18 = 0
            goto L_0x0103
        L_0x0071:
            if (r16 == 0) goto L_0x007c
            com.badlogic.gdx.utils.m r11 = r6.lengths
            float[] r11 = r11.d(r4)
            r18 = r11
            goto L_0x007e
        L_0x007c:
            r18 = 0
        L_0x007e:
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r11 = r13.spacingMode
            com.esotericsoftware.spine.PathConstraintData$SpacingMode r10 = com.esotericsoftware.spine.PathConstraintData.SpacingMode.length
            if (r11 != r10) goto L_0x0086
            r10 = 1
            goto L_0x0087
        L_0x0086:
            r10 = 0
        L_0x0087:
            int r11 = r2 + -1
            r9 = 0
        L_0x008a:
            if (r9 >= r11) goto L_0x00fd
            r22 = r3[r9]
            r23 = r3
            r3 = r22
            com.esotericsoftware.spine.Bone r3 = (com.esotericsoftware.spine.Bone) r3
            r22 = r4
            com.esotericsoftware.spine.BoneData r4 = r3.data
            float r4 = r4.length
            int r24 = (r4 > r19 ? 1 : (r4 == r19 ? 0 : -1))
            if (r24 >= 0) goto L_0x00ac
            r3 = 0
            if (r16 == 0) goto L_0x00a3
            r18[r9] = r3
        L_0x00a3:
            int r9 = r9 + 1
            r17[r9] = r3
            r24 = r11
        L_0x00a9:
            r25 = r12
            goto L_0x00f4
        L_0x00ac:
            if (r5 == 0) goto L_0x00cc
            r24 = r11
            if (r16 == 0) goto L_0x00c7
            float r11 = r3.f6593a
            float r11 = r11 * r4
            float r3 = r3.f6595c
            float r4 = r4 * r3
            float r11 = r11 * r11
            float r4 = r4 * r4
            float r11 = r11 + r4
            double r3 = (double) r11
            double r3 = java.lang.Math.sqrt(r3)
            float r3 = (float) r3
            r18[r9] = r3
        L_0x00c7:
            int r9 = r9 + 1
            r17[r9] = r1
            goto L_0x00a9
        L_0x00cc:
            r24 = r11
            float r11 = r3.f6593a
            float r11 = r11 * r4
            float r3 = r3.f6595c
            float r3 = r3 * r4
            float r11 = r11 * r11
            float r3 = r3 * r3
            float r11 = r11 + r3
            r25 = r12
            double r11 = (double) r11
            double r11 = java.lang.Math.sqrt(r11)
            float r3 = (float) r11
            if (r16 == 0) goto L_0x00e7
            r18[r9] = r3
        L_0x00e7:
            int r9 = r9 + 1
            if (r10 == 0) goto L_0x00ee
            float r11 = r4 + r1
            goto L_0x00ef
        L_0x00ee:
            r11 = r1
        L_0x00ef:
            float r11 = r11 * r3
            float r11 = r11 / r4
            r17[r9] = r11
        L_0x00f4:
            r4 = r22
            r3 = r23
            r11 = r24
            r12 = r25
            goto L_0x008a
        L_0x00fd:
            r23 = r3
            r22 = r4
            r25 = r12
        L_0x0103:
            r1 = r0
            com.esotericsoftware.spine.attachments.PathAttachment r1 = (com.esotericsoftware.spine.attachments.PathAttachment) r1
            com.esotericsoftware.spine.PathConstraintData$PositionMode r0 = r13.positionMode
            com.esotericsoftware.spine.PathConstraintData$PositionMode r3 = com.esotericsoftware.spine.PathConstraintData.PositionMode.percent
            if (r0 != r3) goto L_0x010e
            r4 = 1
            goto L_0x010f
        L_0x010e:
            r4 = 0
        L_0x010f:
            r0 = r30
            r9 = r23
            r3 = r15
            r10 = r22
            float[] r0 = r0.computeWorldPositions(r1, r2, r3, r4, r5)
            r1 = 0
            r2 = r0[r1]
            r1 = 1
            r3 = r0[r1]
            float r4 = r13.offsetRotation
            r5 = 0
            int r11 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r11 != 0) goto L_0x012c
            com.esotericsoftware.spine.PathConstraintData$RotateMode r5 = com.esotericsoftware.spine.PathConstraintData.RotateMode.chain
            if (r14 != r5) goto L_0x014b
            goto L_0x014c
        L_0x012c:
            com.esotericsoftware.spine.Slot r1 = r6.target
            com.esotericsoftware.spine.Bone r1 = r1.bone
            float r5 = r1.f6593a
            float r11 = r1.f6596d
            float r5 = r5 * r11
            float r11 = r1.f6594b
            float r1 = r1.f6595c
            float r11 = r11 * r1
            float r5 = r5 - r11
            r1 = 0
            int r1 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x0146
            r1 = 1016003125(0x3c8efa35, float:0.017453292)
            goto L_0x0149
        L_0x0146:
            r1 = -1131480523(0xffffffffbc8efa35, float:-0.017453292)
        L_0x0149:
            float r4 = r4 * r1
        L_0x014b:
            r1 = 0
        L_0x014c:
            r5 = 3
            r5 = r3
            r11 = 3
            r3 = r2
            r2 = 0
        L_0x0151:
            if (r2 >= r10) goto L_0x0275
            r12 = r9[r2]
            com.esotericsoftware.spine.Bone r12 = (com.esotericsoftware.spine.Bone) r12
            float r13 = r12.worldX
            float r14 = r3 - r13
            float r14 = r14 * r8
            float r13 = r13 + r14
            r12.worldX = r13
            float r13 = r12.worldY
            float r14 = r5 - r13
            float r14 = r14 * r8
            float r13 = r13 + r14
            r12.worldY = r13
            r13 = r0[r11]
            int r14 = r11 + 1
            r14 = r0[r14]
            float r3 = r13 - r3
            float r5 = r14 - r5
            if (r16 == 0) goto L_0x01a0
            r20 = r18[r2]
            int r21 = (r20 > r19 ? 1 : (r20 == r19 ? 0 : -1))
            if (r21 < 0) goto L_0x01a0
            float r21 = r3 * r3
            float r22 = r5 * r5
            float r6 = r21 + r22
            r21 = r8
            r23 = r9
            double r8 = (double) r6
            double r8 = java.lang.Math.sqrt(r8)
            float r6 = (float) r8
            float r6 = r6 / r20
            r8 = 1065353216(0x3f800000, float:1.0)
            float r6 = r6 - r8
            float r6 = r6 * r7
            float r6 = r6 + r8
            float r8 = r12.f6593a
            float r8 = r8 * r6
            r12.f6593a = r8
            float r8 = r12.f6595c
            float r8 = r8 * r6
            r12.f6595c = r8
            goto L_0x01a4
        L_0x01a0:
            r21 = r8
            r23 = r9
        L_0x01a4:
            if (r25 == 0) goto L_0x0254
            float r6 = r12.f6593a
            float r8 = r12.f6594b
            float r9 = r12.f6595c
            r22 = r10
            float r10 = r12.f6596d
            if (r15 == 0) goto L_0x01c1
            int r20 = r11 + -1
            r20 = r0[r20]
        L_0x01b6:
            r24 = r10
            r27 = r14
            r26 = r15
            r10 = r20
            r20 = r11
            goto L_0x01dd
        L_0x01c1:
            int r20 = r2 + 1
            r20 = r17[r20]
            int r20 = (r20 > r19 ? 1 : (r20 == r19 ? 0 : -1))
            if (r20 >= 0) goto L_0x01ce
            int r20 = r11 + 2
            r20 = r0[r20]
            goto L_0x01b6
        L_0x01ce:
            r24 = r10
            r20 = r11
            double r10 = (double) r5
            r27 = r14
            r26 = r15
            double r14 = (double) r3
            double r10 = java.lang.Math.atan2(r10, r14)
            float r10 = (float) r10
        L_0x01dd:
            double r14 = (double) r9
            r11 = r4
            r28 = r5
            double r4 = (double) r6
            double r4 = java.lang.Math.atan2(r14, r4)
            float r4 = (float) r4
            float r10 = r10 - r4
            if (r1 == 0) goto L_0x0215
            double r4 = (double) r10
            double r14 = java.lang.Math.cos(r4)
            float r14 = (float) r14
            double r4 = java.lang.Math.sin(r4)
            float r4 = (float) r4
            com.esotericsoftware.spine.BoneData r5 = r12.data
            float r5 = r5.length
            float r15 = r14 * r6
            float r29 = r4 * r9
            float r15 = r15 - r29
            float r15 = r15 * r5
            float r15 = r15 - r3
            float r15 = r15 * r7
            float r13 = r13 + r15
            float r4 = r4 * r6
            float r14 = r14 * r9
            float r4 = r4 + r14
            float r5 = r5 * r4
            float r5 = r5 - r28
            float r5 = r5 * r7
            float r14 = r27 + r5
            r27 = r14
            goto L_0x0216
        L_0x0215:
            float r10 = r10 + r11
        L_0x0216:
            r3 = 1078530011(0x40490fdb, float:3.1415927)
            r4 = 1086918619(0x40c90fdb, float:6.2831855)
            int r3 = (r10 > r3 ? 1 : (r10 == r3 ? 0 : -1))
            if (r3 <= 0) goto L_0x0222
            float r10 = r10 - r4
            goto L_0x022a
        L_0x0222:
            r3 = -1068953637(0xffffffffc0490fdb, float:-3.1415927)
            int r3 = (r10 > r3 ? 1 : (r10 == r3 ? 0 : -1))
            if (r3 >= 0) goto L_0x022a
            float r10 = r10 + r4
        L_0x022a:
            float r10 = r10 * r7
            double r3 = (double) r10
            double r14 = java.lang.Math.cos(r3)
            float r5 = (float) r14
            double r3 = java.lang.Math.sin(r3)
            float r3 = (float) r3
            float r4 = r5 * r6
            float r10 = r3 * r9
            float r4 = r4 - r10
            r12.f6593a = r4
            float r4 = r5 * r8
            float r10 = r3 * r24
            float r4 = r4 - r10
            r12.f6594b = r4
            float r6 = r6 * r3
            float r9 = r9 * r5
            float r6 = r6 + r9
            r12.f6595c = r6
            float r3 = r3 * r8
            float r5 = r5 * r24
            float r3 = r3 + r5
            r12.f6596d = r3
            goto L_0x025d
        L_0x0254:
            r22 = r10
            r20 = r11
            r27 = r14
            r26 = r15
            r11 = r4
        L_0x025d:
            r3 = r13
            r5 = r27
            r4 = 0
            r12.appliedValid = r4
            int r2 = r2 + 1
            int r6 = r20 + 3
            r4 = r11
            r8 = r21
            r10 = r22
            r9 = r23
            r15 = r26
            r11 = r6
            r6 = r30
            goto L_0x0151
        L_0x0275:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.PathConstraint.update():void");
    }

    public PathConstraint(PathConstraint pathConstraint, Skeleton skeleton) {
        if (pathConstraint == null) {
            throw new IllegalArgumentException("constraint cannot be null.");
        } else if (skeleton != null) {
            this.data = pathConstraint.data;
            this.bones = new a<>(pathConstraint.bones.f5374b);
            Iterator<Bone> it = pathConstraint.bones.iterator();
            while (it.hasNext()) {
                this.bones.add(skeleton.bones.get(it.next().data.index));
            }
            this.target = skeleton.slots.get(pathConstraint.target.data.index);
            this.position = pathConstraint.position;
            this.spacing = pathConstraint.spacing;
            this.rotateMix = pathConstraint.rotateMix;
            this.translateMix = pathConstraint.translateMix;
        } else {
            throw new IllegalArgumentException("skeleton cannot be null.");
        }
    }
}
