package com.esotericsoftware.spine;

import com.badlogic.gdx.utils.a;

public class IkConstraintData {
    int bendDirection = 1;
    final a<BoneData> bones = new a<>();
    boolean compress;
    float mix = 1.0f;
    final String name;
    int order;
    boolean stretch;
    BoneData target;
    boolean uniform;

    public IkConstraintData(String str) {
        if (str != null) {
            this.name = str;
            return;
        }
        throw new IllegalArgumentException("name cannot be null.");
    }

    public int getBendDirection() {
        return this.bendDirection;
    }

    public a<BoneData> getBones() {
        return this.bones;
    }

    public boolean getCompress() {
        return this.compress;
    }

    public float getMix() {
        return this.mix;
    }

    public String getName() {
        return this.name;
    }

    public int getOrder() {
        return this.order;
    }

    public boolean getStretch() {
        return this.stretch;
    }

    public BoneData getTarget() {
        return this.target;
    }

    public boolean getUniform() {
        return this.uniform;
    }

    public void setBendDirection(int i2) {
        this.bendDirection = i2;
    }

    public void setCompress(boolean z) {
        this.compress = z;
    }

    public void setMix(float f2) {
        this.mix = f2;
    }

    public void setOrder(int i2) {
        this.order = i2;
    }

    public void setStretch(boolean z) {
        this.stretch = z;
    }

    public void setTarget(BoneData boneData) {
        if (boneData != null) {
            this.target = boneData;
            return;
        }
        throw new IllegalArgumentException("target cannot be null.");
    }

    public void setUniform(boolean z) {
        this.uniform = z;
    }

    public String toString() {
        return this.name;
    }
}
