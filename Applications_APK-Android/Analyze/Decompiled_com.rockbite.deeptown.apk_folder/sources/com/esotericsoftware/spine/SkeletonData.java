package com.esotericsoftware.spine;

import com.badlogic.gdx.utils.a;
import java.util.Iterator;

public class SkeletonData {
    final a<Animation> animations = new a<>();
    String audioPath;
    final a<BoneData> bones = new a<>();
    Skin defaultSkin;
    final a<EventData> events = new a<>();
    float fps = 30.0f;
    String hash;
    float height;
    final a<IkConstraintData> ikConstraints = new a<>();
    String imagesPath;
    String name;
    final a<PathConstraintData> pathConstraints = new a<>();
    final a<Skin> skins = new a<>();
    final a<SlotData> slots = new a<>();
    final a<TransformConstraintData> transformConstraints = new a<>();
    String version;
    float width;

    public Animation findAnimation(String str) {
        if (str != null) {
            a<Animation> aVar = this.animations;
            int i2 = aVar.f5374b;
            for (int i3 = 0; i3 < i2; i3++) {
                Animation animation = aVar.get(i3);
                if (animation.name.equals(str)) {
                    return animation;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("animationName cannot be null.");
    }

    public BoneData findBone(String str) {
        if (str != null) {
            a<BoneData> aVar = this.bones;
            int i2 = aVar.f5374b;
            for (int i3 = 0; i3 < i2; i3++) {
                BoneData boneData = aVar.get(i3);
                if (boneData.name.equals(str)) {
                    return boneData;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("boneName cannot be null.");
    }

    public EventData findEvent(String str) {
        if (str != null) {
            Iterator<EventData> it = this.events.iterator();
            while (it.hasNext()) {
                EventData next = it.next();
                if (next.name.equals(str)) {
                    return next;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("eventDataName cannot be null.");
    }

    public IkConstraintData findIkConstraint(String str) {
        if (str != null) {
            a<IkConstraintData> aVar = this.ikConstraints;
            int i2 = aVar.f5374b;
            for (int i3 = 0; i3 < i2; i3++) {
                IkConstraintData ikConstraintData = aVar.get(i3);
                if (ikConstraintData.name.equals(str)) {
                    return ikConstraintData;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("constraintName cannot be null.");
    }

    public PathConstraintData findPathConstraint(String str) {
        if (str != null) {
            a<PathConstraintData> aVar = this.pathConstraints;
            int i2 = aVar.f5374b;
            for (int i3 = 0; i3 < i2; i3++) {
                PathConstraintData pathConstraintData = aVar.get(i3);
                if (pathConstraintData.name.equals(str)) {
                    return pathConstraintData;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("constraintName cannot be null.");
    }

    public Skin findSkin(String str) {
        if (str != null) {
            Iterator<Skin> it = this.skins.iterator();
            while (it.hasNext()) {
                Skin next = it.next();
                if (next.name.equals(str)) {
                    return next;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("skinName cannot be null.");
    }

    public SlotData findSlot(String str) {
        if (str != null) {
            a<SlotData> aVar = this.slots;
            int i2 = aVar.f5374b;
            for (int i3 = 0; i3 < i2; i3++) {
                SlotData slotData = aVar.get(i3);
                if (slotData.name.equals(str)) {
                    return slotData;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("slotName cannot be null.");
    }

    public TransformConstraintData findTransformConstraint(String str) {
        if (str != null) {
            a<TransformConstraintData> aVar = this.transformConstraints;
            int i2 = aVar.f5374b;
            for (int i3 = 0; i3 < i2; i3++) {
                TransformConstraintData transformConstraintData = aVar.get(i3);
                if (transformConstraintData.name.equals(str)) {
                    return transformConstraintData;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("constraintName cannot be null.");
    }

    public a<Animation> getAnimations() {
        return this.animations;
    }

    public String getAudioPath() {
        return this.audioPath;
    }

    public a<BoneData> getBones() {
        return this.bones;
    }

    public Skin getDefaultSkin() {
        return this.defaultSkin;
    }

    public a<EventData> getEvents() {
        return this.events;
    }

    public float getFps() {
        return this.fps;
    }

    public String getHash() {
        return this.hash;
    }

    public float getHeight() {
        return this.height;
    }

    public a<IkConstraintData> getIkConstraints() {
        return this.ikConstraints;
    }

    public String getImagesPath() {
        return this.imagesPath;
    }

    public String getName() {
        return this.name;
    }

    public a<PathConstraintData> getPathConstraints() {
        return this.pathConstraints;
    }

    public a<Skin> getSkins() {
        return this.skins;
    }

    public a<SlotData> getSlots() {
        return this.slots;
    }

    public a<TransformConstraintData> getTransformConstraints() {
        return this.transformConstraints;
    }

    public String getVersion() {
        return this.version;
    }

    public float getWidth() {
        return this.width;
    }

    public void setAudioPath(String str) {
        this.audioPath = str;
    }

    public void setDefaultSkin(Skin skin) {
        this.defaultSkin = skin;
    }

    public void setFps(float f2) {
        this.fps = f2;
    }

    public void setHash(String str) {
        this.hash = str;
    }

    public void setHeight(float f2) {
        this.height = f2;
    }

    public void setImagesPath(String str) {
        this.imagesPath = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setVersion(String str) {
        this.version = str;
    }

    public void setWidth(float f2) {
        this.width = f2;
    }

    public String toString() {
        String str = this.name;
        return str != null ? str : super.toString();
    }
}
