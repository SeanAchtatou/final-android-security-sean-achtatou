package com.esotericsoftware.spine;

import com.badlogic.gdx.utils.a;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.utils.SpineUtils;
import java.util.Iterator;

public class IkConstraint implements Constraint {
    int bendDirection;
    final a<Bone> bones;
    boolean compress;
    final IkConstraintData data;
    float mix = 1.0f;
    boolean stretch;
    Bone target;

    public IkConstraint(IkConstraintData ikConstraintData, Skeleton skeleton) {
        if (ikConstraintData == null) {
            throw new IllegalArgumentException("data cannot be null.");
        } else if (skeleton != null) {
            this.data = ikConstraintData;
            this.mix = ikConstraintData.mix;
            this.bendDirection = ikConstraintData.bendDirection;
            this.compress = ikConstraintData.compress;
            this.stretch = ikConstraintData.stretch;
            this.bones = new a<>(ikConstraintData.bones.f5374b);
            Iterator<BoneData> it = ikConstraintData.bones.iterator();
            while (it.hasNext()) {
                this.bones.add(skeleton.findBone(it.next().name));
            }
            this.target = skeleton.findBone(ikConstraintData.target.name);
        } else {
            throw new IllegalArgumentException("skeleton cannot be null.");
        }
    }

    public void apply() {
        update();
    }

    public int getBendDirection() {
        return this.bendDirection;
    }

    public a<Bone> getBones() {
        return this.bones;
    }

    public boolean getCompress() {
        return this.compress;
    }

    public IkConstraintData getData() {
        return this.data;
    }

    public float getMix() {
        return this.mix;
    }

    public int getOrder() {
        return this.data.order;
    }

    public boolean getStretch() {
        return this.stretch;
    }

    public Bone getTarget() {
        return this.target;
    }

    public void setBendDirection(int i2) {
        this.bendDirection = i2;
    }

    public void setCompress(boolean z) {
        this.compress = z;
    }

    public void setMix(float f2) {
        this.mix = f2;
    }

    public void setStretch(boolean z) {
        this.stretch = z;
    }

    public void setTarget(Bone bone) {
        this.target = bone;
    }

    public String toString() {
        return this.data.name;
    }

    public void update() {
        Bone bone = this.target;
        a<Bone> aVar = this.bones;
        int i2 = aVar.f5374b;
        if (i2 == 1) {
            apply(aVar.a(), bone.worldX, bone.worldY, this.compress, this.stretch, this.data.uniform, this.mix);
        } else if (i2 == 2) {
            apply(aVar.a(), aVar.get(1), bone.worldX, bone.worldY, this.bendDirection, this.stretch, this.mix);
        }
    }

    public static void apply(Bone bone, float f2, float f3, boolean z, boolean z2, boolean z3, float f4) {
        Bone bone2 = bone;
        if (!bone2.appliedValid) {
            bone.updateAppliedTransform();
        }
        Bone bone3 = bone2.parent;
        float f5 = bone3.f6593a;
        float f6 = bone3.f6596d;
        float f7 = bone3.f6594b;
        float f8 = bone3.f6595c;
        float f9 = 1.0f / ((f5 * f6) - (f7 * f8));
        float f10 = f2 - bone3.worldX;
        float f11 = f3 - bone3.worldY;
        float f12 = (((f6 * f10) - (f7 * f11)) * f9) - bone2.ax;
        float f13 = (((f11 * f5) - (f10 * f8)) * f9) - bone2.ay;
        float atan2 = ((SpineUtils.atan2(f13, f12) * 57.295776f) - bone2.ashearX) - bone2.arotation;
        if (bone2.ascaleX < Animation.CurveTimeline.LINEAR) {
            atan2 += 180.0f;
        }
        if (atan2 > 180.0f) {
            atan2 -= 360.0f;
        } else if (atan2 < -180.0f) {
            atan2 += 360.0f;
        }
        float f14 = bone2.ascaleX;
        float f15 = bone2.ascaleY;
        if (z || z2) {
            float f16 = bone2.data.length * f14;
            float sqrt = (float) Math.sqrt((double) ((f12 * f12) + (f13 * f13)));
            if ((z && sqrt < f16) || (z2 && sqrt > f16 && f16 > 1.0E-4f)) {
                float f17 = (((sqrt / f16) - 1.0f) * f4) + 1.0f;
                f14 *= f17;
                if (z3) {
                    f15 *= f17;
                }
            }
        }
        bone.updateWorldTransform(bone2.ax, bone2.ay, bone2.arotation + (atan2 * f4), f14, f15, bone2.ashearX, bone2.ashearY);
    }

    public static void apply(Bone bone, Bone bone2, float f2, float f3, int i2, boolean z, float f4) {
        int i3;
        int i4;
        float f5;
        int i5;
        float f6;
        float f7;
        float f8;
        float f9;
        float f10;
        float f11;
        float f12;
        int i6;
        float f13;
        float f14;
        float f15;
        float f16;
        float f17;
        Bone bone3 = bone;
        Bone bone4 = bone2;
        if (f4 == Animation.CurveTimeline.LINEAR) {
            bone2.updateWorldTransform();
            return;
        }
        if (!bone3.appliedValid) {
            bone.updateAppliedTransform();
        }
        if (!bone4.appliedValid) {
            bone2.updateAppliedTransform();
        }
        float f18 = bone3.ax;
        float f19 = bone3.ay;
        float f20 = bone3.ascaleX;
        float f21 = bone3.ascaleY;
        float f22 = bone4.ascaleX;
        int i7 = 180;
        if (f20 < Animation.CurveTimeline.LINEAR) {
            f5 = -f20;
            i4 = -1;
            i3 = 180;
        } else {
            f5 = f20;
            i4 = 1;
            i3 = 0;
        }
        if (f21 < Animation.CurveTimeline.LINEAR) {
            f21 = -f21;
            i4 = -i4;
        }
        if (f22 < Animation.CurveTimeline.LINEAR) {
            f22 = -f22;
        } else {
            i7 = 0;
        }
        float f23 = bone4.ax;
        float f24 = bone3.f6593a;
        float f25 = bone3.f6594b;
        float f26 = bone3.f6595c;
        int i8 = i7;
        float f27 = bone3.f6596d;
        boolean z2 = Math.abs(f5 - f21) <= 1.0E-4f;
        if (!z2) {
            f7 = (f24 * f23) + bone3.worldX;
            f8 = (f26 * f23) + bone3.worldY;
            i5 = i3;
            f6 = Animation.CurveTimeline.LINEAR;
        } else {
            i5 = i3;
            f6 = bone4.ay;
            f7 = (f24 * f23) + (f25 * f6) + bone3.worldX;
            f8 = (f26 * f23) + (f27 * f6) + bone3.worldY;
        }
        Bone bone5 = bone3.parent;
        float f28 = bone5.f6593a;
        float f29 = bone5.f6594b;
        int i9 = i4;
        float f30 = bone5.f6595c;
        float f31 = f6;
        float f32 = bone5.f6596d;
        float f33 = 1.0f / ((f28 * f32) - (f29 * f30));
        float f34 = f23;
        float f35 = bone5.worldX;
        float f36 = f2 - f35;
        float f37 = bone5.worldY;
        float f38 = f3 - f37;
        float f39 = f21;
        float f40 = (((f36 * f32) - (f38 * f29)) * f33) - f18;
        float f41 = (((f38 * f28) - (f36 * f30)) * f33) - f19;
        float f42 = f41;
        float f43 = (f40 * f40) + (f41 * f41);
        float f44 = f7 - f35;
        float f45 = f8 - f37;
        float f46 = (((f32 * f44) - (f29 * f45)) * f33) - f18;
        float f47 = (((f45 * f28) - (f44 * f30)) * f33) - f19;
        float sqrt = (float) Math.sqrt((double) ((f46 * f46) + (f47 * f47)));
        float f48 = bone4.data.length * f22;
        if (z2) {
            float f49 = f48 * f5;
            float f50 = ((f43 - (sqrt * sqrt)) - (f49 * f49)) / ((2.0f * sqrt) * f49);
            if (f50 < -1.0f) {
                f50 = -1.0f;
            } else if (f50 > 1.0f) {
                if (z) {
                    float f51 = sqrt + f49;
                    if (f51 > 1.0E-4f) {
                        f20 *= (((((float) Math.sqrt((double) f43)) / f51) - 1.0f) * f4) + 1.0f;
                    }
                }
                f50 = 1.0f;
            }
            float f52 = f42;
            float acos = ((float) Math.acos((double) f50)) * ((float) i2);
            float f53 = sqrt + (f50 * f49);
            float sin = f49 * SpineUtils.sin(acos);
            f10 = acos;
            f9 = f31;
            f11 = f34;
            f12 = SpineUtils.atan2((f52 * f53) - (f40 * sin), (f40 * f53) + (f52 * sin));
        } else {
            int i10 = i2;
            float f54 = f5 * f48;
            float f55 = f48 * f39;
            float f56 = f54 * f54;
            float f57 = f55 * f55;
            float atan2 = SpineUtils.atan2(f42, f40);
            float f58 = (((f57 * sqrt) * sqrt) + (f56 * f43)) - (f56 * f57);
            float f59 = -2.0f * f57 * sqrt;
            float f60 = f57 - f56;
            float f61 = (f59 * f59) - ((4.0f * f60) * f58);
            float f62 = 0;
            if (f61 >= Animation.CurveTimeline.LINEAR) {
                float sqrt2 = (float) Math.sqrt((double) f61);
                if (f59 < Animation.CurveTimeline.LINEAR) {
                    sqrt2 = -sqrt2;
                }
                float f63 = (-(f59 + sqrt2)) / 2.0f;
                float f64 = f63 / f60;
                float f65 = f58 / f63;
                if (Math.abs(f64) >= Math.abs(f65)) {
                    f64 = f65;
                }
                float f66 = f64 * f64;
                if (f66 <= f43) {
                    float sqrt3 = ((float) Math.sqrt((double) (f43 - f66))) * ((float) i2);
                    f10 = SpineUtils.atan2(sqrt3 / f39, (f64 - sqrt) / f5);
                    f12 = atan2 - SpineUtils.atan2(sqrt3, f64);
                    f9 = f31;
                    f11 = f34;
                } else {
                    i6 = i2;
                }
            } else {
                i6 = i10;
            }
            float f67 = sqrt - f54;
            float f68 = f67 * f67;
            float f69 = sqrt + f54;
            float f70 = f69 * f69;
            float f71 = ((-f54) * sqrt) / (f56 - f57);
            if (f71 < -1.0f || f71 > 1.0f) {
                f13 = f67;
                f16 = Animation.CurveTimeline.LINEAR;
                f15 = Animation.CurveTimeline.LINEAR;
                f14 = 3.1415927f;
            } else {
                f16 = (float) Math.acos((double) f71);
                float cos = sqrt + (f54 * SpineUtils.cos(f16));
                float sin2 = f55 * SpineUtils.sin(f16);
                float f72 = (cos * cos) + (sin2 * sin2);
                if (f72 < f68) {
                    f13 = cos;
                    f15 = sin2;
                    f14 = f16;
                    f68 = f72;
                } else {
                    f13 = f67;
                    f15 = Animation.CurveTimeline.LINEAR;
                    f14 = 3.1415927f;
                }
                if (f72 > f70) {
                    f69 = cos;
                    f62 = sin2;
                    f70 = f72;
                } else {
                    f16 = Animation.CurveTimeline.LINEAR;
                }
            }
            if (f43 <= (f68 + f70) / 2.0f) {
                float f73 = (float) i6;
                f12 = atan2 - SpineUtils.atan2(f15 * f73, f13);
                f17 = f73 * f14;
            } else {
                float f74 = (float) i6;
                f12 = atan2 - SpineUtils.atan2(f62 * f74, f69);
                f17 = f74 * f16;
            }
            f10 = f17;
            f9 = f31;
            f11 = f34;
        }
        float f75 = (float) i9;
        float atan22 = SpineUtils.atan2(f9, f11) * f75;
        Bone bone6 = bone;
        float f76 = bone6.arotation;
        float f77 = (((f12 - atan22) * 57.295776f) + ((float) i5)) - f76;
        if (f77 > 180.0f) {
            f77 -= 360.0f;
        } else if (f77 < -180.0f) {
            f77 += 360.0f;
        }
        bone.updateWorldTransform(f18, f19, f76 + (f77 * f4), f20, bone6.ascaleY, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        float f78 = bone4.arotation;
        float f79 = (((((f10 + atan22) * 57.295776f) - bone4.ashearX) * f75) + ((float) i8)) - f78;
        if (f79 > 180.0f) {
            f79 -= 360.0f;
        } else if (f79 < -180.0f) {
            f79 += 360.0f;
        }
        bone2.updateWorldTransform(f11, f9, f78 + (f79 * f4), bone4.ascaleX, bone4.ascaleY, bone4.ashearX, bone4.ashearY);
    }

    public IkConstraint(IkConstraint ikConstraint, Skeleton skeleton) {
        if (ikConstraint == null) {
            throw new IllegalArgumentException("constraint cannot be null.");
        } else if (skeleton != null) {
            this.data = ikConstraint.data;
            this.bones = new a<>(ikConstraint.bones.f5374b);
            Iterator<Bone> it = ikConstraint.bones.iterator();
            while (it.hasNext()) {
                this.bones.add(skeleton.bones.get(it.next().data.index));
            }
            this.target = skeleton.bones.get(ikConstraint.target.data.index);
            this.mix = ikConstraint.mix;
            this.bendDirection = ikConstraint.bendDirection;
            this.compress = ikConstraint.compress;
            this.stretch = ikConstraint.stretch;
        } else {
            throw new IllegalArgumentException("skeleton cannot be null.");
        }
    }
}
