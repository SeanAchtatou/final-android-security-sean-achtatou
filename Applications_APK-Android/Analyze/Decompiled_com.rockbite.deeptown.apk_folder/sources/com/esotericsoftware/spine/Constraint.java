package com.esotericsoftware.spine;

public interface Constraint extends Updatable {
    int getOrder();
}
