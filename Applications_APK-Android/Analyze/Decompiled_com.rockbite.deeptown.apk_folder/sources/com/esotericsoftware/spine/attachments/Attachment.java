package com.esotericsoftware.spine.attachments;

public abstract class Attachment {
    String name;

    public Attachment(String str) {
        if (str != null) {
            this.name = str;
            return;
        }
        throw new IllegalArgumentException("name cannot be null.");
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return getName();
    }
}
