package com.esotericsoftware.spine.attachments;

import com.esotericsoftware.spine.Skin;

public interface AttachmentLoader {
    BoundingBoxAttachment newBoundingBoxAttachment(Skin skin, String str);

    ClippingAttachment newClippingAttachment(Skin skin, String str);

    MeshAttachment newMeshAttachment(Skin skin, String str, String str2);

    PathAttachment newPathAttachment(Skin skin, String str);

    PointAttachment newPointAttachment(Skin skin, String str);

    RegionAttachment newRegionAttachment(Skin skin, String str, String str2);
}
