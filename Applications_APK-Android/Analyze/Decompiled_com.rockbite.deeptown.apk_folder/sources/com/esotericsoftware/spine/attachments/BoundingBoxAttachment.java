package com.esotericsoftware.spine.attachments;

import com.esotericsoftware.spine.Animation;
import e.d.b.t.b;

public class BoundingBoxAttachment extends VertexAttachment {
    final b color = new b(0.38f, 0.94f, Animation.CurveTimeline.LINEAR, 1.0f);

    public BoundingBoxAttachment(String str) {
        super(str);
    }

    public b getColor() {
        return this.color;
    }
}
