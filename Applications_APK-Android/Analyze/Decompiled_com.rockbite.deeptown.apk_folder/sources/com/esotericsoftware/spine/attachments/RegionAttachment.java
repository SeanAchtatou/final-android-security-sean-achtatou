package com.esotericsoftware.spine.attachments;

import com.badlogic.gdx.graphics.g2d.q;
import com.badlogic.gdx.graphics.g2d.r;
import com.esotericsoftware.spine.Bone;
import e.d.b.t.b;

public class RegionAttachment extends Attachment {
    public static final int BLX = 0;
    public static final int BLY = 1;
    public static final int BRX = 6;
    public static final int BRY = 7;
    public static final int ULX = 2;
    public static final int ULY = 3;
    public static final int URX = 4;
    public static final int URY = 5;
    private final b color = new b(1.0f, 1.0f, 1.0f, 1.0f);
    private float height;
    private final float[] offset = new float[8];
    private String path;
    private r region;
    private float rotation;
    private float scaleX = 1.0f;
    private float scaleY = 1.0f;
    private final float[] uvs = new float[8];
    private float width;
    private float x;
    private float y;

    public RegionAttachment(String str) {
        super(str);
    }

    public void computeWorldVertices(Bone bone, float[] fArr, int i2, int i3) {
        float[] fArr2 = this.offset;
        float worldX = bone.getWorldX();
        float worldY = bone.getWorldY();
        float a2 = bone.getA();
        float b2 = bone.getB();
        float c2 = bone.getC();
        float d2 = bone.getD();
        float f2 = fArr2[6];
        float f3 = fArr2[7];
        fArr[i2] = (f2 * a2) + (f3 * b2) + worldX;
        fArr[i2 + 1] = (f2 * c2) + (f3 * d2) + worldY;
        int i4 = i2 + i3;
        float f4 = fArr2[0];
        float f5 = fArr2[1];
        fArr[i4] = (f4 * a2) + (f5 * b2) + worldX;
        fArr[i4 + 1] = (f4 * c2) + (f5 * d2) + worldY;
        int i5 = i4 + i3;
        float f6 = fArr2[2];
        float f7 = fArr2[3];
        fArr[i5] = (f6 * a2) + (f7 * b2) + worldX;
        fArr[i5 + 1] = (f6 * c2) + (f7 * d2) + worldY;
        int i6 = i5 + i3;
        float f8 = fArr2[4];
        float f9 = fArr2[5];
        fArr[i6] = (a2 * f8) + (b2 * f9) + worldX;
        fArr[i6 + 1] = (f8 * c2) + (f9 * d2) + worldY;
    }

    public b getColor() {
        return this.color;
    }

    public float getHeight() {
        return this.height;
    }

    public float[] getOffset() {
        return this.offset;
    }

    public String getPath() {
        return this.path;
    }

    public r getRegion() {
        r rVar = this.region;
        if (rVar != null) {
            return rVar;
        }
        throw new IllegalStateException("Region has not been set: " + this);
    }

    public float getRotation() {
        return this.rotation;
    }

    public float getScaleX() {
        return this.scaleX;
    }

    public float getScaleY() {
        return this.scaleY;
    }

    public float[] getUVs() {
        return this.uvs;
    }

    public float getWidth() {
        return this.width;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public void setHeight(float f2) {
        this.height = f2;
    }

    public void setPath(String str) {
        this.path = str;
    }

    public void setRegion(r rVar) {
        if (rVar != null) {
            this.region = rVar;
            float[] fArr = this.uvs;
            if (!(rVar instanceof q.b) || !((q.b) rVar).p) {
                fArr[2] = rVar.f();
                fArr[3] = rVar.i();
                fArr[4] = rVar.f();
                fArr[5] = rVar.h();
                fArr[6] = rVar.g();
                fArr[7] = rVar.h();
                fArr[0] = rVar.g();
                fArr[1] = rVar.i();
                return;
            }
            fArr[4] = rVar.f();
            fArr[5] = rVar.i();
            fArr[6] = rVar.f();
            fArr[7] = rVar.h();
            fArr[0] = rVar.g();
            fArr[1] = rVar.h();
            fArr[2] = rVar.g();
            fArr[3] = rVar.i();
            return;
        }
        throw new IllegalArgumentException("region cannot be null.");
    }

    public void setRotation(float f2) {
        this.rotation = f2;
    }

    public void setScaleX(float f2) {
        this.scaleX = f2;
    }

    public void setScaleY(float f2) {
        this.scaleY = f2;
    }

    public void setWidth(float f2) {
        this.width = f2;
    }

    public void setX(float f2) {
        this.x = f2;
    }

    public void setY(float f2) {
        this.y = f2;
    }

    public void updateOffset() {
        int i2;
        float f2;
        float width2 = getWidth();
        float height2 = getHeight();
        float f3 = width2 / 2.0f;
        float f4 = height2 / 2.0f;
        float f5 = -f3;
        float f6 = -f4;
        r rVar = this.region;
        if (rVar instanceof q.b) {
            q.b bVar = (q.b) rVar;
            float f7 = bVar.f5037j;
            int i3 = bVar.n;
            f5 += (f7 / ((float) i3)) * width2;
            float f8 = bVar.f5038k;
            int i4 = bVar.o;
            f6 += (f8 / ((float) i4)) * height2;
            if (bVar.p) {
                f3 -= (((((float) i3) - f7) - ((float) bVar.m)) / ((float) i3)) * width2;
                f2 = ((float) i4) - f8;
                i2 = bVar.l;
            } else {
                f3 -= (((((float) i3) - f7) - ((float) bVar.l)) / ((float) i3)) * width2;
                f2 = ((float) i4) - f8;
                i2 = bVar.m;
            }
            f4 -= ((f2 - ((float) i2)) / ((float) i4)) * height2;
        }
        float scaleX2 = getScaleX();
        float scaleY2 = getScaleY();
        float f9 = f5 * scaleX2;
        float f10 = f6 * scaleY2;
        float f11 = f3 * scaleX2;
        float f12 = f4 * scaleY2;
        double rotation2 = (double) (getRotation() * 0.017453292f);
        float cos = (float) Math.cos(rotation2);
        float sin = (float) Math.sin(rotation2);
        float x2 = getX();
        float y2 = getY();
        float f13 = (f9 * cos) + x2;
        float f14 = f9 * sin;
        float f15 = (f10 * cos) + y2;
        float f16 = f10 * sin;
        float f17 = (f11 * cos) + x2;
        float f18 = f11 * sin;
        float f19 = (cos * f12) + y2;
        float f20 = f12 * sin;
        float[] fArr = this.offset;
        fArr[0] = f13 - f16;
        fArr[1] = f15 + f14;
        fArr[2] = f13 - f20;
        fArr[3] = f14 + f19;
        fArr[4] = f17 - f20;
        fArr[5] = f19 + f18;
        fArr[6] = f17 - f16;
        fArr[7] = f15 + f18;
    }
}
