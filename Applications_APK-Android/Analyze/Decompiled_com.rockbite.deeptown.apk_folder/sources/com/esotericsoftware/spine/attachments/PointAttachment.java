package com.esotericsoftware.spine.attachments;

import com.badlogic.gdx.math.h;
import com.badlogic.gdx.math.p;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.Bone;
import e.d.b.t.b;

public class PointAttachment extends Attachment {
    final b color = new b(0.9451f, 0.9451f, Animation.CurveTimeline.LINEAR, 1.0f);
    float rotation;
    float x;
    float y;

    public PointAttachment(String str) {
        super(str);
    }

    public p computeWorldPosition(Bone bone, p pVar) {
        pVar.f5294a = (this.x * bone.getA()) + (this.y * bone.getB()) + bone.getWorldX();
        pVar.f5295b = (this.x * bone.getC()) + (this.y * bone.getD()) + bone.getWorldY();
        return pVar;
    }

    public float computeWorldRotation(Bone bone) {
        float b2 = h.b(this.rotation);
        float h2 = h.h(this.rotation);
        return ((float) Math.atan2((double) ((b2 * bone.getC()) + (h2 * bone.getD())), (double) ((bone.getA() * b2) + (bone.getB() * h2)))) * 57.295776f;
    }

    public b getColor() {
        return this.color;
    }

    public float getRotation() {
        return this.rotation;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public void setRotation(float f2) {
        this.rotation = f2;
    }

    public void setX(float f2) {
        this.x = f2;
    }

    public void setY(float f2) {
        this.y = f2;
    }
}
