package com.esotericsoftware.spine.attachments;

import com.badlogic.gdx.graphics.g2d.q;
import com.badlogic.gdx.graphics.g2d.r;
import com.esotericsoftware.spine.Animation;
import e.d.b.t.b;

public class MeshAttachment extends VertexAttachment {
    private final b color = new b(1.0f, 1.0f, 1.0f, 1.0f);
    private short[] edges;
    private float height;
    private int hullLength;
    private boolean inheritDeform;
    private MeshAttachment parentMesh;
    private String path;
    private r region;
    private float[] regionUVs;
    private short[] triangles;
    private float[] uvs;
    private float width;

    public MeshAttachment(String str) {
        super(str);
    }

    public boolean applyDeform(VertexAttachment vertexAttachment) {
        return this == vertexAttachment || (this.inheritDeform && this.parentMesh == vertexAttachment);
    }

    public b getColor() {
        return this.color;
    }

    public short[] getEdges() {
        return this.edges;
    }

    public float getHeight() {
        return this.height;
    }

    public int getHullLength() {
        return this.hullLength;
    }

    public boolean getInheritDeform() {
        return this.inheritDeform;
    }

    public MeshAttachment getParentMesh() {
        return this.parentMesh;
    }

    public String getPath() {
        return this.path;
    }

    public r getRegion() {
        r rVar = this.region;
        if (rVar != null) {
            return rVar;
        }
        throw new IllegalStateException("Region has not been set: " + this);
    }

    public float[] getRegionUVs() {
        return this.regionUVs;
    }

    public short[] getTriangles() {
        return this.triangles;
    }

    public float[] getUVs() {
        return this.uvs;
    }

    public float getWidth() {
        return this.width;
    }

    public void setEdges(short[] sArr) {
        this.edges = sArr;
    }

    public void setHeight(float f2) {
        this.height = f2;
    }

    public void setHullLength(int i2) {
        this.hullLength = i2;
    }

    public void setInheritDeform(boolean z) {
        this.inheritDeform = z;
    }

    public void setParentMesh(MeshAttachment meshAttachment) {
        this.parentMesh = meshAttachment;
        if (meshAttachment != null) {
            this.bones = meshAttachment.bones;
            this.vertices = meshAttachment.vertices;
            this.regionUVs = meshAttachment.regionUVs;
            this.triangles = meshAttachment.triangles;
            this.hullLength = meshAttachment.hullLength;
            this.worldVerticesLength = meshAttachment.worldVerticesLength;
            this.edges = meshAttachment.edges;
            this.width = meshAttachment.width;
            this.height = meshAttachment.height;
        }
    }

    public void setPath(String str) {
        this.path = str;
    }

    public void setRegion(r rVar) {
        if (rVar != null) {
            this.region = rVar;
            return;
        }
        throw new IllegalArgumentException("region cannot be null.");
    }

    public void setRegionUVs(float[] fArr) {
        this.regionUVs = fArr;
    }

    public void setTriangles(short[] sArr) {
        this.triangles = sArr;
    }

    public void setUVs(float[] fArr) {
        this.uvs = fArr;
    }

    public void setWidth(float f2) {
        this.width = f2;
    }

    public void updateUVs() {
        float f2;
        float f3;
        float f4;
        float f5;
        float[] fArr = this.regionUVs;
        float[] fArr2 = this.uvs;
        if (fArr2 == null || fArr2.length != fArr.length) {
            this.uvs = new float[fArr.length];
        }
        float[] fArr3 = this.uvs;
        r rVar = this.region;
        int i2 = 0;
        if (rVar instanceof q.b) {
            q.b bVar = (q.b) rVar;
            float r = (float) bVar.e().r();
            float p = (float) bVar.e().p();
            if (bVar.p) {
                float f6 = bVar.f() - (((((float) bVar.o) - bVar.f5038k) - ((float) bVar.l)) / r);
                float h2 = bVar.h();
                int i3 = bVar.n;
                float f7 = h2 - (((((float) i3) - bVar.f5037j) - ((float) bVar.m)) / p);
                float f8 = ((float) bVar.o) / r;
                float f9 = ((float) i3) / p;
                int length = fArr3.length;
                while (i2 < length) {
                    int i4 = i2 + 1;
                    fArr3[i2] = (fArr[i4] * f8) + f6;
                    fArr3[i4] = (f7 + f9) - (fArr[i2] * f9);
                    i2 += 2;
                }
                return;
            }
            f3 = bVar.f() - (bVar.f5037j / r);
            float h3 = bVar.h();
            int i5 = bVar.o;
            f2 = h3 - (((((float) i5) - bVar.f5038k) - ((float) bVar.m)) / p);
            f5 = ((float) bVar.n) / r;
            f4 = ((float) i5) / p;
        } else if (rVar == null) {
            f3 = Animation.CurveTimeline.LINEAR;
            f5 = 1.0f;
            f4 = 1.0f;
            f2 = Animation.CurveTimeline.LINEAR;
        } else {
            f3 = rVar.f();
            f2 = this.region.h();
            f5 = this.region.g() - f3;
            f4 = this.region.i() - f2;
        }
        int length2 = fArr3.length;
        while (i2 < length2) {
            fArr3[i2] = (fArr[i2] * f5) + f3;
            int i6 = i2 + 1;
            fArr3[i6] = (fArr[i6] * f4) + f2;
            i2 += 2;
        }
    }
}
