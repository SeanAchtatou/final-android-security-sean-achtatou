package com.esotericsoftware.spine.attachments;

import com.esotericsoftware.spine.Animation;
import e.d.b.t.b;

public class PathAttachment extends VertexAttachment {
    boolean closed;
    final b color = new b(1.0f, 0.5f, Animation.CurveTimeline.LINEAR, 1.0f);
    boolean constantSpeed;
    float[] lengths;

    public PathAttachment(String str) {
        super(str);
    }

    public boolean getClosed() {
        return this.closed;
    }

    public b getColor() {
        return this.color;
    }

    public boolean getConstantSpeed() {
        return this.constantSpeed;
    }

    public float[] getLengths() {
        return this.lengths;
    }

    public void setClosed(boolean z) {
        this.closed = z;
    }

    public void setConstantSpeed(boolean z) {
        this.constantSpeed = z;
    }

    public void setLengths(float[] fArr) {
        this.lengths = fArr;
    }
}
