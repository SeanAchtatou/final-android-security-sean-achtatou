package com.esotericsoftware.spine.attachments;

public enum AttachmentType {
    region,
    boundingbox,
    mesh,
    linkedmesh,
    path,
    point,
    clipping;
    
    public static AttachmentType[] values = values();
}
