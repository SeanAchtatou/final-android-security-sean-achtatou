package com.esotericsoftware.spine.attachments;

import com.badlogic.gdx.utils.m;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.Slot;

public class VertexAttachment extends Attachment {
    private static int nextID;
    int[] bones;
    private final int id = ((nextID() & 65535) << 11);
    float[] vertices;
    int worldVerticesLength;

    public VertexAttachment(String str) {
        super(str);
    }

    private static synchronized int nextID() {
        int i2;
        synchronized (VertexAttachment.class) {
            i2 = nextID;
            nextID = i2 + 1;
        }
        return i2;
    }

    public boolean applyDeform(VertexAttachment vertexAttachment) {
        return this == vertexAttachment;
    }

    public void computeWorldVertices(Slot slot, int i2, int i3, float[] fArr, int i4, int i5) {
        int i6 = i4 + ((i3 >> 1) * i5);
        Skeleton skeleton = slot.getSkeleton();
        m attachmentVertices = slot.getAttachmentVertices();
        float[] fArr2 = this.vertices;
        int[] iArr = this.bones;
        if (iArr == null) {
            if (attachmentVertices.f5528b > 0) {
                fArr2 = attachmentVertices.f5527a;
            }
            Bone bone = slot.getBone();
            float worldX = bone.getWorldX();
            float worldY = bone.getWorldY();
            float a2 = bone.getA();
            float b2 = bone.getB();
            float c2 = bone.getC();
            float d2 = bone.getD();
            int i7 = i2;
            int i8 = i4;
            while (i8 < i6) {
                float f2 = fArr2[i7];
                float f3 = fArr2[i7 + 1];
                fArr[i8] = (f2 * a2) + (f3 * b2) + worldX;
                fArr[i8 + 1] = (f2 * c2) + (f3 * d2) + worldY;
                i7 += 2;
                i8 += i5;
            }
            return;
        }
        int i9 = i2;
        int i10 = 0;
        int i11 = 0;
        for (int i12 = 0; i12 < i9; i12 += 2) {
            int i13 = iArr[i10];
            i10 += i13 + 1;
            i11 += i13;
        }
        T[] tArr = skeleton.getBones().f5373a;
        if (attachmentVertices.f5528b == 0) {
            int i14 = i11 * 3;
            int i15 = i4;
            while (i15 < i6) {
                int i16 = i10 + 1;
                int i17 = iArr[i10] + i16;
                int i18 = i14;
                float f4 = Animation.CurveTimeline.LINEAR;
                float f5 = Animation.CurveTimeline.LINEAR;
                while (i16 < i17) {
                    Bone bone2 = (Bone) tArr[iArr[i16]];
                    float f6 = fArr2[i18];
                    float f7 = fArr2[i18 + 1];
                    float f8 = fArr2[i18 + 2];
                    f4 += ((bone2.getA() * f6) + (bone2.getB() * f7) + bone2.getWorldX()) * f8;
                    f5 += ((f6 * bone2.getC()) + (f7 * bone2.getD()) + bone2.getWorldY()) * f8;
                    i16++;
                    i18 += 3;
                }
                fArr[i15] = f4;
                fArr[i15 + 1] = f5;
                i15 += i5;
                i10 = i16;
                i14 = i18;
            }
            return;
        }
        float[] fArr3 = attachmentVertices.f5527a;
        int i19 = i11 << 1;
        int i20 = i11 * 3;
        int i21 = i4;
        while (i21 < i6) {
            int i22 = i10 + 1;
            int i23 = iArr[i10] + i22;
            int i24 = i20;
            int i25 = i19;
            float f9 = Animation.CurveTimeline.LINEAR;
            float f10 = Animation.CurveTimeline.LINEAR;
            while (i22 < i23) {
                Bone bone3 = (Bone) tArr[iArr[i22]];
                float f11 = fArr2[i24] + fArr3[i25];
                float f12 = fArr2[i24 + 1] + fArr3[i25 + 1];
                float f13 = fArr2[i24 + 2];
                f9 += ((bone3.getA() * f11) + (bone3.getB() * f12) + bone3.getWorldX()) * f13;
                f10 += ((f11 * bone3.getC()) + (f12 * bone3.getD()) + bone3.getWorldY()) * f13;
                i22++;
                i24 += 3;
                i25 += 2;
            }
            fArr[i21] = f9;
            fArr[i21 + 1] = f10;
            i21 += i5;
            i10 = i22;
            i20 = i24;
            i19 = i25;
        }
    }

    public int[] getBones() {
        return this.bones;
    }

    public int getId() {
        return this.id;
    }

    public float[] getVertices() {
        return this.vertices;
    }

    public int getWorldVerticesLength() {
        return this.worldVerticesLength;
    }

    public void setBones(int[] iArr) {
        this.bones = iArr;
    }

    public void setVertices(float[] fArr) {
        this.vertices = fArr;
    }

    public void setWorldVerticesLength(int i2) {
        this.worldVerticesLength = i2;
    }
}
