package com.esotericsoftware.spine.attachments;

import com.esotericsoftware.spine.Skeleton;

public class SkeletonAttachment extends Attachment {
    private Skeleton skeleton;

    public SkeletonAttachment(String str) {
        super(str);
    }

    public Skeleton getSkeleton() {
        return this.skeleton;
    }

    public void setSkeleton(Skeleton skeleton2) {
        this.skeleton = skeleton2;
    }
}
