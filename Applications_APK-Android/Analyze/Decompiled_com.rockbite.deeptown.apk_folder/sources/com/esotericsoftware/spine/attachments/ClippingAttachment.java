package com.esotericsoftware.spine.attachments;

import com.esotericsoftware.spine.SlotData;
import e.d.b.t.b;

public class ClippingAttachment extends VertexAttachment {
    final b color = new b(0.2275f, 0.2275f, 0.8078f, 1.0f);
    SlotData endSlot;

    public ClippingAttachment(String str) {
        super(str);
    }

    public b getColor() {
        return this.color;
    }

    public SlotData getEndSlot() {
        return this.endSlot;
    }

    public void setEndSlot(SlotData slotData) {
        this.endSlot = slotData;
    }
}
