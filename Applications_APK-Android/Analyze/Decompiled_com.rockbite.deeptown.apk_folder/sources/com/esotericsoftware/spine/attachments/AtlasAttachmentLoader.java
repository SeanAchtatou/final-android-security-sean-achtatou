package com.esotericsoftware.spine.attachments;

import com.badlogic.gdx.graphics.g2d.q;
import com.esotericsoftware.spine.Skin;

public class AtlasAttachmentLoader implements AttachmentLoader {
    private q atlas;

    public AtlasAttachmentLoader(q qVar) {
        if (qVar != null) {
            this.atlas = qVar;
            return;
        }
        throw new IllegalArgumentException("atlas cannot be null.");
    }

    public BoundingBoxAttachment newBoundingBoxAttachment(Skin skin, String str) {
        return new BoundingBoxAttachment(str);
    }

    public ClippingAttachment newClippingAttachment(Skin skin, String str) {
        return new ClippingAttachment(str);
    }

    public MeshAttachment newMeshAttachment(Skin skin, String str, String str2) {
        q.b b2 = this.atlas.b(str2);
        if (b2 != null) {
            MeshAttachment meshAttachment = new MeshAttachment(str);
            meshAttachment.setRegion(b2);
            return meshAttachment;
        }
        throw new RuntimeException("Region not found in atlas: " + str2 + " (mesh attachment: " + str + ")");
    }

    public PathAttachment newPathAttachment(Skin skin, String str) {
        return new PathAttachment(str);
    }

    public PointAttachment newPointAttachment(Skin skin, String str) {
        return new PointAttachment(str);
    }

    public RegionAttachment newRegionAttachment(Skin skin, String str, String str2) {
        q.b b2 = this.atlas.b(str2);
        if (b2 != null) {
            RegionAttachment regionAttachment = new RegionAttachment(str);
            regionAttachment.setRegion(b2);
            return regionAttachment;
        }
        throw new RuntimeException("Region not found in atlas: " + str2 + " (region attachment: " + str + ")");
    }
}
