package com.esotericsoftware.spine;

import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.f0;
import com.badlogic.gdx.utils.m;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.BoundingBoxAttachment;

public class SkeletonBounds {
    private a<BoundingBoxAttachment> boundingBoxes = new a<>();
    private float maxX;
    private float maxY;
    private float minX;
    private float minY;
    private f0<m> polygonPool = new f0() {
        /* access modifiers changed from: protected */
        public Object newObject() {
            return new m();
        }
    };
    private a<m> polygons = new a<>();

    private void aabbCompute() {
        a<m> aVar = this.polygons;
        int i2 = aVar.f5374b;
        float f2 = 2.14748365E9f;
        int i3 = 0;
        float f3 = 2.14748365E9f;
        float f4 = -2.14748365E9f;
        float f5 = -2.14748365E9f;
        while (i3 < i2) {
            m mVar = aVar.get(i3);
            float[] fArr = mVar.f5527a;
            int i4 = mVar.f5528b;
            float f6 = f2;
            for (int i5 = 0; i5 < i4; i5 += 2) {
                float f7 = fArr[i5];
                float f8 = fArr[i5 + 1];
                f6 = Math.min(f6, f7);
                f3 = Math.min(f3, f8);
                f4 = Math.max(f4, f7);
                f5 = Math.max(f5, f8);
            }
            i3++;
            f2 = f6;
        }
        this.minX = f2;
        this.minY = f3;
        this.maxX = f4;
        this.maxY = f5;
    }

    public boolean aabbContainsPoint(float f2, float f3) {
        return f2 >= this.minX && f2 <= this.maxX && f3 >= this.minY && f3 <= this.maxY;
    }

    public boolean aabbIntersectsSegment(float f2, float f3, float f4, float f5) {
        float f6 = this.minX;
        float f7 = this.minY;
        float f8 = this.maxX;
        float f9 = this.maxY;
        if ((f2 <= f6 && f4 <= f6) || ((f3 <= f7 && f5 <= f7) || ((f2 >= f8 && f4 >= f8) || (f3 >= f9 && f5 >= f9)))) {
            return false;
        }
        float f10 = (f5 - f3) / (f4 - f2);
        float f11 = ((f6 - f2) * f10) + f3;
        if (f11 > f7 && f11 < f9) {
            return true;
        }
        float f12 = ((f8 - f2) * f10) + f3;
        if (f12 > f7 && f12 < f9) {
            return true;
        }
        float f13 = ((f7 - f3) / f10) + f2;
        if (f13 > f6 && f13 < f8) {
            return true;
        }
        float f14 = ((f9 - f3) / f10) + f2;
        return f14 > f6 && f14 < f8;
    }

    public boolean aabbIntersectsSkeleton(SkeletonBounds skeletonBounds) {
        return this.minX < skeletonBounds.maxX && this.maxX > skeletonBounds.minX && this.minY < skeletonBounds.maxY && this.maxY > skeletonBounds.minY;
    }

    public BoundingBoxAttachment containsPoint(float f2, float f3) {
        a<m> aVar = this.polygons;
        int i2 = aVar.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            if (containsPoint(aVar.get(i3), f2, f3)) {
                return this.boundingBoxes.get(i3);
            }
        }
        return null;
    }

    public a<BoundingBoxAttachment> getBoundingBoxes() {
        return this.boundingBoxes;
    }

    public float getHeight() {
        return this.maxY - this.minY;
    }

    public float getMaxX() {
        return this.maxX;
    }

    public float getMaxY() {
        return this.maxY;
    }

    public float getMinX() {
        return this.minX;
    }

    public float getMinY() {
        return this.minY;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.a.b(java.lang.Object, boolean):int
     arg types: [com.esotericsoftware.spine.attachments.BoundingBoxAttachment, int]
     candidates:
      com.badlogic.gdx.utils.a.b(int, int):void
      com.badlogic.gdx.utils.a.b(java.lang.Object, boolean):int */
    public m getPolygon(BoundingBoxAttachment boundingBoxAttachment) {
        if (boundingBoxAttachment != null) {
            int b2 = this.boundingBoxes.b((Object) boundingBoxAttachment, true);
            if (b2 == -1) {
                return null;
            }
            return this.polygons.get(b2);
        }
        throw new IllegalArgumentException("boundingBox cannot be null.");
    }

    public a<m> getPolygons() {
        return this.polygons;
    }

    public float getWidth() {
        return this.maxX - this.minX;
    }

    public BoundingBoxAttachment intersectsSegment(float f2, float f3, float f4, float f5) {
        a<m> aVar = this.polygons;
        int i2 = aVar.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            if (intersectsSegment(aVar.get(i3), f2, f3, f4, f5)) {
                return this.boundingBoxes.get(i3);
            }
        }
        return null;
    }

    public void update(Skeleton skeleton, boolean z) {
        if (skeleton != null) {
            a<BoundingBoxAttachment> aVar = this.boundingBoxes;
            a<m> aVar2 = this.polygons;
            a<Slot> aVar3 = skeleton.slots;
            int i2 = aVar3.f5374b;
            aVar.clear();
            this.polygonPool.freeAll(aVar2);
            aVar2.clear();
            for (int i3 = 0; i3 < i2; i3++) {
                Slot slot = aVar3.get(i3);
                Attachment attachment = slot.attachment;
                if (attachment instanceof BoundingBoxAttachment) {
                    BoundingBoxAttachment boundingBoxAttachment = (BoundingBoxAttachment) attachment;
                    aVar.add(boundingBoxAttachment);
                    m obtain = this.polygonPool.obtain();
                    aVar2.add(obtain);
                    boundingBoxAttachment.computeWorldVertices(slot, 0, boundingBoxAttachment.getWorldVerticesLength(), obtain.d(boundingBoxAttachment.getWorldVerticesLength()), 0, 2);
                }
            }
            if (z) {
                aabbCompute();
                return;
            }
            this.minX = -2.14748365E9f;
            this.minY = -2.14748365E9f;
            this.maxX = 2.14748365E9f;
            this.maxY = 2.14748365E9f;
            return;
        }
        throw new IllegalArgumentException("skeleton cannot be null.");
    }

    public boolean containsPoint(m mVar, float f2, float f3) {
        float[] fArr = mVar.f5527a;
        int i2 = mVar.f5528b;
        boolean z = false;
        int i3 = i2 - 2;
        for (int i4 = 0; i4 < i2; i4 += 2) {
            float f4 = fArr[i4 + 1];
            float f5 = fArr[i3 + 1];
            if ((f4 < f3 && f5 >= f3) || (f5 < f3 && f4 >= f3)) {
                float f6 = fArr[i4];
                if (f6 + (((f3 - f4) / (f5 - f4)) * (fArr[i3] - f6)) < f2) {
                    z = !z;
                }
            }
            i3 = i4;
        }
        return z;
    }

    public boolean intersectsSegment(m mVar, float f2, float f3, float f4, float f5) {
        m mVar2 = mVar;
        float[] fArr = mVar2.f5527a;
        int i2 = mVar2.f5528b;
        float f6 = f2 - f4;
        float f7 = f3 - f5;
        float f8 = (f2 * f5) - (f3 * f4);
        float f9 = fArr[i2 - 2];
        float f10 = fArr[i2 - 1];
        float f11 = f9;
        int i3 = 0;
        while (i3 < i2) {
            float f12 = fArr[i3];
            float f13 = fArr[i3 + 1];
            float f14 = (f11 * f13) - (f10 * f12);
            float f15 = f11 - f12;
            float f16 = f10 - f13;
            float f17 = (f6 * f16) - (f7 * f15);
            float f18 = ((f15 * f8) - (f6 * f14)) / f17;
            if (((f18 >= f11 && f18 <= f12) || (f18 >= f12 && f18 <= f11)) && ((f18 >= f2 && f18 <= f4) || (f18 >= f4 && f18 <= f2))) {
                float f19 = ((f16 * f8) - (f14 * f7)) / f17;
                if ((f19 >= f10 && f19 <= f13) || (f19 >= f13 && f19 <= f10)) {
                    if (f19 >= f3 && f19 <= f5) {
                        return true;
                    }
                    if (f19 >= f5 && f19 <= f3) {
                        return true;
                    }
                }
            }
            i3 += 2;
            f11 = f12;
            f10 = f13;
        }
        return false;
    }
}
