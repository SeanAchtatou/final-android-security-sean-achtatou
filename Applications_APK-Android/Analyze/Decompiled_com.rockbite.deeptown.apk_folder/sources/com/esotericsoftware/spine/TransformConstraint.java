package com.esotericsoftware.spine;

import com.badlogic.gdx.math.p;
import com.badlogic.gdx.utils.a;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.utils.SpineUtils;
import java.util.Iterator;

public class TransformConstraint implements Constraint {
    final a<Bone> bones;
    final TransformConstraintData data;
    float rotateMix;
    float scaleMix;
    float shearMix;
    Bone target;
    final p temp = new p();
    float translateMix;

    public TransformConstraint(TransformConstraintData transformConstraintData, Skeleton skeleton) {
        if (transformConstraintData == null) {
            throw new IllegalArgumentException("data cannot be null.");
        } else if (skeleton != null) {
            this.data = transformConstraintData;
            this.rotateMix = transformConstraintData.rotateMix;
            this.translateMix = transformConstraintData.translateMix;
            this.scaleMix = transformConstraintData.scaleMix;
            this.shearMix = transformConstraintData.shearMix;
            this.bones = new a<>(transformConstraintData.bones.f5374b);
            Iterator<BoneData> it = transformConstraintData.bones.iterator();
            while (it.hasNext()) {
                this.bones.add(skeleton.findBone(it.next().name));
            }
            this.target = skeleton.findBone(transformConstraintData.target.name);
        } else {
            throw new IllegalArgumentException("skeleton cannot be null.");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00d2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void applyAbsoluteLocal() {
        /*
            r21 = this;
            r0 = r21
            float r1 = r0.rotateMix
            float r2 = r0.translateMix
            float r3 = r0.scaleMix
            float r4 = r0.shearMix
            com.esotericsoftware.spine.Bone r5 = r0.target
            boolean r6 = r5.appliedValid
            if (r6 != 0) goto L_0x0013
            r5.updateAppliedTransform()
        L_0x0013:
            com.badlogic.gdx.utils.a<com.esotericsoftware.spine.Bone> r6 = r0.bones
            r7 = 0
            int r8 = r6.f5374b
        L_0x0018:
            if (r7 >= r8) goto L_0x00e9
            java.lang.Object r9 = r6.get(r7)
            r10 = r9
            com.esotericsoftware.spine.Bone r10 = (com.esotericsoftware.spine.Bone) r10
            boolean r9 = r10.appliedValid
            if (r9 != 0) goto L_0x0028
            r10.updateAppliedTransform()
        L_0x0028:
            float r9 = r10.arotation
            r11 = 1135869952(0x43b40000, float:360.0)
            r12 = 4670232951022157823(0x40d0001fffffffff, double:16384.499999999996)
            r14 = 0
            int r15 = (r1 > r14 ? 1 : (r1 == r14 ? 0 : -1))
            if (r15 == 0) goto L_0x0055
            float r15 = r5.arotation
            float r15 = r15 - r9
            com.esotericsoftware.spine.TransformConstraintData r14 = r0.data
            float r14 = r14.offsetRotation
            float r15 = r15 + r14
            float r14 = r15 / r11
            r18 = r6
            r19 = r7
            double r6 = (double) r14
            java.lang.Double.isNaN(r6)
            double r6 = r12 - r6
            int r6 = (int) r6
            int r6 = 16384 - r6
            int r6 = r6 * 360
            float r6 = (float) r6
            float r15 = r15 - r6
            float r15 = r15 * r1
            float r9 = r9 + r15
            goto L_0x0059
        L_0x0055:
            r18 = r6
            r19 = r7
        L_0x0059:
            float r6 = r10.ax
            float r7 = r10.ay
            r14 = 0
            int r15 = (r2 > r14 ? 1 : (r2 == r14 ? 0 : -1))
            if (r15 == 0) goto L_0x0076
            float r14 = r5.ax
            float r14 = r14 - r6
            com.esotericsoftware.spine.TransformConstraintData r15 = r0.data
            float r12 = r15.offsetX
            float r14 = r14 + r12
            float r14 = r14 * r2
            float r6 = r6 + r14
            float r12 = r5.ay
            float r12 = r12 - r7
            float r13 = r15.offsetY
            float r12 = r12 + r13
            float r12 = r12 * r2
            float r7 = r7 + r12
        L_0x0076:
            r12 = r7
            float r7 = r10.ascaleX
            float r13 = r10.ascaleY
            r14 = 0
            int r15 = (r3 > r14 ? 1 : (r3 == r14 ? 0 : -1))
            if (r15 == 0) goto L_0x00a5
            int r15 = (r7 > r14 ? 1 : (r7 == r14 ? 0 : -1))
            if (r15 == 0) goto L_0x0091
            float r14 = r5.ascaleX
            float r14 = r14 - r7
            com.esotericsoftware.spine.TransformConstraintData r15 = r0.data
            float r15 = r15.offsetScaleX
            float r14 = r14 + r15
            float r14 = r14 * r3
            float r14 = r14 + r7
            float r7 = r14 / r7
        L_0x0091:
            r14 = 0
            int r15 = (r13 > r14 ? 1 : (r13 == r14 ? 0 : -1))
            if (r15 == 0) goto L_0x00a5
            float r14 = r5.ascaleY
            float r14 = r14 - r13
            com.esotericsoftware.spine.TransformConstraintData r15 = r0.data
            float r15 = r15.offsetScaleY
            float r14 = r14 + r15
            float r14 = r14 * r3
            float r14 = r14 + r13
            float r14 = r14 / r13
            r15 = r14
            r14 = r7
            goto L_0x00a7
        L_0x00a5:
            r14 = r7
            r15 = r13
        L_0x00a7:
            float r7 = r10.ashearY
            r13 = 0
            int r13 = (r4 > r13 ? 1 : (r4 == r13 ? 0 : -1))
            if (r13 == 0) goto L_0x00d2
            float r13 = r5.ashearY
            float r13 = r13 - r7
            com.esotericsoftware.spine.TransformConstraintData r11 = r0.data
            float r11 = r11.offsetShearY
            float r13 = r13 + r11
            r11 = 1135869952(0x43b40000, float:360.0)
            float r11 = r13 / r11
            r20 = r1
            double r0 = (double) r11
            java.lang.Double.isNaN(r0)
            r16 = 4670232951022157823(0x40d0001fffffffff, double:16384.499999999996)
            double r0 = r16 - r0
            int r0 = (int) r0
            int r0 = 16384 - r0
            int r0 = r0 * 360
            float r0 = (float) r0
            float r13 = r13 - r0
            float r13 = r13 * r4
            float r7 = r7 + r13
            goto L_0x00d4
        L_0x00d2:
            r20 = r1
        L_0x00d4:
            r17 = r7
            float r0 = r10.ashearX
            r11 = r6
            r13 = r9
            r16 = r0
            r10.updateWorldTransform(r11, r12, r13, r14, r15, r16, r17)
            int r7 = r19 + 1
            r0 = r21
            r6 = r18
            r1 = r20
            goto L_0x0018
        L_0x00e9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.spine.TransformConstraint.applyAbsoluteLocal():void");
    }

    private void applyAbsoluteWorld() {
        float f2;
        float f3;
        int i2;
        int i3;
        a<Bone> aVar;
        boolean z;
        TransformConstraint transformConstraint = this;
        float f4 = transformConstraint.rotateMix;
        float f5 = transformConstraint.translateMix;
        float f6 = transformConstraint.scaleMix;
        float f7 = transformConstraint.shearMix;
        Bone bone = transformConstraint.target;
        float f8 = bone.f6593a;
        float f9 = bone.f6594b;
        float f10 = bone.f6595c;
        float f11 = bone.f6596d;
        float f12 = (f8 * f11) - (f9 * f10) > Animation.CurveTimeline.LINEAR ? 0.017453292f : -0.017453292f;
        TransformConstraintData transformConstraintData = transformConstraint.data;
        float f13 = transformConstraintData.offsetRotation * f12;
        float f14 = transformConstraintData.offsetShearY * f12;
        a<Bone> aVar2 = transformConstraint.bones;
        int i4 = aVar2.f5374b;
        int i5 = 0;
        while (i5 < i4) {
            Bone bone2 = aVar2.get(i5);
            if (f4 != Animation.CurveTimeline.LINEAR) {
                aVar = aVar2;
                float f15 = bone2.f6593a;
                i3 = i4;
                float f16 = bone2.f6594b;
                i2 = i5;
                float f17 = bone2.f6595c;
                f3 = f14;
                float f18 = bone2.f6596d;
                float atan2 = (SpineUtils.atan2(f10, f8) - SpineUtils.atan2(f17, f15)) + f13;
                if (atan2 > 3.1415927f) {
                    atan2 -= 6.2831855f;
                } else if (atan2 < -3.1415927f) {
                    atan2 += 6.2831855f;
                }
                float f19 = atan2 * f4;
                float cos = SpineUtils.cos(f19);
                float sin = SpineUtils.sin(f19);
                f2 = f4;
                bone2.f6593a = (cos * f15) - (sin * f17);
                bone2.f6594b = (cos * f16) - (sin * f18);
                bone2.f6595c = (f15 * sin) + (f17 * cos);
                bone2.f6596d = (sin * f16) + (cos * f18);
                z = true;
            } else {
                f2 = f4;
                aVar = aVar2;
                f3 = f14;
                i3 = i4;
                i2 = i5;
                z = false;
            }
            if (f5 != Animation.CurveTimeline.LINEAR) {
                p pVar = transformConstraint.temp;
                TransformConstraintData transformConstraintData2 = transformConstraint.data;
                pVar.d(transformConstraintData2.offsetX, transformConstraintData2.offsetY);
                bone.localToWorld(pVar);
                float f20 = bone2.worldX;
                bone2.worldX = f20 + ((pVar.f5294a - f20) * f5);
                float f21 = bone2.worldY;
                bone2.worldY = f21 + ((pVar.f5295b - f21) * f5);
                z = true;
            }
            float f22 = Animation.CurveTimeline.LINEAR;
            if (f6 > Animation.CurveTimeline.LINEAR) {
                float f23 = bone2.f6593a;
                float f24 = bone2.f6595c;
                float sqrt = (float) Math.sqrt((double) ((f23 * f23) + (f24 * f24)));
                if (sqrt != Animation.CurveTimeline.LINEAR) {
                    sqrt = ((((((float) Math.sqrt((double) ((f8 * f8) + (f10 * f10)))) - sqrt) + transformConstraint.data.offsetScaleX) * f6) + sqrt) / sqrt;
                }
                bone2.f6593a *= sqrt;
                bone2.f6595c *= sqrt;
                float f25 = bone2.f6594b;
                float f26 = bone2.f6596d;
                float sqrt2 = (float) Math.sqrt((double) ((f25 * f25) + (f26 * f26)));
                if (sqrt2 != Animation.CurveTimeline.LINEAR) {
                    sqrt2 = ((((((float) Math.sqrt((double) ((f9 * f9) + (f11 * f11)))) - sqrt2) + transformConstraint.data.offsetScaleY) * f6) + sqrt2) / sqrt2;
                }
                bone2.f6594b *= sqrt2;
                bone2.f6596d *= sqrt2;
                z = true;
                f22 = Animation.CurveTimeline.LINEAR;
            }
            if (f7 > f22) {
                float f27 = bone2.f6594b;
                float f28 = bone2.f6596d;
                float atan22 = SpineUtils.atan2(f28, f27);
                float atan23 = (SpineUtils.atan2(f11, f9) - SpineUtils.atan2(f10, f8)) - (atan22 - SpineUtils.atan2(bone2.f6595c, bone2.f6593a));
                if (atan23 > 3.1415927f) {
                    atan23 -= 6.2831855f;
                } else if (atan23 < -3.1415927f) {
                    atan23 += 6.2831855f;
                }
                float f29 = atan22 + ((atan23 + f3) * f7);
                float sqrt3 = (float) Math.sqrt((double) ((f27 * f27) + (f28 * f28)));
                bone2.f6594b = SpineUtils.cos(f29) * sqrt3;
                bone2.f6596d = SpineUtils.sin(f29) * sqrt3;
                z = true;
            }
            if (z) {
                bone2.appliedValid = false;
            }
            i5 = i2 + 1;
            transformConstraint = this;
            aVar2 = aVar;
            i4 = i3;
            f14 = f3;
            f4 = f2;
        }
    }

    private void applyRelativeLocal() {
        float f2;
        float f3;
        float f4 = this.rotateMix;
        float f5 = this.translateMix;
        float f6 = this.scaleMix;
        float f7 = this.shearMix;
        Bone bone = this.target;
        if (!bone.appliedValid) {
            bone.updateAppliedTransform();
        }
        a<Bone> aVar = this.bones;
        int i2 = 0;
        int i3 = aVar.f5374b;
        while (i2 < i3) {
            Bone bone2 = aVar.get(i2);
            if (!bone2.appliedValid) {
                bone2.updateAppliedTransform();
            }
            float f8 = bone2.arotation;
            if (f4 != Animation.CurveTimeline.LINEAR) {
                f8 += (bone.arotation + this.data.offsetRotation) * f4;
            }
            float f9 = f8;
            float f10 = bone2.ax;
            float f11 = bone2.ay;
            if (f5 != Animation.CurveTimeline.LINEAR) {
                float f12 = bone.ax;
                TransformConstraintData transformConstraintData = this.data;
                f10 += (f12 + transformConstraintData.offsetX) * f5;
                f11 += (bone.ay + transformConstraintData.offsetY) * f5;
            }
            float f13 = f10;
            float f14 = bone2.ascaleX;
            float f15 = bone2.ascaleY;
            if (f6 != Animation.CurveTimeline.LINEAR) {
                f3 = f4;
                TransformConstraintData transformConstraintData2 = this.data;
                f2 = f5;
                f14 *= (((bone.ascaleX - 1.0f) + transformConstraintData2.offsetScaleX) * f6) + 1.0f;
                f15 *= (((bone.ascaleY - 1.0f) + transformConstraintData2.offsetScaleY) * f6) + 1.0f;
            } else {
                f3 = f4;
                f2 = f5;
            }
            float f16 = f15;
            float f17 = f14;
            float f18 = bone2.ashearY;
            if (f7 != Animation.CurveTimeline.LINEAR) {
                f18 += (bone.ashearY + this.data.offsetShearY) * f7;
            }
            bone2.updateWorldTransform(f13, f11, f9, f17, f16, bone2.ashearX, f18);
            i2++;
            f4 = f3;
            f5 = f2;
        }
    }

    private void applyRelativeWorld() {
        float f2;
        float f3;
        int i2;
        int i3;
        a<Bone> aVar;
        boolean z;
        Bone bone;
        float f4 = this.rotateMix;
        float f5 = this.translateMix;
        float f6 = this.scaleMix;
        float f7 = this.shearMix;
        Bone bone2 = this.target;
        float f8 = bone2.f6593a;
        float f9 = bone2.f6594b;
        float f10 = bone2.f6595c;
        float f11 = bone2.f6596d;
        float f12 = (f8 * f11) - (f9 * f10) > Animation.CurveTimeline.LINEAR ? 0.017453292f : -0.017453292f;
        TransformConstraintData transformConstraintData = this.data;
        float f13 = transformConstraintData.offsetRotation * f12;
        float f14 = transformConstraintData.offsetShearY * f12;
        a<Bone> aVar2 = this.bones;
        int i4 = aVar2.f5374b;
        int i5 = 0;
        while (i5 < i4) {
            Bone bone3 = aVar2.get(i5);
            if (f4 != Animation.CurveTimeline.LINEAR) {
                aVar = aVar2;
                float f15 = bone3.f6593a;
                i3 = i4;
                float f16 = bone3.f6594b;
                i2 = i5;
                float f17 = bone3.f6595c;
                f3 = f14;
                float f18 = bone3.f6596d;
                float atan2 = SpineUtils.atan2(f10, f8) + f13;
                if (atan2 > 3.1415927f) {
                    atan2 -= 6.2831855f;
                } else if (atan2 < -3.1415927f) {
                    atan2 += 6.2831855f;
                }
                float f19 = atan2 * f4;
                float cos = SpineUtils.cos(f19);
                float sin = SpineUtils.sin(f19);
                f2 = f4;
                bone3.f6593a = (cos * f15) - (sin * f17);
                bone3.f6594b = (cos * f16) - (sin * f18);
                bone3.f6595c = (f15 * sin) + (f17 * cos);
                bone3.f6596d = (sin * f16) + (cos * f18);
                z = true;
            } else {
                f2 = f4;
                aVar = aVar2;
                f3 = f14;
                i3 = i4;
                i2 = i5;
                z = false;
            }
            if (f5 != Animation.CurveTimeline.LINEAR) {
                p pVar = this.temp;
                TransformConstraintData transformConstraintData2 = this.data;
                pVar.d(transformConstraintData2.offsetX, transformConstraintData2.offsetY);
                bone2.localToWorld(pVar);
                bone3.worldX += pVar.f5294a * f5;
                bone3.worldY += pVar.f5295b * f5;
                z = true;
            }
            if (f6 > Animation.CurveTimeline.LINEAR) {
                float sqrt = (((((float) Math.sqrt((double) ((f8 * f8) + (f10 * f10)))) - 1.0f) + this.data.offsetScaleX) * f6) + 1.0f;
                bone3.f6593a *= sqrt;
                bone3.f6595c *= sqrt;
                float sqrt2 = (((((float) Math.sqrt((double) ((f9 * f9) + (f11 * f11)))) - 1.0f) + this.data.offsetScaleY) * f6) + 1.0f;
                bone3.f6594b *= sqrt2;
                bone3.f6596d *= sqrt2;
                z = true;
            }
            if (f7 > Animation.CurveTimeline.LINEAR) {
                float atan22 = SpineUtils.atan2(f11, f9) - SpineUtils.atan2(f10, f8);
                if (atan22 > 3.1415927f) {
                    atan22 -= 6.2831855f;
                } else if (atan22 < -3.1415927f) {
                    atan22 += 6.2831855f;
                }
                float f20 = bone3.f6594b;
                float f21 = bone3.f6596d;
                float atan23 = SpineUtils.atan2(f21, f20) + (((atan22 - 1.5707964f) + f3) * f7);
                float f22 = (f20 * f20) + (f21 * f21);
                bone = bone3;
                float sqrt3 = (float) Math.sqrt((double) f22);
                bone.f6594b = SpineUtils.cos(atan23) * sqrt3;
                bone.f6596d = SpineUtils.sin(atan23) * sqrt3;
                z = true;
            } else {
                bone = bone3;
            }
            if (z) {
                bone.appliedValid = false;
            }
            i5 = i2 + 1;
            aVar2 = aVar;
            i4 = i3;
            f14 = f3;
            f4 = f2;
        }
    }

    public void apply() {
        update();
    }

    public a<Bone> getBones() {
        return this.bones;
    }

    public TransformConstraintData getData() {
        return this.data;
    }

    public int getOrder() {
        return this.data.order;
    }

    public float getRotateMix() {
        return this.rotateMix;
    }

    public float getScaleMix() {
        return this.scaleMix;
    }

    public float getShearMix() {
        return this.shearMix;
    }

    public Bone getTarget() {
        return this.target;
    }

    public float getTranslateMix() {
        return this.translateMix;
    }

    public void setRotateMix(float f2) {
        this.rotateMix = f2;
    }

    public void setScaleMix(float f2) {
        this.scaleMix = f2;
    }

    public void setShearMix(float f2) {
        this.shearMix = f2;
    }

    public void setTarget(Bone bone) {
        this.target = bone;
    }

    public void setTranslateMix(float f2) {
        this.translateMix = f2;
    }

    public String toString() {
        return this.data.name;
    }

    public void update() {
        TransformConstraintData transformConstraintData = this.data;
        if (transformConstraintData.local) {
            if (transformConstraintData.relative) {
                applyRelativeLocal();
            } else {
                applyAbsoluteLocal();
            }
        } else if (transformConstraintData.relative) {
            applyRelativeWorld();
        } else {
            applyAbsoluteWorld();
        }
    }

    public TransformConstraint(TransformConstraint transformConstraint, Skeleton skeleton) {
        if (transformConstraint == null) {
            throw new IllegalArgumentException("constraint cannot be null.");
        } else if (skeleton != null) {
            this.data = transformConstraint.data;
            this.bones = new a<>(transformConstraint.bones.f5374b);
            Iterator<Bone> it = transformConstraint.bones.iterator();
            while (it.hasNext()) {
                this.bones.add(skeleton.bones.get(it.next().data.index));
            }
            this.target = skeleton.bones.get(transformConstraint.target.data.index);
            this.rotateMix = transformConstraint.rotateMix;
            this.translateMix = transformConstraint.translateMix;
            this.scaleMix = transformConstraint.scaleMix;
            this.shearMix = transformConstraint.shearMix;
        } else {
            throw new IllegalArgumentException("skeleton cannot be null.");
        }
    }
}
