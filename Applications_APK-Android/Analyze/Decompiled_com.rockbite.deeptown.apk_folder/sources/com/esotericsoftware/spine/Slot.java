package com.esotericsoftware.spine;

import com.badlogic.gdx.utils.m;
import com.esotericsoftware.spine.attachments.Attachment;
import e.d.b.t.b;

public class Slot {
    Attachment attachment;
    private float attachmentTime;
    private m attachmentVertices = new m();
    final Bone bone;
    final b color = new b();
    final b darkColor;
    final SlotData data;

    public Slot(SlotData slotData, Bone bone2) {
        if (slotData == null) {
            throw new IllegalArgumentException("data cannot be null.");
        } else if (bone2 != null) {
            this.data = slotData;
            this.bone = bone2;
            this.darkColor = slotData.darkColor == null ? null : new b();
            setToSetupPose();
        } else {
            throw new IllegalArgumentException("bone cannot be null.");
        }
    }

    public Attachment getAttachment() {
        return this.attachment;
    }

    public float getAttachmentTime() {
        return this.bone.skeleton.time - this.attachmentTime;
    }

    public m getAttachmentVertices() {
        return this.attachmentVertices;
    }

    public Bone getBone() {
        return this.bone;
    }

    public b getColor() {
        return this.color;
    }

    public b getDarkColor() {
        return this.darkColor;
    }

    public SlotData getData() {
        return this.data;
    }

    public Skeleton getSkeleton() {
        return this.bone.skeleton;
    }

    public void setAttachment(Attachment attachment2) {
        if (this.attachment != attachment2) {
            this.attachment = attachment2;
            this.attachmentTime = this.bone.skeleton.time;
            this.attachmentVertices.a();
        }
    }

    public void setAttachmentTime(float f2) {
        this.attachmentTime = this.bone.skeleton.time - f2;
    }

    public void setAttachmentVertices(m mVar) {
        if (mVar != null) {
            this.attachmentVertices = mVar;
            return;
        }
        throw new IllegalArgumentException("attachmentVertices cannot be null.");
    }

    public void setToSetupPose() {
        this.color.b(this.data.color);
        b bVar = this.darkColor;
        if (bVar != null) {
            bVar.b(this.data.darkColor);
        }
        SlotData slotData = this.data;
        String str = slotData.attachmentName;
        if (str == null) {
            setAttachment(null);
            return;
        }
        this.attachment = null;
        setAttachment(this.bone.skeleton.getAttachment(slotData.index, str));
    }

    public String toString() {
        return this.data.name;
    }

    public Slot(Slot slot, Bone bone2) {
        if (slot == null) {
            throw new IllegalArgumentException("slot cannot be null.");
        } else if (bone2 != null) {
            this.data = slot.data;
            this.bone = bone2;
            this.color.b(slot.color);
            b bVar = slot.darkColor;
            this.darkColor = bVar == null ? null : new b(bVar);
            this.attachment = slot.attachment;
            this.attachmentTime = slot.attachmentTime;
        } else {
            throw new IllegalArgumentException("bone cannot be null.");
        }
    }
}
