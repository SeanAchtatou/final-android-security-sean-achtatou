package com.esotericsoftware.spine;

import com.badlogic.gdx.math.p;
import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.c0;
import com.badlogic.gdx.utils.m;
import com.esotericsoftware.spine.Skin;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.MeshAttachment;
import com.esotericsoftware.spine.attachments.PathAttachment;
import com.esotericsoftware.spine.attachments.RegionAttachment;
import com.esotericsoftware.spine.utils.SpineUtils;
import e.d.b.t.b;
import java.util.Iterator;

public class Skeleton {
    final a<Bone> bones;
    final b color;
    final SkeletonData data;
    a<Slot> drawOrder;
    final a<IkConstraint> ikConstraints;
    final a<PathConstraint> pathConstraints;
    float scaleX = 1.0f;
    float scaleY = 1.0f;
    Skin skin;
    final a<Slot> slots;
    float time;
    final a<TransformConstraint> transformConstraints;
    final a<Updatable> updateCache = new a<>();
    final a<Bone> updateCacheReset = new a<>();
    float x;
    float y;

    public Skeleton(SkeletonData skeletonData) {
        Bone bone;
        if (skeletonData != null) {
            this.data = skeletonData;
            this.bones = new a<>(skeletonData.bones.f5374b);
            Iterator<BoneData> it = skeletonData.bones.iterator();
            while (it.hasNext()) {
                BoneData next = it.next();
                BoneData boneData = next.parent;
                if (boneData == null) {
                    bone = new Bone(next, this, (Bone) null);
                } else {
                    Bone bone2 = this.bones.get(boneData.index);
                    Bone bone3 = new Bone(next, this, bone2);
                    bone2.children.add(bone3);
                    bone = bone3;
                }
                this.bones.add(bone);
            }
            this.slots = new a<>(skeletonData.slots.f5374b);
            this.drawOrder = new a<>(skeletonData.slots.f5374b);
            Iterator<SlotData> it2 = skeletonData.slots.iterator();
            while (it2.hasNext()) {
                SlotData next2 = it2.next();
                Slot slot = new Slot(next2, this.bones.get(next2.boneData.index));
                this.slots.add(slot);
                this.drawOrder.add(slot);
            }
            this.ikConstraints = new a<>(skeletonData.ikConstraints.f5374b);
            Iterator<IkConstraintData> it3 = skeletonData.ikConstraints.iterator();
            while (it3.hasNext()) {
                this.ikConstraints.add(new IkConstraint(it3.next(), this));
            }
            this.transformConstraints = new a<>(skeletonData.transformConstraints.f5374b);
            Iterator<TransformConstraintData> it4 = skeletonData.transformConstraints.iterator();
            while (it4.hasNext()) {
                this.transformConstraints.add(new TransformConstraint(it4.next(), this));
            }
            this.pathConstraints = new a<>(skeletonData.pathConstraints.f5374b);
            Iterator<PathConstraintData> it5 = skeletonData.pathConstraints.iterator();
            while (it5.hasNext()) {
                this.pathConstraints.add(new PathConstraint(it5.next(), this));
            }
            this.color = new b(1.0f, 1.0f, 1.0f, 1.0f);
            updateCache();
            return;
        }
        throw new IllegalArgumentException("data cannot be null.");
    }

    private void sortBone(Bone bone) {
        if (!bone.sorted) {
            Bone bone2 = bone.parent;
            if (bone2 != null) {
                sortBone(bone2);
            }
            bone.sorted = true;
            this.updateCache.add(bone);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.a.a(java.lang.Object, boolean):boolean
     arg types: [com.esotericsoftware.spine.Bone, int]
     candidates:
      com.badlogic.gdx.utils.a.a(int, int):void
      com.badlogic.gdx.utils.a.a(int, java.lang.Object):void
      com.badlogic.gdx.utils.a.a(com.badlogic.gdx.utils.a, boolean):boolean
      com.badlogic.gdx.utils.a.a(java.lang.Object, boolean):boolean */
    private void sortIkConstraint(IkConstraint ikConstraint) {
        sortBone(ikConstraint.target);
        a<Bone> aVar = ikConstraint.bones;
        Bone a2 = aVar.a();
        sortBone(a2);
        if (aVar.f5374b > 1) {
            Bone peek = aVar.peek();
            if (!this.updateCache.a((Object) peek, true)) {
                this.updateCacheReset.add(peek);
            }
        }
        this.updateCache.add(ikConstraint);
        sortReset(a2.children);
        aVar.peek().sorted = true;
    }

    private void sortPathConstraint(PathConstraint pathConstraint) {
        Slot slot = pathConstraint.target;
        int i2 = slot.getData().index;
        Bone bone = slot.bone;
        Skin skin2 = this.skin;
        if (skin2 != null) {
            sortPathConstraintAttachment(skin2, i2, bone);
        }
        Skin skin3 = this.data.defaultSkin;
        if (!(skin3 == null || skin3 == this.skin)) {
            sortPathConstraintAttachment(skin3, i2, bone);
        }
        int i3 = this.data.skins.f5374b;
        for (int i4 = 0; i4 < i3; i4++) {
            sortPathConstraintAttachment(this.data.skins.get(i4), i2, bone);
        }
        Attachment attachment = slot.attachment;
        if (attachment instanceof PathAttachment) {
            sortPathConstraintAttachment(attachment, bone);
        }
        a<Bone> aVar = pathConstraint.bones;
        int i5 = aVar.f5374b;
        for (int i6 = 0; i6 < i5; i6++) {
            sortBone(aVar.get(i6));
        }
        this.updateCache.add(pathConstraint);
        for (int i7 = 0; i7 < i5; i7++) {
            sortReset(aVar.get(i7).children);
        }
        for (int i8 = 0; i8 < i5; i8++) {
            aVar.get(i8).sorted = true;
        }
    }

    private void sortPathConstraintAttachment(Skin skin2, int i2, Bone bone) {
        c0.a<Skin.Key, Attachment> a2 = skin2.attachments.a();
        a2.iterator();
        while (a2.hasNext()) {
            c0.b bVar = (c0.b) a2.next();
            if (((Skin.Key) bVar.f5459a).slotIndex == i2) {
                sortPathConstraintAttachment((Attachment) bVar.f5460b, bone);
            }
        }
    }

    private void sortReset(a<Bone> aVar) {
        int i2 = aVar.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            Bone bone = aVar.get(i3);
            if (bone.sorted) {
                sortReset(bone.children);
            }
            bone.sorted = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.a.a(java.lang.Object, boolean):boolean
     arg types: [com.esotericsoftware.spine.Bone, int]
     candidates:
      com.badlogic.gdx.utils.a.a(int, int):void
      com.badlogic.gdx.utils.a.a(int, java.lang.Object):void
      com.badlogic.gdx.utils.a.a(com.badlogic.gdx.utils.a, boolean):boolean
      com.badlogic.gdx.utils.a.a(java.lang.Object, boolean):boolean */
    private void sortTransformConstraint(TransformConstraint transformConstraint) {
        sortBone(transformConstraint.target);
        a<Bone> aVar = transformConstraint.bones;
        int i2 = aVar.f5374b;
        if (transformConstraint.data.local) {
            for (int i3 = 0; i3 < i2; i3++) {
                Bone bone = aVar.get(i3);
                sortBone(bone.parent);
                if (!this.updateCache.a((Object) bone, true)) {
                    this.updateCacheReset.add(bone);
                }
            }
        } else {
            for (int i4 = 0; i4 < i2; i4++) {
                sortBone(aVar.get(i4));
            }
        }
        this.updateCache.add(transformConstraint);
        for (int i5 = 0; i5 < i2; i5++) {
            sortReset(aVar.get(i5).children);
        }
        for (int i6 = 0; i6 < i2; i6++) {
            aVar.get(i6).sorted = true;
        }
    }

    public Bone findBone(String str) {
        if (str != null) {
            a<Bone> aVar = this.bones;
            int i2 = aVar.f5374b;
            for (int i3 = 0; i3 < i2; i3++) {
                Bone bone = aVar.get(i3);
                if (bone.data.name.equals(str)) {
                    return bone;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("boneName cannot be null.");
    }

    public IkConstraint findIkConstraint(String str) {
        if (str != null) {
            a<IkConstraint> aVar = this.ikConstraints;
            int i2 = aVar.f5374b;
            for (int i3 = 0; i3 < i2; i3++) {
                IkConstraint ikConstraint = aVar.get(i3);
                if (ikConstraint.data.name.equals(str)) {
                    return ikConstraint;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("constraintName cannot be null.");
    }

    public PathConstraint findPathConstraint(String str) {
        if (str != null) {
            a<PathConstraint> aVar = this.pathConstraints;
            int i2 = aVar.f5374b;
            for (int i3 = 0; i3 < i2; i3++) {
                PathConstraint pathConstraint = aVar.get(i3);
                if (pathConstraint.data.name.equals(str)) {
                    return pathConstraint;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("constraintName cannot be null.");
    }

    public Slot findSlot(String str) {
        if (str != null) {
            a<Slot> aVar = this.slots;
            int i2 = aVar.f5374b;
            for (int i3 = 0; i3 < i2; i3++) {
                Slot slot = aVar.get(i3);
                if (slot.data.name.equals(str)) {
                    return slot;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("slotName cannot be null.");
    }

    public TransformConstraint findTransformConstraint(String str) {
        if (str != null) {
            a<TransformConstraint> aVar = this.transformConstraints;
            int i2 = aVar.f5374b;
            for (int i3 = 0; i3 < i2; i3++) {
                TransformConstraint transformConstraint = aVar.get(i3);
                if (transformConstraint.data.name.equals(str)) {
                    return transformConstraint;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("constraintName cannot be null.");
    }

    public Attachment getAttachment(String str, String str2) {
        SlotData findSlot = this.data.findSlot(str);
        if (findSlot != null) {
            return getAttachment(findSlot.getIndex(), str2);
        }
        throw new IllegalArgumentException("Slot not found: " + str);
    }

    public a<Bone> getBones() {
        return this.bones;
    }

    public void getBounds(p pVar, p pVar2, m mVar) {
        float[] fArr;
        int i2;
        p pVar3 = pVar;
        p pVar4 = pVar2;
        m mVar2 = mVar;
        if (pVar3 == null) {
            throw new IllegalArgumentException("offset cannot be null.");
        } else if (pVar4 != null) {
            a<Slot> aVar = this.drawOrder;
            int i3 = aVar.f5374b;
            float f2 = 2.14748365E9f;
            int i4 = 0;
            int i5 = 0;
            float f3 = 2.14748365E9f;
            float f4 = -2.14748365E9f;
            float f5 = -2.14748365E9f;
            while (i5 < i3) {
                Slot slot = aVar.get(i5);
                Attachment attachment = slot.attachment;
                if (attachment instanceof RegionAttachment) {
                    fArr = mVar2.d(8);
                    ((RegionAttachment) attachment).computeWorldVertices(slot.getBone(), fArr, i4, 2);
                    i2 = 8;
                } else if (attachment instanceof MeshAttachment) {
                    MeshAttachment meshAttachment = (MeshAttachment) attachment;
                    i2 = meshAttachment.getWorldVerticesLength();
                    float[] d2 = mVar2.d(i2);
                    meshAttachment.computeWorldVertices(slot, 0, i2, d2, 0, 2);
                    fArr = d2;
                } else {
                    i2 = 0;
                    fArr = null;
                }
                if (fArr != null) {
                    float f6 = f5;
                    float f7 = f4;
                    float f8 = f3;
                    float f9 = f2;
                    for (int i6 = 0; i6 < i2; i6 += 2) {
                        float f10 = fArr[i6];
                        float f11 = fArr[i6 + 1];
                        f9 = Math.min(f9, f10);
                        f8 = Math.min(f8, f11);
                        f7 = Math.max(f7, f10);
                        f6 = Math.max(f6, f11);
                    }
                    f2 = f9;
                    f3 = f8;
                    f4 = f7;
                    f5 = f6;
                }
                i5++;
                i4 = 0;
            }
            pVar3.d(f2, f3);
            pVar4.d(f4 - f2, f5 - f3);
        } else {
            throw new IllegalArgumentException("size cannot be null.");
        }
    }

    public b getColor() {
        return this.color;
    }

    public SkeletonData getData() {
        return this.data;
    }

    public a<Slot> getDrawOrder() {
        return this.drawOrder;
    }

    public a<IkConstraint> getIkConstraints() {
        return this.ikConstraints;
    }

    public a<PathConstraint> getPathConstraints() {
        return this.pathConstraints;
    }

    public Bone getRootBone() {
        a<Bone> aVar = this.bones;
        if (aVar.f5374b == 0) {
            return null;
        }
        return aVar.a();
    }

    public float getScaleX() {
        return this.scaleX;
    }

    public float getScaleY() {
        return this.scaleY;
    }

    public Skin getSkin() {
        return this.skin;
    }

    public a<Slot> getSlots() {
        return this.slots;
    }

    public float getTime() {
        return this.time;
    }

    public a<TransformConstraint> getTransformConstraints() {
        return this.transformConstraints;
    }

    public a<Updatable> getUpdateCache() {
        return this.updateCache;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public void setAttachment(String str, String str2) {
        if (str != null) {
            Slot findSlot = findSlot(str);
            if (findSlot != null) {
                Attachment attachment = null;
                if (str2 == null || (attachment = getAttachment(findSlot.data.index, str2)) != null) {
                    findSlot.setAttachment(attachment);
                    return;
                }
                throw new IllegalArgumentException("Attachment not found: " + str2 + ", for slot: " + str);
            }
            throw new IllegalArgumentException("Slot not found: " + str);
        }
        throw new IllegalArgumentException("slotName cannot be null.");
    }

    public void setBonesToSetupPose() {
        a<Bone> aVar = this.bones;
        int i2 = aVar.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            aVar.get(i3).setToSetupPose();
        }
        a<IkConstraint> aVar2 = this.ikConstraints;
        int i4 = aVar2.f5374b;
        for (int i5 = 0; i5 < i4; i5++) {
            IkConstraint ikConstraint = aVar2.get(i5);
            IkConstraintData ikConstraintData = ikConstraint.data;
            ikConstraint.mix = ikConstraintData.mix;
            ikConstraint.bendDirection = ikConstraintData.bendDirection;
            ikConstraint.compress = ikConstraintData.compress;
            ikConstraint.stretch = ikConstraintData.stretch;
        }
        a<TransformConstraint> aVar3 = this.transformConstraints;
        int i6 = aVar3.f5374b;
        for (int i7 = 0; i7 < i6; i7++) {
            TransformConstraint transformConstraint = aVar3.get(i7);
            TransformConstraintData transformConstraintData = transformConstraint.data;
            transformConstraint.rotateMix = transformConstraintData.rotateMix;
            transformConstraint.translateMix = transformConstraintData.translateMix;
            transformConstraint.scaleMix = transformConstraintData.scaleMix;
            transformConstraint.shearMix = transformConstraintData.shearMix;
        }
        a<PathConstraint> aVar4 = this.pathConstraints;
        int i8 = aVar4.f5374b;
        for (int i9 = 0; i9 < i8; i9++) {
            PathConstraint pathConstraint = aVar4.get(i9);
            PathConstraintData pathConstraintData = pathConstraint.data;
            pathConstraint.position = pathConstraintData.position;
            pathConstraint.spacing = pathConstraintData.spacing;
            pathConstraint.rotateMix = pathConstraintData.rotateMix;
            pathConstraint.translateMix = pathConstraintData.translateMix;
        }
    }

    public void setColor(b bVar) {
        if (bVar != null) {
            this.color.b(bVar);
            return;
        }
        throw new IllegalArgumentException("color cannot be null.");
    }

    public void setDrawOrder(a<Slot> aVar) {
        if (aVar != null) {
            this.drawOrder = aVar;
            return;
        }
        throw new IllegalArgumentException("drawOrder cannot be null.");
    }

    public void setPosition(float f2, float f3) {
        this.x = f2;
        this.y = f3;
    }

    public void setScale(float f2, float f3) {
        this.scaleX = f2;
        this.scaleY = f3;
    }

    public void setScaleX(float f2) {
        this.scaleX = f2;
    }

    public void setScaleY(float f2) {
        this.scaleY = f2;
    }

    public void setSkin(String str) {
        Skin findSkin = this.data.findSkin(str);
        if (findSkin != null) {
            setSkin(findSkin);
            return;
        }
        throw new IllegalArgumentException("Skin not found: " + str);
    }

    public void setSlotsToSetupPose() {
        a<Slot> aVar = this.slots;
        System.arraycopy(aVar.f5373a, 0, this.drawOrder.f5373a, 0, aVar.f5374b);
        int i2 = aVar.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            aVar.get(i3).setToSetupPose();
        }
    }

    public void setTime(float f2) {
        this.time = f2;
    }

    public void setToSetupPose() {
        setBonesToSetupPose();
        setSlotsToSetupPose();
    }

    public void setX(float f2) {
        this.x = f2;
    }

    public void setY(float f2) {
        this.y = f2;
    }

    public String toString() {
        String str = this.data.name;
        return str != null ? str : super.toString();
    }

    public void update(float f2) {
        this.time += f2;
    }

    public void updateCache() {
        this.updateCache.clear();
        this.updateCacheReset.clear();
        a<Bone> aVar = this.bones;
        int i2 = aVar.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            aVar.get(i3).sorted = false;
        }
        a<IkConstraint> aVar2 = this.ikConstraints;
        a<TransformConstraint> aVar3 = this.transformConstraints;
        a<PathConstraint> aVar4 = this.pathConstraints;
        int i4 = aVar2.f5374b;
        int i5 = aVar3.f5374b;
        int i6 = aVar4.f5374b;
        int i7 = i4 + i5 + i6;
        for (int i8 = 0; i8 < i7; i8++) {
            int i9 = 0;
            while (true) {
                if (i9 >= i4) {
                    int i10 = 0;
                    while (true) {
                        if (i10 >= i5) {
                            int i11 = 0;
                            while (true) {
                                if (i11 >= i6) {
                                    break;
                                }
                                PathConstraint pathConstraint = aVar4.get(i11);
                                if (pathConstraint.data.order == i8) {
                                    sortPathConstraint(pathConstraint);
                                    break;
                                }
                                i11++;
                            }
                        } else {
                            TransformConstraint transformConstraint = aVar3.get(i10);
                            if (transformConstraint.data.order == i8) {
                                sortTransformConstraint(transformConstraint);
                                break;
                            }
                            i10++;
                        }
                    }
                } else {
                    IkConstraint ikConstraint = aVar2.get(i9);
                    if (ikConstraint.data.order == i8) {
                        sortIkConstraint(ikConstraint);
                        break;
                    }
                    i9++;
                }
            }
        }
        int i12 = aVar.f5374b;
        for (int i13 = 0; i13 < i12; i13++) {
            sortBone(aVar.get(i13));
        }
    }

    public void updateWorldTransform() {
        a<Bone> aVar = this.updateCacheReset;
        int i2 = aVar.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            Bone bone = aVar.get(i3);
            bone.ax = bone.x;
            bone.ay = bone.y;
            bone.arotation = bone.rotation;
            bone.ascaleX = bone.scaleX;
            bone.ascaleY = bone.scaleY;
            bone.ashearX = bone.shearX;
            bone.ashearY = bone.shearY;
            bone.appliedValid = true;
        }
        a<Updatable> aVar2 = this.updateCache;
        int i4 = aVar2.f5374b;
        for (int i5 = 0; i5 < i4; i5++) {
            aVar2.get(i5).update();
        }
    }

    private void sortPathConstraintAttachment(Attachment attachment, Bone bone) {
        if (attachment instanceof PathAttachment) {
            int[] bones2 = ((PathAttachment) attachment).getBones();
            if (bones2 == null) {
                sortBone(bone);
                return;
            }
            a<Bone> aVar = this.bones;
            int i2 = 0;
            int length = bones2.length;
            while (i2 < length) {
                int i3 = i2 + 1;
                int i4 = bones2[i2] + i3;
                while (i3 < i4) {
                    sortBone(aVar.get(bones2[i3]));
                    i3++;
                }
                i2 = i3;
            }
        }
    }

    public Attachment getAttachment(int i2, String str) {
        Attachment attachment;
        if (str != null) {
            Skin skin2 = this.skin;
            if (skin2 != null && (attachment = skin2.getAttachment(i2, str)) != null) {
                return attachment;
            }
            Skin skin3 = this.data.defaultSkin;
            if (skin3 != null) {
                return skin3.getAttachment(i2, str);
            }
            return null;
        }
        throw new IllegalArgumentException("attachmentName cannot be null.");
    }

    public void setSkin(Skin skin2) {
        Attachment attachment;
        if (skin2 != null) {
            Skin skin3 = this.skin;
            if (skin3 != null) {
                skin2.attachAll(this, skin3);
            } else {
                a<Slot> aVar = this.slots;
                int i2 = aVar.f5374b;
                for (int i3 = 0; i3 < i2; i3++) {
                    Slot slot = aVar.get(i3);
                    String str = slot.data.attachmentName;
                    if (!(str == null || (attachment = skin2.getAttachment(i3, str)) == null)) {
                        slot.setAttachment(attachment);
                    }
                }
            }
        }
        this.skin = skin2;
    }

    public void updateWorldTransform(Bone bone) {
        a<Bone> aVar = this.updateCacheReset;
        int i2 = aVar.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            Bone bone2 = aVar.get(i3);
            bone2.ax = bone2.x;
            bone2.ay = bone2.y;
            bone2.arotation = bone2.rotation;
            bone2.ascaleX = bone2.scaleX;
            bone2.ascaleY = bone2.scaleY;
            bone2.ashearX = bone2.shearX;
            bone2.ashearY = bone2.shearY;
            bone2.appliedValid = true;
        }
        Bone rootBone = getRootBone();
        float f2 = bone.f6593a;
        float f3 = bone.f6594b;
        float f4 = bone.f6595c;
        float f5 = bone.f6596d;
        float f6 = this.x;
        float f7 = this.y;
        rootBone.worldX = (f2 * f6) + (f3 * f7) + bone.worldX;
        rootBone.worldY = (f6 * f4) + (f7 * f5) + bone.worldY;
        float f8 = rootBone.rotation;
        float f9 = 90.0f + f8 + rootBone.shearY;
        float cosDeg = SpineUtils.cosDeg(f8 + rootBone.shearX) * rootBone.scaleX;
        float cosDeg2 = SpineUtils.cosDeg(f9) * rootBone.scaleY;
        float sinDeg = SpineUtils.sinDeg(rootBone.rotation + rootBone.shearX) * rootBone.scaleX;
        float sinDeg2 = SpineUtils.sinDeg(f9) * rootBone.scaleY;
        float f10 = this.scaleX;
        rootBone.f6593a = ((f2 * cosDeg) + (f3 * sinDeg)) * f10;
        rootBone.f6594b = ((f2 * cosDeg2) + (f3 * sinDeg2)) * f10;
        float f11 = this.scaleY;
        rootBone.f6595c = ((cosDeg * f4) + (sinDeg * f5)) * f11;
        rootBone.f6596d = ((f4 * cosDeg2) + (f5 * sinDeg2)) * f11;
        a<Updatable> aVar2 = this.updateCache;
        int i4 = aVar2.f5374b;
        for (int i5 = 0; i5 < i4; i5++) {
            Updatable updatable = aVar2.get(i5);
            if (updatable != rootBone) {
                updatable.update();
            }
        }
    }

    public Skeleton(Skeleton skeleton) {
        Bone bone;
        if (skeleton != null) {
            this.data = skeleton.data;
            this.bones = new a<>(skeleton.bones.f5374b);
            Iterator<Bone> it = skeleton.bones.iterator();
            while (it.hasNext()) {
                Bone next = it.next();
                Bone bone2 = next.parent;
                if (bone2 == null) {
                    bone = new Bone(next, this, (Bone) null);
                } else {
                    Bone bone3 = this.bones.get(bone2.data.index);
                    Bone bone4 = new Bone(next, this, bone3);
                    bone3.children.add(bone4);
                    bone = bone4;
                }
                this.bones.add(bone);
            }
            this.slots = new a<>(skeleton.slots.f5374b);
            Iterator<Slot> it2 = skeleton.slots.iterator();
            while (it2.hasNext()) {
                Slot next2 = it2.next();
                this.slots.add(new Slot(next2, this.bones.get(next2.bone.data.index)));
            }
            this.drawOrder = new a<>(this.slots.f5374b);
            Iterator<Slot> it3 = skeleton.drawOrder.iterator();
            while (it3.hasNext()) {
                this.drawOrder.add(this.slots.get(it3.next().data.index));
            }
            this.ikConstraints = new a<>(skeleton.ikConstraints.f5374b);
            Iterator<IkConstraint> it4 = skeleton.ikConstraints.iterator();
            while (it4.hasNext()) {
                this.ikConstraints.add(new IkConstraint(it4.next(), this));
            }
            this.transformConstraints = new a<>(skeleton.transformConstraints.f5374b);
            Iterator<TransformConstraint> it5 = skeleton.transformConstraints.iterator();
            while (it5.hasNext()) {
                this.transformConstraints.add(new TransformConstraint(it5.next(), this));
            }
            this.pathConstraints = new a<>(skeleton.pathConstraints.f5374b);
            Iterator<PathConstraint> it6 = skeleton.pathConstraints.iterator();
            while (it6.hasNext()) {
                this.pathConstraints.add(new PathConstraint(it6.next(), this));
            }
            this.skin = skeleton.skin;
            this.color = new b(skeleton.color);
            this.time = skeleton.time;
            this.scaleX = skeleton.scaleX;
            this.scaleY = skeleton.scaleY;
            updateCache();
            return;
        }
        throw new IllegalArgumentException("skeleton cannot be null.");
    }
}
