package com.esotericsoftware.spine;

import e.d.b.t.b;

public class SlotData {
    String attachmentName;
    BlendMode blendMode;
    final BoneData boneData;
    final b color = new b(1.0f, 1.0f, 1.0f, 1.0f);
    b darkColor;
    final int index;
    final String name;

    public SlotData(int i2, String str, BoneData boneData2) {
        if (i2 < 0) {
            throw new IllegalArgumentException("index must be >= 0.");
        } else if (str == null) {
            throw new IllegalArgumentException("name cannot be null.");
        } else if (boneData2 != null) {
            this.index = i2;
            this.name = str;
            this.boneData = boneData2;
        } else {
            throw new IllegalArgumentException("boneData cannot be null.");
        }
    }

    public String getAttachmentName() {
        return this.attachmentName;
    }

    public BlendMode getBlendMode() {
        return this.blendMode;
    }

    public BoneData getBoneData() {
        return this.boneData;
    }

    public b getColor() {
        return this.color;
    }

    public b getDarkColor() {
        return this.darkColor;
    }

    public int getIndex() {
        return this.index;
    }

    public String getName() {
        return this.name;
    }

    public void setAttachmentName(String str) {
        this.attachmentName = str;
    }

    public void setBlendMode(BlendMode blendMode2) {
        this.blendMode = blendMode2;
    }

    public void setDarkColor(b bVar) {
        this.darkColor = bVar;
    }

    public String toString() {
        return this.name;
    }
}
