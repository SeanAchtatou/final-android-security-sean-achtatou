package com.esotericsoftware.spine;

public enum BlendMode {
    normal(770, 1, 771),
    additive(770, 1, 1),
    multiply(774, 774, 771),
    screen(1, 1, 769);
    
    public static BlendMode[] values = values();
    int dest;
    int source;
    int sourcePMA;

    private BlendMode(int i2, int i3, int i4) {
        this.source = i2;
        this.sourcePMA = i3;
        this.dest = i4;
    }

    public int getDest() {
        return this.dest;
    }

    public int getSource(boolean z) {
        return z ? this.sourcePMA : this.source;
    }
}
