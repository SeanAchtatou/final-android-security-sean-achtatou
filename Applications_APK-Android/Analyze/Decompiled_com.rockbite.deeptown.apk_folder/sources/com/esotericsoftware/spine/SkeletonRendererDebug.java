package com.esotericsoftware.spine;

import com.badlogic.gdx.graphics.glutils.t;
import com.badlogic.gdx.math.p;
import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.m;
import com.esotericsoftware.spine.Animation;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.BoundingBoxAttachment;
import com.esotericsoftware.spine.attachments.ClippingAttachment;
import com.esotericsoftware.spine.attachments.MeshAttachment;
import com.esotericsoftware.spine.attachments.PathAttachment;
import com.esotericsoftware.spine.attachments.PointAttachment;
import com.esotericsoftware.spine.attachments.RegionAttachment;
import e.d.b.g;
import e.d.b.t.b;

public class SkeletonRendererDebug {
    private static final b aabbColor = new b(Animation.CurveTimeline.LINEAR, 1.0f, Animation.CurveTimeline.LINEAR, 0.5f);
    private static final b attachmentLineColor = new b(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f, 0.5f);
    private static final b boneLineColor = b.E;
    private static final b boneOriginColor = b.s;
    private static final b triangleLineColor = new b(1.0f, 0.64f, Animation.CurveTimeline.LINEAR, 0.5f);
    private float boneWidth;
    private final SkeletonBounds bounds;
    private boolean drawBones;
    private boolean drawBoundingBoxes;
    private boolean drawClipping;
    private boolean drawMeshHull;
    private boolean drawMeshTriangles;
    private boolean drawPaths;
    private boolean drawPoints;
    private boolean drawRegionAttachments;
    private boolean premultipliedAlpha;
    private float scale;
    private final t shapes;
    private final p temp1;
    private final p temp2;
    private final m vertices;

    public SkeletonRendererDebug() {
        this.drawBones = true;
        this.drawRegionAttachments = true;
        this.drawBoundingBoxes = true;
        this.drawPoints = true;
        this.drawMeshHull = true;
        this.drawMeshTriangles = true;
        this.drawPaths = true;
        this.drawClipping = true;
        this.bounds = new SkeletonBounds();
        this.vertices = new m(32);
        this.scale = 1.0f;
        this.boneWidth = 2.0f;
        this.temp1 = new p();
        this.temp2 = new p();
        this.shapes = new t();
    }

    public void draw(Skeleton skeleton) {
        int i2;
        int i3;
        int i4;
        b bVar;
        float f2;
        float f3;
        int i5;
        g.f6806g.a(3042);
        g.f6806g.g(this.premultipliedAlpha ? 1 : 770, 771);
        t tVar = this.shapes;
        a<Bone> bones = skeleton.getBones();
        a<Slot> slots = skeleton.getSlots();
        tVar.a(t.a.Filled);
        char c2 = 0;
        if (this.drawBones) {
            int i6 = bones.f5374b;
            int i7 = 0;
            while (i7 < i6) {
                Bone bone = bones.get(i7);
                if (bone.parent == null) {
                    i5 = i7;
                } else {
                    float f4 = bone.data.length;
                    float f5 = this.boneWidth;
                    if (f4 == Animation.CurveTimeline.LINEAR) {
                        f5 /= 2.0f;
                        tVar.setColor(boneOriginColor);
                        f4 = 8.0f;
                    } else {
                        tVar.setColor(boneLineColor);
                    }
                    float f6 = bone.worldX;
                    float f7 = f4 * bone.f6595c;
                    float f8 = bone.worldY;
                    float f9 = f7 + f8;
                    float f10 = f5 * this.scale;
                    float f11 = f8;
                    i5 = i7;
                    tVar.a(f6, f11, (bone.f6593a * f4) + f6, f9, f10);
                }
                i7 = i5 + 1;
            }
            tVar.d(skeleton.getX(), skeleton.getY(), this.scale * 4.0f);
        }
        if (this.drawPoints) {
            tVar.setColor(boneOriginColor);
            int i8 = slots.f5374b;
            for (int i9 = 0; i9 < i8; i9++) {
                Slot slot = slots.get(i9);
                Attachment attachment = slot.attachment;
                if (attachment instanceof PointAttachment) {
                    PointAttachment pointAttachment = (PointAttachment) attachment;
                    pointAttachment.computeWorldPosition(slot.getBone(), this.temp1);
                    p pVar = this.temp2;
                    pVar.d(8.0f, Animation.CurveTimeline.LINEAR);
                    pVar.a(pointAttachment.computeWorldRotation(slot.getBone()));
                    tVar.a(this.temp1, this.temp2, (this.boneWidth / 2.0f) * this.scale);
                }
            }
        }
        tVar.end();
        tVar.a(t.a.Line);
        if (this.drawRegionAttachments) {
            tVar.setColor(attachmentLineColor);
            int i10 = slots.f5374b;
            for (int i11 = 0; i11 < i10; i11++) {
                Slot slot2 = slots.get(i11);
                Attachment attachment2 = slot2.attachment;
                if (attachment2 instanceof RegionAttachment) {
                    float[] fArr = this.vertices.f5527a;
                    ((RegionAttachment) attachment2).computeWorldVertices(slot2.getBone(), fArr, 0, 2);
                    tVar.a(fArr[0], fArr[1], fArr[2], fArr[3]);
                    tVar.a(fArr[2], fArr[3], fArr[4], fArr[5]);
                    tVar.a(fArr[4], fArr[5], fArr[6], fArr[7]);
                    tVar.a(fArr[6], fArr[7], fArr[0], fArr[1]);
                }
            }
        }
        if (this.drawMeshHull || this.drawMeshTriangles) {
            int i12 = slots.f5374b;
            for (int i13 = 0; i13 < i12; i13++) {
                Slot slot3 = slots.get(i13);
                Attachment attachment3 = slot3.attachment;
                if (attachment3 instanceof MeshAttachment) {
                    MeshAttachment meshAttachment = (MeshAttachment) attachment3;
                    float[] d2 = this.vertices.d(meshAttachment.getWorldVerticesLength());
                    meshAttachment.computeWorldVertices(slot3, 0, meshAttachment.getWorldVerticesLength(), d2, 0, 2);
                    short[] triangles = meshAttachment.getTriangles();
                    int hullLength = meshAttachment.getHullLength();
                    if (this.drawMeshTriangles) {
                        tVar.setColor(triangleLineColor);
                        int length = triangles.length;
                        int i14 = 0;
                        while (i14 < length) {
                            int i15 = triangles[i14] * 2;
                            int i16 = triangles[i14 + 1] * 2;
                            int i17 = triangles[i14 + 2] * 2;
                            tVar.a(d2[i15], d2[i15 + 1], d2[i16], d2[i16 + 1], d2[i17], d2[i17 + 1]);
                            i14 += 3;
                            hullLength = hullLength;
                            length = length;
                            triangles = triangles;
                        }
                    }
                    int i18 = hullLength;
                    if (this.drawMeshHull && i18 > 0) {
                        tVar.setColor(attachmentLineColor);
                        float f12 = d2[i18 - 2];
                        float f13 = d2[i18 - 1];
                        float f14 = f12;
                        int i19 = 0;
                        while (i19 < i18) {
                            float f15 = d2[i19];
                            float f16 = d2[i19 + 1];
                            tVar.a(f15, f16, f14, f13);
                            i19 += 2;
                            f14 = f15;
                            f13 = f16;
                        }
                    }
                }
            }
        }
        if (this.drawBoundingBoxes) {
            SkeletonBounds skeletonBounds = this.bounds;
            skeletonBounds.update(skeleton, true);
            tVar.setColor(aabbColor);
            tVar.b(skeletonBounds.getMinX(), skeletonBounds.getMinY(), skeletonBounds.getWidth(), skeletonBounds.getHeight());
            a<m> polygons = skeletonBounds.getPolygons();
            a<BoundingBoxAttachment> boundingBoxes = skeletonBounds.getBoundingBoxes();
            int i20 = polygons.f5374b;
            for (int i21 = 0; i21 < i20; i21++) {
                m mVar = polygons.get(i21);
                tVar.setColor(boundingBoxes.get(i21).getColor());
                tVar.b(mVar.f5527a, 0, mVar.f5528b);
            }
        }
        if (this.drawClipping) {
            int i22 = slots.f5374b;
            for (int i23 = 0; i23 < i22; i23++) {
                Slot slot4 = slots.get(i23);
                Attachment attachment4 = slot4.attachment;
                if (attachment4 instanceof ClippingAttachment) {
                    ClippingAttachment clippingAttachment = (ClippingAttachment) attachment4;
                    int worldVerticesLength = clippingAttachment.getWorldVerticesLength();
                    float[] d3 = this.vertices.d(worldVerticesLength);
                    clippingAttachment.computeWorldVertices(slot4, 0, worldVerticesLength, d3, 0, 2);
                    tVar.setColor(clippingAttachment.getColor());
                    for (int i24 = 2; i24 < worldVerticesLength; i24 += 2) {
                        tVar.a(d3[i24 - 2], d3[i24 - 1], d3[i24], d3[i24 + 1]);
                    }
                    tVar.a(d3[0], d3[1], d3[worldVerticesLength - 2], d3[worldVerticesLength - 1]);
                }
            }
        }
        if (this.drawPaths) {
            int i25 = slots.f5374b;
            int i26 = 0;
            while (i26 < i25) {
                Slot slot5 = slots.get(i26);
                Attachment attachment5 = slot5.attachment;
                if (!(attachment5 instanceof PathAttachment)) {
                    i2 = i25;
                    i3 = i26;
                } else {
                    PathAttachment pathAttachment = (PathAttachment) attachment5;
                    int worldVerticesLength2 = pathAttachment.getWorldVerticesLength();
                    float[] d4 = this.vertices.d(worldVerticesLength2);
                    pathAttachment.computeWorldVertices(slot5, 0, worldVerticesLength2, d4, 0, 2);
                    b color = pathAttachment.getColor();
                    float f17 = d4[2];
                    float f18 = d4[3];
                    if (pathAttachment.getClosed()) {
                        tVar.setColor(color);
                        float f19 = d4[c2];
                        float f20 = d4[1];
                        float f21 = d4[worldVerticesLength2 - 2];
                        float f22 = d4[worldVerticesLength2 - 1];
                        float f23 = d4[worldVerticesLength2 - 4];
                        float f24 = d4[worldVerticesLength2 - 3];
                        float f25 = f23;
                        float f26 = f22;
                        float f27 = f21;
                        float f28 = f20;
                        bVar = color;
                        i4 = worldVerticesLength2;
                        i3 = i26;
                        tVar.a(f17, f18, f19, f28, f27, f26, f25, f24, 32);
                        tVar.setColor(b.f6928f);
                        f2 = f18;
                        f3 = f17;
                        tVar.a(f3, f2, f19, f28);
                        tVar.a(f25, f24, f27, f26);
                    } else {
                        f2 = f18;
                        f3 = f17;
                        bVar = color;
                        i4 = worldVerticesLength2;
                        i3 = i26;
                    }
                    int i27 = i4 - 4;
                    float f29 = f3;
                    float f30 = f2;
                    int i28 = 4;
                    while (i28 < i27) {
                        float f31 = d4[i28];
                        float f32 = d4[i28 + 1];
                        float f33 = d4[i28 + 2];
                        float f34 = d4[i28 + 3];
                        float f35 = d4[i28 + 4];
                        float f36 = d4[i28 + 5];
                        b bVar2 = bVar;
                        tVar.setColor(bVar2);
                        float f37 = f36;
                        float f38 = f35;
                        float f39 = f34;
                        float f40 = f33;
                        tVar.a(f29, f30, f31, f32, f40, f39, f38, f37, 32);
                        tVar.setColor(b.f6928f);
                        tVar.a(f29, f30, f31, f32);
                        float f41 = f37;
                        float f42 = f38;
                        tVar.a(f42, f41, f40, f39);
                        i28 += 6;
                        f29 = f42;
                        f30 = f41;
                        bVar = bVar2;
                        i25 = i25;
                    }
                    i2 = i25;
                }
                i26 = i3 + 1;
                i25 = i2;
                c2 = 0;
            }
        }
        tVar.end();
        tVar.a(t.a.Filled);
        if (this.drawBones) {
            tVar.setColor(boneOriginColor);
            int i29 = bones.f5374b;
            for (int i30 = 0; i30 < i29; i30++) {
                Bone bone2 = bones.get(i30);
                tVar.a(bone2.worldX, bone2.worldY, this.scale * 3.0f, 8);
            }
        }
        if (this.drawPoints) {
            tVar.setColor(boneOriginColor);
            int i31 = slots.f5374b;
            for (int i32 = 0; i32 < i31; i32++) {
                Slot slot6 = slots.get(i32);
                Attachment attachment6 = slot6.attachment;
                if (attachment6 instanceof PointAttachment) {
                    ((PointAttachment) attachment6).computeWorldPosition(slot6.getBone(), this.temp1);
                    p pVar2 = this.temp1;
                    tVar.a(pVar2.f5294a, pVar2.f5295b, this.scale * 3.0f, 8);
                }
            }
        }
        tVar.end();
    }

    public t getShapeRenderer() {
        return this.shapes;
    }

    public void setBones(boolean z) {
        this.drawBones = z;
    }

    public void setBoundingBoxes(boolean z) {
        this.drawBoundingBoxes = z;
    }

    public void setClipping(boolean z) {
        this.drawClipping = z;
    }

    public void setMeshHull(boolean z) {
        this.drawMeshHull = z;
    }

    public void setMeshTriangles(boolean z) {
        this.drawMeshTriangles = z;
    }

    public void setPaths(boolean z) {
        this.drawPaths = z;
    }

    public void setPoints(boolean z) {
        this.drawPoints = z;
    }

    public void setPremultipliedAlpha(boolean z) {
        this.premultipliedAlpha = z;
    }

    public void setRegionAttachments(boolean z) {
        this.drawRegionAttachments = z;
    }

    public void setScale(float f2) {
        this.scale = f2;
    }

    public SkeletonRendererDebug(t tVar) {
        this.drawBones = true;
        this.drawRegionAttachments = true;
        this.drawBoundingBoxes = true;
        this.drawPoints = true;
        this.drawMeshHull = true;
        this.drawMeshTriangles = true;
        this.drawPaths = true;
        this.drawClipping = true;
        this.bounds = new SkeletonBounds();
        this.vertices = new m(32);
        this.scale = 1.0f;
        this.boneWidth = 2.0f;
        this.temp1 = new p();
        this.temp2 = new p();
        this.shapes = tVar;
    }
}
