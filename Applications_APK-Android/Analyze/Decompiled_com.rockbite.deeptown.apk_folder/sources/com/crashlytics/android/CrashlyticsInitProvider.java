package com.crashlytics.android;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import h.a.a.a.c;
import h.a.a.a.n.b.r;

public class CrashlyticsInitProvider extends ContentProvider {

    interface a {
        boolean a(Context context);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0032 A[SYNTHETIC, Splitter:B:12:0x0032] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private h.a.a.a.i[] a(android.content.Context r7) {
        /*
            r6 = this;
            java.lang.String r0 = "Fabric"
            r1 = 1
            r2 = 0
            android.content.pm.PackageManager r3 = r7.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0020 }
            java.lang.String r7 = r7.getPackageName()     // Catch:{ NameNotFoundException -> 0x0020 }
            r4 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r7 = r3.getApplicationInfo(r7, r4)     // Catch:{ NameNotFoundException -> 0x0020 }
            android.os.Bundle r7 = r7.metaData     // Catch:{ NameNotFoundException -> 0x0020 }
            if (r7 == 0) goto L_0x002a
            java.lang.String r3 = "firebase_crashlytics_ndk_enabled"
            boolean r7 = r7.getBoolean(r3, r2)     // Catch:{ NameNotFoundException -> 0x0020 }
            if (r7 == 0) goto L_0x002a
            r7 = 1
            goto L_0x002b
        L_0x0020:
            r7 = move-exception
            h.a.a.a.l r3 = h.a.a.a.c.f()
            java.lang.String r4 = "Unable to get PackageManager while determining if Crashlytics NDK should be initialized"
            r3.c(r0, r4, r7)
        L_0x002a:
            r7 = 0
        L_0x002b:
            com.crashlytics.android.a r3 = new com.crashlytics.android.a
            r3.<init>()
            if (r7 == 0) goto L_0x0052
            h.a.a.a.l r7 = h.a.a.a.c.f()     // Catch:{ all -> 0x0048 }
            java.lang.String r4 = "Crashlytics is initializing NDK crash reporter."
            r7.e(r0, r4)     // Catch:{ all -> 0x0048 }
            r7 = 2
            h.a.a.a.i[] r7 = new h.a.a.a.i[r7]     // Catch:{ all -> 0x0048 }
            r7[r2] = r3     // Catch:{ all -> 0x0048 }
            com.crashlytics.android.ndk.CrashlyticsNdk r4 = new com.crashlytics.android.ndk.CrashlyticsNdk     // Catch:{ all -> 0x0048 }
            r4.<init>()     // Catch:{ all -> 0x0048 }
            r7[r1] = r4     // Catch:{ all -> 0x0048 }
            return r7
        L_0x0048:
            r7 = move-exception
            h.a.a.a.l r4 = h.a.a.a.c.f()
            java.lang.String r5 = "Crashlytics failed to initialize NDK crash reporting. Attempting to intialize SDK..."
            r4.b(r0, r5, r7)
        L_0x0052:
            h.a.a.a.i[] r7 = new h.a.a.a.i[r1]
            r7[r2] = r3
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crashlytics.android.CrashlyticsInitProvider.a(android.content.Context):h.a.a.a.i[]");
    }

    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    public boolean onCreate() {
        Context context = getContext();
        if (a(context, new r(), new b())) {
            try {
                c.a(context, a(context.getApplicationContext()));
                c.f().e("CrashlyticsInitProvider", "CrashlyticsInitProvider initialization successful");
                return true;
            } catch (IllegalStateException unused) {
                c.f().e("CrashlyticsInitProvider", "CrashlyticsInitProvider initialization unsuccessful");
                return false;
            }
        } else {
            c.f().e("CrashlyticsInitProvider", "CrashlyticsInitProvider skipping initialization");
            return true;
        }
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Context context, r rVar, a aVar) {
        if (rVar.e(context)) {
            return aVar.a(context);
        }
        return rVar.d(context);
    }
}
