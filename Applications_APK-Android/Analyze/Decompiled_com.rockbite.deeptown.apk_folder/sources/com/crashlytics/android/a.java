package com.crashlytics.android;

import com.crashlytics.android.c.b;
import com.crashlytics.android.e.l;
import h.a.a.a.c;
import h.a.a.a.i;
import h.a.a.a.j;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/* compiled from: Crashlytics */
public class a extends i<Void> implements j {

    /* renamed from: g  reason: collision with root package name */
    public final l f6261g;

    /* renamed from: h  reason: collision with root package name */
    public final Collection<? extends i> f6262h;

    public a() {
        this(new b(), new com.crashlytics.android.d.a(), new l());
    }

    private static void n() {
        if (o() == null) {
            throw new IllegalStateException("Crashlytics must be initialized by calling Fabric.with(Context) prior to calling Crashlytics.getInstance()");
        }
    }

    public static a o() {
        return (a) c.a(a.class);
    }

    public Collection<? extends i> a() {
        return this.f6262h;
    }

    /* access modifiers changed from: protected */
    public Void c() {
        return null;
    }

    public String h() {
        return "com.crashlytics.sdk.android:crashlytics";
    }

    public String j() {
        return "2.10.1.34";
    }

    a(b bVar, com.crashlytics.android.d.a aVar, l lVar) {
        this.f6261g = lVar;
        this.f6262h = Collections.unmodifiableCollection(Arrays.asList(bVar, aVar, lVar));
    }

    public static void a(Throwable th) {
        n();
        o().f6261g.a(th);
    }

    public static void a(String str) {
        n();
        o().f6261g.b(str);
    }

    public static void a(String str, String str2) {
        n();
        o().f6261g.a(str, str2);
    }
}
