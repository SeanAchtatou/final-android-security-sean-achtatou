package com.crashlytics.android.d;

import h.a.a.a.c;
import h.a.a.a.i;
import h.a.a.a.n.b.n;
import h.a.a.a.n.b.s;
import java.util.Collections;
import java.util.Map;

/* compiled from: Beta */
public class a extends i<Boolean> implements n {
    public Map<s.a, String> b() {
        return Collections.emptyMap();
    }

    public String h() {
        return "com.crashlytics.sdk.android:beta";
    }

    public String j() {
        return "1.2.10.27";
    }

    /* access modifiers changed from: protected */
    public Boolean c() {
        c.f().d("Beta", "Beta kit initializing...");
        return true;
    }
}
