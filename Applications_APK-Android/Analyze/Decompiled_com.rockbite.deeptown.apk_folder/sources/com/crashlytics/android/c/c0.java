package com.crashlytics.android.c;

import android.annotation.TargetApi;
import android.os.Build;
import h.a.a.a.n.d.a;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: SessionEventTransform */
class c0 implements a<a0> {
    c0() {
    }

    /* renamed from: b */
    public byte[] a(a0 a0Var) throws IOException {
        return a(a0Var).toString().getBytes("UTF-8");
    }

    @TargetApi(9)
    public JSONObject a(a0 a0Var) throws IOException {
        try {
            JSONObject jSONObject = new JSONObject();
            b0 b0Var = a0Var.f6264a;
            jSONObject.put("appBundleId", b0Var.f6290a);
            jSONObject.put("executionId", b0Var.f6291b);
            jSONObject.put("installationId", b0Var.f6292c);
            jSONObject.put("limitAdTrackingEnabled", b0Var.f6293d);
            jSONObject.put("betaDeviceToken", b0Var.f6294e);
            jSONObject.put("buildId", b0Var.f6295f);
            jSONObject.put("osVersion", b0Var.f6296g);
            jSONObject.put("deviceModel", b0Var.f6297h);
            jSONObject.put("appVersionCode", b0Var.f6298i);
            jSONObject.put("appVersionName", b0Var.f6299j);
            jSONObject.put("timestamp", a0Var.f6265b);
            jSONObject.put("type", a0Var.f6266c.toString());
            if (a0Var.f6267d != null) {
                jSONObject.put("details", new JSONObject(a0Var.f6267d));
            }
            jSONObject.put("customType", a0Var.f6268e);
            if (a0Var.f6269f != null) {
                jSONObject.put("customAttributes", new JSONObject(a0Var.f6269f));
            }
            jSONObject.put("predefinedType", a0Var.f6270g);
            if (a0Var.f6271h != null) {
                jSONObject.put("predefinedAttributes", new JSONObject(a0Var.f6271h));
            }
            return jSONObject;
        } catch (JSONException e2) {
            if (Build.VERSION.SDK_INT >= 9) {
                throw new IOException(e2.getMessage(), e2);
            }
            throw new IOException(e2.getMessage());
        }
    }
}
