package com.crashlytics.android.c;

import android.content.Context;
import h.a.a.a.n.b.i;
import h.a.a.a.n.b.s;
import java.util.Map;
import java.util.UUID;

/* compiled from: SessionMetadataCollector */
class d0 {

    /* renamed from: a  reason: collision with root package name */
    private final Context f6319a;

    /* renamed from: b  reason: collision with root package name */
    private final s f6320b;

    /* renamed from: c  reason: collision with root package name */
    private final String f6321c;

    /* renamed from: d  reason: collision with root package name */
    private final String f6322d;

    public d0(Context context, s sVar, String str, String str2) {
        this.f6319a = context;
        this.f6320b = sVar;
        this.f6321c = str;
        this.f6322d = str2;
    }

    public b0 a() {
        Map<s.a, String> e2 = this.f6320b.e();
        String n = i.n(this.f6319a);
        String j2 = this.f6320b.j();
        String g2 = this.f6320b.g();
        return new b0(this.f6320b.c(), UUID.randomUUID().toString(), this.f6320b.d(), this.f6320b.k(), e2.get(s.a.FONT_TOKEN), n, j2, g2, this.f6321c, this.f6322d);
    }
}
