package com.crashlytics.android.c;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import h.a.a.a.n.c.o.a;
import java.util.Random;

/* compiled from: RandomBackoff */
class t implements a {

    /* renamed from: a  reason: collision with root package name */
    final a f6355a;

    /* renamed from: b  reason: collision with root package name */
    final Random f6356b;

    /* renamed from: c  reason: collision with root package name */
    final double f6357c;

    public t(a aVar, double d2) {
        this(aVar, d2, new Random());
    }

    public long a(int i2) {
        double a2 = a();
        double a3 = (double) this.f6355a.a(i2);
        Double.isNaN(a3);
        return (long) (a2 * a3);
    }

    public t(a aVar, double d2, Random random) {
        if (d2 < FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE || d2 > 1.0d) {
            throw new IllegalArgumentException("jitterPercent must be between 0.0 and 1.0");
        } else if (aVar == null) {
            throw new NullPointerException("backoff must not be null");
        } else if (random != null) {
            this.f6355a = aVar;
            this.f6357c = d2;
            this.f6356b = random;
        } else {
            throw new NullPointerException("random must not be null");
        }
    }

    /* access modifiers changed from: package-private */
    public double a() {
        double d2 = this.f6357c;
        double d3 = 1.0d - d2;
        return d3 + (((d2 + 1.0d) - d3) * this.f6356b.nextDouble());
    }
}
