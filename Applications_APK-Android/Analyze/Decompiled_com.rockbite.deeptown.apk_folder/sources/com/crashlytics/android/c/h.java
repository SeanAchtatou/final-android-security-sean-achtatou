package com.crashlytics.android.c;

import h.a.a.a.n.c.o.b;
import h.a.a.a.n.c.o.c;
import h.a.a.a.n.c.o.e;
import h.a.a.a.n.d.f;
import java.io.File;
import java.util.List;

/* compiled from: AnswersRetryFilesSender */
class h implements f {

    /* renamed from: a  reason: collision with root package name */
    private final x f6328a;

    /* renamed from: b  reason: collision with root package name */
    private final u f6329b;

    h(x xVar, u uVar) {
        this.f6328a = xVar;
        this.f6329b = uVar;
    }

    public static h a(x xVar) {
        return new h(xVar, new u(new e(new t(new c(1000, 8), 0.1d), new b(5))));
    }

    public boolean a(List<File> list) {
        long nanoTime = System.nanoTime();
        if (this.f6329b.a(nanoTime)) {
            if (this.f6328a.a(list)) {
                this.f6329b.a();
                return true;
            }
            this.f6329b.b(nanoTime);
        }
        return false;
    }
}
