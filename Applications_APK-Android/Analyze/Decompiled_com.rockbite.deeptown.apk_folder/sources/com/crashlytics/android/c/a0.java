package com.crashlytics.android.c;

import android.app.Activity;
import java.util.Collections;
import java.util.Map;

/* compiled from: SessionEvent */
final class a0 {

    /* renamed from: a  reason: collision with root package name */
    public final b0 f6264a;

    /* renamed from: b  reason: collision with root package name */
    public final long f6265b;

    /* renamed from: c  reason: collision with root package name */
    public final c f6266c;

    /* renamed from: d  reason: collision with root package name */
    public final Map<String, String> f6267d;

    /* renamed from: e  reason: collision with root package name */
    public final String f6268e;

    /* renamed from: f  reason: collision with root package name */
    public final Map<String, Object> f6269f;

    /* renamed from: g  reason: collision with root package name */
    public final String f6270g;

    /* renamed from: h  reason: collision with root package name */
    public final Map<String, Object> f6271h;

    /* renamed from: i  reason: collision with root package name */
    private String f6272i;

    /* compiled from: SessionEvent */
    static class b {

        /* renamed from: a  reason: collision with root package name */
        final c f6273a;

        /* renamed from: b  reason: collision with root package name */
        final long f6274b = System.currentTimeMillis();

        /* renamed from: c  reason: collision with root package name */
        Map<String, String> f6275c = null;

        /* renamed from: d  reason: collision with root package name */
        String f6276d = null;

        /* renamed from: e  reason: collision with root package name */
        Map<String, Object> f6277e = null;

        /* renamed from: f  reason: collision with root package name */
        String f6278f = null;

        /* renamed from: g  reason: collision with root package name */
        Map<String, Object> f6279g = null;

        public b(c cVar) {
            this.f6273a = cVar;
        }

        public b a(Map<String, Object> map) {
            this.f6277e = map;
            return this;
        }

        public b b(Map<String, String> map) {
            this.f6275c = map;
            return this;
        }

        public a0 a(b0 b0Var) {
            return new a0(b0Var, this.f6274b, this.f6273a, this.f6275c, this.f6276d, this.f6277e, this.f6278f, this.f6279g);
        }
    }

    /* compiled from: SessionEvent */
    enum c {
        START,
        RESUME,
        PAUSE,
        STOP,
        CRASH,
        INSTALL,
        CUSTOM,
        PREDEFINED
    }

    public static b a(c cVar, Activity activity) {
        Map singletonMap = Collections.singletonMap("activity", activity.getClass().getName());
        b bVar = new b(cVar);
        bVar.b(singletonMap);
        return bVar;
    }

    public String toString() {
        if (this.f6272i == null) {
            this.f6272i = "[" + a0.class.getSimpleName() + ": " + "timestamp=" + this.f6265b + ", type=" + this.f6266c + ", details=" + this.f6267d + ", customType=" + this.f6268e + ", customAttributes=" + this.f6269f + ", predefinedType=" + this.f6270g + ", predefinedAttributes=" + this.f6271h + ", metadata=[" + this.f6264a + "]]";
        }
        return this.f6272i;
    }

    private a0(b0 b0Var, long j2, c cVar, Map<String, String> map, String str, Map<String, Object> map2, String str2, Map<String, Object> map3) {
        this.f6264a = b0Var;
        this.f6265b = j2;
        this.f6266c = cVar;
        this.f6267d = map;
        this.f6268e = str;
        this.f6269f = map2;
        this.f6270g = str2;
        this.f6271h = map3;
    }

    public static b a(long j2) {
        b bVar = new b(c.INSTALL);
        bVar.b(Collections.singletonMap("installedAt", String.valueOf(j2)));
        return bVar;
    }

    public static b a(String str) {
        Map singletonMap = Collections.singletonMap("sessionId", str);
        b bVar = new b(c.CRASH);
        bVar.b(singletonMap);
        return bVar;
    }

    public static b a(String str, String str2) {
        b a2 = a(str);
        a2.a(Collections.singletonMap("exceptionName", str2));
        return a2;
    }
}
