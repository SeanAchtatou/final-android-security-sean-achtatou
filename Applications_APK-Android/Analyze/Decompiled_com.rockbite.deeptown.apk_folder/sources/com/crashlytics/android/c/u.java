package com.crashlytics.android.c;

import h.a.a.a.n.c.o.e;

/* compiled from: RetryManager */
class u {

    /* renamed from: a  reason: collision with root package name */
    long f6358a;

    /* renamed from: b  reason: collision with root package name */
    private e f6359b;

    public u(e eVar) {
        if (eVar != null) {
            this.f6359b = eVar;
            return;
        }
        throw new NullPointerException("retryState must not be null");
    }

    public boolean a(long j2) {
        return j2 - this.f6358a >= this.f6359b.a() * 1000000;
    }

    public void b(long j2) {
        this.f6358a = j2;
        this.f6359b = this.f6359b.c();
    }

    public void a() {
        this.f6358a = 0;
        this.f6359b = this.f6359b.b();
    }
}
