package com.crashlytics.android.c;

import android.content.Context;
import android.os.Looper;
import h.a.a.a.n.b.w;
import h.a.a.a.n.d.g;
import h.a.a.a.n.f.a;
import java.io.IOException;

/* compiled from: AnswersFilesManagerProvider */
class e {

    /* renamed from: a  reason: collision with root package name */
    final Context f6323a;

    /* renamed from: b  reason: collision with root package name */
    final a f6324b;

    public e(Context context, a aVar) {
        this.f6323a = context;
        this.f6324b = aVar;
    }

    public w a() throws IOException {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            return new w(this.f6323a, new c0(), new w(), new g(this.f6323a, this.f6324b.a(), "session_analytics.tap", "session_analytics_to_send"));
        }
        throw new IllegalStateException("AnswersFilesManagerProvider cannot be called on the main thread");
    }
}
