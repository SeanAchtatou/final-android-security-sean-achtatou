package com.crashlytics.android.c;

import android.content.Context;
import com.crashlytics.android.c.a0;
import h.a.a.a.i;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: AnswersEventsHandler */
class d implements h.a.a.a.n.d.d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final i f6301a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final Context f6302b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final e f6303c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final d0 f6304d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public final h.a.a.a.n.e.e f6305e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public final o f6306f;

    /* renamed from: g  reason: collision with root package name */
    final ScheduledExecutorService f6307g;

    /* renamed from: h  reason: collision with root package name */
    z f6308h = new k();

    /* compiled from: AnswersEventsHandler */
    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ h.a.a.a.n.g.b f6309a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f6310b;

        a(h.a.a.a.n.g.b bVar, String str) {
            this.f6309a = bVar;
            this.f6310b = str;
        }

        public void run() {
            try {
                d.this.f6308h.a(this.f6309a, this.f6310b);
            } catch (Exception e2) {
                h.a.a.a.c.f().b("Answers", "Failed to set analytics settings data", e2);
            }
        }
    }

    /* compiled from: AnswersEventsHandler */
    class b implements Runnable {
        b() {
        }

        public void run() {
            try {
                z zVar = d.this.f6308h;
                d.this.f6308h = new k();
                zVar.d();
            } catch (Exception e2) {
                h.a.a.a.c.f().b("Answers", "Failed to disable events", e2);
            }
        }
    }

    /* compiled from: AnswersEventsHandler */
    class c implements Runnable {
        c() {
        }

        public void run() {
            try {
                d.this.f6308h.a();
            } catch (Exception e2) {
                h.a.a.a.c.f().b("Answers", "Failed to send events files", e2);
            }
        }
    }

    /* renamed from: com.crashlytics.android.c.d$d  reason: collision with other inner class name */
    /* compiled from: AnswersEventsHandler */
    class C0132d implements Runnable {
        C0132d() {
        }

        public void run() {
            try {
                b0 a2 = d.this.f6304d.a();
                w a3 = d.this.f6303c.a();
                a3.a((h.a.a.a.n.d.d) d.this);
                d.this.f6308h = new l(d.this.f6301a, d.this.f6302b, d.this.f6307g, a3, d.this.f6305e, a2, d.this.f6306f);
            } catch (Exception e2) {
                h.a.a.a.c.f().b("Answers", "Failed to enable events", e2);
            }
        }
    }

    /* compiled from: AnswersEventsHandler */
    class e implements Runnable {
        e() {
        }

        public void run() {
            try {
                d.this.f6308h.b();
            } catch (Exception e2) {
                h.a.a.a.c.f().b("Answers", "Failed to flush events", e2);
            }
        }
    }

    /* compiled from: AnswersEventsHandler */
    class f implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ a0.b f6316a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ boolean f6317b;

        f(a0.b bVar, boolean z) {
            this.f6316a = bVar;
            this.f6317b = z;
        }

        public void run() {
            try {
                d.this.f6308h.a(this.f6316a);
                if (this.f6317b) {
                    d.this.f6308h.b();
                }
            } catch (Exception e2) {
                h.a.a.a.c.f().b("Answers", "Failed to process event", e2);
            }
        }
    }

    public d(i iVar, Context context, e eVar, d0 d0Var, h.a.a.a.n.e.e eVar2, ScheduledExecutorService scheduledExecutorService, o oVar) {
        this.f6301a = iVar;
        this.f6302b = context;
        this.f6303c = eVar;
        this.f6304d = d0Var;
        this.f6305e = eVar2;
        this.f6307g = scheduledExecutorService;
        this.f6306f = oVar;
    }

    public void a(a0.b bVar) {
        a(bVar, false, false);
    }

    public void b(a0.b bVar) {
        a(bVar, false, true);
    }

    public void c(a0.b bVar) {
        a(bVar, true, false);
    }

    public void a(h.a.a.a.n.g.b bVar, String str) {
        a(new a(bVar, str));
    }

    public void b() {
        a(new C0132d());
    }

    public void c() {
        a(new e());
    }

    private void b(Runnable runnable) {
        try {
            this.f6307g.submit(runnable).get();
        } catch (Exception e2) {
            h.a.a.a.c.f().b("Answers", "Failed to run events task", e2);
        }
    }

    public void a() {
        a(new b());
    }

    public void a(String str) {
        a(new c());
    }

    /* access modifiers changed from: package-private */
    public void a(a0.b bVar, boolean z, boolean z2) {
        f fVar = new f(bVar, z2);
        if (z) {
            b(fVar);
        } else {
            a(fVar);
        }
    }

    private void a(Runnable runnable) {
        try {
            this.f6307g.submit(runnable);
        } catch (Exception e2) {
            h.a.a.a.c.f().b("Answers", "Failed to submit events task", e2);
        }
    }
}
