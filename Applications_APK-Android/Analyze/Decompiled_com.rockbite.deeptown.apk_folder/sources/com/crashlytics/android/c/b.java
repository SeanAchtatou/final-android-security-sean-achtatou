package com.crashlytics.android.c;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import h.a.a.a.c;
import h.a.a.a.i;
import h.a.a.a.n.b.j;
import h.a.a.a.n.b.l;
import h.a.a.a.n.b.r;
import h.a.a.a.n.g.u;
import java.io.File;

/* compiled from: Answers */
public class b extends i<Boolean> {

    /* renamed from: g  reason: collision with root package name */
    y f6289g;

    public void a(j.b bVar) {
        y yVar = this.f6289g;
        if (yVar != null) {
            yVar.a(bVar.b());
        }
    }

    public String h() {
        return "com.crashlytics.sdk.android:answers";
    }

    public String j() {
        return "1.4.7.32";
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    public boolean m() {
        long lastModified;
        try {
            Context d2 = d();
            PackageManager packageManager = d2.getPackageManager();
            String packageName = d2.getPackageName();
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
            String num = Integer.toString(packageInfo.versionCode);
            String str = packageInfo.versionName == null ? "0.0" : packageInfo.versionName;
            if (Build.VERSION.SDK_INT >= 9) {
                lastModified = packageInfo.firstInstallTime;
            } else {
                lastModified = new File(packageManager.getApplicationInfo(packageName, 0).sourceDir).lastModified();
            }
            this.f6289g = y.a(this, d2, g(), num, str, lastModified);
            this.f6289g.c();
            new r().e(d2);
            return true;
        } catch (Exception e2) {
            c.f().b("Answers", "Error retrieving app properties", e2);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public String n() {
        return h.a.a.a.n.b.i.b(d(), "com.crashlytics.ApiEndpoint");
    }

    /* access modifiers changed from: protected */
    public Boolean c() {
        if (!l.a(d()).a()) {
            c.f().d("Fabric", "Analytics collection disabled, because data collection is disabled by Firebase.");
            this.f6289g.b();
            return false;
        }
        try {
            u a2 = h.a.a.a.n.g.r.d().a();
            if (a2 == null) {
                c.f().b("Answers", "Failed to retrieve settings");
                return false;
            } else if (a2.f15317d.f15290c) {
                c.f().d("Answers", "Analytics collection enabled");
                this.f6289g.a(a2.f15318e, n());
                return true;
            } else {
                c.f().d("Answers", "Analytics collection disabled");
                this.f6289g.b();
                return false;
            }
        } catch (Exception e2) {
            c.f().b("Answers", "Error dealing with settings", e2);
            return false;
        }
    }

    public void a(j.a aVar) {
        y yVar = this.f6289g;
        if (yVar != null) {
            yVar.a(aVar.b(), aVar.a());
        }
    }
}
