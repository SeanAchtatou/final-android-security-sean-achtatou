package com.crashlytics.android.c;

import com.crashlytics.android.c.a0;
import java.util.HashSet;
import java.util.Set;

/* compiled from: SamplingEventFilter */
class v implements m {

    /* renamed from: b  reason: collision with root package name */
    static final Set<a0.c> f6360b = new a();

    /* renamed from: a  reason: collision with root package name */
    final int f6361a;

    /* compiled from: SamplingEventFilter */
    static class a extends HashSet<a0.c> {
        a() {
            add(a0.c.START);
            add(a0.c.RESUME);
            add(a0.c.PAUSE);
            add(a0.c.STOP);
        }
    }

    public v(int i2) {
        this.f6361a = i2;
    }

    public boolean a(a0 a0Var) {
        boolean z = f6360b.contains(a0Var.f6266c) && a0Var.f6264a.f6294e == null;
        boolean z2 = Math.abs(a0Var.f6264a.f6292c.hashCode() % this.f6361a) != 0;
        if (!z || !z2) {
            return false;
        }
        return true;
    }
}
