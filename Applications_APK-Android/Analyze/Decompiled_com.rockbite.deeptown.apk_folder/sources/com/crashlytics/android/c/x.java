package com.crashlytics.android.c;

import h.a.a.a.i;
import h.a.a.a.l;
import h.a.a.a.n.b.a;
import h.a.a.a.n.b.v;
import h.a.a.a.n.d.f;
import h.a.a.a.n.e.c;
import h.a.a.a.n.e.d;
import h.a.a.a.n.e.e;
import java.io.File;
import java.util.List;

/* compiled from: SessionAnalyticsFilesSender */
class x extends a implements f {

    /* renamed from: g  reason: collision with root package name */
    private final String f6363g;

    public x(i iVar, String str, String str2, e eVar, String str3) {
        super(iVar, str, str2, eVar, c.POST);
        this.f6363g = str3;
    }

    public boolean a(List<File> list) {
        d a2 = a();
        a2.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        a2.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.f15055e.j());
        a2.c("X-CRASHLYTICS-API-KEY", this.f6363g);
        int i2 = 0;
        for (File next : list) {
            a2.a("session_analytics_file_" + i2, next.getName(), "application/vnd.crashlytics.android.events", next);
            i2++;
        }
        l f2 = h.a.a.a.c.f();
        f2.d("Answers", "Sending " + list.size() + " analytics files to " + b());
        int g2 = a2.g();
        l f3 = h.a.a.a.c.f();
        f3.d("Answers", "Response code for analytics file send is " + g2);
        if (v.a(g2) == 0) {
            return true;
        }
        return false;
    }
}
