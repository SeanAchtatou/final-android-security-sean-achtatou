package com.crashlytics.android.c;

/* compiled from: SessionEventMetadata */
final class b0 {

    /* renamed from: a  reason: collision with root package name */
    public final String f6290a;

    /* renamed from: b  reason: collision with root package name */
    public final String f6291b;

    /* renamed from: c  reason: collision with root package name */
    public final String f6292c;

    /* renamed from: d  reason: collision with root package name */
    public final Boolean f6293d;

    /* renamed from: e  reason: collision with root package name */
    public final String f6294e;

    /* renamed from: f  reason: collision with root package name */
    public final String f6295f;

    /* renamed from: g  reason: collision with root package name */
    public final String f6296g;

    /* renamed from: h  reason: collision with root package name */
    public final String f6297h;

    /* renamed from: i  reason: collision with root package name */
    public final String f6298i;

    /* renamed from: j  reason: collision with root package name */
    public final String f6299j;

    /* renamed from: k  reason: collision with root package name */
    private String f6300k;

    public b0(String str, String str2, String str3, Boolean bool, String str4, String str5, String str6, String str7, String str8, String str9) {
        this.f6290a = str;
        this.f6291b = str2;
        this.f6292c = str3;
        this.f6293d = bool;
        this.f6294e = str4;
        this.f6295f = str5;
        this.f6296g = str6;
        this.f6297h = str7;
        this.f6298i = str8;
        this.f6299j = str9;
    }

    public String toString() {
        if (this.f6300k == null) {
            this.f6300k = "appBundleId=" + this.f6290a + ", executionId=" + this.f6291b + ", installationId=" + this.f6292c + ", limitAdTrackingEnabled=" + this.f6293d + ", betaDeviceToken=" + this.f6294e + ", buildId=" + this.f6295f + ", osVersion=" + this.f6296g + ", deviceModel=" + this.f6297h + ", appVersionCode=" + this.f6298i + ", appVersionName=" + this.f6299j;
        }
        return this.f6300k;
    }
}
