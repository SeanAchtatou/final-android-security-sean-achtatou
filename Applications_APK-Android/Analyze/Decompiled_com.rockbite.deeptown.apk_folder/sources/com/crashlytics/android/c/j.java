package com.crashlytics.android.c;

import h.a.a.a.c;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: BackgroundManager */
class j {

    /* renamed from: a  reason: collision with root package name */
    private final ScheduledExecutorService f6332a;

    /* renamed from: b  reason: collision with root package name */
    private final List<b> f6333b = new ArrayList();

    /* renamed from: c  reason: collision with root package name */
    private volatile boolean f6334c = true;

    /* renamed from: d  reason: collision with root package name */
    final AtomicReference<ScheduledFuture<?>> f6335d = new AtomicReference<>();

    /* renamed from: e  reason: collision with root package name */
    boolean f6336e = true;

    /* compiled from: BackgroundManager */
    class a implements Runnable {
        a() {
        }

        public void run() {
            j.this.f6335d.set(null);
            j.this.c();
        }
    }

    /* compiled from: BackgroundManager */
    public interface b {
        void a();
    }

    public j(ScheduledExecutorService scheduledExecutorService) {
        this.f6332a = scheduledExecutorService;
    }

    /* access modifiers changed from: private */
    public void c() {
        for (b a2 : this.f6333b) {
            a2.a();
        }
    }

    public void b() {
        this.f6336e = false;
        ScheduledFuture andSet = this.f6335d.getAndSet(null);
        if (andSet != null) {
            andSet.cancel(false);
        }
    }

    public void a(boolean z) {
        this.f6334c = z;
    }

    public void a(b bVar) {
        this.f6333b.add(bVar);
    }

    public void a() {
        if (this.f6334c && !this.f6336e) {
            this.f6336e = true;
            try {
                this.f6335d.compareAndSet(null, this.f6332a.schedule(new a(), 5000, TimeUnit.MILLISECONDS));
            } catch (RejectedExecutionException e2) {
                c.f().c("Answers", "Failed to schedule background detector", e2);
            }
        }
    }
}
