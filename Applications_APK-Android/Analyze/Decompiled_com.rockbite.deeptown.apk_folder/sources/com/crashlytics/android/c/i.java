package com.crashlytics.android.c;

import android.content.Context;
import android.os.Bundle;
import java.lang.reflect.Method;

/* compiled from: AppMeasurementEventLogger */
public class i implements n {

    /* renamed from: a  reason: collision with root package name */
    private final Method f6330a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f6331b;

    public i(Object obj, Method method) {
        this.f6331b = obj;
        this.f6330a = method;
    }

    private static Class a(Context context) {
        try {
            return context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement");
        } catch (Exception unused) {
            return null;
        }
    }

    public static n b(Context context) {
        Object a2;
        Method b2;
        Class a3 = a(context);
        if (a3 == null || (a2 = a(context, a3)) == null || (b2 = b(context, a3)) == null) {
            return null;
        }
        return new i(a2, b2);
    }

    public void logEvent(String str, String str2, Bundle bundle) {
        try {
            this.f6330a.invoke(this.f6331b, str, str2, bundle);
        } catch (Exception unused) {
        }
    }

    private static Object a(Context context, Class cls) {
        try {
            return cls.getDeclaredMethod("getInstance", Context.class).invoke(cls, context);
        } catch (Exception unused) {
            return null;
        }
    }

    public void a(String str, Bundle bundle) {
        logEvent("fab", str, bundle);
    }

    private static Method b(Context context, Class cls) {
        try {
            return cls.getDeclaredMethod("logEventInternal", String.class, String.class, Bundle.class);
        } catch (Exception unused) {
            return null;
        }
    }
}
