package com.crashlytics.android.c;

import android.content.Context;
import h.a.a.a.n.b.k;
import h.a.a.a.n.d.b;
import h.a.a.a.n.d.c;
import java.io.IOException;
import java.util.UUID;

/* compiled from: SessionAnalyticsFilesManager */
class w extends b<a0> {

    /* renamed from: g  reason: collision with root package name */
    private h.a.a.a.n.g.b f6362g;

    w(Context context, c0 c0Var, k kVar, c cVar) throws IOException {
        super(context, c0Var, kVar, cVar, 100);
    }

    /* access modifiers changed from: package-private */
    public void a(h.a.a.a.n.g.b bVar) {
        this.f6362g = bVar;
    }

    /* access modifiers changed from: protected */
    public String c() {
        UUID randomUUID = UUID.randomUUID();
        return "sa" + "_" + randomUUID.toString() + "_" + this.f15199c.a() + ".tap";
    }

    /* access modifiers changed from: protected */
    public int e() {
        h.a.a.a.n.g.b bVar = this.f6362g;
        return bVar == null ? super.e() : bVar.f15255c;
    }

    /* access modifiers changed from: protected */
    public int f() {
        h.a.a.a.n.g.b bVar = this.f6362g;
        return bVar == null ? super.f() : bVar.f15256d;
    }
}
