package com.crashlytics.android.c;

import android.app.Activity;
import android.os.Bundle;
import com.crashlytics.android.c.a0;
import h.a.a.a.a;

/* compiled from: AnswersLifecycleCallbacks */
class f extends a.b {

    /* renamed from: a  reason: collision with root package name */
    private final y f6325a;

    /* renamed from: b  reason: collision with root package name */
    private final j f6326b;

    public f(y yVar, j jVar) {
        this.f6325a = yVar;
        this.f6326b = jVar;
    }

    public void a(Activity activity) {
    }

    public void a(Activity activity, Bundle bundle) {
    }

    public void b(Activity activity) {
        this.f6325a.a(activity, a0.c.PAUSE);
        this.f6326b.a();
    }

    public void b(Activity activity, Bundle bundle) {
    }

    public void c(Activity activity) {
        this.f6325a.a(activity, a0.c.RESUME);
        this.f6326b.b();
    }

    public void d(Activity activity) {
        this.f6325a.a(activity, a0.c.START);
    }

    public void e(Activity activity) {
        this.f6325a.a(activity, a0.c.STOP);
    }
}
