package com.crashlytics.android.c;

import android.content.Context;
import com.crashlytics.android.c.a0;
import com.tapjoy.TJAdUnitConstants;
import h.a.a.a.c;
import h.a.a.a.i;
import h.a.a.a.n.b.g;
import h.a.a.a.n.d.f;
import h.a.a.a.n.e.e;
import h.a.a.a.n.g.b;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: EnabledSessionAnalyticsManagerStrategy */
class l implements z {

    /* renamed from: a  reason: collision with root package name */
    private final i f6338a;

    /* renamed from: b  reason: collision with root package name */
    private final e f6339b;

    /* renamed from: c  reason: collision with root package name */
    private final Context f6340c;

    /* renamed from: d  reason: collision with root package name */
    private final w f6341d;

    /* renamed from: e  reason: collision with root package name */
    private final ScheduledExecutorService f6342e;

    /* renamed from: f  reason: collision with root package name */
    private final AtomicReference<ScheduledFuture<?>> f6343f = new AtomicReference<>();

    /* renamed from: g  reason: collision with root package name */
    final b0 f6344g;

    /* renamed from: h  reason: collision with root package name */
    private final o f6345h;

    /* renamed from: i  reason: collision with root package name */
    f f6346i;

    /* renamed from: j  reason: collision with root package name */
    g f6347j = new g();

    /* renamed from: k  reason: collision with root package name */
    m f6348k = new r();
    boolean l = true;
    boolean m = true;
    volatile int n = -1;
    boolean o = false;
    boolean p = false;

    public l(i iVar, Context context, ScheduledExecutorService scheduledExecutorService, w wVar, e eVar, b0 b0Var, o oVar) {
        this.f6338a = iVar;
        this.f6340c = context;
        this.f6342e = scheduledExecutorService;
        this.f6341d = wVar;
        this.f6339b = eVar;
        this.f6344g = b0Var;
        this.f6345h = oVar;
    }

    public void a(b bVar, String str) {
        String str2;
        String str3;
        this.f6346i = h.a(new x(this.f6338a, str, bVar.f15253a, this.f6339b, this.f6347j.d(this.f6340c)));
        this.f6341d.a(bVar);
        this.o = bVar.f15257e;
        this.p = bVar.f15258f;
        h.a.a.a.l f2 = c.f();
        StringBuilder sb = new StringBuilder();
        sb.append("Firebase analytics forwarding ");
        boolean z = this.o;
        String str4 = TJAdUnitConstants.String.ENABLED;
        sb.append(z ? str4 : "disabled");
        f2.d("Answers", sb.toString());
        h.a.a.a.l f3 = c.f();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Firebase analytics including purchase events ");
        if (this.p) {
            str2 = str4;
        } else {
            str2 = "disabled";
        }
        sb2.append(str2);
        f3.d("Answers", sb2.toString());
        this.l = bVar.f15259g;
        h.a.a.a.l f4 = c.f();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Custom event tracking ");
        if (this.l) {
            str3 = str4;
        } else {
            str3 = "disabled";
        }
        sb3.append(str3);
        f4.d("Answers", sb3.toString());
        this.m = bVar.f15260h;
        h.a.a.a.l f5 = c.f();
        StringBuilder sb4 = new StringBuilder();
        sb4.append("Predefined event tracking ");
        if (!this.m) {
            str4 = "disabled";
        }
        sb4.append(str4);
        f5.d("Answers", sb4.toString());
        if (bVar.f15262j > 1) {
            c.f().d("Answers", "Event sampling enabled");
            this.f6348k = new v(bVar.f15262j);
        }
        this.n = bVar.f15254b;
        a(0, (long) this.n);
    }

    public boolean b() {
        try {
            return this.f6341d.g();
        } catch (IOException e2) {
            h.a.a.a.n.b.i.a(this.f6340c, "Failed to roll file over.", e2);
            return false;
        }
    }

    public void c() {
        if (this.f6343f.get() != null) {
            h.a.a.a.n.b.i.c(this.f6340c, "Cancelling time-based rollover because no events are currently being generated.");
            this.f6343f.get().cancel(false);
            this.f6343f.set(null);
        }
    }

    public void d() {
        this.f6341d.a();
    }

    public void e() {
        if (this.n != -1) {
            a((long) this.n, (long) this.n);
        }
    }

    public void a(a0.b bVar) {
        a0 a2 = bVar.a(this.f6344g);
        if (!this.l && a0.c.CUSTOM.equals(a2.f6266c)) {
            h.a.a.a.l f2 = c.f();
            f2.d("Answers", "Custom events tracking disabled - skipping event: " + a2);
        } else if (!this.m && a0.c.PREDEFINED.equals(a2.f6266c)) {
            h.a.a.a.l f3 = c.f();
            f3.d("Answers", "Predefined events tracking disabled - skipping event: " + a2);
        } else if (this.f6348k.a(a2)) {
            h.a.a.a.l f4 = c.f();
            f4.d("Answers", "Skipping filtered event: " + a2);
        } else {
            try {
                this.f6341d.a(a2);
            } catch (IOException e2) {
                h.a.a.a.l f5 = c.f();
                f5.b("Answers", "Failed to write event: " + a2, e2);
            }
            e();
            boolean z = a0.c.CUSTOM.equals(a2.f6266c) || a0.c.PREDEFINED.equals(a2.f6266c);
            boolean equals = "purchase".equals(a2.f6270g);
            if (this.o && z) {
                if (!equals || this.p) {
                    try {
                        this.f6345h.a(a2);
                    } catch (Exception e3) {
                        h.a.a.a.l f6 = c.f();
                        f6.b("Answers", "Failed to map event to Firebase: " + a2, e3);
                    }
                }
            }
        }
    }

    public void a() {
        if (this.f6346i == null) {
            h.a.a.a.n.b.i.c(this.f6340c, "skipping files send because we don't yet know the target endpoint");
            return;
        }
        h.a.a.a.n.b.i.c(this.f6340c, "Sending all files");
        List<File> d2 = this.f6341d.d();
        int i2 = 0;
        while (true) {
            try {
                if (d2.size() <= 0) {
                    break;
                }
                h.a.a.a.n.b.i.c(this.f6340c, String.format(Locale.US, "attempt to send batch of %d files", Integer.valueOf(d2.size())));
                boolean a2 = this.f6346i.a(d2);
                if (a2) {
                    i2 += d2.size();
                    this.f6341d.a(d2);
                }
                if (!a2) {
                    break;
                }
                d2 = this.f6341d.d();
            } catch (Exception e2) {
                Context context = this.f6340c;
                h.a.a.a.n.b.i.a(context, "Failed to send batch of analytics files to server: " + e2.getMessage(), e2);
            }
        }
        if (i2 == 0) {
            this.f6341d.b();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j2, long j3) {
        if (this.f6343f.get() == null) {
            h.a.a.a.n.d.i iVar = new h.a.a.a.n.d.i(this.f6340c, this);
            Context context = this.f6340c;
            h.a.a.a.n.b.i.c(context, "Scheduling time based file roll over every " + j3 + " seconds");
            try {
                this.f6343f.set(this.f6342e.scheduleAtFixedRate(iVar, j2, j3, TimeUnit.SECONDS));
            } catch (RejectedExecutionException e2) {
                h.a.a.a.n.b.i.a(this.f6340c, "Failed to schedule time based file roll over", e2);
            }
        }
    }
}
