package com.crashlytics.android.c;

import android.os.Bundle;

/* compiled from: FirebaseAnalyticsEvent */
public class p {

    /* renamed from: a  reason: collision with root package name */
    private final String f6352a;

    /* renamed from: b  reason: collision with root package name */
    private final Bundle f6353b;

    p(String str, Bundle bundle) {
        this.f6352a = str;
        this.f6353b = bundle;
    }

    public String a() {
        return this.f6352a;
    }

    public Bundle b() {
        return this.f6353b;
    }
}
