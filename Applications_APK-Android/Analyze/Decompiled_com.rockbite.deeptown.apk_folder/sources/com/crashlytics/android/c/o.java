package com.crashlytics.android.c;

import android.content.Context;
import com.google.firebase.analytics.FirebaseAnalytics;
import h.a.a.a.c;
import h.a.a.a.l;

/* compiled from: FirebaseAnalyticsApiAdapter */
class o {

    /* renamed from: a  reason: collision with root package name */
    private final Context f6349a;

    /* renamed from: b  reason: collision with root package name */
    private final q f6350b;

    /* renamed from: c  reason: collision with root package name */
    private n f6351c;

    public o(Context context) {
        this(context, new q());
    }

    public n a() {
        if (this.f6351c == null) {
            this.f6351c = i.b(this.f6349a);
        }
        return this.f6351c;
    }

    public o(Context context, q qVar) {
        this.f6349a = context;
        this.f6350b = qVar;
    }

    public void a(a0 a0Var) {
        n a2 = a();
        if (a2 == null) {
            c.f().d("Answers", "Firebase analytics logging was enabled, but not available...");
            return;
        }
        p a3 = this.f6350b.a(a0Var);
        if (a3 == null) {
            l f2 = c.f();
            f2.d("Answers", "Fabric event was not mappable to Firebase event: " + a0Var);
            return;
        }
        a2.a(a3.a(), a3.b());
        if ("levelEnd".equals(a0Var.f6270g)) {
            a2.a(FirebaseAnalytics.Event.POST_SCORE, a3.b());
        }
    }
}
