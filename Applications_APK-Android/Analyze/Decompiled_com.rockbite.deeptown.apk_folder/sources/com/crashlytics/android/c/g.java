package com.crashlytics.android.c;

import android.annotation.SuppressLint;
import android.content.Context;
import h.a.a.a.n.f.c;
import h.a.a.a.n.f.d;

/* compiled from: AnswersPreferenceManager */
class g {

    /* renamed from: a  reason: collision with root package name */
    private final c f6327a;

    g(c cVar) {
        this.f6327a = cVar;
    }

    public static g a(Context context) {
        return new g(new d(context, "settings"));
    }

    @SuppressLint({"CommitPrefEdits"})
    public void b() {
        c cVar = this.f6327a;
        cVar.a(cVar.a().putBoolean("analytics_launched", true));
    }

    @SuppressLint({"CommitPrefEdits"})
    public boolean a() {
        return this.f6327a.get().getBoolean("analytics_launched", false);
    }
}
