package com.crashlytics.android.c;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import com.crashlytics.android.c.a0;
import com.crashlytics.android.c.j;
import h.a.a.a.a;
import h.a.a.a.c;
import h.a.a.a.i;
import h.a.a.a.l;
import h.a.a.a.n.b.o;
import h.a.a.a.n.b.s;
import h.a.a.a.n.f.b;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: SessionAnalyticsManager */
class y implements j.b {

    /* renamed from: a  reason: collision with root package name */
    private final long f6364a;

    /* renamed from: b  reason: collision with root package name */
    final d f6365b;

    /* renamed from: c  reason: collision with root package name */
    final a f6366c;

    /* renamed from: d  reason: collision with root package name */
    final j f6367d;

    /* renamed from: e  reason: collision with root package name */
    final g f6368e;

    y(d dVar, a aVar, j jVar, g gVar, long j2) {
        this.f6365b = dVar;
        this.f6366c = aVar;
        this.f6367d = jVar;
        this.f6368e = gVar;
        this.f6364a = j2;
    }

    public static y a(i iVar, Context context, s sVar, String str, String str2, long j2) {
        d0 d0Var = new d0(context, sVar, str, str2);
        e eVar = new e(context, new b(iVar));
        h.a.a.a.n.e.b bVar = new h.a.a.a.n.e.b(c.f());
        a aVar = new a(context);
        ScheduledExecutorService b2 = o.b("Answers Events Handler");
        j jVar = new j(b2);
        return new y(new d(iVar, context, eVar, d0Var, bVar, b2, new o(context)), aVar, jVar, g.a(context), j2);
    }

    public void a(String str) {
    }

    public void b() {
        this.f6366c.a();
        this.f6365b.a();
    }

    public void c() {
        this.f6365b.b();
        this.f6366c.a(new f(this, this.f6367d));
        this.f6367d.a(this);
        if (d()) {
            a(this.f6364a);
            this.f6368e.b();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return !this.f6368e.a();
    }

    public void a(String str, String str2) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            c.f().d("Answers", "Logged crash");
            this.f6365b.c(a0.a(str, str2));
            return;
        }
        throw new IllegalStateException("onCrash called from main thread!!!");
    }

    public void a(long j2) {
        c.f().d("Answers", "Logged install");
        this.f6365b.b(a0.a(j2));
    }

    public void a(Activity activity, a0.c cVar) {
        l f2 = c.f();
        f2.d("Answers", "Logged lifecycle event: " + cVar.name());
        this.f6365b.a(a0.a(cVar, activity));
    }

    public void a() {
        c.f().d("Answers", "Flush events when app is backgrounded");
        this.f6365b.c();
    }

    public void a(h.a.a.a.n.g.b bVar, String str) {
        this.f6367d.a(bVar.f15261i);
        this.f6365b.a(bVar, str);
    }
}
