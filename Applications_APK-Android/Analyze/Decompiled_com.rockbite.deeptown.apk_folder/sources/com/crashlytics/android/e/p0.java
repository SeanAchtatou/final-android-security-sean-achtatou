package com.crashlytics.android.e;

import com.esotericsoftware.spine.Animation;
import h.a.a.a.l;
import h.a.a.a.n.b.h;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/* compiled from: ReportUploader */
class p0 {

    /* renamed from: g  reason: collision with root package name */
    static final Map<String, String> f6537g = Collections.singletonMap("X-CRASHLYTICS-INVALID-SESSION", "1");
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public static final short[] f6538h = {10, 20, 30, 60, 120, 300};

    /* renamed from: a  reason: collision with root package name */
    private final Object f6539a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private final t f6540b;

    /* renamed from: c  reason: collision with root package name */
    private final String f6541c;

    /* renamed from: d  reason: collision with root package name */
    private final c f6542d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public final b f6543e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public Thread f6544f;

    /* compiled from: ReportUploader */
    static final class a implements d {
        a() {
        }

        public boolean a() {
            return true;
        }
    }

    /* compiled from: ReportUploader */
    interface b {
        boolean a();
    }

    /* compiled from: ReportUploader */
    interface c {
        File[] a();

        File[] b();

        File[] c();
    }

    /* compiled from: ReportUploader */
    interface d {
        boolean a();
    }

    /* compiled from: ReportUploader */
    private class e extends h {

        /* renamed from: a  reason: collision with root package name */
        private final float f6545a;

        /* renamed from: b  reason: collision with root package name */
        private final d f6546b;

        e(float f2, d dVar) {
            this.f6545a = f2;
            this.f6546b = dVar;
        }

        private void b() {
            l f2 = h.a.a.a.c.f();
            f2.d("CrashlyticsCore", "Starting report processing in " + this.f6545a + " second(s)...");
            float f3 = this.f6545a;
            if (f3 > Animation.CurveTimeline.LINEAR) {
                try {
                    Thread.sleep((long) (f3 * 1000.0f));
                } catch (InterruptedException unused) {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
            List<o0> a2 = p0.this.a();
            if (!p0.this.f6543e.a()) {
                if (a2.isEmpty() || this.f6546b.a()) {
                    int i2 = 0;
                    while (!a2.isEmpty() && !p0.this.f6543e.a()) {
                        l f4 = h.a.a.a.c.f();
                        f4.d("CrashlyticsCore", "Attempting to send " + a2.size() + " report(s)");
                        for (o0 a3 : a2) {
                            p0.this.a(a3);
                        }
                        a2 = p0.this.a();
                        if (!a2.isEmpty()) {
                            int i3 = i2 + 1;
                            long j2 = (long) p0.f6538h[Math.min(i2, p0.f6538h.length - 1)];
                            l f5 = h.a.a.a.c.f();
                            f5.d("CrashlyticsCore", "Report submisson: scheduling delayed retry in " + j2 + " seconds");
                            try {
                                Thread.sleep(j2 * 1000);
                                i2 = i3;
                            } catch (InterruptedException unused2) {
                                Thread.currentThread().interrupt();
                                return;
                            }
                        }
                    }
                    return;
                }
                l f6 = h.a.a.a.c.f();
                f6.d("CrashlyticsCore", "User declined to send. Removing " + a2.size() + " Report(s).");
                for (o0 remove : a2) {
                    remove.remove();
                }
            }
        }

        public void a() {
            try {
                b();
            } catch (Exception e2) {
                h.a.a.a.c.f().b("CrashlyticsCore", "An unexpected error occurred while attempting to upload crash reports.", e2);
            }
            Thread unused = p0.this.f6544f = (Thread) null;
        }
    }

    public p0(String str, t tVar, c cVar, b bVar) {
        if (tVar != null) {
            this.f6540b = tVar;
            this.f6541c = str;
            this.f6542d = cVar;
            this.f6543e = bVar;
            return;
        }
        throw new IllegalArgumentException("createReportCall must not be null.");
    }

    public synchronized void a(float f2, d dVar) {
        if (this.f6544f != null) {
            h.a.a.a.c.f().d("CrashlyticsCore", "Report upload has already been started.");
            return;
        }
        this.f6544f = new Thread(new e(f2, dVar), "Crashlytics Report Uploader");
        this.f6544f.start();
    }

    /* access modifiers changed from: package-private */
    public boolean a(o0 o0Var) {
        boolean z;
        synchronized (this.f6539a) {
            z = false;
            try {
                boolean a2 = this.f6540b.a(new s(this.f6541c, o0Var));
                l f2 = h.a.a.a.c.f();
                StringBuilder sb = new StringBuilder();
                sb.append("Crashlytics report upload ");
                sb.append(a2 ? "complete: " : "FAILED: ");
                sb.append(o0Var.b());
                f2.e("CrashlyticsCore", sb.toString());
                if (a2) {
                    o0Var.remove();
                    z = true;
                }
            } catch (Exception e2) {
                l f3 = h.a.a.a.c.f();
                f3.b("CrashlyticsCore", "Error occurred sending report " + o0Var, e2);
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public List<o0> a() {
        File[] c2;
        File[] b2;
        File[] a2;
        h.a.a.a.c.f().d("CrashlyticsCore", "Checking for crash reports...");
        synchronized (this.f6539a) {
            c2 = this.f6542d.c();
            b2 = this.f6542d.b();
            a2 = this.f6542d.a();
        }
        LinkedList linkedList = new LinkedList();
        if (c2 != null) {
            for (File file : c2) {
                h.a.a.a.c.f().d("CrashlyticsCore", "Found crash report " + file.getPath());
                linkedList.add(new s0(file));
            }
        }
        HashMap hashMap = new HashMap();
        if (b2 != null) {
            for (File file2 : b2) {
                String a3 = k.a(file2);
                if (!hashMap.containsKey(a3)) {
                    hashMap.put(a3, new LinkedList());
                }
                ((List) hashMap.get(a3)).add(file2);
            }
        }
        for (String str : hashMap.keySet()) {
            h.a.a.a.c.f().d("CrashlyticsCore", "Found invalid session: " + str);
            List list = (List) hashMap.get(str);
            linkedList.add(new z(str, (File[]) list.toArray(new File[list.size()])));
        }
        if (a2 != null) {
            for (File h0Var : a2) {
                linkedList.add(new h0(h0Var));
            }
        }
        if (linkedList.isEmpty()) {
            h.a.a.a.c.f().d("CrashlyticsCore", "No reports found.");
        }
        return linkedList;
    }
}
