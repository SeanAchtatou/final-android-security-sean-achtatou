package com.crashlytics.android.e;

/* compiled from: TrimmedThrowableData */
class v0 {

    /* renamed from: a  reason: collision with root package name */
    public final String f6569a;

    /* renamed from: b  reason: collision with root package name */
    public final String f6570b;

    /* renamed from: c  reason: collision with root package name */
    public final StackTraceElement[] f6571c;

    /* renamed from: d  reason: collision with root package name */
    public final v0 f6572d;

    public v0(Throwable th, u0 u0Var) {
        this.f6569a = th.getLocalizedMessage();
        this.f6570b = th.getClass().getName();
        this.f6571c = u0Var.a(th.getStackTrace());
        Throwable cause = th.getCause();
        this.f6572d = cause != null ? new v0(cause, u0Var) : null;
    }
}
