package com.crashlytics.android.e;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/* compiled from: ByteString */
final class d {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f6386a;

    /* renamed from: b  reason: collision with root package name */
    private volatile int f6387b = 0;

    static {
        new d(new byte[0]);
    }

    private d(byte[] bArr) {
        this.f6386a = bArr;
    }

    public static d a(byte[] bArr, int i2, int i3) {
        byte[] bArr2 = new byte[i3];
        System.arraycopy(bArr, i2, bArr2, 0, i3);
        return new d(bArr2);
    }

    public int b() {
        return this.f6386a.length;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof d)) {
            return false;
        }
        byte[] bArr = this.f6386a;
        int length = bArr.length;
        byte[] bArr2 = ((d) obj).f6386a;
        if (length != bArr2.length) {
            return false;
        }
        for (int i2 = 0; i2 < length; i2++) {
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i2 = this.f6387b;
        if (i2 == 0) {
            int i3 = r1;
            for (byte b2 : this.f6386a) {
                i3 = (i3 * 31) + b2;
            }
            i2 = i3 == 0 ? 1 : i3;
            this.f6387b = i2;
        }
        return i2;
    }

    public static d a(String str) {
        try {
            return new d(str.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException("UTF-8 not supported.", e2);
        }
    }

    public void a(byte[] bArr, int i2, int i3, int i4) {
        System.arraycopy(this.f6386a, i2, bArr, i3, i4);
    }

    public InputStream a() {
        return new ByteArrayInputStream(this.f6386a);
    }
}
