package com.crashlytics.android.e;

import android.annotation.SuppressLint;
import h.a.a.a.n.f.c;
import h.a.a.a.n.f.d;

@SuppressLint({"CommitPrefEdits"})
/* compiled from: PreferenceManager */
class j0 {

    /* renamed from: a  reason: collision with root package name */
    private final c f6417a;

    /* renamed from: b  reason: collision with root package name */
    private final l f6418b;

    public j0(c cVar, l lVar) {
        this.f6417a = cVar;
        this.f6418b = lVar;
    }

    public static j0 a(c cVar, l lVar) {
        return new j0(cVar, lVar);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        c cVar = this.f6417a;
        cVar.a(cVar.a().putBoolean("always_send_reports_opt_in", z));
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        if (!this.f6417a.get().contains("preferences_migration_complete")) {
            d dVar = new d(this.f6418b);
            if (!this.f6417a.get().contains("always_send_reports_opt_in") && dVar.get().contains("always_send_reports_opt_in")) {
                boolean z = dVar.get().getBoolean("always_send_reports_opt_in", false);
                c cVar = this.f6417a;
                cVar.a(cVar.a().putBoolean("always_send_reports_opt_in", z));
            }
            c cVar2 = this.f6417a;
            cVar2.a(cVar2.a().putBoolean("preferences_migration_complete", true));
        }
        return this.f6417a.get().getBoolean("always_send_reports_opt_in", false);
    }
}
