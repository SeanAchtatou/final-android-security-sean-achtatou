package com.crashlytics.android.e;

import h.a.a.a.c;
import h.a.a.a.l;
import h.a.a.a.n.f.a;
import java.io.File;
import java.io.IOException;

/* compiled from: CrashlyticsFileMarker */
class m {

    /* renamed from: a  reason: collision with root package name */
    private final String f6523a;

    /* renamed from: b  reason: collision with root package name */
    private final a f6524b;

    public m(String str, a aVar) {
        this.f6523a = str;
        this.f6524b = aVar;
    }

    private File d() {
        return new File(this.f6524b.a(), this.f6523a);
    }

    public boolean a() {
        try {
            return d().createNewFile();
        } catch (IOException e2) {
            l f2 = c.f();
            f2.b("CrashlyticsCore", "Error creating marker: " + this.f6523a, e2);
            return false;
        }
    }

    public boolean b() {
        return d().exists();
    }

    public boolean c() {
        return d().delete();
    }
}
