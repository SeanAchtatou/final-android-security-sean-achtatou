package com.crashlytics.android.e;

import com.crashlytics.android.e.c;
import h.a.a.a.n.b.i;
import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: Sha1FileIdStrategy */
class t0 implements c.a {
    t0() {
    }

    public String a(File file) throws IOException {
        return a(file.getPath());
    }

    private static String a(String str) throws IOException {
        BufferedInputStream bufferedInputStream = null;
        try {
            BufferedInputStream bufferedInputStream2 = new BufferedInputStream(new FileInputStream(str));
            try {
                String a2 = i.a((InputStream) bufferedInputStream2);
                i.a((Closeable) bufferedInputStream2);
                return a2;
            } catch (Throwable th) {
                th = th;
                bufferedInputStream = bufferedInputStream2;
                i.a((Closeable) bufferedInputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            i.a((Closeable) bufferedInputStream);
            throw th;
        }
    }
}
