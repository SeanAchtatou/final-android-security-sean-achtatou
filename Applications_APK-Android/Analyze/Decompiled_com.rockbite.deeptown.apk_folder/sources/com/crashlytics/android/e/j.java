package com.crashlytics.android.e;

import android.os.Looper;
import h.a.a.a.c;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

/* compiled from: CrashlyticsBackgroundWorker */
class j {

    /* renamed from: a  reason: collision with root package name */
    private final ExecutorService f6414a;

    /* compiled from: CrashlyticsBackgroundWorker */
    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Runnable f6415a;

        a(j jVar, Runnable runnable) {
            this.f6415a = runnable;
        }

        public void run() {
            try {
                this.f6415a.run();
            } catch (Exception e2) {
                c.f().b("CrashlyticsCore", "Failed to execute task.", e2);
            }
        }
    }

    /* compiled from: CrashlyticsBackgroundWorker */
    class b implements Callable<T> {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Callable f6416a;

        b(j jVar, Callable callable) {
            this.f6416a = callable;
        }

        public T call() throws Exception {
            try {
                return this.f6416a.call();
            } catch (Exception e2) {
                c.f().b("CrashlyticsCore", "Failed to execute task.", e2);
                return null;
            }
        }
    }

    public j(ExecutorService executorService) {
        this.f6414a = executorService;
    }

    /* access modifiers changed from: package-private */
    public Future<?> a(Runnable runnable) {
        try {
            return this.f6414a.submit(new a(this, runnable));
        } catch (RejectedExecutionException unused) {
            c.f().d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public <T> T b(Callable<T> callable) {
        try {
            if (Looper.getMainLooper() == Looper.myLooper()) {
                return this.f6414a.submit(callable).get(4, TimeUnit.SECONDS);
            }
            return this.f6414a.submit(callable).get();
        } catch (RejectedExecutionException unused) {
            c.f().d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        } catch (Exception e2) {
            c.f().b("CrashlyticsCore", "Failed to execute task.", e2);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public <T> Future<T> a(Callable callable) {
        try {
            return this.f6414a.submit(new b(this, callable));
        } catch (RejectedExecutionException unused) {
            c.f().d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        }
    }
}
