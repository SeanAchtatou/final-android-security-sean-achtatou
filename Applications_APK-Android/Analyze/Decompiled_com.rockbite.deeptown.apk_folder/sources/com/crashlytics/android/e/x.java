package com.crashlytics.android.e;

import android.content.Context;
import h.a.a.a.n.b.i;
import h.a.a.a.n.g.p;

/* compiled from: DialogStringResolver */
class x {

    /* renamed from: a  reason: collision with root package name */
    private final Context f6583a;

    /* renamed from: b  reason: collision with root package name */
    private final p f6584b;

    public x(Context context, p pVar) {
        this.f6583a = context;
        this.f6584b = pVar;
    }

    public String a() {
        return a("com.crashlytics.CrashSubmissionAlwaysSendTitle", this.f6584b.f15302g);
    }

    public String b() {
        return a("com.crashlytics.CrashSubmissionCancelTitle", this.f6584b.f15300e);
    }

    public String c() {
        return a("com.crashlytics.CrashSubmissionPromptMessage", this.f6584b.f15297b);
    }

    public String d() {
        return a("com.crashlytics.CrashSubmissionSendTitle", this.f6584b.f15298c);
    }

    public String e() {
        return a("com.crashlytics.CrashSubmissionPromptTitle", this.f6584b.f15296a);
    }

    private String a(String str, String str2) {
        return b(i.b(this.f6583a, str), str2);
    }

    private String b(String str, String str2) {
        return a(str) ? str2 : str;
    }

    private boolean a(String str) {
        return str == null || str.length() == 0;
    }
}
