package com.crashlytics.android.e;

import android.content.Context;
import h.a.a.a.n.b.i;

/* compiled from: ResourceUnityVersionProvider */
class q0 implements w0 {

    /* renamed from: a  reason: collision with root package name */
    private final Context f6549a;

    /* renamed from: b  reason: collision with root package name */
    private final w0 f6550b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f6551c = false;

    /* renamed from: d  reason: collision with root package name */
    private String f6552d;

    public q0(Context context, w0 w0Var) {
        this.f6549a = context;
        this.f6550b = w0Var;
    }

    public String a() {
        if (!this.f6551c) {
            this.f6552d = i.o(this.f6549a);
            this.f6551c = true;
        }
        String str = this.f6552d;
        if (str != null) {
            return str;
        }
        w0 w0Var = this.f6550b;
        if (w0Var != null) {
            return w0Var.a();
        }
        return null;
    }
}
