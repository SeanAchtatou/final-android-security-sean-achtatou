package com.crashlytics.android.e;

import com.underwater.demolisher.data.vo.RemoteConfigConst;
import h.a.a.a.c;
import h.a.a.a.l;
import h.a.a.a.n.b.i;
import h.a.a.a.n.b.u;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

/* compiled from: QueueFileLogStore */
class m0 implements y {

    /* renamed from: a  reason: collision with root package name */
    private final File f6525a;

    /* renamed from: b  reason: collision with root package name */
    private final int f6526b;

    /* renamed from: c  reason: collision with root package name */
    private u f6527c;

    /* compiled from: QueueFileLogStore */
    class a implements u.d {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ byte[] f6528a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ int[] f6529b;

        a(m0 m0Var, byte[] bArr, int[] iArr) {
            this.f6528a = bArr;
            this.f6529b = iArr;
        }

        public void a(InputStream inputStream, int i2) throws IOException {
            try {
                inputStream.read(this.f6528a, this.f6529b[0], i2);
                int[] iArr = this.f6529b;
                iArr[0] = iArr[0] + i2;
            } finally {
                inputStream.close();
            }
        }
    }

    /* compiled from: QueueFileLogStore */
    public class b {

        /* renamed from: a  reason: collision with root package name */
        public final byte[] f6530a;

        /* renamed from: b  reason: collision with root package name */
        public final int f6531b;

        public b(m0 m0Var, byte[] bArr, int i2) {
            this.f6530a = bArr;
            this.f6531b = i2;
        }
    }

    public m0(File file, int i2) {
        this.f6525a = file;
        this.f6526b = i2;
    }

    private b e() {
        if (!this.f6525a.exists()) {
            return null;
        }
        f();
        u uVar = this.f6527c;
        if (uVar == null) {
            return null;
        }
        int[] iArr = {0};
        byte[] bArr = new byte[uVar.R()];
        try {
            this.f6527c.a(new a(this, bArr, iArr));
        } catch (IOException e2) {
            c.f().b("CrashlyticsCore", "A problem occurred while reading the Crashlytics log file.", e2);
        }
        return new b(this, bArr, iArr[0]);
    }

    private void f() {
        if (this.f6527c == null) {
            try {
                this.f6527c = new u(this.f6525a);
            } catch (IOException e2) {
                l f2 = c.f();
                f2.b("CrashlyticsCore", "Could not open log file: " + this.f6525a, e2);
            }
        }
    }

    public void a(long j2, String str) {
        f();
        b(j2, str);
    }

    public d b() {
        b e2 = e();
        if (e2 == null) {
            return null;
        }
        return d.a(e2.f6530a, 0, e2.f6531b);
    }

    public byte[] c() {
        b e2 = e();
        if (e2 == null) {
            return null;
        }
        return e2.f6530a;
    }

    public void d() {
        a();
        this.f6525a.delete();
    }

    private void b(long j2, String str) {
        if (this.f6527c != null) {
            if (str == null) {
                str = "null";
            }
            try {
                int i2 = this.f6526b / 4;
                if (str.length() > i2) {
                    str = "..." + str.substring(str.length() - i2);
                }
                this.f6527c.a(String.format(Locale.US, "%d %s%n", Long.valueOf(j2), str.replaceAll("\r", RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER).replaceAll("\n", RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER)).getBytes("UTF-8"));
                while (!this.f6527c.P() && this.f6527c.R() > this.f6526b) {
                    this.f6527c.Q();
                }
            } catch (IOException e2) {
                c.f().b("CrashlyticsCore", "There was a problem writing to the Crashlytics log.", e2);
            }
        }
    }

    public void a() {
        i.a(this.f6527c, "There was a problem closing the Crashlytics log file.");
        this.f6527c = null;
    }
}
