package com.crashlytics.android.e;

import com.tapjoy.TapjoyConstants;
import h.a.a.a.i;
import h.a.a.a.l;
import h.a.a.a.n.b.a;
import h.a.a.a.n.b.v;
import h.a.a.a.n.e.c;
import h.a.a.a.n.e.d;
import h.a.a.a.n.e.e;
import java.io.File;

/* compiled from: NativeCreateReportSpiCall */
class f0 extends a implements t {
    public f0(i iVar, String str, String str2, e eVar) {
        super(iVar, str, str2, eVar, c.POST);
    }

    public boolean a(s sVar) {
        d a2 = a();
        a(a2, sVar.f6560a);
        a(a2, sVar.f6561b);
        l f2 = h.a.a.a.c.f();
        f2.d("CrashlyticsCore", "Sending report to: " + b());
        int g2 = a2.g();
        l f3 = h.a.a.a.c.f();
        f3.d("CrashlyticsCore", "Result was: " + g2);
        return v.a(g2) == 0;
    }

    private d a(d dVar, String str) {
        dVar.c("User-Agent", "Crashlytics Android SDK/" + this.f15055e.j());
        dVar.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        dVar.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.f15055e.j());
        dVar.c("X-CRASHLYTICS-API-KEY", str);
        return dVar;
    }

    private d a(d dVar, o0 o0Var) {
        dVar.e("report_id", o0Var.b());
        for (File file : o0Var.d()) {
            if (file.getName().equals("minidump")) {
                dVar.a("minidump_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("metadata")) {
                dVar.a("crash_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("binaryImages")) {
                dVar.a("binary_images_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("session")) {
                dVar.a("session_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals(TapjoyConstants.TJC_APP_PLACEMENT)) {
                dVar.a("app_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals(TapjoyConstants.TJC_NOTIFICATION_DEVICE_PREFIX)) {
                dVar.a("device_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("os")) {
                dVar.a("os_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("user")) {
                dVar.a("user_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("logs")) {
                dVar.a("logs_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("keys")) {
                dVar.a("keys_file", file.getName(), "application/octet-stream", file);
            }
        }
        return dVar;
    }
}
