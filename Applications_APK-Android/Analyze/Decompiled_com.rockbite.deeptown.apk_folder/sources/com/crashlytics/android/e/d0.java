package com.crashlytics.android.e;

/* compiled from: MiddleOutFallbackStrategy */
class d0 implements u0 {

    /* renamed from: a  reason: collision with root package name */
    private final int f6388a;

    /* renamed from: b  reason: collision with root package name */
    private final u0[] f6389b;

    /* renamed from: c  reason: collision with root package name */
    private final e0 f6390c;

    public d0(int i2, u0... u0VarArr) {
        this.f6388a = i2;
        this.f6389b = u0VarArr;
        this.f6390c = new e0(i2);
    }

    public StackTraceElement[] a(StackTraceElement[] stackTraceElementArr) {
        if (stackTraceElementArr.length <= this.f6388a) {
            return stackTraceElementArr;
        }
        StackTraceElement[] stackTraceElementArr2 = stackTraceElementArr;
        for (u0 u0Var : this.f6389b) {
            if (stackTraceElementArr2.length <= this.f6388a) {
                break;
            }
            stackTraceElementArr2 = u0Var.a(stackTraceElementArr);
        }
        return stackTraceElementArr2.length > this.f6388a ? this.f6390c.a(stackTraceElementArr2) : stackTraceElementArr2;
    }
}
