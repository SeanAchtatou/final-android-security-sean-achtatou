package com.crashlytics.android.e;

import h.a.a.a.c;
import h.a.a.a.n.g.u;
import java.lang.Thread;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: CrashlyticsUncaughtExceptionHandler */
class r implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private final a f6553a;

    /* renamed from: b  reason: collision with root package name */
    private final b f6554b;

    /* renamed from: c  reason: collision with root package name */
    private final boolean f6555c;

    /* renamed from: d  reason: collision with root package name */
    private final Thread.UncaughtExceptionHandler f6556d;

    /* renamed from: e  reason: collision with root package name */
    private final AtomicBoolean f6557e = new AtomicBoolean(false);

    /* compiled from: CrashlyticsUncaughtExceptionHandler */
    interface a {
        void a(b bVar, Thread thread, Throwable th, boolean z);
    }

    /* compiled from: CrashlyticsUncaughtExceptionHandler */
    interface b {
        u a();
    }

    public r(a aVar, b bVar, boolean z, Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.f6553a = aVar;
        this.f6554b = bVar;
        this.f6555c = z;
        this.f6556d = uncaughtExceptionHandler;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.f6557e.get();
    }

    public void uncaughtException(Thread thread, Throwable th) {
        this.f6557e.set(true);
        try {
            this.f6553a.a(this.f6554b, thread, th, this.f6555c);
        } catch (Exception e2) {
            c.f().b("CrashlyticsCore", "An error occurred in the uncaught exception handler", e2);
        } catch (Throwable th2) {
            c.f().d("CrashlyticsCore", "Crashlytics completed exception processing. Invoking default exception handler.");
            this.f6556d.uncaughtException(thread, th);
            this.f6557e.set(false);
            throw th2;
        }
        c.f().d("CrashlyticsCore", "Crashlytics completed exception processing. Invoking default exception handler.");
        this.f6556d.uncaughtException(thread, th);
        this.f6557e.set(false);
    }
}
