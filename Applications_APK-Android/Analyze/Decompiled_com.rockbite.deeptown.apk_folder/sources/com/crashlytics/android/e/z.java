package com.crashlytics.android.e;

import com.crashlytics.android.e.o0;
import h.a.a.a.c;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: InvalidSessionReport */
class z implements o0 {

    /* renamed from: a  reason: collision with root package name */
    private final File[] f6590a;

    /* renamed from: b  reason: collision with root package name */
    private final Map<String, String> f6591b = new HashMap(p0.f6537g);

    /* renamed from: c  reason: collision with root package name */
    private final String f6592c;

    public z(String str, File[] fileArr) {
        this.f6590a = fileArr;
        this.f6592c = str;
    }

    public Map<String, String> a() {
        return Collections.unmodifiableMap(this.f6591b);
    }

    public String b() {
        return this.f6592c;
    }

    public File c() {
        return this.f6590a[0];
    }

    public File[] d() {
        return this.f6590a;
    }

    public String e() {
        return this.f6590a[0].getName();
    }

    public o0.a getType() {
        return o0.a.JAVA;
    }

    public void remove() {
        for (File file : this.f6590a) {
            c.f().d("CrashlyticsCore", "Removing invalid report file at " + file.getPath());
            file.delete();
        }
    }
}
