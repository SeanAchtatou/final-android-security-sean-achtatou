package com.crashlytics.android.e;

import h.a.a.a.c;
import h.a.a.a.l;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: ProcMapEntryParser */
final class l0 {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f6522a = Pattern.compile("\\s*(\\p{XDigit}+)-\\s*(\\p{XDigit}+)\\s+(.{4})\\s+\\p{XDigit}+\\s+.+\\s+\\d+\\s+(.*)");

    public static k0 a(String str) {
        Matcher matcher = f6522a.matcher(str);
        if (!matcher.matches()) {
            return null;
        }
        try {
            long longValue = Long.valueOf(matcher.group(1), 16).longValue();
            return new k0(longValue, Long.valueOf(matcher.group(2), 16).longValue() - longValue, matcher.group(3), matcher.group(4));
        } catch (Exception unused) {
            l f2 = c.f();
            f2.d("CrashlyticsCore", "Could not parse map entry: " + str);
            return null;
        }
    }
}
