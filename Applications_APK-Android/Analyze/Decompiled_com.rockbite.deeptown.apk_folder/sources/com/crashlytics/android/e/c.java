package com.crashlytics.android.e;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import h.a.a.a.l;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: BinaryImagesConverter */
class c {

    /* renamed from: a  reason: collision with root package name */
    private final Context f6381a;

    /* renamed from: b  reason: collision with root package name */
    private final a f6382b;

    /* compiled from: BinaryImagesConverter */
    interface a {
        String a(File file) throws IOException;
    }

    c(Context context, a aVar) {
        this.f6381a = context;
        this.f6382b = aVar;
    }

    private JSONArray b(BufferedReader bufferedReader) throws IOException {
        JSONArray jSONArray = new JSONArray();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return jSONArray;
            }
            JSONObject c2 = c(readLine);
            if (c2 != null) {
                jSONArray.put(c2);
            }
        }
    }

    private JSONObject c(String str) {
        k0 a2 = l0.a(str);
        if (a2 != null && a(a2)) {
            try {
                try {
                    return a(this.f6382b.a(b(a2.f6512d)), a2);
                } catch (JSONException e2) {
                    h.a.a.a.c.f().c("CrashlyticsCore", "Could not create a binary image json string", e2);
                    return null;
                }
            } catch (IOException e3) {
                l f2 = h.a.a.a.c.f();
                f2.c("CrashlyticsCore", "Could not generate ID for file " + a2.f6512d, e3);
            }
        }
        return null;
    }

    private JSONArray d(String str) {
        JSONArray jSONArray = new JSONArray();
        try {
            String[] split = b(new JSONObject(str).getJSONArray("maps")).split("\\|");
            for (String c2 : split) {
                JSONObject c3 = c(c2);
                if (c3 != null) {
                    jSONArray.put(c3);
                }
            }
            return jSONArray;
        } catch (JSONException e2) {
            h.a.a.a.c.f().a("CrashlyticsCore", "Unable to parse proc maps string", e2);
            return jSONArray;
        }
    }

    /* access modifiers changed from: package-private */
    public byte[] a(String str) throws IOException {
        return a(d(str));
    }

    /* access modifiers changed from: package-private */
    public byte[] a(BufferedReader bufferedReader) throws IOException {
        return a(b(bufferedReader));
    }

    private File a(File file) {
        if (Build.VERSION.SDK_INT < 9 || !file.getAbsolutePath().startsWith("/data")) {
            return file;
        }
        try {
            return new File(this.f6381a.getPackageManager().getApplicationInfo(this.f6381a.getPackageName(), 0).nativeLibraryDir, file.getName());
        } catch (PackageManager.NameNotFoundException e2) {
            h.a.a.a.c.f().b("CrashlyticsCore", "Error getting ApplicationInfo", e2);
            return file;
        }
    }

    private File b(String str) {
        File file = new File(str);
        return !file.exists() ? a(file) : file;
    }

    private static String b(JSONArray jSONArray) throws JSONException {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            sb.append(jSONArray.getString(i2));
        }
        return sb.toString();
    }

    private static byte[] a(JSONArray jSONArray) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("binary_images", jSONArray);
            return jSONObject.toString().getBytes();
        } catch (JSONException e2) {
            h.a.a.a.c.f().a("CrashlyticsCore", "Binary images string is null", e2);
            return new byte[0];
        }
    }

    private static JSONObject a(String str, k0 k0Var) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("base_address", k0Var.f6509a);
        jSONObject.put("size", k0Var.f6510b);
        jSONObject.put("name", k0Var.f6512d);
        jSONObject.put("uuid", str);
        return jSONObject;
    }

    private static boolean a(k0 k0Var) {
        return (k0Var.f6511c.indexOf(120) == -1 || k0Var.f6512d.indexOf(47) == -1) ? false : true;
    }
}
