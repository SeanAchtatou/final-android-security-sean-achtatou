package com.crashlytics.android.e;

/* compiled from: ProcMapEntry */
class k0 {

    /* renamed from: a  reason: collision with root package name */
    public final long f6509a;

    /* renamed from: b  reason: collision with root package name */
    public final long f6510b;

    /* renamed from: c  reason: collision with root package name */
    public final String f6511c;

    /* renamed from: d  reason: collision with root package name */
    public final String f6512d;

    public k0(long j2, long j3, String str, String str2) {
        this.f6509a = j2;
        this.f6510b = j3;
        this.f6511c = str;
        this.f6512d = str2;
    }
}
