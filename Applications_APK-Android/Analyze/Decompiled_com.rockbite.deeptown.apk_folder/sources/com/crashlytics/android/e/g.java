package com.crashlytics.android.e;

import com.google.android.gms.games.Notifications;
import com.google.protobuf.CodedOutputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* compiled from: CodedOutputStream */
final class g implements Flushable {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f6398a;

    /* renamed from: b  reason: collision with root package name */
    private final int f6399b;

    /* renamed from: c  reason: collision with root package name */
    private int f6400c = 0;

    /* renamed from: d  reason: collision with root package name */
    private final OutputStream f6401d;

    /* compiled from: CodedOutputStream */
    static class a extends IOException {
        a() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }
    }

    private g(OutputStream outputStream, byte[] bArr) {
        this.f6401d = outputStream;
        this.f6398a = bArr;
        this.f6399b = bArr.length;
    }

    public static g a(OutputStream outputStream) {
        return a(outputStream, (int) CodedOutputStream.DEFAULT_BUFFER_SIZE);
    }

    public static int b(float f2) {
        return 4;
    }

    public static int b(boolean z) {
        return 1;
    }

    public static int c(long j2) {
        if ((-128 & j2) == 0) {
            return 1;
        }
        if ((-16384 & j2) == 0) {
            return 2;
        }
        if ((-2097152 & j2) == 0) {
            return 3;
        }
        if ((-268435456 & j2) == 0) {
            return 4;
        }
        if ((-34359738368L & j2) == 0) {
            return 5;
        }
        if ((-4398046511104L & j2) == 0) {
            return 6;
        }
        if ((-562949953421312L & j2) == 0) {
            return 7;
        }
        if ((-72057594037927936L & j2) == 0) {
            return 8;
        }
        return (j2 & Long.MIN_VALUE) == 0 ? 9 : 10;
    }

    public static int c(d dVar) {
        return l(dVar.b()) + dVar.b();
    }

    public static int e(int i2, int i3) {
        return n(i2) + j(i3);
    }

    public static int f(int i2, int i3) {
        return n(i2) + m(i3);
    }

    public static int g(int i2, int i3) {
        return n(i2) + o(i3);
    }

    public static int j(int i2) {
        return k(i2);
    }

    public static int k(int i2) {
        if (i2 >= 0) {
            return l(i2);
        }
        return 10;
    }

    public static int l(int i2) {
        if ((i2 & -128) == 0) {
            return 1;
        }
        if ((i2 & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i2) == 0) {
            return 3;
        }
        return (i2 & -268435456) == 0 ? 4 : 5;
    }

    public static int m(int i2) {
        return l(p(i2));
    }

    public static int n(int i2) {
        return l(z0.a(i2, 0));
    }

    public static int o(int i2) {
        return l(i2);
    }

    public static int p(int i2) {
        return (i2 >> 31) ^ (i2 << 1);
    }

    public void b(int i2, int i3) throws IOException {
        c(i2, 0);
        h(i3);
    }

    public void d(int i2, int i3) throws IOException {
        c(i2, 0);
        i(i3);
    }

    public void flush() throws IOException {
        if (this.f6401d != null) {
            b();
        }
    }

    public void h(int i2) throws IOException {
        g(p(i2));
    }

    public void i(int i2) throws IOException {
        g(i2);
    }

    public static g a(OutputStream outputStream, int i2) {
        return new g(outputStream, new byte[i2]);
    }

    public void f(int i2) throws IOException {
        c(i2 & 255);
        c((i2 >> 8) & 255);
        c((i2 >> 16) & 255);
        c((i2 >> 24) & 255);
    }

    public void g(int i2) throws IOException {
        while ((i2 & -128) != 0) {
            c((i2 & Notifications.NOTIFICATION_TYPES_ALL) | 128);
            i2 >>>= 7;
        }
        c(i2);
    }

    public static int d(long j2) {
        return c(j2);
    }

    public void a(int i2, float f2) throws IOException {
        c(i2, 5);
        a(f2);
    }

    public void b(long j2) throws IOException {
        a(j2);
    }

    public void c(int i2) throws IOException {
        a((byte) i2);
    }

    public void b(int i2) throws IOException {
        if (i2 >= 0) {
            g(i2);
        } else {
            a((long) i2);
        }
    }

    public void c(int i2, int i3) throws IOException {
        g(z0.a(i2, i3));
    }

    public void a(int i2, long j2) throws IOException {
        c(i2, 0);
        b(j2);
    }

    public static int b(int i2, float f2) {
        return n(i2) + b(f2);
    }

    public static int b(int i2, long j2) {
        return n(i2) + d(j2);
    }

    public void a(int i2, boolean z) throws IOException {
        c(i2, 0);
        a(z);
    }

    public static int b(int i2, boolean z) {
        return n(i2) + b(z);
    }

    public static int b(int i2, d dVar) {
        return n(i2) + c(dVar);
    }

    public void a(int i2, d dVar) throws IOException {
        c(i2, 2);
        a(dVar);
    }

    private void b() throws IOException {
        OutputStream outputStream = this.f6401d;
        if (outputStream != null) {
            outputStream.write(this.f6398a, 0, this.f6400c);
            this.f6400c = 0;
            return;
        }
        throw new a();
    }

    public void a(int i2, int i3) throws IOException {
        c(i2, 0);
        a(i3);
    }

    public void a(float f2) throws IOException {
        f(Float.floatToRawIntBits(f2));
    }

    public void a(boolean z) throws IOException {
        c(z ? 1 : 0);
    }

    public void b(d dVar) throws IOException {
        a(dVar, 0, dVar.b());
    }

    public void a(d dVar) throws IOException {
        g(dVar.b());
        b(dVar);
    }

    public void a(int i2) throws IOException {
        b(i2);
    }

    public void a(byte b2) throws IOException {
        if (this.f6400c == this.f6399b) {
            b();
        }
        byte[] bArr = this.f6398a;
        int i2 = this.f6400c;
        this.f6400c = i2 + 1;
        bArr[i2] = b2;
    }

    public void a(byte[] bArr) throws IOException {
        a(bArr, 0, bArr.length);
    }

    public void a(byte[] bArr, int i2, int i3) throws IOException {
        int i4 = this.f6399b;
        int i5 = this.f6400c;
        if (i4 - i5 >= i3) {
            System.arraycopy(bArr, i2, this.f6398a, i5, i3);
            this.f6400c += i3;
            return;
        }
        int i6 = i4 - i5;
        System.arraycopy(bArr, i2, this.f6398a, i5, i6);
        int i7 = i2 + i6;
        int i8 = i3 - i6;
        this.f6400c = this.f6399b;
        b();
        if (i8 <= this.f6399b) {
            System.arraycopy(bArr, i7, this.f6398a, 0, i8);
            this.f6400c = i8;
            return;
        }
        this.f6401d.write(bArr, i7, i8);
    }

    public void a(d dVar, int i2, int i3) throws IOException {
        int i4 = this.f6399b;
        int i5 = this.f6400c;
        if (i4 - i5 >= i3) {
            dVar.a(this.f6398a, i2, i5, i3);
            this.f6400c += i3;
            return;
        }
        int i6 = i4 - i5;
        dVar.a(this.f6398a, i2, i5, i6);
        int i7 = i2 + i6;
        int i8 = i3 - i6;
        this.f6400c = this.f6399b;
        b();
        if (i8 <= this.f6399b) {
            dVar.a(this.f6398a, i7, 0, i8);
            this.f6400c = i8;
            return;
        }
        InputStream a2 = dVar.a();
        long j2 = (long) i7;
        if (j2 == a2.skip(j2)) {
            while (i8 > 0) {
                int min = Math.min(i8, this.f6399b);
                int read = a2.read(this.f6398a, 0, min);
                if (read == min) {
                    this.f6401d.write(this.f6398a, 0, read);
                    i8 -= read;
                } else {
                    throw new IllegalStateException("Read failed.");
                }
            }
            return;
        }
        throw new IllegalStateException("Skip failed.");
    }

    public void a(long j2) throws IOException {
        while ((-128 & j2) != 0) {
            c((((int) j2) & Notifications.NOTIFICATION_TYPES_ALL) | 128);
            j2 >>>= 7;
        }
        c((int) j2);
    }
}
