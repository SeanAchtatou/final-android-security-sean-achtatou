package com.crashlytics.android.e;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;

/* compiled from: ClsFileOutputStream */
class f extends FileOutputStream {

    /* renamed from: d  reason: collision with root package name */
    public static final FilenameFilter f6394d = new a();

    /* renamed from: a  reason: collision with root package name */
    private final String f6395a;

    /* renamed from: b  reason: collision with root package name */
    private File f6396b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f6397c = false;

    /* compiled from: ClsFileOutputStream */
    static class a implements FilenameFilter {
        a() {
        }

        public boolean accept(File file, String str) {
            return str.endsWith(".cls_temp");
        }
    }

    public f(File file, String str) throws FileNotFoundException {
        super(new File(file, str + ".cls_temp"));
        this.f6395a = file + File.separator + str;
        StringBuilder sb = new StringBuilder();
        sb.append(this.f6395a);
        sb.append(".cls_temp");
        this.f6396b = new File(sb.toString());
    }

    public synchronized void close() throws IOException {
        if (!this.f6397c) {
            this.f6397c = true;
            super.flush();
            super.close();
            File file = new File(this.f6395a + ".cls");
            if (this.f6396b.renameTo(file)) {
                this.f6396b = null;
                return;
            }
            String str = "";
            if (file.exists()) {
                str = " (target already exists)";
            } else if (!this.f6396b.exists()) {
                str = " (source does not exist)";
            }
            throw new IOException("Could not rename temp file: " + this.f6396b + " -> " + file + str);
        }
    }

    public void d() throws IOException {
        if (!this.f6397c) {
            this.f6397c = true;
            super.flush();
            super.close();
        }
    }
}
