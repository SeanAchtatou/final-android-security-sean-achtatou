package com.crashlytics.android.e;

import android.content.Context;
import h.a.a.a.n.b.i;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: NativeFileUtils */
final class g0 {
    private static byte[] a(InputStream inputStream) throws IOException {
        byte[] bArr = new byte[1024];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    static byte[] b(File file) {
        File a2 = a(file, ".dmp");
        if (a2 == null) {
            return new byte[0];
        }
        return c(a2);
    }

    private static byte[] c(File file) {
        return d(file);
    }

    static byte[] d(File file) {
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2 = null;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                byte[] a2 = a(fileInputStream);
                i.a((Closeable) fileInputStream);
                return a2;
            } catch (FileNotFoundException unused) {
                i.a((Closeable) fileInputStream);
                return null;
            } catch (IOException unused2) {
                i.a((Closeable) fileInputStream);
                return null;
            } catch (Throwable th) {
                th = th;
                fileInputStream2 = fileInputStream;
                i.a((Closeable) fileInputStream2);
                throw th;
            }
        } catch (FileNotFoundException unused3) {
            fileInputStream = null;
            i.a((Closeable) fileInputStream);
            return null;
        } catch (IOException unused4) {
            fileInputStream = null;
            i.a((Closeable) fileInputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            i.a((Closeable) fileInputStream2);
            throw th;
        }
    }

    private static byte[] c(File file, Context context) throws IOException {
        BufferedReader bufferedReader;
        if (!file.exists()) {
            return null;
        }
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            try {
                byte[] a2 = new c(context, new t0()).a(bufferedReader);
                i.a(bufferedReader);
                return a2;
            } catch (Throwable th) {
                th = th;
                i.a(bufferedReader);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            i.a(bufferedReader);
            throw th;
        }
    }

    static byte[] b(File file, Context context) throws IOException {
        File a2 = a(file, ".maps");
        if (a2 != null) {
            return c(a2, context);
        }
        File a3 = a(file, ".binary_libs");
        if (a3 != null) {
            return a(a3, context);
        }
        return null;
    }

    private static File a(File file, String str) {
        for (File file2 : file.listFiles()) {
            if (file2.getName().endsWith(str)) {
                return file2;
            }
        }
        return null;
    }

    private static byte[] a(File file, Context context) throws IOException {
        byte[] d2 = d(file);
        if (d2 == null || d2.length == 0) {
            return null;
        }
        return a(context, new String(d2));
    }

    static byte[] a(File file) {
        File a2 = a(file, ".device_info");
        if (a2 == null) {
            return null;
        }
        return d(a2);
    }

    private static byte[] a(Context context, String str) throws IOException {
        return new c(context, new t0()).a(str);
    }
}
