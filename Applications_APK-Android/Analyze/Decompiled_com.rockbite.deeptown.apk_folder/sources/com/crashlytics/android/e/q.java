package com.crashlytics.android.e;

import h.a.a.a.n.e.g;
import java.io.InputStream;

/* compiled from: CrashlyticsPinningInfoProvider */
class q implements g {

    /* renamed from: a  reason: collision with root package name */
    private final i0 f6548a;

    public q(i0 i0Var) {
        this.f6548a = i0Var;
    }

    public String a() {
        return this.f6548a.a();
    }

    public InputStream b() {
        return this.f6548a.b();
    }

    public String[] c() {
        return this.f6548a.c();
    }

    public long d() {
        return -1;
    }
}
