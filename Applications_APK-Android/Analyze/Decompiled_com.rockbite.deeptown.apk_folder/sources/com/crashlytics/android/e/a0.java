package com.crashlytics.android.e;

import android.content.Context;
import h.a.a.a.n.b.i;
import java.io.File;
import java.util.Set;

/* compiled from: LogFileManager */
class a0 {

    /* renamed from: d  reason: collision with root package name */
    private static final c f6375d = new c();

    /* renamed from: a  reason: collision with root package name */
    private final Context f6376a;

    /* renamed from: b  reason: collision with root package name */
    private final b f6377b;

    /* renamed from: c  reason: collision with root package name */
    private y f6378c;

    /* compiled from: LogFileManager */
    public interface b {
        File a();
    }

    /* compiled from: LogFileManager */
    private static final class c implements y {
        private c() {
        }

        public void a() {
        }

        public void a(long j2, String str) {
        }

        public d b() {
            return null;
        }

        public byte[] c() {
            return null;
        }

        public void d() {
        }
    }

    a0(Context context, b bVar) {
        this(context, bVar, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: h.a.a.a.n.b.i.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String, java.lang.String):int
      h.a.a.a.n.b.i.a(java.lang.String, java.lang.String, int):long
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String, java.lang.Throwable):void
      h.a.a.a.n.b.i.a(java.io.InputStream, java.io.OutputStream, byte[]):void
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.f6378c.a();
        this.f6378c = f6375d;
        if (str != null) {
            if (!i.a(this.f6376a, "com.crashlytics.CollectCustomLogs", true)) {
                h.a.a.a.c.f().d("CrashlyticsCore", "Preferences requested no custom logs. Aborting log file creation.");
            } else {
                a(b(str), 65536);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public d b() {
        return this.f6378c.b();
    }

    /* access modifiers changed from: package-private */
    public byte[] c() {
        return this.f6378c.c();
    }

    a0(Context context, b bVar, String str) {
        this.f6376a = context;
        this.f6377b = bVar;
        this.f6378c = f6375d;
        a(str);
    }

    private File b(String str) {
        return new File(this.f6377b.a(), "crashlytics-userlog-" + str + ".temp");
    }

    /* access modifiers changed from: package-private */
    public void a(long j2, String str) {
        this.f6378c.a(j2, str);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f6378c.d();
    }

    /* access modifiers changed from: package-private */
    public void a(Set<String> set) {
        File[] listFiles = this.f6377b.a().listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (!set.contains(a(file))) {
                    file.delete();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(File file, int i2) {
        this.f6378c = new m0(file, i2);
    }

    private String a(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".temp");
        if (lastIndexOf == -1) {
            return name;
        }
        return name.substring(20, lastIndexOf);
    }
}
