package com.crashlytics.android.e;

import com.crashlytics.android.e.o0;
import h.a.a.a.c;
import h.a.a.a.l;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: SessionReport */
class s0 implements o0 {

    /* renamed from: a  reason: collision with root package name */
    private final File f6562a;

    /* renamed from: b  reason: collision with root package name */
    private final File[] f6563b;

    /* renamed from: c  reason: collision with root package name */
    private final Map<String, String> f6564c;

    public s0(File file) {
        this(file, Collections.emptyMap());
    }

    public Map<String, String> a() {
        return Collections.unmodifiableMap(this.f6564c);
    }

    public String b() {
        String e2 = e();
        return e2.substring(0, e2.lastIndexOf(46));
    }

    public File c() {
        return this.f6562a;
    }

    public File[] d() {
        return this.f6563b;
    }

    public String e() {
        return c().getName();
    }

    public o0.a getType() {
        return o0.a.JAVA;
    }

    public void remove() {
        l f2 = c.f();
        f2.d("CrashlyticsCore", "Removing report at " + this.f6562a.getPath());
        this.f6562a.delete();
    }

    public s0(File file, Map<String, String> map) {
        this.f6562a = file;
        this.f6563b = new File[]{file};
        this.f6564c = new HashMap(map);
        if (this.f6562a.length() == 0) {
            this.f6564c.putAll(p0.f6537g);
        }
    }
}
