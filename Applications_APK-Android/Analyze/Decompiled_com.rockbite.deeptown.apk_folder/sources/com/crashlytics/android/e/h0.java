package com.crashlytics.android.e;

import com.crashlytics.android.e.o0;
import h.a.a.a.c;
import java.io.File;
import java.util.Map;

/* compiled from: NativeSessionReport */
class h0 implements o0 {

    /* renamed from: a  reason: collision with root package name */
    private final File f6405a;

    public h0(File file) {
        this.f6405a = file;
    }

    public Map<String, String> a() {
        return null;
    }

    public String b() {
        return this.f6405a.getName();
    }

    public File c() {
        return null;
    }

    public File[] d() {
        return this.f6405a.listFiles();
    }

    public String e() {
        return null;
    }

    public o0.a getType() {
        return o0.a.NATIVE;
    }

    public void remove() {
        for (File file : d()) {
            c.f().d("CrashlyticsCore", "Removing native report file at " + file.getPath());
            file.delete();
        }
        c.f().d("CrashlyticsCore", "Removing native report directory at " + this.f6405a);
        this.f6405a.delete();
    }
}
