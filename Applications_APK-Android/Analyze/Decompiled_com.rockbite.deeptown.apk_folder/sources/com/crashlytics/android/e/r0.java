package com.crashlytics.android.e;

import android.app.ActivityManager;
import com.facebook.appevents.AppEventsConstants;
import h.a.a.a.c;
import h.a.a.a.n.b.s;
import java.util.List;
import java.util.Map;

/* compiled from: SessionProtobufHelper */
class r0 {

    /* renamed from: a  reason: collision with root package name */
    private static final d f6558a = d.a(AppEventsConstants.EVENT_PARAM_VALUE_NO);

    /* renamed from: b  reason: collision with root package name */
    private static final d f6559b = d.a("Unity");

    public static void a(g gVar, String str, String str2, long j2) throws Exception {
        gVar.a(1, d.a(str2));
        gVar.a(2, d.a(str));
        gVar.a(3, j2);
    }

    private static int b(d dVar) {
        return g.b(1, dVar) + 0;
    }

    public static void a(g gVar, String str, String str2, String str3, String str4, String str5, int i2, String str6) throws Exception {
        d a2 = d.a(str);
        d a3 = d.a(str2);
        d a4 = d.a(str3);
        d a5 = d.a(str4);
        d a6 = d.a(str5);
        d a7 = str6 != null ? d.a(str6) : null;
        gVar.c(7, 2);
        gVar.g(a(a2, a3, a4, a5, a6, i2, a7));
        gVar.a(1, a2);
        gVar.a(2, a4);
        gVar.a(3, a5);
        gVar.c(5, 2);
        gVar.g(b(a3));
        gVar.a(1, a3);
        gVar.a(6, a6);
        if (a7 != null) {
            gVar.a(8, f6559b);
            gVar.a(9, a7);
        }
        gVar.a(10, i2);
    }

    public static void a(g gVar, String str, String str2, boolean z) throws Exception {
        d a2 = d.a(str);
        d a3 = d.a(str2);
        gVar.c(8, 2);
        gVar.g(a(a2, a3, z));
        gVar.a(1, 3);
        gVar.a(2, a2);
        gVar.a(3, a3);
        gVar.a(4, z);
    }

    public static void a(g gVar, int i2, String str, int i3, long j2, long j3, boolean z, Map<s.a, String> map, int i4, String str2, String str3) throws Exception {
        g gVar2 = gVar;
        d a2 = a(str);
        d a3 = a(str3);
        d a4 = a(str2);
        gVar2.c(9, 2);
        d dVar = a4;
        gVar2.g(a(i2, a2, i3, j2, j3, z, map, i4, a4, a3));
        gVar2.a(3, i2);
        gVar2.a(4, a2);
        gVar2.d(5, i3);
        gVar2.a(6, j2);
        gVar2.a(7, j3);
        gVar2.a(10, z);
        for (Map.Entry next : map.entrySet()) {
            gVar2.c(11, 2);
            gVar2.g(a((s.a) next.getKey(), (String) next.getValue()));
            gVar2.a(1, ((s.a) next.getKey()).f15125a);
            gVar2.a(2, d.a((String) next.getValue()));
        }
        gVar2.d(12, i4);
        if (dVar != null) {
            gVar2.a(13, dVar);
        }
        if (a3 != null) {
            gVar2.a(14, a3);
        }
    }

    public static void a(g gVar, String str, String str2, String str3) throws Exception {
        if (str == null) {
            str = "";
        }
        d a2 = d.a(str);
        d a3 = a(str2);
        d a4 = a(str3);
        int b2 = g.b(1, a2) + 0;
        if (str2 != null) {
            b2 += g.b(2, a3);
        }
        if (str3 != null) {
            b2 += g.b(3, a4);
        }
        gVar.c(6, 2);
        gVar.g(b2);
        gVar.a(1, a2);
        if (str2 != null) {
            gVar.a(2, a3);
        }
        if (str3 != null) {
            gVar.a(3, a4);
        }
    }

    public static void a(g gVar, long j2, String str, v0 v0Var, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, Map<String, String> map, a0 a0Var, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2, String str2, String str3, Float f2, int i3, boolean z, long j3, long j4) throws Exception {
        d dVar;
        g gVar2 = gVar;
        String str4 = str3;
        d a2 = d.a(str2);
        if (str4 == null) {
            dVar = null;
        } else {
            dVar = d.a(str4.replace("-", ""));
        }
        d dVar2 = dVar;
        d b2 = a0Var.b();
        if (b2 == null) {
            c.f().d("CrashlyticsCore", "No log data to include with this event.");
        }
        a0Var.a();
        gVar2.c(10, 2);
        gVar2.g(a(j2, str, v0Var, thread, stackTraceElementArr, threadArr, list, 8, map, runningAppProcessInfo, i2, a2, dVar2, f2, i3, z, j3, j4, b2));
        gVar2.a(1, j2);
        gVar2.a(2, d.a(str));
        a(gVar, v0Var, thread, stackTraceElementArr, threadArr, list, 8, a2, dVar2, map, runningAppProcessInfo, i2);
        a(gVar, f2, i3, z, i2, j3, j4);
        a(gVar2, b2);
    }

    private static void a(g gVar, v0 v0Var, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i2, d dVar, d dVar2, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i3) throws Exception {
        gVar.c(3, 2);
        gVar.g(a(v0Var, thread, stackTraceElementArr, threadArr, list, i2, dVar, dVar2, map, runningAppProcessInfo, i3));
        a(gVar, v0Var, thread, stackTraceElementArr, threadArr, list, i2, dVar, dVar2);
        if (map != null && !map.isEmpty()) {
            a(gVar, map);
        }
        if (runningAppProcessInfo != null) {
            gVar.a(3, runningAppProcessInfo.importance != 100);
        }
        gVar.d(4, i3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, java.lang.Thread, java.lang.StackTraceElement[], int, boolean):void
     arg types: [com.crashlytics.android.e.g, java.lang.Thread, java.lang.StackTraceElement[], int, int]
     candidates:
      com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, com.crashlytics.android.e.v0, int, int, int):void
      com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, java.lang.Thread, java.lang.StackTraceElement[], int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crashlytics.android.e.g.a(int, long):void
     arg types: [int, int]
     candidates:
      com.crashlytics.android.e.g.a(java.io.OutputStream, int):com.crashlytics.android.e.g
      com.crashlytics.android.e.g.a(int, float):void
      com.crashlytics.android.e.g.a(int, int):void
      com.crashlytics.android.e.g.a(int, com.crashlytics.android.e.d):void
      com.crashlytics.android.e.g.a(int, boolean):void
      com.crashlytics.android.e.g.a(int, long):void */
    private static void a(g gVar, v0 v0Var, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i2, d dVar, d dVar2) throws Exception {
        gVar.c(1, 2);
        gVar.g(a(v0Var, thread, stackTraceElementArr, threadArr, list, i2, dVar, dVar2));
        a(gVar, thread, stackTraceElementArr, 4, true);
        int length = threadArr.length;
        for (int i3 = 0; i3 < length; i3++) {
            a(gVar, threadArr[i3], list.get(i3), 0, false);
        }
        a(gVar, v0Var, 1, i2, 2);
        gVar.c(3, 2);
        gVar.g(a());
        gVar.a(1, f6558a);
        gVar.a(2, f6558a);
        gVar.a(3, 0L);
        gVar.c(4, 2);
        gVar.g(a(dVar, dVar2));
        gVar.a(1, 0L);
        gVar.a(2, 0L);
        gVar.a(3, dVar);
        if (dVar2 != null) {
            gVar.a(4, dVar2);
        }
    }

    private static void a(g gVar, Map<String, String> map) throws Exception {
        for (Map.Entry next : map.entrySet()) {
            gVar.c(2, 2);
            gVar.g(a((String) next.getKey(), (String) next.getValue()));
            gVar.a(1, d.a((String) next.getKey()));
            String str = (String) next.getValue();
            if (str == null) {
                str = "";
            }
            gVar.a(2, d.a(str));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, int, java.lang.StackTraceElement, boolean):void
     arg types: [com.crashlytics.android.e.g, int, java.lang.StackTraceElement, int]
     candidates:
      com.crashlytics.android.e.r0.a(java.lang.Thread, java.lang.StackTraceElement[], int, boolean):int
      com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, java.lang.String, java.lang.String, long):void
      com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, java.lang.String, java.lang.String, java.lang.String):void
      com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, java.lang.String, java.lang.String, boolean):void
      com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, int, java.lang.StackTraceElement, boolean):void */
    private static void a(g gVar, v0 v0Var, int i2, int i3, int i4) throws Exception {
        gVar.c(i4, 2);
        gVar.g(a(v0Var, 1, i3));
        gVar.a(1, d.a(v0Var.f6570b));
        String str = v0Var.f6569a;
        if (str != null) {
            gVar.a(3, d.a(str));
        }
        int i5 = 0;
        for (StackTraceElement a2 : v0Var.f6571c) {
            a(gVar, 4, a2, true);
        }
        v0 v0Var2 = v0Var.f6572d;
        if (v0Var2 == null) {
            return;
        }
        if (i2 < i3) {
            a(gVar, v0Var2, i2 + 1, i3, 6);
            return;
        }
        while (v0Var2 != null) {
            v0Var2 = v0Var2.f6572d;
            i5++;
        }
        gVar.d(7, i5);
    }

    private static void a(g gVar, Thread thread, StackTraceElement[] stackTraceElementArr, int i2, boolean z) throws Exception {
        gVar.c(1, 2);
        gVar.g(a(thread, stackTraceElementArr, i2, z));
        gVar.a(1, d.a(thread.getName()));
        gVar.d(2, i2);
        for (StackTraceElement a2 : stackTraceElementArr) {
            a(gVar, 3, a2, z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crashlytics.android.e.g.a(int, long):void
     arg types: [int, int]
     candidates:
      com.crashlytics.android.e.g.a(java.io.OutputStream, int):com.crashlytics.android.e.g
      com.crashlytics.android.e.g.a(int, float):void
      com.crashlytics.android.e.g.a(int, int):void
      com.crashlytics.android.e.g.a(int, com.crashlytics.android.e.d):void
      com.crashlytics.android.e.g.a(int, boolean):void
      com.crashlytics.android.e.g.a(int, long):void */
    private static void a(g gVar, int i2, StackTraceElement stackTraceElement, boolean z) throws Exception {
        gVar.c(i2, 2);
        gVar.g(a(stackTraceElement, z));
        if (stackTraceElement.isNativeMethod()) {
            gVar.a(1, (long) Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            gVar.a(1, 0L);
        }
        gVar.a(2, d.a(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            gVar.a(3, d.a(stackTraceElement.getFileName()));
        }
        int i3 = 4;
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            gVar.a(4, (long) stackTraceElement.getLineNumber());
        }
        if (!z) {
            i3 = 0;
        }
        gVar.d(5, i3);
    }

    private static void a(g gVar, Float f2, int i2, boolean z, int i3, long j2, long j3) throws Exception {
        gVar.c(5, 2);
        gVar.g(a(f2, i2, z, i3, j2, j3));
        if (f2 != null) {
            gVar.a(1, f2.floatValue());
        }
        gVar.b(2, i2);
        gVar.a(3, z);
        gVar.d(4, i3);
        gVar.a(5, j2);
        gVar.a(6, j3);
    }

    private static void a(g gVar, d dVar) throws Exception {
        if (dVar != null) {
            gVar.c(6, 2);
            gVar.g(a(dVar));
            gVar.a(1, dVar);
        }
    }

    private static int a(d dVar, d dVar2, d dVar3, d dVar4, d dVar5, int i2, d dVar6) {
        int b2 = b(dVar2);
        int b3 = g.b(1, dVar) + 0 + g.b(2, dVar3) + g.b(3, dVar4) + g.n(5) + g.l(b2) + b2 + g.b(6, dVar5);
        if (dVar6 != null) {
            b3 = b3 + g.b(8, f6559b) + g.b(9, dVar6);
        }
        return b3 + g.e(10, i2);
    }

    private static int a(d dVar, d dVar2, boolean z) {
        return g.e(1, 3) + 0 + g.b(2, dVar) + g.b(3, dVar2) + g.b(4, z);
    }

    private static int a(s.a aVar, String str) {
        return g.e(1, aVar.f15125a) + g.b(2, d.a(str));
    }

    private static int a(int i2, d dVar, int i3, long j2, long j3, boolean z, Map<s.a, String> map, int i4, d dVar2, d dVar3) {
        int i5;
        int i6;
        int i7 = 0;
        int e2 = g.e(3, i2) + 0;
        if (dVar == null) {
            i5 = 0;
        } else {
            i5 = g.b(4, dVar);
        }
        int g2 = e2 + i5 + g.g(5, i3) + g.b(6, j2) + g.b(7, j3) + g.b(10, z);
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                int a2 = a((s.a) next.getKey(), (String) next.getValue());
                g2 += g.n(11) + g.l(a2) + a2;
            }
        }
        int g3 = g2 + g.g(12, i4);
        if (dVar2 == null) {
            i6 = 0;
        } else {
            i6 = g.b(13, dVar2);
        }
        int i8 = g3 + i6;
        if (dVar3 != null) {
            i7 = g.b(14, dVar3);
        }
        return i8 + i7;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crashlytics.android.e.g.b(int, long):int
     arg types: [int, int]
     candidates:
      com.crashlytics.android.e.g.b(int, float):int
      com.crashlytics.android.e.g.b(int, com.crashlytics.android.e.d):int
      com.crashlytics.android.e.g.b(int, boolean):int
      com.crashlytics.android.e.g.b(int, int):void
      com.crashlytics.android.e.g.b(int, long):int */
    private static int a(d dVar, d dVar2) {
        int b2 = g.b(1, 0L) + 0 + g.b(2, 0L) + g.b(3, dVar);
        return dVar2 != null ? b2 + g.b(4, dVar2) : b2;
    }

    private static int a(long j2, String str, v0 v0Var, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i2, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i3, d dVar, d dVar2, Float f2, int i4, boolean z, long j3, long j4, d dVar3) {
        int b2 = g.b(1, j2) + 0 + g.b(2, d.a(str));
        int a2 = a(v0Var, thread, stackTraceElementArr, threadArr, list, i2, dVar, dVar2, map, runningAppProcessInfo, i3);
        int a3 = a(f2, i4, z, i3, j3, j4);
        int n = b2 + g.n(3) + g.l(a2) + a2 + g.n(5) + g.l(a3) + a3;
        if (dVar3 == null) {
            return n;
        }
        int a4 = a(dVar3);
        return n + g.n(6) + g.l(a4) + a4;
    }

    private static int a(v0 v0Var, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i2, d dVar, d dVar2, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i3) {
        int a2 = a(v0Var, thread, stackTraceElementArr, threadArr, list, i2, dVar, dVar2);
        int n = g.n(1) + g.l(a2) + a2;
        boolean z = false;
        int i4 = n + 0;
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                int a3 = a((String) next.getKey(), (String) next.getValue());
                i4 += g.n(2) + g.l(a3) + a3;
            }
        }
        if (runningAppProcessInfo != null) {
            if (runningAppProcessInfo.importance != 100) {
                z = true;
            }
            i4 += g.b(3, z);
        }
        return i4 + g.g(4, i3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crashlytics.android.e.r0.a(java.lang.Thread, java.lang.StackTraceElement[], int, boolean):int
     arg types: [java.lang.Thread, java.lang.StackTraceElement[], int, int]
     candidates:
      com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, int, java.lang.StackTraceElement, boolean):void
      com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, java.lang.String, java.lang.String, long):void
      com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, java.lang.String, java.lang.String, java.lang.String):void
      com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, java.lang.String, java.lang.String, boolean):void
      com.crashlytics.android.e.r0.a(java.lang.Thread, java.lang.StackTraceElement[], int, boolean):int */
    private static int a(v0 v0Var, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i2, d dVar, d dVar2) {
        int a2 = a(thread, stackTraceElementArr, 4, true);
        int length = threadArr.length;
        int n = g.n(1) + g.l(a2) + a2 + 0;
        for (int i3 = 0; i3 < length; i3++) {
            int a3 = a(threadArr[i3], list.get(i3), 0, false);
            n += g.n(1) + g.l(a3) + a3;
        }
        int a4 = a(v0Var, 1, i2);
        int a5 = a();
        int a6 = a(dVar, dVar2);
        return n + g.n(2) + g.l(a4) + a4 + g.n(3) + g.l(a5) + a5 + g.n(3) + g.l(a6) + a6;
    }

    private static int a(String str, String str2) {
        int b2 = g.b(1, d.a(str));
        if (str2 == null) {
            str2 = "";
        }
        return b2 + g.b(2, d.a(str2));
    }

    private static int a(Float f2, int i2, boolean z, int i3, long j2, long j3) {
        int i4 = 0;
        if (f2 != null) {
            i4 = 0 + g.b(1, f2.floatValue());
        }
        return i4 + g.f(2, i2) + g.b(3, z) + g.g(4, i3) + g.b(5, j2) + g.b(6, j3);
    }

    private static int a(d dVar) {
        return g.b(1, dVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crashlytics.android.e.r0.a(java.lang.StackTraceElement, boolean):int
     arg types: [java.lang.StackTraceElement, int]
     candidates:
      com.crashlytics.android.e.r0.a(com.crashlytics.android.e.d, com.crashlytics.android.e.d):int
      com.crashlytics.android.e.r0.a(h.a.a.a.n.b.s$a, java.lang.String):int
      com.crashlytics.android.e.r0.a(java.lang.String, java.lang.String):int
      com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, com.crashlytics.android.e.d):void
      com.crashlytics.android.e.r0.a(com.crashlytics.android.e.g, java.util.Map<java.lang.String, java.lang.String>):void
      com.crashlytics.android.e.r0.a(java.lang.StackTraceElement, boolean):int */
    private static int a(v0 v0Var, int i2, int i3) {
        int i4 = 0;
        int b2 = g.b(1, d.a(v0Var.f6570b)) + 0;
        String str = v0Var.f6569a;
        if (str != null) {
            b2 += g.b(3, d.a(str));
        }
        int i5 = b2;
        for (StackTraceElement a2 : v0Var.f6571c) {
            int a3 = a(a2, true);
            i5 += g.n(4) + g.l(a3) + a3;
        }
        v0 v0Var2 = v0Var.f6572d;
        if (v0Var2 == null) {
            return i5;
        }
        if (i2 < i3) {
            int a4 = a(v0Var2, i2 + 1, i3);
            return i5 + g.n(6) + g.l(a4) + a4;
        }
        while (v0Var2 != null) {
            v0Var2 = v0Var2.f6572d;
            i4++;
        }
        return i5 + g.g(7, i4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crashlytics.android.e.g.b(int, long):int
     arg types: [int, int]
     candidates:
      com.crashlytics.android.e.g.b(int, float):int
      com.crashlytics.android.e.g.b(int, com.crashlytics.android.e.d):int
      com.crashlytics.android.e.g.b(int, boolean):int
      com.crashlytics.android.e.g.b(int, int):void
      com.crashlytics.android.e.g.b(int, long):int */
    private static int a() {
        return g.b(1, f6558a) + 0 + g.b(2, f6558a) + g.b(3, 0L);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crashlytics.android.e.g.b(int, long):int
     arg types: [int, int]
     candidates:
      com.crashlytics.android.e.g.b(int, float):int
      com.crashlytics.android.e.g.b(int, com.crashlytics.android.e.d):int
      com.crashlytics.android.e.g.b(int, boolean):int
      com.crashlytics.android.e.g.b(int, int):void
      com.crashlytics.android.e.g.b(int, long):int */
    private static int a(StackTraceElement stackTraceElement, boolean z) {
        int i2;
        int i3 = 0;
        if (stackTraceElement.isNativeMethod()) {
            i2 = g.b(1, (long) Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            i2 = g.b(1, 0L);
        }
        int b2 = i2 + 0 + g.b(2, d.a(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            b2 += g.b(3, d.a(stackTraceElement.getFileName()));
        }
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            b2 += g.b(4, (long) stackTraceElement.getLineNumber());
        }
        if (z) {
            i3 = 2;
        }
        return b2 + g.g(5, i3);
    }

    private static int a(Thread thread, StackTraceElement[] stackTraceElementArr, int i2, boolean z) {
        int b2 = g.b(1, d.a(thread.getName())) + g.g(2, i2);
        for (StackTraceElement a2 : stackTraceElementArr) {
            int a3 = a(a2, z);
            b2 += g.n(3) + g.l(a3) + a3;
        }
        return b2;
    }

    private static d a(String str) {
        if (str == null) {
            return null;
        }
        return d.a(str);
    }
}
