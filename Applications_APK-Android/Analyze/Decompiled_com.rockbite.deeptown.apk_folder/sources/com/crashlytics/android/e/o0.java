package com.crashlytics.android.e;

import java.io.File;
import java.util.Map;

/* compiled from: Report */
interface o0 {

    /* compiled from: Report */
    public enum a {
        JAVA,
        NATIVE
    }

    Map<String, String> a();

    String b();

    File c();

    File[] d();

    String e();

    a getType();

    void remove();
}
