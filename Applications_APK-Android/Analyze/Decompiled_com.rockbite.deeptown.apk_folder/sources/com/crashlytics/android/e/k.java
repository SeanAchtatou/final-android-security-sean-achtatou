package com.crashlytics.android.e;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import com.crashlytics.android.e.a0;
import com.crashlytics.android.e.i;
import com.crashlytics.android.e.p0;
import com.crashlytics.android.e.r;
import com.facebook.internal.ServerProtocol;
import com.tapjoy.TapjoyConstants;
import h.a.a.a.n.b.j;
import h.a.a.a.n.b.s;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Thread;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;
import org.json.JSONObject;

/* compiled from: CrashlyticsController */
class k {
    static final FilenameFilter s = new C0133k("BeginSession");
    static final FilenameFilter t = new r();
    static final FileFilter u = new s();
    static final Comparator<File> v = new t();
    static final Comparator<File> w = new u();
    /* access modifiers changed from: private */
    public static final Pattern x = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
    private static final Map<String, String> y = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
    private static final String[] z = {"SessionUser", "SessionApp", "SessionOS", "SessionDevice"};

    /* renamed from: a  reason: collision with root package name */
    private final AtomicInteger f6419a = new AtomicInteger(0);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final l f6420b;

    /* renamed from: c  reason: collision with root package name */
    private final j f6421c;

    /* renamed from: d  reason: collision with root package name */
    private final h.a.a.a.n.e.e f6422d;

    /* renamed from: e  reason: collision with root package name */
    private final h.a.a.a.n.b.s f6423e;

    /* renamed from: f  reason: collision with root package name */
    private final j0 f6424f;

    /* renamed from: g  reason: collision with root package name */
    private final h.a.a.a.n.f.a f6425g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public final a f6426h;

    /* renamed from: i  reason: collision with root package name */
    private final f0 f6427i;
    /* access modifiers changed from: private */

    /* renamed from: j  reason: collision with root package name */
    public final a0 f6428j;

    /* renamed from: k  reason: collision with root package name */
    private final p0.c f6429k;
    private final p0.b l;
    private final w m;
    private final u0 n;
    /* access modifiers changed from: private */
    public final String o;
    private final b p;
    private final com.crashlytics.android.c.n q;
    private r r;

    /* compiled from: CrashlyticsController */
    class a implements Callable<Void> {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f6430a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f6431b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ String f6432c;

        a(String str, String str2, String str3) {
            this.f6430a = str;
            this.f6431b = str2;
            this.f6432c = str3;
        }

        public Void call() throws Exception {
            new c0(k.this.c()).a(k.this.o(), new x0(this.f6430a, this.f6431b, this.f6432c));
            return null;
        }
    }

    /* compiled from: CrashlyticsController */
    private interface a0 {
        void a(g gVar) throws Exception;
    }

    /* compiled from: CrashlyticsController */
    class b implements Callable<Void> {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Map f6434a;

        b(Map map) {
            this.f6434a = map;
        }

        public Void call() throws Exception {
            new c0(k.this.c()).a(k.this.o(), this.f6434a);
            return null;
        }
    }

    /* compiled from: CrashlyticsController */
    private static final class b0 implements r.b {
        private b0() {
        }

        public h.a.a.a.n.g.u a() {
            return h.a.a.a.n.g.r.d().a();
        }

        /* synthetic */ b0(C0133k kVar) {
            this();
        }
    }

    /* compiled from: CrashlyticsController */
    class c implements Callable<Void> {
        c() {
        }

        public Void call() throws Exception {
            k.this.m();
            return null;
        }
    }

    /* compiled from: CrashlyticsController */
    static class c0 implements FilenameFilter {

        /* renamed from: a  reason: collision with root package name */
        private final String f6437a;

        public c0(String str) {
            this.f6437a = str;
        }

        public boolean accept(File file, String str) {
            return str.contains(this.f6437a) && !str.endsWith(".cls_temp");
        }
    }

    /* compiled from: CrashlyticsController */
    class d implements Callable<Boolean> {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ h.a.a.a.n.g.q f6438a;

        d(h.a.a.a.n.g.q qVar) {
            this.f6438a = qVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.crashlytics.android.e.k.a(com.crashlytics.android.e.k, h.a.a.a.n.g.q, boolean):void
         arg types: [com.crashlytics.android.e.k, h.a.a.a.n.g.q, int]
         candidates:
          com.crashlytics.android.e.k.a(android.content.Context, java.io.File, java.lang.String):void
          com.crashlytics.android.e.k.a(com.crashlytics.android.e.g, java.io.File[], java.lang.String):void
          com.crashlytics.android.e.k.a(java.io.File, java.lang.String, int):void
          com.crashlytics.android.e.k.a(java.io.InputStream, com.crashlytics.android.e.g, int):void
          com.crashlytics.android.e.k.a(java.lang.String, java.lang.String, com.crashlytics.android.e.k$a0):void
          com.crashlytics.android.e.k.a(java.lang.String, java.lang.String, com.crashlytics.android.e.k$d0):void
          com.crashlytics.android.e.k.a(java.util.Date, java.lang.Thread, java.lang.Throwable):void
          com.crashlytics.android.e.k.a(java.io.File[], int, int):void
          com.crashlytics.android.e.k.a(java.lang.String, java.io.File[], int):java.io.File[]
          com.crashlytics.android.e.k.a(java.lang.String, java.lang.String, java.lang.String):void
          com.crashlytics.android.e.k.a(com.crashlytics.android.e.k, h.a.a.a.n.g.q, boolean):void */
        public Boolean call() throws Exception {
            if (k.this.f()) {
                h.a.a.a.c.f().d("CrashlyticsCore", "Skipping session finalization because a crash has already occurred.");
                return Boolean.FALSE;
            }
            h.a.a.a.c.f().d("CrashlyticsCore", "Finalizing previously open sessions.");
            k.this.a(this.f6438a, true);
            h.a.a.a.c.f().d("CrashlyticsCore", "Closed all previously open sessions");
            return Boolean.TRUE;
        }
    }

    /* compiled from: CrashlyticsController */
    private interface d0 {
        void a(FileOutputStream fileOutputStream) throws Exception;
    }

    /* compiled from: CrashlyticsController */
    class e implements Runnable {
        e() {
        }

        public void run() {
            k kVar = k.this;
            kVar.a(kVar.a(new e0()));
        }
    }

    /* compiled from: CrashlyticsController */
    static class e0 implements FilenameFilter {
        e0() {
        }

        public boolean accept(File file, String str) {
            return f.f6394d.accept(file, str) || str.contains("SessionMissingBinaryImages");
        }
    }

    /* compiled from: CrashlyticsController */
    class f implements FilenameFilter {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Set f6441a;

        f(k kVar, Set set) {
            this.f6441a = set;
        }

        public boolean accept(File file, String str) {
            if (str.length() < 35) {
                return false;
            }
            return this.f6441a.contains(str.substring(0, 35));
        }
    }

    /* compiled from: CrashlyticsController */
    private static final class f0 implements a0.b {

        /* renamed from: a  reason: collision with root package name */
        private final h.a.a.a.n.f.a f6442a;

        public f0(h.a.a.a.n.f.a aVar) {
            this.f6442a = aVar;
        }

        public File a() {
            File file = new File(this.f6442a.a(), "log-files");
            if (!file.exists()) {
                file.mkdirs();
            }
            return file;
        }
    }

    /* compiled from: CrashlyticsController */
    class g implements Callable<Boolean> {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ o f6443a;

        g(o oVar) {
            this.f6443a = oVar;
        }

        public Boolean call() throws Exception {
            File first;
            TreeSet<File> treeSet = this.f6443a.f6533a;
            String c2 = k.this.p();
            if (!(c2 == null || treeSet.isEmpty() || (first = treeSet.first()) == null)) {
                k kVar = k.this;
                kVar.a(kVar.f6420b.d(), first, c2);
            }
            k.this.a(treeSet);
            return Boolean.TRUE;
        }
    }

    /* compiled from: CrashlyticsController */
    private static final class g0 implements p0.d {

        /* renamed from: a  reason: collision with root package name */
        private final h.a.a.a.i f6445a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final j0 f6446b;

        /* renamed from: c  reason: collision with root package name */
        private final h.a.a.a.n.g.p f6447c;

        /* compiled from: CrashlyticsController */
        class a implements i.d {
            a() {
            }

            public void a(boolean z) {
                g0.this.f6446b.a(z);
            }
        }

        /* compiled from: CrashlyticsController */
        class b implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ i f6449a;

            b(g0 g0Var, i iVar) {
                this.f6449a = iVar;
            }

            public void run() {
                this.f6449a.c();
            }
        }

        public g0(h.a.a.a.i iVar, j0 j0Var, h.a.a.a.n.g.p pVar) {
            this.f6445a = iVar;
            this.f6446b = j0Var;
            this.f6447c = pVar;
        }

        public boolean a() {
            Activity a2 = this.f6445a.f().a();
            if (a2 == null || a2.isFinishing()) {
                return true;
            }
            i a3 = i.a(a2, this.f6447c, new a());
            a2.runOnUiThread(new b(this, a3));
            h.a.a.a.c.f().d("CrashlyticsCore", "Waiting for user opt-in.");
            a3.a();
            return a3.b();
        }
    }

    /* compiled from: CrashlyticsController */
    class h implements a0 {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f6450a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f6451b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ long f6452c;

        h(k kVar, String str, String str2, long j2) {
            this.f6450a = str;
            this.f6451b = str2;
            this.f6452c = j2;
        }

        public void a(g gVar) throws Exception {
            r0.a(gVar, this.f6450a, this.f6451b, this.f6452c);
        }
    }

    /* compiled from: CrashlyticsController */
    private final class h0 implements p0.c {
        private h0() {
        }

        public File[] a() {
            return k.this.h();
        }

        public File[] b() {
            return k.this.d().listFiles();
        }

        public File[] c() {
            return k.this.g();
        }

        /* synthetic */ h0(k kVar, C0133k kVar2) {
            this();
        }
    }

    /* compiled from: CrashlyticsController */
    class i implements d0 {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f6454a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f6455b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ long f6456c;

        /* compiled from: CrashlyticsController */
        class a extends HashMap<String, Object> {
            a() {
                put(TapjoyConstants.TJC_SESSION_ID, i.this.f6454a);
                put("generator", i.this.f6455b);
                put("started_at_seconds", Long.valueOf(i.this.f6456c));
            }
        }

        i(k kVar, String str, String str2, long j2) {
            this.f6454a = str;
            this.f6455b = str2;
            this.f6456c = j2;
        }

        public void a(FileOutputStream fileOutputStream) throws Exception {
            fileOutputStream.write(new JSONObject(new a()).toString().getBytes());
        }
    }

    /* compiled from: CrashlyticsController */
    private final class i0 implements p0.b {
        private i0() {
        }

        public boolean a() {
            return k.this.f();
        }

        /* synthetic */ i0(k kVar, C0133k kVar2) {
            this();
        }
    }

    /* compiled from: CrashlyticsController */
    class j implements a0 {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f6459a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f6460b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ String f6461c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ String f6462d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ int f6463e;

        j(String str, String str2, String str3, String str4, int i2) {
            this.f6459a = str;
            this.f6460b = str2;
            this.f6461c = str3;
            this.f6462d = str4;
            this.f6463e = i2;
        }

        public void a(g gVar) throws Exception {
            r0.a(gVar, this.f6459a, k.this.f6426h.f6369a, this.f6460b, this.f6461c, this.f6462d, this.f6463e, k.this.o);
        }
    }

    /* compiled from: CrashlyticsController */
    private static final class j0 implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final Context f6465a;

        /* renamed from: b  reason: collision with root package name */
        private final o0 f6466b;

        /* renamed from: c  reason: collision with root package name */
        private final p0 f6467c;

        public j0(Context context, o0 o0Var, p0 p0Var) {
            this.f6465a = context;
            this.f6466b = o0Var;
            this.f6467c = p0Var;
        }

        public void run() {
            if (h.a.a.a.n.b.i.b(this.f6465a)) {
                h.a.a.a.c.f().d("CrashlyticsCore", "Attempting to send crash report at time of crash...");
                this.f6467c.a(this.f6466b);
            }
        }
    }

    /* renamed from: com.crashlytics.android.e.k$k  reason: collision with other inner class name */
    /* compiled from: CrashlyticsController */
    static class C0133k extends c0 {
        C0133k(String str) {
            super(str);
        }

        public boolean accept(File file, String str) {
            return super.accept(file, str) && str.endsWith(".cls");
        }
    }

    /* compiled from: CrashlyticsController */
    static class k0 implements FilenameFilter {

        /* renamed from: a  reason: collision with root package name */
        private final String f6468a;

        public k0(String str) {
            this.f6468a = str;
        }

        public boolean accept(File file, String str) {
            if (!str.equals(this.f6468a + ".cls") && str.contains(this.f6468a) && !str.endsWith(".cls_temp")) {
                return true;
            }
            return false;
        }
    }

    /* compiled from: CrashlyticsController */
    class l implements d0 {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f6469a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f6470b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ String f6471c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ String f6472d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ int f6473e;

        /* compiled from: CrashlyticsController */
        class a extends HashMap<String, Object> {
            a() {
                put("app_identifier", l.this.f6469a);
                put(TapjoyConstants.TJC_API_KEY, k.this.f6426h.f6369a);
                put("version_code", l.this.f6470b);
                put("version_name", l.this.f6471c);
                put("install_uuid", l.this.f6472d);
                put("delivery_mechanism", Integer.valueOf(l.this.f6473e));
                put("unity_version", TextUtils.isEmpty(k.this.o) ? "" : k.this.o);
            }
        }

        l(String str, String str2, String str3, String str4, int i2) {
            this.f6469a = str;
            this.f6470b = str2;
            this.f6471c = str3;
            this.f6472d = str4;
            this.f6473e = i2;
        }

        public void a(FileOutputStream fileOutputStream) throws Exception {
            fileOutputStream.write(new JSONObject(new a()).toString().getBytes());
        }
    }

    /* compiled from: CrashlyticsController */
    class m implements a0 {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ boolean f6476a;

        m(k kVar, boolean z) {
            this.f6476a = z;
        }

        public void a(g gVar) throws Exception {
            r0.a(gVar, Build.VERSION.RELEASE, Build.VERSION.CODENAME, this.f6476a);
        }
    }

    /* compiled from: CrashlyticsController */
    class n implements d0 {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ boolean f6477a;

        /* compiled from: CrashlyticsController */
        class a extends HashMap<String, Object> {
            a() {
                put(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, Build.VERSION.RELEASE);
                put("build_version", Build.VERSION.CODENAME);
                put("is_rooted", Boolean.valueOf(n.this.f6477a));
            }
        }

        n(k kVar, boolean z) {
            this.f6477a = z;
        }

        public void a(FileOutputStream fileOutputStream) throws Exception {
            fileOutputStream.write(new JSONObject(new a()).toString().getBytes());
        }
    }

    /* compiled from: CrashlyticsController */
    class o implements a0 {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ int f6479a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ int f6480b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ long f6481c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ long f6482d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ boolean f6483e;

        /* renamed from: f  reason: collision with root package name */
        final /* synthetic */ Map f6484f;

        /* renamed from: g  reason: collision with root package name */
        final /* synthetic */ int f6485g;

        o(k kVar, int i2, int i3, long j2, long j3, boolean z, Map map, int i4) {
            this.f6479a = i2;
            this.f6480b = i3;
            this.f6481c = j2;
            this.f6482d = j3;
            this.f6483e = z;
            this.f6484f = map;
            this.f6485g = i4;
        }

        public void a(g gVar) throws Exception {
            r0.a(gVar, this.f6479a, Build.MODEL, this.f6480b, this.f6481c, this.f6482d, this.f6483e, this.f6484f, this.f6485g, Build.MANUFACTURER, Build.PRODUCT);
        }
    }

    /* compiled from: CrashlyticsController */
    class p implements d0 {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ int f6486a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ int f6487b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ long f6488c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ long f6489d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ boolean f6490e;

        /* renamed from: f  reason: collision with root package name */
        final /* synthetic */ Map f6491f;

        /* renamed from: g  reason: collision with root package name */
        final /* synthetic */ int f6492g;

        /* compiled from: CrashlyticsController */
        class a extends HashMap<String, Object> {
            a() {
                put("arch", Integer.valueOf(p.this.f6486a));
                put("build_model", Build.MODEL);
                put("available_processors", Integer.valueOf(p.this.f6487b));
                put("total_ram", Long.valueOf(p.this.f6488c));
                put("disk_space", Long.valueOf(p.this.f6489d));
                put("is_emulator", Boolean.valueOf(p.this.f6490e));
                put("ids", p.this.f6491f);
                put("state", Integer.valueOf(p.this.f6492g));
                put("build_manufacturer", Build.MANUFACTURER);
                put("build_product", Build.PRODUCT);
            }
        }

        p(k kVar, int i2, int i3, long j2, long j3, boolean z, Map map, int i4) {
            this.f6486a = i2;
            this.f6487b = i3;
            this.f6488c = j2;
            this.f6489d = j3;
            this.f6490e = z;
            this.f6491f = map;
            this.f6492g = i4;
        }

        public void a(FileOutputStream fileOutputStream) throws Exception {
            fileOutputStream.write(new JSONObject(new a()).toString().getBytes());
        }
    }

    /* compiled from: CrashlyticsController */
    class q implements a0 {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ x0 f6494a;

        q(k kVar, x0 x0Var) {
            this.f6494a = x0Var;
        }

        public void a(g gVar) throws Exception {
            x0 x0Var = this.f6494a;
            r0.a(gVar, x0Var.f6586a, x0Var.f6587b, x0Var.f6588c);
        }
    }

    /* compiled from: CrashlyticsController */
    static class r implements FilenameFilter {
        r() {
        }

        public boolean accept(File file, String str) {
            return str.length() == 39 && str.endsWith(".cls");
        }
    }

    /* compiled from: CrashlyticsController */
    static class s implements FileFilter {
        s() {
        }

        public boolean accept(File file) {
            return file.isDirectory() && file.getName().length() == 35;
        }
    }

    /* compiled from: CrashlyticsController */
    static class t implements Comparator<File> {
        t() {
        }

        /* renamed from: a */
        public int compare(File file, File file2) {
            return file2.getName().compareTo(file.getName());
        }
    }

    /* compiled from: CrashlyticsController */
    static class u implements Comparator<File> {
        u() {
        }

        /* renamed from: a */
        public int compare(File file, File file2) {
            return file.getName().compareTo(file2.getName());
        }
    }

    /* compiled from: CrashlyticsController */
    class v implements r.a {
        v() {
        }

        public void a(r.b bVar, Thread thread, Throwable th, boolean z) {
            k.this.a(bVar, thread, th, z);
        }
    }

    /* compiled from: CrashlyticsController */
    class w implements Callable<Void> {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Date f6496a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Thread f6497b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ Throwable f6498c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ r.b f6499d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ boolean f6500e;

        w(Date date, Thread thread, Throwable th, r.b bVar, boolean z) {
            this.f6496a = date;
            this.f6497b = thread;
            this.f6498c = th;
            this.f6499d = bVar;
            this.f6500e = z;
        }

        public Void call() throws Exception {
            h.a.a.a.n.g.n nVar;
            h.a.a.a.n.g.q qVar;
            k.this.f6420b.n();
            k.this.b(this.f6496a, this.f6497b, this.f6498c);
            h.a.a.a.n.g.u a2 = this.f6499d.a();
            if (a2 != null) {
                qVar = a2.f15315b;
                nVar = a2.f15317d;
            } else {
                qVar = null;
                nVar = null;
            }
            boolean z = false;
            if ((nVar == null || nVar.f15291d) || this.f6500e) {
                k.this.a(this.f6496a.getTime());
            }
            k.this.a(qVar);
            k.this.m();
            if (qVar != null) {
                k.this.a(qVar.f15304b);
            }
            if (h.a.a.a.n.b.l.a(k.this.f6420b.d()).a() && !k.this.c(a2)) {
                z = true;
            }
            if (z) {
                k.this.b(a2);
            }
            return null;
        }
    }

    /* compiled from: CrashlyticsController */
    class x implements Callable<Void> {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ long f6502a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f6503b;

        x(long j2, String str) {
            this.f6502a = j2;
            this.f6503b = str;
        }

        public Void call() throws Exception {
            if (k.this.f()) {
                return null;
            }
            k.this.f6428j.a(this.f6502a, this.f6503b);
            return null;
        }
    }

    /* compiled from: CrashlyticsController */
    class y implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Date f6505a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Thread f6506b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ Throwable f6507c;

        y(Date date, Thread thread, Throwable th) {
            this.f6505a = date;
            this.f6506b = thread;
            this.f6507c = th;
        }

        public void run() {
            if (!k.this.f()) {
                k.this.a(this.f6505a, this.f6506b, this.f6507c);
            }
        }
    }

    /* compiled from: CrashlyticsController */
    private static class z implements FilenameFilter {
        private z() {
        }

        public boolean accept(File file, String str) {
            return !k.t.accept(file, str) && k.x.matcher(str).matches();
        }

        /* synthetic */ z(C0133k kVar) {
            this();
        }
    }

    k(l lVar, j jVar, h.a.a.a.n.e.e eVar, h.a.a.a.n.b.s sVar, j0 j0Var, h.a.a.a.n.f.a aVar, a aVar2, w0 w0Var, b bVar, com.crashlytics.android.c.n nVar) {
        this.f6420b = lVar;
        this.f6421c = jVar;
        this.f6422d = eVar;
        this.f6423e = sVar;
        this.f6424f = j0Var;
        this.f6425g = aVar;
        this.f6426h = aVar2;
        this.o = w0Var.a();
        this.p = bVar;
        this.q = nVar;
        Context d2 = lVar.d();
        this.f6427i = new f0(aVar);
        this.f6428j = new a0(d2, this.f6427i);
        this.f6429k = new h0(this, null);
        this.l = new i0(this, null);
        this.m = new w(d2);
        this.n = new d0(1024, new n0(10));
    }

    private File[] b(File[] fileArr) {
        return fileArr == null ? new File[0] : fileArr;
    }

    /* access modifiers changed from: private */
    public void m() throws Exception {
        Date date = new Date();
        String eVar = new e(this.f6423e).toString();
        h.a.a.a.l f2 = h.a.a.a.c.f();
        f2.d("CrashlyticsCore", "Opening a new session with ID " + eVar);
        a(eVar, date);
        d(eVar);
        f(eVar);
        e(eVar);
        this.f6428j.a(eVar);
    }

    private boolean n() {
        try {
            Class.forName("com.google.firebase.crash.FirebaseCrash");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public String o() {
        File[] q2 = q();
        if (q2.length > 0) {
            return a(q2[0]);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public String p() {
        File[] q2 = q();
        if (q2.length > 1) {
            return a(q2[1]);
        }
        return null;
    }

    private File[] q() {
        File[] i2 = i();
        Arrays.sort(i2, v);
        return i2;
    }

    private void r() {
        File d2 = d();
        if (d2.exists()) {
            File[] a2 = a(d2, new e0());
            Arrays.sort(a2, Collections.reverseOrder());
            HashSet hashSet = new HashSet();
            for (int i2 = 0; i2 < a2.length && hashSet.size() < 4; i2++) {
                hashSet.add(a(a2[i2]));
            }
            a(b(d2), hashSet);
        }
    }

    /* access modifiers changed from: package-private */
    public File[] h() {
        return a(u);
    }

    /* access modifiers changed from: package-private */
    public File[] i() {
        return a(s);
    }

    /* access modifiers changed from: package-private */
    public void j() {
        this.f6421c.a(new c());
    }

    /* access modifiers changed from: package-private */
    public void k() {
        this.m.b();
    }

    private void c(File file) {
        if (file.isDirectory()) {
            for (File c2 : file.listFiles()) {
                c(c2);
            }
        }
        file.delete();
    }

    private void d(String str) throws Exception {
        String c2 = this.f6423e.c();
        a aVar = this.f6426h;
        String str2 = aVar.f6373e;
        String str3 = aVar.f6374f;
        String str4 = c2;
        String str5 = str2;
        String str6 = str3;
        String d2 = this.f6423e.d();
        int a2 = h.a.a.a.n.b.m.a(this.f6426h.f6371c).a();
        a(str, "SessionApp", new j(str4, str5, str6, d2, a2));
        a(str, "SessionApp.json", new l(str4, str5, str6, d2, a2));
    }

    private void e(String str) throws Exception {
        String str2 = str;
        Context d2 = this.f6420b.d();
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        int a2 = h.a.a.a.n.b.i.a();
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        long b2 = h.a.a.a.n.b.i.b();
        long blockCount = ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        boolean l2 = h.a.a.a.n.b.i.l(d2);
        int i2 = a2;
        int i3 = availableProcessors;
        long j2 = b2;
        long j3 = blockCount;
        boolean z2 = l2;
        Map<s.a, String> e2 = this.f6423e.e();
        long j4 = b2;
        o oVar = r0;
        int f2 = h.a.a.a.n.b.i.f(d2);
        o oVar2 = new o(this, i2, i3, j2, j3, z2, e2, f2);
        a(str2, "SessionDevice", oVar);
        a(str2, "SessionDevice.json", new p(this, i2, i3, j4, j3, z2, e2, f2));
    }

    private void f(String str) throws Exception {
        boolean m2 = h.a.a.a.n.b.i.m(this.f6420b.d());
        a(str, "SessionOS", new m(this, m2));
        a(str, "SessionOS.json", new n(this, m2));
    }

    /* access modifiers changed from: package-private */
    public File[] g() {
        LinkedList linkedList = new LinkedList();
        Collections.addAll(linkedList, a(b(), t));
        Collections.addAll(linkedList, a(e(), t));
        Collections.addAll(linkedList, a(c(), t));
        return (File[]) linkedList.toArray(new File[linkedList.size()]);
    }

    /* access modifiers changed from: package-private */
    public boolean b(h.a.a.a.n.g.q qVar) {
        return ((Boolean) this.f6421c.b(new d(qVar))).booleanValue();
    }

    private File[] b(File file) {
        return b(file.listFiles());
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        r rVar = this.r;
        return rVar != null && rVar.a();
    }

    private void b(int i2) {
        HashSet hashSet = new HashSet();
        File[] q2 = q();
        int min = Math.min(i2, q2.length);
        for (int i3 = 0; i3 < min; i3++) {
            hashSet.add(a(q2[i3]));
        }
        this.f6428j.a(hashSet);
        a(a(new z(null)), hashSet);
    }

    private File[] c(String str) {
        return a(new k0(str));
    }

    /* access modifiers changed from: package-private */
    public File c() {
        return this.f6425g.a();
    }

    /* access modifiers changed from: private */
    public boolean c(h.a.a.a.n.g.u uVar) {
        if (uVar != null && uVar.f15317d.f15288a && !this.f6424f.a()) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, boolean z2) {
        j();
        this.r = new r(new v(), new b0(null), z2, uncaughtExceptionHandler);
        Thread.setDefaultUncaughtExceptionHandler(this.r);
    }

    private static void c(String str, String str2) {
        com.crashlytics.android.c.b bVar = (com.crashlytics.android.c.b) h.a.a.a.c.a(com.crashlytics.android.c.b.class);
        if (bVar == null) {
            h.a.a.a.c.f().d("CrashlyticsCore", "Answers is not available");
        } else {
            bVar.a(new j.a(str, str2));
        }
    }

    private void g(String str) throws Exception {
        a(str, "SessionUser", new q(this, b(str)));
    }

    /* access modifiers changed from: package-private */
    public File d() {
        return new File(c(), "invalidClsFiles");
    }

    private static void d(String str, String str2) {
        com.crashlytics.android.c.b bVar = (com.crashlytics.android.c.b) h.a.a.a.c.a(com.crashlytics.android.c.b.class);
        if (bVar == null) {
            h.a.a.a.c.f().d("CrashlyticsCore", "Answers is not available");
        } else {
            bVar.a(new j.b(str, str2));
        }
    }

    private void b(byte[] bArr, File file) throws IOException {
        if (bArr != null && bArr.length > 0) {
            a(bArr, file);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(r.b bVar, Thread thread, Throwable th, boolean z2) {
        h.a.a.a.l f2 = h.a.a.a.c.f();
        f2.d("CrashlyticsCore", "Crashlytics is handling uncaught exception \"" + th + "\" from thread " + thread.getName());
        this.m.a();
        this.f6421c.b(new w(new Date(), thread, th, bVar, z2));
    }

    /* access modifiers changed from: package-private */
    public File e() {
        return new File(c(), "nonfatal-sessions");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void
     arg types: [com.crashlytics.android.e.f, java.lang.String]
     candidates:
      h.a.a.a.n.b.i.a(android.content.Context, boolean):int
      h.a.a.a.n.b.i.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      h.a.a.a.n.b.i.a(java.io.File, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.InputStream, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.lang.String, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(byte[], java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.Flushable, java.lang.String):void
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String):boolean
      h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void */
    /* access modifiers changed from: private */
    public void b(Date date, Thread thread, Throwable th) {
        f fVar;
        g gVar = null;
        try {
            String o2 = o();
            if (o2 == null) {
                h.a.a.a.c.f().b("CrashlyticsCore", "Tried to write a fatal exception while no session was open.", null);
                h.a.a.a.n.b.i.a((Flushable) null, "Failed to flush to session begin file.");
                h.a.a.a.n.b.i.a((Closeable) null, "Failed to close fatal exception file output stream.");
                return;
            }
            c(o2, th.getClass().getName());
            fVar = new f(c(), o2 + "SessionCrash");
            try {
                gVar = g.a(fVar);
                a(gVar, date, thread, th, "crash", true);
            } catch (Exception e2) {
                e = e2;
            }
            h.a.a.a.n.b.i.a(gVar, "Failed to flush to session begin file.");
            h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close fatal exception file output stream.");
        } catch (Exception e3) {
            e = e3;
            fVar = null;
            try {
                h.a.a.a.c.f().b("CrashlyticsCore", "An error occurred in the fatal exception logger", e);
                h.a.a.a.n.b.i.a(gVar, "Failed to flush to session begin file.");
                h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close fatal exception file output stream.");
            } catch (Throwable th2) {
                th = th2;
                h.a.a.a.n.b.i.a(gVar, "Failed to flush to session begin file.");
                h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close fatal exception file output stream.");
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            fVar = null;
            h.a.a.a.n.b.i.a(gVar, "Failed to flush to session begin file.");
            h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close fatal exception file output stream.");
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(float f2, h.a.a.a.n.g.u uVar) {
        if (uVar == null) {
            h.a.a.a.c.f().a("CrashlyticsCore", "Could not send reports. Settings are not available.");
            return;
        }
        h.a.a.a.n.g.e eVar = uVar.f15314a;
        new p0(this.f6426h.f6369a, a(eVar.f15276c, eVar.f15277d), this.f6429k, this.l).a(f2, c(uVar) ? new g0(this.f6420b, this.f6424f, uVar.f15316c) : new p0.a());
    }

    /* access modifiers changed from: package-private */
    public void a(long j2, String str) {
        this.f6421c.a(new x(j2, str));
    }

    /* access modifiers changed from: package-private */
    public void a(Thread thread, Throwable th) {
        this.f6421c.a(new y(new Date(), thread, th));
    }

    private byte[] b(String str, String str2) {
        File c2 = c();
        return g0.d(new File(c2, str + str2));
    }

    private x0 b(String str) {
        if (f()) {
            return new x0(this.f6420b.s(), this.f6420b.t(), this.f6420b.r());
        }
        return new c0(c()).c(str);
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, String str3) {
        this.f6421c.a(new a(str, str2, str3));
    }

    /* access modifiers changed from: package-private */
    public void a(Map<String, String> map) {
        this.f6421c.a(new b(map));
    }

    static String a(File file) {
        return file.getName().substring(0, 35);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.crashlytics.android.e.k.a(h.a.a.a.n.g.q, boolean):void
     arg types: [h.a.a.a.n.g.q, int]
     candidates:
      com.crashlytics.android.e.k.a(java.lang.String, java.lang.String):com.crashlytics.android.e.t
      com.crashlytics.android.e.k.a(com.crashlytics.android.e.g, java.io.File):void
      com.crashlytics.android.e.k.a(com.crashlytics.android.e.g, java.lang.String):void
      com.crashlytics.android.e.k.a(com.crashlytics.android.e.k, long):void
      com.crashlytics.android.e.k.a(com.crashlytics.android.e.k, java.util.Set):void
      com.crashlytics.android.e.k.a(java.lang.String, int):void
      com.crashlytics.android.e.k.a(java.lang.String, java.util.Date):void
      com.crashlytics.android.e.k.a(byte[], java.io.File):void
      com.crashlytics.android.e.k.a(java.io.File[], java.util.Set<java.lang.String>):void
      com.crashlytics.android.e.k.a(com.crashlytics.android.e.k, h.a.a.a.n.g.u):boolean
      com.crashlytics.android.e.k.a(com.crashlytics.android.e.k, java.io.FilenameFilter):java.io.File[]
      com.crashlytics.android.e.k.a(java.io.File, java.io.FilenameFilter):java.io.File[]
      com.crashlytics.android.e.k.a(float, h.a.a.a.n.g.u):void
      com.crashlytics.android.e.k.a(long, java.lang.String):void
      com.crashlytics.android.e.k.a(java.lang.Thread$UncaughtExceptionHandler, boolean):void
      com.crashlytics.android.e.k.a(java.lang.Thread, java.lang.Throwable):void
      com.crashlytics.android.e.k.a(h.a.a.a.n.g.q, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(h.a.a.a.n.g.q qVar) throws Exception {
        a(qVar, false);
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [boolean, int] */
    /* access modifiers changed from: private */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(h.a.a.a.n.g.q r4, boolean r5) throws java.lang.Exception {
        /*
            r3 = this;
            int r0 = r5 + 8
            r3.b(r0)
            java.io.File[] r0 = r3.q()
            int r1 = r0.length
            java.lang.String r2 = "CrashlyticsCore"
            if (r1 > r5) goto L_0x0018
            h.a.a.a.l r4 = h.a.a.a.c.f()
            java.lang.String r5 = "No open sessions to be closed."
            r4.d(r2, r5)
            return
        L_0x0018:
            r1 = r0[r5]
            java.lang.String r1 = a(r1)
            r3.g(r1)
            if (r4 != 0) goto L_0x002d
            h.a.a.a.l r4 = h.a.a.a.c.f()
            java.lang.String r5 = "Unable to close session. Settings are not loaded."
            r4.d(r2, r5)
            return
        L_0x002d:
            int r4 = r4.f15303a
            r3.a(r0, r5, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crashlytics.android.e.k.a(h.a.a.a.n.g.q, boolean):void");
    }

    /* access modifiers changed from: package-private */
    public File b() {
        return new File(c(), "fatal-sessions");
    }

    /* access modifiers changed from: private */
    public void b(h.a.a.a.n.g.u uVar) {
        if (uVar == null) {
            h.a.a.a.c.f().a("CrashlyticsCore", "Cannot send reports. Settings are unavailable.");
            return;
        }
        Context d2 = this.f6420b.d();
        h.a.a.a.n.g.e eVar = uVar.f15314a;
        p0 p0Var = new p0(this.f6426h.f6369a, a(eVar.f15276c, eVar.f15277d), this.f6429k, this.l);
        for (File s0Var : g()) {
            this.f6421c.a(new j0(d2, new s0(s0Var, y), p0Var));
        }
    }

    private void a(File[] fileArr, int i2, int i3) {
        h.a.a.a.c.f().d("CrashlyticsCore", "Closing open sessions.");
        while (i2 < fileArr.length) {
            File file = fileArr[i2];
            String a2 = a(file);
            h.a.a.a.l f2 = h.a.a.a.c.f();
            f2.d("CrashlyticsCore", "Closing session: " + a2);
            a(file, a2, i3);
            i2++;
        }
    }

    private void a(f fVar) {
        if (fVar != null) {
            try {
                fVar.d();
            } catch (IOException e2) {
                h.a.a.a.c.f().b("CrashlyticsCore", "Error closing session file stream in the presence of an exception", e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Set<File> set) {
        for (File c2 : set) {
            c(c2);
        }
    }

    private void a(String str) {
        for (File delete : c(str)) {
            delete.delete();
        }
    }

    private File[] a(FileFilter fileFilter) {
        return b(c().listFiles(fileFilter));
    }

    /* access modifiers changed from: private */
    public File[] a(FilenameFilter filenameFilter) {
        return a(c(), filenameFilter);
    }

    private File[] a(File file, FilenameFilter filenameFilter) {
        return b(file.listFiles(filenameFilter));
    }

    private void a(String str, int i2) {
        File c2 = c();
        y0.a(c2, new c0(str + "SessionEvent"), i2, w);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        int a2 = i2 - y0.a(b(), i2, w);
        y0.a(c(), t, a2 - y0.a(e(), a2, w), w);
    }

    private void a(File[] fileArr, Set<String> set) {
        for (File file : fileArr) {
            String name = file.getName();
            Matcher matcher = x.matcher(name);
            if (!matcher.matches()) {
                h.a.a.a.c.f().d("CrashlyticsCore", "Deleting unknown file: " + name);
                file.delete();
            } else if (!set.contains(matcher.group(1))) {
                h.a.a.a.c.f().d("CrashlyticsCore", "Trimming session file: " + name);
                file.delete();
            }
        }
    }

    private File[] a(String str, File[] fileArr, int i2) {
        if (fileArr.length <= i2) {
            return fileArr;
        }
        h.a.a.a.c.f().d("CrashlyticsCore", String.format(Locale.US, "Trimming down to %d logged exceptions.", Integer.valueOf(i2)));
        a(str, i2);
        return a(new c0(str + "SessionEvent"));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f6421c.a(new e());
    }

    /* access modifiers changed from: package-private */
    public void a(File[] fileArr) {
        HashSet hashSet = new HashSet();
        for (File file : fileArr) {
            h.a.a.a.c.f().d("CrashlyticsCore", "Found invalid session part file: " + file);
            hashSet.add(a(file));
        }
        if (!hashSet.isEmpty()) {
            File d2 = d();
            if (!d2.exists()) {
                d2.mkdir();
            }
            for (File file2 : a(new f(this, hashSet))) {
                h.a.a.a.c.f().d("CrashlyticsCore", "Moving session file: " + file2);
                if (!file2.renameTo(new File(d2, file2.getName()))) {
                    h.a.a.a.c.f().d("CrashlyticsCore", "Could not move session file. Deleting " + file2);
                    file2.delete();
                }
            }
            r();
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context, File file, String str) throws IOException {
        byte[] b2 = g0.b(file);
        byte[] a2 = g0.a(file);
        byte[] b3 = g0.b(file, context);
        if (b2 == null || b2.length == 0) {
            h.a.a.a.l f2 = h.a.a.a.c.f();
            f2.a("CrashlyticsCore", "No minidump data found in directory " + file);
            return;
        }
        c(str, "<native-crash: minidump>");
        byte[] b4 = b(str, "BeginSession.json");
        byte[] b5 = b(str, "SessionApp.json");
        byte[] b6 = b(str, "SessionDevice.json");
        byte[] b7 = b(str, "SessionOS.json");
        byte[] d2 = g0.d(new c0(c()).b(str));
        a0 a0Var = new a0(this.f6420b.d(), this.f6427i, str);
        byte[] c2 = a0Var.c();
        a0Var.a();
        byte[] d3 = g0.d(new c0(c()).a(str));
        File file2 = new File(this.f6425g.a(), str);
        if (!file2.mkdir()) {
            h.a.a.a.c.f().d("CrashlyticsCore", "Couldn't create native sessions directory");
            return;
        }
        b(b2, new File(file2, "minidump"));
        b(a2, new File(file2, "metadata"));
        b(b3, new File(file2, "binaryImages"));
        b(b4, new File(file2, "session"));
        b(b5, new File(file2, TapjoyConstants.TJC_APP_PLACEMENT));
        b(b6, new File(file2, TapjoyConstants.TJC_NOTIFICATION_DEVICE_PREFIX));
        b(b7, new File(file2, "os"));
        b(d2, new File(file2, "user"));
        b(c2, new File(file2, "logs"));
        b(d3, new File(file2, "keys"));
    }

    /* access modifiers changed from: package-private */
    public boolean a(o oVar) {
        if (oVar == null) {
            return true;
        }
        return ((Boolean) this.f6421c.b(new g(oVar))).booleanValue();
    }

    private void a(byte[] bArr, File file) throws IOException {
        GZIPOutputStream gZIPOutputStream = null;
        try {
            GZIPOutputStream gZIPOutputStream2 = new GZIPOutputStream(new FileOutputStream(file));
            try {
                gZIPOutputStream2.write(bArr);
                gZIPOutputStream2.finish();
                h.a.a.a.n.b.i.a(gZIPOutputStream2);
            } catch (Throwable th) {
                th = th;
                gZIPOutputStream = gZIPOutputStream2;
                h.a.a.a.n.b.i.a(gZIPOutputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            h.a.a.a.n.b.i.a(gZIPOutputStream);
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void
     arg types: [com.crashlytics.android.e.f, java.lang.String]
     candidates:
      h.a.a.a.n.b.i.a(android.content.Context, boolean):int
      h.a.a.a.n.b.i.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      h.a.a.a.n.b.i.a(java.io.File, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.InputStream, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.lang.String, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(byte[], java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.Flushable, java.lang.String):void
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String):boolean
      h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void */
    /* access modifiers changed from: private */
    public void a(Date date, Thread thread, Throwable th) {
        f fVar;
        String o2 = o();
        g gVar = null;
        if (o2 == null) {
            h.a.a.a.c.f().b("CrashlyticsCore", "Tried to write a non-fatal exception while no session was open.", null);
            return;
        }
        d(o2, th.getClass().getName());
        try {
            h.a.a.a.c.f().d("CrashlyticsCore", "Crashlytics is logging non-fatal exception \"" + th + "\" from thread " + thread.getName());
            fVar = new f(c(), o2 + "SessionEvent" + h.a.a.a.n.b.i.b(this.f6419a.getAndIncrement()));
            try {
                g a2 = g.a(fVar);
                try {
                    a(a2, date, thread, th, "error", false);
                    h.a.a.a.n.b.i.a(a2, "Failed to flush to non-fatal file.");
                } catch (Exception e2) {
                    e = e2;
                    gVar = a2;
                    try {
                        h.a.a.a.c.f().b("CrashlyticsCore", "An error occurred in the non-fatal exception logger", e);
                        h.a.a.a.n.b.i.a(gVar, "Failed to flush to non-fatal file.");
                        h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close non-fatal file output stream.");
                        a(o2, 64);
                    } catch (Throwable th2) {
                        th = th2;
                        h.a.a.a.n.b.i.a(gVar, "Failed to flush to non-fatal file.");
                        h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close non-fatal file output stream.");
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    gVar = a2;
                    h.a.a.a.n.b.i.a(gVar, "Failed to flush to non-fatal file.");
                    h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close non-fatal file output stream.");
                    throw th;
                }
            } catch (Exception e3) {
                e = e3;
                h.a.a.a.c.f().b("CrashlyticsCore", "An error occurred in the non-fatal exception logger", e);
                h.a.a.a.n.b.i.a(gVar, "Failed to flush to non-fatal file.");
                h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close non-fatal file output stream.");
                a(o2, 64);
            }
        } catch (Exception e4) {
            e = e4;
            fVar = null;
            h.a.a.a.c.f().b("CrashlyticsCore", "An error occurred in the non-fatal exception logger", e);
            h.a.a.a.n.b.i.a(gVar, "Failed to flush to non-fatal file.");
            h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close non-fatal file output stream.");
            a(o2, 64);
        } catch (Throwable th4) {
            th = th4;
            fVar = null;
            h.a.a.a.n.b.i.a(gVar, "Failed to flush to non-fatal file.");
            h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close non-fatal file output stream.");
            throw th;
        }
        h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close non-fatal file output stream.");
        try {
            a(o2, 64);
        } catch (Exception e5) {
            h.a.a.a.c.f().b("CrashlyticsCore", "An error occurred when trimming non-fatal files.", e5);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void
     arg types: [com.crashlytics.android.e.f, java.lang.String]
     candidates:
      h.a.a.a.n.b.i.a(android.content.Context, boolean):int
      h.a.a.a.n.b.i.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      h.a.a.a.n.b.i.a(java.io.File, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.InputStream, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.lang.String, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(byte[], java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.Flushable, java.lang.String):void
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String):boolean
      h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void */
    private void a(String str, String str2, a0 a0Var) throws Exception {
        f fVar;
        g gVar = null;
        try {
            fVar = new f(c(), str + str2);
            try {
                gVar = g.a(fVar);
                a0Var.a(gVar);
                h.a.a.a.n.b.i.a(gVar, "Failed to flush to session " + str2 + " file.");
                h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close session " + str2 + " file.");
            } catch (Throwable th) {
                th = th;
                h.a.a.a.n.b.i.a(gVar, "Failed to flush to session " + str2 + " file.");
                h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close session " + str2 + " file.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fVar = null;
            h.a.a.a.n.b.i.a(gVar, "Failed to flush to session " + str2 + " file.");
            h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close session " + str2 + " file.");
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.FileOutputStream, java.lang.String]
     candidates:
      h.a.a.a.n.b.i.a(android.content.Context, boolean):int
      h.a.a.a.n.b.i.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      h.a.a.a.n.b.i.a(java.io.File, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.InputStream, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.lang.String, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(byte[], java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.Flushable, java.lang.String):void
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String):boolean
      h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void */
    private void a(String str, String str2, d0 d0Var) throws Exception {
        FileOutputStream fileOutputStream = null;
        try {
            File c2 = c();
            FileOutputStream fileOutputStream2 = new FileOutputStream(new File(c2, str + str2));
            try {
                d0Var.a(fileOutputStream2);
                h.a.a.a.n.b.i.a((Closeable) fileOutputStream2, "Failed to close " + str2 + " file.");
            } catch (Throwable th) {
                th = th;
                fileOutputStream = fileOutputStream2;
                h.a.a.a.n.b.i.a((Closeable) fileOutputStream, "Failed to close " + str2 + " file.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            h.a.a.a.n.b.i.a((Closeable) fileOutputStream, "Failed to close " + str2 + " file.");
            throw th;
        }
    }

    private void a(String str, Date date) throws Exception {
        String str2 = str;
        String format = String.format(Locale.US, "Crashlytics Android SDK/%s", this.f6420b.j());
        long time = date.getTime() / 1000;
        a(str, "BeginSession", new h(this, str2, format, time));
        a(str, "BeginSession.json", new i(this, str2, format, time));
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v2, resolved type: java.util.TreeMap} */
    /* JADX WARN: Type inference failed for: r6v2, types: [boolean] */
    /* JADX WARN: Type inference failed for: r6v4 */
    /* JADX WARN: Type inference failed for: r6v5 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.crashlytics.android.e.g r25, java.util.Date r26, java.lang.Thread r27, java.lang.Throwable r28, java.lang.String r29, boolean r30) throws java.lang.Exception {
        /*
            r24 = this;
            r0 = r24
            com.crashlytics.android.e.v0 r5 = new com.crashlytics.android.e.v0
            com.crashlytics.android.e.u0 r1 = r0.n
            r2 = r28
            r5.<init>(r2, r1)
            com.crashlytics.android.e.l r1 = r0.f6420b
            android.content.Context r1 = r1.d()
            long r2 = r26.getTime()
            r6 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r6
            java.lang.Float r16 = h.a.a.a.n.b.i.e(r1)
            com.crashlytics.android.e.w r4 = r0.m
            boolean r4 = r4.c()
            int r17 = h.a.a.a.n.b.i.a(r1, r4)
            boolean r18 = h.a.a.a.n.b.i.g(r1)
            android.content.res.Resources r4 = r1.getResources()
            android.content.res.Configuration r4 = r4.getConfiguration()
            int r13 = r4.orientation
            long r6 = h.a.a.a.n.b.i.b()
            long r8 = h.a.a.a.n.b.i.a(r1)
            long r19 = r6 - r8
            java.io.File r4 = android.os.Environment.getDataDirectory()
            java.lang.String r4 = r4.getPath()
            long r21 = h.a.a.a.n.b.i.a(r4)
            java.lang.String r4 = r1.getPackageName()
            android.app.ActivityManager$RunningAppProcessInfo r12 = h.a.a.a.n.b.i.a(r4, r1)
            java.util.LinkedList r9 = new java.util.LinkedList
            r9.<init>()
            java.lang.StackTraceElement[] r7 = r5.f6571c
            com.crashlytics.android.e.a r4 = r0.f6426h
            java.lang.String r15 = r4.f6370b
            h.a.a.a.n.b.s r4 = r0.f6423e
            java.lang.String r14 = r4.c()
            r4 = 0
            if (r30 == 0) goto L_0x00a1
            java.util.Map r8 = java.lang.Thread.getAllStackTraces()
            int r10 = r8.size()
            java.lang.Thread[] r10 = new java.lang.Thread[r10]
            java.util.Set r8 = r8.entrySet()
            java.util.Iterator r8 = r8.iterator()
        L_0x0078:
            boolean r11 = r8.hasNext()
            if (r11 == 0) goto L_0x009e
            java.lang.Object r11 = r8.next()
            java.util.Map$Entry r11 = (java.util.Map.Entry) r11
            java.lang.Object r23 = r11.getKey()
            java.lang.Thread r23 = (java.lang.Thread) r23
            r10[r4] = r23
            com.crashlytics.android.e.u0 r6 = r0.n
            java.lang.Object r11 = r11.getValue()
            java.lang.StackTraceElement[] r11 = (java.lang.StackTraceElement[]) r11
            java.lang.StackTraceElement[] r6 = r6.a(r11)
            r9.add(r6)
            r6 = 1
            int r4 = r4 + r6
            goto L_0x0078
        L_0x009e:
            r6 = 1
            r8 = r10
            goto L_0x00a5
        L_0x00a1:
            r6 = 1
            java.lang.Thread[] r4 = new java.lang.Thread[r4]
            r8 = r4
        L_0x00a5:
            java.lang.String r4 = "com.crashlytics.CollectCustomKeys"
            boolean r1 = h.a.a.a.n.b.i.a(r1, r4, r6)
            if (r1 != 0) goto L_0x00b3
            java.util.TreeMap r1 = new java.util.TreeMap
            r1.<init>()
            goto L_0x00c8
        L_0x00b3:
            com.crashlytics.android.e.l r1 = r0.f6420b
            java.util.Map r1 = r1.p()
            if (r1 == 0) goto L_0x00c8
            int r4 = r1.size()
            if (r4 <= r6) goto L_0x00c8
            java.util.TreeMap r4 = new java.util.TreeMap
            r4.<init>(r1)
            r10 = r4
            goto L_0x00c9
        L_0x00c8:
            r10 = r1
        L_0x00c9:
            com.crashlytics.android.e.a0 r11 = r0.f6428j
            r1 = r25
            r4 = r29
            r6 = r27
            com.crashlytics.android.e.r0.a(r1, r2, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r21)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crashlytics.android.e.k.a(com.crashlytics.android.e.g, java.util.Date, java.lang.Thread, java.lang.Throwable, java.lang.String, boolean):void");
    }

    private void a(File file, String str, int i2) {
        h.a.a.a.l f2 = h.a.a.a.c.f();
        f2.d("CrashlyticsCore", "Collecting session parts for ID " + str);
        File[] a2 = a(new c0(str + "SessionCrash"));
        boolean z2 = a2 != null && a2.length > 0;
        h.a.a.a.c.f().d("CrashlyticsCore", String.format(Locale.US, "Session %s has fatal exception: %s", str, Boolean.valueOf(z2)));
        File[] a3 = a(new c0(str + "SessionEvent"));
        boolean z3 = a3 != null && a3.length > 0;
        h.a.a.a.c.f().d("CrashlyticsCore", String.format(Locale.US, "Session %s has non-fatal exceptions: %s", str, Boolean.valueOf(z3)));
        if (z2 || z3) {
            a(file, str, a(str, a3, i2), z2 ? a2[0] : null);
        } else {
            h.a.a.a.l f3 = h.a.a.a.c.f();
            f3.d("CrashlyticsCore", "No events present for session ID " + str);
        }
        h.a.a.a.l f4 = h.a.a.a.c.f();
        f4.d("CrashlyticsCore", "Removing session part files for ID " + str);
        a(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void
     arg types: [com.crashlytics.android.e.f, java.lang.String]
     candidates:
      h.a.a.a.n.b.i.a(android.content.Context, boolean):int
      h.a.a.a.n.b.i.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      h.a.a.a.n.b.i.a(java.io.File, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.InputStream, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.lang.String, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(byte[], java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.Flushable, java.lang.String):void
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String):boolean
      h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void */
    private void a(File file, String str, File[] fileArr, File file2) {
        f fVar;
        String str2 = str;
        File file3 = file2;
        boolean z2 = file3 != null;
        File b2 = z2 ? b() : e();
        if (!b2.exists()) {
            b2.mkdirs();
        }
        try {
            fVar = new f(b2, str2);
            try {
                g a2 = g.a(fVar);
                h.a.a.a.c.f().d("CrashlyticsCore", "Collecting SessionStart data for session ID " + str2);
                a(a2, file);
                a2.a(4, new Date().getTime() / 1000);
                a2.a(5, z2);
                a2.d(11, 1);
                a2.a(12, 3);
                a(a2, str2);
                a(a2, fileArr, str2);
                if (z2) {
                    a(a2, file3);
                }
                h.a.a.a.n.b.i.a(a2, "Error flushing session file stream");
                h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close CLS file");
            } catch (Exception e2) {
                e = e2;
                try {
                    h.a.a.a.c.f().b("CrashlyticsCore", "Failed to write session file for session ID: " + str2, e);
                    h.a.a.a.n.b.i.a((Flushable) null, "Error flushing session file stream");
                    a(fVar);
                } catch (Throwable th) {
                    th = th;
                    h.a.a.a.n.b.i.a((Flushable) null, "Error flushing session file stream");
                    h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close CLS file");
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            fVar = null;
            h.a.a.a.c.f().b("CrashlyticsCore", "Failed to write session file for session ID: " + str2, e);
            h.a.a.a.n.b.i.a((Flushable) null, "Error flushing session file stream");
            a(fVar);
        } catch (Throwable th2) {
            th = th2;
            fVar = null;
            h.a.a.a.n.b.i.a((Flushable) null, "Error flushing session file stream");
            h.a.a.a.n.b.i.a((Closeable) fVar, "Failed to close CLS file");
            throw th;
        }
    }

    private static void a(g gVar, File[] fileArr, String str) {
        Arrays.sort(fileArr, h.a.a.a.n.b.i.f15070d);
        for (File file : fileArr) {
            try {
                h.a.a.a.c.f().d("CrashlyticsCore", String.format(Locale.US, "Found Non Fatal for session ID %s in %s ", str, file.getName()));
                a(gVar, file);
            } catch (Exception e2) {
                h.a.a.a.c.f().b("CrashlyticsCore", "Error writting non-fatal to session.", e2);
            }
        }
    }

    private void a(g gVar, String str) throws IOException {
        for (String str2 : z) {
            File[] a2 = a(new c0(str + str2 + ".cls"));
            if (a2.length == 0) {
                h.a.a.a.c.f().b("CrashlyticsCore", "Can't find " + str2 + " data for session ID " + str, null);
            } else {
                h.a.a.a.c.f().d("CrashlyticsCore", "Collecting " + str2 + " data for session ID " + str);
                a(gVar, a2[0]);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.FileInputStream, java.lang.String]
     candidates:
      h.a.a.a.n.b.i.a(android.content.Context, boolean):int
      h.a.a.a.n.b.i.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      h.a.a.a.n.b.i.a(java.io.File, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.InputStream, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.lang.String, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(byte[], java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.Flushable, java.lang.String):void
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String):boolean
      h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void */
    private static void a(g gVar, File file) throws IOException {
        FileInputStream fileInputStream;
        if (!file.exists()) {
            h.a.a.a.c.f().b("CrashlyticsCore", "Tried to include a file that doesn't exist: " + file.getName(), null);
            return;
        }
        try {
            fileInputStream = new FileInputStream(file);
            try {
                a(fileInputStream, gVar, (int) file.length());
                h.a.a.a.n.b.i.a((Closeable) fileInputStream, "Failed to close file input stream.");
            } catch (Throwable th) {
                th = th;
                h.a.a.a.n.b.i.a((Closeable) fileInputStream, "Failed to close file input stream.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            h.a.a.a.n.b.i.a((Closeable) fileInputStream, "Failed to close file input stream.");
            throw th;
        }
    }

    private static void a(InputStream inputStream, g gVar, int i2) throws IOException {
        int read;
        byte[] bArr = new byte[i2];
        int i3 = 0;
        while (i3 < bArr.length && (read = inputStream.read(bArr, i3, bArr.length - i3)) >= 0) {
            i3 += read;
        }
        gVar.a(bArr);
    }

    /* access modifiers changed from: package-private */
    public void a(h.a.a.a.n.g.u uVar) {
        if (uVar.f15317d.f15291d) {
            boolean a2 = this.p.a();
            h.a.a.a.l f2 = h.a.a.a.c.f();
            f2.d("CrashlyticsCore", "Registered Firebase Analytics event listener for breadcrumbs: " + a2);
        }
    }

    private t a(String str, String str2) {
        String b2 = h.a.a.a.n.b.i.b(this.f6420b.d(), "com.crashlytics.ApiEndpoint");
        return new h(new v(this.f6420b, b2, str, this.f6422d), new f0(this.f6420b, b2, str2, this.f6422d));
    }

    /* access modifiers changed from: private */
    public void a(long j2) {
        if (n()) {
            h.a.a.a.c.f().d("CrashlyticsCore", "Skipping logging Crashlytics event to Firebase, FirebaseCrash exists");
        } else if (this.q != null) {
            h.a.a.a.c.f().d("CrashlyticsCore", "Logging Crashlytics event to Firebase");
            Bundle bundle = new Bundle();
            bundle.putInt("_r", 1);
            bundle.putInt("fatal", 1);
            bundle.putLong("timestamp", j2);
            this.q.logEvent("clx", "_ae", bundle);
        } else {
            h.a.a.a.c.f().d("CrashlyticsCore", "Skipping logging Crashlytics event to Firebase, no Firebase Analytics");
        }
    }
}
