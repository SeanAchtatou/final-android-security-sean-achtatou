package com.crashlytics.android.e;

import android.content.Context;
import android.os.Bundle;
import h.a.a.a.c;
import h.a.a.a.l;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: DefaultAppMeasurementEventListenerRegistrar */
class u implements b {

    /* renamed from: c  reason: collision with root package name */
    private static final List<Class<?>> f6565c = Collections.unmodifiableList(Arrays.asList(String.class, String.class, Bundle.class, Long.class));
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final l f6566a;

    /* renamed from: b  reason: collision with root package name */
    private Object f6567b;

    /* compiled from: DefaultAppMeasurementEventListenerRegistrar */
    class a implements InvocationHandler {
        a() {
        }

        public boolean a(Object obj, Object obj2) {
            if (obj == obj2) {
                return true;
            }
            if (obj2 == null || !Proxy.isProxyClass(obj2.getClass()) || !super.equals(Proxy.getInvocationHandler(obj2))) {
                return false;
            }
            return true;
        }

        public Object invoke(Object obj, Method method, Object[] objArr) {
            String name = method.getName();
            if (objArr == null) {
                objArr = new Object[0];
            }
            if (objArr.length == 1 && name.equals("equals")) {
                return Boolean.valueOf(a(obj, objArr[0]));
            }
            if (objArr.length == 0 && name.equals("hashCode")) {
                return Integer.valueOf(super.hashCode());
            }
            if (objArr.length == 0 && name.equals("toString")) {
                return super.toString();
            }
            if (objArr.length == 4 && name.equals("onEvent") && u.a(objArr)) {
                String str = (String) objArr[0];
                String str2 = (String) objArr[1];
                Bundle bundle = (Bundle) objArr[2];
                if (str != null && !str.equals("crash")) {
                    u.b(u.this.f6566a, str2, bundle);
                    return null;
                }
            }
            StringBuilder sb = new StringBuilder("Unexpected method invoked on AppMeasurement.EventListener: " + name + "(");
            for (int i2 = 0; i2 < objArr.length; i2++) {
                if (i2 > 0) {
                    sb.append(", ");
                }
                sb.append(objArr[i2].getClass().getName());
            }
            sb.append("); returning null");
            c.f().b("CrashlyticsCore", sb.toString());
            return null;
        }
    }

    public u(l lVar) {
        this.f6566a = lVar;
    }

    private Object b(Class<?> cls) {
        try {
            return cls.getDeclaredMethod("getInstance", Context.class).invoke(cls, this.f6566a.d());
        } catch (Exception unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static void b(l lVar, String str, Bundle bundle) {
        try {
            lVar.a("$A$:" + a(str, bundle));
        } catch (JSONException unused) {
            l f2 = c.f();
            f2.a("CrashlyticsCore", "Unable to serialize Firebase Analytics event; " + str);
        }
    }

    public boolean a() {
        Class<?> a2 = a("com.google.android.gms.measurement.AppMeasurement");
        if (a2 == null) {
            c.f().d("CrashlyticsCore", "Firebase Analytics is not present; you will not see automatic logging of events before a crash occurs.");
            return false;
        }
        Object b2 = b(a2);
        if (b2 == null) {
            c.f().a("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: Could not create an instance of Firebase Analytics.");
            return false;
        }
        Class<?> a3 = a("com.google.android.gms.measurement.AppMeasurement$OnEventListener");
        if (a3 == null) {
            c.f().a("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: Could not get class com.google.android.gms.measurement.AppMeasurement$OnEventListener");
            return false;
        }
        try {
            a2.getDeclaredMethod("registerOnMeasurementEventListener", a3).invoke(b2, a(a3));
        } catch (NoSuchMethodException e2) {
            c.f().a("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: Method registerOnMeasurementEventListener not found.", e2);
            return false;
        } catch (Exception e3) {
            l f2 = c.f();
            f2.a("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: " + e3.getMessage(), e3);
        }
        return true;
    }

    private Class<?> a(String str) {
        try {
            return this.f6566a.d().getClassLoader().loadClass(str);
        } catch (Exception unused) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized Object a(Class cls) {
        if (this.f6567b == null) {
            this.f6567b = Proxy.newProxyInstance(this.f6566a.d().getClassLoader(), new Class[]{cls}, new a());
        }
        return this.f6567b;
    }

    static boolean a(Object[] objArr) {
        if (objArr.length != f6565c.size()) {
            return false;
        }
        Iterator<Class<?>> it = f6565c.iterator();
        for (Object obj : objArr) {
            if (!obj.getClass().equals(it.next())) {
                return false;
            }
        }
        return true;
    }

    private static String a(String str, Bundle bundle) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        for (String next : bundle.keySet()) {
            jSONObject2.put(next, bundle.get(next));
        }
        jSONObject.put("name", str);
        jSONObject.put("parameters", jSONObject2);
        return jSONObject.toString();
    }
}
