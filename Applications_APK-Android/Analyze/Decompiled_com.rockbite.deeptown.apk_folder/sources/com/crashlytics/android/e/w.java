package com.crashlytics.android.e;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: DevicePowerStateListener */
class w {

    /* renamed from: f  reason: collision with root package name */
    private static final IntentFilter f6573f = new IntentFilter("android.intent.action.BATTERY_CHANGED");

    /* renamed from: g  reason: collision with root package name */
    private static final IntentFilter f6574g = new IntentFilter("android.intent.action.ACTION_POWER_CONNECTED");

    /* renamed from: h  reason: collision with root package name */
    private static final IntentFilter f6575h = new IntentFilter("android.intent.action.ACTION_POWER_DISCONNECTED");

    /* renamed from: a  reason: collision with root package name */
    private final AtomicBoolean f6576a = new AtomicBoolean(false);

    /* renamed from: b  reason: collision with root package name */
    private final Context f6577b;

    /* renamed from: c  reason: collision with root package name */
    private final BroadcastReceiver f6578c = new b();

    /* renamed from: d  reason: collision with root package name */
    private final BroadcastReceiver f6579d = new a();
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public boolean f6580e;

    /* compiled from: DevicePowerStateListener */
    class a extends BroadcastReceiver {
        a() {
        }

        public void onReceive(Context context, Intent intent) {
            boolean unused = w.this.f6580e = true;
        }
    }

    /* compiled from: DevicePowerStateListener */
    class b extends BroadcastReceiver {
        b() {
        }

        public void onReceive(Context context, Intent intent) {
            boolean unused = w.this.f6580e = false;
        }
    }

    public w(Context context) {
        this.f6577b = context;
    }

    public void b() {
        boolean z = true;
        if (!this.f6576a.getAndSet(true)) {
            Intent registerReceiver = this.f6577b.registerReceiver(null, f6573f);
            int i2 = -1;
            if (registerReceiver != null) {
                i2 = registerReceiver.getIntExtra("status", -1);
            }
            if (!(i2 == 2 || i2 == 5)) {
                z = false;
            }
            this.f6580e = z;
            this.f6577b.registerReceiver(this.f6579d, f6574g);
            this.f6577b.registerReceiver(this.f6578c, f6575h);
        }
    }

    public boolean c() {
        return this.f6580e;
    }

    public void a() {
        if (this.f6576a.getAndSet(false)) {
            this.f6577b.unregisterReceiver(this.f6579d);
            this.f6577b.unregisterReceiver(this.f6578c);
        }
    }
}
