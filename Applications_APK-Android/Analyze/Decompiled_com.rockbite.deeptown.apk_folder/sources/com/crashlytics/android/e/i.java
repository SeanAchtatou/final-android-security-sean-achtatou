package com.crashlytics.android.e;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.ScrollView;
import android.widget.TextView;
import h.a.a.a.n.g.p;
import java.util.concurrent.CountDownLatch;

/* compiled from: CrashPromptDialog */
class i {

    /* renamed from: a  reason: collision with root package name */
    private final e f6406a;

    /* renamed from: b  reason: collision with root package name */
    private final AlertDialog.Builder f6407b;

    /* compiled from: CrashPromptDialog */
    static class a implements DialogInterface.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ e f6408a;

        a(e eVar) {
            this.f6408a = eVar;
        }

        public void onClick(DialogInterface dialogInterface, int i2) {
            this.f6408a.a(true);
            dialogInterface.dismiss();
        }
    }

    /* compiled from: CrashPromptDialog */
    static class b implements DialogInterface.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ e f6409a;

        b(e eVar) {
            this.f6409a = eVar;
        }

        public void onClick(DialogInterface dialogInterface, int i2) {
            this.f6409a.a(false);
            dialogInterface.dismiss();
        }
    }

    /* compiled from: CrashPromptDialog */
    static class c implements DialogInterface.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ d f6410a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ e f6411b;

        c(d dVar, e eVar) {
            this.f6410a = dVar;
            this.f6411b = eVar;
        }

        public void onClick(DialogInterface dialogInterface, int i2) {
            this.f6410a.a(true);
            this.f6411b.a(true);
            dialogInterface.dismiss();
        }
    }

    /* compiled from: CrashPromptDialog */
    interface d {
        void a(boolean z);
    }

    private i(AlertDialog.Builder builder, e eVar) {
        this.f6406a = eVar;
        this.f6407b = builder;
    }

    private static int a(float f2, int i2) {
        return (int) (f2 * ((float) i2));
    }

    public static i a(Activity activity, p pVar, d dVar) {
        e eVar = new e(null);
        x xVar = new x(activity, pVar);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        ScrollView a2 = a(activity, xVar.c());
        builder.setView(a2).setTitle(xVar.e()).setCancelable(false).setNeutralButton(xVar.d(), new a(eVar));
        if (pVar.f15299d) {
            builder.setNegativeButton(xVar.b(), new b(eVar));
        }
        if (pVar.f15301f) {
            builder.setPositiveButton(xVar.a(), new c(dVar, eVar));
        }
        return new i(builder, eVar);
    }

    public boolean b() {
        return this.f6406a.b();
    }

    public void c() {
        this.f6407b.show();
    }

    /* compiled from: CrashPromptDialog */
    private static class e {

        /* renamed from: a  reason: collision with root package name */
        private boolean f6412a;

        /* renamed from: b  reason: collision with root package name */
        private final CountDownLatch f6413b;

        private e() {
            this.f6412a = false;
            this.f6413b = new CountDownLatch(1);
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z) {
            this.f6412a = z;
            this.f6413b.countDown();
        }

        /* access modifiers changed from: package-private */
        public boolean b() {
            return this.f6412a;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            try {
                this.f6413b.await();
            } catch (InterruptedException unused) {
            }
        }

        /* synthetic */ e(a aVar) {
            this();
        }
    }

    private static ScrollView a(Activity activity, String str) {
        float f2 = activity.getResources().getDisplayMetrics().density;
        int a2 = a(f2, 5);
        TextView textView = new TextView(activity);
        textView.setAutoLinkMask(15);
        textView.setText(str);
        textView.setTextAppearance(activity, 16973892);
        textView.setPadding(a2, a2, a2, a2);
        textView.setFocusable(false);
        ScrollView scrollView = new ScrollView(activity);
        scrollView.setPadding(a(f2, 14), a(f2, 2), a(f2, 10), a(f2, 12));
        scrollView.addView(textView);
        return scrollView;
    }

    public void a() {
        this.f6406a.a();
    }
}
