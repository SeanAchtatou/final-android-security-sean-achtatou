package com.crashlytics.android.e;

import com.appsflyer.AppsFlyerProperties;
import h.a.a.a.c;
import h.a.a.a.n.b.i;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: MetaDataStore */
class c0 {

    /* renamed from: b  reason: collision with root package name */
    private static final Charset f6383b = Charset.forName("UTF-8");

    /* renamed from: a  reason: collision with root package name */
    private final File f6384a;

    /* compiled from: MetaDataStore */
    static class a extends JSONObject {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ x0 f6385a;

        a(x0 x0Var) throws JSONException {
            this.f6385a = x0Var;
            put("userId", this.f6385a.f6586a);
            put("userName", this.f6385a.f6587b);
            put(AppsFlyerProperties.USER_EMAIL, this.f6385a.f6588c);
        }
    }

    public c0(File file) {
        this.f6384a = file;
    }

    private static x0 d(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        return new x0(a(jSONObject, "userId"), a(jSONObject, "userName"), a(jSONObject, AppsFlyerProperties.USER_EMAIL));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.BufferedWriter, java.lang.String]
     candidates:
      h.a.a.a.n.b.i.a(android.content.Context, boolean):int
      h.a.a.a.n.b.i.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      h.a.a.a.n.b.i.a(java.io.File, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.InputStream, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.lang.String, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(byte[], java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.Flushable, java.lang.String):void
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String):boolean
      h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void */
    public void a(String str, x0 x0Var) {
        File b2 = b(str);
        BufferedWriter bufferedWriter = null;
        try {
            String a2 = a(x0Var);
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(b2), f6383b));
            try {
                bufferedWriter2.write(a2);
                bufferedWriter2.flush();
                i.a((Closeable) bufferedWriter2, "Failed to close user metadata file.");
            } catch (Exception e2) {
                e = e2;
                bufferedWriter = bufferedWriter2;
                try {
                    c.f().b("CrashlyticsCore", "Error serializing user metadata.", e);
                    i.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
                } catch (Throwable th) {
                    th = th;
                    i.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                i.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            c.f().b("CrashlyticsCore", "Error serializing user metadata.", e);
            i.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
        }
    }

    public File b(String str) {
        File file = this.f6384a;
        return new File(file, str + "user" + ".meta");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.FileInputStream, java.lang.String]
     candidates:
      h.a.a.a.n.b.i.a(android.content.Context, boolean):int
      h.a.a.a.n.b.i.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      h.a.a.a.n.b.i.a(java.io.File, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.InputStream, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.lang.String, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(byte[], java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.Flushable, java.lang.String):void
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String):boolean
      h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void */
    public x0 c(String str) {
        File b2 = b(str);
        if (!b2.exists()) {
            return x0.f6585d;
        }
        FileInputStream fileInputStream = null;
        try {
            FileInputStream fileInputStream2 = new FileInputStream(b2);
            try {
                x0 d2 = d(i.b(fileInputStream2));
                i.a((Closeable) fileInputStream2, "Failed to close user metadata file.");
                return d2;
            } catch (Exception e2) {
                e = e2;
                fileInputStream = fileInputStream2;
                try {
                    c.f().b("CrashlyticsCore", "Error deserializing user metadata.", e);
                    i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    return x0.f6585d;
                } catch (Throwable th) {
                    th = th;
                    i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = fileInputStream2;
                i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            c.f().b("CrashlyticsCore", "Error deserializing user metadata.", e);
            i.a((Closeable) fileInputStream, "Failed to close user metadata file.");
            return x0.f6585d;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.BufferedWriter, java.lang.String]
     candidates:
      h.a.a.a.n.b.i.a(android.content.Context, boolean):int
      h.a.a.a.n.b.i.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      h.a.a.a.n.b.i.a(java.io.File, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.InputStream, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.lang.String, java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(byte[], java.lang.String):java.lang.String
      h.a.a.a.n.b.i.a(java.io.Flushable, java.lang.String):void
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String):boolean
      h.a.a.a.n.b.i.a(java.io.Closeable, java.lang.String):void */
    public void a(String str, Map<String, String> map) {
        File a2 = a(str);
        BufferedWriter bufferedWriter = null;
        try {
            String a3 = a(map);
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(a2), f6383b));
            try {
                bufferedWriter2.write(a3);
                bufferedWriter2.flush();
                i.a((Closeable) bufferedWriter2, "Failed to close key/value metadata file.");
            } catch (Exception e2) {
                e = e2;
                bufferedWriter = bufferedWriter2;
                try {
                    c.f().b("CrashlyticsCore", "Error serializing key/value metadata.", e);
                    i.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
                } catch (Throwable th) {
                    th = th;
                    i.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                i.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            c.f().b("CrashlyticsCore", "Error serializing key/value metadata.", e);
            i.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
        }
    }

    public File a(String str) {
        File file = this.f6384a;
        return new File(file, str + "keys" + ".meta");
    }

    private static String a(x0 x0Var) throws JSONException {
        return new a(x0Var).toString();
    }

    private static String a(Map<String, String> map) throws JSONException {
        return new JSONObject(map).toString();
    }

    private static String a(JSONObject jSONObject, String str) {
        if (!jSONObject.isNull(str)) {
            return jSONObject.optString(str, null);
        }
        return null;
    }
}
