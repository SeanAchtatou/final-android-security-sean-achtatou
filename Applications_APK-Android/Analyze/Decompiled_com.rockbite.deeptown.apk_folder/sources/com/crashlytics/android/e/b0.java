package com.crashlytics.android.e;

import android.content.Context;
import android.os.Bundle;

/* compiled from: ManifestUnityVersionProvider */
class b0 implements w0 {

    /* renamed from: a  reason: collision with root package name */
    private final Context f6379a;

    /* renamed from: b  reason: collision with root package name */
    private final String f6380b;

    public b0(Context context, String str) {
        this.f6379a = context;
        this.f6380b = str;
    }

    public String a() {
        try {
            Bundle bundle = this.f6379a.getPackageManager().getApplicationInfo(this.f6380b, 128).metaData;
            if (bundle != null) {
                return bundle.getString("io.fabric.unity.crashlytics.version");
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }
}
