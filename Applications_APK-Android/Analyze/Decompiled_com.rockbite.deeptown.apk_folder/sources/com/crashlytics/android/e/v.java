package com.crashlytics.android.e;

import h.a.a.a.i;
import h.a.a.a.l;
import h.a.a.a.n.b.a;
import h.a.a.a.n.e.c;
import h.a.a.a.n.e.d;
import h.a.a.a.n.e.e;
import java.io.File;
import java.util.Map;

/* compiled from: DefaultCreateReportSpiCall */
class v extends a implements t {
    public v(i iVar, String str, String str2, e eVar) {
        super(iVar, str, str2, eVar, c.POST);
    }

    public boolean a(s sVar) {
        d a2 = a();
        a(a2, sVar);
        a(a2, sVar.f6561b);
        l f2 = h.a.a.a.c.f();
        f2.d("CrashlyticsCore", "Sending report to: " + b());
        int g2 = a2.g();
        l f3 = h.a.a.a.c.f();
        f3.d("CrashlyticsCore", "Create report request ID: " + a2.c("X-REQUEST-ID"));
        l f4 = h.a.a.a.c.f();
        f4.d("CrashlyticsCore", "Result was: " + g2);
        return h.a.a.a.n.b.v.a(g2) == 0;
    }

    private d a(d dVar, s sVar) {
        dVar.c("X-CRASHLYTICS-API-KEY", sVar.f6560a);
        dVar.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        dVar.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.f15055e.j());
        for (Map.Entry<String, String> a2 : sVar.f6561b.a().entrySet()) {
            dVar.a(a2);
        }
        return dVar;
    }

    private d a(d dVar, o0 o0Var) {
        dVar.e("report[identifier]", o0Var.b());
        if (o0Var.d().length == 1) {
            h.a.a.a.c.f().d("CrashlyticsCore", "Adding single file " + o0Var.e() + " to report " + o0Var.b());
            dVar.a("report[file]", o0Var.e(), "application/octet-stream", o0Var.c());
            return dVar;
        }
        int i2 = 0;
        for (File file : o0Var.d()) {
            h.a.a.a.c.f().d("CrashlyticsCore", "Adding file " + file.getName() + " to report " + o0Var.b());
            StringBuilder sb = new StringBuilder();
            sb.append("report[file");
            sb.append(i2);
            sb.append("]");
            dVar.a(sb.toString(), file.getName(), "application/octet-stream", file);
            i2++;
        }
        return dVar;
    }
}
