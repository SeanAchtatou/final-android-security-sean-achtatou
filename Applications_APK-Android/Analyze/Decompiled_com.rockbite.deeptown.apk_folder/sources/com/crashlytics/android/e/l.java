package com.crashlytics.android.e;

import android.content.Context;
import android.util.Log;
import com.appsflyer.share.Constants;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import h.a.a.a.i;
import h.a.a.a.n.b.g;
import h.a.a.a.n.b.o;
import h.a.a.a.n.b.r;
import h.a.a.a.n.b.s;
import h.a.a.a.n.c.f;
import h.a.a.a.n.c.h;
import h.a.a.a.n.c.m;
import h.a.a.a.n.c.n;
import h.a.a.a.n.g.u;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@h.a.a.a.n.c.e({p.class})
/* compiled from: CrashlyticsCore */
public class l extends i<Void> {

    /* renamed from: g  reason: collision with root package name */
    private final long f6513g;

    /* renamed from: h  reason: collision with root package name */
    private final ConcurrentHashMap<String, String> f6514h;
    /* access modifiers changed from: private */

    /* renamed from: i  reason: collision with root package name */
    public m f6515i;

    /* renamed from: j  reason: collision with root package name */
    private m f6516j;

    /* renamed from: k  reason: collision with root package name */
    private n f6517k;
    private k l;
    private String m;
    private String n;
    private String o;
    private float p;
    private boolean q;
    private final i0 r;
    private h.a.a.a.n.e.e s;
    private j t;
    private p u;

    /* compiled from: CrashlyticsCore */
    class a extends h<Void> {
        a() {
        }

        public f getPriority() {
            return f.IMMEDIATE;
        }

        public Void call() throws Exception {
            return l.this.c();
        }
    }

    /* compiled from: CrashlyticsCore */
    class b implements Callable<Void> {
        b() {
        }

        public Void call() throws Exception {
            l.this.f6515i.a();
            h.a.a.a.c.f().d("CrashlyticsCore", "Initialization marker file created.");
            return null;
        }
    }

    /* compiled from: CrashlyticsCore */
    class c implements Callable<Boolean> {
        c() {
        }

        public Boolean call() throws Exception {
            try {
                boolean c2 = l.this.f6515i.c();
                h.a.a.a.l f2 = h.a.a.a.c.f();
                f2.d("CrashlyticsCore", "Initialization marker file removed: " + c2);
                return Boolean.valueOf(c2);
            } catch (Exception e2) {
                h.a.a.a.c.f().b("CrashlyticsCore", "Problem encountered deleting Crashlytics initialization marker.", e2);
                return false;
            }
        }
    }

    /* compiled from: CrashlyticsCore */
    private static final class d implements Callable<Boolean> {

        /* renamed from: a  reason: collision with root package name */
        private final m f6521a;

        public d(m mVar) {
            this.f6521a = mVar;
        }

        public Boolean call() throws Exception {
            if (!this.f6521a.b()) {
                return Boolean.FALSE;
            }
            h.a.a.a.c.f().d("CrashlyticsCore", "Found previous crash marker.");
            this.f6521a.c();
            return Boolean.TRUE;
        }
    }

    /* compiled from: CrashlyticsCore */
    private static final class e implements n {
        private e() {
        }

        public void a() {
        }

        /* synthetic */ e(a aVar) {
            this();
        }
    }

    public l() {
        this(1.0f, null, null, false);
    }

    private static String d(String str) {
        if (str == null) {
            return str;
        }
        String trim = str.trim();
        return trim.length() > 1024 ? trim.substring(0, 1024) : trim;
    }

    private void w() {
        if (Boolean.TRUE.equals((Boolean) this.t.b(new d(this.f6516j)))) {
            try {
                this.f6517k.a();
            } catch (Exception e2) {
                h.a.a.a.c.f().b("CrashlyticsCore", "Exception thrown by CrashlyticsListener while notifying of previous crash.", e2);
            }
        }
    }

    private void x() {
        a aVar = new a();
        for (m a2 : e()) {
            aVar.a(a2);
        }
        Future submit = f().b().submit(aVar);
        h.a.a.a.c.f().d("CrashlyticsCore", "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");
        try {
            submit.get(4, TimeUnit.SECONDS);
        } catch (InterruptedException e2) {
            h.a.a.a.c.f().b("CrashlyticsCore", "Crashlytics was interrupted during initialization.", e2);
        } catch (ExecutionException e3) {
            h.a.a.a.c.f().b("CrashlyticsCore", "Problem encountered during Crashlytics initialization.", e3);
        } catch (TimeoutException e4) {
            h.a.a.a.c.f().b("CrashlyticsCore", "Crashlytics timed out during initialization.", e4);
        }
    }

    public static l y() {
        return (l) h.a.a.a.c.a(l.class);
    }

    public void b(String str) {
        if (!this.q && c("prior to setting user data.")) {
            this.m = d(str);
            this.l.a(this.m, this.o, this.n);
        }
    }

    public String h() {
        return "com.crashlytics.sdk.android.crashlytics-core";
    }

    public String j() {
        return "2.7.0.33";
    }

    /* access modifiers changed from: protected */
    public boolean m() {
        return a(super.d());
    }

    /* access modifiers changed from: package-private */
    public void n() {
        this.f6516j.a();
    }

    /* access modifiers changed from: package-private */
    public boolean o() {
        return this.f6515i.b();
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> p() {
        return Collections.unmodifiableMap(this.f6514h);
    }

    /* access modifiers changed from: package-private */
    public o q() {
        p pVar = this.u;
        if (pVar != null) {
            return pVar.a();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public String r() {
        if (g().a()) {
            return this.n;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public String s() {
        if (g().a()) {
            return this.m;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public String t() {
        if (g().a()) {
            return this.o;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void u() {
        this.t.a(new c());
    }

    /* access modifiers changed from: package-private */
    public void v() {
        this.t.b(new b());
    }

    l(float f2, n nVar, i0 i0Var, boolean z) {
        this(f2, nVar, i0Var, z, o.a("Crashlytics Exception Handler"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: h.a.a.a.n.b.i.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String, java.lang.String):int
      h.a.a.a.n.b.i.a(java.lang.String, java.lang.String, int):long
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String, java.lang.Throwable):void
      h.a.a.a.n.b.i.a(java.io.InputStream, java.io.OutputStream, byte[]):void
      h.a.a.a.n.b.i.a(android.content.Context, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean a(Context context) {
        String d2;
        Context context2 = context;
        if (!h.a.a.a.n.b.l.a(context).a()) {
            h.a.a.a.c.f().d("CrashlyticsCore", "Crashlytics is disabled, because data collection is disabled by Firebase.");
            this.q = true;
        }
        if (this.q || (d2 = new g().d(context2)) == null) {
            return false;
        }
        String n2 = h.a.a.a.n.b.i.n(context);
        if (a(n2, h.a.a.a.n.b.i.a(context2, "com.crashlytics.RequireBuildId", true))) {
            try {
                h.a.a.a.l f2 = h.a.a.a.c.f();
                f2.e("CrashlyticsCore", "Initializing Crashlytics Core " + j());
                h.a.a.a.n.f.b bVar = new h.a.a.a.n.f.b(this);
                this.f6516j = new m("crash_marker", bVar);
                this.f6515i = new m("initialization_marker", bVar);
                j0 a2 = j0.a(new h.a.a.a.n.f.d(d(), "com.crashlytics.android.core.CrashlyticsCore"), this);
                q qVar = this.r != null ? new q(this.r) : null;
                this.s = new h.a.a.a.n.e.b(h.a.a.a.c.f());
                this.s.a(qVar);
                s g2 = g();
                a a3 = a.a(context2, g2, d2, n2);
                k kVar = r1;
                k kVar2 = new k(this, this.t, this.s, g2, a2, bVar, a3, new q0(context2, new b0(context2, a3.f6372d)), new u(this), com.crashlytics.android.c.i.b(context));
                this.l = kVar;
                boolean o2 = o();
                w();
                this.l.a(Thread.getDefaultUncaughtExceptionHandler(), new r().e(context2));
                if (!o2 || !h.a.a.a.n.b.i.b(context)) {
                    h.a.a.a.c.f().d("CrashlyticsCore", "Exception handling initialization successful");
                    return true;
                }
                h.a.a.a.c.f().d("CrashlyticsCore", "Crashlytics did not finish previous background initialization. Initializing synchronously.");
                x();
                return false;
            } catch (Exception e2) {
                h.a.a.a.c.f().b("CrashlyticsCore", "Crashlytics was not started due to an exception during initialization", e2);
                this.l = null;
                return false;
            }
        } else {
            throw new n("The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
        }
    }

    /* access modifiers changed from: protected */
    public Void c() {
        v();
        this.l.a();
        try {
            this.l.k();
            u a2 = h.a.a.a.n.g.r.d().a();
            if (a2 == null) {
                h.a.a.a.c.f().a("CrashlyticsCore", "Received null settings, skipping report submission!");
                u();
                return null;
            }
            this.l.a(a2);
            if (!a2.f15317d.f15289b) {
                h.a.a.a.c.f().d("CrashlyticsCore", "Collection of crash reports disabled in Crashlytics settings.");
                u();
                return null;
            } else if (!h.a.a.a.n.b.l.a(d()).a()) {
                h.a.a.a.c.f().d("CrashlyticsCore", "Automatic collection of crash reports disabled by Firebase settings.");
                u();
                return null;
            } else {
                o q2 = q();
                if (q2 != null && !this.l.a(q2)) {
                    h.a.a.a.c.f().d("CrashlyticsCore", "Could not finalize previous NDK sessions.");
                }
                if (!this.l.b(a2.f15315b)) {
                    h.a.a.a.c.f().d("CrashlyticsCore", "Could not finalize previous sessions.");
                }
                this.l.a(this.p, a2);
                u();
                return null;
            }
        } catch (Exception e2) {
            h.a.a.a.c.f().b("CrashlyticsCore", "Crashlytics encountered a problem during asynchronous initialization.", e2);
        } catch (Throwable th) {
            u();
            throw th;
        }
    }

    l(float f2, n nVar, i0 i0Var, boolean z, ExecutorService executorService) {
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = f2;
        this.f6517k = nVar == null ? new e(null) : nVar;
        this.r = i0Var;
        this.q = z;
        this.t = new j(executorService);
        this.f6514h = new ConcurrentHashMap<>();
        this.f6513g = System.currentTimeMillis();
    }

    private static String b(int i2, String str, String str2) {
        return h.a.a.a.n.b.i.a(i2) + Constants.URL_PATH_DELIMITER + str + RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER + str2;
    }

    private static boolean c(String str) {
        l y = y();
        if (y != null && y.l != null) {
            return true;
        }
        h.a.a.a.l f2 = h.a.a.a.c.f();
        f2.b("CrashlyticsCore", "Crashlytics must be initialized by calling Fabric.with(Context) " + str, null);
        return false;
    }

    public void a(Throwable th) {
        if (this.q || !c("prior to logging exceptions.")) {
            return;
        }
        if (th == null) {
            h.a.a.a.c.f().a(5, "CrashlyticsCore", "Crashlytics is ignoring a request to log a null exception.");
        } else {
            this.l.a(Thread.currentThread(), th);
        }
    }

    public void a(String str) {
        a(3, "CrashlyticsCore", str);
    }

    private void a(int i2, String str, String str2) {
        if (!this.q && c("prior to logging messages.")) {
            this.l.a(System.currentTimeMillis() - this.f6513g, b(i2, str, str2));
        }
    }

    public void a(String str, String str2) {
        String str3;
        if (this.q || !c("prior to setting keys.")) {
            return;
        }
        if (str == null) {
            Context d2 = d();
            if (d2 == null || !h.a.a.a.n.b.i.j(d2)) {
                h.a.a.a.c.f().b("CrashlyticsCore", "Attempting to set custom attribute with null key, ignoring.", null);
                return;
            }
            throw new IllegalArgumentException("Custom attribute key must not be null.");
        }
        String d3 = d(str);
        if (this.f6514h.size() < 64 || this.f6514h.containsKey(d3)) {
            if (str2 == null) {
                str3 = "";
            } else {
                str3 = d(str2);
            }
            this.f6514h.put(d3, str3);
            this.l.a(this.f6514h);
            return;
        }
        h.a.a.a.c.f().d("CrashlyticsCore", "Exceeded maximum number of custom attributes (64)");
    }

    static boolean a(String str, boolean z) {
        if (!z) {
            h.a.a.a.c.f().d("CrashlyticsCore", "Configured not to require a build ID.");
            return true;
        } else if (!h.a.a.a.n.b.i.b(str)) {
            return true;
        } else {
            Log.e("CrashlyticsCore", ".");
            Log.e("CrashlyticsCore", ".     |  | ");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".   \\ |  | /");
            Log.e("CrashlyticsCore", ".    \\    /");
            Log.e("CrashlyticsCore", ".     \\  /");
            Log.e("CrashlyticsCore", ".      \\/");
            Log.e("CrashlyticsCore", ".");
            Log.e("CrashlyticsCore", "The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
            Log.e("CrashlyticsCore", ".");
            Log.e("CrashlyticsCore", ".      /\\");
            Log.e("CrashlyticsCore", ".     /  \\");
            Log.e("CrashlyticsCore", ".    /    \\");
            Log.e("CrashlyticsCore", ".   / |  | \\");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".");
            return false;
        }
    }
}
