package com.crashlytics.android.e;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import h.a.a.a.n.b.s;

/* compiled from: AppData */
class a {

    /* renamed from: a  reason: collision with root package name */
    public final String f6369a;

    /* renamed from: b  reason: collision with root package name */
    public final String f6370b;

    /* renamed from: c  reason: collision with root package name */
    public final String f6371c;

    /* renamed from: d  reason: collision with root package name */
    public final String f6372d;

    /* renamed from: e  reason: collision with root package name */
    public final String f6373e;

    /* renamed from: f  reason: collision with root package name */
    public final String f6374f;

    a(String str, String str2, String str3, String str4, String str5, String str6) {
        this.f6369a = str;
        this.f6370b = str2;
        this.f6371c = str3;
        this.f6372d = str4;
        this.f6373e = str5;
        this.f6374f = str6;
    }

    public static a a(Context context, s sVar, String str, String str2) throws PackageManager.NameNotFoundException {
        String packageName = context.getPackageName();
        String f2 = sVar.f();
        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
        String num = Integer.toString(packageInfo.versionCode);
        String str3 = packageInfo.versionName;
        if (str3 == null) {
            str3 = "0.0";
        }
        return new a(str, str2, f2, packageName, num, str3);
    }
}
