package com.android.billingclient.api;

/* compiled from: BillingResult */
public final class a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f3150a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public String f3151b;

    /* compiled from: BillingResult */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        private int f3152a;

        /* renamed from: b  reason: collision with root package name */
        private String f3153b;

        public b a(int i2) {
            this.f3152a = i2;
            return this;
        }

        private b() {
        }

        public b a(String str) {
            this.f3153b = str;
            return this;
        }

        public a a() {
            a aVar = new a();
            int unused = aVar.f3150a = this.f3152a;
            String unused2 = aVar.f3151b = this.f3153b;
            return aVar;
        }
    }

    public static b b() {
        return new b();
    }

    public int a() {
        return this.f3150a;
    }
}
