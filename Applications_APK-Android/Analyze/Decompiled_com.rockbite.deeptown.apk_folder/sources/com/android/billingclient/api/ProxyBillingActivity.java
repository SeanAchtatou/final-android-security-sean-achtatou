package com.android.billingclient.api;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.ResultReceiver;
import e.b.a.a.a;

public class ProxyBillingActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private ResultReceiver f3149a;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 100) {
            int b2 = a.b(intent, "ProxyBillingActivity");
            if (!(i3 == -1 && b2 == 0)) {
                a.b("ProxyBillingActivity", "Activity finished with resultCode " + i3 + " and billing's responseCode: " + b2);
            }
            this.f3149a.send(b2, intent == null ? null : intent.getExtras());
        } else {
            a.b("ProxyBillingActivity", "Got onActivityResult with wrong requestCode: " + i2 + "; skipping...");
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        PendingIntent pendingIntent;
        super.onCreate(bundle);
        if (bundle == null) {
            a.a("ProxyBillingActivity", "Launching Play Store billing flow");
            this.f3149a = (ResultReceiver) getIntent().getParcelableExtra("result_receiver");
            if (getIntent().hasExtra("BUY_INTENT")) {
                pendingIntent = (PendingIntent) getIntent().getParcelableExtra("BUY_INTENT");
            } else {
                pendingIntent = getIntent().hasExtra("SUBS_MANAGEMENT_INTENT") ? (PendingIntent) getIntent().getParcelableExtra("SUBS_MANAGEMENT_INTENT") : null;
            }
            try {
                startIntentSenderForResult(pendingIntent.getIntentSender(), 100, new Intent(), 0, 0, 0);
            } catch (IntentSender.SendIntentException e2) {
                a.b("ProxyBillingActivity", "Got exception while trying to start a purchase flow: " + e2);
                this.f3149a.send(6, null);
                finish();
            }
        } else {
            a.a("ProxyBillingActivity", "Launching Play Store billing flow from savedInstanceState");
            this.f3149a = (ResultReceiver) bundle.getParcelable("result_receiver");
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putParcelable("result_receiver", this.f3149a);
    }
}
