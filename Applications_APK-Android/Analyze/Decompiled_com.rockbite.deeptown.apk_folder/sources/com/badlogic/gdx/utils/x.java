package com.badlogic.gdx.utils;

import e.d.b.g;

/* compiled from: Logger */
public class x {

    /* renamed from: a  reason: collision with root package name */
    private final String f5677a;

    /* renamed from: b  reason: collision with root package name */
    private int f5678b;

    public x(String str, int i2) {
        this.f5677a = str;
        this.f5678b = i2;
    }

    public void a(String str) {
        if (this.f5678b >= 3) {
            g.f6800a.a(this.f5677a, str);
        }
    }

    public void b(String str) {
        if (this.f5678b >= 1) {
            g.f6800a.c(this.f5677a, str);
        }
    }

    public void c(String str) {
        if (this.f5678b >= 2) {
            g.f6800a.b(this.f5677a, str);
        }
    }

    public void a(String str, Throwable th) {
        if (this.f5678b >= 1) {
            g.f6800a.a(this.f5677a, str, th);
        }
    }

    public int a() {
        return this.f5678b;
    }
}
