package com.badlogic.gdx.utils;

import com.google.android.gms.common.api.Api;

/* compiled from: Pool */
public abstract class f0<T> {
    private final a<T> freeObjects;
    public final int max;
    public int peak;

    /* compiled from: Pool */
    public interface a {
        void reset();
    }

    public f0() {
        this(16, Api.BaseClientBuilder.API_PRIORITY_OTHER);
    }

    public void clear() {
        this.freeObjects.clear();
    }

    public void free(T t) {
        if (t != null) {
            a<T> aVar = this.freeObjects;
            if (aVar.f5374b < this.max) {
                aVar.add(t);
                this.peak = Math.max(this.peak, this.freeObjects.f5374b);
            }
            reset(t);
            return;
        }
        throw new IllegalArgumentException("object cannot be null.");
    }

    public void freeAll(a<T> aVar) {
        if (aVar != null) {
            a<T> aVar2 = this.freeObjects;
            int i2 = this.max;
            for (int i3 = 0; i3 < aVar.f5374b; i3++) {
                T t = aVar.get(i3);
                if (t != null) {
                    if (aVar2.f5374b < i2) {
                        aVar2.add(t);
                    }
                    reset(t);
                }
            }
            this.peak = Math.max(this.peak, aVar2.f5374b);
            return;
        }
        throw new IllegalArgumentException("objects cannot be null.");
    }

    public int getFree() {
        return this.freeObjects.f5374b;
    }

    /* access modifiers changed from: protected */
    public abstract T newObject();

    public T obtain() {
        a<T> aVar = this.freeObjects;
        return aVar.f5374b == 0 ? newObject() : aVar.pop();
    }

    /* access modifiers changed from: protected */
    public void reset(T t) {
        if (t instanceof a) {
            ((a) t).reset();
        }
    }

    public f0(int i2) {
        this(i2, Api.BaseClientBuilder.API_PRIORITY_OTHER);
    }

    public f0(int i2, int i3) {
        this.freeObjects = new a<>(false, i2);
        this.max = i3;
    }
}
