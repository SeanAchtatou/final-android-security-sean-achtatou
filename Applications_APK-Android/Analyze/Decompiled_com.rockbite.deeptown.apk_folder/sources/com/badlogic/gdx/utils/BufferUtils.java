package com.badlogic.gdx.utils;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

public final class BufferUtils {

    /* renamed from: a  reason: collision with root package name */
    static a<ByteBuffer> f5371a = new a<>();

    /* renamed from: b  reason: collision with root package name */
    static int f5372b = 0;

    public static void a(float[] fArr, Buffer buffer, int i2, int i3) {
        if (buffer instanceof ByteBuffer) {
            buffer.limit(i2 << 2);
        } else if (buffer instanceof FloatBuffer) {
            buffer.limit(i2);
        }
        copyJni(fArr, buffer, i2, i3);
        buffer.position(0);
    }

    private static int b(Buffer buffer, int i2) {
        if (buffer instanceof ByteBuffer) {
            return i2;
        }
        if (buffer instanceof ShortBuffer) {
            return i2 << 1;
        }
        if (buffer instanceof CharBuffer) {
            return i2 << 1;
        }
        if (buffer instanceof IntBuffer) {
            return i2 << 2;
        }
        if (buffer instanceof LongBuffer) {
            return i2 << 3;
        }
        if (buffer instanceof FloatBuffer) {
            return i2 << 2;
        }
        if (buffer instanceof DoubleBuffer) {
            return i2 << 3;
        }
        throw new o("Can't copy to a " + buffer.getClass().getName() + " instance");
    }

    public static IntBuffer c(int i2) {
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(i2 * 4);
        allocateDirect.order(ByteOrder.nativeOrder());
        return allocateDirect.asIntBuffer();
    }

    private static native void copyJni(Buffer buffer, int i2, Buffer buffer2, int i3, int i4);

    private static native void copyJni(byte[] bArr, int i2, Buffer buffer, int i3, int i4);

    private static native void copyJni(float[] fArr, Buffer buffer, int i2, int i3);

    public static ByteBuffer d(int i2) {
        ByteBuffer newDisposableByteBuffer = newDisposableByteBuffer(i2);
        newDisposableByteBuffer.order(ByteOrder.nativeOrder());
        f5372b += i2;
        synchronized (f5371a) {
            f5371a.add(newDisposableByteBuffer);
        }
        return newDisposableByteBuffer;
    }

    private static native void freeMemory(ByteBuffer byteBuffer);

    private static native ByteBuffer newDisposableByteBuffer(int i2);

    public static void a(byte[] bArr, int i2, Buffer buffer, int i3) {
        buffer.limit(buffer.position() + a(buffer, i3));
        copyJni(bArr, i2, buffer, a(buffer), i3);
    }

    public static void a(Buffer buffer, Buffer buffer2, int i2) {
        int b2 = b(buffer, i2);
        buffer2.limit(buffer2.position() + a(buffer2, b2));
        copyJni(buffer, a(buffer), buffer2, a(buffer2), b2);
    }

    public static FloatBuffer b(int i2) {
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(i2 * 4);
        allocateDirect.order(ByteOrder.nativeOrder());
        return allocateDirect.asFloatBuffer();
    }

    private static int a(Buffer buffer) {
        if (buffer instanceof ByteBuffer) {
            return buffer.position();
        }
        if (buffer instanceof ShortBuffer) {
            return buffer.position() << 1;
        }
        if (buffer instanceof CharBuffer) {
            return buffer.position() << 1;
        }
        if (buffer instanceof IntBuffer) {
            return buffer.position() << 2;
        }
        if (buffer instanceof LongBuffer) {
            return buffer.position() << 3;
        }
        if (buffer instanceof FloatBuffer) {
            return buffer.position() << 2;
        }
        if (buffer instanceof DoubleBuffer) {
            return buffer.position() << 3;
        }
        throw new o("Can't copy to a " + buffer.getClass().getName() + " instance");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.a.a(java.lang.Object, boolean):boolean
     arg types: [java.nio.ByteBuffer, int]
     candidates:
      com.badlogic.gdx.utils.a.a(int, int):void
      com.badlogic.gdx.utils.a.a(int, java.lang.Object):void
      com.badlogic.gdx.utils.a.a(com.badlogic.gdx.utils.a, boolean):boolean
      com.badlogic.gdx.utils.a.a(java.lang.Object, boolean):boolean */
    public static boolean b(ByteBuffer byteBuffer) {
        boolean a2;
        synchronized (f5371a) {
            a2 = f5371a.a((Object) byteBuffer, true);
        }
        return a2;
    }

    private static int a(Buffer buffer, int i2) {
        if (buffer instanceof ByteBuffer) {
            return i2;
        }
        if (buffer instanceof ShortBuffer) {
            return i2 >>> 1;
        }
        if (buffer instanceof CharBuffer) {
            return i2 >>> 1;
        }
        if (buffer instanceof IntBuffer) {
            return i2 >>> 2;
        }
        if (buffer instanceof LongBuffer) {
            return i2 >>> 3;
        }
        if (buffer instanceof FloatBuffer) {
            return i2 >>> 2;
        }
        if (buffer instanceof DoubleBuffer) {
            return i2 >>> 3;
        }
        throw new o("Can't copy to a " + buffer.getClass().getName() + " instance");
    }

    public static ByteBuffer a(int i2) {
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(i2);
        allocateDirect.order(ByteOrder.nativeOrder());
        return allocateDirect;
    }

    public static void a(ByteBuffer byteBuffer) {
        int capacity = byteBuffer.capacity();
        synchronized (f5371a) {
            if (!f5371a.d(byteBuffer, true)) {
                throw new IllegalArgumentException("buffer not allocated with newUnsafeByteBuffer or already disposed");
            }
        }
        f5372b -= capacity;
        freeMemory(byteBuffer);
    }
}
