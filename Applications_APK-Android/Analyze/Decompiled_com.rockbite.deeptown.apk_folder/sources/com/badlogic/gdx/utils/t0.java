package com.badlogic.gdx.utils;

import java.util.Comparator;

/* compiled from: TimSort */
class t0<T> {

    /* renamed from: a  reason: collision with root package name */
    private T[] f5603a;

    /* renamed from: b  reason: collision with root package name */
    private Comparator<? super T> f5604b;

    /* renamed from: c  reason: collision with root package name */
    private int f5605c = 7;

    /* renamed from: d  reason: collision with root package name */
    private T[] f5606d = new Object[256];

    /* renamed from: e  reason: collision with root package name */
    private int f5607e;

    /* renamed from: f  reason: collision with root package name */
    private int f5608f = 0;

    /* renamed from: g  reason: collision with root package name */
    private final int[] f5609g = new int[40];

    /* renamed from: h  reason: collision with root package name */
    private final int[] f5610h = new int[40];

    t0() {
    }

    private void b() {
        while (true) {
            int i2 = this.f5608f;
            if (i2 > 1) {
                int i3 = i2 - 2;
                if (i3 > 0) {
                    int[] iArr = this.f5610h;
                    if (iArr[i3 - 1] < iArr[i3 + 1]) {
                        i3--;
                    }
                }
                b(i3);
            } else {
                return;
            }
        }
    }

    private static int c(int i2) {
        int i3 = 0;
        while (i2 >= 32) {
            i3 |= i2 & 1;
            i2 >>= 1;
        }
        return i2 + i3;
    }

    public void a(Object[] objArr, Comparator comparator, int i2, int i3) {
        this.f5608f = 0;
        a(objArr.length, i2, i3);
        int i4 = i3 - i2;
        if (i4 >= 2) {
            if (i4 < 32) {
                a(objArr, i2, i3, a(objArr, i2, i3, comparator) + i2, comparator);
                return;
            }
            this.f5603a = objArr;
            this.f5604b = comparator;
            this.f5607e = 0;
            int c2 = c(i4);
            do {
                int a2 = a(objArr, i2, i3, comparator);
                if (a2 < c2) {
                    int i5 = i4 <= c2 ? i4 : c2;
                    a(objArr, i2, i2 + i5, a2 + i2, comparator);
                    a2 = i5;
                }
                a(i2, a2);
                a();
                i2 += a2;
                i4 -= a2;
            } while (i4 != 0);
            b();
            this.f5603a = null;
            this.f5604b = null;
            T[] tArr = this.f5606d;
            int i6 = this.f5607e;
            for (int i7 = 0; i7 < i6; i7++) {
                tArr[i7] = null;
            }
        }
    }

    private void b(int i2) {
        int i3 = i2;
        int[] iArr = this.f5609g;
        int i4 = iArr[i3];
        int[] iArr2 = this.f5610h;
        int i5 = iArr2[i3];
        int i6 = i3 + 1;
        int i7 = iArr[i6];
        int i8 = iArr2[i6];
        iArr2[i3] = i5 + i8;
        if (i3 == this.f5608f - 3) {
            int i9 = i3 + 2;
            iArr[i6] = iArr[i9];
            iArr2[i6] = iArr2[i9];
        }
        this.f5608f--;
        T[] tArr = this.f5603a;
        int b2 = b(tArr[i7], tArr, i4, i5, 0, this.f5604b);
        int i10 = i4 + b2;
        int i11 = i5 - b2;
        if (i11 != 0) {
            T[] tArr2 = this.f5603a;
            int i12 = i7;
            int a2 = a(tArr2[(i10 + i11) - 1], tArr2, i7, i8, i8 - 1, this.f5604b);
            if (a2 != 0) {
                if (i11 <= a2) {
                    b(i10, i11, i12, a2);
                } else {
                    a(i10, i11, i12, a2);
                }
            }
        }
    }

    private static <T> int b(T t, T[] tArr, int i2, int i3, int i4, Comparator<? super T> comparator) {
        int i5;
        int i6;
        int i7 = i2 + i4;
        if (comparator.compare(t, tArr[i7]) < 0) {
            int i8 = i4 + 1;
            int i9 = 0;
            int i10 = 1;
            while (i10 < i8 && comparator.compare(t, tArr[i7 - i10]) < 0) {
                int i11 = (i10 << 1) + 1;
                if (i11 <= 0) {
                    i9 = i10;
                    i10 = i8;
                } else {
                    int i12 = i10;
                    i10 = i11;
                    i9 = i12;
                }
            }
            if (i10 <= i8) {
                i8 = i10;
            }
            i6 = i4 - i8;
            i5 = i4 - i9;
        } else {
            int i13 = i3 - i4;
            int i14 = 0;
            int i15 = 1;
            while (i15 < i13 && comparator.compare(t, tArr[i7 + i15]) >= 0) {
                int i16 = (i15 << 1) + 1;
                if (i16 <= 0) {
                    i14 = i15;
                    i15 = i13;
                } else {
                    int i17 = i15;
                    i15 = i16;
                    i14 = i17;
                }
            }
            if (i15 <= i13) {
                i13 = i15;
            }
            int i18 = i14 + i4;
            i5 = i4 + i13;
            i6 = i18;
        }
        int i19 = i6 + 1;
        while (i19 < i5) {
            int i20 = ((i5 - i19) >>> 1) + i19;
            if (comparator.compare(t, tArr[i2 + i20]) < 0) {
                i5 = i20;
            } else {
                i19 = i20 + 1;
            }
        }
        return i5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:0:0x0000, code lost:
        if (r8 == r6) goto L_0x0002;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001f, code lost:
        r2 = r8 - r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0021, code lost:
        if (r2 == 1) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0024, code lost:
        if (r2 == 2) goto L_0x002c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0026, code lost:
        java.lang.System.arraycopy(r5, r1, r5, r1 + 1, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        r5[r1 + 2] = r5[r1 + 1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0034, code lost:
        r5[r1 + 1] = r5[r1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003a, code lost:
        r5[r1] = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003d, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0002, code lost:
        r8 = r8 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        if (r8 >= r7) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0006, code lost:
        r0 = r5[r8];
        r1 = r6;
        r2 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
        if (r1 >= r2) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000d, code lost:
        r3 = (r1 + r2) >>> 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
        if (r9.compare(r0, r5[r3]) >= 0) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0019, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001b, code lost:
        r1 = r3 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static <T> void a(T[] r5, int r6, int r7, int r8, java.util.Comparator<? super T> r9) {
        /*
            if (r8 != r6) goto L_0x0004
        L_0x0002:
            int r8 = r8 + 1
        L_0x0004:
            if (r8 >= r7) goto L_0x003d
            r0 = r5[r8]
            r1 = r6
            r2 = r8
        L_0x000a:
            r3 = 1
            if (r1 >= r2) goto L_0x001f
            int r4 = r1 + r2
            int r3 = r4 >>> 1
            r4 = r5[r3]
            int r4 = r9.compare(r0, r4)
            if (r4 >= 0) goto L_0x001b
            r2 = r3
            goto L_0x000a
        L_0x001b:
            int r3 = r3 + 1
            r1 = r3
            goto L_0x000a
        L_0x001f:
            int r2 = r8 - r1
            if (r2 == r3) goto L_0x0034
            r3 = 2
            if (r2 == r3) goto L_0x002c
            int r3 = r1 + 1
            java.lang.System.arraycopy(r5, r1, r5, r3, r2)
            goto L_0x003a
        L_0x002c:
            int r2 = r1 + 2
            int r3 = r1 + 1
            r3 = r5[r3]
            r5[r2] = r3
        L_0x0034:
            int r2 = r1 + 1
            r3 = r5[r1]
            r5[r2] = r3
        L_0x003a:
            r5[r1] = r0
            goto L_0x0002
        L_0x003d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.t0.a(java.lang.Object[], int, int, int, java.util.Comparator):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0075, code lost:
        r14 = r2;
        r12 = r4;
        r15 = r5;
        r17 = r6;
        r16 = r13;
        r13 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x007d, code lost:
        r6 = b(r7[r12], r8, r13, r14, 0, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0088, code lost:
        if (r6 == 0) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008a, code lost:
        java.lang.System.arraycopy(r8, r13, r7, r15, r6);
        r1 = r15 + r6;
        r3 = r13 + r6;
        r2 = r14 - r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0093, code lost:
        if (r2 > r10) goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0095, code lost:
        r14 = r2;
        r13 = r3;
        r4 = r12;
        r12 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x009b, code lost:
        r15 = r1;
        r14 = r2;
        r13 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x009e, code lost:
        r5 = r15 + 1;
        r4 = r12 + 1;
        r7[r15] = r7[r12];
        r12 = r16 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a8, code lost:
        if (r12 != 0) goto L_0x00af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00aa, code lost:
        r1 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00af, code lost:
        r9 = r4;
        r10 = r5;
        r15 = r6;
        r1 = a(r8[r13], r7, r4, r12, 0, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00be, code lost:
        if (r1 == 0) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c0, code lost:
        java.lang.System.arraycopy(r7, r9, r7, r10, r1);
        r2 = r10 + r1;
        r4 = r9 + r1;
        r3 = r12 - r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c9, code lost:
        if (r3 != 0) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00cb, code lost:
        r1 = r2;
        r12 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ce, code lost:
        r12 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00d0, code lost:
        r4 = r9;
        r2 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d2, code lost:
        r3 = r2 + 1;
        r5 = r13 + 1;
        r7[r2] = r8[r13];
        r14 = r14 - 1;
        r9 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00dd, code lost:
        if (r14 != 1) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00df, code lost:
        r1 = r3;
        r13 = r5;
        r10 = r17;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0101, code lost:
        r17 = r17 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0104, code lost:
        if (r15 < 7) goto L_0x0108;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0106, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0108, code lost:
        r6 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0109, code lost:
        if (r1 < 7) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x010b, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x010d, code lost:
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x010f, code lost:
        if ((r1 | r6) != false) goto L_0x0122;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0111, code lost:
        if (r17 >= 0) goto L_0x0115;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0113, code lost:
        r17 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0122, code lost:
        r15 = r3;
        r13 = r5;
        r16 = r12;
        r10 = 1;
        r12 = r4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r20, int r21, int r22, int r23) {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r21
            T[] r7 = r0.f5603a
            java.lang.Object[] r8 = r0.a(r2)
            r9 = 0
            java.lang.System.arraycopy(r7, r1, r8, r9, r2)
            int r3 = r1 + 1
            int r4 = r22 + 1
            r5 = r7[r22]
            r7[r1] = r5
            int r1 = r23 + -1
            if (r1 != 0) goto L_0x0020
            java.lang.System.arraycopy(r8, r9, r7, r3, r2)
            return
        L_0x0020:
            r10 = 1
            if (r2 != r10) goto L_0x002c
            java.lang.System.arraycopy(r7, r4, r7, r3, r1)
            int r3 = r3 + r1
            r1 = r8[r9]
            r7[r3] = r1
            return
        L_0x002c:
            java.util.Comparator<? super T> r11 = r0.f5604b
            int r5 = r0.f5605c
            r6 = r5
            r5 = r3
            r3 = 0
        L_0x0033:
            r13 = r1
            r1 = 0
            r12 = 0
        L_0x0036:
            r14 = r7[r4]
            r15 = r8[r3]
            int r14 = r11.compare(r14, r15)
            if (r14 >= 0) goto L_0x0059
            int r1 = r5 + 1
            int r14 = r4 + 1
            r4 = r7[r4]
            r7[r5] = r4
            int r12 = r12 + r10
            int r13 = r13 + -1
            if (r13 != 0) goto L_0x0055
            r10 = r6
            r12 = r13
            r4 = r14
            r9 = 1
            r14 = r2
            r13 = r3
            goto L_0x00e3
        L_0x0055:
            r5 = r1
            r4 = r14
            r1 = 0
            goto L_0x0071
        L_0x0059:
            int r12 = r5 + 1
            int r14 = r3 + 1
            r3 = r8[r3]
            r7[r5] = r3
            int r1 = r1 + r10
            int r2 = r2 + -1
            if (r2 != r10) goto L_0x006e
            r10 = r6
            r1 = r12
            r12 = r13
            r13 = r14
            r9 = 1
            r14 = r2
            goto L_0x00e3
        L_0x006e:
            r5 = r12
            r3 = r14
            r12 = 0
        L_0x0071:
            r14 = r1 | r12
            if (r14 < r6) goto L_0x012b
            r14 = r2
            r12 = r4
            r15 = r5
            r17 = r6
            r16 = r13
            r13 = r3
        L_0x007d:
            r1 = r7[r12]
            r5 = 0
            r2 = r8
            r3 = r13
            r4 = r14
            r6 = r11
            int r6 = b(r1, r2, r3, r4, r5, r6)
            if (r6 == 0) goto L_0x009e
            java.lang.System.arraycopy(r8, r13, r7, r15, r6)
            int r1 = r15 + r6
            int r3 = r13 + r6
            int r2 = r14 - r6
            if (r2 > r10) goto L_0x009b
            r14 = r2
            r13 = r3
            r4 = r12
            r12 = r16
            goto L_0x00ab
        L_0x009b:
            r15 = r1
            r14 = r2
            r13 = r3
        L_0x009e:
            int r5 = r15 + 1
            int r4 = r12 + 1
            r1 = r7[r12]
            r7[r15] = r1
            int r12 = r16 + -1
            if (r12 != 0) goto L_0x00af
            r1 = r5
        L_0x00ab:
            r10 = r17
            r9 = 1
            goto L_0x00e3
        L_0x00af:
            r1 = r8[r13]
            r15 = 0
            r2 = r7
            r3 = r4
            r9 = r4
            r4 = r12
            r10 = r5
            r5 = r15
            r15 = r6
            r6 = r11
            int r1 = a(r1, r2, r3, r4, r5, r6)
            if (r1 == 0) goto L_0x00d0
            java.lang.System.arraycopy(r7, r9, r7, r10, r1)
            int r2 = r10 + r1
            int r4 = r9 + r1
            int r3 = r12 - r1
            if (r3 != 0) goto L_0x00ce
            r1 = r2
            r12 = r3
            goto L_0x00ab
        L_0x00ce:
            r12 = r3
            goto L_0x00d2
        L_0x00d0:
            r4 = r9
            r2 = r10
        L_0x00d2:
            int r3 = r2 + 1
            int r5 = r13 + 1
            r6 = r8[r13]
            r7[r2] = r6
            int r14 = r14 + -1
            r9 = 1
            if (r14 != r9) goto L_0x0101
            r1 = r3
            r13 = r5
            r10 = r17
        L_0x00e3:
            if (r10 >= r9) goto L_0x00e6
            r10 = 1
        L_0x00e6:
            r0.f5605c = r10
            if (r14 != r9) goto L_0x00f3
            java.lang.System.arraycopy(r7, r4, r7, r1, r12)
            int r1 = r1 + r12
            r2 = r8[r13]
            r7[r1] = r2
            goto L_0x00f8
        L_0x00f3:
            if (r14 == 0) goto L_0x00f9
            java.lang.System.arraycopy(r8, r13, r7, r1, r14)
        L_0x00f8:
            return
        L_0x00f9:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Comparison method violates its general contract!"
            r1.<init>(r2)
            throw r1
        L_0x0101:
            int r17 = r17 + -1
            r2 = 7
            if (r15 < r2) goto L_0x0108
            r6 = 1
            goto L_0x0109
        L_0x0108:
            r6 = 0
        L_0x0109:
            if (r1 < r2) goto L_0x010d
            r1 = 1
            goto L_0x010e
        L_0x010d:
            r1 = 0
        L_0x010e:
            r1 = r1 | r6
            if (r1 != 0) goto L_0x0122
            if (r17 >= 0) goto L_0x0115
            r17 = 0
        L_0x0115:
            int r6 = r17 + 2
            r1 = r12
            r2 = r14
            r9 = 0
            r10 = 1
            r18 = r5
            r5 = r3
            r3 = r18
            goto L_0x0033
        L_0x0122:
            r15 = r3
            r13 = r5
            r16 = r12
            r9 = 0
            r10 = 1
            r12 = r4
            goto L_0x007d
        L_0x012b:
            r9 = 0
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.t0.b(int, int, int, int):void");
    }

    private static <T> int a(Object[] objArr, int i2, int i3, Comparator comparator) {
        int i4 = i2 + 1;
        if (i4 == i3) {
            return 1;
        }
        int i5 = i4 + 1;
        if (comparator.compare(objArr[i4], objArr[i2]) < 0) {
            while (i5 < i3 && comparator.compare(objArr[i5], objArr[i5 - 1]) < 0) {
                i5++;
            }
            a(objArr, i2, i5);
        } else {
            while (i5 < i3 && comparator.compare(objArr[i5], objArr[i5 - 1]) >= 0) {
                i5++;
            }
        }
        return i5 - i2;
    }

    private static void a(Object[] objArr, int i2, int i3) {
        int i4 = i3 - 1;
        while (i2 < i4) {
            Object obj = objArr[i2];
            objArr[i2] = objArr[i4];
            objArr[i4] = obj;
            i4--;
            i2++;
        }
    }

    private void a(int i2, int i3) {
        int[] iArr = this.f5609g;
        int i4 = this.f5608f;
        iArr[i4] = i2;
        this.f5610h[i4] = i3;
        this.f5608f = i4 + 1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002a, code lost:
        r1 = r5.f5610h;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0034, code lost:
        if (r1[r0 - 1] >= r1[r0 + 1]) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0036, code lost:
        r0 = r0 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0016, code lost:
        if (r1[r0 - 1] > (r1[r0] + r1[r0 + 1])) goto L_0x0018;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0028, code lost:
        if (r1[r0 - 2] <= (r1[r0] + r1[r0 - 1])) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r5 = this;
        L_0x0000:
            int r0 = r5.f5608f
            r1 = 1
            if (r0 <= r1) goto L_0x0048
            int r0 = r0 + -2
            if (r0 < r1) goto L_0x0018
            int[] r1 = r5.f5610h
            int r2 = r0 + -1
            r2 = r1[r2]
            r3 = r1[r0]
            int r4 = r0 + 1
            r1 = r1[r4]
            int r3 = r3 + r1
            if (r2 <= r3) goto L_0x002a
        L_0x0018:
            r1 = 2
            if (r0 < r1) goto L_0x0039
            int[] r1 = r5.f5610h
            int r2 = r0 + -2
            r2 = r1[r2]
            r3 = r1[r0]
            int r4 = r0 + -1
            r1 = r1[r4]
            int r3 = r3 + r1
            if (r2 > r3) goto L_0x0039
        L_0x002a:
            int[] r1 = r5.f5610h
            int r2 = r0 + -1
            r2 = r1[r2]
            int r3 = r0 + 1
            r1 = r1[r3]
            if (r2 >= r1) goto L_0x0044
            int r0 = r0 + -1
            goto L_0x0044
        L_0x0039:
            int[] r1 = r5.f5610h
            r2 = r1[r0]
            int r3 = r0 + 1
            r1 = r1[r3]
            if (r2 <= r1) goto L_0x0044
            goto L_0x0048
        L_0x0044:
            r5.b(r0)
            goto L_0x0000
        L_0x0048:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.t0.a():void");
    }

    private static <T> int a(T t, T[] tArr, int i2, int i3, int i4, Comparator<? super T> comparator) {
        int i5;
        int i6;
        int i7 = i2 + i4;
        if (comparator.compare(t, tArr[i7]) > 0) {
            int i8 = i3 - i4;
            int i9 = 0;
            int i10 = 1;
            while (i10 < i8 && comparator.compare(t, tArr[i7 + i10]) > 0) {
                int i11 = (i10 << 1) + 1;
                if (i11 <= 0) {
                    i9 = i10;
                    i10 = i8;
                } else {
                    int i12 = i10;
                    i10 = i11;
                    i9 = i12;
                }
            }
            if (i10 <= i8) {
                i8 = i10;
            }
            int i13 = i9 + i4;
            i5 = i8 + i4;
            i6 = i13;
        } else {
            int i14 = i4 + 1;
            int i15 = 0;
            int i16 = 1;
            while (i16 < i14 && comparator.compare(t, tArr[i7 - i16]) <= 0) {
                int i17 = (i16 << 1) + 1;
                if (i17 <= 0) {
                    i15 = i16;
                    i16 = i14;
                } else {
                    int i18 = i16;
                    i16 = i17;
                    i15 = i18;
                }
            }
            if (i16 <= i14) {
                i14 = i16;
            }
            i6 = i4 - i14;
            i5 = i4 - i15;
        }
        int i19 = i6 + 1;
        while (i19 < i5) {
            int i20 = ((i5 - i19) >>> 1) + i19;
            if (comparator.compare(t, tArr[i2 + i20]) > 0) {
                i19 = i20 + 1;
            } else {
                i5 = i20;
            }
        }
        return i5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0077, code lost:
        r16 = r2;
        r17 = r3;
        r12 = r4;
        r14 = r5;
        r15 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007e, code lost:
        r6 = r13 - b(r8[r12], r7, r21, r13, r13 - 1, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008d, code lost:
        if (r6 == 0) goto L_0x00a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x008f, code lost:
        r1 = r14 - r6;
        r2 = r15 - r6;
        r13 = r13 - r6;
        java.lang.System.arraycopy(r7, r2 + 1, r7, r1 + 1, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x009b, code lost:
        if (r13 != 0) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x009d, code lost:
        r15 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a4, code lost:
        r14 = r1;
        r15 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a6, code lost:
        r18 = r14 - 1;
        r19 = r12 - 1;
        r7[r14] = r8[r12];
        r12 = r16 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b0, code lost:
        if (r12 != 1) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b2, code lost:
        r2 = r12;
        r3 = r17;
        r1 = r18;
        r12 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00ba, code lost:
        r14 = r6;
        r1 = r12 - a(r7[r15], r8, 0, r12, r12 - 1, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c9, code lost:
        if (r1 == 0) goto L_0x00e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00cb, code lost:
        r2 = r18 - r1;
        r4 = r19 - r1;
        r3 = r12 - r1;
        java.lang.System.arraycopy(r8, r4 + 1, r7, r2 + 1, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00d8, code lost:
        if (r3 > 1) goto L_0x00de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00da, code lost:
        r1 = r2;
        r2 = r3;
        r12 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00de, code lost:
        r18 = r2;
        r16 = r3;
        r12 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00e4, code lost:
        r16 = r12;
        r12 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00e8, code lost:
        r2 = r18 - 1;
        r3 = r15 - 1;
        r7[r18] = r7[r15];
        r13 = r13 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00f2, code lost:
        if (r13 != 0) goto L_0x011c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f4, code lost:
        r1 = r2;
        r15 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x011c, code lost:
        r17 = r17 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x011f, code lost:
        if (r14 < 7) goto L_0x0123;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0121, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0123, code lost:
        r5 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0124, code lost:
        if (r1 < 7) goto L_0x0128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0126, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0128, code lost:
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x012a, code lost:
        if ((r1 | r5) != false) goto L_0x013b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x012c, code lost:
        if (r17 >= 0) goto L_0x0130;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x012e, code lost:
        r17 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x013b, code lost:
        r14 = r2;
        r15 = r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r21, int r22, int r23, int r24) {
        /*
            r20 = this;
            r0 = r20
            r1 = r23
            r2 = r24
            T[] r7 = r0.f5603a
            java.lang.Object[] r8 = r0.a(r2)
            r9 = 0
            java.lang.System.arraycopy(r7, r1, r8, r9, r2)
            int r3 = r21 + r22
            r10 = 1
            int r3 = r3 - r10
            int r4 = r2 + -1
            int r1 = r1 + r2
            int r1 = r1 - r10
            int r5 = r1 + -1
            int r6 = r3 + -1
            r3 = r7[r3]
            r7[r1] = r3
            int r1 = r22 + -1
            if (r1 != 0) goto L_0x0029
            int r5 = r5 - r4
            java.lang.System.arraycopy(r8, r9, r7, r5, r2)
            return
        L_0x0029:
            if (r2 != r10) goto L_0x0038
            int r5 = r5 - r1
            int r6 = r6 - r1
            int r6 = r6 + r10
            int r2 = r5 + 1
            java.lang.System.arraycopy(r7, r6, r7, r2, r1)
            r1 = r8[r4]
            r7[r5] = r1
            return
        L_0x0038:
            java.util.Comparator<? super T> r11 = r0.f5604b
            int r3 = r0.f5605c
        L_0x003c:
            r13 = r1
            r1 = 0
            r12 = 0
        L_0x003f:
            r14 = r8[r4]
            r15 = r7[r6]
            int r14 = r11.compare(r14, r15)
            if (r14 >= 0) goto L_0x005e
            int r1 = r5 + -1
            int r14 = r6 + -1
            r6 = r7[r6]
            r7[r5] = r6
            int r12 = r12 + r10
            int r13 = r13 + -1
            if (r13 != 0) goto L_0x005a
            r12 = r4
            r15 = r14
            goto L_0x00f7
        L_0x005a:
            r5 = r1
            r6 = r14
            r1 = 0
            goto L_0x0073
        L_0x005e:
            int r12 = r5 + -1
            int r14 = r4 + -1
            r4 = r8[r4]
            r7[r5] = r4
            int r1 = r1 + r10
            int r2 = r2 + -1
            if (r2 != r10) goto L_0x0070
            r15 = r6
            r1 = r12
            r12 = r14
            goto L_0x00f7
        L_0x0070:
            r5 = r12
            r4 = r14
            r12 = 0
        L_0x0073:
            r14 = r12 | r1
            if (r14 < r3) goto L_0x003f
            r16 = r2
            r17 = r3
            r12 = r4
            r14 = r5
            r15 = r6
        L_0x007e:
            r1 = r8[r12]
            int r5 = r13 + -1
            r2 = r7
            r3 = r21
            r4 = r13
            r6 = r11
            int r1 = b(r1, r2, r3, r4, r5, r6)
            int r6 = r13 - r1
            if (r6 == 0) goto L_0x00a6
            int r1 = r14 - r6
            int r2 = r15 - r6
            int r13 = r13 - r6
            int r3 = r2 + 1
            int r4 = r1 + 1
            java.lang.System.arraycopy(r7, r3, r7, r4, r6)
            if (r13 != 0) goto L_0x00a4
            r15 = r2
        L_0x009e:
            r2 = r16
        L_0x00a0:
            r3 = r17
            goto L_0x00f7
        L_0x00a4:
            r14 = r1
            r15 = r2
        L_0x00a6:
            int r18 = r14 + -1
            int r19 = r12 + -1
            r1 = r8[r12]
            r7[r14] = r1
            int r12 = r16 + -1
            if (r12 != r10) goto L_0x00ba
            r2 = r12
            r3 = r17
            r1 = r18
            r12 = r19
            goto L_0x00f7
        L_0x00ba:
            r1 = r7[r15]
            r3 = 0
            int r5 = r12 + -1
            r2 = r8
            r4 = r12
            r14 = r6
            r6 = r11
            int r1 = a(r1, r2, r3, r4, r5, r6)
            int r1 = r12 - r1
            if (r1 == 0) goto L_0x00e4
            int r2 = r18 - r1
            int r4 = r19 - r1
            int r3 = r12 - r1
            int r5 = r4 + 1
            int r6 = r2 + 1
            java.lang.System.arraycopy(r8, r5, r7, r6, r1)
            if (r3 > r10) goto L_0x00de
            r1 = r2
            r2 = r3
            r12 = r4
            goto L_0x00a0
        L_0x00de:
            r18 = r2
            r16 = r3
            r12 = r4
            goto L_0x00e8
        L_0x00e4:
            r16 = r12
            r12 = r19
        L_0x00e8:
            int r2 = r18 + -1
            int r3 = r15 + -1
            r4 = r7[r15]
            r7[r18] = r4
            int r13 = r13 + -1
            if (r13 != 0) goto L_0x011c
            r1 = r2
            r15 = r3
            goto L_0x009e
        L_0x00f7:
            if (r3 >= r10) goto L_0x00fa
            r3 = 1
        L_0x00fa:
            r0.f5605c = r3
            if (r2 != r10) goto L_0x010b
            int r1 = r1 - r13
            int r15 = r15 - r13
            int r15 = r15 + r10
            int r2 = r1 + 1
            java.lang.System.arraycopy(r7, r15, r7, r2, r13)
            r2 = r8[r12]
            r7[r1] = r2
            goto L_0x0113
        L_0x010b:
            if (r2 == 0) goto L_0x0114
            int r3 = r2 + -1
            int r1 = r1 - r3
            java.lang.System.arraycopy(r8, r9, r7, r1, r2)
        L_0x0113:
            return
        L_0x0114:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Comparison method violates its general contract!"
            r1.<init>(r2)
            throw r1
        L_0x011c:
            int r17 = r17 + -1
            r4 = 7
            if (r14 < r4) goto L_0x0123
            r5 = 1
            goto L_0x0124
        L_0x0123:
            r5 = 0
        L_0x0124:
            if (r1 < r4) goto L_0x0128
            r1 = 1
            goto L_0x0129
        L_0x0128:
            r1 = 0
        L_0x0129:
            r1 = r1 | r5
            if (r1 != 0) goto L_0x013b
            if (r17 >= 0) goto L_0x0130
            r17 = 0
        L_0x0130:
            int r1 = r17 + 2
            r5 = r2
            r6 = r3
            r4 = r12
            r2 = r16
            r3 = r1
            r1 = r13
            goto L_0x003c
        L_0x013b:
            r14 = r2
            r15 = r3
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.t0.a(int, int, int, int):void");
    }

    private T[] a(int i2) {
        this.f5607e = Math.max(this.f5607e, i2);
        if (this.f5606d.length < i2) {
            int i3 = (i2 >> 1) | i2;
            int i4 = i3 | (i3 >> 2);
            int i5 = i4 | (i4 >> 4);
            int i6 = i5 | (i5 >> 8);
            int i7 = (i6 | (i6 >> 16)) + 1;
            if (i7 >= 0) {
                i2 = Math.min(i7, this.f5603a.length >>> 1);
            }
            this.f5606d = new Object[i2];
        }
        return this.f5606d;
    }

    private static void a(int i2, int i3, int i4) {
        if (i3 > i4) {
            throw new IllegalArgumentException("fromIndex(" + i3 + ") > toIndex(" + i4 + ")");
        } else if (i3 < 0) {
            throw new ArrayIndexOutOfBoundsException(i3);
        } else if (i4 > i2) {
            throw new ArrayIndexOutOfBoundsException(i4);
        }
    }
}
