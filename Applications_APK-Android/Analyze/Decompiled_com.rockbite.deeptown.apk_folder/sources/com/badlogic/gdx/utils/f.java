package com.badlogic.gdx.utils;

/* compiled from: BooleanArray */
public class f {

    /* renamed from: a  reason: collision with root package name */
    public boolean[] f5486a;

    /* renamed from: b  reason: collision with root package name */
    public int f5487b;

    /* renamed from: c  reason: collision with root package name */
    public boolean f5488c;

    public f() {
        this(true, 16);
    }

    public boolean a(int i2) {
        int i3 = this.f5487b;
        if (i2 < i3) {
            boolean[] zArr = this.f5486a;
            boolean z = zArr[i2];
            this.f5487b = i3 - 1;
            if (this.f5488c) {
                System.arraycopy(zArr, i2 + 1, zArr, i2, this.f5487b - i2);
            } else {
                zArr[i2] = zArr[this.f5487b];
            }
            return z;
        }
        throw new IndexOutOfBoundsException("index can't be >= size: " + i2 + " >= " + this.f5487b);
    }

    /* access modifiers changed from: protected */
    public boolean[] b(int i2) {
        boolean[] zArr = new boolean[i2];
        System.arraycopy(this.f5486a, 0, zArr, 0, Math.min(this.f5487b, zArr.length));
        this.f5486a = zArr;
        return zArr;
    }

    public boolean[] c(int i2) {
        if (i2 >= 0) {
            if (i2 > this.f5486a.length) {
                b(Math.max(8, i2));
            }
            this.f5487b = i2;
            return this.f5486a;
        }
        throw new IllegalArgumentException("newSize must be >= 0: " + i2);
    }

    public boolean equals(Object obj) {
        int i2;
        if (obj == this) {
            return true;
        }
        if (!this.f5488c || !(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        if (!fVar.f5488c || (i2 = this.f5487b) != fVar.f5487b) {
            return false;
        }
        boolean[] zArr = this.f5486a;
        boolean[] zArr2 = fVar.f5486a;
        for (int i3 = 0; i3 < i2; i3++) {
            if (zArr[i3] != zArr2[i3]) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        if (!this.f5488c) {
            return super.hashCode();
        }
        boolean[] zArr = this.f5486a;
        int i2 = this.f5487b;
        int i3 = 1;
        for (int i4 = 0; i4 < i2; i4++) {
            i3 = (i3 * 31) + (zArr[i4] ? 1231 : 1237);
        }
        return i3;
    }

    public String toString() {
        if (this.f5487b == 0) {
            return "[]";
        }
        boolean[] zArr = this.f5486a;
        r0 r0Var = new r0(32);
        r0Var.append('[');
        r0Var.a(zArr[0]);
        for (int i2 = 1; i2 < this.f5487b; i2++) {
            r0Var.a(", ");
            r0Var.a(zArr[i2]);
        }
        r0Var.append(']');
        return r0Var.toString();
    }

    public f(boolean z, int i2) {
        this.f5488c = z;
        this.f5486a = new boolean[i2];
    }
}
