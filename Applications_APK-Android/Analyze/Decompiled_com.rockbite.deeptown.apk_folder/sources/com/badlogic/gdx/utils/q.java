package com.badlogic.gdx.utils;

/* compiled from: IntArray */
public class q {

    /* renamed from: a  reason: collision with root package name */
    public int[] f5558a;

    /* renamed from: b  reason: collision with root package name */
    public int f5559b;

    /* renamed from: c  reason: collision with root package name */
    public boolean f5560c;

    public q() {
        this(true, 16);
    }

    public void a(int i2) {
        int[] iArr = this.f5558a;
        int i3 = this.f5559b;
        if (i3 == iArr.length) {
            iArr = e(Math.max(8, (int) (((float) i3) * 1.75f)));
        }
        int i4 = this.f5559b;
        this.f5559b = i4 + 1;
        iArr[i4] = i2;
    }

    public int b() {
        int[] iArr = this.f5558a;
        int i2 = this.f5559b - 1;
        this.f5559b = i2;
        return iArr[i2];
    }

    public int c(int i2) {
        if (i2 < this.f5559b) {
            return this.f5558a[i2];
        }
        throw new IndexOutOfBoundsException("index can't be >= size: " + i2 + " >= " + this.f5559b);
    }

    public int d(int i2) {
        int i3 = this.f5559b;
        if (i2 < i3) {
            int[] iArr = this.f5558a;
            int i4 = iArr[i2];
            this.f5559b = i3 - 1;
            if (this.f5560c) {
                System.arraycopy(iArr, i2 + 1, iArr, i2, this.f5559b - i2);
            } else {
                iArr[i2] = iArr[this.f5559b];
            }
            return i4;
        }
        throw new IndexOutOfBoundsException("index can't be >= size: " + i2 + " >= " + this.f5559b);
    }

    /* access modifiers changed from: protected */
    public int[] e(int i2) {
        int[] iArr = new int[i2];
        System.arraycopy(this.f5558a, 0, iArr, 0, Math.min(this.f5559b, iArr.length));
        this.f5558a = iArr;
        return iArr;
    }

    public boolean equals(Object obj) {
        int i2;
        if (obj == this) {
            return true;
        }
        if (!this.f5560c || !(obj instanceof q)) {
            return false;
        }
        q qVar = (q) obj;
        if (!qVar.f5560c || (i2 = this.f5559b) != qVar.f5559b) {
            return false;
        }
        int[] iArr = this.f5558a;
        int[] iArr2 = qVar.f5558a;
        for (int i3 = 0; i3 < i2; i3++) {
            if (iArr[i3] != iArr2[i3]) {
                return false;
            }
        }
        return true;
    }

    public int[] f(int i2) {
        if (i2 >= 0) {
            if (i2 > this.f5558a.length) {
                e(Math.max(8, i2));
            }
            this.f5559b = i2;
            return this.f5558a;
        }
        throw new IllegalArgumentException("newSize must be >= 0: " + i2);
    }

    public int hashCode() {
        if (!this.f5560c) {
            return super.hashCode();
        }
        int[] iArr = this.f5558a;
        int i2 = this.f5559b;
        int i3 = 1;
        for (int i4 = 0; i4 < i2; i4++) {
            i3 = (i3 * 31) + iArr[i4];
        }
        return i3;
    }

    public String toString() {
        if (this.f5559b == 0) {
            return "[]";
        }
        int[] iArr = this.f5558a;
        r0 r0Var = new r0(32);
        r0Var.append('[');
        r0Var.a(iArr[0]);
        for (int i2 = 1; i2 < this.f5559b; i2++) {
            r0Var.a(", ");
            r0Var.a(iArr[i2]);
        }
        r0Var.append(']');
        return r0Var.toString();
    }

    public q(int i2) {
        this(true, i2);
    }

    public int[] b(int i2) {
        if (i2 >= 0) {
            int i3 = this.f5559b + i2;
            if (i3 > this.f5558a.length) {
                e(Math.max(8, i3));
            }
            return this.f5558a;
        }
        throw new IllegalArgumentException("additionalCapacity must be >= 0: " + i2);
    }

    public q(boolean z, int i2) {
        this.f5560c = z;
        this.f5558a = new int[i2];
    }

    public void a(int... iArr) {
        a(iArr, 0, iArr.length);
    }

    public int[] c() {
        int i2 = this.f5559b;
        int[] iArr = new int[i2];
        System.arraycopy(this.f5558a, 0, iArr, 0, i2);
        return iArr;
    }

    public void a(int[] iArr, int i2, int i3) {
        int[] iArr2 = this.f5558a;
        int i4 = this.f5559b + i3;
        if (i4 > iArr2.length) {
            iArr2 = e(Math.max(8, (int) (((float) i4) * 1.75f)));
        }
        System.arraycopy(iArr, i2, iArr2, this.f5559b, i3);
        this.f5559b += i3;
    }

    public void a(int i2, int i3) {
        int i4 = this.f5559b;
        if (i2 <= i4) {
            int[] iArr = this.f5558a;
            if (i4 == iArr.length) {
                iArr = e(Math.max(8, (int) (((float) i4) * 1.75f)));
            }
            if (this.f5560c) {
                System.arraycopy(iArr, i2, iArr, i2 + 1, this.f5559b - i2);
            } else {
                iArr[this.f5559b] = iArr[i2];
            }
            this.f5559b++;
            iArr[i2] = i3;
            return;
        }
        throw new IndexOutOfBoundsException("index can't be > size: " + i2 + " > " + this.f5559b);
    }

    public void a() {
        this.f5559b = 0;
    }
}
