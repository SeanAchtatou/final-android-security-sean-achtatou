package com.badlogic.gdx.utils.a1;

import e.d.b.t.a;
import e.d.b.t.k;

/* compiled from: ScreenViewport */
public class c extends e {

    /* renamed from: i  reason: collision with root package name */
    private float f5405i;

    public c() {
        this(new k());
    }

    public void a(int i2, int i3, boolean z) {
        a(0, 0, i2, i3);
        float f2 = this.f5405i;
        a(((float) i2) * f2, ((float) i3) * f2);
        a(z);
    }

    public c(a aVar) {
        this.f5405i = 1.0f;
        a(aVar);
    }
}
