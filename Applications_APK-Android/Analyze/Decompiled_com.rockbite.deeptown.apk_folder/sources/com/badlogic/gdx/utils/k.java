package com.badlogic.gdx.utils;

import java.util.Comparator;

/* compiled from: DelayedRemovalArray */
public class k<T> extends a<T> {

    /* renamed from: e  reason: collision with root package name */
    private int f5512e;

    /* renamed from: f  reason: collision with root package name */
    private q f5513f = new q(0);

    /* renamed from: g  reason: collision with root package name */
    private int f5514g;

    public k(int i2) {
        super(i2);
    }

    private void remove(int i2) {
        if (i2 >= this.f5514g) {
            int i3 = 0;
            int i4 = this.f5513f.f5559b;
            while (i3 < i4) {
                int c2 = this.f5513f.c(i3);
                if (i2 != c2) {
                    if (i2 < c2) {
                        this.f5513f.a(i3, i2);
                        return;
                    }
                    i3++;
                } else {
                    return;
                }
            }
            this.f5513f.a(i2);
        }
    }

    public void a(int i2, int i3) {
        if (this.f5512e > 0) {
            while (i3 >= i2) {
                remove(i3);
                i3--;
            }
            return;
        }
        super.a(i2, i3);
    }

    public void b(int i2, int i3) {
        if (this.f5512e <= 0) {
            super.b(i2, i3);
            return;
        }
        throw new IllegalStateException("Invalid between begin/end.");
    }

    public void clear() {
        if (this.f5512e > 0) {
            this.f5514g = this.f5374b;
        } else {
            super.clear();
        }
    }

    public boolean d(T t, boolean z) {
        if (this.f5512e <= 0) {
            return super.d(t, z);
        }
        int b2 = b(t, z);
        if (b2 == -1) {
            return false;
        }
        remove(b2);
        return true;
    }

    public void e() {
        this.f5512e++;
    }

    public void f() {
        int i2 = this.f5512e;
        if (i2 != 0) {
            this.f5512e = i2 - 1;
            if (this.f5512e == 0) {
                int i3 = this.f5514g;
                if (i3 <= 0 || i3 != this.f5374b) {
                    int i4 = this.f5513f.f5559b;
                    for (int i5 = 0; i5 < i4; i5++) {
                        int b2 = this.f5513f.b();
                        if (b2 >= this.f5514g) {
                            d(b2);
                        }
                    }
                    for (int i6 = this.f5514g - 1; i6 >= 0; i6--) {
                        d(i6);
                    }
                } else {
                    this.f5513f.a();
                    clear();
                }
                this.f5514g = 0;
                return;
            }
            return;
        }
        throw new IllegalStateException("begin must be called before end.");
    }

    public void g(int i2) {
        if (this.f5512e <= 0) {
            super.g(i2);
            return;
        }
        throw new IllegalStateException("Invalid between begin/end.");
    }

    public T pop() {
        if (this.f5512e <= 0) {
            return super.pop();
        }
        throw new IllegalStateException("Invalid between begin/end.");
    }

    public void set(int i2, T t) {
        if (this.f5512e <= 0) {
            super.set(i2, t);
            return;
        }
        throw new IllegalStateException("Invalid between begin/end.");
    }

    public void sort(Comparator<? super T> comparator) {
        if (this.f5512e <= 0) {
            super.sort(comparator);
            return;
        }
        throw new IllegalStateException("Invalid between begin/end.");
    }

    public void a(int i2, T t) {
        if (this.f5512e <= 0) {
            super.a(i2, t);
            return;
        }
        throw new IllegalStateException("Invalid between begin/end.");
    }

    public T d(int i2) {
        if (this.f5512e <= 0) {
            return super.d(i2);
        }
        remove(i2);
        return get(i2);
    }

    public void d() {
        if (this.f5512e <= 0) {
            super.d();
            return;
        }
        throw new IllegalStateException("Invalid between begin/end.");
    }

    public T[] f(int i2) {
        if (this.f5512e <= 0) {
            return super.f(i2);
        }
        throw new IllegalStateException("Invalid between begin/end.");
    }
}
