package com.badlogic.gdx.utils;

/* compiled from: ShortArray */
public class n0 {

    /* renamed from: a  reason: collision with root package name */
    public short[] f5542a;

    /* renamed from: b  reason: collision with root package name */
    public int f5543b;

    /* renamed from: c  reason: collision with root package name */
    public boolean f5544c;

    public n0() {
        this(true, 16);
    }

    public void a(int i2) {
        short[] sArr = this.f5542a;
        int i3 = this.f5543b;
        if (i3 == sArr.length) {
            sArr = e(Math.max(8, (int) (((float) i3) * 1.75f)));
        }
        int i4 = this.f5543b;
        this.f5543b = i4 + 1;
        sArr[i4] = (short) i2;
    }

    public short[] b(int i2) {
        if (i2 >= 0) {
            int i3 = this.f5543b + i2;
            if (i3 > this.f5542a.length) {
                e(Math.max(8, i3));
            }
            return this.f5542a;
        }
        throw new IllegalArgumentException("additionalCapacity must be >= 0: " + i2);
    }

    public short c(int i2) {
        if (i2 < this.f5543b) {
            return this.f5542a[i2];
        }
        throw new IndexOutOfBoundsException("index can't be >= size: " + i2 + " >= " + this.f5543b);
    }

    public short d(int i2) {
        int i3 = this.f5543b;
        if (i2 < i3) {
            short[] sArr = this.f5542a;
            short s = sArr[i2];
            this.f5543b = i3 - 1;
            if (this.f5544c) {
                System.arraycopy(sArr, i2 + 1, sArr, i2, this.f5543b - i2);
            } else {
                sArr[i2] = sArr[this.f5543b];
            }
            return s;
        }
        throw new IndexOutOfBoundsException("index can't be >= size: " + i2 + " >= " + this.f5543b);
    }

    /* access modifiers changed from: protected */
    public short[] e(int i2) {
        short[] sArr = new short[i2];
        System.arraycopy(this.f5542a, 0, sArr, 0, Math.min(this.f5543b, sArr.length));
        this.f5542a = sArr;
        return sArr;
    }

    public boolean equals(Object obj) {
        int i2;
        if (obj == this) {
            return true;
        }
        if (!this.f5544c || !(obj instanceof n0)) {
            return false;
        }
        n0 n0Var = (n0) obj;
        if (!n0Var.f5544c || (i2 = this.f5543b) != n0Var.f5543b) {
            return false;
        }
        short[] sArr = this.f5542a;
        short[] sArr2 = n0Var.f5542a;
        for (int i3 = 0; i3 < i2; i3++) {
            if (sArr[i3] != sArr2[i3]) {
                return false;
            }
        }
        return true;
    }

    public short[] f(int i2) {
        if (i2 >= 0) {
            if (i2 > this.f5542a.length) {
                e(Math.max(8, i2));
            }
            this.f5543b = i2;
            return this.f5542a;
        }
        throw new IllegalArgumentException("newSize must be >= 0: " + i2);
    }

    public int hashCode() {
        if (!this.f5544c) {
            return super.hashCode();
        }
        short[] sArr = this.f5542a;
        int i2 = this.f5543b;
        int i3 = 1;
        for (int i4 = 0; i4 < i2; i4++) {
            i3 = (i3 * 31) + sArr[i4];
        }
        return i3;
    }

    public String toString() {
        if (this.f5543b == 0) {
            return "[]";
        }
        short[] sArr = this.f5542a;
        r0 r0Var = new r0(32);
        r0Var.append('[');
        r0Var.a((int) sArr[0]);
        for (int i2 = 1; i2 < this.f5543b; i2++) {
            r0Var.a(", ");
            r0Var.a((int) sArr[i2]);
        }
        r0Var.append(']');
        return r0Var.toString();
    }

    public n0(int i2) {
        this(true, i2);
    }

    public n0(boolean z, int i2) {
        this.f5544c = z;
        this.f5542a = new short[i2];
    }

    public void a(short s) {
        short[] sArr = this.f5542a;
        int i2 = this.f5543b;
        if (i2 == sArr.length) {
            sArr = e(Math.max(8, (int) (((float) i2) * 1.75f)));
        }
        int i3 = this.f5543b;
        this.f5543b = i3 + 1;
        sArr[i3] = s;
    }

    public short[] b() {
        int i2 = this.f5543b;
        short[] sArr = new short[i2];
        System.arraycopy(this.f5542a, 0, sArr, 0, i2);
        return sArr;
    }

    public void a() {
        this.f5543b = 0;
    }
}
