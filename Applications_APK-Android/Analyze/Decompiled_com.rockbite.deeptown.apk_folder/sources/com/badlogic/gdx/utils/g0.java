package com.badlogic.gdx.utils;

/* compiled from: Pools */
public class g0 {

    /* renamed from: a  reason: collision with root package name */
    private static final c0<Class, f0> f5489a = new c0<>();

    public static <T> f0<T> a(Class cls, int i2) {
        f0<T> b2 = f5489a.b(cls);
        if (b2 != null) {
            return b2;
        }
        j0 j0Var = new j0(cls, 4, i2);
        f5489a.b(cls, j0Var);
        return j0Var;
    }

    public static <T> T b(Class<T> cls) {
        return a((Class) cls).obtain();
    }

    public static <T> f0<T> a(Class cls) {
        return a(cls, 100);
    }

    public static void a(Object obj) {
        if (obj != null) {
            f0 b2 = f5489a.b(obj.getClass());
            if (b2 != null) {
                b2.free(obj);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Object cannot be null.");
    }

    public static void a(a aVar, boolean z) {
        if (aVar != null) {
            int i2 = aVar.f5374b;
            f0 f0Var = null;
            for (int i3 = 0; i3 < i2; i3++) {
                Object obj = aVar.get(i3);
                if (!(obj == null || (f0Var == null && (f0Var = f5489a.b(obj.getClass())) == null))) {
                    f0Var.free(obj);
                    if (!z) {
                        f0Var = null;
                    }
                }
            }
            return;
        }
        throw new IllegalArgumentException("Objects cannot be null.");
    }
}
