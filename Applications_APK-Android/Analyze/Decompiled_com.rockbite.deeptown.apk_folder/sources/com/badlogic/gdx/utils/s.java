package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.h;
import com.esotericsoftware.spine.Animation;
import java.util.NoSuchElementException;

/* compiled from: IntSet */
public class s {

    /* renamed from: a  reason: collision with root package name */
    public int f5583a;

    /* renamed from: b  reason: collision with root package name */
    int[] f5584b;

    /* renamed from: c  reason: collision with root package name */
    int f5585c;

    /* renamed from: d  reason: collision with root package name */
    int f5586d;

    /* renamed from: e  reason: collision with root package name */
    boolean f5587e;

    /* renamed from: f  reason: collision with root package name */
    private float f5588f;

    /* renamed from: g  reason: collision with root package name */
    private int f5589g;

    /* renamed from: h  reason: collision with root package name */
    private int f5590h;

    /* renamed from: i  reason: collision with root package name */
    private int f5591i;

    /* renamed from: j  reason: collision with root package name */
    private int f5592j;

    /* renamed from: k  reason: collision with root package name */
    private int f5593k;
    private a l;
    private a m;

    /* compiled from: IntSet */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public boolean f5594a;

        /* renamed from: b  reason: collision with root package name */
        final s f5595b;

        /* renamed from: c  reason: collision with root package name */
        int f5596c;

        /* renamed from: d  reason: collision with root package name */
        boolean f5597d = true;

        public a(s sVar) {
            this.f5595b = sVar;
            c();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f5594a = false;
            s sVar = this.f5595b;
            int[] iArr = sVar.f5584b;
            int i2 = sVar.f5585c + sVar.f5586d;
            do {
                int i3 = this.f5596c + 1;
                this.f5596c = i3;
                if (i3 >= i2) {
                    return;
                }
            } while (iArr[this.f5596c] == 0);
            this.f5594a = true;
        }

        public int b() {
            if (!this.f5594a) {
                throw new NoSuchElementException();
            } else if (this.f5597d) {
                int i2 = this.f5596c;
                int i3 = i2 == -1 ? 0 : this.f5595b.f5584b[i2];
                a();
                return i3;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }

        public void c() {
            this.f5596c = -1;
            if (this.f5595b.f5587e) {
                this.f5594a = true;
            } else {
                a();
            }
        }
    }

    public s() {
        this(51, 0.8f);
    }

    private void g(int i2) {
        if (i2 == 0) {
            this.f5587e = true;
            return;
        }
        int i3 = i2 & this.f5590h;
        int[] iArr = this.f5584b;
        int i4 = iArr[i3];
        if (i4 == 0) {
            iArr[i3] = i2;
            int i5 = this.f5583a;
            this.f5583a = i5 + 1;
            if (i5 >= this.f5591i) {
                l(this.f5585c << 1);
                return;
            }
            return;
        }
        int j2 = j(i2);
        int[] iArr2 = this.f5584b;
        int i6 = iArr2[j2];
        if (i6 == 0) {
            iArr2[j2] = i2;
            int i7 = this.f5583a;
            this.f5583a = i7 + 1;
            if (i7 >= this.f5591i) {
                l(this.f5585c << 1);
                return;
            }
            return;
        }
        int k2 = k(i2);
        int[] iArr3 = this.f5584b;
        int i8 = iArr3[k2];
        if (i8 == 0) {
            iArr3[k2] = i2;
            int i9 = this.f5583a;
            this.f5583a = i9 + 1;
            if (i9 >= this.f5591i) {
                l(this.f5585c << 1);
                return;
            }
            return;
        }
        a(i2, i3, i4, j2, i6, k2, i8);
    }

    private void h(int i2) {
        int i3 = this.f5586d;
        if (i3 == this.f5592j) {
            l(this.f5585c << 1);
            g(i2);
            return;
        }
        this.f5584b[this.f5585c + i3] = i2;
        this.f5586d = i3 + 1;
        this.f5583a++;
    }

    private boolean i(int i2) {
        int[] iArr = this.f5584b;
        int i3 = this.f5585c;
        int i4 = this.f5586d + i3;
        while (i3 < i4) {
            if (iArr[i3] == i2) {
                return true;
            }
            i3++;
        }
        return false;
    }

    private int j(int i2) {
        int i3 = i2 * -1262997959;
        return (i3 ^ (i3 >>> this.f5589g)) & this.f5590h;
    }

    private int k(int i2) {
        int i3 = i2 * -825114047;
        return (i3 ^ (i3 >>> this.f5589g)) & this.f5590h;
    }

    private void l(int i2) {
        int i3 = this.f5585c + this.f5586d;
        this.f5585c = i2;
        this.f5591i = (int) (((float) i2) * this.f5588f);
        this.f5590h = i2 - 1;
        this.f5589g = 31 - Integer.numberOfTrailingZeros(i2);
        double d2 = (double) i2;
        this.f5592j = Math.max(3, ((int) Math.ceil(Math.log(d2))) * 2);
        this.f5593k = Math.max(Math.min(i2, 8), ((int) Math.sqrt(d2)) / 8);
        int[] iArr = this.f5584b;
        this.f5584b = new int[(i2 + this.f5592j)];
        int i4 = this.f5583a;
        this.f5583a = this.f5587e ? 1 : 0;
        this.f5586d = 0;
        if (i4 > 0) {
            for (int i5 = 0; i5 < i3; i5++) {
                int i6 = iArr[i5];
                if (i6 != 0) {
                    g(i6);
                }
            }
        }
    }

    public boolean a(int i2) {
        int j2;
        int i3;
        int k2;
        int i4;
        if (i2 != 0) {
            int[] iArr = this.f5584b;
            int i5 = i2 & this.f5590h;
            int i6 = iArr[i5];
            if (i6 == i2 || (i3 = iArr[(j2 = j(i2))]) == i2 || (i4 = iArr[(k2 = k(i2))]) == i2) {
                return false;
            }
            int i7 = this.f5585c;
            int i8 = this.f5586d + i7;
            while (i7 < i8) {
                if (iArr[i7] == i2) {
                    return false;
                }
                i7++;
            }
            if (i6 == 0) {
                iArr[i5] = i2;
                int i9 = this.f5583a;
                this.f5583a = i9 + 1;
                if (i9 >= this.f5591i) {
                    l(this.f5585c << 1);
                }
                return true;
            } else if (i3 == 0) {
                iArr[j2] = i2;
                int i10 = this.f5583a;
                this.f5583a = i10 + 1;
                if (i10 >= this.f5591i) {
                    l(this.f5585c << 1);
                }
                return true;
            } else if (i4 == 0) {
                iArr[k2] = i2;
                int i11 = this.f5583a;
                this.f5583a = i11 + 1;
                if (i11 >= this.f5591i) {
                    l(this.f5585c << 1);
                }
                return true;
            } else {
                a(i2, i5, i6, j2, i3, k2, i4);
                return true;
            }
        } else if (this.f5587e) {
            return false;
        } else {
            this.f5587e = true;
            this.f5583a++;
            return true;
        }
    }

    public void b(int i2) {
        if (this.f5585c <= i2) {
            a();
            return;
        }
        this.f5587e = false;
        this.f5583a = 0;
        l(i2);
    }

    public boolean c(int i2) {
        if (i2 == 0) {
            return this.f5587e;
        }
        if (this.f5584b[this.f5590h & i2] == i2) {
            return true;
        }
        if (this.f5584b[j(i2)] == i2) {
            return true;
        }
        if (this.f5584b[k(i2)] != i2) {
            return i(i2);
        }
        return true;
    }

    public boolean d(int i2) {
        if (i2 != 0) {
            int i3 = this.f5590h & i2;
            int[] iArr = this.f5584b;
            if (iArr[i3] == i2) {
                iArr[i3] = 0;
                this.f5583a--;
                return true;
            }
            int j2 = j(i2);
            int[] iArr2 = this.f5584b;
            if (iArr2[j2] == i2) {
                iArr2[j2] = 0;
                this.f5583a--;
                return true;
            }
            int k2 = k(i2);
            int[] iArr3 = this.f5584b;
            if (iArr3[k2] != i2) {
                return e(i2);
            }
            iArr3[k2] = 0;
            this.f5583a--;
            return true;
        } else if (!this.f5587e) {
            return false;
        } else {
            this.f5587e = false;
            this.f5583a--;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean e(int i2) {
        int[] iArr = this.f5584b;
        int i3 = this.f5585c;
        int i4 = this.f5586d + i3;
        while (i3 < i4) {
            if (iArr[i3] == i2) {
                f(i3);
                this.f5583a--;
                return true;
            }
            i3++;
        }
        return false;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof s)) {
            return false;
        }
        s sVar = (s) obj;
        if (sVar.f5583a != this.f5583a || sVar.f5587e != this.f5587e) {
            return false;
        }
        int[] iArr = this.f5584b;
        int i2 = this.f5585c + this.f5586d;
        for (int i3 = 0; i3 < i2; i3++) {
            if (iArr[i3] != 0 && !sVar.c(iArr[i3])) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void f(int i2) {
        this.f5586d--;
        int i3 = this.f5585c + this.f5586d;
        if (i2 < i3) {
            int[] iArr = this.f5584b;
            iArr[i2] = iArr[i3];
        }
    }

    public int hashCode() {
        int i2 = this.f5585c + this.f5586d;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            int[] iArr = this.f5584b;
            if (iArr[i4] != 0) {
                i3 += iArr[i4];
            }
        }
        return i3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r5 = this;
            int r0 = r5.f5583a
            if (r0 != 0) goto L_0x0007
            java.lang.String r0 = "[]"
            return r0
        L_0x0007:
            com.badlogic.gdx.utils.r0 r0 = new com.badlogic.gdx.utils.r0
            r1 = 32
            r0.<init>(r1)
            r1 = 91
            r0.append(r1)
            int[] r1 = r5.f5584b
            int r2 = r1.length
            boolean r3 = r5.f5587e
            if (r3 == 0) goto L_0x0020
            java.lang.String r3 = "0"
            r0.a(r3)
            goto L_0x002e
        L_0x0020:
            int r3 = r2 + -1
            if (r2 <= 0) goto L_0x002d
            r2 = r1[r3]
            if (r2 != 0) goto L_0x002a
            r2 = r3
            goto L_0x0020
        L_0x002a:
            r0.a(r2)
        L_0x002d:
            r2 = r3
        L_0x002e:
            int r3 = r2 + -1
            if (r2 <= 0) goto L_0x0040
            r2 = r1[r3]
            if (r2 != 0) goto L_0x0037
            goto L_0x002d
        L_0x0037:
            java.lang.String r4 = ", "
            r0.a(r4)
            r0.a(r2)
            goto L_0x002d
        L_0x0040:
            r1 = 93
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.s.toString():java.lang.String");
    }

    public s(int i2, float f2) {
        if (i2 >= 0) {
            int b2 = h.b((int) Math.ceil((double) (((float) i2) / f2)));
            if (b2 <= 1073741824) {
                this.f5585c = b2;
                if (f2 > Animation.CurveTimeline.LINEAR) {
                    this.f5588f = f2;
                    int i3 = this.f5585c;
                    this.f5591i = (int) (((float) i3) * f2);
                    this.f5590h = i3 - 1;
                    this.f5589g = 31 - Integer.numberOfTrailingZeros(i3);
                    this.f5592j = Math.max(3, ((int) Math.ceil(Math.log((double) this.f5585c))) * 2);
                    this.f5593k = Math.max(Math.min(this.f5585c, 8), ((int) Math.sqrt((double) this.f5585c)) / 8);
                    this.f5584b = new int[(this.f5585c + this.f5592j)];
                    return;
                }
                throw new IllegalArgumentException("loadFactor must be > 0: " + f2);
            }
            throw new IllegalArgumentException("initialCapacity is too large: " + b2);
        }
        throw new IllegalArgumentException("initialCapacity must be >= 0: " + i2);
    }

    public a b() {
        if (h.f5490a) {
            return new a(this);
        }
        if (this.l == null) {
            this.l = new a(this);
            this.m = new a(this);
        }
        a aVar = this.l;
        if (!aVar.f5597d) {
            aVar.c();
            a aVar2 = this.l;
            aVar2.f5597d = true;
            this.m.f5597d = false;
            return aVar2;
        }
        this.m.c();
        a aVar3 = this.m;
        aVar3.f5597d = true;
        this.l.f5597d = false;
        return aVar3;
    }

    private void a(int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int[] iArr = this.f5584b;
        int i9 = this.f5590h;
        int i10 = this.f5593k;
        int i11 = 0;
        do {
            int c2 = h.c(2);
            if (c2 == 0) {
                iArr[i3] = i2;
                i2 = i4;
            } else if (c2 != 1) {
                iArr[i7] = i2;
                i2 = i8;
            } else {
                iArr[i5] = i2;
                i2 = i6;
            }
            i3 = i2 & i9;
            i4 = iArr[i3];
            if (i4 == 0) {
                iArr[i3] = i2;
                int i12 = this.f5583a;
                this.f5583a = i12 + 1;
                if (i12 >= this.f5591i) {
                    l(this.f5585c << 1);
                    return;
                }
                return;
            }
            i5 = j(i2);
            i6 = iArr[i5];
            if (i6 == 0) {
                iArr[i5] = i2;
                int i13 = this.f5583a;
                this.f5583a = i13 + 1;
                if (i13 >= this.f5591i) {
                    l(this.f5585c << 1);
                    return;
                }
                return;
            }
            i7 = k(i2);
            i8 = iArr[i7];
            if (i8 == 0) {
                iArr[i7] = i2;
                int i14 = this.f5583a;
                this.f5583a = i14 + 1;
                if (i14 >= this.f5591i) {
                    l(this.f5585c << 1);
                    return;
                }
                return;
            }
            i11++;
        } while (i11 != i10);
        h(i2);
    }

    public void a() {
        if (this.f5583a != 0) {
            int[] iArr = this.f5584b;
            int i2 = this.f5585c + this.f5586d;
            while (true) {
                int i3 = i2 - 1;
                if (i2 > 0) {
                    iArr[i3] = 0;
                    i2 = i3;
                } else {
                    this.f5583a = 0;
                    this.f5586d = 0;
                    this.f5587e = false;
                    return;
                }
            }
        }
    }
}
