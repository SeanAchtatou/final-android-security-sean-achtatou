package com.badlogic.gdx.utils;

/* compiled from: Base64Coder */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public static final a f5443a = new a('+', '/');

    /* renamed from: b  reason: collision with root package name */
    public static final a f5444b = new a('-', '_');

    /* compiled from: Base64Coder */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        protected final char[] f5445a = new char[64];

        /* renamed from: b  reason: collision with root package name */
        protected final byte[] f5446b = new byte[128];

        public a(char c2, char c3) {
            char c4 = 'A';
            int i2 = 0;
            while (c4 <= 'Z') {
                this.f5445a[i2] = c4;
                c4 = (char) (c4 + 1);
                i2++;
            }
            char c5 = 'a';
            while (c5 <= 'z') {
                this.f5445a[i2] = c5;
                c5 = (char) (c5 + 1);
                i2++;
            }
            char c6 = '0';
            while (c6 <= '9') {
                this.f5445a[i2] = c6;
                c6 = (char) (c6 + 1);
                i2++;
            }
            char[] cArr = this.f5445a;
            cArr[i2] = c2;
            cArr[i2 + 1] = c3;
            int i3 = 0;
            while (true) {
                byte[] bArr = this.f5446b;
                if (i3 >= bArr.length) {
                    break;
                }
                bArr[i3] = -1;
                i3++;
            }
            for (int i4 = 0; i4 < 64; i4++) {
                this.f5446b[this.f5445a[i4]] = (byte) i4;
            }
        }
    }

    public static String a(byte[] bArr) {
        return a(bArr, 0, bArr.length, 76, "\n", f5443a.f5445a);
    }

    public static String b(String str, boolean z) {
        return new String(a(str.getBytes(), (z ? f5444b : f5443a).f5445a));
    }

    public static String c(String str) {
        return b(str, false);
    }

    public static String a(byte[] bArr, int i2, int i3, int i4, String str, char[] cArr) {
        int i5 = (i4 * 3) / 4;
        if (i5 > 0) {
            r0 r0Var = new r0((((i3 + 2) / 3) * 4) + ((((i3 + i5) - 1) / i5) * str.length()));
            int i6 = 0;
            while (i6 < i3) {
                int min = Math.min(i3 - i6, i5);
                r0Var.a(a(bArr, i2 + i6, min, cArr));
                r0Var.a(str);
                i6 += min;
            }
            return r0Var.toString();
        }
        throw new IllegalArgumentException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.c.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.badlogic.gdx.utils.c.a(java.lang.String, byte[]):byte[]
      com.badlogic.gdx.utils.c.a(char[], byte[]):byte[]
      com.badlogic.gdx.utils.c.a(byte[], char[]):char[]
      com.badlogic.gdx.utils.c.a(java.lang.String, boolean):java.lang.String */
    public static String b(String str) {
        return a(str, false);
    }

    public static char[] a(byte[] bArr, char[] cArr) {
        return a(bArr, 0, bArr.length, cArr);
    }

    public static char[] a(byte[] bArr, int i2, int i3, char[] cArr) {
        int i4;
        byte b2;
        int i5;
        byte b3;
        int i6 = ((i3 * 4) + 2) / 3;
        char[] cArr2 = new char[(((i3 + 2) / 3) * 4)];
        int i7 = i3 + i2;
        int i8 = 0;
        while (i2 < i7) {
            int i9 = i2 + 1;
            byte b4 = bArr[i2] & 255;
            if (i9 < i7) {
                i4 = i9 + 1;
                b2 = bArr[i9] & 255;
            } else {
                i4 = i9;
                b2 = 0;
            }
            if (i4 < i7) {
                i5 = i4 + 1;
                b3 = bArr[i4] & 255;
            } else {
                i5 = i4;
                b3 = 0;
            }
            int i10 = b4 >>> 2;
            int i11 = ((b4 & 3) << 4) | (b2 >>> 4);
            int i12 = ((b2 & 15) << 2) | (b3 >>> 6);
            byte b5 = b3 & 63;
            int i13 = i8 + 1;
            cArr2[i8] = cArr[i10];
            int i14 = i13 + 1;
            cArr2[i13] = cArr[i11];
            char c2 = '=';
            cArr2[i14] = i14 < i6 ? cArr[i12] : '=';
            int i15 = i14 + 1;
            if (i15 < i6) {
                c2 = cArr[b5];
            }
            cArr2[i15] = c2;
            i8 = i15 + 1;
            i2 = i5;
        }
        return cArr2;
    }

    public static String a(String str, boolean z) {
        return new String(a(str.toCharArray(), (z ? f5444b : f5443a).f5446b));
    }

    public static byte[] a(String str) {
        return a(str, f5443a.f5446b);
    }

    public static byte[] a(String str, byte[] bArr) {
        char[] cArr = new char[str.length()];
        int i2 = 0;
        for (int i3 = 0; i3 < str.length(); i3++) {
            char charAt = str.charAt(i3);
            if (!(charAt == ' ' || charAt == 13 || charAt == 10 || charAt == 9)) {
                cArr[i2] = charAt;
                i2++;
            }
        }
        return a(cArr, 0, i2, bArr);
    }

    public static byte[] a(char[] cArr, byte[] bArr) {
        return a(cArr, 0, cArr.length, bArr);
    }

    public static byte[] a(char[] cArr, int i2, int i3, byte[] bArr) {
        int i4;
        char c2;
        int i5;
        if (i3 % 4 == 0) {
            while (i3 > 0 && cArr[(i2 + i3) - 1] == '=') {
                i3--;
            }
            int i6 = (i3 * 3) / 4;
            byte[] bArr2 = new byte[i6];
            int i7 = i3 + i2;
            int i8 = 0;
            while (i2 < i7) {
                int i9 = i2 + 1;
                char c3 = cArr[i2];
                int i10 = i9 + 1;
                char c4 = cArr[i9];
                char c5 = 'A';
                if (i10 < i7) {
                    i4 = i10 + 1;
                    c2 = cArr[i10];
                } else {
                    i4 = i10;
                    c2 = 'A';
                }
                if (i4 < i7) {
                    char c6 = cArr[i4];
                    i4++;
                    c5 = c6;
                }
                if (c3 > 127 || c4 > 127 || c2 > 127 || c5 > 127) {
                    throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
                }
                byte b2 = bArr[c3];
                byte b3 = bArr[c4];
                byte b4 = bArr[c2];
                byte b5 = bArr[c5];
                if (b2 < 0 || b3 < 0 || b4 < 0 || b5 < 0) {
                    throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
                }
                int i11 = (b2 << 2) | (b3 >>> 4);
                int i12 = ((b3 & 15) << 4) | (b4 >>> 2);
                byte b6 = ((b4 & 3) << 6) | b5;
                int i13 = i8 + 1;
                bArr2[i8] = (byte) i11;
                if (i13 < i6) {
                    i5 = i13 + 1;
                    bArr2[i13] = (byte) i12;
                } else {
                    i5 = i13;
                }
                if (i5 < i6) {
                    i8 = i5 + 1;
                    bArr2[i5] = (byte) b6;
                } else {
                    i8 = i5;
                }
                i2 = i4;
            }
            return bArr2;
        }
        throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
    }
}
