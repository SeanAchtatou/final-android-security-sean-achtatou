package com.badlogic.gdx.utils.z0;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/* compiled from: Field */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final Field f5718a;

    d(Field field) {
        this.f5718a = field;
    }

    public Class a() {
        return this.f5718a.getDeclaringClass();
    }

    public String b() {
        return this.f5718a.getName();
    }

    public Class c() {
        return this.f5718a.getType();
    }

    public boolean d() {
        return this.f5718a.isAccessible();
    }

    public boolean e() {
        return Modifier.isStatic(this.f5718a.getModifiers());
    }

    public boolean f() {
        return this.f5718a.isSynthetic();
    }

    public boolean g() {
        return Modifier.isTransient(this.f5718a.getModifiers());
    }

    public void a(boolean z) {
        this.f5718a.setAccessible(z);
    }

    public Class a(int i2) {
        Type genericType = this.f5718a.getGenericType();
        if (!(genericType instanceof ParameterizedType)) {
            return null;
        }
        Type[] actualTypeArguments = ((ParameterizedType) genericType).getActualTypeArguments();
        if (actualTypeArguments.length - 1 < i2) {
            return null;
        }
        Type type = actualTypeArguments[i2];
        if (type instanceof Class) {
            return (Class) type;
        }
        if (type instanceof ParameterizedType) {
            return (Class) ((ParameterizedType) type).getRawType();
        }
        if (!(type instanceof GenericArrayType)) {
            return null;
        }
        Type genericComponentType = ((GenericArrayType) type).getGenericComponentType();
        if (genericComponentType instanceof Class) {
            return a.a((Class) genericComponentType, 0).getClass();
        }
        return null;
    }

    public boolean a(Class<? extends Annotation> cls) {
        return this.f5718a.isAnnotationPresent(cls);
    }

    public Object a(Object obj) throws f {
        try {
            return this.f5718a.get(obj);
        } catch (IllegalArgumentException e2) {
            throw new f("Object is not an instance of " + a(), e2);
        } catch (IllegalAccessException e3) {
            throw new f("Illegal access to field: " + b(), e3);
        }
    }

    public void a(Object obj, Object obj2) throws f {
        try {
            this.f5718a.set(obj, obj2);
        } catch (IllegalArgumentException e2) {
            throw new f("Argument not valid for field: " + b(), e2);
        } catch (IllegalAccessException e3) {
            throw new f("Illegal access to field: " + b(), e3);
        }
    }
}
