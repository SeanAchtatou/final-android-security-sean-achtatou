package com.badlogic.gdx.utils;

import com.badlogic.gdx.utils.z0.c;

/* compiled from: ReflectionPool */
public class j0<T> extends f0<T> {

    /* renamed from: a  reason: collision with root package name */
    private final c f5511a;

    public j0(Class<T> cls, int i2, int i3) {
        super(i2, i3);
        this.f5511a = a(cls);
        if (this.f5511a == null) {
            throw new RuntimeException("Class cannot be created (missing no-arg constructor): " + cls.getName());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.z0.b.a(java.lang.Class, java.lang.Class[]):com.badlogic.gdx.utils.z0.c
     arg types: [java.lang.Class<T>, ?[OBJECT, ARRAY]]
     candidates:
      com.badlogic.gdx.utils.z0.b.a(java.lang.Class, java.lang.Class):boolean
      com.badlogic.gdx.utils.z0.b.a(java.lang.Class, java.lang.Object):boolean
      com.badlogic.gdx.utils.z0.b.a(java.lang.Class, java.lang.Class[]):com.badlogic.gdx.utils.z0.c */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:4|5|6) */
    /* JADX WARNING: Code restructure failed: missing block: B:5:?, code lost:
        r3 = com.badlogic.gdx.utils.z0.b.b(r3, null);
        r3.a(true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000f, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x0006 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.badlogic.gdx.utils.z0.c a(java.lang.Class<T> r3) {
        /*
            r2 = this;
            r0 = 0
            com.badlogic.gdx.utils.z0.c r3 = com.badlogic.gdx.utils.z0.b.a(r3, r0)     // Catch:{ Exception -> 0x0006 }
            return r3
        L_0x0006:
            com.badlogic.gdx.utils.z0.c r3 = com.badlogic.gdx.utils.z0.b.b(r3, r0)     // Catch:{ f -> 0x000f }
            r1 = 1
            r3.a(r1)     // Catch:{ f -> 0x000f }
            return r3
        L_0x000f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.j0.a(java.lang.Class):com.badlogic.gdx.utils.z0.c");
    }

    /* access modifiers changed from: protected */
    public T newObject() {
        try {
            return this.f5511a.a((Object[]) null);
        } catch (Exception e2) {
            throw new o("Unable to create new instance: " + this.f5511a.a().getName(), e2);
        }
    }
}
