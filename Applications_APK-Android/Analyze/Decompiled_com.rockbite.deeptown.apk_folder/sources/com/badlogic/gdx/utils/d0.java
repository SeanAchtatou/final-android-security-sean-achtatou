package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.h;
import com.esotericsoftware.spine.Animation;
import e.d.b.t.n;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: ObjectSet */
public class d0<T> implements Iterable<T> {

    /* renamed from: a  reason: collision with root package name */
    public int f5466a;

    /* renamed from: b  reason: collision with root package name */
    T[] f5467b;

    /* renamed from: c  reason: collision with root package name */
    int f5468c;

    /* renamed from: d  reason: collision with root package name */
    int f5469d;

    /* renamed from: e  reason: collision with root package name */
    private float f5470e;

    /* renamed from: f  reason: collision with root package name */
    private int f5471f;

    /* renamed from: g  reason: collision with root package name */
    private int f5472g;

    /* renamed from: h  reason: collision with root package name */
    private int f5473h;

    /* renamed from: i  reason: collision with root package name */
    private int f5474i;

    /* renamed from: j  reason: collision with root package name */
    private int f5475j;

    /* renamed from: k  reason: collision with root package name */
    private a f5476k;
    private a l;

    /* compiled from: ObjectSet */
    public static class a<K> implements Iterable<K>, Iterator<K> {

        /* renamed from: a  reason: collision with root package name */
        public boolean f5477a;

        /* renamed from: b  reason: collision with root package name */
        final d0<K> f5478b;

        /* renamed from: c  reason: collision with root package name */
        int f5479c;

        /* renamed from: d  reason: collision with root package name */
        int f5480d;

        /* renamed from: e  reason: collision with root package name */
        boolean f5481e = true;

        public a(d0<K> d0Var) {
            this.f5478b = d0Var;
            a();
        }

        private void b() {
            this.f5477a = false;
            d0<K> d0Var = this.f5478b;
            T[] tArr = d0Var.f5467b;
            int i2 = d0Var.f5468c + d0Var.f5469d;
            do {
                int i3 = this.f5479c + 1;
                this.f5479c = i3;
                if (i3 >= i2) {
                    return;
                }
            } while (tArr[this.f5479c] == null);
            this.f5477a = true;
        }

        public void a() {
            this.f5480d = -1;
            this.f5479c = -1;
            b();
        }

        public boolean hasNext() {
            if (this.f5481e) {
                return this.f5477a;
            }
            throw new o("#iterator() cannot be used nested.");
        }

        public a<K> iterator() {
            return this;
        }

        public K next() {
            if (!this.f5477a) {
                throw new NoSuchElementException();
            } else if (this.f5481e) {
                Object[] objArr = this.f5478b.f5467b;
                int i2 = this.f5479c;
                Object obj = objArr[i2];
                this.f5480d = i2;
                b();
                return obj;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }

        public void remove() {
            int i2 = this.f5480d;
            if (i2 >= 0) {
                d0<K> d0Var = this.f5478b;
                if (i2 >= d0Var.f5468c) {
                    d0Var.d(i2);
                    this.f5479c = this.f5480d - 1;
                    b();
                } else {
                    d0Var.f5467b[i2] = null;
                }
                this.f5480d = -1;
                d0<K> d0Var2 = this.f5478b;
                d0Var2.f5466a--;
                return;
            }
            throw new IllegalStateException("next must be called before remove.");
        }
    }

    public d0() {
        this(51, 0.8f);
    }

    private void a(T t, int i2, T t2, int i3, T t3, int i4, T t4) {
        T[] tArr = this.f5467b;
        int i5 = this.f5472g;
        int i6 = this.f5475j;
        int i7 = 0;
        while (true) {
            int c2 = h.c(2);
            if (c2 == 0) {
                tArr[i2] = t;
                t = t2;
            } else if (c2 != 1) {
                tArr[i4] = t;
                t = t4;
            } else {
                tArr[i3] = t;
                t = t3;
            }
            int hashCode = t.hashCode();
            int i8 = hashCode & i5;
            T t5 = tArr[i8];
            if (t5 == null) {
                tArr[i8] = t;
                int i9 = this.f5466a;
                this.f5466a = i9 + 1;
                if (i9 >= this.f5473h) {
                    g(this.f5468c << 1);
                    return;
                }
                return;
            }
            int e2 = e(hashCode);
            T t6 = tArr[e2];
            if (t6 == null) {
                tArr[e2] = t;
                int i10 = this.f5466a;
                this.f5466a = i10 + 1;
                if (i10 >= this.f5473h) {
                    g(this.f5468c << 1);
                    return;
                }
                return;
            }
            int f2 = f(hashCode);
            t4 = tArr[f2];
            if (t4 == null) {
                tArr[f2] = t;
                int i11 = this.f5466a;
                this.f5466a = i11 + 1;
                if (i11 >= this.f5473h) {
                    g(this.f5468c << 1);
                    return;
                }
                return;
            }
            i7++;
            if (i7 == i6) {
                c(t);
                return;
            }
            T t7 = t6;
            i4 = f2;
            i2 = i8;
            t2 = t5;
            i3 = e2;
            t3 = t7;
        }
    }

    private void b(T t) {
        int hashCode = t.hashCode();
        int i2 = hashCode & this.f5472g;
        T[] tArr = this.f5467b;
        T t2 = tArr[i2];
        if (t2 == null) {
            tArr[i2] = t;
            int i3 = this.f5466a;
            this.f5466a = i3 + 1;
            if (i3 >= this.f5473h) {
                g(this.f5468c << 1);
                return;
            }
            return;
        }
        int e2 = e(hashCode);
        T[] tArr2 = this.f5467b;
        T t3 = tArr2[e2];
        if (t3 == null) {
            tArr2[e2] = t;
            int i4 = this.f5466a;
            this.f5466a = i4 + 1;
            if (i4 >= this.f5473h) {
                g(this.f5468c << 1);
                return;
            }
            return;
        }
        int f2 = f(hashCode);
        T[] tArr3 = this.f5467b;
        T t4 = tArr3[f2];
        if (t4 == null) {
            tArr3[f2] = t;
            int i5 = this.f5466a;
            this.f5466a = i5 + 1;
            if (i5 >= this.f5473h) {
                g(this.f5468c << 1);
                return;
            }
            return;
        }
        a(t, i2, t2, e2, t3, f2, t4);
    }

    private void c(n nVar) {
        int i2 = this.f5469d;
        if (i2 == this.f5474i) {
            g(this.f5468c << 1);
            b(nVar);
            return;
        }
        this.f5467b[this.f5468c + i2] = nVar;
        this.f5469d = i2 + 1;
        this.f5466a++;
    }

    private int e(int i2) {
        int i3 = i2 * -1262997959;
        return (i3 ^ (i3 >>> this.f5471f)) & this.f5472g;
    }

    private int f(int i2) {
        int i3 = i2 * -825114047;
        return (i3 ^ (i3 >>> this.f5471f)) & this.f5472g;
    }

    private void g(int i2) {
        int i3 = this.f5468c + this.f5469d;
        this.f5468c = i2;
        this.f5473h = (int) (((float) i2) * this.f5470e);
        this.f5472g = i2 - 1;
        this.f5471f = 31 - Integer.numberOfTrailingZeros(i2);
        double d2 = (double) i2;
        this.f5474i = Math.max(3, ((int) Math.ceil(Math.log(d2))) * 2);
        this.f5475j = Math.max(Math.min(i2, 8), ((int) Math.sqrt(d2)) / 8);
        T[] tArr = this.f5467b;
        this.f5467b = new Object[(i2 + this.f5474i)];
        int i4 = this.f5466a;
        this.f5466a = 0;
        this.f5469d = 0;
        if (i4 > 0) {
            for (int i5 = 0; i5 < i3; i5++) {
                T t = tArr[i5];
                if (t != null) {
                    b(t);
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean add(T r12) {
        /*
            r11 = this;
            if (r12 == 0) goto L_0x0089
            T[] r0 = r11.f5467b
            int r1 = r12.hashCode()
            int r2 = r11.f5472g
            r5 = r1 & r2
            r6 = r0[r5]
            boolean r2 = r12.equals(r6)
            r3 = 0
            if (r2 == 0) goto L_0x0016
            return r3
        L_0x0016:
            int r7 = r11.e(r1)
            r8 = r0[r7]
            boolean r2 = r12.equals(r8)
            if (r2 == 0) goto L_0x0023
            return r3
        L_0x0023:
            int r9 = r11.f(r1)
            r10 = r0[r9]
            boolean r1 = r12.equals(r10)
            if (r1 == 0) goto L_0x0030
            return r3
        L_0x0030:
            int r1 = r11.f5468c
            int r2 = r11.f5469d
            int r2 = r2 + r1
        L_0x0035:
            if (r1 >= r2) goto L_0x0043
            r4 = r0[r1]
            boolean r4 = r12.equals(r4)
            if (r4 == 0) goto L_0x0040
            return r3
        L_0x0040:
            int r1 = r1 + 1
            goto L_0x0035
        L_0x0043:
            r1 = 1
            if (r6 != 0) goto L_0x0059
            r0[r5] = r12
            int r12 = r11.f5466a
            int r0 = r12 + 1
            r11.f5466a = r0
            int r0 = r11.f5473h
            if (r12 < r0) goto L_0x0058
            int r12 = r11.f5468c
            int r12 = r12 << r1
            r11.g(r12)
        L_0x0058:
            return r1
        L_0x0059:
            if (r8 != 0) goto L_0x006e
            r0[r7] = r12
            int r12 = r11.f5466a
            int r0 = r12 + 1
            r11.f5466a = r0
            int r0 = r11.f5473h
            if (r12 < r0) goto L_0x006d
            int r12 = r11.f5468c
            int r12 = r12 << r1
            r11.g(r12)
        L_0x006d:
            return r1
        L_0x006e:
            if (r10 != 0) goto L_0x0083
            r0[r9] = r12
            int r12 = r11.f5466a
            int r0 = r12 + 1
            r11.f5466a = r0
            int r0 = r11.f5473h
            if (r12 < r0) goto L_0x0082
            int r12 = r11.f5468c
            int r12 = r12 << r1
            r11.g(r12)
        L_0x0082:
            return r1
        L_0x0083:
            r3 = r11
            r4 = r12
            r3.a(r4, r5, r6, r7, r8, r9, r10)
            return r1
        L_0x0089:
            java.lang.IllegalArgumentException r12 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "key cannot be null."
            r12.<init>(r0)
            goto L_0x0092
        L_0x0091:
            throw r12
        L_0x0092:
            goto L_0x0091
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.d0.add(java.lang.Object):boolean");
    }

    public void clear() {
        if (this.f5466a != 0) {
            T[] tArr = this.f5467b;
            int i2 = this.f5468c + this.f5469d;
            while (true) {
                int i3 = i2 - 1;
                if (i2 > 0) {
                    tArr[i3] = null;
                    i2 = i3;
                } else {
                    this.f5466a = 0;
                    this.f5469d = 0;
                    return;
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean contains(T r5) {
        /*
            r4 = this;
            int r0 = r5.hashCode()
            int r1 = r4.f5472g
            r1 = r1 & r0
            T[] r2 = r4.f5467b
            r1 = r2[r1]
            boolean r1 = r5.equals(r1)
            r2 = 1
            if (r1 != 0) goto L_0x0036
            int r1 = r4.e(r0)
            T[] r3 = r4.f5467b
            r1 = r3[r1]
            boolean r1 = r5.equals(r1)
            if (r1 != 0) goto L_0x0036
            int r0 = r4.f(r0)
            T[] r1 = r4.f5467b
            r0 = r1[r0]
            boolean r0 = r5.equals(r0)
            if (r0 != 0) goto L_0x0036
            java.lang.Object r5 = r4.d(r5)
            if (r5 == 0) goto L_0x0035
            goto L_0x0036
        L_0x0035:
            r2 = 0
        L_0x0036:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.d0.contains(java.lang.Object):boolean");
    }

    /* access modifiers changed from: package-private */
    public void d(int i2) {
        this.f5469d--;
        int i3 = this.f5468c + this.f5469d;
        if (i2 < i3) {
            T[] tArr = this.f5467b;
            tArr[i2] = tArr[i3];
            tArr[i3] = null;
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof d0)) {
            return false;
        }
        d0 d0Var = (d0) obj;
        if (d0Var.f5466a != this.f5466a) {
            return false;
        }
        T[] tArr = this.f5467b;
        int i2 = this.f5468c + this.f5469d;
        for (int i3 = 0; i3 < i2; i3++) {
            if (tArr[i3] != null && !d0Var.contains(tArr[i3])) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i2 = this.f5468c + this.f5469d;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            T[] tArr = this.f5467b;
            if (tArr[i4] != null) {
                i3 += tArr[i4].hashCode();
            }
        }
        return i3;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean remove(T r6) {
        /*
            r5 = this;
            int r0 = r6.hashCode()
            int r1 = r5.f5472g
            r1 = r1 & r0
            T[] r2 = r5.f5467b
            r2 = r2[r1]
            boolean r2 = r6.equals(r2)
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x001d
            T[] r6 = r5.f5467b
            r6[r1] = r3
            int r6 = r5.f5466a
            int r6 = r6 - r4
            r5.f5466a = r6
            return r4
        L_0x001d:
            int r1 = r5.e(r0)
            T[] r2 = r5.f5467b
            r2 = r2[r1]
            boolean r2 = r6.equals(r2)
            if (r2 == 0) goto L_0x0035
            T[] r6 = r5.f5467b
            r6[r1] = r3
            int r6 = r5.f5466a
            int r6 = r6 - r4
            r5.f5466a = r6
            return r4
        L_0x0035:
            int r0 = r5.f(r0)
            T[] r1 = r5.f5467b
            r1 = r1[r0]
            boolean r1 = r6.equals(r1)
            if (r1 == 0) goto L_0x004d
            T[] r6 = r5.f5467b
            r6[r0] = r3
            int r6 = r5.f5466a
            int r6 = r6 - r4
            r5.f5466a = r6
            return r4
        L_0x004d:
            boolean r6 = r5.a(r6)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.d0.remove(java.lang.Object):boolean");
    }

    public String toString() {
        return '{' + a(", ") + '}';
    }

    public d0(int i2) {
        this(i2, 0.8f);
    }

    public a<T> iterator() {
        if (h.f5490a) {
            return new a<>(this);
        }
        if (this.f5476k == null) {
            this.f5476k = new a(this);
            this.l = new a(this);
        }
        a aVar = this.f5476k;
        if (!aVar.f5481e) {
            aVar.a();
            a<T> aVar2 = this.f5476k;
            aVar2.f5481e = true;
            this.l.f5481e = false;
            return aVar2;
        }
        this.l.a();
        a<T> aVar3 = this.l;
        aVar3.f5481e = true;
        this.f5476k.f5481e = false;
        return aVar3;
    }

    public d0(int i2, float f2) {
        if (i2 >= 0) {
            int b2 = h.b((int) Math.ceil((double) (((float) i2) / f2)));
            if (b2 <= 1073741824) {
                this.f5468c = b2;
                if (f2 > Animation.CurveTimeline.LINEAR) {
                    this.f5470e = f2;
                    int i3 = this.f5468c;
                    this.f5473h = (int) (((float) i3) * f2);
                    this.f5472g = i3 - 1;
                    this.f5471f = 31 - Integer.numberOfTrailingZeros(i3);
                    this.f5474i = Math.max(3, ((int) Math.ceil(Math.log((double) this.f5468c))) * 2);
                    this.f5475j = Math.max(Math.min(this.f5468c, 8), ((int) Math.sqrt((double) this.f5468c)) / 8);
                    this.f5467b = new Object[(this.f5468c + this.f5474i)];
                    return;
                }
                throw new IllegalArgumentException("loadFactor must be > 0: " + f2);
            }
            throw new IllegalArgumentException("initialCapacity is too large: " + b2);
        }
        throw new IllegalArgumentException("initialCapacity must be >= 0: " + i2);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private T d(T r5) {
        /*
            r4 = this;
            T[] r0 = r4.f5467b
            int r1 = r4.f5468c
            int r2 = r4.f5469d
            int r2 = r2 + r1
        L_0x0007:
            if (r1 >= r2) goto L_0x0017
            r3 = r0[r1]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0014
            r5 = r0[r1]
            return r5
        L_0x0014:
            int r1 = r1 + 1
            goto L_0x0007
        L_0x0017:
            r5 = 0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.d0.d(java.lang.Object):java.lang.Object");
    }

    public void c(int i2) {
        if (this.f5468c <= i2) {
            clear();
            return;
        }
        this.f5466a = 0;
        g(i2);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    boolean a(T r5) {
        /*
            r4 = this;
            T[] r0 = r4.f5467b
            int r1 = r4.f5468c
            int r2 = r4.f5469d
            int r2 = r2 + r1
        L_0x0007:
            if (r1 >= r2) goto L_0x001e
            r3 = r0[r1]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x001b
            r4.d(r1)
            int r5 = r4.f5466a
            r0 = 1
            int r5 = r5 - r0
            r4.f5466a = r5
            return r0
        L_0x001b:
            int r1 = r1 + 1
            goto L_0x0007
        L_0x001e:
            r5 = 0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.d0.a(java.lang.Object):boolean");
    }

    public String a(String str) {
        int i2;
        if (this.f5466a == 0) {
            return "";
        }
        r0 r0Var = new r0(32);
        T[] tArr = this.f5467b;
        int length = tArr.length;
        while (true) {
            i2 = length - 1;
            if (length > 0) {
                T t = tArr[i2];
                if (t != null) {
                    r0Var.a((Object) t);
                    break;
                }
                length = i2;
            } else {
                break;
            }
        }
        while (true) {
            int i3 = i2 - 1;
            if (i2 <= 0) {
                return r0Var.toString();
            }
            T t2 = tArr[i3];
            if (t2 != null) {
                r0Var.a(str);
                r0Var.a((Object) t2);
            }
            i2 = i3;
        }
    }
}
