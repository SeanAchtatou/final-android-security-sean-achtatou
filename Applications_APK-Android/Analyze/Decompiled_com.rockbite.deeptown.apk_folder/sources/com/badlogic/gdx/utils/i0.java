package com.badlogic.gdx.utils;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: Queue */
public class i0<T> implements Iterable<T> {

    /* renamed from: a  reason: collision with root package name */
    protected T[] f5498a;

    /* renamed from: b  reason: collision with root package name */
    protected int f5499b;

    /* renamed from: c  reason: collision with root package name */
    protected int f5500c;

    /* renamed from: d  reason: collision with root package name */
    public int f5501d;

    /* renamed from: e  reason: collision with root package name */
    private a f5502e;

    /* compiled from: Queue */
    public static class a<T> implements Iterable<T> {

        /* renamed from: a  reason: collision with root package name */
        private final i0<T> f5503a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f5504b;

        /* renamed from: c  reason: collision with root package name */
        private b f5505c;

        /* renamed from: d  reason: collision with root package name */
        private b f5506d;

        public a(i0<T> i0Var) {
            this(i0Var, true);
        }

        public Iterator<T> iterator() {
            if (h.f5490a) {
                return new b(this.f5503a, this.f5504b);
            }
            if (this.f5505c == null) {
                this.f5505c = new b(this.f5503a, this.f5504b);
                this.f5506d = new b(this.f5503a, this.f5504b);
            }
            b bVar = this.f5505c;
            if (!bVar.f5510d) {
                bVar.f5509c = 0;
                bVar.f5510d = true;
                this.f5506d.f5510d = false;
                return bVar;
            }
            b bVar2 = this.f5506d;
            bVar2.f5509c = 0;
            bVar2.f5510d = true;
            bVar.f5510d = false;
            return bVar2;
        }

        public a(i0<T> i0Var, boolean z) {
            this.f5503a = i0Var;
            this.f5504b = z;
        }
    }

    /* compiled from: Queue */
    public static class b<T> implements Iterator<T>, Iterable<T> {

        /* renamed from: a  reason: collision with root package name */
        private final i0<T> f5507a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f5508b;

        /* renamed from: c  reason: collision with root package name */
        int f5509c;

        /* renamed from: d  reason: collision with root package name */
        boolean f5510d = true;

        public b(i0<T> i0Var, boolean z) {
            this.f5507a = i0Var;
            this.f5508b = z;
        }

        public boolean hasNext() {
            if (this.f5510d) {
                return this.f5509c < this.f5507a.f5501d;
            }
            throw new o("#iterator() cannot be used nested.");
        }

        public Iterator<T> iterator() {
            return this;
        }

        public T next() {
            int i2 = this.f5509c;
            i0<T> i0Var = this.f5507a;
            if (i2 >= i0Var.f5501d) {
                throw new NoSuchElementException(String.valueOf(i2));
            } else if (this.f5510d) {
                this.f5509c = i2 + 1;
                return i0Var.get(i2);
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }

        public void remove() {
            if (this.f5508b) {
                this.f5509c--;
                this.f5507a.c(this.f5509c);
                return;
            }
            throw new o("Remove not allowed.");
        }
    }

    public i0() {
        this(16);
    }

    public void addLast(T t) {
        T[] tArr = this.f5498a;
        if (this.f5501d == tArr.length) {
            d(tArr.length << 1);
            tArr = this.f5498a;
        }
        int i2 = this.f5500c;
        this.f5500c = i2 + 1;
        tArr[i2] = t;
        if (this.f5500c == tArr.length) {
            this.f5500c = 0;
        }
        this.f5501d++;
    }

    public T c(int i2) {
        T t;
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("index can't be < 0: " + i2);
        } else if (i2 < this.f5501d) {
            T[] tArr = this.f5498a;
            int i3 = this.f5499b;
            int i4 = this.f5500c;
            int i5 = i2 + i3;
            if (i3 < i4) {
                t = tArr[i5];
                System.arraycopy(tArr, i5 + 1, tArr, i5, i4 - i5);
                tArr[i4] = null;
                this.f5500c--;
            } else if (i5 >= tArr.length) {
                int length = i5 - tArr.length;
                t = tArr[length];
                System.arraycopy(tArr, length + 1, tArr, length, i4 - length);
                this.f5500c--;
            } else {
                T t2 = tArr[i5];
                System.arraycopy(tArr, i3, tArr, i3 + 1, i5 - i3);
                tArr[i3] = null;
                this.f5499b++;
                if (this.f5499b == tArr.length) {
                    this.f5499b = 0;
                }
                t = t2;
            }
            this.f5501d--;
            return t;
        } else {
            throw new IndexOutOfBoundsException("index can't be >= size: " + i2 + " >= " + this.f5501d);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.z0.a.a(java.lang.Class, int):java.lang.Object
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.badlogic.gdx.utils.z0.a.a(java.lang.Object, int):java.lang.Object
      com.badlogic.gdx.utils.z0.a.a(java.lang.Class, int):java.lang.Object */
    /* access modifiers changed from: protected */
    public void d(int i2) {
        T[] tArr = this.f5498a;
        int i3 = this.f5499b;
        int i4 = this.f5500c;
        T[] tArr2 = (Object[]) com.badlogic.gdx.utils.z0.a.a((Class) tArr.getClass().getComponentType(), i2);
        if (i3 < i4) {
            System.arraycopy(tArr, i3, tArr2, 0, i4 - i3);
        } else if (this.f5501d > 0) {
            int length = tArr.length - i3;
            System.arraycopy(tArr, i3, tArr2, 0, length);
            System.arraycopy(tArr, 0, tArr2, length, i4);
        }
        this.f5498a = tArr2;
        this.f5499b = 0;
        this.f5500c = this.f5501d;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x003d A[SYNTHETIC] */
    public boolean equals(java.lang.Object r12) {
        /*
            r11 = this;
            r0 = 1
            if (r11 != r12) goto L_0x0004
            return r0
        L_0x0004:
            r1 = 0
            if (r12 == 0) goto L_0x0041
            boolean r2 = r12 instanceof com.badlogic.gdx.utils.i0
            if (r2 != 0) goto L_0x000c
            goto L_0x0041
        L_0x000c:
            com.badlogic.gdx.utils.i0 r12 = (com.badlogic.gdx.utils.i0) r12
            int r2 = r11.f5501d
            int r3 = r12.f5501d
            if (r3 == r2) goto L_0x0015
            return r1
        L_0x0015:
            T[] r3 = r11.f5498a
            int r4 = r3.length
            T[] r5 = r12.f5498a
            int r6 = r5.length
            int r7 = r11.f5499b
            int r12 = r12.f5499b
            r8 = r12
            r12 = 0
        L_0x0021:
            if (r12 >= r2) goto L_0x0040
            r9 = r3[r7]
            r10 = r5[r8]
            if (r9 != 0) goto L_0x002c
            if (r10 != 0) goto L_0x0032
            goto L_0x0033
        L_0x002c:
            boolean r9 = r9.equals(r10)
            if (r9 != 0) goto L_0x0033
        L_0x0032:
            return r1
        L_0x0033:
            int r7 = r7 + 1
            int r8 = r8 + 1
            if (r7 != r4) goto L_0x003a
            r7 = 0
        L_0x003a:
            if (r8 != r6) goto L_0x003d
            r8 = 0
        L_0x003d:
            int r12 = r12 + 1
            goto L_0x0021
        L_0x0040:
            return r0
        L_0x0041:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.i0.equals(java.lang.Object):boolean");
    }

    public T get(int i2) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("index can't be < 0: " + i2);
        } else if (i2 < this.f5501d) {
            T[] tArr = this.f5498a;
            int i3 = this.f5499b + i2;
            if (i3 >= tArr.length) {
                i3 -= tArr.length;
            }
            return tArr[i3];
        } else {
            throw new IndexOutOfBoundsException("index can't be >= size: " + i2 + " >= " + this.f5501d);
        }
    }

    public int hashCode() {
        int i2 = this.f5501d;
        T[] tArr = this.f5498a;
        int length = tArr.length;
        int i3 = i2 + 1;
        int i4 = this.f5499b;
        for (int i5 = 0; i5 < i2; i5++) {
            T t = tArr[i4];
            i3 *= 31;
            if (t != null) {
                i3 += t.hashCode();
            }
            i4++;
            if (i4 == length) {
                i4 = 0;
            }
        }
        return i3;
    }

    public Iterator<T> iterator() {
        if (h.f5490a) {
            return new b(this, true);
        }
        if (this.f5502e == null) {
            this.f5502e = new a(this);
        }
        return this.f5502e.iterator();
    }

    public String toString() {
        if (this.f5501d == 0) {
            return "[]";
        }
        T[] tArr = this.f5498a;
        int i2 = this.f5499b;
        int i3 = this.f5500c;
        r0 r0Var = new r0(64);
        r0Var.append('[');
        r0Var.a((Object) tArr[i2]);
        while (true) {
            i2 = (i2 + 1) % tArr.length;
            if (i2 != i3) {
                r0Var.a(", ");
                r0Var.a((Object) tArr[i2]);
            } else {
                r0Var.append(']');
                return r0Var.toString();
            }
        }
    }

    public i0(int i2) {
        this.f5499b = 0;
        this.f5500c = 0;
        this.f5501d = 0;
        this.f5498a = new Object[i2];
    }
}
