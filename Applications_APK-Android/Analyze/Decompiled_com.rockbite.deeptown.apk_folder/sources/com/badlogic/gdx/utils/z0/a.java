package com.badlogic.gdx.utils.z0;

import java.lang.reflect.Array;

/* compiled from: ArrayReflection */
public final class a {
    public static Object a(Class cls, int i2) {
        return Array.newInstance(cls, i2);
    }

    public static int a(Object obj) {
        return Array.getLength(obj);
    }

    public static Object a(Object obj, int i2) {
        return Array.get(obj, i2);
    }

    public static void a(Object obj, int i2, Object obj2) {
        Array.set(obj, i2, obj2);
    }
}
