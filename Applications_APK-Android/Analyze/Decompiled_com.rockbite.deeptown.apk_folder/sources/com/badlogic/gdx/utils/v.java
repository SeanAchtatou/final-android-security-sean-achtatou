package com.badlogic.gdx.utils;

import com.badlogic.gdx.utils.w;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.ads.AdRequest;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: JsonValue */
public class v implements Iterable<v> {

    /* renamed from: a  reason: collision with root package name */
    private d f5622a;

    /* renamed from: b  reason: collision with root package name */
    private String f5623b;

    /* renamed from: c  reason: collision with root package name */
    private double f5624c;

    /* renamed from: d  reason: collision with root package name */
    private long f5625d;

    /* renamed from: e  reason: collision with root package name */
    public String f5626e;

    /* renamed from: f  reason: collision with root package name */
    public v f5627f;

    /* renamed from: g  reason: collision with root package name */
    public v f5628g;

    /* renamed from: h  reason: collision with root package name */
    public v f5629h;

    /* renamed from: i  reason: collision with root package name */
    public v f5630i;

    /* renamed from: j  reason: collision with root package name */
    public int f5631j;

    /* compiled from: JsonValue */
    static /* synthetic */ class a {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f5632a = new int[d.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.badlogic.gdx.utils.v$d[] r0 = com.badlogic.gdx.utils.v.d.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.badlogic.gdx.utils.v.a.f5632a = r0
                int[] r0 = com.badlogic.gdx.utils.v.a.f5632a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.badlogic.gdx.utils.v$d r1 = com.badlogic.gdx.utils.v.d.stringValue     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.badlogic.gdx.utils.v.a.f5632a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.badlogic.gdx.utils.v$d r1 = com.badlogic.gdx.utils.v.d.doubleValue     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.badlogic.gdx.utils.v.a.f5632a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.badlogic.gdx.utils.v$d r1 = com.badlogic.gdx.utils.v.d.longValue     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.badlogic.gdx.utils.v.a.f5632a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.badlogic.gdx.utils.v$d r1 = com.badlogic.gdx.utils.v.d.booleanValue     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.badlogic.gdx.utils.v.a.f5632a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.badlogic.gdx.utils.v$d r1 = com.badlogic.gdx.utils.v.d.nullValue     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.v.a.<clinit>():void");
        }
    }

    /* compiled from: JsonValue */
    public class b implements Iterator<v>, Iterable<v> {

        /* renamed from: a  reason: collision with root package name */
        v f5633a = v.this.f5627f;

        /* renamed from: b  reason: collision with root package name */
        v f5634b;

        public b() {
        }

        public boolean hasNext() {
            return this.f5633a != null;
        }

        public Iterator<v> iterator() {
            return this;
        }

        public void remove() {
            v vVar = this.f5634b;
            v vVar2 = vVar.f5629h;
            if (vVar2 == null) {
                v vVar3 = v.this;
                vVar3.f5627f = vVar.f5628g;
                v vVar4 = vVar3.f5627f;
                if (vVar4 != null) {
                    vVar4.f5629h = null;
                }
            } else {
                vVar2.f5628g = vVar.f5628g;
                v vVar5 = vVar.f5628g;
                if (vVar5 != null) {
                    vVar5.f5629h = vVar2;
                }
            }
            v vVar6 = v.this;
            vVar6.f5631j--;
        }

        public v next() {
            this.f5634b = this.f5633a;
            v vVar = this.f5634b;
            if (vVar != null) {
                this.f5633a = vVar.f5628g;
                return vVar;
            }
            throw new NoSuchElementException();
        }
    }

    /* compiled from: JsonValue */
    public static class c {

        /* renamed from: a  reason: collision with root package name */
        public w.c f5636a;

        /* renamed from: b  reason: collision with root package name */
        public int f5637b;

        /* renamed from: c  reason: collision with root package name */
        public boolean f5638c;
    }

    /* compiled from: JsonValue */
    public enum d {
        object,
        array,
        stringValue,
        doubleValue,
        longValue,
        booleanValue,
        nullValue
    }

    public v(d dVar) {
        this.f5622a = dVar;
    }

    public v a(String str) {
        v vVar = this.f5627f;
        while (vVar != null) {
            String str2 = vVar.f5626e;
            if (str2 != null && str2.equalsIgnoreCase(str)) {
                break;
            }
            vVar = vVar.f5628g;
        }
        return vVar;
    }

    public byte b() {
        int i2 = a.f5632a[this.f5622a.ordinal()];
        if (i2 == 1) {
            return Byte.parseByte(this.f5623b);
        }
        if (i2 == 2) {
            return (byte) ((int) this.f5624c);
        }
        if (i2 == 3) {
            return (byte) ((int) this.f5625d);
        }
        if (i2 != 4) {
            throw new IllegalStateException("Value cannot be converted to byte: " + this.f5622a);
        } else if (this.f5625d != 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public double c() {
        int i2 = a.f5632a[this.f5622a.ordinal()];
        if (i2 == 1) {
            return Double.parseDouble(this.f5623b);
        }
        if (i2 == 2) {
            return this.f5624c;
        }
        if (i2 == 3) {
            return (double) this.f5625d;
        }
        if (i2 != 4) {
            throw new IllegalStateException("Value cannot be converted to double: " + this.f5622a);
        } else if (this.f5625d != 0) {
            return 1.0d;
        } else {
            return FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        }
    }

    public float d() {
        int i2 = a.f5632a[this.f5622a.ordinal()];
        if (i2 == 1) {
            return Float.parseFloat(this.f5623b);
        }
        if (i2 == 2) {
            return (float) this.f5624c;
        }
        if (i2 == 3) {
            return (float) this.f5625d;
        }
        if (i2 != 4) {
            throw new IllegalStateException("Value cannot be converted to float: " + this.f5622a);
        } else if (this.f5625d != 0) {
            return 1.0f;
        } else {
            return Animation.CurveTimeline.LINEAR;
        }
    }

    public float[] e() {
        float f2;
        if (this.f5622a == d.array) {
            float[] fArr = new float[this.f5631j];
            int i2 = 0;
            v vVar = this.f5627f;
            while (vVar != null) {
                int i3 = a.f5632a[vVar.f5622a.ordinal()];
                if (i3 == 1) {
                    f2 = Float.parseFloat(vVar.f5623b);
                } else if (i3 == 2) {
                    f2 = (float) vVar.f5624c;
                } else if (i3 == 3) {
                    f2 = (float) vVar.f5625d;
                } else if (i3 == 4) {
                    f2 = vVar.f5625d != 0 ? 1.0f : Animation.CurveTimeline.LINEAR;
                } else {
                    throw new IllegalStateException("Value cannot be converted to float: " + vVar.f5622a);
                }
                fArr[i2] = f2;
                vVar = vVar.f5628g;
                i2++;
            }
            return fArr;
        }
        throw new IllegalStateException("Value is not an array: " + this.f5622a);
    }

    public int f() {
        int i2 = a.f5632a[this.f5622a.ordinal()];
        if (i2 == 1) {
            return Integer.parseInt(this.f5623b);
        }
        if (i2 == 2) {
            return (int) this.f5624c;
        }
        if (i2 == 3) {
            return (int) this.f5625d;
        }
        if (i2 != 4) {
            throw new IllegalStateException("Value cannot be converted to int: " + this.f5622a);
        } else if (this.f5625d != 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public int[] g() {
        int i2;
        if (this.f5622a == d.array) {
            int[] iArr = new int[this.f5631j];
            v vVar = this.f5627f;
            int i3 = 0;
            while (vVar != null) {
                int i4 = a.f5632a[vVar.f5622a.ordinal()];
                if (i4 == 1) {
                    i2 = Integer.parseInt(vVar.f5623b);
                } else if (i4 == 2) {
                    i2 = (int) vVar.f5624c;
                } else if (i4 == 3) {
                    i2 = (int) vVar.f5625d;
                } else if (i4 == 4) {
                    i2 = vVar.f5625d != 0 ? 1 : 0;
                } else {
                    throw new IllegalStateException("Value cannot be converted to int: " + vVar.f5622a);
                }
                iArr[i3] = i2;
                vVar = vVar.f5628g;
                i3++;
            }
            return iArr;
        }
        throw new IllegalStateException("Value is not an array: " + this.f5622a);
    }

    public v get(int i2) {
        v vVar = this.f5627f;
        while (vVar != null && i2 > 0) {
            i2--;
            vVar = vVar.f5628g;
        }
        return vVar;
    }

    public float getFloat(int i2) {
        v vVar = get(i2);
        if (vVar != null) {
            return vVar.d();
        }
        throw new IllegalArgumentException("Indexed value not found: " + this.f5626e);
    }

    public long h() {
        int i2 = a.f5632a[this.f5622a.ordinal()];
        if (i2 == 1) {
            return Long.parseLong(this.f5623b);
        }
        if (i2 == 2) {
            return (long) this.f5624c;
        }
        if (i2 == 3) {
            return this.f5625d;
        }
        if (i2 == 4) {
            return this.f5625d != 0 ? 1 : 0;
        }
        throw new IllegalStateException("Value cannot be converted to long: " + this.f5622a);
    }

    public boolean i(String str) {
        return a(str) != null;
    }

    public v j(String str) {
        v a2 = a(str);
        if (a2 == null) {
            return null;
        }
        v vVar = a2.f5629h;
        if (vVar == null) {
            this.f5627f = a2.f5628g;
            v vVar2 = this.f5627f;
            if (vVar2 != null) {
                vVar2.f5629h = null;
            }
        } else {
            vVar.f5628g = a2.f5628g;
            v vVar3 = a2.f5628g;
            if (vVar3 != null) {
                vVar3.f5629h = vVar;
            }
        }
        this.f5631j--;
        return a2;
    }

    public v k(String str) {
        v vVar = this.f5627f;
        while (vVar != null) {
            String str2 = vVar.f5626e;
            if (str2 != null && str2.equalsIgnoreCase(str)) {
                break;
            }
            vVar = vVar.f5628g;
        }
        if (vVar != null) {
            return vVar;
        }
        throw new IllegalArgumentException("Child not found with name: " + str);
    }

    public String[] l() {
        String str;
        if (this.f5622a == d.array) {
            String[] strArr = new String[this.f5631j];
            int i2 = 0;
            v vVar = this.f5627f;
            while (vVar != null) {
                int i3 = a.f5632a[vVar.f5622a.ordinal()];
                if (i3 == 1) {
                    str = vVar.f5623b;
                } else if (i3 == 2) {
                    str = this.f5623b;
                    if (str == null) {
                        str = Double.toString(vVar.f5624c);
                    }
                } else if (i3 == 3) {
                    str = this.f5623b;
                    if (str == null) {
                        str = Long.toString(vVar.f5625d);
                    }
                } else if (i3 == 4) {
                    str = vVar.f5625d != 0 ? "true" : "false";
                } else if (i3 == 5) {
                    str = null;
                } else {
                    throw new IllegalStateException("Value cannot be converted to string: " + vVar.f5622a);
                }
                strArr[i2] = str;
                vVar = vVar.f5628g;
                i2++;
            }
            return strArr;
        }
        throw new IllegalStateException("Value is not an array: " + this.f5622a);
    }

    public void m(String str) {
        this.f5626e = str;
    }

    public boolean n() {
        return this.f5622a == d.array;
    }

    public boolean o() {
        return this.f5622a == d.booleanValue;
    }

    public boolean p() {
        return this.f5622a == d.doubleValue;
    }

    public boolean q() {
        return this.f5622a == d.longValue;
    }

    public boolean r() {
        return this.f5622a == d.nullValue;
    }

    public boolean s() {
        d dVar = this.f5622a;
        return dVar == d.doubleValue || dVar == d.longValue;
    }

    public boolean t() {
        return this.f5622a == d.object;
    }

    public String toString() {
        String str;
        if (!v()) {
            StringBuilder sb = new StringBuilder();
            if (this.f5626e == null) {
                str = "";
            } else {
                str = this.f5626e + ": ";
            }
            sb.append(str);
            sb.append(a(w.c.minimal, 0));
            return sb.toString();
        } else if (this.f5626e == null) {
            return k();
        } else {
            return this.f5626e + ": " + k();
        }
    }

    public boolean u() {
        return this.f5622a == d.stringValue;
    }

    public boolean v() {
        int i2 = a.f5632a[this.f5622a.ordinal()];
        return i2 == 1 || i2 == 2 || i2 == 3 || i2 == 4 || i2 == 5;
    }

    public String w() {
        return this.f5626e;
    }

    public v x() {
        return this.f5628g;
    }

    public String y() {
        v vVar = this.f5630i;
        String str = "[]";
        if (vVar == null) {
            d dVar = this.f5622a;
            if (dVar == d.array) {
                return str;
            }
            return dVar == d.object ? "{}" : "";
        }
        if (vVar.f5622a == d.array) {
            int i2 = 0;
            v vVar2 = vVar.f5627f;
            while (true) {
                if (vVar2 == null) {
                    break;
                } else if (vVar2 == this) {
                    str = "[" + i2 + "]";
                    break;
                } else {
                    vVar2 = vVar2.f5628g;
                    i2++;
                }
            }
        } else if (this.f5626e.indexOf(46) != -1) {
            str = ".\"" + this.f5626e.replace("\"", "\\\"") + "\"";
        } else {
            str = '.' + this.f5626e;
        }
        return this.f5630i.y() + str;
    }

    public short i() {
        int i2 = a.f5632a[this.f5622a.ordinal()];
        if (i2 == 1) {
            return Short.parseShort(this.f5623b);
        }
        if (i2 == 2) {
            return (short) ((int) this.f5624c);
        }
        if (i2 == 3) {
            return (short) ((int) this.f5625d);
        }
        if (i2 != 4) {
            throw new IllegalStateException("Value cannot be converted to short: " + this.f5622a);
        } else if (this.f5625d != 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public b iterator() {
        return new b();
    }

    public v m() {
        return this.f5627f;
    }

    public v(String str) {
        l(str);
    }

    public boolean a() {
        int i2 = a.f5632a[this.f5622a.ordinal()];
        if (i2 == 1) {
            return this.f5623b.equalsIgnoreCase("true");
        }
        if (i2 != 2) {
            if (i2 != 3) {
                if (i2 != 4) {
                    throw new IllegalStateException("Value cannot be converted to boolean: " + this.f5622a);
                } else if (this.f5625d != 0) {
                    return true;
                } else {
                    return false;
                }
            } else if (this.f5625d != 0) {
                return true;
            } else {
                return false;
            }
        } else if (this.f5624c != FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
            return true;
        } else {
            return false;
        }
    }

    public v(double d2) {
        a(d2, (String) null);
    }

    public String k() {
        int i2 = a.f5632a[this.f5622a.ordinal()];
        if (i2 == 1) {
            return this.f5623b;
        }
        if (i2 == 2) {
            String str = this.f5623b;
            return str != null ? str : Double.toString(this.f5624c);
        } else if (i2 == 3) {
            String str2 = this.f5623b;
            return str2 != null ? str2 : Long.toString(this.f5625d);
        } else if (i2 == 4) {
            return this.f5625d != 0 ? "true" : "false";
        } else {
            if (i2 == 5) {
                return null;
            }
            throw new IllegalStateException("Value cannot be converted to string: " + this.f5622a);
        }
    }

    public v(long j2) {
        a(j2, (String) null);
    }

    public boolean b(String str) {
        v a2 = a(str);
        if (a2 != null) {
            return a2.a();
        }
        throw new IllegalArgumentException("Named value not found: " + str);
    }

    public v c(String str) {
        v a2 = a(str);
        if (a2 == null) {
            return null;
        }
        return a2.f5627f;
    }

    public double d(String str) {
        v a2 = a(str);
        if (a2 != null) {
            return a2.c();
        }
        throw new IllegalArgumentException("Named value not found: " + str);
    }

    public int f(String str) {
        v a2 = a(str);
        if (a2 != null) {
            return a2.f();
        }
        throw new IllegalArgumentException("Named value not found: " + str);
    }

    public String h(String str) {
        v a2 = a(str);
        if (a2 != null) {
            return a2.k();
        }
        throw new IllegalArgumentException("Named value not found: " + str);
    }

    public short[] j() {
        short s;
        int i2;
        if (this.f5622a == d.array) {
            short[] sArr = new short[this.f5631j];
            v vVar = this.f5627f;
            int i3 = 0;
            while (vVar != null) {
                int i4 = a.f5632a[vVar.f5622a.ordinal()];
                if (i4 != 1) {
                    if (i4 == 2) {
                        i2 = (int) vVar.f5624c;
                    } else if (i4 == 3) {
                        i2 = (int) vVar.f5625d;
                    } else if (i4 == 4) {
                        s = vVar.f5625d != 0 ? (short) 1 : 0;
                    } else {
                        throw new IllegalStateException("Value cannot be converted to short: " + vVar.f5622a);
                    }
                    s = (short) i2;
                } else {
                    s = Short.parseShort(vVar.f5623b);
                }
                sArr[i3] = s;
                vVar = vVar.f5628g;
                i3++;
            }
            return sArr;
        }
        throw new IllegalStateException("Value is not an array: " + this.f5622a);
    }

    public v(double d2, String str) {
        a(d2, str);
    }

    public short c(int i2) {
        v vVar = get(i2);
        if (vVar != null) {
            return vVar.i();
        }
        throw new IllegalArgumentException("Indexed value not found: " + this.f5626e);
    }

    private static boolean b(v vVar) {
        for (v vVar2 = vVar.f5627f; vVar2 != null; vVar2 = vVar2.f5628g) {
            if (vVar2.t() || vVar2.n()) {
                return false;
            }
        }
        return true;
    }

    public String a(String str, String str2) {
        v a2 = a(str);
        return (a2 == null || !a2.v() || a2.r()) ? str2 : a2.k();
    }

    public v(long j2, String str) {
        a(j2, str);
    }

    private static boolean c(v vVar) {
        for (v vVar2 = vVar.f5627f; vVar2 != null; vVar2 = vVar2.f5628g) {
            if (!vVar2.s()) {
                return false;
            }
        }
        return true;
    }

    public float a(String str, float f2) {
        v a2 = a(str);
        return (a2 == null || !a2.v() || a2.r()) ? f2 : a2.d();
    }

    public v(boolean z) {
        a(z);
    }

    public float e(String str) {
        v a2 = a(str);
        if (a2 != null) {
            return a2.d();
        }
        throw new IllegalArgumentException("Named value not found: " + str);
    }

    public long g(String str) {
        v a2 = a(str);
        if (a2 != null) {
            return a2.h();
        }
        throw new IllegalArgumentException("Named value not found: " + str);
    }

    public void l(String str) {
        this.f5623b = str;
        this.f5622a = str == null ? d.nullValue : d.stringValue;
    }

    public int a(String str, int i2) {
        v a2 = a(str);
        return (a2 == null || !a2.v() || a2.r()) ? i2 : a2.f();
    }

    public boolean a(String str, boolean z) {
        v a2 = a(str);
        return (a2 == null || !a2.v() || a2.r()) ? z : a2.a();
    }

    public void a(d dVar) {
        if (dVar != null) {
            this.f5622a = dVar;
            return;
        }
        throw new IllegalArgumentException("type cannot be null.");
    }

    public void a(String str, v vVar) {
        if (str != null) {
            vVar.f5626e = str;
            a(vVar);
            return;
        }
        throw new IllegalArgumentException("name cannot be null.");
    }

    public void a(v vVar) {
        vVar.f5630i = this;
        v vVar2 = this.f5627f;
        if (vVar2 == null) {
            this.f5627f = vVar;
            return;
        }
        while (true) {
            v vVar3 = vVar2.f5628g;
            if (vVar3 == null) {
                vVar2.f5628g = vVar;
                return;
            }
            vVar2 = vVar3;
        }
    }

    public void a(double d2, String str) {
        this.f5624c = d2;
        this.f5625d = (long) d2;
        this.f5623b = str;
        this.f5622a = d.doubleValue;
    }

    public void a(long j2, String str) {
        this.f5625d = j2;
        this.f5624c = (double) j2;
        this.f5623b = str;
        this.f5622a = d.longValue;
    }

    public void a(boolean z) {
        this.f5625d = z ? 1 : 0;
        this.f5622a = d.booleanValue;
    }

    public String a(w.c cVar) {
        if (v()) {
            return k();
        }
        r0 r0Var = new r0((int) AdRequest.MAX_CONTENT_URL_LENGTH);
        a(this, r0Var, cVar);
        return r0Var.toString();
    }

    private void a(v vVar, r0 r0Var, w.c cVar) {
        if (vVar.t()) {
            if (vVar.f5627f == null) {
                r0Var.a("{}");
                return;
            }
            r0Var.length();
            r0Var.append('{');
            for (v vVar2 = vVar.f5627f; vVar2 != null; vVar2 = vVar2.f5628g) {
                r0Var.a(cVar.a(vVar2.f5626e));
                r0Var.append(':');
                a(vVar2, r0Var, cVar);
                if (vVar2.f5628g != null) {
                    r0Var.append(',');
                }
            }
            r0Var.append('}');
        } else if (vVar.n()) {
            if (vVar.f5627f == null) {
                r0Var.a("[]");
                return;
            }
            r0Var.length();
            r0Var.append('[');
            for (v vVar3 = vVar.f5627f; vVar3 != null; vVar3 = vVar3.f5628g) {
                a(vVar3, r0Var, cVar);
                if (vVar3.f5628g != null) {
                    r0Var.append(',');
                }
            }
            r0Var.append(']');
        } else if (vVar.u()) {
            r0Var.a(cVar.a((Object) vVar.k()));
        } else if (vVar.p()) {
            double c2 = vVar.c();
            double h2 = (double) vVar.h();
            if (c2 == h2) {
                c2 = h2;
            }
            r0Var.a(c2);
        } else if (vVar.q()) {
            r0Var.a(vVar.h());
        } else if (vVar.o()) {
            r0Var.a(vVar.a());
        } else if (vVar.r()) {
            r0Var.a("null");
        } else {
            throw new l0("Unknown object type: " + vVar);
        }
    }

    public String a(w.c cVar, int i2) {
        c cVar2 = new c();
        cVar2.f5636a = cVar;
        cVar2.f5637b = i2;
        return a(cVar2);
    }

    public String a(c cVar) {
        r0 r0Var = new r0((int) AdRequest.MAX_CONTENT_URL_LENGTH);
        a(this, r0Var, 0, cVar);
        return r0Var.toString();
    }

    private void a(v vVar, r0 r0Var, int i2, c cVar) {
        w.c cVar2 = cVar.f5636a;
        if (vVar.t()) {
            if (vVar.f5627f == null) {
                r0Var.a("{}");
                return;
            }
            boolean z = !b(vVar);
            int length = r0Var.length();
            loop0:
            while (true) {
                r0Var.a(z ? "{\n" : "{ ");
                v vVar2 = vVar.f5627f;
                while (vVar2 != null) {
                    if (z) {
                        a(i2, r0Var);
                    }
                    r0Var.a(cVar2.a(vVar2.f5626e));
                    r0Var.a(": ");
                    a(vVar2, r0Var, i2 + 1, cVar);
                    if ((!z || cVar2 != w.c.minimal) && vVar2.f5628g != null) {
                        r0Var.append(',');
                    }
                    r0Var.append(z ? 10 : ' ');
                    if (z || r0Var.length() - length <= cVar.f5637b) {
                        vVar2 = vVar2.f5628g;
                    } else {
                        r0Var.b(length);
                        z = true;
                    }
                }
                break loop0;
            }
            if (z) {
                a(i2 - 1, r0Var);
            }
            r0Var.append('}');
        } else if (vVar.n()) {
            if (vVar.f5627f == null) {
                r0Var.a("[]");
                return;
            }
            boolean z2 = !b(vVar);
            boolean z3 = cVar.f5638c || !c(vVar);
            int length2 = r0Var.length();
            loop2:
            while (true) {
                r0Var.a(z2 ? "[\n" : "[ ");
                v vVar3 = vVar.f5627f;
                while (vVar3 != null) {
                    if (z2) {
                        a(i2, r0Var);
                    }
                    a(vVar3, r0Var, i2 + 1, cVar);
                    if ((!z2 || cVar2 != w.c.minimal) && vVar3.f5628g != null) {
                        r0Var.append(',');
                    }
                    r0Var.append(z2 ? 10 : ' ');
                    if (!z3 || z2 || r0Var.length() - length2 <= cVar.f5637b) {
                        vVar3 = vVar3.f5628g;
                    } else {
                        r0Var.b(length2);
                        z2 = true;
                    }
                }
                break loop2;
            }
            if (z2) {
                a(i2 - 1, r0Var);
            }
            r0Var.append(']');
        } else if (vVar.u()) {
            r0Var.a(cVar2.a((Object) vVar.k()));
        } else if (vVar.p()) {
            double c2 = vVar.c();
            double h2 = (double) vVar.h();
            if (c2 == h2) {
                c2 = h2;
            }
            r0Var.a(c2);
        } else if (vVar.q()) {
            r0Var.a(vVar.h());
        } else if (vVar.o()) {
            r0Var.a(vVar.a());
        } else if (vVar.r()) {
            r0Var.a("null");
        } else {
            throw new l0("Unknown object type: " + vVar);
        }
    }

    private static void a(int i2, r0 r0Var) {
        for (int i3 = 0; i3 < i2; i3++) {
            r0Var.append(9);
        }
    }
}
