package com.badlogic.gdx.utils.a1;

import com.badlogic.gdx.graphics.glutils.h;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.n;
import com.badlogic.gdx.math.p;
import com.badlogic.gdx.math.q;
import com.esotericsoftware.spine.Animation;
import e.d.b.t.a;
import e.d.b.w.a.l.m;

/* compiled from: Viewport */
public abstract class e {

    /* renamed from: a  reason: collision with root package name */
    private a f5406a;

    /* renamed from: b  reason: collision with root package name */
    private float f5407b;

    /* renamed from: c  reason: collision with root package name */
    private float f5408c;

    /* renamed from: d  reason: collision with root package name */
    private int f5409d;

    /* renamed from: e  reason: collision with root package name */
    private int f5410e;

    /* renamed from: f  reason: collision with root package name */
    private int f5411f;

    /* renamed from: g  reason: collision with root package name */
    private int f5412g;

    /* renamed from: h  reason: collision with root package name */
    private final q f5413h = new q();

    public void a() {
        a(false);
    }

    public abstract void a(int i2, int i3, boolean z);

    public p b(p pVar) {
        this.f5413h.c(pVar.f5294a, pVar.f5295b, 1.0f);
        this.f5406a.b(this.f5413h, (float) this.f5409d, (float) this.f5410e, (float) this.f5411f, (float) this.f5412g);
        q qVar = this.f5413h;
        pVar.d(qVar.f5296a, qVar.f5297b);
        return pVar;
    }

    public int c() {
        return this.f5412g;
    }

    public int d() {
        return this.f5411f;
    }

    public int e() {
        return this.f5409d;
    }

    public int f() {
        return this.f5410e;
    }

    public float g() {
        return this.f5408c;
    }

    public float h() {
        return this.f5407b;
    }

    public void a(boolean z) {
        h.b(this.f5409d, this.f5410e, this.f5411f, this.f5412g);
        a aVar = this.f5406a;
        float f2 = this.f5407b;
        aVar.f6925j = f2;
        float f3 = this.f5408c;
        aVar.f6926k = f3;
        if (z) {
            aVar.f6916a.c(f2 / 2.0f, f3 / 2.0f, Animation.CurveTimeline.LINEAR);
        }
        this.f5406a.a();
    }

    public a b() {
        return this.f5406a;
    }

    public p a(p pVar) {
        this.f5413h.c(pVar.f5294a, pVar.f5295b, 1.0f);
        this.f5406a.a(this.f5413h, (float) this.f5409d, (float) this.f5410e, (float) this.f5411f, (float) this.f5412g);
        q qVar = this.f5413h;
        pVar.d(qVar.f5296a, qVar.f5297b);
        return pVar;
    }

    public void a(Matrix4 matrix4, n nVar, n nVar2) {
        m.a(this.f5406a, (float) this.f5409d, (float) this.f5410e, (float) this.f5411f, (float) this.f5412g, matrix4, nVar, nVar2);
    }

    public void a(a aVar) {
        this.f5406a = aVar;
    }

    public void a(float f2, float f3) {
        this.f5407b = f2;
        this.f5408c = f3;
    }

    public void a(int i2, int i3, int i4, int i5) {
        this.f5409d = i2;
        this.f5410e = i3;
        this.f5411f = i4;
        this.f5412g = i5;
    }
}
