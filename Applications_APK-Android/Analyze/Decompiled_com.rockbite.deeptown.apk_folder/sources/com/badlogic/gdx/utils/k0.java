package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.p;

/* compiled from: Scaling */
public enum k0 {
    fit,
    fill,
    fillX,
    fillY,
    stretch,
    stretchX,
    stretchY,
    none;
    

    /* renamed from: i  reason: collision with root package name */
    private static final p f5523i = new p();

    /* compiled from: Scaling */
    static /* synthetic */ class a {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f5525a = new int[k0.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.badlogic.gdx.utils.k0[] r0 = com.badlogic.gdx.utils.k0.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.badlogic.gdx.utils.k0.a.f5525a = r0
                int[] r0 = com.badlogic.gdx.utils.k0.a.f5525a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.badlogic.gdx.utils.k0 r1 = com.badlogic.gdx.utils.k0.fit     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.badlogic.gdx.utils.k0.a.f5525a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.badlogic.gdx.utils.k0 r1 = com.badlogic.gdx.utils.k0.fill     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.badlogic.gdx.utils.k0.a.f5525a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.badlogic.gdx.utils.k0 r1 = com.badlogic.gdx.utils.k0.fillX     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.badlogic.gdx.utils.k0.a.f5525a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.badlogic.gdx.utils.k0 r1 = com.badlogic.gdx.utils.k0.fillY     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.badlogic.gdx.utils.k0.a.f5525a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.badlogic.gdx.utils.k0 r1 = com.badlogic.gdx.utils.k0.stretch     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = com.badlogic.gdx.utils.k0.a.f5525a     // Catch:{ NoSuchFieldError -> 0x004b }
                com.badlogic.gdx.utils.k0 r1 = com.badlogic.gdx.utils.k0.stretchX     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = com.badlogic.gdx.utils.k0.a.f5525a     // Catch:{ NoSuchFieldError -> 0x0056 }
                com.badlogic.gdx.utils.k0 r1 = com.badlogic.gdx.utils.k0.stretchY     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                int[] r0 = com.badlogic.gdx.utils.k0.a.f5525a     // Catch:{ NoSuchFieldError -> 0x0062 }
                com.badlogic.gdx.utils.k0 r1 = com.badlogic.gdx.utils.k0.none     // Catch:{ NoSuchFieldError -> 0x0062 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0062 }
            L_0x0062:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.k0.a.<clinit>():void");
        }
    }

    public p a(float f2, float f3, float f4, float f5) {
        switch (a.f5525a[ordinal()]) {
            case 1:
                float f6 = f5 / f4 > f3 / f2 ? f4 / f2 : f5 / f3;
                p pVar = f5523i;
                pVar.f5294a = f2 * f6;
                pVar.f5295b = f3 * f6;
                break;
            case 2:
                float f7 = f5 / f4 < f3 / f2 ? f4 / f2 : f5 / f3;
                p pVar2 = f5523i;
                pVar2.f5294a = f2 * f7;
                pVar2.f5295b = f3 * f7;
                break;
            case 3:
                float f8 = f4 / f2;
                p pVar3 = f5523i;
                pVar3.f5294a = f2 * f8;
                pVar3.f5295b = f3 * f8;
                break;
            case 4:
                float f9 = f5 / f3;
                p pVar4 = f5523i;
                pVar4.f5294a = f2 * f9;
                pVar4.f5295b = f3 * f9;
                break;
            case 5:
                p pVar5 = f5523i;
                pVar5.f5294a = f4;
                pVar5.f5295b = f5;
                break;
            case 6:
                p pVar6 = f5523i;
                pVar6.f5294a = f4;
                pVar6.f5295b = f3;
                break;
            case 7:
                p pVar7 = f5523i;
                pVar7.f5294a = f2;
                pVar7.f5295b = f5;
                break;
            case 8:
                p pVar8 = f5523i;
                pVar8.f5294a = f2;
                pVar8.f5295b = f3;
                break;
        }
        return f5523i;
    }
}
