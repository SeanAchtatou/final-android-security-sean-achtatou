package com.badlogic.gdx.utils;

import com.badlogic.gdx.utils.v;
import e.d.b.s.a;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/* compiled from: JsonReader */
public class u implements d {

    /* renamed from: e  reason: collision with root package name */
    private static final byte[] f5611e = b();

    /* renamed from: f  reason: collision with root package name */
    private static final short[] f5612f = f();

    /* renamed from: g  reason: collision with root package name */
    private static final char[] f5613g = j();

    /* renamed from: h  reason: collision with root package name */
    private static final byte[] f5614h = h();

    /* renamed from: i  reason: collision with root package name */
    private static final byte[] f5615i = g();

    /* renamed from: j  reason: collision with root package name */
    private static final short[] f5616j = d();

    /* renamed from: k  reason: collision with root package name */
    private static final byte[] f5617k = e();
    private static final byte[] l = k();
    private static final byte[] m = i();
    private static final byte[] n = c();

    /* renamed from: a  reason: collision with root package name */
    private final a<v> f5618a = new a<>(8);

    /* renamed from: b  reason: collision with root package name */
    private final a<v> f5619b = new a<>(8);

    /* renamed from: c  reason: collision with root package name */
    private v f5620c;

    /* renamed from: d  reason: collision with root package name */
    private v f5621d;

    private static byte[] b() {
        return new byte[]{0, 1, 1, 1, 2, 1, 3, 1, 4, 1, 5, 1, 6, 1, 7, 1, 8, 2, 0, 7, 2, 0, 8, 2, 1, 3, 2, 1, 5};
    }

    private static byte[] c() {
        return new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0};
    }

    private static short[] d() {
        return new short[]{0, 0, 11, 14, 16, 19, 28, 34, 40, 43, 54, 62, 70, 79, 81, 90, 93, 96, 105, 108, 111, 113, 116, 119, 130, 138, 146, 157, 159, 170, 173, 176, 187, 190, 193, 196, 201, 206, 207};
    }

    private static byte[] e() {
        return new byte[]{1, 1, 2, 3, 4, 3, 5, 3, 6, 1, 0, 7, 7, 3, 8, 3, 9, 9, 3, 11, 11, 12, 13, 14, 3, 15, 11, 10, 16, 16, 17, 18, 16, 3, 19, 19, 20, 21, 19, 3, 22, 22, 3, 21, 21, 24, 3, 25, 3, 26, 3, 27, 21, 23, 28, 29, 29, 28, 30, 31, 32, 3, 33, 34, 34, 33, 13, 35, 15, 3, 34, 34, 12, 36, 37, 3, 15, 34, 10, 16, 3, 36, 36, 12, 3, 38, 3, 3, 36, 10, 39, 39, 3, 40, 40, 3, 13, 13, 12, 3, 41, 3, 15, 13, 10, 42, 42, 3, 43, 43, 3, 28, 3, 44, 44, 3, 45, 45, 3, 47, 47, 48, 49, 50, 3, 51, 52, 53, 47, 46, 54, 55, 55, 54, 56, 57, 58, 3, 59, 60, 60, 59, 49, 61, 52, 3, 60, 60, 48, 62, 63, 3, 51, 52, 53, 60, 46, 54, 3, 62, 62, 48, 3, 64, 3, 51, 3, 53, 62, 46, 65, 65, 3, 66, 66, 3, 49, 49, 48, 3, 67, 3, 51, 52, 53, 49, 46, 68, 68, 3, 69, 69, 3, 70, 70, 3, 8, 8, 71, 8, 3, 72, 72, 73, 72, 3, 3, 3, 0};
    }

    private static short[] f() {
        return new short[]{0, 0, 11, 13, 14, 16, 25, 31, 37, 39, 50, 57, 64, 73, 74, 83, 85, 87, 96, 98, 100, 101, 103, 105, 116, 123, 130, 141, 142, 153, 155, 157, 168, 170, 172, 174, 179, 184, 184};
    }

    private static byte[] g() {
        return new byte[]{0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0};
    }

    private static byte[] h() {
        return new byte[]{0, 9, 2, 1, 2, 7, 4, 4, 2, 9, 7, 7, 7, 1, 7, 2, 2, 7, 2, 2, 1, 2, 2, 9, 7, 7, 9, 1, 9, 2, 2, 9, 2, 2, 2, 3, 3, 0, 0};
    }

    private static byte[] i() {
        return new byte[]{13, 0, 15, 0, 0, 7, 3, 11, 1, 11, 17, 0, 20, 0, 0, 5, 1, 1, 1, 0, 0, 0, 11, 13, 15, 0, 7, 3, 1, 1, 1, 1, 23, 0, 0, 0, 0, 0, 0, 11, 11, 0, 11, 11, 11, 11, 13, 0, 15, 0, 0, 7, 9, 3, 1, 1, 1, 1, 26, 0, 0, 0, 0, 0, 0, 11, 11, 0, 11, 11, 11, 1, 0, 0};
    }

    private static char[] j() {
        return new char[]{13, ' ', '\"', ',', '/', ':', '[', ']', '{', 9, 10, '*', '/', '\"', '*', '/', 13, ' ', '\"', ',', '/', ':', '}', 9, 10, 13, ' ', '/', ':', 9, 10, 13, ' ', '/', ':', 9, 10, '*', '/', 13, ' ', '\"', ',', '/', ':', '[', ']', '{', 9, 10, 9, 10, 13, ' ', ',', '/', '}', 9, 10, 13, ' ', ',', '/', '}', 13, ' ', '\"', ',', '/', ':', '}', 9, 10, '\"', 13, ' ', '\"', ',', '/', ':', '}', 9, 10, '*', '/', '*', '/', 13, ' ', '\"', ',', '/', ':', '}', 9, 10, '*', '/', '*', '/', '\"', '*', '/', '*', '/', 13, ' ', '\"', ',', '/', ':', '[', ']', '{', 9, 10, 9, 10, 13, ' ', ',', '/', ']', 9, 10, 13, ' ', ',', '/', ']', 13, ' ', '\"', ',', '/', ':', '[', ']', '{', 9, 10, '\"', 13, ' ', '\"', ',', '/', ':', '[', ']', '{', 9, 10, '*', '/', '*', '/', 13, ' ', '\"', ',', '/', ':', '[', ']', '{', 9, 10, '*', '/', '*', '/', '*', '/', 13, ' ', '/', 9, 10, 13, ' ', '/', 9, 10, 0};
    }

    private static byte[] k() {
        return new byte[]{35, 1, 3, 0, 4, 36, 36, 36, 36, 1, 6, 5, 13, 17, 22, 37, 7, 8, 9, 7, 8, 9, 7, 10, 20, 21, 11, 11, 11, 12, 17, 19, 37, 11, 12, 19, 14, 16, 15, 14, 12, 18, 17, 11, 9, 5, 24, 23, 27, 31, 34, 25, 38, 25, 25, 26, 31, 33, 38, 25, 26, 33, 28, 30, 29, 28, 26, 32, 31, 25, 23, 2, 36, 2};
    }

    public v a(String str) {
        char[] charArray = str.toCharArray();
        return a(charArray, 0, charArray.length);
    }

    private String d(String str) {
        int length = str.length();
        r0 r0Var = new r0(length + 16);
        int i2 = 0;
        while (i2 < length) {
            int i3 = i2 + 1;
            char charAt = str.charAt(i2);
            if (charAt != '\\') {
                r0Var.append(charAt);
            } else if (i3 == length) {
                break;
            } else {
                i2 = i3 + 1;
                char charAt2 = str.charAt(i3);
                if (charAt2 == 'u') {
                    i3 = i2 + 4;
                    r0Var.a(Character.toChars(Integer.parseInt(str.substring(i2, i3), 16)));
                } else {
                    if (!(charAt2 == '\"' || charAt2 == '/' || charAt2 == '\\')) {
                        if (charAt2 == 'b') {
                            charAt2 = 8;
                        } else if (charAt2 == 'f') {
                            charAt2 = 12;
                        } else if (charAt2 == 'n') {
                            charAt2 = 10;
                        } else if (charAt2 == 'r') {
                            charAt2 = 13;
                        } else if (charAt2 == 't') {
                            charAt2 = 9;
                        } else {
                            throw new l0("Illegal escaped character: \\" + charAt2);
                        }
                    }
                    r0Var.append(charAt2);
                }
            }
            i2 = i3;
        }
        return r0Var.toString();
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        v vVar = new v(v.d.array);
        if (this.f5621d != null) {
            a(str, vVar);
        }
        this.f5618a.add(vVar);
        this.f5621d = vVar;
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
        v vVar = new v(v.d.object);
        if (this.f5621d != null) {
            a(str, vVar);
        }
        this.f5618a.add(vVar);
        this.f5621d = vVar;
    }

    public v a(Reader reader) {
        try {
            char[] cArr = new char[1024];
            int i2 = 0;
            while (true) {
                int read = reader.read(cArr, i2, cArr.length - i2);
                if (read == -1) {
                    v a2 = a(cArr, 0, i2);
                    q0.a(reader);
                    return a2;
                } else if (read == 0) {
                    char[] cArr2 = new char[(cArr.length * 2)];
                    System.arraycopy(cArr, 0, cArr2, 0, cArr.length);
                    cArr = cArr2;
                } else {
                    i2 += read;
                }
            }
        } catch (IOException e2) {
            throw new l0(e2);
        } catch (Throwable th) {
            q0.a(reader);
            throw th;
        }
    }

    public v a(InputStream inputStream) {
        try {
            v a2 = a(new InputStreamReader(inputStream, "UTF-8"));
            q0.a(inputStream);
            return a2;
        } catch (IOException e2) {
            throw new l0(e2);
        } catch (Throwable th) {
            q0.a(inputStream);
            throw th;
        }
    }

    public v a(a aVar) {
        try {
            return a(aVar.c("UTF-8"));
        } catch (Exception e2) {
            throw new l0("Error parsing file: " + aVar, e2);
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:351:0x001a */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:348:0x001a */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:354:0x001a */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:350:0x036f */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:352:0x001a */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:364:0x00a2 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:362:0x00a4 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:361:0x00a2 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:356:0x0062 */
    /* JADX WARN: Type inference failed for: r6v7, types: [int] */
    /* JADX WARN: Type inference failed for: r20v2, types: [int] */
    /* JADX WARN: Type inference failed for: r11v12 */
    /* JADX WARN: Type inference failed for: r11v14 */
    /* JADX WARN: Type inference failed for: r13v36, types: [int] */
    /* JADX WARN: Type inference failed for: r13v37 */
    /* JADX WARN: Type inference failed for: r13v41, types: [int] */
    /* JADX WARN: Type inference failed for: r17v34, types: [int] */
    /* JADX WARN: Type inference failed for: r13v43 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.u.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.badlogic.gdx.utils.u.a(java.lang.String, com.badlogic.gdx.utils.v):void
      com.badlogic.gdx.utils.u.a(java.lang.String, java.lang.String):void
      com.badlogic.gdx.utils.u.a(java.lang.String, boolean):void */
    /* JADX WARNING: Can't wrap try/catch for region: R(9:192|193|(2:195|196)(1:197)|(4:199|200|(3:202|203|204)(3:206|207|(3:209|210|211)(4:212|213|214|(7:216|217|218|191|248|373|252)(3:219|(1:(6:221|222|(5:224|(2:228|(2:230|(1:232)))|234|400|236)|235|399|236)(2:398|237))|(6:239|240|241|248|373|252)(2:242|(4:244|248|373|252)))))|205)(1:245)|246|247|248|373|252) */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x023d, code lost:
        r4 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:249:0x0329, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:250:0x032a, code lost:
        r10 = r30;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:246:0x0320 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:317:0x0434 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x0350  */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x035b  */
    /* JADX WARNING: Removed duplicated region for block: B:260:0x0363  */
    /* JADX WARNING: Removed duplicated region for block: B:326:0x0454  */
    /* JADX WARNING: Removed duplicated region for block: B:334:0x04a4  */
    /* JADX WARNING: Removed duplicated region for block: B:377:0x0163 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e7 A[Catch:{ RuntimeException -> 0x0444 }] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.badlogic.gdx.utils.v a(char[] r29, int r30, int r31) {
        /*
            r28 = this;
            r1 = r28
            r2 = r29
            r3 = r31
            r0 = 4
            int[] r4 = new int[r0]
            com.badlogic.gdx.utils.a r5 = new com.badlogic.gdx.utils.a
            r6 = 8
            r5.<init>(r6)
            r10 = r30
            r8 = r4
            r4 = 0
            r6 = 0
            r11 = 1
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
        L_0x001a:
            java.lang.String r0 = "null"
            java.lang.String r7 = "false"
            java.lang.String r9 = "true"
            r30 = r12
            if (r4 == 0) goto L_0x003d
            r12 = 1
            if (r4 == r12) goto L_0x003b
            r12 = 2
            if (r4 == r12) goto L_0x0033
            r12 = 4
            if (r4 == r12) goto L_0x002f
            goto L_0x0442
        L_0x002f:
            r12 = r30
            goto L_0x036f
        L_0x0033:
            r12 = r30
            r17 = r6
            r23 = r8
            goto L_0x0359
        L_0x003b:
            r12 = 4
            goto L_0x004b
        L_0x003d:
            r12 = 4
            if (r10 != r3) goto L_0x0045
            r12 = r30
            r0 = 4
            r4 = 4
            goto L_0x001a
        L_0x0045:
            if (r11 != 0) goto L_0x004b
            r12 = r30
            goto L_0x035f
        L_0x004b:
            short[] r4 = com.badlogic.gdx.utils.u.f5612f     // Catch:{ RuntimeException -> 0x0444 }
            short r4 = r4[r11]     // Catch:{ RuntimeException -> 0x0444 }
            short[] r17 = com.badlogic.gdx.utils.u.f5616j     // Catch:{ RuntimeException -> 0x0444 }
            short r17 = r17[r11]     // Catch:{ RuntimeException -> 0x0444 }
            byte[] r20 = com.badlogic.gdx.utils.u.f5614h     // Catch:{ RuntimeException -> 0x0444 }
            byte r20 = r20[r11]     // Catch:{ RuntimeException -> 0x0444 }
            if (r20 <= 0) goto L_0x0094
            int r21 = r4 + r20
            int r22 = r21 + -1
            r12 = r22
            r22 = r13
            r13 = r4
        L_0x0062:
            if (r12 >= r13) goto L_0x0069
            int r17 = r17 + r20
            r4 = r21
            goto L_0x0096
        L_0x0069:
            int r23 = r12 - r13
            r19 = 1
            int r23 = r23 >> 1
            int r23 = r13 + r23
            r24 = r12
            char r12 = r2[r10]     // Catch:{ RuntimeException -> 0x0444 }
            char[] r25 = com.badlogic.gdx.utils.u.f5613g     // Catch:{ RuntimeException -> 0x0444 }
            r26 = r13
            char r13 = r25[r23]     // Catch:{ RuntimeException -> 0x0444 }
            if (r12 >= r13) goto L_0x0082
            int r12 = r23 + -1
            r13 = r26
            goto L_0x0062
        L_0x0082:
            char r12 = r2[r10]     // Catch:{ RuntimeException -> 0x0444 }
            char[] r13 = com.badlogic.gdx.utils.u.f5613g     // Catch:{ RuntimeException -> 0x0444 }
            char r13 = r13[r23]     // Catch:{ RuntimeException -> 0x0444 }
            if (r12 <= r13) goto L_0x008f
            int r13 = r23 + 1
            r12 = r24
            goto L_0x0062
        L_0x008f:
            int r23 = r23 - r4
            int r17 = r17 + r23
            goto L_0x00d9
        L_0x0094:
            r22 = r13
        L_0x0096:
            byte[] r12 = com.badlogic.gdx.utils.u.f5615i     // Catch:{ RuntimeException -> 0x0444 }
            byte r11 = r12[r11]     // Catch:{ RuntimeException -> 0x0444 }
            if (r11 <= 0) goto L_0x00d9
            int r12 = r11 << 1
            int r12 = r12 + r4
            r13 = 2
            int r12 = r12 - r13
            r13 = r4
        L_0x00a2:
            if (r12 >= r13) goto L_0x00a7
        L_0x00a4:
            int r17 = r17 + r11
            goto L_0x00d9
        L_0x00a7:
            int r20 = r12 - r13
            r19 = 1
            int r20 = r20 >> 1
            r20 = r20 & -2
            int r20 = r13 + r20
            r21 = r11
            char r11 = r2[r10]     // Catch:{ RuntimeException -> 0x0444 }
            char[] r23 = com.badlogic.gdx.utils.u.f5613g     // Catch:{ RuntimeException -> 0x0444 }
            r24 = r12
            char r12 = r23[r20]     // Catch:{ RuntimeException -> 0x0444 }
            if (r11 >= r12) goto L_0x00c2
            int r12 = r20 + -2
            r11 = r21
            goto L_0x00a2
        L_0x00c2:
            char r11 = r2[r10]     // Catch:{ RuntimeException -> 0x0444 }
            char[] r12 = com.badlogic.gdx.utils.u.f5613g     // Catch:{ RuntimeException -> 0x0444 }
            int r13 = r20 + 1
            char r12 = r12[r13]     // Catch:{ RuntimeException -> 0x0444 }
            if (r11 <= r12) goto L_0x00d3
            int r13 = r20 + 2
            r11 = r21
            r12 = r24
            goto L_0x00a2
        L_0x00d3:
            int r20 = r20 - r4
            r4 = 1
            int r11 = r20 >> 1
            goto L_0x00a4
        L_0x00d9:
            byte[] r4 = com.badlogic.gdx.utils.u.f5617k     // Catch:{ RuntimeException -> 0x0444 }
            byte r4 = r4[r17]     // Catch:{ RuntimeException -> 0x0444 }
            byte[] r11 = com.badlogic.gdx.utils.u.l     // Catch:{ RuntimeException -> 0x0444 }
            byte r11 = r11[r4]     // Catch:{ RuntimeException -> 0x0444 }
            byte[] r12 = com.badlogic.gdx.utils.u.m     // Catch:{ RuntimeException -> 0x0444 }
            byte r12 = r12[r4]     // Catch:{ RuntimeException -> 0x0444 }
            if (r12 == 0) goto L_0x0350
            byte[] r12 = com.badlogic.gdx.utils.u.m     // Catch:{ RuntimeException -> 0x0444 }
            byte r4 = r12[r4]     // Catch:{ RuntimeException -> 0x0444 }
            byte[] r12 = com.badlogic.gdx.utils.u.f5611e     // Catch:{ RuntimeException -> 0x0444 }
            int r13 = r4 + 1
            byte r4 = r12[r4]     // Catch:{ RuntimeException -> 0x0444 }
            r12 = r30
            r17 = r13
            r13 = r22
        L_0x00f7:
            int r20 = r4 + -1
            if (r4 <= 0) goto L_0x0348
            byte[] r4 = com.badlogic.gdx.utils.u.f5611e     // Catch:{ RuntimeException -> 0x0343 }
            int r21 = r17 + 1
            byte r4 = r4[r17]     // Catch:{ RuntimeException -> 0x0343 }
            r17 = r0
            r0 = 47
            switch(r4) {
                case 0: goto L_0x032e;
                case 1: goto L_0x026a;
                case 2: goto L_0x0241;
                case 3: goto L_0x0236;
                case 4: goto L_0x0210;
                case 5: goto L_0x0208;
                case 6: goto L_0x01bf;
                case 7: goto L_0x0132;
                case 8: goto L_0x0113;
                default: goto L_0x0108;
            }
        L_0x0108:
            r23 = r8
            r30 = r10
        L_0x010c:
            r0 = r17
            r17 = r6
            r6 = r11
            goto L_0x0338
        L_0x0113:
            int r10 = r10 + 1
            r4 = r10
            r0 = 0
        L_0x0117:
            char r12 = r2[r4]     // Catch:{ RuntimeException -> 0x0203 }
            r13 = 34
            if (r12 == r13) goto L_0x012a
            r13 = 92
            if (r12 == r13) goto L_0x0123
        L_0x0121:
            r12 = 1
            goto L_0x0127
        L_0x0123:
            int r4 = r4 + 1
            r0 = 1
            goto L_0x0121
        L_0x0127:
            int r4 = r4 + r12
            if (r4 != r3) goto L_0x0117
        L_0x012a:
            r13 = r0
            int r0 = r4 + -1
            r23 = r8
            r12 = r10
            r10 = r0
            goto L_0x010c
        L_0x0132:
            r4 = 13
            r12 = r10
            r13 = 0
            if (r14 == 0) goto L_0x016b
        L_0x0138:
            char r15 = r2[r12]     // Catch:{ RuntimeException -> 0x0167 }
            r23 = r13
            r13 = 10
            if (r15 == r13) goto L_0x0164
            if (r15 == r4) goto L_0x0164
            if (r15 == r0) goto L_0x014f
            r13 = 58
            if (r15 == r13) goto L_0x0164
            r13 = 92
            if (r15 == r13) goto L_0x014d
            goto L_0x015d
        L_0x014d:
            r13 = 1
            goto L_0x015f
        L_0x014f:
            int r13 = r12 + 1
            if (r13 != r3) goto L_0x0154
            goto L_0x015d
        L_0x0154:
            char r13 = r2[r13]     // Catch:{ RuntimeException -> 0x0167 }
            if (r13 == r0) goto L_0x0164
            r15 = 42
            if (r13 != r15) goto L_0x015d
            goto L_0x0164
        L_0x015d:
            r13 = r23
        L_0x015f:
            int r12 = r12 + 1
            if (r12 != r3) goto L_0x0138
            goto L_0x01ad
        L_0x0164:
            r13 = r23
            goto L_0x01ad
        L_0x0167:
            r0 = move-exception
            r10 = r12
            goto L_0x0445
        L_0x016b:
            char r15 = r2[r12]     // Catch:{ RuntimeException -> 0x0167 }
            r23 = r13
            r13 = 10
            if (r15 == r13) goto L_0x0164
            if (r15 == r4) goto L_0x0164
            r13 = 44
            if (r15 == r13) goto L_0x0164
            if (r15 == r0) goto L_0x018b
            r13 = 125(0x7d, float:1.75E-43)
            if (r15 == r13) goto L_0x0164
            r13 = 92
            if (r15 == r13) goto L_0x0188
            r4 = 93
            if (r15 == r4) goto L_0x0164
            goto L_0x019b
        L_0x0188:
            r23 = 1
            goto L_0x019b
        L_0x018b:
            r13 = 92
            int r4 = r12 + 1
            if (r4 != r3) goto L_0x0192
            goto L_0x019b
        L_0x0192:
            char r4 = r2[r4]     // Catch:{ RuntimeException -> 0x0167 }
            if (r4 == r0) goto L_0x0164
            r15 = 42
            if (r4 != r15) goto L_0x019b
            goto L_0x0164
        L_0x019b:
            int r12 = r12 + 1
            if (r12 != r3) goto L_0x01a0
            goto L_0x0164
        L_0x01a0:
            r13 = r23
            r4 = 13
            goto L_0x016b
        L_0x01a5:
            char r0 = r2[r12]     // Catch:{ RuntimeException -> 0x0167 }
            boolean r0 = java.lang.Character.isSpace(r0)     // Catch:{ RuntimeException -> 0x0167 }
            if (r0 == 0) goto L_0x01b0
        L_0x01ad:
            int r12 = r12 + -1
            goto L_0x01a5
        L_0x01b0:
            r23 = r8
            r0 = r17
            r15 = 1
            r17 = r6
            r6 = r11
            r27 = r12
            r12 = r10
            r10 = r27
            goto L_0x0338
        L_0x01bf:
            int r4 = r10 + 1
            char r10 = r2[r10]     // Catch:{ RuntimeException -> 0x0203 }
            if (r10 != r0) goto L_0x01d4
            r10 = r4
        L_0x01c6:
            if (r10 == r3) goto L_0x01d1
            char r0 = r2[r10]     // Catch:{ RuntimeException -> 0x0444 }
            r4 = 10
            if (r0 == r4) goto L_0x01d1
            int r10 = r10 + 1
            goto L_0x01c6
        L_0x01d1:
            int r10 = r10 + -1
            goto L_0x01f9
        L_0x01d4:
            r10 = r4
        L_0x01d5:
            int r4 = r10 + 1
            if (r4 >= r3) goto L_0x01ea
            char r0 = r2[r10]     // Catch:{ RuntimeException -> 0x01e5 }
            r23 = r10
            r10 = 42
            if (r0 != r10) goto L_0x01e2
            goto L_0x01ee
        L_0x01e2:
            r10 = 47
            goto L_0x01f4
        L_0x01e5:
            r0 = move-exception
            r23 = r10
            goto L_0x0445
        L_0x01ea:
            r23 = r10
            r10 = 42
        L_0x01ee:
            char r0 = r2[r4]     // Catch:{ RuntimeException -> 0x01fd }
            r10 = 47
            if (r0 == r10) goto L_0x01f8
        L_0x01f4:
            r10 = r4
            r0 = 47
            goto L_0x01d5
        L_0x01f8:
            r10 = r4
        L_0x01f9:
            r23 = r8
            goto L_0x010c
        L_0x01fd:
            r0 = move-exception
            r12 = r0
            r10 = r23
            goto L_0x0446
        L_0x0203:
            r0 = move-exception
            r12 = r0
            r10 = r4
            goto L_0x0446
        L_0x0208:
            r28.a()     // Catch:{ RuntimeException -> 0x0444 }
            int r6 = r6 + -1
            r11 = r8[r6]     // Catch:{ RuntimeException -> 0x0444 }
            goto L_0x023d
        L_0x0210:
            int r0 = r5.f5374b     // Catch:{ RuntimeException -> 0x0444 }
            if (r0 <= 0) goto L_0x021b
            java.lang.Object r0 = r5.pop()     // Catch:{ RuntimeException -> 0x0444 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ RuntimeException -> 0x0444 }
            goto L_0x021c
        L_0x021b:
            r0 = 0
        L_0x021c:
            r1.b(r0)     // Catch:{ RuntimeException -> 0x0444 }
            int r0 = r8.length     // Catch:{ RuntimeException -> 0x0444 }
            if (r6 != r0) goto L_0x022e
            int r0 = r8.length     // Catch:{ RuntimeException -> 0x0444 }
            r4 = 2
            int r0 = r0 * 2
            int[] r0 = new int[r0]     // Catch:{ RuntimeException -> 0x0444 }
            int r4 = r8.length     // Catch:{ RuntimeException -> 0x0444 }
            r7 = 0
            java.lang.System.arraycopy(r8, r7, r0, r7, r4)     // Catch:{ RuntimeException -> 0x0444 }
            r8 = r0
        L_0x022e:
            int r0 = r6 + 1
            r8[r6] = r11     // Catch:{ RuntimeException -> 0x0444 }
            r11 = 23
            r6 = r0
            goto L_0x023d
        L_0x0236:
            r28.a()     // Catch:{ RuntimeException -> 0x0444 }
            int r6 = r6 + -1
            r11 = r8[r6]     // Catch:{ RuntimeException -> 0x0444 }
        L_0x023d:
            r0 = 4
            r4 = 2
            goto L_0x001a
        L_0x0241:
            int r0 = r5.f5374b     // Catch:{ RuntimeException -> 0x0444 }
            if (r0 <= 0) goto L_0x024c
            java.lang.Object r0 = r5.pop()     // Catch:{ RuntimeException -> 0x0444 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ RuntimeException -> 0x0444 }
            goto L_0x024d
        L_0x024c:
            r0 = 0
        L_0x024d:
            r1.c(r0)     // Catch:{ RuntimeException -> 0x0444 }
            int r0 = r8.length     // Catch:{ RuntimeException -> 0x0444 }
            if (r6 != r0) goto L_0x0260
            int r0 = r8.length     // Catch:{ RuntimeException -> 0x0444 }
            r4 = 2
            int r0 = r0 * 2
            int[] r0 = new int[r0]     // Catch:{ RuntimeException -> 0x0444 }
            int r7 = r8.length     // Catch:{ RuntimeException -> 0x0444 }
            r9 = 0
            java.lang.System.arraycopy(r8, r9, r0, r9, r7)     // Catch:{ RuntimeException -> 0x0444 }
            r8 = r0
            goto L_0x0261
        L_0x0260:
            r4 = 2
        L_0x0261:
            int r0 = r6 + 1
            r8[r6] = r11     // Catch:{ RuntimeException -> 0x0444 }
            r6 = r0
            r0 = 4
            r11 = 5
            goto L_0x001a
        L_0x026a:
            r4 = 2
            java.lang.String r0 = new java.lang.String     // Catch:{ RuntimeException -> 0x0343 }
            int r4 = r10 - r12
            r0.<init>(r2, r12, r4)     // Catch:{ RuntimeException -> 0x0343 }
            if (r13 == 0) goto L_0x0278
            java.lang.String r0 = r1.d(r0)     // Catch:{ RuntimeException -> 0x0444 }
        L_0x0278:
            if (r14 == 0) goto L_0x0289
            r5.add(r0)     // Catch:{ RuntimeException -> 0x0444 }
            r23 = r8
            r30 = r10
            r15 = r17
            r14 = 0
        L_0x0284:
            r17 = r6
        L_0x0286:
            r6 = r11
            goto L_0x0323
        L_0x0289:
            int r4 = r5.f5374b     // Catch:{ RuntimeException -> 0x0343 }
            if (r4 <= 0) goto L_0x0294
            java.lang.Object r4 = r5.pop()     // Catch:{ RuntimeException -> 0x0444 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x0444 }
            goto L_0x0295
        L_0x0294:
            r4 = 0
        L_0x0295:
            if (r15 == 0) goto L_0x0317
            boolean r15 = r0.equals(r9)     // Catch:{ RuntimeException -> 0x0343 }
            if (r15 == 0) goto L_0x02a8
            r12 = 1
            r1.a(r4, r12)     // Catch:{ RuntimeException -> 0x0444 }
        L_0x02a1:
            r23 = r8
            r30 = r10
            r15 = r17
            goto L_0x0284
        L_0x02a8:
            boolean r15 = r0.equals(r7)     // Catch:{ RuntimeException -> 0x0343 }
            if (r15 == 0) goto L_0x02b3
            r12 = 0
            r1.a(r4, r12)     // Catch:{ RuntimeException -> 0x0444 }
            goto L_0x02a1
        L_0x02b3:
            r15 = r17
            boolean r17 = r0.equals(r15)     // Catch:{ RuntimeException -> 0x0343 }
            if (r17 == 0) goto L_0x02c6
            r17 = r6
            r6 = 0
            r1.a(r4, r6)     // Catch:{ RuntimeException -> 0x0444 }
            r23 = r8
            r30 = r10
            goto L_0x0286
        L_0x02c6:
            r17 = r6
            r6 = 0
            r22 = 1
        L_0x02cb:
            if (r12 >= r10) goto L_0x02f9
            r30 = r6
            char r6 = r2[r12]     // Catch:{ RuntimeException -> 0x0444 }
            r23 = r8
            r8 = 43
            if (r6 == r8) goto L_0x02f2
            r8 = 69
            if (r6 == r8) goto L_0x02ee
            r8 = 101(0x65, float:1.42E-43)
            if (r6 == r8) goto L_0x02ee
            r8 = 45
            if (r6 == r8) goto L_0x02f2
            r8 = 46
            if (r6 == r8) goto L_0x02ee
            switch(r6) {
                case 48: goto L_0x02f2;
                case 49: goto L_0x02f2;
                case 50: goto L_0x02f2;
                case 51: goto L_0x02f2;
                case 52: goto L_0x02f2;
                case 53: goto L_0x02f2;
                case 54: goto L_0x02f2;
                case 55: goto L_0x02f2;
                case 56: goto L_0x02f2;
                case 57: goto L_0x02f2;
                default: goto L_0x02ea;
            }
        L_0x02ea:
            r6 = 0
            r22 = 0
            goto L_0x02fd
        L_0x02ee:
            r6 = 1
            r22 = 0
            goto L_0x02f4
        L_0x02f2:
            r6 = r30
        L_0x02f4:
            int r12 = r12 + 1
            r8 = r23
            goto L_0x02cb
        L_0x02f9:
            r30 = r6
            r23 = r8
        L_0x02fd:
            if (r6 == 0) goto L_0x030a
            r30 = r10
            r6 = r11
            double r10 = java.lang.Double.parseDouble(r0)     // Catch:{ NumberFormatException -> 0x0320 }
            r1.a(r4, r10, r0)     // Catch:{ NumberFormatException -> 0x0320 }
            goto L_0x0323
        L_0x030a:
            r30 = r10
            r6 = r11
            if (r22 == 0) goto L_0x0320
            long r10 = java.lang.Long.parseLong(r0)     // Catch:{ NumberFormatException -> 0x0320 }
            r1.a(r4, r10, r0)     // Catch:{ NumberFormatException -> 0x0320 }
            goto L_0x0323
        L_0x0317:
            r23 = r8
            r30 = r10
            r15 = r17
            r17 = r6
            r6 = r11
        L_0x0320:
            r1.a(r4, r0)     // Catch:{ RuntimeException -> 0x0329 }
        L_0x0323:
            r10 = r30
            r12 = r10
            r0 = r15
            r15 = 0
            goto L_0x0338
        L_0x0329:
            r0 = move-exception
            r10 = r30
            goto L_0x0445
        L_0x032e:
            r23 = r8
            r30 = r10
            r0 = r17
            r17 = r6
            r6 = r11
            r14 = 1
        L_0x0338:
            r11 = r6
            r6 = r17
            r4 = r20
            r17 = r21
            r8 = r23
            goto L_0x00f7
        L_0x0343:
            r0 = move-exception
            r30 = r10
            goto L_0x0445
        L_0x0348:
            r17 = r6
            r23 = r8
            r30 = r10
            r6 = r11
            goto L_0x0359
        L_0x0350:
            r17 = r6
            r23 = r8
            r6 = r11
            r12 = r30
            r13 = r22
        L_0x0359:
            if (r11 != 0) goto L_0x0363
            r6 = r17
            r8 = r23
        L_0x035f:
            r0 = 4
            r4 = 5
            goto L_0x001a
        L_0x0363:
            int r10 = r10 + 1
            if (r10 == r3) goto L_0x036f
            r6 = r17
            r8 = r23
            r0 = 4
            r4 = 1
            goto L_0x001a
        L_0x036f:
            if (r10 != r3) goto L_0x0442
            byte[] r4 = com.badlogic.gdx.utils.u.n     // Catch:{ RuntimeException -> 0x0444 }
            byte r4 = r4[r11]     // Catch:{ RuntimeException -> 0x0444 }
            byte[] r6 = com.badlogic.gdx.utils.u.f5611e     // Catch:{ RuntimeException -> 0x0444 }
            int r8 = r4 + 1
            byte r4 = r6[r4]     // Catch:{ RuntimeException -> 0x0444 }
        L_0x037b:
            int r6 = r4 + -1
            if (r4 <= 0) goto L_0x0442
            byte[] r4 = com.badlogic.gdx.utils.u.f5611e     // Catch:{ RuntimeException -> 0x0444 }
            int r11 = r8 + 1
            byte r4 = r4[r8]     // Catch:{ RuntimeException -> 0x0444 }
            r8 = 1
            if (r4 == r8) goto L_0x0390
            r17 = r0
            r18 = r5
            r30 = r6
            goto L_0x0439
        L_0x0390:
            java.lang.String r4 = new java.lang.String     // Catch:{ RuntimeException -> 0x0444 }
            int r8 = r10 - r12
            r4.<init>(r2, r12, r8)     // Catch:{ RuntimeException -> 0x0444 }
            if (r13 == 0) goto L_0x039d
            java.lang.String r4 = r1.d(r4)     // Catch:{ RuntimeException -> 0x0444 }
        L_0x039d:
            if (r14 == 0) goto L_0x03ab
            r5.add(r4)     // Catch:{ RuntimeException -> 0x0444 }
            r17 = r0
            r18 = r5
            r30 = r6
            r14 = 0
            goto L_0x0437
        L_0x03ab:
            int r8 = r5.f5374b     // Catch:{ RuntimeException -> 0x0444 }
            if (r8 <= 0) goto L_0x03b6
            java.lang.Object r8 = r5.pop()     // Catch:{ RuntimeException -> 0x0444 }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ RuntimeException -> 0x0444 }
            goto L_0x03b7
        L_0x03b6:
            r8 = 0
        L_0x03b7:
            if (r15 == 0) goto L_0x042e
            boolean r15 = r4.equals(r9)     // Catch:{ RuntimeException -> 0x0444 }
            if (r15 == 0) goto L_0x03cb
            r15 = 1
            r1.a(r8, r15)     // Catch:{ RuntimeException -> 0x0444 }
        L_0x03c3:
            r17 = r0
            r18 = r5
            r30 = r6
            goto L_0x0437
        L_0x03cb:
            r15 = 1
            boolean r16 = r4.equals(r7)     // Catch:{ RuntimeException -> 0x0444 }
            if (r16 == 0) goto L_0x03d7
            r4 = 0
            r1.a(r8, r4)     // Catch:{ RuntimeException -> 0x0444 }
            goto L_0x03c3
        L_0x03d7:
            boolean r16 = r4.equals(r0)     // Catch:{ RuntimeException -> 0x0444 }
            if (r16 == 0) goto L_0x03e2
            r15 = 0
            r1.a(r8, r15)     // Catch:{ RuntimeException -> 0x0444 }
            goto L_0x03c3
        L_0x03e2:
            r15 = 0
            r16 = 1
        L_0x03e5:
            if (r12 >= r10) goto L_0x0412
            r17 = r0
            char r0 = r2[r12]     // Catch:{ RuntimeException -> 0x0444 }
            r18 = r5
            r5 = 43
            if (r0 == r5) goto L_0x040b
            r5 = 69
            if (r0 == r5) goto L_0x0408
            r5 = 101(0x65, float:1.42E-43)
            if (r0 == r5) goto L_0x0408
            r5 = 45
            if (r0 == r5) goto L_0x040b
            r5 = 46
            if (r0 == r5) goto L_0x0408
            switch(r0) {
                case 48: goto L_0x040b;
                case 49: goto L_0x040b;
                case 50: goto L_0x040b;
                case 51: goto L_0x040b;
                case 52: goto L_0x040b;
                case 53: goto L_0x040b;
                case 54: goto L_0x040b;
                case 55: goto L_0x040b;
                case 56: goto L_0x040b;
                case 57: goto L_0x040b;
                default: goto L_0x0404;
            }
        L_0x0404:
            r15 = 0
            r16 = 0
            goto L_0x0416
        L_0x0408:
            r15 = 1
            r16 = 0
        L_0x040b:
            int r12 = r12 + 1
            r0 = r17
            r5 = r18
            goto L_0x03e5
        L_0x0412:
            r17 = r0
            r18 = r5
        L_0x0416:
            if (r15 == 0) goto L_0x0422
            r30 = r6
            double r5 = java.lang.Double.parseDouble(r4)     // Catch:{ NumberFormatException -> 0x0434 }
            r1.a(r8, r5, r4)     // Catch:{ NumberFormatException -> 0x0434 }
            goto L_0x0437
        L_0x0422:
            r30 = r6
            if (r16 == 0) goto L_0x0434
            long r5 = java.lang.Long.parseLong(r4)     // Catch:{ NumberFormatException -> 0x0434 }
            r1.a(r8, r5, r4)     // Catch:{ NumberFormatException -> 0x0434 }
            goto L_0x0437
        L_0x042e:
            r17 = r0
            r18 = r5
            r30 = r6
        L_0x0434:
            r1.a(r8, r4)     // Catch:{ RuntimeException -> 0x0444 }
        L_0x0437:
            r12 = r10
            r15 = 0
        L_0x0439:
            r4 = r30
            r8 = r11
            r0 = r17
            r5 = r18
            goto L_0x037b
        L_0x0442:
            r12 = 0
            goto L_0x0446
        L_0x0444:
            r0 = move-exception
        L_0x0445:
            r12 = r0
        L_0x0446:
            com.badlogic.gdx.utils.v r0 = r1.f5620c
            r4 = 0
            r1.f5620c = r4
            r1.f5621d = r4
            com.badlogic.gdx.utils.a<com.badlogic.gdx.utils.v> r4 = r1.f5619b
            r4.clear()
            if (r10 >= r3) goto L_0x04a4
            r0 = 0
            r4 = 1
        L_0x0456:
            if (r0 >= r10) goto L_0x0463
            char r5 = r2[r0]
            r6 = 10
            if (r5 != r6) goto L_0x0460
            int r4 = r4 + 1
        L_0x0460:
            int r0 = r0 + 1
            goto L_0x0456
        L_0x0463:
            int r0 = r10 + -32
            r5 = 0
            int r0 = java.lang.Math.max(r5, r0)
            com.badlogic.gdx.utils.l0 r5 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Error parsing JSON on line "
            r6.append(r7)
            r6.append(r4)
            java.lang.String r4 = " near: "
            r6.append(r4)
            java.lang.String r4 = new java.lang.String
            int r7 = r10 - r0
            r4.<init>(r2, r0, r7)
            r6.append(r4)
            java.lang.String r0 = "*ERROR*"
            r6.append(r0)
            java.lang.String r0 = new java.lang.String
            r4 = 64
            int r3 = r3 - r10
            int r3 = java.lang.Math.min(r4, r3)
            r0.<init>(r2, r10, r3)
            r6.append(r0)
            java.lang.String r0 = r6.toString()
            r5.<init>(r0, r12)
            throw r5
        L_0x04a4:
            com.badlogic.gdx.utils.a<com.badlogic.gdx.utils.v> r3 = r1.f5618a
            int r4 = r3.f5374b
            if (r4 == 0) goto L_0x04cd
            java.lang.Object r0 = r3.peek()
            com.badlogic.gdx.utils.v r0 = (com.badlogic.gdx.utils.v) r0
            com.badlogic.gdx.utils.a<com.badlogic.gdx.utils.v> r2 = r1.f5618a
            r2.clear()
            if (r0 == 0) goto L_0x04c5
            boolean r0 = r0.t()
            if (r0 == 0) goto L_0x04c5
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.String r2 = "Error parsing JSON, unmatched brace."
            r0.<init>(r2)
            throw r0
        L_0x04c5:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.String r2 = "Error parsing JSON, unmatched bracket."
            r0.<init>(r2)
            throw r0
        L_0x04cd:
            if (r12 != 0) goto L_0x04d0
            return r0
        L_0x04d0:
            com.badlogic.gdx.utils.l0 r0 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Error parsing JSON: "
            r3.append(r4)
            java.lang.String r4 = new java.lang.String
            r4.<init>(r2)
            r3.append(r4)
            java.lang.String r2 = r3.toString()
            r0.<init>(r2, r12)
            goto L_0x04ed
        L_0x04ec:
            throw r0
        L_0x04ed:
            goto L_0x04ec
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.u.a(char[], int, int):com.badlogic.gdx.utils.v");
    }

    private void a(String str, v vVar) {
        vVar.m(str);
        v vVar2 = this.f5621d;
        if (vVar2 == null) {
            this.f5621d = vVar;
            this.f5620c = vVar;
        } else if (vVar2.n() || this.f5621d.t()) {
            v vVar3 = this.f5621d;
            vVar.f5630i = vVar3;
            if (vVar3.f5631j == 0) {
                vVar3.f5627f = vVar;
            } else {
                v pop = this.f5619b.pop();
                pop.f5628g = vVar;
                vVar.f5629h = pop;
            }
            this.f5619b.add(vVar);
            this.f5621d.f5631j++;
        } else {
            this.f5620c = this.f5621d;
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.f5620c = this.f5618a.pop();
        if (this.f5621d.f5631j > 0) {
            this.f5619b.pop();
        }
        a<v> aVar = this.f5618a;
        this.f5621d = aVar.f5374b > 0 ? aVar.peek() : null;
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2) {
        a(str, new v(str2));
    }

    /* access modifiers changed from: protected */
    public void a(String str, double d2, String str2) {
        a(str, new v(d2, str2));
    }

    /* access modifiers changed from: protected */
    public void a(String str, long j2, String str2) {
        a(str, new v(j2, str2));
    }

    /* access modifiers changed from: protected */
    public void a(String str, boolean z) {
        a(str, new v(z));
    }
}
