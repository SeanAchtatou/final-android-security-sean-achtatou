package com.badlogic.gdx.utils;

import com.google.android.gms.ads.AdRequest;

/* compiled from: SerializationException */
public class l0 extends RuntimeException {

    /* renamed from: a  reason: collision with root package name */
    private r0 f5526a;

    public l0(String str, Throwable th) {
        super(str, th);
    }

    public void a(String str) {
        if (str != null) {
            if (this.f5526a == null) {
                this.f5526a = new r0((int) AdRequest.MAX_CONTENT_URL_LENGTH);
            }
            this.f5526a.append(10);
            this.f5526a.a(str);
            return;
        }
        throw new IllegalArgumentException("info cannot be null.");
    }

    public String getMessage() {
        if (this.f5526a == null) {
            return super.getMessage();
        }
        r0 r0Var = new r0((int) AdRequest.MAX_CONTENT_URL_LENGTH);
        r0Var.a(super.getMessage());
        if (r0Var.length() > 0) {
            r0Var.append(10);
        }
        r0Var.a("Serialization trace:");
        r0Var.a(this.f5526a);
        return r0Var.toString();
    }

    public l0(String str) {
        super(str);
    }

    public l0(Throwable th) {
        super("", th);
    }
}
