package com.badlogic.gdx.utils;

import com.google.android.gms.games.Notifications;

/* compiled from: Bits */
public class e {

    /* renamed from: a  reason: collision with root package name */
    long[] f5482a = {0};

    private void e(int i2) {
        long[] jArr = this.f5482a;
        if (i2 >= jArr.length) {
            long[] jArr2 = new long[(i2 + 1)];
            System.arraycopy(jArr, 0, jArr2, 0, jArr.length);
            this.f5482a = jArr2;
        }
    }

    public void a(int i2) {
        int i3 = i2 >>> 6;
        long[] jArr = this.f5482a;
        if (i3 < jArr.length) {
            jArr[i3] = jArr[i3] & ((1 << (i2 & 63)) ^ -1);
        }
    }

    public boolean b(int i2) {
        int i3 = i2 >>> 6;
        long[] jArr = this.f5482a;
        if (i3 < jArr.length && (jArr[i3] & (1 << (i2 & 63))) != 0) {
            return true;
        }
        return false;
    }

    public int c() {
        long[] jArr = this.f5482a;
        for (int length = jArr.length - 1; length >= 0; length--) {
            long j2 = jArr[length];
            if (j2 != 0) {
                for (int i2 = 63; i2 >= 0; i2--) {
                    if (((1 << (i2 & 63)) & j2) != 0) {
                        return (length << 6) + i2 + 1;
                    }
                }
                continue;
            }
        }
        return 0;
    }

    public void d(int i2) {
        int i3 = i2 >>> 6;
        e(i3);
        long[] jArr = this.f5482a;
        jArr[i3] = jArr[i3] | (1 << (i2 & 63));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || e.class != obj.getClass()) {
            return false;
        }
        e eVar = (e) obj;
        long[] jArr = eVar.f5482a;
        int min = Math.min(this.f5482a.length, jArr.length);
        for (int i2 = 0; min > i2; i2++) {
            if (this.f5482a[i2] != jArr[i2]) {
                return false;
            }
        }
        if (this.f5482a.length == jArr.length || c() == eVar.c()) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int c2 = c() >>> 6;
        int i2 = 0;
        for (int i3 = 0; c2 >= i3; i3++) {
            int i4 = i2 * Notifications.NOTIFICATION_TYPES_ALL;
            long[] jArr = this.f5482a;
            i2 = i4 + ((int) (jArr[i3] ^ (jArr[i3] >>> 32)));
        }
        return i2;
    }

    public void a() {
        long[] jArr = this.f5482a;
        int length = jArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            jArr[i2] = 0;
        }
    }

    public boolean b() {
        for (long j2 : this.f5482a) {
            if (j2 != 0) {
                return false;
            }
        }
        return true;
    }

    public int c(int i2) {
        int i3;
        long[] jArr = this.f5482a;
        int i4 = i2 >>> 6;
        int length = jArr.length;
        if (i4 >= length) {
            return -1;
        }
        long j2 = jArr[i4];
        if (j2 != 0) {
            i3 = i2 & 63;
            while (true) {
                if (i3 >= 64) {
                    break;
                } else if (((1 << (i3 & 63)) & j2) != 0) {
                    break;
                } else {
                    i3++;
                }
            }
        }
        loop1:
        while (true) {
            i4++;
            if (i4 >= length) {
                return -1;
            }
            if (i4 != 0) {
                long j3 = jArr[i4];
                if (j3 != 0) {
                    int i5 = 0;
                    while (i3 < 64) {
                        if (((1 << (i3 & 63)) & j3) != 0) {
                            break loop1;
                        }
                        i5 = i3 + 1;
                    }
                    continue;
                } else {
                    continue;
                }
            }
        }
        return (i4 << 6) + i3;
    }

    public boolean a(e eVar) {
        long[] jArr = this.f5482a;
        long[] jArr2 = eVar.f5482a;
        int length = jArr2.length;
        int length2 = jArr.length;
        for (int i2 = length2; i2 < length; i2++) {
            if (jArr2[i2] != 0) {
                return false;
            }
        }
        for (int min = Math.min(length2, length) - 1; min >= 0; min--) {
            if ((jArr[min] & jArr2[min]) != jArr2[min]) {
                return false;
            }
        }
        return true;
    }

    public boolean b(e eVar) {
        long[] jArr = this.f5482a;
        long[] jArr2 = eVar.f5482a;
        for (int min = Math.min(jArr.length, jArr2.length) - 1; min >= 0; min--) {
            if ((jArr[min] & jArr2[min]) != 0) {
                return true;
            }
        }
        return false;
    }

    public void c(e eVar) {
        int min = Math.min(this.f5482a.length, eVar.f5482a.length);
        for (int i2 = 0; min > i2; i2++) {
            long[] jArr = this.f5482a;
            jArr[i2] = jArr[i2] | eVar.f5482a[i2];
        }
        long[] jArr2 = eVar.f5482a;
        if (min < jArr2.length) {
            e(jArr2.length);
            int length = eVar.f5482a.length;
            while (length > min) {
                this.f5482a[min] = eVar.f5482a[min];
                min++;
            }
        }
    }
}
