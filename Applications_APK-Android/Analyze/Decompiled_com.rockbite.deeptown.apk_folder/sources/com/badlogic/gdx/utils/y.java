package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.h;
import com.esotericsoftware.spine.Animation;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: LongMap */
public class y<V> implements Iterable<b<V>> {

    /* renamed from: a  reason: collision with root package name */
    public int f5694a;

    /* renamed from: b  reason: collision with root package name */
    long[] f5695b;

    /* renamed from: c  reason: collision with root package name */
    V[] f5696c;

    /* renamed from: d  reason: collision with root package name */
    int f5697d;

    /* renamed from: e  reason: collision with root package name */
    int f5698e;

    /* renamed from: f  reason: collision with root package name */
    V f5699f;

    /* renamed from: g  reason: collision with root package name */
    boolean f5700g;

    /* renamed from: h  reason: collision with root package name */
    private float f5701h;

    /* renamed from: i  reason: collision with root package name */
    private int f5702i;

    /* renamed from: j  reason: collision with root package name */
    private int f5703j;

    /* renamed from: k  reason: collision with root package name */
    private int f5704k;
    private int l;
    private int m;
    private a n;
    private a o;
    private d p;
    private d q;

    /* compiled from: LongMap */
    public static class a<V> extends c<V> implements Iterable<b<V>>, Iterator<b<V>> {

        /* renamed from: f  reason: collision with root package name */
        private b<V> f5705f = new b<>();

        public a(y yVar) {
            super(yVar);
        }

        public boolean hasNext() {
            if (this.f5712e) {
                return this.f5708a;
            }
            throw new o("#iterator() cannot be used nested.");
        }

        public Iterator<b<V>> iterator() {
            return this;
        }

        public void remove() {
            super.remove();
        }

        public b<V> next() {
            if (!this.f5708a) {
                throw new NoSuchElementException();
            } else if (this.f5712e) {
                y<V> yVar = this.f5709b;
                long[] jArr = yVar.f5695b;
                int i2 = this.f5710c;
                if (i2 == -1) {
                    b<V> bVar = this.f5705f;
                    bVar.f5706a = 0;
                    bVar.f5707b = yVar.f5699f;
                } else {
                    b<V> bVar2 = this.f5705f;
                    bVar2.f5706a = jArr[i2];
                    bVar2.f5707b = yVar.f5696c[i2];
                }
                this.f5711d = this.f5710c;
                a();
                return this.f5705f;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }
    }

    /* compiled from: LongMap */
    public static class b<V> {

        /* renamed from: a  reason: collision with root package name */
        public long f5706a;

        /* renamed from: b  reason: collision with root package name */
        public V f5707b;

        public String toString() {
            return this.f5706a + "=" + ((Object) this.f5707b);
        }
    }

    /* compiled from: LongMap */
    private static class c<V> {

        /* renamed from: a  reason: collision with root package name */
        public boolean f5708a;

        /* renamed from: b  reason: collision with root package name */
        final y<V> f5709b;

        /* renamed from: c  reason: collision with root package name */
        int f5710c;

        /* renamed from: d  reason: collision with root package name */
        int f5711d;

        /* renamed from: e  reason: collision with root package name */
        boolean f5712e = true;

        public c(y<V> yVar) {
            this.f5709b = yVar;
            b();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f5708a = false;
            y<V> yVar = this.f5709b;
            long[] jArr = yVar.f5695b;
            int i2 = yVar.f5697d + yVar.f5698e;
            do {
                int i3 = this.f5710c + 1;
                this.f5710c = i3;
                if (i3 >= i2) {
                    return;
                }
            } while (jArr[this.f5710c] == 0);
            this.f5708a = true;
        }

        public void b() {
            this.f5711d = -2;
            this.f5710c = -1;
            if (this.f5709b.f5700g) {
                this.f5708a = true;
            } else {
                a();
            }
        }

        public void remove() {
            if (this.f5711d == -1) {
                y<V> yVar = this.f5709b;
                if (yVar.f5700g) {
                    yVar.f5699f = null;
                    yVar.f5700g = false;
                    this.f5711d = -2;
                    y<V> yVar2 = this.f5709b;
                    yVar2.f5694a--;
                    return;
                }
            }
            int i2 = this.f5711d;
            if (i2 >= 0) {
                y<V> yVar3 = this.f5709b;
                if (i2 >= yVar3.f5697d) {
                    yVar3.c(i2);
                    this.f5710c = this.f5711d - 1;
                    a();
                } else {
                    yVar3.f5695b[i2] = 0;
                    yVar3.f5696c[i2] = null;
                }
                this.f5711d = -2;
                y<V> yVar22 = this.f5709b;
                yVar22.f5694a--;
                return;
            }
            throw new IllegalStateException("next must be called before remove.");
        }
    }

    /* compiled from: LongMap */
    public static class d<V> extends c<V> implements Iterable<V>, Iterator<V> {
        public d(y<V> yVar) {
            super(yVar);
        }

        public boolean hasNext() {
            if (this.f5712e) {
                return this.f5708a;
            }
            throw new o("#iterator() cannot be used nested.");
        }

        public Iterator<V> iterator() {
            return this;
        }

        public V next() {
            V v;
            if (!this.f5708a) {
                throw new NoSuchElementException();
            } else if (this.f5712e) {
                int i2 = this.f5710c;
                if (i2 == -1) {
                    v = this.f5709b.f5699f;
                } else {
                    v = this.f5709b.f5696c[i2];
                }
                this.f5711d = this.f5710c;
                a();
                return v;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }

        public void remove() {
            super.remove();
        }
    }

    public y() {
        this(51, 0.8f);
    }

    private void a(long j2, V v, int i2, long j3, int i3, long j4, int i4, long j5) {
        long[] jArr = this.f5695b;
        V[] vArr = this.f5696c;
        int i5 = this.f5703j;
        int i6 = this.m;
        long j6 = j2;
        V v2 = v;
        int i7 = i2;
        long j7 = j3;
        int i8 = i3;
        long j8 = j4;
        int i9 = i4;
        long j9 = j5;
        int i10 = 0;
        while (true) {
            long j10 = j9;
            int c2 = h.c(2);
            if (c2 == 0) {
                V v3 = vArr[i7];
                jArr[i7] = j6;
                vArr[i7] = v2;
                v2 = v3;
                j6 = j7;
            } else if (c2 != 1) {
                V v4 = vArr[i9];
                jArr[i9] = j6;
                vArr[i9] = v2;
                j6 = j10;
                v2 = v4;
            } else {
                V v5 = vArr[i8];
                jArr[i8] = j6;
                vArr[i8] = v2;
                v2 = v5;
                j6 = j8;
            }
            i7 = (int) (((long) i5) & j6);
            j7 = jArr[i7];
            if (j7 == 0) {
                jArr[i7] = j6;
                vArr[i7] = v2;
                int i11 = this.f5694a;
                this.f5694a = i11 + 1;
                if (i11 >= this.f5704k) {
                    d(this.f5697d << 1);
                    return;
                }
                return;
            }
            i8 = d(j6);
            long j11 = jArr[i8];
            if (j11 == 0) {
                jArr[i8] = j6;
                vArr[i8] = v2;
                int i12 = this.f5694a;
                this.f5694a = i12 + 1;
                if (i12 >= this.f5704k) {
                    d(this.f5697d << 1);
                    return;
                }
                return;
            }
            i9 = e(j6);
            long j12 = jArr[i9];
            if (j12 == 0) {
                jArr[i9] = j6;
                vArr[i9] = v2;
                int i13 = this.f5694a;
                this.f5694a = i13 + 1;
                if (i13 >= this.f5704k) {
                    d(this.f5697d << 1);
                    return;
                }
                return;
            }
            int i14 = i10 + 1;
            if (i14 == i6) {
                e(j6, v2);
                return;
            }
            i10 = i14;
            j8 = j11;
            j9 = j12;
        }
    }

    private V c(long j2, V v) {
        long[] jArr = this.f5695b;
        int i2 = this.f5697d;
        int i3 = this.f5698e + i2;
        while (i2 < i3) {
            if (jArr[i2] == j2) {
                return this.f5696c[i2];
            }
            i2++;
        }
        return v;
    }

    private void d(long j2, V v) {
        V v2 = v;
        if (j2 == 0) {
            this.f5699f = v2;
            this.f5700g = true;
            return;
        }
        int i2 = (int) (j2 & ((long) this.f5703j));
        long[] jArr = this.f5695b;
        long j3 = jArr[i2];
        if (j3 == 0) {
            jArr[i2] = j2;
            this.f5696c[i2] = v2;
            int i3 = this.f5694a;
            this.f5694a = i3 + 1;
            if (i3 >= this.f5704k) {
                d(this.f5697d << 1);
                return;
            }
            return;
        }
        int d2 = d(j2);
        long[] jArr2 = this.f5695b;
        long j4 = jArr2[d2];
        if (j4 == 0) {
            jArr2[d2] = j2;
            this.f5696c[d2] = v2;
            int i4 = this.f5694a;
            this.f5694a = i4 + 1;
            if (i4 >= this.f5704k) {
                d(this.f5697d << 1);
                return;
            }
            return;
        }
        int e2 = e(j2);
        long[] jArr3 = this.f5695b;
        long j5 = jArr3[e2];
        if (j5 == 0) {
            jArr3[e2] = j2;
            this.f5696c[e2] = v2;
            int i5 = this.f5694a;
            this.f5694a = i5 + 1;
            if (i5 >= this.f5704k) {
                d(this.f5697d << 1);
                return;
            }
            return;
        }
        a(j2, v, i2, j3, d2, j4, e2, j5);
    }

    private void e(long j2, V v) {
        int i2 = this.f5698e;
        if (i2 == this.l) {
            d(this.f5697d << 1);
            d(j2, v);
            return;
        }
        int i3 = this.f5697d + i2;
        this.f5695b[i3] = j2;
        this.f5696c[i3] = v;
        this.f5698e = i2 + 1;
        this.f5694a++;
    }

    public V b(long j2, V v) {
        V v2 = v;
        if (j2 == 0) {
            V v3 = this.f5699f;
            this.f5699f = v2;
            if (!this.f5700g) {
                this.f5700g = true;
                this.f5694a++;
            }
            return v3;
        }
        long[] jArr = this.f5695b;
        int i2 = (int) (j2 & ((long) this.f5703j));
        long j3 = jArr[i2];
        if (j3 == j2) {
            V[] vArr = this.f5696c;
            V v4 = vArr[i2];
            vArr[i2] = v2;
            return v4;
        }
        int d2 = d(j2);
        long j4 = jArr[d2];
        if (j4 == j2) {
            V[] vArr2 = this.f5696c;
            V v5 = vArr2[d2];
            vArr2[d2] = v2;
            return v5;
        }
        int e2 = e(j2);
        long j5 = jArr[e2];
        if (j5 == j2) {
            V[] vArr3 = this.f5696c;
            V v6 = vArr3[e2];
            vArr3[e2] = v2;
            return v6;
        }
        int i3 = this.f5697d;
        int i4 = this.f5698e + i3;
        while (i3 < i4) {
            if (jArr[i3] == j2) {
                V[] vArr4 = this.f5696c;
                V v7 = vArr4[i3];
                vArr4[i3] = v2;
                return v7;
            }
            i3++;
        }
        if (j3 == 0) {
            jArr[i2] = j2;
            this.f5696c[i2] = v2;
            int i5 = this.f5694a;
            this.f5694a = i5 + 1;
            if (i5 >= this.f5704k) {
                d(this.f5697d << 1);
            }
            return null;
        } else if (j4 == 0) {
            jArr[d2] = j2;
            this.f5696c[d2] = v2;
            int i6 = this.f5694a;
            this.f5694a = i6 + 1;
            if (i6 >= this.f5704k) {
                d(this.f5697d << 1);
            }
            return null;
        } else if (j5 == 0) {
            jArr[e2] = j2;
            this.f5696c[e2] = v2;
            int i7 = this.f5694a;
            this.f5694a = i7 + 1;
            if (i7 >= this.f5704k) {
                d(this.f5697d << 1);
            }
            return null;
        } else {
            a(j2, v, i2, j3, d2, j4, e2, j5);
            return null;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean equals(java.lang.Object r12) {
        /*
            r11 = this;
            r0 = 1
            if (r12 != r11) goto L_0x0004
            return r0
        L_0x0004:
            boolean r1 = r12 instanceof com.badlogic.gdx.utils.y
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            com.badlogic.gdx.utils.y r12 = (com.badlogic.gdx.utils.y) r12
            int r1 = r12.f5694a
            int r3 = r11.f5694a
            if (r1 == r3) goto L_0x0013
            return r2
        L_0x0013:
            boolean r1 = r12.f5700g
            boolean r3 = r11.f5700g
            if (r1 == r3) goto L_0x001a
            return r2
        L_0x001a:
            if (r3 == 0) goto L_0x002e
            V r1 = r12.f5699f
            if (r1 != 0) goto L_0x0025
            V r1 = r11.f5699f
            if (r1 == 0) goto L_0x002e
            return r2
        L_0x0025:
            V r3 = r11.f5699f
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x002e
            return r2
        L_0x002e:
            long[] r1 = r11.f5695b
            V[] r3 = r11.f5696c
            int r4 = r11.f5697d
            int r5 = r11.f5698e
            int r4 = r4 + r5
            r5 = 0
        L_0x0038:
            if (r5 >= r4) goto L_0x005d
            r6 = r1[r5]
            r8 = 0
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x005a
            r8 = r3[r5]
            if (r8 != 0) goto L_0x004f
            java.lang.Object r8 = com.badlogic.gdx.utils.c0.r
            java.lang.Object r6 = r12.a(r6, r8)
            if (r6 == 0) goto L_0x005a
            return r2
        L_0x004f:
            java.lang.Object r6 = r12.a(r6)
            boolean r6 = r8.equals(r6)
            if (r6 != 0) goto L_0x005a
            return r2
        L_0x005a:
            int r5 = r5 + 1
            goto L_0x0038
        L_0x005d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.y.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        V v;
        int hashCode = (!this.f5700g || (v = this.f5699f) == null) ? 0 : v.hashCode() + 0;
        long[] jArr = this.f5695b;
        V[] vArr = this.f5696c;
        int i2 = this.f5697d + this.f5698e;
        for (int i3 = 0; i3 < i2; i3++) {
            long j2 = jArr[i3];
            if (j2 != 0) {
                hashCode += ((int) (j2 ^ (j2 >>> 32))) * 31;
                V v2 = vArr[i3];
                if (v2 != null) {
                    hashCode += v2.hashCode();
                }
            }
        }
        return hashCode;
    }

    public Iterator<b<V>> iterator() {
        return a();
    }

    public String toString() {
        int i2;
        if (this.f5694a == 0) {
            return "[]";
        }
        r0 r0Var = new r0(32);
        r0Var.append('[');
        long[] jArr = this.f5695b;
        V[] vArr = this.f5696c;
        int length = jArr.length;
        while (true) {
            i2 = length - 1;
            if (length <= 0) {
                break;
            }
            long j2 = jArr[i2];
            if (j2 != 0) {
                r0Var.a(j2);
                r0Var.append('=');
                r0Var.a((Object) vArr[i2]);
                break;
            }
            length = i2;
        }
        while (true) {
            int i3 = i2 - 1;
            if (i2 > 0) {
                long j3 = jArr[i3];
                if (j3 != 0) {
                    r0Var.a(", ");
                    r0Var.a(j3);
                    r0Var.append('=');
                    r0Var.a((Object) vArr[i3]);
                }
                i2 = i3;
            } else {
                r0Var.append(']');
                return r0Var.toString();
            }
        }
    }

    public y(int i2) {
        this(i2, 0.8f);
    }

    public y(int i2, float f2) {
        if (i2 >= 0) {
            int b2 = h.b((int) Math.ceil((double) (((float) i2) / f2)));
            if (b2 <= 1073741824) {
                this.f5697d = b2;
                if (f2 > Animation.CurveTimeline.LINEAR) {
                    this.f5701h = f2;
                    int i3 = this.f5697d;
                    this.f5704k = (int) (((float) i3) * f2);
                    this.f5703j = i3 - 1;
                    this.f5702i = 63 - Long.numberOfTrailingZeros((long) i3);
                    this.l = Math.max(3, ((int) Math.ceil(Math.log((double) this.f5697d))) * 2);
                    this.m = Math.max(Math.min(this.f5697d, 8), ((int) Math.sqrt((double) this.f5697d)) / 8);
                    this.f5695b = new long[(this.f5697d + this.l)];
                    this.f5696c = new Object[this.f5695b.length];
                    return;
                }
                throw new IllegalArgumentException("loadFactor must be > 0: " + f2);
            }
            throw new IllegalArgumentException("initialCapacity is too large: " + b2);
        }
        throw new IllegalArgumentException("initialCapacity must be >= 0: " + i2);
    }

    /* access modifiers changed from: package-private */
    public V c(long j2) {
        long[] jArr = this.f5695b;
        int i2 = this.f5697d;
        int i3 = this.f5698e + i2;
        while (i2 < i3) {
            if (jArr[i2] == j2) {
                V v = this.f5696c[i2];
                c(i2);
                this.f5694a--;
                return v;
            }
            i2++;
        }
        return null;
    }

    private int e(long j2) {
        long j3 = j2 * -825114047;
        return (int) ((j3 ^ (j3 >>> this.f5702i)) & ((long) this.f5703j));
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        this.f5698e--;
        int i3 = this.f5697d + this.f5698e;
        if (i2 < i3) {
            long[] jArr = this.f5695b;
            jArr[i2] = jArr[i3];
            V[] vArr = this.f5696c;
            vArr[i2] = vArr[i3];
            vArr[i3] = null;
            return;
        }
        this.f5696c[i2] = null;
    }

    private void d(int i2) {
        int i3 = this.f5697d + this.f5698e;
        this.f5697d = i2;
        this.f5704k = (int) (((float) i2) * this.f5701h);
        this.f5703j = i2 - 1;
        this.f5702i = 63 - Long.numberOfTrailingZeros((long) i2);
        double d2 = (double) i2;
        this.l = Math.max(3, ((int) Math.ceil(Math.log(d2))) * 2);
        this.m = Math.max(Math.min(i2, 8), ((int) Math.sqrt(d2)) / 8);
        long[] jArr = this.f5695b;
        V[] vArr = this.f5696c;
        int i4 = this.l;
        this.f5695b = new long[(i2 + i4)];
        this.f5696c = new Object[(i2 + i4)];
        int i5 = this.f5694a;
        this.f5694a = this.f5700g ? 1 : 0;
        this.f5698e = 0;
        if (i5 > 0) {
            for (int i6 = 0; i6 < i3; i6++) {
                long j2 = jArr[i6];
                if (j2 != 0) {
                    d(j2, vArr[i6]);
                }
            }
        }
    }

    public V a(long j2) {
        if (j2 != 0) {
            int i2 = (int) (((long) this.f5703j) & j2);
            if (this.f5695b[i2] != j2) {
                i2 = d(j2);
                if (this.f5695b[i2] != j2) {
                    i2 = e(j2);
                    if (this.f5695b[i2] != j2) {
                        return c(j2, null);
                    }
                }
            }
            return this.f5696c[i2];
        } else if (!this.f5700g) {
            return null;
        } else {
            return this.f5699f;
        }
    }

    public V b(long j2) {
        if (j2 != 0) {
            int i2 = (int) (((long) this.f5703j) & j2);
            long[] jArr = this.f5695b;
            if (jArr[i2] == j2) {
                jArr[i2] = 0;
                V[] vArr = this.f5696c;
                V v = vArr[i2];
                vArr[i2] = null;
                this.f5694a--;
                return v;
            }
            int d2 = d(j2);
            long[] jArr2 = this.f5695b;
            if (jArr2[d2] == j2) {
                jArr2[d2] = 0;
                V[] vArr2 = this.f5696c;
                V v2 = vArr2[d2];
                vArr2[d2] = null;
                this.f5694a--;
                return v2;
            }
            int e2 = e(j2);
            long[] jArr3 = this.f5695b;
            if (jArr3[e2] != j2) {
                return c(j2);
            }
            jArr3[e2] = 0;
            V[] vArr3 = this.f5696c;
            V v3 = vArr3[e2];
            vArr3[e2] = null;
            this.f5694a--;
            return v3;
        } else if (!this.f5700g) {
            return null;
        } else {
            V v4 = this.f5699f;
            this.f5699f = null;
            this.f5700g = false;
            this.f5694a--;
            return v4;
        }
    }

    private int d(long j2) {
        long j3 = j2 * -1262997959;
        return (int) ((j3 ^ (j3 >>> this.f5702i)) & ((long) this.f5703j));
    }

    public V a(long j2, V v) {
        if (j2 != 0) {
            int i2 = (int) (((long) this.f5703j) & j2);
            if (this.f5695b[i2] != j2) {
                i2 = d(j2);
                if (this.f5695b[i2] != j2) {
                    i2 = e(j2);
                    if (this.f5695b[i2] != j2) {
                        return c(j2, v);
                    }
                }
            }
            return this.f5696c[i2];
        } else if (!this.f5700g) {
            return v;
        } else {
            return this.f5699f;
        }
    }

    public a<V> a() {
        if (h.f5490a) {
            return new a<>(this);
        }
        if (this.n == null) {
            this.n = new a(this);
            this.o = new a(this);
        }
        a aVar = this.n;
        if (!aVar.f5712e) {
            aVar.b();
            a<V> aVar2 = this.n;
            aVar2.f5712e = true;
            this.o.f5712e = false;
            return aVar2;
        }
        this.o.b();
        a<V> aVar3 = this.o;
        aVar3.f5712e = true;
        this.n.f5712e = false;
        return aVar3;
    }

    public d<V> b() {
        if (h.f5490a) {
            return new d<>(this);
        }
        if (this.p == null) {
            this.p = new d(this);
            this.q = new d(this);
        }
        d dVar = this.p;
        if (!dVar.f5712e) {
            dVar.b();
            d<V> dVar2 = this.p;
            dVar2.f5712e = true;
            this.q.f5712e = false;
            return dVar2;
        }
        this.q.b();
        d<V> dVar3 = this.q;
        dVar3.f5712e = true;
        this.p.f5712e = false;
        return dVar3;
    }
}
