package com.badlogic.gdx.utils.z0;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/* compiled from: ClassReflection */
public final class b {
    public static Class a(String str) throws f {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e2) {
            throw new f("Class not found: " + str, e2);
        }
    }

    public static c b(Class cls, Class... clsArr) throws f {
        try {
            return new c(cls.getDeclaredConstructor(clsArr));
        } catch (SecurityException e2) {
            throw new f("Security violation while getting constructor for class: " + cls.getName(), e2);
        } catch (NoSuchMethodException e3) {
            throw new f("Constructor not found for class: " + cls.getName(), e3);
        }
    }

    public static String c(Class cls) {
        return cls.getSimpleName();
    }

    public static boolean d(Class cls) {
        return cls.isMemberClass();
    }

    public static boolean e(Class cls) {
        return Modifier.isStatic(cls.getModifiers());
    }

    public static <T> T f(Class<T> cls) throws f {
        try {
            return cls.newInstance();
        } catch (InstantiationException e2) {
            throw new f("Could not instantiate instance of class: " + cls.getName(), e2);
        } catch (IllegalAccessException e3) {
            throw new f("Could not instantiate instance of class: " + cls.getName(), e3);
        }
    }

    public static boolean a(Class cls, Object obj) {
        return cls.isInstance(obj);
    }

    public static boolean a(Class cls, Class cls2) {
        return cls.isAssignableFrom(cls2);
    }

    public static e[] b(Class cls) {
        Method[] methods = cls.getMethods();
        e[] eVarArr = new e[methods.length];
        int length = methods.length;
        for (int i2 = 0; i2 < length; i2++) {
            eVarArr[i2] = new e(methods[i2]);
        }
        return eVarArr;
    }

    public static c a(Class cls, Class... clsArr) throws f {
        try {
            return new c(cls.getConstructor(clsArr));
        } catch (SecurityException e2) {
            throw new f("Security violation occurred while getting constructor for class: '" + cls.getName() + "'.", e2);
        } catch (NoSuchMethodException e3) {
            throw new f("Constructor not found for class: " + cls.getName(), e3);
        }
    }

    public static d[] a(Class cls) {
        Field[] declaredFields = cls.getDeclaredFields();
        d[] dVarArr = new d[declaredFields.length];
        int length = declaredFields.length;
        for (int i2 = 0; i2 < length; i2++) {
            dVarArr[i2] = new d(declaredFields[i2]);
        }
        return dVarArr;
    }
}
