package com.badlogic.gdx.utils;

import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.regex.Pattern;

/* compiled from: JsonWriter */
public class w extends Writer {

    /* renamed from: a  reason: collision with root package name */
    final Writer f5659a;

    /* renamed from: b  reason: collision with root package name */
    private final a<b> f5660b = new a<>();

    /* renamed from: c  reason: collision with root package name */
    private b f5661c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f5662d;

    /* renamed from: e  reason: collision with root package name */
    private c f5663e = c.json;

    /* renamed from: f  reason: collision with root package name */
    private boolean f5664f = false;

    /* compiled from: JsonWriter */
    static /* synthetic */ class a {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f5665a = new int[c.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.badlogic.gdx.utils.w$c[] r0 = com.badlogic.gdx.utils.w.c.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.badlogic.gdx.utils.w.a.f5665a = r0
                int[] r0 = com.badlogic.gdx.utils.w.a.f5665a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.badlogic.gdx.utils.w$c r1 = com.badlogic.gdx.utils.w.c.minimal     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.badlogic.gdx.utils.w.a.f5665a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.badlogic.gdx.utils.w$c r1 = com.badlogic.gdx.utils.w.c.javascript     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.w.a.<clinit>():void");
        }
    }

    /* compiled from: JsonWriter */
    private class b {

        /* renamed from: a  reason: collision with root package name */
        final boolean f5666a;

        /* renamed from: b  reason: collision with root package name */
        boolean f5667b;

        b(boolean z) throws IOException {
            this.f5666a = z;
            w.this.f5659a.write(z ? 91 : 123);
        }

        /* access modifiers changed from: package-private */
        public void a() throws IOException {
            w.this.f5659a.write(this.f5666a ? 93 : 125);
        }
    }

    public w(Writer writer) {
        this.f5659a = writer;
    }

    private void R() throws IOException {
        b bVar = this.f5661c;
        if (bVar != null) {
            if (bVar.f5666a) {
                if (!bVar.f5667b) {
                    bVar.f5667b = true;
                } else {
                    this.f5659a.write(44);
                }
            } else if (this.f5662d) {
                this.f5662d = false;
            } else {
                throw new IllegalStateException("Name must be set.");
            }
        }
    }

    public w P() throws IOException {
        R();
        a<b> aVar = this.f5660b;
        b bVar = new b(false);
        this.f5661c = bVar;
        aVar.add(bVar);
        return this;
    }

    public w Q() throws IOException {
        if (!this.f5662d) {
            this.f5660b.pop().a();
            a<b> aVar = this.f5660b;
            this.f5661c = aVar.f5374b == 0 ? null : aVar.peek();
            return this;
        }
        throw new IllegalStateException("Expected an object, array, or value since a name was set.");
    }

    public void a(c cVar) {
        this.f5663e = cVar;
    }

    public void close() throws IOException {
        while (this.f5660b.f5374b > 0) {
            Q();
        }
        this.f5659a.close();
    }

    public w d() throws IOException {
        R();
        a<b> aVar = this.f5660b;
        b bVar = new b(true);
        this.f5661c = bVar;
        aVar.add(bVar);
        return this;
    }

    public void flush() throws IOException {
        this.f5659a.flush();
    }

    public void write(char[] cArr, int i2, int i3) throws IOException {
        this.f5659a.write(cArr, i2, i3);
    }

    public void a(boolean z) {
        this.f5664f = z;
    }

    public w a(String str) throws IOException {
        b bVar = this.f5661c;
        if (bVar == null || bVar.f5666a) {
            throw new IllegalStateException("Current item must be an object.");
        }
        if (!bVar.f5667b) {
            bVar.f5667b = true;
        } else {
            this.f5659a.write(44);
        }
        this.f5659a.write(this.f5663e.a(str));
        this.f5659a.write(58);
        this.f5662d = true;
        return this;
    }

    /* compiled from: JsonWriter */
    public enum c {
        json,
        javascript,
        minimal;
        

        /* renamed from: d  reason: collision with root package name */
        private static Pattern f5672d = Pattern.compile("^[a-zA-Z_$][a-zA-Z_$0-9]*$");

        /* renamed from: e  reason: collision with root package name */
        private static Pattern f5673e = Pattern.compile("^[^\":,}/ ][^:]*$");

        /* renamed from: f  reason: collision with root package name */
        private static Pattern f5674f = Pattern.compile("^[^\":,{\\[\\]/ ][^}\\],]*$");

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.badlogic.gdx.utils.r0.a(char, java.lang.String):com.badlogic.gdx.utils.r0
         arg types: [int, java.lang.String]
         candidates:
          com.badlogic.gdx.utils.r0.a(java.lang.String, int):int
          com.badlogic.gdx.utils.r0.a(int, int):com.badlogic.gdx.utils.r0
          com.badlogic.gdx.utils.r0.a(long, int):com.badlogic.gdx.utils.r0
          com.badlogic.gdx.utils.r0.a(java.lang.String, java.lang.String):com.badlogic.gdx.utils.r0
          com.badlogic.gdx.utils.r0.a(int, java.lang.String):void
          com.badlogic.gdx.utils.r0.a(char, java.lang.String):com.badlogic.gdx.utils.r0 */
        public String a(Object obj) {
            int length;
            if (obj == null) {
                return "null";
            }
            String obj2 = obj.toString();
            if ((obj instanceof Number) || (obj instanceof Boolean)) {
                return obj2;
            }
            r0 r0Var = new r0(obj2);
            r0Var.a('\\', "\\\\");
            r0Var.a(13, "\\r");
            r0Var.a(10, "\\n");
            r0Var.a(9, "\\t");
            if (this == minimal && !obj2.equals("true") && !obj2.equals("false") && !obj2.equals("null") && !obj2.contains("//") && !obj2.contains("/*") && (length = r0Var.length()) > 0 && r0Var.charAt(length - 1) != ' ' && f5674f.matcher(r0Var).matches()) {
                return r0Var.toString();
            }
            StringBuilder sb = new StringBuilder();
            sb.append('\"');
            r0Var.a('\"', "\\\"");
            sb.append(r0Var.toString());
            sb.append('\"');
            return sb.toString();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.badlogic.gdx.utils.r0.a(char, java.lang.String):com.badlogic.gdx.utils.r0
         arg types: [int, java.lang.String]
         candidates:
          com.badlogic.gdx.utils.r0.a(java.lang.String, int):int
          com.badlogic.gdx.utils.r0.a(int, int):com.badlogic.gdx.utils.r0
          com.badlogic.gdx.utils.r0.a(long, int):com.badlogic.gdx.utils.r0
          com.badlogic.gdx.utils.r0.a(java.lang.String, java.lang.String):com.badlogic.gdx.utils.r0
          com.badlogic.gdx.utils.r0.a(int, java.lang.String):void
          com.badlogic.gdx.utils.r0.a(char, java.lang.String):com.badlogic.gdx.utils.r0 */
        /* JADX WARNING: Code restructure failed: missing block: B:3:0x002d, code lost:
            if (r1 != 2) goto L_0x0062;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String a(java.lang.String r4) {
            /*
                r3 = this;
                com.badlogic.gdx.utils.r0 r0 = new com.badlogic.gdx.utils.r0
                r0.<init>(r4)
                r1 = 92
                java.lang.String r2 = "\\\\"
                r0.a(r1, r2)
                r1 = 13
                java.lang.String r2 = "\\r"
                r0.a(r1, r2)
                r1 = 10
                java.lang.String r2 = "\\n"
                r0.a(r1, r2)
                r1 = 9
                java.lang.String r2 = "\\t"
                r0.a(r1, r2)
                int[] r1 = com.badlogic.gdx.utils.w.a.f5665a
                int r2 = r3.ordinal()
                r1 = r1[r2]
                r2 = 1
                if (r1 == r2) goto L_0x0030
                r4 = 2
                if (r1 == r4) goto L_0x0051
                goto L_0x0062
            L_0x0030:
                java.lang.String r1 = "//"
                boolean r1 = r4.contains(r1)
                if (r1 != 0) goto L_0x0051
                java.lang.String r1 = "/*"
                boolean r4 = r4.contains(r1)
                if (r4 != 0) goto L_0x0051
                java.util.regex.Pattern r4 = com.badlogic.gdx.utils.w.c.f5673e
                java.util.regex.Matcher r4 = r4.matcher(r0)
                boolean r4 = r4.matches()
                if (r4 == 0) goto L_0x0051
                java.lang.String r4 = r0.toString()
                return r4
            L_0x0051:
                java.util.regex.Pattern r4 = com.badlogic.gdx.utils.w.c.f5672d
                java.util.regex.Matcher r4 = r4.matcher(r0)
                boolean r4 = r4.matches()
                if (r4 == 0) goto L_0x0062
                java.lang.String r4 = r0.toString()
                return r4
            L_0x0062:
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                r1 = 34
                r4.append(r1)
                java.lang.String r2 = "\\\""
                r0.a(r1, r2)
                java.lang.String r0 = r0.toString()
                r4.append(r0)
                r4.append(r1)
                java.lang.String r4 = r4.toString()
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.w.c.a(java.lang.String):java.lang.String");
        }
    }

    public w a(Object obj) throws IOException {
        if (this.f5664f && ((obj instanceof Long) || (obj instanceof Double) || (obj instanceof BigDecimal) || (obj instanceof BigInteger))) {
            obj = obj.toString();
        } else if (obj instanceof Number) {
            Number number = (Number) obj;
            long longValue = number.longValue();
            if (number.doubleValue() == ((double) longValue)) {
                obj = Long.valueOf(longValue);
            }
        }
        R();
        this.f5659a.write(this.f5663e.a(obj));
        return this;
    }

    public w a(String str, Object obj) throws IOException {
        a(str);
        a(obj);
        return this;
    }
}
