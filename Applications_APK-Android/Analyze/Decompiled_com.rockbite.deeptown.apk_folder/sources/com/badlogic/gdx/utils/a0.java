package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.h;
import com.esotericsoftware.spine.Animation;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: ObjectFloatMap */
public class a0<K> implements Iterable<b<K>> {

    /* renamed from: a  reason: collision with root package name */
    public int f5385a;

    /* renamed from: b  reason: collision with root package name */
    K[] f5386b;

    /* renamed from: c  reason: collision with root package name */
    float[] f5387c;

    /* renamed from: d  reason: collision with root package name */
    int f5388d;

    /* renamed from: e  reason: collision with root package name */
    int f5389e;

    /* renamed from: f  reason: collision with root package name */
    private float f5390f;

    /* renamed from: g  reason: collision with root package name */
    private int f5391g;

    /* renamed from: h  reason: collision with root package name */
    private int f5392h;

    /* renamed from: i  reason: collision with root package name */
    private int f5393i;

    /* renamed from: j  reason: collision with root package name */
    private int f5394j;

    /* renamed from: k  reason: collision with root package name */
    private int f5395k;
    private a l;
    private a m;

    /* compiled from: ObjectFloatMap */
    public static class a<K> extends c<K> implements Iterable<b<K>>, Iterator<b<K>> {

        /* renamed from: f  reason: collision with root package name */
        private b<K> f5396f = new b<>();

        public a(a0<K> a0Var) {
            super(a0Var);
        }

        public boolean hasNext() {
            if (this.f5403e) {
                return this.f5399a;
            }
            throw new o("#iterator() cannot be used nested.");
        }

        public a<K> iterator() {
            return this;
        }

        public void remove() {
            super.remove();
        }

        public b<K> next() {
            if (!this.f5399a) {
                throw new NoSuchElementException();
            } else if (this.f5403e) {
                a0<K> a0Var = this.f5400b;
                K[] kArr = a0Var.f5386b;
                b<K> bVar = this.f5396f;
                int i2 = this.f5401c;
                bVar.f5397a = kArr[i2];
                bVar.f5398b = a0Var.f5387c[i2];
                this.f5402d = i2;
                a();
                return this.f5396f;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }
    }

    /* compiled from: ObjectFloatMap */
    public static class b<K> {

        /* renamed from: a  reason: collision with root package name */
        public K f5397a;

        /* renamed from: b  reason: collision with root package name */
        public float f5398b;

        public String toString() {
            return ((Object) this.f5397a) + "=" + this.f5398b;
        }
    }

    /* compiled from: ObjectFloatMap */
    private static class c<K> {

        /* renamed from: a  reason: collision with root package name */
        public boolean f5399a;

        /* renamed from: b  reason: collision with root package name */
        final a0<K> f5400b;

        /* renamed from: c  reason: collision with root package name */
        int f5401c;

        /* renamed from: d  reason: collision with root package name */
        int f5402d;

        /* renamed from: e  reason: collision with root package name */
        boolean f5403e = true;

        public c(a0<K> a0Var) {
            this.f5400b = a0Var;
            b();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f5399a = false;
            a0<K> a0Var = this.f5400b;
            K[] kArr = a0Var.f5386b;
            int i2 = a0Var.f5388d + a0Var.f5389e;
            do {
                int i3 = this.f5401c + 1;
                this.f5401c = i3;
                if (i3 >= i2) {
                    return;
                }
            } while (kArr[this.f5401c] == null);
            this.f5399a = true;
        }

        public void b() {
            this.f5402d = -1;
            this.f5401c = -1;
            a();
        }

        public void remove() {
            int i2 = this.f5402d;
            if (i2 >= 0) {
                a0<K> a0Var = this.f5400b;
                if (i2 >= a0Var.f5388d) {
                    a0Var.c(i2);
                    this.f5401c = this.f5402d - 1;
                    a();
                } else {
                    a0Var.f5386b[i2] = null;
                }
                this.f5402d = -1;
                a0<K> a0Var2 = this.f5400b;
                a0Var2.f5385a--;
                return;
            }
            throw new IllegalStateException("next must be called before remove.");
        }
    }

    public a0() {
        this(51, 0.8f);
    }

    private void a(K k2, float f2, int i2, K k3, int i3, K k4, int i4, K k5) {
        K[] kArr = this.f5386b;
        float[] fArr = this.f5387c;
        int i5 = this.f5392h;
        int i6 = this.f5395k;
        K k6 = k2;
        float f3 = f2;
        int i7 = i2;
        K k7 = k3;
        int i8 = i3;
        K k8 = k4;
        int i9 = i4;
        K k9 = k5;
        int i10 = 0;
        do {
            int c2 = h.c(2);
            if (c2 == 0) {
                float f4 = fArr[i7];
                kArr[i7] = k6;
                fArr[i7] = f3;
                f3 = f4;
                k6 = k7;
            } else if (c2 != 1) {
                float f5 = fArr[i9];
                kArr[i9] = k6;
                fArr[i9] = f3;
                k6 = k9;
                f3 = f5;
            } else {
                float f6 = fArr[i8];
                kArr[i8] = k6;
                fArr[i8] = f3;
                f3 = f6;
                k6 = k8;
            }
            int hashCode = k6.hashCode();
            i7 = hashCode & i5;
            k7 = kArr[i7];
            if (k7 == null) {
                kArr[i7] = k6;
                fArr[i7] = f3;
                int i11 = this.f5385a;
                this.f5385a = i11 + 1;
                if (i11 >= this.f5393i) {
                    f(this.f5388d << 1);
                    return;
                }
                return;
            }
            i8 = d(hashCode);
            k8 = kArr[i8];
            if (k8 == null) {
                kArr[i8] = k6;
                fArr[i8] = f3;
                int i12 = this.f5385a;
                this.f5385a = i12 + 1;
                if (i12 >= this.f5393i) {
                    f(this.f5388d << 1);
                    return;
                }
                return;
            }
            i9 = e(hashCode);
            k9 = kArr[i9];
            if (k9 == null) {
                kArr[i9] = k6;
                fArr[i9] = f3;
                int i13 = this.f5385a;
                this.f5385a = i13 + 1;
                if (i13 >= this.f5393i) {
                    f(this.f5388d << 1);
                    return;
                }
                return;
            }
            i10++;
        } while (i10 != i6);
        e(k6, f3);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private float c(K r5, float r6) {
        /*
            r4 = this;
            K[] r0 = r4.f5386b
            int r1 = r4.f5388d
            int r2 = r4.f5389e
            int r2 = r2 + r1
        L_0x0007:
            if (r1 >= r2) goto L_0x0019
            r3 = r0[r1]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0016
            float[] r5 = r4.f5387c
            r5 = r5[r1]
            return r5
        L_0x0016:
            int r1 = r1 + 1
            goto L_0x0007
        L_0x0019:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.a0.c(java.lang.Object, float):float");
    }

    private void d(K k2, float f2) {
        int hashCode = k2.hashCode();
        int i2 = hashCode & this.f5392h;
        K[] kArr = this.f5386b;
        K k3 = kArr[i2];
        if (k3 == null) {
            kArr[i2] = k2;
            this.f5387c[i2] = f2;
            int i3 = this.f5385a;
            this.f5385a = i3 + 1;
            if (i3 >= this.f5393i) {
                f(this.f5388d << 1);
                return;
            }
            return;
        }
        int d2 = d(hashCode);
        K[] kArr2 = this.f5386b;
        K k4 = kArr2[d2];
        if (k4 == null) {
            kArr2[d2] = k2;
            this.f5387c[d2] = f2;
            int i4 = this.f5385a;
            this.f5385a = i4 + 1;
            if (i4 >= this.f5393i) {
                f(this.f5388d << 1);
                return;
            }
            return;
        }
        int e2 = e(hashCode);
        K[] kArr3 = this.f5386b;
        K k5 = kArr3[e2];
        if (k5 == null) {
            kArr3[e2] = k2;
            this.f5387c[e2] = f2;
            int i5 = this.f5385a;
            this.f5385a = i5 + 1;
            if (i5 >= this.f5393i) {
                f(this.f5388d << 1);
                return;
            }
            return;
        }
        a(k2, f2, i2, k3, d2, k4, e2, k5);
    }

    private void e(K k2, float f2) {
        int i2 = this.f5389e;
        if (i2 == this.f5394j) {
            f(this.f5388d << 1);
            d(k2, f2);
            return;
        }
        int i3 = this.f5388d + i2;
        this.f5386b[i3] = k2;
        this.f5387c[i3] = f2;
        this.f5389e = i2 + 1;
        this.f5385a++;
    }

    private void f(int i2) {
        int i3 = this.f5388d + this.f5389e;
        this.f5388d = i2;
        this.f5393i = (int) (((float) i2) * this.f5390f);
        this.f5392h = i2 - 1;
        this.f5391g = 31 - Integer.numberOfTrailingZeros(i2);
        double d2 = (double) i2;
        this.f5394j = Math.max(3, ((int) Math.ceil(Math.log(d2))) * 2);
        this.f5395k = Math.max(Math.min(i2, 8), ((int) Math.sqrt(d2)) / 8);
        K[] kArr = this.f5386b;
        float[] fArr = this.f5387c;
        int i4 = this.f5394j;
        this.f5386b = new Object[(i2 + i4)];
        this.f5387c = new float[(i2 + i4)];
        int i5 = this.f5385a;
        this.f5385a = 0;
        this.f5389e = 0;
        if (i5 > 0) {
            for (int i6 = 0; i6 < i3; i6++) {
                K k2 = kArr[i6];
                if (k2 != null) {
                    d(k2, fArr[i6]);
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void b(K r13, float r14) {
        /*
            r12 = this;
            if (r13 == 0) goto L_0x00a7
            K[] r0 = r12.f5386b
            int r1 = r13.hashCode()
            int r2 = r12.f5392h
            r6 = r1 & r2
            r7 = r0[r6]
            boolean r2 = r13.equals(r7)
            if (r2 == 0) goto L_0x0019
            float[] r13 = r12.f5387c
            r13[r6] = r14
            return
        L_0x0019:
            int r8 = r12.d(r1)
            r9 = r0[r8]
            boolean r2 = r13.equals(r9)
            if (r2 == 0) goto L_0x002a
            float[] r13 = r12.f5387c
            r13[r8] = r14
            return
        L_0x002a:
            int r10 = r12.e(r1)
            r11 = r0[r10]
            boolean r1 = r13.equals(r11)
            if (r1 == 0) goto L_0x003b
            float[] r13 = r12.f5387c
            r13[r10] = r14
            return
        L_0x003b:
            int r1 = r12.f5388d
            int r2 = r12.f5389e
            int r2 = r2 + r1
        L_0x0040:
            if (r1 >= r2) goto L_0x0052
            r3 = r0[r1]
            boolean r3 = r13.equals(r3)
            if (r3 == 0) goto L_0x004f
            float[] r13 = r12.f5387c
            r13[r1] = r14
            return
        L_0x004f:
            int r1 = r1 + 1
            goto L_0x0040
        L_0x0052:
            if (r7 != 0) goto L_0x006c
            r0[r6] = r13
            float[] r13 = r12.f5387c
            r13[r6] = r14
            int r13 = r12.f5385a
            int r14 = r13 + 1
            r12.f5385a = r14
            int r14 = r12.f5393i
            if (r13 < r14) goto L_0x006b
            int r13 = r12.f5388d
            int r13 = r13 << 1
            r12.f(r13)
        L_0x006b:
            return
        L_0x006c:
            if (r9 != 0) goto L_0x0086
            r0[r8] = r13
            float[] r13 = r12.f5387c
            r13[r8] = r14
            int r13 = r12.f5385a
            int r14 = r13 + 1
            r12.f5385a = r14
            int r14 = r12.f5393i
            if (r13 < r14) goto L_0x0085
            int r13 = r12.f5388d
            int r13 = r13 << 1
            r12.f(r13)
        L_0x0085:
            return
        L_0x0086:
            if (r11 != 0) goto L_0x00a0
            r0[r10] = r13
            float[] r13 = r12.f5387c
            r13[r10] = r14
            int r13 = r12.f5385a
            int r14 = r13 + 1
            r12.f5385a = r14
            int r14 = r12.f5393i
            if (r13 < r14) goto L_0x009f
            int r13 = r12.f5388d
            int r13 = r13 << 1
            r12.f(r13)
        L_0x009f:
            return
        L_0x00a0:
            r3 = r12
            r4 = r13
            r5 = r14
            r3.a(r4, r5, r6, r7, r8, r9, r10, r11)
            return
        L_0x00a7:
            java.lang.IllegalArgumentException r13 = new java.lang.IllegalArgumentException
            java.lang.String r14 = "key cannot be null."
            r13.<init>(r14)
            goto L_0x00b0
        L_0x00af:
            throw r13
        L_0x00b0:
            goto L_0x00af
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.a0.b(java.lang.Object, float):void");
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof a0)) {
            return false;
        }
        a0 a0Var = (a0) obj;
        if (a0Var.f5385a != this.f5385a) {
            return false;
        }
        K[] kArr = this.f5386b;
        float[] fArr = this.f5387c;
        int i2 = this.f5388d + this.f5389e;
        for (int i3 = 0; i3 < i2; i3++) {
            K k2 = kArr[i3];
            if (k2 != null) {
                float a2 = a0Var.a(k2, Animation.CurveTimeline.LINEAR);
                if ((a2 == Animation.CurveTimeline.LINEAR && !a0Var.a(k2)) || a2 != fArr[i3]) {
                    return false;
                }
            }
        }
        return true;
    }

    public int hashCode() {
        K[] kArr = this.f5386b;
        float[] fArr = this.f5387c;
        int i2 = this.f5388d + this.f5389e;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            K k2 = kArr[i4];
            if (k2 != null) {
                i3 = i3 + (k2.hashCode() * 31) + Float.floatToIntBits(fArr[i4]);
            }
        }
        return i3;
    }

    public String toString() {
        int i2;
        if (this.f5385a == 0) {
            return "{}";
        }
        r0 r0Var = new r0(32);
        r0Var.append('{');
        K[] kArr = this.f5386b;
        float[] fArr = this.f5387c;
        int length = kArr.length;
        while (true) {
            i2 = length - 1;
            if (length > 0) {
                K k2 = kArr[i2];
                if (k2 != null) {
                    r0Var.a((Object) k2);
                    r0Var.append('=');
                    r0Var.a(fArr[i2]);
                    break;
                }
                length = i2;
            } else {
                break;
            }
        }
        while (true) {
            int i3 = i2 - 1;
            if (i2 > 0) {
                K k3 = kArr[i3];
                if (k3 != null) {
                    r0Var.a(", ");
                    r0Var.a((Object) k3);
                    r0Var.append('=');
                    r0Var.a(fArr[i3]);
                }
                i2 = i3;
            } else {
                r0Var.append('}');
                return r0Var.toString();
            }
        }
    }

    public a0(int i2, float f2) {
        if (i2 >= 0) {
            int b2 = h.b((int) Math.ceil((double) (((float) i2) / f2)));
            if (b2 <= 1073741824) {
                this.f5388d = b2;
                if (f2 > Animation.CurveTimeline.LINEAR) {
                    this.f5390f = f2;
                    int i3 = this.f5388d;
                    this.f5393i = (int) (((float) i3) * f2);
                    this.f5392h = i3 - 1;
                    this.f5391g = 31 - Integer.numberOfTrailingZeros(i3);
                    this.f5394j = Math.max(3, ((int) Math.ceil(Math.log((double) this.f5388d))) * 2);
                    this.f5395k = Math.max(Math.min(this.f5388d, 8), ((int) Math.sqrt((double) this.f5388d)) / 8);
                    this.f5386b = new Object[(this.f5388d + this.f5394j)];
                    this.f5387c = new float[this.f5386b.length];
                    return;
                }
                throw new IllegalArgumentException("loadFactor must be > 0: " + f2);
            }
            throw new IllegalArgumentException("initialCapacity is too large: " + b2);
        }
        throw new IllegalArgumentException("initialCapacity must be >= 0: " + i2);
    }

    public a<K> iterator() {
        return a();
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        this.f5389e--;
        int i3 = this.f5388d + this.f5389e;
        if (i2 < i3) {
            K[] kArr = this.f5386b;
            kArr[i2] = kArr[i3];
            float[] fArr = this.f5387c;
            fArr[i2] = fArr[i3];
            kArr[i3] = null;
        }
    }

    private int e(int i2) {
        int i3 = i2 * -825114047;
        return (i3 ^ (i3 >>> this.f5391g)) & this.f5392h;
    }

    private int d(int i2) {
        int i3 = i2 * -1262997959;
        return (i3 ^ (i3 >>> this.f5391g)) & this.f5392h;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private boolean b(K r5) {
        /*
            r4 = this;
            K[] r0 = r4.f5386b
            int r1 = r4.f5388d
            int r2 = r4.f5389e
            int r2 = r2 + r1
        L_0x0007:
            if (r1 >= r2) goto L_0x0016
            r3 = r0[r1]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0013
            r5 = 1
            return r5
        L_0x0013:
            int r1 = r1 + 1
            goto L_0x0007
        L_0x0016:
            r5 = 0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.a0.b(java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public float a(K r4, float r5) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            int r1 = r3.f5392h
            r1 = r1 & r0
            K[] r2 = r3.f5386b
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0032
            int r1 = r3.d(r0)
            K[] r2 = r3.f5386b
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0032
            int r1 = r3.e(r0)
            K[] r0 = r3.f5386b
            r0 = r0[r1]
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x0032
            float r4 = r3.c(r4, r5)
            return r4
        L_0x0032:
            float[] r4 = r3.f5387c
            r4 = r4[r1]
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.a0.a(java.lang.Object, float):float");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean a(K r4) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            int r1 = r3.f5392h
            r1 = r1 & r0
            K[] r2 = r3.f5386b
            r1 = r2[r1]
            boolean r1 = r4.equals(r1)
            if (r1 != 0) goto L_0x0032
            int r1 = r3.d(r0)
            K[] r2 = r3.f5386b
            r1 = r2[r1]
            boolean r1 = r4.equals(r1)
            if (r1 != 0) goto L_0x0032
            int r0 = r3.e(r0)
            K[] r1 = r3.f5386b
            r0 = r1[r0]
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x0032
            boolean r4 = r3.b(r4)
            return r4
        L_0x0032:
            r4 = 1
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.a0.a(java.lang.Object):boolean");
    }

    public a<K> a() {
        if (h.f5490a) {
            return new a<>(this);
        }
        if (this.l == null) {
            this.l = new a(this);
            this.m = new a(this);
        }
        a aVar = this.l;
        if (!aVar.f5403e) {
            aVar.b();
            a<K> aVar2 = this.l;
            aVar2.f5403e = true;
            this.m.f5403e = false;
            return aVar2;
        }
        this.m.b();
        a<K> aVar3 = this.m;
        aVar3.f5403e = true;
        this.l.f5403e = false;
        return aVar3;
    }
}
