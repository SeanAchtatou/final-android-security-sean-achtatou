package com.badlogic.gdx.utils;

/* compiled from: PropertiesUtils */
public final class h0 {
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x012f, code lost:
        if (r11 != 3) goto L_0x0132;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(com.badlogic.gdx.utils.c0<java.lang.String, java.lang.String> r16, java.io.Reader r17) throws java.io.IOException {
        /*
            r0 = r16
            r1 = r17
            if (r0 == 0) goto L_0x0171
            if (r1 == 0) goto L_0x0169
            r2 = 40
            char[] r2 = new char[r2]
            java.io.BufferedReader r3 = new java.io.BufferedReader
            r3.<init>(r1)
            r4 = 2
            r6 = 4
            r7 = 1
            r8 = -1
            r9 = 0
            r10 = r2
            r2 = 0
            r11 = 0
            r12 = 0
            r13 = 0
        L_0x001b:
            r14 = -1
            r15 = 1
        L_0x001d:
            int r5 = r3.read()
            if (r5 != r8) goto L_0x005b
            if (r11 != r4) goto L_0x0030
            if (r12 <= r6) goto L_0x0028
            goto L_0x0030
        L_0x0028:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Invalid Unicode sequence: expected format \\uxxxx"
            r0.<init>(r1)
            throw r0
        L_0x0030:
            if (r14 != r8) goto L_0x0035
            if (r2 <= 0) goto L_0x0035
            r14 = r2
        L_0x0035:
            if (r14 < 0) goto L_0x005a
            java.lang.String r1 = new java.lang.String
            r1.<init>(r10, r9, r2)
            java.lang.String r2 = r1.substring(r9, r14)
            java.lang.String r1 = r1.substring(r14)
            if (r11 != r7) goto L_0x0057
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r1)
            java.lang.String r1 = "\u0000"
            r3.append(r1)
            java.lang.String r1 = r3.toString()
        L_0x0057:
            r0.b(r2, r1)
        L_0x005a:
            return
        L_0x005b:
            char r5 = (char) r5
            int r1 = r10.length
            if (r2 != r1) goto L_0x0068
            int r1 = r10.length
            int r1 = r1 * 2
            char[] r1 = new char[r1]
            java.lang.System.arraycopy(r10, r9, r1, r9, r2)
            r10 = r1
        L_0x0068:
            r1 = 10
            if (r11 != r4) goto L_0x0094
            r4 = 16
            int r4 = java.lang.Character.digit(r5, r4)
            if (r4 < 0) goto L_0x007d
            int r13 = r13 << 4
            int r13 = r13 + r4
            int r12 = r12 + 1
            if (r12 >= r6) goto L_0x007f
            r4 = 2
            goto L_0x001d
        L_0x007d:
            if (r12 <= r6) goto L_0x008c
        L_0x007f:
            int r4 = r2 + 1
            char r11 = (char) r13
            r10[r2] = r11
            r2 = r4
            if (r5 == r1) goto L_0x008a
            r4 = 2
            goto L_0x00fb
        L_0x008a:
            r11 = 0
            goto L_0x0094
        L_0x008c:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Invalid Unicode sequence: illegal character"
            r0.<init>(r1)
            throw r0
        L_0x0094:
            r4 = 13
            if (r11 != r7) goto L_0x00d5
            if (r5 == r1) goto L_0x00d1
            if (r5 == r4) goto L_0x00cd
            r11 = 98
            if (r5 == r11) goto L_0x00c7
            r11 = 102(0x66, float:1.43E-43)
            if (r5 == r11) goto L_0x00c4
            r11 = 110(0x6e, float:1.54E-43)
            if (r5 == r11) goto L_0x00c1
            r1 = 114(0x72, float:1.6E-43)
            if (r5 == r1) goto L_0x00be
            r1 = 116(0x74, float:1.63E-43)
            if (r5 == r1) goto L_0x00bb
            r1 = 117(0x75, float:1.64E-43)
            if (r5 == r1) goto L_0x00b5
            goto L_0x00c9
        L_0x00b5:
            r4 = 2
            r11 = 2
            r12 = 0
            r13 = 0
            goto L_0x001d
        L_0x00bb:
            r5 = 9
            goto L_0x00c9
        L_0x00be:
            r5 = 13
            goto L_0x00c9
        L_0x00c1:
            r5 = 10
            goto L_0x00c9
        L_0x00c4:
            r5 = 12
            goto L_0x00c9
        L_0x00c7:
            r5 = 8
        L_0x00c9:
            r1 = 5
        L_0x00ca:
            r11 = 0
            goto L_0x0132
        L_0x00cd:
            r4 = 2
            r11 = 3
            goto L_0x001d
        L_0x00d1:
            r4 = 2
        L_0x00d2:
            r11 = 5
            goto L_0x001d
        L_0x00d5:
            if (r5 == r1) goto L_0x0143
            if (r5 == r4) goto L_0x0140
            r7 = 33
            if (r5 == r7) goto L_0x00fe
            r7 = 35
            if (r5 == r7) goto L_0x00fe
            r1 = 58
            if (r5 == r1) goto L_0x00f6
            r1 = 61
            if (r5 == r1) goto L_0x00f6
            r1 = 92
            if (r5 == r1) goto L_0x00ee
            goto L_0x010d
        L_0x00ee:
            if (r11 != r6) goto L_0x00f1
            r14 = r2
        L_0x00f1:
            r4 = 2
            r7 = 1
            r11 = 1
            goto L_0x001d
        L_0x00f6:
            if (r14 != r8) goto L_0x010d
            r14 = r2
            r4 = 2
            r7 = 1
        L_0x00fb:
            r11 = 0
            goto L_0x001d
        L_0x00fe:
            if (r15 == 0) goto L_0x010d
        L_0x0100:
            int r5 = r3.read()
            if (r5 != r8) goto L_0x0107
            goto L_0x010c
        L_0x0107:
            char r5 = (char) r5
            if (r5 == r4) goto L_0x0127
            if (r5 != r1) goto L_0x0100
        L_0x010c:
            goto L_0x0127
        L_0x010d:
            boolean r1 = java.lang.Character.isSpace(r5)
            if (r1 == 0) goto L_0x012b
            r1 = 3
            if (r11 != r1) goto L_0x0117
            r11 = 5
        L_0x0117:
            if (r2 == 0) goto L_0x0126
            if (r2 == r14) goto L_0x0126
            r1 = 5
            if (r11 != r1) goto L_0x011f
            goto L_0x0127
        L_0x011f:
            if (r14 != r8) goto L_0x012c
            r4 = 2
            r7 = 1
            r11 = 4
            goto L_0x001d
        L_0x0126:
            r1 = 5
        L_0x0127:
            r4 = 2
            r7 = 1
            goto L_0x001d
        L_0x012b:
            r1 = 5
        L_0x012c:
            if (r11 == r1) goto L_0x00ca
            r4 = 3
            if (r11 != r4) goto L_0x0132
            goto L_0x00ca
        L_0x0132:
            if (r11 != r6) goto L_0x0136
            r14 = r2
            r11 = 0
        L_0x0136:
            int r4 = r2 + 1
            r10[r2] = r5
            r2 = r4
            r4 = 2
            r7 = 1
            r15 = 0
            goto L_0x001d
        L_0x0140:
            r1 = 5
            r4 = 3
            goto L_0x014a
        L_0x0143:
            r1 = 5
            r4 = 3
            if (r11 != r4) goto L_0x014a
            r4 = 2
            r7 = 1
            goto L_0x00d2
        L_0x014a:
            if (r2 > 0) goto L_0x0150
            if (r2 != 0) goto L_0x0163
            if (r14 != 0) goto L_0x0163
        L_0x0150:
            if (r14 != r8) goto L_0x0153
            r14 = r2
        L_0x0153:
            java.lang.String r5 = new java.lang.String
            r5.<init>(r10, r9, r2)
            java.lang.String r2 = r5.substring(r9, r14)
            java.lang.String r5 = r5.substring(r14)
            r0.b(r2, r5)
        L_0x0163:
            r2 = 0
            r4 = 2
            r7 = 1
            r11 = 0
            goto L_0x001b
        L_0x0169:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            java.lang.String r1 = "Reader cannot be null"
            r0.<init>(r1)
            throw r0
        L_0x0171:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            java.lang.String r1 = "ObjectMap cannot be null"
            r0.<init>(r1)
            goto L_0x017a
        L_0x0179:
            throw r0
        L_0x017a:
            goto L_0x0179
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.h0.a(com.badlogic.gdx.utils.c0, java.io.Reader):void");
    }
}
