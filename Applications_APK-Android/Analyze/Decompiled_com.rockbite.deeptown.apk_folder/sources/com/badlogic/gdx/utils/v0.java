package com.badlogic.gdx.utils;

import com.esotericsoftware.spine.Animation;
import e.d.b.e;
import e.d.b.g;
import e.d.b.m;

/* compiled from: Timer */
public class v0 {

    /* renamed from: b  reason: collision with root package name */
    static final Object f5647b = new Object();

    /* renamed from: c  reason: collision with root package name */
    static b f5648c;

    /* renamed from: a  reason: collision with root package name */
    final a<a> f5649a = new a<>(false, 8);

    /* compiled from: Timer */
    public static abstract class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final e.d.b.a f5650a = g.f6800a;

        /* renamed from: b  reason: collision with root package name */
        long f5651b;

        /* renamed from: c  reason: collision with root package name */
        long f5652c;

        /* renamed from: d  reason: collision with root package name */
        int f5653d;

        /* renamed from: e  reason: collision with root package name */
        volatile v0 f5654e;

        public a() {
            if (this.f5650a == null) {
                throw new IllegalStateException("Gdx.app not available.");
            }
        }

        public void a() {
            v0 v0Var = this.f5654e;
            if (v0Var != null) {
                synchronized (v0Var) {
                    synchronized (this) {
                        this.f5651b = 0;
                        this.f5654e = null;
                        v0Var.f5649a.d(this, true);
                    }
                }
                return;
            }
            synchronized (this) {
                this.f5651b = 0;
                this.f5654e = null;
            }
        }

        public boolean b() {
            return this.f5654e != null;
        }
    }

    /* compiled from: Timer */
    static class b implements Runnable, m {

        /* renamed from: a  reason: collision with root package name */
        final e f5655a = g.f6804e;

        /* renamed from: b  reason: collision with root package name */
        final a<v0> f5656b = new a<>(1);

        /* renamed from: c  reason: collision with root package name */
        v0 f5657c;

        /* renamed from: d  reason: collision with root package name */
        private long f5658d;

        public b() {
            g.f6800a.a((m) this);
            resume();
            Thread thread = new Thread(this, "Timer");
            thread.setDaemon(true);
            thread.start();
        }

        public void dispose() {
            synchronized (v0.f5647b) {
                if (v0.f5648c == this) {
                    v0.f5648c = null;
                }
                this.f5656b.clear();
                v0.f5647b.notifyAll();
            }
            g.f6800a.b(this);
        }

        public void pause() {
            synchronized (v0.f5647b) {
                this.f5658d = System.nanoTime() / 1000000;
                v0.f5647b.notifyAll();
            }
        }

        public void resume() {
            synchronized (v0.f5647b) {
                long nanoTime = (System.nanoTime() / 1000000) - this.f5658d;
                int i2 = this.f5656b.f5374b;
                for (int i3 = 0; i3 < i2; i3++) {
                    this.f5656b.get(i3).a(nanoTime);
                }
                this.f5658d = 0;
                v0.f5647b.notifyAll();
            }
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(3:23|(2:25|26)|27) */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0072 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r10 = this;
            L_0x0000:
                java.lang.Object r0 = com.badlogic.gdx.utils.v0.f5647b
                monitor-enter(r0)
                com.badlogic.gdx.utils.v0$b r1 = com.badlogic.gdx.utils.v0.f5648c     // Catch:{ all -> 0x007b }
                if (r1 != r10) goto L_0x0076
                e.d.b.e r1 = r10.f5655a     // Catch:{ all -> 0x007b }
                e.d.b.e r2 = e.d.b.g.f6804e     // Catch:{ all -> 0x007b }
                if (r1 == r2) goto L_0x000e
                goto L_0x0076
            L_0x000e:
                r1 = 5000(0x1388, double:2.4703E-320)
                long r3 = r10.f5658d     // Catch:{ all -> 0x007b }
                r5 = 0
                int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r7 != 0) goto L_0x005e
                long r3 = java.lang.System.nanoTime()     // Catch:{ all -> 0x007b }
                r7 = 1000000(0xf4240, double:4.940656E-318)
                long r3 = r3 / r7
                r7 = 0
                com.badlogic.gdx.utils.a<com.badlogic.gdx.utils.v0> r8 = r10.f5656b     // Catch:{ all -> 0x007b }
                int r8 = r8.f5374b     // Catch:{ all -> 0x007b }
            L_0x0025:
                if (r7 >= r8) goto L_0x005e
                com.badlogic.gdx.utils.a<com.badlogic.gdx.utils.v0> r9 = r10.f5656b     // Catch:{ all -> 0x0036 }
                java.lang.Object r9 = r9.get(r7)     // Catch:{ all -> 0x0036 }
                com.badlogic.gdx.utils.v0 r9 = (com.badlogic.gdx.utils.v0) r9     // Catch:{ all -> 0x0036 }
                long r1 = r9.a(r3, r1)     // Catch:{ all -> 0x0036 }
                int r7 = r7 + 1
                goto L_0x0025
            L_0x0036:
                r1 = move-exception
                com.badlogic.gdx.utils.o r2 = new com.badlogic.gdx.utils.o     // Catch:{ all -> 0x007b }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x007b }
                r3.<init>()     // Catch:{ all -> 0x007b }
                java.lang.String r4 = "Task failed: "
                r3.append(r4)     // Catch:{ all -> 0x007b }
                com.badlogic.gdx.utils.a<com.badlogic.gdx.utils.v0> r4 = r10.f5656b     // Catch:{ all -> 0x007b }
                java.lang.Object r4 = r4.get(r7)     // Catch:{ all -> 0x007b }
                com.badlogic.gdx.utils.v0 r4 = (com.badlogic.gdx.utils.v0) r4     // Catch:{ all -> 0x007b }
                java.lang.Class r4 = r4.getClass()     // Catch:{ all -> 0x007b }
                java.lang.String r4 = r4.getName()     // Catch:{ all -> 0x007b }
                r3.append(r4)     // Catch:{ all -> 0x007b }
                java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x007b }
                r2.<init>(r3, r1)     // Catch:{ all -> 0x007b }
                throw r2     // Catch:{ all -> 0x007b }
            L_0x005e:
                com.badlogic.gdx.utils.v0$b r3 = com.badlogic.gdx.utils.v0.f5648c     // Catch:{ all -> 0x007b }
                if (r3 != r10) goto L_0x0074
                e.d.b.e r3 = r10.f5655a     // Catch:{ all -> 0x007b }
                e.d.b.e r4 = e.d.b.g.f6804e     // Catch:{ all -> 0x007b }
                if (r3 == r4) goto L_0x0069
                goto L_0x0074
            L_0x0069:
                int r3 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
                if (r3 <= 0) goto L_0x0072
                java.lang.Object r3 = com.badlogic.gdx.utils.v0.f5647b     // Catch:{ InterruptedException -> 0x0072 }
                r3.wait(r1)     // Catch:{ InterruptedException -> 0x0072 }
            L_0x0072:
                monitor-exit(r0)     // Catch:{ all -> 0x007b }
                goto L_0x0000
            L_0x0074:
                monitor-exit(r0)     // Catch:{ all -> 0x007b }
                goto L_0x0077
            L_0x0076:
                monitor-exit(r0)     // Catch:{ all -> 0x007b }
            L_0x0077:
                r10.dispose()
                return
            L_0x007b:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x007b }
                goto L_0x007f
            L_0x007e:
                throw r1
            L_0x007f:
                goto L_0x007e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.v0.b.run():void");
        }
    }

    public v0() {
        b();
    }

    public static v0 d() {
        v0 v0Var;
        synchronized (f5647b) {
            b e2 = e();
            if (e2.f5657c == null) {
                e2.f5657c = new v0();
            }
            v0Var = e2.f5657c;
        }
        return v0Var;
    }

    private static b e() {
        b bVar;
        synchronized (f5647b) {
            if (f5648c == null || f5648c.f5655a != g.f6804e) {
                if (f5648c != null) {
                    f5648c.dispose();
                }
                f5648c = new b();
            }
            bVar = f5648c;
        }
        return bVar;
    }

    public a a(a aVar, float f2) {
        a(aVar, f2, Animation.CurveTimeline.LINEAR, 0);
        return aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.a.a(java.lang.Object, boolean):boolean
     arg types: [com.badlogic.gdx.utils.v0, int]
     candidates:
      com.badlogic.gdx.utils.a.a(int, int):void
      com.badlogic.gdx.utils.a.a(int, java.lang.Object):void
      com.badlogic.gdx.utils.a.a(com.badlogic.gdx.utils.a, boolean):boolean
      com.badlogic.gdx.utils.a.a(java.lang.Object, boolean):boolean */
    public void b() {
        synchronized (f5647b) {
            a<v0> aVar = e().f5656b;
            if (!aVar.a((Object) this, true)) {
                aVar.add(this);
                f5647b.notifyAll();
            }
        }
    }

    public void c() {
        synchronized (f5647b) {
            e().f5656b.d(this, true);
        }
    }

    public a a(a aVar, float f2, float f3) {
        a(aVar, f2, f3, -1);
        return aVar;
    }

    public a a(a aVar, float f2, float f3, int i2) {
        synchronized (this) {
            synchronized (aVar) {
                if (aVar.f5654e == null) {
                    aVar.f5654e = this;
                    aVar.f5651b = (System.nanoTime() / 1000000) + ((long) (f2 * 1000.0f));
                    aVar.f5652c = (long) (f3 * 1000.0f);
                    aVar.f5653d = i2;
                    this.f5649a.add(aVar);
                } else {
                    throw new IllegalArgumentException("The same task may not be scheduled twice.");
                }
            }
        }
        synchronized (f5647b) {
            f5647b.notifyAll();
        }
        return aVar;
    }

    public static a b(a aVar, float f2) {
        d().a(aVar, f2);
        return aVar;
    }

    public static a b(a aVar, float f2, float f3) {
        d().a(aVar, f2, f3);
        return aVar;
    }

    public synchronized void a() {
        int i2 = this.f5649a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            a aVar = this.f5649a.get(i3);
            synchronized (aVar) {
                aVar.f5651b = 0;
                aVar.f5654e = null;
            }
        }
        this.f5649a.clear();
    }

    /* access modifiers changed from: package-private */
    public synchronized long a(long j2, long j3) {
        int i2 = 0;
        int i3 = this.f5649a.f5374b;
        while (i2 < i3) {
            a aVar = this.f5649a.get(i2);
            synchronized (aVar) {
                if (aVar.f5651b > j2) {
                    j3 = Math.min(j3, aVar.f5651b - j2);
                } else {
                    if (aVar.f5653d == 0) {
                        aVar.f5654e = null;
                        this.f5649a.d(i2);
                        i2--;
                        i3--;
                    } else {
                        aVar.f5651b = aVar.f5652c + j2;
                        j3 = Math.min(j3, aVar.f5652c);
                        if (aVar.f5653d > 0) {
                            aVar.f5653d--;
                        }
                    }
                    aVar.f5650a.a(aVar);
                }
            }
            i2++;
        }
        return j3;
    }

    public synchronized void a(long j2) {
        int i2 = this.f5649a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            a aVar = this.f5649a.get(i3);
            synchronized (aVar) {
                aVar.f5651b += j2;
            }
        }
    }
}
