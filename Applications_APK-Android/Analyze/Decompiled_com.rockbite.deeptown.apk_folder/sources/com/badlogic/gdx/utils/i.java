package com.badlogic.gdx.utils;

/* compiled from: ComparableTimSort */
class i {

    /* renamed from: a  reason: collision with root package name */
    private Object[] f5491a;

    /* renamed from: b  reason: collision with root package name */
    private int f5492b = 7;

    /* renamed from: c  reason: collision with root package name */
    private Object[] f5493c = new Object[256];

    /* renamed from: d  reason: collision with root package name */
    private int f5494d;

    /* renamed from: e  reason: collision with root package name */
    private int f5495e = 0;

    /* renamed from: f  reason: collision with root package name */
    private final int[] f5496f = new int[40];

    /* renamed from: g  reason: collision with root package name */
    private final int[] f5497g = new int[40];

    i() {
    }

    private static int b(Object[] objArr, int i2, int i3) {
        int i4 = i2 + 1;
        if (i4 == i3) {
            return 1;
        }
        int i5 = i4 + 1;
        if (((Comparable) objArr[i4]).compareTo(objArr[i2]) < 0) {
            while (i5 < i3 && ((Comparable) objArr[i5]).compareTo(objArr[i5 - 1]) < 0) {
                i5++;
            }
            c(objArr, i2, i5);
        } else {
            while (i5 < i3 && ((Comparable) objArr[i5]).compareTo(objArr[i5 - 1]) >= 0) {
                i5++;
            }
        }
        return i5 - i2;
    }

    private static int c(int i2) {
        int i3 = 0;
        while (i2 >= 32) {
            i3 |= i2 & 1;
            i2 >>= 1;
        }
        return i2 + i3;
    }

    private static void c(Object[] objArr, int i2, int i3) {
        int i4 = i3 - 1;
        while (i2 < i4) {
            Object obj = objArr[i2];
            objArr[i2] = objArr[i4];
            objArr[i4] = obj;
            i4--;
            i2++;
        }
    }

    public void a(Object[] objArr, int i2, int i3) {
        this.f5495e = 0;
        a(objArr.length, i2, i3);
        int i4 = i3 - i2;
        if (i4 >= 2) {
            if (i4 < 32) {
                a(objArr, i2, i3, b(objArr, i2, i3) + i2);
                return;
            }
            this.f5491a = objArr;
            this.f5494d = 0;
            int c2 = c(i4);
            do {
                int b2 = b(objArr, i2, i3);
                if (b2 < c2) {
                    int i5 = i4 <= c2 ? i4 : c2;
                    a(objArr, i2, i2 + i5, b2 + i2);
                    b2 = i5;
                }
                a(i2, b2);
                a();
                i2 += b2;
                i4 -= b2;
            } while (i4 != 0);
            b();
            this.f5491a = null;
            Object[] objArr2 = this.f5493c;
            int i6 = this.f5494d;
            for (int i7 = 0; i7 < i6; i7++) {
                objArr2[i7] = null;
            }
        }
    }

    private void b() {
        while (true) {
            int i2 = this.f5495e;
            if (i2 > 1) {
                int i3 = i2 - 2;
                if (i3 > 0) {
                    int[] iArr = this.f5497g;
                    if (iArr[i3 - 1] < iArr[i3 + 1]) {
                        i3--;
                    }
                }
                b(i3);
            } else {
                return;
            }
        }
    }

    private void b(int i2) {
        int[] iArr = this.f5496f;
        int i3 = iArr[i2];
        int[] iArr2 = this.f5497g;
        int i4 = iArr2[i2];
        int i5 = i2 + 1;
        int i6 = iArr[i5];
        int i7 = iArr2[i5];
        iArr2[i2] = i4 + i7;
        if (i2 == this.f5495e - 3) {
            int i8 = i2 + 2;
            iArr[i5] = iArr[i8];
            iArr2[i5] = iArr2[i8];
        }
        this.f5495e--;
        Object[] objArr = this.f5491a;
        int b2 = b((Comparable) objArr[i6], objArr, i3, i4, 0);
        int i9 = i3 + b2;
        int i10 = i4 - b2;
        if (i10 != 0) {
            Object[] objArr2 = this.f5491a;
            int a2 = a((Comparable) objArr2[(i9 + i10) - 1], objArr2, i6, i7, i7 - 1);
            if (a2 != 0) {
                if (i10 <= a2) {
                    b(i9, i10, i6, a2);
                } else {
                    a(i9, i10, i6, a2);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:0:0x0000, code lost:
        if (r8 == r6) goto L_0x0002;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0021, code lost:
        r2 = r8 - r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        if (r2 == 1) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0026, code lost:
        if (r2 == 2) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        java.lang.System.arraycopy(r5, r1, r5, r1 + 1, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002e, code lost:
        r5[r1 + 2] = r5[r1 + 1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0036, code lost:
        r5[r1 + 1] = r5[r1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003c, code lost:
        r5[r1] = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0002, code lost:
        r8 = r8 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        if (r8 >= r7) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0006, code lost:
        r0 = (java.lang.Comparable) r5[r8];
        r1 = r6;
        r2 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r1 >= r2) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000f, code lost:
        r3 = (r1 + r2) >>> 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0019, code lost:
        if (r0.compareTo(r5[r3]) >= 0) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
        r1 = r3 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(java.lang.Object[] r5, int r6, int r7, int r8) {
        /*
            if (r8 != r6) goto L_0x0004
        L_0x0002:
            int r8 = r8 + 1
        L_0x0004:
            if (r8 >= r7) goto L_0x003f
            r0 = r5[r8]
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            r1 = r6
            r2 = r8
        L_0x000c:
            r3 = 1
            if (r1 >= r2) goto L_0x0021
            int r4 = r1 + r2
            int r3 = r4 >>> 1
            r4 = r5[r3]
            int r4 = r0.compareTo(r4)
            if (r4 >= 0) goto L_0x001d
            r2 = r3
            goto L_0x000c
        L_0x001d:
            int r3 = r3 + 1
            r1 = r3
            goto L_0x000c
        L_0x0021:
            int r2 = r8 - r1
            if (r2 == r3) goto L_0x0036
            r3 = 2
            if (r2 == r3) goto L_0x002e
            int r3 = r1 + 1
            java.lang.System.arraycopy(r5, r1, r5, r3, r2)
            goto L_0x003c
        L_0x002e:
            int r2 = r1 + 2
            int r3 = r1 + 1
            r3 = r5[r3]
            r5[r2] = r3
        L_0x0036:
            int r2 = r1 + 1
            r3 = r5[r1]
            r5[r2] = r3
        L_0x003c:
            r5[r1] = r0
            goto L_0x0002
        L_0x003f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.i.a(java.lang.Object[], int, int, int):void");
    }

    private static int b(Comparable<Object> comparable, Object[] objArr, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7 = i2 + i4;
        if (comparable.compareTo(objArr[i7]) < 0) {
            int i8 = i4 + 1;
            int i9 = 0;
            int i10 = 1;
            while (i10 < i8 && comparable.compareTo(objArr[i7 - i10]) < 0) {
                int i11 = (i10 << 1) + 1;
                if (i11 <= 0) {
                    i9 = i10;
                    i10 = i8;
                } else {
                    int i12 = i10;
                    i10 = i11;
                    i9 = i12;
                }
            }
            if (i10 <= i8) {
                i8 = i10;
            }
            i6 = i4 - i8;
            i5 = i4 - i9;
        } else {
            int i13 = i3 - i4;
            int i14 = 0;
            int i15 = 1;
            while (i15 < i13 && comparable.compareTo(objArr[i7 + i15]) >= 0) {
                int i16 = (i15 << 1) + 1;
                if (i16 <= 0) {
                    i14 = i15;
                    i15 = i13;
                } else {
                    int i17 = i15;
                    i15 = i16;
                    i14 = i17;
                }
            }
            if (i15 <= i13) {
                i13 = i15;
            }
            int i18 = i14 + i4;
            i5 = i4 + i13;
            i6 = i18;
        }
        int i19 = i6 + 1;
        while (i19 < i5) {
            int i20 = ((i5 - i19) >>> 1) + i19;
            if (comparable.compareTo(objArr[i2 + i20]) < 0) {
                i5 = i20;
            } else {
                i19 = i20 + 1;
            }
        }
        return i5;
    }

    private void a(int i2, int i3) {
        int[] iArr = this.f5496f;
        int i4 = this.f5495e;
        iArr[i4] = i2;
        this.f5497g[i4] = i3;
        this.f5495e = i4 + 1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0047, code lost:
        r4 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0064, code lost:
        r15 = b((java.lang.Comparable) r0[r4], r1, r13, r5, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006c, code lost:
        if (r15 == 0) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006e, code lost:
        java.lang.System.arraycopy(r1, r13, r0, r3, r15);
        r3 = r3 + r15;
        r13 = r13 + r15;
        r5 = r5 - r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0074, code lost:
        if (r5 > 1) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0076, code lost:
        r6 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0078, code lost:
        r6 = r3 + 1;
        r8 = r4 + 1;
        r0[r3] = r0[r4];
        r7 = r7 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0082, code lost:
        if (r7 != 0) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0085, code lost:
        r3 = a((java.lang.Comparable) r1[r13], r0, r8, r7, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x008d, code lost:
        if (r3 == 0) goto L_0x009d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x008f, code lost:
        java.lang.System.arraycopy(r0, r8, r0, r6, r3);
        r4 = r6 + r3;
        r6 = r8 + r3;
        r7 = r7 - r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0097, code lost:
        if (r7 != 0) goto L_0x009f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0099, code lost:
        r10 = r6;
        r6 = r4;
        r4 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x009d, code lost:
        r4 = r6;
        r6 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x009f, code lost:
        r8 = r4 + 1;
        r9 = r13 + 1;
        r0[r4] = r1[r13];
        r5 = r5 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a9, code lost:
        if (r5 != 1) goto L_0x00cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ab, code lost:
        r4 = r6;
        r6 = r8;
        r13 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00cc, code lost:
        r14 = r14 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00cf, code lost:
        if (r15 < 7) goto L_0x00d3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00d1, code lost:
        r15 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d3, code lost:
        r15 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00d4, code lost:
        if (r3 < 7) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00d6, code lost:
        r13 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00d8, code lost:
        r13 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00da, code lost:
        if ((r13 | r15) != false) goto L_0x00e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00dc, code lost:
        if (r14 >= 0) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00de, code lost:
        r14 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00e7, code lost:
        r4 = r6;
        r3 = r8;
        r13 = r9;
     */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0064 A[EDGE_INSN: B:69:0x0064->B:23:0x0064 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r12, int r13, int r14, int r15) {
        /*
            r11 = this;
            java.lang.Object[] r0 = r11.f5491a
            java.lang.Object[] r1 = r11.a(r13)
            r2 = 0
            java.lang.System.arraycopy(r0, r12, r1, r2, r13)
            int r3 = r12 + 1
            int r4 = r14 + 1
            r14 = r0[r14]
            r0[r12] = r14
            int r15 = r15 + -1
            if (r15 != 0) goto L_0x001a
            java.lang.System.arraycopy(r1, r2, r0, r3, r13)
            return
        L_0x001a:
            r12 = 1
            if (r13 != r12) goto L_0x0026
            java.lang.System.arraycopy(r0, r4, r0, r3, r15)
            int r3 = r3 + r15
            r12 = r1[r2]
            r0[r3] = r12
            return
        L_0x0026:
            int r14 = r11.f5492b
            r5 = r13
            r13 = 0
        L_0x002a:
            r7 = r15
            r15 = 0
            r6 = 0
        L_0x002d:
            r8 = r0[r4]
            java.lang.Comparable r8 = (java.lang.Comparable) r8
            r9 = r1[r13]
            int r8 = r8.compareTo(r9)
            if (r8 >= 0) goto L_0x004e
            int r15 = r3 + 1
            int r8 = r4 + 1
            r4 = r0[r4]
            r0[r3] = r4
            int r6 = r6 + r12
            int r7 = r7 + -1
            if (r7 != 0) goto L_0x004a
            r6 = r15
        L_0x0047:
            r4 = r8
            goto L_0x00ae
        L_0x004a:
            r3 = r15
            r4 = r8
            r15 = 0
            goto L_0x0060
        L_0x004e:
            int r6 = r3 + 1
            int r8 = r13 + 1
            r13 = r1[r13]
            r0[r3] = r13
            int r15 = r15 + r12
            int r5 = r5 + -1
            if (r5 != r12) goto L_0x005d
            r13 = r8
            goto L_0x00ae
        L_0x005d:
            r3 = r6
            r13 = r8
            r6 = 0
        L_0x0060:
            r8 = r15 | r6
            if (r8 < r14) goto L_0x002d
        L_0x0064:
            r15 = r0[r4]
            java.lang.Comparable r15 = (java.lang.Comparable) r15
            int r15 = b(r15, r1, r13, r5, r2)
            if (r15 == 0) goto L_0x0078
            java.lang.System.arraycopy(r1, r13, r0, r3, r15)
            int r3 = r3 + r15
            int r13 = r13 + r15
            int r5 = r5 - r15
            if (r5 > r12) goto L_0x0078
            r6 = r3
            goto L_0x00ae
        L_0x0078:
            int r6 = r3 + 1
            int r8 = r4 + 1
            r4 = r0[r4]
            r0[r3] = r4
            int r7 = r7 + -1
            if (r7 != 0) goto L_0x0085
            goto L_0x0047
        L_0x0085:
            r3 = r1[r13]
            java.lang.Comparable r3 = (java.lang.Comparable) r3
            int r3 = a(r3, r0, r8, r7, r2)
            if (r3 == 0) goto L_0x009d
            java.lang.System.arraycopy(r0, r8, r0, r6, r3)
            int r4 = r6 + r3
            int r6 = r8 + r3
            int r7 = r7 - r3
            if (r7 != 0) goto L_0x009f
            r10 = r6
            r6 = r4
            r4 = r10
            goto L_0x00ae
        L_0x009d:
            r4 = r6
            r6 = r8
        L_0x009f:
            int r8 = r4 + 1
            int r9 = r13 + 1
            r13 = r1[r13]
            r0[r4] = r13
            int r5 = r5 + -1
            if (r5 != r12) goto L_0x00cc
            r4 = r6
            r6 = r8
            r13 = r9
        L_0x00ae:
            if (r14 >= r12) goto L_0x00b1
            r14 = 1
        L_0x00b1:
            r11.f5492b = r14
            if (r5 != r12) goto L_0x00be
            java.lang.System.arraycopy(r0, r4, r0, r6, r7)
            int r6 = r6 + r7
            r12 = r1[r13]
            r0[r6] = r12
            goto L_0x00c3
        L_0x00be:
            if (r5 == 0) goto L_0x00c4
            java.lang.System.arraycopy(r1, r13, r0, r6, r5)
        L_0x00c3:
            return
        L_0x00c4:
            java.lang.IllegalArgumentException r12 = new java.lang.IllegalArgumentException
            java.lang.String r13 = "Comparison method violates its general contract!"
            r12.<init>(r13)
            throw r12
        L_0x00cc:
            int r14 = r14 + -1
            r13 = 7
            if (r15 < r13) goto L_0x00d3
            r15 = 1
            goto L_0x00d4
        L_0x00d3:
            r15 = 0
        L_0x00d4:
            if (r3 < r13) goto L_0x00d8
            r13 = 1
            goto L_0x00d9
        L_0x00d8:
            r13 = 0
        L_0x00d9:
            r13 = r13 | r15
            if (r13 != 0) goto L_0x00e7
            if (r14 >= 0) goto L_0x00df
            r14 = 0
        L_0x00df:
            int r14 = r14 + 2
            r4 = r6
            r15 = r7
            r3 = r8
            r13 = r9
            goto L_0x002a
        L_0x00e7:
            r4 = r6
            r3 = r8
            r13 = r9
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.i.b(int, int, int, int):void");
    }

    private void a() {
        while (true) {
            int i2 = this.f5495e;
            if (i2 > 1) {
                int i3 = i2 - 2;
                if (i3 > 0) {
                    int[] iArr = this.f5497g;
                    int i4 = i3 - 1;
                    int i5 = i3 + 1;
                    if (iArr[i4] <= iArr[i3] + iArr[i5]) {
                        if (iArr[i4] < iArr[i5]) {
                            i3--;
                        }
                        b(i3);
                    }
                }
                int[] iArr2 = this.f5497g;
                if (iArr2[i3] <= iArr2[i3 + 1]) {
                    b(i3);
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    private static int a(Comparable<Object> comparable, Object[] objArr, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7 = i2 + i4;
        if (comparable.compareTo(objArr[i7]) > 0) {
            int i8 = i3 - i4;
            int i9 = 0;
            int i10 = 1;
            while (i10 < i8 && comparable.compareTo(objArr[i7 + i10]) > 0) {
                int i11 = (i10 << 1) + 1;
                if (i11 <= 0) {
                    i9 = i10;
                    i10 = i8;
                } else {
                    int i12 = i10;
                    i10 = i11;
                    i9 = i12;
                }
            }
            if (i10 <= i8) {
                i8 = i10;
            }
            int i13 = i9 + i4;
            i5 = i8 + i4;
            i6 = i13;
        } else {
            int i14 = i4 + 1;
            int i15 = 0;
            int i16 = 1;
            while (i16 < i14 && comparable.compareTo(objArr[i7 - i16]) <= 0) {
                int i17 = (i16 << 1) + 1;
                if (i17 <= 0) {
                    i15 = i16;
                    i16 = i14;
                } else {
                    int i18 = i16;
                    i16 = i17;
                    i15 = i18;
                }
            }
            if (i16 <= i14) {
                i14 = i16;
            }
            i6 = i4 - i14;
            i5 = i4 - i15;
        }
        int i19 = i6 + 1;
        while (i19 < i5) {
            int i20 = ((i5 - i19) >>> 1) + i19;
            if (comparable.compareTo(objArr[i2 + i20]) > 0) {
                i19 = i20 + 1;
            } else {
                i5 = i20;
            }
        }
        return i5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0065, code lost:
        r5 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006f, code lost:
        r13 = r8 - b((java.lang.Comparable) r1[r5], r0, r12, r8, r8 - 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007b, code lost:
        if (r13 == 0) goto L_0x008b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x007d, code lost:
        r3 = r6 - r13;
        r7 = r7 - r13;
        r8 = r8 - r13;
        java.lang.System.arraycopy(r0, r7 + 1, r0, r3 + 1, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0088, code lost:
        if (r8 != 0) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008b, code lost:
        r3 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x008c, code lost:
        r6 = r3 - 1;
        r9 = r5 - 1;
        r0[r3] = r1[r5];
        r15 = r15 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0096, code lost:
        if (r15 != 1) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0098, code lost:
        r3 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x009a, code lost:
        r3 = r15 - a((java.lang.Comparable) r0[r7], r1, 0, r15, r15 - 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a6, code lost:
        if (r3 == 0) goto L_0x00b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a8, code lost:
        r5 = r6 - r3;
        r6 = r9 - r3;
        r15 = r15 - r3;
        java.lang.System.arraycopy(r1, r6 + 1, r0, r5 + 1, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b4, code lost:
        if (r15 > 1) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b6, code lost:
        r3 = r5;
        r5 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00b9, code lost:
        r5 = r6;
        r6 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00bb, code lost:
        r9 = r5 - 1;
        r10 = r7 - 1;
        r0[r5] = r0[r7];
        r8 = r8 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c5, code lost:
        if (r8 != 0) goto L_0x00ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00c7, code lost:
        r5 = r6;
        r3 = r9;
        r7 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ef, code lost:
        r14 = r14 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00f2, code lost:
        if (r13 < 7) goto L_0x00f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00f4, code lost:
        r13 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00f6, code lost:
        r13 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00f7, code lost:
        if (r3 < 7) goto L_0x00fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00f9, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00fb, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00fd, code lost:
        if ((r13 | r3) != false) goto L_0x010a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ff, code lost:
        if (r14 >= 0) goto L_0x0102;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0101, code lost:
        r14 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x010a, code lost:
        r5 = r6;
        r6 = r9;
        r7 = r10;
     */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x006f A[EDGE_INSN: B:68:0x006f->B:21:0x006f ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r12, int r13, int r14, int r15) {
        /*
            r11 = this;
            java.lang.Object[] r0 = r11.f5491a
            java.lang.Object[] r1 = r11.a(r15)
            r2 = 0
            java.lang.System.arraycopy(r0, r14, r1, r2, r15)
            int r3 = r12 + r13
            r4 = 1
            int r3 = r3 - r4
            int r5 = r15 + -1
            int r14 = r14 + r15
            int r14 = r14 - r4
            int r6 = r14 + -1
            int r7 = r3 + -1
            r3 = r0[r3]
            r0[r14] = r3
            int r13 = r13 + -1
            if (r13 != 0) goto L_0x0023
            int r6 = r6 - r5
            java.lang.System.arraycopy(r1, r2, r0, r6, r15)
            return
        L_0x0023:
            if (r15 != r4) goto L_0x0032
            int r6 = r6 - r13
            int r7 = r7 - r13
            int r7 = r7 + r4
            int r12 = r6 + 1
            java.lang.System.arraycopy(r0, r7, r0, r12, r13)
            r12 = r1[r5]
            r0[r6] = r12
            return
        L_0x0032:
            int r14 = r11.f5492b
        L_0x0034:
            r8 = r13
            r13 = 0
            r3 = 0
        L_0x0037:
            r9 = r1[r5]
            java.lang.Comparable r9 = (java.lang.Comparable) r9
            r10 = r0[r7]
            int r9 = r9.compareTo(r10)
            if (r9 >= 0) goto L_0x0058
            int r13 = r6 + -1
            int r9 = r7 + -1
            r7 = r0[r7]
            r0[r6] = r7
            int r3 = r3 + r4
            int r8 = r8 + -1
            if (r8 != 0) goto L_0x0054
            r3 = r13
            r7 = r9
            goto L_0x00ca
        L_0x0054:
            r6 = r13
            r7 = r9
            r13 = 0
            goto L_0x006b
        L_0x0058:
            int r3 = r6 + -1
            int r9 = r5 + -1
            r5 = r1[r5]
            r0[r6] = r5
            int r13 = r13 + r4
            int r15 = r15 + -1
            if (r15 != r4) goto L_0x0068
        L_0x0065:
            r5 = r9
            goto L_0x00ca
        L_0x0068:
            r6 = r3
            r5 = r9
            r3 = 0
        L_0x006b:
            r9 = r3 | r13
            if (r9 < r14) goto L_0x0037
        L_0x006f:
            r13 = r1[r5]
            java.lang.Comparable r13 = (java.lang.Comparable) r13
            int r3 = r8 + -1
            int r13 = b(r13, r0, r12, r8, r3)
            int r13 = r8 - r13
            if (r13 == 0) goto L_0x008b
            int r3 = r6 - r13
            int r7 = r7 - r13
            int r8 = r8 - r13
            int r6 = r7 + 1
            int r9 = r3 + 1
            java.lang.System.arraycopy(r0, r6, r0, r9, r13)
            if (r8 != 0) goto L_0x008c
            goto L_0x00ca
        L_0x008b:
            r3 = r6
        L_0x008c:
            int r6 = r3 + -1
            int r9 = r5 + -1
            r5 = r1[r5]
            r0[r3] = r5
            int r15 = r15 + -1
            if (r15 != r4) goto L_0x009a
            r3 = r6
            goto L_0x0065
        L_0x009a:
            r3 = r0[r7]
            java.lang.Comparable r3 = (java.lang.Comparable) r3
            int r5 = r15 + -1
            int r3 = a(r3, r1, r2, r15, r5)
            int r3 = r15 - r3
            if (r3 == 0) goto L_0x00b9
            int r5 = r6 - r3
            int r6 = r9 - r3
            int r15 = r15 - r3
            int r9 = r6 + 1
            int r10 = r5 + 1
            java.lang.System.arraycopy(r1, r9, r0, r10, r3)
            if (r15 > r4) goto L_0x00bb
            r3 = r5
            r5 = r6
            goto L_0x00ca
        L_0x00b9:
            r5 = r6
            r6 = r9
        L_0x00bb:
            int r9 = r5 + -1
            int r10 = r7 + -1
            r7 = r0[r7]
            r0[r5] = r7
            int r8 = r8 + -1
            if (r8 != 0) goto L_0x00ef
            r5 = r6
            r3 = r9
            r7 = r10
        L_0x00ca:
            if (r14 >= r4) goto L_0x00cd
            r14 = 1
        L_0x00cd:
            r11.f5492b = r14
            if (r15 != r4) goto L_0x00de
            int r3 = r3 - r8
            int r7 = r7 - r8
            int r7 = r7 + r4
            int r12 = r3 + 1
            java.lang.System.arraycopy(r0, r7, r0, r12, r8)
            r12 = r1[r5]
            r0[r3] = r12
            goto L_0x00e6
        L_0x00de:
            if (r15 == 0) goto L_0x00e7
            int r12 = r15 + -1
            int r3 = r3 - r12
            java.lang.System.arraycopy(r1, r2, r0, r3, r15)
        L_0x00e6:
            return
        L_0x00e7:
            java.lang.IllegalArgumentException r12 = new java.lang.IllegalArgumentException
            java.lang.String r13 = "Comparison method violates its general contract!"
            r12.<init>(r13)
            throw r12
        L_0x00ef:
            int r14 = r14 + -1
            r5 = 7
            if (r13 < r5) goto L_0x00f6
            r13 = 1
            goto L_0x00f7
        L_0x00f6:
            r13 = 0
        L_0x00f7:
            if (r3 < r5) goto L_0x00fb
            r3 = 1
            goto L_0x00fc
        L_0x00fb:
            r3 = 0
        L_0x00fc:
            r13 = r13 | r3
            if (r13 != 0) goto L_0x010a
            if (r14 >= 0) goto L_0x0102
            r14 = 0
        L_0x0102:
            int r14 = r14 + 2
            r5 = r6
            r13 = r8
            r6 = r9
            r7 = r10
            goto L_0x0034
        L_0x010a:
            r5 = r6
            r6 = r9
            r7 = r10
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.i.a(int, int, int, int):void");
    }

    private Object[] a(int i2) {
        this.f5494d = Math.max(this.f5494d, i2);
        if (this.f5493c.length < i2) {
            int i3 = (i2 >> 1) | i2;
            int i4 = i3 | (i3 >> 2);
            int i5 = i4 | (i4 >> 4);
            int i6 = i5 | (i5 >> 8);
            int i7 = (i6 | (i6 >> 16)) + 1;
            if (i7 >= 0) {
                i2 = Math.min(i7, this.f5491a.length >>> 1);
            }
            this.f5493c = new Object[i2];
        }
        return this.f5493c;
    }

    private static void a(int i2, int i3, int i4) {
        if (i3 > i4) {
            throw new IllegalArgumentException("fromIndex(" + i3 + ") > toIndex(" + i4 + ")");
        } else if (i3 < 0) {
            throw new ArrayIndexOutOfBoundsException(i3);
        } else if (i4 > i2) {
            throw new ArrayIndexOutOfBoundsException(i4);
        }
    }
}
