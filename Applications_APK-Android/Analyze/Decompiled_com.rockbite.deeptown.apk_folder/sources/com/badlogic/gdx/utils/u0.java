package com.badlogic.gdx.utils;

/* compiled from: TimeUtils */
public final class u0 {
    public static long a() {
        return System.currentTimeMillis();
    }

    public static long b() {
        return System.nanoTime();
    }

    public static long a(long j2) {
        return a() - j2;
    }
}
