package com.badlogic.gdx.utils;

import com.badlogic.gdx.utils.c0;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: ArrayMap */
public class b<K, V> implements Iterable<c0.b<K, V>> {

    /* renamed from: a  reason: collision with root package name */
    public K[] f5414a;

    /* renamed from: b  reason: collision with root package name */
    public V[] f5415b;

    /* renamed from: c  reason: collision with root package name */
    public int f5416c;

    /* renamed from: d  reason: collision with root package name */
    public boolean f5417d;

    /* renamed from: e  reason: collision with root package name */
    private a f5418e;

    /* renamed from: f  reason: collision with root package name */
    private a f5419f;

    /* compiled from: ArrayMap */
    public static class a<K, V> implements Iterable<c0.b<K, V>>, Iterator<c0.b<K, V>> {

        /* renamed from: a  reason: collision with root package name */
        private final b<K, V> f5420a;

        /* renamed from: b  reason: collision with root package name */
        c0.b<K, V> f5421b = new c0.b<>();

        /* renamed from: c  reason: collision with root package name */
        int f5422c;

        /* renamed from: d  reason: collision with root package name */
        boolean f5423d = true;

        public a(b<K, V> bVar) {
            this.f5420a = bVar;
        }

        public boolean hasNext() {
            if (this.f5423d) {
                return this.f5422c < this.f5420a.f5416c;
            }
            throw new o("#iterator() cannot be used nested.");
        }

        public Iterator<c0.b<K, V>> iterator() {
            return this;
        }

        public void remove() {
            this.f5422c--;
            this.f5420a.c(this.f5422c);
        }

        public c0.b<K, V> next() {
            int i2 = this.f5422c;
            b<K, V> bVar = this.f5420a;
            if (i2 >= bVar.f5416c) {
                throw new NoSuchElementException(String.valueOf(i2));
            } else if (this.f5423d) {
                c0.b<K, V> bVar2 = this.f5421b;
                bVar2.f5459a = bVar.f5414a[i2];
                V[] vArr = bVar.f5415b;
                this.f5422c = i2 + 1;
                bVar2.f5460b = vArr[i2];
                return bVar2;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.b.<init>(boolean, int):void
     arg types: [int, int]
     candidates:
      com.badlogic.gdx.utils.b.<init>(java.lang.Class, java.lang.Class):void
      com.badlogic.gdx.utils.b.<init>(boolean, int):void */
    public b() {
        this(true, 16);
    }

    public V a(K k2) {
        return a(k2, null);
    }

    public int b(K k2, V v) {
        int b2 = b(k2);
        if (b2 == -1) {
            int i2 = this.f5416c;
            if (i2 == this.f5414a.length) {
                d(Math.max(8, (int) (((float) i2) * 1.75f)));
            }
            b2 = this.f5416c;
            this.f5416c = b2 + 1;
        }
        this.f5414a[b2] = k2;
        this.f5415b[b2] = v;
        return b2;
    }

    public void c(int i2) {
        int i3 = this.f5416c;
        if (i2 < i3) {
            K[] kArr = this.f5414a;
            this.f5416c = i3 - 1;
            if (this.f5417d) {
                int i4 = i2 + 1;
                System.arraycopy(kArr, i4, kArr, i2, this.f5416c - i2);
                V[] vArr = this.f5415b;
                System.arraycopy(vArr, i4, vArr, i2, this.f5416c - i2);
            } else {
                int i5 = this.f5416c;
                kArr[i2] = kArr[i5];
                V[] vArr2 = this.f5415b;
                vArr2[i2] = vArr2[i5];
            }
            int i6 = this.f5416c;
            kArr[i6] = null;
            this.f5415b[i6] = null;
            return;
        }
        throw new IndexOutOfBoundsException(String.valueOf(i2));
    }

    public void clear() {
        K[] kArr = this.f5414a;
        V[] vArr = this.f5415b;
        int i2 = this.f5416c;
        for (int i3 = 0; i3 < i2; i3++) {
            kArr[i3] = null;
            vArr[i3] = null;
        }
        this.f5416c = 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.z0.a.a(java.lang.Class, int):java.lang.Object
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.badlogic.gdx.utils.z0.a.a(java.lang.Object, int):java.lang.Object
      com.badlogic.gdx.utils.z0.a.a(java.lang.Class, int):java.lang.Object */
    /* access modifiers changed from: protected */
    public void d(int i2) {
        K[] kArr = (Object[]) com.badlogic.gdx.utils.z0.a.a((Class) this.f5414a.getClass().getComponentType(), i2);
        System.arraycopy(this.f5414a, 0, kArr, 0, Math.min(this.f5416c, kArr.length));
        this.f5414a = kArr;
        V[] vArr = (Object[]) com.badlogic.gdx.utils.z0.a.a((Class) this.f5415b.getClass().getComponentType(), i2);
        System.arraycopy(this.f5415b, 0, vArr, 0, Math.min(this.f5416c, vArr.length));
        this.f5415b = vArr;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean equals(java.lang.Object r9) {
        /*
            r8 = this;
            r0 = 1
            if (r9 != r8) goto L_0x0004
            return r0
        L_0x0004:
            boolean r1 = r9 instanceof com.badlogic.gdx.utils.b
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            com.badlogic.gdx.utils.b r9 = (com.badlogic.gdx.utils.b) r9
            int r1 = r9.f5416c
            int r3 = r8.f5416c
            if (r1 == r3) goto L_0x0013
            return r2
        L_0x0013:
            K[] r1 = r8.f5414a
            V[] r4 = r8.f5415b
            r5 = 0
        L_0x0018:
            if (r5 >= r3) goto L_0x0037
            r6 = r1[r5]
            r7 = r4[r5]
            if (r7 != 0) goto L_0x0029
            java.lang.Object r7 = com.badlogic.gdx.utils.c0.r
            java.lang.Object r6 = r9.a(r6, r7)
            if (r6 == 0) goto L_0x0034
            return r2
        L_0x0029:
            java.lang.Object r6 = r9.a(r6)
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x0034
            return r2
        L_0x0034:
            int r5 = r5 + 1
            goto L_0x0018
        L_0x0037:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.b.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        K[] kArr = this.f5414a;
        V[] vArr = this.f5415b;
        int i2 = this.f5416c;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            K k2 = kArr[i4];
            V v = vArr[i4];
            if (k2 != null) {
                i3 += k2.hashCode() * 31;
            }
            if (v != null) {
                i3 += v.hashCode();
            }
        }
        return i3;
    }

    public Iterator<c0.b<K, V>> iterator() {
        return a();
    }

    public String toString() {
        if (this.f5416c == 0) {
            return "{}";
        }
        K[] kArr = this.f5414a;
        V[] vArr = this.f5415b;
        r0 r0Var = new r0(32);
        r0Var.append('{');
        r0Var.a((Object) kArr[0]);
        r0Var.append('=');
        r0Var.a((Object) vArr[0]);
        for (int i2 = 1; i2 < this.f5416c; i2++) {
            r0Var.a(", ");
            r0Var.a((Object) kArr[i2]);
            r0Var.append('=');
            r0Var.a((Object) vArr[i2]);
        }
        r0Var.append('}');
        return r0Var.toString();
    }

    public b(boolean z, int i2) {
        this.f5417d = z;
        this.f5414a = new Object[i2];
        this.f5415b = new Object[i2];
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public V a(K r4, V r5) {
        /*
            r3 = this;
            K[] r0 = r3.f5414a
            int r1 = r3.f5416c
            int r1 = r1 + -1
            if (r4 != 0) goto L_0x0016
        L_0x0008:
            if (r1 < 0) goto L_0x0028
            r2 = r0[r1]
            if (r2 != r4) goto L_0x0013
            V[] r4 = r3.f5415b
            r4 = r4[r1]
            return r4
        L_0x0013:
            int r1 = r1 + -1
            goto L_0x0008
        L_0x0016:
            if (r1 < 0) goto L_0x0028
            r2 = r0[r1]
            boolean r2 = r4.equals(r2)
            if (r2 == 0) goto L_0x0025
            V[] r4 = r3.f5415b
            r4 = r4[r1]
            return r4
        L_0x0025:
            int r1 = r1 + -1
            goto L_0x0016
        L_0x0028:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.b.a(java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public b(boolean z, int i2, Class cls, Class cls2) {
        this.f5417d = z;
        this.f5414a = (Object[]) com.badlogic.gdx.utils.z0.a.a(cls, i2);
        this.f5415b = (Object[]) com.badlogic.gdx.utils.z0.a.a(cls2, i2);
    }

    public a<K, V> a() {
        if (h.f5490a) {
            return new a<>(this);
        }
        if (this.f5418e == null) {
            this.f5418e = new a(this);
            this.f5419f = new a(this);
        }
        a<K, V> aVar = this.f5418e;
        if (!aVar.f5423d) {
            aVar.f5422c = 0;
            aVar.f5423d = true;
            this.f5419f.f5423d = false;
            return aVar;
        }
        a<K, V> aVar2 = this.f5419f;
        aVar2.f5422c = 0;
        aVar2.f5423d = true;
        aVar.f5423d = false;
        return aVar2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int b(K r5) {
        /*
            r4 = this;
            K[] r0 = r4.f5414a
            r1 = 0
            if (r5 != 0) goto L_0x0011
            int r2 = r4.f5416c
        L_0x0007:
            if (r1 >= r2) goto L_0x0021
            r3 = r0[r1]
            if (r3 != r5) goto L_0x000e
            return r1
        L_0x000e:
            int r1 = r1 + 1
            goto L_0x0007
        L_0x0011:
            int r2 = r4.f5416c
        L_0x0013:
            if (r1 >= r2) goto L_0x0021
            r3 = r0[r1]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x001e
            return r1
        L_0x001e:
            int r1 = r1 + 1
            goto L_0x0013
        L_0x0021:
            r5 = -1
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.b.b(java.lang.Object):int");
    }

    public b(Class cls, Class cls2) {
        this(false, 16, cls, cls2);
    }
}
