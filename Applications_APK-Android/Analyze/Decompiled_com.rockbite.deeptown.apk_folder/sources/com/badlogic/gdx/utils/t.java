package com.badlogic.gdx.utils;

import com.badlogic.gdx.utils.c0;
import com.badlogic.gdx.utils.d0;
import com.badlogic.gdx.utils.s;
import com.badlogic.gdx.utils.v;
import com.badlogic.gdx.utils.w;
import com.badlogic.gdx.utils.z0.f;
import com.tapjoy.TJAdUnitConstants;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: Json */
public class t {
    private static final boolean debug = false;
    private final c0<Class, Object[]> classToDefaultValues;
    private final c0<Class, d> classToSerializer;
    private final c0<Class, String> classToTag;
    private d defaultSerializer;
    private boolean enumNames;
    private final Object[] equals1;
    private final Object[] equals2;
    private boolean ignoreDeprecated;
    private boolean ignoreUnknownFields;
    private w.c outputType;
    private boolean quoteLongValues;
    private boolean readDeprecated;
    private boolean sortFields;
    private final c0<String, Class> tagToClass;
    private String typeName;
    private final c0<Class, e0<String, a>> typeToFields;
    private boolean usePrototypes;
    private w writer;

    /* compiled from: Json */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        final com.badlogic.gdx.utils.z0.d f5600a;

        /* renamed from: b  reason: collision with root package name */
        Class f5601b;

        /* renamed from: c  reason: collision with root package name */
        boolean f5602c;

        public a(com.badlogic.gdx.utils.z0.d dVar) {
            this.f5600a = dVar;
            this.f5601b = dVar.a((com.badlogic.gdx.utils.z0.b.a(c0.class, dVar.c()) || com.badlogic.gdx.utils.z0.b.a(Map.class, dVar.c())) ? 1 : 0);
            this.f5602c = dVar.a((Class<? extends Annotation>) Deprecated.class);
        }
    }

    /* compiled from: Json */
    public static abstract class b<T> implements d<T> {
        public abstract T read(t tVar, v vVar, Class cls);

        public void write(t tVar, T t, Class cls) {
        }
    }

    /* compiled from: Json */
    public interface c {
        void read(t tVar, v vVar);

        void write(t tVar);
    }

    /* compiled from: Json */
    public interface d<T> {
        T read(t tVar, v vVar, Class cls);

        void write(t tVar, T t, Class cls);
    }

    public t() {
        this.typeName = "class";
        this.usePrototypes = true;
        this.enumNames = true;
        this.typeToFields = new c0<>();
        this.tagToClass = new c0<>();
        this.classToTag = new c0<>();
        this.classToSerializer = new c0<>();
        this.classToDefaultValues = new c0<>();
        this.equals1 = new Object[]{null};
        this.equals2 = new Object[]{null};
        this.outputType = w.c.minimal;
    }

    private String convertToString(Enum enumR) {
        return this.enumNames ? enumR.name() : enumR.toString();
    }

    private Object[] getDefaultValues(Class cls) {
        if (!this.usePrototypes) {
            return null;
        }
        if (this.classToDefaultValues.a(cls)) {
            return this.classToDefaultValues.b(cls);
        }
        try {
            Object newInstance = newInstance(cls);
            e0<String, a> fields = getFields(cls);
            Object[] objArr = new Object[fields.f5447a];
            this.classToDefaultValues.b(cls, objArr);
            int i2 = 0;
            c0.e<a> c2 = fields.c();
            c2.iterator();
            while (c2.hasNext()) {
                a aVar = (a) c2.next();
                if (!this.ignoreDeprecated || !aVar.f5602c) {
                    com.badlogic.gdx.utils.z0.d dVar = aVar.f5600a;
                    int i3 = i2 + 1;
                    try {
                        objArr[i2] = dVar.a(newInstance);
                        i2 = i3;
                    } catch (f e2) {
                        throw new l0("Error accessing field: " + dVar.b() + " (" + cls.getName() + ")", e2);
                    } catch (l0 e3) {
                        e3.a(dVar + " (" + cls.getName() + ")");
                        throw e3;
                    } catch (RuntimeException e4) {
                        l0 l0Var = new l0(e4);
                        l0Var.a(dVar + " (" + cls.getName() + ")");
                        throw l0Var;
                    }
                }
            }
            return objArr;
        } catch (Exception unused) {
            this.classToDefaultValues.b(cls, null);
            return null;
        }
    }

    private e0<String, a> getFields(Class cls) {
        e0<String, a> b2 = this.typeToFields.b(cls);
        if (b2 != null) {
            return b2;
        }
        a aVar = new a();
        for (Class cls2 = cls; cls2 != Object.class; cls2 = cls2.getSuperclass()) {
            aVar.add(cls2);
        }
        ArrayList arrayList = new ArrayList();
        for (int i2 = aVar.f5374b - 1; i2 >= 0; i2--) {
            Collections.addAll(arrayList, com.badlogic.gdx.utils.z0.b.a((Class) aVar.get(i2)));
        }
        e0<String, a> e0Var = new e0<>(arrayList.size());
        int size = arrayList.size();
        for (int i3 = 0; i3 < size; i3++) {
            com.badlogic.gdx.utils.z0.d dVar = (com.badlogic.gdx.utils.z0.d) arrayList.get(i3);
            if (!dVar.g() && !dVar.e() && !dVar.f()) {
                if (!dVar.d()) {
                    try {
                        dVar.a(true);
                    } catch (AccessControlException unused) {
                    }
                }
                e0Var.b(dVar.b(), new a(dVar));
            }
        }
        if (this.sortFields) {
            e0Var.s.d();
        }
        this.typeToFields.b(cls, e0Var);
        return e0Var;
    }

    public void addClassTag(String str, Class cls) {
        this.tagToClass.b(str, cls);
        this.classToTag.b(cls, str);
    }

    public void copyFields(Object obj, Object obj2) {
        e0<String, a> fields = getFields(obj.getClass());
        c0.a<String, a> it = getFields(obj.getClass()).iterator();
        while (it.hasNext()) {
            c0.b bVar = (c0.b) it.next();
            a b2 = fields.b(bVar.f5459a);
            com.badlogic.gdx.utils.z0.d dVar = ((a) bVar.f5460b).f5600a;
            if (b2 != null) {
                try {
                    b2.f5600a.a(obj2, dVar.a(obj));
                } catch (f e2) {
                    throw new l0("Error copying field: " + dVar.b(), e2);
                }
            } else {
                throw new l0("To object is missing field" + ((String) bVar.f5459a));
            }
        }
    }

    public <T> T fromJson(Class<T> cls, Reader reader) {
        return readValue(cls, (Class) null, new u().a(reader));
    }

    public Class getClass(String str) {
        return this.tagToClass.b(str);
    }

    public boolean getIgnoreUnknownFields() {
        return this.ignoreUnknownFields;
    }

    public <T> d<T> getSerializer(Class<T> cls) {
        return this.classToSerializer.b(cls);
    }

    public String getTag(Class cls) {
        return this.classToTag.b(cls);
    }

    public w getWriter() {
        return this.writer;
    }

    /* access modifiers changed from: protected */
    public boolean ignoreUnknownField(Class cls, String str) {
        return false;
    }

    /* access modifiers changed from: protected */
    public Object newInstance(Class cls) {
        try {
            return com.badlogic.gdx.utils.z0.b.f(cls);
        } catch (Exception e2) {
            e = e2;
            try {
                com.badlogic.gdx.utils.z0.c b2 = com.badlogic.gdx.utils.z0.b.b(cls, new Class[0]);
                b2.a(true);
                return b2.a(new Object[0]);
            } catch (SecurityException unused) {
                throw new l0("Error constructing instance of class: " + cls.getName(), e);
            } catch (f unused2) {
                if (com.badlogic.gdx.utils.z0.b.a(Enum.class, cls)) {
                    if (cls.getEnumConstants() == null) {
                        cls = cls.getSuperclass();
                    }
                    return cls.getEnumConstants()[0];
                } else if (cls.isArray()) {
                    throw new l0("Encountered JSON object when expected array of type: " + cls.getName(), e);
                } else if (!com.badlogic.gdx.utils.z0.b.d(cls) || com.badlogic.gdx.utils.z0.b.e(cls)) {
                    throw new l0("Class cannot be created (missing no-arg constructor): " + cls.getName(), e);
                } else {
                    throw new l0("Class cannot be created (non-static member class): " + cls.getName(), e);
                }
            } catch (Exception e3) {
                e = e3;
                throw new l0("Error constructing instance of class: " + cls.getName(), e);
            }
        }
    }

    public String prettyPrint(Object obj) {
        return prettyPrint(obj, 0);
    }

    public void readField(Object obj, String str, v vVar) {
        readField(obj, str, str, (Class) null, vVar);
    }

    public void readFields(Object obj, v vVar) {
        Class<?> cls = obj.getClass();
        e0<String, a> fields = getFields(cls);
        for (v vVar2 = vVar.f5627f; vVar2 != null; vVar2 = vVar2.f5628g) {
            a b2 = fields.b(vVar2.w().replace(RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER, "_"));
            if (b2 == null) {
                if (!vVar2.f5626e.equals(this.typeName) && !this.ignoreUnknownFields && !ignoreUnknownField(cls, vVar2.f5626e)) {
                    l0 l0Var = new l0("Field not found: " + vVar2.f5626e + " (" + cls.getName() + ")");
                    l0Var.a(vVar2.y());
                    throw l0Var;
                }
            } else if (!this.ignoreDeprecated || this.readDeprecated || !b2.f5602c) {
                com.badlogic.gdx.utils.z0.d dVar = b2.f5600a;
                try {
                    dVar.a(obj, readValue(dVar.c(), b2.f5601b, vVar2));
                } catch (f e2) {
                    throw new l0("Error accessing field: " + dVar.b() + " (" + cls.getName() + ")", e2);
                } catch (l0 e3) {
                    e3.a(dVar.b() + " (" + cls.getName() + ")");
                    throw e3;
                } catch (RuntimeException e4) {
                    l0 l0Var2 = new l0(e4);
                    l0Var2.a(vVar2.y());
                    l0Var2.a(dVar.b() + " (" + cls.getName() + ")");
                    throw l0Var2;
                }
            }
        }
    }

    public <T> T readValue(String str, Class<T> cls, v vVar) {
        return readValue(cls, (Class) null, vVar.a(str));
    }

    public void setDefaultSerializer(d dVar) {
        this.defaultSerializer = dVar;
    }

    public void setDeprecated(Class cls, String str, boolean z) {
        a b2 = getFields(cls).b(str);
        if (b2 != null) {
            b2.f5602c = z;
            return;
        }
        throw new l0("Field not found: " + str + " (" + cls.getName() + ")");
    }

    public void setElementType(Class cls, String str, Class cls2) {
        a b2 = getFields(cls).b(str);
        if (b2 != null) {
            b2.f5601b = cls2;
            return;
        }
        throw new l0("Field not found: " + str + " (" + cls.getName() + ")");
    }

    public void setEnumNames(boolean z) {
        this.enumNames = z;
    }

    public void setIgnoreDeprecated(boolean z) {
        this.ignoreDeprecated = z;
    }

    public void setIgnoreUnknownFields(boolean z) {
        this.ignoreUnknownFields = z;
    }

    public void setOutputType(w.c cVar) {
        this.outputType = cVar;
    }

    public void setQuoteLongValues(boolean z) {
        this.quoteLongValues = z;
    }

    public void setReadDeprecated(boolean z) {
        this.readDeprecated = z;
    }

    public <T> void setSerializer(Class<T> cls, d<T> dVar) {
        this.classToSerializer.b(cls, dVar);
    }

    public void setSortFields(boolean z) {
        this.sortFields = z;
    }

    public void setTypeName(String str) {
        this.typeName = str;
    }

    public void setUsePrototypes(boolean z) {
        this.usePrototypes = z;
    }

    public void setWriter(Writer writer2) {
        if (!(writer2 instanceof w)) {
            writer2 = new w(writer2);
        }
        this.writer = (w) writer2;
        this.writer.a(this.outputType);
        this.writer.a(this.quoteLongValues);
    }

    public String toJson(Object obj) {
        return toJson(obj, obj == null ? null : obj.getClass(), (Class) null);
    }

    public void writeArrayEnd() {
        try {
            this.writer.Q();
        } catch (IOException e2) {
            throw new l0(e2);
        }
    }

    public void writeArrayStart(String str) {
        try {
            this.writer.a(str);
            this.writer.d();
        } catch (IOException e2) {
            throw new l0(e2);
        }
    }

    public void writeField(Object obj, String str) {
        writeField(obj, str, str, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006f, code lost:
        if (java.util.Arrays.deepEquals(r12.equals1, r12.equals2) != false) goto L_0x0071;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void writeFields(java.lang.Object r13) {
        /*
            r12 = this;
            java.lang.String r0 = ")"
            java.lang.String r1 = " ("
            java.lang.Class r2 = r13.getClass()
            java.lang.Object[] r3 = r12.getDefaultValues(r2)
            com.badlogic.gdx.utils.e0 r4 = r12.getFields(r2)
            com.badlogic.gdx.utils.e0$c r5 = new com.badlogic.gdx.utils.e0$c
            r5.<init>(r4)
            r5.iterator()
            r4 = 0
            r6 = 0
        L_0x001a:
            boolean r7 = r5.hasNext()
            if (r7 == 0) goto L_0x00f1
            java.lang.Object r7 = r5.next()
            com.badlogic.gdx.utils.t$a r7 = (com.badlogic.gdx.utils.t.a) r7
            boolean r8 = r12.ignoreDeprecated
            if (r8 == 0) goto L_0x002f
            boolean r8 = r7.f5602c
            if (r8 == 0) goto L_0x002f
            goto L_0x001a
        L_0x002f:
            com.badlogic.gdx.utils.z0.d r8 = r7.f5600a
            java.lang.Object r9 = r8.a(r13)     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            if (r3 == 0) goto L_0x0074
            int r10 = r6 + 1
            r6 = r3[r6]     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            if (r9 != 0) goto L_0x0040
            if (r6 != 0) goto L_0x0040
            goto L_0x0071
        L_0x0040:
            if (r9 == 0) goto L_0x0073
            if (r6 == 0) goto L_0x0073
            boolean r11 = r9.equals(r6)     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            if (r11 == 0) goto L_0x004b
            goto L_0x0071
        L_0x004b:
            java.lang.Class r11 = r9.getClass()     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            boolean r11 = r11.isArray()     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            if (r11 == 0) goto L_0x0073
            java.lang.Class r11 = r6.getClass()     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            boolean r11 = r11.isArray()     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            if (r11 == 0) goto L_0x0073
            java.lang.Object[] r11 = r12.equals1     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            r11[r4] = r9     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            java.lang.Object[] r11 = r12.equals2     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            r11[r4] = r6     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            java.lang.Object[] r6 = r12.equals1     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            java.lang.Object[] r11 = r12.equals2     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            boolean r6 = java.util.Arrays.deepEquals(r6, r11)     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            if (r6 == 0) goto L_0x0073
        L_0x0071:
            r6 = r10
            goto L_0x001a
        L_0x0073:
            r6 = r10
        L_0x0074:
            com.badlogic.gdx.utils.w r10 = r12.writer     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            java.lang.String r11 = r8.b()     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            r10.a(r11)     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            java.lang.Class r10 = r8.c()     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            java.lang.Class r7 = r7.f5601b     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            r12.writeValue(r9, r10, r7)     // Catch:{ f -> 0x00c8, l0 -> 0x00aa, Exception -> 0x0087 }
            goto L_0x001a
        L_0x0087:
            r13 = move-exception
            com.badlogic.gdx.utils.l0 r3 = new com.badlogic.gdx.utils.l0
            r3.<init>(r13)
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            r13.append(r8)
            r13.append(r1)
            java.lang.String r1 = r2.getName()
            r13.append(r1)
            r13.append(r0)
            java.lang.String r13 = r13.toString()
            r3.a(r13)
            throw r3
        L_0x00aa:
            r13 = move-exception
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r8)
            r3.append(r1)
            java.lang.String r1 = r2.getName()
            r3.append(r1)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r13.a(r0)
            throw r13
        L_0x00c8:
            r13 = move-exception
            com.badlogic.gdx.utils.l0 r3 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Error accessing field: "
            r4.append(r5)
            java.lang.String r5 = r8.b()
            r4.append(r5)
            r4.append(r1)
            java.lang.String r1 = r2.getName()
            r4.append(r1)
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r3.<init>(r0, r13)
            throw r3
        L_0x00f1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.t.writeFields(java.lang.Object):void");
    }

    public void writeObjectEnd() {
        try {
            this.writer.Q();
        } catch (IOException e2) {
            throw new l0(e2);
        }
    }

    public void writeObjectStart(String str) {
        try {
            this.writer.a(str);
            writeObjectStart();
        } catch (IOException e2) {
            throw new l0(e2);
        }
    }

    public void writeType(Class cls) {
        if (this.typeName != null) {
            String tag = getTag(cls);
            if (tag == null) {
                tag = cls.getName();
            }
            try {
                this.writer.a(this.typeName, tag);
            } catch (IOException e2) {
                throw new l0(e2);
            }
        }
    }

    public void writeValue(String str, Object obj) {
        try {
            this.writer.a(str);
            if (obj == null) {
                writeValue(obj, (Class) null, (Class) null);
            } else {
                writeValue(obj, obj.getClass(), (Class) null);
            }
        } catch (IOException e2) {
            throw new l0(e2);
        }
    }

    private String convertToString(Object obj) {
        if (obj instanceof Enum) {
            return convertToString((Enum) obj);
        }
        if (obj instanceof Class) {
            return ((Class) obj).getName();
        }
        return String.valueOf(obj);
    }

    public <T> T fromJson(Class<T> cls, Class cls2, Reader reader) {
        return readValue(cls, cls2, new u().a(reader));
    }

    public String prettyPrint(String str) {
        return prettyPrint(str, 0);
    }

    public void readField(Object obj, String str, Class cls, v vVar) {
        readField(obj, str, str, cls, vVar);
    }

    public <T> T readValue(String str, Class<T> cls, T t, v vVar) {
        v a2 = vVar.a(str);
        if (a2 == null) {
            return t;
        }
        return readValue(cls, (Class) null, a2);
    }

    public String toJson(Object obj, Class cls) {
        return toJson(obj, cls, (Class) null);
    }

    public void writeField(Object obj, String str, Class cls) {
        writeField(obj, str, str, cls);
    }

    public <T> T fromJson(Class<T> cls, InputStream inputStream) {
        return readValue(cls, (Class) null, new u().a(inputStream));
    }

    public String prettyPrint(Object obj, int i2) {
        return prettyPrint(toJson(obj), i2);
    }

    public void readField(Object obj, String str, String str2, v vVar) {
        readField(obj, str, str2, (Class) null, vVar);
    }

    public String toJson(Object obj, Class cls, Class cls2) {
        StringWriter stringWriter = new StringWriter();
        toJson(obj, cls, cls2, stringWriter);
        return stringWriter.toString();
    }

    public void writeField(Object obj, String str, String str2) {
        writeField(obj, str, str2, null);
    }

    public <T> T fromJson(Class<T> cls, Class cls2, InputStream inputStream) {
        return readValue(cls, cls2, new u().a(inputStream));
    }

    public String prettyPrint(String str, int i2) {
        return new u().a(str).a(this.outputType, i2);
    }

    public void readField(Object obj, String str, String str2, Class cls, v vVar) {
        Class<?> cls2 = obj.getClass();
        a b2 = getFields(cls2).b(str);
        if (b2 != null) {
            com.badlogic.gdx.utils.z0.d dVar = b2.f5600a;
            if (cls == null) {
                cls = b2.f5601b;
            }
            readField(obj, dVar, str2, cls, vVar);
            return;
        }
        throw new l0("Field not found: " + str + " (" + cls2.getName() + ")");
    }

    public <T> T readValue(String str, Class<T> cls, Class cls2, v vVar) {
        return readValue(cls, cls2, vVar.a(str));
    }

    public void writeArrayStart() {
        try {
            this.writer.d();
        } catch (IOException e2) {
            throw new l0(e2);
        }
    }

    public void writeField(Object obj, String str, String str2, Class cls) {
        Class<?> cls2 = obj.getClass();
        a b2 = getFields(cls2).b(str);
        if (b2 != null) {
            com.badlogic.gdx.utils.z0.d dVar = b2.f5600a;
            if (cls == null) {
                cls = b2.f5601b;
            }
            try {
                this.writer.a(str2);
                writeValue(dVar.a(obj), dVar.c(), cls);
            } catch (f e2) {
                throw new l0("Error accessing field: " + dVar.b() + " (" + cls2.getName() + ")", e2);
            } catch (l0 e3) {
                e3.a(dVar + " (" + cls2.getName() + ")");
                throw e3;
            } catch (Exception e4) {
                l0 l0Var = new l0(e4);
                l0Var.a(dVar + " (" + cls2.getName() + ")");
                throw l0Var;
            }
        } else {
            throw new l0("Field not found: " + str + " (" + cls2.getName() + ")");
        }
    }

    public void writeObjectStart(String str, Class cls, Class cls2) {
        try {
            this.writer.a(str);
            writeObjectStart(cls, cls2);
        } catch (IOException e2) {
            throw new l0(e2);
        }
    }

    public <T> T fromJson(Class<T> cls, e.d.b.s.a aVar) {
        try {
            return readValue(cls, (Class) null, new u().a(aVar));
        } catch (Exception e2) {
            throw new l0("Error reading file: " + aVar, e2);
        }
    }

    public String prettyPrint(Object obj, v.c cVar) {
        return prettyPrint(toJson(obj), cVar);
    }

    public <T> T readValue(String str, Class<T> cls, Class cls2, T t, v vVar) {
        return readValue(cls, cls2, t, vVar.a(str));
    }

    public void writeValue(String str, Object obj, Class cls) {
        try {
            this.writer.a(str);
            writeValue(obj, cls, (Class) null);
        } catch (IOException e2) {
            throw new l0(e2);
        }
    }

    public String prettyPrint(String str, v.c cVar) {
        return new u().a(str).a(cVar);
    }

    public void toJson(Object obj, e.d.b.s.a aVar) {
        toJson(obj, obj == null ? null : obj.getClass(), (Class) null, aVar);
    }

    public <T> T fromJson(Class<T> cls, Class cls2, e.d.b.s.a aVar) {
        try {
            return readValue(cls, cls2, new u().a(aVar));
        } catch (Exception e2) {
            throw new l0("Error reading file: " + aVar, e2);
        }
    }

    public <T> T readValue(Class<T> cls, Class cls2, T t, v vVar) {
        return vVar == null ? t : readValue(cls, cls2, vVar);
    }

    public void toJson(Object obj, Class cls, e.d.b.s.a aVar) {
        toJson(obj, cls, (Class) null, aVar);
    }

    public void writeObjectStart() {
        try {
            this.writer.P();
        } catch (IOException e2) {
            throw new l0(e2);
        }
    }

    public <T> T readValue(Class<T> cls, v vVar) {
        return readValue(cls, (Class) null, vVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.s.a.a(boolean, java.lang.String):java.io.Writer
     arg types: [int, java.lang.String]
     candidates:
      e.d.b.s.a.a(java.lang.String, boolean):void
      e.d.b.s.a.a(boolean, java.lang.String):java.io.Writer */
    public void toJson(Object obj, Class cls, Class cls2, e.d.b.s.a aVar) {
        Writer writer2 = null;
        try {
            writer2 = aVar.a(false, "UTF-8");
            toJson(obj, cls, cls2, writer2);
            q0.a(writer2);
        } catch (Exception e2) {
            throw new l0("Error writing file: " + aVar, e2);
        } catch (Throwable th) {
            q0.a(writer2);
            throw th;
        }
    }

    public void writeValue(String str, Object obj, Class cls, Class cls2) {
        try {
            this.writer.a(str);
            writeValue(obj, cls, cls2);
        } catch (IOException e2) {
            throw new l0(e2);
        }
    }

    public <T> T fromJson(Class<T> cls, char[] cArr, int i2, int i3) {
        return readValue(cls, (Class) null, new u().a(cArr, i2, i3));
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r10v0, types: [java.lang.Class] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.z0.b.a(java.lang.Class, java.lang.Class):boolean
     arg types: [java.lang.Class, java.lang.Class<T>]
     candidates:
      com.badlogic.gdx.utils.z0.b.a(java.lang.Class, java.lang.Class[]):com.badlogic.gdx.utils.z0.c
      com.badlogic.gdx.utils.z0.b.a(java.lang.Class, java.lang.Object):boolean
      com.badlogic.gdx.utils.z0.b.a(java.lang.Class, java.lang.Class):boolean */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x02ec  */
    /* JADX WARNING: Removed duplicated region for block: B:291:0x03b5 A[RETURN] */
    public <T> T readValue(java.lang.Class<T> r9, java.lang.Class r10, com.badlogic.gdx.utils.v r11) {
        /*
            r8 = this;
            r0 = 0
            if (r11 != 0) goto L_0x0004
            return r0
        L_0x0004:
            boolean r1 = r11.t()
            java.lang.String r2 = ")"
            java.lang.String r3 = " ("
            if (r1 == 0) goto L_0x0149
            java.lang.String r1 = r8.typeName
            if (r1 != 0) goto L_0x0014
            r1 = r0
            goto L_0x0018
        L_0x0014:
            java.lang.String r1 = r11.a(r1, r0)
        L_0x0018:
            if (r1 == 0) goto L_0x002c
            java.lang.Class r9 = r8.getClass(r1)
            if (r9 != 0) goto L_0x002c
            java.lang.Class r9 = com.badlogic.gdx.utils.z0.b.a(r1)     // Catch:{ f -> 0x0025 }
            goto L_0x002c
        L_0x0025:
            r9 = move-exception
            com.badlogic.gdx.utils.l0 r10 = new com.badlogic.gdx.utils.l0
            r10.<init>(r9)
            throw r10
        L_0x002c:
            if (r9 != 0) goto L_0x0038
            com.badlogic.gdx.utils.t$d r10 = r8.defaultSerializer
            if (r10 == 0) goto L_0x0037
            java.lang.Object r9 = r10.read(r8, r11, r9)
            return r9
        L_0x0037:
            return r11
        L_0x0038:
            java.lang.String r1 = r8.typeName
            if (r1 == 0) goto L_0x0072
            java.lang.Class<java.util.Collection> r1 = java.util.Collection.class
            boolean r1 = com.badlogic.gdx.utils.z0.b.a(r1, r9)
            if (r1 == 0) goto L_0x0072
            java.lang.String r1 = "items"
            com.badlogic.gdx.utils.v r11 = r11.a(r1)
            if (r11 == 0) goto L_0x004e
            goto L_0x0149
        L_0x004e:
            com.badlogic.gdx.utils.l0 r10 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unable to convert object to collection: "
            r0.append(r1)
            r0.append(r11)
            r0.append(r3)
            java.lang.String r9 = r9.getName()
            r0.append(r9)
            r0.append(r2)
            java.lang.String r9 = r0.toString()
            r10.<init>(r9)
            throw r10
        L_0x0072:
            com.badlogic.gdx.utils.c0<java.lang.Class, com.badlogic.gdx.utils.t$d> r1 = r8.classToSerializer
            java.lang.Object r1 = r1.b(r9)
            com.badlogic.gdx.utils.t$d r1 = (com.badlogic.gdx.utils.t.d) r1
            if (r1 == 0) goto L_0x0081
            java.lang.Object r9 = r1.read(r8, r11, r9)
            return r9
        L_0x0081:
            java.lang.Class<java.lang.String> r1 = java.lang.String.class
            if (r9 == r1) goto L_0x0142
            java.lang.Class<java.lang.Integer> r1 = java.lang.Integer.class
            if (r9 == r1) goto L_0x0142
            java.lang.Class<java.lang.Boolean> r1 = java.lang.Boolean.class
            if (r9 == r1) goto L_0x0142
            java.lang.Class<java.lang.Float> r1 = java.lang.Float.class
            if (r9 == r1) goto L_0x0142
            java.lang.Class<java.lang.Long> r1 = java.lang.Long.class
            if (r9 == r1) goto L_0x0142
            java.lang.Class<java.lang.Double> r1 = java.lang.Double.class
            if (r9 == r1) goto L_0x0142
            java.lang.Class<java.lang.Short> r1 = java.lang.Short.class
            if (r9 == r1) goto L_0x0142
            java.lang.Class<java.lang.Byte> r1 = java.lang.Byte.class
            if (r9 == r1) goto L_0x0142
            java.lang.Class<java.lang.Character> r1 = java.lang.Character.class
            if (r9 == r1) goto L_0x0142
            java.lang.Class<java.lang.Enum> r1 = java.lang.Enum.class
            boolean r1 = com.badlogic.gdx.utils.z0.b.a(r1, r9)
            if (r1 == 0) goto L_0x00af
            goto L_0x0142
        L_0x00af:
            java.lang.Object r9 = r8.newInstance(r9)
            boolean r1 = r9 instanceof com.badlogic.gdx.utils.t.c
            if (r1 == 0) goto L_0x00be
            r10 = r9
            com.badlogic.gdx.utils.t$c r10 = (com.badlogic.gdx.utils.t.c) r10
            r10.read(r8, r11)
            return r9
        L_0x00be:
            boolean r1 = r9 instanceof com.badlogic.gdx.utils.c0
            if (r1 == 0) goto L_0x00d5
            com.badlogic.gdx.utils.c0 r9 = (com.badlogic.gdx.utils.c0) r9
            com.badlogic.gdx.utils.v r11 = r11.f5627f
        L_0x00c6:
            if (r11 == 0) goto L_0x00d4
            java.lang.String r1 = r11.f5626e
            java.lang.Object r2 = r8.readValue(r10, r0, r11)
            r9.b(r1, r2)
            com.badlogic.gdx.utils.v r11 = r11.f5628g
            goto L_0x00c6
        L_0x00d4:
            return r9
        L_0x00d5:
            boolean r1 = r9 instanceof com.badlogic.gdx.utils.d0
            java.lang.String r2 = "values"
            if (r1 == 0) goto L_0x00ee
            com.badlogic.gdx.utils.d0 r9 = (com.badlogic.gdx.utils.d0) r9
            com.badlogic.gdx.utils.v r11 = r11.c(r2)
        L_0x00e1:
            if (r11 == 0) goto L_0x00ed
            java.lang.Object r1 = r8.readValue(r10, r0, r11)
            r9.add(r1)
            com.badlogic.gdx.utils.v r11 = r11.f5628g
            goto L_0x00e1
        L_0x00ed:
            return r9
        L_0x00ee:
            boolean r1 = r9 instanceof com.badlogic.gdx.utils.s
            if (r1 == 0) goto L_0x0105
            com.badlogic.gdx.utils.s r9 = (com.badlogic.gdx.utils.s) r9
            com.badlogic.gdx.utils.v r10 = r11.c(r2)
        L_0x00f8:
            if (r10 == 0) goto L_0x0104
            int r11 = r10.f()
            r9.a(r11)
            com.badlogic.gdx.utils.v r10 = r10.f5628g
            goto L_0x00f8
        L_0x0104:
            return r9
        L_0x0105:
            boolean r1 = r9 instanceof com.badlogic.gdx.utils.b
            if (r1 == 0) goto L_0x011c
            com.badlogic.gdx.utils.b r9 = (com.badlogic.gdx.utils.b) r9
            com.badlogic.gdx.utils.v r11 = r11.f5627f
        L_0x010d:
            if (r11 == 0) goto L_0x011b
            java.lang.String r1 = r11.f5626e
            java.lang.Object r2 = r8.readValue(r10, r0, r11)
            r9.b(r1, r2)
            com.badlogic.gdx.utils.v r11 = r11.f5628g
            goto L_0x010d
        L_0x011b:
            return r9
        L_0x011c:
            boolean r1 = r9 instanceof java.util.Map
            if (r1 == 0) goto L_0x013e
            java.util.Map r9 = (java.util.Map) r9
            com.badlogic.gdx.utils.v r11 = r11.f5627f
        L_0x0124:
            if (r11 == 0) goto L_0x013d
            java.lang.String r1 = r11.f5626e
            java.lang.String r2 = r8.typeName
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0131
            goto L_0x013a
        L_0x0131:
            java.lang.String r1 = r11.f5626e
            java.lang.Object r2 = r8.readValue(r10, r0, r11)
            r9.put(r1, r2)
        L_0x013a:
            com.badlogic.gdx.utils.v r11 = r11.f5628g
            goto L_0x0124
        L_0x013d:
            return r9
        L_0x013e:
            r8.readFields(r9, r11)
            return r9
        L_0x0142:
            java.lang.String r10 = "value"
            java.lang.Object r9 = r8.readValue(r10, r9, r11)
            return r9
        L_0x0149:
            if (r9 == 0) goto L_0x016d
            com.badlogic.gdx.utils.c0<java.lang.Class, com.badlogic.gdx.utils.t$d> r1 = r8.classToSerializer
            java.lang.Object r1 = r1.b(r9)
            com.badlogic.gdx.utils.t$d r1 = (com.badlogic.gdx.utils.t.d) r1
            if (r1 == 0) goto L_0x015a
            java.lang.Object r9 = r1.read(r8, r11, r9)
            return r9
        L_0x015a:
            java.lang.Class<com.badlogic.gdx.utils.t$c> r1 = com.badlogic.gdx.utils.t.c.class
            boolean r1 = com.badlogic.gdx.utils.z0.b.a(r1, r9)
            if (r1 == 0) goto L_0x016d
            java.lang.Object r9 = r8.newInstance(r9)
            r10 = r9
            com.badlogic.gdx.utils.t$c r10 = (com.badlogic.gdx.utils.t.c) r10
            r10.read(r8, r11)
            return r9
        L_0x016d:
            boolean r1 = r11.n()
            java.lang.String r4 = "Unable to convert value to required type: "
            r5 = 0
            if (r1 == 0) goto L_0x023c
            if (r9 == 0) goto L_0x017c
            java.lang.Class<java.lang.Object> r1 = java.lang.Object.class
            if (r9 != r1) goto L_0x017e
        L_0x017c:
            java.lang.Class<com.badlogic.gdx.utils.a> r9 = com.badlogic.gdx.utils.a.class
        L_0x017e:
            java.lang.Class<com.badlogic.gdx.utils.a> r1 = com.badlogic.gdx.utils.a.class
            boolean r1 = com.badlogic.gdx.utils.z0.b.a(r1, r9)
            if (r1 == 0) goto L_0x01a5
            java.lang.Class<com.badlogic.gdx.utils.a> r1 = com.badlogic.gdx.utils.a.class
            if (r9 != r1) goto L_0x0190
            com.badlogic.gdx.utils.a r9 = new com.badlogic.gdx.utils.a
            r9.<init>()
            goto L_0x0196
        L_0x0190:
            java.lang.Object r9 = r8.newInstance(r9)
            com.badlogic.gdx.utils.a r9 = (com.badlogic.gdx.utils.a) r9
        L_0x0196:
            com.badlogic.gdx.utils.v r11 = r11.f5627f
        L_0x0198:
            if (r11 == 0) goto L_0x01a4
            java.lang.Object r1 = r8.readValue(r10, r0, r11)
            r9.add(r1)
            com.badlogic.gdx.utils.v r11 = r11.f5628g
            goto L_0x0198
        L_0x01a4:
            return r9
        L_0x01a5:
            java.lang.Class<com.badlogic.gdx.utils.i0> r1 = com.badlogic.gdx.utils.i0.class
            boolean r1 = com.badlogic.gdx.utils.z0.b.a(r1, r9)
            if (r1 == 0) goto L_0x01cc
            java.lang.Class<com.badlogic.gdx.utils.i0> r1 = com.badlogic.gdx.utils.i0.class
            if (r9 != r1) goto L_0x01b7
            com.badlogic.gdx.utils.i0 r9 = new com.badlogic.gdx.utils.i0
            r9.<init>()
            goto L_0x01bd
        L_0x01b7:
            java.lang.Object r9 = r8.newInstance(r9)
            com.badlogic.gdx.utils.i0 r9 = (com.badlogic.gdx.utils.i0) r9
        L_0x01bd:
            com.badlogic.gdx.utils.v r11 = r11.f5627f
        L_0x01bf:
            if (r11 == 0) goto L_0x01cb
            java.lang.Object r1 = r8.readValue(r10, r0, r11)
            r9.addLast(r1)
            com.badlogic.gdx.utils.v r11 = r11.f5628g
            goto L_0x01bf
        L_0x01cb:
            return r9
        L_0x01cc:
            java.lang.Class<java.util.Collection> r1 = java.util.Collection.class
            boolean r1 = com.badlogic.gdx.utils.z0.b.a(r1, r9)
            if (r1 == 0) goto L_0x01f5
            boolean r1 = r9.isInterface()
            if (r1 == 0) goto L_0x01e0
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            goto L_0x01e6
        L_0x01e0:
            java.lang.Object r9 = r8.newInstance(r9)
            java.util.Collection r9 = (java.util.Collection) r9
        L_0x01e6:
            com.badlogic.gdx.utils.v r11 = r11.f5627f
        L_0x01e8:
            if (r11 == 0) goto L_0x01f4
            java.lang.Object r1 = r8.readValue(r10, r0, r11)
            r9.add(r1)
            com.badlogic.gdx.utils.v r11 = r11.f5628g
            goto L_0x01e8
        L_0x01f4:
            return r9
        L_0x01f5:
            boolean r1 = r9.isArray()
            if (r1 == 0) goto L_0x021a
            java.lang.Class r9 = r9.getComponentType()
            if (r10 != 0) goto L_0x0202
            r10 = r9
        L_0x0202:
            int r1 = r11.f5631j
            java.lang.Object r9 = com.badlogic.gdx.utils.z0.a.a(r9, r1)
            com.badlogic.gdx.utils.v r11 = r11.f5627f
        L_0x020a:
            if (r11 == 0) goto L_0x0219
            int r1 = r5 + 1
            java.lang.Object r2 = r8.readValue(r10, r0, r11)
            com.badlogic.gdx.utils.z0.a.a(r9, r5, r2)
            com.badlogic.gdx.utils.v r11 = r11.f5628g
            r5 = r1
            goto L_0x020a
        L_0x0219:
            return r9
        L_0x021a:
            com.badlogic.gdx.utils.l0 r10 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r4)
            r0.append(r11)
            r0.append(r3)
            java.lang.String r9 = r9.getName()
            r0.append(r9)
            r0.append(r2)
            java.lang.String r9 = r0.toString()
            r10.<init>(r9)
            throw r10
        L_0x023c:
            boolean r10 = r11.s()
            if (r10 == 0) goto L_0x02c2
            if (r9 == 0) goto L_0x02af
            java.lang.Class r10 = java.lang.Float.TYPE     // Catch:{ NumberFormatException -> 0x02b8 }
            if (r9 == r10) goto L_0x02af
            java.lang.Class<java.lang.Float> r10 = java.lang.Float.class
            if (r9 != r10) goto L_0x024d
            goto L_0x02af
        L_0x024d:
            java.lang.Class r10 = java.lang.Integer.TYPE     // Catch:{ NumberFormatException -> 0x02b8 }
            if (r9 == r10) goto L_0x02a6
            java.lang.Class<java.lang.Integer> r10 = java.lang.Integer.class
            if (r9 != r10) goto L_0x0256
            goto L_0x02a6
        L_0x0256:
            java.lang.Class r10 = java.lang.Long.TYPE     // Catch:{ NumberFormatException -> 0x02b8 }
            if (r9 == r10) goto L_0x029d
            java.lang.Class<java.lang.Long> r10 = java.lang.Long.class
            if (r9 != r10) goto L_0x025f
            goto L_0x029d
        L_0x025f:
            java.lang.Class r10 = java.lang.Double.TYPE     // Catch:{ NumberFormatException -> 0x02b8 }
            if (r9 == r10) goto L_0x0294
            java.lang.Class<java.lang.Double> r10 = java.lang.Double.class
            if (r9 != r10) goto L_0x0268
            goto L_0x0294
        L_0x0268:
            java.lang.Class<java.lang.String> r10 = java.lang.String.class
            if (r9 != r10) goto L_0x0271
            java.lang.String r9 = r11.k()     // Catch:{ NumberFormatException -> 0x02b8 }
            return r9
        L_0x0271:
            java.lang.Class r10 = java.lang.Short.TYPE     // Catch:{ NumberFormatException -> 0x02b8 }
            if (r9 == r10) goto L_0x028b
            java.lang.Class<java.lang.Short> r10 = java.lang.Short.class
            if (r9 != r10) goto L_0x027a
            goto L_0x028b
        L_0x027a:
            java.lang.Class r10 = java.lang.Byte.TYPE     // Catch:{ NumberFormatException -> 0x02b8 }
            if (r9 == r10) goto L_0x0282
            java.lang.Class<java.lang.Byte> r10 = java.lang.Byte.class
            if (r9 != r10) goto L_0x02b8
        L_0x0282:
            byte r10 = r11.b()     // Catch:{ NumberFormatException -> 0x02b8 }
            java.lang.Byte r9 = java.lang.Byte.valueOf(r10)     // Catch:{ NumberFormatException -> 0x02b8 }
            return r9
        L_0x028b:
            short r10 = r11.i()     // Catch:{ NumberFormatException -> 0x02b8 }
            java.lang.Short r9 = java.lang.Short.valueOf(r10)     // Catch:{ NumberFormatException -> 0x02b8 }
            return r9
        L_0x0294:
            double r6 = r11.c()     // Catch:{ NumberFormatException -> 0x02b8 }
            java.lang.Double r9 = java.lang.Double.valueOf(r6)     // Catch:{ NumberFormatException -> 0x02b8 }
            return r9
        L_0x029d:
            long r6 = r11.h()     // Catch:{ NumberFormatException -> 0x02b8 }
            java.lang.Long r9 = java.lang.Long.valueOf(r6)     // Catch:{ NumberFormatException -> 0x02b8 }
            return r9
        L_0x02a6:
            int r10 = r11.f()     // Catch:{ NumberFormatException -> 0x02b8 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r10)     // Catch:{ NumberFormatException -> 0x02b8 }
            return r9
        L_0x02af:
            float r10 = r11.d()     // Catch:{ NumberFormatException -> 0x02b8 }
            java.lang.Float r9 = java.lang.Float.valueOf(r10)     // Catch:{ NumberFormatException -> 0x02b8 }
            return r9
        L_0x02b8:
            com.badlogic.gdx.utils.v r10 = new com.badlogic.gdx.utils.v
            java.lang.String r11 = r11.k()
            r10.<init>(r11)
            goto L_0x02c3
        L_0x02c2:
            r10 = r11
        L_0x02c3:
            boolean r11 = r10.o()
            if (r11 == 0) goto L_0x02e6
            if (r9 == 0) goto L_0x02d3
            java.lang.Class r11 = java.lang.Boolean.TYPE     // Catch:{ NumberFormatException -> 0x02dc }
            if (r9 == r11) goto L_0x02d3
            java.lang.Class<java.lang.Boolean> r11 = java.lang.Boolean.class
            if (r9 != r11) goto L_0x02dc
        L_0x02d3:
            boolean r11 = r10.a()     // Catch:{ NumberFormatException -> 0x02dc }
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r11)     // Catch:{ NumberFormatException -> 0x02dc }
            return r9
        L_0x02dc:
            com.badlogic.gdx.utils.v r11 = new com.badlogic.gdx.utils.v
            java.lang.String r10 = r10.k()
            r11.<init>(r10)
            r10 = r11
        L_0x02e6:
            boolean r11 = r10.u()
            if (r11 == 0) goto L_0x03b5
            java.lang.String r11 = r10.k()
            if (r9 == 0) goto L_0x03b4
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            if (r9 != r0) goto L_0x02f8
            goto L_0x03b4
        L_0x02f8:
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch:{ NumberFormatException -> 0x034b }
            if (r9 == r0) goto L_0x0346
            java.lang.Class<java.lang.Integer> r0 = java.lang.Integer.class
            if (r9 != r0) goto L_0x0301
            goto L_0x0346
        L_0x0301:
            java.lang.Class r0 = java.lang.Float.TYPE     // Catch:{ NumberFormatException -> 0x034b }
            if (r9 == r0) goto L_0x0341
            java.lang.Class<java.lang.Float> r0 = java.lang.Float.class
            if (r9 != r0) goto L_0x030a
            goto L_0x0341
        L_0x030a:
            java.lang.Class r0 = java.lang.Long.TYPE     // Catch:{ NumberFormatException -> 0x034b }
            if (r9 == r0) goto L_0x033c
            java.lang.Class<java.lang.Long> r0 = java.lang.Long.class
            if (r9 != r0) goto L_0x0313
            goto L_0x033c
        L_0x0313:
            java.lang.Class r0 = java.lang.Double.TYPE     // Catch:{ NumberFormatException -> 0x034b }
            if (r9 == r0) goto L_0x0337
            java.lang.Class<java.lang.Double> r0 = java.lang.Double.class
            if (r9 != r0) goto L_0x031c
            goto L_0x0337
        L_0x031c:
            java.lang.Class r0 = java.lang.Short.TYPE     // Catch:{ NumberFormatException -> 0x034b }
            if (r9 == r0) goto L_0x0332
            java.lang.Class<java.lang.Short> r0 = java.lang.Short.class
            if (r9 != r0) goto L_0x0325
            goto L_0x0332
        L_0x0325:
            java.lang.Class r0 = java.lang.Byte.TYPE     // Catch:{ NumberFormatException -> 0x034b }
            if (r9 == r0) goto L_0x032d
            java.lang.Class<java.lang.Byte> r0 = java.lang.Byte.class
            if (r9 != r0) goto L_0x034c
        L_0x032d:
            java.lang.Byte r9 = java.lang.Byte.valueOf(r11)     // Catch:{ NumberFormatException -> 0x034b }
            return r9
        L_0x0332:
            java.lang.Short r9 = java.lang.Short.valueOf(r11)     // Catch:{ NumberFormatException -> 0x034b }
            return r9
        L_0x0337:
            java.lang.Double r9 = java.lang.Double.valueOf(r11)     // Catch:{ NumberFormatException -> 0x034b }
            return r9
        L_0x033c:
            java.lang.Long r9 = java.lang.Long.valueOf(r11)     // Catch:{ NumberFormatException -> 0x034b }
            return r9
        L_0x0341:
            java.lang.Float r9 = java.lang.Float.valueOf(r11)     // Catch:{ NumberFormatException -> 0x034b }
            return r9
        L_0x0346:
            java.lang.Integer r9 = java.lang.Integer.valueOf(r11)     // Catch:{ NumberFormatException -> 0x034b }
            return r9
        L_0x034b:
        L_0x034c:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            if (r9 == r0) goto L_0x03af
            java.lang.Class<java.lang.Boolean> r0 = java.lang.Boolean.class
            if (r9 != r0) goto L_0x0355
            goto L_0x03af
        L_0x0355:
            java.lang.Class r0 = java.lang.Character.TYPE
            if (r9 == r0) goto L_0x03a6
            java.lang.Class<java.lang.Character> r0 = java.lang.Character.class
            if (r9 != r0) goto L_0x035e
            goto L_0x03a6
        L_0x035e:
            java.lang.Class<java.lang.Enum> r0 = java.lang.Enum.class
            boolean r0 = com.badlogic.gdx.utils.z0.b.a(r0, r9)
            if (r0 == 0) goto L_0x037f
            java.lang.Object[] r0 = r9.getEnumConstants()
            java.lang.Enum[] r0 = (java.lang.Enum[]) r0
            int r1 = r0.length
        L_0x036d:
            if (r5 >= r1) goto L_0x037f
            r6 = r0[r5]
            java.lang.String r7 = r8.convertToString(r6)
            boolean r7 = r11.equals(r7)
            if (r7 == 0) goto L_0x037c
            return r6
        L_0x037c:
            int r5 = r5 + 1
            goto L_0x036d
        L_0x037f:
            java.lang.Class<java.lang.CharSequence> r0 = java.lang.CharSequence.class
            if (r9 != r0) goto L_0x0384
            return r11
        L_0x0384:
            com.badlogic.gdx.utils.l0 r11 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r4)
            r0.append(r10)
            r0.append(r3)
            java.lang.String r9 = r9.getName()
            r0.append(r9)
            r0.append(r2)
            java.lang.String r9 = r0.toString()
            r11.<init>(r9)
            throw r11
        L_0x03a6:
            char r9 = r11.charAt(r5)
            java.lang.Character r9 = java.lang.Character.valueOf(r9)
            return r9
        L_0x03af:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r11)
            return r9
        L_0x03b4:
            return r11
        L_0x03b5:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.t.readValue(java.lang.Class, java.lang.Class, com.badlogic.gdx.utils.v):java.lang.Object");
    }

    public void writeObjectStart(Class cls, Class cls2) {
        try {
            this.writer.P();
            if (cls2 == null || cls2 != cls) {
                writeType(cls);
            }
        } catch (IOException e2) {
            throw new l0(e2);
        }
    }

    public <T> T fromJson(Class<T> cls, Class cls2, char[] cArr, int i2, int i3) {
        return readValue(cls, cls2, new u().a(cArr, i2, i3));
    }

    public <T> T fromJson(Class<T> cls, String str) {
        return readValue(cls, (Class) null, new u().a(str));
    }

    public void readField(Object obj, com.badlogic.gdx.utils.z0.d dVar, String str, Class cls, v vVar) {
        v a2 = vVar.a(str);
        if (a2 != null) {
            try {
                dVar.a(obj, readValue(dVar.c(), cls, a2));
            } catch (f e2) {
                throw new l0("Error accessing field: " + dVar.b() + " (" + dVar.a().getName() + ")", e2);
            } catch (l0 e3) {
                e3.a(dVar.b() + " (" + dVar.a().getName() + ")");
                throw e3;
            } catch (RuntimeException e4) {
                l0 l0Var = new l0(e4);
                l0Var.a(a2.y());
                l0Var.a(dVar.b() + " (" + dVar.a().getName() + ")");
                throw l0Var;
            }
        }
    }

    public void writeValue(Object obj) {
        if (obj == null) {
            writeValue(obj, (Class) null, (Class) null);
        } else {
            writeValue(obj, obj.getClass(), (Class) null);
        }
    }

    public t(w.c cVar) {
        this.typeName = "class";
        this.usePrototypes = true;
        this.enumNames = true;
        this.typeToFields = new c0<>();
        this.tagToClass = new c0<>();
        this.classToTag = new c0<>();
        this.classToSerializer = new c0<>();
        this.classToDefaultValues = new c0<>();
        this.equals1 = new Object[]{null};
        this.equals2 = new Object[]{null};
        this.outputType = cVar;
    }

    public <T> T fromJson(Class<T> cls, Class cls2, String str) {
        return readValue(cls, cls2, new u().a(str));
    }

    public void toJson(Object obj, Writer writer2) {
        toJson(obj, obj == null ? null : obj.getClass(), (Class) null, writer2);
    }

    public void writeValue(Object obj, Class cls) {
        writeValue(obj, cls, (Class) null);
    }

    public void toJson(Object obj, Class cls, Writer writer2) {
        toJson(obj, cls, (Class) null, writer2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.t.writeValue(java.lang.Object, java.lang.Class, java.lang.Class):void
     arg types: [V, java.lang.Class, ?[OBJECT, ARRAY]]
     candidates:
      com.badlogic.gdx.utils.t.writeValue(java.lang.String, java.lang.Object, java.lang.Class):void
      com.badlogic.gdx.utils.t.writeValue(java.lang.Object, java.lang.Class, java.lang.Class):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.z0.b.a(java.lang.Class, java.lang.Class):boolean
     arg types: [java.lang.Class, java.lang.Class<?>]
     candidates:
      com.badlogic.gdx.utils.z0.b.a(java.lang.Class, java.lang.Class[]):com.badlogic.gdx.utils.z0.c
      com.badlogic.gdx.utils.z0.b.a(java.lang.Class, java.lang.Object):boolean
      com.badlogic.gdx.utils.z0.b.a(java.lang.Class, java.lang.Class):boolean */
    public void writeValue(Object obj, Class cls, Class cls2) {
        if (obj == null) {
            try {
                this.writer.a((Object) null);
            } catch (IOException e2) {
                throw new l0(e2);
            }
        } else {
            if (!((cls != null && cls.isPrimitive()) || cls == String.class || cls == Integer.class || cls == Boolean.class || cls == Float.class || cls == Long.class || cls == Double.class || cls == Short.class || cls == Byte.class)) {
                if (cls != Character.class) {
                    Class<?> cls3 = obj.getClass();
                    if (cls3.isPrimitive() || cls3 == String.class || cls3 == Integer.class || cls3 == Boolean.class || cls3 == Float.class || cls3 == Long.class || cls3 == Double.class || cls3 == Short.class || cls3 == Byte.class || cls3 == Character.class) {
                        writeObjectStart(cls3, null);
                        writeValue("value", obj);
                        writeObjectEnd();
                        return;
                    } else if (obj instanceof c) {
                        writeObjectStart(cls3, cls);
                        ((c) obj).write(this);
                        writeObjectEnd();
                        return;
                    } else {
                        d b2 = this.classToSerializer.b(cls3);
                        if (b2 != null) {
                            b2.write(this, obj, cls);
                            return;
                        }
                        int i2 = 0;
                        if (obj instanceof a) {
                            if (cls == null || cls3 == cls || cls3 == a.class) {
                                writeArrayStart();
                                a aVar = (a) obj;
                                int i3 = aVar.f5374b;
                                while (i2 < i3) {
                                    writeValue(aVar.get(i2), cls2, (Class) null);
                                    i2++;
                                }
                                writeArrayEnd();
                                return;
                            }
                            throw new l0("Serialization of an Array other than the known type is not supported.\nKnown type: " + cls + "\nActual type: " + cls3);
                        } else if (obj instanceof i0) {
                            if (!(cls == null || cls3 == cls)) {
                                if (cls3 != i0.class) {
                                    throw new l0("Serialization of a Queue other than the known type is not supported.\nKnown type: " + cls + "\nActual type: " + cls3);
                                }
                            }
                            writeArrayStart();
                            i0 i0Var = (i0) obj;
                            int i4 = i0Var.f5501d;
                            while (i2 < i4) {
                                writeValue(i0Var.get(i2), cls2, (Class) null);
                                i2++;
                            }
                            writeArrayEnd();
                            return;
                        } else if (obj instanceof Collection) {
                            if (this.typeName == null || cls3 == ArrayList.class || (cls != null && cls == cls3)) {
                                writeArrayStart();
                                for (Object writeValue : (Collection) obj) {
                                    writeValue(writeValue, cls2, (Class) null);
                                }
                                writeArrayEnd();
                                return;
                            }
                            writeObjectStart(cls3, cls);
                            writeArrayStart("items");
                            for (Object writeValue2 : (Collection) obj) {
                                writeValue(writeValue2, cls2, (Class) null);
                            }
                            writeArrayEnd();
                            writeObjectEnd();
                            return;
                        } else if (cls3.isArray()) {
                            if (cls2 == null) {
                                cls2 = cls3.getComponentType();
                            }
                            int a2 = com.badlogic.gdx.utils.z0.a.a(obj);
                            writeArrayStart();
                            while (i2 < a2) {
                                writeValue(com.badlogic.gdx.utils.z0.a.a(obj, i2), cls2, (Class) null);
                                i2++;
                            }
                            writeArrayEnd();
                            return;
                        } else if (obj instanceof c0) {
                            if (cls == null) {
                                cls = c0.class;
                            }
                            writeObjectStart(cls3, cls);
                            c0.a a3 = ((c0) obj).a();
                            a3.iterator();
                            while (a3.hasNext()) {
                                c0.b bVar = (c0.b) a3.next();
                                this.writer.a(convertToString((Object) bVar.f5459a));
                                writeValue((Object) bVar.f5460b, cls2, (Class) null);
                            }
                            writeObjectEnd();
                            return;
                        } else if (obj instanceof d0) {
                            if (cls == null) {
                                cls = d0.class;
                            }
                            writeObjectStart(cls3, cls);
                            this.writer.a(TJAdUnitConstants.String.USAGE_TRACKER_VALUES);
                            writeArrayStart();
                            d0.a it = ((d0) obj).iterator();
                            while (it.hasNext()) {
                                writeValue(it.next(), cls2, (Class) null);
                            }
                            writeArrayEnd();
                            writeObjectEnd();
                            return;
                        } else if (obj instanceof s) {
                            if (cls == null) {
                                cls = s.class;
                            }
                            writeObjectStart(cls3, cls);
                            this.writer.a(TJAdUnitConstants.String.USAGE_TRACKER_VALUES);
                            writeArrayStart();
                            s.a b3 = ((s) obj).b();
                            while (b3.f5594a) {
                                writeValue(Integer.valueOf(b3.b()), Integer.class, (Class) null);
                            }
                            writeArrayEnd();
                            writeObjectEnd();
                            return;
                        } else if (obj instanceof b) {
                            if (cls == null) {
                                cls = b.class;
                            }
                            writeObjectStart(cls3, cls);
                            b bVar2 = (b) obj;
                            int i5 = bVar2.f5416c;
                            while (i2 < i5) {
                                this.writer.a(convertToString((Object) bVar2.f5414a[i2]));
                                writeValue((Object) bVar2.f5415b[i2], cls2, (Class) null);
                                i2++;
                            }
                            writeObjectEnd();
                            return;
                        } else if (obj instanceof Map) {
                            if (cls == null) {
                                cls = HashMap.class;
                            }
                            writeObjectStart(cls3, cls);
                            for (Map.Entry entry : ((Map) obj).entrySet()) {
                                this.writer.a(convertToString(entry.getKey()));
                                writeValue(entry.getValue(), cls2, (Class) null);
                            }
                            writeObjectEnd();
                            return;
                        } else if (!com.badlogic.gdx.utils.z0.b.a(Enum.class, (Class) cls3)) {
                            writeObjectStart(cls3, cls);
                            writeFields(obj);
                            writeObjectEnd();
                            return;
                        } else if (this.typeName == null || (cls != null && cls == cls3)) {
                            this.writer.a((Object) convertToString((Enum) obj));
                            return;
                        } else {
                            if (cls3.getEnumConstants() == null) {
                                cls3 = cls3.getSuperclass();
                            }
                            writeObjectStart(cls3, null);
                            this.writer.a("value");
                            this.writer.a((Object) convertToString((Enum) obj));
                            writeObjectEnd();
                            return;
                        }
                    }
                }
            }
            this.writer.a(obj);
        }
    }

    public void toJson(Object obj, Class cls, Class cls2, Writer writer2) {
        setWriter(writer2);
        try {
            writeValue(obj, cls, cls2);
        } finally {
            q0.a(this.writer);
            this.writer = null;
        }
    }
}
