package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.h;
import com.esotericsoftware.spine.Animation;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: IntMap */
public class r<V> implements Iterable<b<V>> {

    /* renamed from: a  reason: collision with root package name */
    public int f5561a;

    /* renamed from: b  reason: collision with root package name */
    int[] f5562b;

    /* renamed from: c  reason: collision with root package name */
    V[] f5563c;

    /* renamed from: d  reason: collision with root package name */
    int f5564d;

    /* renamed from: e  reason: collision with root package name */
    int f5565e;

    /* renamed from: f  reason: collision with root package name */
    V f5566f;

    /* renamed from: g  reason: collision with root package name */
    boolean f5567g;

    /* renamed from: h  reason: collision with root package name */
    private float f5568h;

    /* renamed from: i  reason: collision with root package name */
    private int f5569i;

    /* renamed from: j  reason: collision with root package name */
    private int f5570j;

    /* renamed from: k  reason: collision with root package name */
    private int f5571k;
    private int l;
    private int m;
    private a n;
    private a o;
    private d p;
    private d q;

    /* compiled from: IntMap */
    public static class a<V> extends c<V> implements Iterable<b<V>>, Iterator<b<V>> {

        /* renamed from: f  reason: collision with root package name */
        private b<V> f5572f = new b<>();

        public a(r rVar) {
            super(rVar);
        }

        public boolean hasNext() {
            if (this.f5579e) {
                return this.f5575a;
            }
            throw new o("#iterator() cannot be used nested.");
        }

        public Iterator<b<V>> iterator() {
            return this;
        }

        public void remove() {
            super.remove();
        }

        public b<V> next() {
            if (!this.f5575a) {
                throw new NoSuchElementException();
            } else if (this.f5579e) {
                r<V> rVar = this.f5576b;
                int[] iArr = rVar.f5562b;
                int i2 = this.f5577c;
                if (i2 == -1) {
                    b<V> bVar = this.f5572f;
                    bVar.f5573a = 0;
                    bVar.f5574b = rVar.f5566f;
                } else {
                    b<V> bVar2 = this.f5572f;
                    bVar2.f5573a = iArr[i2];
                    bVar2.f5574b = rVar.f5563c[i2];
                }
                this.f5578d = this.f5577c;
                a();
                return this.f5572f;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }
    }

    /* compiled from: IntMap */
    public static class b<V> {

        /* renamed from: a  reason: collision with root package name */
        public int f5573a;

        /* renamed from: b  reason: collision with root package name */
        public V f5574b;

        public String toString() {
            return this.f5573a + "=" + ((Object) this.f5574b);
        }
    }

    /* compiled from: IntMap */
    private static class c<V> {

        /* renamed from: a  reason: collision with root package name */
        public boolean f5575a;

        /* renamed from: b  reason: collision with root package name */
        final r<V> f5576b;

        /* renamed from: c  reason: collision with root package name */
        int f5577c;

        /* renamed from: d  reason: collision with root package name */
        int f5578d;

        /* renamed from: e  reason: collision with root package name */
        boolean f5579e = true;

        public c(r<V> rVar) {
            this.f5576b = rVar;
            b();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f5575a = false;
            r<V> rVar = this.f5576b;
            int[] iArr = rVar.f5562b;
            int i2 = rVar.f5564d + rVar.f5565e;
            do {
                int i3 = this.f5577c + 1;
                this.f5577c = i3;
                if (i3 >= i2) {
                    return;
                }
            } while (iArr[this.f5577c] == 0);
            this.f5575a = true;
        }

        public void b() {
            this.f5578d = -2;
            this.f5577c = -1;
            if (this.f5576b.f5567g) {
                this.f5575a = true;
            } else {
                a();
            }
        }

        public void remove() {
            if (this.f5578d == -1) {
                r<V> rVar = this.f5576b;
                if (rVar.f5567g) {
                    rVar.f5566f = null;
                    rVar.f5567g = false;
                    this.f5578d = -2;
                    r<V> rVar2 = this.f5576b;
                    rVar2.f5561a--;
                    return;
                }
            }
            int i2 = this.f5578d;
            if (i2 >= 0) {
                r<V> rVar3 = this.f5576b;
                if (i2 >= rVar3.f5564d) {
                    rVar3.d(i2);
                    this.f5577c = this.f5578d - 1;
                    a();
                } else {
                    rVar3.f5562b[i2] = 0;
                    rVar3.f5563c[i2] = null;
                }
                this.f5578d = -2;
                r<V> rVar22 = this.f5576b;
                rVar22.f5561a--;
                return;
            }
            throw new IllegalStateException("next must be called before remove.");
        }
    }

    /* compiled from: IntMap */
    public static class d<V> extends c<V> implements Iterable<V>, Iterator<V> {
        public d(r<V> rVar) {
            super(rVar);
        }

        public boolean hasNext() {
            if (this.f5579e) {
                return this.f5575a;
            }
            throw new o("#iterator() cannot be used nested.");
        }

        public Iterator<V> iterator() {
            return this;
        }

        public V next() {
            V v;
            if (!this.f5575a) {
                throw new NoSuchElementException();
            } else if (this.f5579e) {
                int i2 = this.f5577c;
                if (i2 == -1) {
                    v = this.f5576b.f5566f;
                } else {
                    v = this.f5576b.f5563c[i2];
                }
                this.f5578d = this.f5577c;
                a();
                return v;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }

        public void remove() {
            super.remove();
        }

        public a<V> toArray() {
            a<V> aVar = new a<>(true, this.f5576b.f5561a);
            while (this.f5575a) {
                aVar.add(next());
            }
            return aVar;
        }
    }

    public r() {
        this(51, 0.8f);
    }

    private void a(int i2, V v, int i3, int i4, int i5, int i6, int i7, int i8) {
        int[] iArr = this.f5562b;
        V[] vArr = this.f5563c;
        int i9 = this.f5570j;
        int i10 = this.m;
        int i11 = 0;
        do {
            int c2 = h.c(2);
            if (c2 == 0) {
                V v2 = vArr[i3];
                iArr[i3] = i2;
                vArr[i3] = v;
                i2 = i4;
                v = v2;
            } else if (c2 != 1) {
                V v3 = vArr[i7];
                iArr[i7] = i2;
                vArr[i7] = v;
                v = v3;
                i2 = i8;
            } else {
                V v4 = vArr[i5];
                iArr[i5] = i2;
                vArr[i5] = v;
                v = v4;
                i2 = i6;
            }
            i3 = i2 & i9;
            i4 = iArr[i3];
            if (i4 == 0) {
                iArr[i3] = i2;
                vArr[i3] = v;
                int i12 = this.f5561a;
                this.f5561a = i12 + 1;
                if (i12 >= this.f5571k) {
                    h(this.f5564d << 1);
                    return;
                }
                return;
            }
            i5 = f(i2);
            i6 = iArr[i5];
            if (i6 == 0) {
                iArr[i5] = i2;
                vArr[i5] = v;
                int i13 = this.f5561a;
                this.f5561a = i13 + 1;
                if (i13 >= this.f5571k) {
                    h(this.f5564d << 1);
                    return;
                }
                return;
            }
            i7 = g(i2);
            i8 = iArr[i7];
            if (i8 == 0) {
                iArr[i7] = i2;
                vArr[i7] = v;
                int i14 = this.f5561a;
                this.f5561a = i14 + 1;
                if (i14 >= this.f5571k) {
                    h(this.f5564d << 1);
                    return;
                }
                return;
            }
            i11++;
        } while (i11 != i10);
        e(i2, v);
    }

    private V c(int i2, V v) {
        int[] iArr = this.f5562b;
        int i3 = this.f5564d;
        int i4 = this.f5565e + i3;
        while (i3 < i4) {
            if (iArr[i3] == i2) {
                return this.f5563c[i3];
            }
            i3++;
        }
        return v;
    }

    private void d(int i2, V v) {
        if (i2 == 0) {
            this.f5566f = v;
            this.f5567g = true;
            return;
        }
        int i3 = i2 & this.f5570j;
        int[] iArr = this.f5562b;
        int i4 = iArr[i3];
        if (i4 == 0) {
            iArr[i3] = i2;
            this.f5563c[i3] = v;
            int i5 = this.f5561a;
            this.f5561a = i5 + 1;
            if (i5 >= this.f5571k) {
                h(this.f5564d << 1);
                return;
            }
            return;
        }
        int f2 = f(i2);
        int[] iArr2 = this.f5562b;
        int i6 = iArr2[f2];
        if (i6 == 0) {
            iArr2[f2] = i2;
            this.f5563c[f2] = v;
            int i7 = this.f5561a;
            this.f5561a = i7 + 1;
            if (i7 >= this.f5571k) {
                h(this.f5564d << 1);
                return;
            }
            return;
        }
        int g2 = g(i2);
        int[] iArr3 = this.f5562b;
        int i8 = iArr3[g2];
        if (i8 == 0) {
            iArr3[g2] = i2;
            this.f5563c[g2] = v;
            int i9 = this.f5561a;
            this.f5561a = i9 + 1;
            if (i9 >= this.f5571k) {
                h(this.f5564d << 1);
                return;
            }
            return;
        }
        a(i2, v, i3, i4, f2, i6, g2, i8);
    }

    private void e(int i2, V v) {
        int i3 = this.f5565e;
        if (i3 == this.l) {
            h(this.f5564d << 1);
            d(i2, v);
            return;
        }
        int i4 = this.f5564d + i3;
        this.f5562b[i4] = i2;
        this.f5563c[i4] = v;
        this.f5565e = i3 + 1;
        this.f5561a++;
    }

    private int f(int i2) {
        int i3 = i2 * -1262997959;
        return (i3 ^ (i3 >>> this.f5569i)) & this.f5570j;
    }

    private int g(int i2) {
        int i3 = i2 * -825114047;
        return (i3 ^ (i3 >>> this.f5569i)) & this.f5570j;
    }

    private void h(int i2) {
        int i3 = this.f5564d + this.f5565e;
        this.f5564d = i2;
        this.f5571k = (int) (((float) i2) * this.f5568h);
        this.f5570j = i2 - 1;
        this.f5569i = 31 - Integer.numberOfTrailingZeros(i2);
        double d2 = (double) i2;
        this.l = Math.max(3, ((int) Math.ceil(Math.log(d2))) * 2);
        this.m = Math.max(Math.min(i2, 8), ((int) Math.sqrt(d2)) / 8);
        int[] iArr = this.f5562b;
        V[] vArr = this.f5563c;
        int i4 = this.l;
        this.f5562b = new int[(i2 + i4)];
        this.f5563c = new Object[(i2 + i4)];
        int i5 = this.f5561a;
        this.f5561a = this.f5567g ? 1 : 0;
        this.f5565e = 0;
        if (i5 > 0) {
            for (int i6 = 0; i6 < i3; i6++) {
                int i7 = iArr[i6];
                if (i7 != 0) {
                    d(i7, vArr[i6]);
                }
            }
        }
    }

    public V b(int i2, V v) {
        if (i2 == 0) {
            V v2 = this.f5566f;
            this.f5566f = v;
            if (!this.f5567g) {
                this.f5567g = true;
                this.f5561a++;
            }
            return v2;
        }
        int[] iArr = this.f5562b;
        int i3 = i2 & this.f5570j;
        int i4 = iArr[i3];
        if (i4 == i2) {
            V[] vArr = this.f5563c;
            V v3 = vArr[i3];
            vArr[i3] = v;
            return v3;
        }
        int f2 = f(i2);
        int i5 = iArr[f2];
        if (i5 == i2) {
            V[] vArr2 = this.f5563c;
            V v4 = vArr2[f2];
            vArr2[f2] = v;
            return v4;
        }
        int g2 = g(i2);
        int i6 = iArr[g2];
        if (i6 == i2) {
            V[] vArr3 = this.f5563c;
            V v5 = vArr3[g2];
            vArr3[g2] = v;
            return v5;
        }
        int i7 = this.f5564d;
        int i8 = this.f5565e + i7;
        while (i7 < i8) {
            if (iArr[i7] == i2) {
                V[] vArr4 = this.f5563c;
                V v6 = vArr4[i7];
                vArr4[i7] = v;
                return v6;
            }
            i7++;
        }
        if (i4 == 0) {
            iArr[i3] = i2;
            this.f5563c[i3] = v;
            int i9 = this.f5561a;
            this.f5561a = i9 + 1;
            if (i9 >= this.f5571k) {
                h(this.f5564d << 1);
            }
            return null;
        } else if (i5 == 0) {
            iArr[f2] = i2;
            this.f5563c[f2] = v;
            int i10 = this.f5561a;
            this.f5561a = i10 + 1;
            if (i10 >= this.f5571k) {
                h(this.f5564d << 1);
            }
            return null;
        } else if (i6 == 0) {
            iArr[g2] = i2;
            this.f5563c[g2] = v;
            int i11 = this.f5561a;
            this.f5561a = i11 + 1;
            if (i11 >= this.f5571k) {
                h(this.f5564d << 1);
            }
            return null;
        } else {
            a(i2, v, i3, i4, f2, i5, g2, i6);
            return null;
        }
    }

    public void clear() {
        if (this.f5561a != 0) {
            int[] iArr = this.f5562b;
            V[] vArr = this.f5563c;
            int i2 = this.f5564d + this.f5565e;
            while (true) {
                int i3 = i2 - 1;
                if (i2 > 0) {
                    iArr[i3] = 0;
                    vArr[i3] = null;
                    i2 = i3;
                } else {
                    this.f5561a = 0;
                    this.f5565e = 0;
                    this.f5566f = null;
                    this.f5567g = false;
                    return;
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean equals(java.lang.Object r9) {
        /*
            r8 = this;
            r0 = 1
            if (r9 != r8) goto L_0x0004
            return r0
        L_0x0004:
            boolean r1 = r9 instanceof com.badlogic.gdx.utils.r
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            com.badlogic.gdx.utils.r r9 = (com.badlogic.gdx.utils.r) r9
            int r1 = r9.f5561a
            int r3 = r8.f5561a
            if (r1 == r3) goto L_0x0013
            return r2
        L_0x0013:
            boolean r1 = r9.f5567g
            boolean r3 = r8.f5567g
            if (r1 == r3) goto L_0x001a
            return r2
        L_0x001a:
            if (r3 == 0) goto L_0x002e
            V r1 = r9.f5566f
            if (r1 != 0) goto L_0x0025
            V r1 = r8.f5566f
            if (r1 == 0) goto L_0x002e
            return r2
        L_0x0025:
            V r3 = r8.f5566f
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x002e
            return r2
        L_0x002e:
            int[] r1 = r8.f5562b
            V[] r3 = r8.f5563c
            int r4 = r8.f5564d
            int r5 = r8.f5565e
            int r4 = r4 + r5
            r5 = 0
        L_0x0038:
            if (r5 >= r4) goto L_0x0059
            r6 = r1[r5]
            if (r6 == 0) goto L_0x0056
            r7 = r3[r5]
            if (r7 != 0) goto L_0x004b
            java.lang.Object r7 = com.badlogic.gdx.utils.c0.r
            java.lang.Object r6 = r9.a(r6, r7)
            if (r6 == 0) goto L_0x0056
            return r2
        L_0x004b:
            java.lang.Object r6 = r9.get(r6)
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x0056
            return r2
        L_0x0056:
            int r5 = r5 + 1
            goto L_0x0038
        L_0x0059:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.r.equals(java.lang.Object):boolean");
    }

    public V get(int i2) {
        if (i2 != 0) {
            int i3 = this.f5570j & i2;
            if (this.f5562b[i3] != i2) {
                i3 = f(i2);
                if (this.f5562b[i3] != i2) {
                    i3 = g(i2);
                    if (this.f5562b[i3] != i2) {
                        return c(i2, null);
                    }
                }
            }
            return this.f5563c[i3];
        } else if (!this.f5567g) {
            return null;
        } else {
            return this.f5566f;
        }
    }

    public int hashCode() {
        V v;
        int hashCode = (!this.f5567g || (v = this.f5566f) == null) ? 0 : v.hashCode() + 0;
        int[] iArr = this.f5562b;
        V[] vArr = this.f5563c;
        int i2 = this.f5564d + this.f5565e;
        for (int i3 = 0; i3 < i2; i3++) {
            int i4 = iArr[i3];
            if (i4 != 0) {
                hashCode += i4 * 31;
                V v2 = vArr[i3];
                if (v2 != null) {
                    hashCode += v2.hashCode();
                }
            }
        }
        return hashCode;
    }

    public Iterator<b<V>> iterator() {
        return a();
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0059  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r7 = this;
            int r0 = r7.f5561a
            if (r0 != 0) goto L_0x0007
            java.lang.String r0 = "[]"
            return r0
        L_0x0007:
            com.badlogic.gdx.utils.r0 r0 = new com.badlogic.gdx.utils.r0
            r1 = 32
            r0.<init>(r1)
            r1 = 91
            r0.append(r1)
            int[] r1 = r7.f5562b
            V[] r2 = r7.f5563c
            int r3 = r1.length
            boolean r4 = r7.f5567g
            r5 = 61
            if (r4 == 0) goto L_0x0029
            java.lang.String r4 = "0="
            r0.a(r4)
            V r4 = r7.f5566f
            r0.a(r4)
            goto L_0x003f
        L_0x0029:
            int r4 = r3 + -1
            if (r3 <= 0) goto L_0x003e
            r3 = r1[r4]
            if (r3 != 0) goto L_0x0033
            r3 = r4
            goto L_0x0029
        L_0x0033:
            r0.a(r3)
            r0.append(r5)
            r3 = r2[r4]
            r0.a(r3)
        L_0x003e:
            r3 = r4
        L_0x003f:
            int r4 = r3 + -1
            if (r3 <= 0) goto L_0x0059
            r3 = r1[r4]
            if (r3 != 0) goto L_0x0048
            goto L_0x003e
        L_0x0048:
            java.lang.String r6 = ", "
            r0.a(r6)
            r0.a(r3)
            r0.append(r5)
            r3 = r2[r4]
            r0.a(r3)
            goto L_0x003e
        L_0x0059:
            r1 = 93
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.r.toString():java.lang.String");
    }

    public r(int i2, float f2) {
        if (i2 >= 0) {
            int b2 = h.b((int) Math.ceil((double) (((float) i2) / f2)));
            if (b2 <= 1073741824) {
                this.f5564d = b2;
                if (f2 > Animation.CurveTimeline.LINEAR) {
                    this.f5568h = f2;
                    int i3 = this.f5564d;
                    this.f5571k = (int) (((float) i3) * f2);
                    this.f5570j = i3 - 1;
                    this.f5569i = 31 - Integer.numberOfTrailingZeros(i3);
                    this.l = Math.max(3, ((int) Math.ceil(Math.log((double) this.f5564d))) * 2);
                    this.m = Math.max(Math.min(this.f5564d, 8), ((int) Math.sqrt((double) this.f5564d)) / 8);
                    this.f5562b = new int[(this.f5564d + this.l)];
                    this.f5563c = new Object[this.f5562b.length];
                    return;
                }
                throw new IllegalArgumentException("loadFactor must be > 0: " + f2);
            }
            throw new IllegalArgumentException("initialCapacity is too large: " + b2);
        }
        throw new IllegalArgumentException("initialCapacity must be >= 0: " + i2);
    }

    public boolean c(int i2) {
        if (i2 == 0) {
            return this.f5567g;
        }
        if (this.f5562b[this.f5570j & i2] == i2) {
            return true;
        }
        if (this.f5562b[f(i2)] == i2) {
            return true;
        }
        if (this.f5562b[g(i2)] != i2) {
            return e(i2);
        }
        return true;
    }

    private boolean e(int i2) {
        int[] iArr = this.f5562b;
        int i3 = this.f5564d;
        int i4 = this.f5565e + i3;
        while (i3 < i4) {
            if (iArr[i3] == i2) {
                return true;
            }
            i3++;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void d(int i2) {
        this.f5565e--;
        int i3 = this.f5564d + this.f5565e;
        if (i2 < i3) {
            int[] iArr = this.f5562b;
            iArr[i2] = iArr[i3];
            V[] vArr = this.f5563c;
            vArr[i2] = vArr[i3];
            vArr[i3] = null;
            return;
        }
        this.f5563c[i2] = null;
    }

    public V a(int i2, V v) {
        if (i2 != 0) {
            int i3 = this.f5570j & i2;
            if (this.f5562b[i3] != i2) {
                i3 = f(i2);
                if (this.f5562b[i3] != i2) {
                    i3 = g(i2);
                    if (this.f5562b[i3] != i2) {
                        return c(i2, v);
                    }
                }
            }
            return this.f5563c[i3];
        } else if (!this.f5567g) {
            return v;
        } else {
            return this.f5566f;
        }
    }

    public d<V> b() {
        if (h.f5490a) {
            return new d<>(this);
        }
        if (this.p == null) {
            this.p = new d(this);
            this.q = new d(this);
        }
        d dVar = this.p;
        if (!dVar.f5579e) {
            dVar.b();
            d<V> dVar2 = this.p;
            dVar2.f5579e = true;
            this.q.f5579e = false;
            return dVar2;
        }
        this.q.b();
        d<V> dVar3 = this.q;
        dVar3.f5579e = true;
        this.p.f5579e = false;
        return dVar3;
    }

    public a<V> a() {
        if (h.f5490a) {
            return new a<>(this);
        }
        if (this.n == null) {
            this.n = new a(this);
            this.o = new a(this);
        }
        a aVar = this.n;
        if (!aVar.f5579e) {
            aVar.b();
            a<V> aVar2 = this.n;
            aVar2.f5579e = true;
            this.o.f5579e = false;
            return aVar2;
        }
        this.o.b();
        a<V> aVar3 = this.o;
        aVar3.f5579e = true;
        this.n.f5579e = false;
        return aVar3;
    }
}
