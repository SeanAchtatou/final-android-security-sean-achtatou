package com.badlogic.gdx.utils;

import java.util.Comparator;

/* compiled from: Sort */
public class p0 {

    /* renamed from: c  reason: collision with root package name */
    private static p0 f5555c;

    /* renamed from: a  reason: collision with root package name */
    private t0 f5556a;

    /* renamed from: b  reason: collision with root package name */
    private i f5557b;

    public void a(Object[] objArr, int i2, int i3) {
        if (this.f5557b == null) {
            this.f5557b = new i();
        }
        this.f5557b.a(objArr, i2, i3);
    }

    public <T> void a(T[] tArr, Comparator<? super T> comparator, int i2, int i3) {
        if (this.f5556a == null) {
            this.f5556a = new t0();
        }
        this.f5556a.a(tArr, comparator, i2, i3);
    }

    public static p0 a() {
        if (f5555c == null) {
            f5555c = new p0();
        }
        return f5555c;
    }
}
