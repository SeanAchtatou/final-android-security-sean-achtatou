package com.badlogic.gdx.utils;

import java.util.Comparator;

/* compiled from: SnapshotArray */
public class o0<T> extends a<T> {

    /* renamed from: e  reason: collision with root package name */
    private T[] f5545e;

    /* renamed from: f  reason: collision with root package name */
    private T[] f5546f;

    /* renamed from: g  reason: collision with root package name */
    private int f5547g;

    public o0() {
    }

    private void g() {
        T[] tArr;
        int i2;
        T[] tArr2 = this.f5545e;
        if (tArr2 != null && tArr2 == (tArr = this.f5373a)) {
            T[] tArr3 = this.f5546f;
            if (tArr3 == null || tArr3.length < (i2 = this.f5374b)) {
                e(this.f5373a.length);
                return;
            }
            System.arraycopy(tArr, 0, tArr3, 0, i2);
            this.f5373a = this.f5546f;
            this.f5546f = null;
        }
    }

    public void a(int i2, T t) {
        g();
        super.a(i2, t);
    }

    public void b(int i2, int i3) {
        g();
        super.b(i2, i3);
    }

    public void clear() {
        g();
        super.clear();
    }

    public boolean d(T t, boolean z) {
        g();
        return super.d(t, z);
    }

    public T[] e() {
        g();
        T[] tArr = this.f5373a;
        this.f5545e = tArr;
        this.f5547g++;
        return tArr;
    }

    public void f() {
        this.f5547g = Math.max(0, this.f5547g - 1);
        T[] tArr = this.f5545e;
        if (tArr != null) {
            if (tArr != this.f5373a && this.f5547g == 0) {
                this.f5546f = tArr;
                int length = this.f5546f.length;
                for (int i2 = 0; i2 < length; i2++) {
                    this.f5546f[i2] = null;
                }
            }
            this.f5545e = null;
        }
    }

    public T pop() {
        g();
        return super.pop();
    }

    public void set(int i2, T t) {
        g();
        super.set(i2, t);
    }

    public void sort(Comparator<? super T> comparator) {
        g();
        super.sort(comparator);
    }

    public o0(boolean z, int i2, Class cls) {
        super(z, i2, cls);
    }

    public o0(boolean z, int i2) {
        super(z, i2);
    }

    public void a(int i2, int i3) {
        g();
        super.a(i2, i3);
    }

    public T d(int i2) {
        g();
        return super.d(i2);
    }

    public o0(Class cls) {
        super(cls);
    }

    public o0(int i2) {
        super(i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.a.a(com.badlogic.gdx.utils.a, boolean):boolean
     arg types: [com.badlogic.gdx.utils.a<? extends T>, boolean]
     candidates:
      com.badlogic.gdx.utils.o0.a(int, int):void
      com.badlogic.gdx.utils.o0.a(int, java.lang.Object):void
      com.badlogic.gdx.utils.a.a(int, int):void
      com.badlogic.gdx.utils.a.a(int, java.lang.Object):void
      com.badlogic.gdx.utils.a.a(java.lang.Object, boolean):boolean
      com.badlogic.gdx.utils.a.a(com.badlogic.gdx.utils.a, boolean):boolean */
    public boolean a(a<? extends T> aVar, boolean z) {
        g();
        return super.a((a) aVar, z);
    }

    public void d() {
        g();
        super.d();
    }

    public void g(int i2) {
        g();
        super.g(i2);
    }

    public T[] f(int i2) {
        g();
        return super.f(i2);
    }
}
