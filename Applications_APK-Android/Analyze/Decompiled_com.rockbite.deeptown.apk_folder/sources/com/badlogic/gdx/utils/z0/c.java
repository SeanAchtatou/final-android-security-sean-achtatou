package com.badlogic.gdx.utils.z0;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/* compiled from: Constructor */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final Constructor f5717a;

    c(Constructor constructor) {
        this.f5717a = constructor;
    }

    public Class a() {
        return this.f5717a.getDeclaringClass();
    }

    public void a(boolean z) {
        this.f5717a.setAccessible(z);
    }

    public Object a(Object... objArr) throws f {
        try {
            return this.f5717a.newInstance(objArr);
        } catch (IllegalArgumentException e2) {
            throw new f("Illegal argument(s) supplied to constructor for class: " + a().getName(), e2);
        } catch (InstantiationException e3) {
            throw new f("Could not instantiate instance of class: " + a().getName(), e3);
        } catch (IllegalAccessException e4) {
            throw new f("Could not instantiate instance of class: " + a().getName(), e4);
        } catch (InvocationTargetException e5) {
            throw new f("Exception occurred in constructor for class: " + a().getName(), e5);
        }
    }
}
