package com.badlogic.gdx.utils;

/* compiled from: GdxRuntimeException */
public class o extends RuntimeException {
    public o(String str) {
        super(str);
    }

    public o(Throwable th) {
        super(th);
    }

    public o(String str, Throwable th) {
        super(str, th);
    }
}
