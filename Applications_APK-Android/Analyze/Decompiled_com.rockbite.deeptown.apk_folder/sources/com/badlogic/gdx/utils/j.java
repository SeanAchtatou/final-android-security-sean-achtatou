package com.badlogic.gdx.utils;

import com.google.android.gms.games.Notifications;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: DataInput */
public class j extends DataInputStream {
    private char[] chars = new char[32];

    public j(InputStream inputStream) {
        super(inputStream);
    }

    private void readUtf8_slow(int i2, int i3, int i4) throws IOException {
        char[] cArr = this.chars;
        while (true) {
            switch (i4 >> 4) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    cArr[i3] = (char) i4;
                    break;
                case 12:
                case 13:
                    cArr[i3] = (char) (((i4 & 31) << 6) | (read() & 63));
                    break;
                case 14:
                    cArr[i3] = (char) (((i4 & 15) << 12) | ((read() & 63) << 6) | (read() & 63));
                    break;
            }
            i3++;
            if (i3 < i2) {
                i4 = read() & 255;
            } else {
                return;
            }
        }
    }

    public int readInt(boolean z) throws IOException {
        int read = read();
        int i2 = read & Notifications.NOTIFICATION_TYPES_ALL;
        if ((read & 128) != 0) {
            int read2 = read();
            i2 |= (read2 & Notifications.NOTIFICATION_TYPES_ALL) << 7;
            if ((read2 & 128) != 0) {
                int read3 = read();
                i2 |= (read3 & Notifications.NOTIFICATION_TYPES_ALL) << 14;
                if ((read3 & 128) != 0) {
                    int read4 = read();
                    i2 |= (read4 & Notifications.NOTIFICATION_TYPES_ALL) << 21;
                    if ((read4 & 128) != 0) {
                        i2 |= (read() & Notifications.NOTIFICATION_TYPES_ALL) << 28;
                    }
                }
            }
        }
        return z ? i2 : (i2 >>> 1) ^ (-(i2 & 1));
    }

    public String readString() throws IOException {
        throw null;
    }
}
