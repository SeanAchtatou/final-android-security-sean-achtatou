package com.badlogic.gdx.utils.a1;

import com.badlogic.gdx.math.p;
import com.badlogic.gdx.utils.k0;
import e.d.b.t.a;
import e.d.b.t.k;

/* compiled from: ScalingViewport */
public class b extends e {

    /* renamed from: i  reason: collision with root package name */
    private k0 f5404i;

    public b(k0 k0Var, float f2, float f3) {
        this(k0Var, f2, f3, new k());
    }

    public void a(int i2, int i3, boolean z) {
        p a2 = this.f5404i.a(h(), g(), (float) i2, (float) i3);
        int round = Math.round(a2.f5294a);
        int round2 = Math.round(a2.f5295b);
        a((i2 - round) / 2, (i3 - round2) / 2, round, round2);
        a(z);
    }

    public b(k0 k0Var, float f2, float f3, a aVar) {
        this.f5404i = k0Var;
        a(f2, f3);
        a(aVar);
    }
}
