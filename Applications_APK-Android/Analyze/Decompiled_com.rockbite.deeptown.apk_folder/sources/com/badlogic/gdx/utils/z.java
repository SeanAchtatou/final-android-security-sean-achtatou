package com.badlogic.gdx.utils;

/* compiled from: NumberUtils */
public final class z {
    public static int a(float f2) {
        return Float.floatToIntBits(f2);
    }

    public static int b(float f2) {
        int floatToRawIntBits = Float.floatToRawIntBits(f2);
        return floatToRawIntBits | (((int) (((float) (floatToRawIntBits >>> 24)) * 1.003937f)) << 24);
    }

    public static int c(float f2) {
        return Float.floatToRawIntBits(f2);
    }

    public static float a(int i2) {
        return Float.intBitsToFloat(i2 & -16777217);
    }
}
