package com.badlogic.gdx.utils.y0;

import com.badlogic.gdx.utils.o;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/* compiled from: AsyncResult */
public class b<T> {

    /* renamed from: a  reason: collision with root package name */
    private final Future<T> f5716a;

    b(Future<T> future) {
        this.f5716a = future;
    }

    public T a() {
        try {
            return this.f5716a.get();
        } catch (InterruptedException unused) {
            return null;
        } catch (ExecutionException e2) {
            throw new o(e2.getCause());
        }
    }

    public boolean b() {
        return this.f5716a.isDone();
    }
}
