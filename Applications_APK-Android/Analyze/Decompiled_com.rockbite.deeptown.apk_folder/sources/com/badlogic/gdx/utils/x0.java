package com.badlogic.gdx.utils;

import com.badlogic.gdx.utils.c0;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;

/* compiled from: XmlReader */
public class x0 {

    /* renamed from: e  reason: collision with root package name */
    private static final byte[] f5679e = b();

    /* renamed from: f  reason: collision with root package name */
    private static final byte[] f5680f = e();

    /* renamed from: g  reason: collision with root package name */
    private static final char[] f5681g = i();

    /* renamed from: h  reason: collision with root package name */
    private static final byte[] f5682h = g();

    /* renamed from: i  reason: collision with root package name */
    private static final byte[] f5683i = f();

    /* renamed from: j  reason: collision with root package name */
    private static final short[] f5684j = c();

    /* renamed from: k  reason: collision with root package name */
    private static final byte[] f5685k = d();
    private static final byte[] l = j();
    private static final byte[] m = h();

    /* renamed from: a  reason: collision with root package name */
    private final a<a> f5686a = new a<>(8);

    /* renamed from: b  reason: collision with root package name */
    private a f5687b;

    /* renamed from: c  reason: collision with root package name */
    private a f5688c;

    /* renamed from: d  reason: collision with root package name */
    private final r0 f5689d = new r0(64);

    /* compiled from: XmlReader */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private final String f5690a;

        /* renamed from: b  reason: collision with root package name */
        private c0<String, String> f5691b;

        /* renamed from: c  reason: collision with root package name */
        private a<a> f5692c;

        /* renamed from: d  reason: collision with root package name */
        private String f5693d;

        public a(String str, a aVar) {
            this.f5690a = str;
        }

        public c0<String, String> a() {
            return this.f5691b;
        }

        public String b(String str) {
            c0<String, String> c0Var = this.f5691b;
            if (c0Var != null) {
                String b2 = c0Var.b(str);
                if (b2 != null) {
                    return b2;
                }
                throw new o("Element " + this.f5690a + " doesn't have attribute: " + str);
            }
            throw new o("Element " + this.f5690a + " doesn't have attribute: " + str);
        }

        public String c() {
            return this.f5690a;
        }

        public String d() {
            return this.f5693d;
        }

        public float e(String str) {
            String a2 = a(str, null);
            if (a2 != null) {
                return Float.parseFloat(a2);
            }
            throw new o("Element " + this.f5690a + " doesn't have attribute or child: " + str);
        }

        public float f(String str) {
            return Float.parseFloat(b(str));
        }

        public int g(String str) {
            String a2 = a(str, null);
            if (a2 != null) {
                return Integer.parseInt(a2);
            }
            throw new o("Element " + this.f5690a + " doesn't have attribute or child: " + str);
        }

        public int h(String str) {
            return Integer.parseInt(b(str));
        }

        public boolean i(String str) {
            c0<String, String> c0Var = this.f5691b;
            if (c0Var == null) {
                return false;
            }
            return c0Var.a(str);
        }

        public boolean j(String str) {
            if (this.f5692c == null || c(str) == null) {
                return false;
            }
            return true;
        }

        public void k(String str) {
            this.f5693d = str;
        }

        public String l(String str) {
            String str2;
            r0 r0Var = new r0(128);
            r0Var.a(str);
            r0Var.append('<');
            r0Var.a(this.f5690a);
            c0<String, String> c0Var = this.f5691b;
            if (c0Var != null) {
                c0.a<String, String> a2 = c0Var.a();
                a2.iterator();
                while (a2.hasNext()) {
                    c0.b bVar = (c0.b) a2.next();
                    r0Var.append(' ');
                    r0Var.a((String) bVar.f5459a);
                    r0Var.a("=\"");
                    r0Var.a((String) bVar.f5460b);
                    r0Var.append('\"');
                }
            }
            if (this.f5692c == null && ((str2 = this.f5693d) == null || str2.length() == 0)) {
                r0Var.a("/>");
            } else {
                r0Var.a(">\n");
                String str3 = str + 9;
                String str4 = this.f5693d;
                if (str4 != null && str4.length() > 0) {
                    r0Var.a(str3);
                    r0Var.a(this.f5693d);
                    r0Var.append(10);
                }
                a<a> aVar = this.f5692c;
                if (aVar != null) {
                    Iterator<a> it = aVar.iterator();
                    while (it.hasNext()) {
                        r0Var.a(it.next().l(str3));
                        r0Var.append(10);
                    }
                }
                r0Var.a(str);
                r0Var.a("</");
                r0Var.a(this.f5690a);
                r0Var.append('>');
            }
            return r0Var.toString();
        }

        public String toString() {
            return l("");
        }

        public a a(int i2) {
            a<a> aVar = this.f5692c;
            if (aVar != null) {
                return aVar.get(i2);
            }
            throw new o("Element has no children: " + this.f5690a);
        }

        public void c(String str, String str2) {
            if (this.f5691b == null) {
                this.f5691b = new c0<>(8);
            }
            this.f5691b.b(str, str2);
        }

        public a<a> d(String str) {
            a<a> aVar = new a<>();
            if (this.f5692c == null) {
                return aVar;
            }
            int i2 = 0;
            while (true) {
                a<a> aVar2 = this.f5692c;
                if (i2 >= aVar2.f5374b) {
                    return aVar;
                }
                a aVar3 = aVar2.get(i2);
                if (aVar3.f5690a.equals(str)) {
                    aVar.add(aVar3);
                }
                i2++;
            }
        }

        public a c(String str) {
            if (this.f5692c == null) {
                return null;
            }
            int i2 = 0;
            while (true) {
                a<a> aVar = this.f5692c;
                if (i2 >= aVar.f5374b) {
                    return null;
                }
                a aVar2 = aVar.get(i2);
                if (aVar2.f5690a.equals(str)) {
                    return aVar2;
                }
                i2++;
            }
        }

        public void a(a aVar) {
            if (this.f5692c == null) {
                this.f5692c = new a<>(8);
            }
            this.f5692c.add(aVar);
        }

        public String b(String str, String str2) {
            String b2;
            c0<String, String> c0Var = this.f5691b;
            return (c0Var == null || (b2 = c0Var.b(str)) == null) ? str2 : b2;
        }

        public String a(String str) {
            String a2 = a(str, null);
            if (a2 != null) {
                return a2;
            }
            throw new o("Element " + this.f5690a + " doesn't have attribute or child: " + str);
        }

        public int b() {
            a<a> aVar = this.f5692c;
            if (aVar == null) {
                return 0;
            }
            return aVar.f5374b;
        }

        public String a(String str, String str2) {
            String d2;
            String b2;
            c0<String, String> c0Var = this.f5691b;
            if (c0Var != null && (b2 = c0Var.b(str)) != null) {
                return b2;
            }
            a c2 = c(str);
            return (c2 == null || (d2 = c2.d()) == null) ? str2 : d2;
        }
    }

    private static byte[] b() {
        return new byte[]{0, 1, 0, 1, 1, 1, 2, 1, 3, 1, 4, 1, 5, 1, 6, 1, 7, 2, 0, 6, 2, 1, 4, 2, 2, 4};
    }

    private static byte[] d() {
        return new byte[]{0, 2, 0, 1, 2, 1, 1, 2, 3, 5, 6, 7, 5, 4, 9, 10, 1, 11, 9, 8, 13, 1, 14, 1, 13, 12, 15, 16, 15, 1, 16, 17, 18, 16, 1, 20, 19, 22, 21, 9, 10, 11, 9, 1, 23, 24, 23, 1, 25, 11, 25, 1, 20, 26, 22, 27, 29, 30, 29, 28, 32, 31, 30, 34, 1, 30, 33, 36, 37, 38, 36, 35, 40, 41, 1, 42, 40, 39, 44, 1, 45, 1, 44, 43, 46, 47, 46, 1, 47, 48, 49, 47, 1, 51, 50, 53, 52, 40, 41, 42, 40, 1, 54, 55, 54, 1, 56, 42, 56, 1, 57, 1, 57, 34, 57, 1, 1, 58, 59, 58, 51, 60, 53, 61, 62, 62, 1, 1, 0};
    }

    private static byte[] e() {
        return new byte[]{0, 0, 4, 9, 14, 20, 26, 30, 35, 36, 37, 42, 46, 50, 51, 52, 56, 57, 62, 67, 73, 79, 83, 88, 89, 90, 95, 99, 103, 104, 108, 109, 110, 111, 112, 115};
    }

    private static byte[] f() {
        return new byte[]{0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0};
    }

    private static byte[] g() {
        return new byte[]{0, 2, 3, 3, 4, 4, 2, 3, 1, 1, 3, 2, 2, 1, 1, 2, 1, 3, 3, 4, 4, 2, 3, 1, 1, 3, 2, 2, 1, 2, 1, 1, 1, 1, 1, 0};
    }

    private static byte[] h() {
        return new byte[]{0, 0, 0, 1, 0, 3, 3, 20, 1, 0, 0, 9, 0, 11, 11, 0, 0, 0, 0, 1, 17, 0, 13, 5, 23, 0, 1, 0, 1, 0, 0, 0, 15, 1, 0, 0, 3, 3, 20, 1, 0, 0, 9, 0, 11, 11, 0, 0, 0, 0, 1, 17, 0, 13, 5, 23, 0, 0, 0, 7, 1, 0, 0};
    }

    private static char[] i() {
        return new char[]{' ', '<', 9, 13, ' ', '/', '>', 9, 13, ' ', '/', '>', 9, 13, ' ', '/', '=', '>', 9, 13, ' ', '/', '=', '>', 9, 13, ' ', '=', 9, 13, ' ', '\"', '\'', 9, 13, '\"', '\"', ' ', '/', '>', 9, 13, ' ', '>', 9, 13, ' ', '>', 9, 13, '\'', '\'', ' ', '<', 9, 13, '<', ' ', '/', '>', 9, 13, ' ', '/', '>', 9, 13, ' ', '/', '=', '>', 9, 13, ' ', '/', '=', '>', 9, 13, ' ', '=', 9, 13, ' ', '\"', '\'', 9, 13, '\"', '\"', ' ', '/', '>', 9, 13, ' ', '>', 9, 13, ' ', '>', 9, 13, '<', ' ', '/', 9, 13, '>', '>', '\'', '\'', ' ', 9, 13, 0};
    }

    private static byte[] j() {
        return new byte[]{1, 0, 2, 3, 3, 4, 11, 34, 5, 4, 11, 34, 5, 6, 7, 6, 7, 8, 13, 9, 10, 9, 10, 12, 34, 12, 14, 14, 16, 15, 17, 16, 17, 18, 30, 18, 19, 26, 28, 20, 19, 26, 28, 20, 21, 22, 21, 22, 23, 32, 24, 25, 24, 25, 27, 28, 27, 29, 31, 35, 33, 33, 34};
    }

    public a a(Reader reader) {
        try {
            char[] cArr = new char[1024];
            int i2 = 0;
            while (true) {
                int read = reader.read(cArr, i2, cArr.length - i2);
                if (read == -1) {
                    a a2 = a(cArr, 0, i2);
                    q0.a(reader);
                    return a2;
                } else if (read == 0) {
                    char[] cArr2 = new char[(cArr.length * 2)];
                    System.arraycopy(cArr, 0, cArr2, 0, cArr.length);
                    cArr = cArr2;
                } else {
                    i2 += read;
                }
            }
        } catch (IOException e2) {
            throw new l0(e2);
        } catch (Throwable th) {
            q0.a(reader);
            throw th;
        }
    }

    public a c(String str) {
        char[] charArray = str.toCharArray();
        return a(charArray, 0, charArray.length);
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        a aVar = new a(str, this.f5688c);
        a aVar2 = this.f5688c;
        if (aVar2 != null) {
            aVar2.a(aVar);
        }
        this.f5686a.add(aVar);
        this.f5688c = aVar;
    }

    /* access modifiers changed from: protected */
    public void d(String str) {
        String d2 = this.f5688c.d();
        a aVar = this.f5688c;
        if (d2 != null) {
            str = d2 + str;
        }
        aVar.k(str);
    }

    private static short[] c() {
        return new short[]{0, 0, 4, 9, 14, 20, 26, 30, 35, 37, 39, 44, 48, 52, 54, 56, 60, 62, 67, 72, 78, 84, 88, 93, 95, 97, 102, 106, 110, 112, 116, 118, 120, 122, 124, 127};
    }

    public a a(InputStream inputStream) {
        try {
            a a2 = a(new InputStreamReader(inputStream, "UTF-8"));
            q0.a(inputStream);
            return a2;
        } catch (IOException e2) {
            throw new l0(e2);
        } catch (Throwable th) {
            q0.a(inputStream);
            throw th;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:170:0x0075 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:167:0x0040 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:164:0x0040 */
    /* JADX WARN: Type inference failed for: r5v4, types: [int] */
    /* JADX WARN: Type inference failed for: r3v37, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r3v38, types: [byte, int] */
    /* JADX WARN: Type inference failed for: r3v40, types: [int] */
    /* JADX WARN: Type inference failed for: r3v42, types: [int] */
    /* JADX WARN: Type inference failed for: r16v5, types: [int] */
    /* JADX WARN: Type inference failed for: r3v43 */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01c9, code lost:
        if (r1[r13 + 5] != 'T') goto L_0x0206;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01d1, code lost:
        if (r1[r13 + 6] != 'A') goto L_0x0206;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01d7, code lost:
        if (r1[r13 + 7] != '[') goto L_0x0206;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x01d9, code lost:
        r5 = r13 + 8;
        r3 = r5 + 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x01e3, code lost:
        if (r1[r3 - 2] != ']') goto L_0x0202;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x01eb, code lost:
        if (r1[r3 - 1] != ']') goto L_0x0202;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x01ef, code lost:
        if (r1[r3] == '>') goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x01f2, code lost:
        d(new java.lang.String(r1, r5, (r3 - r5) - 2));
        r10 = r3;
        r13 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0202, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0207, code lost:
        if (r3 != '!') goto L_0x022e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x020d, code lost:
        if (r1[r4] != '-') goto L_0x022e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0027, code lost:
        if (r11 == 0) goto L_0x0246;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0213, code lost:
        if (r1[r13 + 2] != '-') goto L_0x022e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0215, code lost:
        r3 = r13 + 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0219, code lost:
        if (r1[r3] != '-') goto L_0x022b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x021f, code lost:
        if (r1[r3 + 1] != '-') goto L_0x022b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x0221, code lost:
        r5 = r3 + 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x0225, code lost:
        if (r1[r5] == '>') goto L_0x0228;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0228, code lost:
        r10 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x022b, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0230, code lost:
        if (r1[r10] == '>') goto L_0x0171;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x0232, code lost:
        r10 = r10 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01a9, code lost:
        if (r1[r4] != '[') goto L_0x0206;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01b1, code lost:
        if (r1[r13 + 2] != 'C') goto L_0x0206;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01b9, code lost:
        if (r1[r13 + 3] != 'D') goto L_0x0206;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01c1, code lost:
        if (r1[r13 + 4] != 'A') goto L_0x0206;
     */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r3v38, types: [byte, int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x013e  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.badlogic.gdx.utils.x0.a a(char[] r23, int r24, int r25) {
        /*
            r22 = this;
            r0 = r22
            r1 = r23
            r2 = r25
            r7 = 2
            r8 = 1
            r10 = r24
            r9 = 0
            r11 = 1
            r12 = 0
            r13 = 0
        L_0x000e:
            r14 = 0
        L_0x000f:
            r15 = 10
            if (r9 == 0) goto L_0x0023
            if (r9 == r8) goto L_0x002b
            if (r9 == r7) goto L_0x001c
            r6 = 0
            r18 = 1
            goto L_0x0253
        L_0x001c:
            r4 = r11
        L_0x001d:
            r6 = 0
            r11 = 2
            r18 = 1
            goto L_0x0241
        L_0x0023:
            if (r10 != r2) goto L_0x0027
            r9 = 4
            goto L_0x000f
        L_0x0027:
            if (r11 != 0) goto L_0x002b
            goto L_0x0246
        L_0x002b:
            byte[] r9 = com.badlogic.gdx.utils.x0.f5680f
            byte r9 = r9[r11]
            short[] r16 = com.badlogic.gdx.utils.x0.f5684j
            short r16 = r16[r11]
            byte[] r17 = com.badlogic.gdx.utils.x0.f5682h
            byte r17 = r17[r11]
            if (r17 <= 0) goto L_0x0064
            int r18 = r9 + r17
            int r19 = r18 + -1
            r3 = r9
            r5 = r19
        L_0x0040:
            if (r5 >= r3) goto L_0x0045
            int r16 = r16 + r17
            goto L_0x0066
        L_0x0045:
            int r20 = r5 - r3
            int r20 = r20 >> 1
            int r20 = r3 + r20
            char r6 = r1[r10]
            char[] r21 = com.badlogic.gdx.utils.x0.f5681g
            char r4 = r21[r20]
            if (r6 >= r4) goto L_0x0056
            int r5 = r20 + -1
            goto L_0x0040
        L_0x0056:
            char r3 = r1[r10]
            char r4 = r21[r20]
            if (r3 <= r4) goto L_0x005f
            int r3 = r20 + 1
            goto L_0x0040
        L_0x005f:
            int r20 = r20 - r9
            int r16 = r16 + r20
            goto L_0x009a
        L_0x0064:
            r18 = r9
        L_0x0066:
            byte[] r3 = com.badlogic.gdx.utils.x0.f5683i
            byte r3 = r3[r11]
            if (r3 <= 0) goto L_0x009a
            int r4 = r3 << 1
            int r4 = r18 + r4
            int r4 = r4 - r7
            r5 = r18
        L_0x0073:
            if (r4 >= r5) goto L_0x0078
        L_0x0075:
            int r16 = r16 + r3
            goto L_0x009a
        L_0x0078:
            int r6 = r4 - r5
            int r6 = r6 >> r8
            r6 = r6 & -2
            int r6 = r6 + r5
            char r9 = r1[r10]
            char[] r11 = com.badlogic.gdx.utils.x0.f5681g
            char r7 = r11[r6]
            if (r9 >= r7) goto L_0x008a
            int r4 = r6 + -2
        L_0x0088:
            r7 = 2
            goto L_0x0073
        L_0x008a:
            char r5 = r1[r10]
            int r7 = r6 + 1
            char r7 = r11[r7]
            if (r5 <= r7) goto L_0x0095
            int r5 = r6 + 2
            goto L_0x0088
        L_0x0095:
            int r6 = r6 - r18
            int r3 = r6 >> 1
            goto L_0x0075
        L_0x009a:
            byte[] r3 = com.badlogic.gdx.utils.x0.f5685k
            byte r3 = r3[r16]
            byte[] r4 = com.badlogic.gdx.utils.x0.l
            byte r4 = r4[r3]
            byte[] r5 = com.badlogic.gdx.utils.x0.m
            byte r6 = r5[r3]
            if (r6 == 0) goto L_0x001d
            byte r3 = r5[r3]
            byte[] r5 = com.badlogic.gdx.utils.x0.f5679e
            int r6 = r3 + 1
            byte r3 = r5[r3]
        L_0x00b0:
            int r5 = r3 + -1
            if (r3 <= 0) goto L_0x001d
            byte[] r3 = com.badlogic.gdx.utils.x0.f5679e
            int r7 = r6 + 1
            byte r3 = r3[r6]
            switch(r3) {
                case 0: goto L_0x0235;
                case 1: goto L_0x0185;
                case 2: goto L_0x0178;
                case 3: goto L_0x016b;
                case 4: goto L_0x0165;
                case 5: goto L_0x0158;
                case 6: goto L_0x014a;
                case 7: goto L_0x00c3;
                default: goto L_0x00bd;
            }
        L_0x00bd:
            r6 = 0
            r11 = 2
            r18 = 1
            goto L_0x023a
        L_0x00c3:
            r3 = r10
        L_0x00c4:
            if (r3 == r13) goto L_0x00dc
            int r6 = r3 + -1
            char r6 = r1[r6]
            r9 = 9
            if (r6 == r9) goto L_0x00d9
            if (r6 == r15) goto L_0x00d9
            r9 = 13
            if (r6 == r9) goto L_0x00d9
            r9 = 32
            if (r6 == r9) goto L_0x00d9
            goto L_0x00dc
        L_0x00d9:
            int r3 = r3 + -1
            goto L_0x00c4
        L_0x00dc:
            r9 = r13
            r6 = 0
        L_0x00de:
            if (r13 == r3) goto L_0x0122
            int r11 = r13 + 1
            char r13 = r1[r13]
            r15 = 38
            if (r13 == r15) goto L_0x00ec
            r13 = r11
        L_0x00e9:
            r15 = 10
            goto L_0x00de
        L_0x00ec:
            r13 = r11
        L_0x00ed:
            if (r13 == r3) goto L_0x011e
            int r15 = r13 + 1
            char r13 = r1[r13]
            r8 = 59
            if (r13 == r8) goto L_0x00fa
            r13 = r15
            r8 = 1
            goto L_0x00ed
        L_0x00fa:
            com.badlogic.gdx.utils.r0 r6 = r0.f5689d
            int r8 = r11 - r9
            r18 = 1
            int r8 = r8 + -1
            r6.a(r1, r9, r8)
            java.lang.String r6 = new java.lang.String
            int r8 = r15 - r11
            int r8 = r8 + -1
            r6.<init>(r1, r11, r8)
            java.lang.String r8 = r0.a(r6)
            com.badlogic.gdx.utils.r0 r9 = r0.f5689d
            if (r8 == 0) goto L_0x0117
            r6 = r8
        L_0x0117:
            r9.a(r6)
            r9 = r15
            r13 = r9
            r6 = 1
            goto L_0x0120
        L_0x011e:
            r18 = 1
        L_0x0120:
            r8 = 1
            goto L_0x00e9
        L_0x0122:
            r18 = 1
            if (r6 == 0) goto L_0x013e
            if (r9 >= r3) goto L_0x012e
            com.badlogic.gdx.utils.r0 r6 = r0.f5689d
            int r3 = r3 - r9
            r6.a(r1, r9, r3)
        L_0x012e:
            com.badlogic.gdx.utils.r0 r3 = r0.f5689d
            java.lang.String r3 = r3.toString()
            r0.d(r3)
            com.badlogic.gdx.utils.r0 r3 = r0.f5689d
            r6 = 0
            r3.b(r6)
            goto L_0x0148
        L_0x013e:
            r6 = 0
            java.lang.String r8 = new java.lang.String
            int r3 = r3 - r9
            r8.<init>(r1, r9, r3)
            r0.d(r8)
        L_0x0148:
            r13 = r9
            goto L_0x0162
        L_0x014a:
            r6 = 0
            r18 = 1
            java.lang.String r3 = new java.lang.String
            int r8 = r10 - r13
            r3.<init>(r1, r13, r8)
            r0.a(r12, r3)
            goto L_0x0162
        L_0x0158:
            r6 = 0
            r18 = 1
            java.lang.String r12 = new java.lang.String
            int r3 = r10 - r13
            r12.<init>(r1, r13, r3)
        L_0x0162:
            r11 = 2
            goto L_0x023a
        L_0x0165:
            r6 = 0
            r18 = 1
            if (r14 == 0) goto L_0x0162
            goto L_0x0171
        L_0x016b:
            r6 = 0
            r18 = 1
            r22.a()
        L_0x0171:
            r7 = 2
            r8 = 1
            r9 = 2
            r11 = 15
            goto L_0x000f
        L_0x0178:
            r6 = 0
            r18 = 1
            r22.a()
            r7 = 2
            r8 = 1
            r9 = 2
            r11 = 15
            goto L_0x000e
        L_0x0185:
            r6 = 0
            r18 = 1
            char r3 = r1[r13]
            r8 = 63
            r9 = 33
            if (r3 == r8) goto L_0x01a1
            if (r3 != r9) goto L_0x0193
            goto L_0x01a1
        L_0x0193:
            java.lang.String r3 = new java.lang.String
            int r8 = r10 - r13
            r3.<init>(r1, r13, r8)
            r0.b(r3)
            r11 = 2
            r14 = 1
            goto L_0x023a
        L_0x01a1:
            int r4 = r13 + 1
            char r5 = r1[r4]
            r7 = 91
            r8 = 62
            if (r5 != r7) goto L_0x0206
            int r5 = r13 + 2
            char r5 = r1[r5]
            r11 = 67
            if (r5 != r11) goto L_0x0206
            int r5 = r13 + 3
            char r5 = r1[r5]
            r11 = 68
            if (r5 != r11) goto L_0x0206
            int r5 = r13 + 4
            char r5 = r1[r5]
            r11 = 65
            if (r5 != r11) goto L_0x0206
            int r5 = r13 + 5
            char r5 = r1[r5]
            r11 = 84
            if (r5 != r11) goto L_0x0206
            int r5 = r13 + 6
            char r5 = r1[r5]
            r11 = 65
            if (r5 != r11) goto L_0x0206
            int r5 = r13 + 7
            char r5 = r1[r5]
            if (r5 != r7) goto L_0x0206
            int r5 = r13 + 8
            int r3 = r5 + 2
        L_0x01dd:
            int r4 = r3 + -2
            char r4 = r1[r4]
            r7 = 93
            if (r4 != r7) goto L_0x0202
            int r4 = r3 + -1
            char r4 = r1[r4]
            r7 = 93
            if (r4 != r7) goto L_0x0202
            char r4 = r1[r3]
            if (r4 == r8) goto L_0x01f2
            goto L_0x0202
        L_0x01f2:
            java.lang.String r4 = new java.lang.String
            int r7 = r3 - r5
            r11 = 2
            int r7 = r7 - r11
            r4.<init>(r1, r5, r7)
            r0.d(r4)
            r10 = r3
            r13 = r5
            goto L_0x0171
        L_0x0202:
            r11 = 2
            int r3 = r3 + 1
            goto L_0x01dd
        L_0x0206:
            r11 = 2
            if (r3 != r9) goto L_0x022e
            char r3 = r1[r4]
            r4 = 45
            if (r3 != r4) goto L_0x022e
            int r3 = r13 + 2
            char r3 = r1[r3]
            if (r3 != r4) goto L_0x022e
            int r3 = r13 + 3
        L_0x0217:
            char r5 = r1[r3]
            if (r5 != r4) goto L_0x022b
            int r5 = r3 + 1
            char r5 = r1[r5]
            if (r5 != r4) goto L_0x022b
            int r5 = r3 + 2
            char r7 = r1[r5]
            if (r7 == r8) goto L_0x0228
            goto L_0x022b
        L_0x0228:
            r10 = r5
            goto L_0x0171
        L_0x022b:
            int r3 = r3 + 1
            goto L_0x0217
        L_0x022e:
            char r3 = r1[r10]
            if (r3 == r8) goto L_0x0171
            int r10 = r10 + 1
            goto L_0x022e
        L_0x0235:
            r6 = 0
            r11 = 2
            r18 = 1
            r13 = r10
        L_0x023a:
            r3 = r5
            r6 = r7
            r8 = 1
            r15 = 10
            goto L_0x00b0
        L_0x0241:
            if (r4 != 0) goto L_0x0249
            r11 = r4
            r7 = 2
            r8 = 1
        L_0x0246:
            r9 = 5
            goto L_0x000f
        L_0x0249:
            int r10 = r10 + 1
            if (r10 == r2) goto L_0x0253
            r11 = r4
            r7 = 2
            r8 = 1
            r9 = 1
            goto L_0x000f
        L_0x0253:
            if (r10 >= r2) goto L_0x028e
            r3 = 1
        L_0x0256:
            if (r6 >= r10) goto L_0x0263
            char r4 = r1[r6]
            r5 = 10
            if (r4 != r5) goto L_0x0260
            int r3 = r3 + 1
        L_0x0260:
            int r6 = r6 + 1
            goto L_0x0256
        L_0x0263:
            com.badlogic.gdx.utils.l0 r4 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Error parsing XML on line "
            r5.append(r6)
            r5.append(r3)
            java.lang.String r3 = " near: "
            r5.append(r3)
            java.lang.String r3 = new java.lang.String
            int r2 = r2 - r10
            r6 = 32
            int r2 = java.lang.Math.min(r6, r2)
            r3.<init>(r1, r10, r2)
            r5.append(r3)
            java.lang.String r1 = r5.toString()
            r4.<init>(r1)
            throw r4
        L_0x028e:
            com.badlogic.gdx.utils.a<com.badlogic.gdx.utils.x0$a> r1 = r0.f5686a
            int r2 = r1.f5374b
            if (r2 != 0) goto L_0x029a
            com.badlogic.gdx.utils.x0$a r1 = r0.f5687b
            r2 = 0
            r0.f5687b = r2
            return r1
        L_0x029a:
            java.lang.Object r1 = r1.peek()
            com.badlogic.gdx.utils.x0$a r1 = (com.badlogic.gdx.utils.x0.a) r1
            com.badlogic.gdx.utils.a<com.badlogic.gdx.utils.x0$a> r2 = r0.f5686a
            r2.clear()
            com.badlogic.gdx.utils.l0 r2 = new com.badlogic.gdx.utils.l0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Error parsing XML, unclosed element: "
            r3.append(r4)
            java.lang.String r1 = r1.c()
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r2.<init>(r1)
            goto L_0x02c1
        L_0x02c0:
            throw r2
        L_0x02c1:
            goto L_0x02c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.x0.a(char[], int, int):com.badlogic.gdx.utils.x0$a");
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2) {
        this.f5688c.c(str, str2);
    }

    /* access modifiers changed from: protected */
    public String a(String str) {
        if (str.equals("lt")) {
            return "<";
        }
        if (str.equals("gt")) {
            return ">";
        }
        if (str.equals("amp")) {
            return "&";
        }
        if (str.equals("apos")) {
            return "'";
        }
        if (str.equals("quot")) {
            return "\"";
        }
        if (str.startsWith("#x")) {
            return Character.toString((char) Integer.parseInt(str.substring(2), 16));
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.f5687b = this.f5686a.pop();
        a<a> aVar = this.f5686a;
        this.f5688c = aVar.f5374b > 0 ? aVar.peek() : null;
    }
}
