package com.badlogic.gdx.utils;

import com.badlogic.gdx.utils.v;
import com.facebook.internal.Utility;
import e.d.b.s.a;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: UBJsonReader */
public class w0 implements d {

    /* renamed from: a  reason: collision with root package name */
    public boolean f5676a = true;

    public v a(InputStream inputStream) {
        DataInputStream dataInputStream = null;
        try {
            DataInputStream dataInputStream2 = new DataInputStream(inputStream);
            try {
                v a2 = a(dataInputStream2);
                q0.a(dataInputStream2);
                return a2;
            } catch (IOException e2) {
                e = e2;
                dataInputStream = dataInputStream2;
                try {
                    throw new l0(e);
                } catch (Throwable th) {
                    th = th;
                    q0.a(dataInputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                dataInputStream = dataInputStream2;
                q0.a(dataInputStream);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
            throw new l0(e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, long):long
     arg types: [java.io.DataInputStream, int, int]
     candidates:
      com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, byte):java.lang.String
      com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, long):long */
    /* access modifiers changed from: protected */
    public v b(DataInputStream dataInputStream) throws IOException {
        byte b2;
        v vVar = new v(v.d.array);
        byte readByte = dataInputStream.readByte();
        if (readByte == 36) {
            b2 = dataInputStream.readByte();
            readByte = dataInputStream.readByte();
        } else {
            b2 = 0;
        }
        long j2 = -1;
        if (readByte == 35) {
            j2 = a(dataInputStream, false, -1L);
            if (j2 < 0) {
                throw new o("Unrecognized data type");
            } else if (j2 == 0) {
                return vVar;
            } else {
                readByte = b2 == 0 ? dataInputStream.readByte() : b2;
            }
        }
        v vVar2 = null;
        long j3 = 0;
        while (dataInputStream.available() > 0 && readByte != 93) {
            v a2 = a(dataInputStream, readByte);
            a2.f5630i = vVar;
            if (vVar2 != null) {
                a2.f5629h = vVar2;
                vVar2.f5628g = a2;
                vVar.f5631j++;
            } else {
                vVar.f5627f = a2;
                vVar.f5631j = 1;
            }
            if (j2 > 0) {
                j3++;
                if (j3 >= j2) {
                    break;
                }
            }
            vVar2 = a2;
            readByte = b2 == 0 ? dataInputStream.readByte() : b2;
        }
        return vVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, long):long
     arg types: [java.io.DataInputStream, int, int]
     candidates:
      com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, byte):java.lang.String
      com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, byte):java.lang.String
     arg types: [java.io.DataInputStream, int, byte]
     candidates:
      com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, long):long
      com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, byte):java.lang.String */
    /* access modifiers changed from: protected */
    public v c(DataInputStream dataInputStream) throws IOException {
        byte b2;
        v vVar = new v(v.d.object);
        byte readByte = dataInputStream.readByte();
        if (readByte == 36) {
            b2 = dataInputStream.readByte();
            readByte = dataInputStream.readByte();
        } else {
            b2 = 0;
        }
        long j2 = -1;
        if (readByte == 35) {
            j2 = a(dataInputStream, false, -1L);
            if (j2 < 0) {
                throw new o("Unrecognized data type");
            } else if (j2 == 0) {
                return vVar;
            } else {
                readByte = dataInputStream.readByte();
            }
        }
        v vVar2 = null;
        long j3 = 0;
        while (dataInputStream.available() > 0 && readByte != 125) {
            String a2 = a(dataInputStream, true, readByte);
            v a3 = a(dataInputStream, b2 == 0 ? dataInputStream.readByte() : b2);
            a3.m(a2);
            a3.f5630i = vVar;
            if (vVar2 != null) {
                a3.f5629h = vVar2;
                vVar2.f5628g = a3;
                vVar.f5631j++;
            } else {
                vVar.f5627f = a3;
                vVar.f5631j = 1;
            }
            if (j2 > 0) {
                j3++;
                if (j3 >= j2) {
                    break;
                }
            }
            readByte = dataInputStream.readByte();
            vVar2 = a3;
        }
        return vVar;
    }

    /* access modifiers changed from: protected */
    public short d(DataInputStream dataInputStream) throws IOException {
        return (short) (((short) dataInputStream.readByte()) & 255);
    }

    /* access modifiers changed from: protected */
    public long e(DataInputStream dataInputStream) throws IOException {
        return ((long) dataInputStream.readInt()) & -1;
    }

    /* access modifiers changed from: protected */
    public int f(DataInputStream dataInputStream) throws IOException {
        return dataInputStream.readShort() & 65535;
    }

    public v a(a aVar) {
        try {
            return a(aVar.a((int) Utility.DEFAULT_STREAM_BUFFER_SIZE));
        } catch (Exception e2) {
            throw new l0("Error parsing file: " + aVar, e2);
        }
    }

    public v a(DataInputStream dataInputStream) throws IOException {
        try {
            return a(dataInputStream, dataInputStream.readByte());
        } finally {
            q0.a(dataInputStream);
        }
    }

    /* access modifiers changed from: protected */
    public v a(DataInputStream dataInputStream, byte b2) throws IOException {
        if (b2 == 91) {
            return b(dataInputStream);
        }
        if (b2 == 123) {
            return c(dataInputStream);
        }
        if (b2 == 90) {
            return new v(v.d.nullValue);
        }
        if (b2 == 84) {
            return new v(true);
        }
        if (b2 == 70) {
            return new v(false);
        }
        if (b2 == 66) {
            return new v((long) d(dataInputStream));
        }
        if (b2 == 85) {
            return new v((long) d(dataInputStream));
        }
        if (b2 == 105) {
            return new v((long) (this.f5676a ? dataInputStream.readShort() : dataInputStream.readByte()));
        } else if (b2 == 73) {
            return new v((long) (this.f5676a ? dataInputStream.readInt() : dataInputStream.readShort()));
        } else if (b2 == 108) {
            return new v((long) dataInputStream.readInt());
        } else {
            if (b2 == 76) {
                return new v(dataInputStream.readLong());
            }
            if (b2 == 100) {
                return new v((double) dataInputStream.readFloat());
            }
            if (b2 == 68) {
                return new v(dataInputStream.readDouble());
            }
            if (b2 == 115 || b2 == 83) {
                return new v(c(dataInputStream, b2));
            }
            if (b2 == 97 || b2 == 65) {
                return b(dataInputStream, b2);
            }
            if (b2 == 67) {
                return new v((long) dataInputStream.readChar());
            }
            throw new o("Unrecognized data type");
        }
    }

    /* access modifiers changed from: protected */
    public v b(DataInputStream dataInputStream, byte b2) throws IOException {
        byte readByte = dataInputStream.readByte();
        long e2 = b2 == 65 ? e(dataInputStream) : (long) d(dataInputStream);
        v vVar = new v(v.d.array);
        v vVar2 = null;
        long j2 = 0;
        while (j2 < e2) {
            v a2 = a(dataInputStream, readByte);
            a2.f5630i = vVar;
            if (vVar2 != null) {
                vVar2.f5628g = a2;
                vVar.f5631j++;
            } else {
                vVar.f5627f = a2;
                vVar.f5631j = 1;
            }
            j2++;
            vVar2 = a2;
        }
        return vVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, byte):java.lang.String
     arg types: [java.io.DataInputStream, int, byte]
     candidates:
      com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, long):long
      com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, byte):java.lang.String */
    /* access modifiers changed from: protected */
    public String c(DataInputStream dataInputStream, byte b2) throws IOException {
        return a(dataInputStream, false, b2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, long):long
     arg types: [java.io.DataInputStream, int, int]
     candidates:
      com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, byte):java.lang.String
      com.badlogic.gdx.utils.w0.a(java.io.DataInputStream, boolean, long):long */
    /* access modifiers changed from: protected */
    public String a(DataInputStream dataInputStream, boolean z, byte b2) throws IOException {
        long j2 = -1;
        if (b2 == 83) {
            j2 = a(dataInputStream, true, -1L);
        } else if (b2 == 115) {
            j2 = (long) d(dataInputStream);
        } else if (z) {
            j2 = a(dataInputStream, b2, false, -1);
        }
        if (j2 >= 0) {
            return j2 > 0 ? a(dataInputStream, j2) : "";
        }
        throw new o("Unrecognized data type, string expected");
    }

    /* access modifiers changed from: protected */
    public long a(DataInputStream dataInputStream, boolean z, long j2) throws IOException {
        return a(dataInputStream, dataInputStream.readByte(), z, j2);
    }

    /* access modifiers changed from: protected */
    public long a(DataInputStream dataInputStream, byte b2, boolean z, long j2) throws IOException {
        int f2;
        if (b2 == 105) {
            f2 = d(dataInputStream);
        } else if (b2 == 73) {
            f2 = f(dataInputStream);
        } else if (b2 == 108) {
            return e(dataInputStream);
        } else {
            if (b2 == 76) {
                return dataInputStream.readLong();
            }
            return z ? (((long) (((short) b2) & 255)) << 24) | (((long) (((short) dataInputStream.readByte()) & 255)) << 16) | (((long) (((short) dataInputStream.readByte()) & 255)) << 8) | ((long) (((short) dataInputStream.readByte()) & 255)) : j2;
        }
        return (long) f2;
    }

    /* access modifiers changed from: protected */
    public String a(DataInputStream dataInputStream, long j2) throws IOException {
        byte[] bArr = new byte[((int) j2)];
        dataInputStream.readFully(bArr);
        return new String(bArr, "UTF-8");
    }
}
