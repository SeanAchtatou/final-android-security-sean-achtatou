package com.badlogic.gdx.utils;

import com.tapjoy.TapjoyConstants;
import java.util.Arrays;

/* compiled from: StringBuilder */
public class r0 implements Appendable, CharSequence {

    /* renamed from: c  reason: collision with root package name */
    private static final char[] f5580c = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    /* renamed from: a  reason: collision with root package name */
    public char[] f5581a;

    /* renamed from: b  reason: collision with root package name */
    public int f5582b;

    public r0() {
        this.f5581a = new char[16];
    }

    public static int b(long j2, int i2) {
        int i3 = j2 < 0 ? 2 : 1;
        while (true) {
            j2 /= (long) i2;
            if (j2 == 0) {
                return i3;
            }
            i3++;
        }
    }

    private void c(int i2) {
        char[] cArr = this.f5581a;
        int length = (cArr.length >> 1) + cArr.length + 2;
        if (i2 <= length) {
            i2 = length;
        }
        char[] cArr2 = new char[i2];
        System.arraycopy(this.f5581a, 0, cArr2, 0, this.f5582b);
        this.f5581a = cArr2;
    }

    public static int d(int i2, int i3) {
        int i4 = i2 < 0 ? 2 : 1;
        while (true) {
            i2 /= i3;
            if (i2 == 0) {
                return i4;
            }
            i4++;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        int i2 = this.f5582b + 4;
        if (i2 > this.f5581a.length) {
            c(i2);
        }
        char[] cArr = this.f5581a;
        int i3 = this.f5582b;
        this.f5582b = i3 + 1;
        cArr[i3] = 'n';
        int i4 = this.f5582b;
        this.f5582b = i4 + 1;
        cArr[i4] = 'u';
        int i5 = this.f5582b;
        this.f5582b = i5 + 1;
        cArr[i5] = 'l';
        int i6 = this.f5582b;
        this.f5582b = i6 + 1;
        cArr[i6] = 'l';
    }

    public char charAt(int i2) {
        if (i2 >= 0 && i2 < this.f5582b) {
            return this.f5581a[i2];
        }
        throw new StringIndexOutOfBoundsException(i2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || r0.class != obj.getClass()) {
            return false;
        }
        r0 r0Var = (r0) obj;
        int i2 = this.f5582b;
        if (i2 != r0Var.f5582b) {
            return false;
        }
        char[] cArr = this.f5581a;
        char[] cArr2 = r0Var.f5581a;
        for (int i3 = 0; i3 < i2; i3++) {
            if (cArr[i3] != cArr2[i3]) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return ((this.f5582b + 31) * 31) + Arrays.hashCode(this.f5581a);
    }

    public int length() {
        return this.f5582b;
    }

    public CharSequence subSequence(int i2, int i3) {
        return b(i2, i3);
    }

    public String toString() {
        int i2 = this.f5582b;
        if (i2 == 0) {
            return "";
        }
        return new String(this.f5581a, 0, i2);
    }

    /* access modifiers changed from: package-private */
    public final void b(char[] cArr) {
        int length = this.f5582b + cArr.length;
        if (length > this.f5581a.length) {
            c(length);
        }
        System.arraycopy(cArr, 0, this.f5581a, this.f5582b, cArr.length);
        this.f5582b = length;
    }

    public r0(int i2) {
        if (i2 >= 0) {
            this.f5581a = new char[i2];
            return;
        }
        throw new NegativeArraySizeException();
    }

    public r0 append(char c2) {
        a(c2);
        return this;
    }

    private void c(int i2, int i3) {
        char[] cArr = this.f5581a;
        int length = cArr.length;
        int i4 = this.f5582b;
        if (length - i4 >= i2) {
            System.arraycopy(cArr, i3, cArr, i2 + i3, i4 - i3);
            return;
        }
        int i5 = i4 + i2;
        int length2 = (cArr.length << 1) + 2;
        if (i5 > length2) {
            length2 = i5;
        }
        char[] cArr2 = new char[length2];
        System.arraycopy(this.f5581a, 0, cArr2, 0, i3);
        System.arraycopy(this.f5581a, i3, cArr2, i2 + i3, this.f5582b - i3);
        this.f5581a = cArr2;
    }

    public r0 append(CharSequence charSequence) {
        if (charSequence == null) {
            a();
        } else if (charSequence instanceof r0) {
            r0 r0Var = (r0) charSequence;
            b(r0Var.f5581a, 0, r0Var.f5582b);
        } else {
            b(charSequence.toString());
        }
        return this;
    }

    public r0(String str) {
        this.f5582b = str.length();
        int i2 = this.f5582b;
        this.f5581a = new char[(i2 + 16)];
        str.getChars(0, i2, this.f5581a, 0);
    }

    /* access modifiers changed from: package-private */
    public final void b(char[] cArr, int i2, int i3) {
        if (i2 > cArr.length || i2 < 0) {
            throw new ArrayIndexOutOfBoundsException("Offset out of bounds: " + i2);
        } else if (i3 < 0 || cArr.length - i2 < i3) {
            throw new ArrayIndexOutOfBoundsException("Length out of bounds: " + i3);
        } else {
            int i4 = this.f5582b + i3;
            if (i4 > this.f5581a.length) {
                c(i4);
            }
            System.arraycopy(cArr, i2, this.f5581a, this.f5582b, i3);
            this.f5582b = i4;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(char c2) {
        int i2 = this.f5582b;
        if (i2 == this.f5581a.length) {
            c(i2 + 1);
        }
        char[] cArr = this.f5581a;
        int i3 = this.f5582b;
        this.f5582b = i3 + 1;
        cArr[i3] = c2;
    }

    public r0 append(CharSequence charSequence, int i2, int i3) {
        a(charSequence, i2, i3);
        return this;
    }

    /* access modifiers changed from: package-private */
    public final void a(CharSequence charSequence, int i2, int i3) {
        if (charSequence == null) {
            charSequence = "null";
        }
        if (i2 < 0 || i3 < 0 || i2 > i3 || i3 > charSequence.length()) {
            throw new IndexOutOfBoundsException();
        }
        b(charSequence.subSequence(i2, i3).toString());
    }

    public int c(String str) {
        return a(str, 0);
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, String str) {
        if (i2 < 0 || i2 > this.f5582b) {
            throw new StringIndexOutOfBoundsException(i2);
        }
        if (str == null) {
            str = "null";
        }
        int length = str.length();
        if (length != 0) {
            c(length, i2);
            str.getChars(0, length, this.f5581a, i2);
            this.f5582b += length;
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(String str) {
        if (str == null) {
            a();
            return;
        }
        int length = str.length();
        int i2 = this.f5582b + length;
        if (i2 > this.f5581a.length) {
            c(i2);
        }
        str.getChars(0, length, this.f5581a, this.f5582b);
        this.f5582b = i2;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, int i3, String str) {
        if (i2 >= 0) {
            int i4 = this.f5582b;
            if (i3 > i4) {
                i3 = i4;
            }
            if (i3 > i2) {
                int length = str.length();
                int i5 = (i3 - i2) - length;
                if (i5 > 0) {
                    char[] cArr = this.f5581a;
                    System.arraycopy(cArr, i3, cArr, i2 + length, this.f5582b - i3);
                } else if (i5 < 0) {
                    c(-i5, i3);
                }
                str.getChars(0, length, this.f5581a, i2);
                this.f5582b -= i5;
                return;
            } else if (i2 == i3) {
                if (str != null) {
                    a(i2, str);
                    return;
                }
                throw new NullPointerException();
            }
        }
        throw new StringIndexOutOfBoundsException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(char[], int, int, char):void}
     arg types: [char[], int, int, int]
     candidates:
      ClspMth{java.util.Arrays.fill(java.lang.Object[], int, int, java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int, int, int):void}
      ClspMth{java.util.Arrays.fill(boolean[], int, int, boolean):void}
      ClspMth{java.util.Arrays.fill(byte[], int, int, byte):void}
      ClspMth{java.util.Arrays.fill(long[], int, int, long):void}
      ClspMth{java.util.Arrays.fill(double[], int, int, double):void}
      ClspMth{java.util.Arrays.fill(float[], int, int, float):void}
      ClspMth{java.util.Arrays.fill(short[], int, int, short):void}
      ClspMth{java.util.Arrays.fill(char[], int, int, char):void} */
    public void b(int i2) {
        if (i2 >= 0) {
            char[] cArr = this.f5581a;
            if (i2 > cArr.length) {
                c(i2);
            } else {
                int i3 = this.f5582b;
                if (i3 < i2) {
                    Arrays.fill(cArr, i3, i2, 0);
                }
            }
            this.f5582b = i2;
            return;
        }
        throw new StringIndexOutOfBoundsException(i2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x002e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int a(java.lang.String r11, int r12) {
        /*
            r10 = this;
            r0 = 0
            if (r12 >= 0) goto L_0x0004
            r12 = 0
        L_0x0004:
            int r1 = r11.length()
            if (r1 != 0) goto L_0x0012
            int r11 = r10.f5582b
            if (r12 < r11) goto L_0x0010
            if (r12 != 0) goto L_0x0011
        L_0x0010:
            r11 = r12
        L_0x0011:
            return r11
        L_0x0012:
            int r2 = r10.f5582b
            int r2 = r2 - r1
            r3 = -1
            if (r12 <= r2) goto L_0x0019
            return r3
        L_0x0019:
            char r4 = r11.charAt(r0)
        L_0x001d:
            r5 = 1
            if (r12 > r2) goto L_0x002b
            char[] r6 = r10.f5581a
            char r6 = r6[r12]
            if (r6 != r4) goto L_0x0028
            r6 = 1
            goto L_0x002c
        L_0x0028:
            int r12 = r12 + 1
            goto L_0x001d
        L_0x002b:
            r6 = 0
        L_0x002c:
            if (r6 != 0) goto L_0x002f
            return r3
        L_0x002f:
            r7 = r12
            r6 = 0
        L_0x0031:
            int r6 = r6 + r5
            if (r6 >= r1) goto L_0x0040
            char[] r8 = r10.f5581a
            int r7 = r7 + r5
            char r8 = r8[r7]
            char r9 = r11.charAt(r6)
            if (r8 != r9) goto L_0x0040
            goto L_0x0031
        L_0x0040:
            if (r6 != r1) goto L_0x0028
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.r0.a(java.lang.String, int):int");
    }

    public String b(int i2, int i3) {
        if (i2 < 0 || i2 > i3 || i3 > this.f5582b) {
            throw new StringIndexOutOfBoundsException();
        } else if (i2 == i3) {
            return "";
        } else {
            return new String(this.f5581a, i2, i3 - i2);
        }
    }

    public r0 a(boolean z) {
        b(z ? "true" : "false");
        return this;
    }

    public r0 a(int i2) {
        a(i2, 0);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.r0.a(int, int, char):com.badlogic.gdx.utils.r0
     arg types: [int, int, int]
     candidates:
      com.badlogic.gdx.utils.r0.a(long, int, char):com.badlogic.gdx.utils.r0
      com.badlogic.gdx.utils.r0.a(char[], int, int):com.badlogic.gdx.utils.r0
      com.badlogic.gdx.utils.r0.a(int, int, java.lang.String):void
      com.badlogic.gdx.utils.r0.a(java.lang.CharSequence, int, int):void
      com.badlogic.gdx.utils.r0.a(int, int, char):com.badlogic.gdx.utils.r0 */
    public r0 a(int i2, int i3) {
        a(i2, i3, '0');
        return this;
    }

    public r0 a(int i2, int i3, char c2) {
        if (i2 == Integer.MIN_VALUE) {
            b("-2147483648");
            return this;
        }
        if (i2 < 0) {
            a('-');
            i2 = -i2;
        }
        if (i3 > 1) {
            for (int d2 = i3 - d(i2, 10); d2 > 0; d2--) {
                append(c2);
            }
        }
        if (i2 >= 10000) {
            if (i2 >= 1000000000) {
                a(f5580c[(int) ((((long) i2) % 10000000000L) / 1000000000)]);
            }
            if (i2 >= 100000000) {
                a(f5580c[(i2 % 1000000000) / 100000000]);
            }
            if (i2 >= 10000000) {
                a(f5580c[(i2 % 100000000) / 10000000]);
            }
            if (i2 >= 1000000) {
                a(f5580c[(i2 % 10000000) / 1000000]);
            }
            if (i2 >= 100000) {
                a(f5580c[(i2 % 1000000) / 100000]);
            }
            a(f5580c[(i2 % 100000) / 10000]);
        }
        if (i2 >= 1000) {
            a(f5580c[(i2 % 10000) / 1000]);
        }
        if (i2 >= 100) {
            a(f5580c[(i2 % 1000) / 100]);
        }
        if (i2 >= 10) {
            a(f5580c[(i2 % 100) / 10]);
        }
        a(f5580c[i2 % 10]);
        return this;
    }

    public r0 a(long j2) {
        a(j2, 0);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.r0.a(long, int, char):com.badlogic.gdx.utils.r0
     arg types: [long, int, int]
     candidates:
      com.badlogic.gdx.utils.r0.a(int, int, char):com.badlogic.gdx.utils.r0
      com.badlogic.gdx.utils.r0.a(char[], int, int):com.badlogic.gdx.utils.r0
      com.badlogic.gdx.utils.r0.a(int, int, java.lang.String):void
      com.badlogic.gdx.utils.r0.a(java.lang.CharSequence, int, int):void
      com.badlogic.gdx.utils.r0.a(long, int, char):com.badlogic.gdx.utils.r0 */
    public r0 a(long j2, int i2) {
        a(j2, i2, '0');
        return this;
    }

    public r0 a(long j2, int i2, char c2) {
        if (j2 == Long.MIN_VALUE) {
            b("-9223372036854775808");
            return this;
        }
        if (j2 < 0) {
            a('-');
            j2 = -j2;
        }
        if (i2 > 1) {
            for (int b2 = i2 - b(j2, 10); b2 > 0; b2--) {
                append(c2);
            }
        }
        if (j2 >= TapjoyConstants.TIMER_INCREMENT) {
            if (j2 >= 1000000000000000000L) {
                char[] cArr = f5580c;
                double d2 = (double) j2;
                Double.isNaN(d2);
                a(cArr[(int) ((d2 % 1.0E19d) / 1.0E18d)]);
            }
            if (j2 >= 100000000000000000L) {
                a(f5580c[(int) ((j2 % 1000000000000000000L) / 100000000000000000L)]);
            }
            if (j2 >= 10000000000000000L) {
                a(f5580c[(int) ((j2 % 100000000000000000L) / 10000000000000000L)]);
            }
            if (j2 >= 1000000000000000L) {
                a(f5580c[(int) ((j2 % 10000000000000000L) / 1000000000000000L)]);
            }
            if (j2 >= 100000000000000L) {
                a(f5580c[(int) ((j2 % 1000000000000000L) / 100000000000000L)]);
            }
            if (j2 >= 10000000000000L) {
                a(f5580c[(int) ((j2 % 100000000000000L) / 10000000000000L)]);
            }
            if (j2 >= 1000000000000L) {
                a(f5580c[(int) ((j2 % 10000000000000L) / 1000000000000L)]);
            }
            if (j2 >= 100000000000L) {
                a(f5580c[(int) ((j2 % 1000000000000L) / 100000000000L)]);
            }
            if (j2 >= 10000000000L) {
                a(f5580c[(int) ((j2 % 100000000000L) / 10000000000L)]);
            }
            if (j2 >= 1000000000) {
                a(f5580c[(int) ((j2 % 10000000000L) / 1000000000)]);
            }
            if (j2 >= 100000000) {
                a(f5580c[(int) ((j2 % 1000000000) / 100000000)]);
            }
            if (j2 >= 10000000) {
                a(f5580c[(int) ((j2 % 100000000) / 10000000)]);
            }
            if (j2 >= 1000000) {
                a(f5580c[(int) ((j2 % 10000000) / 1000000)]);
            }
            if (j2 >= 100000) {
                a(f5580c[(int) ((j2 % 1000000) / 100000)]);
            }
            a(f5580c[(int) ((j2 % 100000) / TapjoyConstants.TIMER_INCREMENT)]);
        }
        if (j2 >= 1000) {
            a(f5580c[(int) ((j2 % TapjoyConstants.TIMER_INCREMENT) / 1000)]);
        }
        if (j2 >= 100) {
            a(f5580c[(int) ((j2 % 1000) / 100)]);
        }
        if (j2 >= 10) {
            a(f5580c[(int) ((j2 % 100) / 10)]);
        }
        a(f5580c[(int) (j2 % 10)]);
        return this;
    }

    public r0 a(float f2) {
        b(Float.toString(f2));
        return this;
    }

    public r0 a(double d2) {
        b(Double.toString(d2));
        return this;
    }

    public r0 a(Object obj) {
        if (obj == null) {
            a();
        } else {
            b(obj.toString());
        }
        return this;
    }

    public r0 a(String str) {
        b(str);
        return this;
    }

    public r0 a(char[] cArr) {
        b(cArr);
        return this;
    }

    public r0 a(char[] cArr, int i2, int i3) {
        b(cArr, i2, i3);
        return this;
    }

    public r0 a(r0 r0Var) {
        if (r0Var == null) {
            a();
        } else {
            b(r0Var.f5581a, 0, r0Var.f5582b);
        }
        return this;
    }

    public r0 a(String str, String str2) {
        int length = str.length();
        int length2 = str2.length();
        int i2 = 0;
        while (true) {
            int a2 = a(str, i2);
            if (a2 == -1) {
                return this;
            }
            a(a2, a2 + length, str2);
            i2 = a2 + length2;
        }
    }

    public r0 a(char c2, String str) {
        int length = str.length();
        int i2 = 0;
        while (i2 != this.f5582b) {
            if (this.f5581a[i2] == c2) {
                a(i2, i2 + 1, str);
                i2 += length;
            } else {
                i2++;
            }
        }
        return this;
    }
}
