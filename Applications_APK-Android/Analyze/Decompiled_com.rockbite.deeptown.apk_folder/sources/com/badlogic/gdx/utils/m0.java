package com.badlogic.gdx.utils;

import com.appsflyer.share.Constants;
import com.google.protobuf.CodedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* compiled from: SharedLibraryLoader */
public class m0 {

    /* renamed from: b  reason: collision with root package name */
    public static boolean f5530b;

    /* renamed from: c  reason: collision with root package name */
    public static boolean f5531c;

    /* renamed from: d  reason: collision with root package name */
    public static boolean f5532d;

    /* renamed from: e  reason: collision with root package name */
    public static boolean f5533e;

    /* renamed from: f  reason: collision with root package name */
    public static boolean f5534f;

    /* renamed from: g  reason: collision with root package name */
    public static boolean f5535g = System.getProperty("os.arch").startsWith("arm");

    /* renamed from: h  reason: collision with root package name */
    public static boolean f5536h;

    /* renamed from: i  reason: collision with root package name */
    public static String f5537i = (System.getProperty("sun.arch.abi") != null ? System.getProperty("sun.arch.abi") : "");

    /* renamed from: j  reason: collision with root package name */
    private static final HashSet<String> f5538j = new HashSet<>();

    /* renamed from: a  reason: collision with root package name */
    private String f5539a;

    static {
        f5530b = System.getProperty("os.name").contains("Windows");
        f5531c = System.getProperty("os.name").contains("Linux");
        f5532d = System.getProperty("os.name").contains("Mac");
        f5533e = false;
        f5534f = false;
        f5536h = System.getProperty("os.arch").equals("amd64") || System.getProperty("os.arch").equals("x86_64");
        boolean equals = "iOS".equals(System.getProperty("moe.platform.name"));
        String property = System.getProperty("java.runtime.name");
        if (property != null && property.contains("Android Runtime")) {
            f5534f = true;
            f5530b = false;
            f5531c = false;
            f5532d = false;
            f5536h = false;
        }
        if (equals || (!f5534f && !f5530b && !f5531c && !f5532d)) {
            f5533e = true;
            f5534f = false;
            f5530b = false;
            f5531c = false;
            f5532d = false;
            f5536h = false;
        }
    }

    public static synchronized boolean c(String str) {
        boolean contains;
        synchronized (m0.class) {
            contains = f5538j.contains(str);
        }
        return contains;
    }

    private void d(String str) {
        String a2 = a(e(str));
        String name = new File(str).getName();
        Throwable b2 = b(str, a2, new File(System.getProperty("java.io.tmpdir") + "/libgdx" + System.getProperty("user.name") + Constants.URL_PATH_DELIMITER + a2, name));
        if (b2 != null) {
            try {
                File createTempFile = File.createTempFile(a2, null);
                if (createTempFile.delete() && b(str, a2, createTempFile) == null) {
                    return;
                }
            } catch (Throwable unused) {
            }
            if (b(str, a2, new File(System.getProperty("user.home") + "/.libgdx/" + a2, name)) != null) {
                if (b(str, a2, new File(".temp/" + a2, name)) != null) {
                    File file = new File(System.getProperty("java.library.path"), str);
                    if (file.exists()) {
                        System.load(file.getAbsolutePath());
                        return;
                    }
                    throw new o(b2);
                }
            }
        }
    }

    private InputStream e(String str) {
        String str2 = this.f5539a;
        if (str2 == null) {
            InputStream resourceAsStream = m0.class.getResourceAsStream(Constants.URL_PATH_DELIMITER + str);
            if (resourceAsStream != null) {
                return resourceAsStream;
            }
            throw new o("Unable to read file for extraction: " + str);
        }
        try {
            ZipFile zipFile = new ZipFile(str2);
            ZipEntry entry = zipFile.getEntry(str);
            if (entry != null) {
                return zipFile.getInputStream(entry);
            }
            throw new o("Couldn't find '" + str + "' in JAR: " + this.f5539a);
        } catch (IOException e2) {
            throw new o("Error reading '" + str + "' in JAR: " + this.f5539a, e2);
        }
    }

    public static synchronized void f(String str) {
        synchronized (m0.class) {
            f5538j.add(str);
        }
    }

    public String a(InputStream inputStream) {
        if (inputStream != null) {
            CRC32 crc32 = new CRC32();
            byte[] bArr = new byte[CodedOutputStream.DEFAULT_BUFFER_SIZE];
            while (true) {
                try {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    crc32.update(bArr, 0, read);
                } catch (Exception unused) {
                } catch (Throwable th) {
                    q0.a(inputStream);
                    throw th;
                }
            }
            q0.a(inputStream);
            return Long.toString(crc32.getValue(), 16);
        }
        throw new IllegalArgumentException("input cannot be null.");
    }

    public String b(String str) {
        String str2;
        if (f5530b) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(f5536h ? "64.dll" : ".dll");
            return sb.toString();
        } else if (f5531c) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("lib");
            sb2.append(str);
            if (f5535g) {
                str2 = "arm" + f5537i;
            } else {
                str2 = "";
            }
            sb2.append(str2);
            sb2.append(f5536h ? "64.so" : ".so");
            return sb2.toString();
        } else if (!f5532d) {
            return str;
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("lib");
            sb3.append(str);
            sb3.append(f5536h ? "64.dylib" : ".dylib");
            return sb3.toString();
        }
    }

    private Throwable b(String str, String str2, File file) {
        try {
            a(str, str2, file);
            System.load(file.getAbsolutePath());
            return null;
        } catch (Throwable th) {
            return th;
        }
    }

    public void a(String str) {
        if (!f5533e) {
            synchronized (m0.class) {
                if (!c(str)) {
                    String b2 = b(str);
                    try {
                        if (f5534f) {
                            System.loadLibrary(b2);
                        } else {
                            d(b2);
                        }
                        f(str);
                    } catch (Throwable th) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Couldn't load shared library '");
                        sb.append(b2);
                        sb.append("' for target: ");
                        sb.append(System.getProperty("os.name"));
                        sb.append(f5536h ? ", 64-bit" : ", 32-bit");
                        throw new o(sb.toString(), th);
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0035 A[EDGE_INSN: B:42:0x0035->B:19:0x0035 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.io.File a(java.lang.String r7, java.lang.String r8, java.io.File r9) throws java.io.IOException {
        /*
            r6 = this;
            boolean r0 = r9.exists()
            r1 = 0
            if (r0 == 0) goto L_0x0011
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0011 }
            r0.<init>(r9)     // Catch:{ FileNotFoundException -> 0x0011 }
            java.lang.String r0 = r6.a(r0)     // Catch:{ FileNotFoundException -> 0x0011 }
            goto L_0x0012
        L_0x0011:
            r0 = r1
        L_0x0012:
            if (r0 == 0) goto L_0x001a
            boolean r8 = r0.equals(r8)
            if (r8 != 0) goto L_0x003b
        L_0x001a:
            java.io.InputStream r8 = r6.e(r7)     // Catch:{ IOException -> 0x0058, all -> 0x0055 }
            java.io.File r0 = r9.getParentFile()     // Catch:{ IOException -> 0x0050, all -> 0x004b }
            r0.mkdirs()     // Catch:{ IOException -> 0x0050, all -> 0x004b }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0050, all -> 0x004b }
            r0.<init>(r9)     // Catch:{ IOException -> 0x0050, all -> 0x004b }
            r1 = 4096(0x1000, float:5.74E-42)
            byte[] r1 = new byte[r1]     // Catch:{ IOException -> 0x0045, all -> 0x0041 }
        L_0x002e:
            int r2 = r8.read(r1)     // Catch:{ IOException -> 0x0045, all -> 0x0041 }
            r3 = -1
            if (r2 != r3) goto L_0x003c
            com.badlogic.gdx.utils.q0.a(r8)
            com.badlogic.gdx.utils.q0.a(r0)
        L_0x003b:
            return r9
        L_0x003c:
            r3 = 0
            r0.write(r1, r3, r2)     // Catch:{ IOException -> 0x0045, all -> 0x0041 }
            goto L_0x002e
        L_0x0041:
            r7 = move-exception
            r1 = r8
            r8 = r0
            goto L_0x007f
        L_0x0045:
            r1 = move-exception
            r5 = r1
            r1 = r8
            r8 = r0
            r0 = r5
            goto L_0x005b
        L_0x004b:
            r7 = move-exception
            r5 = r1
            r1 = r8
            r8 = r5
            goto L_0x007f
        L_0x0050:
            r0 = move-exception
            r5 = r1
            r1 = r8
            r8 = r5
            goto L_0x005b
        L_0x0055:
            r7 = move-exception
            r8 = r1
            goto L_0x007f
        L_0x0058:
            r8 = move-exception
            r0 = r8
            r8 = r1
        L_0x005b:
            com.badlogic.gdx.utils.o r2 = new com.badlogic.gdx.utils.o     // Catch:{ all -> 0x007e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x007e }
            r3.<init>()     // Catch:{ all -> 0x007e }
            java.lang.String r4 = "Error extracting file: "
            r3.append(r4)     // Catch:{ all -> 0x007e }
            r3.append(r7)     // Catch:{ all -> 0x007e }
            java.lang.String r7 = "\nTo: "
            r3.append(r7)     // Catch:{ all -> 0x007e }
            java.lang.String r7 = r9.getAbsolutePath()     // Catch:{ all -> 0x007e }
            r3.append(r7)     // Catch:{ all -> 0x007e }
            java.lang.String r7 = r3.toString()     // Catch:{ all -> 0x007e }
            r2.<init>(r7, r0)     // Catch:{ all -> 0x007e }
            throw r2     // Catch:{ all -> 0x007e }
        L_0x007e:
            r7 = move-exception
        L_0x007f:
            com.badlogic.gdx.utils.q0.a(r1)
            com.badlogic.gdx.utils.q0.a(r8)
            goto L_0x0087
        L_0x0086:
            throw r7
        L_0x0087:
            goto L_0x0086
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.m0.a(java.lang.String, java.lang.String, java.io.File):java.io.File");
    }
}
