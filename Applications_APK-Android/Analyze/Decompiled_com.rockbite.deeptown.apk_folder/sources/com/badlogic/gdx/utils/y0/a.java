package com.badlogic.gdx.utils.y0;

import com.badlogic.gdx.utils.l;
import com.badlogic.gdx.utils.o;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/* compiled from: AsyncExecutor */
public class a implements l {

    /* renamed from: a  reason: collision with root package name */
    private final ExecutorService f5713a;

    /* renamed from: com.badlogic.gdx.utils.y0.a$a  reason: collision with other inner class name */
    /* compiled from: AsyncExecutor */
    class C0124a implements ThreadFactory {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f5714a;

        C0124a(a aVar, String str) {
            this.f5714a = str;
        }

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, this.f5714a);
            thread.setDaemon(true);
            return thread;
        }
    }

    /* compiled from: AsyncExecutor */
    class b implements Callable<T> {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ c f5715a;

        b(a aVar, c cVar) {
            this.f5715a = cVar;
        }

        public T call() throws Exception {
            return this.f5715a.call();
        }
    }

    public a(int i2, String str) {
        this.f5713a = Executors.newFixedThreadPool(i2, new C0124a(this, str));
    }

    public <T> b<T> a(c<T> cVar) {
        if (!this.f5713a.isShutdown()) {
            return new b<>(this.f5713a.submit(new b(this, cVar)));
        }
        throw new o("Cannot run tasks on an executor that has been shutdown (disposed)");
    }

    public void dispose() {
        this.f5713a.shutdown();
        try {
            this.f5713a.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e2) {
            throw new o("Couldn't shutdown loading thread", e2);
        }
    }
}
