package com.badlogic.gdx.utils;

import java.text.MessageFormat;
import java.util.Locale;

/* compiled from: TextFormatter */
class s0 {

    /* renamed from: a  reason: collision with root package name */
    private MessageFormat f5598a;

    /* renamed from: b  reason: collision with root package name */
    private r0 f5599b = new r0();

    public s0(Locale locale, boolean z) {
        if (z) {
            this.f5598a = new MessageFormat("", locale);
        }
    }

    private String b(String str, Object... objArr) {
        this.f5599b.b(0);
        int length = str.length();
        int i2 = 0;
        int i3 = -1;
        boolean z = false;
        while (i2 < length) {
            char charAt = str.charAt(i2);
            if (i3 < 0) {
                if (charAt == '{') {
                    int i4 = i2 + 1;
                    if (i4 >= length || str.charAt(i4) != '{') {
                        i3 = 0;
                    } else {
                        this.f5599b.append(charAt);
                        i2 = i4;
                    }
                    z = true;
                } else {
                    this.f5599b.append(charAt);
                }
            } else if (charAt == '}') {
                if (i3 >= objArr.length) {
                    throw new IllegalArgumentException("Argument index out of bounds: " + i3);
                } else if (str.charAt(i2 - 1) != '{') {
                    if (objArr[i3] == null) {
                        this.f5599b.a("null");
                    } else {
                        this.f5599b.a(objArr[i3].toString());
                    }
                    i3 = -1;
                } else {
                    throw new IllegalArgumentException("Missing argument index after a left curly brace");
                }
            } else if (charAt < '0' || charAt > '9') {
                throw new IllegalArgumentException("Unexpected '" + charAt + "' while parsing argument index");
            } else {
                i3 = (i3 * 10) + (charAt - '0');
            }
            i2++;
        }
        if (i3 < 0) {
            return z ? this.f5599b.toString() : str;
        }
        throw new IllegalArgumentException("Unmatched braces in the pattern.");
    }

    public String a(String str, Object... objArr) {
        MessageFormat messageFormat = this.f5598a;
        if (messageFormat == null) {
            return b(str, objArr);
        }
        messageFormat.applyPattern(a(str));
        return this.f5598a.format(objArr);
    }

    private String a(String str) {
        int i2 = 0;
        this.f5599b.b(0);
        int length = str.length();
        boolean z = false;
        while (i2 < length) {
            char charAt = str.charAt(i2);
            if (charAt == '\'') {
                this.f5599b.a("''");
                z = true;
            } else if (charAt == '{') {
                int i3 = i2 + 1;
                while (i3 < length && str.charAt(i3) == '{') {
                    i3++;
                }
                int i4 = i3 - i2;
                int i5 = i4 / 2;
                if (i5 > 0) {
                    this.f5599b.append('\'');
                    do {
                        this.f5599b.append('{');
                        i5--;
                    } while (i5 > 0);
                    this.f5599b.append('\'');
                    z = true;
                }
                if (i4 % 2 != 0) {
                    this.f5599b.append('{');
                }
                i2 = i3 - 1;
            } else {
                this.f5599b.append(charAt);
            }
            i2++;
        }
        return z ? this.f5599b.toString() : str;
    }
}
