package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.h;
import com.esotericsoftware.spine.Animation;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: ObjectIntMap */
public class b0<K> implements Iterable<b<K>> {

    /* renamed from: a  reason: collision with root package name */
    public int f5424a;

    /* renamed from: b  reason: collision with root package name */
    K[] f5425b;

    /* renamed from: c  reason: collision with root package name */
    int[] f5426c;

    /* renamed from: d  reason: collision with root package name */
    int f5427d;

    /* renamed from: e  reason: collision with root package name */
    int f5428e;

    /* renamed from: f  reason: collision with root package name */
    private float f5429f;

    /* renamed from: g  reason: collision with root package name */
    private int f5430g;

    /* renamed from: h  reason: collision with root package name */
    private int f5431h;

    /* renamed from: i  reason: collision with root package name */
    private int f5432i;

    /* renamed from: j  reason: collision with root package name */
    private int f5433j;

    /* renamed from: k  reason: collision with root package name */
    private int f5434k;
    private a l;
    private a m;

    /* compiled from: ObjectIntMap */
    public static class a<K> extends c<K> implements Iterable<b<K>>, Iterator<b<K>> {

        /* renamed from: f  reason: collision with root package name */
        private b<K> f5435f = new b<>();

        public a(b0<K> b0Var) {
            super(b0Var);
        }

        public boolean hasNext() {
            if (this.f5442e) {
                return this.f5438a;
            }
            throw new o("#iterator() cannot be used nested.");
        }

        public a<K> iterator() {
            return this;
        }

        public void remove() {
            super.remove();
        }

        public b<K> next() {
            if (!this.f5438a) {
                throw new NoSuchElementException();
            } else if (this.f5442e) {
                b0<K> b0Var = this.f5439b;
                K[] kArr = b0Var.f5425b;
                b<K> bVar = this.f5435f;
                int i2 = this.f5440c;
                bVar.f5436a = kArr[i2];
                bVar.f5437b = b0Var.f5426c[i2];
                this.f5441d = i2;
                a();
                return this.f5435f;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }
    }

    /* compiled from: ObjectIntMap */
    public static class b<K> {

        /* renamed from: a  reason: collision with root package name */
        public K f5436a;

        /* renamed from: b  reason: collision with root package name */
        public int f5437b;

        public String toString() {
            return ((Object) this.f5436a) + "=" + this.f5437b;
        }
    }

    /* compiled from: ObjectIntMap */
    private static class c<K> {

        /* renamed from: a  reason: collision with root package name */
        public boolean f5438a;

        /* renamed from: b  reason: collision with root package name */
        final b0<K> f5439b;

        /* renamed from: c  reason: collision with root package name */
        int f5440c;

        /* renamed from: d  reason: collision with root package name */
        int f5441d;

        /* renamed from: e  reason: collision with root package name */
        boolean f5442e = true;

        public c(b0<K> b0Var) {
            this.f5439b = b0Var;
            b();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f5438a = false;
            b0<K> b0Var = this.f5439b;
            K[] kArr = b0Var.f5425b;
            int i2 = b0Var.f5427d + b0Var.f5428e;
            do {
                int i3 = this.f5440c + 1;
                this.f5440c = i3;
                if (i3 >= i2) {
                    return;
                }
            } while (kArr[this.f5440c] == null);
            this.f5438a = true;
        }

        public void b() {
            this.f5441d = -1;
            this.f5440c = -1;
            a();
        }

        public void remove() {
            int i2 = this.f5441d;
            if (i2 >= 0) {
                b0<K> b0Var = this.f5439b;
                if (i2 >= b0Var.f5427d) {
                    b0Var.c(i2);
                    this.f5440c = this.f5441d - 1;
                    a();
                } else {
                    b0Var.f5425b[i2] = null;
                }
                this.f5441d = -1;
                b0<K> b0Var2 = this.f5439b;
                b0Var2.f5424a--;
                return;
            }
            throw new IllegalStateException("next must be called before remove.");
        }
    }

    public b0() {
        this(51, 0.8f);
    }

    private void a(K k2, int i2, int i3, K k3, int i4, K k4, int i5, K k5) {
        K[] kArr = this.f5425b;
        int[] iArr = this.f5426c;
        int i6 = this.f5431h;
        int i7 = this.f5434k;
        K k6 = k2;
        int i8 = i2;
        int i9 = i3;
        K k7 = k3;
        int i10 = i4;
        K k8 = k4;
        int i11 = i5;
        K k9 = k5;
        int i12 = 0;
        do {
            int c2 = h.c(2);
            if (c2 == 0) {
                int i13 = iArr[i9];
                kArr[i9] = k6;
                iArr[i9] = i8;
                i8 = i13;
                k6 = k7;
            } else if (c2 != 1) {
                int i14 = iArr[i11];
                kArr[i11] = k6;
                iArr[i11] = i8;
                k6 = k9;
                i8 = i14;
            } else {
                int i15 = iArr[i10];
                kArr[i10] = k6;
                iArr[i10] = i8;
                i8 = i15;
                k6 = k8;
            }
            int hashCode = k6.hashCode();
            i9 = hashCode & i6;
            k7 = kArr[i9];
            if (k7 == null) {
                kArr[i9] = k6;
                iArr[i9] = i8;
                int i16 = this.f5424a;
                this.f5424a = i16 + 1;
                if (i16 >= this.f5432i) {
                    f(this.f5427d << 1);
                    return;
                }
                return;
            }
            i10 = d(hashCode);
            k8 = kArr[i10];
            if (k8 == null) {
                kArr[i10] = k6;
                iArr[i10] = i8;
                int i17 = this.f5424a;
                this.f5424a = i17 + 1;
                if (i17 >= this.f5432i) {
                    f(this.f5427d << 1);
                    return;
                }
                return;
            }
            i11 = e(hashCode);
            k9 = kArr[i11];
            if (k9 == null) {
                kArr[i11] = k6;
                iArr[i11] = i8;
                int i18 = this.f5424a;
                this.f5424a = i18 + 1;
                if (i18 >= this.f5432i) {
                    f(this.f5427d << 1);
                    return;
                }
                return;
            }
            i12++;
        } while (i12 != i7);
        e(k6, i8);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private int c(K r5, int r6) {
        /*
            r4 = this;
            K[] r0 = r4.f5425b
            int r1 = r4.f5427d
            int r2 = r4.f5428e
            int r2 = r2 + r1
        L_0x0007:
            if (r1 >= r2) goto L_0x0019
            r3 = r0[r1]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0016
            int[] r5 = r4.f5426c
            r5 = r5[r1]
            return r5
        L_0x0016:
            int r1 = r1 + 1
            goto L_0x0007
        L_0x0019:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.b0.c(java.lang.Object, int):int");
    }

    private void d(K k2, int i2) {
        int hashCode = k2.hashCode();
        int i3 = hashCode & this.f5431h;
        K[] kArr = this.f5425b;
        K k3 = kArr[i3];
        if (k3 == null) {
            kArr[i3] = k2;
            this.f5426c[i3] = i2;
            int i4 = this.f5424a;
            this.f5424a = i4 + 1;
            if (i4 >= this.f5432i) {
                f(this.f5427d << 1);
                return;
            }
            return;
        }
        int d2 = d(hashCode);
        K[] kArr2 = this.f5425b;
        K k4 = kArr2[d2];
        if (k4 == null) {
            kArr2[d2] = k2;
            this.f5426c[d2] = i2;
            int i5 = this.f5424a;
            this.f5424a = i5 + 1;
            if (i5 >= this.f5432i) {
                f(this.f5427d << 1);
                return;
            }
            return;
        }
        int e2 = e(hashCode);
        K[] kArr3 = this.f5425b;
        K k5 = kArr3[e2];
        if (k5 == null) {
            kArr3[e2] = k2;
            this.f5426c[e2] = i2;
            int i6 = this.f5424a;
            this.f5424a = i6 + 1;
            if (i6 >= this.f5432i) {
                f(this.f5427d << 1);
                return;
            }
            return;
        }
        a(k2, i2, i3, k3, d2, k4, e2, k5);
    }

    private void e(K k2, int i2) {
        int i3 = this.f5428e;
        if (i3 == this.f5433j) {
            f(this.f5427d << 1);
            d(k2, i2);
            return;
        }
        int i4 = this.f5427d + i3;
        this.f5425b[i4] = k2;
        this.f5426c[i4] = i2;
        this.f5428e = i3 + 1;
        this.f5424a++;
    }

    private void f(int i2) {
        int i3 = this.f5427d + this.f5428e;
        this.f5427d = i2;
        this.f5432i = (int) (((float) i2) * this.f5429f);
        this.f5431h = i2 - 1;
        this.f5430g = 31 - Integer.numberOfTrailingZeros(i2);
        double d2 = (double) i2;
        this.f5433j = Math.max(3, ((int) Math.ceil(Math.log(d2))) * 2);
        this.f5434k = Math.max(Math.min(i2, 8), ((int) Math.sqrt(d2)) / 8);
        K[] kArr = this.f5425b;
        int[] iArr = this.f5426c;
        int i4 = this.f5433j;
        this.f5425b = new Object[(i2 + i4)];
        this.f5426c = new int[(i2 + i4)];
        int i5 = this.f5424a;
        this.f5424a = 0;
        this.f5428e = 0;
        if (i5 > 0) {
            for (int i6 = 0; i6 < i3; i6++) {
                K k2 = kArr[i6];
                if (k2 != null) {
                    d(k2, iArr[i6]);
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void b(K r13, int r14) {
        /*
            r12 = this;
            if (r13 == 0) goto L_0x00a7
            K[] r0 = r12.f5425b
            int r1 = r13.hashCode()
            int r2 = r12.f5431h
            r6 = r1 & r2
            r7 = r0[r6]
            boolean r2 = r13.equals(r7)
            if (r2 == 0) goto L_0x0019
            int[] r13 = r12.f5426c
            r13[r6] = r14
            return
        L_0x0019:
            int r8 = r12.d(r1)
            r9 = r0[r8]
            boolean r2 = r13.equals(r9)
            if (r2 == 0) goto L_0x002a
            int[] r13 = r12.f5426c
            r13[r8] = r14
            return
        L_0x002a:
            int r10 = r12.e(r1)
            r11 = r0[r10]
            boolean r1 = r13.equals(r11)
            if (r1 == 0) goto L_0x003b
            int[] r13 = r12.f5426c
            r13[r10] = r14
            return
        L_0x003b:
            int r1 = r12.f5427d
            int r2 = r12.f5428e
            int r2 = r2 + r1
        L_0x0040:
            if (r1 >= r2) goto L_0x0052
            r3 = r0[r1]
            boolean r3 = r13.equals(r3)
            if (r3 == 0) goto L_0x004f
            int[] r13 = r12.f5426c
            r13[r1] = r14
            return
        L_0x004f:
            int r1 = r1 + 1
            goto L_0x0040
        L_0x0052:
            if (r7 != 0) goto L_0x006c
            r0[r6] = r13
            int[] r13 = r12.f5426c
            r13[r6] = r14
            int r13 = r12.f5424a
            int r14 = r13 + 1
            r12.f5424a = r14
            int r14 = r12.f5432i
            if (r13 < r14) goto L_0x006b
            int r13 = r12.f5427d
            int r13 = r13 << 1
            r12.f(r13)
        L_0x006b:
            return
        L_0x006c:
            if (r9 != 0) goto L_0x0086
            r0[r8] = r13
            int[] r13 = r12.f5426c
            r13[r8] = r14
            int r13 = r12.f5424a
            int r14 = r13 + 1
            r12.f5424a = r14
            int r14 = r12.f5432i
            if (r13 < r14) goto L_0x0085
            int r13 = r12.f5427d
            int r13 = r13 << 1
            r12.f(r13)
        L_0x0085:
            return
        L_0x0086:
            if (r11 != 0) goto L_0x00a0
            r0[r10] = r13
            int[] r13 = r12.f5426c
            r13[r10] = r14
            int r13 = r12.f5424a
            int r14 = r13 + 1
            r12.f5424a = r14
            int r14 = r12.f5432i
            if (r13 < r14) goto L_0x009f
            int r13 = r12.f5427d
            int r13 = r13 << 1
            r12.f(r13)
        L_0x009f:
            return
        L_0x00a0:
            r3 = r12
            r4 = r13
            r5 = r14
            r3.a(r4, r5, r6, r7, r8, r9, r10, r11)
            return
        L_0x00a7:
            java.lang.IllegalArgumentException r13 = new java.lang.IllegalArgumentException
            java.lang.String r14 = "key cannot be null."
            r13.<init>(r14)
            goto L_0x00b0
        L_0x00af:
            throw r13
        L_0x00b0:
            goto L_0x00af
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.b0.b(java.lang.Object, int):void");
    }

    public void clear() {
        if (this.f5424a != 0) {
            K[] kArr = this.f5425b;
            int i2 = this.f5427d + this.f5428e;
            while (true) {
                int i3 = i2 - 1;
                if (i2 > 0) {
                    kArr[i3] = null;
                    i2 = i3;
                } else {
                    this.f5424a = 0;
                    this.f5428e = 0;
                    return;
                }
            }
        }
    }

    public boolean equals(Object obj) {
        int a2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof b0)) {
            return false;
        }
        b0 b0Var = (b0) obj;
        if (b0Var.f5424a != this.f5424a) {
            return false;
        }
        K[] kArr = this.f5425b;
        int[] iArr = this.f5426c;
        int i2 = this.f5427d + this.f5428e;
        for (int i3 = 0; i3 < i2; i3++) {
            K k2 = kArr[i3];
            if (k2 != null && (((a2 = b0Var.a(k2, 0)) == 0 && !b0Var.a(k2)) || a2 != iArr[i3])) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        K[] kArr = this.f5425b;
        int[] iArr = this.f5426c;
        int i2 = this.f5427d + this.f5428e;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            K k2 = kArr[i4];
            if (k2 != null) {
                i3 = i3 + (k2.hashCode() * 31) + iArr[i4];
            }
        }
        return i3;
    }

    public String toString() {
        int i2;
        if (this.f5424a == 0) {
            return "{}";
        }
        r0 r0Var = new r0(32);
        r0Var.append('{');
        K[] kArr = this.f5425b;
        int[] iArr = this.f5426c;
        int length = kArr.length;
        while (true) {
            i2 = length - 1;
            if (length > 0) {
                K k2 = kArr[i2];
                if (k2 != null) {
                    r0Var.a((Object) k2);
                    r0Var.append('=');
                    r0Var.a(iArr[i2]);
                    break;
                }
                length = i2;
            } else {
                break;
            }
        }
        while (true) {
            int i3 = i2 - 1;
            if (i2 > 0) {
                K k3 = kArr[i3];
                if (k3 != null) {
                    r0Var.a(", ");
                    r0Var.a((Object) k3);
                    r0Var.append('=');
                    r0Var.a(iArr[i3]);
                }
                i2 = i3;
            } else {
                r0Var.append('}');
                return r0Var.toString();
            }
        }
    }

    public b0(int i2, float f2) {
        if (i2 >= 0) {
            int b2 = h.b((int) Math.ceil((double) (((float) i2) / f2)));
            if (b2 <= 1073741824) {
                this.f5427d = b2;
                if (f2 > Animation.CurveTimeline.LINEAR) {
                    this.f5429f = f2;
                    int i3 = this.f5427d;
                    this.f5432i = (int) (((float) i3) * f2);
                    this.f5431h = i3 - 1;
                    this.f5430g = 31 - Integer.numberOfTrailingZeros(i3);
                    this.f5433j = Math.max(3, ((int) Math.ceil(Math.log((double) this.f5427d))) * 2);
                    this.f5434k = Math.max(Math.min(this.f5427d, 8), ((int) Math.sqrt((double) this.f5427d)) / 8);
                    this.f5425b = new Object[(this.f5427d + this.f5433j)];
                    this.f5426c = new int[this.f5425b.length];
                    return;
                }
                throw new IllegalArgumentException("loadFactor must be > 0: " + f2);
            }
            throw new IllegalArgumentException("initialCapacity is too large: " + b2);
        }
        throw new IllegalArgumentException("initialCapacity must be >= 0: " + i2);
    }

    public a<K> iterator() {
        return a();
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        this.f5428e--;
        int i3 = this.f5427d + this.f5428e;
        if (i2 < i3) {
            K[] kArr = this.f5425b;
            kArr[i2] = kArr[i3];
            int[] iArr = this.f5426c;
            iArr[i2] = iArr[i3];
            kArr[i3] = null;
        }
    }

    private int e(int i2) {
        int i3 = i2 * -825114047;
        return (i3 ^ (i3 >>> this.f5430g)) & this.f5431h;
    }

    private int d(int i2) {
        int i3 = i2 * -1262997959;
        return (i3 ^ (i3 >>> this.f5430g)) & this.f5431h;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private boolean b(K r5) {
        /*
            r4 = this;
            K[] r0 = r4.f5425b
            int r1 = r4.f5427d
            int r2 = r4.f5428e
            int r2 = r2 + r1
        L_0x0007:
            if (r1 >= r2) goto L_0x0016
            r3 = r0[r1]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0013
            r5 = 1
            return r5
        L_0x0013:
            int r1 = r1 + 1
            goto L_0x0007
        L_0x0016:
            r5 = 0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.b0.b(java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int a(K r4, int r5) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            int r1 = r3.f5431h
            r1 = r1 & r0
            K[] r2 = r3.f5425b
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0032
            int r1 = r3.d(r0)
            K[] r2 = r3.f5425b
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0032
            int r1 = r3.e(r0)
            K[] r0 = r3.f5425b
            r0 = r0[r1]
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x0032
            int r4 = r3.c(r4, r5)
            return r4
        L_0x0032:
            int[] r4 = r3.f5426c
            r4 = r4[r1]
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.b0.a(java.lang.Object, int):int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean a(K r4) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            int r1 = r3.f5431h
            r1 = r1 & r0
            K[] r2 = r3.f5425b
            r1 = r2[r1]
            boolean r1 = r4.equals(r1)
            if (r1 != 0) goto L_0x0032
            int r1 = r3.d(r0)
            K[] r2 = r3.f5425b
            r1 = r2[r1]
            boolean r1 = r4.equals(r1)
            if (r1 != 0) goto L_0x0032
            int r0 = r3.e(r0)
            K[] r1 = r3.f5425b
            r0 = r1[r0]
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x0032
            boolean r4 = r3.b(r4)
            return r4
        L_0x0032:
            r4 = 1
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.b0.a(java.lang.Object):boolean");
    }

    public a<K> a() {
        if (h.f5490a) {
            return new a<>(this);
        }
        if (this.l == null) {
            this.l = new a(this);
            this.m = new a(this);
        }
        a aVar = this.l;
        if (!aVar.f5442e) {
            aVar.b();
            a<K> aVar2 = this.l;
            aVar2.f5442e = true;
            this.m.f5442e = false;
            return aVar2;
        }
        this.m.b();
        a<K> aVar3 = this.m;
        aVar3.f5442e = true;
        this.l.f5442e = false;
        return aVar3;
    }
}
