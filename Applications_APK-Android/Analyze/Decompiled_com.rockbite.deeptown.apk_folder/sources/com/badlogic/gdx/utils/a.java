package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.h;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: Array */
public class a<T> implements Iterable<T> {

    /* renamed from: a  reason: collision with root package name */
    public T[] f5373a;

    /* renamed from: b  reason: collision with root package name */
    public int f5374b;

    /* renamed from: c  reason: collision with root package name */
    public boolean f5375c;

    /* renamed from: d  reason: collision with root package name */
    private C0123a f5376d;

    /* renamed from: com.badlogic.gdx.utils.a$a  reason: collision with other inner class name */
    /* compiled from: Array */
    public static class C0123a<T> implements Iterable<T> {

        /* renamed from: a  reason: collision with root package name */
        private final a<T> f5377a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f5378b;

        /* renamed from: c  reason: collision with root package name */
        private b f5379c;

        /* renamed from: d  reason: collision with root package name */
        private b f5380d;

        public C0123a(a<T> aVar) {
            this(aVar, true);
        }

        public Iterator<T> iterator() {
            if (h.f5490a) {
                return new b(this.f5377a, this.f5378b);
            }
            if (this.f5379c == null) {
                this.f5379c = new b(this.f5377a, this.f5378b);
                this.f5380d = new b(this.f5377a, this.f5378b);
            }
            b bVar = this.f5379c;
            if (!bVar.f5384d) {
                bVar.f5383c = 0;
                bVar.f5384d = true;
                this.f5380d.f5384d = false;
                return bVar;
            }
            b bVar2 = this.f5380d;
            bVar2.f5383c = 0;
            bVar2.f5384d = true;
            bVar.f5384d = false;
            return bVar2;
        }

        public C0123a(a<T> aVar, boolean z) {
            this.f5377a = aVar;
            this.f5378b = z;
        }
    }

    /* compiled from: Array */
    public static class b<T> implements Iterator<T>, Iterable<T> {

        /* renamed from: a  reason: collision with root package name */
        private final a<T> f5381a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f5382b;

        /* renamed from: c  reason: collision with root package name */
        int f5383c;

        /* renamed from: d  reason: collision with root package name */
        boolean f5384d = true;

        public b(a<T> aVar, boolean z) {
            this.f5381a = aVar;
            this.f5382b = z;
        }

        public boolean hasNext() {
            if (this.f5384d) {
                return this.f5383c < this.f5381a.f5374b;
            }
            throw new o("#iterator() cannot be used nested.");
        }

        public Iterator<T> iterator() {
            return this;
        }

        public T next() {
            int i2 = this.f5383c;
            a<T> aVar = this.f5381a;
            if (i2 >= aVar.f5374b) {
                throw new NoSuchElementException(String.valueOf(i2));
            } else if (this.f5384d) {
                T[] tArr = aVar.f5373a;
                this.f5383c = i2 + 1;
                return tArr[i2];
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }

        public void remove() {
            if (this.f5382b) {
                this.f5383c--;
                this.f5381a.d(this.f5383c);
                return;
            }
            throw new o("Remove not allowed.");
        }
    }

    public a() {
        this(true, 16);
    }

    public void a(a<? extends Runnable> aVar) {
        a(aVar.f5373a, 0, aVar.f5374b);
    }

    public void add(T t) {
        T[] tArr = this.f5373a;
        int i2 = this.f5374b;
        if (i2 == tArr.length) {
            tArr = e(Math.max(8, (int) (((float) i2) * 1.75f)));
        }
        int i3 = this.f5374b;
        this.f5374b = i3 + 1;
        tArr[i3] = t;
    }

    public void b(int i2, int i3) {
        int i4 = this.f5374b;
        if (i2 >= i4) {
            throw new IndexOutOfBoundsException("first can't be >= size: " + i2 + " >= " + this.f5374b);
        } else if (i3 < i4) {
            T[] tArr = this.f5373a;
            T t = tArr[i2];
            tArr[i2] = tArr[i3];
            tArr[i3] = t;
        } else {
            throw new IndexOutOfBoundsException("second can't be >= size: " + i3 + " >= " + this.f5374b);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int c(T r3, boolean r4) {
        /*
            r2 = this;
            T[] r0 = r2.f5373a
            if (r4 != 0) goto L_0x0019
            if (r3 != 0) goto L_0x0007
            goto L_0x0019
        L_0x0007:
            int r4 = r2.f5374b
            int r4 = r4 + -1
        L_0x000b:
            if (r4 < 0) goto L_0x0027
            r1 = r0[r4]
            boolean r1 = r3.equals(r1)
            if (r1 == 0) goto L_0x0016
            return r4
        L_0x0016:
            int r4 = r4 + -1
            goto L_0x000b
        L_0x0019:
            int r4 = r2.f5374b
            int r4 = r4 + -1
        L_0x001d:
            if (r4 < 0) goto L_0x0027
            r1 = r0[r4]
            if (r1 != r3) goto L_0x0024
            return r4
        L_0x0024:
            int r4 = r4 + -1
            goto L_0x001d
        L_0x0027:
            r3 = -1
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.a.c(java.lang.Object, boolean):int");
    }

    public void clear() {
        T[] tArr = this.f5373a;
        int i2 = this.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            tArr[i3] = null;
        }
        this.f5374b = 0;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean d(T r6, boolean r7) {
        /*
            r5 = this;
            T[] r0 = r5.f5373a
            r1 = 0
            r2 = 1
            if (r7 != 0) goto L_0x001d
            if (r6 != 0) goto L_0x0009
            goto L_0x001d
        L_0x0009:
            int r7 = r5.f5374b
            r3 = 0
        L_0x000c:
            if (r3 >= r7) goto L_0x002d
            r4 = r0[r3]
            boolean r4 = r6.equals(r4)
            if (r4 == 0) goto L_0x001a
            r5.d(r3)
            return r2
        L_0x001a:
            int r3 = r3 + 1
            goto L_0x000c
        L_0x001d:
            int r7 = r5.f5374b
            r3 = 0
        L_0x0020:
            if (r3 >= r7) goto L_0x002d
            r4 = r0[r3]
            if (r4 != r6) goto L_0x002a
            r5.d(r3)
            return r2
        L_0x002a:
            int r3 = r3 + 1
            goto L_0x0020
        L_0x002d:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.a.d(java.lang.Object, boolean):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.z0.a.a(java.lang.Class, int):java.lang.Object
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.badlogic.gdx.utils.z0.a.a(java.lang.Object, int):java.lang.Object
      com.badlogic.gdx.utils.z0.a.a(java.lang.Class, int):java.lang.Object */
    /* access modifiers changed from: protected */
    public T[] e(int i2) {
        T[] tArr = this.f5373a;
        T[] tArr2 = (Object[]) com.badlogic.gdx.utils.z0.a.a((Class) tArr.getClass().getComponentType(), i2);
        System.arraycopy(tArr, 0, tArr2, 0, Math.min(this.f5374b, tArr2.length));
        this.f5373a = tArr2;
        return tArr2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean equals(java.lang.Object r8) {
        /*
            r7 = this;
            r0 = 1
            if (r8 != r7) goto L_0x0004
            return r0
        L_0x0004:
            boolean r1 = r7.f5375c
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            boolean r1 = r8 instanceof com.badlogic.gdx.utils.a
            if (r1 != 0) goto L_0x000f
            return r2
        L_0x000f:
            com.badlogic.gdx.utils.a r8 = (com.badlogic.gdx.utils.a) r8
            boolean r1 = r8.f5375c
            if (r1 != 0) goto L_0x0016
            return r2
        L_0x0016:
            int r1 = r7.f5374b
            int r3 = r8.f5374b
            if (r1 == r3) goto L_0x001d
            return r2
        L_0x001d:
            T[] r3 = r7.f5373a
            T[] r8 = r8.f5373a
            r4 = 0
        L_0x0022:
            if (r4 >= r1) goto L_0x0037
            r5 = r3[r4]
            r6 = r8[r4]
            if (r5 != 0) goto L_0x002d
            if (r6 != 0) goto L_0x0033
            goto L_0x0034
        L_0x002d:
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x0034
        L_0x0033:
            return r2
        L_0x0034:
            int r4 = r4 + 1
            goto L_0x0022
        L_0x0037:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.a.equals(java.lang.Object):boolean");
    }

    public T[] f(int i2) {
        g(i2);
        if (i2 > this.f5373a.length) {
            e(Math.max(8, i2));
        }
        this.f5374b = i2;
        return this.f5373a;
    }

    public void g(int i2) {
        if (i2 < 0) {
            throw new IllegalArgumentException("newSize must be >= 0: " + i2);
        } else if (this.f5374b > i2) {
            for (int i3 = i2; i3 < this.f5374b; i3++) {
                this.f5373a[i3] = null;
            }
            this.f5374b = i2;
        }
    }

    public T get(int i2) {
        if (i2 < this.f5374b) {
            return this.f5373a[i2];
        }
        throw new IndexOutOfBoundsException("index can't be >= size: " + i2 + " >= " + this.f5374b);
    }

    public int hashCode() {
        if (!this.f5375c) {
            return super.hashCode();
        }
        T[] tArr = this.f5373a;
        int i2 = this.f5374b;
        int i3 = 1;
        for (int i4 = 0; i4 < i2; i4++) {
            i3 *= 31;
            T t = tArr[i4];
            if (t != null) {
                i3 += t.hashCode();
            }
        }
        return i3;
    }

    public boolean isEmpty() {
        return this.f5374b == 0;
    }

    public Iterator<T> iterator() {
        if (h.f5490a) {
            return new b(this, true);
        }
        if (this.f5376d == null) {
            this.f5376d = new C0123a(this);
        }
        return this.f5376d.iterator();
    }

    public T peek() {
        int i2 = this.f5374b;
        if (i2 != 0) {
            return this.f5373a[i2 - 1];
        }
        throw new IllegalStateException("Array is empty.");
    }

    public T pop() {
        int i2 = this.f5374b;
        if (i2 != 0) {
            this.f5374b = i2 - 1;
            T[] tArr = this.f5373a;
            int i3 = this.f5374b;
            T t = tArr[i3];
            tArr[i3] = null;
            return t;
        }
        throw new IllegalStateException("Array is empty.");
    }

    public void set(int i2, T t) {
        if (i2 < this.f5374b) {
            this.f5373a[i2] = t;
            return;
        }
        throw new IndexOutOfBoundsException("index can't be >= size: " + i2 + " >= " + this.f5374b);
    }

    public void sort(Comparator<? super T> comparator) {
        p0.a().a(this.f5373a, comparator, 0, this.f5374b);
    }

    public String toString() {
        if (this.f5374b == 0) {
            return "[]";
        }
        T[] tArr = this.f5373a;
        r0 r0Var = new r0(32);
        r0Var.append('[');
        r0Var.a((Object) tArr[0]);
        for (int i2 = 1; i2 < this.f5374b; i2++) {
            r0Var.a(", ");
            r0Var.a((Object) tArr[i2]);
        }
        r0Var.append(']');
        return r0Var.toString();
    }

    public a(int i2) {
        this(true, i2);
    }

    public void a(a<? extends T> aVar, int i2, int i3) {
        if (i2 + i3 <= aVar.f5374b) {
            a(aVar.f5373a, i2, i3);
            return;
        }
        throw new IllegalArgumentException("start + count must be <= size: " + i2 + " + " + i3 + " <= " + aVar.f5374b);
    }

    public a(boolean z, int i2) {
        this.f5375c = z;
        this.f5373a = new Object[i2];
    }

    public void a(Object... objArr) {
        a(objArr, 0, objArr.length);
    }

    public a(boolean z, int i2, Class cls) {
        this.f5375c = z;
        this.f5373a = (Object[]) com.badlogic.gdx.utils.z0.a.a(cls, i2);
    }

    public void a(T[] tArr, int i2, int i3) {
        T[] tArr2 = this.f5373a;
        int i4 = this.f5374b + i3;
        if (i4 > tArr2.length) {
            tArr2 = e(Math.max(8, (int) (((float) i4) * 1.75f)));
        }
        System.arraycopy(tArr, i2, tArr2, this.f5374b, i3);
        this.f5374b += i3;
    }

    public T[] c() {
        int length = this.f5373a.length;
        int i2 = this.f5374b;
        if (length != i2) {
            e(i2);
        }
        return this.f5373a;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int b(T r4, boolean r5) {
        /*
            r3 = this;
            T[] r0 = r3.f5373a
            r1 = 0
            if (r5 != 0) goto L_0x0018
            if (r4 != 0) goto L_0x0008
            goto L_0x0018
        L_0x0008:
            int r5 = r3.f5374b
        L_0x000a:
            if (r1 >= r5) goto L_0x0024
            r2 = r0[r1]
            boolean r2 = r4.equals(r2)
            if (r2 == 0) goto L_0x0015
            return r1
        L_0x0015:
            int r1 = r1 + 1
            goto L_0x000a
        L_0x0018:
            int r5 = r3.f5374b
        L_0x001a:
            if (r1 >= r5) goto L_0x0024
            r2 = r0[r1]
            if (r2 != r4) goto L_0x0021
            return r1
        L_0x0021:
            int r1 = r1 + 1
            goto L_0x001a
        L_0x0024:
            r4 = -1
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.a.b(java.lang.Object, boolean):int");
    }

    public T[] c(int i2) {
        if (i2 >= 0) {
            int i3 = this.f5374b + i2;
            if (i3 > this.f5373a.length) {
                e(Math.max(8, i3));
            }
            return this.f5373a;
        }
        throw new IllegalArgumentException("additionalCapacity must be >= 0: " + i2);
    }

    public T d(int i2) {
        int i3 = this.f5374b;
        if (i2 < i3) {
            T[] tArr = this.f5373a;
            T t = tArr[i2];
            this.f5374b = i3 - 1;
            if (this.f5375c) {
                System.arraycopy(tArr, i2 + 1, tArr, i2, this.f5374b - i2);
            } else {
                tArr[i2] = tArr[this.f5374b];
            }
            tArr[this.f5374b] = null;
            return t;
        }
        throw new IndexOutOfBoundsException("index can't be >= size: " + i2 + " >= " + this.f5374b);
    }

    public a(Class cls) {
        this(true, 16, cls);
    }

    public a(a<? extends T> aVar) {
        this(aVar.f5375c, aVar.f5374b, aVar.f5373a.getClass().getComponentType());
        this.f5374b = aVar.f5374b;
        System.arraycopy(aVar.f5373a, 0, this.f5373a, 0, this.f5374b);
    }

    public void a(int i2, T t) {
        int i3 = this.f5374b;
        if (i2 <= i3) {
            T[] tArr = this.f5373a;
            if (i3 == tArr.length) {
                tArr = e(Math.max(8, (int) (((float) i3) * 1.75f)));
            }
            if (this.f5375c) {
                System.arraycopy(tArr, i2, tArr, i2 + 1, this.f5374b - i2);
            } else {
                tArr[this.f5374b] = tArr[i2];
            }
            this.f5374b++;
            tArr[i2] = t;
            return;
        }
        throw new IndexOutOfBoundsException("index can't be > size: " + i2 + " > " + this.f5374b);
    }

    public a(T[] tArr) {
        this(true, tArr, 0, tArr.length);
    }

    public T b() {
        int i2 = this.f5374b;
        if (i2 == 0) {
            return null;
        }
        return this.f5373a[h.a(0, i2 - 1)];
    }

    public a(boolean z, T[] tArr, int i2, int i3) {
        this(z, i3, tArr.getClass().getComponentType());
        this.f5374b = i3;
        System.arraycopy(tArr, i2, this.f5373a, 0, this.f5374b);
    }

    public static <T> a<T> b(T... tArr) {
        return new a<>(tArr);
    }

    public void d() {
        p0.a().a(this.f5373a, 0, this.f5374b);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean a(T r4, boolean r5) {
        /*
            r3 = this;
            T[] r0 = r3.f5373a
            int r1 = r3.f5374b
            r2 = 1
            int r1 = r1 - r2
            if (r5 != 0) goto L_0x001a
            if (r4 != 0) goto L_0x000b
            goto L_0x001a
        L_0x000b:
            if (r1 < 0) goto L_0x0025
            int r5 = r1 + -1
            r1 = r0[r1]
            boolean r1 = r4.equals(r1)
            if (r1 == 0) goto L_0x0018
            return r2
        L_0x0018:
            r1 = r5
            goto L_0x000b
        L_0x001a:
            if (r1 < 0) goto L_0x0025
            int r5 = r1 + -1
            r1 = r0[r1]
            if (r1 != r4) goto L_0x0023
            return r2
        L_0x0023:
            r1 = r5
            goto L_0x001a
        L_0x0025:
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.a.a(java.lang.Object, boolean):boolean");
    }

    public void a(int i2, int i3) {
        int i4 = this.f5374b;
        if (i3 >= i4) {
            throw new IndexOutOfBoundsException("end can't be >= size: " + i3 + " >= " + this.f5374b);
        } else if (i2 <= i3) {
            T[] tArr = this.f5373a;
            int i5 = (i3 - i2) + 1;
            int i6 = i4 - i5;
            if (this.f5375c) {
                int i7 = i5 + i2;
                System.arraycopy(tArr, i7, tArr, i2, i4 - i7);
            } else {
                int max = Math.max(i6, i3 + 1);
                System.arraycopy(tArr, max, tArr, i2, i4 - max);
            }
            for (int i8 = i6; i8 < i4; i8++) {
                tArr[i8] = null;
            }
            this.f5374b = i6;
        } else {
            throw new IndexOutOfBoundsException("start can't be > end: " + i2 + " > " + i3);
        }
    }

    public boolean a(a<? extends T> aVar, boolean z) {
        int i2;
        int i3 = this.f5374b;
        T[] tArr = this.f5373a;
        if (z) {
            int i4 = aVar.f5374b;
            i2 = i3;
            for (int i5 = 0; i5 < i4; i5++) {
                T t = aVar.get(i5);
                int i6 = 0;
                while (true) {
                    if (i6 >= i2) {
                        break;
                    } else if (t == tArr[i6]) {
                        d(i6);
                        i2--;
                        break;
                    } else {
                        i6++;
                    }
                }
            }
        } else {
            int i7 = aVar.f5374b;
            i2 = i3;
            for (int i8 = 0; i8 < i7; i8++) {
                Object obj = aVar.get(i8);
                int i9 = 0;
                while (true) {
                    if (i9 >= i2) {
                        break;
                    } else if (obj.equals(tArr[i9])) {
                        d(i9);
                        i2--;
                        break;
                    } else {
                        i9++;
                    }
                }
            }
        }
        if (i2 != i3) {
            return true;
        }
        return false;
    }

    public T a() {
        if (this.f5374b != 0) {
            return this.f5373a[0];
        }
        throw new IllegalStateException("Array is empty.");
    }

    public <V> V[] a(Class cls) {
        V[] vArr = (Object[]) com.badlogic.gdx.utils.z0.a.a(cls, this.f5374b);
        System.arraycopy(this.f5373a, 0, vArr, 0, this.f5374b);
        return vArr;
    }
}
