package com.badlogic.gdx.utils;

import e.d.b.s.a;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;

/* compiled from: I18NBundle */
public class p {

    /* renamed from: e  reason: collision with root package name */
    private static final Locale f5548e = new Locale("", "", "");

    /* renamed from: f  reason: collision with root package name */
    private static boolean f5549f = false;

    /* renamed from: g  reason: collision with root package name */
    private static boolean f5550g = true;

    /* renamed from: a  reason: collision with root package name */
    private p f5551a;

    /* renamed from: b  reason: collision with root package name */
    private Locale f5552b;

    /* renamed from: c  reason: collision with root package name */
    private c0<String, String> f5553c;

    /* renamed from: d  reason: collision with root package name */
    private s0 f5554d;

    public static p a(a aVar, Locale locale) {
        return b(aVar, locale, "UTF-8");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0082  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.badlogic.gdx.utils.p b(e.d.b.s.a r9, java.util.Locale r10, java.lang.String r11) {
        /*
            if (r9 == 0) goto L_0x0084
            if (r10 == 0) goto L_0x0084
            if (r11 == 0) goto L_0x0084
            r0 = 0
            r1 = r0
            r0 = r10
        L_0x0009:
            java.util.List r2 = a(r0)
            r3 = 0
            com.badlogic.gdx.utils.p r4 = a(r9, r11, r2, r3, r1)
            if (r4 == 0) goto L_0x003e
            java.util.Locale r5 = r4.a()
            java.util.Locale r6 = com.badlogic.gdx.utils.p.f5548e
            boolean r6 = r5.equals(r6)
            if (r6 == 0) goto L_0x0044
            boolean r7 = r5.equals(r10)
            if (r7 == 0) goto L_0x0027
            goto L_0x0044
        L_0x0027:
            int r7 = r2.size()
            r8 = 1
            if (r7 != r8) goto L_0x0039
            java.lang.Object r2 = r2.get(r3)
            boolean r2 = r5.equals(r2)
            if (r2 == 0) goto L_0x0039
            goto L_0x0044
        L_0x0039:
            if (r6 == 0) goto L_0x003e
            if (r1 != 0) goto L_0x003e
            r1 = r4
        L_0x003e:
            java.util.Locale r0 = b(r0)
            if (r0 != 0) goto L_0x0009
        L_0x0044:
            if (r4 != 0) goto L_0x0082
            if (r1 == 0) goto L_0x0049
            goto L_0x0083
        L_0x0049:
            java.util.MissingResourceException r11 = new java.util.MissingResourceException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Can't find bundle for base file handle "
            r0.append(r1)
            java.lang.String r1 = r9.j()
            r0.append(r1)
            java.lang.String r1 = ", locale "
            r0.append(r1)
            r0.append(r10)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r9)
            java.lang.String r9 = "_"
            r1.append(r9)
            r1.append(r10)
            java.lang.String r9 = r1.toString()
            java.lang.String r10 = ""
            r11.<init>(r0, r9, r10)
            throw r11
        L_0x0082:
            r1 = r4
        L_0x0083:
            return r1
        L_0x0084:
            java.lang.NullPointerException r9 = new java.lang.NullPointerException
            r9.<init>()
            goto L_0x008b
        L_0x008a:
            throw r9
        L_0x008b:
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.p.b(e.d.b.s.a, java.util.Locale, java.lang.String):com.badlogic.gdx.utils.p");
    }

    private void c(Locale locale) {
        this.f5552b = locale;
        this.f5554d = new s0(locale, !f5549f);
    }

    public static p a(a aVar, Locale locale, String str) {
        return b(aVar, locale, str);
    }

    private static List<Locale> a(Locale locale) {
        String language = locale.getLanguage();
        String country = locale.getCountry();
        String variant = locale.getVariant();
        ArrayList arrayList = new ArrayList(4);
        if (variant.length() > 0) {
            arrayList.add(locale);
        }
        if (country.length() > 0) {
            arrayList.add(arrayList.isEmpty() ? locale : new Locale(language, country));
        }
        if (language.length() > 0) {
            if (!arrayList.isEmpty()) {
                locale = new Locale(language);
            }
            arrayList.add(locale);
        }
        arrayList.add(f5548e);
        return arrayList;
    }

    private static Locale b(Locale locale) {
        Locale locale2 = Locale.getDefault();
        if (locale.equals(locale2)) {
            return null;
        }
        return locale2;
    }

    private static a b(a aVar, Locale locale) {
        r0 r0Var = new r0(aVar.g());
        if (!locale.equals(f5548e)) {
            String language = locale.getLanguage();
            String country = locale.getCountry();
            String variant = locale.getVariant();
            boolean equals = "".equals(language);
            boolean equals2 = "".equals(country);
            boolean equals3 = "".equals(variant);
            if (!equals || !equals2 || !equals3) {
                r0Var.append('_');
                if (!equals3) {
                    r0Var.a(language);
                    r0Var.append('_');
                    r0Var.a(country);
                    r0Var.append('_');
                    r0Var.a(variant);
                } else if (!equals2) {
                    r0Var.a(language);
                    r0Var.append('_');
                    r0Var.a(country);
                } else {
                    r0Var.a(language);
                }
            }
        }
        r0Var.a(".properties");
        return aVar.d(r0Var.toString());
    }

    private static p a(a aVar, String str, List<Locale> list, int i2, p pVar) {
        p pVar2;
        Locale locale = list.get(i2);
        if (i2 != list.size() - 1) {
            pVar2 = a(aVar, str, list, i2 + 1, pVar);
        } else if (pVar != null && locale.equals(f5548e)) {
            return pVar;
        } else {
            pVar2 = null;
        }
        p a2 = a(aVar, str, locale);
        if (a2 == null) {
            return pVar2;
        }
        a2.f5551a = pVar2;
        return a2;
    }

    private static p a(a aVar, String str, Locale locale) {
        p pVar;
        Reader reader = null;
        try {
            a b2 = b(aVar, locale);
            if (a(b2)) {
                pVar = new p();
                reader = b2.c(str);
                pVar.a(reader);
            } else {
                pVar = null;
            }
            q0.a(reader);
            if (pVar != null) {
                pVar.c(locale);
            }
            return pVar;
        } catch (IOException e2) {
            throw new o(e2);
        } catch (Throwable th) {
            q0.a(null);
            throw th;
        }
    }

    private static boolean a(a aVar) {
        try {
            aVar.l().close();
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Reader reader) throws IOException {
        this.f5553c = new c0<>();
        h0.a(this.f5553c, reader);
    }

    public Locale a() {
        return this.f5552b;
    }

    public final String a(String str) {
        String b2 = this.f5553c.b(str);
        if (b2 == null) {
            p pVar = this.f5551a;
            if (pVar != null) {
                b2 = pVar.a(str);
            }
            if (b2 == null) {
                if (!f5550g) {
                    return "???" + str + "???";
                }
                throw new MissingResourceException("Can't find bundle key " + str, p.class.getName(), str);
            }
        }
        return b2;
    }

    public String a(String str, Object... objArr) {
        return this.f5554d.a(a(str), objArr);
    }
}
