package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.h;
import com.esotericsoftware.spine.Animation;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: ObjectMap */
public class c0<K, V> implements Iterable<b<K, V>> {
    static final Object r = new Object();

    /* renamed from: a  reason: collision with root package name */
    public int f5447a;

    /* renamed from: b  reason: collision with root package name */
    K[] f5448b;

    /* renamed from: c  reason: collision with root package name */
    V[] f5449c;

    /* renamed from: d  reason: collision with root package name */
    int f5450d;

    /* renamed from: e  reason: collision with root package name */
    int f5451e;

    /* renamed from: f  reason: collision with root package name */
    private float f5452f;

    /* renamed from: g  reason: collision with root package name */
    private int f5453g;

    /* renamed from: h  reason: collision with root package name */
    private int f5454h;

    /* renamed from: i  reason: collision with root package name */
    private int f5455i;

    /* renamed from: j  reason: collision with root package name */
    private int f5456j;

    /* renamed from: k  reason: collision with root package name */
    private int f5457k;
    a l;
    a m;
    e n;
    e o;
    c p;
    c q;

    /* compiled from: ObjectMap */
    public static class a<K, V> extends d<K, V, b<K, V>> {

        /* renamed from: f  reason: collision with root package name */
        b<K, V> f5458f = new b<>();

        public a(c0<K, V> c0Var) {
            super(c0Var);
        }

        public boolean hasNext() {
            if (this.f5465e) {
                return this.f5461a;
            }
            throw new o("#iterator() cannot be used nested.");
        }

        public a<K, V> iterator() {
            return this;
        }

        public b<K, V> next() {
            if (!this.f5461a) {
                throw new NoSuchElementException();
            } else if (this.f5465e) {
                c0<K, V> c0Var = this.f5462b;
                K[] kArr = c0Var.f5448b;
                b<K, V> bVar = this.f5458f;
                int i2 = this.f5463c;
                bVar.f5459a = kArr[i2];
                bVar.f5460b = c0Var.f5449c[i2];
                this.f5464d = i2;
                a();
                return this.f5458f;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }
    }

    /* compiled from: ObjectMap */
    public static class b<K, V> {

        /* renamed from: a  reason: collision with root package name */
        public K f5459a;

        /* renamed from: b  reason: collision with root package name */
        public V f5460b;

        public String toString() {
            return ((Object) this.f5459a) + "=" + ((Object) this.f5460b);
        }
    }

    /* compiled from: ObjectMap */
    public static class c<K> extends d<K, Object, K> {
        public c(c0<K, ?> c0Var) {
            super(c0Var);
        }

        public a<K> a(a<K> aVar) {
            while (this.f5461a) {
                aVar.add(next());
            }
            return aVar;
        }

        public boolean hasNext() {
            if (this.f5465e) {
                return this.f5461a;
            }
            throw new o("#iterator() cannot be used nested.");
        }

        public c<K> iterator() {
            return this;
        }

        public K next() {
            if (!this.f5461a) {
                throw new NoSuchElementException();
            } else if (this.f5465e) {
                K[] kArr = this.f5462b.f5448b;
                int i2 = this.f5463c;
                K k2 = kArr[i2];
                this.f5464d = i2;
                a();
                return k2;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }

        public a<K> toArray() {
            a<K> aVar = new a<>(true, this.f5462b.f5447a);
            a(aVar);
            return aVar;
        }
    }

    /* compiled from: ObjectMap */
    private static abstract class d<K, V, I> implements Iterable<I>, Iterator<I> {

        /* renamed from: a  reason: collision with root package name */
        public boolean f5461a;

        /* renamed from: b  reason: collision with root package name */
        final c0<K, V> f5462b;

        /* renamed from: c  reason: collision with root package name */
        int f5463c;

        /* renamed from: d  reason: collision with root package name */
        int f5464d;

        /* renamed from: e  reason: collision with root package name */
        boolean f5465e = true;

        public d(c0<K, V> c0Var) {
            this.f5462b = c0Var;
            b();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f5461a = false;
            c0<K, V> c0Var = this.f5462b;
            K[] kArr = c0Var.f5448b;
            int i2 = c0Var.f5450d + c0Var.f5451e;
            do {
                int i3 = this.f5463c + 1;
                this.f5463c = i3;
                if (i3 >= i2) {
                    return;
                }
            } while (kArr[this.f5463c] == null);
            this.f5461a = true;
        }

        public void b() {
            this.f5464d = -1;
            this.f5463c = -1;
            a();
        }

        public void remove() {
            int i2 = this.f5464d;
            if (i2 >= 0) {
                c0<K, V> c0Var = this.f5462b;
                if (i2 >= c0Var.f5450d) {
                    c0Var.d(i2);
                    this.f5463c = this.f5464d - 1;
                    a();
                } else {
                    c0Var.f5448b[i2] = null;
                    c0Var.f5449c[i2] = null;
                }
                this.f5464d = -1;
                c0<K, V> c0Var2 = this.f5462b;
                c0Var2.f5447a--;
                return;
            }
            throw new IllegalStateException("next must be called before remove.");
        }
    }

    /* compiled from: ObjectMap */
    public static class e<V> extends d<Object, V, V> {
        public e(c0<?, V> c0Var) {
            super(c0Var);
        }

        public boolean hasNext() {
            if (this.f5465e) {
                return this.f5461a;
            }
            throw new o("#iterator() cannot be used nested.");
        }

        public e<V> iterator() {
            return this;
        }

        public V next() {
            if (!this.f5461a) {
                throw new NoSuchElementException();
            } else if (this.f5465e) {
                V[] vArr = this.f5462b.f5449c;
                int i2 = this.f5463c;
                V v = vArr[i2];
                this.f5464d = i2;
                a();
                return v;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }
    }

    public c0() {
        this(51, 0.8f);
    }

    private void a(K k2, V v, int i2, K k3, int i3, K k4, int i4, K k5) {
        K[] kArr = this.f5448b;
        V[] vArr = this.f5449c;
        int i5 = this.f5454h;
        int i6 = this.f5457k;
        K k6 = k2;
        V v2 = v;
        int i7 = i2;
        K k7 = k3;
        int i8 = i3;
        K k8 = k4;
        int i9 = i4;
        K k9 = k5;
        int i10 = 0;
        do {
            int c2 = h.c(2);
            if (c2 == 0) {
                V v3 = vArr[i7];
                kArr[i7] = k6;
                vArr[i7] = v2;
                v2 = v3;
                k6 = k7;
            } else if (c2 != 1) {
                V v4 = vArr[i9];
                kArr[i9] = k6;
                vArr[i9] = v2;
                k6 = k9;
                v2 = v4;
            } else {
                V v5 = vArr[i8];
                kArr[i8] = k6;
                vArr[i8] = v2;
                v2 = v5;
                k6 = k8;
            }
            int hashCode = k6.hashCode();
            i7 = hashCode & i5;
            k7 = kArr[i7];
            if (k7 == null) {
                kArr[i7] = k6;
                vArr[i7] = v2;
                int i11 = this.f5447a;
                this.f5447a = i11 + 1;
                if (i11 >= this.f5455i) {
                    g(this.f5450d << 1);
                    return;
                }
                return;
            }
            i8 = e(hashCode);
            k8 = kArr[i8];
            if (k8 == null) {
                kArr[i8] = k6;
                vArr[i8] = v2;
                int i12 = this.f5447a;
                this.f5447a = i12 + 1;
                if (i12 >= this.f5455i) {
                    g(this.f5450d << 1);
                    return;
                }
                return;
            }
            i9 = f(hashCode);
            k9 = kArr[i9];
            if (k9 == null) {
                kArr[i9] = k6;
                vArr[i9] = v2;
                int i13 = this.f5447a;
                this.f5447a = i13 + 1;
                if (i13 >= this.f5455i) {
                    g(this.f5450d << 1);
                    return;
                }
                return;
            }
            i10++;
        } while (i10 != i6);
        e(k6, v2);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private V c(K r5, V r6) {
        /*
            r4 = this;
            K[] r0 = r4.f5448b
            int r1 = r4.f5450d
            int r2 = r4.f5451e
            int r2 = r2 + r1
        L_0x0007:
            if (r1 >= r2) goto L_0x0019
            r3 = r0[r1]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0016
            V[] r5 = r4.f5449c
            r5 = r5[r1]
            return r5
        L_0x0016:
            int r1 = r1 + 1
            goto L_0x0007
        L_0x0019:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.c0.c(java.lang.Object, java.lang.Object):java.lang.Object");
    }

    private void d(K k2, V v) {
        int hashCode = k2.hashCode();
        int i2 = hashCode & this.f5454h;
        K[] kArr = this.f5448b;
        K k3 = kArr[i2];
        if (k3 == null) {
            kArr[i2] = k2;
            this.f5449c[i2] = v;
            int i3 = this.f5447a;
            this.f5447a = i3 + 1;
            if (i3 >= this.f5455i) {
                g(this.f5450d << 1);
                return;
            }
            return;
        }
        int e2 = e(hashCode);
        K[] kArr2 = this.f5448b;
        K k4 = kArr2[e2];
        if (k4 == null) {
            kArr2[e2] = k2;
            this.f5449c[e2] = v;
            int i4 = this.f5447a;
            this.f5447a = i4 + 1;
            if (i4 >= this.f5455i) {
                g(this.f5450d << 1);
                return;
            }
            return;
        }
        int f2 = f(hashCode);
        K[] kArr3 = this.f5448b;
        K k5 = kArr3[f2];
        if (k5 == null) {
            kArr3[f2] = k2;
            this.f5449c[f2] = v;
            int i5 = this.f5447a;
            this.f5447a = i5 + 1;
            if (i5 >= this.f5455i) {
                g(this.f5450d << 1);
                return;
            }
            return;
        }
        a(k2, v, i2, k3, e2, k4, f2, k5);
    }

    private void e(K k2, V v) {
        int i2 = this.f5451e;
        if (i2 == this.f5456j) {
            g(this.f5450d << 1);
            d(k2, v);
            return;
        }
        int i3 = this.f5450d + i2;
        this.f5448b[i3] = k2;
        this.f5449c[i3] = v;
        this.f5451e = i2 + 1;
        this.f5447a++;
    }

    private int f(int i2) {
        int i3 = i2 * -825114047;
        return (i3 ^ (i3 >>> this.f5453g)) & this.f5454h;
    }

    private void g(int i2) {
        int i3 = this.f5450d + this.f5451e;
        this.f5450d = i2;
        this.f5455i = (int) (((float) i2) * this.f5452f);
        this.f5454h = i2 - 1;
        this.f5453g = 31 - Integer.numberOfTrailingZeros(i2);
        double d2 = (double) i2;
        this.f5456j = Math.max(3, ((int) Math.ceil(Math.log(d2))) * 2);
        this.f5457k = Math.max(Math.min(i2, 8), ((int) Math.sqrt(d2)) / 8);
        K[] kArr = this.f5448b;
        V[] vArr = this.f5449c;
        int i4 = this.f5456j;
        this.f5448b = new Object[(i2 + i4)];
        this.f5449c = new Object[(i2 + i4)];
        int i5 = this.f5447a;
        this.f5447a = 0;
        this.f5451e = 0;
        if (i5 > 0) {
            for (int i6 = 0; i6 < i3; i6++) {
                K k2 = kArr[i6];
                if (k2 != null) {
                    d(k2, vArr[i6]);
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public V b(K r13, V r14) {
        /*
            r12 = this;
            if (r13 == 0) goto L_0x00b0
            K[] r0 = r12.f5448b
            int r1 = r13.hashCode()
            int r2 = r12.f5454h
            r6 = r1 & r2
            r7 = r0[r6]
            boolean r2 = r13.equals(r7)
            if (r2 == 0) goto L_0x001b
            V[] r13 = r12.f5449c
            r0 = r13[r6]
            r13[r6] = r14
            return r0
        L_0x001b:
            int r8 = r12.e(r1)
            r9 = r0[r8]
            boolean r2 = r13.equals(r9)
            if (r2 == 0) goto L_0x002e
            V[] r13 = r12.f5449c
            r0 = r13[r8]
            r13[r8] = r14
            return r0
        L_0x002e:
            int r10 = r12.f(r1)
            r11 = r0[r10]
            boolean r1 = r13.equals(r11)
            if (r1 == 0) goto L_0x0041
            V[] r13 = r12.f5449c
            r0 = r13[r10]
            r13[r10] = r14
            return r0
        L_0x0041:
            int r1 = r12.f5450d
            int r2 = r12.f5451e
            int r2 = r2 + r1
        L_0x0046:
            if (r1 >= r2) goto L_0x005a
            r3 = r0[r1]
            boolean r3 = r13.equals(r3)
            if (r3 == 0) goto L_0x0057
            V[] r13 = r12.f5449c
            r0 = r13[r1]
            r13[r1] = r14
            return r0
        L_0x0057:
            int r1 = r1 + 1
            goto L_0x0046
        L_0x005a:
            r1 = 0
            if (r7 != 0) goto L_0x0075
            r0[r6] = r13
            V[] r13 = r12.f5449c
            r13[r6] = r14
            int r13 = r12.f5447a
            int r14 = r13 + 1
            r12.f5447a = r14
            int r14 = r12.f5455i
            if (r13 < r14) goto L_0x0074
            int r13 = r12.f5450d
            int r13 = r13 << 1
            r12.g(r13)
        L_0x0074:
            return r1
        L_0x0075:
            if (r9 != 0) goto L_0x008f
            r0[r8] = r13
            V[] r13 = r12.f5449c
            r13[r8] = r14
            int r13 = r12.f5447a
            int r14 = r13 + 1
            r12.f5447a = r14
            int r14 = r12.f5455i
            if (r13 < r14) goto L_0x008e
            int r13 = r12.f5450d
            int r13 = r13 << 1
            r12.g(r13)
        L_0x008e:
            return r1
        L_0x008f:
            if (r11 != 0) goto L_0x00a9
            r0[r10] = r13
            V[] r13 = r12.f5449c
            r13[r10] = r14
            int r13 = r12.f5447a
            int r14 = r13 + 1
            r12.f5447a = r14
            int r14 = r12.f5455i
            if (r13 < r14) goto L_0x00a8
            int r13 = r12.f5450d
            int r13 = r13 << 1
            r12.g(r13)
        L_0x00a8:
            return r1
        L_0x00a9:
            r3 = r12
            r4 = r13
            r5 = r14
            r3.a(r4, r5, r6, r7, r8, r9, r10, r11)
            return r1
        L_0x00b0:
            java.lang.IllegalArgumentException r13 = new java.lang.IllegalArgumentException
            java.lang.String r14 = "key cannot be null."
            r13.<init>(r14)
            goto L_0x00b9
        L_0x00b8:
            throw r13
        L_0x00b9:
            goto L_0x00b8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.c0.b(java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public void clear() {
        if (this.f5447a != 0) {
            K[] kArr = this.f5448b;
            V[] vArr = this.f5449c;
            int i2 = this.f5450d + this.f5451e;
            while (true) {
                int i3 = i2 - 1;
                if (i2 > 0) {
                    kArr[i3] = null;
                    vArr[i3] = null;
                    i2 = i3;
                } else {
                    this.f5447a = 0;
                    this.f5451e = 0;
                    return;
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean equals(java.lang.Object r9) {
        /*
            r8 = this;
            r0 = 1
            if (r9 != r8) goto L_0x0004
            return r0
        L_0x0004:
            boolean r1 = r9 instanceof com.badlogic.gdx.utils.c0
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            com.badlogic.gdx.utils.c0 r9 = (com.badlogic.gdx.utils.c0) r9
            int r1 = r9.f5447a
            int r3 = r8.f5447a
            if (r1 == r3) goto L_0x0013
            return r2
        L_0x0013:
            K[] r1 = r8.f5448b
            V[] r3 = r8.f5449c
            int r4 = r8.f5450d
            int r5 = r8.f5451e
            int r4 = r4 + r5
            r5 = 0
        L_0x001d:
            if (r5 >= r4) goto L_0x003e
            r6 = r1[r5]
            if (r6 == 0) goto L_0x003b
            r7 = r3[r5]
            if (r7 != 0) goto L_0x0030
            java.lang.Object r7 = com.badlogic.gdx.utils.c0.r
            java.lang.Object r6 = r9.a(r6, r7)
            if (r6 == 0) goto L_0x003b
            return r2
        L_0x0030:
            java.lang.Object r6 = r9.b(r6)
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x003b
            return r2
        L_0x003b:
            int r5 = r5 + 1
            goto L_0x001d
        L_0x003e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.c0.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        K[] kArr = this.f5448b;
        V[] vArr = this.f5449c;
        int i2 = this.f5450d + this.f5451e;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            K k2 = kArr[i4];
            if (k2 != null) {
                i3 += k2.hashCode() * 31;
                V v = vArr[i4];
                if (v != null) {
                    i3 += v.hashCode();
                }
            }
        }
        return i3;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public V remove(K r5) {
        /*
            r4 = this;
            int r0 = r5.hashCode()
            int r1 = r4.f5454h
            r1 = r1 & r0
            K[] r2 = r4.f5448b
            r2 = r2[r1]
            boolean r2 = r5.equals(r2)
            r3 = 0
            if (r2 == 0) goto L_0x0023
            K[] r5 = r4.f5448b
            r5[r1] = r3
            V[] r5 = r4.f5449c
            r0 = r5[r1]
            r5[r1] = r3
            int r5 = r4.f5447a
            int r5 = r5 + -1
            r4.f5447a = r5
            return r0
        L_0x0023:
            int r1 = r4.e(r0)
            K[] r2 = r4.f5448b
            r2 = r2[r1]
            boolean r2 = r5.equals(r2)
            if (r2 == 0) goto L_0x0042
            K[] r5 = r4.f5448b
            r5[r1] = r3
            V[] r5 = r4.f5449c
            r0 = r5[r1]
            r5[r1] = r3
            int r5 = r4.f5447a
            int r5 = r5 + -1
            r4.f5447a = r5
            return r0
        L_0x0042:
            int r0 = r4.f(r0)
            K[] r1 = r4.f5448b
            r1 = r1[r0]
            boolean r1 = r5.equals(r1)
            if (r1 == 0) goto L_0x0061
            K[] r5 = r4.f5448b
            r5[r0] = r3
            V[] r5 = r4.f5449c
            r1 = r5[r0]
            r5[r0] = r3
            int r5 = r4.f5447a
            int r5 = r5 + -1
            r4.f5447a = r5
            return r1
        L_0x0061:
            java.lang.Object r5 = r4.c(r5)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.c0.remove(java.lang.Object):java.lang.Object");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.c0.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.badlogic.gdx.utils.c0.a(java.lang.Object, java.lang.Object):V
      com.badlogic.gdx.utils.c0.a(java.lang.Object, boolean):K
      com.badlogic.gdx.utils.c0.a(java.lang.String, boolean):java.lang.String */
    public String toString() {
        return a(", ", true);
    }

    public c0(int i2) {
        this(i2, 0.8f);
    }

    public a<K, V> iterator() {
        return a();
    }

    public c0(int i2, float f2) {
        if (i2 >= 0) {
            int b2 = h.b((int) Math.ceil((double) (((float) i2) / f2)));
            if (b2 <= 1073741824) {
                this.f5450d = b2;
                if (f2 > Animation.CurveTimeline.LINEAR) {
                    this.f5452f = f2;
                    int i3 = this.f5450d;
                    this.f5455i = (int) (((float) i3) * f2);
                    this.f5454h = i3 - 1;
                    this.f5453g = 31 - Integer.numberOfTrailingZeros(i3);
                    this.f5456j = Math.max(3, ((int) Math.ceil(Math.log((double) this.f5450d))) * 2);
                    this.f5457k = Math.max(Math.min(this.f5450d, 8), ((int) Math.sqrt((double) this.f5450d)) / 8);
                    this.f5448b = new Object[(this.f5450d + this.f5456j)];
                    this.f5449c = new Object[this.f5448b.length];
                    return;
                }
                throw new IllegalArgumentException("loadFactor must be > 0: " + f2);
            }
            throw new IllegalArgumentException("initialCapacity is too large: " + b2);
        }
        throw new IllegalArgumentException("initialCapacity must be >= 0: " + i2);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    V c(K r5) {
        /*
            r4 = this;
            K[] r0 = r4.f5448b
            int r1 = r4.f5450d
            int r2 = r4.f5451e
            int r2 = r2 + r1
        L_0x0007:
            if (r1 >= r2) goto L_0x0022
            r3 = r0[r1]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x001f
            V[] r5 = r4.f5449c
            r5 = r5[r1]
            r4.d(r1)
            int r0 = r4.f5447a
            int r0 = r0 + -1
            r4.f5447a = r0
            return r5
        L_0x001f:
            int r1 = r1 + 1
            goto L_0x0007
        L_0x0022:
            r5 = 0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.c0.c(java.lang.Object):java.lang.Object");
    }

    private int e(int i2) {
        int i3 = i2 * -1262997959;
        return (i3 ^ (i3 >>> this.f5453g)) & this.f5454h;
    }

    public void c(int i2) {
        if (this.f5450d <= i2) {
            clear();
            return;
        }
        this.f5447a = 0;
        g(i2);
    }

    public e<V> c() {
        if (h.f5490a) {
            return new e<>(this);
        }
        if (this.n == null) {
            this.n = new e(this);
            this.o = new e(this);
        }
        e eVar = this.n;
        if (!eVar.f5465e) {
            eVar.b();
            e<V> eVar2 = this.n;
            eVar2.f5465e = true;
            this.o.f5465e = false;
            return eVar2;
        }
        this.o.b();
        e<V> eVar3 = this.o;
        eVar3.f5465e = true;
        this.n.f5465e = false;
        return eVar3;
    }

    /* access modifiers changed from: package-private */
    public void d(int i2) {
        this.f5451e--;
        int i3 = this.f5450d + this.f5451e;
        if (i2 < i3) {
            K[] kArr = this.f5448b;
            kArr[i2] = kArr[i3];
            V[] vArr = this.f5449c;
            vArr[i2] = vArr[i3];
            kArr[i3] = null;
            vArr[i3] = null;
            return;
        }
        this.f5448b[i2] = null;
        this.f5449c[i2] = null;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private boolean d(K r5) {
        /*
            r4 = this;
            K[] r0 = r4.f5448b
            int r1 = r4.f5450d
            int r2 = r4.f5451e
            int r2 = r2 + r1
        L_0x0007:
            if (r1 >= r2) goto L_0x0016
            r3 = r0[r1]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0013
            r5 = 1
            return r5
        L_0x0013:
            int r1 = r1 + 1
            goto L_0x0007
        L_0x0016:
            r5 = 0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.c0.d(java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public V a(K r4, V r5) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            int r1 = r3.f5454h
            r1 = r1 & r0
            K[] r2 = r3.f5448b
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0032
            int r1 = r3.e(r0)
            K[] r2 = r3.f5448b
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0032
            int r1 = r3.f(r0)
            K[] r0 = r3.f5448b
            r0 = r0[r1]
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x0032
            java.lang.Object r4 = r3.c(r4, r5)
            return r4
        L_0x0032:
            V[] r4 = r3.f5449c
            r4 = r4[r1]
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.c0.a(java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public V b(K r4) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            int r1 = r3.f5454h
            r1 = r1 & r0
            K[] r2 = r3.f5448b
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0033
            int r1 = r3.e(r0)
            K[] r2 = r3.f5448b
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0033
            int r1 = r3.f(r0)
            K[] r0 = r3.f5448b
            r0 = r0[r1]
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x0033
            r0 = 0
            java.lang.Object r4 = r3.c(r4, r0)
            return r4
        L_0x0033:
            V[] r4 = r3.f5449c
            r4 = r4[r1]
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.c0.b(java.lang.Object):java.lang.Object");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean a(K r4) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            int r1 = r3.f5454h
            r1 = r1 & r0
            K[] r2 = r3.f5448b
            r1 = r2[r1]
            boolean r1 = r4.equals(r1)
            if (r1 != 0) goto L_0x0032
            int r1 = r3.e(r0)
            K[] r2 = r3.f5448b
            r1 = r2[r1]
            boolean r1 = r4.equals(r1)
            if (r1 != 0) goto L_0x0032
            int r0 = r3.f(r0)
            K[] r1 = r3.f5448b
            r0 = r1[r0]
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x0032
            boolean r4 = r3.d(r4)
            return r4
        L_0x0032:
            r4 = 1
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.c0.a(java.lang.Object):boolean");
    }

    public c<K> b() {
        if (h.f5490a) {
            return new c<>(this);
        }
        if (this.p == null) {
            this.p = new c(this);
            this.q = new c(this);
        }
        c cVar = this.p;
        if (!cVar.f5465e) {
            cVar.b();
            c<K> cVar2 = this.p;
            cVar2.f5465e = true;
            this.q.f5465e = false;
            return cVar2;
        }
        this.q.b();
        c<K> cVar3 = this.q;
        cVar3.f5465e = true;
        this.p.f5465e = false;
        return cVar3;
    }

    public K a(Object obj, boolean z) {
        V[] vArr = this.f5449c;
        if (obj == null) {
            K[] kArr = this.f5448b;
            int i2 = this.f5450d + this.f5451e;
            while (true) {
                int i3 = i2 - 1;
                if (i2 <= 0) {
                    return null;
                }
                if (kArr[i3] != null && vArr[i3] == null) {
                    return kArr[i3];
                }
                i2 = i3;
            }
        } else if (z) {
            int i4 = this.f5450d + this.f5451e;
            while (true) {
                int i5 = i4 - 1;
                if (i4 <= 0) {
                    return null;
                }
                if (vArr[i5] == obj) {
                    return this.f5448b[i5];
                }
                i4 = i5;
            }
        } else {
            int i6 = this.f5450d + this.f5451e;
            while (true) {
                int i7 = i6 - 1;
                if (i6 <= 0) {
                    return null;
                }
                if (obj.equals(vArr[i7])) {
                    return this.f5448b[i7];
                }
                i6 = i7;
            }
        }
    }

    private String a(String str, boolean z) {
        int i2;
        if (this.f5447a == 0) {
            return z ? "{}" : "";
        }
        r0 r0Var = new r0(32);
        if (z) {
            r0Var.append('{');
        }
        K[] kArr = this.f5448b;
        V[] vArr = this.f5449c;
        int length = kArr.length;
        while (true) {
            i2 = length - 1;
            if (length > 0) {
                K k2 = kArr[i2];
                if (k2 != null) {
                    r0Var.a((Object) k2);
                    r0Var.append('=');
                    r0Var.a((Object) vArr[i2]);
                    break;
                }
                length = i2;
            } else {
                break;
            }
        }
        while (true) {
            int i3 = i2 - 1;
            if (i2 <= 0) {
                break;
            }
            K k3 = kArr[i3];
            if (k3 != null) {
                r0Var.a(str);
                r0Var.a((Object) k3);
                r0Var.append('=');
                r0Var.a((Object) vArr[i3]);
            }
            i2 = i3;
        }
        if (z) {
            r0Var.append('}');
        }
        return r0Var.toString();
    }

    public a<K, V> a() {
        if (h.f5490a) {
            return new a<>(this);
        }
        if (this.l == null) {
            this.l = new a(this);
            this.m = new a(this);
        }
        a aVar = this.l;
        if (!aVar.f5465e) {
            aVar.b();
            a<K, V> aVar2 = this.l;
            aVar2.f5465e = true;
            this.m.f5465e = false;
            return aVar2;
        }
        this.m.b();
        a<K, V> aVar3 = this.m;
        aVar3.f5465e = true;
        this.l.f5465e = false;
        return aVar3;
    }
}
