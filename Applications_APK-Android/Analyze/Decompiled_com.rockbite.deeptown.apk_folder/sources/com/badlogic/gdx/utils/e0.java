package com.badlogic.gdx.utils;

import com.badlogic.gdx.utils.c0;
import java.util.NoSuchElementException;

/* compiled from: OrderedMap */
public class e0<K, V> extends c0<K, V> {
    final a<K> s;

    /* compiled from: OrderedMap */
    public static class a<K, V> extends c0.a<K, V> {

        /* renamed from: g  reason: collision with root package name */
        private a<K> f5483g;

        public a(e0<K, V> e0Var) {
            super(e0Var);
            this.f5483g = e0Var.s;
        }

        public void b() {
            boolean z = false;
            this.f5463c = 0;
            if (this.f5462b.f5447a > 0) {
                z = true;
            }
            this.f5461a = z;
        }

        public void remove() {
            if (this.f5464d >= 0) {
                this.f5462b.remove(this.f5458f.f5459a);
                this.f5463c--;
                return;
            }
            throw new IllegalStateException("next must be called before remove.");
        }

        public c0.b next() {
            if (!this.f5461a) {
                throw new NoSuchElementException();
            } else if (this.f5465e) {
                this.f5458f.f5459a = this.f5483g.get(this.f5463c);
                c0.b<K, V> bVar = this.f5458f;
                bVar.f5460b = this.f5462b.b(bVar.f5459a);
                boolean z = true;
                this.f5463c++;
                if (this.f5463c >= this.f5462b.f5447a) {
                    z = false;
                }
                this.f5461a = z;
                return this.f5458f;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }
    }

    /* compiled from: OrderedMap */
    public static class b<K> extends c0.c<K> {

        /* renamed from: f  reason: collision with root package name */
        private a<K> f5484f;

        public b(e0<K, ?> e0Var) {
            super(e0Var);
            this.f5484f = e0Var.s;
        }

        public void b() {
            boolean z = false;
            this.f5463c = 0;
            if (this.f5462b.f5447a > 0) {
                z = true;
            }
            this.f5461a = z;
        }

        public K next() {
            if (!this.f5461a) {
                throw new NoSuchElementException();
            } else if (this.f5465e) {
                K k2 = this.f5484f.get(this.f5463c);
                int i2 = this.f5463c;
                this.f5464d = i2;
                boolean z = true;
                this.f5463c = i2 + 1;
                if (this.f5463c >= this.f5462b.f5447a) {
                    z = false;
                }
                this.f5461a = z;
                return k2;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }

        public void remove() {
            if (this.f5464d >= 0) {
                ((e0) this.f5462b).e(this.f5463c - 1);
                this.f5463c = this.f5464d;
                this.f5464d = -1;
                return;
            }
            throw new IllegalStateException("next must be called before remove.");
        }
    }

    /* compiled from: OrderedMap */
    public static class c<V> extends c0.e<V> {

        /* renamed from: f  reason: collision with root package name */
        private a f5485f;

        public c(e0<?, V> e0Var) {
            super(e0Var);
            this.f5485f = e0Var.s;
        }

        public void b() {
            boolean z = false;
            this.f5463c = 0;
            if (this.f5462b.f5447a > 0) {
                z = true;
            }
            this.f5461a = z;
        }

        public V next() {
            if (!this.f5461a) {
                throw new NoSuchElementException();
            } else if (this.f5465e) {
                V b2 = this.f5462b.b(this.f5485f.get(this.f5463c));
                int i2 = this.f5463c;
                this.f5464d = i2;
                boolean z = true;
                this.f5463c = i2 + 1;
                if (this.f5463c >= this.f5462b.f5447a) {
                    z = false;
                }
                this.f5461a = z;
                return b2;
            } else {
                throw new o("#iterator() cannot be used nested.");
            }
        }

        public void remove() {
            int i2 = this.f5464d;
            if (i2 >= 0) {
                ((e0) this.f5462b).e(i2);
                this.f5463c = this.f5464d;
                this.f5464d = -1;
                return;
            }
            throw new IllegalStateException("next must be called before remove.");
        }
    }

    public e0() {
        this.s = new a<>();
    }

    public c0.a<K, V> a() {
        if (h.f5490a) {
            return new c0.a<>(this);
        }
        if (this.l == null) {
            this.l = new a(this);
            this.m = new a(this);
        }
        c0.a aVar = this.l;
        if (!aVar.f5465e) {
            aVar.b();
            c0.a<K, V> aVar2 = this.l;
            aVar2.f5465e = true;
            this.m.f5465e = false;
            return aVar2;
        }
        this.m.b();
        c0.a<K, V> aVar3 = this.m;
        aVar3.f5465e = true;
        this.l.f5465e = false;
        return aVar3;
    }

    public V b(K k2, V v) {
        if (!a(k2)) {
            this.s.add(k2);
        }
        return super.b(k2, v);
    }

    public void c(int i2) {
        this.s.clear();
        super.c(i2);
    }

    public void clear() {
        this.s.clear();
        super.clear();
    }

    public V e(int i2) {
        return super.remove(this.s.d(i2));
    }

    public V remove(K k2) {
        this.s.d(k2, false);
        return super.remove(k2);
    }

    public String toString() {
        if (this.f5447a == 0) {
            return "{}";
        }
        r0 r0Var = new r0(32);
        r0Var.append('{');
        a<K> aVar = this.s;
        int i2 = aVar.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            K k2 = aVar.get(i3);
            if (i3 > 0) {
                r0Var.a(", ");
            }
            r0Var.a((Object) k2);
            r0Var.append('=');
            r0Var.a(b(k2));
        }
        r0Var.append('}');
        return r0Var.toString();
    }

    public c0.a<K, V> iterator() {
        return a();
    }

    public e0(int i2) {
        super(i2);
        this.s = new a<>(this.f5450d);
    }

    public c0.c<K> b() {
        if (h.f5490a) {
            return new c0.c<>(this);
        }
        if (this.p == null) {
            this.p = new b(this);
            this.q = new b(this);
        }
        c0.c cVar = this.p;
        if (!cVar.f5465e) {
            cVar.b();
            c0.c<K> cVar2 = this.p;
            cVar2.f5465e = true;
            this.q.f5465e = false;
            return cVar2;
        }
        this.q.b();
        c0.c<K> cVar3 = this.q;
        cVar3.f5465e = true;
        this.p.f5465e = false;
        return cVar3;
    }

    public c0.e<V> c() {
        if (h.f5490a) {
            return new c0.e<>(this);
        }
        if (this.n == null) {
            this.n = new c(this);
            this.o = new c(this);
        }
        c0.e eVar = this.n;
        if (!eVar.f5465e) {
            eVar.b();
            c0.e<V> eVar2 = this.n;
            eVar2.f5465e = true;
            this.o.f5465e = false;
            return eVar2;
        }
        this.o.b();
        c0.e<V> eVar3 = this.o;
        eVar3.f5465e = true;
        this.n.f5465e = false;
        return eVar3;
    }
}
