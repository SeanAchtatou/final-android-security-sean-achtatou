package com.badlogic.gdx.utils.z0;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: Method */
public final class e {

    /* renamed from: a  reason: collision with root package name */
    private final Method f5719a;

    e(Method method) {
        this.f5719a = method;
    }

    public String a() {
        return this.f5719a.getName();
    }

    public Object a(Object obj, Object... objArr) throws f {
        try {
            return this.f5719a.invoke(obj, objArr);
        } catch (IllegalArgumentException e2) {
            throw new f("Illegal argument(s) supplied to method: " + a(), e2);
        } catch (IllegalAccessException e3) {
            throw new f("Illegal access to method: " + a(), e3);
        } catch (InvocationTargetException e4) {
            throw new f("Exception occurred in method: " + a(), e4);
        }
    }
}
