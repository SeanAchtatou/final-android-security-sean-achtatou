package com.badlogic.gdx.utils;

import com.google.protobuf.CodedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/* compiled from: StreamUtils */
public final class q0 {

    /* compiled from: StreamUtils */
    public static class a extends ByteArrayOutputStream {
        public a(int i2) {
            super(i2);
        }

        public synchronized byte[] toByteArray() {
            if (this.count == this.buf.length) {
                return this.buf;
            }
            return super.toByteArray();
        }
    }

    public static void a(InputStream inputStream, OutputStream outputStream) throws IOException {
        a(inputStream, outputStream, new byte[CodedOutputStream.DEFAULT_BUFFER_SIZE]);
    }

    public static void a(InputStream inputStream, OutputStream outputStream, byte[] bArr) throws IOException {
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public static void a(InputStream inputStream, ByteBuffer byteBuffer) throws IOException {
        a(inputStream, byteBuffer, new byte[CodedOutputStream.DEFAULT_BUFFER_SIZE]);
    }

    public static int a(InputStream inputStream, ByteBuffer byteBuffer, byte[] bArr) throws IOException {
        int position = byteBuffer.position();
        int i2 = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                BufferUtils.a(bArr, 0, byteBuffer, read);
                i2 += read;
                byteBuffer.position(position + i2);
            } else {
                byteBuffer.position(position);
                return i2;
            }
        }
    }

    public static byte[] a(InputStream inputStream, int i2) throws IOException {
        a aVar = new a(Math.max(0, i2));
        a(inputStream, aVar);
        return aVar.toByteArray();
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable unused) {
            }
        }
    }
}
