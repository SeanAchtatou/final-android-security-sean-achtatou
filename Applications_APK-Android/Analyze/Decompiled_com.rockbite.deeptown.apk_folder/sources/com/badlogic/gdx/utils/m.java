package com.badlogic.gdx.utils;

/* compiled from: FloatArray */
public class m {

    /* renamed from: a  reason: collision with root package name */
    public float[] f5527a;

    /* renamed from: b  reason: collision with root package name */
    public int f5528b;

    /* renamed from: c  reason: collision with root package name */
    public boolean f5529c;

    public m() {
        this(true, 16);
    }

    public void a(float f2) {
        float[] fArr = this.f5527a;
        int i2 = this.f5528b;
        if (i2 == fArr.length) {
            fArr = c(Math.max(8, (int) (((float) i2) * 1.75f)));
        }
        int i3 = this.f5528b;
        this.f5528b = i3 + 1;
        fArr[i3] = f2;
    }

    public float b(int i2) {
        if (i2 < this.f5528b) {
            return this.f5527a[i2];
        }
        throw new IndexOutOfBoundsException("index can't be >= size: " + i2 + " >= " + this.f5528b);
    }

    public float c() {
        return this.f5527a[this.f5528b - 1];
    }

    public float[] d(int i2) {
        if (i2 >= 0) {
            if (i2 > this.f5527a.length) {
                c(Math.max(8, i2));
            }
            this.f5528b = i2;
            return this.f5527a;
        }
        throw new IllegalArgumentException("newSize must be >= 0: " + i2);
    }

    public void e(int i2) {
        if (this.f5528b > i2) {
            this.f5528b = i2;
        }
    }

    public boolean equals(Object obj) {
        int i2;
        if (obj == this) {
            return true;
        }
        if (!this.f5529c || !(obj instanceof m)) {
            return false;
        }
        m mVar = (m) obj;
        if (!mVar.f5529c || (i2 = this.f5528b) != mVar.f5528b) {
            return false;
        }
        float[] fArr = this.f5527a;
        float[] fArr2 = mVar.f5527a;
        for (int i3 = 0; i3 < i2; i3++) {
            if (fArr[i3] != fArr2[i3]) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        if (!this.f5529c) {
            return super.hashCode();
        }
        float[] fArr = this.f5527a;
        int i2 = this.f5528b;
        int i3 = 1;
        for (int i4 = 0; i4 < i2; i4++) {
            i3 = (i3 * 31) + Float.floatToIntBits(fArr[i4]);
        }
        return i3;
    }

    public String toString() {
        if (this.f5528b == 0) {
            return "[]";
        }
        float[] fArr = this.f5527a;
        r0 r0Var = new r0(32);
        r0Var.append('[');
        r0Var.a(fArr[0]);
        for (int i2 = 1; i2 < this.f5528b; i2++) {
            r0Var.a(", ");
            r0Var.a(fArr[i2]);
        }
        r0Var.append(']');
        return r0Var.toString();
    }

    public m(int i2) {
        this(true, i2);
    }

    /* access modifiers changed from: protected */
    public float[] c(int i2) {
        float[] fArr = new float[i2];
        System.arraycopy(this.f5527a, 0, fArr, 0, Math.min(this.f5528b, fArr.length));
        this.f5527a = fArr;
        return fArr;
    }

    public m(boolean z, int i2) {
        this.f5529c = z;
        this.f5527a = new float[i2];
    }

    public void a(m mVar) {
        a(mVar.f5527a, 0, mVar.f5528b);
    }

    public float b() {
        if (this.f5528b != 0) {
            return this.f5527a[0];
        }
        throw new IllegalStateException("Array is empty.");
    }

    public void a(m mVar, int i2, int i3) {
        if (i2 + i3 <= mVar.f5528b) {
            a(mVar.f5527a, i2, i3);
            return;
        }
        throw new IllegalArgumentException("offset + length must be <= size: " + i2 + " + " + i3 + " <= " + mVar.f5528b);
    }

    public float[] d() {
        int i2 = this.f5528b;
        float[] fArr = new float[i2];
        System.arraycopy(this.f5527a, 0, fArr, 0, i2);
        return fArr;
    }

    public void a(float[] fArr, int i2, int i3) {
        float[] fArr2 = this.f5527a;
        int i4 = this.f5528b + i3;
        if (i4 > fArr2.length) {
            fArr2 = c(Math.max(8, (int) (((float) i4) * 1.75f)));
        }
        System.arraycopy(fArr, i2, fArr2, this.f5528b, i3);
        this.f5528b += i3;
    }

    public void a(int i2, float f2) {
        if (i2 < this.f5528b) {
            this.f5527a[i2] = f2;
            return;
        }
        throw new IndexOutOfBoundsException("index can't be >= size: " + i2 + " >= " + this.f5528b);
    }

    public void a(int i2, int i3) {
        int i4 = this.f5528b;
        if (i3 >= i4) {
            throw new IndexOutOfBoundsException("end can't be >= size: " + i3 + " >= " + this.f5528b);
        } else if (i2 <= i3) {
            int i5 = (i3 - i2) + 1;
            int i6 = i4 - i5;
            if (this.f5529c) {
                float[] fArr = this.f5527a;
                int i7 = i5 + i2;
                System.arraycopy(fArr, i7, fArr, i2, i4 - i7);
            } else {
                int max = Math.max(i6, i3 + 1);
                float[] fArr2 = this.f5527a;
                System.arraycopy(fArr2, max, fArr2, i2, i4 - max);
            }
            this.f5528b = i6;
        } else {
            throw new IndexOutOfBoundsException("start can't be > end: " + i2 + " > " + i3);
        }
    }

    public void a() {
        this.f5528b = 0;
    }

    public float[] a(int i2) {
        if (i2 >= 0) {
            int i3 = this.f5528b + i2;
            if (i3 > this.f5527a.length) {
                c(Math.max(8, i3));
            }
            return this.f5527a;
        }
        throw new IllegalArgumentException("additionalCapacity must be >= 0: " + i2);
    }
}
