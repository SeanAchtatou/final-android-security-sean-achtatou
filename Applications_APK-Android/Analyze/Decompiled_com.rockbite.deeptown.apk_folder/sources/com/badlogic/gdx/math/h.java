package com.badlogic.gdx.math;

import java.util.Random;

/* compiled from: MathUtils */
public final class h {

    /* renamed from: a  reason: collision with root package name */
    public static Random f5267a = new m();

    /* compiled from: MathUtils */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        static final float[] f5268a = new float[16384];

        static {
            for (int i2 = 0; i2 < 16384; i2++) {
                f5268a[i2] = (float) Math.sin((double) (((((float) i2) + 0.5f) / 16384.0f) * 6.2831855f));
            }
            for (int i3 = 0; i3 < 360; i3 += 90) {
                float f2 = (float) i3;
                f5268a[((int) (45.511112f * f2)) & 16383] = (float) Math.sin((double) (f2 * 0.017453292f));
            }
        }
    }

    public static float a(float f2) {
        return a.f5268a[((int) ((f2 + 1.5707964f) * 2607.5945f)) & 16383];
    }

    public static float a(float f2, float f3, float f4) {
        return f2 < f3 ? f3 : f2 > f4 ? f4 : f2;
    }

    public static int a(int i2, int i3, int i4) {
        return i2 < i3 ? i3 : i2 > i4 ? i4 : i2;
    }

    public static boolean a(int i2) {
        return i2 != 0 && (i2 & (i2 + -1)) == 0;
    }

    public static float b(float f2) {
        return a.f5268a[((int) ((f2 + 90.0f) * 45.511112f)) & 16383];
    }

    public static int b(int i2) {
        if (i2 == 0) {
            return 1;
        }
        int i3 = i2 - 1;
        int i4 = i3 | (i3 >> 1);
        int i5 = i4 | (i4 >> 2);
        int i6 = i5 | (i5 >> 4);
        int i7 = i6 | (i6 >> 8);
        return (i7 | (i7 >> 16)) + 1;
    }

    public static int c(int i2) {
        return f5267a.nextInt(i2 + 1);
    }

    public static float d(float f2) {
        return f5267a.nextFloat() * f2;
    }

    public static boolean e(float f2) {
        return a() < f2;
    }

    public static int f(float f2) {
        double d2 = (double) f2;
        Double.isNaN(d2);
        return ((int) (d2 + 16384.5d)) - 16384;
    }

    public static float g(float f2) {
        return a.f5268a[((int) (f2 * 2607.5945f)) & 16383];
    }

    public static float h(float f2) {
        return a.f5268a[((int) (f2 * 45.511112f)) & 16383];
    }

    public static int a(int i2, int i3) {
        return i2 + f5267a.nextInt((i3 - i2) + 1);
    }

    public static boolean b() {
        return f5267a.nextBoolean();
    }

    public static float c(float f2, float f3) {
        return c(f2, f3, (f2 + f3) * 0.5f);
    }

    public static float a() {
        return f5267a.nextFloat();
    }

    public static float b(float f2, float f3) {
        return f2 + (f5267a.nextFloat() * (f3 - f2));
    }

    public static float c(float f2, float f3, float f4) {
        float nextFloat = f5267a.nextFloat();
        float f5 = f3 - f2;
        float f6 = f4 - f2;
        if (nextFloat <= f6 / f5) {
            return f2 + ((float) Math.sqrt((double) (nextFloat * f5 * f6)));
        }
        return f3 - ((float) Math.sqrt((double) (((1.0f - nextFloat) * f5) * (f3 - f4))));
    }

    public static boolean a(float f2, float f3) {
        return Math.abs(f2 - f3) <= 1.0E-6f;
    }

    public static boolean b(float f2, float f3, float f4) {
        return Math.abs(f2 - f3) <= f4;
    }

    public static int c(float f2) {
        double d2 = (double) f2;
        Double.isNaN(d2);
        return ((int) (d2 + 16384.0d)) - 16384;
    }
}
