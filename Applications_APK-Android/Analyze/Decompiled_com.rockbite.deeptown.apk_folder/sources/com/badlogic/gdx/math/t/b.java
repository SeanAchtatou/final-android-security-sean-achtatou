package com.badlogic.gdx.math.t;

import com.badlogic.gdx.math.q;
import java.io.Serializable;

/* compiled from: Ray */
public class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public final q f5308a = new q();

    /* renamed from: b  reason: collision with root package name */
    public final q f5309b = new q();

    static {
        new q();
    }

    public b() {
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != b.class) {
            return false;
        }
        b bVar = (b) obj;
        if (!this.f5309b.equals(bVar.f5309b) || !this.f5308a.equals(bVar.f5308a)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((this.f5309b.hashCode() + 73) * 73) + this.f5308a.hashCode();
    }

    public String toString() {
        return "ray [" + this.f5308a + ":" + this.f5309b + "]";
    }

    public b(q qVar, q qVar2) {
        this.f5308a.d(qVar);
        q qVar3 = this.f5309b;
        qVar3.d(qVar2);
        qVar3.c();
    }
}
