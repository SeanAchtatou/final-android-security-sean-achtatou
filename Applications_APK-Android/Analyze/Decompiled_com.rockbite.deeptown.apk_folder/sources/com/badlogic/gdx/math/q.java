package com.badlogic.gdx.math;

import com.badlogic.gdx.utils.z;
import com.esotericsoftware.spine.Animation;
import java.io.Serializable;

/* compiled from: Vector3 */
public class q implements Serializable, r<q> {

    /* renamed from: a  reason: collision with root package name */
    public float f5296a;

    /* renamed from: b  reason: collision with root package name */
    public float f5297b;

    /* renamed from: c  reason: collision with root package name */
    public float f5298c;

    static {
        new q(1.0f, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        new q(Animation.CurveTimeline.LINEAR, 1.0f, Animation.CurveTimeline.LINEAR);
        new q(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f);
        new q(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        new Matrix4();
    }

    public q() {
    }

    public q a(q qVar) {
        a(qVar.f5296a, qVar.f5297b, qVar.f5298c);
        return this;
    }

    public float b() {
        float f2 = this.f5296a;
        float f3 = this.f5297b;
        float f4 = (f2 * f2) + (f3 * f3);
        float f5 = this.f5298c;
        return f4 + (f5 * f5);
    }

    public q c(float f2, float f3, float f4) {
        this.f5296a = f2;
        this.f5297b = f3;
        this.f5298c = f4;
        return this;
    }

    public q d(q qVar) {
        c(qVar.f5296a, qVar.f5297b, qVar.f5298c);
        return this;
    }

    public q e(q qVar) {
        d(qVar.f5296a, qVar.f5297b, qVar.f5298c);
        return this;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || q.class != obj.getClass()) {
            return false;
        }
        q qVar = (q) obj;
        return z.a(this.f5296a) == z.a(qVar.f5296a) && z.a(this.f5297b) == z.a(qVar.f5297b) && z.a(this.f5298c) == z.a(qVar.f5298c);
    }

    public int hashCode() {
        return ((((z.a(this.f5296a) + 31) * 31) + z.a(this.f5297b)) * 31) + z.a(this.f5298c);
    }

    public String toString() {
        return "(" + this.f5296a + "," + this.f5297b + "," + this.f5298c + ")";
    }

    public q(float f2, float f3, float f4) {
        c(f2, f3, f4);
    }

    public q a(float f2, float f3, float f4) {
        c(this.f5296a + f2, this.f5297b + f3, this.f5298c + f4);
        return this;
    }

    public q b(q qVar) {
        float f2 = this.f5297b;
        float f3 = qVar.f5298c;
        float f4 = this.f5298c;
        float f5 = qVar.f5297b;
        float f6 = qVar.f5296a;
        float f7 = this.f5296a;
        c((f2 * f3) - (f4 * f5), (f4 * f6) - (f3 * f7), (f7 * f5) - (f2 * f6));
        return this;
    }

    public q d(float f2, float f3, float f4) {
        c(this.f5296a - f2, this.f5297b - f3, this.f5298c - f4);
        return this;
    }

    public q a(float f2) {
        c(this.f5296a * f2, this.f5297b * f2, this.f5298c * f2);
        return this;
    }

    public q b(float f2, float f3, float f4) {
        float f5 = this.f5297b;
        float f6 = this.f5298c;
        float f7 = this.f5296a;
        c((f5 * f4) - (f6 * f3), (f6 * f2) - (f4 * f7), (f7 * f3) - (f5 * f2));
        return this;
    }

    public q(q qVar) {
        d(qVar);
    }

    public float a() {
        float f2 = this.f5296a;
        float f3 = this.f5297b;
        float f4 = (f2 * f2) + (f3 * f3);
        float f5 = this.f5298c;
        return (float) Math.sqrt((double) (f4 + (f5 * f5)));
    }

    public q b(Matrix4 matrix4) {
        float[] fArr = matrix4.f5238a;
        float f2 = this.f5296a;
        float f3 = this.f5297b;
        float f4 = (fArr[3] * f2) + (fArr[7] * f3);
        float f5 = this.f5298c;
        float f6 = 1.0f / ((f4 + (fArr[11] * f5)) + fArr[15]);
        c(((fArr[0] * f2) + (fArr[4] * f3) + (fArr[8] * f5) + fArr[12]) * f6, ((fArr[1] * f2) + (fArr[5] * f3) + (fArr[9] * f5) + fArr[13]) * f6, ((f2 * fArr[2]) + (f3 * fArr[6]) + (f5 * fArr[10]) + fArr[14]) * f6);
        return this;
    }

    public q c() {
        float b2 = b();
        if (!(b2 == Animation.CurveTimeline.LINEAR || b2 == 1.0f)) {
            a(1.0f / ((float) Math.sqrt((double) b2)));
        }
        return this;
    }

    public q a(Matrix4 matrix4) {
        float[] fArr = matrix4.f5238a;
        float f2 = this.f5296a;
        float f3 = this.f5297b;
        float f4 = (fArr[0] * f2) + (fArr[4] * f3);
        float f5 = this.f5298c;
        c(f4 + (fArr[8] * f5) + fArr[12], (fArr[1] * f2) + (fArr[5] * f3) + (fArr[9] * f5) + fArr[13], (f2 * fArr[2]) + (f3 * fArr[6]) + (f5 * fArr[10]) + fArr[14]);
        return this;
    }

    public float c(q qVar) {
        return (this.f5296a * qVar.f5296a) + (this.f5297b * qVar.f5297b) + (this.f5298c * qVar.f5298c);
    }
}
