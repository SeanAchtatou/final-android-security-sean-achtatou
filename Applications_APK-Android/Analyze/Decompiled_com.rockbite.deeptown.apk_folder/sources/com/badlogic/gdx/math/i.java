package com.badlogic.gdx.math;

import com.esotericsoftware.spine.Animation;
import java.io.Serializable;

/* compiled from: Matrix3 */
public class i implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public float[] f5269a = new float[9];

    /* renamed from: b  reason: collision with root package name */
    private float[] f5270b = new float[9];

    public i() {
        a();
    }

    public i a() {
        float[] fArr = this.f5269a;
        fArr[0] = 1.0f;
        fArr[1] = 0.0f;
        fArr[2] = 0.0f;
        fArr[3] = 0.0f;
        fArr[4] = 1.0f;
        fArr[5] = 0.0f;
        fArr[6] = 0.0f;
        fArr[7] = 0.0f;
        fArr[8] = 1.0f;
        return this;
    }

    public i b(float f2, float f3) {
        float[] fArr = this.f5269a;
        float[] fArr2 = this.f5270b;
        fArr2[0] = 1.0f;
        fArr2[1] = 0.0f;
        fArr2[2] = 0.0f;
        fArr2[3] = 0.0f;
        fArr2[4] = 1.0f;
        fArr2[5] = 0.0f;
        fArr2[6] = f2;
        fArr2[7] = f3;
        fArr2[8] = 1.0f;
        a(fArr, fArr2);
        return this;
    }

    public String toString() {
        float[] fArr = this.f5269a;
        return "[" + fArr[0] + "|" + fArr[3] + "|" + fArr[6] + "]\n[" + fArr[1] + "|" + fArr[4] + "|" + fArr[7] + "]\n[" + fArr[2] + "|" + fArr[5] + "|" + fArr[8] + "]";
    }

    public i a(float f2) {
        b(f2 * 0.017453292f);
        return this;
    }

    public i a(float f2, float f3) {
        float[] fArr = this.f5270b;
        fArr[0] = f2;
        fArr[1] = 0.0f;
        fArr[2] = 0.0f;
        fArr[3] = 0.0f;
        fArr[4] = f3;
        fArr[5] = 0.0f;
        fArr[6] = 0.0f;
        fArr[7] = 0.0f;
        fArr[8] = 1.0f;
        a(this.f5269a, fArr);
        return this;
    }

    public i b(float f2) {
        if (f2 == Animation.CurveTimeline.LINEAR) {
            return this;
        }
        double d2 = (double) f2;
        float cos = (float) Math.cos(d2);
        float sin = (float) Math.sin(d2);
        float[] fArr = this.f5270b;
        fArr[0] = cos;
        fArr[1] = sin;
        fArr[2] = 0.0f;
        fArr[3] = -sin;
        fArr[4] = cos;
        fArr[5] = 0.0f;
        fArr[6] = 0.0f;
        fArr[7] = 0.0f;
        fArr[8] = 1.0f;
        a(this.f5269a, fArr);
        return this;
    }

    private static void a(float[] fArr, float[] fArr2) {
        float f2 = (fArr[0] * fArr2[0]) + (fArr[3] * fArr2[1]) + (fArr[6] * fArr2[2]);
        float f3 = (fArr[0] * fArr2[3]) + (fArr[3] * fArr2[4]) + (fArr[6] * fArr2[5]);
        float f4 = (fArr[0] * fArr2[6]) + (fArr[3] * fArr2[7]) + (fArr[6] * fArr2[8]);
        float f5 = (fArr[1] * fArr2[0]) + (fArr[4] * fArr2[1]) + (fArr[7] * fArr2[2]);
        float f6 = (fArr[1] * fArr2[3]) + (fArr[4] * fArr2[4]) + (fArr[7] * fArr2[5]);
        float f7 = (fArr[1] * fArr2[6]) + (fArr[4] * fArr2[7]) + (fArr[7] * fArr2[8]);
        float f8 = (fArr[2] * fArr2[0]) + (fArr[5] * fArr2[1]) + (fArr[8] * fArr2[2]);
        float f9 = (fArr[2] * fArr2[3]) + (fArr[5] * fArr2[4]) + (fArr[8] * fArr2[5]);
        fArr[0] = f2;
        fArr[1] = f5;
        fArr[2] = f8;
        fArr[3] = f3;
        fArr[4] = f6;
        fArr[5] = f9;
        fArr[6] = f4;
        fArr[7] = f7;
        fArr[8] = (fArr[2] * fArr2[6]) + (fArr[5] * fArr2[7]) + (fArr[8] * fArr2[8]);
    }
}
