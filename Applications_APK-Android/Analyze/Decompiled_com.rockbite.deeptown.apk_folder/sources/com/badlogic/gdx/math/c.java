package com.badlogic.gdx.math;

import com.badlogic.gdx.utils.n0;
import com.badlogic.gdx.utils.q;

/* compiled from: EarClippingTriangulator */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private final n0 f5245a = new n0();

    /* renamed from: b  reason: collision with root package name */
    private short[] f5246b;

    /* renamed from: c  reason: collision with root package name */
    private float[] f5247c;

    /* renamed from: d  reason: collision with root package name */
    private int f5248d;

    /* renamed from: e  reason: collision with root package name */
    private final q f5249e = new q();

    /* renamed from: f  reason: collision with root package name */
    private final n0 f5250f = new n0();

    private void b() {
        int i2;
        int[] iArr = this.f5249e.f5558a;
        while (true) {
            i2 = this.f5248d;
            if (i2 <= 3) {
                break;
            }
            int a2 = a();
            b(a2);
            int e2 = e(a2);
            if (a2 == this.f5248d) {
                a2 = 0;
            }
            iArr[e2] = a(e2);
            iArr[a2] = a(a2);
        }
        if (i2 == 3) {
            n0 n0Var = this.f5250f;
            short[] sArr = this.f5246b;
            n0Var.a(sArr[0]);
            n0Var.a(sArr[1]);
            n0Var.a(sArr[2]);
        }
    }

    private boolean c(int i2) {
        int[] iArr = this.f5249e.f5558a;
        if (iArr[i2] == -1) {
            return false;
        }
        int e2 = e(i2);
        int d2 = d(i2);
        short[] sArr = this.f5246b;
        int i3 = sArr[e2] * 2;
        int i4 = sArr[i2] * 2;
        int i5 = sArr[d2] * 2;
        float[] fArr = this.f5247c;
        float f2 = fArr[i3];
        int i6 = 1;
        float f3 = fArr[i3 + 1];
        float f4 = fArr[i4];
        float f5 = fArr[i4 + 1];
        float f6 = fArr[i5];
        float f7 = fArr[i5 + 1];
        int d3 = d(d2);
        while (d3 != e2) {
            if (iArr[d3] != i6) {
                int i7 = sArr[d3] * 2;
                float f8 = fArr[i7];
                float f9 = fArr[i7 + i6];
                if (a(f6, f7, f2, f3, f8, f9) >= 0 && a(f2, f3, f4, f5, f8, f9) >= 0 && a(f4, f5, f6, f7, f8, f9) >= 0) {
                    return false;
                }
            }
            d3 = d(d3);
            i6 = 1;
        }
        return true;
    }

    private int d(int i2) {
        return (i2 + 1) % this.f5248d;
    }

    private int e(int i2) {
        if (i2 == 0) {
            i2 = this.f5248d;
        }
        return i2 - 1;
    }

    public n0 a(float[] fArr) {
        return a(fArr, 0, fArr.length);
    }

    public n0 a(float[] fArr, int i2, int i3) {
        this.f5247c = fArr;
        int i4 = i3 / 2;
        this.f5248d = i4;
        int i5 = i2 / 2;
        n0 n0Var = this.f5245a;
        n0Var.a();
        n0Var.b(i4);
        n0Var.f5543b = i4;
        short[] sArr = n0Var.f5542a;
        this.f5246b = sArr;
        if (e.a(fArr, i2, i3)) {
            for (short s = 0; s < i4; s = (short) (s + 1)) {
                sArr[s] = (short) (i5 + s);
            }
        } else {
            int i6 = i4 - 1;
            for (int i7 = 0; i7 < i4; i7++) {
                sArr[i7] = (short) ((i5 + i6) - i7);
            }
        }
        q qVar = this.f5249e;
        qVar.a();
        qVar.b(i4);
        for (int i8 = 0; i8 < i4; i8++) {
            qVar.a(a(i8));
        }
        n0 n0Var2 = this.f5250f;
        n0Var2.a();
        n0Var2.b(Math.max(0, i4 - 2) * 3);
        b();
        return n0Var2;
    }

    private void b(int i2) {
        short[] sArr = this.f5246b;
        n0 n0Var = this.f5250f;
        n0Var.a(sArr[e(i2)]);
        n0Var.a(sArr[i2]);
        n0Var.a(sArr[d(i2)]);
        this.f5245a.d(i2);
        this.f5249e.d(i2);
        this.f5248d--;
    }

    private int a(int i2) {
        short[] sArr = this.f5246b;
        int i3 = sArr[e(i2)] * 2;
        int i4 = sArr[i2] * 2;
        int i5 = sArr[d(i2)] * 2;
        float[] fArr = this.f5247c;
        return a(fArr[i3], fArr[i3 + 1], fArr[i4], fArr[i4 + 1], fArr[i5], fArr[i5 + 1]);
    }

    private int a() {
        int i2 = this.f5248d;
        for (int i3 = 0; i3 < i2; i3++) {
            if (c(i3)) {
                return i3;
            }
        }
        int[] iArr = this.f5249e.f5558a;
        for (int i4 = 0; i4 < i2; i4++) {
            if (iArr[i4] != -1) {
                return i4;
            }
        }
        return 0;
    }

    private static int a(float f2, float f3, float f4, float f5, float f6, float f7) {
        return (int) Math.signum((f2 * (f7 - f5)) + (f4 * (f3 - f7)) + (f6 * (f5 - f3)));
    }
}
