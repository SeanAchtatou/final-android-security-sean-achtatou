package com.badlogic.gdx.math;

import com.esotericsoftware.spine.Animation;
import java.io.Serializable;

/* compiled from: Plane */
public class j implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public final q f5271a = new q();

    /* renamed from: b  reason: collision with root package name */
    public float f5272b = Animation.CurveTimeline.LINEAR;

    public j(q qVar, float f2) {
        q qVar2 = this.f5271a;
        qVar2.d(qVar);
        qVar2.c();
        this.f5272b = f2;
    }

    public void a(q qVar, q qVar2, q qVar3) {
        q qVar4 = this.f5271a;
        qVar4.d(qVar);
        qVar4.e(qVar2);
        qVar4.b(qVar2.f5296a - qVar3.f5296a, qVar2.f5297b - qVar3.f5297b, qVar2.f5298c - qVar3.f5298c);
        qVar4.c();
        this.f5272b = -qVar.c(this.f5271a);
    }

    public String toString() {
        return this.f5271a.toString() + ", " + this.f5272b;
    }
}
