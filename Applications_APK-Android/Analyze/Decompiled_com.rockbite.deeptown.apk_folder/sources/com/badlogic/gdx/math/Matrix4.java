package com.badlogic.gdx.math;

import com.esotericsoftware.spine.Animation;
import java.io.Serializable;

public class Matrix4 implements Serializable {

    /* renamed from: b  reason: collision with root package name */
    private static final float[] f5232b = new float[16];

    /* renamed from: c  reason: collision with root package name */
    static final q f5233c = new q();

    /* renamed from: d  reason: collision with root package name */
    static final q f5234d = new q();

    /* renamed from: e  reason: collision with root package name */
    static final q f5235e = new q();

    /* renamed from: f  reason: collision with root package name */
    static final q f5236f = new q();

    /* renamed from: g  reason: collision with root package name */
    static final Matrix4 f5237g = new Matrix4();

    /* renamed from: a  reason: collision with root package name */
    public final float[] f5238a = new float[16];

    static {
        new l();
        new l();
        new q();
        new q();
        new q();
    }

    public Matrix4() {
        float[] fArr = this.f5238a;
        fArr[0] = 1.0f;
        fArr[5] = 1.0f;
        fArr[10] = 1.0f;
        fArr[15] = 1.0f;
    }

    public static native boolean inv(float[] fArr);

    public static native void mul(float[] fArr, float[] fArr2);

    public static native void prj(float[] fArr, float[] fArr2, int i2, int i3, int i4);

    public Matrix4 a(float[] fArr) {
        float[] fArr2 = this.f5238a;
        System.arraycopy(fArr, 0, fArr2, 0, fArr2.length);
        return this;
    }

    public Matrix4 b(Matrix4 matrix4) {
        a(matrix4.f5238a);
        return this;
    }

    public Matrix4 c(float f2, float f3, float f4) {
        float[] fArr = f5232b;
        fArr[0] = 1.0f;
        fArr[4] = 0.0f;
        fArr[8] = 0.0f;
        fArr[12] = f2;
        fArr[1] = 0.0f;
        fArr[5] = 1.0f;
        fArr[9] = 0.0f;
        fArr[13] = f3;
        fArr[2] = 0.0f;
        fArr[6] = 0.0f;
        fArr[10] = 1.0f;
        fArr[14] = f4;
        fArr[3] = 0.0f;
        fArr[7] = 0.0f;
        fArr[11] = 0.0f;
        fArr[15] = 1.0f;
        mul(this.f5238a, fArr);
        return this;
    }

    public String toString() {
        return "[" + this.f5238a[0] + "|" + this.f5238a[4] + "|" + this.f5238a[8] + "|" + this.f5238a[12] + "]\n[" + this.f5238a[1] + "|" + this.f5238a[5] + "|" + this.f5238a[9] + "|" + this.f5238a[13] + "]\n[" + this.f5238a[2] + "|" + this.f5238a[6] + "|" + this.f5238a[10] + "|" + this.f5238a[14] + "]\n[" + this.f5238a[3] + "|" + this.f5238a[7] + "|" + this.f5238a[11] + "|" + this.f5238a[15] + "]\n";
    }

    public Matrix4 a(q qVar, l lVar, q qVar2) {
        a(qVar.f5296a, qVar.f5297b, qVar.f5298c, lVar.f5283a, lVar.f5284b, lVar.f5285c, lVar.f5286d, qVar2.f5296a, qVar2.f5297b, qVar2.f5298c);
        return this;
    }

    public Matrix4 b() {
        float[] fArr = this.f5238a;
        float f2 = (((((((((((((((((((((((((fArr[3] * fArr[6]) * fArr[9]) * fArr[12]) - (((fArr[2] * fArr[7]) * fArr[9]) * fArr[12])) - (((fArr[3] * fArr[5]) * fArr[10]) * fArr[12])) + (((fArr[1] * fArr[7]) * fArr[10]) * fArr[12])) + (((fArr[2] * fArr[5]) * fArr[11]) * fArr[12])) - (((fArr[1] * fArr[6]) * fArr[11]) * fArr[12])) - (((fArr[3] * fArr[6]) * fArr[8]) * fArr[13])) + (((fArr[2] * fArr[7]) * fArr[8]) * fArr[13])) + (((fArr[3] * fArr[4]) * fArr[10]) * fArr[13])) - (((fArr[0] * fArr[7]) * fArr[10]) * fArr[13])) - (((fArr[2] * fArr[4]) * fArr[11]) * fArr[13])) + (((fArr[0] * fArr[6]) * fArr[11]) * fArr[13])) + (((fArr[3] * fArr[5]) * fArr[8]) * fArr[14])) - (((fArr[1] * fArr[7]) * fArr[8]) * fArr[14])) - (((fArr[3] * fArr[4]) * fArr[9]) * fArr[14])) + (((fArr[0] * fArr[7]) * fArr[9]) * fArr[14])) + (((fArr[1] * fArr[4]) * fArr[11]) * fArr[14])) - (((fArr[0] * fArr[5]) * fArr[11]) * fArr[14])) - (((fArr[2] * fArr[5]) * fArr[8]) * fArr[15])) + (((fArr[1] * fArr[6]) * fArr[8]) * fArr[15])) + (((fArr[2] * fArr[4]) * fArr[9]) * fArr[15])) - (((fArr[0] * fArr[6]) * fArr[9]) * fArr[15])) - (((fArr[1] * fArr[4]) * fArr[10]) * fArr[15])) + (fArr[0] * fArr[5] * fArr[10] * fArr[15]);
        if (f2 != Animation.CurveTimeline.LINEAR) {
            float f3 = 1.0f / f2;
            float[] fArr2 = f5232b;
            fArr2[0] = ((((((fArr[9] * fArr[14]) * fArr[7]) - ((fArr[13] * fArr[10]) * fArr[7])) + ((fArr[13] * fArr[6]) * fArr[11])) - ((fArr[5] * fArr[14]) * fArr[11])) - ((fArr[9] * fArr[6]) * fArr[15])) + (fArr[5] * fArr[10] * fArr[15]);
            fArr2[4] = ((((((fArr[12] * fArr[10]) * fArr[7]) - ((fArr[8] * fArr[14]) * fArr[7])) - ((fArr[12] * fArr[6]) * fArr[11])) + ((fArr[4] * fArr[14]) * fArr[11])) + ((fArr[8] * fArr[6]) * fArr[15])) - ((fArr[4] * fArr[10]) * fArr[15]);
            fArr2[8] = ((((((fArr[8] * fArr[13]) * fArr[7]) - ((fArr[12] * fArr[9]) * fArr[7])) + ((fArr[12] * fArr[5]) * fArr[11])) - ((fArr[4] * fArr[13]) * fArr[11])) - ((fArr[8] * fArr[5]) * fArr[15])) + (fArr[4] * fArr[9] * fArr[15]);
            fArr2[12] = ((((((fArr[12] * fArr[9]) * fArr[6]) - ((fArr[8] * fArr[13]) * fArr[6])) - ((fArr[12] * fArr[5]) * fArr[10])) + ((fArr[4] * fArr[13]) * fArr[10])) + ((fArr[8] * fArr[5]) * fArr[14])) - ((fArr[4] * fArr[9]) * fArr[14]);
            fArr2[1] = ((((((fArr[13] * fArr[10]) * fArr[3]) - ((fArr[9] * fArr[14]) * fArr[3])) - ((fArr[13] * fArr[2]) * fArr[11])) + ((fArr[1] * fArr[14]) * fArr[11])) + ((fArr[9] * fArr[2]) * fArr[15])) - ((fArr[1] * fArr[10]) * fArr[15]);
            fArr2[5] = ((((((fArr[8] * fArr[14]) * fArr[3]) - ((fArr[12] * fArr[10]) * fArr[3])) + ((fArr[12] * fArr[2]) * fArr[11])) - ((fArr[0] * fArr[14]) * fArr[11])) - ((fArr[8] * fArr[2]) * fArr[15])) + (fArr[0] * fArr[10] * fArr[15]);
            fArr2[9] = ((((((fArr[12] * fArr[9]) * fArr[3]) - ((fArr[8] * fArr[13]) * fArr[3])) - ((fArr[12] * fArr[1]) * fArr[11])) + ((fArr[0] * fArr[13]) * fArr[11])) + ((fArr[8] * fArr[1]) * fArr[15])) - ((fArr[0] * fArr[9]) * fArr[15]);
            fArr2[13] = ((((((fArr[8] * fArr[13]) * fArr[2]) - ((fArr[12] * fArr[9]) * fArr[2])) + ((fArr[12] * fArr[1]) * fArr[10])) - ((fArr[0] * fArr[13]) * fArr[10])) - ((fArr[8] * fArr[1]) * fArr[14])) + (fArr[0] * fArr[9] * fArr[14]);
            fArr2[2] = ((((((fArr[5] * fArr[14]) * fArr[3]) - ((fArr[13] * fArr[6]) * fArr[3])) + ((fArr[13] * fArr[2]) * fArr[7])) - ((fArr[1] * fArr[14]) * fArr[7])) - ((fArr[5] * fArr[2]) * fArr[15])) + (fArr[1] * fArr[6] * fArr[15]);
            fArr2[6] = ((((((fArr[12] * fArr[6]) * fArr[3]) - ((fArr[4] * fArr[14]) * fArr[3])) - ((fArr[12] * fArr[2]) * fArr[7])) + ((fArr[0] * fArr[14]) * fArr[7])) + ((fArr[4] * fArr[2]) * fArr[15])) - ((fArr[0] * fArr[6]) * fArr[15]);
            fArr2[10] = ((((((fArr[4] * fArr[13]) * fArr[3]) - ((fArr[12] * fArr[5]) * fArr[3])) + ((fArr[12] * fArr[1]) * fArr[7])) - ((fArr[0] * fArr[13]) * fArr[7])) - ((fArr[4] * fArr[1]) * fArr[15])) + (fArr[0] * fArr[5] * fArr[15]);
            fArr2[14] = ((((((fArr[12] * fArr[5]) * fArr[2]) - ((fArr[4] * fArr[13]) * fArr[2])) - ((fArr[12] * fArr[1]) * fArr[6])) + ((fArr[0] * fArr[13]) * fArr[6])) + ((fArr[4] * fArr[1]) * fArr[14])) - ((fArr[0] * fArr[5]) * fArr[14]);
            fArr2[3] = ((((((fArr[9] * fArr[6]) * fArr[3]) - ((fArr[5] * fArr[10]) * fArr[3])) - ((fArr[9] * fArr[2]) * fArr[7])) + ((fArr[1] * fArr[10]) * fArr[7])) + ((fArr[5] * fArr[2]) * fArr[11])) - ((fArr[1] * fArr[6]) * fArr[11]);
            fArr2[7] = ((((((fArr[4] * fArr[10]) * fArr[3]) - ((fArr[8] * fArr[6]) * fArr[3])) + ((fArr[8] * fArr[2]) * fArr[7])) - ((fArr[0] * fArr[10]) * fArr[7])) - ((fArr[4] * fArr[2]) * fArr[11])) + (fArr[0] * fArr[6] * fArr[11]);
            fArr2[11] = ((((((fArr[8] * fArr[5]) * fArr[3]) - ((fArr[4] * fArr[9]) * fArr[3])) - ((fArr[8] * fArr[1]) * fArr[7])) + ((fArr[0] * fArr[9]) * fArr[7])) + ((fArr[4] * fArr[1]) * fArr[11])) - ((fArr[0] * fArr[5]) * fArr[11]);
            fArr2[15] = ((((((fArr[4] * fArr[9]) * fArr[2]) - ((fArr[8] * fArr[5]) * fArr[2])) + ((fArr[8] * fArr[1]) * fArr[6])) - ((fArr[0] * fArr[9]) * fArr[6])) - ((fArr[4] * fArr[1]) * fArr[10])) + (fArr[0] * fArr[5] * fArr[10]);
            fArr[0] = fArr2[0] * f3;
            fArr[4] = fArr2[4] * f3;
            fArr[8] = fArr2[8] * f3;
            fArr[12] = fArr2[12] * f3;
            fArr[1] = fArr2[1] * f3;
            fArr[5] = fArr2[5] * f3;
            fArr[9] = fArr2[9] * f3;
            fArr[13] = fArr2[13] * f3;
            fArr[2] = fArr2[2] * f3;
            fArr[6] = fArr2[6] * f3;
            fArr[10] = fArr2[10] * f3;
            fArr[14] = fArr2[14] * f3;
            fArr[3] = fArr2[3] * f3;
            fArr[7] = fArr2[7] * f3;
            fArr[11] = fArr2[11] * f3;
            fArr[15] = fArr2[15] * f3;
            return this;
        }
        throw new RuntimeException("non-invertible matrix");
    }

    public Matrix4 a(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11) {
        float f12 = f5 * 2.0f;
        float f13 = f6 * 2.0f;
        float f14 = 2.0f * f7;
        float f15 = f8 * f12;
        float f16 = f8 * f13;
        float f17 = f8 * f14;
        float f18 = f12 * f5;
        float f19 = f5 * f13;
        float f20 = f5 * f14;
        float f21 = f13 * f6;
        float f22 = f6 * f14;
        float f23 = f14 * f7;
        float[] fArr = this.f5238a;
        fArr[0] = (1.0f - (f21 + f23)) * f9;
        fArr[4] = (f19 - f17) * f10;
        fArr[8] = (f20 + f16) * f11;
        fArr[12] = f2;
        fArr[1] = f9 * (f19 + f17);
        fArr[5] = (1.0f - (f23 + f18)) * f10;
        fArr[9] = (f22 - f15) * f11;
        fArr[13] = f3;
        fArr[2] = f9 * (f20 - f16);
        fArr[6] = f10 * (f22 + f15);
        fArr[10] = (1.0f - (f18 + f21)) * f11;
        fArr[14] = f4;
        fArr[3] = 0.0f;
        fArr[7] = 0.0f;
        fArr[11] = 0.0f;
        fArr[15] = 1.0f;
        return this;
    }

    public Matrix4(Matrix4 matrix4) {
        b(matrix4);
    }

    public Matrix4 a(Matrix4 matrix4) {
        mul(this.f5238a, matrix4.f5238a);
        return this;
    }

    public Matrix4 a() {
        float[] fArr = this.f5238a;
        fArr[0] = 1.0f;
        fArr[4] = 0.0f;
        fArr[8] = 0.0f;
        fArr[12] = 0.0f;
        fArr[1] = 0.0f;
        fArr[5] = 1.0f;
        fArr[9] = 0.0f;
        fArr[13] = 0.0f;
        fArr[2] = 0.0f;
        fArr[6] = 0.0f;
        fArr[10] = 1.0f;
        fArr[14] = 0.0f;
        fArr[3] = 0.0f;
        fArr[7] = 0.0f;
        fArr[11] = 0.0f;
        fArr[15] = 1.0f;
        return this;
    }

    public Matrix4 a(float f2, float f3, float f4, float f5) {
        a(f2, f2 + f4, f3, f3 + f5, Animation.CurveTimeline.LINEAR, 1.0f);
        return this;
    }

    public Matrix4 b(float f2, float f3, float f4) {
        a();
        float[] fArr = this.f5238a;
        fArr[12] = f2;
        fArr[13] = f3;
        fArr[14] = f4;
        return this;
    }

    public Matrix4 a(float f2, float f3, float f4, float f5, float f6, float f7) {
        a();
        float f8 = f3 - f2;
        float f9 = f5 - f4;
        float f10 = f7 - f6;
        float[] fArr = this.f5238a;
        fArr[0] = 2.0f / f8;
        fArr[1] = 0.0f;
        fArr[2] = 0.0f;
        fArr[3] = 0.0f;
        fArr[4] = 0.0f;
        fArr[5] = 2.0f / f9;
        fArr[6] = 0.0f;
        fArr[7] = 0.0f;
        fArr[8] = 0.0f;
        fArr[9] = 0.0f;
        fArr[10] = -2.0f / f10;
        fArr[11] = 0.0f;
        fArr[12] = (-(f3 + f2)) / f8;
        fArr[13] = (-(f5 + f4)) / f9;
        fArr[14] = (-(f7 + f6)) / f10;
        fArr[15] = 1.0f;
        return this;
    }

    public Matrix4 a(q qVar, q qVar2) {
        q qVar3 = f5233c;
        qVar3.d(qVar);
        qVar3.c();
        q qVar4 = f5234d;
        qVar4.d(qVar);
        qVar4.c();
        q qVar5 = f5234d;
        qVar5.b(qVar2);
        qVar5.c();
        q qVar6 = f5235e;
        qVar6.d(f5234d);
        qVar6.b(f5233c);
        qVar6.c();
        a();
        float[] fArr = this.f5238a;
        q qVar7 = f5234d;
        fArr[0] = qVar7.f5296a;
        fArr[4] = qVar7.f5297b;
        fArr[8] = qVar7.f5298c;
        q qVar8 = f5235e;
        fArr[1] = qVar8.f5296a;
        fArr[5] = qVar8.f5297b;
        fArr[9] = qVar8.f5298c;
        q qVar9 = f5233c;
        fArr[2] = -qVar9.f5296a;
        fArr[6] = -qVar9.f5297b;
        fArr[10] = -qVar9.f5298c;
        return this;
    }

    public Matrix4 a(q qVar, q qVar2, q qVar3) {
        q qVar4 = f5236f;
        qVar4.d(qVar2);
        qVar4.e(qVar);
        a(f5236f, qVar3);
        Matrix4 matrix4 = f5237g;
        matrix4.b(-qVar.f5296a, -qVar.f5297b, -qVar.f5298c);
        a(matrix4);
        return this;
    }

    public Matrix4 a(a aVar) {
        float[] fArr = this.f5238a;
        fArr[0] = aVar.f5239a;
        fArr[1] = aVar.f5242d;
        fArr[2] = 0.0f;
        fArr[3] = 0.0f;
        fArr[4] = aVar.f5240b;
        fArr[5] = aVar.f5243e;
        fArr[6] = 0.0f;
        fArr[7] = 0.0f;
        fArr[8] = 0.0f;
        fArr[9] = 0.0f;
        fArr[10] = 1.0f;
        fArr[11] = 0.0f;
        fArr[12] = aVar.f5241c;
        fArr[13] = aVar.f5244f;
        fArr[14] = 0.0f;
        fArr[15] = 1.0f;
        return this;
    }

    public Matrix4 a(float f2) {
        float[] fArr = this.f5238a;
        fArr[0] = fArr[0] * f2;
        fArr[5] = fArr[5] * f2;
        fArr[10] = fArr[10] * f2;
        return this;
    }

    public Matrix4 a(l lVar) {
        lVar.a(f5232b);
        mul(this.f5238a, f5232b);
        return this;
    }

    public Matrix4 a(float f2, float f3, float f4) {
        float[] fArr = f5232b;
        fArr[0] = f2;
        fArr[4] = 0.0f;
        fArr[8] = 0.0f;
        fArr[12] = 0.0f;
        fArr[1] = 0.0f;
        fArr[5] = f3;
        fArr[9] = 0.0f;
        fArr[13] = 0.0f;
        fArr[2] = 0.0f;
        fArr[6] = 0.0f;
        fArr[10] = f4;
        fArr[14] = 0.0f;
        fArr[3] = 0.0f;
        fArr[7] = 0.0f;
        fArr[11] = 0.0f;
        fArr[15] = 1.0f;
        mul(this.f5238a, fArr);
        return this;
    }
}
