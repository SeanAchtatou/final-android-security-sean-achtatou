package com.badlogic.gdx.math.t;

import com.badlogic.gdx.math.q;
import com.esotericsoftware.spine.Animation;
import java.io.Serializable;

/* compiled from: BoundingBox */
public class a implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public final q f5304a = new q();

    /* renamed from: b  reason: collision with root package name */
    public final q f5305b = new q();

    /* renamed from: c  reason: collision with root package name */
    private final q f5306c = new q();

    /* renamed from: d  reason: collision with root package name */
    private final q f5307d = new q();

    static {
        new q();
    }

    public a() {
        a();
    }

    static final float a(float f2, float f3) {
        return f2 > f3 ? f3 : f2;
    }

    public a a(q qVar, q qVar2) {
        q qVar3 = this.f5304a;
        float f2 = qVar.f5296a;
        float f3 = qVar2.f5296a;
        if (f2 >= f3) {
            f2 = f3;
        }
        float f4 = qVar.f5297b;
        float f5 = qVar2.f5297b;
        if (f4 >= f5) {
            f4 = f5;
        }
        float f6 = qVar.f5298c;
        float f7 = qVar2.f5298c;
        if (f6 >= f7) {
            f6 = f7;
        }
        qVar3.c(f2, f4, f6);
        q qVar4 = this.f5305b;
        float f8 = qVar.f5296a;
        float f9 = qVar2.f5296a;
        if (f8 <= f9) {
            f8 = f9;
        }
        float f10 = qVar.f5297b;
        float f11 = qVar2.f5297b;
        if (f10 <= f11) {
            f10 = f11;
        }
        float f12 = qVar.f5298c;
        float f13 = qVar2.f5298c;
        if (f12 <= f13) {
            f12 = f13;
        }
        qVar4.c(f8, f10, f12);
        q qVar5 = this.f5306c;
        qVar5.d(this.f5304a);
        qVar5.a(this.f5305b);
        qVar5.a(0.5f);
        q qVar6 = this.f5307d;
        qVar6.d(this.f5305b);
        qVar6.e(this.f5304a);
        return this;
    }

    public q b(q qVar) {
        qVar.d(this.f5306c);
        return qVar;
    }

    public q c(q qVar) {
        qVar.d(this.f5307d);
        return qVar;
    }

    public String toString() {
        return "[" + this.f5304a + "|" + this.f5305b + "]";
    }

    public a b() {
        this.f5304a.c(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY);
        this.f5305b.c(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);
        this.f5306c.c(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        this.f5307d.c(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        return this;
    }

    public a a(q qVar) {
        q qVar2 = this.f5304a;
        qVar2.c(a(qVar2.f5296a, qVar.f5296a), a(this.f5304a.f5297b, qVar.f5297b), a(this.f5304a.f5298c, qVar.f5298c));
        q qVar3 = this.f5305b;
        qVar3.c(Math.max(qVar3.f5296a, qVar.f5296a), Math.max(this.f5305b.f5297b, qVar.f5297b), Math.max(this.f5305b.f5298c, qVar.f5298c));
        a(qVar2, qVar3);
        return this;
    }

    public a a() {
        q qVar = this.f5304a;
        qVar.c(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        q qVar2 = this.f5305b;
        qVar2.c(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        a(qVar, qVar2);
        return this;
    }
}
