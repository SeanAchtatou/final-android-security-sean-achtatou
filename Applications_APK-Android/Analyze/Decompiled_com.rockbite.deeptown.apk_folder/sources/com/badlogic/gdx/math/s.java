package com.badlogic.gdx.math;

import com.esotericsoftware.spine.Animation;

/* compiled from: WindowedMean */
public final class s {

    /* renamed from: a  reason: collision with root package name */
    float[] f5299a;

    /* renamed from: b  reason: collision with root package name */
    int f5300b = 0;

    /* renamed from: c  reason: collision with root package name */
    int f5301c;

    /* renamed from: d  reason: collision with root package name */
    float f5302d = Animation.CurveTimeline.LINEAR;

    /* renamed from: e  reason: collision with root package name */
    boolean f5303e = true;

    public s(int i2) {
        this.f5299a = new float[i2];
    }

    public void a() {
        int i2 = 0;
        this.f5300b = 0;
        this.f5301c = 0;
        while (true) {
            float[] fArr = this.f5299a;
            if (i2 < fArr.length) {
                fArr[i2] = 0.0f;
                i2++;
            } else {
                this.f5303e = true;
                return;
            }
        }
    }

    public float b() {
        float[] fArr;
        if (!c()) {
            return Animation.CurveTimeline.LINEAR;
        }
        if (this.f5303e) {
            int i2 = 0;
            float f2 = Animation.CurveTimeline.LINEAR;
            while (true) {
                fArr = this.f5299a;
                if (i2 >= fArr.length) {
                    break;
                }
                f2 += fArr[i2];
                i2++;
            }
            this.f5302d = f2 / ((float) fArr.length);
            this.f5303e = false;
        }
        return this.f5302d;
    }

    public boolean c() {
        return this.f5300b >= this.f5299a.length;
    }

    public void a(float f2) {
        int i2 = this.f5300b;
        if (i2 < this.f5299a.length) {
            this.f5300b = i2 + 1;
        }
        float[] fArr = this.f5299a;
        int i3 = this.f5301c;
        this.f5301c = i3 + 1;
        fArr[i3] = f2;
        if (this.f5301c > fArr.length - 1) {
            this.f5301c = 0;
        }
        this.f5303e = true;
    }
}
