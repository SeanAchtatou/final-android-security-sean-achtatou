package com.badlogic.gdx.math;

import com.esotericsoftware.spine.Animation;

/* compiled from: Polygon */
public class k implements o {

    /* renamed from: a  reason: collision with root package name */
    private float[] f5273a;

    /* renamed from: b  reason: collision with root package name */
    private float[] f5274b;

    /* renamed from: c  reason: collision with root package name */
    private float f5275c;

    /* renamed from: d  reason: collision with root package name */
    private float f5276d;

    /* renamed from: e  reason: collision with root package name */
    private float f5277e;

    /* renamed from: f  reason: collision with root package name */
    private float f5278f;

    /* renamed from: g  reason: collision with root package name */
    private float f5279g;

    /* renamed from: h  reason: collision with root package name */
    private float f5280h = 1.0f;

    /* renamed from: i  reason: collision with root package name */
    private float f5281i = 1.0f;

    /* renamed from: j  reason: collision with root package name */
    private boolean f5282j = true;

    public k(float[] fArr) {
        if (fArr.length >= 6) {
            this.f5273a = fArr;
            return;
        }
        throw new IllegalArgumentException("polygons must contain at least 3 points.");
    }

    public float[] a() {
        if (!this.f5282j) {
            return this.f5274b;
        }
        this.f5282j = false;
        float[] fArr = this.f5273a;
        float[] fArr2 = this.f5274b;
        if (fArr2 == null || fArr2.length != fArr.length) {
            this.f5274b = new float[fArr.length];
        }
        float[] fArr3 = this.f5274b;
        float f2 = this.f5275c;
        float f3 = this.f5276d;
        float f4 = this.f5277e;
        float f5 = this.f5278f;
        float f6 = this.f5280h;
        float f7 = this.f5281i;
        boolean z = (f6 == 1.0f && f7 == 1.0f) ? false : true;
        float f8 = this.f5279g;
        float b2 = h.b(f8);
        float h2 = h.h(f8);
        int length = fArr.length;
        for (int i2 = 0; i2 < length; i2 += 2) {
            float f9 = fArr[i2] - f4;
            int i3 = i2 + 1;
            float f10 = fArr[i3] - f5;
            if (z) {
                f9 *= f6;
                f10 *= f7;
            }
            if (f8 != Animation.CurveTimeline.LINEAR) {
                f10 = (f9 * h2) + (f10 * b2);
                f9 = (b2 * f9) - (h2 * f10);
            }
            fArr3[i2] = f9 + f2 + f4;
            fArr3[i3] = f3 + f10 + f5;
        }
        return fArr3;
    }

    public boolean a(float f2, float f3) {
        float[] a2 = a();
        int length = a2.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            float f4 = a2[i2];
            float f5 = a2[i2 + 1];
            int i4 = i2 + 2;
            float f6 = a2[i4 % length];
            float f7 = a2[(i2 + 3) % length];
            if (((f5 <= f3 && f3 < f7) || (f7 <= f3 && f3 < f5)) && f2 < (((f6 - f4) / (f7 - f5)) * (f3 - f5)) + f4) {
                i3++;
            }
            i2 = i4;
        }
        return (i3 & 1) == 1;
    }
}
