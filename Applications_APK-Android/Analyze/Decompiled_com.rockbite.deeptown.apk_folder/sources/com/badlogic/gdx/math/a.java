package com.badlogic.gdx.math;

import com.esotericsoftware.spine.Animation;
import java.io.Serializable;

/* compiled from: Affine2 */
public final class a implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public float f5239a = 1.0f;

    /* renamed from: b  reason: collision with root package name */
    public float f5240b = Animation.CurveTimeline.LINEAR;

    /* renamed from: c  reason: collision with root package name */
    public float f5241c = Animation.CurveTimeline.LINEAR;

    /* renamed from: d  reason: collision with root package name */
    public float f5242d = Animation.CurveTimeline.LINEAR;

    /* renamed from: e  reason: collision with root package name */
    public float f5243e = 1.0f;

    /* renamed from: f  reason: collision with root package name */
    public float f5244f = Animation.CurveTimeline.LINEAR;

    public a a(float f2, float f3, float f4, float f5, float f6) {
        this.f5241c = f2;
        this.f5244f = f3;
        if (f4 == Animation.CurveTimeline.LINEAR) {
            this.f5239a = f5;
            this.f5240b = Animation.CurveTimeline.LINEAR;
            this.f5242d = Animation.CurveTimeline.LINEAR;
            this.f5243e = f6;
        } else {
            float h2 = h.h(f4);
            float b2 = h.b(f4);
            this.f5239a = b2 * f5;
            this.f5240b = (-h2) * f6;
            this.f5242d = h2 * f5;
            this.f5243e = b2 * f6;
        }
        return this;
    }

    public String toString() {
        return "[" + this.f5239a + "|" + this.f5240b + "|" + this.f5241c + "]\n[" + this.f5242d + "|" + this.f5243e + "|" + this.f5244f + "]\n[0.0|0.0|0.1]";
    }

    public a a(a aVar) {
        float f2 = aVar.f5239a;
        float f3 = this.f5239a;
        float f4 = aVar.f5240b;
        float f5 = this.f5242d;
        float f6 = (f2 * f3) + (f4 * f5);
        float f7 = this.f5240b;
        float f8 = this.f5243e;
        float f9 = (f2 * f7) + (f4 * f8);
        float f10 = this.f5241c;
        float f11 = this.f5244f;
        float f12 = (f2 * f10) + (f4 * f11) + aVar.f5241c;
        float f13 = aVar.f5242d;
        float f14 = aVar.f5243e;
        this.f5239a = f6;
        this.f5240b = f9;
        this.f5241c = f12;
        this.f5242d = (f3 * f13) + (f5 * f14);
        this.f5243e = (f7 * f13) + (f8 * f14);
        this.f5244f = (f13 * f10) + (f14 * f11) + aVar.f5244f;
        return this;
    }

    public a a(float f2, float f3) {
        this.f5241c += (this.f5239a * f2) + (this.f5240b * f3);
        this.f5244f += (this.f5242d * f2) + (this.f5243e * f3);
        return this;
    }
}
