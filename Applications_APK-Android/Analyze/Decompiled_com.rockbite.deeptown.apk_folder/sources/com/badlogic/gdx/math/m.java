package com.badlogic.gdx.math;

import java.util.Random;

/* compiled from: RandomXS128 */
public class m extends Random {

    /* renamed from: a  reason: collision with root package name */
    private long f5287a;

    /* renamed from: b  reason: collision with root package name */
    private long f5288b;

    public m() {
        setSeed(new Random().nextLong());
    }

    private static final long b(long j2) {
        long j3 = (j2 ^ (j2 >>> 33)) * -49064778989728563L;
        long j4 = (j3 ^ (j3 >>> 33)) * -4265267296055464877L;
        return j4 ^ (j4 >>> 33);
    }

    public long a(long j2) {
        long nextLong;
        long j3;
        if (j2 > 0) {
            do {
                nextLong = nextLong() >>> 1;
                j3 = nextLong % j2;
            } while ((nextLong - j3) + (j2 - 1) < 0);
            return j3;
        }
        throw new IllegalArgumentException("n must be positive");
    }

    /* access modifiers changed from: protected */
    public final int next(int i2) {
        return (int) (nextLong() & ((1 << i2) - 1));
    }

    public boolean nextBoolean() {
        return (nextLong() & 1) != 0;
    }

    public void nextBytes(byte[] bArr) {
        int length = bArr.length;
        while (length != 0) {
            int i2 = length < 8 ? length : 8;
            long nextLong = nextLong();
            while (true) {
                int i3 = i2 - 1;
                if (i2 != 0) {
                    length--;
                    bArr[length] = (byte) ((int) nextLong);
                    nextLong >>= 8;
                    i2 = i3;
                }
            }
        }
    }

    public double nextDouble() {
        double nextLong = (double) (nextLong() >>> 11);
        Double.isNaN(nextLong);
        return nextLong * 1.1102230246251565E-16d;
    }

    public float nextFloat() {
        double nextLong = (double) (nextLong() >>> 40);
        Double.isNaN(nextLong);
        return (float) (nextLong * 5.9604644775390625E-8d);
    }

    public int nextInt() {
        return (int) nextLong();
    }

    public long nextLong() {
        long j2 = this.f5287a;
        long j3 = this.f5288b;
        this.f5287a = j3;
        long j4 = j2 ^ (j2 << 23);
        long j5 = ((j4 >>> 17) ^ (j4 ^ j3)) ^ (j3 >>> 26);
        this.f5288b = j5;
        return j5 + j3;
    }

    public void setSeed(long j2) {
        if (j2 == 0) {
            j2 = Long.MIN_VALUE;
        }
        long b2 = b(j2);
        a(b2, b(b2));
    }

    public int nextInt(int i2) {
        return (int) a((long) i2);
    }

    public void a(long j2, long j3) {
        this.f5287a = j2;
        this.f5288b = j3;
    }
}
