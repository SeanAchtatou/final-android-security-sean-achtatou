package com.badlogic.gdx.math;

import com.esotericsoftware.spine.Animation;

/* compiled from: GeometryUtils */
public final class e {
    static {
        new p();
        new p();
        new p();
    }

    public static boolean a(float[] fArr, int i2, int i3) {
        if (i3 <= 2) {
            return false;
        }
        int i4 = (i3 + i2) - 2;
        float f2 = fArr[i4];
        float f3 = fArr[i4 + 1];
        float f4 = f2;
        float f5 = Animation.CurveTimeline.LINEAR;
        while (i2 <= i4) {
            float f6 = fArr[i2];
            float f7 = fArr[i2 + 1];
            f5 += (f4 * f7) - (f3 * f6);
            i2 += 2;
            f4 = f6;
            f3 = f7;
        }
        if (f5 < Animation.CurveTimeline.LINEAR) {
            return true;
        }
        return false;
    }
}
