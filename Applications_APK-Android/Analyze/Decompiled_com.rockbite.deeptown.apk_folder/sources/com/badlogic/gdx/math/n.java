package com.badlogic.gdx.math;

import com.badlogic.gdx.utils.z;
import java.io.Serializable;

/* compiled from: Rectangle */
public class n implements Serializable, o {

    /* renamed from: e  reason: collision with root package name */
    public static final n f5289e = new n();

    /* renamed from: a  reason: collision with root package name */
    public float f5290a;

    /* renamed from: b  reason: collision with root package name */
    public float f5291b;

    /* renamed from: c  reason: collision with root package name */
    public float f5292c;

    /* renamed from: d  reason: collision with root package name */
    public float f5293d;

    static {
        new n();
    }

    public n() {
    }

    public n a(float f2, float f3, float f4, float f5) {
        this.f5290a = f2;
        this.f5291b = f3;
        this.f5292c = f4;
        this.f5293d = f5;
        return this;
    }

    public float b() {
        return this.f5292c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        n nVar = (n) obj;
        return z.c(this.f5293d) == z.c(nVar.f5293d) && z.c(this.f5292c) == z.c(nVar.f5292c) && z.c(this.f5290a) == z.c(nVar.f5290a) && z.c(this.f5291b) == z.c(nVar.f5291b);
    }

    public int hashCode() {
        return ((((((z.c(this.f5293d) + 31) * 31) + z.c(this.f5292c)) * 31) + z.c(this.f5290a)) * 31) + z.c(this.f5291b);
    }

    public String toString() {
        return "[" + this.f5290a + "," + this.f5291b + "," + this.f5292c + "," + this.f5293d + "]";
    }

    public n(float f2, float f3, float f4, float f5) {
        this.f5290a = f2;
        this.f5291b = f3;
        this.f5292c = f4;
        this.f5293d = f5;
    }

    public float a() {
        return this.f5293d;
    }

    public boolean a(float f2, float f3) {
        float f4 = this.f5290a;
        if (f4 <= f2 && f4 + this.f5292c >= f2) {
            float f5 = this.f5291b;
            return f5 <= f3 && f5 + this.f5293d >= f3;
        }
    }

    public boolean a(n nVar) {
        float f2 = this.f5290a;
        float f3 = nVar.f5290a;
        if (f2 < nVar.f5292c + f3 && f2 + this.f5292c > f3) {
            float f4 = this.f5291b;
            float f5 = nVar.f5291b;
            return f4 < nVar.f5293d + f5 && f4 + this.f5293d > f5;
        }
    }
}
