package com.badlogic.gdx.math;

import com.badlogic.gdx.utils.z;
import com.esotericsoftware.spine.Animation;
import java.io.Serializable;

/* compiled from: Quaternion */
public class l implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public float f5283a;

    /* renamed from: b  reason: collision with root package name */
    public float f5284b;

    /* renamed from: c  reason: collision with root package name */
    public float f5285c;

    /* renamed from: d  reason: collision with root package name */
    public float f5286d;

    static {
        new l(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        new l(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    }

    public l(float f2, float f3, float f4, float f5) {
        a(f2, f3, f4, f5);
    }

    public l a(float f2, float f3, float f4, float f5) {
        this.f5283a = f2;
        this.f5284b = f3;
        this.f5285c = f4;
        this.f5286d = f5;
        return this;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof l)) {
            return false;
        }
        l lVar = (l) obj;
        if (z.c(this.f5286d) == z.c(lVar.f5286d) && z.c(this.f5283a) == z.c(lVar.f5283a) && z.c(this.f5284b) == z.c(lVar.f5284b) && z.c(this.f5285c) == z.c(lVar.f5285c)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((((((z.c(this.f5286d) + 31) * 31) + z.c(this.f5283a)) * 31) + z.c(this.f5284b)) * 31) + z.c(this.f5285c);
    }

    public String toString() {
        return "[" + this.f5283a + "|" + this.f5284b + "|" + this.f5285c + "|" + this.f5286d + "]";
    }

    public l() {
        a();
    }

    public l(l lVar) {
        a(lVar);
    }

    public l a(l lVar) {
        a(lVar.f5283a, lVar.f5284b, lVar.f5285c, lVar.f5286d);
        return this;
    }

    public void a(float[] fArr) {
        float f2 = this.f5283a;
        float f3 = f2 * f2;
        float f4 = this.f5284b;
        float f5 = f2 * f4;
        float f6 = this.f5285c;
        float f7 = f2 * f6;
        float f8 = this.f5286d;
        float f9 = f2 * f8;
        float f10 = f4 * f4;
        float f11 = f4 * f6;
        float f12 = f4 * f8;
        float f13 = f6 * f6;
        float f14 = f6 * f8;
        fArr[0] = 1.0f - ((f10 + f13) * 2.0f);
        fArr[4] = (f5 - f14) * 2.0f;
        fArr[8] = (f7 + f12) * 2.0f;
        fArr[12] = 0.0f;
        fArr[1] = (f5 + f14) * 2.0f;
        fArr[5] = 1.0f - ((f13 + f3) * 2.0f);
        fArr[9] = (f11 - f9) * 2.0f;
        fArr[13] = 0.0f;
        fArr[2] = (f7 - f12) * 2.0f;
        fArr[6] = (f11 + f9) * 2.0f;
        fArr[10] = 1.0f - ((f3 + f10) * 2.0f);
        fArr[14] = 0.0f;
        fArr[3] = 0.0f;
        fArr[7] = 0.0f;
        fArr[11] = 0.0f;
        fArr[15] = 1.0f;
    }

    public l a() {
        a(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f);
        return this;
    }
}
