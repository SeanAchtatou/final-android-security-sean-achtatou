package com.badlogic.gdx.math;

import com.esotericsoftware.spine.Animation;

/* compiled from: Interpolation */
public abstract class f {

    /* renamed from: a  reason: collision with root package name */
    public static final f f5256a = new C0121f();

    /* renamed from: b  reason: collision with root package name */
    public static final f f5257b = new i();

    /* renamed from: c  reason: collision with root package name */
    public static final f f5258c = f5257b;

    /* renamed from: d  reason: collision with root package name */
    public static final x f5259d = new x(2);

    /* renamed from: e  reason: collision with root package name */
    public static final y f5260e = new y(2);

    /* renamed from: f  reason: collision with root package name */
    public static final z f5261f = new z(2);

    /* renamed from: g  reason: collision with root package name */
    public static final x f5262g = new x(3);

    /* renamed from: h  reason: collision with root package name */
    public static final y f5263h = new y(5);

    /* renamed from: i  reason: collision with root package name */
    public static final z f5264i = new z(5);

    /* renamed from: j  reason: collision with root package name */
    public static final f f5265j = new c();

    /* renamed from: k  reason: collision with root package name */
    public static final a0 f5266k = new a0(1.5f);
    public static final c0 l = new c0(2.0f);
    public static final q m = new q(4);

    /* compiled from: Interpolation */
    static class a extends f {
        a() {
        }

        public float a(float f2) {
            return 1.0f - h.a((f2 * 3.1415927f) / 2.0f);
        }
    }

    /* compiled from: Interpolation */
    public static class a0 extends f {
        private final float n;

        public a0(float f2) {
            this.n = f2 * 2.0f;
        }

        public float a(float f2) {
            if (f2 <= 0.5f) {
                float f3 = f2 * 2.0f;
                float f4 = this.n;
                return ((f3 * f3) * (((1.0f + f4) * f3) - f4)) / 2.0f;
            }
            float f5 = (f2 - 1.0f) * 2.0f;
            float f6 = this.n;
            return (((f5 * f5) * (((f6 + 1.0f) * f5) + f6)) / 2.0f) + 1.0f;
        }
    }

    /* compiled from: Interpolation */
    static class b extends f {
        b() {
        }

        public float a(float f2) {
            return h.g((f2 * 3.1415927f) / 2.0f);
        }
    }

    /* compiled from: Interpolation */
    public static class b0 extends f {
        private final float n;

        public b0(float f2) {
            this.n = f2;
        }

        public float a(float f2) {
            float f3 = this.n;
            return f2 * f2 * (((1.0f + f3) * f2) - f3);
        }
    }

    /* compiled from: Interpolation */
    static class c extends f {
        c() {
        }

        public float a(float f2) {
            if (f2 <= 0.5f) {
                float f3 = f2 * 2.0f;
                return (1.0f - ((float) Math.sqrt((double) (1.0f - (f3 * f3))))) / 2.0f;
            }
            float f4 = (f2 - 1.0f) * 2.0f;
            return (((float) Math.sqrt((double) (1.0f - (f4 * f4)))) + 1.0f) / 2.0f;
        }
    }

    /* compiled from: Interpolation */
    public static class c0 extends f {
        private final float n;

        public c0(float f2) {
            this.n = f2;
        }

        public float a(float f2) {
            float f3 = f2 - 1.0f;
            float f4 = this.n;
            return (f3 * f3 * (((f4 + 1.0f) * f3) + f4)) + 1.0f;
        }
    }

    /* compiled from: Interpolation */
    static class d extends f {
        d() {
        }

        public float a(float f2) {
            return 1.0f - ((float) Math.sqrt((double) (1.0f - (f2 * f2))));
        }
    }

    /* compiled from: Interpolation */
    static class e extends f {
        e() {
        }

        public float a(float f2) {
            float f3 = f2 - 1.0f;
            return (float) Math.sqrt((double) (1.0f - (f3 * f3)));
        }
    }

    /* renamed from: com.badlogic.gdx.math.f$f  reason: collision with other inner class name */
    /* compiled from: Interpolation */
    static class C0121f extends f {
        C0121f() {
        }

        public float a(float f2) {
            return f2;
        }
    }

    /* compiled from: Interpolation */
    static class g extends f {
        g() {
        }

        public float a(float f2) {
            return f2 * f2 * (3.0f - (f2 * 2.0f));
        }
    }

    /* compiled from: Interpolation */
    static class h extends f {
        h() {
        }

        public float a(float f2) {
            float f3 = f2 * f2 * (3.0f - (f2 * 2.0f));
            return f3 * f3 * (3.0f - (f3 * 2.0f));
        }
    }

    /* compiled from: Interpolation */
    static class i extends f {
        i() {
        }

        public float a(float f2) {
            return f2 * f2 * f2 * ((f2 * ((6.0f * f2) - 15.0f)) + 10.0f);
        }
    }

    /* compiled from: Interpolation */
    static class j extends f {
        j() {
        }

        public float a(float f2) {
            return (float) Math.sqrt((double) f2);
        }
    }

    /* compiled from: Interpolation */
    static class k extends f {
        k() {
        }

        public float a(float f2) {
            return 1.0f - ((float) Math.sqrt((double) (-(f2 - 1.0f))));
        }
    }

    /* compiled from: Interpolation */
    static class l extends f {
        l() {
        }

        public float a(float f2) {
            return (float) Math.cbrt((double) f2);
        }
    }

    /* compiled from: Interpolation */
    static class m extends f {
        m() {
        }

        public float a(float f2) {
            return 1.0f - ((float) Math.cbrt((double) (-(f2 - 1.0f))));
        }
    }

    /* compiled from: Interpolation */
    static class n extends f {
        n() {
        }

        public float a(float f2) {
            return (1.0f - h.a(f2 * 3.1415927f)) / 2.0f;
        }
    }

    /* compiled from: Interpolation */
    public static class o extends q {
        public o(int i2) {
            super(i2);
        }

        private float b(float f2) {
            float[] fArr = this.n;
            float f3 = (fArr[0] / 2.0f) + f2;
            if (f3 < fArr[0]) {
                return (f3 / (fArr[0] / 2.0f)) - 1.0f;
            }
            return super.a(f2);
        }

        public float a(float f2) {
            if (f2 <= 0.5f) {
                return (1.0f - b(1.0f - (f2 * 2.0f))) / 2.0f;
            }
            return (b((f2 * 2.0f) - 1.0f) / 2.0f) + 0.5f;
        }
    }

    /* compiled from: Interpolation */
    public static class p extends q {
        public p(int i2) {
            super(i2);
        }

        public float a(float f2) {
            return 1.0f - super.a(1.0f - f2);
        }
    }

    /* compiled from: Interpolation */
    public static class q extends f {
        final float[] n;
        final float[] o;

        public q(int i2) {
            if (i2 < 2 || i2 > 5) {
                throw new IllegalArgumentException("bounces cannot be < 2 or > 5: " + i2);
            }
            this.n = new float[i2];
            this.o = new float[i2];
            float[] fArr = this.o;
            fArr[0] = 1.0f;
            if (i2 == 2) {
                float[] fArr2 = this.n;
                fArr2[0] = 0.6f;
                fArr2[1] = 0.4f;
                fArr[1] = 0.33f;
            } else if (i2 == 3) {
                float[] fArr3 = this.n;
                fArr3[0] = 0.4f;
                fArr3[1] = 0.4f;
                fArr3[2] = 0.2f;
                fArr[1] = 0.33f;
                fArr[2] = 0.1f;
            } else if (i2 == 4) {
                float[] fArr4 = this.n;
                fArr4[0] = 0.34f;
                fArr4[1] = 0.34f;
                fArr4[2] = 0.2f;
                fArr4[3] = 0.15f;
                fArr[1] = 0.26f;
                fArr[2] = 0.11f;
                fArr[3] = 0.03f;
            } else if (i2 == 5) {
                float[] fArr5 = this.n;
                fArr5[0] = 0.3f;
                fArr5[1] = 0.3f;
                fArr5[2] = 0.2f;
                fArr5[3] = 0.1f;
                fArr5[4] = 0.1f;
                fArr[1] = 0.45f;
                fArr[2] = 0.3f;
                fArr[3] = 0.15f;
                fArr[4] = 0.06f;
            }
            float[] fArr6 = this.n;
            fArr6[0] = fArr6[0] * 2.0f;
        }

        public float a(float f2) {
            if (f2 == 1.0f) {
                return 1.0f;
            }
            float[] fArr = this.n;
            int i2 = 0;
            float f3 = f2 + (fArr[0] / 2.0f);
            int length = fArr.length;
            float f4 = Animation.CurveTimeline.LINEAR;
            float f5 = Animation.CurveTimeline.LINEAR;
            while (true) {
                if (i2 >= length) {
                    break;
                }
                f5 = this.n[i2];
                if (f3 <= f5) {
                    f4 = this.o[i2];
                    break;
                }
                f3 -= f5;
                i2++;
            }
            float f6 = f3 / f5;
            float f7 = (4.0f / f5) * f4 * f6;
            return 1.0f - ((f7 - (f6 * f7)) * f5);
        }
    }

    /* compiled from: Interpolation */
    public static class r extends f {
        final float n;
        final float o;
        final float p;
        final float q;

        public r(float f2, float f3, int i2, float f4) {
            this.n = f2;
            this.o = f3;
            this.p = f4;
            this.q = ((float) i2) * 3.1415927f * ((float) (i2 % 2 == 0 ? 1 : -1));
        }

        public float a(float f2) {
            if (f2 <= 0.5f) {
                float f3 = f2 * 2.0f;
                return ((((float) Math.pow((double) this.n, (double) (this.o * (f3 - 1.0f)))) * h.g(f3 * this.q)) * this.p) / 2.0f;
            }
            float f4 = (1.0f - f2) * 2.0f;
            return 1.0f - (((((float) Math.pow((double) this.n, (double) (this.o * (f4 - 1.0f)))) * h.g(f4 * this.q)) * this.p) / 2.0f);
        }
    }

    /* compiled from: Interpolation */
    public static class s extends r {
        public s(float f2, float f3, int i2, float f4) {
            super(f2, f3, i2, f4);
        }

        public float a(float f2) {
            if (((double) f2) >= 0.99d) {
                return 1.0f;
            }
            return ((float) Math.pow((double) this.n, (double) (this.o * (f2 - 1.0f)))) * h.g(f2 * this.q) * this.p;
        }
    }

    /* compiled from: Interpolation */
    public static class t extends r {
        public t(float f2, float f3, int i2, float f4) {
            super(f2, f3, i2, f4);
        }

        public float a(float f2) {
            if (f2 == Animation.CurveTimeline.LINEAR) {
                return Animation.CurveTimeline.LINEAR;
            }
            float f3 = 1.0f - f2;
            return 1.0f - ((((float) Math.pow((double) this.n, (double) (this.o * (f3 - 1.0f)))) * h.g(f3 * this.q)) * this.p);
        }
    }

    /* compiled from: Interpolation */
    public static class u extends f {
        final float n;
        final float o;
        final float p;
        final float q = (1.0f / (1.0f - this.p));

        public u(float f2, float f3) {
            this.n = f2;
            this.o = f3;
            this.p = (float) Math.pow((double) f2, (double) (-f3));
        }

        public float a(float f2) {
            float pow;
            if (f2 <= 0.5f) {
                pow = (((float) Math.pow((double) this.n, (double) (this.o * ((f2 * 2.0f) - 1.0f)))) - this.p) * this.q;
            } else {
                pow = 2.0f - ((((float) Math.pow((double) this.n, (double) ((-this.o) * ((f2 * 2.0f) - 1.0f)))) - this.p) * this.q);
            }
            return pow / 2.0f;
        }
    }

    /* compiled from: Interpolation */
    public static class v extends u {
        public v(float f2, float f3) {
            super(f2, f3);
        }

        public float a(float f2) {
            return (((float) Math.pow((double) this.n, (double) (this.o * (f2 - 1.0f)))) - this.p) * this.q;
        }
    }

    /* compiled from: Interpolation */
    public static class w extends u {
        public w(float f2, float f3) {
            super(f2, f3);
        }

        public float a(float f2) {
            return 1.0f - ((((float) Math.pow((double) this.n, (double) ((-this.o) * f2))) - this.p) * this.q);
        }
    }

    /* compiled from: Interpolation */
    public static class x extends f {
        final int n;

        public x(int i2) {
            this.n = i2;
        }

        public float a(float f2) {
            if (f2 <= 0.5f) {
                return ((float) Math.pow((double) (f2 * 2.0f), (double) this.n)) / 2.0f;
            }
            float pow = (float) Math.pow((double) ((f2 - 1.0f) * 2.0f), (double) this.n);
            int i2 = 2;
            if (this.n % 2 == 0) {
                i2 = -2;
            }
            return (pow / ((float) i2)) + 1.0f;
        }
    }

    /* compiled from: Interpolation */
    public static class y extends x {
        public y(int i2) {
            super(i2);
        }

        public float a(float f2) {
            return (float) Math.pow((double) f2, (double) this.n);
        }
    }

    /* compiled from: Interpolation */
    public static class z extends x {
        public z(int i2) {
            super(i2);
        }

        public float a(float f2) {
            return (((float) Math.pow((double) (f2 - 1.0f), (double) this.n)) * ((float) (this.n % 2 == 0 ? -1 : 1))) + 1.0f;
        }
    }

    static {
        new g();
        new h();
        new j();
        new k();
        new y(3);
        new z(3);
        new l();
        new m();
        new x(4);
        new y(4);
        new z(4);
        new x(5);
        new n();
        new a();
        new b();
        new u(2.0f, 10.0f);
        new v(2.0f, 10.0f);
        new w(2.0f, 10.0f);
        new u(2.0f, 5.0f);
        new v(2.0f, 5.0f);
        new w(2.0f, 5.0f);
        new d();
        new e();
        new r(2.0f, 10.0f, 7, 1.0f);
        new s(2.0f, 10.0f, 6, 1.0f);
        new t(2.0f, 10.0f, 7, 1.0f);
        new b0(2.0f);
        new o(4);
        new p(4);
    }

    public abstract float a(float f2);

    public float a(float f2, float f3, float f4) {
        return f2 + ((f3 - f2) * a(f4));
    }
}
