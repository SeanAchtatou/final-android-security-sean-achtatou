package com.badlogic.gdx.math;

import com.badlogic.gdx.utils.z;
import com.esotericsoftware.spine.Animation;
import java.io.Serializable;

/* compiled from: Vector2 */
public class p implements Serializable, r<p> {

    /* renamed from: a  reason: collision with root package name */
    public float f5294a;

    /* renamed from: b  reason: collision with root package name */
    public float f5295b;

    static {
        new p(1.0f, Animation.CurveTimeline.LINEAR);
        new p(Animation.CurveTimeline.LINEAR, 1.0f);
        new p(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    }

    public p() {
    }

    public p a(p pVar) {
        this.f5294a += pVar.f5294a;
        this.f5295b += pVar.f5295b;
        return this;
    }

    public p b() {
        return new p(this);
    }

    public float c() {
        float f2 = this.f5294a;
        float f3 = this.f5295b;
        return (float) Math.sqrt((double) ((f2 * f2) + (f3 * f3)));
    }

    public float d() {
        float f2 = this.f5294a;
        float f3 = this.f5295b;
        return (f2 * f2) + (f3 * f3);
    }

    public p e(float f2, float f3) {
        this.f5294a -= f2;
        this.f5295b -= f3;
        return this;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || p.class != obj.getClass()) {
            return false;
        }
        p pVar = (p) obj;
        return z.a(this.f5294a) == z.a(pVar.f5294a) && z.a(this.f5295b) == z.a(pVar.f5295b);
    }

    public p f(float f2) {
        g(f2 * f2);
        return this;
    }

    public p g(float f2) {
        float d2 = d();
        if (!(d2 == Animation.CurveTimeline.LINEAR || d2 == f2)) {
            c((float) Math.sqrt((double) (f2 / d2)));
        }
        return this;
    }

    public int hashCode() {
        return ((z.a(this.f5294a) + 31) * 31) + z.a(this.f5295b);
    }

    public String toString() {
        return "(" + this.f5294a + "," + this.f5295b + ")";
    }

    public p(float f2, float f3) {
        this.f5294a = f2;
        this.f5295b = f3;
    }

    public float b(p pVar) {
        float f2 = pVar.f5294a - this.f5294a;
        float f3 = pVar.f5295b - this.f5295b;
        return (float) Math.sqrt((double) ((f2 * f2) + (f3 * f3)));
    }

    public p c(p pVar) {
        this.f5294a = pVar.f5294a;
        this.f5295b = pVar.f5295b;
        return this;
    }

    public p d(float f2, float f3) {
        this.f5294a = f2;
        this.f5295b = f3;
        return this;
    }

    public p a(float f2, float f3) {
        this.f5294a += f2;
        this.f5295b += f3;
        return this;
    }

    public p e() {
        float c2 = c();
        if (c2 != Animation.CurveTimeline.LINEAR) {
            this.f5294a /= c2;
            this.f5295b /= c2;
        }
        return this;
    }

    public p c(float f2) {
        this.f5294a *= f2;
        this.f5295b *= f2;
        return this;
    }

    public p d(p pVar) {
        this.f5294a -= pVar.f5294a;
        this.f5295b -= pVar.f5295b;
        return this;
    }

    public p(p pVar) {
        c(pVar);
    }

    public p a(i iVar) {
        float f2 = this.f5294a;
        float[] fArr = iVar.f5269a;
        float f3 = this.f5295b;
        this.f5294a = (fArr[0] * f2) + (fArr[3] * f3) + fArr[6];
        this.f5295b = (f2 * fArr[1]) + (f3 * fArr[4]) + fArr[7];
        return this;
    }

    public float b(float f2, float f3) {
        float f4 = f2 - this.f5294a;
        float f5 = f3 - this.f5295b;
        return (float) Math.sqrt((double) ((f4 * f4) + (f5 * f5)));
    }

    public p c(float f2, float f3) {
        this.f5294a *= f2;
        this.f5295b *= f3;
        return this;
    }

    public p d(float f2) {
        e(f2 * 0.017453292f);
        return this;
    }

    public p e(float f2) {
        d(c(), Animation.CurveTimeline.LINEAR);
        b(f2);
        return this;
    }

    public p b(float f2) {
        double d2 = (double) f2;
        float cos = (float) Math.cos(d2);
        float sin = (float) Math.sin(d2);
        float f3 = this.f5294a;
        float f4 = this.f5295b;
        this.f5294a = (f3 * cos) - (f4 * sin);
        this.f5295b = (f3 * sin) + (f4 * cos);
        return this;
    }

    public float a() {
        float atan2 = ((float) Math.atan2((double) this.f5295b, (double) this.f5294a)) * 57.295776f;
        return atan2 < Animation.CurveTimeline.LINEAR ? atan2 + 360.0f : atan2;
    }

    public p a(float f2) {
        b(f2 * 0.017453292f);
        return this;
    }
}
