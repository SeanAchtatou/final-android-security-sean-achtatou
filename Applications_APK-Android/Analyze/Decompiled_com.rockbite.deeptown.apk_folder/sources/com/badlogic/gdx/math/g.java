package com.badlogic.gdx.math;

import com.badlogic.gdx.utils.m;
import com.esotericsoftware.spine.Animation;

/* compiled from: Intersector */
public final class g {
    static {
        new q();
        new q();
        new q();
        new m();
        new m();
        new p();
        new p();
        new p();
        new p();
        new p();
        new j(new q(), Animation.CurveTimeline.LINEAR);
        new q();
        new q();
        new q();
        new q();
        new q();
        new q();
        new q();
        new q();
        new p();
        new q();
    }

    public static int a(p pVar, p pVar2, p pVar3) {
        float f2 = pVar2.f5294a;
        float f3 = pVar.f5294a;
        float f4 = pVar3.f5295b;
        float f5 = pVar.f5295b;
        return (int) Math.signum(((f2 - f3) * (f4 - f5)) - ((pVar2.f5295b - f5) * (pVar3.f5294a - f3)));
    }

    public static boolean a(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, p pVar) {
        float f10 = f9 - f7;
        float f11 = f4 - f2;
        float f12 = f8 - f6;
        float f13 = f5 - f3;
        float f14 = (f10 * f11) - (f12 * f13);
        if (f14 == Animation.CurveTimeline.LINEAR) {
            return false;
        }
        float f15 = f3 - f7;
        float f16 = f2 - f6;
        float f17 = ((f12 * f15) - (f10 * f16)) / f14;
        if (f17 >= Animation.CurveTimeline.LINEAR && f17 <= 1.0f) {
            float f18 = ((f15 * f11) - (f16 * f13)) / f14;
            if (f18 >= Animation.CurveTimeline.LINEAR && f18 <= 1.0f) {
                if (pVar == null) {
                    return true;
                }
                pVar.d(f2 + (f11 * f17), f3 + (f13 * f17));
                return true;
            }
        }
        return false;
    }
}
