package com.badlogic.gdx.math;

import com.esotericsoftware.spine.Animation;

/* compiled from: Frustum */
public class d {

    /* renamed from: d  reason: collision with root package name */
    protected static final q[] f5251d = {new q(-1.0f, -1.0f, -1.0f), new q(1.0f, -1.0f, -1.0f), new q(1.0f, 1.0f, -1.0f), new q(-1.0f, 1.0f, -1.0f), new q(-1.0f, -1.0f, 1.0f), new q(1.0f, -1.0f, 1.0f), new q(1.0f, 1.0f, 1.0f), new q(-1.0f, 1.0f, 1.0f)};

    /* renamed from: e  reason: collision with root package name */
    protected static final float[] f5252e = new float[24];

    /* renamed from: a  reason: collision with root package name */
    public final j[] f5253a = new j[6];

    /* renamed from: b  reason: collision with root package name */
    public final q[] f5254b;

    /* renamed from: c  reason: collision with root package name */
    protected final float[] f5255c;

    static {
        int i2 = 0;
        q[] qVarArr = f5251d;
        int length = qVarArr.length;
        int i3 = 0;
        while (i2 < length) {
            q qVar = qVarArr[i2];
            float[] fArr = f5252e;
            int i4 = i3 + 1;
            fArr[i3] = qVar.f5296a;
            int i5 = i4 + 1;
            fArr[i4] = qVar.f5297b;
            fArr[i5] = qVar.f5298c;
            i2++;
            i3 = i5 + 1;
        }
        new q();
    }

    public d() {
        this.f5254b = new q[]{new q(), new q(), new q(), new q(), new q(), new q(), new q(), new q()};
        this.f5255c = new float[24];
        for (int i2 = 0; i2 < 6; i2++) {
            this.f5253a[i2] = new j(new q(), Animation.CurveTimeline.LINEAR);
        }
    }

    public void a(Matrix4 matrix4) {
        float[] fArr = f5252e;
        System.arraycopy(fArr, 0, this.f5255c, 0, fArr.length);
        Matrix4.prj(matrix4.f5238a, this.f5255c, 0, 8, 3);
        int i2 = 0;
        int i3 = 0;
        while (i2 < 8) {
            q qVar = this.f5254b[i2];
            float[] fArr2 = this.f5255c;
            int i4 = i3 + 1;
            qVar.f5296a = fArr2[i3];
            int i5 = i4 + 1;
            qVar.f5297b = fArr2[i4];
            qVar.f5298c = fArr2[i5];
            i2++;
            i3 = i5 + 1;
        }
        j jVar = this.f5253a[0];
        q[] qVarArr = this.f5254b;
        jVar.a(qVarArr[1], qVarArr[0], qVarArr[2]);
        j jVar2 = this.f5253a[1];
        q[] qVarArr2 = this.f5254b;
        jVar2.a(qVarArr2[4], qVarArr2[5], qVarArr2[7]);
        j jVar3 = this.f5253a[2];
        q[] qVarArr3 = this.f5254b;
        jVar3.a(qVarArr3[0], qVarArr3[4], qVarArr3[3]);
        j jVar4 = this.f5253a[3];
        q[] qVarArr4 = this.f5254b;
        jVar4.a(qVarArr4[5], qVarArr4[1], qVarArr4[6]);
        j jVar5 = this.f5253a[4];
        q[] qVarArr5 = this.f5254b;
        jVar5.a(qVarArr5[2], qVarArr5[3], qVarArr5[6]);
        j jVar6 = this.f5253a[5];
        q[] qVarArr6 = this.f5254b;
        jVar6.a(qVarArr6[4], qVarArr6[0], qVarArr6[1]);
    }
}
