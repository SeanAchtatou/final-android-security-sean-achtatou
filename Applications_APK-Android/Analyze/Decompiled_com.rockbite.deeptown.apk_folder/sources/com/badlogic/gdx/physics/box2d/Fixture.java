package com.badlogic.gdx.physics.box2d;

public class Fixture {

    /* renamed from: a  reason: collision with root package name */
    private Body f5319a;

    /* renamed from: b  reason: collision with root package name */
    protected long f5320b;

    /* renamed from: c  reason: collision with root package name */
    private final short[] f5321c = new short[3];

    /* renamed from: d  reason: collision with root package name */
    private final d f5322d = new d();

    protected Fixture(Body body, long j2) {
        this.f5319a = body;
        this.f5320b = j2;
    }

    private native void jniGetFilterData(long j2, short[] sArr);

    /* access modifiers changed from: protected */
    public void a(Body body, long j2) {
        this.f5319a = body;
        this.f5320b = j2;
    }

    public void a(Object obj) {
    }

    public d b() {
        jniGetFilterData(this.f5320b, this.f5321c);
        d dVar = this.f5322d;
        short[] sArr = this.f5321c;
        dVar.f5361b = sArr[0];
        dVar.f5360a = sArr[1];
        dVar.f5362c = sArr[2];
        return dVar;
    }

    public Body a() {
        return this.f5319a;
    }
}
