package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.p;
import com.badlogic.gdx.utils.a;

public class Body {

    /* renamed from: a  reason: collision with root package name */
    protected long f5310a;

    /* renamed from: b  reason: collision with root package name */
    private final float[] f5311b = new float[4];

    /* renamed from: c  reason: collision with root package name */
    private final World f5312c;

    /* renamed from: d  reason: collision with root package name */
    private a<Fixture> f5313d = new a<>(2);

    /* renamed from: e  reason: collision with root package name */
    protected a<f> f5314e = new a<>(2);

    /* renamed from: f  reason: collision with root package name */
    private final p f5315f;

    /* renamed from: g  reason: collision with root package name */
    private final p f5316g;

    protected Body(World world, long j2) {
        new j();
        this.f5315f = new p();
        new p();
        new p();
        this.f5316g = new p();
        new g();
        new p();
        new p();
        new p();
        new p();
        new p();
        new p();
        this.f5312c = world;
        this.f5310a = j2;
    }

    private native void jniApplyForce(long j2, float f2, float f3, float f4, float f5, boolean z);

    private native long jniCreateFixture(long j2, long j3, float f2, float f3, float f4, boolean z, short s, short s2, short s3);

    private native float jniGetAngle(long j2);

    private native void jniGetLinearVelocity(long j2, float[] fArr);

    private native float jniGetMass(long j2);

    private native void jniGetPosition(long j2, float[] fArr);

    /* access modifiers changed from: protected */
    public void a(long j2) {
        this.f5310a = j2;
        int i2 = 0;
        while (true) {
            a<Fixture> aVar = this.f5313d;
            if (i2 < aVar.f5374b) {
                this.f5312c.f5333b.free(aVar.get(i2));
                i2++;
            } else {
                aVar.clear();
                this.f5314e.clear();
                return;
            }
        }
    }

    public void a(Object obj) {
    }

    public a<Fixture> b() {
        return this.f5313d;
    }

    public a<f> c() {
        return this.f5314e;
    }

    public p d() {
        jniGetLinearVelocity(this.f5310a, this.f5311b);
        p pVar = this.f5316g;
        float[] fArr = this.f5311b;
        pVar.f5294a = fArr[0];
        pVar.f5295b = fArr[1];
        return pVar;
    }

    public float e() {
        return jniGetMass(this.f5310a);
    }

    public p f() {
        jniGetPosition(this.f5310a, this.f5311b);
        p pVar = this.f5315f;
        float[] fArr = this.f5311b;
        pVar.f5294a = fArr[0];
        pVar.f5295b = fArr[1];
        return pVar;
    }

    public Fixture a(e eVar) {
        long j2 = this.f5310a;
        long j3 = eVar.f5363a.f5331a;
        float f2 = eVar.f5364b;
        float f3 = eVar.f5365c;
        float f4 = eVar.f5366d;
        boolean z = eVar.f5367e;
        d dVar = eVar.f5368f;
        long jniCreateFixture = jniCreateFixture(j2, j3, f2, f3, f4, z, dVar.f5360a, dVar.f5361b, dVar.f5362c);
        Fixture obtain = this.f5312c.f5333b.obtain();
        obtain.a(this, jniCreateFixture);
        this.f5312c.f5336e.b(obtain.f5320b, obtain);
        this.f5313d.add(obtain);
        return obtain;
    }

    public float a() {
        return jniGetAngle(this.f5310a);
    }

    public void a(p pVar, p pVar2, boolean z) {
        jniApplyForce(this.f5310a, pVar.f5294a, pVar.f5295b, pVar2.f5294a, pVar2.f5295b, z);
    }
}
