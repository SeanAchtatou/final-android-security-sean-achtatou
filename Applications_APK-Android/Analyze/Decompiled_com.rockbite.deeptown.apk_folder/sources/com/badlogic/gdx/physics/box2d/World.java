package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.p;
import com.badlogic.gdx.utils.f0;
import com.badlogic.gdx.utils.l;
import com.badlogic.gdx.utils.m0;
import com.badlogic.gdx.utils.y;
import com.esotericsoftware.spine.Animation;

public final class World implements l {

    /* renamed from: a  reason: collision with root package name */
    protected final f0<Body> f5332a = new a(100, 200);

    /* renamed from: b  reason: collision with root package name */
    protected final f0<Fixture> f5333b = new b(this, 100, 200);

    /* renamed from: c  reason: collision with root package name */
    protected final long f5334c;

    /* renamed from: d  reason: collision with root package name */
    protected final y<Body> f5335d = new y<>(100);

    /* renamed from: e  reason: collision with root package name */
    protected final y<Fixture> f5336e = new y<>(100);

    /* renamed from: f  reason: collision with root package name */
    protected final y<Joint> f5337f = new y<>(100);

    /* renamed from: g  reason: collision with root package name */
    protected b f5338g = null;

    /* renamed from: h  reason: collision with root package name */
    protected c f5339h = null;

    /* renamed from: i  reason: collision with root package name */
    private h f5340i;

    /* renamed from: j  reason: collision with root package name */
    private long[] f5341j;

    /* renamed from: k  reason: collision with root package name */
    private final com.badlogic.gdx.utils.a<Contact> f5342k;
    private final com.badlogic.gdx.utils.a<Contact> l;
    private final Contact m;
    private final Manifold n;
    private final ContactImpulse o;
    private i p;
    private p q;
    private p r;

    class a extends f0<Body> {
        a(int i2, int i3) {
            super(i2, i3);
        }

        /* access modifiers changed from: protected */
        public Body newObject() {
            return new Body(World.this, 0);
        }
    }

    class b extends f0<Fixture> {
        b(World world, int i2, int i3) {
            super(i2, i3);
        }

        /* access modifiers changed from: protected */
        public Fixture newObject() {
            return new Fixture(null, 0);
        }
    }

    static {
        new m0().a("gdx-box2d");
    }

    public World(p pVar, boolean z) {
        new p();
        this.f5340i = null;
        this.f5341j = new long[200];
        this.f5342k = new com.badlogic.gdx.utils.a<>();
        this.l = new com.badlogic.gdx.utils.a<>();
        this.m = new Contact(this, 0);
        this.n = new Manifold(0);
        this.o = new ContactImpulse(this, 0);
        this.p = null;
        this.q = new p();
        this.r = new p();
        this.f5334c = newWorld(pVar.f5294a, pVar.f5295b, z);
        this.f5342k.c(this.f5341j.length);
        this.l.c(this.f5341j.length);
        for (int i2 = 0; i2 < this.f5341j.length; i2++) {
            this.l.add(new Contact(this, 0));
        }
    }

    private void beginContact(long j2) {
        Contact contact = this.m;
        contact.f5317a = j2;
        c cVar = this.f5339h;
        if (cVar != null) {
            cVar.b(contact);
        }
    }

    private boolean contactFilter(long j2, long j3) {
        b bVar = this.f5338g;
        if (bVar != null) {
            return bVar.a(this.f5336e.a(j2), this.f5336e.a(j3));
        }
        d b2 = this.f5336e.a(j2).b();
        d b3 = this.f5336e.a(j3).b();
        short s = b2.f5362c;
        if (s == b3.f5362c && s != 0) {
            return s > 0;
        }
        if ((b2.f5361b & b3.f5360a) == 0 || (b2.f5360a & b3.f5361b) == 0) {
            return false;
        }
        return true;
    }

    private void endContact(long j2) {
        Contact contact = this.m;
        contact.f5317a = j2;
        c cVar = this.f5339h;
        if (cVar != null) {
            cVar.a(contact);
        }
    }

    private native long jniCreateBody(long j2, int i2, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, float f10);

    private native void jniDestroyBody(long j2, long j3);

    private native void jniDestroyJoint(long j2, long j3);

    private native void jniDispose(long j2);

    private native void jniRayCast(long j2, float f2, float f3, float f4, float f5);

    private native void jniSetGravity(long j2, float f2, float f3);

    private native void jniStep(long j2, float f2, int i2, int i3);

    private native long newWorld(float f2, float f3, boolean z);

    private void postSolve(long j2, long j3) {
        Contact contact = this.m;
        contact.f5317a = j2;
        ContactImpulse contactImpulse = this.o;
        contactImpulse.f5318a = j3;
        c cVar = this.f5339h;
        if (cVar != null) {
            cVar.a(contact, contactImpulse);
        }
    }

    private void preSolve(long j2, long j3) {
        Contact contact = this.m;
        contact.f5317a = j2;
        Manifold manifold = this.n;
        manifold.f5326a = j3;
        c cVar = this.f5339h;
        if (cVar != null) {
            cVar.a(contact, manifold);
        }
    }

    private boolean reportFixture(long j2) {
        h hVar = this.f5340i;
        if (hVar != null) {
            return hVar.a(this.f5336e.a(j2));
        }
        return false;
    }

    private float reportRayFixture(long j2, float f2, float f3, float f4, float f5, float f6) {
        i iVar = this.p;
        if (iVar == null) {
            return Animation.CurveTimeline.LINEAR;
        }
        p pVar = this.q;
        pVar.f5294a = f2;
        pVar.f5295b = f3;
        p pVar2 = this.r;
        pVar2.f5294a = f4;
        pVar2.f5295b = f5;
        return iVar.a(this.f5336e.a(j2), this.q, this.r, f6);
    }

    public Body a(a aVar) {
        a aVar2 = aVar;
        long j2 = this.f5334c;
        int a2 = aVar2.f5344a.a();
        p pVar = aVar2.f5345b;
        float f2 = pVar.f5294a;
        float f3 = pVar.f5295b;
        float f4 = aVar2.f5346c;
        p pVar2 = aVar2.f5347d;
        a aVar3 = aVar2;
        long jniCreateBody = jniCreateBody(j2, a2, f2, f3, f4, pVar2.f5294a, pVar2.f5295b, aVar2.f5348e, aVar2.f5349f, aVar2.f5350g, aVar2.f5351h, aVar2.f5352i, aVar2.f5353j, aVar3.f5354k, aVar3.l, aVar3.m);
        Body obtain = this.f5332a.obtain();
        obtain.a(jniCreateBody);
        this.f5335d.b(obtain.f5310a, obtain);
        return obtain;
    }

    public void dispose() {
        jniDispose(this.f5334c);
    }

    public void a(Body body) {
        com.badlogic.gdx.utils.a<f> c2 = body.c();
        while (c2.f5374b > 0) {
            a(body.c().get(0).f5370b);
        }
        jniDestroyBody(this.f5334c, body.f5310a);
        body.a((Object) null);
        this.f5335d.b(body.f5310a);
        com.badlogic.gdx.utils.a<Fixture> b2 = body.b();
        while (b2.f5374b > 0) {
            Fixture d2 = b2.d(0);
            this.f5336e.b(d2.f5320b).a(null);
            this.f5333b.free(d2);
        }
        this.f5332a.free(body);
    }

    public void a(Joint joint) {
        joint.a(null);
        this.f5337f.b(joint.f5323a);
        joint.f5324b.f5369a.f5314e.d(joint.f5325c, true);
        joint.f5325c.f5369a.f5314e.d(joint.f5324b, true);
        jniDestroyJoint(this.f5334c, joint.f5323a);
    }

    public void a(float f2, int i2, int i3) {
        jniStep(this.f5334c, f2, i2, i3);
    }

    public void a(p pVar) {
        jniSetGravity(this.f5334c, pVar.f5294a, pVar.f5295b);
    }

    public void a(i iVar, p pVar, p pVar2) {
        a(iVar, pVar.f5294a, pVar.f5295b, pVar2.f5294a, pVar2.f5295b);
    }

    public void a(i iVar, float f2, float f3, float f4, float f5) {
        this.p = iVar;
        jniRayCast(this.f5334c, f2, f3, f4, f5);
    }
}
