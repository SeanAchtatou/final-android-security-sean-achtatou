package com.badlogic.gdx.physics.box2d;

/* compiled from: ContactListener */
public interface c {
    void a(Contact contact);

    void a(Contact contact, ContactImpulse contactImpulse);

    void a(Contact contact, Manifold manifold);

    void b(Contact contact);
}
