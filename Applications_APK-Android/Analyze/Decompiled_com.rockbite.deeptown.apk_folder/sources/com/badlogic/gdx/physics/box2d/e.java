package com.badlogic.gdx.physics.box2d;

import com.esotericsoftware.spine.Animation;

/* compiled from: FixtureDef */
public class e {

    /* renamed from: a  reason: collision with root package name */
    public Shape f5363a;

    /* renamed from: b  reason: collision with root package name */
    public float f5364b = 0.2f;

    /* renamed from: c  reason: collision with root package name */
    public float f5365c = Animation.CurveTimeline.LINEAR;

    /* renamed from: d  reason: collision with root package name */
    public float f5366d = Animation.CurveTimeline.LINEAR;

    /* renamed from: e  reason: collision with root package name */
    public boolean f5367e = false;

    /* renamed from: f  reason: collision with root package name */
    public final d f5368f = new d();
}
