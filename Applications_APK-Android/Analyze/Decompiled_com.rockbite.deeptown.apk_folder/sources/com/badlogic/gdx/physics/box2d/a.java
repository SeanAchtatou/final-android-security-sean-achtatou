package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.p;
import com.esotericsoftware.spine.Animation;

/* compiled from: BodyDef */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public C0122a f5344a = C0122a.StaticBody;

    /* renamed from: b  reason: collision with root package name */
    public final p f5345b = new p();

    /* renamed from: c  reason: collision with root package name */
    public float f5346c = Animation.CurveTimeline.LINEAR;

    /* renamed from: d  reason: collision with root package name */
    public final p f5347d = new p();

    /* renamed from: e  reason: collision with root package name */
    public float f5348e = Animation.CurveTimeline.LINEAR;

    /* renamed from: f  reason: collision with root package name */
    public float f5349f = Animation.CurveTimeline.LINEAR;

    /* renamed from: g  reason: collision with root package name */
    public float f5350g = Animation.CurveTimeline.LINEAR;

    /* renamed from: h  reason: collision with root package name */
    public boolean f5351h = true;

    /* renamed from: i  reason: collision with root package name */
    public boolean f5352i = true;

    /* renamed from: j  reason: collision with root package name */
    public boolean f5353j = false;

    /* renamed from: k  reason: collision with root package name */
    public boolean f5354k = false;
    public boolean l = true;
    public float m = 1.0f;

    /* renamed from: com.badlogic.gdx.physics.box2d.a$a  reason: collision with other inner class name */
    /* compiled from: BodyDef */
    public enum C0122a {
        StaticBody(0),
        KinematicBody(1),
        DynamicBody(2);
        

        /* renamed from: a  reason: collision with root package name */
        private int f5359a;

        private C0122a(int i2) {
            this.f5359a = i2;
        }

        public int a() {
            return this.f5359a;
        }
    }
}
