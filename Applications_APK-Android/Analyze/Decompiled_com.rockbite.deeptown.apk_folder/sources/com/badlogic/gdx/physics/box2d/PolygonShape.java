package com.badlogic.gdx.physics.box2d;

public class PolygonShape extends Shape {
    public PolygonShape() {
        this.f5331a = newPolygonShape();
    }

    private native void jniSet(long j2, float[] fArr, int i2, int i3);

    private native long newPolygonShape();

    public void a(float[] fArr) {
        jniSet(this.f5331a, fArr, 0, fArr.length);
    }
}
