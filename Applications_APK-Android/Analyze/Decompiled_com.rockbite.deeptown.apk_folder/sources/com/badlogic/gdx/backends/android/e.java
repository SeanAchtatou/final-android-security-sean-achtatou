package com.badlogic.gdx.backends.android;

import android.content.ClipData;
import android.content.Context;
import android.os.Build;
import android.text.ClipboardManager;
import com.badlogic.gdx.utils.g;

/* compiled from: AndroidClipboard */
public class e implements g {

    /* renamed from: a  reason: collision with root package name */
    private ClipboardManager f4638a;

    /* renamed from: b  reason: collision with root package name */
    private android.content.ClipboardManager f4639b;

    public e(Context context) {
        if (Build.VERSION.SDK_INT < 11) {
            this.f4638a = (ClipboardManager) context.getSystemService("clipboard");
        } else {
            this.f4639b = (android.content.ClipboardManager) context.getSystemService("clipboard");
        }
    }

    public String a() {
        CharSequence text;
        if (Build.VERSION.SDK_INT >= 11) {
            ClipData primaryClip = this.f4639b.getPrimaryClip();
            if (primaryClip == null || (text = primaryClip.getItemAt(0).getText()) == null) {
                return null;
            }
            return text.toString();
        } else if (this.f4638a.getText() == null) {
            return null;
        } else {
            return this.f4638a.getText().toString();
        }
    }

    public void a(String str) {
        if (Build.VERSION.SDK_INT < 11) {
            this.f4638a.setText(str);
            return;
        }
        this.f4639b.setPrimaryClip(ClipData.newPlainText(str, str));
    }
}
