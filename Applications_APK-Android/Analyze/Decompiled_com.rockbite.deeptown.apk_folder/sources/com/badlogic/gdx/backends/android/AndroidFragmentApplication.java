package com.badlogic.gdx.backends.android;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import androidx.fragment.app.Fragment;
import com.badlogic.gdx.backends.android.z.f;
import com.badlogic.gdx.utils.g;
import com.badlogic.gdx.utils.n;
import com.badlogic.gdx.utils.o;
import com.badlogic.gdx.utils.o0;
import e.d.b.a;
import e.d.b.d;
import e.d.b.e;
import e.d.b.h;
import e.d.b.m;

public class AndroidFragmentApplication extends Fragment implements a {

    /* renamed from: a  reason: collision with root package name */
    protected k f4611a;

    /* renamed from: b  reason: collision with root package name */
    protected l f4612b;

    /* renamed from: c  reason: collision with root package name */
    protected d f4613c;

    /* renamed from: d  reason: collision with root package name */
    protected h f4614d;

    /* renamed from: e  reason: collision with root package name */
    protected r f4615e;

    /* renamed from: f  reason: collision with root package name */
    protected e f4616f;

    /* renamed from: g  reason: collision with root package name */
    protected e.d.b.b f4617g;

    /* renamed from: h  reason: collision with root package name */
    public Handler f4618h;

    /* renamed from: i  reason: collision with root package name */
    protected boolean f4619i = true;

    /* renamed from: j  reason: collision with root package name */
    protected final com.badlogic.gdx.utils.a<Runnable> f4620j = new com.badlogic.gdx.utils.a<>();

    /* renamed from: k  reason: collision with root package name */
    protected final com.badlogic.gdx.utils.a<Runnable> f4621k = new com.badlogic.gdx.utils.a<>();
    protected final o0<m> l = new o0<>(m.class);
    private final com.badlogic.gdx.utils.a<f> m = new com.badlogic.gdx.utils.a<>();
    protected int n = 2;
    protected c o;

    class a implements m {
        a() {
        }

        public void dispose() {
            AndroidFragmentApplication.this.f4613c.a();
        }

        public void pause() {
            AndroidFragmentApplication.this.f4613c.b();
        }

        public void resume() {
            AndroidFragmentApplication.this.f4613c.c();
        }
    }

    class b implements Runnable {
        b() {
        }

        public void run() {
            AndroidFragmentApplication.this.o.a();
        }
    }

    public interface c {
        void a();
    }

    static {
        n.a();
    }

    private boolean n() {
        for (Fragment parentFragment = getParentFragment(); parentFragment != null; parentFragment = parentFragment.getParentFragment()) {
            if (parentFragment.isRemoving()) {
                return true;
            }
        }
        return false;
    }

    public void a(e.d.b.c cVar) {
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        if (z) {
            getActivity().getWindow().addFlags(128);
        }
    }

    @TargetApi(19)
    public void b(boolean z) {
        if (z && m() >= 19) {
            try {
                View l2 = this.f4611a.l();
                View.class.getMethod("setSystemUiVisibility", Integer.TYPE).invoke(l2, 5894);
            } catch (Exception e2) {
                b("AndroidApplication", "Failed to setup immersive mode, a throwable has occurred.", e2);
            }
        }
    }

    public h c() {
        return this.f4611a;
    }

    public com.badlogic.gdx.utils.a<Runnable> d() {
        return this.f4621k;
    }

    public e.d.b.b e() {
        return this.f4617g;
    }

    public com.badlogic.gdx.utils.a<Runnable> f() {
        return this.f4620j;
    }

    public g g() {
        return this.f4616f;
    }

    public Context getContext() {
        return getActivity();
    }

    public a.C0167a getType() {
        return a.C0167a.Android;
    }

    public WindowManager h() {
        return (WindowManager) getContext().getSystemService("window");
    }

    public o0<m> i() {
        return this.l;
    }

    public d j() {
        return this.f4613c;
    }

    public e k() {
        return this.f4614d;
    }

    public e.d.b.n l() {
        return this.f4615e;
    }

    public int m() {
        return Build.VERSION.SDK_INT;
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        synchronized (this.m) {
            for (int i4 = 0; i4 < this.m.f5374b; i4++) {
                this.m.get(i4).onActivityResult(i2, i3, intent);
            }
        }
    }

    public void onAttach(Activity activity) {
        if (activity instanceof c) {
            this.o = (c) activity;
        } else if (getParentFragment() instanceof c) {
            this.o = (c) getParentFragment();
        } else if (getTargetFragment() instanceof c) {
            this.o = (c) getTargetFragment();
        } else {
            throw new RuntimeException("Missing AndroidFragmentApplication.Callbacks. Please implement AndroidFragmentApplication.Callbacks on the parent activity, fragment or target fragment.");
        }
        super.onAttach(activity);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        boolean z = true;
        if (configuration.hardKeyboardHidden != 1) {
            z = false;
        }
        this.f4612b.F = z;
    }

    public void onDetach() {
        super.onDetach();
        this.o = null;
    }

    public void onPause() {
        boolean m2 = this.f4611a.m();
        boolean z = k.y;
        k.y = true;
        this.f4611a.a(true);
        this.f4611a.q();
        this.f4612b.g();
        if (isRemoving() || n() || getActivity().isFinishing()) {
            this.f4611a.i();
            this.f4611a.j();
        }
        k.y = z;
        this.f4611a.a(m2);
        this.f4611a.o();
        super.onPause();
    }

    public void onResume() {
        e.d.b.g.f6800a = this;
        e.d.b.g.f6803d = b();
        e.d.b.g.f6802c = j();
        e.d.b.g.f6804e = k();
        e.d.b.g.f6801b = c();
        e.d.b.g.f6805f = l();
        this.f4612b.h();
        k kVar = this.f4611a;
        if (kVar != null) {
            kVar.p();
        }
        if (!this.f4619i) {
            this.f4611a.s();
        } else {
            this.f4619i = false;
        }
        super.onResume();
    }

    public View a(e.d.b.b bVar, b bVar2) {
        if (m() >= 9) {
            a(new c());
            f fVar = bVar2.q;
            if (fVar == null) {
                fVar = new com.badlogic.gdx.backends.android.z.a();
            }
            this.f4611a = new k(this, bVar2, fVar);
            this.f4612b = m.a(this, getActivity(), this.f4611a.f4649a, bVar2);
            this.f4613c = new d(getActivity(), bVar2);
            this.f4614d = new h(getResources().getAssets(), getActivity().getFilesDir().getAbsolutePath());
            this.f4615e = new r(this, bVar2);
            this.f4617g = bVar;
            this.f4618h = new Handler();
            this.f4616f = new e(getActivity());
            a(new a());
            e.d.b.g.f6800a = this;
            e.d.b.g.f6803d = b();
            e.d.b.g.f6802c = j();
            e.d.b.g.f6804e = k();
            e.d.b.g.f6801b = c();
            e.d.b.g.f6805f = l();
            a(bVar2.n);
            b(bVar2.r);
            if (bVar2.r && m() >= 19) {
                try {
                    Class<?> cls = Class.forName("com.badlogic.gdx.backends.android.w");
                    Object newInstance = cls.newInstance();
                    cls.getDeclaredMethod("createListener", a.class).invoke(newInstance, this);
                } catch (Exception e2) {
                    b("AndroidApplication", "Failed to create AndroidVisibilityListener", e2);
                }
            }
            if (getResources().getConfiguration().keyboard != 1) {
                b().F = true;
            }
            return this.f4611a.l();
        }
        throw new o("LibGDX requires Android API Level 9 or later.");
    }

    public void c(String str, String str2) {
        if (this.n >= 1) {
            Log.e(str, str2);
        }
    }

    public l b() {
        return this.f4612b;
    }

    public void b(String str, String str2) {
        if (this.n >= 2) {
            Log.i(str, str2);
        }
    }

    public void b(String str, String str2, Throwable th) {
        if (this.n >= 2) {
            Log.i(str, str2, th);
        }
    }

    public void b(m mVar) {
        synchronized (this.l) {
            this.l.d(mVar, true);
        }
    }

    public e.d.b.o a(String str) {
        return new t(getActivity().getSharedPreferences(str, 0));
    }

    public void a(Runnable runnable) {
        synchronized (this.f4620j) {
            this.f4620j.add(runnable);
            e.d.b.g.f6801b.f();
        }
    }

    public void a() {
        this.f4618h.post(new b());
    }

    public void a(String str, String str2) {
        if (this.n >= 3) {
            Log.d(str, str2);
        }
    }

    public void a(String str, String str2, Throwable th) {
        if (this.n >= 1) {
            Log.e(str, str2, th);
        }
    }

    public void a(m mVar) {
        synchronized (this.l) {
            this.l.add(mVar);
        }
    }
}
