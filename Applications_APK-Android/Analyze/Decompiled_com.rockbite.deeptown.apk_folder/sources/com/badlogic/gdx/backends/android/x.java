package com.badlogic.gdx.backends.android;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import com.badlogic.gdx.utils.o;
import e.d.b.e;
import e.d.b.g;
import e.d.b.s.a;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: AndroidZipFileHandle */
public class x extends g {

    /* renamed from: d  reason: collision with root package name */
    private boolean f4700d;

    /* renamed from: e  reason: collision with root package name */
    private long f4701e;

    /* renamed from: f  reason: collision with root package name */
    private y f4702f;

    /* renamed from: g  reason: collision with root package name */
    private String f4703g;

    public x(String str) {
        super((AssetManager) null, str, e.a.Internal);
        r();
        throw null;
    }

    private String q() {
        return this.f4703g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private void r() {
        this.f4703g = this.f6913a.getPath().replace('\\', '/');
        this.f4702f = ((h) g.f6804e).c();
        this.f4702f.a(q());
        throw null;
    }

    public a a(String str) {
        if (this.f6913a.getPath().length() == 0) {
            new x(new File(str), this.f6914b);
            throw null;
        }
        new x(new File(this.f6913a, str), this.f6914b);
        throw null;
    }

    public a d(String str) {
        if (this.f6913a.getPath().length() != 0) {
            return g.f6804e.a(new File(this.f6913a.getParent(), str).getPath(), this.f6914b);
        }
        throw new o("Cannot get the sibling of the root.");
    }

    public a i() {
        File parentFile = this.f6913a.getParentFile();
        if (parentFile == null) {
            parentFile = new File("");
        }
        new x(parentFile.getPath());
        throw null;
    }

    public InputStream l() {
        try {
            this.f4702f.c(q());
            throw null;
        } catch (IOException e2) {
            throw new o("Error reading file: " + this.f6913a + " (ZipResourceFile)", e2);
        }
    }

    public AssetFileDescriptor p() throws IOException {
        this.f4702f.a(q());
        throw null;
    }

    public x(File file, e.a aVar) {
        super((AssetManager) null, file, aVar);
        r();
        throw null;
    }

    public boolean a() {
        if (this.f4700d) {
            return true;
        }
        this.f4702f.b(q());
        throw null;
    }

    public long d() {
        if (this.f4700d) {
            return this.f4701e;
        }
        return 0;
    }
}
