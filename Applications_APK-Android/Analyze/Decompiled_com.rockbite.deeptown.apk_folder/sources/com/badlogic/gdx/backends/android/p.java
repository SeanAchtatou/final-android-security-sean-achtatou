package com.badlogic.gdx.backends.android;

import android.content.Context;
import com.badlogic.gdx.backends.android.l;

/* compiled from: AndroidMultiTouchHandler */
public class p implements v {
    private int a(int i2) {
        if (i2 == 0 || i2 == 1) {
            return 0;
        }
        if (i2 == 2) {
            return 1;
        }
        if (i2 == 4) {
            return 2;
        }
        if (i2 == 8) {
            return 3;
        }
        return i2 == 16 ? 4 : -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0146, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0191, code lost:
        e.d.b.g.f6800a.c().f();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x019a, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x019b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:5:0x0029, B:39:0x013a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.view.MotionEvent r21, com.badlogic.gdx.backends.android.l r22) {
        /*
            r20 = this;
            r0 = r21
            r10 = r22
            int r1 = r21.getAction()
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r2 = r21.getAction()
            r3 = 65280(0xff00, float:9.1477E-41)
            r2 = r2 & r3
            int r11 = r2 >> 8
            int r2 = r0.getPointerId(r11)
            long r12 = java.lang.System.nanoTime()
            monitor-enter(r22)
            r9 = 0
            r14 = 20
            r15 = -1
            r16 = 0
            switch(r1) {
                case 0: goto L_0x011c;
                case 1: goto L_0x00cd;
                case 2: goto L_0x0051;
                case 3: goto L_0x0028;
                case 4: goto L_0x00cd;
                case 5: goto L_0x011c;
                case 6: goto L_0x00cd;
                default: goto L_0x0026;
            }
        L_0x0026:
            goto L_0x0190
        L_0x0028:
            r0 = 0
        L_0x0029:
            int[] r1 = r10.l     // Catch:{ all -> 0x019b }
            int r1 = r1.length     // Catch:{ all -> 0x019b }
            if (r0 >= r1) goto L_0x0190
            int[] r1 = r10.l     // Catch:{ all -> 0x019b }
            r1[r0] = r15     // Catch:{ all -> 0x019b }
            int[] r1 = r10.f4665f     // Catch:{ all -> 0x019b }
            r1[r0] = r16     // Catch:{ all -> 0x019b }
            int[] r1 = r10.f4666g     // Catch:{ all -> 0x019b }
            r1[r0] = r16     // Catch:{ all -> 0x019b }
            int[] r1 = r10.f4667h     // Catch:{ all -> 0x019b }
            r1[r0] = r16     // Catch:{ all -> 0x019b }
            int[] r1 = r10.f4668i     // Catch:{ all -> 0x019b }
            r1[r0] = r16     // Catch:{ all -> 0x019b }
            boolean[] r1 = r10.f4669j     // Catch:{ all -> 0x019b }
            r1[r0] = r16     // Catch:{ all -> 0x019b }
            int[] r1 = r10.f4670k     // Catch:{ all -> 0x019b }
            r1[r0] = r16     // Catch:{ all -> 0x019b }
            float[] r1 = r10.m     // Catch:{ all -> 0x019b }
            r1[r0] = r9     // Catch:{ all -> 0x019b }
            int r0 = r0 + 1
            goto L_0x0029
        L_0x0051:
            int r11 = r21.getPointerCount()     // Catch:{ all -> 0x019b }
            r8 = 0
        L_0x0056:
            if (r8 >= r11) goto L_0x0190
            int r1 = r0.getPointerId(r8)     // Catch:{ all -> 0x019b }
            float r2 = r0.getX(r8)     // Catch:{ all -> 0x019b }
            int r9 = (int) r2     // Catch:{ all -> 0x019b }
            float r2 = r0.getY(r8)     // Catch:{ all -> 0x019b }
            int r7 = (int) r2     // Catch:{ all -> 0x019b }
            int r6 = r10.c(r1)     // Catch:{ all -> 0x019b }
            if (r6 != r15) goto L_0x006e
            r14 = r8
            goto L_0x00c8
        L_0x006e:
            if (r6 < r14) goto L_0x0072
            goto L_0x0190
        L_0x0072:
            int[] r1 = r10.f4670k     // Catch:{ all -> 0x019b }
            r5 = r1[r6]     // Catch:{ all -> 0x019b }
            if (r5 == r15) goto L_0x008f
            r3 = 2
            r1 = r20
            r2 = r22
            r4 = r9
            r16 = r5
            r5 = r7
            r17 = r6
            r18 = r7
            r7 = r16
            r14 = r8
            r16 = r9
            r8 = r12
            r1.a(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x019b }
            goto L_0x00a4
        L_0x008f:
            r17 = r6
            r18 = r7
            r14 = r8
            r16 = r9
            r3 = 4
            r7 = 0
            r1 = r20
            r2 = r22
            r4 = r16
            r5 = r18
            r8 = r12
            r1.a(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x019b }
        L_0x00a4:
            int[] r1 = r10.f4667h     // Catch:{ all -> 0x019b }
            int[] r2 = r10.f4665f     // Catch:{ all -> 0x019b }
            r2 = r2[r17]     // Catch:{ all -> 0x019b }
            int r9 = r16 - r2
            r1[r17] = r9     // Catch:{ all -> 0x019b }
            int[] r1 = r10.f4668i     // Catch:{ all -> 0x019b }
            int[] r2 = r10.f4666g     // Catch:{ all -> 0x019b }
            r2 = r2[r17]     // Catch:{ all -> 0x019b }
            int r7 = r18 - r2
            r1[r17] = r7     // Catch:{ all -> 0x019b }
            int[] r1 = r10.f4665f     // Catch:{ all -> 0x019b }
            r1[r17] = r16     // Catch:{ all -> 0x019b }
            int[] r1 = r10.f4666g     // Catch:{ all -> 0x019b }
            r1[r17] = r18     // Catch:{ all -> 0x019b }
            float[] r1 = r10.m     // Catch:{ all -> 0x019b }
            float r2 = r0.getPressure(r14)     // Catch:{ all -> 0x019b }
            r1[r17] = r2     // Catch:{ all -> 0x019b }
        L_0x00c8:
            int r8 = r14 + 1
            r14 = 20
            goto L_0x0056
        L_0x00cd:
            int r14 = r10.c(r2)     // Catch:{ all -> 0x019b }
            if (r14 != r15) goto L_0x00d5
            goto L_0x0190
        L_0x00d5:
            r1 = 20
            if (r14 < r1) goto L_0x00db
            goto L_0x0190
        L_0x00db:
            int[] r1 = r10.l     // Catch:{ all -> 0x019b }
            r1[r14] = r15     // Catch:{ all -> 0x019b }
            float r1 = r0.getX(r11)     // Catch:{ all -> 0x019b }
            int r7 = (int) r1     // Catch:{ all -> 0x019b }
            float r0 = r0.getY(r11)     // Catch:{ all -> 0x019b }
            int r11 = (int) r0     // Catch:{ all -> 0x019b }
            int[] r0 = r10.f4670k     // Catch:{ all -> 0x019b }
            r6 = r0[r14]     // Catch:{ all -> 0x019b }
            if (r6 == r15) goto L_0x00fd
            r2 = 1
            r0 = r20
            r1 = r22
            r3 = r7
            r4 = r11
            r5 = r14
            r15 = r7
            r7 = r12
            r0.a(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x019b }
            goto L_0x00fe
        L_0x00fd:
            r15 = r7
        L_0x00fe:
            int[] r0 = r10.f4665f     // Catch:{ all -> 0x019b }
            r0[r14] = r15     // Catch:{ all -> 0x019b }
            int[] r0 = r10.f4666g     // Catch:{ all -> 0x019b }
            r0[r14] = r11     // Catch:{ all -> 0x019b }
            int[] r0 = r10.f4667h     // Catch:{ all -> 0x019b }
            r0[r14] = r16     // Catch:{ all -> 0x019b }
            int[] r0 = r10.f4668i     // Catch:{ all -> 0x019b }
            r0[r14] = r16     // Catch:{ all -> 0x019b }
            boolean[] r0 = r10.f4669j     // Catch:{ all -> 0x019b }
            r0[r14] = r16     // Catch:{ all -> 0x019b }
            int[] r0 = r10.f4670k     // Catch:{ all -> 0x019b }
            r0[r14] = r16     // Catch:{ all -> 0x019b }
            float[] r0 = r10.m     // Catch:{ all -> 0x019b }
            r0[r14] = r9     // Catch:{ all -> 0x019b }
            goto L_0x0190
        L_0x011c:
            int r14 = r22.e()     // Catch:{ all -> 0x019b }
            r1 = 20
            if (r14 < r1) goto L_0x0126
            goto L_0x0190
        L_0x0126:
            int[] r1 = r10.l     // Catch:{ all -> 0x019b }
            r1[r14] = r2     // Catch:{ all -> 0x019b }
            float r1 = r0.getX(r11)     // Catch:{ all -> 0x019b }
            int r8 = (int) r1     // Catch:{ all -> 0x019b }
            float r1 = r0.getY(r11)     // Catch:{ all -> 0x019b }
            int r9 = (int) r1     // Catch:{ all -> 0x019b }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x019b }
            r2 = 14
            if (r1 < r2) goto L_0x014a
            int r1 = r21.getButtonState()     // Catch:{ all -> 0x0146 }
            r7 = r20
            int r1 = r7.a(r1)     // Catch:{ all -> 0x019b }
            r6 = r1
            goto L_0x014d
        L_0x0146:
            r0 = move-exception
            r7 = r20
            goto L_0x019c
        L_0x014a:
            r7 = r20
            r6 = 0
        L_0x014d:
            if (r6 == r15) goto L_0x0164
            r3 = 0
            r1 = r20
            r2 = r22
            r4 = r8
            r5 = r9
            r17 = r6
            r6 = r14
            r7 = r17
            r18 = r8
            r19 = r9
            r8 = r12
            r1.a(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x019b }
            goto L_0x016a
        L_0x0164:
            r17 = r6
            r18 = r8
            r19 = r9
        L_0x016a:
            int[] r1 = r10.f4665f     // Catch:{ all -> 0x019b }
            r1[r14] = r18     // Catch:{ all -> 0x019b }
            int[] r1 = r10.f4666g     // Catch:{ all -> 0x019b }
            r1[r14] = r19     // Catch:{ all -> 0x019b }
            int[] r1 = r10.f4667h     // Catch:{ all -> 0x019b }
            r1[r14] = r16     // Catch:{ all -> 0x019b }
            int[] r1 = r10.f4668i     // Catch:{ all -> 0x019b }
            r1[r14] = r16     // Catch:{ all -> 0x019b }
            boolean[] r1 = r10.f4669j     // Catch:{ all -> 0x019b }
            r2 = r17
            if (r2 == r15) goto L_0x0182
            r16 = 1
        L_0x0182:
            r1[r14] = r16     // Catch:{ all -> 0x019b }
            int[] r1 = r10.f4670k     // Catch:{ all -> 0x019b }
            r1[r14] = r2     // Catch:{ all -> 0x019b }
            float[] r1 = r10.m     // Catch:{ all -> 0x019b }
            float r0 = r0.getPressure(r11)     // Catch:{ all -> 0x019b }
            r1[r14] = r0     // Catch:{ all -> 0x019b }
        L_0x0190:
            monitor-exit(r22)     // Catch:{ all -> 0x019b }
            e.d.b.a r0 = e.d.b.g.f6800a
            e.d.b.h r0 = r0.c()
            r0.f()
            return
        L_0x019b:
            r0 = move-exception
        L_0x019c:
            monitor-exit(r22)     // Catch:{ all -> 0x019b }
            goto L_0x019f
        L_0x019e:
            throw r0
        L_0x019f:
            goto L_0x019e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.p.a(android.view.MotionEvent, com.badlogic.gdx.backends.android.l):void");
    }

    private void a(l lVar, int i2, int i3, int i4, int i5, int i6, long j2) {
        l.f obtain = lVar.f4661b.obtain();
        obtain.f4678a = j2;
        obtain.f4684g = i5;
        obtain.f4680c = i3;
        obtain.f4681d = i4;
        obtain.f4679b = i2;
        obtain.f4683f = i6;
        lVar.f4664e.add(obtain);
    }

    public boolean a(Context context) {
        return context.getPackageManager().hasSystemFeature("android.hardware.touchscreen.multitouch");
    }
}
