package com.badlogic.gdx.backends.android;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import com.badlogic.gdx.utils.f0;
import com.badlogic.gdx.utils.s;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import e.d.b.g;
import e.d.b.h;
import e.d.b.i;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/* compiled from: AndroidInput */
public class l implements i, View.OnKeyListener, View.OnTouchListener {
    protected final v A;
    private int B;
    private s C;
    private boolean D;
    private boolean E;
    boolean F;
    protected final float[] G;
    protected final float[] H;
    private boolean I;
    private e.d.b.l J;
    private final b K;
    protected final i.a L;
    private long M;
    private SensorEventListener N;
    private SensorEventListener O;
    private SensorEventListener P;
    private SensorEventListener Q;
    boolean R;

    /* renamed from: a  reason: collision with root package name */
    f0<d> f4660a = new a(this, 16, 1000);

    /* renamed from: b  reason: collision with root package name */
    f0<f> f4661b = new b(this, 16, 1000);

    /* renamed from: c  reason: collision with root package name */
    ArrayList<View.OnKeyListener> f4662c = new ArrayList<>();

    /* renamed from: d  reason: collision with root package name */
    ArrayList<d> f4663d = new ArrayList<>();

    /* renamed from: e  reason: collision with root package name */
    ArrayList<f> f4664e = new ArrayList<>();

    /* renamed from: f  reason: collision with root package name */
    int[] f4665f = new int[20];

    /* renamed from: g  reason: collision with root package name */
    int[] f4666g = new int[20];

    /* renamed from: h  reason: collision with root package name */
    int[] f4667h = new int[20];

    /* renamed from: i  reason: collision with root package name */
    int[] f4668i = new int[20];

    /* renamed from: j  reason: collision with root package name */
    boolean[] f4669j = new boolean[20];

    /* renamed from: k  reason: collision with root package name */
    int[] f4670k = new int[20];
    int[] l = new int[20];
    float[] m = new float[20];
    final boolean n;
    private int o;
    private boolean[] p;
    private boolean q;
    private boolean[] r;
    private boolean[] s;
    private SensorManager t;
    public boolean u;
    protected final float[] v;
    protected final float[] w;
    private Handler x;
    final e.d.b.a y;
    final Context z;

    /* compiled from: AndroidInput */
    class a extends f0<d> {
        a(l lVar, int i2, int i3) {
            super(i2, i3);
        }

        /* access modifiers changed from: protected */
        public d newObject() {
            return new d();
        }
    }

    /* compiled from: AndroidInput */
    class b extends f0<f> {
        b(l lVar, int i2, int i3) {
            super(i2, i3);
        }

        /* access modifiers changed from: protected */
        public f newObject() {
            return new f();
        }
    }

    /* compiled from: AndroidInput */
    class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ boolean f4671a;

        c(boolean z) {
            this.f4671a = z;
        }

        public void run() {
            InputMethodManager inputMethodManager = (InputMethodManager) l.this.z.getSystemService("input_method");
            if (this.f4671a) {
                View l = ((k) l.this.y.c()).l();
                l.setFocusable(true);
                l.setFocusableInTouchMode(true);
                inputMethodManager.showSoftInput(((k) l.this.y.c()).l(), 0);
                return;
            }
            inputMethodManager.hideSoftInputFromWindow(((k) l.this.y.c()).l().getWindowToken(), 0);
        }
    }

    /* compiled from: AndroidInput */
    static class d {

        /* renamed from: a  reason: collision with root package name */
        long f4673a;

        /* renamed from: b  reason: collision with root package name */
        int f4674b;

        /* renamed from: c  reason: collision with root package name */
        int f4675c;

        /* renamed from: d  reason: collision with root package name */
        char f4676d;

        d() {
        }
    }

    /* compiled from: AndroidInput */
    private class e implements SensorEventListener {
        public e() {
        }

        public void onAccuracyChanged(Sensor sensor, int i2) {
        }

        public void onSensorChanged(SensorEvent sensorEvent) {
            if (sensorEvent.sensor.getType() == 1) {
                l lVar = l.this;
                if (lVar.L == i.a.Portrait) {
                    float[] fArr = sensorEvent.values;
                    float[] fArr2 = lVar.v;
                    System.arraycopy(fArr, 0, fArr2, 0, fArr2.length);
                } else {
                    float[] fArr3 = lVar.v;
                    float[] fArr4 = sensorEvent.values;
                    fArr3[0] = fArr4[1];
                    fArr3[1] = -fArr4[0];
                    fArr3[2] = fArr4[2];
                }
            }
            if (sensorEvent.sensor.getType() == 2) {
                float[] fArr5 = sensorEvent.values;
                float[] fArr6 = l.this.G;
                System.arraycopy(fArr5, 0, fArr6, 0, fArr6.length);
            }
            if (sensorEvent.sensor.getType() == 4) {
                l lVar2 = l.this;
                if (lVar2.L == i.a.Portrait) {
                    float[] fArr7 = sensorEvent.values;
                    float[] fArr8 = lVar2.w;
                    System.arraycopy(fArr7, 0, fArr8, 0, fArr8.length);
                } else {
                    float[] fArr9 = lVar2.w;
                    float[] fArr10 = sensorEvent.values;
                    fArr9[0] = fArr10[1];
                    fArr9[1] = -fArr10[0];
                    fArr9[2] = fArr10[2];
                }
            }
            if (sensorEvent.sensor.getType() == 11) {
                l lVar3 = l.this;
                if (lVar3.L == i.a.Portrait) {
                    float[] fArr11 = sensorEvent.values;
                    float[] fArr12 = lVar3.H;
                    System.arraycopy(fArr11, 0, fArr12, 0, fArr12.length);
                    return;
                }
                float[] fArr13 = lVar3.H;
                float[] fArr14 = sensorEvent.values;
                fArr13[0] = fArr14[1];
                fArr13[1] = -fArr14[0];
                fArr13[2] = fArr14[2];
            }
        }
    }

    /* compiled from: AndroidInput */
    static class f {

        /* renamed from: a  reason: collision with root package name */
        long f4678a;

        /* renamed from: b  reason: collision with root package name */
        int f4679b;

        /* renamed from: c  reason: collision with root package name */
        int f4680c;

        /* renamed from: d  reason: collision with root package name */
        int f4681d;

        /* renamed from: e  reason: collision with root package name */
        int f4682e;

        /* renamed from: f  reason: collision with root package name */
        int f4683f;

        /* renamed from: g  reason: collision with root package name */
        int f4684g;

        f() {
        }
    }

    public l(e.d.b.a aVar, Context context, Object obj, b bVar) {
        int i2 = 0;
        this.o = 0;
        this.p = new boolean[260];
        this.q = false;
        this.r = new boolean[260];
        this.s = new boolean[20];
        this.u = false;
        this.v = new float[3];
        this.w = new float[3];
        this.B = 0;
        this.C = new s();
        this.D = false;
        this.E = false;
        this.G = new float[3];
        this.H = new float[3];
        this.I = false;
        this.M = 0;
        this.R = true;
        if (obj instanceof View) {
            View view = (View) obj;
            view.setOnKeyListener(this);
            view.setOnTouchListener(this);
            view.setFocusable(true);
            view.setFocusableInTouchMode(true);
            view.requestFocus();
        }
        this.K = bVar;
        new s(context, new Handler(), this);
        while (true) {
            int[] iArr = this.l;
            if (i2 >= iArr.length) {
                break;
            }
            iArr[i2] = -1;
            i2++;
        }
        this.x = new Handler();
        this.y = aVar;
        this.z = context;
        this.B = bVar.m;
        this.A = new p();
        this.n = this.A.a(context);
        Vibrator vibrator = (Vibrator) context.getSystemService("vibrator");
        int f2 = f();
        h.b g2 = this.y.c().g();
        if (((f2 == 0 || f2 == 180) && g2.f6817a >= g2.f6818b) || ((f2 == 90 || f2 == 270) && g2.f6817a <= g2.f6818b)) {
            this.L = i.a.Landscape;
        } else {
            this.L = i.a.Portrait;
        }
        this.C.a(255);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x001e, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000b, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean a(int r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            r0 = -1
            r1 = 0
            if (r3 != r0) goto L_0x000e
            int r3 = r2.o     // Catch:{ all -> 0x000c }
            if (r3 <= 0) goto L_0x000a
            r1 = 1
        L_0x000a:
            monitor-exit(r2)
            return r1
        L_0x000c:
            r3 = move-exception
            goto L_0x001b
        L_0x000e:
            if (r3 < 0) goto L_0x001d
            r0 = 260(0x104, float:3.64E-43)
            if (r3 < r0) goto L_0x0015
            goto L_0x001d
        L_0x0015:
            boolean[] r0 = r2.p     // Catch:{ all -> 0x000c }
            boolean r3 = r0[r3]     // Catch:{ all -> 0x000c }
            monitor-exit(r2)
            return r3
        L_0x001b:
            monitor-exit(r2)
            throw r3
        L_0x001d:
            monitor-exit(r2)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.l.a(int):boolean");
    }

    public boolean b(int i2) {
        boolean z2;
        synchronized (this) {
            z2 = this.f4669j[i2];
        }
        return z2;
    }

    public int c() {
        int i2;
        synchronized (this) {
            i2 = this.f4665f[0];
        }
        return i2;
    }

    public int d() {
        int i2;
        synchronized (this) {
            i2 = this.f4666g[0];
        }
        return i2;
    }

    public int e() {
        int length = this.l.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (this.l[i2] == -1) {
                return i2;
            }
        }
        this.l = a(this.l);
        this.f4665f = a(this.f4665f);
        this.f4666g = a(this.f4666g);
        this.f4667h = a(this.f4667h);
        this.f4668i = a(this.f4668i);
        this.f4669j = a(this.f4669j);
        this.f4670k = a(this.f4670k);
        return length;
    }

    public int f() {
        int i2;
        Context context = this.z;
        if (context instanceof Activity) {
            i2 = ((Activity) context).getWindowManager().getDefaultDisplay().getRotation();
        } else {
            i2 = ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getRotation();
        }
        if (i2 == 0) {
            return 0;
        }
        if (i2 == 1) {
            return 90;
        }
        if (i2 != 2) {
            return i2 != 3 ? 0 : 270;
        }
        return 180;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
     arg types: [boolean[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void} */
    public void g() {
        k();
        Arrays.fill(this.l, -1);
        Arrays.fill(this.f4669j, false);
    }

    public void h() {
        j();
    }

    /* access modifiers changed from: package-private */
    public void i() {
        synchronized (this) {
            if (this.I) {
                this.I = false;
                for (int i2 = 0; i2 < this.s.length; i2++) {
                    this.s[i2] = false;
                }
            }
            if (this.q) {
                this.q = false;
                for (int i3 = 0; i3 < this.r.length; i3++) {
                    this.r[i3] = false;
                }
            }
            if (this.J != null) {
                e.d.b.l lVar = this.J;
                int size = this.f4663d.size();
                for (int i4 = 0; i4 < size; i4++) {
                    d dVar = this.f4663d.get(i4);
                    this.M = dVar.f4673a;
                    int i5 = dVar.f4674b;
                    if (i5 == 0) {
                        lVar.c(dVar.f4675c);
                        this.q = true;
                        this.r[dVar.f4675c] = true;
                    } else if (i5 == 1) {
                        lVar.a(dVar.f4675c);
                    } else if (i5 == 2) {
                        lVar.a(dVar.f4676d);
                    }
                    this.f4660a.free(dVar);
                }
                int size2 = this.f4664e.size();
                for (int i6 = 0; i6 < size2; i6++) {
                    f fVar = this.f4664e.get(i6);
                    this.M = fVar.f4678a;
                    int i7 = fVar.f4679b;
                    if (i7 == 0) {
                        lVar.a(fVar.f4680c, fVar.f4681d, fVar.f4684g, fVar.f4683f);
                        this.I = true;
                        this.s[fVar.f4683f] = true;
                    } else if (i7 == 1) {
                        lVar.b(fVar.f4680c, fVar.f4681d, fVar.f4684g, fVar.f4683f);
                    } else if (i7 == 2) {
                        lVar.a(fVar.f4680c, fVar.f4681d, fVar.f4684g);
                    } else if (i7 == 3) {
                        lVar.b(fVar.f4682e);
                    } else if (i7 == 4) {
                        lVar.a(fVar.f4680c, fVar.f4681d);
                    }
                    this.f4661b.free(fVar);
                }
            } else {
                int size3 = this.f4664e.size();
                for (int i8 = 0; i8 < size3; i8++) {
                    f fVar2 = this.f4664e.get(i8);
                    if (fVar2.f4679b == 0) {
                        this.I = true;
                    }
                    this.f4661b.free(fVar2);
                }
                int size4 = this.f4663d.size();
                for (int i9 = 0; i9 < size4; i9++) {
                    this.f4660a.free(this.f4663d.get(i9));
                }
            }
            if (this.f4664e.isEmpty()) {
                for (int i10 = 0; i10 < this.f4667h.length; i10++) {
                    this.f4667h[0] = 0;
                    this.f4668i[0] = 0;
                }
            }
            this.f4663d.clear();
            this.f4664e.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public void j() {
        if (this.K.f4631h) {
            this.t = (SensorManager) this.z.getSystemService("sensor");
            if (this.t.getSensorList(1).isEmpty()) {
                this.u = false;
            } else {
                this.N = new e();
                this.u = this.t.registerListener(this.N, this.t.getSensorList(1).get(0), this.K.l);
            }
        } else {
            this.u = false;
        }
        if (this.K.f4632i) {
            this.t = (SensorManager) this.z.getSystemService("sensor");
            if (!this.t.getSensorList(4).isEmpty()) {
                this.O = new e();
                this.t.registerListener(this.O, this.t.getSensorList(4).get(0), this.K.l);
            }
        }
        this.E = false;
        if (this.K.f4634k) {
            if (this.t == null) {
                this.t = (SensorManager) this.z.getSystemService("sensor");
            }
            List<Sensor> sensorList = this.t.getSensorList(11);
            if (!sensorList.isEmpty()) {
                this.Q = new e();
                Iterator<Sensor> it = sensorList.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    Sensor next = it.next();
                    if (next.getVendor().equals("Google Inc.") && next.getVersion() == 3) {
                        this.E = this.t.registerListener(this.Q, next, this.K.l);
                        break;
                    }
                }
                if (!this.E) {
                    this.E = this.t.registerListener(this.Q, sensorList.get(0), this.K.l);
                }
            }
        }
        if (!this.K.f4633j || this.E) {
            this.D = false;
        } else {
            if (this.t == null) {
                this.t = (SensorManager) this.z.getSystemService("sensor");
            }
            Sensor defaultSensor = this.t.getDefaultSensor(2);
            if (defaultSensor != null) {
                this.D = this.u;
                if (this.D) {
                    this.P = new e();
                    this.D = this.t.registerListener(this.P, defaultSensor, this.K.l);
                }
            } else {
                this.D = false;
            }
        }
        g.f6800a.b("AndroidInput", "sensor listener setup");
    }

    /* access modifiers changed from: package-private */
    public void k() {
        SensorManager sensorManager = this.t;
        if (sensorManager != null) {
            SensorEventListener sensorEventListener = this.N;
            if (sensorEventListener != null) {
                sensorManager.unregisterListener(sensorEventListener);
                this.N = null;
            }
            SensorEventListener sensorEventListener2 = this.O;
            if (sensorEventListener2 != null) {
                this.t.unregisterListener(sensorEventListener2);
                this.O = null;
            }
            SensorEventListener sensorEventListener3 = this.Q;
            if (sensorEventListener3 != null) {
                this.t.unregisterListener(sensorEventListener3);
                this.Q = null;
            }
            SensorEventListener sensorEventListener4 = this.P;
            if (sensorEventListener4 != null) {
                this.t.unregisterListener(sensorEventListener4);
                this.P = null;
            }
            this.t = null;
        }
        g.f6800a.b("AndroidInput", "sensor listener tear down");
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        int size = this.f4662c.size();
        for (int i3 = 0; i3 < size; i3++) {
            if (this.f4662c.get(i3).onKey(view, i2, keyEvent)) {
                return true;
            }
        }
        if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() > 0) {
            return this.C.c(i2);
        }
        synchronized (this) {
            if (keyEvent.getKeyCode() == 0 && keyEvent.getAction() == 2) {
                String characters = keyEvent.getCharacters();
                for (int i4 = 0; i4 < characters.length(); i4++) {
                    d obtain = this.f4660a.obtain();
                    obtain.f4673a = System.nanoTime();
                    obtain.f4675c = 0;
                    obtain.f4676d = characters.charAt(i4);
                    obtain.f4674b = 2;
                    this.f4663d.add(obtain);
                }
                return false;
            }
            char unicodeChar = (char) keyEvent.getUnicodeChar();
            if (i2 == 67) {
                unicodeChar = 8;
            }
            if (keyEvent.getKeyCode() >= 0) {
                if (keyEvent.getKeyCode() < 260) {
                    int action = keyEvent.getAction();
                    if (action == 0) {
                        d obtain2 = this.f4660a.obtain();
                        obtain2.f4673a = System.nanoTime();
                        obtain2.f4676d = 0;
                        obtain2.f4675c = keyEvent.getKeyCode();
                        obtain2.f4674b = 0;
                        if (i2 == 4 && keyEvent.isAltPressed()) {
                            obtain2.f4675c = 255;
                            i2 = 255;
                        }
                        this.f4663d.add(obtain2);
                        if (!this.p[obtain2.f4675c]) {
                            this.o++;
                            this.p[obtain2.f4675c] = true;
                        }
                    } else if (action == 1) {
                        long nanoTime = System.nanoTime();
                        d obtain3 = this.f4660a.obtain();
                        obtain3.f4673a = nanoTime;
                        obtain3.f4676d = 0;
                        obtain3.f4675c = keyEvent.getKeyCode();
                        obtain3.f4674b = 1;
                        if (i2 == 4 && keyEvent.isAltPressed()) {
                            obtain3.f4675c = 255;
                            i2 = 255;
                        }
                        this.f4663d.add(obtain3);
                        d obtain4 = this.f4660a.obtain();
                        obtain4.f4673a = nanoTime;
                        obtain4.f4676d = unicodeChar;
                        obtain4.f4675c = 0;
                        obtain4.f4674b = 2;
                        this.f4663d.add(obtain4);
                        if (i2 == 255) {
                            if (this.p[255]) {
                                this.o--;
                                this.p[255] = false;
                            }
                        } else if (this.p[keyEvent.getKeyCode()]) {
                            this.o--;
                            this.p[keyEvent.getKeyCode()] = false;
                        }
                    }
                    this.y.c().f();
                    return this.C.c(i2);
                }
            }
            return false;
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.R && view != null) {
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            this.R = false;
        }
        this.A.a(motionEvent, this);
        int i2 = this.B;
        if (i2 != 0) {
            try {
                Thread.sleep((long) i2);
            } catch (InterruptedException unused) {
            }
        }
        return true;
    }

    public void a(e.d.b.l lVar) {
        synchronized (this) {
            this.J = lVar;
        }
    }

    public boolean b() {
        synchronized (this) {
            if (this.n) {
                for (int i2 = 0; i2 < 20; i2++) {
                    if (this.f4669j[i2]) {
                        return true;
                    }
                }
            }
            boolean z2 = this.f4669j[0];
            return z2;
        }
    }

    public int c(int i2) {
        int length = this.l.length;
        for (int i3 = 0; i3 < length; i3++) {
            if (this.l[i3] == i2) {
                return i3;
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i4 = 0; i4 < length; i4++) {
            sb.append(i4 + ":" + this.l[i4] + RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER);
        }
        e.d.b.a aVar = g.f6800a;
        aVar.b("AndroidInput", "Pointer ID lookup failed: " + i2 + ", " + sb.toString());
        return -1;
    }

    public void a(boolean z2) {
        a(4, z2);
    }

    public void a(int i2, boolean z2) {
        if (!z2) {
            this.C.d(i2);
        } else if (z2) {
            this.C.a(i2);
        }
    }

    private int[] a(int[] iArr) {
        int[] iArr2 = new int[(iArr.length + 2)];
        System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
        return iArr2;
    }

    public void b(boolean z2) {
        this.x.post(new c(z2));
    }

    private boolean[] a(boolean[] zArr) {
        boolean[] zArr2 = new boolean[(zArr.length + 2)];
        System.arraycopy(zArr, 0, zArr2, 0, zArr.length);
        return zArr2;
    }

    public long a() {
        return this.M;
    }
}
