package com.badlogic.gdx.backends.android;

import android.media.MediaPlayer;
import e.d.b.g;
import e.d.b.r.a;
import java.io.IOException;

/* compiled from: AndroidMusic */
public class q implements e.d.b.r.a, MediaPlayer.OnCompletionListener {

    /* renamed from: a  reason: collision with root package name */
    private final d f4687a;

    /* renamed from: b  reason: collision with root package name */
    private MediaPlayer f4688b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f4689c = true;

    /* renamed from: d  reason: collision with root package name */
    protected boolean f4690d = false;

    /* renamed from: e  reason: collision with root package name */
    private float f4691e = 1.0f;

    /* renamed from: f  reason: collision with root package name */
    protected a.C0168a f4692f;

    /* compiled from: AndroidMusic */
    class a implements Runnable {
        a() {
        }

        public void run() {
            q qVar = q.this;
            qVar.f4692f.a(qVar);
        }
    }

    q(d dVar, MediaPlayer mediaPlayer) {
        this.f4687a = dVar;
        this.f4688b = mediaPlayer;
        this.f4692f = null;
        this.f4688b.setOnCompletionListener(this);
    }

    public boolean a() {
        MediaPlayer mediaPlayer = this.f4688b;
        if (mediaPlayer == null) {
            return false;
        }
        try {
            return mediaPlayer.isPlaying();
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:15|16|17|2f|39) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        e.d.b.g.f6800a.b("AndroidMusic", "error while disposing AndroidMusic instance, non-fatal");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0027, code lost:
        r4.f4688b = null;
        r4.f4692f = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002f, code lost:
        monitor-enter(r4.f4687a.f4637c);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.f4687a.f4637c.remove(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x003d, code lost:
        r4.f4688b = null;
        r4.f4692f = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0045, code lost:
        monitor-enter(r4.f4687a.f4637c);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r4.f4687a.f4637c.remove(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004e, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x001e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void dispose() {
        /*
            r4 = this;
            android.media.MediaPlayer r0 = r4.f4688b
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            r1 = 0
            r0.release()     // Catch:{ all -> 0x001e }
            r4.f4688b = r1
            r4.f4692f = r1
            com.badlogic.gdx.backends.android.d r0 = r4.f4687a
            java.util.List<com.badlogic.gdx.backends.android.q> r0 = r0.f4637c
            monitor-enter(r0)
            com.badlogic.gdx.backends.android.d r1 = r4.f4687a     // Catch:{ all -> 0x001b }
            java.util.List<com.badlogic.gdx.backends.android.q> r1 = r1.f4637c     // Catch:{ all -> 0x001b }
            r1.remove(r4)     // Catch:{ all -> 0x001b }
            monitor-exit(r0)     // Catch:{ all -> 0x001b }
            goto L_0x0038
        L_0x001b:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001b }
            throw r1
        L_0x001e:
            e.d.b.a r0 = e.d.b.g.f6800a     // Catch:{ all -> 0x003c }
            java.lang.String r2 = "AndroidMusic"
            java.lang.String r3 = "error while disposing AndroidMusic instance, non-fatal"
            r0.b(r2, r3)     // Catch:{ all -> 0x003c }
            r4.f4688b = r1
            r4.f4692f = r1
            com.badlogic.gdx.backends.android.d r0 = r4.f4687a
            java.util.List<com.badlogic.gdx.backends.android.q> r0 = r0.f4637c
            monitor-enter(r0)
            com.badlogic.gdx.backends.android.d r1 = r4.f4687a     // Catch:{ all -> 0x0039 }
            java.util.List<com.badlogic.gdx.backends.android.q> r1 = r1.f4637c     // Catch:{ all -> 0x0039 }
            r1.remove(r4)     // Catch:{ all -> 0x0039 }
            monitor-exit(r0)     // Catch:{ all -> 0x0039 }
        L_0x0038:
            return
        L_0x0039:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0039 }
            throw r1
        L_0x003c:
            r0 = move-exception
            r4.f4688b = r1
            r4.f4692f = r1
            com.badlogic.gdx.backends.android.d r1 = r4.f4687a
            java.util.List<com.badlogic.gdx.backends.android.q> r1 = r1.f4637c
            monitor-enter(r1)
            com.badlogic.gdx.backends.android.d r2 = r4.f4687a     // Catch:{ all -> 0x004f }
            java.util.List<com.badlogic.gdx.backends.android.q> r2 = r2.f4637c     // Catch:{ all -> 0x004f }
            r2.remove(r4)     // Catch:{ all -> 0x004f }
            monitor-exit(r1)     // Catch:{ all -> 0x004f }
            throw r0
        L_0x004f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x004f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.q.dispose():void");
    }

    public float getVolume() {
        return this.f4691e;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        if (this.f4692f != null) {
            g.f6800a.a(new a());
        }
    }

    public void pause() {
        MediaPlayer mediaPlayer = this.f4688b;
        if (mediaPlayer != null) {
            try {
                if (mediaPlayer.isPlaying()) {
                    this.f4688b.pause();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            this.f4690d = false;
        }
    }

    public void play() {
        MediaPlayer mediaPlayer = this.f4688b;
        if (mediaPlayer != null) {
            try {
                if (!mediaPlayer.isPlaying()) {
                    try {
                        if (!this.f4689c) {
                            this.f4688b.prepare();
                            this.f4689c = true;
                        }
                        this.f4688b.start();
                    } catch (IllegalStateException e2) {
                        e2.printStackTrace();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        }
    }

    public void setVolume(float f2) {
        MediaPlayer mediaPlayer = this.f4688b;
        if (mediaPlayer != null) {
            mediaPlayer.setVolume(f2, f2);
            this.f4691e = f2;
        }
    }

    public void stop() {
        MediaPlayer mediaPlayer = this.f4688b;
        if (mediaPlayer != null) {
            if (this.f4689c) {
                mediaPlayer.seekTo(0);
            }
            this.f4688b.stop();
            this.f4689c = false;
        }
    }

    public void a(boolean z) {
        MediaPlayer mediaPlayer = this.f4688b;
        if (mediaPlayer != null) {
            mediaPlayer.setLooping(z);
        }
    }

    public void a(a.C0168a aVar) {
        this.f4692f = aVar;
    }
}
