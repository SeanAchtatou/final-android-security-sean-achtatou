package com.badlogic.gdx.backends.android;

import android.opengl.GLES20;
import com.google.android.gms.ads.AdRequest;
import e.d.b.t.g;
import java.nio.Buffer;
import java.nio.IntBuffer;

/* compiled from: AndroidGL20 */
public class i implements g {

    /* renamed from: a  reason: collision with root package name */
    private int[] f4645a = new int[1];

    /* renamed from: b  reason: collision with root package name */
    private int[] f4646b = new int[1];

    /* renamed from: c  reason: collision with root package name */
    private int[] f4647c = new int[1];

    /* renamed from: d  reason: collision with root package name */
    private byte[] f4648d = new byte[AdRequest.MAX_CONTENT_URL_LENGTH];

    public void a(int i2, int i3) {
        GLES20.glBindRenderbuffer(i2, i3);
    }

    public void b(int i2, int i3) {
        GLES20.glAttachShader(i2, i3);
    }

    public void c(int i2) {
        GLES20.glActiveTexture(i2);
    }

    public void d(int i2, int i3, int i4, int i5) {
        GLES20.glDrawElements(i2, i3, i4, i5);
    }

    public void e(int i2, int i3) {
        GLES20.glBindBuffer(i2, i3);
    }

    public int f() {
        GLES20.glGenFramebuffers(1, this.f4645a, 0);
        return this.f4645a[0];
    }

    public void g(int i2, int i3) {
        GLES20.glBlendFunc(i2, i3);
    }

    public void h(int i2, int i3) {
        GLES20.glBindTexture(i2, i3);
    }

    public void j(int i2) {
        GLES20.glDeleteShader(i2);
    }

    public void k(int i2) {
        GLES20.glDisable(i2);
    }

    public void l(int i2) {
        GLES20.glClear(i2);
    }

    public void m(int i2) {
        int[] iArr = this.f4645a;
        iArr[0] = i2;
        GLES20.glDeleteFramebuffers(1, iArr, 0);
    }

    public void n(int i2) {
        GLES20.glGenerateMipmap(i2);
    }

    public void o(int i2) {
        GLES20.glLinkProgram(i2);
    }

    public void p(int i2) {
        GLES20.glDisableVertexAttribArray(i2);
    }

    public void q(int i2) {
        GLES20.glCompileShader(i2);
    }

    public int r(int i2) {
        return GLES20.glCheckFramebufferStatus(i2);
    }

    public void s(int i2) {
        GLES20.glEnableVertexAttribArray(i2);
    }

    public String t(int i2) {
        return GLES20.glGetShaderInfoLog(i2);
    }

    public void u(int i2) {
        int[] iArr = this.f4645a;
        iArr[0] = i2;
        GLES20.glDeleteTextures(1, iArr, 0);
    }

    public int v(int i2) {
        return GLES20.glCreateShader(i2);
    }

    public void a(int i2, int i3, Buffer buffer, int i4) {
        GLES20.glBufferData(i2, i3, buffer, i4);
    }

    public void b(int i2) {
        GLES20.glDeleteProgram(i2);
    }

    public void c(int i2, int i3) {
        GLES20.glBindFramebuffer(i2, i3);
    }

    public int d() {
        GLES20.glGenRenderbuffers(1, this.f4645a, 0);
        return this.f4645a[0];
    }

    public void e(int i2, int i3, int i4, int i5) {
        GLES20.glBlendFuncSeparate(i2, i3, i4, i5);
    }

    public void g(int i2) {
        int[] iArr = this.f4645a;
        iArr[0] = i2;
        GLES20.glDeleteRenderbuffers(1, iArr, 0);
    }

    public String h(int i2) {
        return GLES20.glGetProgramInfoLog(i2);
    }

    public void a(int i2, int i3, int i4, Buffer buffer) {
        GLES20.glBufferSubData(i2, i3, i4, buffer);
    }

    public void b(int i2, int i3, int i4) {
        GLES20.glDrawArrays(i2, i3, i4);
    }

    public int c() {
        return GLES20.glCreateProgram();
    }

    public void e(int i2) {
        int[] iArr = this.f4645a;
        iArr[0] = i2;
        GLES20.glDeleteBuffers(1, iArr, 0);
    }

    public void f(int i2, int i3) {
        GLES20.glPixelStorei(i2, i3);
    }

    public void a(float f2, float f3, float f4, float f5) {
        GLES20.glClearColor(f2, f3, f4, f5);
    }

    public void b(int i2, int i3, int i4, Buffer buffer) {
        GLES20.glDrawElements(i2, i3, i4, buffer);
    }

    public int c(int i2, String str) {
        return GLES20.glGetAttribLocation(i2, str);
    }

    public String d(int i2) {
        return GLES20.glGetString(i2);
    }

    public void f(int i2) {
        GLES20.glUseProgram(i2);
    }

    public void g(int i2, int i3, int i4, int i5) {
        GLES20.glScissor(i2, i3, i4, i5);
    }

    public void a(int i2, int i3, int i4, int i5, int i6, int i7, int i8, Buffer buffer) {
        GLES20.glCompressedTexImage2D(i2, i3, i4, i5, i6, i7, i8, buffer);
    }

    public int b() {
        GLES20.glGenTextures(1, this.f4645a, 0);
        return this.f4645a[0];
    }

    public void c(int i2, IntBuffer intBuffer) {
        GLES20.glGetIntegerv(i2, intBuffer);
    }

    public void d(int i2, int i3) {
        GLES20.glUniform1i(i2, i3);
    }

    public int e() {
        return GLES20.glGetError();
    }

    public void f(int i2, int i3, int i4, int i5) {
        GLES20.glViewport(i2, i3, i4, i5);
    }

    public void a(boolean z) {
        GLES20.glDepthMask(z);
    }

    public void c(int i2, int i3, int i4, int i5) {
        GLES20.glRenderbufferStorage(i2, i3, i4, i5);
    }

    public void a(int i2) {
        GLES20.glEnable(i2);
    }

    public String b(int i2, int i3, IntBuffer intBuffer, Buffer buffer) {
        this.f4645a[0] = 0;
        this.f4646b[0] = intBuffer.get(0);
        this.f4647c[0] = ((IntBuffer) buffer).get(0);
        byte[] bArr = this.f4648d;
        GLES20.glGetActiveAttrib(i2, i3, bArr.length, this.f4645a, 0, this.f4646b, 0, this.f4647c, 0, bArr, 0);
        return new String(this.f4648d, 0, this.f4645a[0]);
    }

    public void a(int i2, int i3, int i4, int i5) {
        GLES20.glFramebufferRenderbuffer(i2, i3, i4, i5);
    }

    public void a(int i2, int i3, int i4, int i5, int i6) {
        GLES20.glFramebufferTexture2D(i2, i3, i4, i5, i6);
    }

    public int a() {
        GLES20.glGenBuffers(1, this.f4645a, 0);
        return this.f4645a[0];
    }

    public String a(int i2, int i3, IntBuffer intBuffer, Buffer buffer) {
        this.f4645a[0] = 0;
        this.f4646b[0] = intBuffer.get(0);
        this.f4647c[0] = ((IntBuffer) buffer).get(0);
        byte[] bArr = this.f4648d;
        GLES20.glGetActiveUniform(i2, i3, bArr.length, this.f4645a, 0, this.f4646b, 0, this.f4647c, 0, bArr, 0);
        return new String(this.f4648d, 0, this.f4645a[0]);
    }

    public void b(int i2, int i3, IntBuffer intBuffer) {
        GLES20.glGetShaderiv(i2, i3, intBuffer);
    }

    public int b(int i2, String str) {
        return GLES20.glGetUniformLocation(i2, str);
    }

    public void b(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, Buffer buffer) {
        GLES20.glTexImage2D(i2, i3, i4, i5, i6, i7, i8, i9, buffer);
    }

    public void a(int i2, int i3, IntBuffer intBuffer) {
        GLES20.glGetProgramiv(i2, i3, intBuffer);
    }

    public void a(int i2, String str) {
        GLES20.glShaderSource(i2, str);
    }

    public void a(int i2, int i3, int i4) {
        GLES20.glTexParameteri(i2, i3, i4);
    }

    public void a(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, Buffer buffer) {
        GLES20.glTexSubImage2D(i2, i3, i4, i5, i6, i7, i8, i9, buffer);
    }

    public void a(int i2, float f2) {
        GLES20.glUniform1f(i2, f2);
    }

    public void a(int i2, float f2, float f3) {
        GLES20.glUniform2f(i2, f2, f3);
    }

    public void a(int i2, int i3, float[] fArr, int i4) {
        GLES20.glUniform3fv(i2, i3, fArr, i4);
    }

    public void a(int i2, float f2, float f3, float f4, float f5) {
        GLES20.glUniform4f(i2, f2, f3, f4, f5);
    }

    public void a(int i2, int i3, boolean z, float[] fArr, int i4) {
        GLES20.glUniformMatrix4fv(i2, i3, z, fArr, i4);
    }

    public void a(int i2, int i3, int i4, boolean z, int i5, Buffer buffer) {
        GLES20.glVertexAttribPointer(i2, i3, i4, z, i5, buffer);
    }

    public void a(int i2, int i3, int i4, boolean z, int i5, int i6) {
        GLES20.glVertexAttribPointer(i2, i3, i4, z, i5, i6);
    }
}
