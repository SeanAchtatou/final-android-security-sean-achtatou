package com.badlogic.gdx.backends.android;

import android.media.AudioManager;
import android.media.SoundPool;
import com.badlogic.gdx.utils.q;
import com.esotericsoftware.spine.Animation;
import e.d.b.r.b;

/* compiled from: AndroidSound */
final class u implements b {

    /* renamed from: a  reason: collision with root package name */
    final SoundPool f4697a;

    /* renamed from: b  reason: collision with root package name */
    final int f4698b;

    /* renamed from: c  reason: collision with root package name */
    final q f4699c = new q(8);

    u(SoundPool soundPool, AudioManager audioManager, int i2) {
        this.f4697a = soundPool;
        this.f4698b = i2;
    }

    public void a(long j2) {
        this.f4697a.stop((int) j2);
    }

    public long b(float f2, float f3, float f4) {
        float f5;
        float f6;
        q qVar = this.f4699c;
        if (qVar.f5559b == 8) {
            qVar.b();
        }
        if (f4 < Animation.CurveTimeline.LINEAR) {
            f6 = f2;
            f5 = f2 * (1.0f - Math.abs(f4));
        } else if (f4 > Animation.CurveTimeline.LINEAR) {
            f5 = f2;
            f6 = f2 * (1.0f - Math.abs(f4));
        } else {
            f6 = f2;
            f5 = f6;
        }
        int play = this.f4697a.play(this.f4698b, f6, f5, 1, -1, f3);
        if (play == 0) {
            return -1;
        }
        this.f4699c.a(0, play);
        return (long) play;
    }

    public void dispose() {
        this.f4697a.unload(this.f4698b);
    }

    public void pause() {
        this.f4697a.autoPause();
    }

    public void resume() {
        this.f4697a.autoResume();
    }

    public void stop() {
        int i2 = this.f4699c.f5559b;
        for (int i3 = 0; i3 < i2; i3++) {
            this.f4697a.stop(this.f4699c.c(i3));
        }
    }

    public void a(long j2, float f2) {
        this.f4697a.setVolume((int) j2, f2, f2);
    }

    public long a(float f2, float f3, float f4) {
        float f5;
        float f6;
        q qVar = this.f4699c;
        if (qVar.f5559b == 8) {
            qVar.b();
        }
        if (f4 < Animation.CurveTimeline.LINEAR) {
            f6 = f2;
            f5 = f2 * (1.0f - Math.abs(f4));
        } else if (f4 > Animation.CurveTimeline.LINEAR) {
            f5 = f2;
            f6 = f2 * (1.0f - Math.abs(f4));
        } else {
            f6 = f2;
            f5 = f6;
        }
        int play = this.f4697a.play(this.f4698b, f6, f5, 1, 0, f3);
        if (play == 0) {
            return -1;
        }
        this.f4699c.a(0, play);
        return (long) play;
    }
}
