package com.badlogic.gdx.backends.android;

import android.opengl.GLSurfaceView;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import com.badlogic.gdx.backends.android.z.b;
import com.badlogic.gdx.backends.android.z.c;
import com.badlogic.gdx.backends.android.z.d;
import com.badlogic.gdx.backends.android.z.e;
import com.badlogic.gdx.graphics.glutils.f;
import com.badlogic.gdx.math.s;
import com.badlogic.gdx.utils.o;
import com.badlogic.gdx.utils.o0;
import com.esotericsoftware.spine.Animation;
import com.tapjoy.TJAdUnitConstants;
import e.d.b.a;
import e.d.b.h;
import e.d.b.m;
import e.d.b.t.g;
import e.d.b.t.j;
import e.d.b.t.n;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

/* compiled from: AndroidGraphics */
public class k implements h, GLSurfaceView.Renderer {
    static volatile boolean y = false;

    /* renamed from: a  reason: collision with root package name */
    final View f4649a;

    /* renamed from: b  reason: collision with root package name */
    int f4650b;

    /* renamed from: c  reason: collision with root package name */
    int f4651c;

    /* renamed from: d  reason: collision with root package name */
    a f4652d;

    /* renamed from: e  reason: collision with root package name */
    g f4653e;

    /* renamed from: f  reason: collision with root package name */
    e.d.b.t.h f4654f;

    /* renamed from: g  reason: collision with root package name */
    f f4655g;

    /* renamed from: h  reason: collision with root package name */
    String f4656h;

    /* renamed from: i  reason: collision with root package name */
    protected long f4657i;

    /* renamed from: j  reason: collision with root package name */
    protected float f4658j;

    /* renamed from: k  reason: collision with root package name */
    protected long f4659k;
    protected long l;
    protected int m;
    protected int n;
    protected s o;
    volatile boolean p;
    volatile boolean q;
    volatile boolean r;
    volatile boolean s;
    volatile boolean t;
    protected final b u;
    private boolean v;
    int[] w;
    Object x;

    /* compiled from: AndroidGraphics */
    private class a extends h.b {
        protected a(k kVar, int i2, int i3, int i4, int i5) {
            super(i2, i3, i4, i5);
        }
    }

    public k(a aVar, b bVar, com.badlogic.gdx.backends.android.z.f fVar) {
        this(aVar, bVar, fVar, true);
    }

    /* access modifiers changed from: protected */
    public View a(a aVar, com.badlogic.gdx.backends.android.z.f fVar) {
        com.badlogic.gdx.backends.android.z.f fVar2 = fVar;
        if (h()) {
            GLSurfaceView.EGLConfigChooser k2 = k();
            if (Build.VERSION.SDK_INT > 10 || !this.u.t) {
                b bVar = new b(aVar.getContext(), fVar2, this.u.s ? 3 : 2);
                if (k2 != null) {
                    bVar.setEGLConfigChooser(k2);
                } else {
                    b bVar2 = this.u;
                    bVar.setEGLConfigChooser(bVar2.f4624a, bVar2.f4625b, bVar2.f4626c, bVar2.f4627d, bVar2.f4628e, bVar2.f4629f);
                }
                bVar.setRenderer(this);
                return bVar;
            }
            c cVar = new c(aVar.getContext(), fVar2);
            if (k2 != null) {
                cVar.setEGLConfigChooser(k2);
            } else {
                b bVar3 = this.u;
                cVar.a(bVar3.f4624a, bVar3.f4625b, bVar3.f4626c, bVar3.f4627d, bVar3.f4628e, bVar3.f4629f);
            }
            cVar.setRenderer(this);
            return cVar;
        }
        throw new o("Libgdx requires OpenGL ES 2.0");
    }

    public int b() {
        return this.f4650b;
    }

    public int c() {
        return this.n;
    }

    public float d() {
        return this.o.b() == Animation.CurveTimeline.LINEAR ? this.f4658j : this.o.b();
    }

    public int e() {
        return this.f4651c;
    }

    public void f() {
        View view = this.f4649a;
        if (view != null) {
            if (view instanceof d) {
                ((d) view).c();
            }
            View view2 = this.f4649a;
            if (view2 instanceof GLSurfaceView) {
                ((GLSurfaceView) view2).requestRender();
            }
        }
    }

    public h.b g() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.f4652d.h().getDefaultDisplay().getMetrics(displayMetrics);
        return new a(this, displayMetrics.widthPixels, displayMetrics.heightPixels, 0, 0);
    }

    public int getHeight() {
        return this.f4651c;
    }

    public int getWidth() {
        return this.f4650b;
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        EGL10 egl10 = (EGL10) EGLContext.getEGL();
        EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        egl10.eglInitialize(eglGetDisplay, new int[2]);
        int[] iArr = new int[1];
        egl10.eglChooseConfig(eglGetDisplay, new int[]{12324, 4, 12323, 4, 12322, 4, 12352, 4, 12344}, new EGLConfig[10], 10, iArr);
        egl10.eglTerminate(eglGetDisplay);
        if (iArr[0] > 0) {
            return true;
        }
        return false;
    }

    public void i() {
        j.a(this.f4652d);
        n.a(this.f4652d);
        e.d.b.t.d.a(this.f4652d);
        e.d.b.t.o.a(this.f4652d);
        com.badlogic.gdx.graphics.glutils.s.a(this.f4652d);
        com.badlogic.gdx.graphics.glutils.d.a(this.f4652d);
        n();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:7|8|9|10|19|16|5) */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0009, code lost:
        continue;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0013 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void j() {
        /*
            r4 = this;
            java.lang.Object r0 = r4.x
            monitor-enter(r0)
            r1 = 0
            r4.q = r1     // Catch:{ all -> 0x001f }
            r1 = 1
            r4.t = r1     // Catch:{ all -> 0x001f }
        L_0x0009:
            boolean r1 = r4.t     // Catch:{ all -> 0x001f }
            if (r1 == 0) goto L_0x001d
            java.lang.Object r1 = r4.x     // Catch:{ InterruptedException -> 0x0013 }
            r1.wait()     // Catch:{ InterruptedException -> 0x0013 }
            goto L_0x0009
        L_0x0013:
            e.d.b.a r1 = e.d.b.g.f6800a     // Catch:{ all -> 0x001f }
            java.lang.String r2 = "AndroidGraphics"
            java.lang.String r3 = "waiting for destroy synchronization failed!"
            r1.b(r2, r3)     // Catch:{ all -> 0x001f }
            goto L_0x0009
        L_0x001d:
            monitor-exit(r0)     // Catch:{ all -> 0x001f }
            return
        L_0x001f:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001f }
            goto L_0x0023
        L_0x0022:
            throw r1
        L_0x0023:
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.k.j():void");
    }

    /* access modifiers changed from: protected */
    public GLSurfaceView.EGLConfigChooser k() {
        b bVar = this.u;
        return new e(bVar.f4624a, bVar.f4625b, bVar.f4626c, bVar.f4627d, bVar.f4628e, bVar.f4629f, bVar.f4630g);
    }

    public View l() {
        return this.f4649a;
    }

    public boolean m() {
        return this.v;
    }

    /* access modifiers changed from: protected */
    public void n() {
        e.d.b.g.f6800a.b("AndroidGraphics", j.m());
        e.d.b.g.f6800a.b("AndroidGraphics", n.u());
        e.d.b.g.f6800a.b("AndroidGraphics", e.d.b.t.d.s());
        e.d.b.g.f6800a.b("AndroidGraphics", com.badlogic.gdx.graphics.glutils.s.p());
        e.d.b.g.f6800a.b("AndroidGraphics", com.badlogic.gdx.graphics.glutils.d.p());
    }

    public void o() {
        View view = this.f4649a;
        if (view != null) {
            if (view instanceof d) {
                ((d) view).a();
            }
            View view2 = this.f4649a;
            if (view2 instanceof GLSurfaceView) {
                ((GLSurfaceView) view2).onPause();
            }
        }
    }

    public void onDrawFrame(GL10 gl10) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        long nanoTime = System.nanoTime();
        this.f4658j = ((float) (nanoTime - this.f4657i)) / 1.0E9f;
        this.f4657i = nanoTime;
        if (!this.s) {
            this.o.a(this.f4658j);
        } else {
            this.f4658j = Animation.CurveTimeline.LINEAR;
        }
        synchronized (this.x) {
            z = this.q;
            z2 = this.r;
            z3 = this.t;
            z4 = this.s;
            if (this.s) {
                this.s = false;
            }
            if (this.r) {
                this.r = false;
                this.x.notifyAll();
            }
            if (this.t) {
                this.t = false;
                this.x.notifyAll();
            }
        }
        if (z4) {
            o0<m> i2 = this.f4652d.i();
            synchronized (i2) {
                m[] mVarArr = (m[]) i2.e();
                int i3 = i2.f5374b;
                for (int i4 = 0; i4 < i3; i4++) {
                    mVarArr[i4].resume();
                }
                i2.f();
            }
            this.f4652d.e().resume();
            e.d.b.g.f6800a.b("AndroidGraphics", "resumed");
        }
        if (z) {
            synchronized (this.f4652d.f()) {
                this.f4652d.d().clear();
                this.f4652d.d().a(this.f4652d.f());
                this.f4652d.f().clear();
            }
            for (int i5 = 0; i5 < this.f4652d.d().f5374b; i5++) {
                try {
                    this.f4652d.d().get(i5).run();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
            this.f4652d.b().i();
            this.l++;
            this.f4652d.e().a();
        }
        if (z2) {
            o0<m> i6 = this.f4652d.i();
            synchronized (i6) {
                m[] mVarArr2 = (m[]) i6.e();
                int i7 = i6.f5374b;
                for (int i8 = 0; i8 < i7; i8++) {
                    mVarArr2[i8].pause();
                }
            }
            this.f4652d.e().pause();
            e.d.b.g.f6800a.b("AndroidGraphics", TJAdUnitConstants.String.VIDEO_PAUSED);
        }
        if (z3) {
            o0<m> i9 = this.f4652d.i();
            synchronized (i9) {
                m[] mVarArr3 = (m[]) i9.e();
                int i10 = i9.f5374b;
                for (int i11 = 0; i11 < i10; i11++) {
                    mVarArr3[i11].dispose();
                }
            }
            this.f4652d.e().dispose();
            e.d.b.g.f6800a.b("AndroidGraphics", "destroyed");
        }
        if (nanoTime - this.f4659k > 1000000000) {
            this.n = this.m;
            this.m = 0;
            this.f4659k = nanoTime;
        }
        this.m++;
    }

    public void onSurfaceChanged(GL10 gl10, int i2, int i3) {
        this.f4650b = i2;
        this.f4651c = i3;
        t();
        gl10.glViewport(0, 0, this.f4650b, this.f4651c);
        if (!this.p) {
            this.f4652d.e().create();
            this.p = true;
            synchronized (this) {
                this.q = true;
            }
        }
        this.f4652d.e().a(i2, i3);
    }

    public void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
        ((EGL10) EGLContext.getEGL()).eglGetCurrentContext();
        a(gl10);
        a(eGLConfig);
        t();
        j.b(this.f4652d);
        n.b(this.f4652d);
        e.d.b.t.d.b(this.f4652d);
        e.d.b.t.o.b(this.f4652d);
        com.badlogic.gdx.graphics.glutils.s.b(this.f4652d);
        com.badlogic.gdx.graphics.glutils.d.b(this.f4652d);
        n();
        Display defaultDisplay = this.f4652d.h().getDefaultDisplay();
        this.f4650b = defaultDisplay.getWidth();
        this.f4651c = defaultDisplay.getHeight();
        this.o = new s(5);
        this.f4657i = System.nanoTime();
        gl10.glViewport(0, 0, this.f4650b, this.f4651c);
    }

    public void p() {
        View view = this.f4649a;
        if (view != null) {
            if (view instanceof d) {
                ((d) view).b();
            }
            View view2 = this.f4649a;
            if (view2 instanceof GLSurfaceView) {
                ((GLSurfaceView) view2).onResume();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:14|15|25) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        e.d.b.g.f6800a.b("AndroidGraphics", "waiting for pause synchronization failed!");
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x002f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void q() {
        /*
            r4 = this;
            java.lang.Object r0 = r4.x
            monitor-enter(r0)
            boolean r1 = r4.q     // Catch:{ all -> 0x003b }
            if (r1 != 0) goto L_0x0009
            monitor-exit(r0)     // Catch:{ all -> 0x003b }
            return
        L_0x0009:
            r1 = 0
            r4.q = r1     // Catch:{ all -> 0x003b }
            r1 = 1
            r4.r = r1     // Catch:{ all -> 0x003b }
        L_0x000f:
            boolean r1 = r4.r     // Catch:{ all -> 0x003b }
            if (r1 == 0) goto L_0x0039
            java.lang.Object r1 = r4.x     // Catch:{ InterruptedException -> 0x002f }
            r2 = 4000(0xfa0, double:1.9763E-320)
            r1.wait(r2)     // Catch:{ InterruptedException -> 0x002f }
            boolean r1 = r4.r     // Catch:{ InterruptedException -> 0x002f }
            if (r1 == 0) goto L_0x000f
            e.d.b.a r1 = e.d.b.g.f6800a     // Catch:{ InterruptedException -> 0x002f }
            java.lang.String r2 = "AndroidGraphics"
            java.lang.String r3 = "waiting for pause synchronization took too long; assuming deadlock and killing"
            r1.c(r2, r3)     // Catch:{ InterruptedException -> 0x002f }
            int r1 = android.os.Process.myPid()     // Catch:{ InterruptedException -> 0x002f }
            android.os.Process.killProcess(r1)     // Catch:{ InterruptedException -> 0x002f }
            goto L_0x000f
        L_0x002f:
            e.d.b.a r1 = e.d.b.g.f6800a     // Catch:{ all -> 0x003b }
            java.lang.String r2 = "AndroidGraphics"
            java.lang.String r3 = "waiting for pause synchronization failed!"
            r1.b(r2, r3)     // Catch:{ all -> 0x003b }
            goto L_0x000f
        L_0x0039:
            monitor-exit(r0)     // Catch:{ all -> 0x003b }
            return
        L_0x003b:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003b }
            goto L_0x003f
        L_0x003e:
            throw r1
        L_0x003f:
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.k.q():void");
    }

    /* access modifiers changed from: protected */
    public void r() {
        if ((Build.VERSION.SDK_INT >= 11 && (this.f4649a instanceof b)) || (this.f4649a instanceof c)) {
            try {
                this.f4649a.getClass().getMethod("setPreserveEGLContextOnPause", Boolean.TYPE).invoke(this.f4649a, true);
            } catch (Exception unused) {
                e.d.b.g.f6800a.b("AndroidGraphics", "Method GLSurfaceView.setPreserveEGLContextOnPause not found");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void s() {
        synchronized (this.x) {
            this.q = true;
            this.s = true;
        }
    }

    /* access modifiers changed from: protected */
    public void t() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.f4652d.h().getDefaultDisplay().getMetrics(displayMetrics);
        float f2 = displayMetrics.xdpi;
        float f3 = displayMetrics.ydpi;
        float f4 = displayMetrics.density;
    }

    public k(a aVar, b bVar, com.badlogic.gdx.backends.android.z.f fVar, boolean z) {
        this.f4657i = System.nanoTime();
        this.f4658j = Animation.CurveTimeline.LINEAR;
        this.f4659k = System.nanoTime();
        this.l = -1;
        this.m = 0;
        this.o = new s(5);
        this.p = false;
        this.q = false;
        this.r = false;
        this.s = false;
        this.t = false;
        new h.a(5, 6, 5, 0, 16, 0, 0, false);
        this.v = true;
        this.w = new int[1];
        this.x = new Object();
        this.u = bVar;
        this.f4652d = aVar;
        this.f4649a = a(aVar, fVar);
        r();
        if (z) {
            this.f4649a.setFocusable(true);
            this.f4649a.setFocusableInTouchMode(true);
        }
    }

    public boolean a() {
        return this.f4654f != null;
    }

    /* access modifiers changed from: protected */
    public void a(GL10 gl10) {
        this.f4655g = new f(a.C0167a.Android, gl10.glGetString(7938), gl10.glGetString(7936), gl10.glGetString(7937));
        if (!this.u.s || this.f4655g.a() <= 2) {
            if (this.f4653e == null) {
                this.f4653e = new i();
                g gVar = this.f4653e;
                e.d.b.g.f6806g = gVar;
                e.d.b.g.f6807h = gVar;
            } else {
                return;
            }
        } else if (this.f4654f == null) {
            j jVar = new j();
            this.f4654f = jVar;
            this.f4653e = jVar;
            e.d.b.t.h hVar = this.f4654f;
            e.d.b.g.f6806g = hVar;
            e.d.b.g.f6807h = hVar;
            e.d.b.g.f6808i = hVar;
        } else {
            return;
        }
        e.d.b.a aVar = e.d.b.g.f6800a;
        aVar.b("AndroidGraphics", "OGL renderer: " + gl10.glGetString(7937));
        e.d.b.a aVar2 = e.d.b.g.f6800a;
        aVar2.b("AndroidGraphics", "OGL vendor: " + gl10.glGetString(7936));
        e.d.b.a aVar3 = e.d.b.g.f6800a;
        aVar3.b("AndroidGraphics", "OGL version: " + gl10.glGetString(7938));
        e.d.b.a aVar4 = e.d.b.g.f6800a;
        aVar4.b("AndroidGraphics", "OGL extensions: " + gl10.glGetString(7939));
    }

    /* access modifiers changed from: protected */
    public void a(EGLConfig eGLConfig) {
        EGL10 egl10 = (EGL10) EGLContext.getEGL();
        EGL10 egl102 = egl10;
        EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        EGLConfig eGLConfig2 = eGLConfig;
        int a2 = a(egl102, eglGetDisplay, eGLConfig2, 12324, 0);
        int a3 = a(egl102, eglGetDisplay, eGLConfig2, 12323, 0);
        int a4 = a(egl102, eglGetDisplay, eGLConfig2, 12322, 0);
        int a5 = a(egl102, eglGetDisplay, eGLConfig2, 12321, 0);
        int a6 = a(egl102, eglGetDisplay, eGLConfig2, 12325, 0);
        int a7 = a(egl102, eglGetDisplay, eGLConfig2, 12326, 0);
        int max = Math.max(a(egl102, eglGetDisplay, eGLConfig2, 12337, 0), a(egl102, eglGetDisplay, eGLConfig2, 12513, 0));
        boolean z = a(egl102, eglGetDisplay, eGLConfig2, 12513, 0) != 0;
        e.d.b.a aVar = e.d.b.g.f6800a;
        aVar.b("AndroidGraphics", "framebuffer: (" + a2 + ", " + a3 + ", " + a4 + ", " + a5 + ")");
        e.d.b.a aVar2 = e.d.b.g.f6800a;
        StringBuilder sb = new StringBuilder();
        sb.append("depthbuffer: (");
        sb.append(a6);
        sb.append(")");
        aVar2.b("AndroidGraphics", sb.toString());
        e.d.b.a aVar3 = e.d.b.g.f6800a;
        aVar3.b("AndroidGraphics", "stencilbuffer: (" + a7 + ")");
        e.d.b.a aVar4 = e.d.b.g.f6800a;
        aVar4.b("AndroidGraphics", "samples: (" + max + ")");
        e.d.b.a aVar5 = e.d.b.g.f6800a;
        aVar5.b("AndroidGraphics", "coverage sampling: (" + z + ")");
        new h.a(a2, a3, a4, a5, a6, a7, max, z);
    }

    private int a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i2, int i3) {
        return egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i2, this.w) ? this.w[0] : i3;
    }

    public boolean a(String str) {
        if (this.f4656h == null) {
            this.f4656h = e.d.b.g.f6806g.d(7939);
        }
        return this.f4656h.contains(str);
    }

    public void a(boolean z) {
        if (this.f4649a != null) {
            this.v = y || z;
            boolean z2 = this.v;
            View view = this.f4649a;
            if (view instanceof d) {
                ((d) view).setRenderMode(z2);
            }
            View view2 = this.f4649a;
            if (view2 instanceof GLSurfaceView) {
                ((GLSurfaceView) view2).setRenderMode(z2 ? 1 : 0);
            }
            this.o.a();
        }
    }
}
