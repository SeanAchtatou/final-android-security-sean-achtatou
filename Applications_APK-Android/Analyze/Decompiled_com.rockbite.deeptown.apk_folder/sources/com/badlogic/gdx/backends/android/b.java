package com.badlogic.gdx.backends.android;

import com.badlogic.gdx.backends.android.z.a;
import com.badlogic.gdx.backends.android.z.f;
import com.google.android.gms.common.api.Api;

/* compiled from: AndroidApplicationConfiguration */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public int f4624a = 5;

    /* renamed from: b  reason: collision with root package name */
    public int f4625b = 6;

    /* renamed from: c  reason: collision with root package name */
    public int f4626c = 5;

    /* renamed from: d  reason: collision with root package name */
    public int f4627d = 0;

    /* renamed from: e  reason: collision with root package name */
    public int f4628e = 16;

    /* renamed from: f  reason: collision with root package name */
    public int f4629f = 0;

    /* renamed from: g  reason: collision with root package name */
    public int f4630g = 0;

    /* renamed from: h  reason: collision with root package name */
    public boolean f4631h = true;

    /* renamed from: i  reason: collision with root package name */
    public boolean f4632i = false;

    /* renamed from: j  reason: collision with root package name */
    public boolean f4633j = true;

    /* renamed from: k  reason: collision with root package name */
    public boolean f4634k = false;
    public int l = 1;
    public int m = 0;
    public boolean n = false;
    public boolean o = false;
    public int p = 16;
    public f q = new a();
    public boolean r = false;
    @Deprecated
    public boolean s = false;
    public boolean t = false;
    public int u = Api.BaseClientBuilder.API_PRIORITY_OTHER;
}
