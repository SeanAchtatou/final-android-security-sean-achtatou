package com.badlogic.gdx.backends.android;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import e.d.b.a;
import java.util.ArrayList;

/* compiled from: AndroidInputThreePlus */
public class n extends l implements View.OnGenericMotionListener {
    ArrayList<View.OnGenericMotionListener> S = new ArrayList<>();
    private final o T;

    public n(a aVar, Context context, Object obj, b bVar) {
        super(aVar, context, obj, bVar);
        if (obj instanceof View) {
            ((View) obj).setOnGenericMotionListener(this);
        }
        this.T = new o();
    }

    public boolean onGenericMotion(View view, MotionEvent motionEvent) {
        if (this.T.a(motionEvent, this)) {
            return true;
        }
        int size = this.S.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (this.S.get(i2).onGenericMotion(view, motionEvent)) {
                return true;
            }
        }
        return false;
    }
}
