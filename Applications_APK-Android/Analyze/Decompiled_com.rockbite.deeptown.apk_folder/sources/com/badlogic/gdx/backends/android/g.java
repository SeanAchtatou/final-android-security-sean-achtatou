package com.badlogic.gdx.backends.android;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import com.appsflyer.share.Constants;
import com.badlogic.gdx.utils.o;
import com.badlogic.gdx.utils.q0;
import e.d.b.e;
import e.d.b.s.a;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/* compiled from: AndroidFileHandle */
public class g extends a {

    /* renamed from: c  reason: collision with root package name */
    private final AssetManager f4640c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    g(AssetManager assetManager, String str, e.a aVar) {
        super(str.replace('\\', '/'), aVar);
        this.f4640c = assetManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public a a(String str) {
        String replace = str.replace('\\', '/');
        if (this.f6913a.getPath().length() == 0) {
            return new g(this.f4640c, new File(replace), this.f6914b);
        }
        return new g(this.f4640c, new File(this.f6913a, replace), this.f6914b);
    }

    public File c() {
        if (this.f6914b == e.a.Local) {
            return new File(e.d.b.g.f6804e.b(), this.f6913a.getPath());
        }
        return super.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public a d(String str) {
        String replace = str.replace('\\', '/');
        if (this.f6913a.getPath().length() != 0) {
            return e.d.b.g.f6804e.a(new File(this.f6913a.getParent(), replace).getPath(), this.f6914b);
        }
        throw new o("Cannot get the sibling of the root.");
    }

    public a i() {
        File parentFile = this.f6913a.getParentFile();
        if (parentFile == null) {
            if (this.f6914b == e.a.Absolute) {
                parentFile = new File(Constants.URL_PATH_DELIMITER);
            } else {
                parentFile = new File("");
            }
        }
        return new g(this.f4640c, parentFile, this.f6914b);
    }

    public InputStream l() {
        if (this.f6914b != e.a.Internal) {
            return super.l();
        }
        try {
            return this.f4640c.open(this.f6913a.getPath());
        } catch (IOException e2) {
            throw new o("Error reading file: " + this.f6913a + " (" + this.f6914b + ")", e2);
        }
    }

    public AssetFileDescriptor p() throws IOException {
        AssetManager assetManager = this.f4640c;
        if (assetManager != null) {
            return assetManager.openFd(j());
        }
        return null;
    }

    g(AssetManager assetManager, File file, e.a aVar) {
        super(file, aVar);
        this.f4640c = assetManager;
    }

    public ByteBuffer a(FileChannel.MapMode mapMode) {
        if (this.f6914b != e.a.Internal) {
            return super.a(mapMode);
        }
        FileInputStream fileInputStream = null;
        try {
            AssetFileDescriptor p = p();
            long startOffset = p.getStartOffset();
            long declaredLength = p.getDeclaredLength();
            FileInputStream fileInputStream2 = new FileInputStream(p.getFileDescriptor());
            try {
                MappedByteBuffer map = fileInputStream2.getChannel().map(mapMode, startOffset, declaredLength);
                map.order(ByteOrder.nativeOrder());
                q0.a(fileInputStream2);
                return map;
            } catch (Exception e2) {
                e = e2;
                fileInputStream = fileInputStream2;
                try {
                    throw new o("Error memory mapping file: " + this + " (" + this.f6914b + ")", e);
                } catch (Throwable th) {
                    th = th;
                    q0.a(fileInputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = fileInputStream2;
                q0.a(fileInputStream);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            throw new o("Error memory mapping file: " + this + " (" + this.f6914b + ")", e);
        }
    }

    public long d() {
        if (this.f6914b == e.a.Internal) {
            AssetFileDescriptor assetFileDescriptor = null;
            try {
                AssetFileDescriptor openFd = this.f4640c.openFd(this.f6913a.getPath());
                long length = openFd.getLength();
                if (openFd != null) {
                    try {
                        openFd.close();
                    } catch (IOException unused) {
                    }
                }
                return length;
            } catch (IOException unused2) {
                if (assetFileDescriptor != null) {
                    try {
                        assetFileDescriptor.close();
                    } catch (IOException unused3) {
                    }
                }
            } catch (Throwable th) {
                if (assetFileDescriptor != null) {
                    try {
                        assetFileDescriptor.close();
                    } catch (IOException unused4) {
                    }
                }
                throw th;
            }
        }
        return super.d();
    }

    public boolean a() {
        if (this.f6914b != e.a.Internal) {
            return super.a();
        }
        String path = this.f6913a.getPath();
        try {
            this.f4640c.open(path).close();
            return true;
        } catch (Exception unused) {
            try {
                if (this.f4640c.list(path).length > 0) {
                    return true;
                }
                return false;
            } catch (Exception unused2) {
                return false;
            }
        }
    }
}
