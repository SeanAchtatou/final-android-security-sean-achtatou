package com.badlogic.gdx.backends.android;

import android.annotation.TargetApi;
import android.opengl.GLES30;
import e.d.b.t.h;
import java.nio.Buffer;
import java.nio.IntBuffer;

@TargetApi(18)
/* compiled from: AndroidGL30 */
public class j extends i implements h {
    public void a(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, Buffer buffer) {
        if (buffer == null) {
            GLES30.glTexImage3D(i2, i3, i4, i5, i6, i7, i8, i9, i10, 0);
        } else {
            GLES30.glTexImage3D(i2, i3, i4, i5, i6, i7, i8, i9, i10, buffer);
        }
    }

    public void b(int i2, IntBuffer intBuffer) {
        GLES30.glDrawBuffers(i2, intBuffer);
    }

    public void d(int i2, IntBuffer intBuffer) {
        GLES30.glGenVertexArrays(i2, intBuffer);
    }

    public void i(int i2) {
        GLES30.glBindVertexArray(i2);
    }

    public void b(int i2, int i3, int i4, int i5) {
        GLES30.glDrawArraysInstanced(i2, i3, i4, i5);
    }

    public void a(int i2, IntBuffer intBuffer) {
        GLES30.glDeleteVertexArrays(i2, intBuffer);
    }

    public void b(int i2, int i3, int i4, int i5, int i6) {
        GLES30.glDrawElementsInstanced(i2, i3, i4, i5, i6);
    }
}
