package com.badlogic.gdx.backends.android;

import android.content.Context;
import android.view.WindowManager;
import com.badlogic.gdx.utils.o0;
import e.d.b.m;

/* compiled from: AndroidApplicationBase */
public interface a extends e.d.b.a {
    l b();

    com.badlogic.gdx.utils.a<Runnable> d();

    com.badlogic.gdx.utils.a<Runnable> f();

    Context getContext();

    WindowManager h();

    o0<m> i();
}
