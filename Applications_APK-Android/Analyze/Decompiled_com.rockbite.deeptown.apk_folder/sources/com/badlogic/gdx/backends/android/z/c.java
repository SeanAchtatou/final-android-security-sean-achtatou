package com.badlogic.gdx.backends.android.z;

import android.annotation.TargetApi;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import com.badlogic.gdx.backends.android.z.d;
import com.badlogic.gdx.backends.android.z.f;
import com.google.android.gms.drive.DriveFile;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;

/* compiled from: GLSurfaceView20API18 */
public class c extends d {
    static String n = "GL2JNIView";
    final f m;

    /* compiled from: GLSurfaceView20API18 */
    class a extends BaseInputConnection {
        a(c cVar, View view, boolean z) {
            super(view, z);
        }

        @TargetApi(16)
        private void a(int i2) {
            long uptimeMillis = SystemClock.uptimeMillis();
            long j2 = uptimeMillis;
            int i3 = i2;
            super.sendKeyEvent(new KeyEvent(uptimeMillis, j2, 0, i3, 0, 0, -1, 0, 6));
            super.sendKeyEvent(new KeyEvent(SystemClock.uptimeMillis(), j2, 1, i3, 0, 0, -1, 0, 6));
        }

        public boolean deleteSurroundingText(int i2, int i3) {
            if (Build.VERSION.SDK_INT < 16 || i2 != 1 || i3 != 0) {
                return super.deleteSurroundingText(i2, i3);
            }
            a(67);
            return true;
        }
    }

    /* renamed from: com.badlogic.gdx.backends.android.z.c$c  reason: collision with other inner class name */
    /* compiled from: GLSurfaceView20API18 */
    static class C0113c implements d.f {

        /* renamed from: a  reason: collision with root package name */
        private static int f4726a = 12440;

        C0113c() {
        }

        public EGLContext createContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig) {
            Log.w(c.n, "creating OpenGL ES 2.0 context");
            c.a("Before eglCreateContext", egl10);
            EGLContext eglCreateContext = egl10.eglCreateContext(eGLDisplay, eGLConfig, EGL10.EGL_NO_CONTEXT, new int[]{f4726a, 2, 12344});
            c.a("After eglCreateContext", egl10);
            return eglCreateContext;
        }

        public void destroyContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLContext eGLContext) {
            egl10.eglDestroyContext(eGLDisplay, eGLContext);
        }
    }

    public c(Context context, f fVar) {
        super(context);
        this.m = fVar;
        a(false, 16, 0);
    }

    private void a(boolean z, int i2, int i3) {
        b bVar;
        if (z) {
            getHolder().setFormat(-3);
        }
        setEGLContextFactory(new C0113c());
        if (!z) {
            new b(5, 6, 5, 0, i2, i3);
        }
        setEGLConfigChooser(bVar);
    }

    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        if (editorInfo != null) {
            editorInfo.imeOptions |= DriveFile.MODE_READ_ONLY;
        }
        return new a(this, this, false);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        f.a a2 = this.m.a(i2, i3);
        setMeasuredDimension(a2.f4782a, a2.f4783b);
    }

    static void a(String str, EGL10 egl10) {
        while (true) {
            int eglGetError = egl10.eglGetError();
            if (eglGetError != 12288) {
                Log.e(n, String.format("%s: EGL error: 0x%x", str, Integer.valueOf(eglGetError)));
            } else {
                return;
            }
        }
    }

    /* compiled from: GLSurfaceView20API18 */
    private static class b implements GLSurfaceView.EGLConfigChooser {

        /* renamed from: h  reason: collision with root package name */
        private static int f4717h = 4;

        /* renamed from: i  reason: collision with root package name */
        private static int[] f4718i = {12324, 4, 12323, 4, 12322, 4, 12352, f4717h, 12344};

        /* renamed from: a  reason: collision with root package name */
        protected int f4719a;

        /* renamed from: b  reason: collision with root package name */
        protected int f4720b;

        /* renamed from: c  reason: collision with root package name */
        protected int f4721c;

        /* renamed from: d  reason: collision with root package name */
        protected int f4722d;

        /* renamed from: e  reason: collision with root package name */
        protected int f4723e;

        /* renamed from: f  reason: collision with root package name */
        protected int f4724f;

        /* renamed from: g  reason: collision with root package name */
        private int[] f4725g = new int[1];

        public b(int i2, int i3, int i4, int i5, int i6, int i7) {
            this.f4719a = i2;
            this.f4720b = i3;
            this.f4721c = i4;
            this.f4722d = i5;
            this.f4723e = i6;
            this.f4724f = i7;
        }

        public EGLConfig a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr) {
            for (EGLConfig eGLConfig : eGLConfigArr) {
                EGL10 egl102 = egl10;
                EGLDisplay eGLDisplay2 = eGLDisplay;
                EGLConfig eGLConfig2 = eGLConfig;
                int a2 = a(egl102, eGLDisplay2, eGLConfig2, 12325, 0);
                int a3 = a(egl102, eGLDisplay2, eGLConfig2, 12326, 0);
                if (a2 >= this.f4723e && a3 >= this.f4724f) {
                    EGL10 egl103 = egl10;
                    EGLDisplay eGLDisplay3 = eGLDisplay;
                    EGLConfig eGLConfig3 = eGLConfig;
                    int a4 = a(egl103, eGLDisplay3, eGLConfig3, 12324, 0);
                    int a5 = a(egl103, eGLDisplay3, eGLConfig3, 12323, 0);
                    int a6 = a(egl103, eGLDisplay3, eGLConfig3, 12322, 0);
                    int a7 = a(egl103, eGLDisplay3, eGLConfig3, 12321, 0);
                    if (a4 == this.f4719a && a5 == this.f4720b && a6 == this.f4721c && a7 == this.f4722d) {
                        return eGLConfig;
                    }
                }
            }
            return null;
        }

        public EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay) {
            int[] iArr = new int[1];
            egl10.eglChooseConfig(eGLDisplay, f4718i, null, 0, iArr);
            int i2 = iArr[0];
            if (i2 > 0) {
                EGLConfig[] eGLConfigArr = new EGLConfig[i2];
                egl10.eglChooseConfig(eGLDisplay, f4718i, eGLConfigArr, i2, iArr);
                return a(egl10, eGLDisplay, eGLConfigArr);
            }
            throw new IllegalArgumentException("No configs match configSpec");
        }

        private int a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i2, int i3) {
            return egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i2, this.f4725g) ? this.f4725g[0] : i3;
        }
    }
}
