package com.badlogic.gdx.backends.android;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import com.badlogic.gdx.utils.o;
import e.d.b.e;
import e.d.b.r.b;
import e.d.b.s.a;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: AndroidAudio */
public final class d implements e.d.b.d {

    /* renamed from: a  reason: collision with root package name */
    private final SoundPool f4635a;

    /* renamed from: b  reason: collision with root package name */
    private final AudioManager f4636b;

    /* renamed from: c  reason: collision with root package name */
    protected final List<q> f4637c = new ArrayList();

    public d(Context context, b bVar) {
        if (!bVar.o) {
            if (Build.VERSION.SDK_INT >= 21) {
                this.f4635a = new SoundPool.Builder().setAudioAttributes(new AudioAttributes.Builder().setUsage(14).setContentType(4).build()).setMaxStreams(bVar.p).build();
            } else {
                this.f4635a = new SoundPool(bVar.p, 3, 0);
            }
            this.f4636b = (AudioManager) context.getSystemService("audio");
            if (context instanceof Activity) {
                ((Activity) context).setVolumeControlStream(3);
                return;
            }
            return;
        }
        this.f4635a = null;
        this.f4636b = null;
    }

    public b a(a aVar) {
        if (this.f4635a != null) {
            g gVar = (g) aVar;
            if (gVar.o() == e.a.Internal) {
                try {
                    AssetFileDescriptor p = gVar.p();
                    u uVar = new u(this.f4635a, this.f4636b, this.f4635a.load(p, 1));
                    p.close();
                    return uVar;
                } catch (IOException e2) {
                    throw new o("Error loading audio file: " + aVar + "\nNote: Internal audio files must be placed in the assets directory.", e2);
                }
            } else {
                try {
                    return new u(this.f4635a, this.f4636b, this.f4635a.load(gVar.c().getPath(), 1));
                } catch (Exception e3) {
                    throw new o("Error loading audio file: " + aVar, e3);
                }
            }
        } else {
            throw new o("Android audio is not enabled by the application config.");
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.f4635a != null) {
            synchronized (this.f4637c) {
                for (q next : this.f4637c) {
                    if (next.a()) {
                        next.pause();
                        next.f4690d = true;
                    } else {
                        next.f4690d = false;
                    }
                }
            }
            this.f4635a.autoPause();
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        if (this.f4635a != null) {
            synchronized (this.f4637c) {
                for (int i2 = 0; i2 < this.f4637c.size(); i2++) {
                    if (this.f4637c.get(i2).f4690d) {
                        this.f4637c.get(i2).play();
                    }
                }
            }
            this.f4635a.autoResume();
        }
    }

    public void a() {
        if (this.f4635a != null) {
            synchronized (this.f4637c) {
                Iterator it = new ArrayList(this.f4637c).iterator();
                while (it.hasNext()) {
                    ((q) it.next()).dispose();
                }
            }
            this.f4635a.release();
        }
    }

    public e.d.b.r.a b(a aVar) {
        if (this.f4635a != null) {
            g gVar = (g) aVar;
            MediaPlayer mediaPlayer = new MediaPlayer();
            if (gVar.o() == e.a.Internal) {
                try {
                    AssetFileDescriptor p = gVar.p();
                    mediaPlayer.setDataSource(p.getFileDescriptor(), p.getStartOffset(), p.getLength());
                    p.close();
                    mediaPlayer.prepare();
                    q qVar = new q(this, mediaPlayer);
                    synchronized (this.f4637c) {
                        this.f4637c.add(qVar);
                    }
                    return qVar;
                } catch (Exception e2) {
                    throw new o("Error loading audio file: " + aVar + "\nNote: Internal audio files must be placed in the assets directory.", e2);
                }
            } else {
                try {
                    mediaPlayer.setDataSource(gVar.c().getPath());
                    mediaPlayer.prepare();
                    q qVar2 = new q(this, mediaPlayer);
                    synchronized (this.f4637c) {
                        this.f4637c.add(qVar2);
                    }
                    return qVar2;
                } catch (Exception e3) {
                    throw new o("Error loading audio file: " + aVar, e3);
                }
            }
        } else {
            throw new o("Android audio is not enabled by the application config.");
        }
    }
}
