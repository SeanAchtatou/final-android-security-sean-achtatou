package com.badlogic.gdx.backends.android.z;

import android.annotation.TargetApi;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import com.badlogic.gdx.backends.android.z.f;
import com.google.android.gms.drive.DriveFile;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;

/* compiled from: GLSurfaceView20 */
public class b extends GLSurfaceView {

    /* renamed from: b  reason: collision with root package name */
    static String f4704b = "GL2JNIView";

    /* renamed from: c  reason: collision with root package name */
    static int f4705c;

    /* renamed from: a  reason: collision with root package name */
    final f f4706a;

    /* compiled from: GLSurfaceView20 */
    class a extends BaseInputConnection {
        a(b bVar, View view, boolean z) {
            super(view, z);
        }

        @TargetApi(16)
        private void a(int i2) {
            long uptimeMillis = SystemClock.uptimeMillis();
            long j2 = uptimeMillis;
            int i3 = i2;
            super.sendKeyEvent(new KeyEvent(uptimeMillis, j2, 0, i3, 0, 0, -1, 0, 6));
            super.sendKeyEvent(new KeyEvent(SystemClock.uptimeMillis(), j2, 1, i3, 0, 0, -1, 0, 6));
        }

        public boolean deleteSurroundingText(int i2, int i3) {
            if (Build.VERSION.SDK_INT < 16 || i2 != 1 || i3 != 0) {
                return super.deleteSurroundingText(i2, i3);
            }
            a(67);
            return true;
        }
    }

    /* compiled from: GLSurfaceView20 */
    static class c implements GLSurfaceView.EGLContextFactory {

        /* renamed from: a  reason: collision with root package name */
        private static int f4716a = 12440;

        c() {
        }

        public EGLContext createContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig) {
            String str = b.f4704b;
            Log.w(str, "creating OpenGL ES " + b.f4705c + ".0 context");
            StringBuilder sb = new StringBuilder();
            sb.append("Before eglCreateContext ");
            sb.append(b.f4705c);
            b.a(sb.toString(), egl10);
            EGLContext eglCreateContext = egl10.eglCreateContext(eGLDisplay, eGLConfig, EGL10.EGL_NO_CONTEXT, new int[]{f4716a, b.f4705c, 12344});
            if ((!b.a("After eglCreateContext " + b.f4705c, egl10) || eglCreateContext == null) && b.f4705c > 2) {
                Log.w(b.f4704b, "Falling back to GLES 2");
                b.f4705c = 2;
                return createContext(egl10, eGLDisplay, eGLConfig);
            }
            String str2 = b.f4704b;
            Log.w(str2, "Returning a GLES " + b.f4705c + " context");
            return eglCreateContext;
        }

        public void destroyContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLContext eGLContext) {
            egl10.eglDestroyContext(eGLDisplay, eGLContext);
        }
    }

    public b(Context context, f fVar, int i2) {
        super(context);
        f4705c = i2;
        this.f4706a = fVar;
        a(false, 16, 0);
    }

    private void a(boolean z, int i2, int i3) {
        C0112b bVar;
        if (z) {
            getHolder().setFormat(-3);
        }
        setEGLContextFactory(new c());
        if (!z) {
            new C0112b(5, 6, 5, 0, i2, i3);
        }
        setEGLConfigChooser(bVar);
    }

    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        if (editorInfo != null) {
            editorInfo.imeOptions |= DriveFile.MODE_READ_ONLY;
        }
        return new a(this, this, false);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        f.a a2 = this.f4706a.a(i2, i3);
        setMeasuredDimension(a2.f4782a, a2.f4783b);
    }

    static boolean a(String str, EGL10 egl10) {
        boolean z = true;
        while (true) {
            int eglGetError = egl10.eglGetError();
            if (eglGetError == 12288) {
                return z;
            }
            Log.e(f4704b, String.format("%s: EGL error: 0x%x", str, Integer.valueOf(eglGetError)));
            z = false;
        }
    }

    /* renamed from: com.badlogic.gdx.backends.android.z.b$b  reason: collision with other inner class name */
    /* compiled from: GLSurfaceView20 */
    private static class C0112b implements GLSurfaceView.EGLConfigChooser {

        /* renamed from: h  reason: collision with root package name */
        private static int f4707h = 4;

        /* renamed from: i  reason: collision with root package name */
        private static int[] f4708i = {12324, 4, 12323, 4, 12322, 4, 12352, f4707h, 12344};

        /* renamed from: a  reason: collision with root package name */
        protected int f4709a;

        /* renamed from: b  reason: collision with root package name */
        protected int f4710b;

        /* renamed from: c  reason: collision with root package name */
        protected int f4711c;

        /* renamed from: d  reason: collision with root package name */
        protected int f4712d;

        /* renamed from: e  reason: collision with root package name */
        protected int f4713e;

        /* renamed from: f  reason: collision with root package name */
        protected int f4714f;

        /* renamed from: g  reason: collision with root package name */
        private int[] f4715g = new int[1];

        public C0112b(int i2, int i3, int i4, int i5, int i6, int i7) {
            this.f4709a = i2;
            this.f4710b = i3;
            this.f4711c = i4;
            this.f4712d = i5;
            this.f4713e = i6;
            this.f4714f = i7;
        }

        public EGLConfig a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr) {
            for (EGLConfig eGLConfig : eGLConfigArr) {
                EGL10 egl102 = egl10;
                EGLDisplay eGLDisplay2 = eGLDisplay;
                EGLConfig eGLConfig2 = eGLConfig;
                int a2 = a(egl102, eGLDisplay2, eGLConfig2, 12325, 0);
                int a3 = a(egl102, eGLDisplay2, eGLConfig2, 12326, 0);
                if (a2 >= this.f4713e && a3 >= this.f4714f) {
                    EGL10 egl103 = egl10;
                    EGLDisplay eGLDisplay3 = eGLDisplay;
                    EGLConfig eGLConfig3 = eGLConfig;
                    int a4 = a(egl103, eGLDisplay3, eGLConfig3, 12324, 0);
                    int a5 = a(egl103, eGLDisplay3, eGLConfig3, 12323, 0);
                    int a6 = a(egl103, eGLDisplay3, eGLConfig3, 12322, 0);
                    int a7 = a(egl103, eGLDisplay3, eGLConfig3, 12321, 0);
                    if (a4 == this.f4709a && a5 == this.f4710b && a6 == this.f4711c && a7 == this.f4712d) {
                        return eGLConfig;
                    }
                }
            }
            return null;
        }

        public EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay) {
            int[] iArr = new int[1];
            egl10.eglChooseConfig(eGLDisplay, f4708i, null, 0, iArr);
            int i2 = iArr[0];
            if (i2 > 0) {
                EGLConfig[] eGLConfigArr = new EGLConfig[i2];
                egl10.eglChooseConfig(eGLDisplay, f4708i, eGLConfigArr, i2, iArr);
                return a(egl10, eGLDisplay, eGLConfigArr);
            }
            throw new IllegalArgumentException("No configs match configSpec");
        }

        private int a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i2, int i3) {
            return egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i2, this.f4715g) ? this.f4715g[0] : i3;
        }
    }
}
