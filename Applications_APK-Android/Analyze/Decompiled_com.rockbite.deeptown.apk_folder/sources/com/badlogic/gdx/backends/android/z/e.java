package com.badlogic.gdx.backends.android.z;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

/* compiled from: GdxEglConfigChooser */
public class e implements GLSurfaceView.EGLConfigChooser {

    /* renamed from: a  reason: collision with root package name */
    protected int f4773a;

    /* renamed from: b  reason: collision with root package name */
    protected int f4774b;

    /* renamed from: c  reason: collision with root package name */
    protected int f4775c;

    /* renamed from: d  reason: collision with root package name */
    protected int f4776d;

    /* renamed from: e  reason: collision with root package name */
    protected int f4777e;

    /* renamed from: f  reason: collision with root package name */
    protected int f4778f;

    /* renamed from: g  reason: collision with root package name */
    protected int f4779g;

    /* renamed from: h  reason: collision with root package name */
    protected final int[] f4780h;

    /* renamed from: i  reason: collision with root package name */
    private int[] f4781i = new int[1];

    public e(int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.f4773a = i2;
        this.f4774b = i3;
        this.f4775c = i4;
        this.f4776d = i5;
        this.f4777e = i6;
        this.f4778f = i7;
        this.f4779g = i8;
        this.f4780h = new int[]{12324, 4, 12323, 4, 12322, 4, 12352, 4, 12344};
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00e2, code lost:
        if (r8 == r6.f4776d) goto L_0x00e4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public javax.microedition.khronos.egl.EGLConfig a(javax.microedition.khronos.egl.EGL10 r20, javax.microedition.khronos.egl.EGLDisplay r21, javax.microedition.khronos.egl.EGLConfig[] r22) {
        /*
            r19 = this;
            r6 = r19
            r7 = r22
            int r8 = r7.length
            r0 = 0
            r1 = 0
            r10 = r0
            r11 = r10
            r12 = r11
            r9 = 0
        L_0x000b:
            if (r9 >= r8) goto L_0x00f4
            r13 = r7[r9]
            r4 = 12325(0x3025, float:1.7271E-41)
            r5 = 0
            r0 = r19
            r1 = r20
            r2 = r21
            r3 = r13
            int r14 = r0.a(r1, r2, r3, r4, r5)
            r4 = 12326(0x3026, float:1.7272E-41)
            int r0 = r0.a(r1, r2, r3, r4, r5)
            int r1 = r6.f4777e
            if (r14 < r1) goto L_0x00ea
            int r1 = r6.f4778f
            if (r0 >= r1) goto L_0x002d
            goto L_0x00ea
        L_0x002d:
            r4 = 12324(0x3024, float:1.727E-41)
            r5 = 0
            r0 = r19
            r1 = r20
            r2 = r21
            r3 = r13
            int r14 = r0.a(r1, r2, r3, r4, r5)
            r4 = 12323(0x3023, float:1.7268E-41)
            int r15 = r0.a(r1, r2, r3, r4, r5)
            r4 = 12322(0x3022, float:1.7267E-41)
            int r5 = r0.a(r1, r2, r3, r4, r5)
            r4 = 12321(0x3021, float:1.7265E-41)
            r16 = 0
            r7 = r5
            r5 = r16
            int r5 = r0.a(r1, r2, r3, r4, r5)
            if (r10 != 0) goto L_0x005f
            r0 = 5
            if (r14 != r0) goto L_0x005f
            r1 = 6
            if (r15 != r1) goto L_0x005f
            if (r7 != r0) goto L_0x005f
            if (r5 != 0) goto L_0x005f
            r10 = r13
        L_0x005f:
            if (r11 != 0) goto L_0x0078
            int r0 = r6.f4773a
            if (r14 != r0) goto L_0x0078
            int r0 = r6.f4774b
            if (r15 != r0) goto L_0x0078
            int r0 = r6.f4775c
            if (r7 != r0) goto L_0x0078
            int r0 = r6.f4776d
            if (r5 != r0) goto L_0x0078
            int r0 = r6.f4779g
            if (r0 != 0) goto L_0x0077
            goto L_0x00f5
        L_0x0077:
            r11 = r13
        L_0x0078:
            r4 = 12338(0x3032, float:1.7289E-41)
            r16 = 0
            r0 = r19
            r1 = r20
            r2 = r21
            r3 = r13
            r17 = r8
            r8 = r5
            r5 = r16
            int r5 = r0.a(r1, r2, r3, r4, r5)
            r4 = 12337(0x3031, float:1.7288E-41)
            r18 = r10
            r10 = r5
            r5 = r16
            int r0 = r0.a(r1, r2, r3, r4, r5)
            r5 = 1
            if (r12 != 0) goto L_0x00b3
            if (r10 != r5) goto L_0x00b3
            int r1 = r6.f4779g
            if (r0 < r1) goto L_0x00b3
            int r0 = r6.f4773a
            if (r14 != r0) goto L_0x00b3
            int r0 = r6.f4774b
            if (r15 != r0) goto L_0x00b3
            int r0 = r6.f4775c
            if (r7 != r0) goto L_0x00b3
            int r0 = r6.f4776d
            if (r8 != r0) goto L_0x00b3
            r16 = r11
            goto L_0x00e4
        L_0x00b3:
            r4 = 12512(0x30e0, float:1.7533E-41)
            r10 = 0
            r0 = r19
            r1 = r20
            r2 = r21
            r3 = r13
            r16 = r11
            r11 = 1
            r5 = r10
            int r10 = r0.a(r1, r2, r3, r4, r5)
            r4 = 12513(0x30e1, float:1.7534E-41)
            r5 = 0
            int r0 = r0.a(r1, r2, r3, r4, r5)
            if (r12 != 0) goto L_0x00e5
            if (r10 != r11) goto L_0x00e5
            int r1 = r6.f4779g
            if (r0 < r1) goto L_0x00e5
            int r0 = r6.f4773a
            if (r14 != r0) goto L_0x00e5
            int r0 = r6.f4774b
            if (r15 != r0) goto L_0x00e5
            int r0 = r6.f4775c
            if (r7 != r0) goto L_0x00e5
            int r0 = r6.f4776d
            if (r8 != r0) goto L_0x00e5
        L_0x00e4:
            r12 = r13
        L_0x00e5:
            r11 = r16
            r10 = r18
            goto L_0x00ec
        L_0x00ea:
            r17 = r8
        L_0x00ec:
            int r9 = r9 + 1
            r7 = r22
            r8 = r17
            goto L_0x000b
        L_0x00f4:
            r13 = r11
        L_0x00f5:
            if (r12 == 0) goto L_0x00f8
            return r12
        L_0x00f8:
            if (r13 == 0) goto L_0x00fb
            return r13
        L_0x00fb:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.z.e.a(javax.microedition.khronos.egl.EGL10, javax.microedition.khronos.egl.EGLDisplay, javax.microedition.khronos.egl.EGLConfig[]):javax.microedition.khronos.egl.EGLConfig");
    }

    public EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay) {
        int[] iArr = new int[1];
        egl10.eglChooseConfig(eGLDisplay, this.f4780h, null, 0, iArr);
        int i2 = iArr[0];
        if (i2 > 0) {
            EGLConfig[] eGLConfigArr = new EGLConfig[i2];
            egl10.eglChooseConfig(eGLDisplay, this.f4780h, eGLConfigArr, i2, iArr);
            return a(egl10, eGLDisplay, eGLConfigArr);
        }
        throw new IllegalArgumentException("No configs match configSpec");
    }

    private int a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i2, int i3) {
        return egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i2, this.f4781i) ? this.f4781i[0] : i3;
    }
}
