package com.badlogic.gdx.backends.android;

import android.view.MotionEvent;
import com.badlogic.gdx.backends.android.l;
import e.d.b.g;

/* compiled from: AndroidMouseHandler */
public class o {

    /* renamed from: a  reason: collision with root package name */
    private int f4685a = 0;

    /* renamed from: b  reason: collision with root package name */
    private int f4686b = 0;

    public boolean a(MotionEvent motionEvent, l lVar) {
        if ((motionEvent.getSource() & 2) == 0) {
            return false;
        }
        int action = motionEvent.getAction() & 255;
        long nanoTime = System.nanoTime();
        synchronized (lVar) {
            if (action == 7) {
                int x = (int) motionEvent.getX();
                int y = (int) motionEvent.getY();
                if (!(x == this.f4685a && y == this.f4686b)) {
                    a(lVar, 4, x, y, 0, nanoTime);
                    this.f4685a = x;
                    this.f4686b = y;
                }
            } else if (action == 8) {
                a(lVar, 3, 0, 0, (int) (-Math.signum(motionEvent.getAxisValue(9))), nanoTime);
            }
        }
        g.f6800a.c().f();
        return true;
    }

    private void a(l lVar, int i2, int i3, int i4, int i5, long j2) {
        l.f obtain = lVar.f4661b.obtain();
        obtain.f4678a = j2;
        obtain.f4680c = i3;
        obtain.f4681d = i4;
        obtain.f4679b = i2;
        obtain.f4682e = i5;
        lVar.f4664e.add(obtain);
    }
}
