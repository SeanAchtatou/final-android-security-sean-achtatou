package com.badlogic.gdx.backends.android;

import android.content.res.AssetManager;
import android.os.Environment;
import com.appsflyer.share.Constants;
import e.d.b.e;
import e.d.b.s.a;

/* compiled from: AndroidFiles */
public class h implements e {

    /* renamed from: a  reason: collision with root package name */
    protected final String f4641a = (Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.URL_PATH_DELIMITER);

    /* renamed from: b  reason: collision with root package name */
    protected final String f4642b;

    /* renamed from: c  reason: collision with root package name */
    protected final AssetManager f4643c;

    /* renamed from: d  reason: collision with root package name */
    private y f4644d = null;

    public h(AssetManager assetManager, String str) {
        this.f4643c = assetManager;
        if (!str.endsWith(Constants.URL_PATH_DELIMITER)) {
            str = str + Constants.URL_PATH_DELIMITER;
        }
        this.f4642b = str;
    }

    public a a(String str, e.a aVar) {
        g gVar = new g(aVar == e.a.Internal ? this.f4643c : null, str, aVar);
        if (this.f4644d != null && aVar == e.a.Internal) {
            a(gVar, str);
        }
        return gVar;
    }

    public a b(String str) {
        return new g((AssetManager) null, str, e.a.Absolute);
    }

    public a c(String str) {
        return new g((AssetManager) null, str, e.a.Classpath);
    }

    public a d(String str) {
        return new g((AssetManager) null, str, e.a.Local);
    }

    public String b() {
        return this.f4642b;
    }

    public y c() {
        return this.f4644d;
    }

    private a a(a aVar, String str) {
        try {
            this.f4643c.open(str).close();
            return aVar;
        } catch (Exception unused) {
            new x(str);
            throw null;
        }
    }

    public a a(String str) {
        g gVar = new g(this.f4643c, str, e.a.Internal);
        if (this.f4644d != null) {
            a(gVar, str);
        }
        return gVar;
    }

    public String a() {
        return this.f4641a;
    }
}
