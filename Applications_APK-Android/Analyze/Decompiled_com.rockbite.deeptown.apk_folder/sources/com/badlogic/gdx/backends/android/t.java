package com.badlogic.gdx.backends.android;

import android.content.SharedPreferences;
import android.os.Build;
import e.d.b.o;

/* compiled from: AndroidPreferences */
public class t implements o {

    /* renamed from: a  reason: collision with root package name */
    SharedPreferences f4695a;

    /* renamed from: b  reason: collision with root package name */
    SharedPreferences.Editor f4696b;

    public t(SharedPreferences sharedPreferences) {
        this.f4695a = sharedPreferences;
    }

    public o a(String str, long j2) {
        a();
        this.f4696b.putLong(str, j2);
        return this;
    }

    public o b(String str, int i2) {
        a();
        this.f4696b.putInt(str, i2);
        return this;
    }

    public void flush() {
        SharedPreferences.Editor editor = this.f4696b;
        if (editor != null) {
            if (Build.VERSION.SDK_INT >= 9) {
                editor.apply();
            } else {
                editor.commit();
            }
            this.f4696b = null;
        }
    }

    public long getLong(String str, long j2) {
        return this.f4695a.getLong(str, j2);
    }

    public String getString(String str) {
        return this.f4695a.getString(str, "");
    }

    public String getString(String str, String str2) {
        return this.f4695a.getString(str, str2);
    }

    public o a(String str, String str2) {
        a();
        this.f4696b.putString(str, str2);
        return this;
    }

    public int a(String str, int i2) {
        return this.f4695a.getInt(str, i2);
    }

    private void a() {
        if (this.f4696b == null) {
            this.f4696b = this.f4695a.edit();
        }
    }
}
