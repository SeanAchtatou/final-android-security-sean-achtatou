package com.badlogic.gdx.backends.android.z;

import android.content.Context;
import android.opengl.GLDebugHelper;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.google.android.gms.drive.MetadataChangeSet;
import java.io.Writer;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

/* compiled from: GLSurfaceViewAPI18 */
public class d extends SurfaceView implements SurfaceHolder.Callback {
    /* access modifiers changed from: private */
    public static final j l = new j();

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<d> f4727a = new WeakReference<>(this);

    /* renamed from: b  reason: collision with root package name */
    private i f4728b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public GLSurfaceView.Renderer f4729c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f4730d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public GLSurfaceView.EGLConfigChooser f4731e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public f f4732f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public g f4733g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public k f4734h;
    /* access modifiers changed from: private */

    /* renamed from: i  reason: collision with root package name */
    public int f4735i;
    /* access modifiers changed from: private */

    /* renamed from: j  reason: collision with root package name */
    public int f4736j;
    /* access modifiers changed from: private */

    /* renamed from: k  reason: collision with root package name */
    public boolean f4737k;

    /* compiled from: GLSurfaceViewAPI18 */
    private abstract class b implements GLSurfaceView.EGLConfigChooser {

        /* renamed from: a  reason: collision with root package name */
        protected int[] f4738a;

        public b(int[] iArr) {
            this.f4738a = a(iArr);
        }

        private int[] a(int[] iArr) {
            if (d.this.f4736j != 2) {
                return iArr;
            }
            int length = iArr.length;
            int[] iArr2 = new int[(length + 2)];
            int i2 = length - 1;
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            iArr2[i2] = 12352;
            iArr2[length] = 4;
            iArr2[length + 1] = 12344;
            return iArr2;
        }

        /* access modifiers changed from: package-private */
        public abstract EGLConfig a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr);

        public EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay) {
            int[] iArr = new int[1];
            if (egl10.eglChooseConfig(eGLDisplay, this.f4738a, null, 0, iArr)) {
                int i2 = iArr[0];
                if (i2 > 0) {
                    EGLConfig[] eGLConfigArr = new EGLConfig[i2];
                    if (egl10.eglChooseConfig(eGLDisplay, this.f4738a, eGLConfigArr, i2, iArr)) {
                        EGLConfig a2 = a(egl10, eGLDisplay, eGLConfigArr);
                        if (a2 != null) {
                            return a2;
                        }
                        throw new IllegalArgumentException("No config chosen");
                    }
                    throw new IllegalArgumentException("eglChooseConfig#2 failed");
                }
                throw new IllegalArgumentException("No configs match configSpec");
            }
            throw new IllegalArgumentException("eglChooseConfig failed");
        }
    }

    /* renamed from: com.badlogic.gdx.backends.android.z.d$d  reason: collision with other inner class name */
    /* compiled from: GLSurfaceViewAPI18 */
    private class C0114d implements f {

        /* renamed from: a  reason: collision with root package name */
        private int f4747a;

        private C0114d() {
            this.f4747a = 12440;
        }

        public EGLContext createContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig) {
            int[] iArr = {this.f4747a, d.this.f4736j, 12344};
            EGLContext eGLContext = EGL10.EGL_NO_CONTEXT;
            if (d.this.f4736j == 0) {
                iArr = null;
            }
            return egl10.eglCreateContext(eGLDisplay, eGLConfig, eGLContext, iArr);
        }

        public void destroyContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLContext eGLContext) {
            if (!egl10.eglDestroyContext(eGLDisplay, eGLContext)) {
                Log.e("DefaultContextFactory", "display:" + eGLDisplay + " context: " + eGLContext);
                h.b("eglDestroyContex", egl10.eglGetError());
                throw null;
            }
        }
    }

    /* compiled from: GLSurfaceViewAPI18 */
    public interface f {
        EGLContext createContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig);

        void destroyContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLContext eGLContext);
    }

    /* compiled from: GLSurfaceViewAPI18 */
    public interface g {
        EGLSurface a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, Object obj);

        void a(EGL10 egl10, EGLDisplay eGLDisplay, EGLSurface eGLSurface);
    }

    /* compiled from: GLSurfaceViewAPI18 */
    static class i extends Thread {

        /* renamed from: a  reason: collision with root package name */
        private boolean f4755a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public boolean f4756b;

        /* renamed from: c  reason: collision with root package name */
        private boolean f4757c;

        /* renamed from: d  reason: collision with root package name */
        private boolean f4758d;

        /* renamed from: e  reason: collision with root package name */
        private boolean f4759e;

        /* renamed from: f  reason: collision with root package name */
        private boolean f4760f;

        /* renamed from: g  reason: collision with root package name */
        private boolean f4761g;

        /* renamed from: h  reason: collision with root package name */
        private boolean f4762h;

        /* renamed from: i  reason: collision with root package name */
        private boolean f4763i;

        /* renamed from: j  reason: collision with root package name */
        private boolean f4764j;

        /* renamed from: k  reason: collision with root package name */
        private boolean f4765k;
        private int l = 0;
        private int m = 0;
        private int n = 1;
        private boolean o = true;
        private boolean p;
        private ArrayList<Runnable> q = new ArrayList<>();
        private boolean r = true;
        private h s;
        private WeakReference<d> t;

        i(WeakReference<d> weakReference) {
            this.t = weakReference;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:100:0x0163, code lost:
            if (r1.s.b() == false) goto L_0x017a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:101:0x0165, code lost:
            r10 = com.badlogic.gdx.backends.android.z.d.d();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:102:0x0169, code lost:
            monitor-enter(r10);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:105:?, code lost:
            r1.f4764j = true;
            com.badlogic.gdx.backends.android.z.d.d().notifyAll();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:106:0x0174, code lost:
            monitor-exit(r10);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:107:0x0175, code lost:
            r10 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:112:0x017a, code lost:
            r15 = com.badlogic.gdx.backends.android.z.d.d();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:113:0x017e, code lost:
            monitor-enter(r15);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:116:?, code lost:
            r1.f4764j = true;
            r1.f4760f = true;
            com.badlogic.gdx.backends.android.z.d.d().notifyAll();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:117:0x018b, code lost:
            monitor-exit(r15);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:123:0x0192, code lost:
            if (r11 == false) goto L_0x01a5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:124:0x0194, code lost:
            r0 = (javax.microedition.khronos.opengles.GL10) r1.s.a();
            com.badlogic.gdx.backends.android.z.d.d().a(r0);
            r6 = r0;
            r11 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:125:0x01a5, code lost:
            if (r9 == false) goto L_0x01bd;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:126:0x01a7, code lost:
            r0 = r1.t.get();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:127:0x01af, code lost:
            if (r0 == null) goto L_0x01bc;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:128:0x01b1, code lost:
            com.badlogic.gdx.backends.android.z.d.a(r0).onSurfaceCreated(r6, r1.s.f4753e);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:129:0x01bc, code lost:
            r9 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:130:0x01bd, code lost:
            if (r12 == false) goto L_0x01d1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:131:0x01bf, code lost:
            r0 = r1.t.get();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:132:0x01c7, code lost:
            if (r0 == null) goto L_0x01d0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:133:0x01c9, code lost:
            com.badlogic.gdx.backends.android.z.d.a(r0).onSurfaceChanged(r6, r7, r8);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:134:0x01d0, code lost:
            r12 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:135:0x01d1, code lost:
            r0 = r1.t.get();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:136:0x01d9, code lost:
            if (r0 == null) goto L_0x01e2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:137:0x01db, code lost:
            com.badlogic.gdx.backends.android.z.d.a(r0).onDrawFrame(r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:138:0x01e2, code lost:
            r0 = r1.s.f();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:139:0x01ea, code lost:
            if (r0 == 12288) goto L_0x020e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:141:0x01ee, code lost:
            if (r0 == 12302) goto L_0x020b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:142:0x01f0, code lost:
            com.badlogic.gdx.backends.android.z.d.h.a("GLThread", "eglSwapBuffers", r0);
            r2 = com.badlogic.gdx.backends.android.z.d.d();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:143:0x01fb, code lost:
            monitor-enter(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:146:?, code lost:
            r1.f4760f = true;
            com.badlogic.gdx.backends.android.z.d.d().notifyAll();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:147:0x0206, code lost:
            monitor-exit(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:153:0x020b, code lost:
            r3 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:155:0x020f, code lost:
            if (r13 == false) goto L_0x018c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:156:0x0211, code lost:
            r4 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:95:0x0153, code lost:
            if (r14 == null) goto L_0x015b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:0x015b, code lost:
            if (r10 == false) goto L_0x0192;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void j() throws java.lang.InterruptedException {
            /*
                r16 = this;
                r1 = r16
                com.badlogic.gdx.backends.android.z.d$h r0 = new com.badlogic.gdx.backends.android.z.d$h
                java.lang.ref.WeakReference<com.badlogic.gdx.backends.android.z.d> r2 = r1.t
                r0.<init>(r2)
                r1.s = r0
                r0 = 0
                r1.f4762h = r0
                r1.f4763i = r0
                r3 = 0
                r4 = 0
                r5 = 0
                r6 = 0
                r7 = 0
                r8 = 0
                r9 = 0
                r10 = 0
                r11 = 0
                r12 = 0
                r13 = 0
            L_0x001b:
                r14 = 0
            L_0x001c:
                com.badlogic.gdx.backends.android.z.d$j r15 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x0222 }
                monitor-enter(r15)     // Catch:{ all -> 0x0222 }
            L_0x0021:
                boolean r2 = r1.f4755a     // Catch:{ all -> 0x021f }
                if (r2 == 0) goto L_0x0036
                monitor-exit(r15)     // Catch:{ all -> 0x021f }
                com.badlogic.gdx.backends.android.z.d$j r2 = com.badlogic.gdx.backends.android.z.d.l
                monitor-enter(r2)
                r16.m()     // Catch:{ all -> 0x0033 }
                r16.l()     // Catch:{ all -> 0x0033 }
                monitor-exit(r2)     // Catch:{ all -> 0x0033 }
                return
            L_0x0033:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0033 }
                throw r0
            L_0x0036:
                java.util.ArrayList<java.lang.Runnable> r2 = r1.q     // Catch:{ all -> 0x021f }
                boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x021f }
                if (r2 != 0) goto L_0x004b
                java.util.ArrayList<java.lang.Runnable> r2 = r1.q     // Catch:{ all -> 0x021f }
                r14 = 0
                java.lang.Object r2 = r2.remove(r14)     // Catch:{ all -> 0x021f }
                java.lang.Runnable r2 = (java.lang.Runnable) r2     // Catch:{ all -> 0x021f }
                r14 = r2
                r2 = 0
                goto L_0x0152
            L_0x004b:
                boolean r2 = r1.f4758d     // Catch:{ all -> 0x021f }
                boolean r0 = r1.f4757c     // Catch:{ all -> 0x021f }
                if (r2 == r0) goto L_0x005f
                boolean r0 = r1.f4757c     // Catch:{ all -> 0x021f }
                boolean r2 = r1.f4757c     // Catch:{ all -> 0x021f }
                r1.f4758d = r2     // Catch:{ all -> 0x021f }
                com.badlogic.gdx.backends.android.z.d$j r2 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x021f }
                r2.notifyAll()     // Catch:{ all -> 0x021f }
                goto L_0x0060
            L_0x005f:
                r0 = 0
            L_0x0060:
                boolean r2 = r1.f4765k     // Catch:{ all -> 0x021f }
                if (r2 == 0) goto L_0x006e
                r16.m()     // Catch:{ all -> 0x021f }
                r16.l()     // Catch:{ all -> 0x021f }
                r2 = 0
                r1.f4765k = r2     // Catch:{ all -> 0x021f }
                r5 = 1
            L_0x006e:
                if (r3 == 0) goto L_0x0077
                r16.m()     // Catch:{ all -> 0x021f }
                r16.l()     // Catch:{ all -> 0x021f }
                r3 = 0
            L_0x0077:
                if (r0 == 0) goto L_0x0080
                boolean r2 = r1.f4763i     // Catch:{ all -> 0x021f }
                if (r2 == 0) goto L_0x0080
                r16.m()     // Catch:{ all -> 0x021f }
            L_0x0080:
                if (r0 == 0) goto L_0x00a5
                boolean r2 = r1.f4762h     // Catch:{ all -> 0x021f }
                if (r2 == 0) goto L_0x00a5
                java.lang.ref.WeakReference<com.badlogic.gdx.backends.android.z.d> r2 = r1.t     // Catch:{ all -> 0x021f }
                java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x021f }
                com.badlogic.gdx.backends.android.z.d r2 = (com.badlogic.gdx.backends.android.z.d) r2     // Catch:{ all -> 0x021f }
                if (r2 != 0) goto L_0x0092
                r2 = 0
                goto L_0x0096
            L_0x0092:
                boolean r2 = r2.f4737k     // Catch:{ all -> 0x021f }
            L_0x0096:
                if (r2 == 0) goto L_0x00a2
                com.badlogic.gdx.backends.android.z.d$j r2 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x021f }
                boolean r2 = r2.a()     // Catch:{ all -> 0x021f }
                if (r2 == 0) goto L_0x00a5
            L_0x00a2:
                r16.l()     // Catch:{ all -> 0x021f }
            L_0x00a5:
                if (r0 == 0) goto L_0x00b6
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x021f }
                boolean r0 = r0.b()     // Catch:{ all -> 0x021f }
                if (r0 == 0) goto L_0x00b6
                com.badlogic.gdx.backends.android.z.d$h r0 = r1.s     // Catch:{ all -> 0x021f }
                r0.d()     // Catch:{ all -> 0x021f }
            L_0x00b6:
                boolean r0 = r1.f4759e     // Catch:{ all -> 0x021f }
                if (r0 != 0) goto L_0x00d2
                boolean r0 = r1.f4761g     // Catch:{ all -> 0x021f }
                if (r0 != 0) goto L_0x00d2
                boolean r0 = r1.f4763i     // Catch:{ all -> 0x021f }
                if (r0 == 0) goto L_0x00c5
                r16.m()     // Catch:{ all -> 0x021f }
            L_0x00c5:
                r0 = 1
                r1.f4761g = r0     // Catch:{ all -> 0x021f }
                r0 = 0
                r1.f4760f = r0     // Catch:{ all -> 0x021f }
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x021f }
                r0.notifyAll()     // Catch:{ all -> 0x021f }
            L_0x00d2:
                boolean r0 = r1.f4759e     // Catch:{ all -> 0x021f }
                if (r0 == 0) goto L_0x00e4
                boolean r0 = r1.f4761g     // Catch:{ all -> 0x021f }
                if (r0 == 0) goto L_0x00e4
                r0 = 0
                r1.f4761g = r0     // Catch:{ all -> 0x021f }
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x021f }
                r0.notifyAll()     // Catch:{ all -> 0x021f }
            L_0x00e4:
                if (r4 == 0) goto L_0x00f2
                r0 = 1
                r1.p = r0     // Catch:{ all -> 0x021f }
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x021f }
                r0.notifyAll()     // Catch:{ all -> 0x021f }
                r4 = 0
                r13 = 0
            L_0x00f2:
                boolean r0 = r16.k()     // Catch:{ all -> 0x021f }
                if (r0 == 0) goto L_0x0215
                boolean r0 = r1.f4762h     // Catch:{ all -> 0x021f }
                if (r0 != 0) goto L_0x0124
                if (r5 == 0) goto L_0x0100
                r5 = 0
                goto L_0x0124
            L_0x0100:
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x021f }
                boolean r0 = r0.c(r1)     // Catch:{ all -> 0x021f }
                if (r0 == 0) goto L_0x0124
                com.badlogic.gdx.backends.android.z.d$h r0 = r1.s     // Catch:{ RuntimeException -> 0x011b }
                r0.e()     // Catch:{ RuntimeException -> 0x011b }
                r0 = 1
                r1.f4762h = r0     // Catch:{ all -> 0x021f }
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x021f }
                r0.notifyAll()     // Catch:{ all -> 0x021f }
                r9 = 1
                goto L_0x0124
            L_0x011b:
                r0 = move-exception
                com.badlogic.gdx.backends.android.z.d$j r2 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x021f }
                r2.a(r1)     // Catch:{ all -> 0x021f }
                throw r0     // Catch:{ all -> 0x021f }
            L_0x0124:
                boolean r0 = r1.f4762h     // Catch:{ all -> 0x021f }
                if (r0 == 0) goto L_0x0133
                boolean r0 = r1.f4763i     // Catch:{ all -> 0x021f }
                if (r0 != 0) goto L_0x0133
                r0 = 1
                r1.f4763i = r0     // Catch:{ all -> 0x021f }
                r0 = 1
                r11 = 1
                r12 = 1
                goto L_0x0134
            L_0x0133:
                r0 = r10
            L_0x0134:
                boolean r2 = r1.f4763i     // Catch:{ all -> 0x021f }
                if (r2 == 0) goto L_0x0214
                boolean r2 = r1.r     // Catch:{ all -> 0x021f }
                if (r2 == 0) goto L_0x0147
                int r7 = r1.l     // Catch:{ all -> 0x021f }
                int r8 = r1.m     // Catch:{ all -> 0x021f }
                r2 = 0
                r1.r = r2     // Catch:{ all -> 0x021f }
                r0 = 1
                r12 = 1
                r13 = 1
                goto L_0x0148
            L_0x0147:
                r2 = 0
            L_0x0148:
                r1.o = r2     // Catch:{ all -> 0x021f }
                com.badlogic.gdx.backends.android.z.d$j r10 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x021f }
                r10.notifyAll()     // Catch:{ all -> 0x021f }
                r10 = r0
            L_0x0152:
                monitor-exit(r15)     // Catch:{ all -> 0x021f }
                if (r14 == 0) goto L_0x015b
                r14.run()     // Catch:{ all -> 0x0222 }
                r0 = 0
                goto L_0x001b
            L_0x015b:
                if (r10 == 0) goto L_0x0192
                com.badlogic.gdx.backends.android.z.d$h r0 = r1.s     // Catch:{ all -> 0x0222 }
                boolean r0 = r0.b()     // Catch:{ all -> 0x0222 }
                if (r0 == 0) goto L_0x017a
                com.badlogic.gdx.backends.android.z.d$j r10 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x0222 }
                monitor-enter(r10)     // Catch:{ all -> 0x0222 }
                r0 = 1
                r1.f4764j = r0     // Catch:{ all -> 0x0177 }
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x0177 }
                r0.notifyAll()     // Catch:{ all -> 0x0177 }
                monitor-exit(r10)     // Catch:{ all -> 0x0177 }
                r10 = 0
                goto L_0x0192
            L_0x0177:
                r0 = move-exception
                monitor-exit(r10)     // Catch:{ all -> 0x0177 }
                throw r0     // Catch:{ all -> 0x0222 }
            L_0x017a:
                com.badlogic.gdx.backends.android.z.d$j r15 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x0222 }
                monitor-enter(r15)     // Catch:{ all -> 0x0222 }
                r0 = 1
                r1.f4764j = r0     // Catch:{ all -> 0x018f }
                r1.f4760f = r0     // Catch:{ all -> 0x018f }
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x018f }
                r0.notifyAll()     // Catch:{ all -> 0x018f }
                monitor-exit(r15)     // Catch:{ all -> 0x018f }
            L_0x018c:
                r0 = 0
                goto L_0x001c
            L_0x018f:
                r0 = move-exception
                monitor-exit(r15)     // Catch:{ all -> 0x018f }
                throw r0     // Catch:{ all -> 0x0222 }
            L_0x0192:
                if (r11 == 0) goto L_0x01a5
                com.badlogic.gdx.backends.android.z.d$h r0 = r1.s     // Catch:{ all -> 0x0222 }
                javax.microedition.khronos.opengles.GL r0 = r0.a()     // Catch:{ all -> 0x0222 }
                javax.microedition.khronos.opengles.GL10 r0 = (javax.microedition.khronos.opengles.GL10) r0     // Catch:{ all -> 0x0222 }
                com.badlogic.gdx.backends.android.z.d$j r6 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x0222 }
                r6.a(r0)     // Catch:{ all -> 0x0222 }
                r6 = r0
                r11 = 0
            L_0x01a5:
                if (r9 == 0) goto L_0x01bd
                java.lang.ref.WeakReference<com.badlogic.gdx.backends.android.z.d> r0 = r1.t     // Catch:{ all -> 0x0222 }
                java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0222 }
                com.badlogic.gdx.backends.android.z.d r0 = (com.badlogic.gdx.backends.android.z.d) r0     // Catch:{ all -> 0x0222 }
                if (r0 == 0) goto L_0x01bc
                android.opengl.GLSurfaceView$Renderer r0 = r0.f4729c     // Catch:{ all -> 0x0222 }
                com.badlogic.gdx.backends.android.z.d$h r9 = r1.s     // Catch:{ all -> 0x0222 }
                javax.microedition.khronos.egl.EGLConfig r9 = r9.f4753e     // Catch:{ all -> 0x0222 }
                r0.onSurfaceCreated(r6, r9)     // Catch:{ all -> 0x0222 }
            L_0x01bc:
                r9 = 0
            L_0x01bd:
                if (r12 == 0) goto L_0x01d1
                java.lang.ref.WeakReference<com.badlogic.gdx.backends.android.z.d> r0 = r1.t     // Catch:{ all -> 0x0222 }
                java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0222 }
                com.badlogic.gdx.backends.android.z.d r0 = (com.badlogic.gdx.backends.android.z.d) r0     // Catch:{ all -> 0x0222 }
                if (r0 == 0) goto L_0x01d0
                android.opengl.GLSurfaceView$Renderer r0 = r0.f4729c     // Catch:{ all -> 0x0222 }
                r0.onSurfaceChanged(r6, r7, r8)     // Catch:{ all -> 0x0222 }
            L_0x01d0:
                r12 = 0
            L_0x01d1:
                java.lang.ref.WeakReference<com.badlogic.gdx.backends.android.z.d> r0 = r1.t     // Catch:{ all -> 0x0222 }
                java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0222 }
                com.badlogic.gdx.backends.android.z.d r0 = (com.badlogic.gdx.backends.android.z.d) r0     // Catch:{ all -> 0x0222 }
                if (r0 == 0) goto L_0x01e2
                android.opengl.GLSurfaceView$Renderer r0 = r0.f4729c     // Catch:{ all -> 0x0222 }
                r0.onDrawFrame(r6)     // Catch:{ all -> 0x0222 }
            L_0x01e2:
                com.badlogic.gdx.backends.android.z.d$h r0 = r1.s     // Catch:{ all -> 0x0222 }
                int r0 = r0.f()     // Catch:{ all -> 0x0222 }
                r15 = 12288(0x3000, float:1.7219E-41)
                if (r0 == r15) goto L_0x020e
                r15 = 12302(0x300e, float:1.7239E-41)
                if (r0 == r15) goto L_0x020b
                java.lang.String r15 = "GLThread"
                java.lang.String r2 = "eglSwapBuffers"
                com.badlogic.gdx.backends.android.z.d.h.a(r15, r2, r0)     // Catch:{ all -> 0x0222 }
                com.badlogic.gdx.backends.android.z.d$j r2 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x0222 }
                monitor-enter(r2)     // Catch:{ all -> 0x0222 }
                r0 = 1
                r1.f4760f = r0     // Catch:{ all -> 0x0208 }
                com.badlogic.gdx.backends.android.z.d$j r15 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x0208 }
                r15.notifyAll()     // Catch:{ all -> 0x0208 }
                monitor-exit(r2)     // Catch:{ all -> 0x0208 }
                goto L_0x020f
            L_0x0208:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0208 }
                throw r0     // Catch:{ all -> 0x0222 }
            L_0x020b:
                r0 = 1
                r3 = 1
                goto L_0x020f
            L_0x020e:
                r0 = 1
            L_0x020f:
                if (r13 == 0) goto L_0x018c
                r4 = 1
                goto L_0x018c
            L_0x0214:
                r10 = r0
            L_0x0215:
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x021f }
                r0.wait()     // Catch:{ all -> 0x021f }
                r0 = 0
                goto L_0x0021
            L_0x021f:
                r0 = move-exception
                monitor-exit(r15)     // Catch:{ all -> 0x021f }
                throw r0     // Catch:{ all -> 0x0222 }
            L_0x0222:
                r0 = move-exception
                com.badlogic.gdx.backends.android.z.d$j r2 = com.badlogic.gdx.backends.android.z.d.l
                monitor-enter(r2)
                r16.m()     // Catch:{ all -> 0x0230 }
                r16.l()     // Catch:{ all -> 0x0230 }
                monitor-exit(r2)     // Catch:{ all -> 0x0230 }
                throw r0
            L_0x0230:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0230 }
                goto L_0x0234
            L_0x0233:
                throw r0
            L_0x0234:
                goto L_0x0233
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.z.d.i.j():void");
        }

        private boolean k() {
            return !this.f4758d && this.f4759e && !this.f4760f && this.l > 0 && this.m > 0 && (this.o || this.n == 1);
        }

        private void l() {
            if (this.f4762h) {
                this.s.d();
                this.f4762h = false;
                d.l.a(this);
            }
        }

        private void m() {
            if (this.f4763i) {
                this.f4763i = false;
                this.s.c();
            }
        }

        public int b() {
            int i2;
            synchronized (d.l) {
                i2 = this.n;
            }
            return i2;
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:9|10|11|12|22|18|5) */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x000f, code lost:
            continue;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x001f */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void c() {
            /*
                r2 = this;
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l
                monitor-enter(r0)
                r1 = 1
                r2.f4757c = r1     // Catch:{ all -> 0x0029 }
                com.badlogic.gdx.backends.android.z.d$j r1 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x0029 }
                r1.notifyAll()     // Catch:{ all -> 0x0029 }
            L_0x000f:
                boolean r1 = r2.f4756b     // Catch:{ all -> 0x0029 }
                if (r1 != 0) goto L_0x0027
                boolean r1 = r2.f4758d     // Catch:{ all -> 0x0029 }
                if (r1 != 0) goto L_0x0027
                com.badlogic.gdx.backends.android.z.d$j r1 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ InterruptedException -> 0x001f }
                r1.wait()     // Catch:{ InterruptedException -> 0x001f }
                goto L_0x000f
            L_0x001f:
                java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0029 }
                r1.interrupt()     // Catch:{ all -> 0x0029 }
                goto L_0x000f
            L_0x0027:
                monitor-exit(r0)     // Catch:{ all -> 0x0029 }
                return
            L_0x0029:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0029 }
                goto L_0x002d
            L_0x002c:
                throw r1
            L_0x002d:
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.z.d.i.c():void");
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:11|12|13|14|25|20|5) */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0014, code lost:
            continue;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0028 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void d() {
            /*
                r3 = this;
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l
                monitor-enter(r0)
                r1 = 0
                r3.f4757c = r1     // Catch:{ all -> 0x0032 }
                r2 = 1
                r3.o = r2     // Catch:{ all -> 0x0032 }
                r3.p = r1     // Catch:{ all -> 0x0032 }
                com.badlogic.gdx.backends.android.z.d$j r1 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x0032 }
                r1.notifyAll()     // Catch:{ all -> 0x0032 }
            L_0x0014:
                boolean r1 = r3.f4756b     // Catch:{ all -> 0x0032 }
                if (r1 != 0) goto L_0x0030
                boolean r1 = r3.f4758d     // Catch:{ all -> 0x0032 }
                if (r1 == 0) goto L_0x0030
                boolean r1 = r3.p     // Catch:{ all -> 0x0032 }
                if (r1 != 0) goto L_0x0030
                com.badlogic.gdx.backends.android.z.d$j r1 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ InterruptedException -> 0x0028 }
                r1.wait()     // Catch:{ InterruptedException -> 0x0028 }
                goto L_0x0014
            L_0x0028:
                java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0032 }
                r1.interrupt()     // Catch:{ all -> 0x0032 }
                goto L_0x0014
            L_0x0030:
                monitor-exit(r0)     // Catch:{ all -> 0x0032 }
                return
            L_0x0032:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0032 }
                goto L_0x0036
            L_0x0035:
                throw r1
            L_0x0036:
                goto L_0x0035
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.z.d.i.d():void");
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:7|8|9|10|19|16|5) */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x000f, code lost:
            continue;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x001b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void e() {
            /*
                r2 = this;
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l
                monitor-enter(r0)
                r1 = 1
                r2.f4755a = r1     // Catch:{ all -> 0x0025 }
                com.badlogic.gdx.backends.android.z.d$j r1 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x0025 }
                r1.notifyAll()     // Catch:{ all -> 0x0025 }
            L_0x000f:
                boolean r1 = r2.f4756b     // Catch:{ all -> 0x0025 }
                if (r1 != 0) goto L_0x0023
                com.badlogic.gdx.backends.android.z.d$j r1 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ InterruptedException -> 0x001b }
                r1.wait()     // Catch:{ InterruptedException -> 0x001b }
                goto L_0x000f
            L_0x001b:
                java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0025 }
                r1.interrupt()     // Catch:{ all -> 0x0025 }
                goto L_0x000f
            L_0x0023:
                monitor-exit(r0)     // Catch:{ all -> 0x0025 }
                return
            L_0x0025:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0025 }
                goto L_0x0029
            L_0x0028:
                throw r1
            L_0x0029:
                goto L_0x0028
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.z.d.i.e():void");
        }

        public void f() {
            this.f4765k = true;
            d.l.notifyAll();
        }

        public void g() {
            synchronized (d.l) {
                this.o = true;
                d.l.notifyAll();
            }
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:11|12|13|14|25|20|5) */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0012, code lost:
            continue;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0026 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void h() {
            /*
                r2 = this;
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l
                monitor-enter(r0)
                r1 = 1
                r2.f4759e = r1     // Catch:{ all -> 0x0030 }
                r1 = 0
                r2.f4764j = r1     // Catch:{ all -> 0x0030 }
                com.badlogic.gdx.backends.android.z.d$j r1 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x0030 }
                r1.notifyAll()     // Catch:{ all -> 0x0030 }
            L_0x0012:
                boolean r1 = r2.f4761g     // Catch:{ all -> 0x0030 }
                if (r1 == 0) goto L_0x002e
                boolean r1 = r2.f4764j     // Catch:{ all -> 0x0030 }
                if (r1 != 0) goto L_0x002e
                boolean r1 = r2.f4756b     // Catch:{ all -> 0x0030 }
                if (r1 != 0) goto L_0x002e
                com.badlogic.gdx.backends.android.z.d$j r1 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ InterruptedException -> 0x0026 }
                r1.wait()     // Catch:{ InterruptedException -> 0x0026 }
                goto L_0x0012
            L_0x0026:
                java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0030 }
                r1.interrupt()     // Catch:{ all -> 0x0030 }
                goto L_0x0012
            L_0x002e:
                monitor-exit(r0)     // Catch:{ all -> 0x0030 }
                return
            L_0x0030:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0030 }
                goto L_0x0034
            L_0x0033:
                throw r1
            L_0x0034:
                goto L_0x0033
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.z.d.i.h():void");
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:9|10|11|12|22|18|5) */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x000f, code lost:
            continue;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x001f */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void i() {
            /*
                r2 = this;
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l
                monitor-enter(r0)
                r1 = 0
                r2.f4759e = r1     // Catch:{ all -> 0x0029 }
                com.badlogic.gdx.backends.android.z.d$j r1 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x0029 }
                r1.notifyAll()     // Catch:{ all -> 0x0029 }
            L_0x000f:
                boolean r1 = r2.f4761g     // Catch:{ all -> 0x0029 }
                if (r1 != 0) goto L_0x0027
                boolean r1 = r2.f4756b     // Catch:{ all -> 0x0029 }
                if (r1 != 0) goto L_0x0027
                com.badlogic.gdx.backends.android.z.d$j r1 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ InterruptedException -> 0x001f }
                r1.wait()     // Catch:{ InterruptedException -> 0x001f }
                goto L_0x000f
            L_0x001f:
                java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0029 }
                r1.interrupt()     // Catch:{ all -> 0x0029 }
                goto L_0x000f
            L_0x0027:
                monitor-exit(r0)     // Catch:{ all -> 0x0029 }
                return
            L_0x0029:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0029 }
                goto L_0x002d
            L_0x002c:
                throw r1
            L_0x002d:
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.z.d.i.i():void");
        }

        public void run() {
            setName("GLThread " + getId());
            try {
                j();
            } catch (InterruptedException unused) {
            } catch (Throwable th) {
                d.l.b(this);
                throw th;
            }
            d.l.b(this);
        }

        public boolean a() {
            return this.f4762h && this.f4763i && k();
        }

        public void a(int i2) {
            if (i2 < 0 || i2 > 1) {
                throw new IllegalArgumentException("renderMode");
            }
            synchronized (d.l) {
                this.n = i2;
                d.l.notifyAll();
            }
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:12|13|14|15|27|21|4) */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0018, code lost:
            continue;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0032 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(int r2, int r3) {
            /*
                r1 = this;
                com.badlogic.gdx.backends.android.z.d$j r0 = com.badlogic.gdx.backends.android.z.d.l
                monitor-enter(r0)
                r1.l = r2     // Catch:{ all -> 0x003c }
                r1.m = r3     // Catch:{ all -> 0x003c }
                r2 = 1
                r1.r = r2     // Catch:{ all -> 0x003c }
                r1.o = r2     // Catch:{ all -> 0x003c }
                r2 = 0
                r1.p = r2     // Catch:{ all -> 0x003c }
                com.badlogic.gdx.backends.android.z.d$j r2 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ all -> 0x003c }
                r2.notifyAll()     // Catch:{ all -> 0x003c }
            L_0x0018:
                boolean r2 = r1.f4756b     // Catch:{ all -> 0x003c }
                if (r2 != 0) goto L_0x003a
                boolean r2 = r1.f4758d     // Catch:{ all -> 0x003c }
                if (r2 != 0) goto L_0x003a
                boolean r2 = r1.p     // Catch:{ all -> 0x003c }
                if (r2 != 0) goto L_0x003a
                boolean r2 = r1.a()     // Catch:{ all -> 0x003c }
                if (r2 == 0) goto L_0x003a
                com.badlogic.gdx.backends.android.z.d$j r2 = com.badlogic.gdx.backends.android.z.d.l     // Catch:{ InterruptedException -> 0x0032 }
                r2.wait()     // Catch:{ InterruptedException -> 0x0032 }
                goto L_0x0018
            L_0x0032:
                java.lang.Thread r2 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x003c }
                r2.interrupt()     // Catch:{ all -> 0x003c }
                goto L_0x0018
            L_0x003a:
                monitor-exit(r0)     // Catch:{ all -> 0x003c }
                return
            L_0x003c:
                r2 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x003c }
                goto L_0x0040
            L_0x003f:
                throw r2
            L_0x0040:
                goto L_0x003f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.z.d.i.a(int, int):void");
        }
    }

    /* compiled from: GLSurfaceViewAPI18 */
    public interface k {
        GL a(GL gl);
    }

    /* compiled from: GLSurfaceViewAPI18 */
    static class l extends Writer {

        /* renamed from: a  reason: collision with root package name */
        private StringBuilder f4772a = new StringBuilder();

        l() {
        }

        private void d() {
            if (this.f4772a.length() > 0) {
                Log.v("GLSurfaceView", this.f4772a.toString());
                StringBuilder sb = this.f4772a;
                sb.delete(0, sb.length());
            }
        }

        public void close() {
            d();
        }

        public void flush() {
            d();
        }

        public void write(char[] cArr, int i2, int i3) {
            for (int i4 = 0; i4 < i3; i4++) {
                char c2 = cArr[i2 + i4];
                if (c2 == 10) {
                    d();
                } else {
                    this.f4772a.append(c2);
                }
            }
        }
    }

    /* compiled from: GLSurfaceViewAPI18 */
    private class m extends c {
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(d dVar, boolean z) {
            super(dVar, 8, 8, 8, 0, z ? 16 : 0, 0);
        }
    }

    public d(Context context) {
        super(context);
        f();
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        try {
            if (this.f4728b != null) {
                this.f4728b.e();
            }
        } finally {
            super.finalize();
        }
    }

    public int getDebugFlags() {
        return this.f4735i;
    }

    public boolean getPreserveEGLContextOnPause() {
        return this.f4737k;
    }

    public int getRenderMode() {
        return this.f4728b.b();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f4730d && this.f4729c != null) {
            i iVar = this.f4728b;
            int b2 = iVar != null ? iVar.b() : 1;
            this.f4728b = new i(this.f4727a);
            if (b2 != 1) {
                this.f4728b.a(b2);
            }
            this.f4728b.start();
        }
        this.f4730d = false;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        i iVar = this.f4728b;
        if (iVar != null) {
            iVar.e();
        }
        this.f4730d = true;
        super.onDetachedFromWindow();
    }

    public void setDebugFlags(int i2) {
        this.f4735i = i2;
    }

    public void setEGLConfigChooser(GLSurfaceView.EGLConfigChooser eGLConfigChooser) {
        e();
        this.f4731e = eGLConfigChooser;
    }

    public void setEGLContextClientVersion(int i2) {
        e();
        this.f4736j = i2;
    }

    public void setEGLContextFactory(f fVar) {
        e();
        this.f4732f = fVar;
    }

    public void setEGLWindowSurfaceFactory(g gVar) {
        e();
        this.f4733g = gVar;
    }

    public void setGLWrapper(k kVar) {
        this.f4734h = kVar;
    }

    public void setPreserveEGLContextOnPause(boolean z) {
        this.f4737k = z;
    }

    public void setRenderMode(int i2) {
        this.f4728b.a(i2);
    }

    public void setRenderer(GLSurfaceView.Renderer renderer) {
        e();
        if (this.f4731e == null) {
            this.f4731e = new m(this, true);
        }
        if (this.f4732f == null) {
            this.f4732f = new C0114d();
        }
        if (this.f4733g == null) {
            this.f4733g = new e();
        }
        this.f4729c = renderer;
        this.f4728b = new i(this.f4727a);
        this.f4728b.start();
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.f4728b.a(i3, i4);
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.f4728b.h();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.f4728b.i();
    }

    /* compiled from: GLSurfaceViewAPI18 */
    private static class e implements g {
        private e() {
        }

        public EGLSurface a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, Object obj) {
            try {
                return egl10.eglCreateWindowSurface(eGLDisplay, eGLConfig, obj, null);
            } catch (IllegalArgumentException e2) {
                Log.e("GLSurfaceViewAPI18", "eglCreateWindowSurface", e2);
                return null;
            }
        }

        public void a(EGL10 egl10, EGLDisplay eGLDisplay, EGLSurface eGLSurface) {
            egl10.eglDestroySurface(eGLDisplay, eGLSurface);
        }
    }

    private void e() {
        if (this.f4728b != null) {
            throw new IllegalStateException("setRenderer has already been called for this instance.");
        }
    }

    private void f() {
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        if (Build.VERSION.SDK_INT <= 8) {
            holder.setFormat(4);
        }
    }

    public void a(int i2, int i3, int i4, int i5, int i6, int i7) {
        setEGLConfigChooser(new c(this, i2, i3, i4, i5, i6, i7));
    }

    public void b() {
        this.f4728b.d();
    }

    public void c() {
        this.f4728b.g();
    }

    /* compiled from: GLSurfaceViewAPI18 */
    private static class j {

        /* renamed from: a  reason: collision with root package name */
        private boolean f4766a;

        /* renamed from: b  reason: collision with root package name */
        private int f4767b;

        /* renamed from: c  reason: collision with root package name */
        private boolean f4768c;

        /* renamed from: d  reason: collision with root package name */
        private boolean f4769d;

        /* renamed from: e  reason: collision with root package name */
        private boolean f4770e;

        /* renamed from: f  reason: collision with root package name */
        private i f4771f;

        private j() {
        }

        public void a(i iVar) {
            if (this.f4771f == iVar) {
                this.f4771f = null;
            }
            notifyAll();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.badlogic.gdx.backends.android.z.d.i.a(com.badlogic.gdx.backends.android.z.d$i, boolean):boolean
         arg types: [com.badlogic.gdx.backends.android.z.d$i, int]
         candidates:
          com.badlogic.gdx.backends.android.z.d.i.a(int, int):void
          com.badlogic.gdx.backends.android.z.d.i.a(com.badlogic.gdx.backends.android.z.d$i, boolean):boolean */
        public synchronized void b(i iVar) {
            boolean unused = iVar.f4756b = true;
            if (this.f4771f == iVar) {
                this.f4771f = null;
            }
            notifyAll();
        }

        public boolean c(i iVar) {
            i iVar2 = this.f4771f;
            if (iVar2 == iVar || iVar2 == null) {
                this.f4771f = iVar;
                notifyAll();
                return true;
            }
            c();
            if (this.f4769d) {
                return true;
            }
            i iVar3 = this.f4771f;
            if (iVar3 == null) {
                return false;
            }
            iVar3.f();
            return false;
        }

        public synchronized boolean a() {
            return this.f4770e;
        }

        public synchronized void a(GL10 gl10) {
            if (!this.f4768c) {
                c();
                String glGetString = gl10.glGetString(7937);
                boolean z = false;
                if (this.f4767b < 131072) {
                    this.f4769d = !glGetString.startsWith("Q3Dimension MSM7500 ");
                    notifyAll();
                }
                if (!this.f4769d) {
                    z = true;
                }
                this.f4770e = z;
                this.f4768c = true;
            }
        }

        public synchronized boolean b() {
            c();
            return !this.f4769d;
        }

        private void c() {
            if (!this.f4766a) {
                this.f4767b = MetadataChangeSet.INDEXABLE_TEXT_SIZE_LIMIT_BYTES;
                if (this.f4767b >= 131072) {
                    this.f4769d = true;
                }
                this.f4766a = true;
            }
        }
    }

    public void a() {
        this.f4728b.c();
    }

    public void setEGLConfigChooser(boolean z) {
        setEGLConfigChooser(new m(this, z));
    }

    /* compiled from: GLSurfaceViewAPI18 */
    private class c extends b {

        /* renamed from: c  reason: collision with root package name */
        private int[] f4740c = new int[1];

        /* renamed from: d  reason: collision with root package name */
        protected int f4741d;

        /* renamed from: e  reason: collision with root package name */
        protected int f4742e;

        /* renamed from: f  reason: collision with root package name */
        protected int f4743f;

        /* renamed from: g  reason: collision with root package name */
        protected int f4744g;

        /* renamed from: h  reason: collision with root package name */
        protected int f4745h;

        /* renamed from: i  reason: collision with root package name */
        protected int f4746i;

        public c(d dVar, int i2, int i3, int i4, int i5, int i6, int i7) {
            super(new int[]{12324, i2, 12323, i3, 12322, i4, 12321, i5, 12325, i6, 12326, i7, 12344});
            this.f4741d = i2;
            this.f4742e = i3;
            this.f4743f = i4;
            this.f4744g = i5;
            this.f4745h = i6;
            this.f4746i = i7;
        }

        public EGLConfig a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr) {
            for (EGLConfig eGLConfig : eGLConfigArr) {
                EGL10 egl102 = egl10;
                EGLDisplay eGLDisplay2 = eGLDisplay;
                EGLConfig eGLConfig2 = eGLConfig;
                int a2 = a(egl102, eGLDisplay2, eGLConfig2, 12325, 0);
                int a3 = a(egl102, eGLDisplay2, eGLConfig2, 12326, 0);
                if (a2 >= this.f4745h && a3 >= this.f4746i) {
                    EGL10 egl103 = egl10;
                    EGLDisplay eGLDisplay3 = eGLDisplay;
                    EGLConfig eGLConfig3 = eGLConfig;
                    int a4 = a(egl103, eGLDisplay3, eGLConfig3, 12324, 0);
                    int a5 = a(egl103, eGLDisplay3, eGLConfig3, 12323, 0);
                    int a6 = a(egl103, eGLDisplay3, eGLConfig3, 12322, 0);
                    int a7 = a(egl103, eGLDisplay3, eGLConfig3, 12321, 0);
                    if (a4 == this.f4741d && a5 == this.f4742e && a6 == this.f4743f && a7 == this.f4744g) {
                        return eGLConfig;
                    }
                }
            }
            return null;
        }

        private int a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i2, int i3) {
            return egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i2, this.f4740c) ? this.f4740c[0] : i3;
        }
    }

    /* compiled from: GLSurfaceViewAPI18 */
    private static class h {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<d> f4749a;

        /* renamed from: b  reason: collision with root package name */
        EGL10 f4750b;

        /* renamed from: c  reason: collision with root package name */
        EGLDisplay f4751c;

        /* renamed from: d  reason: collision with root package name */
        EGLSurface f4752d;

        /* renamed from: e  reason: collision with root package name */
        EGLConfig f4753e;

        /* renamed from: f  reason: collision with root package name */
        EGLContext f4754f;

        public h(WeakReference<d> weakReference) {
            this.f4749a = weakReference;
        }

        private void g() {
            EGLSurface eGLSurface;
            EGLSurface eGLSurface2 = this.f4752d;
            if (eGLSurface2 != null && eGLSurface2 != (eGLSurface = EGL10.EGL_NO_SURFACE)) {
                this.f4750b.eglMakeCurrent(this.f4751c, eGLSurface, eGLSurface, EGL10.EGL_NO_CONTEXT);
                d dVar = this.f4749a.get();
                if (dVar != null) {
                    dVar.f4733g.a(this.f4750b, this.f4751c, this.f4752d);
                }
                this.f4752d = null;
            }
        }

        /* access modifiers changed from: package-private */
        public GL a() {
            GL gl = this.f4754f.getGL();
            d dVar = this.f4749a.get();
            if (dVar == null) {
                return gl;
            }
            if (dVar.f4734h != null) {
                gl = dVar.f4734h.a(gl);
            }
            if ((dVar.f4735i & 3) == 0) {
                return gl;
            }
            int i2 = 0;
            l lVar = null;
            if ((dVar.f4735i & 1) != 0) {
                i2 = 1;
            }
            if ((dVar.f4735i & 2) != 0) {
                lVar = new l();
            }
            return GLDebugHelper.wrap(gl, i2, lVar);
        }

        public boolean b() {
            if (this.f4750b == null) {
                throw new RuntimeException("egl not initialized");
            } else if (this.f4751c == null) {
                throw new RuntimeException("eglDisplay not initialized");
            } else if (this.f4753e != null) {
                g();
                d dVar = this.f4749a.get();
                if (dVar != null) {
                    this.f4752d = dVar.f4733g.a(this.f4750b, this.f4751c, this.f4753e, dVar.getHolder());
                } else {
                    this.f4752d = null;
                }
                EGLSurface eGLSurface = this.f4752d;
                if (eGLSurface == null || eGLSurface == EGL10.EGL_NO_SURFACE) {
                    if (this.f4750b.eglGetError() == 12299) {
                        Log.e("EglHelper", "createWindowSurface returned EGL_BAD_NATIVE_WINDOW.");
                    }
                    return false;
                } else if (this.f4750b.eglMakeCurrent(this.f4751c, eGLSurface, eGLSurface, this.f4754f)) {
                    return true;
                } else {
                    a("EGLHelper", "eglMakeCurrent", this.f4750b.eglGetError());
                    return false;
                }
            } else {
                throw new RuntimeException("mEglConfig not initialized");
            }
        }

        public void c() {
            g();
        }

        public void d() {
            if (this.f4754f != null) {
                d dVar = this.f4749a.get();
                if (dVar != null) {
                    dVar.f4732f.destroyContext(this.f4750b, this.f4751c, this.f4754f);
                }
                this.f4754f = null;
            }
            EGLDisplay eGLDisplay = this.f4751c;
            if (eGLDisplay != null) {
                this.f4750b.eglTerminate(eGLDisplay);
                this.f4751c = null;
            }
        }

        public void e() {
            this.f4750b = (EGL10) EGLContext.getEGL();
            this.f4751c = this.f4750b.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            EGLDisplay eGLDisplay = this.f4751c;
            if (eGLDisplay != EGL10.EGL_NO_DISPLAY) {
                if (this.f4750b.eglInitialize(eGLDisplay, new int[2])) {
                    d dVar = this.f4749a.get();
                    if (dVar == null) {
                        this.f4753e = null;
                        this.f4754f = null;
                    } else {
                        this.f4753e = dVar.f4731e.chooseConfig(this.f4750b, this.f4751c);
                        this.f4754f = dVar.f4732f.createContext(this.f4750b, this.f4751c, this.f4753e);
                    }
                    EGLContext eGLContext = this.f4754f;
                    if (eGLContext == null || eGLContext == EGL10.EGL_NO_CONTEXT) {
                        this.f4754f = null;
                        a("createContext");
                        throw null;
                    }
                    this.f4752d = null;
                    return;
                }
                throw new RuntimeException("eglInitialize failed");
            }
            throw new RuntimeException("eglGetDisplay failed");
        }

        public int f() {
            if (!this.f4750b.eglSwapBuffers(this.f4751c, this.f4752d)) {
                return this.f4750b.eglGetError();
            }
            return 12288;
        }

        private void a(String str) {
            b(str, this.f4750b.eglGetError());
            throw null;
        }

        public static void a(String str, String str2, int i2) {
            Log.w(str, a(str2, i2));
        }

        private static String a(int i2) {
            switch (i2) {
                case 12288:
                    return "EGL_SUCCESS";
                case 12289:
                    return "EGL_NOT_INITIALIZED";
                case 12290:
                    return "EGL_BAD_ACCESS";
                case 12291:
                    return "EGL_BAD_ALLOC";
                case 12292:
                    return "EGL_BAD_ATTRIBUTE";
                case 12293:
                    return "EGL_BAD_CONFIG";
                case 12294:
                    return "EGL_BAD_CONTEXT";
                case 12295:
                    return "EGL_BAD_CURRENT_SURFACE";
                case 12296:
                    return "EGL_BAD_DISPLAY";
                case 12297:
                    return "EGL_BAD_MATCH";
                case 12298:
                    return "EGL_BAD_NATIVE_PIXMAP";
                case 12299:
                    return "EGL_BAD_NATIVE_WINDOW";
                case 12300:
                    return "EGL_BAD_PARAMETER";
                case 12301:
                    return "EGL_BAD_SURFACE";
                case 12302:
                    return "EGL_CONTEXT_LOST";
                default:
                    return "0x" + Integer.toHexString(i2);
            }
        }

        public static String a(String str, int i2) {
            return str + " failed: " + a(i2);
        }

        public static void b(String str, int i2) {
            throw new RuntimeException(a(str, i2));
        }
    }
}
