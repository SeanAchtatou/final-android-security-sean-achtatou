package com.badlogic.gdx.backends.android;

import android.content.Context;
import android.os.Build;
import e.d.b.a;

/* compiled from: AndroidInputFactory */
public class m {
    public static l a(a aVar, Context context, Object obj, b bVar) {
        Class<?> cls;
        try {
            if (Build.VERSION.SDK_INT >= 12) {
                cls = Class.forName("com.badlogic.gdx.backends.android.n");
            } else {
                cls = Class.forName("com.badlogic.gdx.backends.android.l");
            }
            return (l) cls.getConstructor(a.class, Context.class, Object.class, b.class).newInstance(aVar, context, obj, bVar);
        } catch (Exception e2) {
            throw new RuntimeException("Couldn't construct AndroidInput, this should never happen", e2);
        }
    }
}
