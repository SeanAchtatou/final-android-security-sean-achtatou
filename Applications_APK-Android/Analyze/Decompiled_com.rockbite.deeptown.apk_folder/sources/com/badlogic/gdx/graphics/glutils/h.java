package com.badlogic.gdx.graphics.glutils;

import e.d.b.g;

/* compiled from: HdpiUtils */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private static g f5130a = g.Logical;

    public static void a(int i2, int i3, int i4, int i5) {
        if (f5130a != g.Logical || (g.f6801b.getWidth() == g.f6801b.b() && g.f6801b.getHeight() == g.f6801b.e())) {
            g.f6806g.g(i2, i3, i4, i5);
        } else {
            g.f6806g.g(a(i2), b(i3), a(i4), b(i5));
        }
    }

    public static void b(int i2, int i3, int i4, int i5) {
        if (f5130a != g.Logical || (g.f6801b.getWidth() == g.f6801b.b() && g.f6801b.getHeight() == g.f6801b.e())) {
            g.f6806g.f(i2, i3, i4, i5);
        } else {
            g.f6806g.f(a(i2), b(i3), a(i4), b(i5));
        }
    }

    public static int a(int i2) {
        return (int) (((float) (i2 * g.f6801b.b())) / ((float) g.f6801b.getWidth()));
    }

    public static int b(int i2) {
        return (int) (((float) (i2 * g.f6801b.e())) / ((float) g.f6801b.getHeight()));
    }
}
