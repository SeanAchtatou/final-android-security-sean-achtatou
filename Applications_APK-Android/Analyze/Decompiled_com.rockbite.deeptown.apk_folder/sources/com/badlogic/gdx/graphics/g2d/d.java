package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.g2d.c;
import com.badlogic.gdx.graphics.g2d.e;
import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.g0;
import com.badlogic.gdx.utils.m;
import com.badlogic.gdx.utils.q;
import com.esotericsoftware.spine.Animation;
import e.d.b.t.b;

/* compiled from: BitmapFontCache */
public class d {
    private static final b n = new b(1.0f, 1.0f, 1.0f, 1.0f);

    /* renamed from: a  reason: collision with root package name */
    private final c f4831a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f4832b;

    /* renamed from: c  reason: collision with root package name */
    private final a<e> f4833c;

    /* renamed from: d  reason: collision with root package name */
    private final a<e> f4834d;

    /* renamed from: e  reason: collision with root package name */
    private int f4835e;

    /* renamed from: f  reason: collision with root package name */
    private float f4836f;

    /* renamed from: g  reason: collision with root package name */
    private float f4837g;

    /* renamed from: h  reason: collision with root package name */
    private final b f4838h;

    /* renamed from: i  reason: collision with root package name */
    private float f4839i;

    /* renamed from: j  reason: collision with root package name */
    private float[][] f4840j;

    /* renamed from: k  reason: collision with root package name */
    private int[] f4841k;
    private q[] l;
    private int[] m;

    public d(c cVar) {
        this(cVar, cVar.s());
    }

    private void c(e eVar, float f2, float f3) {
        int i2;
        int i3 = this.f4831a.f4804b.f5374b;
        float[][] fArr = this.f4840j;
        if (fArr.length < i3) {
            float[][] fArr2 = new float[i3][];
            System.arraycopy(fArr, 0, fArr2, 0, fArr.length);
            this.f4840j = fArr2;
            int[] iArr = new int[i3];
            int[] iArr2 = this.f4841k;
            System.arraycopy(iArr2, 0, iArr, 0, iArr2.length);
            this.f4841k = iArr;
            q[] qVarArr = new q[i3];
            q[] qVarArr2 = this.l;
            if (qVarArr2 != null) {
                i2 = qVarArr2.length;
                System.arraycopy(qVarArr2, 0, qVarArr, 0, qVarArr2.length);
            } else {
                i2 = 0;
            }
            while (i2 < i3) {
                qVarArr[i2] = new q();
                i2++;
            }
            this.l = qVarArr;
            this.m = new int[i3];
        }
        this.f4833c.add(eVar);
        a(eVar);
        int i4 = eVar.f4842a.f5374b;
        for (int i5 = 0; i5 < i4; i5++) {
            e.a aVar = eVar.f4842a.get(i5);
            a<c.b> aVar2 = aVar.f4846a;
            m mVar = aVar.f4847b;
            float b2 = aVar.f4851f.b();
            float f4 = aVar.f4849d + f3;
            int i6 = aVar2.f5374b;
            float f5 = aVar.f4848c + f2;
            for (int i7 = 0; i7 < i6; i7++) {
                f5 += mVar.b(i7);
                a(aVar2.get(i7), f5, f4, b2);
            }
        }
        this.f4839i = b.f6932j;
    }

    public void a(float f2, float f3) {
        b(f2 - this.f4836f, f3 - this.f4837g);
    }

    public void b(float f2, float f3) {
        if (f2 != Animation.CurveTimeline.LINEAR || f3 != Animation.CurveTimeline.LINEAR) {
            if (this.f4832b) {
                f2 = (float) Math.round(f2);
                f3 = (float) Math.round(f3);
            }
            this.f4836f += f2;
            this.f4837g += f3;
            float[][] fArr = this.f4840j;
            int length = fArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                float[] fArr2 = fArr[i2];
                int i3 = this.f4841k[i2];
                for (int i4 = 0; i4 < i3; i4 += 5) {
                    fArr2[i4] = fArr2[i4] + f2;
                    int i5 = i4 + 1;
                    fArr2[i5] = fArr2[i5] + f3;
                }
            }
        }
    }

    public d(c cVar, boolean z) {
        this.f4833c = new a<>();
        this.f4834d = new a<>();
        this.f4838h = new b(1.0f, 1.0f, 1.0f, 1.0f);
        this.f4831a = cVar;
        this.f4832b = z;
        int i2 = cVar.f4804b.f5374b;
        if (i2 != 0) {
            this.f4840j = new float[i2][];
            this.f4841k = new int[i2];
            if (i2 > 1) {
                this.l = new q[i2];
                int length = this.l.length;
                for (int i3 = 0; i3 < length; i3++) {
                    this.l[i3] = new q();
                }
            }
            this.m = new int[i2];
            return;
        }
        throw new IllegalArgumentException("The specified font must contain at least one texture page.");
    }

    public void a(b bVar) {
        float b2 = bVar.b();
        if (this.f4839i != b2) {
            this.f4839i = b2;
            int[] iArr = this.m;
            int length = iArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                iArr[i2] = 0;
            }
            int i3 = this.f4833c.f5374b;
            for (int i4 = 0; i4 < i3; i4++) {
                e eVar = this.f4833c.get(i4);
                int i5 = eVar.f4842a.f5374b;
                for (int i6 = 0; i6 < i5; i6++) {
                    e.a aVar = eVar.f4842a.get(i6);
                    a<c.b> aVar2 = aVar.f4846a;
                    b bVar2 = n;
                    bVar2.b(aVar.f4851f);
                    bVar2.a(bVar);
                    float b3 = bVar2.b();
                    int i7 = aVar2.f5374b;
                    for (int i8 = 0; i8 < i7; i8++) {
                        int i9 = aVar2.get(i8).o;
                        int i10 = (iArr[i9] * 20) + 2;
                        iArr[i9] = iArr[i9] + 1;
                        float[] fArr = this.f4840j[i9];
                        for (int i11 = 0; i11 < 20; i11 += 5) {
                            fArr[i10 + i11] = b3;
                        }
                    }
                }
            }
        }
    }

    public b b() {
        return this.f4838h;
    }

    public void b(e eVar, float f2, float f3) {
        a();
        a(eVar, f2, f3);
    }

    public void a(b bVar) {
        a<r> n2 = this.f4831a.n();
        int length = this.f4840j.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (this.f4841k[i2] > 0) {
                bVar.draw(n2.get(i2).e(), this.f4840j[i2], 0, this.f4841k[i2]);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.g0.a(com.badlogic.gdx.utils.a, boolean):void
     arg types: [com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.e>, int]
     candidates:
      com.badlogic.gdx.utils.g0.a(java.lang.Class, int):com.badlogic.gdx.utils.f0<T>
      com.badlogic.gdx.utils.g0.a(com.badlogic.gdx.utils.a, boolean):void */
    public void a() {
        this.f4836f = Animation.CurveTimeline.LINEAR;
        this.f4837g = Animation.CurveTimeline.LINEAR;
        g0.a((a) this.f4834d, true);
        this.f4834d.clear();
        this.f4833c.clear();
        int length = this.f4841k.length;
        for (int i2 = 0; i2 < length; i2++) {
            q[] qVarArr = this.l;
            if (qVarArr != null) {
                qVarArr[i2].a();
            }
            this.f4841k[i2] = 0;
        }
    }

    public c c() {
        return this.f4831a;
    }

    private void a(e eVar) {
        if (this.f4840j.length == 1) {
            int i2 = eVar.f4842a.f5374b;
            int i3 = 0;
            for (int i4 = 0; i4 < i2; i4++) {
                i3 += eVar.f4842a.get(i4).f4846a.f5374b;
            }
            a(0, i3);
            return;
        }
        int[] iArr = this.m;
        int length = iArr.length;
        for (int i5 = 0; i5 < length; i5++) {
            iArr[i5] = 0;
        }
        int i6 = eVar.f4842a.f5374b;
        for (int i7 = 0; i7 < i6; i7++) {
            a<c.b> aVar = eVar.f4842a.get(i7).f4846a;
            int i8 = aVar.f5374b;
            for (int i9 = 0; i9 < i8; i9++) {
                int i10 = aVar.get(i9).o;
                iArr[i10] = iArr[i10] + 1;
            }
        }
        int length2 = iArr.length;
        for (int i11 = 0; i11 < length2; i11++) {
            a(i11, iArr[i11]);
        }
    }

    private void a(int i2, int i3) {
        q[] qVarArr = this.l;
        if (qVarArr != null && i3 > qVarArr[i2].f5558a.length) {
            qVarArr[i2].b(i3 - qVarArr[i2].f5558a.length);
        }
        int[] iArr = this.f4841k;
        int i4 = iArr[i2] + (i3 * 20);
        float[][] fArr = this.f4840j;
        float[] fArr2 = fArr[i2];
        if (fArr2 == null) {
            fArr[i2] = new float[i4];
        } else if (fArr2.length < i4) {
            float[] fArr3 = new float[i4];
            System.arraycopy(fArr2, 0, fArr3, 0, iArr[i2]);
            this.f4840j[i2] = fArr3;
        }
    }

    private void a(c.b bVar, float f2, float f3, float f4) {
        c.a aVar = this.f4831a.f4803a;
        float f5 = aVar.o;
        float f6 = aVar.p;
        float f7 = f2 + (((float) bVar.f4829j) * f5);
        float f8 = f3 + (((float) bVar.f4830k) * f6);
        float f9 = ((float) bVar.f4823d) * f5;
        float f10 = ((float) bVar.f4824e) * f6;
        float f11 = bVar.f4825f;
        float f12 = bVar.f4827h;
        float f13 = bVar.f4826g;
        float f14 = bVar.f4828i;
        if (this.f4832b) {
            f7 = (float) Math.round(f7);
            f8 = (float) Math.round(f8);
            f9 = (float) Math.round(f9);
            f10 = (float) Math.round(f10);
        }
        float f15 = f9 + f7;
        float f16 = f10 + f8;
        int i2 = bVar.o;
        int[] iArr = this.f4841k;
        int i3 = iArr[i2];
        iArr[i2] = iArr[i2] + 20;
        q[] qVarArr = this.l;
        if (qVarArr != null) {
            q qVar = qVarArr[i2];
            int i4 = this.f4835e;
            this.f4835e = i4 + 1;
            qVar.a(i4);
        }
        float[] fArr = this.f4840j[i2];
        int i5 = i3 + 1;
        fArr[i3] = f7;
        int i6 = i5 + 1;
        fArr[i5] = f8;
        int i7 = i6 + 1;
        fArr[i6] = f4;
        int i8 = i7 + 1;
        fArr[i7] = f11;
        int i9 = i8 + 1;
        fArr[i8] = f13;
        int i10 = i9 + 1;
        fArr[i9] = f7;
        int i11 = i10 + 1;
        fArr[i10] = f16;
        int i12 = i11 + 1;
        fArr[i11] = f4;
        int i13 = i12 + 1;
        fArr[i12] = f11;
        int i14 = i13 + 1;
        fArr[i13] = f14;
        int i15 = i14 + 1;
        fArr[i14] = f15;
        int i16 = i15 + 1;
        fArr[i15] = f16;
        int i17 = i16 + 1;
        fArr[i16] = f4;
        int i18 = i17 + 1;
        fArr[i17] = f12;
        int i19 = i18 + 1;
        fArr[i18] = f14;
        int i20 = i19 + 1;
        fArr[i19] = f15;
        int i21 = i20 + 1;
        fArr[i20] = f8;
        int i22 = i21 + 1;
        fArr[i21] = f4;
        fArr[i22] = f12;
        fArr[i22 + 1] = f13;
    }

    public e a(CharSequence charSequence, float f2, float f3, float f4, int i2, boolean z) {
        a();
        return a(charSequence, f2, f3, 0, charSequence.length(), f4, i2, z);
    }

    public e a(CharSequence charSequence, float f2, float f3, int i2, int i3, float f4, int i4, boolean z) {
        return a(charSequence, f2, f3, i2, i3, f4, i4, z, null);
    }

    public e a(CharSequence charSequence, float f2, float f3, int i2, int i3, float f4, int i4, boolean z, String str) {
        e eVar = (e) g0.b(e.class);
        this.f4834d.add(eVar);
        eVar.a(this.f4831a, charSequence, i2, i3, this.f4838h, f4, i4, z, str);
        a(eVar, f2, f3);
        return eVar;
    }

    public void a(e eVar, float f2, float f3) {
        c(eVar, f2, f3 + this.f4831a.f4803a.f4819k);
    }
}
