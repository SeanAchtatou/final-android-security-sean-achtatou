package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.o;
import e.d.b.g;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

/* compiled from: IndexBufferObject */
public class l implements n {

    /* renamed from: a  reason: collision with root package name */
    final ShortBuffer f5145a;

    /* renamed from: b  reason: collision with root package name */
    final ByteBuffer f5146b;

    /* renamed from: c  reason: collision with root package name */
    final boolean f5147c;

    /* renamed from: d  reason: collision with root package name */
    int f5148d;

    /* renamed from: e  reason: collision with root package name */
    boolean f5149e = true;

    /* renamed from: f  reason: collision with root package name */
    boolean f5150f;

    /* renamed from: g  reason: collision with root package name */
    final int f5151g;

    /* renamed from: h  reason: collision with root package name */
    private final boolean f5152h;

    public l(boolean z, int i2) {
        boolean z2 = false;
        this.f5150f = false;
        this.f5152h = i2 == 0 ? true : z2;
        this.f5146b = BufferUtils.d((this.f5152h ? 1 : i2) * 2);
        this.f5145a = this.f5146b.asShortBuffer();
        this.f5147c = true;
        this.f5145a.flip();
        this.f5146b.flip();
        this.f5148d = g.f6807h.a();
        this.f5151g = z ? 35044 : 35048;
    }

    public void a(short[] sArr, int i2, int i3) {
        this.f5149e = true;
        this.f5145a.clear();
        this.f5145a.put(sArr, i2, i3);
        this.f5145a.flip();
        this.f5146b.position(0);
        this.f5146b.limit(i3 << 1);
        if (this.f5150f) {
            g.f6807h.a(34963, this.f5146b.limit(), this.f5146b, this.f5151g);
            this.f5149e = false;
        }
    }

    public void b() {
        this.f5148d = g.f6807h.a();
        this.f5149e = true;
    }

    public void dispose() {
        g.f6807h.e(34963, 0);
        g.f6807h.e(this.f5148d);
        this.f5148d = 0;
        if (this.f5147c) {
            BufferUtils.a(this.f5146b);
        }
    }

    public void e() {
        g.f6807h.e(34963, 0);
        this.f5150f = false;
    }

    public void f() {
        int i2 = this.f5148d;
        if (i2 != 0) {
            g.f6807h.e(34963, i2);
            if (this.f5149e) {
                this.f5146b.limit(this.f5145a.limit() * 2);
                g.f6807h.a(34963, this.f5146b.limit(), this.f5146b, this.f5151g);
                this.f5149e = false;
            }
            this.f5150f = true;
            return;
        }
        throw new o("No buffer allocated!");
    }

    public int g() {
        if (this.f5152h) {
            return 0;
        }
        return this.f5145a.limit();
    }

    public int h() {
        if (this.f5152h) {
            return 0;
        }
        return this.f5145a.capacity();
    }

    public ShortBuffer a() {
        this.f5149e = true;
        return this.f5145a;
    }
}
