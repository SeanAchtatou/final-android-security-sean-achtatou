package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.graphics.glutils.ETC1;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.o;
import com.badlogic.gdx.utils.q0;
import e.d.b.g;
import e.d.b.s.a;
import e.d.b.t.e;
import e.d.b.t.l;
import e.d.b.t.q;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.zip.GZIPInputStream;

/* compiled from: KTXTextureData */
public class p implements q, e {

    /* renamed from: a  reason: collision with root package name */
    private a f5159a;

    /* renamed from: b  reason: collision with root package name */
    private int f5160b;

    /* renamed from: c  reason: collision with root package name */
    private int f5161c;

    /* renamed from: d  reason: collision with root package name */
    private int f5162d;

    /* renamed from: e  reason: collision with root package name */
    private int f5163e = -1;

    /* renamed from: f  reason: collision with root package name */
    private int f5164f = -1;

    /* renamed from: g  reason: collision with root package name */
    private int f5165g = -1;

    /* renamed from: h  reason: collision with root package name */
    private int f5166h;

    /* renamed from: i  reason: collision with root package name */
    private int f5167i;

    /* renamed from: j  reason: collision with root package name */
    private int f5168j;

    /* renamed from: k  reason: collision with root package name */
    private int f5169k;
    private ByteBuffer l;
    private boolean m;

    public p(a aVar, boolean z) {
        this.f5159a = aVar;
        this.m = z;
    }

    public void a(int i2) {
        boolean z;
        int i3;
        int i4;
        int i5;
        boolean z2;
        int i6;
        int i7 = i2;
        if (this.l != null) {
            IntBuffer c2 = BufferUtils.c(16);
            int i8 = 1;
            if (this.f5160b != 0 && this.f5161c != 0) {
                z = false;
            } else if (this.f5160b + this.f5161c == 0) {
                z = true;
            } else {
                throw new o("either both or none of glType, glFormat must be zero");
            }
            if (this.f5164f > 0) {
                i4 = 2;
                i3 = 3553;
            } else {
                i4 = 1;
                i3 = 4660;
            }
            if (this.f5165g > 0) {
                i4 = 3;
                i3 = 4660;
            }
            int i9 = this.f5167i;
            if (i9 == 6) {
                if (i4 == 2) {
                    i3 = 34067;
                } else {
                    throw new o("cube map needs 2D faces");
                }
            } else if (i9 != 1) {
                throw new o("numberOfFaces must be either 1 or 6");
            }
            if (this.f5166h > 0) {
                if (i3 == 4660 || i3 == 3553) {
                    i4++;
                    i3 = 4660;
                } else {
                    throw new o("No API for 3D and cube arrays yet");
                }
            }
            if (i3 != 4660) {
                int i10 = -1;
                int i11 = 34069;
                if (this.f5167i != 6 || i7 == 34067) {
                    if (!(this.f5167i == 6 && i7 == 34067)) {
                        if (i7 == i3 || (34069 <= i7 && i7 <= 34074 && i7 == 3553)) {
                            i11 = i7;
                        } else {
                            throw new o("Invalid target requested : 0x" + Integer.toHexString(i2) + ", expecting : 0x" + Integer.toHexString(i3));
                        }
                    }
                    i5 = -1;
                } else if (34069 > i7 || i7 > 34074) {
                    throw new o("You must specify either GL_TEXTURE_CUBE_MAP to bind all 6 faces of the cube or the requested face GL_TEXTURE_CUBE_MAP_POSITIVE_X and followings.");
                } else {
                    i5 = i7 - 34069;
                }
                g.f6806g.c(3317, c2);
                int i12 = c2.get(0);
                int i13 = 4;
                if (i12 != 4) {
                    g.f6806g.f(3317, 4);
                }
                int i14 = this.f5162d;
                int i15 = this.f5161c;
                int i16 = this.f5169k;
                int i17 = 0;
                while (i17 < this.f5168j) {
                    int max = Math.max(i8, this.f5163e >> i17);
                    int max2 = Math.max(i8, this.f5164f >> i17);
                    Math.max(i8, this.f5165g >> i17);
                    this.l.position(i16);
                    int i18 = this.l.getInt();
                    int i19 = (i18 + 3) & -4;
                    i16 += i13;
                    int i20 = max2;
                    int i21 = 0;
                    while (i21 < this.f5167i) {
                        this.l.position(i16);
                        i16 += i19;
                        if (i5 == i10 || i5 == i21) {
                            ByteBuffer slice = this.l.slice();
                            slice.limit(i19);
                            if (i4 != 1) {
                                if (i4 == 2) {
                                    int i22 = this.f5166h;
                                    if (i22 <= 0) {
                                        i22 = i20;
                                    }
                                    if (z) {
                                        i6 = i5;
                                        if (i14 == ETC1.f5068b) {
                                            z2 = z;
                                            if (!g.f6801b.a("GL_OES_compressed_ETC1_RGB8_texture")) {
                                                l a2 = ETC1.a(new ETC1.a(max, i22, slice, 0), l.c.RGB888);
                                                g.f6806g.b(i11 + i21, i17, a2.m(), a2.q(), a2.o(), 0, a2.l(), a2.n(), a2.p());
                                                a2.dispose();
                                            } else {
                                                g.f6806g.a(i11 + i21, i17, i14, max, i22, 0, i18, slice);
                                            }
                                        } else {
                                            z2 = z;
                                            g.f6806g.a(i11 + i21, i17, i14, max, i22, 0, i18, slice);
                                        }
                                    } else {
                                        i6 = i5;
                                        z2 = z;
                                        g.f6806g.b(i11 + i21, i17, i14, max, i22, 0, i15, this.f5160b, slice);
                                    }
                                    i20 = i22;
                                } else {
                                    i6 = i5;
                                    z2 = z;
                                    if (i4 == 3) {
                                        int i23 = this.f5166h;
                                    }
                                }
                                i21++;
                                i5 = i6;
                                z = z2;
                                i10 = -1;
                            }
                        }
                        i6 = i5;
                        z2 = z;
                        i21++;
                        i5 = i6;
                        z = z2;
                        i10 = -1;
                    }
                    i17++;
                    i5 = i5;
                    z = z;
                    i8 = 1;
                    i13 = 4;
                    i10 = -1;
                }
                if (i12 != 4) {
                    g.f6806g.f(3317, i12);
                }
                if (e()) {
                    g.f6806g.n(i11);
                }
                h();
                return;
            }
            throw new o("Unsupported texture format (only 2D texture are supported in LibGdx for the time being)");
        }
        throw new o("Call prepare() before calling consumeCompressedData()");
    }

    public boolean a() {
        return true;
    }

    public void b() {
        if (this.l == null) {
            a aVar = this.f5159a;
            if (aVar != null) {
                if (aVar.g().endsWith(".zktx")) {
                    byte[] bArr = new byte[10240];
                    DataInputStream dataInputStream = null;
                    try {
                        DataInputStream dataInputStream2 = new DataInputStream(new BufferedInputStream(new GZIPInputStream(this.f5159a.l())));
                        try {
                            this.l = BufferUtils.d(dataInputStream2.readInt());
                            while (true) {
                                int read = dataInputStream2.read(bArr);
                                if (read == -1) {
                                    break;
                                }
                                this.l.put(bArr, 0, read);
                            }
                            this.l.position(0);
                            this.l.limit(this.l.capacity());
                            q0.a(dataInputStream2);
                        } catch (Exception e2) {
                            e = e2;
                            dataInputStream = dataInputStream2;
                            try {
                                throw new o("Couldn't load zktx file '" + this.f5159a + "'", e);
                            } catch (Throwable th) {
                                th = th;
                                q0.a(dataInputStream);
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            dataInputStream = dataInputStream2;
                            q0.a(dataInputStream);
                            throw th;
                        }
                    } catch (Exception e3) {
                        e = e3;
                        throw new o("Couldn't load zktx file '" + this.f5159a + "'", e);
                    }
                } else {
                    this.l = ByteBuffer.wrap(this.f5159a.m());
                }
                if (this.l.get() != -85) {
                    throw new o("Invalid KTX Header");
                } else if (this.l.get() != 75) {
                    throw new o("Invalid KTX Header");
                } else if (this.l.get() != 84) {
                    throw new o("Invalid KTX Header");
                } else if (this.l.get() != 88) {
                    throw new o("Invalid KTX Header");
                } else if (this.l.get() != 32) {
                    throw new o("Invalid KTX Header");
                } else if (this.l.get() != 49) {
                    throw new o("Invalid KTX Header");
                } else if (this.l.get() != 49) {
                    throw new o("Invalid KTX Header");
                } else if (this.l.get() != -69) {
                    throw new o("Invalid KTX Header");
                } else if (this.l.get() != 13) {
                    throw new o("Invalid KTX Header");
                } else if (this.l.get() != 10) {
                    throw new o("Invalid KTX Header");
                } else if (this.l.get() != 26) {
                    throw new o("Invalid KTX Header");
                } else if (this.l.get() == 10) {
                    int i2 = this.l.getInt();
                    if (i2 == 67305985 || i2 == 16909060) {
                        if (i2 != 67305985) {
                            ByteBuffer byteBuffer = this.l;
                            ByteOrder order = byteBuffer.order();
                            ByteOrder byteOrder = ByteOrder.BIG_ENDIAN;
                            if (order == byteOrder) {
                                byteOrder = ByteOrder.LITTLE_ENDIAN;
                            }
                            byteBuffer.order(byteOrder);
                        }
                        this.f5160b = this.l.getInt();
                        this.l.getInt();
                        this.f5161c = this.l.getInt();
                        this.f5162d = this.l.getInt();
                        this.l.getInt();
                        this.f5163e = this.l.getInt();
                        this.f5164f = this.l.getInt();
                        this.f5165g = this.l.getInt();
                        this.f5166h = this.l.getInt();
                        this.f5167i = this.l.getInt();
                        this.f5168j = this.l.getInt();
                        if (this.f5168j == 0) {
                            this.f5168j = 1;
                            this.m = true;
                        }
                        this.f5169k = this.l.position() + this.l.getInt();
                        if (!this.l.isDirect()) {
                            int i3 = this.f5169k;
                            for (int i4 = 0; i4 < this.f5168j; i4++) {
                                i3 += (((this.l.getInt(i3) + 3) & -4) * this.f5167i) + 4;
                            }
                            this.l.limit(i3);
                            this.l.position(0);
                            ByteBuffer d2 = BufferUtils.d(i3);
                            d2.order(this.l.order());
                            d2.put(this.l);
                            this.l = d2;
                            return;
                        }
                        return;
                    }
                    throw new o("Invalid KTX Header");
                } else {
                    throw new o("Invalid KTX Header");
                }
            } else {
                throw new o("Need a file to load from");
            }
        } else {
            throw new o("Already prepared");
        }
    }

    public boolean c() {
        return this.l != null;
    }

    public l d() {
        throw new o("This TextureData implementation does not return a Pixmap");
    }

    public boolean e() {
        return this.m;
    }

    public boolean f() {
        throw new o("This TextureData implementation does not return a Pixmap");
    }

    public void g() {
        a(34067);
    }

    public l.c getFormat() {
        throw new o("This TextureData implementation directly handles texture formats.");
    }

    public int getHeight() {
        return this.f5164f;
    }

    public q.b getType() {
        return q.b.Custom;
    }

    public int getWidth() {
        return this.f5163e;
    }

    public void h() {
        ByteBuffer byteBuffer = this.l;
        if (byteBuffer != null) {
            BufferUtils.a(byteBuffer);
        }
        this.l = null;
    }
}
