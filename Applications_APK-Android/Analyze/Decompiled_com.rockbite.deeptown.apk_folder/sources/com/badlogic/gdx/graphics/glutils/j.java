package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.math.Matrix4;

/* compiled from: ImmediateModeRenderer */
public interface j {
    void a(float f2);

    void a(float f2, float f3, float f4);

    void a(float f2, float f3, float f4, float f5);

    void a(Matrix4 matrix4, int i2);

    int c();

    int d();

    void dispose();

    void end();
}
