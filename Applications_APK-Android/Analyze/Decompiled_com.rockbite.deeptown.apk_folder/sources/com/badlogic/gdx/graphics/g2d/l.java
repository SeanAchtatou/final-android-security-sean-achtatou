package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.math.c;
import com.badlogic.gdx.utils.o;
import com.badlogic.gdx.utils.q0;
import e.d.b.q.g.e;
import e.d.b.q.g.n;
import java.io.BufferedReader;
import java.io.IOException;

/* compiled from: PolygonRegionLoader */
public class l extends n<k, a> {

    /* renamed from: a  reason: collision with root package name */
    private a f4989a = new a();

    /* renamed from: b  reason: collision with root package name */
    private c f4990b = new c();

    /* compiled from: PolygonRegionLoader */
    public static class a extends e.d.b.q.c<k> {

        /* renamed from: a  reason: collision with root package name */
        public String f4991a = "i ";

        /* renamed from: b  reason: collision with root package name */
        public int f4992b = 1024;

        /* renamed from: c  reason: collision with root package name */
        public String[] f4993c = {"png", "PNG", "jpeg", "JPEG", "jpg", "JPG", "cim", "CIM", "etc1", "ETC1", "ktx", "KTX", "zktx", "ZKTX"};
    }

    public l(e eVar) {
        super(eVar);
    }

    public k a(e.d.b.q.e eVar, String str, e.d.b.s.a aVar, a aVar2) {
        return a(new r((e.d.b.t.n) eVar.a(eVar.b(str).a())), aVar);
    }

    /* renamed from: a */
    public com.badlogic.gdx.utils.a<e.d.b.q.a> getDependencies(String str, e.d.b.s.a aVar, a aVar2) {
        String str2;
        String[] strArr;
        if (aVar2 == null) {
            aVar2 = this.f4989a;
        }
        try {
            BufferedReader b2 = aVar.b(aVar2.f4992b);
            while (true) {
                String readLine = b2.readLine();
                if (readLine != null) {
                    if (readLine.startsWith(aVar2.f4991a)) {
                        str2 = readLine.substring(aVar2.f4991a.length());
                        break;
                    }
                } else {
                    str2 = null;
                    break;
                }
            }
            b2.close();
            if (str2 == null && (strArr = aVar2.f4993c) != null) {
                for (String str3 : strArr) {
                    e.d.b.s.a d2 = aVar.d(aVar.h().concat("." + str3));
                    if (d2.a()) {
                        str2 = d2.g();
                    }
                }
            }
            if (str2 == null) {
                return null;
            }
            com.badlogic.gdx.utils.a<e.d.b.q.a> aVar3 = new com.badlogic.gdx.utils.a<>(1);
            aVar3.add(new e.d.b.q.a(aVar.d(str2), e.d.b.t.n.class));
            return aVar3;
        } catch (IOException e2) {
            throw new o("Error reading " + str, e2);
        }
    }

    public k a(r rVar, e.d.b.s.a aVar) {
        String readLine;
        BufferedReader b2 = aVar.b(256);
        do {
            try {
                readLine = b2.readLine();
                if (readLine == null) {
                    q0.a(b2);
                    throw new o("Polygon shape not found: " + aVar);
                }
            } catch (IOException e2) {
                throw new o("Error reading polygon shape file: " + aVar, e2);
            } catch (Throwable th) {
                q0.a(b2);
                throw th;
            }
        } while (!readLine.startsWith("s"));
        String[] split = readLine.substring(1).trim().split(",");
        float[] fArr = new float[split.length];
        int length = fArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            fArr[i2] = Float.parseFloat(split[i2]);
        }
        k kVar = new k(rVar, fArr, this.f4990b.a(fArr).b());
        q0.a(b2);
        return kVar;
    }
}
