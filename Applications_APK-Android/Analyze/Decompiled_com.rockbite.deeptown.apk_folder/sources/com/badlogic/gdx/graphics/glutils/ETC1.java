package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.math.h;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.o;
import com.badlogic.gdx.utils.q0;
import e.d.b.t.l;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.nio.ByteBuffer;
import java.util.zip.GZIPInputStream;

public class ETC1 {

    /* renamed from: a  reason: collision with root package name */
    public static int f5067a = 16;

    /* renamed from: b  reason: collision with root package name */
    public static int f5068b = 36196;

    private static int a(l.c cVar) {
        if (cVar == l.c.RGB565) {
            return 2;
        }
        if (cVar == l.c.RGB888) {
            return 3;
        }
        throw new o("Can only handle RGB565 or RGB888 images");
    }

    private static native void decodeImage(ByteBuffer byteBuffer, int i2, ByteBuffer byteBuffer2, int i3, int i4, int i5, int i6);

    static native int getHeightPKM(ByteBuffer byteBuffer, int i2);

    static native int getWidthPKM(ByteBuffer byteBuffer, int i2);

    static native boolean isValidPKM(ByteBuffer byteBuffer, int i2);

    public static l a(a aVar, l.c cVar) {
        int i2;
        int i3;
        int i4;
        if (aVar.j()) {
            int widthPKM = getWidthPKM(aVar.f5071c, 0);
            i2 = getHeightPKM(aVar.f5071c, 0);
            i3 = widthPKM;
            i4 = 16;
        } else {
            int i5 = aVar.f5069a;
            i2 = aVar.f5070b;
            i3 = i5;
            i4 = 0;
        }
        int a2 = a(cVar);
        l lVar = new l(i3, i2, cVar);
        decodeImage(aVar.f5071c, i4, lVar.p(), 0, i3, i2, a2);
        return lVar;
    }

    public static final class a implements com.badlogic.gdx.utils.l {

        /* renamed from: a  reason: collision with root package name */
        public final int f5069a;

        /* renamed from: b  reason: collision with root package name */
        public final int f5070b;

        /* renamed from: c  reason: collision with root package name */
        public final ByteBuffer f5071c;

        /* renamed from: d  reason: collision with root package name */
        public final int f5072d;

        public a(int i2, int i3, ByteBuffer byteBuffer, int i4) {
            this.f5069a = i2;
            this.f5070b = i3;
            this.f5071c = byteBuffer;
            this.f5072d = i4;
            k();
        }

        private void k() {
            if (!h.a(this.f5069a) || !h.a(this.f5070b)) {
                System.out.println("ETC1Data warning: non-power-of-two ETC1 textures may crash the driver of PowerVR GPUs");
            }
        }

        public void dispose() {
            BufferUtils.a(this.f5071c);
        }

        public boolean j() {
            return this.f5072d == 16;
        }

        public String toString() {
            if (j()) {
                StringBuilder sb = new StringBuilder();
                sb.append(ETC1.isValidPKM(this.f5071c, 0) ? "valid" : "invalid");
                sb.append(" pkm [");
                sb.append(ETC1.getWidthPKM(this.f5071c, 0));
                sb.append("x");
                sb.append(ETC1.getHeightPKM(this.f5071c, 0));
                sb.append("], compressed: ");
                sb.append(this.f5071c.capacity() - ETC1.f5067a);
                return sb.toString();
            }
            return "raw [" + this.f5069a + "x" + this.f5070b + "], compressed: " + (this.f5071c.capacity() - ETC1.f5067a);
        }

        public a(e.d.b.s.a aVar) {
            byte[] bArr = new byte[10240];
            DataInputStream dataInputStream = null;
            try {
                DataInputStream dataInputStream2 = new DataInputStream(new BufferedInputStream(new GZIPInputStream(aVar.l())));
                try {
                    this.f5071c = BufferUtils.d(dataInputStream2.readInt());
                    while (true) {
                        int read = dataInputStream2.read(bArr);
                        if (read != -1) {
                            this.f5071c.put(bArr, 0, read);
                        } else {
                            this.f5071c.position(0);
                            this.f5071c.limit(this.f5071c.capacity());
                            q0.a(dataInputStream2);
                            this.f5069a = ETC1.getWidthPKM(this.f5071c, 0);
                            this.f5070b = ETC1.getHeightPKM(this.f5071c, 0);
                            this.f5072d = ETC1.f5067a;
                            this.f5071c.position(this.f5072d);
                            k();
                            return;
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    dataInputStream = dataInputStream2;
                    try {
                        throw new o("Couldn't load pkm file '" + aVar + "'", e);
                    } catch (Throwable th) {
                        th = th;
                        q0.a(dataInputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    dataInputStream = dataInputStream2;
                    q0.a(dataInputStream);
                    throw th;
                }
            } catch (Exception e3) {
                e = e3;
                throw new o("Couldn't load pkm file '" + aVar + "'", e);
            }
        }
    }
}
