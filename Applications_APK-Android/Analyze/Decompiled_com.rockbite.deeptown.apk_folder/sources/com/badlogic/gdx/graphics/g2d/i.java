package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.g2d.q;
import com.badlogic.gdx.graphics.glutils.r;
import com.badlogic.gdx.math.n;
import com.badlogic.gdx.utils.e0;
import com.badlogic.gdx.utils.l;
import com.esotericsoftware.spine.Animation;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import e.d.b.t.l;
import e.d.b.t.n;
import e.d.b.t.q;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: PixmapPacker */
public class i implements l {
    static Pattern o = Pattern.compile("(.+)_(\\d+)$");

    /* renamed from: a  reason: collision with root package name */
    boolean f4953a;

    /* renamed from: b  reason: collision with root package name */
    boolean f4954b;

    /* renamed from: c  reason: collision with root package name */
    int f4955c;

    /* renamed from: d  reason: collision with root package name */
    int f4956d;

    /* renamed from: e  reason: collision with root package name */
    l.c f4957e;

    /* renamed from: f  reason: collision with root package name */
    int f4958f;

    /* renamed from: g  reason: collision with root package name */
    boolean f4959g;

    /* renamed from: h  reason: collision with root package name */
    boolean f4960h;

    /* renamed from: i  reason: collision with root package name */
    boolean f4961i;

    /* renamed from: j  reason: collision with root package name */
    int f4962j;

    /* renamed from: k  reason: collision with root package name */
    e.d.b.t.b f4963k;
    final com.badlogic.gdx.utils.a<c> l;
    b m;
    private e.d.b.t.b n;

    /* compiled from: PixmapPacker */
    public interface b {
        c a(i iVar, String str, n nVar);
    }

    /* compiled from: PixmapPacker */
    public static class c {

        /* renamed from: a  reason: collision with root package name */
        e0<String, d> f4969a = new e0<>();

        /* renamed from: b  reason: collision with root package name */
        e.d.b.t.l f4970b;

        /* renamed from: c  reason: collision with root package name */
        e.d.b.t.n f4971c;

        /* renamed from: d  reason: collision with root package name */
        final com.badlogic.gdx.utils.a<String> f4972d = new com.badlogic.gdx.utils.a<>();

        /* renamed from: e  reason: collision with root package name */
        boolean f4973e;

        /* compiled from: PixmapPacker */
        class a extends e.d.b.t.n {
            a(q qVar) {
                super(qVar);
            }

            public void dispose() {
                super.dispose();
                c.this.f4970b.dispose();
            }
        }

        public c(i iVar) {
            this.f4970b = new e.d.b.t.l(iVar.f4955c, iVar.f4956d, iVar.f4957e);
            this.f4970b.a(l.a.None);
            this.f4970b.setColor(iVar.k());
            this.f4970b.j();
        }

        public boolean a(n.b bVar, n.b bVar2, boolean z) {
            e.d.b.t.n nVar = this.f4971c;
            if (nVar == null) {
                e.d.b.t.l lVar = this.f4970b;
                this.f4971c = new a(new r(lVar, lVar.k(), z, false, true));
                this.f4971c.a(bVar, bVar2);
            } else if (!this.f4973e) {
                return false;
            } else {
                nVar.a(nVar.q());
            }
            this.f4973e = false;
            return true;
        }
    }

    /* compiled from: PixmapPacker */
    public static class e implements b {

        /* compiled from: PixmapPacker */
        static class a extends c {

            /* renamed from: f  reason: collision with root package name */
            com.badlogic.gdx.utils.a<C0119a> f4981f = new com.badlogic.gdx.utils.a<>();

            /* renamed from: com.badlogic.gdx.graphics.g2d.i$e$a$a  reason: collision with other inner class name */
            /* compiled from: PixmapPacker */
            static class C0119a {

                /* renamed from: a  reason: collision with root package name */
                int f4982a;

                /* renamed from: b  reason: collision with root package name */
                int f4983b;

                /* renamed from: c  reason: collision with root package name */
                int f4984c;

                C0119a() {
                }
            }

            public a(i iVar) {
                super(iVar);
            }
        }

        public c a(i iVar, String str, com.badlogic.gdx.math.n nVar) {
            int i2;
            i iVar2 = iVar;
            com.badlogic.gdx.math.n nVar2 = nVar;
            int i3 = iVar2.f4958f;
            int i4 = i3 * 2;
            int i5 = iVar2.f4955c - i4;
            int i6 = iVar2.f4956d - i4;
            int i7 = ((int) nVar2.f5292c) + i3;
            int i8 = ((int) nVar2.f5293d) + i3;
            int i9 = iVar2.l.f5374b;
            for (int i10 = 0; i10 < i9; i10++) {
                a aVar = (a) iVar2.l.get(i10);
                int i11 = aVar.f4981f.f5374b - 1;
                a.C0119a aVar2 = null;
                for (int i12 = 0; i12 < i11; i12++) {
                    a.C0119a aVar3 = aVar.f4981f.get(i12);
                    if (aVar3.f4982a + i7 < i5 && aVar3.f4983b + i8 < i6 && i8 <= (i2 = aVar3.f4984c) && (aVar2 == null || i2 < aVar2.f4984c)) {
                        aVar2 = aVar3;
                    }
                }
                if (aVar2 == null) {
                    a.C0119a peek = aVar.f4981f.peek();
                    int i13 = peek.f4983b;
                    if (i13 + i8 >= i6) {
                        continue;
                    } else if (peek.f4982a + i7 < i5) {
                        peek.f4984c = Math.max(peek.f4984c, i8);
                        aVar2 = peek;
                    } else if (i13 + peek.f4984c + i8 < i6) {
                        aVar2 = new a.C0119a();
                        aVar2.f4983b = peek.f4983b + peek.f4984c;
                        aVar2.f4984c = i8;
                        aVar.f4981f.add(aVar2);
                    }
                }
                if (aVar2 != null) {
                    int i14 = aVar2.f4982a;
                    nVar2.f5290a = (float) i14;
                    nVar2.f5291b = (float) aVar2.f4983b;
                    aVar2.f4982a = i14 + i7;
                    return aVar;
                }
            }
            a aVar4 = new a(iVar2);
            iVar2.l.add(aVar4);
            a.C0119a aVar5 = new a.C0119a();
            aVar5.f4982a = i7 + i3;
            aVar5.f4983b = i3;
            aVar5.f4984c = i8;
            aVar4.f4981f.add(aVar5);
            float f2 = (float) i3;
            nVar2.f5290a = f2;
            nVar2.f5291b = f2;
            return aVar4;
        }
    }

    public i(int i2, int i3, l.c cVar, int i4, boolean z) {
        this(i2, i3, cVar, i4, z, false, false, new a());
    }

    public synchronized com.badlogic.gdx.math.n a(e.d.b.t.l lVar) {
        return a((String) null, lVar);
    }

    public synchronized void b(n.b bVar, n.b bVar2, boolean z) {
        Iterator<c> it = this.l.iterator();
        while (it.hasNext()) {
            it.next().a(bVar, bVar2, z);
        }
    }

    public synchronized void dispose() {
        Iterator<c> it = this.l.iterator();
        while (it.hasNext()) {
            c next = it.next();
            if (next.f4971c == null) {
                next.f4970b.dispose();
            }
        }
        this.f4954b = true;
    }

    public com.badlogic.gdx.utils.a<c> j() {
        return this.l;
    }

    public e.d.b.t.b k() {
        return this.f4963k;
    }

    public i(int i2, int i3, l.c cVar, int i4, boolean z, b bVar) {
        this(i2, i3, cVar, i4, z, false, false, bVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:104:0x02bb, code lost:
        return r12;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.badlogic.gdx.math.n a(java.lang.String r28, e.d.b.t.l r29) {
        /*
            r27 = this;
            r1 = r27
            r0 = r28
            r3 = r29
            monitor-enter(r27)
            boolean r2 = r1.f4954b     // Catch:{ all -> 0x02dd }
            r4 = 0
            if (r2 == 0) goto L_0x000e
            monitor-exit(r27)
            return r4
        L_0x000e:
            if (r0 == 0) goto L_0x002e
            com.badlogic.gdx.math.n r2 = r27.a(r28)     // Catch:{ all -> 0x02dd }
            if (r2 != 0) goto L_0x0017
            goto L_0x002e
        L_0x0017:
            com.badlogic.gdx.utils.o r2 = new com.badlogic.gdx.utils.o     // Catch:{ all -> 0x02dd }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x02dd }
            r3.<init>()     // Catch:{ all -> 0x02dd }
            java.lang.String r4 = "Pixmap has already been packed with name: "
            r3.append(r4)     // Catch:{ all -> 0x02dd }
            r3.append(r0)     // Catch:{ all -> 0x02dd }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x02dd }
            r2.<init>(r0)     // Catch:{ all -> 0x02dd }
            throw r2     // Catch:{ all -> 0x02dd }
        L_0x002e:
            r10 = 0
            r11 = 1
            if (r0 == 0) goto L_0x003c
            java.lang.String r2 = ".9"
            boolean r2 = r0.endsWith(r2)     // Catch:{ all -> 0x02dd }
            if (r2 == 0) goto L_0x003c
            r2 = 1
            goto L_0x003d
        L_0x003c:
            r2 = 0
        L_0x003d:
            if (r2 == 0) goto L_0x009a
            com.badlogic.gdx.graphics.g2d.i$d r12 = new com.badlogic.gdx.graphics.g2d.i$d     // Catch:{ all -> 0x02dd }
            int r2 = r29.q()     // Catch:{ all -> 0x02dd }
            int r2 = r2 + -2
            int r4 = r29.o()     // Catch:{ all -> 0x02dd }
            int r4 = r4 + -2
            r12.<init>(r10, r10, r2, r4)     // Catch:{ all -> 0x02dd }
            e.d.b.t.l r13 = new e.d.b.t.l     // Catch:{ all -> 0x02dd }
            int r2 = r29.q()     // Catch:{ all -> 0x02dd }
            int r2 = r2 + -2
            int r4 = r29.o()     // Catch:{ all -> 0x02dd }
            int r4 = r4 + -2
            e.d.b.t.l$c r5 = r29.k()     // Catch:{ all -> 0x02dd }
            r13.<init>(r2, r4, r5)     // Catch:{ all -> 0x02dd }
            e.d.b.t.l$a r2 = e.d.b.t.l.a.None     // Catch:{ all -> 0x02dd }
            r13.a(r2)     // Catch:{ all -> 0x02dd }
            int[] r2 = r1.b(r3)     // Catch:{ all -> 0x02dd }
            r12.f4975f = r2     // Catch:{ all -> 0x02dd }
            int[] r2 = r12.f4975f     // Catch:{ all -> 0x02dd }
            int[] r2 = r1.a(r3, r2)     // Catch:{ all -> 0x02dd }
            r12.f4976g = r2     // Catch:{ all -> 0x02dd }
            r4 = 0
            r5 = 0
            r6 = 1
            r7 = 1
            int r2 = r29.q()     // Catch:{ all -> 0x02dd }
            int r8 = r2 + -1
            int r2 = r29.o()     // Catch:{ all -> 0x02dd }
            int r9 = r2 + -1
            r2 = r13
            r3 = r29
            r2.a(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x02dd }
            java.lang.String r2 = "\\."
            java.lang.String[] r0 = r0.split(r2)     // Catch:{ all -> 0x02dd }
            r0 = r0[r10]     // Catch:{ all -> 0x02dd }
            r3 = r13
            r4 = r3
            goto L_0x0181
        L_0x009a:
            boolean r2 = r1.f4960h     // Catch:{ all -> 0x02dd }
            if (r2 != 0) goto L_0x00b2
            boolean r2 = r1.f4961i     // Catch:{ all -> 0x02dd }
            if (r2 == 0) goto L_0x00a3
            goto L_0x00b2
        L_0x00a3:
            com.badlogic.gdx.graphics.g2d.i$d r12 = new com.badlogic.gdx.graphics.g2d.i$d     // Catch:{ all -> 0x02dd }
            int r2 = r29.q()     // Catch:{ all -> 0x02dd }
            int r5 = r29.o()     // Catch:{ all -> 0x02dd }
            r12.<init>(r10, r10, r2, r5)     // Catch:{ all -> 0x02dd }
            goto L_0x0181
        L_0x00b2:
            int r20 = r29.q()     // Catch:{ all -> 0x02dd }
            int r21 = r29.o()     // Catch:{ all -> 0x02dd }
            int r2 = r29.o()     // Catch:{ all -> 0x02dd }
            boolean r4 = r1.f4961i     // Catch:{ all -> 0x02dd }
            if (r4 == 0) goto L_0x0107
            r4 = 0
            r5 = 0
        L_0x00c4:
            int r6 = r29.o()     // Catch:{ all -> 0x02dd }
            if (r4 >= r6) goto L_0x00e4
            r6 = 0
        L_0x00cb:
            int r7 = r29.q()     // Catch:{ all -> 0x02dd }
            if (r6 >= r7) goto L_0x00df
            int r7 = r3.a(r6, r4)     // Catch:{ all -> 0x02dd }
            r7 = r7 & 255(0xff, float:3.57E-43)
            int r8 = r1.f4962j     // Catch:{ all -> 0x02dd }
            if (r7 <= r8) goto L_0x00dc
            goto L_0x00e4
        L_0x00dc:
            int r6 = r6 + 1
            goto L_0x00cb
        L_0x00df:
            int r5 = r5 + 1
            int r4 = r4 + 1
            goto L_0x00c4
        L_0x00e4:
            int r4 = r29.o()     // Catch:{ all -> 0x02dd }
        L_0x00e8:
            int r4 = r4 + -1
            if (r4 < r5) goto L_0x0104
            r6 = 0
        L_0x00ed:
            int r7 = r29.q()     // Catch:{ all -> 0x02dd }
            if (r6 >= r7) goto L_0x0101
            int r7 = r3.a(r6, r4)     // Catch:{ all -> 0x02dd }
            r7 = r7 & 255(0xff, float:3.57E-43)
            int r8 = r1.f4962j     // Catch:{ all -> 0x02dd }
            if (r7 <= r8) goto L_0x00fe
            goto L_0x0104
        L_0x00fe:
            int r6 = r6 + 1
            goto L_0x00ed
        L_0x0101:
            int r2 = r2 + -1
            goto L_0x00e8
        L_0x0104:
            r19 = r5
            goto L_0x0109
        L_0x0107:
            r19 = 0
        L_0x0109:
            int r4 = r29.q()     // Catch:{ all -> 0x02dd }
            boolean r5 = r1.f4960h     // Catch:{ all -> 0x02dd }
            if (r5 == 0) goto L_0x014f
            r5 = 0
        L_0x0112:
            int r6 = r29.q()     // Catch:{ all -> 0x02dd }
            if (r10 >= r6) goto L_0x012f
            r6 = r19
        L_0x011a:
            if (r6 >= r2) goto L_0x012a
            int r7 = r3.a(r10, r6)     // Catch:{ all -> 0x02dd }
            r7 = r7 & 255(0xff, float:3.57E-43)
            int r8 = r1.f4962j     // Catch:{ all -> 0x02dd }
            if (r7 <= r8) goto L_0x0127
            goto L_0x012f
        L_0x0127:
            int r6 = r6 + 1
            goto L_0x011a
        L_0x012a:
            int r5 = r5 + 1
            int r10 = r10 + 1
            goto L_0x0112
        L_0x012f:
            int r6 = r29.q()     // Catch:{ all -> 0x02dd }
        L_0x0133:
            int r6 = r6 + -1
            if (r6 < r5) goto L_0x014c
            r7 = r19
        L_0x0139:
            if (r7 >= r2) goto L_0x0149
            int r8 = r3.a(r6, r7)     // Catch:{ all -> 0x02dd }
            r8 = r8 & 255(0xff, float:3.57E-43)
            int r9 = r1.f4962j     // Catch:{ all -> 0x02dd }
            if (r8 <= r9) goto L_0x0146
            goto L_0x014c
        L_0x0146:
            int r7 = r7 + 1
            goto L_0x0139
        L_0x0149:
            int r4 = r4 + -1
            goto L_0x0133
        L_0x014c:
            r18 = r5
            goto L_0x0151
        L_0x014f:
            r18 = 0
        L_0x0151:
            int r10 = r4 - r18
            int r12 = r2 - r19
            e.d.b.t.l r15 = new e.d.b.t.l     // Catch:{ all -> 0x02dd }
            e.d.b.t.l$c r2 = r29.k()     // Catch:{ all -> 0x02dd }
            r15.<init>(r10, r12, r2)     // Catch:{ all -> 0x02dd }
            e.d.b.t.l$a r2 = e.d.b.t.l.a.None     // Catch:{ all -> 0x02dd }
            r15.a(r2)     // Catch:{ all -> 0x02dd }
            r4 = 0
            r5 = 0
            r2 = r15
            r3 = r29
            r6 = r18
            r7 = r19
            r8 = r10
            r9 = r12
            r2.a(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x02dd }
            com.badlogic.gdx.graphics.g2d.i$d r2 = new com.badlogic.gdx.graphics.g2d.i$d     // Catch:{ all -> 0x02dd }
            r14 = 0
            r3 = 0
            r13 = r2
            r4 = r15
            r15 = r3
            r16 = r10
            r17 = r12
            r13.<init>(r14, r15, r16, r17, r18, r19, r20, r21)     // Catch:{ all -> 0x02dd }
            r12 = r2
            r3 = r4
        L_0x0181:
            float r2 = r12.b()     // Catch:{ all -> 0x02dd }
            int r5 = r1.f4955c     // Catch:{ all -> 0x02dd }
            float r5 = (float) r5     // Catch:{ all -> 0x02dd }
            int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r2 > 0) goto L_0x02bc
            float r2 = r12.a()     // Catch:{ all -> 0x02dd }
            int r5 = r1.f4956d     // Catch:{ all -> 0x02dd }
            float r5 = (float) r5     // Catch:{ all -> 0x02dd }
            int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r2 <= 0) goto L_0x0199
            goto L_0x02bc
        L_0x0199:
            com.badlogic.gdx.graphics.g2d.i$b r2 = r1.m     // Catch:{ all -> 0x02dd }
            com.badlogic.gdx.graphics.g2d.i$c r2 = r2.a(r1, r0, r12)     // Catch:{ all -> 0x02dd }
            if (r0 == 0) goto L_0x01ab
            com.badlogic.gdx.utils.e0<java.lang.String, com.badlogic.gdx.graphics.g2d.i$d> r5 = r2.f4969a     // Catch:{ all -> 0x02dd }
            r5.b(r0, r12)     // Catch:{ all -> 0x02dd }
            com.badlogic.gdx.utils.a<java.lang.String> r5 = r2.f4972d     // Catch:{ all -> 0x02dd }
            r5.add(r0)     // Catch:{ all -> 0x02dd }
        L_0x01ab:
            float r0 = r12.f5290a     // Catch:{ all -> 0x02dd }
            int r0 = (int) r0     // Catch:{ all -> 0x02dd }
            float r5 = r12.f5291b     // Catch:{ all -> 0x02dd }
            int r5 = (int) r5     // Catch:{ all -> 0x02dd }
            float r6 = r12.f5292c     // Catch:{ all -> 0x02dd }
            int r6 = (int) r6     // Catch:{ all -> 0x02dd }
            float r7 = r12.f5293d     // Catch:{ all -> 0x02dd }
            int r7 = (int) r7     // Catch:{ all -> 0x02dd }
            boolean r8 = r1.f4953a     // Catch:{ all -> 0x02dd }
            if (r8 == 0) goto L_0x01eb
            boolean r8 = r1.f4959g     // Catch:{ all -> 0x02dd }
            if (r8 != 0) goto L_0x01eb
            e.d.b.t.n r8 = r2.f4971c     // Catch:{ all -> 0x02dd }
            if (r8 == 0) goto L_0x01eb
            boolean r8 = r2.f4973e     // Catch:{ all -> 0x02dd }
            if (r8 != 0) goto L_0x01eb
            e.d.b.t.n r8 = r2.f4971c     // Catch:{ all -> 0x02dd }
            r8.f()     // Catch:{ all -> 0x02dd }
            e.d.b.t.g r13 = e.d.b.g.f6806g     // Catch:{ all -> 0x02dd }
            e.d.b.t.n r8 = r2.f4971c     // Catch:{ all -> 0x02dd }
            int r14 = r8.f6943a     // Catch:{ all -> 0x02dd }
            r15 = 0
            int r20 = r3.l()     // Catch:{ all -> 0x02dd }
            int r21 = r3.n()     // Catch:{ all -> 0x02dd }
            java.nio.ByteBuffer r22 = r3.p()     // Catch:{ all -> 0x02dd }
            r16 = r0
            r17 = r5
            r18 = r6
            r19 = r7
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ all -> 0x02dd }
            goto L_0x01ed
        L_0x01eb:
            r2.f4973e = r11     // Catch:{ all -> 0x02dd }
        L_0x01ed:
            e.d.b.t.l r8 = r2.f4970b     // Catch:{ all -> 0x02dd }
            r8.a(r3, r0, r5)     // Catch:{ all -> 0x02dd }
            boolean r8 = r1.f4959g     // Catch:{ all -> 0x02dd }
            if (r8 == 0) goto L_0x02b5
            int r8 = r3.q()     // Catch:{ all -> 0x02dd }
            int r9 = r3.o()     // Catch:{ all -> 0x02dd }
            e.d.b.t.l r13 = r2.f4970b     // Catch:{ all -> 0x02dd }
            r15 = 0
            r16 = 0
            r17 = 1
            r18 = 1
            int r10 = r0 + -1
            int r11 = r5 + -1
            r21 = 1
            r22 = 1
            r14 = r3
            r19 = r10
            r20 = r11
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ all -> 0x02dd }
            e.d.b.t.l r13 = r2.f4970b     // Catch:{ all -> 0x02dd }
            int r23 = r8 + -1
            r16 = 0
            r17 = 1
            r18 = 1
            int r24 = r0 + r6
            r21 = 1
            r22 = 1
            r14 = r3
            r15 = r23
            r19 = r24
            r20 = r11
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ all -> 0x02dd }
            e.d.b.t.l r13 = r2.f4970b     // Catch:{ all -> 0x02dd }
            r15 = 0
            int r25 = r9 + -1
            r17 = 1
            r18 = 1
            int r26 = r5 + r7
            r21 = 1
            r22 = 1
            r14 = r3
            r16 = r25
            r19 = r10
            r20 = r26
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ all -> 0x02dd }
            e.d.b.t.l r13 = r2.f4970b     // Catch:{ all -> 0x02dd }
            r17 = 1
            r18 = 1
            r21 = 1
            r22 = 1
            r14 = r3
            r15 = r23
            r16 = r25
            r19 = r24
            r20 = r26
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ all -> 0x02dd }
            e.d.b.t.l r13 = r2.f4970b     // Catch:{ all -> 0x02dd }
            r15 = 0
            r16 = 0
            r18 = 1
            r22 = 1
            r14 = r3
            r17 = r8
            r19 = r0
            r20 = r11
            r21 = r6
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ all -> 0x02dd }
            e.d.b.t.l r13 = r2.f4970b     // Catch:{ all -> 0x02dd }
            r15 = 0
            r18 = 1
            r22 = 1
            r14 = r3
            r16 = r25
            r17 = r8
            r19 = r0
            r20 = r26
            r21 = r6
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ all -> 0x02dd }
            e.d.b.t.l r13 = r2.f4970b     // Catch:{ all -> 0x02dd }
            r15 = 0
            r16 = 0
            r17 = 1
            r21 = 1
            r14 = r3
            r18 = r9
            r19 = r10
            r20 = r5
            r22 = r7
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ all -> 0x02dd }
            e.d.b.t.l r13 = r2.f4970b     // Catch:{ all -> 0x02dd }
            r16 = 0
            r17 = 1
            r21 = 1
            r14 = r3
            r15 = r23
            r18 = r9
            r19 = r24
            r20 = r5
            r22 = r7
            r13.a(r14, r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ all -> 0x02dd }
        L_0x02b5:
            if (r4 == 0) goto L_0x02ba
            r4.dispose()     // Catch:{ all -> 0x02dd }
        L_0x02ba:
            monitor-exit(r27)
            return r12
        L_0x02bc:
            if (r0 != 0) goto L_0x02c6
            com.badlogic.gdx.utils.o r0 = new com.badlogic.gdx.utils.o     // Catch:{ all -> 0x02dd }
            java.lang.String r2 = "Page size too small for pixmap."
            r0.<init>(r2)     // Catch:{ all -> 0x02dd }
            throw r0     // Catch:{ all -> 0x02dd }
        L_0x02c6:
            com.badlogic.gdx.utils.o r2 = new com.badlogic.gdx.utils.o     // Catch:{ all -> 0x02dd }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x02dd }
            r3.<init>()     // Catch:{ all -> 0x02dd }
            java.lang.String r4 = "Page size too small for pixmap: "
            r3.append(r4)     // Catch:{ all -> 0x02dd }
            r3.append(r0)     // Catch:{ all -> 0x02dd }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x02dd }
            r2.<init>(r0)     // Catch:{ all -> 0x02dd }
            throw r2     // Catch:{ all -> 0x02dd }
        L_0x02dd:
            r0 = move-exception
            monitor-exit(r27)
            goto L_0x02e1
        L_0x02e0:
            throw r0
        L_0x02e1:
            goto L_0x02e0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.i.a(java.lang.String, e.d.b.t.l):com.badlogic.gdx.math.n");
    }

    public i(int i2, int i3, l.c cVar, int i4, boolean z, boolean z2, boolean z3, b bVar) {
        this.f4963k = new e.d.b.t.b(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        this.l = new com.badlogic.gdx.utils.a<>();
        this.n = new e.d.b.t.b();
        this.f4955c = i2;
        this.f4956d = i3;
        this.f4957e = cVar;
        this.f4958f = i4;
        this.f4959g = z;
        this.f4960h = z2;
        this.f4961i = z3;
        this.m = bVar;
    }

    public void b(boolean z) {
        this.f4953a = z;
    }

    /* compiled from: PixmapPacker */
    public static class d extends com.badlogic.gdx.math.n {

        /* renamed from: f  reason: collision with root package name */
        int[] f4975f;

        /* renamed from: g  reason: collision with root package name */
        int[] f4976g;

        /* renamed from: h  reason: collision with root package name */
        int f4977h;

        /* renamed from: i  reason: collision with root package name */
        int f4978i;

        /* renamed from: j  reason: collision with root package name */
        int f4979j;

        /* renamed from: k  reason: collision with root package name */
        int f4980k;

        d(int i2, int i3, int i4, int i5) {
            super((float) i2, (float) i3, (float) i4, (float) i5);
            this.f4977h = 0;
            this.f4978i = 0;
            this.f4979j = i4;
            this.f4980k = i5;
        }

        d(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
            super((float) i2, (float) i3, (float) i4, (float) i5);
            this.f4977h = i6;
            this.f4978i = i7;
            this.f4979j = i8;
            this.f4980k = i9;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.i.a(e.d.b.t.l, int, int, boolean, boolean):int
     arg types: [e.d.b.t.l, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.i.a(com.badlogic.gdx.graphics.g2d.q, e.d.b.t.n$b, e.d.b.t.n$b, boolean, boolean):void
      com.badlogic.gdx.graphics.g2d.i.a(e.d.b.t.l, int, int, boolean, boolean):int */
    private int[] b(e.d.b.t.l lVar) {
        int i2;
        int i3;
        int a2 = a(lVar, 1, 0, true, true);
        int a3 = a(lVar, a2, 0, false, true);
        int a4 = a(lVar, 0, 1, true, false);
        int a5 = a(lVar, 0, a4, false, false);
        a(lVar, a3 + 1, 0, true, true);
        a(lVar, 0, a5 + 1, true, false);
        if (a2 == 0 && a3 == 0 && a4 == 0 && a5 == 0) {
            return null;
        }
        if (a2 != 0) {
            a2--;
            i2 = (lVar.q() - 2) - (a3 - 1);
        } else {
            i2 = lVar.q() - 2;
        }
        if (a4 != 0) {
            a4--;
            i3 = (lVar.o() - 2) - (a5 - 1);
        } else {
            i3 = lVar.o() - 2;
        }
        return new int[]{a2, i2, a4, i3};
    }

    /* compiled from: PixmapPacker */
    public static class a implements b {

        /* renamed from: com.badlogic.gdx.graphics.g2d.i$a$a  reason: collision with other inner class name */
        /* compiled from: PixmapPacker */
        static class C0118a extends c {

            /* renamed from: f  reason: collision with root package name */
            b f4964f = new b();

            public C0118a(i iVar) {
                super(iVar);
                com.badlogic.gdx.math.n nVar = this.f4964f.f4967c;
                int i2 = iVar.f4958f;
                nVar.f5290a = (float) i2;
                nVar.f5291b = (float) i2;
                nVar.f5292c = (float) (iVar.f4955c - (i2 * 2));
                nVar.f5293d = (float) (iVar.f4956d - (i2 * 2));
            }
        }

        /* compiled from: PixmapPacker */
        static final class b {

            /* renamed from: a  reason: collision with root package name */
            public b f4965a;

            /* renamed from: b  reason: collision with root package name */
            public b f4966b;

            /* renamed from: c  reason: collision with root package name */
            public final com.badlogic.gdx.math.n f4967c = new com.badlogic.gdx.math.n();

            /* renamed from: d  reason: collision with root package name */
            public boolean f4968d;

            b() {
            }
        }

        public c a(i iVar, String str, com.badlogic.gdx.math.n nVar) {
            C0118a aVar;
            com.badlogic.gdx.utils.a<c> aVar2 = iVar.l;
            if (aVar2.f5374b == 0) {
                aVar = new C0118a(iVar);
                iVar.l.add(aVar);
            } else {
                aVar = (C0118a) aVar2.peek();
            }
            float f2 = (float) iVar.f4958f;
            nVar.f5292c += f2;
            nVar.f5293d += f2;
            b a2 = a(aVar.f4964f, nVar);
            if (a2 == null) {
                aVar = new C0118a(iVar);
                iVar.l.add(aVar);
                a2 = a(aVar.f4964f, nVar);
            }
            a2.f4968d = true;
            com.badlogic.gdx.math.n nVar2 = a2.f4967c;
            nVar.a(nVar2.f5290a, nVar2.f5291b, nVar2.f5292c - f2, nVar2.f5293d - f2);
            return aVar;
        }

        private b a(b bVar, com.badlogic.gdx.math.n nVar) {
            b bVar2;
            if (!bVar.f4968d && (bVar2 = bVar.f4965a) != null && bVar.f4966b != null) {
                b a2 = a(bVar2, nVar);
                return a2 == null ? a(bVar.f4966b, nVar) : a2;
            } else if (bVar.f4968d) {
                return null;
            } else {
                com.badlogic.gdx.math.n nVar2 = bVar.f4967c;
                if (nVar2.f5292c == nVar.f5292c && nVar2.f5293d == nVar.f5293d) {
                    return bVar;
                }
                com.badlogic.gdx.math.n nVar3 = bVar.f4967c;
                if (nVar3.f5292c < nVar.f5292c || nVar3.f5293d < nVar.f5293d) {
                    return null;
                }
                bVar.f4965a = new b();
                bVar.f4966b = new b();
                com.badlogic.gdx.math.n nVar4 = bVar.f4967c;
                float f2 = nVar4.f5292c;
                float f3 = nVar.f5292c;
                int i2 = ((int) f2) - ((int) f3);
                float f4 = nVar4.f5293d;
                float f5 = nVar.f5293d;
                if (i2 > ((int) f4) - ((int) f5)) {
                    com.badlogic.gdx.math.n nVar5 = bVar.f4965a.f4967c;
                    nVar5.f5290a = nVar4.f5290a;
                    nVar5.f5291b = nVar4.f5291b;
                    nVar5.f5292c = f3;
                    nVar5.f5293d = f4;
                    com.badlogic.gdx.math.n nVar6 = bVar.f4966b.f4967c;
                    float f6 = nVar4.f5290a;
                    float f7 = nVar.f5292c;
                    nVar6.f5290a = f6 + f7;
                    nVar6.f5291b = nVar4.f5291b;
                    nVar6.f5292c = nVar4.f5292c - f7;
                    nVar6.f5293d = nVar4.f5293d;
                } else {
                    com.badlogic.gdx.math.n nVar7 = bVar.f4965a.f4967c;
                    nVar7.f5290a = nVar4.f5290a;
                    nVar7.f5291b = nVar4.f5291b;
                    nVar7.f5292c = f2;
                    nVar7.f5293d = f5;
                    com.badlogic.gdx.math.n nVar8 = bVar.f4966b.f4967c;
                    nVar8.f5290a = nVar4.f5290a;
                    float f8 = nVar4.f5291b;
                    float f9 = nVar.f5293d;
                    nVar8.f5291b = f8 + f9;
                    nVar8.f5292c = nVar4.f5292c;
                    nVar8.f5293d = nVar4.f5293d - f9;
                }
                return a(bVar.f4965a, nVar);
            }
        }
    }

    public synchronized com.badlogic.gdx.math.n a(String str) {
        Iterator<c> it = this.l.iterator();
        while (it.hasNext()) {
            com.badlogic.gdx.math.n b2 = it.next().f4969a.b(str);
            if (b2 != null) {
                return b2;
            }
        }
        return null;
    }

    public synchronized q a(n.b bVar, n.b bVar2, boolean z) {
        q qVar;
        qVar = new q();
        a(qVar, bVar, bVar2, z);
        return qVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.i.a(com.badlogic.gdx.graphics.g2d.q, e.d.b.t.n$b, e.d.b.t.n$b, boolean, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.q, e.d.b.t.n$b, e.d.b.t.n$b, boolean, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.i.a(e.d.b.t.l, int, int, boolean, boolean):int
      com.badlogic.gdx.graphics.g2d.i.a(com.badlogic.gdx.graphics.g2d.q, e.d.b.t.n$b, e.d.b.t.n$b, boolean, boolean):void */
    public synchronized void a(q qVar, n.b bVar, n.b bVar2, boolean z) {
        a(qVar, bVar, bVar2, z, true);
    }

    public synchronized void a(q qVar, n.b bVar, n.b bVar2, boolean z, boolean z2) {
        b(bVar, bVar2, z);
        Iterator<c> it = this.l.iterator();
        while (it.hasNext()) {
            c next = it.next();
            if (next.f4972d.f5374b > 0) {
                Iterator<String> it2 = next.f4972d.iterator();
                while (it2.hasNext()) {
                    String next2 = it2.next();
                    d b2 = next.f4969a.b(next2);
                    q.b bVar3 = new q.b(next.f4971c, (int) b2.f5290a, (int) b2.f5291b, (int) b2.f5292c, (int) b2.f5293d);
                    if (b2.f4975f != null) {
                        bVar3.r = b2.f4975f;
                        bVar3.s = b2.f4976g;
                    }
                    int i2 = -1;
                    if (z2) {
                        Matcher matcher = o.matcher(next2);
                        if (matcher.matches()) {
                            next2 = matcher.group(1);
                            i2 = Integer.parseInt(matcher.group(2));
                        }
                    }
                    bVar3.f5036i = next2;
                    bVar3.f5035h = i2;
                    bVar3.f5037j = (float) b2.f4977h;
                    bVar3.f5038k = (float) ((int) ((((float) b2.f4980k) - b2.f5293d) - ((float) b2.f4978i)));
                    bVar3.n = b2.f4979j;
                    bVar3.o = b2.f4980k;
                    qVar.j().add(bVar3);
                }
                next.f4972d.clear();
                qVar.k().add(next.f4971c);
            }
        }
    }

    public synchronized void a(com.badlogic.gdx.utils.a<r> aVar, n.b bVar, n.b bVar2, boolean z) {
        b(bVar, bVar2, z);
        while (aVar.f5374b < this.l.f5374b) {
            aVar.add(new r(this.l.get(aVar.f5374b).f4971c));
        }
    }

    public void a(e.d.b.t.b bVar) {
        this.f4963k.b(bVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.i.a(e.d.b.t.l, int, int, boolean, boolean):int
     arg types: [e.d.b.t.l, int, int, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.i.a(com.badlogic.gdx.graphics.g2d.q, e.d.b.t.n$b, e.d.b.t.n$b, boolean, boolean):void
      com.badlogic.gdx.graphics.g2d.i.a(e.d.b.t.l, int, int, boolean, boolean):int */
    private int[] a(e.d.b.t.l lVar, int[] iArr) {
        int i2;
        int i3;
        int i4;
        int[] iArr2 = iArr;
        int o2 = lVar.o() - 1;
        int q = lVar.q() - 1;
        int a2 = a(lVar, 1, o2, true, true);
        int a3 = a(lVar, q, 1, true, false);
        if (a2 != 0) {
            i2 = a(lVar, a2 + 1, o2, false, true);
        } else {
            i2 = 0;
        }
        if (a3 != 0) {
            i3 = a(lVar, q, a3 + 1, false, false);
        } else {
            i3 = 0;
        }
        a(lVar, i2 + 1, o2, true, true);
        a(lVar, q, i3 + 1, true, false);
        if (a2 == 0 && i2 == 0 && a3 == 0 && i3 == 0) {
            return null;
        }
        int i5 = -1;
        if (a2 == 0 && i2 == 0) {
            i4 = -1;
            a2 = -1;
        } else if (a2 > 0) {
            a2--;
            i4 = (lVar.q() - 2) - (i2 - 1);
        } else {
            i4 = lVar.q() - 2;
        }
        if (a3 == 0 && i3 == 0) {
            a3 = -1;
        } else if (a3 > 0) {
            a3--;
            i5 = (lVar.o() - 2) - (i3 - 1);
        } else {
            i5 = lVar.o() - 2;
        }
        int[] iArr3 = {a2, i4, a3, i5};
        if (iArr2 == null || !Arrays.equals(iArr3, iArr2)) {
            return iArr3;
        }
        return null;
    }

    private int a(e.d.b.t.l lVar, int i2, int i3, boolean z, boolean z2) {
        int i4;
        int i5;
        e.d.b.t.l lVar2;
        int[] iArr = new int[4];
        int i6 = z2 ? i2 : i3;
        int q = z2 ? lVar.q() : lVar.o();
        int i7 = z ? 255 : 0;
        int i8 = i2;
        int i9 = i3;
        while (i6 != q) {
            if (z2) {
                lVar2 = lVar;
                i4 = i9;
                i5 = i6;
            } else {
                i4 = i6;
                i5 = i8;
                lVar2 = lVar;
            }
            this.n.a(lVar2.a(i5, i4));
            e.d.b.t.b bVar = this.n;
            iArr[0] = (int) (bVar.f6934a * 255.0f);
            iArr[1] = (int) (bVar.f6935b * 255.0f);
            iArr[2] = (int) (bVar.f6936c * 255.0f);
            iArr[3] = (int) (bVar.f6937d * 255.0f);
            if (iArr[3] == i7) {
                return i6;
            }
            if (!z && !(iArr[0] == 0 && iArr[1] == 0 && iArr[2] == 0 && iArr[3] == 255)) {
                PrintStream printStream = System.out;
                printStream.println(i5 + "  " + i4 + RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER + iArr + RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER);
            }
            i6++;
            i8 = i5;
            i9 = i4;
        }
        return 0;
    }
}
