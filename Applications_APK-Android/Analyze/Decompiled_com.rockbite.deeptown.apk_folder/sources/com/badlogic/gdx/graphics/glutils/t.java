package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.h;
import com.badlogic.gdx.math.p;
import com.badlogic.gdx.utils.l;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.gass.AdShield2Logger;
import e.d.b.g;
import e.d.b.t.b;

/* compiled from: ShapeRenderer */
public class t implements l {

    /* renamed from: a  reason: collision with root package name */
    private final j f5187a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f5188b;

    /* renamed from: c  reason: collision with root package name */
    private final Matrix4 f5189c;

    /* renamed from: d  reason: collision with root package name */
    private final Matrix4 f5190d;

    /* renamed from: e  reason: collision with root package name */
    private final Matrix4 f5191e;

    /* renamed from: f  reason: collision with root package name */
    private final p f5192f;

    /* renamed from: g  reason: collision with root package name */
    private final b f5193g;

    /* renamed from: h  reason: collision with root package name */
    private a f5194h;

    /* renamed from: i  reason: collision with root package name */
    private boolean f5195i;

    /* renamed from: j  reason: collision with root package name */
    private float f5196j;

    /* compiled from: ShapeRenderer */
    public enum a {
        Point(0),
        Line(1),
        Filled(4);
        

        /* renamed from: a  reason: collision with root package name */
        private final int f5201a;

        private a(int i2) {
            this.f5201a = i2;
        }

        public int a() {
            return this.f5201a;
        }
    }

    public t() {
        this(AdShield2Logger.EVENTID_CLICK_SIGNALS);
    }

    public void a(a aVar) {
        if (this.f5194h == null) {
            this.f5194h = aVar;
            if (this.f5188b) {
                this.f5191e.b(this.f5189c);
                Matrix4.mul(this.f5191e.f5238a, this.f5190d.f5238a);
                this.f5188b = false;
            }
            this.f5187a.a(this.f5191e, this.f5194h.a());
            return;
        }
        throw new IllegalStateException("Call end() before beginning a new shape batch.");
    }

    public void b(boolean z) {
        this.f5195i = z;
    }

    public void begin() {
        if (this.f5195i) {
            a(a.Line);
            return;
        }
        throw new IllegalStateException("autoShapeType must be true to use this method.");
    }

    public void c(float f2, float f3, float f4) {
        a(f2, f3, f4, Math.max(1, (int) (((float) Math.cbrt((double) f4)) * 6.0f)));
    }

    public void d(float f2, float f3, float f4) {
        float f5 = f2 - f4;
        float f6 = f3 - f4;
        float f7 = f2 + f4;
        float f8 = f3 + f4;
        a(f5, f6, f7, f8);
        a(f5, f8, f7, f6);
    }

    public void dispose() {
        this.f5187a.dispose();
    }

    public void end() {
        this.f5187a.end();
        this.f5194h = null;
    }

    public void flush() {
        a aVar = this.f5194h;
        if (aVar != null) {
            end();
            a(aVar);
        }
    }

    public Matrix4 getTransformMatrix() {
        return this.f5190d;
    }

    public boolean j() {
        return this.f5194h != null;
    }

    public void setColor(b bVar) {
        this.f5193g.b(bVar);
    }

    public void setProjectionMatrix(Matrix4 matrix4) {
        this.f5189c.b(matrix4);
        this.f5188b = true;
    }

    public void setTransformMatrix(Matrix4 matrix4) {
        this.f5190d.b(matrix4);
        this.f5188b = true;
    }

    public t(int i2) {
        this(i2, null);
    }

    public void b(a aVar) {
        a aVar2 = this.f5194h;
        if (aVar2 != aVar) {
            if (aVar2 == null) {
                throw new IllegalStateException("begin must be called first.");
            } else if (this.f5195i) {
                end();
                a(aVar);
            } else {
                throw new IllegalStateException("autoShapeType must be enabled.");
            }
        }
    }

    public void setColor(float f2, float f3, float f4, float f5) {
        this.f5193g.b(f2, f3, f4, f5);
    }

    public t(int i2, s sVar) {
        this.f5188b = false;
        this.f5189c = new Matrix4();
        this.f5190d = new Matrix4();
        this.f5191e = new Matrix4();
        this.f5192f = new p();
        this.f5193g = new b(1.0f, 1.0f, 1.0f, 1.0f);
        this.f5196j = 0.75f;
        if (sVar == null) {
            this.f5187a = new i(i2, false, true, 0);
        } else {
            this.f5187a = new i(i2, false, true, 0, sVar);
        }
        this.f5189c.a(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) g.f6801b.getWidth(), (float) g.f6801b.getHeight());
        this.f5188b = true;
    }

    public void b(float f2, float f3, float f4, float f5) {
        a(a.Line, a.Filled, 8);
        float b2 = this.f5193g.b();
        if (this.f5194h == a.Line) {
            this.f5187a.a(b2);
            this.f5187a.a(f2, f3, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            float f6 = f4 + f2;
            this.f5187a.a(f6, f3, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f6, f3, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            float f7 = f5 + f3;
            this.f5187a.a(f6, f7, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f6, f7, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f2, f7, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f2, f7, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f2, f3, Animation.CurveTimeline.LINEAR);
            return;
        }
        this.f5187a.a(b2);
        this.f5187a.a(f2, f3, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        float f8 = f4 + f2;
        this.f5187a.a(f8, f3, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        float f9 = f5 + f3;
        this.f5187a.a(f8, f9, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        this.f5187a.a(f8, f9, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        this.f5187a.a(f2, f9, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        this.f5187a.a(f2, f3, Animation.CurveTimeline.LINEAR);
    }

    public final void a(float f2, float f3, float f4, float f5) {
        b bVar = this.f5193g;
        a(f2, f3, Animation.CurveTimeline.LINEAR, f4, f5, Animation.CurveTimeline.LINEAR, bVar, bVar);
    }

    public void a(float f2, float f3, float f4, float f5, float f6, float f7, b bVar, b bVar2) {
        b bVar3 = bVar;
        b bVar4 = bVar2;
        if (this.f5194h == a.Filled) {
            a(f2, f3, f5, f6, this.f5196j, bVar, bVar2);
            return;
        }
        a(a.Line, (a) null, 2);
        this.f5187a.a(bVar3.f6934a, bVar3.f6935b, bVar3.f6936c, bVar3.f6937d);
        this.f5187a.a(f2, f3, f4);
        this.f5187a.a(bVar4.f6934a, bVar4.f6935b, bVar4.f6936c, bVar4.f6937d);
        this.f5187a.a(f5, f6, f7);
    }

    public void a(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, int i2) {
        float f10 = f8;
        float f11 = f9;
        int i3 = i2;
        a(a.Line, (a) null, (i3 * 2) + 2);
        float b2 = this.f5193g.b();
        float f12 = 1.0f / ((float) i3);
        float f13 = f12 * f12;
        float f14 = f13 * f12;
        float f15 = f12 * 3.0f;
        float f16 = f13 * 3.0f;
        float f17 = f13 * 6.0f;
        float f18 = 6.0f * f14;
        float f19 = (f2 - (f4 * 2.0f)) + f6;
        float f20 = (f3 - (2.0f * f5)) + f7;
        float f21 = (((f4 - f6) * 3.0f) - f2) + f10;
        float f22 = (((f5 - f7) * 3.0f) - f3) + f11;
        float f23 = ((f4 - f2) * f15) + (f19 * f16) + (f21 * f14);
        float f24 = ((f5 - f3) * f15) + (f16 * f20) + (f14 * f22);
        float f25 = f21 * f18;
        float f26 = (f19 * f17) + f25;
        float f27 = f22 * f18;
        float f28 = (f20 * f17) + f27;
        float f29 = f2;
        float f30 = f3;
        while (true) {
            int i4 = i3 - 1;
            if (i3 > 0) {
                this.f5187a.a(b2);
                this.f5187a.a(f29, f30, Animation.CurveTimeline.LINEAR);
                f29 += f23;
                f30 += f24;
                f23 += f26;
                f24 += f28;
                f26 += f25;
                f28 += f27;
                this.f5187a.a(b2);
                this.f5187a.a(f29, f30, Animation.CurveTimeline.LINEAR);
                i3 = i4;
            } else {
                this.f5187a.a(b2);
                this.f5187a.a(f29, f30, Animation.CurveTimeline.LINEAR);
                this.f5187a.a(b2);
                this.f5187a.a(f10, f11, Animation.CurveTimeline.LINEAR);
                return;
            }
        }
    }

    public void a(float f2, float f3, float f4, float f5, float f6, float f7) {
        a(a.Line, a.Filled, 6);
        float b2 = this.f5193g.b();
        if (this.f5194h == a.Line) {
            this.f5187a.a(b2);
            this.f5187a.a(f2, f3, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f4, f5, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f4, f5, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f6, f7, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f6, f7, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f2, f3, Animation.CurveTimeline.LINEAR);
            return;
        }
        this.f5187a.a(b2);
        this.f5187a.a(f2, f3, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        this.f5187a.a(f4, f5, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        this.f5187a.a(f6, f7, Animation.CurveTimeline.LINEAR);
    }

    public void b(float[] fArr, int i2, int i3) {
        float f2;
        float f3;
        if (i3 < 6) {
            throw new IllegalArgumentException("Polygons must contain at least 3 points.");
        } else if (i3 % 2 == 0) {
            a(a.Line, (a) null, i3);
            float b2 = this.f5193g.b();
            float f4 = fArr[0];
            float f5 = fArr[1];
            int i4 = i2 + i3;
            while (i2 < i4) {
                float f6 = fArr[i2];
                float f7 = fArr[i2 + 1];
                int i5 = i2 + 2;
                if (i5 >= i3) {
                    f3 = f4;
                    f2 = f5;
                } else {
                    f3 = fArr[i5];
                    f2 = fArr[i2 + 3];
                }
                this.f5187a.a(b2);
                this.f5187a.a(f6, f7, Animation.CurveTimeline.LINEAR);
                this.f5187a.a(b2);
                this.f5187a.a(f3, f2, Animation.CurveTimeline.LINEAR);
                i2 = i5;
            }
        } else {
            throw new IllegalArgumentException("Polygons must have an even number of vertices.");
        }
    }

    public void a(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10) {
        b bVar = this.f5193g;
        a(f2, f3, f4, f5, f6, f7, f8, f9, f10, bVar, bVar, bVar, bVar);
    }

    public void a(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, b bVar, b bVar2, b bVar3, b bVar4) {
        float f11 = f4;
        float f12 = f5;
        b bVar5 = bVar;
        b bVar6 = bVar2;
        b bVar7 = bVar3;
        b bVar8 = bVar4;
        a(a.Line, a.Filled, 8);
        float b2 = h.b(f10);
        float h2 = h.h(f10);
        float f13 = -f11;
        float f14 = -f12;
        float f15 = f6 - f11;
        float f16 = f7 - f12;
        if (!(f8 == 1.0f && f9 == 1.0f)) {
            f13 *= f8;
            f14 *= f9;
            f15 *= f8;
            f16 *= f9;
        }
        float f17 = f2 + f11;
        float f18 = f3 + f12;
        float f19 = h2 * f14;
        float f20 = ((b2 * f13) - f19) + f17;
        float f21 = f14 * b2;
        float f22 = (f13 * h2) + f21 + f18;
        float f23 = b2 * f15;
        float f24 = (f23 - f19) + f17;
        float f25 = f15 * h2;
        float f26 = f21 + f25 + f18;
        float f27 = (f23 - (h2 * f16)) + f17;
        float f28 = f25 + (b2 * f16) + f18;
        float f29 = (f27 - f24) + f20;
        float f30 = f28 - (f26 - f22);
        if (this.f5194h == a.Line) {
            this.f5187a.a(bVar5.f6934a, bVar5.f6935b, bVar5.f6936c, bVar5.f6937d);
            this.f5187a.a(f20, f22, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(bVar6.f6934a, bVar6.f6935b, bVar6.f6936c, bVar6.f6937d);
            this.f5187a.a(f24, f26, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(bVar6.f6934a, bVar6.f6935b, bVar6.f6936c, bVar6.f6937d);
            this.f5187a.a(f24, f26, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(bVar7.f6934a, bVar7.f6935b, bVar7.f6936c, bVar7.f6937d);
            this.f5187a.a(f27, f28, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(bVar7.f6934a, bVar7.f6935b, bVar7.f6936c, bVar7.f6937d);
            this.f5187a.a(f27, f28, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(bVar8.f6934a, bVar8.f6935b, bVar8.f6936c, bVar8.f6937d);
            float f31 = f29;
            float f32 = f30;
            this.f5187a.a(f31, f32, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(bVar8.f6934a, bVar8.f6935b, bVar8.f6936c, bVar8.f6937d);
            this.f5187a.a(f31, f32, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(bVar5.f6934a, bVar5.f6935b, bVar5.f6936c, bVar5.f6937d);
            this.f5187a.a(f20, f22, Animation.CurveTimeline.LINEAR);
            return;
        }
        this.f5187a.a(bVar5.f6934a, bVar5.f6935b, bVar5.f6936c, bVar5.f6937d);
        this.f5187a.a(f20, f22, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(bVar6.f6934a, bVar6.f6935b, bVar6.f6936c, bVar6.f6937d);
        this.f5187a.a(f24, f26, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(bVar7.f6934a, bVar7.f6935b, bVar7.f6936c, bVar7.f6937d);
        this.f5187a.a(f27, f28, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(bVar7.f6934a, bVar7.f6935b, bVar7.f6936c, bVar7.f6937d);
        this.f5187a.a(f27, f28, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(bVar8.f6934a, bVar8.f6935b, bVar8.f6936c, bVar8.f6937d);
        this.f5187a.a(f29, f30, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(bVar5.f6934a, bVar5.f6935b, bVar5.f6936c, bVar5.f6937d);
        this.f5187a.a(f20, f22, Animation.CurveTimeline.LINEAR);
    }

    public void a(float f2, float f3, float f4, float f5, float f6) {
        a(a.Line, a.Filled, 8);
        float b2 = this.f5193g.b();
        p pVar = this.f5192f;
        pVar.d(f5 - f3, f2 - f4);
        pVar.e();
        float f7 = f6 * 0.5f;
        float f8 = pVar.f5294a * f7;
        float f9 = pVar.f5295b * f7;
        if (this.f5194h == a.Line) {
            this.f5187a.a(b2);
            float f10 = f2 + f8;
            float f11 = f3 + f9;
            this.f5187a.a(f10, f11, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            float f12 = f2 - f8;
            float f13 = f3 - f9;
            this.f5187a.a(f12, f13, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            float f14 = f4 + f8;
            float f15 = f5 + f9;
            this.f5187a.a(f14, f15, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            float f16 = f4 - f8;
            float f17 = f5 - f9;
            this.f5187a.a(f16, f17, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f14, f15, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f10, f11, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f16, f17, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f12, f13, Animation.CurveTimeline.LINEAR);
            return;
        }
        this.f5187a.a(b2);
        this.f5187a.a(f2 + f8, f3 + f9, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        float f18 = f2 - f8;
        float f19 = f3 - f9;
        this.f5187a.a(f18, f19, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        float f20 = f4 + f8;
        float f21 = f5 + f9;
        this.f5187a.a(f20, f21, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        this.f5187a.a(f4 - f8, f5 - f9, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        this.f5187a.a(f20, f21, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        this.f5187a.a(f18, f19, Animation.CurveTimeline.LINEAR);
    }

    public void a(float f2, float f3, float f4, float f5, float f6, b bVar, b bVar2) {
        a(a.Line, a.Filled, 8);
        float b2 = bVar.b();
        float b3 = bVar2.b();
        p pVar = this.f5192f;
        pVar.d(f5 - f3, f2 - f4);
        pVar.e();
        float f7 = f6 * 0.5f;
        float f8 = pVar.f5294a * f7;
        float f9 = pVar.f5295b * f7;
        if (this.f5194h == a.Line) {
            this.f5187a.a(b2);
            float f10 = f2 + f8;
            float f11 = f3 + f9;
            this.f5187a.a(f10, f11, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            float f12 = f2 - f8;
            float f13 = f3 - f9;
            this.f5187a.a(f12, f13, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b3);
            float f14 = f4 + f8;
            float f15 = f5 + f9;
            this.f5187a.a(f14, f15, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b3);
            float f16 = f4 - f8;
            float f17 = f5 - f9;
            this.f5187a.a(f16, f17, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b3);
            this.f5187a.a(f14, f15, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f10, f11, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b3);
            this.f5187a.a(f16, f17, Animation.CurveTimeline.LINEAR);
            this.f5187a.a(b2);
            this.f5187a.a(f12, f13, Animation.CurveTimeline.LINEAR);
            return;
        }
        this.f5187a.a(b2);
        this.f5187a.a(f2 + f8, f3 + f9, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        float f18 = f2 - f8;
        float f19 = f3 - f9;
        this.f5187a.a(f18, f19, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b3);
        float f20 = f4 + f8;
        float f21 = f5 + f9;
        this.f5187a.a(f20, f21, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b3);
        this.f5187a.a(f4 - f8, f5 - f9, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b3);
        this.f5187a.a(f20, f21, Animation.CurveTimeline.LINEAR);
        this.f5187a.a(b2);
        this.f5187a.a(f18, f19, Animation.CurveTimeline.LINEAR);
    }

    public void a(p pVar, p pVar2, float f2) {
        a(pVar.f5294a, pVar.f5295b, pVar2.f5294a, pVar2.f5295b, f2);
    }

    public void a(float f2, float f3, float f4, int i2) {
        if (i2 > 0) {
            float b2 = this.f5193g.b();
            float f5 = 6.2831855f / ((float) i2);
            float a2 = h.a(f5);
            float g2 = h.g(f5);
            a aVar = this.f5194h;
            a aVar2 = a.Line;
            int i3 = 0;
            if (aVar == aVar2) {
                a(aVar2, a.Filled, (i2 * 2) + 2);
                float f6 = f4;
                float f7 = Animation.CurveTimeline.LINEAR;
                while (i3 < i2) {
                    this.f5187a.a(b2);
                    this.f5187a.a(f2 + f6, f3 + f7, Animation.CurveTimeline.LINEAR);
                    float f8 = (a2 * f6) - (g2 * f7);
                    f7 = (f7 * a2) + (f6 * g2);
                    this.f5187a.a(b2);
                    this.f5187a.a(f2 + f8, f3 + f7, Animation.CurveTimeline.LINEAR);
                    i3++;
                    f6 = f8;
                }
                this.f5187a.a(b2);
                this.f5187a.a(f6 + f2, f7 + f3, Animation.CurveTimeline.LINEAR);
            } else {
                a(aVar2, a.Filled, (i2 * 3) + 3);
                int i4 = i2 - 1;
                float f9 = f4;
                float f10 = Animation.CurveTimeline.LINEAR;
                while (i3 < i4) {
                    this.f5187a.a(b2);
                    this.f5187a.a(f2, f3, Animation.CurveTimeline.LINEAR);
                    this.f5187a.a(b2);
                    this.f5187a.a(f2 + f9, f3 + f10, Animation.CurveTimeline.LINEAR);
                    float f11 = (a2 * f9) - (g2 * f10);
                    f10 = (f10 * a2) + (f9 * g2);
                    this.f5187a.a(b2);
                    this.f5187a.a(f2 + f11, f3 + f10, Animation.CurveTimeline.LINEAR);
                    i3++;
                    f9 = f11;
                }
                this.f5187a.a(b2);
                this.f5187a.a(f2, f3, Animation.CurveTimeline.LINEAR);
                this.f5187a.a(b2);
                this.f5187a.a(f9 + f2, f10 + f3, Animation.CurveTimeline.LINEAR);
            }
            this.f5187a.a(b2);
            this.f5187a.a(f2 + f4, f3 + Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
            return;
        }
        throw new IllegalArgumentException("segments must be > 0.");
    }

    private void a(a aVar, a aVar2, int i2) {
        a aVar3 = this.f5194h;
        if (aVar3 == null) {
            throw new IllegalStateException("begin must be called first.");
        } else if (aVar3 == aVar || aVar3 == aVar2) {
            if (this.f5188b) {
                a aVar4 = this.f5194h;
                end();
                a(aVar4);
            } else if (this.f5187a.d() - this.f5187a.c() < i2) {
                a aVar5 = this.f5194h;
                end();
                a(aVar5);
            }
        } else if (this.f5195i) {
            end();
            a(aVar);
        } else if (aVar2 == null) {
            throw new IllegalStateException("Must call begin(ShapeType." + aVar + ").");
        } else {
            throw new IllegalStateException("Must call begin(ShapeType." + aVar + ") or begin(ShapeType." + aVar2 + ").");
        }
    }
}
