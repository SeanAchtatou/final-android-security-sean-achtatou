package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.p;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.b0;
import com.badlogic.gdx.utils.c0;
import com.badlogic.gdx.utils.l;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import e.d.b.a;
import e.d.b.g;
import e.d.b.t.b;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

/* compiled from: ShaderProgram */
public class s implements l {
    public static boolean s = true;
    public static String t = "";
    public static String u = "";
    private static final c0<a, com.badlogic.gdx.utils.a<s>> v = new c0<>();

    /* renamed from: a  reason: collision with root package name */
    private String f5176a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f5177b;

    /* renamed from: c  reason: collision with root package name */
    private final b0<String> f5178c;

    /* renamed from: d  reason: collision with root package name */
    private final b0<String> f5179d;

    /* renamed from: e  reason: collision with root package name */
    private final b0<String> f5180e;

    /* renamed from: f  reason: collision with root package name */
    private String[] f5181f;

    /* renamed from: g  reason: collision with root package name */
    private final b0<String> f5182g;

    /* renamed from: h  reason: collision with root package name */
    private final b0<String> f5183h;

    /* renamed from: i  reason: collision with root package name */
    private final b0<String> f5184i;

    /* renamed from: j  reason: collision with root package name */
    private String[] f5185j;

    /* renamed from: k  reason: collision with root package name */
    private int f5186k;
    private int l;
    private int m;
    private final String n;
    private final String o;
    private boolean p;
    IntBuffer q;
    IntBuffer r;

    static {
        BufferUtils.c(1);
    }

    public s(String str, String str2) {
        this.f5176a = "";
        this.f5178c = new b0<>();
        this.f5179d = new b0<>();
        this.f5180e = new b0<>();
        this.f5182g = new b0<>();
        this.f5183h = new b0<>();
        this.f5184i = new b0<>();
        this.q = BufferUtils.c(1);
        this.r = BufferUtils.c(1);
        if (str == null) {
            throw new IllegalArgumentException("vertex shader must not be null");
        } else if (str2 != null) {
            String str3 = t;
            if (str3 != null && str3.length() > 0) {
                str = t + str;
            }
            String str4 = u;
            if (str4 != null && str4.length() > 0) {
                str2 = u + str2;
            }
            this.n = str;
            this.o = str2;
            BufferUtils.b(16);
            a(str, str2);
            if (l()) {
                n();
                o();
                a(g.f6800a, this);
            }
        } else {
            throw new IllegalArgumentException("fragment shader must not be null");
        }
    }

    private void a(String str, String str2) {
        this.l = a(35633, str);
        this.m = a(35632, str2);
        if (this.l == -1 || this.m == -1) {
            this.f5177b = false;
            return;
        }
        this.f5186k = c(j());
        if (this.f5186k == -1) {
            this.f5177b = false;
        } else {
            this.f5177b = true;
        }
    }

    private int c(int i2) {
        e.d.b.t.g gVar = g.f6807h;
        if (i2 == -1) {
            return -1;
        }
        gVar.b(i2, this.l);
        gVar.b(i2, this.m);
        gVar.o(i2);
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(4);
        allocateDirect.order(ByteOrder.nativeOrder());
        IntBuffer asIntBuffer = allocateDirect.asIntBuffer();
        gVar.a(i2, 35714, asIntBuffer);
        if (asIntBuffer.get(0) != 0) {
            return i2;
        }
        this.f5176a = g.f6807h.h(i2);
        return -1;
    }

    private int d(String str) {
        e.d.b.t.g gVar = g.f6807h;
        int a2 = this.f5182g.a(str, -2);
        if (a2 != -2) {
            return a2;
        }
        int c2 = gVar.c(this.f5186k, str);
        this.f5182g.b(str, c2);
        return c2;
    }

    private int e(String str) {
        return a(str, s);
    }

    private void m() {
        if (this.p) {
            a(this.n, this.o);
            this.p = false;
        }
    }

    private void n() {
        this.q.clear();
        g.f6807h.a(this.f5186k, 35721, this.q);
        int i2 = this.q.get(0);
        this.f5185j = new String[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            this.q.clear();
            this.q.put(0, 1);
            this.r.clear();
            String b2 = g.f6807h.b(this.f5186k, i3, this.q, this.r);
            this.f5182g.b(b2, g.f6807h.c(this.f5186k, b2));
            this.f5183h.b(b2, this.r.get(0));
            this.f5184i.b(b2, this.q.get(0));
            this.f5185j[i3] = b2;
        }
    }

    private void o() {
        this.q.clear();
        g.f6807h.a(this.f5186k, 35718, this.q);
        int i2 = this.q.get(0);
        this.f5181f = new String[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            this.q.clear();
            this.q.put(0, 1);
            this.r.clear();
            String a2 = g.f6807h.a(this.f5186k, i3, this.q, this.r);
            this.f5178c.b(a2, g.f6807h.b(this.f5186k, a2));
            this.f5179d.b(a2, this.r.get(0));
            this.f5180e.b(a2, this.q.get(0));
            this.f5181f[i3] = a2;
        }
    }

    public static String p() {
        StringBuilder sb = new StringBuilder();
        sb.append("Managed shaders/app: { ");
        c0.c<a> b2 = v.b();
        b2.iterator();
        while (b2.hasNext()) {
            sb.append(v.b((a) b2.next()).f5374b);
            sb.append(RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER);
        }
        sb.append("}");
        return sb.toString();
    }

    public void b(int i2) {
        e.d.b.t.g gVar = g.f6807h;
        m();
        gVar.s(i2);
    }

    public void begin() {
        e.d.b.t.g gVar = g.f6807h;
        m();
        gVar.f(this.f5186k);
    }

    public void dispose() {
        e.d.b.t.g gVar = g.f6807h;
        gVar.f(0);
        gVar.j(this.l);
        gVar.j(this.m);
        gVar.b(this.f5186k);
        if (v.b(g.f6800a) != null) {
            v.b(g.f6800a).d(this, true);
        }
    }

    public void end() {
        g.f6807h.f(0);
    }

    /* access modifiers changed from: protected */
    public int j() {
        int c2 = g.f6807h.c();
        if (c2 != 0) {
            return c2;
        }
        return -1;
    }

    public String k() {
        if (!this.f5177b) {
            return this.f5176a;
        }
        this.f5176a = g.f6807h.h(this.f5186k);
        return this.f5176a;
    }

    public boolean l() {
        return this.f5177b;
    }

    public static void b(a aVar) {
        com.badlogic.gdx.utils.a b2;
        if (g.f6807h != null && (b2 = v.b(aVar)) != null) {
            for (int i2 = 0; i2 < b2.f5374b; i2++) {
                ((s) b2.get(i2)).p = true;
                ((s) b2.get(i2)).m();
            }
        }
    }

    private int a(int i2, String str) {
        e.d.b.t.g gVar = g.f6807h;
        IntBuffer c2 = BufferUtils.c(1);
        int v2 = gVar.v(i2);
        if (v2 == 0) {
            return -1;
        }
        gVar.a(v2, str);
        gVar.q(v2);
        gVar.b(v2, 35713, c2);
        if (c2.get(0) != 0) {
            return v2;
        }
        String t2 = gVar.t(v2);
        StringBuilder sb = new StringBuilder();
        sb.append(this.f5176a);
        sb.append(i2 == 35633 ? "Vertex shader\n" : "Fragment shader:\n");
        this.f5176a = sb.toString();
        this.f5176a += t2;
        return -1;
    }

    public int b(String str) {
        return this.f5182g.a(str, -1);
    }

    public boolean c(String str) {
        return this.f5178c.a(str);
    }

    public int a(String str, boolean z) {
        e.d.b.t.g gVar = g.f6807h;
        int a2 = this.f5178c.a(str, -2);
        if (a2 == -2) {
            a2 = gVar.b(this.f5186k, str);
            if (a2 != -1 || !z) {
                this.f5178c.b(str, a2);
            } else {
                throw new IllegalArgumentException("no uniform with name '" + str + "' in shader");
            }
        }
        return a2;
    }

    public void a(String str, int i2) {
        e.d.b.t.g gVar = g.f6807h;
        m();
        gVar.d(e(str), i2);
    }

    public s(e.d.b.s.a aVar, e.d.b.s.a aVar2) {
        this(aVar.n(), aVar2.n());
    }

    public void a(String str, float f2) {
        e.d.b.t.g gVar = g.f6807h;
        m();
        gVar.a(e(str), f2);
    }

    public void a(String str, float f2, float f3) {
        e.d.b.t.g gVar = g.f6807h;
        m();
        gVar.a(e(str), f2, f3);
    }

    public void a(String str, float f2, float f3, float f4, float f5) {
        e.d.b.t.g gVar = g.f6807h;
        m();
        gVar.a(e(str), f2, f3, f4, f5);
    }

    public void a(String str, float[] fArr, int i2, int i3) {
        e.d.b.t.g gVar = g.f6807h;
        m();
        gVar.a(e(str), i3 / 3, fArr, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.glutils.s.a(java.lang.String, com.badlogic.gdx.math.Matrix4, boolean):void
     arg types: [java.lang.String, com.badlogic.gdx.math.Matrix4, int]
     candidates:
      com.badlogic.gdx.graphics.glutils.s.a(int, com.badlogic.gdx.math.Matrix4, boolean):void
      com.badlogic.gdx.graphics.glutils.s.a(java.lang.String, float, float):void
      com.badlogic.gdx.graphics.glutils.s.a(java.lang.String, com.badlogic.gdx.math.Matrix4, boolean):void */
    public void a(String str, Matrix4 matrix4) {
        a(str, matrix4, false);
    }

    public void a(String str, Matrix4 matrix4, boolean z) {
        a(e(str), matrix4, z);
    }

    public void a(int i2, Matrix4 matrix4, boolean z) {
        e.d.b.t.g gVar = g.f6807h;
        m();
        gVar.a(i2, 1, z, matrix4.f5238a, 0);
    }

    public void a(String str, p pVar) {
        a(str, pVar.f5294a, pVar.f5295b);
    }

    public void a(String str, b bVar) {
        a(str, bVar.f6934a, bVar.f6935b, bVar.f6936c, bVar.f6937d);
    }

    public void a(int i2, int i3, int i4, boolean z, int i5, Buffer buffer) {
        e.d.b.t.g gVar = g.f6807h;
        m();
        gVar.a(i2, i3, i4, z, i5, buffer);
    }

    public void a(int i2, int i3, int i4, boolean z, int i5, int i6) {
        e.d.b.t.g gVar = g.f6807h;
        m();
        gVar.a(i2, i3, i4, z, i5, i6);
    }

    public void a(String str) {
        e.d.b.t.g gVar = g.f6807h;
        m();
        int d2 = d(str);
        if (d2 != -1) {
            gVar.p(d2);
        }
    }

    public void a(int i2) {
        e.d.b.t.g gVar = g.f6807h;
        m();
        gVar.p(i2);
    }

    private void a(a aVar, s sVar) {
        com.badlogic.gdx.utils.a b2 = v.b(aVar);
        if (b2 == null) {
            b2 = new com.badlogic.gdx.utils.a();
        }
        b2.add(sVar);
        v.b(aVar, b2);
    }

    public static void a(a aVar) {
        v.remove(aVar);
    }
}
