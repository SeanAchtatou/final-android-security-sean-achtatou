package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.g2d.e;
import com.badlogic.gdx.graphics.g2d.q;
import com.badlogic.gdx.utils.l;
import com.badlogic.gdx.utils.m;
import com.badlogic.gdx.utils.o;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.ads.AdRequest;
import e.d.b.g;
import e.d.b.t.n;

/* compiled from: BitmapFont */
public class c implements l {

    /* renamed from: a  reason: collision with root package name */
    final a f4803a;

    /* renamed from: b  reason: collision with root package name */
    com.badlogic.gdx.utils.a<r> f4804b;

    /* renamed from: c  reason: collision with root package name */
    private final d f4805c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f4806d;

    /* renamed from: e  reason: collision with root package name */
    boolean f4807e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f4808f;

    public c() {
        this(g.f6804e.c("com/badlogic/gdx/utils/arial-15.fnt"), g.f6804e.c("com/badlogic/gdx/utils/arial-15.png"), false, true);
    }

    /* access modifiers changed from: protected */
    public void a(a aVar) {
        for (b[] bVarArr : aVar.s) {
            if (bVarArr != null) {
                for (b bVar : bVarArr) {
                    if (bVar != null) {
                        aVar.a(bVar, this.f4804b.get(bVar.o));
                    }
                }
            }
        }
        b bVar2 = aVar.t;
        if (bVar2 != null) {
            aVar.a(bVar2, this.f4804b.get(bVar2.o));
        }
    }

    public void b(boolean z) {
        this.f4808f = z;
    }

    public void dispose() {
        if (this.f4808f) {
            int i2 = 0;
            while (true) {
                com.badlogic.gdx.utils.a<r> aVar = this.f4804b;
                if (i2 < aVar.f5374b) {
                    aVar.get(i2).e().dispose();
                    i2++;
                } else {
                    return;
                }
            }
        }
    }

    public e.d.b.t.b getColor() {
        return this.f4805c.b();
    }

    public float j() {
        return this.f4803a.f4818j;
    }

    public a k() {
        return this.f4803a;
    }

    public float l() {
        return this.f4803a.l;
    }

    public float m() {
        return this.f4803a.f4817i;
    }

    public com.badlogic.gdx.utils.a<r> n() {
        return this.f4804b;
    }

    public float o() {
        return this.f4803a.o;
    }

    public float p() {
        return this.f4803a.p;
    }

    public boolean q() {
        return this.f4806d;
    }

    public d r() {
        return new d(this, this.f4807e);
    }

    public boolean s() {
        return this.f4807e;
    }

    public void setColor(float f2, float f3, float f4, float f5) {
        this.f4805c.b().b(f2, f3, f4, f5);
    }

    public String toString() {
        String str = this.f4803a.f4809a;
        return str != null ? str : super.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.c.<init>(e.d.b.s.a, com.badlogic.gdx.graphics.g2d.r, boolean):void
     arg types: [e.d.b.s.a, com.badlogic.gdx.graphics.g2d.r, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.c.<init>(com.badlogic.gdx.graphics.g2d.c$a, com.badlogic.gdx.graphics.g2d.r, boolean):void
      com.badlogic.gdx.graphics.g2d.c.<init>(com.badlogic.gdx.graphics.g2d.c$a, com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.r>, boolean):void
      com.badlogic.gdx.graphics.g2d.c.<init>(e.d.b.s.a, e.d.b.s.a, boolean):void
      com.badlogic.gdx.graphics.g2d.c.<init>(e.d.b.s.a, com.badlogic.gdx.graphics.g2d.r, boolean):void */
    public c(e.d.b.s.a aVar, r rVar) {
        this(aVar, rVar, false);
    }

    /* compiled from: BitmapFont */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public int f4820a;

        /* renamed from: b  reason: collision with root package name */
        public int f4821b;

        /* renamed from: c  reason: collision with root package name */
        public int f4822c;

        /* renamed from: d  reason: collision with root package name */
        public int f4823d;

        /* renamed from: e  reason: collision with root package name */
        public int f4824e;

        /* renamed from: f  reason: collision with root package name */
        public float f4825f;

        /* renamed from: g  reason: collision with root package name */
        public float f4826g;

        /* renamed from: h  reason: collision with root package name */
        public float f4827h;

        /* renamed from: i  reason: collision with root package name */
        public float f4828i;

        /* renamed from: j  reason: collision with root package name */
        public int f4829j;

        /* renamed from: k  reason: collision with root package name */
        public int f4830k;
        public int l;
        public byte[][] m;
        public boolean n;
        public int o = 0;

        public int a(char c2) {
            byte[] bArr;
            byte[][] bArr2 = this.m;
            if (bArr2 == null || (bArr = bArr2[c2 >>> 9]) == null) {
                return 0;
            }
            return bArr[c2 & 511];
        }

        public String toString() {
            return Character.toString((char) this.f4820a);
        }

        public void a(int i2, int i3) {
            if (this.m == null) {
                this.m = new byte[128][];
            }
            byte[][] bArr = this.m;
            int i4 = i2 >>> 9;
            byte[] bArr2 = bArr[i4];
            if (bArr2 == null) {
                bArr2 = new byte[AdRequest.MAX_CONTENT_URL_LENGTH];
                bArr[i4] = bArr2;
            }
            bArr2[i2 & 511] = (byte) i3;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.c.<init>(com.badlogic.gdx.graphics.g2d.c$a, com.badlogic.gdx.graphics.g2d.r, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.c$a, com.badlogic.gdx.graphics.g2d.r, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.c.<init>(com.badlogic.gdx.graphics.g2d.c$a, com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.r>, boolean):void
      com.badlogic.gdx.graphics.g2d.c.<init>(e.d.b.s.a, com.badlogic.gdx.graphics.g2d.r, boolean):void
      com.badlogic.gdx.graphics.g2d.c.<init>(e.d.b.s.a, e.d.b.s.a, boolean):void
      com.badlogic.gdx.graphics.g2d.c.<init>(com.badlogic.gdx.graphics.g2d.c$a, com.badlogic.gdx.graphics.g2d.r, boolean):void */
    public c(e.d.b.s.a aVar, r rVar, boolean z) {
        this(new a(aVar, z), rVar, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.c.<init>(com.badlogic.gdx.graphics.g2d.c$a, com.badlogic.gdx.graphics.g2d.r, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.c$a, ?[OBJECT, ARRAY], int]
     candidates:
      com.badlogic.gdx.graphics.g2d.c.<init>(com.badlogic.gdx.graphics.g2d.c$a, com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.r>, boolean):void
      com.badlogic.gdx.graphics.g2d.c.<init>(e.d.b.s.a, com.badlogic.gdx.graphics.g2d.r, boolean):void
      com.badlogic.gdx.graphics.g2d.c.<init>(e.d.b.s.a, e.d.b.s.a, boolean):void
      com.badlogic.gdx.graphics.g2d.c.<init>(com.badlogic.gdx.graphics.g2d.c$a, com.badlogic.gdx.graphics.g2d.r, boolean):void */
    public c(e.d.b.s.a aVar, boolean z) {
        this(new a(aVar, z), (r) null, true);
    }

    public c(e.d.b.s.a aVar, e.d.b.s.a aVar2, boolean z) {
        this(aVar, aVar2, z, true);
    }

    public e a(b bVar, CharSequence charSequence, float f2, float f3, int i2, int i3, float f4, int i4, boolean z) {
        this.f4805c.a();
        e a2 = this.f4805c.a(charSequence, f2, f3, i2, i3, f4, i4, z);
        this.f4805c.a(bVar);
        return a2;
    }

    public c(e.d.b.s.a aVar, e.d.b.s.a aVar2, boolean z, boolean z2) {
        this(new a(aVar, z), new r(new n(aVar2, false)), z2);
        this.f4808f = true;
    }

    /* compiled from: BitmapFont */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public String f4809a;

        /* renamed from: b  reason: collision with root package name */
        public String[] f4810b;

        /* renamed from: c  reason: collision with root package name */
        public e.d.b.s.a f4811c;

        /* renamed from: d  reason: collision with root package name */
        public boolean f4812d;

        /* renamed from: e  reason: collision with root package name */
        public float f4813e;

        /* renamed from: f  reason: collision with root package name */
        public float f4814f;

        /* renamed from: g  reason: collision with root package name */
        public float f4815g;

        /* renamed from: h  reason: collision with root package name */
        public float f4816h;

        /* renamed from: i  reason: collision with root package name */
        public float f4817i;

        /* renamed from: j  reason: collision with root package name */
        public float f4818j = 1.0f;

        /* renamed from: k  reason: collision with root package name */
        public float f4819k;
        public float l;
        public float m;
        public float n = 1.0f;
        public float o = 1.0f;
        public float p = 1.0f;
        public boolean q;
        public float r;
        public final b[][] s = new b[128][];
        public b t;
        public float u;
        public float v = 1.0f;
        public char[] w;
        public char[] x = {'x', 'e', 'a', 'o', 'n', 's', 'r', 'c', 'u', 'm', 'v', 'w', 'z'};
        public char[] y = {'M', 'N', 'B', 'D', 'C', 'E', 'F', 'K', 'A', 'G', 'H', 'I', 'J', 'L', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

        public a() {
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(11:144|(1:146)(1:(1:148)(1:201))|149|(1:151)(1:152)|153|(1:155)|156|(2:158|159)|160|161|(2:165|204)) */
        /* JADX WARNING: Missing exception handler attribute for start block: B:160:0x0402 */
        /* JADX WARNING: Removed duplicated region for block: B:121:0x02d1 A[Catch:{ Exception -> 0x0454 }] */
        /* JADX WARNING: Removed duplicated region for block: B:124:0x02e9 A[Catch:{ Exception -> 0x0454 }] */
        /* JADX WARNING: Removed duplicated region for block: B:126:0x02f5 A[Catch:{ Exception -> 0x0454 }] */
        /* JADX WARNING: Removed duplicated region for block: B:199:0x0189 A[EDGE_INSN: B:199:0x0189->B:63:0x0189 ?: BREAK  , SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:205:0x0196 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:214:0x025c A[EDGE_INSN: B:214:0x025c->B:89:0x025c ?: BREAK  , SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:215:0x0282 A[EDGE_INSN: B:215:0x0282->B:97:0x0282 ?: BREAK  , SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00d8 A[Catch:{ NumberFormatException -> 0x0110, Exception -> 0x045b, all -> 0x0456 }] */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x017a  */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x0197 A[Catch:{ NumberFormatException -> 0x0110, Exception -> 0x045b, all -> 0x0456 }] */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x020f A[Catch:{ NumberFormatException -> 0x0110, Exception -> 0x045b, all -> 0x0456 }] */
        /* JADX WARNING: Removed duplicated region for block: B:82:0x022d A[Catch:{ NumberFormatException -> 0x0110, Exception -> 0x045b, all -> 0x0456 }] */
        /* JADX WARNING: Removed duplicated region for block: B:85:0x024c A[Catch:{ NumberFormatException -> 0x0110, Exception -> 0x045b, all -> 0x0456 }] */
        /* JADX WARNING: Removed duplicated region for block: B:90:0x025e A[Catch:{ NumberFormatException -> 0x0110, Exception -> 0x045b, all -> 0x0456 }] */
        /* JADX WARNING: Removed duplicated region for block: B:93:0x0272 A[Catch:{ NumberFormatException -> 0x0110, Exception -> 0x045b, all -> 0x0456 }] */
        /* JADX WARNING: Removed duplicated region for block: B:99:0x0286 A[Catch:{ NumberFormatException -> 0x0110, Exception -> 0x045b, all -> 0x0456 }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(e.d.b.s.a r23, boolean r24) {
            /*
                r22 = this;
                r1 = r22
                java.lang.String[] r2 = r1.f4810b
                if (r2 != 0) goto L_0x047e
                java.lang.String r2 = r23.h()
                r1.f4809a = r2
                java.io.BufferedReader r2 = new java.io.BufferedReader
                java.io.InputStreamReader r3 = new java.io.InputStreamReader
                java.io.InputStream r4 = r23.l()
                r3.<init>(r4)
                r4 = 512(0x200, float:7.175E-43)
                r2.<init>(r3, r4)
                java.lang.String r3 = r2.readLine()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r3 == 0) goto L_0x044a
                java.lang.String r4 = "padding="
                int r4 = r3.indexOf(r4)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r4 = r4 + 8
                java.lang.String r3 = r3.substring(r4)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r4 = 32
                int r5 = r3.indexOf(r4)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r6 = 0
                java.lang.String r3 = r3.substring(r6, r5)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r5 = ","
                r7 = 4
                java.lang.String[] r3 = r3.split(r5, r7)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r5 = r3.length     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r5 != r7) goto L_0x0440
                r5 = r3[r6]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r5 = java.lang.Integer.parseInt(r5)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r5 = (float) r5     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r1.f4813e = r5     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r5 = 1
                r7 = r3[r5]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r7 = java.lang.Integer.parseInt(r7)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r7 = (float) r7     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r1.f4814f = r7     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r7 = 2
                r8 = r3[r7]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r8 = java.lang.Integer.parseInt(r8)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r8 = (float) r8     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r1.f4815g = r8     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r8 = 3
                r3 = r3[r8]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r3 = (float) r3     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r1.f4816h = r3     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r3 = r1.f4813e     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r9 = r1.f4815g     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r3 = r3 + r9
                java.lang.String r9 = r2.readLine()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r9 == 0) goto L_0x0436
                java.lang.String r10 = " "
                r11 = 9
                java.lang.String[] r9 = r9.split(r10, r11)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r10 = r9.length     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r10 < r8) goto L_0x042c
                r8 = r9[r5]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r10 = "lineHeight="
                boolean r8 = r8.startsWith(r10)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r8 == 0) goto L_0x0422
                r8 = r9[r5]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r10 = 11
                java.lang.String r8 = r8.substring(r10)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r8 = java.lang.Integer.parseInt(r8)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r8 = (float) r8     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r1.f4817i = r8     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r8 = r9[r7]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r10 = "base="
                boolean r8 = r8.startsWith(r10)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r8 == 0) goto L_0x0418
                r7 = r9[r7]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r8 = 5
                java.lang.String r7 = r7.substring(r8)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r7 = java.lang.Integer.parseInt(r7)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r7 = (float) r7     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r10 = r9.length     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r11 = 6
                if (r10 < r11) goto L_0x00d0
                r10 = r9[r8]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r10 == 0) goto L_0x00d0
                r10 = r9[r8]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r12 = "pages="
                boolean r10 = r10.startsWith(r12)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r10 == 0) goto L_0x00d0
                r8 = r9[r8]     // Catch:{ NumberFormatException -> 0x00d0 }
                java.lang.String r8 = r8.substring(r11)     // Catch:{ NumberFormatException -> 0x00d0 }
                int r8 = java.lang.Integer.parseInt(r8)     // Catch:{ NumberFormatException -> 0x00d0 }
                int r8 = java.lang.Math.max(r5, r8)     // Catch:{ NumberFormatException -> 0x00d0 }
                goto L_0x00d1
            L_0x00d0:
                r8 = 1
            L_0x00d1:
                java.lang.String[] r9 = new java.lang.String[r8]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r1.f4810b = r9     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r9 = 0
            L_0x00d6:
                if (r9 >= r8) goto L_0x0169
                java.lang.String r10 = r2.readLine()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r10 == 0) goto L_0x0161
                java.lang.String r11 = ".*id=(\\d+)"
                java.util.regex.Pattern r11 = java.util.regex.Pattern.compile(r11)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.util.regex.Matcher r11 = r11.matcher(r10)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                boolean r12 = r11.find()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r12 == 0) goto L_0x0129
                java.lang.String r11 = r11.group(r5)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r12 = java.lang.Integer.parseInt(r11)     // Catch:{ NumberFormatException -> 0x0110 }
                if (r12 != r9) goto L_0x00f9
                goto L_0x0129
            L_0x00f9:
                com.badlogic.gdx.utils.o r3 = new com.badlogic.gdx.utils.o     // Catch:{ NumberFormatException -> 0x0110 }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x0110 }
                r4.<init>()     // Catch:{ NumberFormatException -> 0x0110 }
                java.lang.String r5 = "Page IDs must be indices starting at 0: "
                r4.append(r5)     // Catch:{ NumberFormatException -> 0x0110 }
                r4.append(r11)     // Catch:{ NumberFormatException -> 0x0110 }
                java.lang.String r4 = r4.toString()     // Catch:{ NumberFormatException -> 0x0110 }
                r3.<init>(r4)     // Catch:{ NumberFormatException -> 0x0110 }
                throw r3     // Catch:{ NumberFormatException -> 0x0110 }
            L_0x0110:
                r0 = move-exception
                r3 = r0
                com.badlogic.gdx.utils.o r4 = new com.badlogic.gdx.utils.o     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r5.<init>()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r6 = "Invalid page id: "
                r5.append(r6)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r5.append(r11)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r4.<init>(r5, r3)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                throw r4     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
            L_0x0129:
                java.lang.String r11 = ".*file=\"?([^\"]+)\"?"
                java.util.regex.Pattern r11 = java.util.regex.Pattern.compile(r11)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.util.regex.Matcher r10 = r11.matcher(r10)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                boolean r11 = r10.find()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r11 == 0) goto L_0x0159
                java.lang.String r10 = r10.group(r5)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String[] r11 = r1.f4810b     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                e.d.b.s.a r12 = r23.i()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                e.d.b.s.a r10 = r12.a(r10)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r10 = r10.j()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r12 = "\\\\"
                java.lang.String r13 = "/"
                java.lang.String r10 = r10.replaceAll(r12, r13)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r11[r9] = r10     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r9 = r9 + 1
                goto L_0x00d6
            L_0x0159:
                com.badlogic.gdx.utils.o r3 = new com.badlogic.gdx.utils.o     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r4 = "Missing: file"
                r3.<init>(r4)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                throw r3     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
            L_0x0161:
                com.badlogic.gdx.utils.o r3 = new com.badlogic.gdx.utils.o     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r4 = "Missing additional page definitions."
                r3.<init>(r4)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                throw r3     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
            L_0x0169:
                r8 = 0
                r1.l = r8     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
            L_0x016c:
                java.lang.String r9 = r2.readLine()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r10 = "metrics "
                r11 = 65535(0xffff, float:9.1834E-41)
                java.lang.String r12 = " ="
                if (r9 != 0) goto L_0x017a
                goto L_0x0189
            L_0x017a:
                java.lang.String r13 = "kernings "
                boolean r13 = r9.startsWith(r13)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r13 == 0) goto L_0x0183
                goto L_0x0189
            L_0x0183:
                boolean r13 = r9.startsWith(r10)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r13 == 0) goto L_0x034a
            L_0x0189:
                float r9 = r1.l     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r13 = r1.f4815g     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r9 = r9 + r13
                r1.l = r9     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
            L_0x0190:
                java.lang.String r9 = r2.readLine()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r9 != 0) goto L_0x0197
                goto L_0x019f
            L_0x0197:
                java.lang.String r13 = "kerning "
                boolean r13 = r9.startsWith(r13)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r13 != 0) goto L_0x0307
            L_0x019f:
                if (r9 == 0) goto L_0x0202
                boolean r10 = r9.startsWith(r10)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r10 == 0) goto L_0x0202
                java.util.StringTokenizer r8 = new java.util.StringTokenizer     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r8.<init>(r9, r12)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r9 = r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r9 = java.lang.Float.parseFloat(r9)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r10 = r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r10 = java.lang.Float.parseFloat(r10)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r11 = r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r11 = java.lang.Float.parseFloat(r11)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r12 = r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r12 = java.lang.Float.parseFloat(r12)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r13 = r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r13 = java.lang.Float.parseFloat(r13)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r14 = r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r14 = java.lang.Float.parseFloat(r14)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                java.lang.String r8 = r8.nextToken()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r8 = java.lang.Float.parseFloat(r8)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r21 = r9
                r9 = r8
                r8 = r21
                goto L_0x0209
            L_0x0202:
                r5 = 0
                r9 = 0
                r10 = 0
                r11 = 0
                r12 = 0
                r13 = 0
                r14 = 0
            L_0x0209:
                com.badlogic.gdx.graphics.g2d.c$b r15 = r1.a(r4)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r15 != 0) goto L_0x0229
                com.badlogic.gdx.graphics.g2d.c$b r15 = new com.badlogic.gdx.graphics.g2d.c$b     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r15.<init>()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r15.f4820a = r4     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r6 = 108(0x6c, float:1.51E-43)
                com.badlogic.gdx.graphics.g2d.c$b r6 = r1.a(r6)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r6 != 0) goto L_0x0222
                com.badlogic.gdx.graphics.g2d.c$b r6 = r22.j()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
            L_0x0222:
                int r6 = r6.l     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r15.l = r6     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r1.a(r4, r15)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
            L_0x0229:
                int r4 = r15.f4823d     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r4 != 0) goto L_0x023f
                float r4 = r1.f4816h     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r6 = r15.l     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r6 = (float) r6     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r4 = r4 + r6
                float r6 = r1.f4814f     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r4 = r4 + r6
                int r4 = (int) r4     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r15.f4823d = r4     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r4 = r1.f4816h     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r4 = -r4
                int r4 = (int) r4     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r15.f4829j = r4     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
            L_0x023f:
                int r4 = r15.l     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r4 = (float) r4     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r1.u = r4     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                char[] r4 = r1.x     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r6 = r4.length     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r15 = 0
                r17 = 0
            L_0x024a:
                if (r15 >= r6) goto L_0x025c
                r18 = r6
                char r6 = r4[r15]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                com.badlogic.gdx.graphics.g2d.c$b r17 = r1.a(r6)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r17 == 0) goto L_0x0257
                goto L_0x025c
            L_0x0257:
                int r15 = r15 + 1
                r6 = r18
                goto L_0x024a
            L_0x025c:
                if (r17 != 0) goto L_0x0262
                com.badlogic.gdx.graphics.g2d.c$b r17 = r22.j()     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
            L_0x0262:
                r4 = r17
                int r4 = r4.f4824e     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r4 = (float) r4     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                float r4 = r4 - r3
                r1.v = r4     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                char[] r4 = r1.y     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r6 = r4.length     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r15 = 0
                r16 = 0
            L_0x0270:
                if (r15 >= r6) goto L_0x0282
                r17 = r6
                char r6 = r4[r15]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                com.badlogic.gdx.graphics.g2d.c$b r16 = r1.a(r6)     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r16 == 0) goto L_0x027d
                goto L_0x0282
            L_0x027d:
                int r15 = r15 + 1
                r6 = r17
                goto L_0x0270
            L_0x0282:
                r4 = r16
                if (r4 != 0) goto L_0x02d1
                com.badlogic.gdx.graphics.g2d.c$b[][] r4 = r1.s     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                int r6 = r4.length     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r15 = 0
            L_0x028a:
                if (r15 >= r6) goto L_0x02ce
                r16 = r6
                r6 = r4[r15]     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                if (r6 != 0) goto L_0x0297
                r18 = r2
                r17 = r4
                goto L_0x02c5
            L_0x0297:
                r17 = r4
                int r4 = r6.length     // Catch:{ Exception -> 0x045b, all -> 0x0456 }
                r18 = r2
                r2 = 0
            L_0x029d:
                if (r2 >= r4) goto L_0x02c5
                r19 = r4
                r4 = r6[r2]     // Catch:{ Exception -> 0x0454 }
                if (r4 == 0) goto L_0x02bc
                r20 = r6
                int r6 = r4.f4824e     // Catch:{ Exception -> 0x0454 }
                if (r6 == 0) goto L_0x02be
                int r6 = r4.f4823d     // Catch:{ Exception -> 0x0454 }
                if (r6 != 0) goto L_0x02b0
                goto L_0x02be
            L_0x02b0:
                float r6 = r1.f4818j     // Catch:{ Exception -> 0x0454 }
                int r4 = r4.f4824e     // Catch:{ Exception -> 0x0454 }
                float r4 = (float) r4     // Catch:{ Exception -> 0x0454 }
                float r4 = java.lang.Math.max(r6, r4)     // Catch:{ Exception -> 0x0454 }
                r1.f4818j = r4     // Catch:{ Exception -> 0x0454 }
                goto L_0x02be
            L_0x02bc:
                r20 = r6
            L_0x02be:
                int r2 = r2 + 1
                r4 = r19
                r6 = r20
                goto L_0x029d
            L_0x02c5:
                int r15 = r15 + 1
                r6 = r16
                r4 = r17
                r2 = r18
                goto L_0x028a
            L_0x02ce:
                r18 = r2
                goto L_0x02d8
            L_0x02d1:
                r18 = r2
                int r2 = r4.f4824e     // Catch:{ Exception -> 0x0454 }
                float r2 = (float) r2     // Catch:{ Exception -> 0x0454 }
                r1.f4818j = r2     // Catch:{ Exception -> 0x0454 }
            L_0x02d8:
                float r2 = r1.f4818j     // Catch:{ Exception -> 0x0454 }
                float r2 = r2 - r3
                r1.f4818j = r2     // Catch:{ Exception -> 0x0454 }
                float r2 = r1.f4818j     // Catch:{ Exception -> 0x0454 }
                float r7 = r7 - r2
                r1.f4819k = r7     // Catch:{ Exception -> 0x0454 }
                float r2 = r1.f4817i     // Catch:{ Exception -> 0x0454 }
                float r2 = -r2
                r1.m = r2     // Catch:{ Exception -> 0x0454 }
                if (r24 == 0) goto L_0x02f3
                float r2 = r1.f4819k     // Catch:{ Exception -> 0x0454 }
                float r2 = -r2
                r1.f4819k = r2     // Catch:{ Exception -> 0x0454 }
                float r2 = r1.m     // Catch:{ Exception -> 0x0454 }
                float r2 = -r2
                r1.m = r2     // Catch:{ Exception -> 0x0454 }
            L_0x02f3:
                if (r5 == 0) goto L_0x0303
                r1.f4819k = r8     // Catch:{ Exception -> 0x0454 }
                r1.l = r10     // Catch:{ Exception -> 0x0454 }
                r1.m = r11     // Catch:{ Exception -> 0x0454 }
                r1.f4818j = r12     // Catch:{ Exception -> 0x0454 }
                r1.f4817i = r13     // Catch:{ Exception -> 0x0454 }
                r1.u = r14     // Catch:{ Exception -> 0x0454 }
                r1.v = r9     // Catch:{ Exception -> 0x0454 }
            L_0x0303:
                com.badlogic.gdx.utils.q0.a(r18)
                return
            L_0x0307:
                r18 = r2
                java.util.StringTokenizer r2 = new java.util.StringTokenizer     // Catch:{ Exception -> 0x0454 }
                r2.<init>(r9, r12)     // Catch:{ Exception -> 0x0454 }
                r2.nextToken()     // Catch:{ Exception -> 0x0454 }
                r2.nextToken()     // Catch:{ Exception -> 0x0454 }
                java.lang.String r6 = r2.nextToken()     // Catch:{ Exception -> 0x0454 }
                int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ Exception -> 0x0454 }
                r2.nextToken()     // Catch:{ Exception -> 0x0454 }
                java.lang.String r9 = r2.nextToken()     // Catch:{ Exception -> 0x0454 }
                int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ Exception -> 0x0454 }
                if (r6 < 0) goto L_0x0345
                if (r6 > r11) goto L_0x0345
                if (r9 < 0) goto L_0x0345
                if (r9 <= r11) goto L_0x0330
                goto L_0x0345
            L_0x0330:
                char r6 = (char) r6     // Catch:{ Exception -> 0x0454 }
                com.badlogic.gdx.graphics.g2d.c$b r6 = r1.a(r6)     // Catch:{ Exception -> 0x0454 }
                r2.nextToken()     // Catch:{ Exception -> 0x0454 }
                java.lang.String r2 = r2.nextToken()     // Catch:{ Exception -> 0x0454 }
                int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x0454 }
                if (r6 == 0) goto L_0x0345
                r6.a(r9, r2)     // Catch:{ Exception -> 0x0454 }
            L_0x0345:
                r2 = r18
                r6 = 0
                goto L_0x0190
            L_0x034a:
                r18 = r2
                java.lang.String r2 = "char "
                boolean r2 = r9.startsWith(r2)     // Catch:{ Exception -> 0x0454 }
                if (r2 != 0) goto L_0x0359
            L_0x0354:
                r2 = r18
                r6 = 0
                goto L_0x016c
            L_0x0359:
                com.badlogic.gdx.graphics.g2d.c$b r2 = new com.badlogic.gdx.graphics.g2d.c$b     // Catch:{ Exception -> 0x0454 }
                r2.<init>()     // Catch:{ Exception -> 0x0454 }
                java.util.StringTokenizer r6 = new java.util.StringTokenizer     // Catch:{ Exception -> 0x0454 }
                r6.<init>(r9, r12)     // Catch:{ Exception -> 0x0454 }
                r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                java.lang.String r9 = r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ Exception -> 0x0454 }
                if (r9 > 0) goto L_0x0376
                r1.t = r2     // Catch:{ Exception -> 0x0454 }
                goto L_0x037b
            L_0x0376:
                if (r9 > r11) goto L_0x0354
                r1.a(r9, r2)     // Catch:{ Exception -> 0x0454 }
            L_0x037b:
                r2.f4820a = r9     // Catch:{ Exception -> 0x0454 }
                r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                java.lang.String r9 = r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ Exception -> 0x0454 }
                r2.f4821b = r9     // Catch:{ Exception -> 0x0454 }
                r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                java.lang.String r9 = r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ Exception -> 0x0454 }
                r2.f4822c = r9     // Catch:{ Exception -> 0x0454 }
                r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                java.lang.String r9 = r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ Exception -> 0x0454 }
                r2.f4823d = r9     // Catch:{ Exception -> 0x0454 }
                r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                java.lang.String r9 = r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ Exception -> 0x0454 }
                r2.f4824e = r9     // Catch:{ Exception -> 0x0454 }
                r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                java.lang.String r9 = r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ Exception -> 0x0454 }
                r2.f4829j = r9     // Catch:{ Exception -> 0x0454 }
                r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                if (r24 == 0) goto L_0x03ce
                java.lang.String r9 = r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ Exception -> 0x0454 }
                r2.f4830k = r9     // Catch:{ Exception -> 0x0454 }
                goto L_0x03dc
            L_0x03ce:
                int r9 = r2.f4824e     // Catch:{ Exception -> 0x0454 }
                java.lang.String r10 = r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                int r10 = java.lang.Integer.parseInt(r10)     // Catch:{ Exception -> 0x0454 }
                int r9 = r9 + r10
                int r9 = -r9
                r2.f4830k = r9     // Catch:{ Exception -> 0x0454 }
            L_0x03dc:
                r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                java.lang.String r9 = r6.nextToken()     // Catch:{ Exception -> 0x0454 }
                int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ Exception -> 0x0454 }
                r2.l = r9     // Catch:{ Exception -> 0x0454 }
                boolean r9 = r6.hasMoreTokens()     // Catch:{ Exception -> 0x0454 }
                if (r9 == 0) goto L_0x03f2
                r6.nextToken()     // Catch:{ Exception -> 0x0454 }
            L_0x03f2:
                boolean r9 = r6.hasMoreTokens()     // Catch:{ Exception -> 0x0454 }
                if (r9 == 0) goto L_0x0402
                java.lang.String r6 = r6.nextToken()     // Catch:{ NumberFormatException -> 0x0402 }
                int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ NumberFormatException -> 0x0402 }
                r2.o = r6     // Catch:{ NumberFormatException -> 0x0402 }
            L_0x0402:
                int r6 = r2.f4823d     // Catch:{ Exception -> 0x0454 }
                if (r6 <= 0) goto L_0x0354
                int r6 = r2.f4824e     // Catch:{ Exception -> 0x0454 }
                if (r6 <= 0) goto L_0x0354
                int r2 = r2.f4830k     // Catch:{ Exception -> 0x0454 }
                float r2 = (float) r2     // Catch:{ Exception -> 0x0454 }
                float r2 = r2 + r7
                float r6 = r1.l     // Catch:{ Exception -> 0x0454 }
                float r2 = java.lang.Math.min(r2, r6)     // Catch:{ Exception -> 0x0454 }
                r1.l = r2     // Catch:{ Exception -> 0x0454 }
                goto L_0x0354
            L_0x0418:
                r18 = r2
                com.badlogic.gdx.utils.o r2 = new com.badlogic.gdx.utils.o     // Catch:{ Exception -> 0x0454 }
                java.lang.String r3 = "Missing: base"
                r2.<init>(r3)     // Catch:{ Exception -> 0x0454 }
                throw r2     // Catch:{ Exception -> 0x0454 }
            L_0x0422:
                r18 = r2
                com.badlogic.gdx.utils.o r2 = new com.badlogic.gdx.utils.o     // Catch:{ Exception -> 0x0454 }
                java.lang.String r3 = "Missing: lineHeight"
                r2.<init>(r3)     // Catch:{ Exception -> 0x0454 }
                throw r2     // Catch:{ Exception -> 0x0454 }
            L_0x042c:
                r18 = r2
                com.badlogic.gdx.utils.o r2 = new com.badlogic.gdx.utils.o     // Catch:{ Exception -> 0x0454 }
                java.lang.String r3 = "Invalid common header."
                r2.<init>(r3)     // Catch:{ Exception -> 0x0454 }
                throw r2     // Catch:{ Exception -> 0x0454 }
            L_0x0436:
                r18 = r2
                com.badlogic.gdx.utils.o r2 = new com.badlogic.gdx.utils.o     // Catch:{ Exception -> 0x0454 }
                java.lang.String r3 = "Missing common header."
                r2.<init>(r3)     // Catch:{ Exception -> 0x0454 }
                throw r2     // Catch:{ Exception -> 0x0454 }
            L_0x0440:
                r18 = r2
                com.badlogic.gdx.utils.o r2 = new com.badlogic.gdx.utils.o     // Catch:{ Exception -> 0x0454 }
                java.lang.String r3 = "Invalid padding."
                r2.<init>(r3)     // Catch:{ Exception -> 0x0454 }
                throw r2     // Catch:{ Exception -> 0x0454 }
            L_0x044a:
                r18 = r2
                com.badlogic.gdx.utils.o r2 = new com.badlogic.gdx.utils.o     // Catch:{ Exception -> 0x0454 }
                java.lang.String r3 = "File is empty."
                r2.<init>(r3)     // Catch:{ Exception -> 0x0454 }
                throw r2     // Catch:{ Exception -> 0x0454 }
            L_0x0454:
                r0 = move-exception
                goto L_0x045e
            L_0x0456:
                r0 = move-exception
                r18 = r2
            L_0x0459:
                r2 = r0
                goto L_0x047a
            L_0x045b:
                r0 = move-exception
                r18 = r2
            L_0x045e:
                r2 = r0
                com.badlogic.gdx.utils.o r3 = new com.badlogic.gdx.utils.o     // Catch:{ all -> 0x0478 }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0478 }
                r4.<init>()     // Catch:{ all -> 0x0478 }
                java.lang.String r5 = "Error loading font file: "
                r4.append(r5)     // Catch:{ all -> 0x0478 }
                r5 = r23
                r4.append(r5)     // Catch:{ all -> 0x0478 }
                java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0478 }
                r3.<init>(r4, r2)     // Catch:{ all -> 0x0478 }
                throw r3     // Catch:{ all -> 0x0478 }
            L_0x0478:
                r0 = move-exception
                goto L_0x0459
            L_0x047a:
                com.badlogic.gdx.utils.q0.a(r18)
                throw r2
            L_0x047e:
                java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
                java.lang.String r3 = "Already loaded."
                r2.<init>(r3)
                goto L_0x0487
            L_0x0486:
                throw r2
            L_0x0487:
                goto L_0x0486
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.c.a.a(e.d.b.s.a, boolean):void");
        }

        public boolean b(char c2) {
            if (this.t == null && a(c2) == null) {
                return false;
            }
            return true;
        }

        public boolean c(char c2) {
            char[] cArr = this.w;
            if (cArr == null) {
                return false;
            }
            for (char c3 : cArr) {
                if (c2 == c3) {
                    return true;
                }
            }
            return false;
        }

        public boolean d(char c2) {
            return c2 == 9 || c2 == 10 || c2 == 13 || c2 == ' ';
        }

        public b j() {
            for (b[] bVarArr : this.s) {
                if (bVarArr != null) {
                    for (b bVar : bVarArr) {
                        if (bVar != null && bVar.f4824e != 0 && bVar.f4823d != 0) {
                            return bVar;
                        }
                    }
                    continue;
                }
            }
            throw new o("No glyphs found.");
        }

        public String[] k() {
            return this.f4810b;
        }

        public String toString() {
            String str = this.f4809a;
            return str != null ? str : super.toString();
        }

        public a(e.d.b.s.a aVar, boolean z) {
            this.f4811c = aVar;
            this.f4812d = z;
            a(aVar, z);
        }

        public void a(b bVar, r rVar) {
            float f2;
            float f3;
            b bVar2 = bVar;
            r rVar2 = rVar;
            n e2 = rVar.e();
            float r2 = 1.0f / ((float) e2.r());
            float p2 = 1.0f / ((float) e2.p());
            float f4 = rVar2.f5061b;
            float f5 = rVar2.f5062c;
            float b2 = (float) rVar.b();
            float a2 = (float) rVar.a();
            if (rVar2 instanceof q.b) {
                q.b bVar3 = (q.b) rVar2;
                f2 = bVar3.f5037j;
                f3 = ((float) (bVar3.o - bVar3.m)) - bVar3.f5038k;
            } else {
                f3 = Animation.CurveTimeline.LINEAR;
                f2 = Animation.CurveTimeline.LINEAR;
            }
            int i2 = bVar2.f4821b;
            float f6 = (float) i2;
            int i3 = bVar2.f4823d;
            float f7 = (float) (i2 + i3);
            int i4 = bVar2.f4822c;
            float f8 = (float) i4;
            float f9 = (float) (i4 + bVar2.f4824e);
            if (f2 > Animation.CurveTimeline.LINEAR) {
                f6 -= f2;
                if (f6 < Animation.CurveTimeline.LINEAR) {
                    bVar2.f4823d = (int) (((float) i3) + f6);
                    bVar2.f4829j = (int) (((float) bVar2.f4829j) - f6);
                    f6 = Animation.CurveTimeline.LINEAR;
                }
                float f10 = f7 - f2;
                if (f10 > b2) {
                    bVar2.f4823d = (int) (((float) bVar2.f4823d) - (f10 - b2));
                } else {
                    b2 = f10;
                }
            } else {
                b2 = f7;
            }
            if (f3 > Animation.CurveTimeline.LINEAR) {
                float f11 = f8 - f3;
                if (f11 < Animation.CurveTimeline.LINEAR) {
                    bVar2.f4824e = (int) (((float) bVar2.f4824e) + f11);
                    if (bVar2.f4824e < 0) {
                        bVar2.f4824e = 0;
                    }
                    f8 = Animation.CurveTimeline.LINEAR;
                } else {
                    f8 = f11;
                }
                float f12 = f9 - f3;
                if (f12 > a2) {
                    float f13 = f12 - a2;
                    bVar2.f4824e = (int) (((float) bVar2.f4824e) - f13);
                    bVar2.f4830k = (int) (((float) bVar2.f4830k) + f13);
                    f9 = a2;
                } else {
                    f9 = f12;
                }
            }
            bVar2.f4825f = (f6 * r2) + f4;
            bVar2.f4827h = f4 + (b2 * r2);
            if (this.f4812d) {
                bVar2.f4826g = (f8 * p2) + f5;
                bVar2.f4828i = f5 + (f9 * p2);
                return;
            }
            bVar2.f4828i = (f8 * p2) + f5;
            bVar2.f4826g = f5 + (f9 * p2);
        }

        public void a(int i2, b bVar) {
            b[][] bVarArr = this.s;
            int i3 = i2 / AdRequest.MAX_CONTENT_URL_LENGTH;
            b[] bVarArr2 = bVarArr[i3];
            if (bVarArr2 == null) {
                bVarArr2 = new b[AdRequest.MAX_CONTENT_URL_LENGTH];
                bVarArr[i3] = bVarArr2;
            }
            bVarArr2[i2 & 511] = bVar;
        }

        public b a(char c2) {
            b[] bVarArr = this.s[c2 / 512];
            if (bVarArr != null) {
                return bVarArr[c2 & 511];
            }
            return null;
        }

        public void a(e.a aVar, CharSequence charSequence, int i2, int i3, b bVar) {
            boolean z = this.q;
            float f2 = this.o;
            b bVar2 = this.t;
            com.badlogic.gdx.utils.a<b> aVar2 = aVar.f4846a;
            m mVar = aVar.f4847b;
            int i4 = i3 - i2;
            aVar2.c(i4);
            mVar.a(i4 + 1);
            while (i2 < i3) {
                int i5 = i2 + 1;
                char charAt = charSequence.charAt(i2);
                if (charAt != 13) {
                    b a2 = a(charAt);
                    if (a2 == null) {
                        if (bVar2 != null) {
                            a2 = bVar2;
                        }
                    }
                    aVar2.add(a2);
                    if (bVar == null) {
                        mVar.a(a2.n ? Animation.CurveTimeline.LINEAR : (((float) (-a2.f4829j)) * f2) - this.f4816h);
                    } else {
                        mVar.a(((float) (bVar.l + bVar.a(charAt))) * f2);
                    }
                    if (z && charAt == '[' && i5 < i3 && charSequence.charAt(i5) == '[') {
                        i5++;
                    }
                    i2 = i5;
                    bVar = a2;
                }
                i2 = i5;
            }
            if (bVar != null) {
                mVar.a(bVar.n ? ((float) bVar.l) * f2 : (((float) (bVar.f4823d + bVar.f4829j)) * f2) - this.f4814f);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x002d, code lost:
            return r4 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0032, code lost:
            if (d(r0) == false) goto L_0x0018;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0036, code lost:
            return r4 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
            return 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:4:0x0016, code lost:
            if (c(r0) != false) goto L_0x0018;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:5:0x0018, code lost:
            r4 = r4 - 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:6:0x001a, code lost:
            if (r4 <= 0) goto L_0x0037;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x001c, code lost:
            r0 = (char) r3.get(r4).f4820a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0029, code lost:
            if (c(r0) == false) goto L_0x002e;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int a(com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.c.b> r3, int r4) {
            /*
                r2 = this;
                int r4 = r4 + -1
                java.lang.Object r0 = r3.get(r4)
                com.badlogic.gdx.graphics.g2d.c$b r0 = (com.badlogic.gdx.graphics.g2d.c.b) r0
                int r0 = r0.f4820a
                char r0 = (char) r0
                boolean r1 = r2.d(r0)
                if (r1 == 0) goto L_0x0012
                return r4
            L_0x0012:
                boolean r0 = r2.c(r0)
                if (r0 == 0) goto L_0x001a
            L_0x0018:
                int r4 = r4 + -1
            L_0x001a:
                if (r4 <= 0) goto L_0x0037
                java.lang.Object r0 = r3.get(r4)
                com.badlogic.gdx.graphics.g2d.c$b r0 = (com.badlogic.gdx.graphics.g2d.c.b) r0
                int r0 = r0.f4820a
                char r0 = (char) r0
                boolean r1 = r2.c(r0)
                if (r1 == 0) goto L_0x002e
                int r4 = r4 + 1
                return r4
            L_0x002e:
                boolean r0 = r2.d(r0)
                if (r0 == 0) goto L_0x0018
                int r4 = r4 + 1
                return r4
            L_0x0037:
                r3 = 0
                return r3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.c.a.a(com.badlogic.gdx.utils.a, int):int");
        }

        public String a(int i2) {
            return this.f4810b[i2];
        }

        public void a(float f2, float f3) {
            if (f2 == Animation.CurveTimeline.LINEAR) {
                throw new IllegalArgumentException("scaleX cannot be 0.");
            } else if (f3 != Animation.CurveTimeline.LINEAR) {
                float f4 = f2 / this.o;
                float f5 = f3 / this.p;
                this.f4817i *= f5;
                this.u *= f4;
                this.v *= f5;
                this.f4818j *= f5;
                this.f4819k *= f5;
                this.l *= f5;
                this.m *= f5;
                this.f4816h *= f4;
                this.f4814f *= f4;
                this.f4813e *= f5;
                this.f4815g *= f5;
                this.o = f2;
                this.p = f3;
            } else {
                throw new IllegalArgumentException("scaleY cannot be 0.");
            }
        }

        public void a(float f2) {
            a(f2, f2);
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public c(com.badlogic.gdx.graphics.g2d.c.a r3, com.badlogic.gdx.graphics.g2d.r r4, boolean r5) {
        /*
            r2 = this;
            if (r4 == 0) goto L_0x000d
            r0 = 1
            com.badlogic.gdx.graphics.g2d.r[] r0 = new com.badlogic.gdx.graphics.g2d.r[r0]
            r1 = 0
            r0[r1] = r4
            com.badlogic.gdx.utils.a r4 = com.badlogic.gdx.utils.a.b(r0)
            goto L_0x000e
        L_0x000d:
            r4 = 0
        L_0x000e:
            r2.<init>(r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.c.<init>(com.badlogic.gdx.graphics.g2d.c$a, com.badlogic.gdx.graphics.g2d.r, boolean):void");
    }

    public e a(b bVar, CharSequence charSequence, float f2, float f3, int i2, int i3, float f4, int i4, boolean z, String str) {
        this.f4805c.a();
        e a2 = this.f4805c.a(charSequence, f2, f3, i2, i3, f4, i4, z, str);
        this.f4805c.a(bVar);
        return a2;
    }

    public c(a aVar, com.badlogic.gdx.utils.a<r> aVar2, boolean z) {
        e.d.b.s.a aVar3;
        this.f4806d = aVar.f4812d;
        this.f4803a = aVar;
        this.f4807e = z;
        if (aVar2 == null || aVar2.f5374b == 0) {
            String[] strArr = aVar.f4810b;
            if (strArr != null) {
                int length = strArr.length;
                this.f4804b = new com.badlogic.gdx.utils.a<>(length);
                for (int i2 = 0; i2 < length; i2++) {
                    e.d.b.s.a aVar4 = aVar.f4811c;
                    if (aVar4 == null) {
                        aVar3 = g.f6804e.a(aVar.f4810b[i2]);
                    } else {
                        aVar3 = g.f6804e.a(aVar.f4810b[i2], aVar4.o());
                    }
                    this.f4804b.add(new r(new n(aVar3, false)));
                }
                this.f4808f = true;
            } else {
                throw new IllegalArgumentException("If no regions are specified, the font data must have an images path.");
            }
        } else {
            this.f4804b = aVar2;
            this.f4808f = false;
        }
        this.f4805c = r();
        a(aVar);
    }
}
