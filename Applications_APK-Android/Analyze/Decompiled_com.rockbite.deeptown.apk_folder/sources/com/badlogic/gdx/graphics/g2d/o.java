package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.math.h;
import com.badlogic.gdx.math.n;
import com.badlogic.gdx.utils.z;
import com.esotericsoftware.spine.Animation;
import e.d.b.t.b;

/* compiled from: Sprite */
public class o extends r {

    /* renamed from: h  reason: collision with root package name */
    final float[] f5016h;

    /* renamed from: i  reason: collision with root package name */
    private final b f5017i;

    /* renamed from: j  reason: collision with root package name */
    private float f5018j;

    /* renamed from: k  reason: collision with root package name */
    private float f5019k;
    float l;
    float m;
    private float n;
    private float o;
    private float p;
    private float q;
    private float r;
    private boolean s;
    private n t;

    public o() {
        this.f5016h = new float[20];
        this.f5017i = new b(1.0f, 1.0f, 1.0f, 1.0f);
        this.q = 1.0f;
        this.r = 1.0f;
        this.s = true;
        c(1.0f, 1.0f, 1.0f, 1.0f);
    }

    public void a(o oVar) {
        if (oVar != null) {
            System.arraycopy(oVar.f5016h, 0, this.f5016h, 0, 20);
            this.f5060a = oVar.f5060a;
            this.f5061b = oVar.f5061b;
            this.f5062c = oVar.f5062c;
            this.f5063d = oVar.f5063d;
            this.f5064e = oVar.f5064e;
            this.f5018j = oVar.f5018j;
            this.f5019k = oVar.f5019k;
            this.l = oVar.l;
            this.m = oVar.m;
            this.f5065f = oVar.f5065f;
            this.f5066g = oVar.f5066g;
            this.n = oVar.n;
            this.o = oVar.o;
            this.p = oVar.p;
            this.q = oVar.q;
            this.r = oVar.r;
            this.f5017i.b(oVar.f5017i);
            this.s = oVar.s;
            return;
        }
        throw new IllegalArgumentException("sprite cannot be null.");
    }

    public void b(float f2, float f3, float f4, float f5) {
        this.f5018j = f2;
        this.f5019k = f3;
        this.l = f4;
        this.m = f5;
        if (!this.s) {
            float f6 = f4 + f2;
            float f7 = f5 + f3;
            float[] fArr = this.f5016h;
            fArr[0] = f2;
            fArr[1] = f3;
            fArr[5] = f2;
            fArr[6] = f7;
            fArr[10] = f6;
            fArr[11] = f7;
            fArr[15] = f6;
            fArr[16] = f3;
            if (this.p != Animation.CurveTimeline.LINEAR || this.q != 1.0f || this.r != 1.0f) {
                this.s = true;
            }
        }
    }

    public void c(float f2, float f3, float f4, float f5) {
        this.f5017i.b(f2, f3, f4, f5);
        float b2 = this.f5017i.b();
        float[] fArr = this.f5016h;
        fArr[2] = b2;
        fArr[7] = b2;
        fArr[12] = b2;
        fArr[17] = b2;
    }

    public void d(float f2, float f3) {
        this.l = f2;
        this.m = f3;
        if (!this.s) {
            float f4 = this.f5018j;
            float f5 = f2 + f4;
            float f6 = this.f5019k;
            float f7 = f3 + f6;
            float[] fArr = this.f5016h;
            fArr[0] = f4;
            fArr[1] = f6;
            fArr[5] = f4;
            fArr[6] = f7;
            fArr[10] = f5;
            fArr[11] = f7;
            fArr[15] = f5;
            fArr[16] = f6;
            if (this.p != Animation.CurveTimeline.LINEAR || this.q != 1.0f || this.r != 1.0f) {
                this.s = true;
            }
        }
    }

    public void e(float f2, float f3) {
        this.f5018j += f2;
        this.f5019k += f3;
        if (!this.s) {
            float[] fArr = this.f5016h;
            fArr[0] = fArr[0] + f2;
            fArr[1] = fArr[1] + f3;
            fArr[5] = fArr[5] + f2;
            fArr[6] = fArr[6] + f3;
            fArr[10] = fArr[10] + f2;
            fArr[11] = fArr[11] + f3;
            fArr[15] = fArr[15] + f2;
            fArr[16] = fArr[16] + f3;
        }
    }

    public void f(float f2) {
        this.p = f2;
        this.s = true;
    }

    public void g(float f2) {
        this.q = f2;
        this.r = f2;
        this.s = true;
    }

    public void h(float f2) {
        j(f2 - this.f5018j);
    }

    public void i(float f2) {
        k(f2 - this.f5019k);
    }

    public void j(float f2) {
        this.f5018j += f2;
        if (!this.s) {
            float[] fArr = this.f5016h;
            fArr[0] = fArr[0] + f2;
            fArr[5] = fArr[5] + f2;
            fArr[10] = fArr[10] + f2;
            fArr[15] = fArr[15] + f2;
        }
    }

    public void k(float f2) {
        this.f5019k += f2;
        if (!this.s) {
            float[] fArr = this.f5016h;
            fArr[1] = fArr[1] + f2;
            fArr[6] = fArr[6] + f2;
            fArr[11] = fArr[11] + f2;
            fArr[16] = fArr[16] + f2;
        }
    }

    public n l() {
        float[] q2 = q();
        float f2 = q2[0];
        float f3 = q2[1];
        float f4 = q2[0];
        float f5 = q2[1];
        if (f2 > q2[5]) {
            f2 = q2[5];
        }
        if (f2 > q2[10]) {
            f2 = q2[10];
        }
        if (f2 > q2[15]) {
            f2 = q2[15];
        }
        if (f4 < q2[5]) {
            f4 = q2[5];
        }
        if (f4 < q2[10]) {
            f4 = q2[10];
        }
        if (f4 < q2[15]) {
            f4 = q2[15];
        }
        if (f3 > q2[6]) {
            f3 = q2[6];
        }
        if (f3 > q2[11]) {
            f3 = q2[11];
        }
        if (f3 > q2[16]) {
            f3 = q2[16];
        }
        if (f5 < q2[6]) {
            f5 = q2[6];
        }
        if (f5 < q2[11]) {
            f5 = q2[11];
        }
        if (f5 < q2[16]) {
            f5 = q2[16];
        }
        if (this.t == null) {
            this.t = new n();
        }
        n nVar = this.t;
        nVar.f5290a = f2;
        nVar.f5291b = f3;
        nVar.f5292c = f4 - f2;
        nVar.f5293d = f5 - f3;
        return nVar;
    }

    public b m() {
        int b2 = z.b(this.f5016h[2]);
        b bVar = this.f5017i;
        bVar.f6934a = ((float) (b2 & 255)) / 255.0f;
        bVar.f6935b = ((float) ((b2 >>> 8) & 255)) / 255.0f;
        bVar.f6936c = ((float) ((b2 >>> 16) & 255)) / 255.0f;
        bVar.f6937d = ((float) ((b2 >>> 24) & 255)) / 255.0f;
        return bVar;
    }

    public float n() {
        return this.m;
    }

    public float o() {
        return this.n;
    }

    public float p() {
        return this.o;
    }

    public float[] q() {
        if (this.s) {
            this.s = false;
            float[] fArr = this.f5016h;
            float f2 = -this.n;
            float f3 = -this.o;
            float f4 = this.l + f2;
            float f5 = this.m + f3;
            float f6 = this.f5018j - f2;
            float f7 = this.f5019k - f3;
            if (!(this.q == 1.0f && this.r == 1.0f)) {
                float f8 = this.q;
                f2 *= f8;
                float f9 = this.r;
                f3 *= f9;
                f4 *= f8;
                f5 *= f9;
            }
            float f10 = this.p;
            if (f10 != Animation.CurveTimeline.LINEAR) {
                float b2 = h.b(f10);
                float h2 = h.h(this.p);
                float f11 = f2 * b2;
                float f12 = f2 * h2;
                float f13 = f3 * b2;
                float f14 = f4 * b2;
                float f15 = b2 * f5;
                float f16 = f5 * h2;
                float f17 = (f11 - (f3 * h2)) + f6;
                float f18 = f13 + f12 + f7;
                fArr[0] = f17;
                fArr[1] = f18;
                float f19 = (f11 - f16) + f6;
                float f20 = f12 + f15 + f7;
                fArr[5] = f19;
                fArr[6] = f20;
                float f21 = (f14 - f16) + f6;
                float f22 = f15 + (f4 * h2) + f7;
                fArr[10] = f21;
                fArr[11] = f22;
                fArr[15] = f17 + (f21 - f19);
                fArr[16] = f22 - (f20 - f18);
            } else {
                float f23 = f2 + f6;
                float f24 = f3 + f7;
                float f25 = f4 + f6;
                float f26 = f5 + f7;
                fArr[0] = f23;
                fArr[1] = f24;
                fArr[5] = f23;
                fArr[6] = f26;
                fArr[10] = f25;
                fArr[11] = f26;
                fArr[15] = f25;
                fArr[16] = f24;
            }
        }
        return this.f5016h;
    }

    public float r() {
        return this.l;
    }

    public float s() {
        return this.f5018j;
    }

    public float t() {
        return this.f5019k;
    }

    public void u() {
        this.n = this.l / 2.0f;
        this.o = this.m / 2.0f;
        this.s = true;
    }

    public o(e.d.b.t.n nVar) {
        this(nVar, 0, 0, nVar.r(), nVar.p());
    }

    public o(e.d.b.t.n nVar, int i2, int i3, int i4, int i5) {
        this.f5016h = new float[20];
        this.f5017i = new b(1.0f, 1.0f, 1.0f, 1.0f);
        this.q = 1.0f;
        this.r = 1.0f;
        this.s = true;
        if (nVar != null) {
            this.f5060a = nVar;
            a(i2, i3, i4, i5);
            c(1.0f, 1.0f, 1.0f, 1.0f);
            d((float) Math.abs(i4), (float) Math.abs(i5));
            a(this.l / 2.0f, this.m / 2.0f);
            return;
        }
        throw new IllegalArgumentException("texture cannot be null.");
    }

    public void c(float f2, float f3) {
        this.q = f2;
        this.r = f3;
        this.s = true;
    }

    public void c(float f2) {
        super.c(f2);
        float[] fArr = this.f5016h;
        fArr[9] = f2;
        fArr[14] = f2;
    }

    public void e(float f2) {
        b.a(this.f5017i, f2);
        float[] fArr = this.f5016h;
        fArr[2] = f2;
        fArr[7] = f2;
        fArr[12] = f2;
        fArr[17] = f2;
    }

    public void b(float f2, float f3) {
        e(f2 - this.f5018j, f3 - this.f5019k);
    }

    public void d(float f2) {
        super.d(f2);
        float[] fArr = this.f5016h;
        fArr[4] = f2;
        fArr[19] = f2;
    }

    public void b(float f2) {
        super.b(f2);
        float[] fArr = this.f5016h;
        fArr[13] = f2;
        fArr[18] = f2;
    }

    public o(r rVar) {
        this.f5016h = new float[20];
        this.f5017i = new b(1.0f, 1.0f, 1.0f, 1.0f);
        this.q = 1.0f;
        this.r = 1.0f;
        this.s = true;
        a(rVar);
        c(1.0f, 1.0f, 1.0f, 1.0f);
        d((float) rVar.b(), (float) rVar.a());
        a(this.l / 2.0f, this.m / 2.0f);
    }

    public void b(boolean z, boolean z2) {
        boolean z3 = true;
        boolean z4 = j() != z;
        if (k() == z2) {
            z3 = false;
        }
        a(z4, z3);
    }

    public void a(b bVar) {
        this.f5017i.b(bVar);
        float b2 = bVar.b();
        float[] fArr = this.f5016h;
        fArr[2] = b2;
        fArr[7] = b2;
        fArr[12] = b2;
        fArr[17] = b2;
    }

    public o(o oVar) {
        this.f5016h = new float[20];
        this.f5017i = new b(1.0f, 1.0f, 1.0f, 1.0f);
        this.q = 1.0f;
        this.r = 1.0f;
        this.s = true;
        a(oVar);
    }

    public void a(float f2, float f3) {
        this.n = f2;
        this.o = f3;
        this.s = true;
    }

    public void a(boolean z) {
        float[] fArr = this.f5016h;
        if (z) {
            float f2 = fArr[4];
            fArr[4] = fArr[19];
            fArr[19] = fArr[14];
            fArr[14] = fArr[9];
            fArr[9] = f2;
            float f3 = fArr[3];
            fArr[3] = fArr[18];
            fArr[18] = fArr[13];
            fArr[13] = fArr[8];
            fArr[8] = f3;
            return;
        }
        float f4 = fArr[4];
        fArr[4] = fArr[9];
        fArr[9] = fArr[14];
        fArr[14] = fArr[19];
        fArr[19] = f4;
        float f5 = fArr[3];
        fArr[3] = fArr[8];
        fArr[8] = fArr[13];
        fArr[13] = fArr[18];
        fArr[18] = f5;
    }

    public void a(b bVar) {
        bVar.draw(this.f5060a, q(), 0, 20);
    }

    public void a(float f2, float f3, float f4, float f5) {
        super.a(f2, f3, f4, f5);
        float[] fArr = this.f5016h;
        fArr[3] = f2;
        fArr[4] = f5;
        fArr[8] = f2;
        fArr[9] = f3;
        fArr[13] = f4;
        fArr[14] = f3;
        fArr[18] = f4;
        fArr[19] = f5;
    }

    public void a(float f2) {
        super.a(f2);
        float[] fArr = this.f5016h;
        fArr[3] = f2;
        fArr[8] = f2;
    }

    public void a(boolean z, boolean z2) {
        super.a(z, z2);
        float[] fArr = this.f5016h;
        if (z) {
            float f2 = fArr[3];
            fArr[3] = fArr[13];
            fArr[13] = f2;
            float f3 = fArr[8];
            fArr[8] = fArr[18];
            fArr[18] = f3;
        }
        if (z2) {
            float f4 = fArr[4];
            fArr[4] = fArr[14];
            fArr[14] = f4;
            float f5 = fArr[9];
            fArr[9] = fArr[19];
            fArr[19] = f5;
        }
    }
}
