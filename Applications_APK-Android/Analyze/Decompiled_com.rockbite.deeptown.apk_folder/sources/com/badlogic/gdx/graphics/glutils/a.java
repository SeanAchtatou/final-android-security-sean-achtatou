package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.graphics.glutils.ETC1;
import com.badlogic.gdx.utils.o;
import e.d.b.g;
import e.d.b.t.l;
import e.d.b.t.q;

/* compiled from: ETC1TextureData */
public class a implements q {

    /* renamed from: a  reason: collision with root package name */
    e.d.b.s.a f5073a;

    /* renamed from: b  reason: collision with root package name */
    ETC1.a f5074b;

    /* renamed from: c  reason: collision with root package name */
    boolean f5075c;

    /* renamed from: d  reason: collision with root package name */
    int f5076d = 0;

    /* renamed from: e  reason: collision with root package name */
    int f5077e = 0;

    /* renamed from: f  reason: collision with root package name */
    boolean f5078f = false;

    public a(e.d.b.s.a aVar, boolean z) {
        this.f5073a = aVar;
        this.f5075c = z;
    }

    public void a(int i2) {
        if (this.f5078f) {
            if (!g.f6801b.a("GL_OES_compressed_ETC1_RGB8_texture")) {
                l a2 = ETC1.a(this.f5074b, l.c.RGB565);
                g.f6806g.b(i2, 0, a2.m(), a2.q(), a2.o(), 0, a2.l(), a2.n(), a2.p());
                if (this.f5075c) {
                    q.a(i2, a2, a2.q(), a2.o());
                }
                a2.dispose();
                this.f5075c = false;
            } else {
                e.d.b.t.g gVar = g.f6806g;
                int i3 = ETC1.f5068b;
                int i4 = this.f5076d;
                int i5 = this.f5077e;
                int capacity = this.f5074b.f5071c.capacity();
                ETC1.a aVar = this.f5074b;
                gVar.a(i2, 0, i3, i4, i5, 0, capacity - aVar.f5072d, aVar.f5071c);
                if (e()) {
                    g.f6807h.n(3553);
                }
            }
            this.f5074b.dispose();
            this.f5074b = null;
            this.f5078f = false;
            return;
        }
        throw new o("Call prepare() before calling consumeCompressedData()");
    }

    public boolean a() {
        return true;
    }

    public void b() {
        if (this.f5078f) {
            throw new o("Already prepared");
        } else if (this.f5073a == null && this.f5074b == null) {
            throw new o("Can only load once from ETC1Data");
        } else {
            e.d.b.s.a aVar = this.f5073a;
            if (aVar != null) {
                this.f5074b = new ETC1.a(aVar);
            }
            ETC1.a aVar2 = this.f5074b;
            this.f5076d = aVar2.f5069a;
            this.f5077e = aVar2.f5070b;
            this.f5078f = true;
        }
    }

    public boolean c() {
        return this.f5078f;
    }

    public l d() {
        throw new o("This TextureData implementation does not return a Pixmap");
    }

    public boolean e() {
        return this.f5075c;
    }

    public boolean f() {
        throw new o("This TextureData implementation does not return a Pixmap");
    }

    public l.c getFormat() {
        return l.c.RGB565;
    }

    public int getHeight() {
        return this.f5077e;
    }

    public q.b getType() {
        return q.b.Custom;
    }

    public int getWidth() {
        return this.f5076d;
    }
}
