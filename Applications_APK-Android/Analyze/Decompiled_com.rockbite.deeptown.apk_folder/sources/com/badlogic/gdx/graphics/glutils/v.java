package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.o;
import e.d.b.g;
import e.d.b.t.r;
import e.d.b.t.s;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

/* compiled from: VertexBufferObject */
public class v implements y {

    /* renamed from: a  reason: collision with root package name */
    private s f5205a;

    /* renamed from: b  reason: collision with root package name */
    private FloatBuffer f5206b;

    /* renamed from: c  reason: collision with root package name */
    private ByteBuffer f5207c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f5208d;

    /* renamed from: e  reason: collision with root package name */
    private int f5209e = g.f6807h.a();

    /* renamed from: f  reason: collision with root package name */
    private int f5210f;

    /* renamed from: g  reason: collision with root package name */
    boolean f5211g = false;

    /* renamed from: h  reason: collision with root package name */
    boolean f5212h = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.glutils.v.a(java.nio.Buffer, boolean, e.d.b.t.s):void
     arg types: [java.nio.ByteBuffer, int, e.d.b.t.s]
     candidates:
      com.badlogic.gdx.graphics.glutils.v.a(float[], int, int):void
      com.badlogic.gdx.graphics.glutils.y.a(float[], int, int):void
      com.badlogic.gdx.graphics.glutils.v.a(java.nio.Buffer, boolean, e.d.b.t.s):void */
    public v(boolean z, int i2, s sVar) {
        ByteBuffer d2 = BufferUtils.d(sVar.f7013b * i2);
        d2.limit(0);
        a((Buffer) d2, true, sVar);
        a(z ? 35044 : 35048);
    }

    private void e() {
        if (this.f5212h) {
            g.f6807h.a(34962, this.f5207c.limit(), this.f5207c, this.f5210f);
            this.f5211g = false;
        }
    }

    public FloatBuffer a() {
        this.f5211g = true;
        return this.f5206b;
    }

    public void b(s sVar, int[] iArr) {
        e.d.b.t.g gVar = g.f6807h;
        int size = this.f5205a.size();
        if (iArr == null) {
            for (int i2 = 0; i2 < size; i2++) {
                sVar.a(this.f5205a.get(i2).f7009f);
            }
        } else {
            for (int i3 = 0; i3 < size; i3++) {
                int i4 = iArr[i3];
                if (i4 >= 0) {
                    sVar.a(i4);
                }
            }
        }
        gVar.e(34962, 0);
        this.f5212h = false;
    }

    public int c() {
        return (this.f5206b.limit() * 4) / this.f5205a.f7013b;
    }

    public s d() {
        return this.f5205a;
    }

    public void dispose() {
        e.d.b.t.g gVar = g.f6807h;
        gVar.e(34962, 0);
        gVar.e(this.f5209e);
        this.f5209e = 0;
        if (this.f5208d) {
            BufferUtils.a(this.f5207c);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Buffer buffer, boolean z, s sVar) {
        ByteBuffer byteBuffer;
        if (!this.f5212h) {
            if (this.f5208d && (byteBuffer = this.f5207c) != null) {
                BufferUtils.a(byteBuffer);
            }
            this.f5205a = sVar;
            if (buffer instanceof ByteBuffer) {
                this.f5207c = (ByteBuffer) buffer;
                this.f5208d = z;
                int limit = this.f5207c.limit();
                ByteBuffer byteBuffer2 = this.f5207c;
                byteBuffer2.limit(byteBuffer2.capacity());
                this.f5206b = this.f5207c.asFloatBuffer();
                this.f5207c.limit(limit);
                this.f5206b.limit(limit / 4);
                return;
            }
            throw new o("Only ByteBuffer is currently supported");
        }
        throw new o("Cannot change attributes while VBO is bound");
    }

    public void b() {
        this.f5209e = g.f6807h.a();
        this.f5211g = true;
    }

    public void a(float[] fArr, int i2, int i3) {
        this.f5211g = true;
        BufferUtils.a(fArr, this.f5207c, i3, i2);
        this.f5206b.position(0);
        this.f5206b.limit(i3);
        e();
    }

    /* access modifiers changed from: protected */
    public void a(int i2) {
        if (!this.f5212h) {
            this.f5210f = i2;
            return;
        }
        throw new o("Cannot change usage while VBO is bound");
    }

    public void a(s sVar, int[] iArr) {
        e.d.b.t.g gVar = g.f6807h;
        gVar.e(34962, this.f5209e);
        int i2 = 0;
        if (this.f5211g) {
            this.f5207c.limit(this.f5206b.limit() * 4);
            gVar.a(34962, this.f5207c.limit(), this.f5207c, this.f5210f);
            this.f5211g = false;
        }
        int size = this.f5205a.size();
        if (iArr == null) {
            while (i2 < size) {
                r rVar = this.f5205a.get(i2);
                int b2 = sVar.b(rVar.f7009f);
                if (b2 >= 0) {
                    sVar.b(b2);
                    sVar.a(b2, rVar.f7005b, rVar.f7007d, rVar.f7006c, this.f5205a.f7013b, rVar.f7008e);
                }
                i2++;
            }
        } else {
            while (i2 < size) {
                r rVar2 = this.f5205a.get(i2);
                int i3 = iArr[i2];
                if (i3 >= 0) {
                    sVar.b(i3);
                    sVar.a(i3, rVar2.f7005b, rVar2.f7007d, rVar2.f7006c, this.f5205a.f7013b, rVar2.f7008e);
                }
                i2++;
            }
        }
        this.f5212h = true;
    }
}
