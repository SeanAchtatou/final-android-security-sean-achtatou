package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.q;
import e.d.b.g;
import e.d.b.t.h;
import e.d.b.t.r;
import e.d.b.t.s;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/* compiled from: VertexBufferObjectWithVAO */
public class x implements y {

    /* renamed from: k  reason: collision with root package name */
    static final IntBuffer f5221k = BufferUtils.c(1);

    /* renamed from: a  reason: collision with root package name */
    final s f5222a;

    /* renamed from: b  reason: collision with root package name */
    final FloatBuffer f5223b;

    /* renamed from: c  reason: collision with root package name */
    final ByteBuffer f5224c;

    /* renamed from: d  reason: collision with root package name */
    final boolean f5225d;

    /* renamed from: e  reason: collision with root package name */
    int f5226e;

    /* renamed from: f  reason: collision with root package name */
    final int f5227f;

    /* renamed from: g  reason: collision with root package name */
    boolean f5228g = false;

    /* renamed from: h  reason: collision with root package name */
    boolean f5229h = false;

    /* renamed from: i  reason: collision with root package name */
    int f5230i = -1;

    /* renamed from: j  reason: collision with root package name */
    q f5231j = new q();

    public x(boolean z, int i2, s sVar) {
        this.f5222a = sVar;
        this.f5224c = BufferUtils.d(this.f5222a.f7013b * i2);
        this.f5223b = this.f5224c.asFloatBuffer();
        this.f5225d = true;
        this.f5223b.flip();
        this.f5224c.flip();
        this.f5226e = g.f6807h.a();
        this.f5227f = z ? 35044 : 35048;
        f();
    }

    private void e() {
        if (this.f5229h) {
            g.f6807h.a(34962, this.f5224c.limit(), this.f5224c, this.f5227f);
            this.f5228g = false;
        }
    }

    private void f() {
        f5221k.clear();
        g.f6808i.d(1, f5221k);
        this.f5230i = f5221k.get();
    }

    private void g() {
        if (this.f5230i != -1) {
            f5221k.clear();
            f5221k.put(this.f5230i);
            f5221k.flip();
            g.f6808i.a(1, f5221k);
            this.f5230i = -1;
        }
    }

    public FloatBuffer a() {
        this.f5228g = true;
        return this.f5223b;
    }

    public void b(s sVar, int[] iArr) {
        g.f6808i.i(0);
        this.f5229h = false;
    }

    public int c() {
        return (this.f5223b.limit() * 4) / this.f5222a.f7013b;
    }

    public s d() {
        return this.f5222a;
    }

    public void dispose() {
        h hVar = g.f6808i;
        hVar.e(34962, 0);
        hVar.e(this.f5226e);
        this.f5226e = 0;
        if (this.f5225d) {
            BufferUtils.a(this.f5224c);
        }
        g();
    }

    private void c(s sVar, int[] iArr) {
        boolean z = this.f5231j.f5559b != 0;
        int size = this.f5222a.size();
        if (z) {
            if (iArr == null) {
                int i2 = 0;
                while (z && i2 < size) {
                    z = sVar.b(this.f5222a.get(i2).f7009f) == this.f5231j.c(i2);
                    i2++;
                }
            } else {
                boolean z2 = iArr.length == this.f5231j.f5559b;
                int i3 = 0;
                while (z && i3 < size) {
                    z2 = iArr[i3] == this.f5231j.c(i3);
                    i3++;
                }
            }
        }
        if (!z) {
            g.f6806g.e(34962, this.f5226e);
            a(sVar);
            this.f5231j.a();
            for (int i4 = 0; i4 < size; i4++) {
                r rVar = this.f5222a.get(i4);
                if (iArr == null) {
                    this.f5231j.a(sVar.b(rVar.f7009f));
                } else {
                    this.f5231j.a(iArr[i4]);
                }
                int c2 = this.f5231j.c(i4);
                if (c2 >= 0) {
                    sVar.b(c2);
                    sVar.a(c2, rVar.f7005b, rVar.f7007d, rVar.f7006c, this.f5222a.f7013b, rVar.f7008e);
                }
            }
        }
    }

    public void a(float[] fArr, int i2, int i3) {
        this.f5228g = true;
        BufferUtils.a(fArr, this.f5224c, i3, i2);
        this.f5223b.position(0);
        this.f5223b.limit(i3);
        e();
    }

    public void b() {
        this.f5226e = g.f6808i.a();
        f();
        this.f5228g = true;
    }

    public void a(s sVar, int[] iArr) {
        h hVar = g.f6808i;
        hVar.i(this.f5230i);
        c(sVar, iArr);
        a(hVar);
        this.f5229h = true;
    }

    private void a(s sVar) {
        if (this.f5231j.f5559b != 0) {
            int size = this.f5222a.size();
            for (int i2 = 0; i2 < size; i2++) {
                int c2 = this.f5231j.c(i2);
                if (c2 >= 0) {
                    sVar.a(c2);
                }
            }
        }
    }

    private void a(e.d.b.t.g gVar) {
        if (this.f5228g) {
            gVar.e(34962, this.f5226e);
            this.f5224c.limit(this.f5223b.limit() * 4);
            gVar.a(34962, this.f5224c.limit(), this.f5224c, this.f5227f);
            this.f5228g = false;
        }
    }
}
