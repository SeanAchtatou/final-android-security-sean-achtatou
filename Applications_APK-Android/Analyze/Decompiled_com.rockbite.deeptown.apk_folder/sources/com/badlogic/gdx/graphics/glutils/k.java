package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.utils.BufferUtils;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

/* compiled from: IndexArray */
public class k implements n {

    /* renamed from: a  reason: collision with root package name */
    final ShortBuffer f5142a;

    /* renamed from: b  reason: collision with root package name */
    final ByteBuffer f5143b;

    /* renamed from: c  reason: collision with root package name */
    private final boolean f5144c;

    public k(int i2) {
        this.f5144c = i2 == 0;
        this.f5143b = BufferUtils.d((this.f5144c ? 1 : i2) * 2);
        this.f5142a = this.f5143b.asShortBuffer();
        this.f5142a.flip();
        this.f5143b.flip();
    }

    public void a(short[] sArr, int i2, int i3) {
        this.f5142a.clear();
        this.f5142a.put(sArr, i2, i3);
        this.f5142a.flip();
        this.f5143b.position(0);
        this.f5143b.limit(i3 << 1);
    }

    public void b() {
    }

    public void dispose() {
        BufferUtils.a(this.f5143b);
    }

    public void e() {
    }

    public void f() {
    }

    public int g() {
        if (this.f5144c) {
            return 0;
        }
        return this.f5142a.limit();
    }

    public int h() {
        if (this.f5144c) {
            return 0;
        }
        return this.f5142a.capacity();
    }

    public ShortBuffer a() {
        return this.f5142a;
    }
}
