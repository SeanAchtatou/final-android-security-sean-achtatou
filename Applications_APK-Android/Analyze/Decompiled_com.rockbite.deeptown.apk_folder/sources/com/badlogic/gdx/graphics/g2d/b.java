package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.glutils.s;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.l;
import e.d.b.t.n;

/* compiled from: Batch */
public interface b extends l {
    void begin();

    void draw(r rVar, float f2, float f3, float f4, float f5);

    void draw(r rVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10);

    void draw(n nVar, float f2, float f3, float f4, float f5);

    void draw(n nVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9);

    void draw(n nVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, int i2, int i3, int i4, int i5, boolean z, boolean z2);

    void draw(n nVar, float[] fArr, int i2, int i3);

    void end();

    void flush();

    int getBlendDstFunc();

    int getBlendDstFuncAlpha();

    int getBlendSrcFunc();

    int getBlendSrcFuncAlpha();

    e.d.b.t.b getColor();

    s getShader();

    Matrix4 getTransformMatrix();

    void setBlendFunction(int i2, int i3);

    void setBlendFunctionSeparate(int i2, int i3, int i4, int i5);

    void setColor(float f2, float f3, float f4, float f5);

    void setColor(e.d.b.t.b bVar);

    void setProjectionMatrix(Matrix4 matrix4);

    void setShader(s sVar);

    void setTransformMatrix(Matrix4 matrix4);
}
