package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.o;
import e.d.b.g;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

/* compiled from: IndexBufferObjectSubData */
public class m implements n {

    /* renamed from: a  reason: collision with root package name */
    final ShortBuffer f5153a;

    /* renamed from: b  reason: collision with root package name */
    final ByteBuffer f5154b;

    /* renamed from: c  reason: collision with root package name */
    int f5155c;

    /* renamed from: d  reason: collision with root package name */
    boolean f5156d = true;

    /* renamed from: e  reason: collision with root package name */
    boolean f5157e = false;

    /* renamed from: f  reason: collision with root package name */
    final int f5158f;

    public m(boolean z, int i2) {
        this.f5154b = BufferUtils.a(i2 * 2);
        this.f5158f = z ? 35044 : 35048;
        this.f5153a = this.f5154b.asShortBuffer();
        this.f5153a.flip();
        this.f5154b.flip();
        this.f5155c = c();
    }

    private int c() {
        int a2 = g.f6807h.a();
        g.f6807h.e(34963, a2);
        g.f6807h.a(34963, this.f5154b.capacity(), (Buffer) null, this.f5158f);
        g.f6807h.e(34963, 0);
        return a2;
    }

    public void a(short[] sArr, int i2, int i3) {
        this.f5156d = true;
        this.f5153a.clear();
        this.f5153a.put(sArr, i2, i3);
        this.f5153a.flip();
        this.f5154b.position(0);
        this.f5154b.limit(i3 << 1);
        if (this.f5157e) {
            g.f6807h.a(34963, 0, this.f5154b.limit(), this.f5154b);
            this.f5156d = false;
        }
    }

    public void b() {
        this.f5155c = c();
        this.f5156d = true;
    }

    public void dispose() {
        e.d.b.t.g gVar = g.f6807h;
        gVar.e(34963, 0);
        gVar.e(this.f5155c);
        this.f5155c = 0;
    }

    public void e() {
        g.f6807h.e(34963, 0);
        this.f5157e = false;
    }

    public void f() {
        int i2 = this.f5155c;
        if (i2 != 0) {
            g.f6807h.e(34963, i2);
            if (this.f5156d) {
                this.f5154b.limit(this.f5153a.limit() * 2);
                g.f6807h.a(34963, 0, this.f5154b.limit(), this.f5154b);
                this.f5156d = false;
            }
            this.f5157e = true;
            return;
        }
        throw new o("IndexBufferObject cannot be used after it has been disposed.");
    }

    public int g() {
        return this.f5153a.limit();
    }

    public int h() {
        return this.f5153a.capacity();
    }

    public ShortBuffer a() {
        this.f5156d = true;
        return this.f5153a;
    }
}
