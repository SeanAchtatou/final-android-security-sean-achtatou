package com.badlogic.gdx.graphics.g2d;

import com.esotericsoftware.spine.Animation;
import com.facebook.appevents.codeless.internal.Constants;
import com.google.android.gms.measurement.api.AppMeasurementSdk;
import java.io.BufferedReader;
import java.io.IOException;

/* compiled from: ParticleEmitter */
public class h {
    private int A;
    private int B = 4;
    private float C;
    private float D;
    private String E;
    private com.badlogic.gdx.utils.a<String> F;
    private int G;
    private boolean[] H;
    private boolean I;
    private boolean J;
    private boolean K;
    private int L;
    private boolean M;
    private int N;
    private int O;
    private int P;
    private int Q;
    private int R;
    private int S;
    private int T;
    private float U;
    private float V;
    private float W;
    private float X;
    public float Y = 1.0f;
    public float Z;

    /* renamed from: a  reason: collision with root package name */
    private f f4910a = new f();
    private float a0;

    /* renamed from: b  reason: collision with root package name */
    private c f4911b = new c();
    private float b0;

    /* renamed from: c  reason: collision with root package name */
    private f f4912c = new f();
    private boolean c0;

    /* renamed from: d  reason: collision with root package name */
    private c f4913d = new c();
    private boolean d0;

    /* renamed from: e  reason: collision with root package name */
    private g f4914e = new g();
    private boolean e0;

    /* renamed from: f  reason: collision with root package name */
    private g f4915f = new g();
    private boolean f0;

    /* renamed from: g  reason: collision with root package name */
    private g f4916g = new g();
    private boolean g0 = true;

    /* renamed from: h  reason: collision with root package name */
    private g f4917h = new g();
    private boolean h0 = false;

    /* renamed from: i  reason: collision with root package name */
    private g f4918i = new g();
    boolean i0 = true;

    /* renamed from: j  reason: collision with root package name */
    private g f4919j = new g();

    /* renamed from: k  reason: collision with root package name */
    private g f4920k = new g();
    private g l = new g();
    private g m = new g();
    private b n = new b();
    private f o = new g();
    private f p = new g();
    private g q = new g();
    private g r = new g();
    private j s = new j();
    private f[] t;
    private f[] u;
    private f[] v;
    private float w;
    private com.badlogic.gdx.utils.a<o> x;
    private k y = k.single;
    private d[] z;

    /* compiled from: ParticleEmitter */
    static /* synthetic */ class a {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f4921a = new int[k.values().length];

        /* renamed from: b  reason: collision with root package name */
        static final /* synthetic */ int[] f4922b = new int[C0117h.values().length];

        /* renamed from: c  reason: collision with root package name */
        static final /* synthetic */ int[] f4923c = new int[i.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|(2:9|10)|11|13|14|15|16|17|19|20|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(21:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|16|17|19|20|21|22|23|24|26) */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x003d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x005a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0064 */
        static {
            /*
                com.badlogic.gdx.graphics.g2d.h$i[] r0 = com.badlogic.gdx.graphics.g2d.h.i.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.badlogic.gdx.graphics.g2d.h.a.f4923c = r0
                r0 = 1
                int[] r1 = com.badlogic.gdx.graphics.g2d.h.a.f4923c     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.badlogic.gdx.graphics.g2d.h$i r2 = com.badlogic.gdx.graphics.g2d.h.i.square     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = com.badlogic.gdx.graphics.g2d.h.a.f4923c     // Catch:{ NoSuchFieldError -> 0x001f }
                com.badlogic.gdx.graphics.g2d.h$i r3 = com.badlogic.gdx.graphics.g2d.h.i.ellipse     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = com.badlogic.gdx.graphics.g2d.h.a.f4923c     // Catch:{ NoSuchFieldError -> 0x002a }
                com.badlogic.gdx.graphics.g2d.h$i r4 = com.badlogic.gdx.graphics.g2d.h.i.line     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                com.badlogic.gdx.graphics.g2d.h$h[] r3 = com.badlogic.gdx.graphics.g2d.h.C0117h.values()
                int r3 = r3.length
                int[] r3 = new int[r3]
                com.badlogic.gdx.graphics.g2d.h.a.f4922b = r3
                int[] r3 = com.badlogic.gdx.graphics.g2d.h.a.f4922b     // Catch:{ NoSuchFieldError -> 0x003d }
                com.badlogic.gdx.graphics.g2d.h$h r4 = com.badlogic.gdx.graphics.g2d.h.C0117h.top     // Catch:{ NoSuchFieldError -> 0x003d }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x003d }
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x003d }
            L_0x003d:
                int[] r3 = com.badlogic.gdx.graphics.g2d.h.a.f4922b     // Catch:{ NoSuchFieldError -> 0x0047 }
                com.badlogic.gdx.graphics.g2d.h$h r4 = com.badlogic.gdx.graphics.g2d.h.C0117h.bottom     // Catch:{ NoSuchFieldError -> 0x0047 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
                r3[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0047 }
            L_0x0047:
                com.badlogic.gdx.graphics.g2d.h$k[] r3 = com.badlogic.gdx.graphics.g2d.h.k.values()
                int r3 = r3.length
                int[] r3 = new int[r3]
                com.badlogic.gdx.graphics.g2d.h.a.f4921a = r3
                int[] r3 = com.badlogic.gdx.graphics.g2d.h.a.f4921a     // Catch:{ NoSuchFieldError -> 0x005a }
                com.badlogic.gdx.graphics.g2d.h$k r4 = com.badlogic.gdx.graphics.g2d.h.k.single     // Catch:{ NoSuchFieldError -> 0x005a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x005a }
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x005a }
            L_0x005a:
                int[] r0 = com.badlogic.gdx.graphics.g2d.h.a.f4921a     // Catch:{ NoSuchFieldError -> 0x0064 }
                com.badlogic.gdx.graphics.g2d.h$k r3 = com.badlogic.gdx.graphics.g2d.h.k.animated     // Catch:{ NoSuchFieldError -> 0x0064 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0064 }
                r0[r3] = r1     // Catch:{ NoSuchFieldError -> 0x0064 }
            L_0x0064:
                int[] r0 = com.badlogic.gdx.graphics.g2d.h.a.f4921a     // Catch:{ NoSuchFieldError -> 0x006e }
                com.badlogic.gdx.graphics.g2d.h$k r1 = com.badlogic.gdx.graphics.g2d.h.k.random     // Catch:{ NoSuchFieldError -> 0x006e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006e }
            L_0x006e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.h.a.<clinit>():void");
        }
    }

    /* compiled from: ParticleEmitter */
    public static class b extends e {

        /* renamed from: e  reason: collision with root package name */
        private static float[] f4924e = new float[4];

        /* renamed from: c  reason: collision with root package name */
        private float[] f4925c = {1.0f, 1.0f, 1.0f};

        /* renamed from: d  reason: collision with root package name */
        float[] f4926d = {0.0f};

        public b() {
            this.f4929b = true;
        }

        public void a(float[] fArr) {
            this.f4925c = fArr;
        }

        public float[] a(float f2) {
            float[] fArr = this.f4926d;
            int length = fArr.length;
            int i2 = 1;
            int i3 = 0;
            while (true) {
                if (i2 >= length) {
                    i2 = -1;
                    break;
                } else if (fArr[i2] > f2) {
                    break;
                } else {
                    i3 = i2;
                    i2++;
                }
            }
            float f3 = fArr[i3];
            int i4 = i3 * 3;
            float[] fArr2 = this.f4925c;
            float f4 = fArr2[i4];
            float f5 = fArr2[i4 + 1];
            float f6 = fArr2[i4 + 2];
            if (i2 == -1) {
                float[] fArr3 = f4924e;
                fArr3[0] = f4;
                fArr3[1] = f5;
                fArr3[2] = f6;
                return fArr3;
            }
            float f7 = (f2 - f3) / (fArr[i2] - f3);
            int i5 = i2 * 3;
            float[] fArr4 = f4924e;
            fArr4[0] = f4 + ((fArr2[i5] - f4) * f7);
            fArr4[1] = f5 + ((fArr2[i5 + 1] - f5) * f7);
            fArr4[2] = f6 + ((fArr2[i5 + 2] - f6) * f7);
            return fArr4;
        }

        public void a(BufferedReader bufferedReader) throws IOException {
            super.a(bufferedReader);
            if (this.f4928a) {
                this.f4925c = new float[h.c(bufferedReader, "colorsCount")];
                int i2 = 0;
                int i3 = 0;
                while (true) {
                    float[] fArr = this.f4925c;
                    if (i3 >= fArr.length) {
                        break;
                    }
                    fArr[i3] = h.b(bufferedReader, "colors" + i3);
                    i3++;
                }
                this.f4926d = new float[h.c(bufferedReader, "timelineCount")];
                while (true) {
                    float[] fArr2 = this.f4926d;
                    if (i2 < fArr2.length) {
                        fArr2[i2] = h.b(bufferedReader, "timeline" + i2);
                        i2++;
                    } else {
                        return;
                    }
                }
            }
        }

        public void a(b bVar) {
            super.a((e) bVar);
            this.f4925c = new float[bVar.f4925c.length];
            float[] fArr = bVar.f4925c;
            float[] fArr2 = this.f4925c;
            System.arraycopy(fArr, 0, fArr2, 0, fArr2.length);
            this.f4926d = new float[bVar.f4926d.length];
            float[] fArr3 = bVar.f4926d;
            float[] fArr4 = this.f4926d;
            System.arraycopy(fArr3, 0, fArr4, 0, fArr4.length);
        }
    }

    /* compiled from: ParticleEmitter */
    public static class d extends o {
        protected float A;
        protected float B;
        protected float C;
        protected float D;
        protected float E;
        protected float F;
        protected float G;
        protected float H;
        protected float I;
        protected float J;
        protected float K;
        protected float L;
        protected float M;
        protected float N;
        protected float[] O;
        protected int P;
        protected int u;
        protected int v;
        protected float w;
        protected float x;
        protected float y;
        protected float z;

        public d(o oVar) {
            super(oVar);
        }
    }

    /* compiled from: ParticleEmitter */
    public static class e {

        /* renamed from: a  reason: collision with root package name */
        boolean f4928a;

        /* renamed from: b  reason: collision with root package name */
        boolean f4929b;

        public void a(boolean z) {
            this.f4928a = z;
        }

        public void b(boolean z) {
            this.f4929b = z;
        }

        public void a(BufferedReader bufferedReader) throws IOException {
            if (!this.f4929b) {
                this.f4928a = h.a(bufferedReader, AppMeasurementSdk.ConditionalUserProperty.ACTIVE);
            } else {
                this.f4928a = true;
            }
        }

        public void a(e eVar) {
            this.f4928a = eVar.f4928a;
            this.f4929b = eVar.f4929b;
        }
    }

    /* compiled from: ParticleEmitter */
    public static class f extends e {

        /* renamed from: c  reason: collision with root package name */
        private float f4930c;

        /* renamed from: d  reason: collision with root package name */
        private float f4931d;

        public void a(float f2, float f3) {
            this.f4930c = f2;
            this.f4931d = f3;
        }

        public float b() {
            float f2 = this.f4930c;
            return f2 + ((this.f4931d - f2) * com.badlogic.gdx.math.h.a());
        }

        public void c(float f2) {
            this.f4931d = f2;
        }

        public void d(float f2) {
            this.f4930c = f2;
        }

        public void b(float f2) {
            this.f4930c = f2;
            this.f4931d = f2;
        }

        public float a() {
            return this.f4931d;
        }

        public void a(float f2) {
            this.f4930c *= f2;
            this.f4931d *= f2;
        }

        public void a(BufferedReader bufferedReader) throws IOException {
            super.a(bufferedReader);
            if (this.f4928a) {
                this.f4930c = h.b(bufferedReader, "lowMin");
                this.f4931d = h.b(bufferedReader, "lowMax");
            }
        }

        public void a(f fVar) {
            super.a((e) fVar);
            this.f4931d = fVar.f4931d;
            this.f4930c = fVar.f4930c;
        }
    }

    /* compiled from: ParticleEmitter */
    public static class g extends f {

        /* renamed from: e  reason: collision with root package name */
        private float[] f4932e = {1.0f};

        /* renamed from: f  reason: collision with root package name */
        float[] f4933f = {0.0f};

        /* renamed from: g  reason: collision with root package name */
        private float f4934g;

        /* renamed from: h  reason: collision with root package name */
        private float f4935h;

        /* renamed from: i  reason: collision with root package name */
        private boolean f4936i;

        public void a(float f2) {
            super.a(f2);
            this.f4934g *= f2;
            this.f4935h *= f2;
        }

        public void b(float f2, float f3) {
            this.f4934g = f2;
            this.f4935h = f3;
        }

        public float c() {
            return this.f4935h;
        }

        public boolean d() {
            return this.f4936i;
        }

        public float e() {
            float f2 = this.f4934g;
            return f2 + ((this.f4935h - f2) * com.badlogic.gdx.math.h.a());
        }

        public void f(float f2) {
            this.f4934g = f2;
            this.f4935h = f2;
        }

        public float e(float f2) {
            float[] fArr = this.f4933f;
            int length = fArr.length;
            int i2 = 1;
            while (true) {
                if (i2 >= length) {
                    i2 = -1;
                    break;
                } else if (fArr[i2] > f2) {
                    break;
                } else {
                    i2++;
                }
            }
            if (i2 == -1) {
                return this.f4932e[length - 1];
            }
            float[] fArr2 = this.f4932e;
            int i3 = i2 - 1;
            float f3 = fArr2[i3];
            float f4 = fArr[i3];
            return f3 + ((fArr2[i2] - f3) * ((f2 - f4) / (fArr[i2] - f4)));
        }

        public void a(float[] fArr) {
            this.f4932e = fArr;
        }

        public void a(BufferedReader bufferedReader) throws IOException {
            super.a(bufferedReader);
            if (this.f4928a) {
                this.f4934g = h.b(bufferedReader, "highMin");
                this.f4935h = h.b(bufferedReader, "highMax");
                this.f4936i = h.a(bufferedReader, Constants.PATH_TYPE_RELATIVE);
                this.f4932e = new float[h.c(bufferedReader, "scalingCount")];
                int i2 = 0;
                int i3 = 0;
                while (true) {
                    float[] fArr = this.f4932e;
                    if (i3 >= fArr.length) {
                        break;
                    }
                    fArr[i3] = h.b(bufferedReader, "scaling" + i3);
                    i3++;
                }
                this.f4933f = new float[h.c(bufferedReader, "timelineCount")];
                while (true) {
                    float[] fArr2 = this.f4933f;
                    if (i2 < fArr2.length) {
                        fArr2[i2] = h.b(bufferedReader, "timeline" + i2);
                        i2++;
                    } else {
                        return;
                    }
                }
            }
        }

        public void a(g gVar) {
            super.a((f) gVar);
            this.f4935h = gVar.f4935h;
            this.f4934g = gVar.f4934g;
            this.f4932e = new float[gVar.f4932e.length];
            float[] fArr = gVar.f4932e;
            float[] fArr2 = this.f4932e;
            System.arraycopy(fArr, 0, fArr2, 0, fArr2.length);
            this.f4933f = new float[gVar.f4933f.length];
            float[] fArr3 = gVar.f4933f;
            float[] fArr4 = this.f4933f;
            System.arraycopy(fArr3, 0, fArr4, 0, fArr4.length);
            this.f4936i = gVar.f4936i;
        }
    }

    /* renamed from: com.badlogic.gdx.graphics.g2d.h$h  reason: collision with other inner class name */
    /* compiled from: ParticleEmitter */
    public enum C0117h {
        both,
        top,
        bottom
    }

    /* compiled from: ParticleEmitter */
    public enum i {
        point,
        line,
        square,
        ellipse
    }

    /* compiled from: ParticleEmitter */
    public enum k {
        single,
        random,
        animated
    }

    public h() {
        z();
    }

    private void A() {
        f fVar = this.f4910a;
        this.a0 = fVar.f4928a ? fVar.b() : Animation.CurveTimeline.LINEAR;
        this.b0 = Animation.CurveTimeline.LINEAR;
        this.Z -= this.Y;
        this.Y = this.f4912c.b();
        this.N = (int) this.f4914e.b();
        this.O = (int) this.f4914e.e();
        if (!this.f4914e.d()) {
            this.O -= this.N;
        }
        if (!this.f4913d.f4927j) {
            y();
        }
        if (!this.f4911b.f4927j) {
            x();
        }
        this.U = this.q.b();
        this.V = this.q.e();
        if (!this.q.d()) {
            this.V -= this.U;
        }
        this.W = this.r.b();
        this.X = this.r.e();
        if (!this.r.d()) {
            this.X -= this.W;
        }
        this.L = 0;
        g gVar = this.f4919j;
        if (gVar.f4928a && gVar.f4933f.length > 1) {
            this.L |= 2;
        }
        if (this.f4918i.f4928a) {
            this.L |= 8;
        }
        if (this.f4915f.f4933f.length > 1) {
            this.L |= 1;
        }
        g gVar2 = this.f4916g;
        if (gVar2.f4928a && gVar2.f4933f.length > 1) {
            this.L |= 1;
        }
        g gVar3 = this.f4917h;
        if (gVar3.f4928a && gVar3.f4933f.length > 1) {
            this.L |= 4;
        }
        if (this.f4920k.f4928a) {
            this.L |= 16;
        }
        if (this.l.f4928a) {
            this.L |= 32;
        }
        if (this.n.f4926d.length > 1) {
            this.L |= 64;
        }
        if (this.y == k.animated) {
            this.L |= 128;
        }
    }

    private void d(int i2) {
        o oVar;
        float f2;
        float d2;
        float d3;
        float f3;
        int i3 = a.f4921a[this.y.ordinal()];
        if (i3 == 1 || i3 == 2) {
            oVar = this.x.a();
        } else if (i3 != 3) {
            oVar = null;
        } else {
            oVar = this.x.b();
        }
        d[] dVarArr = this.z;
        d dVar = dVarArr[i2];
        if (dVar == null) {
            dVar = a(oVar);
            dVarArr[i2] = dVar;
            dVar.a(this.J, this.K);
        } else {
            dVar.a(oVar);
        }
        float f4 = this.Z / this.Y;
        int i4 = this.L;
        if (this.f4913d.f4927j) {
            y();
        }
        if (this.f4911b.f4927j) {
            x();
        }
        int e2 = this.S + ((int) (((float) this.T) * this.f4913d.e(f4)));
        dVar.u = e2;
        dVar.v = e2;
        g gVar = this.f4918i;
        if (gVar.f4928a) {
            dVar.C = gVar.b();
            dVar.D = this.f4918i.e();
            if (!this.f4918i.d()) {
                dVar.D -= dVar.C;
            }
        }
        dVar.E = this.f4919j.b();
        dVar.F = this.f4919j.e();
        if (!this.f4919j.d()) {
            dVar.F -= dVar.E;
        }
        int i5 = i4 & 2;
        if (i5 == 0) {
            f2 = dVar.E + (dVar.F * this.f4919j.e(Animation.CurveTimeline.LINEAR));
            dVar.E = f2;
            dVar.G = com.badlogic.gdx.math.h.b(f2);
            dVar.H = com.badlogic.gdx.math.h.h(f2);
        } else {
            f2 = Animation.CurveTimeline.LINEAR;
        }
        float r2 = oVar.r();
        float n2 = oVar.n();
        dVar.w = this.f4915f.b() / r2;
        dVar.x = this.f4915f.e() / r2;
        if (!this.f4915f.d()) {
            dVar.x -= dVar.w;
        }
        g gVar2 = this.f4916g;
        if (gVar2.f4928a) {
            dVar.y = gVar2.b() / n2;
            dVar.z = this.f4916g.e() / n2;
            if (!this.f4916g.d()) {
                dVar.z -= dVar.y;
            }
            dVar.c(dVar.w + (dVar.x * this.f4915f.e(Animation.CurveTimeline.LINEAR)), dVar.y + (dVar.z * this.f4916g.e(Animation.CurveTimeline.LINEAR)));
        } else {
            dVar.g(dVar.w + (dVar.x * this.f4915f.e(Animation.CurveTimeline.LINEAR)));
        }
        g gVar3 = this.f4917h;
        if (gVar3.f4928a) {
            dVar.A = gVar3.b();
            dVar.B = this.f4917h.e();
            if (!this.f4917h.d()) {
                dVar.B -= dVar.A;
            }
            float e3 = dVar.A + (dVar.B * this.f4917h.e(Animation.CurveTimeline.LINEAR));
            if (this.e0) {
                e3 += f2;
            }
            dVar.f(e3);
        }
        g gVar4 = this.f4920k;
        if (gVar4.f4928a) {
            dVar.K = gVar4.b();
            dVar.L = this.f4920k.e();
            if (!this.f4920k.d()) {
                dVar.L -= dVar.K;
            }
        }
        g gVar5 = this.l;
        if (gVar5.f4928a) {
            dVar.M = gVar5.b();
            dVar.N = this.l.e();
            if (!this.l.d()) {
                dVar.N -= dVar.M;
            }
        }
        float[] fArr = dVar.O;
        if (fArr == null) {
            fArr = new float[3];
            dVar.O = fArr;
        }
        float[] a2 = this.n.a((float) Animation.CurveTimeline.LINEAR);
        fArr[0] = a2[0];
        fArr[1] = a2[1];
        fArr[2] = a2[2];
        dVar.I = this.m.b();
        dVar.J = this.m.e() - dVar.I;
        float f5 = this.C;
        f fVar = this.o;
        if (fVar.f4928a) {
            f5 += fVar.b();
        }
        float f6 = this.D;
        f fVar2 = this.p;
        if (fVar2.f4928a) {
            f6 += fVar2.b();
        }
        int i6 = a.f4923c[this.s.f4946c.ordinal()];
        if (i6 == 1) {
            float e4 = this.U + (this.V * this.q.e(f4));
            float e5 = this.W + (this.X * this.r.e(f4));
            f5 += com.badlogic.gdx.math.h.d(e4) - (e4 / 2.0f);
            f6 += com.badlogic.gdx.math.h.d(e5) - (e5 / 2.0f);
        } else if (i6 == 2) {
            float e6 = this.U + (this.V * this.q.e(f4));
            float f7 = e6 / 2.0f;
            float e7 = (this.W + (this.X * this.r.e(f4))) / 2.0f;
            if (!(f7 == Animation.CurveTimeline.LINEAR || e7 == Animation.CurveTimeline.LINEAR)) {
                float f8 = f7 / e7;
                j jVar = this.s;
                if (jVar.f4947d) {
                    int i7 = a.f4922b[jVar.f4948e.ordinal()];
                    if (i7 == 1) {
                        f3 = -com.badlogic.gdx.math.h.d(179.0f);
                    } else if (i7 != 2) {
                        f3 = com.badlogic.gdx.math.h.d(360.0f);
                    } else {
                        f3 = com.badlogic.gdx.math.h.d(179.0f);
                    }
                    float b2 = com.badlogic.gdx.math.h.b(f3);
                    float h2 = com.badlogic.gdx.math.h.h(f3);
                    f5 += b2 * f7;
                    f6 += (f7 * h2) / f8;
                    if (i5 == 0) {
                        dVar.E = f3;
                        dVar.G = b2;
                        dVar.H = h2;
                    }
                } else {
                    float f9 = f7 * f7;
                    do {
                        d2 = com.badlogic.gdx.math.h.d(e6) - f7;
                        d3 = com.badlogic.gdx.math.h.d(e6) - f7;
                    } while ((d2 * d2) + (d3 * d3) > f9);
                    f5 += d2;
                    f6 += d3 / f8;
                }
            }
        } else if (i6 == 3) {
            float e8 = this.U + (this.V * this.q.e(f4));
            float e9 = this.W + (this.X * this.r.e(f4));
            if (e8 != Animation.CurveTimeline.LINEAR) {
                float a3 = com.badlogic.gdx.math.h.a() * e8;
                f5 += a3;
                f6 += a3 * (e9 / e8);
            } else {
                f6 += e9 * com.badlogic.gdx.math.h.a();
            }
        }
        dVar.b(f5 - (r2 / 2.0f), f6 - (n2 / 2.0f), r2, n2);
        int e10 = (int) (((float) this.Q) + (((float) this.R) * this.f4911b.e(f4)));
        if (e10 > 0) {
            int i8 = dVar.v;
            if (e10 >= i8) {
                e10 = i8 - 1;
            }
            a(dVar, ((float) e10) / 1000.0f, e10);
        }
    }

    private void x() {
        c cVar = this.f4911b;
        this.Q = cVar.f4928a ? (int) cVar.b() : 0;
        this.R = (int) this.f4911b.e();
        if (!this.f4911b.d()) {
            this.R -= this.Q;
        }
    }

    private void y() {
        this.S = (int) this.f4913d.b();
        this.T = (int) this.f4913d.e();
        if (!this.f4913d.d()) {
            this.T -= this.S;
        }
    }

    private void z() {
        this.x = new com.badlogic.gdx.utils.a<>();
        this.F = new com.badlogic.gdx.utils.a<>();
        this.f4912c.b(true);
        this.f4914e.b(true);
        this.f4913d.b(true);
        this.f4915f.b(true);
        this.m.b(true);
        this.s.b(true);
        this.q.b(true);
        this.r.b(true);
    }

    public void a() {
        int i2 = this.G;
        if (i2 != this.B) {
            boolean[] zArr = this.H;
            int length = zArr.length;
            for (int i3 = 0; i3 < length; i3++) {
                if (!zArr[i3]) {
                    d(i3);
                    zArr[i3] = true;
                    this.G = i2 + 1;
                    return;
                }
            }
        }
    }

    public void b(int i2) {
        this.B = i2;
        this.H = new boolean[i2];
        this.G = 0;
        this.z = new d[i2];
    }

    public g c() {
        return this.f4919j;
    }

    public f e() {
        return this.f4912c;
    }

    public g f() {
        return this.f4914e;
    }

    public com.badlogic.gdx.utils.a<String> g() {
        return this.F;
    }

    public g h() {
        return this.f4913d;
    }

    /* access modifiers changed from: protected */
    public f[] i() {
        if (this.v == null) {
            this.v = new f[3];
            f[] fVarArr = this.v;
            fVarArr[0] = this.f4918i;
            fVarArr[1] = this.f4920k;
            fVarArr[2] = this.l;
        }
        return this.v;
    }

    public String j() {
        return this.E;
    }

    public g k() {
        return this.f4917h;
    }

    public com.badlogic.gdx.utils.a<o> l() {
        return this.x;
    }

    public b m() {
        return this.n;
    }

    public g n() {
        return this.m;
    }

    public g o() {
        return this.f4918i;
    }

    public float p() {
        return this.C;
    }

    /* access modifiers changed from: protected */
    public f[] q() {
        if (this.t == null) {
            this.t = new f[3];
            f[] fVarArr = this.t;
            fVarArr[0] = this.f4915f;
            fVarArr[1] = this.q;
            fVarArr[2] = this.o;
        }
        return this.t;
    }

    public float r() {
        return this.D;
    }

    public f s() {
        return this.p;
    }

    /* access modifiers changed from: protected */
    public f[] t() {
        if (this.u == null) {
            this.u = new f[3];
            f[] fVarArr = this.u;
            fVarArr[0] = this.f4916g;
            fVarArr[1] = this.r;
            fVarArr[2] = this.p;
        }
        return this.u;
    }

    public boolean u() {
        return this.d0;
    }

    public void v() {
        this.P = 0;
        this.Z = this.Y;
        boolean[] zArr = this.H;
        int length = zArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            zArr[i2] = false;
        }
        this.G = 0;
        w();
    }

    public void w() {
        this.I = true;
        this.M = false;
        A();
    }

    public void c(int i2) {
        this.A = i2;
    }

    static int c(BufferedReader bufferedReader, String str) throws IOException {
        return Integer.parseInt(d(bufferedReader, str));
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x004b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(float r9) {
        /*
            r8 = this;
            float r0 = r8.w
            r1 = 1148846080(0x447a0000, float:1000.0)
            float r2 = r9 * r1
            float r0 = r0 + r2
            r8.w = r0
            float r0 = r8.w
            r2 = 1065353216(0x3f800000, float:1.0)
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 >= 0) goto L_0x0012
            return
        L_0x0012:
            int r2 = (int) r0
            float r3 = (float) r2
            float r0 = r0 - r3
            r8.w = r0
            float r0 = r8.b0
            float r4 = r8.a0
            r5 = 0
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 >= 0) goto L_0x0025
            float r0 = r0 + r3
            r8.b0 = r0
            goto L_0x009b
        L_0x0025:
            boolean r0 = r8.I
            if (r0 == 0) goto L_0x002e
            r8.I = r5
            r8.a()
        L_0x002e:
            float r0 = r8.Z
            float r4 = r8.Y
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 >= 0) goto L_0x003a
            float r0 = r0 + r3
            r8.Z = r0
            goto L_0x0046
        L_0x003a:
            boolean r0 = r8.d0
            if (r0 == 0) goto L_0x0048
            boolean r0 = r8.M
            if (r0 == 0) goto L_0x0043
            goto L_0x0048
        L_0x0043:
            r8.A()
        L_0x0046:
            r0 = 0
            goto L_0x0049
        L_0x0048:
            r0 = 1
        L_0x0049:
            if (r0 != 0) goto L_0x009b
            int r0 = r8.P
            int r0 = r0 + r2
            r8.P = r0
            int r0 = r8.N
            float r0 = (float) r0
            int r3 = r8.O
            float r3 = (float) r3
            com.badlogic.gdx.graphics.g2d.h$g r4 = r8.f4914e
            float r6 = r8.Z
            float r7 = r8.Y
            float r6 = r6 / r7
            float r4 = r4.e(r6)
            float r3 = r3 * r4
            float r0 = r0 + r3
            r3 = 0
            int r3 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r3 <= 0) goto L_0x0091
            float r1 = r1 / r0
            int r0 = r8.P
            float r3 = (float) r0
            int r3 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r3 < 0) goto L_0x0091
            float r0 = (float) r0
            float r0 = r0 / r1
            int r0 = (int) r0
            int r3 = r8.B
            int r4 = r8.G
            int r3 = r3 - r4
            int r0 = java.lang.Math.min(r0, r3)
            int r3 = r8.P
            float r3 = (float) r3
            float r4 = (float) r0
            float r4 = r4 * r1
            float r3 = r3 - r4
            int r3 = (int) r3
            r8.P = r3
            int r3 = r8.P
            float r3 = (float) r3
            float r3 = r3 % r1
            int r1 = (int) r3
            r8.P = r1
            r8.a(r0)
        L_0x0091:
            int r0 = r8.G
            int r1 = r8.A
            if (r0 >= r1) goto L_0x009b
            int r1 = r1 - r0
            r8.a(r1)
        L_0x009b:
            boolean[] r0 = r8.H
            int r1 = r8.G
            com.badlogic.gdx.graphics.g2d.h$d[] r3 = r8.z
            int r4 = r0.length
            r6 = r1
            r1 = 0
        L_0x00a4:
            if (r1 >= r4) goto L_0x00b9
            boolean r7 = r0[r1]
            if (r7 == 0) goto L_0x00b6
            r7 = r3[r1]
            boolean r7 = r8.a(r7, r9, r2)
            if (r7 != 0) goto L_0x00b6
            r0[r1] = r5
            int r6 = r6 + -1
        L_0x00b6:
            int r1 = r1 + 1
            goto L_0x00a4
        L_0x00b9:
            r8.G = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.h.b(float):void");
    }

    /* compiled from: ParticleEmitter */
    public static class j extends e {

        /* renamed from: c  reason: collision with root package name */
        i f4946c = i.point;

        /* renamed from: d  reason: collision with root package name */
        boolean f4947d;

        /* renamed from: e  reason: collision with root package name */
        C0117h f4948e = C0117h.both;

        public void a(BufferedReader bufferedReader) throws IOException {
            super.a(bufferedReader);
            if (this.f4928a) {
                this.f4946c = i.valueOf(h.d(bufferedReader, "shape"));
                if (this.f4946c == i.ellipse) {
                    this.f4947d = h.a(bufferedReader, "edges");
                    this.f4948e = C0117h.valueOf(h.d(bufferedReader, "side"));
                }
            }
        }

        public void a(j jVar) {
            super.a((e) jVar);
            this.f4946c = jVar.f4946c;
            this.f4947d = jVar.f4947d;
            this.f4948e = jVar.f4948e;
        }
    }

    public void a(int i2) {
        int min = Math.min(i2, this.B - this.G);
        if (min != 0) {
            boolean[] zArr = this.H;
            int length = zArr.length;
            int i3 = 0;
            int i4 = 0;
            loop0:
            while (i3 < min) {
                while (i4 < length) {
                    if (!zArr[i4]) {
                        d(i4);
                        zArr[i4] = true;
                        i3++;
                        i4++;
                    } else {
                        i4++;
                    }
                }
                break loop0;
            }
            this.G += min;
        }
    }

    /* compiled from: ParticleEmitter */
    public static class c extends g {

        /* renamed from: j  reason: collision with root package name */
        boolean f4927j;

        public void a(BufferedReader bufferedReader) throws IOException {
            super.a(bufferedReader);
            if (bufferedReader.markSupported()) {
                bufferedReader.mark(100);
            }
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                throw new IOException("Missing value: independent");
            } else if (readLine.contains("independent")) {
                this.f4927j = Boolean.parseBoolean(h.b(readLine));
            } else if (bufferedReader.markSupported()) {
                bufferedReader.reset();
            } else {
                e.d.b.g.f6800a.c("ParticleEmitter", "The loaded particle effect descriptor file uses an old invalid format. Please download the latest version of the Particle Editor tool and recreate the file by loading and saving it again.");
                throw new IOException("The loaded particle effect descriptor file uses an old invalid format. Please download the latest version of the Particle Editor tool and recreate the file by loading and saving it again.");
            }
        }

        public void a(c cVar) {
            super.a((g) cVar);
            this.f4927j = cVar.f4927j;
        }
    }

    public void a(b bVar) {
        if (this.h0) {
            bVar.setBlendFunction(1, 771);
        } else if (this.g0) {
            bVar.setBlendFunction(770, 1);
        } else {
            bVar.setBlendFunction(770, 771);
        }
        d[] dVarArr = this.z;
        boolean[] zArr = this.H;
        int length = zArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (zArr[i2]) {
                dVarArr[i2].a(bVar);
            }
        }
        if (!this.i0) {
            return;
        }
        if (this.g0 || this.h0) {
            bVar.setBlendFunction(770, 771);
        }
    }

    /* access modifiers changed from: protected */
    public d a(o oVar) {
        return new d(oVar);
    }

    public h(BufferedReader bufferedReader) throws IOException {
        z();
        a(bufferedReader);
    }

    private boolean a(d dVar, float f2, int i2) {
        float[] fArr;
        float f3;
        float f4;
        int i3 = dVar.v - i2;
        if (i3 <= 0) {
            return false;
        }
        dVar.v = i3;
        float f5 = 1.0f;
        float f6 = 1.0f - (((float) dVar.v) / ((float) dVar.u));
        int i4 = this.L;
        if ((i4 & 1) != 0) {
            if (this.f4916g.f4928a) {
                dVar.c(dVar.w + (dVar.x * this.f4915f.e(f6)), dVar.y + (dVar.z * this.f4916g.e(f6)));
            } else {
                dVar.g(dVar.w + (dVar.x * this.f4915f.e(f6)));
            }
        }
        if ((i4 & 8) != 0) {
            float e2 = (dVar.C + (dVar.D * this.f4918i.e(f6))) * f2;
            if ((i4 & 2) != 0) {
                float e3 = dVar.E + (dVar.F * this.f4919j.e(f6));
                f3 = com.badlogic.gdx.math.h.b(e3) * e2;
                f4 = e2 * com.badlogic.gdx.math.h.h(e3);
                if ((i4 & 4) != 0) {
                    float e4 = dVar.A + (dVar.B * this.f4917h.e(f6));
                    if (this.e0) {
                        e4 += e3;
                    }
                    dVar.f(e4);
                }
            } else {
                f3 = e2 * dVar.G;
                f4 = e2 * dVar.H;
                if (this.e0 || (i4 & 4) != 0) {
                    float e5 = dVar.A + (dVar.B * this.f4917h.e(f6));
                    if (this.e0) {
                        e5 += dVar.E;
                    }
                    dVar.f(e5);
                }
            }
            if ((i4 & 16) != 0) {
                f3 += (dVar.K + (dVar.L * this.f4920k.e(f6))) * f2;
            }
            if ((i4 & 32) != 0) {
                f4 += (dVar.M + (dVar.N * this.l.e(f6))) * f2;
            }
            dVar.e(f3, f4);
        } else if ((i4 & 4) != 0) {
            dVar.f(dVar.A + (dVar.B * this.f4917h.e(f6)));
        }
        if ((i4 & 64) != 0) {
            fArr = this.n.a(f6);
        } else {
            fArr = dVar.O;
        }
        if (this.h0) {
            if (this.g0) {
                f5 = Animation.CurveTimeline.LINEAR;
            }
            float e6 = dVar.I + (dVar.J * this.m.e(f6));
            dVar.c(fArr[0] * e6, fArr[1] * e6, fArr[2] * e6, e6 * f5);
        } else {
            dVar.c(fArr[0], fArr[1], fArr[2], dVar.I + (dVar.J * this.m.e(f6)));
        }
        if ((i4 & 128) != 0) {
            int i5 = this.x.f5374b;
            int min = Math.min((int) (f6 * ((float) i5)), i5 - 1);
            if (dVar.P != min) {
                o oVar = this.x.get(min);
                float r2 = dVar.r();
                float n2 = dVar.n();
                dVar.a((r) oVar);
                dVar.d(oVar.r(), oVar.n());
                dVar.a(oVar.o(), oVar.p());
                dVar.e((r2 - oVar.r()) / 2.0f, (n2 - oVar.n()) / 2.0f);
                dVar.P = min;
            }
        }
        return true;
    }

    public void b(float f2, float f3) {
        if (this.c0) {
            float f4 = f2 - this.C;
            float f5 = f3 - this.D;
            boolean[] zArr = this.H;
            int length = zArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                if (zArr[i2]) {
                    this.z[i2].e(f4, f5);
                }
            }
        }
        this.C = f2;
        this.D = f3;
    }

    public void b(com.badlogic.gdx.utils.a<o> aVar) {
        this.x = aVar;
        if (aVar.f5374b != 0) {
            int i2 = 0;
            int length = this.z.length;
            while (i2 < length) {
                d dVar = this.z[i2];
                if (dVar != null) {
                    o oVar = null;
                    int i3 = a.f4921a[this.y.ordinal()];
                    if (i3 == 1) {
                        oVar = aVar.a();
                    } else if (i3 == 2) {
                        float f2 = 1.0f - (((float) dVar.v) / ((float) dVar.u));
                        int i4 = aVar.f5374b;
                        dVar.P = Math.min((int) (f2 * ((float) i4)), i4 - 1);
                        oVar = aVar.get(dVar.P);
                    } else if (i3 == 3) {
                        oVar = aVar.b();
                    }
                    dVar.a((r) oVar);
                    dVar.a(oVar.o(), oVar.p());
                    i2++;
                } else {
                    return;
                }
            }
        }
    }

    public void b() {
        this.M = true;
        this.Z = this.Y;
    }

    static String b(String str) throws IOException {
        return str.substring(str.indexOf(":") + 1).trim();
    }

    static float b(BufferedReader bufferedReader, String str) throws IOException {
        return Float.parseFloat(d(bufferedReader, str));
    }

    public h(h hVar) {
        this.x = new com.badlogic.gdx.utils.a<>(hVar.x);
        this.E = hVar.E;
        this.F = new com.badlogic.gdx.utils.a<>(hVar.F);
        b(hVar.B);
        this.A = hVar.A;
        this.f4910a.a(hVar.f4910a);
        this.f4912c.a(hVar.f4912c);
        this.f4914e.a(hVar.f4914e);
        this.f4913d.a(hVar.f4913d);
        this.f4911b.a(hVar.f4911b);
        this.f4915f.a(hVar.f4915f);
        this.f4916g.a(hVar.f4916g);
        this.f4917h.a(hVar.f4917h);
        this.f4918i.a(hVar.f4918i);
        this.f4919j.a(hVar.f4919j);
        this.f4920k.a(hVar.f4920k);
        this.l.a(hVar.l);
        this.m.a(hVar.m);
        this.n.a(hVar.n);
        this.o.a(hVar.o);
        this.p.a(hVar.p);
        this.q.a(hVar.q);
        this.r.a(hVar.r);
        this.s.a(hVar.s);
        this.c0 = hVar.c0;
        this.d0 = hVar.d0;
        this.e0 = hVar.e0;
        this.f0 = hVar.f0;
        this.g0 = hVar.g0;
        this.h0 = hVar.h0;
        this.i0 = hVar.i0;
        this.y = hVar.y;
        b(hVar.p(), hVar.r());
    }

    public void a(boolean z2) {
        this.i0 = z2;
    }

    public void a(com.badlogic.gdx.utils.a<String> aVar) {
        this.F = aVar;
    }

    public void a(float f2, float f3) {
        if (f2 != 1.0f || f3 != 1.0f) {
            for (f a2 : q()) {
                a2.a(f2);
            }
            for (f a3 : t()) {
                a3.a(f3);
            }
        }
    }

    public void a(float f2) {
        if (f2 != 1.0f) {
            for (f a2 : i()) {
                a2.a(f2);
            }
        }
    }

    public void a(BufferedReader bufferedReader) throws IOException {
        try {
            this.E = d(bufferedReader, "name");
            bufferedReader.readLine();
            this.f4910a.a(bufferedReader);
            bufferedReader.readLine();
            this.f4912c.a(bufferedReader);
            bufferedReader.readLine();
            c(c(bufferedReader, "minParticleCount"));
            b(c(bufferedReader, "maxParticleCount"));
            bufferedReader.readLine();
            this.f4914e.a(bufferedReader);
            bufferedReader.readLine();
            this.f4913d.a(bufferedReader);
            bufferedReader.readLine();
            this.f4911b.a(bufferedReader);
            bufferedReader.readLine();
            this.o.a(bufferedReader);
            bufferedReader.readLine();
            this.p.a(bufferedReader);
            bufferedReader.readLine();
            this.s.a(bufferedReader);
            bufferedReader.readLine();
            this.q.a(bufferedReader);
            bufferedReader.readLine();
            this.r.a(bufferedReader);
            if (bufferedReader.readLine().trim().equals("- Scale -")) {
                this.f4915f.a(bufferedReader);
                this.f4916g.a(false);
            } else {
                this.f4915f.a(bufferedReader);
                bufferedReader.readLine();
                this.f4916g.a(bufferedReader);
            }
            bufferedReader.readLine();
            this.f4918i.a(bufferedReader);
            bufferedReader.readLine();
            this.f4919j.a(bufferedReader);
            bufferedReader.readLine();
            this.f4917h.a(bufferedReader);
            bufferedReader.readLine();
            this.f4920k.a(bufferedReader);
            bufferedReader.readLine();
            this.l.a(bufferedReader);
            bufferedReader.readLine();
            this.n.a(bufferedReader);
            bufferedReader.readLine();
            this.m.a(bufferedReader);
            bufferedReader.readLine();
            this.c0 = a(bufferedReader, "attached");
            this.d0 = a(bufferedReader, "continuous");
            this.e0 = a(bufferedReader, "aligned");
            this.g0 = a(bufferedReader, "additive");
            this.f0 = a(bufferedReader, "behind");
            String readLine = bufferedReader.readLine();
            if (readLine.startsWith("premultipliedAlpha")) {
                this.h0 = a(readLine);
                readLine = bufferedReader.readLine();
            }
            if (readLine.startsWith("spriteMode")) {
                this.y = k.valueOf(b(readLine));
                bufferedReader.readLine();
            }
            com.badlogic.gdx.utils.a aVar = new com.badlogic.gdx.utils.a();
            while (true) {
                String readLine2 = bufferedReader.readLine();
                if (readLine2 == null || readLine2.isEmpty()) {
                    a(aVar);
                } else {
                    aVar.add(readLine2);
                }
            }
            a(aVar);
        } catch (RuntimeException e2) {
            if (this.E == null) {
                throw e2;
            }
            throw new RuntimeException("Error parsing emitter: " + this.E, e2);
        }
    }

    public f d() {
        return this.f4910a;
    }

    static String d(BufferedReader bufferedReader, String str) throws IOException {
        String readLine = bufferedReader.readLine();
        if (readLine != null) {
            return b(readLine);
        }
        throw new IOException("Missing value: " + str);
    }

    static boolean a(String str) throws IOException {
        return Boolean.parseBoolean(b(str));
    }

    static boolean a(BufferedReader bufferedReader, String str) throws IOException {
        return Boolean.parseBoolean(d(bufferedReader, str));
    }
}
