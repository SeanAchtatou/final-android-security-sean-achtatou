package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.utils.o;
import e.d.b.g;
import e.d.b.t.l;
import e.d.b.t.q;

/* compiled from: GLOnlyTextureData */
public class e implements q {

    /* renamed from: a  reason: collision with root package name */
    int f5113a = 0;

    /* renamed from: b  reason: collision with root package name */
    int f5114b = 0;

    /* renamed from: c  reason: collision with root package name */
    boolean f5115c = false;

    /* renamed from: d  reason: collision with root package name */
    int f5116d = 0;

    /* renamed from: e  reason: collision with root package name */
    int f5117e;

    /* renamed from: f  reason: collision with root package name */
    int f5118f;

    /* renamed from: g  reason: collision with root package name */
    int f5119g;

    public e(int i2, int i3, int i4, int i5, int i6, int i7) {
        this.f5113a = i2;
        this.f5114b = i3;
        this.f5116d = i4;
        this.f5117e = i5;
        this.f5118f = i6;
        this.f5119g = i7;
    }

    public void a(int i2) {
        g.f6806g.b(i2, this.f5116d, this.f5117e, this.f5113a, this.f5114b, 0, this.f5118f, this.f5119g, null);
    }

    public boolean a() {
        return false;
    }

    public void b() {
        if (!this.f5115c) {
            this.f5115c = true;
            return;
        }
        throw new o("Already prepared");
    }

    public boolean c() {
        return this.f5115c;
    }

    public l d() {
        throw new o("This TextureData implementation does not return a Pixmap");
    }

    public boolean e() {
        return false;
    }

    public boolean f() {
        throw new o("This TextureData implementation does not return a Pixmap");
    }

    public l.c getFormat() {
        return l.c.RGBA8888;
    }

    public int getHeight() {
        return this.f5114b;
    }

    public q.b getType() {
        return q.b.Custom;
    }

    public int getWidth() {
        return this.f5113a;
    }
}
