package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.math.h;
import com.esotericsoftware.spine.Animation;
import e.d.b.t.b;
import e.d.b.t.n;

/* compiled from: NinePatch */
public class f {
    private static final b x = new b();

    /* renamed from: a  reason: collision with root package name */
    private n f4852a;

    /* renamed from: b  reason: collision with root package name */
    private int f4853b = -1;

    /* renamed from: c  reason: collision with root package name */
    private int f4854c = -1;

    /* renamed from: d  reason: collision with root package name */
    private int f4855d = -1;

    /* renamed from: e  reason: collision with root package name */
    private int f4856e = -1;

    /* renamed from: f  reason: collision with root package name */
    private int f4857f = -1;

    /* renamed from: g  reason: collision with root package name */
    private int f4858g = -1;

    /* renamed from: h  reason: collision with root package name */
    private int f4859h = -1;

    /* renamed from: i  reason: collision with root package name */
    private int f4860i = -1;

    /* renamed from: j  reason: collision with root package name */
    private int f4861j = -1;

    /* renamed from: k  reason: collision with root package name */
    private float f4862k;
    private float l;
    private float m;
    private float n;
    private float o;
    private float p;
    private float[] q = new float[180];
    private int r;
    private final b s = new b(b.f6927e);
    private float t = -1.0f;
    private float u = -1.0f;
    private float v = -1.0f;
    private float w = -1.0f;

    public f(r rVar, int i2, int i3, int i4, int i5) {
        if (rVar != null) {
            int b2 = (rVar.b() - i2) - i3;
            int a2 = (rVar.a() - i4) - i5;
            r[] rVarArr = new r[9];
            if (i4 > 0) {
                if (i2 > 0) {
                    rVarArr[0] = new r(rVar, 0, 0, i2, i4);
                }
                if (b2 > 0) {
                    rVarArr[1] = new r(rVar, i2, 0, b2, i4);
                }
                if (i3 > 0) {
                    rVarArr[2] = new r(rVar, i2 + b2, 0, i3, i4);
                }
            }
            if (a2 > 0) {
                if (i2 > 0) {
                    rVarArr[3] = new r(rVar, 0, i4, i2, a2);
                }
                if (b2 > 0) {
                    rVarArr[4] = new r(rVar, i2, i4, b2, a2);
                }
                if (i3 > 0) {
                    rVarArr[5] = new r(rVar, i2 + b2, i4, i3, a2);
                }
            }
            if (i5 > 0) {
                if (i2 > 0) {
                    rVarArr[6] = new r(rVar, 0, i4 + a2, i2, i5);
                }
                if (b2 > 0) {
                    rVarArr[7] = new r(rVar, i2, i4 + a2, b2, i5);
                }
                if (i3 > 0) {
                    rVarArr[8] = new r(rVar, i2 + b2, i4 + a2, i3, i5);
                }
            }
            if (i2 == 0 && b2 == 0) {
                rVarArr[1] = rVarArr[2];
                rVarArr[4] = rVarArr[5];
                rVarArr[7] = rVarArr[8];
                rVarArr[2] = null;
                rVarArr[5] = null;
                rVarArr[8] = null;
            }
            if (i4 == 0 && a2 == 0) {
                rVarArr[3] = rVarArr[6];
                rVarArr[4] = rVarArr[7];
                rVarArr[5] = rVarArr[8];
                rVarArr[6] = null;
                rVarArr[7] = null;
                rVarArr[8] = null;
            }
            a(rVarArr);
            return;
        }
        throw new IllegalArgumentException("region cannot be null.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.f.a(com.badlogic.gdx.graphics.g2d.r, float, boolean, boolean):int
     arg types: [com.badlogic.gdx.graphics.g2d.r, float, int, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.f.a(float, float, float, float):void
      com.badlogic.gdx.graphics.g2d.f.a(com.badlogic.gdx.graphics.g2d.r, float, boolean, boolean):int */
    private void a(r[] rVarArr) {
        float f2 = b.f6932j;
        if (rVarArr[6] != null) {
            this.f4853b = a(rVarArr[6], f2, false, false);
            this.f4862k = (float) rVarArr[6].b();
            this.p = (float) rVarArr[6].a();
        }
        if (rVarArr[7] != null) {
            this.f4854c = a(rVarArr[7], f2, true, false);
            this.m = Math.max(this.m, (float) rVarArr[7].b());
            this.p = Math.max(this.p, (float) rVarArr[7].a());
        }
        if (rVarArr[8] != null) {
            this.f4855d = a(rVarArr[8], f2, false, false);
            this.l = Math.max(this.l, (float) rVarArr[8].b());
            this.p = Math.max(this.p, (float) rVarArr[8].a());
        }
        if (rVarArr[3] != null) {
            this.f4856e = a(rVarArr[3], f2, false, true);
            this.f4862k = Math.max(this.f4862k, (float) rVarArr[3].b());
            this.n = Math.max(this.n, (float) rVarArr[3].a());
        }
        if (rVarArr[4] != null) {
            this.f4857f = a(rVarArr[4], f2, true, true);
            this.m = Math.max(this.m, (float) rVarArr[4].b());
            this.n = Math.max(this.n, (float) rVarArr[4].a());
        }
        if (rVarArr[5] != null) {
            this.f4858g = a(rVarArr[5], f2, false, true);
            this.l = Math.max(this.l, (float) rVarArr[5].b());
            this.n = Math.max(this.n, (float) rVarArr[5].a());
        }
        if (rVarArr[0] != null) {
            this.f4859h = a(rVarArr[0], f2, false, false);
            this.f4862k = Math.max(this.f4862k, (float) rVarArr[0].b());
            this.o = Math.max(this.o, (float) rVarArr[0].a());
        }
        if (rVarArr[1] != null) {
            this.f4860i = a(rVarArr[1], f2, true, false);
            this.m = Math.max(this.m, (float) rVarArr[1].b());
            this.o = Math.max(this.o, (float) rVarArr[1].a());
        }
        if (rVarArr[2] != null) {
            this.f4861j = a(rVarArr[2], f2, false, false);
            this.l = Math.max(this.l, (float) rVarArr[2].b());
            this.o = Math.max(this.o, (float) rVarArr[2].a());
        }
        int i2 = this.r;
        float[] fArr = this.q;
        if (i2 < fArr.length) {
            float[] fArr2 = new float[i2];
            System.arraycopy(fArr, 0, fArr2, 0, i2);
            this.q = fArr2;
        }
    }

    private void b(b bVar, float f2, float f3, float f4, float f5) {
        float f6 = f2 + this.f4862k;
        float f7 = f2 + f4;
        float f8 = f7 - this.l;
        float f9 = f3 + this.p;
        float f10 = f3 + f5;
        float f11 = f10 - this.o;
        b bVar2 = x;
        bVar2.b(this.s);
        bVar2.a(bVar.getColor());
        float b2 = bVar2.b();
        int i2 = this.f4853b;
        if (i2 != -1) {
            a(i2, f2, f3, f6 - f2, f9 - f3, b2);
        }
        int i3 = this.f4854c;
        if (i3 != -1) {
            a(i3, f6, f3, f8 - f6, f9 - f3, b2);
        }
        int i4 = this.f4855d;
        if (i4 != -1) {
            a(i4, f8, f3, f7 - f8, f9 - f3, b2);
        }
        int i5 = this.f4856e;
        if (i5 != -1) {
            a(i5, f2, f9, f6 - f2, f11 - f9, b2);
        }
        int i6 = this.f4857f;
        if (i6 != -1) {
            a(i6, f6, f9, f8 - f6, f11 - f9, b2);
        }
        int i7 = this.f4858g;
        if (i7 != -1) {
            a(i7, f8, f9, f7 - f8, f11 - f9, b2);
        }
        int i8 = this.f4859h;
        if (i8 != -1) {
            a(i8, f2, f11, f6 - f2, f10 - f11, b2);
        }
        int i9 = this.f4860i;
        if (i9 != -1) {
            a(i9, f6, f11, f8 - f6, f10 - f11, b2);
        }
        int i10 = this.f4861j;
        if (i10 != -1) {
            a(i10, f8, f11, f7 - f8, f10 - f11, b2);
        }
    }

    public float c() {
        float f2 = this.w;
        return f2 == -1.0f ? a() : f2;
    }

    public float d() {
        float f2 = this.t;
        return f2 == -1.0f ? b() : f2;
    }

    public float e() {
        float f2 = this.u;
        return f2 == -1.0f ? g() : f2;
    }

    public float f() {
        float f2 = this.v;
        return f2 == -1.0f ? h() : f2;
    }

    public float g() {
        return this.l;
    }

    public float h() {
        return this.o;
    }

    public float i() {
        return this.o + this.n + this.p;
    }

    public float j() {
        return this.f4862k + this.m + this.l;
    }

    public float b() {
        return this.f4862k;
    }

    public f(r rVar) {
        a(new r[]{null, null, null, null, rVar, null, null, null, null});
    }

    public f(f fVar, b bVar) {
        this.f4852a = fVar.f4852a;
        this.f4853b = fVar.f4853b;
        this.f4854c = fVar.f4854c;
        this.f4855d = fVar.f4855d;
        this.f4856e = fVar.f4856e;
        this.f4857f = fVar.f4857f;
        this.f4858g = fVar.f4858g;
        this.f4859h = fVar.f4859h;
        this.f4860i = fVar.f4860i;
        this.f4861j = fVar.f4861j;
        this.f4862k = fVar.f4862k;
        this.l = fVar.l;
        this.m = fVar.m;
        this.n = fVar.n;
        this.o = fVar.o;
        this.p = fVar.p;
        this.t = fVar.t;
        this.v = fVar.v;
        this.w = fVar.w;
        this.u = fVar.u;
        this.q = new float[fVar.q.length];
        float[] fArr = fVar.q;
        System.arraycopy(fArr, 0, this.q, 0, fArr.length);
        this.r = fVar.r;
        this.s.b(bVar);
    }

    private int a(r rVar, float f2, boolean z, boolean z2) {
        n nVar = this.f4852a;
        if (nVar == null) {
            this.f4852a = rVar.e();
        } else if (nVar != rVar.e()) {
            throw new IllegalArgumentException("All regions must be from the same texture.");
        }
        float f3 = rVar.f5061b;
        float f4 = rVar.f5064e;
        float f5 = rVar.f5063d;
        float f6 = rVar.f5062c;
        if (this.f4852a.k() == n.b.Linear || this.f4852a.l() == n.b.Linear) {
            if (z) {
                float r2 = 0.5f / ((float) this.f4852a.r());
                f3 += r2;
                f5 -= r2;
            }
            if (z2) {
                float p2 = 0.5f / ((float) this.f4852a.p());
                f4 -= p2;
                f6 += p2;
            }
        }
        float[] fArr = this.q;
        int i2 = this.r;
        fArr[i2 + 2] = f2;
        fArr[i2 + 3] = f3;
        fArr[i2 + 4] = f4;
        fArr[i2 + 7] = f2;
        fArr[i2 + 8] = f3;
        fArr[i2 + 9] = f6;
        fArr[i2 + 12] = f2;
        fArr[i2 + 13] = f5;
        fArr[i2 + 14] = f6;
        fArr[i2 + 17] = f2;
        fArr[i2 + 18] = f5;
        fArr[i2 + 19] = f4;
        this.r = i2 + 20;
        return this.r - 20;
    }

    private void a(int i2, float f2, float f3, float f4, float f5, float f6) {
        float f7 = f4 + f2;
        float f8 = f5 + f3;
        float[] fArr = this.q;
        fArr[i2] = f2;
        fArr[i2 + 1] = f3;
        fArr[i2 + 2] = f6;
        fArr[i2 + 5] = f2;
        fArr[i2 + 6] = f8;
        fArr[i2 + 7] = f6;
        fArr[i2 + 10] = f7;
        fArr[i2 + 11] = f8;
        fArr[i2 + 12] = f6;
        fArr[i2 + 15] = f7;
        fArr[i2 + 16] = f3;
        fArr[i2 + 17] = f6;
    }

    public void a(b bVar, float f2, float f3, float f4, float f5) {
        b(bVar, f2, f3, f4, f5);
        bVar.draw(this.f4852a, this.q, 0, this.r);
    }

    public void a(b bVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10) {
        b(bVar, f2, f3, f6, f7);
        float f11 = f2 + f4;
        float f12 = f3 + f5;
        int i2 = this.r;
        float[] fArr = this.q;
        if (f10 != Animation.CurveTimeline.LINEAR) {
            for (int i3 = 0; i3 < i2; i3 += 5) {
                float f13 = (fArr[i3] - f11) * f8;
                int i4 = i3 + 1;
                float f14 = (fArr[i4] - f12) * f9;
                float b2 = h.b(f10);
                float h2 = h.h(f10);
                fArr[i3] = ((b2 * f13) - (h2 * f14)) + f11;
                fArr[i4] = (h2 * f13) + (b2 * f14) + f12;
            }
        } else if (!(f8 == 1.0f && f9 == 1.0f)) {
            for (int i5 = 0; i5 < i2; i5 += 5) {
                fArr[i5] = ((fArr[i5] - f11) * f8) + f11;
                int i6 = i5 + 1;
                fArr[i6] = ((fArr[i6] - f12) * f9) + f12;
            }
        }
        bVar.draw(this.f4852a, fArr, 0, i2);
    }

    public void a(b bVar) {
        this.s.b(bVar);
    }

    public float a() {
        return this.p;
    }

    public void a(float f2, float f3, float f4, float f5) {
        this.t = f2;
        this.u = f3;
        this.v = f4;
        this.w = f5;
    }

    public void a(float f2, float f3) {
        this.f4862k *= f2;
        this.l *= f2;
        this.o *= f3;
        this.p *= f3;
        this.m *= f2;
        this.n *= f3;
        float f4 = this.t;
        if (f4 != -1.0f) {
            this.t = f4 * f2;
        }
        float f5 = this.u;
        if (f5 != -1.0f) {
            this.u = f5 * f2;
        }
        float f6 = this.v;
        if (f6 != -1.0f) {
            this.v = f6 * f3;
        }
        float f7 = this.w;
        if (f7 != -1.0f) {
            this.w = f7 * f3;
        }
    }
}
