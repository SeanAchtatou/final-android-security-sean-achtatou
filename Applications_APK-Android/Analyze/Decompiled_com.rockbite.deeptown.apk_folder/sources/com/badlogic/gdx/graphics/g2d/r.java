package com.badlogic.gdx.graphics.g2d;

import e.d.b.t.n;

/* compiled from: TextureRegion */
public class r {

    /* renamed from: a  reason: collision with root package name */
    n f5060a;

    /* renamed from: b  reason: collision with root package name */
    float f5061b;

    /* renamed from: c  reason: collision with root package name */
    float f5062c;

    /* renamed from: d  reason: collision with root package name */
    float f5063d;

    /* renamed from: e  reason: collision with root package name */
    float f5064e;

    /* renamed from: f  reason: collision with root package name */
    int f5065f;

    /* renamed from: g  reason: collision with root package name */
    int f5066g;

    public r() {
    }

    public void a(int i2, int i3, int i4, int i5) {
        float r = 1.0f / ((float) this.f5060a.r());
        float p = 1.0f / ((float) this.f5060a.p());
        a(((float) i2) * r, ((float) i3) * p, ((float) (i2 + i4)) * r, ((float) (i3 + i5)) * p);
        this.f5065f = Math.abs(i4);
        this.f5066g = Math.abs(i5);
    }

    public void b(float f2) {
        this.f5063d = f2;
        this.f5065f = Math.round(Math.abs(f2 - this.f5061b) * ((float) this.f5060a.r()));
    }

    public void c(float f2) {
        this.f5062c = f2;
        this.f5066g = Math.round(Math.abs(this.f5064e - f2) * ((float) this.f5060a.p()));
    }

    public void d(float f2) {
        this.f5064e = f2;
        this.f5066g = Math.round(Math.abs(f2 - this.f5062c) * ((float) this.f5060a.p()));
    }

    public n e() {
        return this.f5060a;
    }

    public float f() {
        return this.f5061b;
    }

    public float g() {
        return this.f5063d;
    }

    public float h() {
        return this.f5062c;
    }

    public float i() {
        return this.f5064e;
    }

    public boolean j() {
        return this.f5061b > this.f5063d;
    }

    public boolean k() {
        return this.f5062c > this.f5064e;
    }

    public r(n nVar) {
        if (nVar != null) {
            this.f5060a = nVar;
            a(0, 0, nVar.r(), nVar.p());
            return;
        }
        throw new IllegalArgumentException("texture cannot be null.");
    }

    public int b() {
        return this.f5065f;
    }

    public int c() {
        return Math.round(this.f5061b * ((float) this.f5060a.r()));
    }

    public int d() {
        return Math.round(this.f5062c * ((float) this.f5060a.p()));
    }

    public void b(int i2) {
        if (j()) {
            a(this.f5063d + (((float) i2) / ((float) this.f5060a.r())));
        } else {
            b(this.f5061b + (((float) i2) / ((float) this.f5060a.r())));
        }
    }

    public r(n nVar, int i2, int i3) {
        this.f5060a = nVar;
        a(0, 0, i2, i3);
    }

    public void a(float f2, float f3, float f4, float f5) {
        int r = this.f5060a.r();
        int p = this.f5060a.p();
        float f6 = (float) r;
        this.f5065f = Math.round(Math.abs(f4 - f2) * f6);
        float f7 = (float) p;
        this.f5066g = Math.round(Math.abs(f5 - f3) * f7);
        if (this.f5065f == 1 && this.f5066g == 1) {
            float f8 = 0.25f / f6;
            f2 += f8;
            f4 -= f8;
            float f9 = 0.25f / f7;
            f3 += f9;
            f5 -= f9;
        }
        this.f5061b = f2;
        this.f5062c = f3;
        this.f5063d = f4;
        this.f5064e = f5;
    }

    public r(n nVar, int i2, int i3, int i4, int i5) {
        this.f5060a = nVar;
        a(i2, i3, i4, i5);
    }

    public r(r rVar) {
        a(rVar);
    }

    public r(r rVar, int i2, int i3, int i4, int i5) {
        a(rVar, i2, i3, i4, i5);
    }

    public void a(r rVar) {
        this.f5060a = rVar.f5060a;
        a(rVar.f5061b, rVar.f5062c, rVar.f5063d, rVar.f5064e);
    }

    public void a(r rVar, int i2, int i3, int i4, int i5) {
        this.f5060a = rVar.f5060a;
        a(rVar.c() + i2, rVar.d() + i3, i4, i5);
    }

    public void a(float f2) {
        this.f5061b = f2;
        this.f5065f = Math.round(Math.abs(this.f5063d - f2) * ((float) this.f5060a.r()));
    }

    public int a() {
        return this.f5066g;
    }

    public void a(int i2) {
        if (k()) {
            c(this.f5064e + (((float) i2) / ((float) this.f5060a.p())));
        } else {
            d(this.f5062c + (((float) i2) / ((float) this.f5060a.p())));
        }
    }

    public void a(boolean z, boolean z2) {
        if (z) {
            float f2 = this.f5061b;
            this.f5061b = this.f5063d;
            this.f5063d = f2;
        }
        if (z2) {
            float f3 = this.f5062c;
            this.f5062c = this.f5064e;
            this.f5064e = f3;
        }
    }
}
