package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.c0;
import com.badlogic.gdx.utils.l;
import com.badlogic.gdx.utils.o;
import com.badlogic.gdx.utils.q0;
import com.google.android.gms.ads.AdRequest;
import e.d.b.t.n;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

/* compiled from: ParticleEffect */
public class g implements l {

    /* renamed from: a  reason: collision with root package name */
    private final a<h> f4905a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f4906b;

    /* renamed from: c  reason: collision with root package name */
    protected float f4907c;

    /* renamed from: d  reason: collision with root package name */
    protected float f4908d;

    /* renamed from: e  reason: collision with root package name */
    protected float f4909e;

    public g() {
        this.f4907c = 1.0f;
        this.f4908d = 1.0f;
        this.f4909e = 1.0f;
        this.f4905a = new a<>(8);
    }

    public void a(b bVar) {
        int i2 = this.f4905a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            this.f4905a.get(i3).a(bVar);
        }
    }

    public void b(boolean z) {
        int i2 = this.f4905a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            this.f4905a.get(i3).v();
        }
        if (!z) {
            return;
        }
        if (this.f4907c != 1.0f || this.f4908d != 1.0f || this.f4909e != 1.0f) {
            c(1.0f / this.f4907c, 1.0f / this.f4908d, 1.0f / this.f4909e);
            this.f4909e = 1.0f;
            this.f4908d = 1.0f;
            this.f4907c = 1.0f;
        }
    }

    /* access modifiers changed from: protected */
    public n c(e.d.b.s.a aVar) {
        return new n(aVar, false);
    }

    public void dispose() {
        if (this.f4906b) {
            int i2 = this.f4905a.f5374b;
            for (int i3 = 0; i3 < i2; i3++) {
                Iterator<o> it = this.f4905a.get(i3).l().iterator();
                while (it.hasNext()) {
                    it.next().e().dispose();
                }
            }
        }
    }

    public void j() {
        int i2 = this.f4905a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            this.f4905a.get(i3).b();
        }
    }

    public a<h> k() {
        return this.f4905a;
    }

    public void l() {
        b(true);
    }

    public void m() {
        int i2 = this.f4905a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            this.f4905a.get(i3).w();
        }
    }

    public void c(float f2, float f3, float f4) {
        this.f4907c *= f2;
        this.f4908d *= f3;
        this.f4909e *= f4;
        Iterator<h> it = this.f4905a.iterator();
        while (it.hasNext()) {
            h next = it.next();
            next.a(f2, f3);
            next.a(f4);
        }
    }

    public void a(float f2, float f3) {
        int i2 = this.f4905a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            this.f4905a.get(i3).b(f2, f3);
        }
    }

    public h a(String str) {
        int i2 = this.f4905a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            h hVar = this.f4905a.get(i3);
            if (hVar.j().equals(str)) {
                return hVar;
            }
        }
        return null;
    }

    public g(g gVar) {
        this.f4907c = 1.0f;
        this.f4908d = 1.0f;
        this.f4909e = 1.0f;
        this.f4905a = new a<>(true, gVar.f4905a.f5374b);
        int i2 = gVar.f4905a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            this.f4905a.add(a(gVar.f4905a.get(i3)));
        }
    }

    public void b(float f2) {
        int i2 = this.f4905a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            this.f4905a.get(i3).b(f2);
        }
    }

    public void a(e.d.b.s.a aVar, e.d.b.s.a aVar2) {
        b(aVar);
        a(aVar2);
    }

    public void b(e.d.b.s.a aVar) {
        InputStream l = aVar.l();
        this.f4905a.clear();
        BufferedReader bufferedReader = null;
        try {
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(l), AdRequest.MAX_CONTENT_URL_LENGTH);
            do {
                try {
                    this.f4905a.add(a(bufferedReader2));
                } catch (IOException e2) {
                    e = e2;
                    bufferedReader = bufferedReader2;
                    try {
                        throw new o("Error loading effect: " + aVar, e);
                    } catch (Throwable th) {
                        th = th;
                        q0.a(bufferedReader);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bufferedReader = bufferedReader2;
                    q0.a(bufferedReader);
                    throw th;
                }
            } while (bufferedReader2.readLine() != null);
            q0.a(bufferedReader2);
        } catch (IOException e3) {
            e = e3;
            throw new o("Error loading effect: " + aVar, e);
        }
    }

    public void c(boolean z) {
        int i2 = this.f4905a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            this.f4905a.get(i3).a(z);
        }
    }

    public void a(e.d.b.s.a aVar, q qVar, String str) {
        b(aVar);
        a(qVar, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public void a(q qVar, String str) {
        int i2 = this.f4905a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            h hVar = this.f4905a.get(i3);
            if (hVar.g().f5374b != 0) {
                a aVar = new a();
                Iterator<String> it = hVar.g().iterator();
                while (it.hasNext()) {
                    String name = new File(it.next().replace('\\', '/')).getName();
                    int lastIndexOf = name.lastIndexOf(46);
                    if (lastIndexOf != -1) {
                        name = name.substring(0, lastIndexOf);
                    }
                    if (str != null) {
                        name = str + name;
                    }
                    o a2 = qVar.a(name);
                    if (a2 != null) {
                        aVar.add(a2);
                    } else {
                        throw new IllegalArgumentException("SpriteSheet missing image: " + name);
                    }
                }
                hVar.b(aVar);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public void a(e.d.b.s.a aVar) {
        this.f4906b = true;
        c0 c0Var = new c0(this.f4905a.f5374b);
        int i2 = this.f4905a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            h hVar = this.f4905a.get(i3);
            if (hVar.g().f5374b != 0) {
                a aVar2 = new a();
                Iterator<String> it = hVar.g().iterator();
                while (it.hasNext()) {
                    String name = new File(it.next().replace('\\', '/')).getName();
                    o oVar = (o) c0Var.b(name);
                    if (oVar == null) {
                        oVar = new o(c(aVar.a(name)));
                        c0Var.b(name, oVar);
                    }
                    aVar2.add(oVar);
                }
                hVar.b(aVar2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public h a(BufferedReader bufferedReader) throws IOException {
        return new h(bufferedReader);
    }

    /* access modifiers changed from: protected */
    public h a(h hVar) {
        return new h(hVar);
    }

    public void a(float f2) {
        c(f2, f2, f2);
    }
}
