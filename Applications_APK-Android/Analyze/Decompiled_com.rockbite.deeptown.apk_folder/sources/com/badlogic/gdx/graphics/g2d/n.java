package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.glutils.s;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.h;
import com.esotericsoftware.spine.Animation;
import e.d.b.g;
import e.d.b.t.b;
import e.d.b.t.j;
import e.d.b.t.r;

/* compiled from: PolygonSpriteBatch */
public class n implements j {

    /* renamed from: a  reason: collision with root package name */
    private j f5005a;

    /* renamed from: b  reason: collision with root package name */
    private final float[] f5006b;

    /* renamed from: c  reason: collision with root package name */
    private final short[] f5007c;

    /* renamed from: d  reason: collision with root package name */
    private int f5008d;

    /* renamed from: e  reason: collision with root package name */
    private int f5009e;

    /* renamed from: f  reason: collision with root package name */
    private e.d.b.t.n f5010f;

    /* renamed from: g  reason: collision with root package name */
    private float f5011g;

    /* renamed from: h  reason: collision with root package name */
    private float f5012h;

    /* renamed from: i  reason: collision with root package name */
    private boolean f5013i;

    /* renamed from: j  reason: collision with root package name */
    private final Matrix4 f5014j;

    /* renamed from: k  reason: collision with root package name */
    private final Matrix4 f5015k;
    private final Matrix4 l;
    private boolean m;
    private int n;
    private int o;
    private int p;
    private int q;
    private final s r;
    private s s;
    private boolean t;
    private final b u;
    float v;
    public int w;
    public int x;
    public int y;

    public n() {
        this(2000, null);
    }

    private void c() {
        Matrix4 matrix4 = this.l;
        matrix4.b(this.f5015k);
        matrix4.a(this.f5014j);
        s sVar = this.s;
        if (sVar != null) {
            sVar.a("u_projTrans", this.l);
            this.s.a("u_texture", 0);
            return;
        }
        this.r.a("u_projTrans", this.l);
        this.r.a("u_texture", 0);
    }

    public void a(e.d.b.t.n nVar, float[] fArr, int i2, int i3, short[] sArr, int i4, int i5) {
        if (this.f5013i) {
            short[] sArr2 = this.f5007c;
            float[] fArr2 = this.f5006b;
            if (nVar != this.f5010f) {
                a(nVar);
            } else if (this.f5009e + i5 > sArr2.length || this.f5008d + i3 > fArr2.length) {
                flush();
            }
            int i6 = this.f5009e;
            int i7 = this.f5008d;
            int i8 = i7 / 5;
            int i9 = i5 + i4;
            while (i4 < i9) {
                sArr2[i6] = (short) (sArr[i4] + i8);
                i4++;
                i6++;
            }
            this.f5009e = i6;
            System.arraycopy(fArr, i2, fArr2, i7, i3);
            this.f5008d += i3;
            return;
        }
        throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
    }

    public boolean b() {
        return !this.m;
    }

    public void begin() {
        if (!this.f5013i) {
            this.w = 0;
            g.f6806g.a(false);
            s sVar = this.s;
            if (sVar != null) {
                sVar.begin();
            } else {
                this.r.begin();
            }
            c();
            this.f5013i = true;
            return;
        }
        throw new IllegalStateException("PolygonSpriteBatch.end must be called before begin.");
    }

    public void dispose() {
        s sVar;
        this.f5005a.dispose();
        if (this.t && (sVar = this.r) != null) {
            sVar.dispose();
        }
    }

    public void draw(e.d.b.t.n nVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, int i2, int i3, int i4, int i5, boolean z, boolean z2) {
        float f11;
        float f12;
        float f13;
        float f14;
        float f15 = f4;
        float f16 = f5;
        int i6 = i2;
        int i7 = i3;
        if (this.f5013i) {
            short[] sArr = this.f5007c;
            float[] fArr = this.f5006b;
            if (nVar != this.f5010f) {
                a(nVar);
            } else if (this.f5009e + 6 > sArr.length || this.f5008d + 20 > fArr.length) {
                flush();
            }
            int i8 = this.f5009e;
            int i9 = this.f5008d / 5;
            int i10 = i8 + 1;
            short s2 = (short) i9;
            sArr[i8] = s2;
            int i11 = i10 + 1;
            sArr[i10] = (short) (i9 + 1);
            int i12 = i11 + 1;
            short s3 = (short) (i9 + 2);
            sArr[i11] = s3;
            int i13 = i12 + 1;
            sArr[i12] = s3;
            int i14 = i13 + 1;
            sArr[i13] = (short) (i9 + 3);
            sArr[i14] = s2;
            this.f5009e = i14 + 1;
            float f17 = f2 + f15;
            float f18 = f3 + f16;
            float f19 = -f15;
            float f20 = -f16;
            float f21 = f6 - f15;
            float f22 = f7 - f16;
            if (!(f8 == 1.0f && f9 == 1.0f)) {
                f19 *= f8;
                f20 *= f9;
                f21 *= f8;
                f22 *= f9;
            }
            if (f10 != Animation.CurveTimeline.LINEAR) {
                float b2 = h.b(f10);
                float h2 = h.h(f10);
                float f23 = b2 * f19;
                float f24 = f23 - (h2 * f20);
                float f25 = f19 * h2;
                f20 = (f20 * b2) + f25;
                float f26 = h2 * f22;
                f12 = f23 - f26;
                float f27 = f22 * b2;
                float f28 = f25 + f27;
                float f29 = (b2 * f21) - f26;
                float f30 = f27 + (h2 * f21);
                float f31 = f30 - (f28 - f20);
                f13 = (f29 - f12) + f24;
                f21 = f29;
                f14 = f30;
                f22 = f28;
                f19 = f24;
                f11 = f31;
            } else {
                f13 = f21;
                f14 = f22;
                f12 = f19;
                f11 = f20;
            }
            float f32 = f19 + f17;
            float f33 = f20 + f18;
            float f34 = f12 + f17;
            float f35 = f22 + f18;
            float f36 = f21 + f17;
            float f37 = f14 + f18;
            float f38 = f13 + f17;
            float f39 = f11 + f18;
            float f40 = this.f5011g;
            float f41 = ((float) i6) * f40;
            float f42 = this.f5012h;
            float f43 = ((float) (i7 + i5)) * f42;
            float f44 = ((float) (i6 + i4)) * f40;
            float f45 = ((float) i7) * f42;
            if (!z) {
                float f46 = f41;
                f41 = f44;
                f44 = f46;
            }
            if (!z2) {
                float f47 = f43;
                f43 = f45;
                f45 = f47;
            }
            float f48 = this.v;
            int i15 = this.f5008d;
            int i16 = i15 + 1;
            fArr[i15] = f32;
            int i17 = i16 + 1;
            fArr[i16] = f33;
            int i18 = i17 + 1;
            fArr[i17] = f48;
            int i19 = i18 + 1;
            fArr[i18] = f44;
            int i20 = i19 + 1;
            fArr[i19] = f45;
            int i21 = i20 + 1;
            fArr[i20] = f34;
            int i22 = i21 + 1;
            fArr[i21] = f35;
            int i23 = i22 + 1;
            fArr[i22] = f48;
            int i24 = i23 + 1;
            fArr[i23] = f44;
            int i25 = i24 + 1;
            fArr[i24] = f43;
            int i26 = i25 + 1;
            fArr[i25] = f36;
            int i27 = i26 + 1;
            fArr[i26] = f37;
            int i28 = i27 + 1;
            fArr[i27] = f48;
            int i29 = i28 + 1;
            fArr[i28] = f41;
            int i30 = i29 + 1;
            fArr[i29] = f43;
            int i31 = i30 + 1;
            fArr[i30] = f38;
            int i32 = i31 + 1;
            fArr[i31] = f39;
            int i33 = i32 + 1;
            fArr[i32] = f48;
            int i34 = i33 + 1;
            fArr[i33] = f41;
            fArr[i34] = f45;
            this.f5008d = i34 + 1;
            return;
        }
        throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
    }

    public void end() {
        if (this.f5013i) {
            if (this.f5008d > 0) {
                flush();
            }
            this.f5010f = null;
            this.f5013i = false;
            e.d.b.t.g gVar = g.f6806g;
            gVar.a(true);
            if (b()) {
                gVar.k(3042);
            }
            s sVar = this.s;
            if (sVar != null) {
                sVar.end();
            } else {
                this.r.end();
            }
        } else {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before end.");
        }
    }

    public void flush() {
        if (this.f5008d != 0) {
            this.w++;
            this.x++;
            int i2 = this.f5009e;
            if (i2 > this.y) {
                this.y = i2;
            }
            this.f5010f.f();
            j jVar = this.f5005a;
            jVar.a(this.f5006b, 0, this.f5008d);
            jVar.a(this.f5007c, 0, i2);
            if (this.m) {
                g.f6806g.k(3042);
            } else {
                g.f6806g.a(3042);
                int i3 = this.n;
                if (i3 != -1) {
                    g.f6806g.e(i3, this.o, this.p, this.q);
                }
            }
            s sVar = this.s;
            if (sVar == null) {
                sVar = this.r;
            }
            jVar.a(sVar, 4, 0, i2);
            this.f5008d = 0;
            this.f5009e = 0;
        }
    }

    public int getBlendDstFunc() {
        return this.o;
    }

    public int getBlendDstFuncAlpha() {
        return this.q;
    }

    public int getBlendSrcFunc() {
        return this.n;
    }

    public int getBlendSrcFuncAlpha() {
        return this.p;
    }

    public b getColor() {
        return this.u;
    }

    public s getShader() {
        s sVar = this.s;
        return sVar == null ? this.r : sVar;
    }

    public Matrix4 getTransformMatrix() {
        return this.f5014j;
    }

    public void setBlendFunction(int i2, int i3) {
        setBlendFunctionSeparate(i2, i3, i2, i3);
    }

    public void setBlendFunctionSeparate(int i2, int i3, int i4, int i5) {
        if (this.n != i2 || this.o != i3 || this.p != i4 || this.q != i5) {
            flush();
            this.n = i2;
            this.o = i3;
            this.p = i4;
            this.q = i5;
        }
    }

    public void setColor(b bVar) {
        this.u.b(bVar);
        this.v = bVar.b();
    }

    public void setProjectionMatrix(Matrix4 matrix4) {
        if (this.f5013i) {
            flush();
        }
        this.f5015k.b(matrix4);
        if (this.f5013i) {
            c();
        }
    }

    public void setShader(s sVar) {
        if (this.f5013i) {
            flush();
            s sVar2 = this.s;
            if (sVar2 != null) {
                sVar2.end();
            } else {
                this.r.end();
            }
        }
        this.s = sVar;
        if (this.f5013i) {
            s sVar3 = this.s;
            if (sVar3 != null) {
                sVar3.begin();
            } else {
                this.r.begin();
            }
            c();
        }
    }

    public void setTransformMatrix(Matrix4 matrix4) {
        if (this.f5013i) {
            flush();
        }
        this.f5014j.b(matrix4);
        if (this.f5013i) {
            c();
        }
    }

    public n(int i2) {
        this(i2, i2 * 2, null);
    }

    public n(int i2, s sVar) {
        this(i2, i2 * 2, sVar);
    }

    public void setColor(float f2, float f3, float f4, float f5) {
        this.u.b(f2, f3, f4, f5);
        this.v = this.u.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.t.j.<init>(e.d.b.t.j$b, boolean, int, int, e.d.b.t.r[]):void
     arg types: [e.d.b.t.j$b, int, int, int, e.d.b.t.r[]]
     candidates:
      e.d.b.t.j.<init>(e.d.b.t.j$b, boolean, int, int, e.d.b.t.s):void
      e.d.b.t.j.<init>(e.d.b.t.j$b, boolean, int, int, e.d.b.t.r[]):void */
    public n(int i2, int i3, s sVar) {
        this.f5011g = Animation.CurveTimeline.LINEAR;
        this.f5012h = Animation.CurveTimeline.LINEAR;
        this.f5014j = new Matrix4();
        this.f5015k = new Matrix4();
        this.l = new Matrix4();
        this.n = 770;
        this.o = 771;
        this.p = 770;
        this.q = 771;
        this.u = new b(1.0f, 1.0f, 1.0f, 1.0f);
        this.v = b.f6932j;
        this.w = 0;
        this.x = 0;
        this.y = 0;
        if (i2 <= 32767) {
            int i4 = i3 * 3;
            this.f5005a = new j(g.f6808i != null ? j.b.VertexBufferObjectWithVAO : j.b.VertexArray, false, i2, i4, new r(1, 2, "a_position"), new r(4, 4, "a_color"), new r(16, 2, "a_texCoord0"));
            this.f5006b = new float[(i2 * 5)];
            this.f5007c = new short[i4];
            if (sVar == null) {
                this.r = p.b();
                this.t = true;
            } else {
                this.r = sVar;
            }
            this.f5015k.a(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) g.f6801b.getWidth(), (float) g.f6801b.getHeight());
            return;
        }
        throw new IllegalArgumentException("Can't have more than 32767 vertices per batch: " + i2);
    }

    public void a(r rVar, float f2, float f3) {
        draw(rVar, f2, f3, (float) rVar.b(), (float) rVar.a());
    }

    public void a() {
        flush();
        this.m = false;
    }

    private void a(e.d.b.t.n nVar) {
        flush();
        this.f5010f = nVar;
        this.f5011g = 1.0f / ((float) nVar.r());
        this.f5012h = 1.0f / ((float) nVar.p());
    }

    public void draw(e.d.b.t.n nVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
        if (this.f5013i) {
            short[] sArr = this.f5007c;
            float[] fArr = this.f5006b;
            if (nVar != this.f5010f) {
                a(nVar);
            } else if (this.f5009e + 6 > sArr.length || this.f5008d + 20 > fArr.length) {
                flush();
            }
            int i2 = this.f5009e;
            int i3 = this.f5008d;
            int i4 = i3 / 5;
            int i5 = i2 + 1;
            short s2 = (short) i4;
            sArr[i2] = s2;
            int i6 = i5 + 1;
            sArr[i5] = (short) (i4 + 1);
            int i7 = i6 + 1;
            short s3 = (short) (i4 + 2);
            sArr[i6] = s3;
            int i8 = i7 + 1;
            sArr[i7] = s3;
            int i9 = i8 + 1;
            sArr[i8] = (short) (i4 + 3);
            sArr[i9] = s2;
            this.f5009e = i9 + 1;
            float f10 = f2 + f4;
            float f11 = f3 + f5;
            float f12 = this.v;
            int i10 = i3 + 1;
            fArr[i3] = f2;
            int i11 = i10 + 1;
            fArr[i10] = f3;
            int i12 = i11 + 1;
            fArr[i11] = f12;
            int i13 = i12 + 1;
            fArr[i12] = f6;
            int i14 = i13 + 1;
            fArr[i13] = f7;
            int i15 = i14 + 1;
            fArr[i14] = f2;
            int i16 = i15 + 1;
            fArr[i15] = f11;
            int i17 = i16 + 1;
            fArr[i16] = f12;
            int i18 = i17 + 1;
            fArr[i17] = f6;
            int i19 = i18 + 1;
            fArr[i18] = f9;
            int i20 = i19 + 1;
            fArr[i19] = f10;
            int i21 = i20 + 1;
            fArr[i20] = f11;
            int i22 = i21 + 1;
            fArr[i21] = f12;
            int i23 = i22 + 1;
            fArr[i22] = f8;
            int i24 = i23 + 1;
            fArr[i23] = f9;
            int i25 = i24 + 1;
            fArr[i24] = f10;
            int i26 = i25 + 1;
            fArr[i25] = f3;
            int i27 = i26 + 1;
            fArr[i26] = f12;
            int i28 = i27 + 1;
            fArr[i27] = f8;
            fArr[i28] = f7;
            this.f5008d = i28 + 1;
            return;
        }
        throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
    }

    public void draw(e.d.b.t.n nVar, float f2, float f3, float f4, float f5) {
        if (this.f5013i) {
            short[] sArr = this.f5007c;
            float[] fArr = this.f5006b;
            if (nVar != this.f5010f) {
                a(nVar);
            } else if (this.f5009e + 6 > sArr.length || this.f5008d + 20 > fArr.length) {
                flush();
            }
            int i2 = this.f5009e;
            int i3 = this.f5008d;
            int i4 = i3 / 5;
            int i5 = i2 + 1;
            short s2 = (short) i4;
            sArr[i2] = s2;
            int i6 = i5 + 1;
            sArr[i5] = (short) (i4 + 1);
            int i7 = i6 + 1;
            short s3 = (short) (i4 + 2);
            sArr[i6] = s3;
            int i8 = i7 + 1;
            sArr[i7] = s3;
            int i9 = i8 + 1;
            sArr[i8] = (short) (i4 + 3);
            sArr[i9] = s2;
            this.f5009e = i9 + 1;
            float f6 = f4 + f2;
            float f7 = f5 + f3;
            float f8 = this.v;
            int i10 = i3 + 1;
            fArr[i3] = f2;
            int i11 = i10 + 1;
            fArr[i10] = f3;
            int i12 = i11 + 1;
            fArr[i11] = f8;
            int i13 = i12 + 1;
            fArr[i12] = 0.0f;
            int i14 = i13 + 1;
            fArr[i13] = 1.0f;
            int i15 = i14 + 1;
            fArr[i14] = f2;
            int i16 = i15 + 1;
            fArr[i15] = f7;
            int i17 = i16 + 1;
            fArr[i16] = f8;
            int i18 = i17 + 1;
            fArr[i17] = 0.0f;
            int i19 = i18 + 1;
            fArr[i18] = 0.0f;
            int i20 = i19 + 1;
            fArr[i19] = f6;
            int i21 = i20 + 1;
            fArr[i20] = f7;
            int i22 = i21 + 1;
            fArr[i21] = f8;
            int i23 = i22 + 1;
            fArr[i22] = 1.0f;
            int i24 = i23 + 1;
            fArr[i23] = 0.0f;
            int i25 = i24 + 1;
            fArr[i24] = f6;
            int i26 = i25 + 1;
            fArr[i25] = f3;
            int i27 = i26 + 1;
            fArr[i26] = f8;
            int i28 = i27 + 1;
            fArr[i27] = 1.0f;
            fArr[i28] = 1.0f;
            this.f5008d = i28 + 1;
            return;
        }
        throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x005b A[LOOP:0: B:13:0x0059->B:14:0x005b, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(e.d.b.t.n r9, float[] r10, int r11, int r12) {
        /*
            r8 = this;
            boolean r0 = r8.f5013i
            if (r0 == 0) goto L_0x00a1
            short[] r0 = r8.f5007c
            float[] r1 = r8.f5006b
            int r2 = r12 / 20
            int r2 = r2 * 6
            e.d.b.t.n r3 = r8.f5010f
            if (r9 == r3) goto L_0x002a
            r8.a(r9)
            int r9 = r1.length
            int r2 = r1.length
            int r2 = r2 % 20
            int r9 = r9 - r2
            int r9 = java.lang.Math.min(r12, r9)
            int r2 = r0.length
            int r2 = r2 / 6
            int r2 = r2 * 20
            int r9 = java.lang.Math.min(r9, r2)
            int r2 = r9 / 20
        L_0x0027:
            int r2 = r2 * 6
            goto L_0x0051
        L_0x002a:
            int r9 = r8.f5009e
            int r9 = r9 + r2
            int r3 = r0.length
            if (r9 > r3) goto L_0x0039
            int r9 = r8.f5008d
            int r9 = r9 + r12
            int r3 = r1.length
            if (r9 <= r3) goto L_0x0037
            goto L_0x0039
        L_0x0037:
            r9 = r12
            goto L_0x0051
        L_0x0039:
            r8.flush()
            int r9 = r1.length
            int r2 = r1.length
            int r2 = r2 % 20
            int r9 = r9 - r2
            int r9 = java.lang.Math.min(r12, r9)
            int r2 = r0.length
            int r2 = r2 / 6
            int r2 = r2 * 20
            int r9 = java.lang.Math.min(r9, r2)
            int r2 = r9 / 20
            goto L_0x0027
        L_0x0051:
            int r3 = r8.f5008d
            int r4 = r3 / 5
            short r4 = (short) r4
            int r5 = r8.f5009e
            int r2 = r2 + r5
        L_0x0059:
            if (r5 >= r2) goto L_0x0080
            r0[r5] = r4
            int r6 = r5 + 1
            int r7 = r4 + 1
            short r7 = (short) r7
            r0[r6] = r7
            int r6 = r5 + 2
            int r7 = r4 + 2
            short r7 = (short) r7
            r0[r6] = r7
            int r6 = r5 + 3
            r0[r6] = r7
            int r6 = r5 + 4
            int r7 = r4 + 3
            short r7 = (short) r7
            r0[r6] = r7
            int r6 = r5 + 5
            r0[r6] = r4
            int r5 = r5 + 6
            int r4 = r4 + 4
            short r4 = (short) r4
            goto L_0x0059
        L_0x0080:
            java.lang.System.arraycopy(r10, r11, r1, r3, r9)
            int r3 = r3 + r9
            r8.f5008d = r3
            r8.f5009e = r5
            int r12 = r12 - r9
            if (r12 != 0) goto L_0x008c
            return
        L_0x008c:
            int r11 = r11 + r9
            r8.flush()
            r3 = 0
            if (r9 <= r12) goto L_0x0080
            int r9 = r0.length
            int r9 = r9 / 6
            int r9 = r9 * 20
            int r9 = java.lang.Math.min(r12, r9)
            int r2 = r9 / 20
            int r5 = r2 * 6
            goto L_0x0080
        L_0x00a1:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "PolygonSpriteBatch.begin must be called before draw."
            r9.<init>(r10)
            goto L_0x00aa
        L_0x00a9:
            throw r9
        L_0x00aa:
            goto L_0x00a9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.n.draw(e.d.b.t.n, float[], int, int):void");
    }

    public void draw(r rVar, float f2, float f3, float f4, float f5) {
        if (this.f5013i) {
            short[] sArr = this.f5007c;
            float[] fArr = this.f5006b;
            e.d.b.t.n nVar = rVar.f5060a;
            if (nVar != this.f5010f) {
                a(nVar);
            } else if (this.f5009e + 6 > sArr.length || this.f5008d + 20 > fArr.length) {
                flush();
            }
            int i2 = this.f5009e;
            int i3 = this.f5008d;
            int i4 = i3 / 5;
            int i5 = i2 + 1;
            short s2 = (short) i4;
            sArr[i2] = s2;
            int i6 = i5 + 1;
            sArr[i5] = (short) (i4 + 1);
            int i7 = i6 + 1;
            short s3 = (short) (i4 + 2);
            sArr[i6] = s3;
            int i8 = i7 + 1;
            sArr[i7] = s3;
            int i9 = i8 + 1;
            sArr[i8] = (short) (i4 + 3);
            sArr[i9] = s2;
            this.f5009e = i9 + 1;
            float f6 = f4 + f2;
            float f7 = f5 + f3;
            float f8 = rVar.f5061b;
            float f9 = rVar.f5064e;
            float f10 = rVar.f5063d;
            float f11 = rVar.f5062c;
            float f12 = this.v;
            int i10 = i3 + 1;
            fArr[i3] = f2;
            int i11 = i10 + 1;
            fArr[i10] = f3;
            int i12 = i11 + 1;
            fArr[i11] = f12;
            int i13 = i12 + 1;
            fArr[i12] = f8;
            int i14 = i13 + 1;
            fArr[i13] = f9;
            int i15 = i14 + 1;
            fArr[i14] = f2;
            int i16 = i15 + 1;
            fArr[i15] = f7;
            int i17 = i16 + 1;
            fArr[i16] = f12;
            int i18 = i17 + 1;
            fArr[i17] = f8;
            int i19 = i18 + 1;
            fArr[i18] = f11;
            int i20 = i19 + 1;
            fArr[i19] = f6;
            int i21 = i20 + 1;
            fArr[i20] = f7;
            int i22 = i21 + 1;
            fArr[i21] = f12;
            int i23 = i22 + 1;
            fArr[i22] = f10;
            int i24 = i23 + 1;
            fArr[i23] = f11;
            int i25 = i24 + 1;
            fArr[i24] = f6;
            int i26 = i25 + 1;
            fArr[i25] = f3;
            int i27 = i26 + 1;
            fArr[i26] = f12;
            int i28 = i27 + 1;
            fArr[i27] = f10;
            fArr[i28] = f9;
            this.f5008d = i28 + 1;
            return;
        }
        throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
    }

    public void draw(r rVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10) {
        float f11;
        float f12;
        float f13;
        float f14;
        float f15;
        r rVar2 = rVar;
        float f16 = f4;
        float f17 = f5;
        if (this.f5013i) {
            short[] sArr = this.f5007c;
            float[] fArr = this.f5006b;
            e.d.b.t.n nVar = rVar2.f5060a;
            if (nVar != this.f5010f) {
                a(nVar);
            } else if (this.f5009e + 6 > sArr.length || this.f5008d + 20 > fArr.length) {
                flush();
            }
            int i2 = this.f5009e;
            int i3 = this.f5008d / 5;
            int i4 = i2 + 1;
            short s2 = (short) i3;
            sArr[i2] = s2;
            int i5 = i4 + 1;
            sArr[i4] = (short) (i3 + 1);
            int i6 = i5 + 1;
            short s3 = (short) (i3 + 2);
            sArr[i5] = s3;
            int i7 = i6 + 1;
            sArr[i6] = s3;
            int i8 = i7 + 1;
            sArr[i7] = (short) (i3 + 3);
            sArr[i8] = s2;
            this.f5009e = i8 + 1;
            float f18 = f2 + f16;
            float f19 = f3 + f17;
            float f20 = -f16;
            float f21 = -f17;
            float f22 = f6 - f16;
            float f23 = f7 - f17;
            if (!(f8 == 1.0f && f9 == 1.0f)) {
                f20 *= f8;
                f21 *= f9;
                f22 *= f8;
                f23 *= f9;
            }
            if (f10 != Animation.CurveTimeline.LINEAR) {
                float b2 = h.b(f10);
                float h2 = h.h(f10);
                float f24 = b2 * f20;
                float f25 = f24 - (h2 * f21);
                float f26 = f20 * h2;
                f21 = (f21 * b2) + f26;
                float f27 = h2 * f23;
                f12 = f24 - f27;
                float f28 = f23 * b2;
                float f29 = f26 + f28;
                f14 = (b2 * f22) - f27;
                float f30 = f28 + (h2 * f22);
                float f31 = f30 - (f29 - f21);
                f13 = (f14 - f12) + f25;
                f15 = f30;
                f23 = f29;
                f20 = f25;
                f11 = f31;
            } else {
                f14 = f22;
                f13 = f14;
                f15 = f23;
                f12 = f20;
                f11 = f21;
            }
            float f32 = f20 + f18;
            float f33 = f21 + f19;
            float f34 = f12 + f18;
            float f35 = f23 + f19;
            float f36 = f14 + f18;
            float f37 = f15 + f19;
            float f38 = f13 + f18;
            float f39 = f11 + f19;
            float f40 = rVar2.f5061b;
            float f41 = rVar2.f5064e;
            float f42 = rVar2.f5063d;
            float f43 = rVar2.f5062c;
            float f44 = this.v;
            int i9 = this.f5008d;
            int i10 = i9 + 1;
            fArr[i9] = f32;
            int i11 = i10 + 1;
            fArr[i10] = f33;
            int i12 = i11 + 1;
            fArr[i11] = f44;
            int i13 = i12 + 1;
            fArr[i12] = f40;
            int i14 = i13 + 1;
            fArr[i13] = f41;
            int i15 = i14 + 1;
            fArr[i14] = f34;
            int i16 = i15 + 1;
            fArr[i15] = f35;
            int i17 = i16 + 1;
            fArr[i16] = f44;
            int i18 = i17 + 1;
            fArr[i17] = f40;
            int i19 = i18 + 1;
            fArr[i18] = f43;
            int i20 = i19 + 1;
            fArr[i19] = f36;
            int i21 = i20 + 1;
            fArr[i20] = f37;
            int i22 = i21 + 1;
            fArr[i21] = f44;
            int i23 = i22 + 1;
            fArr[i22] = f42;
            int i24 = i23 + 1;
            fArr[i23] = f43;
            int i25 = i24 + 1;
            fArr[i24] = f38;
            int i26 = i25 + 1;
            fArr[i25] = f39;
            int i27 = i26 + 1;
            fArr[i26] = f44;
            int i28 = i27 + 1;
            fArr[i27] = f42;
            fArr[i28] = f41;
            this.f5008d = i28 + 1;
            return;
        }
        throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
    }
}
